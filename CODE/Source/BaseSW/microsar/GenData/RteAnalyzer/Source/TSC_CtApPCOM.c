/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPCOM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApPCOM.h"
#include "TSC_CtApPCOM.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data)
{
  return Rte_Write_PpDCDC_CurrentReference_DCDC_CurrentReference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_Fault_DCDC_Fault(DCDC_Fault data)
{
  return Rte_Write_PpDCDC_Fault_DCDC_Fault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data)
{
  return Rte_Write_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
{
  return Rte_Write_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data)
{
  return Rte_Write_PpDCDC_InputVoltage_DCDC_InputVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data)
{
  return Rte_Write_PpDCDC_OVERTEMP_DCDC_OVERTEMP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data)
{
  return Rte_Write_PpDCDC_OutputVoltage_DCDC_OutputVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_Status_DCDC_Status(DCDC_Status data)
{
  return Rte_Write_PpDCDC_Status_DCDC_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_Temperature_DCDC_Temperature(DCDC_Temperature data)
{
  return Rte_Write_PpDCDC_Temperature_DCDC_Temperature(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data)
{
  return Rte_Write_PpDCDC_VoltageReference_DCDC_VoltageReference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data)
{
  return Rte_Write_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data)
{
  return Rte_Write_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd data)
{
  return Rte_Write_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag data)
{
  return Rte_Write_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt data)
{
  return Rte_Write_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status data)
{
  return Rte_Write_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage data)
{
  return Rte_Write_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt data)
{
  return Rte_Write_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_Fault_BMS_Fault(BMS_Fault data)
{
  return Rte_Write_PpInt_BMS_Fault_BMS_Fault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow data)
{
  return Rte_Write_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow data)
{
  return Rte_Write_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState data)
{
  return Rte_Write_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable data)
{
  return Rte_Write_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState data)
{
  return Rte_Write_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq data)
{
  return Rte_Write_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_SOC_BMS_SOC(BMS_SOC data)
{
  return Rte_Write_PpInt_BMS_SOC_BMS_SOC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt data)
{
  return Rte_Write_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage data)
{
  return Rte_Write_PpInt_BMS_Voltage_BMS_Voltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState data)
{
  return Rte_Write_PpInt_BSI_ChargeState_BSI_ChargeState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus data)
{
  return Rte_Write_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle data)
{
  return Rte_Write_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup data)
{
  return Rte_Write_PpInt_BSI_MainWakeup_BSI_MainWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup data)
{
  return Rte_Write_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup data)
{
  return Rte_Write_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode data)
{
  return Rte_Write_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(BSI_VCUSynchroGPC data)
{
  return Rte_Write_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP data)
{
  return Rte_Write_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 data)
{
  return Rte_Write_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC data)
{
  return Rte_Write_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG data)
{
  return Rte_Write_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_MODE_DIAG_MODE_DIAG(MODE_DIAG data)
{
  return Rte_Write_PpInt_MODE_DIAG_MODE_DIAG(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(VCU_AccPedalPosition data)
{
  return Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand data)
{
  return Rte_Write_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD data)
{
  return Rte_Write_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD data)
{
  return Rte_Write_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct data)
{
  return Rte_Write_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel data)
{
  return Rte_Write_PpInt_VCU_CptTemporel_VCU_CptTemporel(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation data)
{
  return Rte_Write_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq data)
{
  return Rte_Write_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM data)
{
  return Rte_Write_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller data)
{
  return Rte_Write_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM data)
{
  return Rte_Write_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt data)
{
  return Rte_Write_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(VCU_EPWT_Status data)
{
  return Rte_Write_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation data)
{
  return Rte_Write_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb data)
{
  return Rte_Write_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev data)
{
  return Rte_Write_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec data)
{
  return Rte_Write_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_Keyposition_VCU_Keyposition(VCU_Keyposition data)
{
  return Rte_Write_PpInt_VCU_Keyposition_VCU_Keyposition(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage data)
{
  return Rte_Write_PpInt_VCU_Kilometrage_VCU_Kilometrage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest data)
{
  return Rte_Write_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD data)
{
  return Rte_Write_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup data)
{
  return Rte_Write_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup data)
{
  return Rte_Write_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_ACRange_OBC_ACRange(OBC_ACRange data)
{
  return Rte_Write_PpOBC_ACRange_OBC_ACRange(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data)
{
  return Rte_Write_PpOBC_CP_connection_Status_OBC_CP_connection_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data)
{
  return Rte_Write_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data)
{
  return Rte_Write_PpOBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt data)
{
  return Rte_Write_PpOBC_CommunicationSt_OBC_CommunicationSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data)
{
  return Rte_Write_PpOBC_CoolingWakeup_OBC_CoolingWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data)
{
  return Rte_Write_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_ElockState_OBC_ElockState(OBC_ElockState data)
{
  return Rte_Write_PpOBC_ElockState_OBC_ElockState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_Fault_OBC_Fault(OBC_Fault data)
{
  return Rte_Write_PpOBC_Fault_OBC_Fault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data)
{
  return Rte_Write_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
{
  return Rte_Write_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data)
{
  return Rte_Write_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data)
{
  return Rte_Write_PpOBC_InputVoltageSt_OBC_InputVoltageSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data)
{
  return Rte_Write_PpOBC_OBCStartSt_OBC_OBCStartSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data)
{
  return Rte_Write_PpOBC_OBCTemp_OBC_OBCTemp(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data)
{
  return Rte_Write_PpOBC_OutputCurrent_OBC_OutputCurrent(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data)
{
  return Rte_Write_PpOBC_OutputVoltage_OBC_OutputVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data)
{
  return Rte_Write_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data)
{
  return Rte_Write_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_PowerMax_OBC_PowerMax(OBC_PowerMax data)
{
  return Rte_Write_PpOBC_PowerMax_OBC_PowerMax(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data)
{
  return Rte_Write_PpOBC_PushChargeType_OBC_PushChargeType(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data)
{
  return Rte_Write_PpOBC_RechargeHMIState_OBC_RechargeHMIState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data)
{
  return Rte_Write_PpOBC_SocketTemp_OBC_SocketTempL(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data)
{
  return Rte_Write_PpOBC_SocketTemp_OBC_SocketTempN(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBC_Status_OBC_Status(OBC_Status data)
{
  return Rte_Write_PpOBC_Status_OBC_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSG_DC2_SG_DC2(const SG_DC2 *data)
{
  return Rte_Write_PpSG_DC2_SG_DC2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data)
{
  return Rte_Write_PpSUPV_CoolingWupState_SUPV_CoolingWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data)
{
  return Rte_Write_PpSUPV_DTCRegistred_SUPV_DTCRegistred(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data)
{
  return Rte_Write_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data)
{
  return Rte_Write_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data)
{
  return Rte_Write_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data)
{
  return Rte_Write_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data)
{
  return Rte_Write_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data)
{
  return Rte_Write_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data)
{
  return Rte_Write_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data)
{
  return Rte_Write_PpSUPV_RCDLineState_SUPV_RCDLineState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data)
{
  return Rte_Write_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data)
{
  return Rte_Write_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data)
{
  return Rte_Write_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data)
{
  return Rte_Write_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data)
{
  return Rte_Write_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApPCOM_Rte_Read_PpABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag *data)
{
  return Rte_Read_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt *data)
{
  return Rte_Read_PpBMS_AuxBattVolt_BMS_AuxBattVolt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status *data)
{
  return Rte_Read_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
{
  return Rte_Read_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt *data)
{
  return Rte_Read_PpBMS_FastChargeSt_BMS_FastChargeSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_Fault_BMS_Fault(BMS_Fault *data)
{
  return Rte_Read_PpBMS_Fault_BMS_Fault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
{
  return Rte_Read_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
{
  return Rte_Read_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
{
  return Rte_Read_PpBMS_MainConnectorState_BMS_MainConnectorState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
{
  return Rte_Read_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState *data)
{
  return Rte_Read_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq *data)
{
  return Rte_Read_PpBMS_RelayOpenReq_BMS_RelayOpenReq(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_SOC_BMS_SOC(BMS_SOC *data)
{
  return Rte_Read_PpBMS_SOC_BMS_SOC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt *data)
{
  return Rte_Read_PpBMS_SlowChargeSt_BMS_SlowChargeSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBMS_Voltage_BMS_Voltage(BMS_Voltage *data)
{
  return Rte_Read_PpBMS_Voltage_BMS_Voltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data)
{
  return Rte_Read_PpBSI_ChargeState_BSI_ChargeState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus *data)
{
  return Rte_Read_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
{
  return Rte_Read_PpBSI_LockedVehicle_BSI_LockedVehicle(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
{
  return Rte_Read_PpBSI_MainWakeup_BSI_MainWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data)
{
  return Rte_Read_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data)
{
  return Rte_Read_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode *data)
{
  return Rte_Read_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC(BSI_VCUSynchroGPC *data)
{
  return Rte_Read_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
{
  return Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA0(DATA0 *data)
{
  return Rte_Read_PpBootEnterData_DATA0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA1(DATA1 *data)
{
  return Rte_Read_PpBootEnterData_DATA1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA2(DATA2 *data)
{
  return Rte_Read_PpBootEnterData_DATA2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA3(DATA3 *data)
{
  return Rte_Read_PpBootEnterData_DATA3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA4(DATA4 *data)
{
  return Rte_Read_PpBootEnterData_DATA4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA5(DATA5 *data)
{
  return Rte_Read_PpBootEnterData_DATA5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA6(DATA6 *data)
{
  return Rte_Read_PpBootEnterData_DATA6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpBootEnterData_DATA7(DATA7 *data)
{
  return Rte_Read_PpBootEnterData_DATA7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP *data)
{
  return Rte_Read_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 *data)
{
  return Rte_Read_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 *data)
{
  return Rte_Read_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
{
  return Rte_Read_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data)
{
  return Rte_Read_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data)
{
  return Rte_Read_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
{
  return Rte_Read_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
{
  return Rte_Read_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
{
  return Rte_Read_PpDCHV_DCHVStatus_DCHV_DCHVStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V *data)
{
  return Rte_Read_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
{
  return Rte_Read_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
{
  return Rte_Read_PpDCLV_DCLVStatus_DCLV_DCLVStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
{
  return Rte_Read_PpDCLV_Input_Current_DCLV_Input_Current(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
{
  return Rte_Read_PpDCLV_Input_Voltage_DCLV_Input_Voltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
{
  return Rte_Read_PpDCLV_Measured_Current_DCLV_Measured_Current(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
{
  return Rte_Read_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT *data)
{
  return Rte_Read_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT *data)
{
  return Rte_Read_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT *data)
{
  return Rte_Read_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT *data)
{
  return Rte_Read_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT *data)
{
  return Rte_Read_PpDCLV_OT_FAULT_DCLV_OT_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT *data)
{
  return Rte_Read_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT *data)
{
  return Rte_Read_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT *data)
{
  return Rte_Read_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP *data)
{
  return Rte_Read_PpDCLV_PWM_STOP_DCLV_PWM_STOP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_Power_DCLV_Power(DCLV_Power *data)
{
  return Rte_Read_PpDCLV_Power_DCLV_Power(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK(DCLV_T_L_BUCK *data)
{
  return Rte_Read_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data)
{
  return Rte_Read_PpDCLV_T_PP_A_DCLV_T_PP_A(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data)
{
  return Rte_Read_PpDCLV_T_PP_B_DCLV_T_PP_B(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data)
{
  return Rte_Read_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data)
{
  return Rte_Read_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCLV_T_TX_PP_DCLV_T_TX_PP(DCLV_T_TX_PP *data)
{
  return Rte_Read_PpDCLV_T_TX_PP_DCLV_T_TX_PP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC *data)
{
  return Rte_Read_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG *data)
{
  return Rte_Read_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpMODE_DIAG_MODE_DIAG(MODE_DIAG *data)
{
  return Rte_Read_PpMODE_DIAG_MODE_DIAG(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 *data)
{
  return Rte_Read_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V *data)
{
  return Rte_Read_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
{
  return Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 *data)
{
  return Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 *data)
{
  return Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
{
  return Rte_Read_PpPFC_PFCStatus_PFC_PFCStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data)
{
  return Rte_Read_PpPFC_Temp_M1_C_PFC_Temp_M1_C(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_Temp_M3_C_PFC_Temp_M3_C(PFC_Temp_M3_C *data)
{
  return Rte_Read_PpPFC_Temp_M3_C_PFC_Temp_M3_C(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data)
{
  return Rte_Read_PpPFC_Temp_M4_C_PFC_Temp_M4_C(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
{
  return Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data)
{
  return Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data)
{
  return Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz *data)
{
  return Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz *data)
{
  return Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz *data)
{
  return Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
{
  return Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
{
  return Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
{
  return Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
{
  return Rte_Read_PpPFC_Vdclink_V_PFC_Vdclink_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(boolean *data)
{
  return Rte_Read_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
{
  return Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_AccPedalPosition_VCU_AccPedalPosition(VCU_AccPedalPosition *data)
{
  return Rte_Read_PpVCU_AccPedalPosition_VCU_AccPedalPosition(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand *data)
{
  return Rte_Read_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD *data)
{
  return Rte_Read_PpVCU_CDEAccJDD_VCU_CDEAccJDD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD *data)
{
  return Rte_Read_PpVCU_CDEApcJDD_VCU_CDEApcJDD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data)
{
  return Rte_Read_PpVCU_CompteurRazGct_VCU_CompteurRazGct(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
{
  return Rte_Read_PpVCU_CptTemporel_VCU_CptTemporel(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation *data)
{
  return Rte_Read_PpVCU_DCDCActivation_VCU_DCDCActivation(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq *data)
{
  return Rte_Read_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM *data)
{
  return Rte_Read_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller *data)
{
  return Rte_Read_PpVCU_DMDActivChiller_VCU_DMDActivChiller(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM *data)
{
  return Rte_Read_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
{
  return Rte_Read_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_EPWT_Status_VCU_EPWT_Status(VCU_EPWT_Status *data)
{
  return Rte_Read_PpVCU_EPWT_Status_VCU_EPWT_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation *data)
{
  return Rte_Read_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
{
  return Rte_Read_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev *data)
{
  return Rte_Read_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec *data)
{
  return Rte_Read_PpVCU_EtatReseauElec_VCU_EtatReseauElec(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_Keyposition_VCU_Keyposition(VCU_Keyposition *data)
{
  return Rte_Read_PpVCU_Keyposition_VCU_Keyposition(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage *data)
{
  return Rte_Read_PpVCU_Kilometrage_VCU_Kilometrage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
{
  return Rte_Read_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
{
  return Rte_Read_PpVCU_PosShuntJDD_VCU_PosShuntJDD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data)
{
  return Rte_Read_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data)
{
  return Rte_Read_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data);
}




Std_ReturnType TSC_CtApPCOM_Rte_Write_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(boolean data)
{
  return Rte_Write_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(boolean data)
{
  return Rte_Write_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpBusOFFEnableConditions_DeBusOFFEnableConditions(boolean data)
{
  return Rte_Write_PpBusOFFEnableConditions_DeBusOFFEnableConditions(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpBusOffCANFault_DeBusOffCANFault(boolean data)
{
  return Rte_Write_PpBusOffCANFault_DeBusOffCANFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(boolean data)
{
  return Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(boolean data)
{
  return Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(boolean data)
{
  return Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean data)
{
  return Rte_Write_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(boolean data)
{
  return Rte_Write_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(boolean data)
{
  return Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(boolean data)
{
  return Rte_Write_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(boolean data)
{
  return Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 data)
{
  return Rte_Write_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT data)
{
  return Rte_Write_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 data)
{
  return Rte_Write_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 data)
{
  return Rte_Write_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT data)
{
  return Rte_Write_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference data)
{
  return Rte_Write_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus data)
{
  return Rte_Write_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V data)
{
  return Rte_Write_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor data)
{
  return Rte_Write_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus data)
{
  return Rte_Write_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current data)
{
  return Rte_Write_PpInt_DCLV_Input_Current_DCLV_Input_Current(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage data)
{
  return Rte_Write_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current data)
{
  return Rte_Write_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage data)
{
  return Rte_Write_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT data)
{
  return Rte_Write_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP data)
{
  return Rte_Write_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_Power_DCLV_Power(DCLV_Power data)
{
  return Rte_Write_PpInt_DCLV_Power_DCLV_Power(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(DCLV_T_L_BUCK data)
{
  return Rte_Write_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A data)
{
  return Rte_Write_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B data)
{
  return Rte_Write_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A data)
{
  return Rte_Write_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B data)
{
  return Rte_Write_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(DCLV_T_TX_PP data)
{
  return Rte_Write_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V data)
{
  return Rte_Write_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 data)
{
  return Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 data)
{
  return Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 data)
{
  return Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus data)
{
  return Rte_Write_PpInt_PFC_PFCStatus_PFC_PFCStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C data)
{
  return Rte_Write_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(PFC_Temp_M3_C data)
{
  return Rte_Write_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C data)
{
  return Rte_Write_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz data)
{
  return Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz data)
{
  return Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz data)
{
  return Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V data)
{
  return Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V data)
{
  return Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V data)
{
  return Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V data)
{
  return Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V data)
{
  return Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V data)
{
  return Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V data)
{
  return Rte_Write_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpOBCFramesReception_Error_DeOBCFramesReception_Error(boolean data)
{
  return Rte_Write_PpOBCFramesReception_Error_DeOBCFramesReception_Error(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean data)
{
  return Rte_Write_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void)
{
  return Rte_Call_PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd();
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void)
{
  return Rte_Call_PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd();
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData)
{
  return Rte_Call_PpWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(frameData);
}


     /* Service calls */
Std_ReturnType TSC_CtApPCOM_Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_ReadBlock(DstPtr);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
{
  return Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_SetRamBlockStatus(RamBlockStatus);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_WriteBlock(SrcPtr);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpComMUserNeed_ECANUserRequest_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_PpComMUserNeed_ECANUserRequest_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
{
  return Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComMode);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
{
  return Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComMode);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
{
  return Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN();
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
{
  return Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN();
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_alive_WdgMSupervisedEntity_CheckpointReached(WdgM_CheckpointIdType CPID)
{
  return Rte_Call_alive_WdgMSupervisedEntity_CheckpointReached(CPID);
}
Std_ReturnType TSC_CtApPCOM_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
{
  return Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(CPID);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApPCOM_Rte_Read_PpCANComRequest_DeCANComRequest(sint32 *data)
{
  return Rte_Read_PpCANComRequest_DeCANComRequest(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(boolean *data)
{
  return Rte_Read_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(boolean *data)
{
  return Rte_Read_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(boolean *data)
{
  return Rte_Read_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(boolean *data)
{
  return Rte_Read_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(boolean *data)
{
  return Rte_Read_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpECU_WakeupMain_DeECU_WakeupMain(boolean *data)
{
  return Rte_Read_PpECU_WakeupMain_DeECU_WakeupMain(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(DCDC_ActivedischargeSt *data)
{
  return Rte_Read_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference *data)
{
  return Rte_Read_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_Fault_DCDC_Fault(DCDC_Fault *data)
{
  return Rte_Read_PpInt_DCDC_Fault_DCDC_Fault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest *data)
{
  return Rte_Read_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed *data)
{
  return Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(DCDC_InputCurrent *data)
{
  return Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage *data)
{
  return Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad *data)
{
  return Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(DCDC_OBCMainContactorReq *data)
{
  return Rte_Read_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(DCDC_OBCQuickChargeContactorReq *data)
{
  return Rte_Read_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP *data)
{
  return Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCDC_OutputCurrent *data)
{
  return Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage *data)
{
  return Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_Status_DCDC_Status(DCDC_Status *data)
{
  return Rte_Read_PpInt_DCDC_Status_DCDC_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature *data)
{
  return Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference *data)
{
  return Rte_Read_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor *data)
{
  return Rte_Read_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference *data)
{
  return Rte_Read_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_EDITION_CALIB_EDITION_CALIB(EDITION_CALIB *data)
{
  return Rte_Read_PpInt_EDITION_CALIB_EDITION_CALIB(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_EDITION_SOFT_EDITION_SOFT(EDITION_SOFT *data)
{
  return Rte_Read_PpInt_EDITION_SOFT_EDITION_SOFT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE *data)
{
  return Rte_Read_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 *data)
{
  return Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange *data)
{
  return Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status *data)
{
  return Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
{
  return Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt *data)
{
  return Rte_Read_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup *data)
{
  return Rte_Read_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf *data)
{
  return Rte_Read_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_ElockState_OBC_ElockState(OBC_ElockState *data)
{
  return Rte_Read_PpInt_OBC_ElockState_OBC_ElockState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
{
  return Rte_Read_PpInt_OBC_Fault_OBC_Fault(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup *data)
{
  return Rte_Read_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
{
  return Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup *data)
{
  return Rte_Read_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
{
  return Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt *data)
{
  return Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp *data)
{
  return Rte_Read_PpInt_OBC_OBCTemp_OBC_OBCTemp(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent *data)
{
  return Rte_Read_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage *data)
{
  return Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup *data)
{
  return Rte_Read_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
{
  return Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax *data)
{
  return Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType *data)
{
  return Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
{
  return Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL *data)
{
  return Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN *data)
{
  return Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
{
  return Rte_Read_PpInt_OBC_Status_OBC_Status(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState *data)
{
  return Rte_Read_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred *data)
{
  return Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD *data)
{
  return Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState *data)
{
  return Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState *data)
{
  return Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState *data)
{
  return Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState *data)
{
  return Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState *data)
{
  return Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState *data)
{
  return Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
{
  return Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState *data)
{
  return Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V *data)
{
  return Rte_Read_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus *data)
{
  return Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus *data)
{
  return Rte_Read_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV *data)
{
  return Rte_Read_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_VERSION_APPLI_VERSION_APPLI(VERSION_APPLI *data)
{
  return Rte_Read_PpInt_VERSION_APPLI_VERSION_APPLI(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_VERSION_SOFT_VERSION_SOFT(VERSION_SOFT *data)
{
  return Rte_Read_PpInt_VERSION_SOFT_VERSION_SOFT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME *data)
{
  return Rte_Read_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE *data)
{
  return Rte_Read_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR *data)
{
  return Rte_Read_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS *data)
{
  return Rte_Read_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(boolean *data)
{
  return Rte_Read_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(boolean *data)
{
  return Rte_Read_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(boolean *data)
{
  return Rte_Read_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(boolean *data)
{
  return Rte_Read_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
{
  return Rte_Read_PpPlantModeState_DePlantModeState(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Read_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(boolean *data)
{
  return Rte_Read_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(data);
}




Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_1_Debug1_1(Debug1_1 data)
{
  return Rte_Write_Debug1_1_Debug1_1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_2_Debug1_2(Debug1_2 data)
{
  return Rte_Write_Debug1_2_Debug1_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_3_Debug1_3(Debug1_3 data)
{
  return Rte_Write_Debug1_3_Debug1_3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_4_Debug1_4(Debug1_4 data)
{
  return Rte_Write_Debug1_4_Debug1_4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_5_Debug1_5(Debug1_5 data)
{
  return Rte_Write_Debug1_5_Debug1_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_6_Debug1_6(Debug1_6 data)
{
  return Rte_Write_Debug1_6_Debug1_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug1_7_Debug1_7(Debug1_7 data)
{
  return Rte_Write_Debug1_7_Debug1_7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_0_Debug2_0(Debug2_0 data)
{
  return Rte_Write_Debug2_0_Debug2_0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_1_Debug2_1(Debug2_1 data)
{
  return Rte_Write_Debug2_1_Debug2_1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_2_Debug2_2(Debug2_2 data)
{
  return Rte_Write_Debug2_2_Debug2_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_3_Debug2_3(Debug2_3 data)
{
  return Rte_Write_Debug2_3_Debug2_3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_4_Debug2_4(Debug2_4 data)
{
  return Rte_Write_Debug2_4_Debug2_4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_5_Debug2_5(Debug2_5 data)
{
  return Rte_Write_Debug2_5_Debug2_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_6_Debug2_6(Debug2_6 data)
{
  return Rte_Write_Debug2_6_Debug2_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug2_7_Debug2_7(Debug2_7 data)
{
  return Rte_Write_Debug2_7_Debug2_7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_0_Debug3_0(Debug3_0 data)
{
  return Rte_Write_Debug3_0_Debug3_0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_1_Debug3_1(Debug3_1 data)
{
  return Rte_Write_Debug3_1_Debug3_1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_2_Debug3_2(Debug3_2 data)
{
  return Rte_Write_Debug3_2_Debug3_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_3_Debug3_3(Debug3_3 data)
{
  return Rte_Write_Debug3_3_Debug3_3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_4_Debug3_4(Debug3_4 data)
{
  return Rte_Write_Debug3_4_Debug3_4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_5_Debug3_5(Debug3_5 data)
{
  return Rte_Write_Debug3_5_Debug3_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_6_Debug3_6(Debug3_6 data)
{
  return Rte_Write_Debug3_6_Debug3_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug3_7_Debug3_7(Debug3_7 data)
{
  return Rte_Write_Debug3_7_Debug3_7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_0_Debug4_0(Debug4_0 data)
{
  return Rte_Write_Debug4_0_Debug4_0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_1_Debug4_1(Debug4_1 data)
{
  return Rte_Write_Debug4_1_Debug4_1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_2_Debug4_2(Debug4_2 data)
{
  return Rte_Write_Debug4_2_Debug4_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_3_Debug4_3(Debug4_3 data)
{
  return Rte_Write_Debug4_3_Debug4_3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_4_Debug4_4(Debug4_4 data)
{
  return Rte_Write_Debug4_4_Debug4_4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_5_Debug4_5(Debug4_5 data)
{
  return Rte_Write_Debug4_5_Debug4_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_6_Debug4_6(Debug4_6 data)
{
  return Rte_Write_Debug4_6_Debug4_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Debug4_7_Debug4_7(Debug4_7 data)
{
  return Rte_Write_Debug4_7_Debug4_7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_Degug1_0_Degug1_0(Degug1_0 data)
{
  return Rte_Write_Degug1_0_Degug1_0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpDCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data)
{
  return Rte_Write_PpDCLV_VoltageReference_DCLV_VoltageReference(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpEDITION_CALIB_EDITION_CALIB(EDITION_CALIB data)
{
  return Rte_Write_PpEDITION_CALIB_EDITION_CALIB(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpEDITION_SOFT_EDITION_SOFT(EDITION_SOFT data)
{
  return Rte_Write_PpEDITION_SOFT_EDITION_SOFT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data)
{
  return Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpVERSION_APPLI_VERSION_APPLI(VERSION_APPLI data)
{
  return Rte_Write_PpVERSION_APPLI_VERSION_APPLI(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpVERSION_SOFT_VERSION_SOFT(VERSION_SOFT data)
{
  return Rte_Write_PpVERSION_SOFT_VERSION_SOFT(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpVERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME data)
{
  return Rte_Write_PpVERSION_SYSTEME_VERSION_SYSTEME(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE data)
{
  return Rte_Write_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR data)
{
  return Rte_Write_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR(data);
}

Std_ReturnType TSC_CtApPCOM_Rte_Write_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS data)
{
  return Rte_Write_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






uint32  TSC_CtApPCOM_Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_REFERENCE_HORAIRE(void)
{
  return (uint32 ) Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_REFERENCE_HORAIRE();
}
uint32  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL(void)
{
  return (uint32 ) Rte_CData_CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL();
}
uint32  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE(void)
{
  return (uint32 ) Rte_CData_CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE();
}
uint32  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL(void)
{
  return (uint32 ) Rte_CData_CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_SOC(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_SOC();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_Voltage(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_Voltage();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE(void)
{
  return (uint16 ) Rte_CData_CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage(void)
{
  return (uint16 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent(void)
{
  return (uint16 ) Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent(void)
{
  return (uint16 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage(void)
{
  return (uint16 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd(void)
{
  return (uint16 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd();
}
uint16  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd(void)
{
  return (uint16 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_0F0_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_0F0_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_0F0_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_0F0_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_125_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_125_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_125_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_125_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_127_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_127_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_127_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_127_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_129_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_129_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_129_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_129_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_17B_ChkSumConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_17B_ChkSumConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_17B_ChkSumHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_17B_ChkSumHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_17B_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_17B_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_17B_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_17B_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_17B_RCConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_17B_RCConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_17B_RCHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_17B_RCHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_27A_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_27A_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_27A_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_27A_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_31B_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_31B_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_31B_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_31B_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_31E_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_31E_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_31E_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_31E_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_359_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_359_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_359_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_359_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_361_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_361_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_361_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_361_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_372_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_372_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_372_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_372_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_37E_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_37E_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_37E_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_37E_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_382_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_382_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_382_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_382_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_486_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_486_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_486_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_486_LostFrameHeal();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_552_LostFrameConfirm(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_552_LostFrameConfirm();
}
IdtFrameDiagTime  TSC_CtApPCOM_Rte_CData_CalFrame_552_LostFrameHeal(void)
{
  return (IdtFrameDiagTime ) Rte_CData_CalFrame_552_LostFrameHeal();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS5_359_Initial_Value_BMS_Fault(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS5_359_Initial_Value_BMS_Fault();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC(void)
{
  return (uint8 ) Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand(void)
{
  return (uint8 ) Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation(void)
{
  return (uint8 ) Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq(void)
{
  return (uint8 ) Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand(void)
{
  return (uint8 ) Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation(void)
{
  return (uint8 ) Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq(void)
{
  return (uint8 ) Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Fault(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Fault();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Status();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Temperature(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Temperature();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ(void)
{
  return (uint8 ) Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC(void)
{
  return (uint8 ) Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG(void)
{
  return (uint8 ) Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG(void)
{
  return (uint8 ) Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_NOMBRE_TRAMES(void)
{
  return (uint8 ) Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_NOMBRE_TRAMES();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Fault(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Fault();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Status();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status(void)
{
  return (uint8 ) Rte_CData_CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE(void)
{
  return (uint8 ) Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition();
}
uint8  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition(void)
{
  return (uint8 ) Rte_CData_CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition();
}
IdtSignalDiagTime  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition(void)
{
  return (IdtSignalDiagTime ) Rte_CData_CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition();
}
IdtT_INHIB_DIAG_COM  TSC_CtApPCOM_Rte_CData_CalT_INHIB_DIAG_COM(void)
{
  return (IdtT_INHIB_DIAG_COM ) Rte_CData_CalT_INHIB_DIAG_COM();
}
IdtTimeBeforeRunningDiag  TSC_CtApPCOM_Rte_CData_CalTimeBeforeRunningDiagBSIInfo(void)
{
  return (IdtTimeBeforeRunningDiag ) Rte_CData_CalTimeBeforeRunningDiagBSIInfo();
}
IdtTimeBeforeRunningDiag  TSC_CtApPCOM_Rte_CData_CalTimeBeforeRunningDiagEVCU(void)
{
  return (IdtTimeBeforeRunningDiag ) Rte_CData_CalTimeBeforeRunningDiagEVCU();
}
IdtTimeBeforeRunningDiag  TSC_CtApPCOM_Rte_CData_CalTimeBeforeRunningDiagTBMU(void)
{
  return (IdtTimeBeforeRunningDiag ) Rte_CData_CalTimeBeforeRunningDiagTBMU();
}
IdtTo_DgMux_Prod  TSC_CtApPCOM_Rte_CData_CalTo_DgMux_Prod(void)
{
  return (IdtTo_DgMux_Prod ) Rte_CData_CalTo_DgMux_Prod();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE(void)
{
  return (boolean ) Rte_CData_CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC(void)
{
  return (boolean ) Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand(void)
{
  return (boolean ) Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation(void)
{
  return (boolean ) Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq(void)
{
  return (boolean ) Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status(void)
{
  return (boolean ) Rte_CData_CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2();
}
boolean  TSC_CtApPCOM_Rte_CData_CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition(void)
{
  return (boolean ) Rte_CData_CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition();
}

     /* CtApPCOM */
      /* CtApPCOM */

/** Per Instance Memories */
uint32 *TSC_CtApPCOM_Rte_Pim_PimBMS1_LostFrameCounter(void)
{
  return Rte_Pim_PimBMS1_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimBMS3_LostFrameCounter(void)
{
  return Rte_Pim_PimBMS3_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimBMS5_LostFrameCounter(void)
{
  return Rte_Pim_PimBMS5_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimBMS6_LostFrameCounter(void)
{
  return Rte_Pim_PimBMS6_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimBMS8_LostFrameCounter(void)
{
  return Rte_Pim_PimBMS8_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimBMS9_LostFrameCounter(void)
{
  return Rte_Pim_PimBMS9_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimBSIInfo_LostFrameCounter(void)
{
  return Rte_Pim_PimBSIInfo_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimCtrlDCDC_LostFrameCounter(void)
{
  return Rte_Pim_PimCtrlDCDC_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimParkCommand_LostFrameCounter(void)
{
  return Rte_Pim_PimParkCommand_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimVCU2_LostFrameCounter(void)
{
  return Rte_Pim_PimVCU2_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimVCU3_LostFrameCounter(void)
{
  return Rte_Pim_PimVCU3_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimVCU_552_LostFrameCounter(void)
{
  return Rte_Pim_PimVCU_552_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimVCU_BSI_Wakeup_LostFrameCounter(void)
{
  return Rte_Pim_PimVCU_BSI_Wakeup_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimVCU_PCANInfo_LostFrameCounter(void)
{
  return Rte_Pim_PimVCU_PCANInfo_LostFrameCounter();
}
uint32 *TSC_CtApPCOM_Rte_Pim_PimVCU_TU_LostFrameCounter(void)
{
  return Rte_Pim_PimVCU_TU_LostFrameCounter();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimABS_VehSpd_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimABS_VehSpd_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimABS_VehSpd_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimABS_VehSpd_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_SOC_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimBMS_SOC_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_SOC_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimBMS_SOC_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_SOC_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimBMS_SOC_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_SOC_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimBMS_SOC_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_Voltage_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimBMS_Voltage_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBMS_Voltage_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimBMS_Voltage_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimKILOMETRAGE_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimKILOMETRAGE_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimKILOMETRAGE_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimKILOMETRAGE_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterHeal();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm(void)
{
  return Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm();
}
uint16 *TSC_CtApPCOM_Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal(void)
{
  return Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimABS_VehSpd_InvalidValuePrefault(void)
{
  return Rte_Pim_PimABS_VehSpd_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS1_LostFramePrefault(void)
{
  return Rte_Pim_PimBMS1_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS3_LostFramePrefault(void)
{
  return Rte_Pim_PimBMS3_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS5_LostFramePrefault(void)
{
  return Rte_Pim_PimBMS5_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS6_LostFramePrefault(void)
{
  return Rte_Pim_PimBMS6_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS8_LostFramePrefault(void)
{
  return Rte_Pim_PimBMS8_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS9_LostFramePrefault(void)
{
  return Rte_Pim_PimBMS9_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS_MainConnectorState_InvalidValuePrefault(void)
{
  return Rte_Pim_PimBMS_MainConnectorState_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS_SOC_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimBMS_SOC_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS_SOC_InvalidValuePrefault(void)
{
  return Rte_Pim_PimBMS_SOC_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS_Voltage_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimBMS_Voltage_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBMS_Voltage_InvalidValuePrefault(void)
{
  return Rte_Pim_PimBMS_Voltage_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBSIInfo_LostFramePrefault(void)
{
  return Rte_Pim_PimBSIInfo_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault(void)
{
  return Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimCPT_TEMPOREL_InvalidValuePrefault(void)
{
  return Rte_Pim_PimCPT_TEMPOREL_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimCtrlDCDC_LostFramePrefault(void)
{
  return Rte_Pim_PimCtrlDCDC_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimDDE_GMV_SEEM_InvalidValuePrefault(void)
{
  return Rte_Pim_PimDDE_GMV_SEEM_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimETAT_GMP_HYB_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimETAT_GMP_HYB_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimKILOMETRAGE_InvalidValuePrefault(void)
{
  return Rte_Pim_PimKILOMETRAGE_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimParkCommand_LostFramePrefault(void)
{
  return Rte_Pim_PimParkCommand_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU2_LostFramePrefault(void)
{
  return Rte_Pim_PimVCU2_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU3_LostFramePrefault(void)
{
  return Rte_Pim_PimVCU3_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_552_LostFramePrefault(void)
{
  return Rte_Pim_PimVCU_552_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_AccPedalPosition_InvalidValuePrefault(void)
{
  return Rte_Pim_PimVCU_AccPedalPosition_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_BSI_Wakeup_LostFramePrefault(void)
{
  return Rte_Pim_PimVCU_BSI_Wakeup_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCActivation_InvalidValuePrefault(void)
{
  return Rte_Pim_PimVCU_DCDCActivation_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValuePrefault(void)
{
  return Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_EPWT_Status_InvalidValuePrefault(void)
{
  return Rte_Pim_PimVCU_EPWT_Status_InvalidValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault(void)
{
  return Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_PCANInfo_LostFramePrefault(void)
{
  return Rte_Pim_PimVCU_PCANInfo_LostFramePrefault();
}
boolean *TSC_CtApPCOM_Rte_Pim_PimVCU_TU_LostFramePrefault(void)
{
  return Rte_Pim_PimVCU_TU_LostFramePrefault();
}
Rte_DT_IdtPCOMNvMArray_0 *TSC_CtApPCOM_Rte_Pim_NvPCOMBlockNeed_MirrorBlock(void)
{
  return Rte_Pim_NvPCOMBlockNeed_MirrorBlock();
}



