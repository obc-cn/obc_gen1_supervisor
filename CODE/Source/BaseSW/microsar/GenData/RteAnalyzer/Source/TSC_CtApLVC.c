/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLVC.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApLVC.h"
#include "TSC_CtApLVC.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
{
  return Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status data)
{
  return Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean data)
{
  return Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApLVC_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
{
  return Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpDCLVError_DeDCLVError(boolean *data)
{
  return Rte_Read_PpDCLVError_DeDCLVError(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data)
{
  return Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean *data)
{
  return Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean *data)
{
  return Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
{
  return Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
{
  return Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
{
  return Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
{
  return Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
{
  return Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
{
  return Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation *data)
{
  return Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq *data)
{
  return Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(data);
}




Std_ReturnType TSC_CtApLVC_Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result data)
{
  return Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(boolean data)
{
  return Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(boolean data)
{
  return Rte_Write_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
{
  return Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(DCDC_InputCurrent data)
{
  return Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_DCDC_Status_DCDC_Status(DCDC_Status data)
{
  return Rte_Write_PpInt_DCDC_Status_DCDC_Status(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data)
{
  return Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data)
{
  return Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(boolean data)
{
  return Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(data);
}

Std_ReturnType TSC_CtApLVC_Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(boolean data)
{
  return Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApLVC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}
Std_ReturnType TSC_CtApLVC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






uint16  TSC_CtApLVC_Rte_CData_CalDCLVDeratingThreshold(void)
{
  return (uint16 ) Rte_CData_CalDCLVDeratingThreshold();
}
IdtDCLVTimeConfirmDerating  TSC_CtApLVC_Rte_CData_CalDCLVTimeConfirmDerating(void)
{
  return (IdtDCLVTimeConfirmDerating ) Rte_CData_CalDCLVTimeConfirmDerating();
}

     /* CtApLVC */
      /* CtApLVC */

/** Per Instance Memories */
boolean *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathFSP(void)
{
  return Rte_Pim_PimDCLVShutdownPathFSP();
}
boolean *TSC_CtApLVC_Rte_Pim_PimDCLVShutdownPathGPIO(void)
{
  return Rte_Pim_PimDCLVShutdownPathGPIO();
}



