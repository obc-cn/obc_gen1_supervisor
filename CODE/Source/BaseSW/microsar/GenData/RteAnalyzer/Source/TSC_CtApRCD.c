/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApRCD.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApRCD.h"
#include "TSC_CtApRCD.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApRCD_Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean *data)
{
  return Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(data);
}




Std_ReturnType TSC_CtApRCD_Rte_Write_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data)
{
  return Rte_Write_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApRCD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtDebounceRCD  TSC_CtApRCD_Rte_CData_CalDebounceRCDHigh(void)
{
  return (IdtDebounceRCD ) Rte_CData_CalDebounceRCDHigh();
}
IdtDebounceRCD  TSC_CtApRCD_Rte_CData_CalDebounceRCDLow(void)
{
  return (IdtDebounceRCD ) Rte_CData_CalDebounceRCDLow();
}
boolean  TSC_CtApRCD_Rte_CData_CalCtrlFlowRCDLine(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowRCDLine();
}

     /* CtApRCD */
      /* CtApRCD */



