/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApFCL.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApFCL_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl *data);
Std_ReturnType TSC_CtApFCL_Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApFCL_Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultOC(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultSCG(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedBlueFaultSCP(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultOC(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultSCG(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedGreenFaultSCP(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultOC(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultSCG(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedFaults_DeLedRedFaultSCP(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(boolean data);
Std_ReturnType TSC_CtApFCL_Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApFCL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApFCL_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** Calibration Component Calibration Parameters */
IdtBatteryVoltMaxTest  TSC_CtApFCL_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void);
IdtBatteryVoltMinTest  TSC_CtApFCL_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void);
IdtVehStopMaxTest  TSC_CtApFCL_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void);

/** SW-C local Calibration Parameters */
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOff(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedBlueOn(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOff(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedGreenOn(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOff(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedPlugOn(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOff(void);
IdtAfts_DelayLed  TSC_CtApFCL_Rte_CData_CalAfts_DelayLedRedOn(void);
IdtLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCG(void);
IdtLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalLedBlueThresholdSCP(void);
IdtLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCG(void);
IdtLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalLedGreenThresholdSCP(void);
IdtLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCG(void);
IdtLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalLedRedThresholdSCP(void);
IdtMaxNormalPlugLed  TSC_CtApFCL_Rte_CData_CalMaxNormalPlugLed(void);
IdtMinNormalPlugLed  TSC_CtApFCL_Rte_CData_CalMinNormalPlugLed(void);
IdtPlugLedMaxOCDetection  TSC_CtApFCL_Rte_CData_CalPlugLedMaxOCDetection(void);
IdtPlugLedMinOCDetection  TSC_CtApFCL_Rte_CData_CalPlugLedMinOCDetection(void);
IdtPlugLedThresholdSCG  TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCG(void);
IdtPlugLedThresholdSCP  TSC_CtApFCL_Rte_CData_CalPlugLedThresholdSCP(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueOCConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueOCHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCGConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCGHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCPConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedBlueSCPHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenOCConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenOCHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCGConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCGHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCPConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedGreenSCPHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugOCConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugOCHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCGConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCGHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCPConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedPlugSCPHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedOCConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedOCHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCGConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCGHealTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCPConfirmTime(void);
IdtLedFaultTime  TSC_CtApFCL_Rte_CData_CalLedRedSCPHealTime(void);
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedBlue(void);
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedGreen(void);
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedPlug(void);
boolean  TSC_CtApFCL_Rte_CData_CalEnableLedRed(void);

/** Per Instance Memories */
uint16 *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterOC(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCG(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedBlueCounterSCP(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterOC(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCG(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedGreenCounterSCP(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterOC(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCG(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedPlugCounterSCP(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedRedCounterOC(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCG(void);
uint16 *TSC_CtApFCL_Rte_Pim_PimLedRedCounterSCP(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultOC(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCG(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedBluePrefaultSCP(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultOC(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCG(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedGreenPrefaultSCP(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultOC(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCG(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedPlugPrefaultSCP(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultOC(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCG(void);
boolean *TSC_CtApFCL_Rte_Pim_PimLedRedPrefaultSCP(void);



