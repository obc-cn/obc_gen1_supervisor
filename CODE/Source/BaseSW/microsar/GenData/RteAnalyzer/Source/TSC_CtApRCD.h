/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApRCD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApRCD_Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApRCD_Rte_Write_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data);

/** Client server interfaces */
Std_ReturnType TSC_CtApRCD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtDebounceRCD  TSC_CtApRCD_Rte_CData_CalDebounceRCDHigh(void);
IdtDebounceRCD  TSC_CtApRCD_Rte_CData_CalDebounceRCDLow(void);
boolean  TSC_CtApRCD_Rte_CData_CalCtrlFlowRCDLine(void);




