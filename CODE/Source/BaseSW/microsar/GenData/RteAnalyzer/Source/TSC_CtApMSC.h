/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApMSC.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_DCLV_Power_DCLV_Power(DCLV_Power *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC *data);
Std_ReturnType TSC_CtApMSC_Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data);
Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data);
Std_ReturnType TSC_CtApMSC_Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint data);
Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad data);
Std_ReturnType TSC_CtApMSC_Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax data);

/** Client server interfaces */
Std_ReturnType TSC_CtApMSC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApMSC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);
Std_ReturnType TSC_CtApMSC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApMSC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Service interfaces */
Std_ReturnType TSC_CtApMSC_Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr);
Std_ReturnType TSC_CtApMSC_Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr);

/** Calibration Component Calibration Parameters */
IdtInputVoltageThreshold  TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(void);

/** SW-C local Calibration Parameters */
IdtOBC_PowerMaxValue  TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(void);
IdtTimeLockDelay  TSC_CtApMSC_Rte_CData_CalTimeLockDelay(void);

/** Per Instance Memories */
IdtOBC_PowerAvailableControlPilot *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot(void);
IdtOBC_PowerAvailableHardware *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware(void);
uint8 *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag(void);
IdtOBC_PowerMaxCalculated *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated(void);



