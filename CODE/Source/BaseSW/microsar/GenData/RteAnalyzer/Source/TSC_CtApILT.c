/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApILT.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApILT.h"
#include "TSC_CtApILT.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApILT_Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(boolean *data)
{
  return Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(data);
}




Std_ReturnType TSC_CtApILT_Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data)
{
  return Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data)
{
  return Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus *data)
{
  return Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
{
  return Rte_Read_PpInt_OBC_Fault_OBC_Fault(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation *data)
{
  return Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
{
  return Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeChargeError(boolean *data)
{
  return Rte_Read_PpLedModes_PlantMode_DeChargeError(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(boolean *data)
{
  return Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(boolean *data)
{
  return Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeGuideManagement(boolean *data)
{
  return Rte_Read_PpLedModes_PlantMode_DeGuideManagement(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(boolean *data)
{
  return Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
{
  return Rte_Read_PpPlantModeState_DePlantModeState(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE *data)
{
  return Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(data);
}




Std_ReturnType TSC_CtApILT_Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data)
{
  return Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data);
}

Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeChargeError(boolean data)
{
  return Rte_Write_PpLedModes_DeChargeError(data);
}

Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeChargeInProgress(boolean data)
{
  return Rte_Write_PpLedModes_DeChargeInProgress(data);
}

Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeEndOfCharge(boolean data)
{
  return Rte_Write_PpLedModes_DeEndOfCharge(data);
}

Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeGuideManagement(boolean data)
{
  return Rte_Write_PpLedModes_DeGuideManagement(data);
}

Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeProgrammingCharge(boolean data)
{
  return Rte_Write_PpLedModes_DeProgrammingCharge(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApILT_Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean *data)
{
  return Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(data);
}

Std_ReturnType TSC_CtApILT_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
{
  return Rte_Read_PpOutputELockSensor_DeOutputELockSensor(data);
}




Std_ReturnType TSC_CtApILT_Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(boolean data)
{
  return Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(data);
}

Std_ReturnType TSC_CtApILT_Rte_Write_PpLockStateError_DeLockStateError(boolean data)
{
  return Rte_Write_PpLockStateError_DeLockStateError(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtMaxTimeChargeError  TSC_CtApILT_Rte_CData_CalMaxTimeChargeError(void)
{
  return (IdtMaxTimeChargeError ) Rte_CData_CalMaxTimeChargeError();
}
IdtMaxTimeChargeInProgress  TSC_CtApILT_Rte_CData_CalMaxTimeChargeInProgress(void)
{
  return (IdtMaxTimeChargeInProgress ) Rte_CData_CalMaxTimeChargeInProgress();
}
IdtMaxTimeEndOfCharge  TSC_CtApILT_Rte_CData_CalMaxTimeEndOfCharge(void)
{
  return (IdtMaxTimeEndOfCharge ) Rte_CData_CalMaxTimeEndOfCharge();
}
IdtMaxTimeGuideManagement  TSC_CtApILT_Rte_CData_CalMaxTimeGuideManagement(void)
{
  return (IdtMaxTimeGuideManagement ) Rte_CData_CalMaxTimeGuideManagement();
}
IdtMaxTimeProgrammingCharge  TSC_CtApILT_Rte_CData_CalMaxTimeProgrammingCharge(void)
{
  return (IdtMaxTimeProgrammingCharge ) Rte_CData_CalMaxTimeProgrammingCharge();
}
IdtTimePushButtonKeepValue  TSC_CtApILT_Rte_CData_CalTimePushButtonKeepValue(void)
{
  return (IdtTimePushButtonKeepValue ) Rte_CData_CalTimePushButtonKeepValue();
}
IdtDebounceStateFailure  TSC_CtApILT_Rte_CData_CalDebounceStateFailure(void)
{
  return (IdtDebounceStateFailure ) Rte_CData_CalDebounceStateFailure();
}
IdtDebounceStateFinished  TSC_CtApILT_Rte_CData_CalDebounceStateFinished(void)
{
  return (IdtDebounceStateFinished ) Rte_CData_CalDebounceStateFinished();
}
IdtDebounceStateInProgress  TSC_CtApILT_Rte_CData_CalDebounceStateInProgress(void)
{
  return (IdtDebounceStateInProgress ) Rte_CData_CalDebounceStateInProgress();
}
IdtDebounceStateStopped  TSC_CtApILT_Rte_CData_CalDebounceStateStopped(void)
{
  return (IdtDebounceStateStopped ) Rte_CData_CalDebounceStateStopped();
}
IdtElockTimeError  TSC_CtApILT_Rte_CData_CalElockTimeError(void)
{
  return (IdtElockTimeError ) Rte_CData_CalElockTimeError();
}

     /* CtApILT */
      /* CtApILT */



