/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApPSH.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApPSH.h"
#include "TSC_CtApPSH.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApPSH_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApPSH_Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(boolean *data)
{
  return Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(data);
}

Std_ReturnType TSC_CtApPSH_Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
{
  return Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(data);
}

Std_ReturnType TSC_CtApPSH_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
{
  return Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data);
}

Std_ReturnType TSC_CtApPSH_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
{
  return Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(data);
}




Std_ReturnType TSC_CtApPSH_Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(boolean data)
{
  return Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApPSH_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */
Std_ReturnType TSC_CtApPSH_Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
{
  return Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(RamBlockStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtTimePushInhDelay  TSC_CtApPSH_Rte_CData_CalTimePushInhDelay(void)
{
  return (IdtTimePushInhDelay ) Rte_CData_CalTimePushInhDelay();
}
IdtTimeDebChargePush  TSC_CtApPSH_Rte_CData_CalTimeDebChargePushHigh(void)
{
  return (IdtTimeDebChargePush ) Rte_CData_CalTimeDebChargePushHigh();
}
IdtTimeDebChargePush  TSC_CtApPSH_Rte_CData_CalTimeDebChargePushLow(void)
{
  return (IdtTimeDebChargePush ) Rte_CData_CalTimeDebChargePushLow();
}
boolean  TSC_CtApPSH_Rte_CData_CalCtrlFlowChargePush(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowChargePush();
}

     /* CtApPSH */
      /* CtApPSH */

/** Per Instance Memories */
Rte_DT_IdtPSHNvMArray_0 *TSC_CtApPSH_Rte_Pim_NvPSHBlockNeed_MirrorBlock(void)
{
  return Rte_Pim_NvPSHBlockNeed_MirrorBlock();
}



