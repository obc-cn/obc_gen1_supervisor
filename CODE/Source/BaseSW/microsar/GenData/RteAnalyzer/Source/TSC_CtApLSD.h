/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLSD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApLSD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApLSD_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApLSD_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data);
Std_ReturnType TSC_CtApLSD_Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(IdtELockSetPoint *data);
Std_ReturnType TSC_CtApLSD_Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApLSD_Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(boolean data);
Std_ReturnType TSC_CtApLSD_Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(boolean data);
Std_ReturnType TSC_CtApLSD_Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApLSD_Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(OBC_ElockState data);
Std_ReturnType TSC_CtApLSD_Rte_Write_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor data);

/** Client server interfaces */
Std_ReturnType TSC_CtApLSD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtELockDebounce  TSC_CtApLSD_Rte_CData_CalELockDebounce(void);
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorDriveHigh(void);
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorDriveLow(void);
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorLockHigh(void);
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorLockLow(void);
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorUnlockHigh(void);
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorUnlockLow(void);
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCGConfirmTime(void);
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCGHealTime(void);
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCPConfirmTime(void);
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCPHealTime(void);
IdtELockSensorFaultThreshold  TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCG(void);
IdtELockSensorFaultThreshold  TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCP(void);
boolean  TSC_CtApLSD_Rte_CData_CalCtrlFlowPlugLock(void);

/** Per Instance Memories */
uint16 *TSC_CtApLSD_Rte_Pim_PimElockCounterSCG(void);
uint16 *TSC_CtApLSD_Rte_Pim_PimElockCounterSCP(void);
boolean *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCG(void);
boolean *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCP(void);



