/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApDGN.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApDGN.h"
#include "TSC_CtApDGN.h"








Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(VCU_AccPedalPosition *data)
{
  return Rte_Read_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
{
  return Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage *data)
{
  return Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
{
  return Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean *data)
{
  return Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage *data)
{
  return Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(DCDC_InputCurrent *data)
{
  return Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_Status_DCDC_Status(DCDC_Status *data)
{
  return Rte_Read_PpInt_DCDC_Status_DCDC_Status(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage *data)
{
  return Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCDC_OutputCurrent *data)
{
  return Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
{
  return Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data)
{
  return Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType *data)
{
  return Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
{
  return Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad *data)
{
  return Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP *data)
{
  return Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw *data)
{
  return Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_ElockState_OBC_ElockState(OBC_ElockState *data)
{
  return Rte_Read_PpInt_OBC_ElockState_OBC_ElockState(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL *data)
{
  return Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN *data)
{
  return Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl *data)
{
  return Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl *data)
{
  return Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl *data)
{
  return Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl *data)
{
  return Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl *data)
{
  return Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl *data)
{
  return Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl *data)
{
  return Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl *data)
{
  return Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
{
  return Rte_Read_PpInt_OBC_Status_OBC_Status(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_BMS_SOC_BMS_SOC(BMS_SOC *data)
{
  return Rte_Read_PpInt_BMS_SOC_BMS_SOC(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data)
{
  return Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
{
  return Rte_Read_PpDutyControlPilot_DeDutyControlPilot(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
{
  return Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */
uint8 TSC_CtApDGN_Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void)
{
  return Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl();
}




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_CpApDGN_DGN_EOL_NVM_Needs_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_PS_CpApDGN_DGN_EOL_NVM_Needs_WriteBlock(SrcPtr);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(IdtPlantModeTestInfoDID_Result *data)
{
  return Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(IdtPlantModeTestInfoDID_Test *data)
{
  return Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(IdtPlantModeTestInfoDID_Test *data)
{
  return Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(IdtPlantModeTestInfoDID_Test *data)
{
  return Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(IdtPlantModeTestInfoDID_Test *data)
{
  return Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(IdtPlantModeTestInfoDID_State *data)
{
  return Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 *data)
{
  return Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 *data)
{
  return Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data)
{
  return Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApDGN_Rte_Call_PpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(uint8 *data)
{
  return Rte_Call_PpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApDGN_Rte_Call_PpLFMGetDCLVHWFaults_OpLFMGetDCLVHWFaults(uint8 *data)
{
  return Rte_Call_PpLFMGetDCLVHWFaults_OpLFMGetDCLVHWFaults(data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data)
{
  return Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(PFC_Temp_M3_C *data)
{
  return Rte_Read_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApDGN_Rte_Call_PpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(uint8 *data)
{
  return Rte_Call_PpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data)
{
  return Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data)
{
  return Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data)
{
  return Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data)
{
  return Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data)
{
  return Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data)
{
  return Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(DCLV_T_L_BUCK *data)
{
  return Rte_Read_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(DCLV_T_TX_PP *data)
{
  return Rte_Read_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(boolean *data)
{
  return Rte_Read_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(boolean *data)
{
  return Rte_Read_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data)
{
  return Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
{
  return Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
{
  return Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
{
  return Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data);
}








     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtApDGN_Rte_Call_ClearDTC_DemClient_CDD_ClearDTC(void)
{
  return Rte_Call_ClearDTC_DemClient_CDD_ClearDTC();
}
Std_ReturnType TSC_CtApDGN_Rte_Call_ClearDTC_DemClient_CDD_SelectDTC(uint32 DTC, Dem_DTCFormatType DTCFormat, Dem_DTCOriginType DTCOrigin)
{
  return Rte_Call_ClearDTC_DemClient_CDD_SelectDTC(DTC, DTCFormat, DTCOrigin);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_EDITION_CALIB_EDITION_CALIB(EDITION_CALIB data)
{
  return Rte_Write_PpInt_EDITION_CALIB_EDITION_CALIB(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_EDITION_SOFT_EDITION_SOFT(EDITION_SOFT data)
{
  return Rte_Write_PpInt_EDITION_SOFT_EDITION_SOFT(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_VERSION_APPLI_VERSION_APPLI(VERSION_APPLI data)
{
  return Rte_Write_PpInt_VERSION_APPLI_VERSION_APPLI(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_VERSION_SOFT_VERSION_SOFT(VERSION_SOFT data)
{
  return Rte_Write_PpInt_VERSION_SOFT_VERSION_SOFT(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME data)
{
  return Rte_Write_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE data)
{
  return Rte_Write_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR data)
{
  return Rte_Write_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS data)
{
  return Rte_Write_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApDGN_Rte_Call_PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx(void)
{
  return Rte_Call_PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx();
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApDGN_Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(boolean *data)
{
  return Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
{
  return Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(boolean *data)
{
  return Rte_Read_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
{
  return Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(boolean *data)
{
  return Rte_Read_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpBusOFFEnableConditions_DeBusOFFEnableConditions(boolean *data)
{
  return Rte_Read_PpBusOFFEnableConditions_DeBusOFFEnableConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpBusOffCANFault_DeBusOffCANFault(boolean *data)
{
  return Rte_Read_PpBusOffCANFault_DeBusOffCANFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpCANComRequest_DeCANComRequest(sint32 *data)
{
  return Rte_Read_PpCANComRequest_DeCANComRequest(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(boolean *data)
{
  return Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(boolean *data)
{
  return Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(boolean *data)
{
  return Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpControlPilotFreqError_DeControlPilotFreqError(boolean *data)
{
  return Rte_Read_PpControlPilotFreqError_DeControlPilotFreqError(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(boolean *data)
{
  return Rte_Read_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(boolean *data)
{
  return Rte_Read_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(boolean *data)
{
  return Rte_Read_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean *data)
{
  return Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(boolean *data)
{
  return Rte_Read_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(boolean *data)
{
  return Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(boolean *data)
{
  return Rte_Read_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(boolean *data)
{
  return Rte_Read_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(boolean *data)
{
  return Rte_Read_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(boolean *data)
{
  return Rte_Read_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(boolean *data)
{
  return Rte_Read_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(boolean *data)
{
  return Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean *data)
{
  return Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockHFaultOpenCircuit(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockHFaultOpenCircuit(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockHFaultSCG(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockHFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockHFaultSCP(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockHFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockLFaultOpenCircuit(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockLFaultOpenCircuit(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockLFaultSCG(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockLFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockLFaultSCP(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockLFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockFaults_DeOutputElockOvercurrent(boolean *data)
{
  return Rte_Read_PpELockFaults_DeOutputElockOvercurrent(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(boolean *data)
{
  return Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(boolean *data)
{
  return Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(boolean *data)
{
  return Rte_Read_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpElockFaultDetected_DeElockFaultDetected(boolean *data)
{
  return Rte_Read_PpElockFaultDetected_DeElockFaultDetected(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(boolean *data)
{
  return Rte_Read_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(boolean *data)
{
  return Rte_Read_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(boolean *data)
{
  return Rte_Read_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpFaultRCDLineSC_DeFaultRCDLineSC(boolean *data)
{
  return Rte_Read_PpFaultRCDLineSC_DeFaultRCDLineSC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(boolean *data)
{
  return Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
{
  return Rte_Read_PpInputVoltageMode_DeInputVoltageMode(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
{
  return Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(BSI_VCUSynchroGPC *data)
{
  return Rte_Read_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature *data)
{
  return Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
{
  return Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage *data)
{
  return Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus *data)
{
  return Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation *data)
{
  return Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
{
  return Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(VCU_EPWT_Status *data)
{
  return Rte_Read_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedBlueFaultOC(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedBlueFaultOC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedBlueFaultSCG(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedBlueFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedBlueFaultSCP(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedBlueFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedGreenFaultOC(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedGreenFaultOC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedGreenFaultSCG(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedGreenFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedGreenFaultSCP(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedGreenFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedRedFaultOC(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedRedFaultOC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedRedFaultSCG(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedRedFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedFaults_DeLedRedFaultSCP(boolean *data)
{
  return Rte_Read_PpLedFaults_DeLedRedFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(boolean *data)
{
  return Rte_Read_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(boolean *data)
{
  return Rte_Read_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedMonitoringConditions_DePlugLedMonitoringConditions(boolean *data)
{
  return Rte_Read_PpLedMonitoringConditions_DePlugLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpLedMonitoringConditions_DeRedLedMonitoringConditions(boolean *data)
{
  return Rte_Read_PpLedMonitoringConditions_DeRedLedMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(boolean *data)
{
  return Rte_Read_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(boolean *data)
{
  return Rte_Read_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(boolean *data)
{
  return Rte_Read_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeInlet_OvertempACSensorFault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeInlet_OvertempACSensorFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeOBC_HWErrors_Fault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeOBC_HWErrors_Fault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeOBC_InternalComFault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeOBC_InternalComFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFaultsList_DeOBC_OvertemperatureFault(boolean *data)
{
  return Rte_Read_PpOBCFaultsList_DeOBC_OvertemperatureFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(boolean *data)
{
  return Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(boolean *data)
{
  return Rte_Read_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputRelayMono_DeOutputRelayMono(boolean *data)
{
  return Rte_Read_PpOutputRelayMono_DeOutputRelayMono(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
{
  return Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlugLedFaults_DeLedPlugFaultOC(boolean *data)
{
  return Rte_Read_PpPlugLedFaults_DeLedPlugFaultOC(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlugLedFaults_DeLedPlugFaultSCG(boolean *data)
{
  return Rte_Read_PpPlugLedFaults_DeLedPlugFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpPlugLedFaults_DeLedPlugFaultSCP(boolean *data)
{
  return Rte_Read_PpPlugLedFaults_DeLedPlugFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean *data)
{
  return Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempAC1FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempAC1FaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempAC1FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempAC1FaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempAC2FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempAC2FaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempAC2FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempAC2FaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempAC3FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempAC3FaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempAC3FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempAC3FaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempACNFaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempACNFaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempACNFaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempACNFaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempDC1FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC1FaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempDC1FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC1FaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempDC2FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC2FaultSCG(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempFaults_DeTempDC2FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC2FaultSCP(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(boolean *data)
{
  return Rte_Read_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(boolean *data)
{
  return Rte_Read_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(boolean *data)
{
  return Rte_Read_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempMonitoringConditions_DeACNTempMonitoringConditions(boolean *data)
{
  return Rte_Read_PpTempMonitoringConditions_DeACNTempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(boolean *data)
{
  return Rte_Read_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApDGN_Rte_Read_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(boolean *data)
{
  return Rte_Read_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(data);
}




Std_ReturnType TSC_CtApDGN_Rte_Write_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt data)
{
  return Rte_Write_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableConditionDTC_0x10C413_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableConditionDTC_0x10C413_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableConditionDTC_0x10C512_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableConditionDTC_0x10C512_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableConditionDTC_0x10C613_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableConditionDTC_0x10C613_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableConditionDTC_0x10C713_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableConditionDTC_0x10C713_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableConditionDTC_0x12e912_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableConditionDTC_0x12e912_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableConditionDTC_0x12f316_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableConditionDTC_0x12f316_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x056216_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x056216_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x056317_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x056317_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x060251_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x060251_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x061055_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x061055_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a0804_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a0804_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a084b_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a084b_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a9464_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a9464_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x0af864_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x0af864_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x0cf464_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x0cf464_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x108093_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x108093_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120a11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120a11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120a12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120a12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120b11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120b11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120b12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120b12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120c64_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120c64_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120c98_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120c98_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120d64_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120d64_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x120d98_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x120d98_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d711_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d711_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d712_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d712_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d713_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d713_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d811_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d811_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d812_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d812_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d813_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d813_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d911_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d911_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d912_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d912_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d913_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d913_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da13_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da13_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12db12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12db12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12dc11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12dc11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12dd12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12dd12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12de11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12de11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12df13_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12df13_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e012_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e012_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e111_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e111_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e213_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e213_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e319_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e319_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e712_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e712_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e811_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e811_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12ea11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12ea11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x12f917_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x12f917_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x13e919_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x13e919_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x160a51_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x160a51_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x166C64_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x166C64_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x16e600_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x16e600_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x179e11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x179e11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x179e12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x179e12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x179f11_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x179f11_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x179f12_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x179f12_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a0064_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a0064_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a7104_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a7104_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a714b_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a714b_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a7172_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a7172_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xc07988_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xc07988_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xc08913_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xc08913_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xd18787_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xd18787_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xd1a087_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xd1a087_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xd20781_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xd20781_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xd2a081_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xd2a081_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xd38782_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xd38782_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xd38783_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xd38783_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00081_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00081_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00087_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00087_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00214_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00214_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00362_SetEnableCondition(boolean ConditionFulfilled)
{
  return Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00362_SetEnableCondition(ConditionFulfilled);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x056216_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x056216_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x056317_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x056317_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x060251_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x060251_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x061055_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x061055_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x0a0804_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x0a0804_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x0a084b_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x0a084b_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x0a9464_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x0a9464_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x0af864_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x0af864_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x0cf464_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x0cf464_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x108093_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x108093_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x10c413_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x10c413_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x10c512_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x10c512_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x10c613_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x10c613_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x10c713_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x10c713_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120a11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120a11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120a12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120a12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120b11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120b11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120b12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120b12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120c64_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120c64_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120c98_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120c98_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120d64_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120d64_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x120d98_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x120d98_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d711_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d711_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d712_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d712_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d713_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d713_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d811_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d811_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d812_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d812_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d813_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d813_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d911_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d911_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d912_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d912_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12d913_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12d913_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12da11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12da11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12da12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12da12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12da13_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12da13_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12db12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12db12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12dc11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12dc11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12dd12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12dd12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12de11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12de11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12df13_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12df13_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e012_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e012_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e111_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e111_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e213_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e213_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e319_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e319_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e712_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e712_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e811_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e811_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12e912_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12e912_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12ea11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12ea11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12f316_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12f316_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x12f917_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x12f917_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x13e919_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x13e919_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x160a51_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x160a51_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x166c64_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x166c64_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x16e600_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x16e600_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x179e11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x179e11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x179e12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x179e12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x179f11_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x179f11_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x179f12_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x179f12_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x1a0064_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x1a0064_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x1a7104_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x1a7104_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x1a714b_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x1a714b_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0x1a7172_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0x1a7172_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xc07988_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xc07988_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xc08913_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xc08913_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xd18787_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xd18787_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xd1a087_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xd1a087_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xd20781_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xd20781_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xd2a081_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xd2a081_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xd38782_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xd38782_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xd38783_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xd38783_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xe00081_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xe00081_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xe00087_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xe00087_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xe00214_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xe00214_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_Event_DTC_0xe00362_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_DTC_0xe00362_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_OpCycle_DemAgingCycle_GetOperationCycleState(Dem_OperationCycleStateType *CycleState)
{
  return Rte_Call_OpCycle_DemAgingCycle_GetOperationCycleState(CycleState);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_OpCycle_DemAgingCycle_SetOperationCycleState(Dem_OperationCycleStateType CycleState)
{
  return Rte_Call_OpCycle_DemAgingCycle_SetOperationCycleState(CycleState);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_OpCycle_DemDrivingCycle_GetOperationCycleState(Dem_OperationCycleStateType *CycleState)
{
  return Rte_Call_OpCycle_DemDrivingCycle_GetOperationCycleState(CycleState);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_OpCycle_DemDrivingCycle_SetOperationCycleState(Dem_OperationCycleStateType CycleState)
{
  return Rte_Call_OpCycle_DemDrivingCycle_SetOperationCycleState(CycleState);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_OpCycle_PowerCycle_GetOperationCycleState(Dem_OperationCycleStateType *CycleState)
{
  return Rte_Call_OpCycle_PowerCycle_GetOperationCycleState(CycleState);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(Dem_OperationCycleStateType CycleState)
{
  return Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(CycleState);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_EraseBlock(void)
{
  return Rte_Call_PS_EthMACAdressDataBlock_EraseBlock();
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_GetErrorStatus(NvM_RequestResultType *ErrorStatus)
{
  return Rte_Call_PS_EthMACAdressDataBlock_GetErrorStatus(ErrorStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_InvalidateNvBlock(void)
{
  return Rte_Call_PS_EthMACAdressDataBlock_InvalidateNvBlock();
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_ReadBlock(dtRef_VOID DstPtr)
{
  return Rte_Call_PS_EthMACAdressDataBlock_ReadBlock(DstPtr);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_RestoreBlockDefaults(dtRef_VOID DstPtr)
{
  return Rte_Call_PS_EthMACAdressDataBlock_RestoreBlockDefaults(DstPtr);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_SetRamBlockStatus(boolean RamBlockStatus)
{
  return Rte_Call_PS_EthMACAdressDataBlock_SetRamBlockStatus(RamBlockStatus);
}
Std_ReturnType TSC_CtApDGN_Rte_Call_PS_EthMACAdressDataBlock_WriteBlock(dtRef_const_VOID SrcPtr)
{
  return Rte_Call_PS_EthMACAdressDataBlock_WriteBlock(SrcPtr);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApDGN_Rte_Call_PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void)
{
  return Rte_Call_PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd();
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtVehicleSpeedThresholdDiag  TSC_CtApDGN_Rte_CData_CalVehicleSpeedThresholdDiag(void)
{
  return (IdtVehicleSpeedThresholdDiag ) Rte_CData_CalVehicleSpeedThresholdDiag();
}

     /* CtApDGN */
      /* CtApDGN */

/** Per Instance Memories */
Rte_DT_IdtArrayDGN_EOL_NVM_RamMirror_0 *TSC_CtApDGN_Rte_Pim_PimDGN_EOL_NVM_RamMirror(void)
{
  return Rte_Pim_PimDGN_EOL_NVM_RamMirror();
}



