/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Fls_17_Pmu.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
void TSC_Fls_17_Pmu_SchM_Enter_Fls_17_Pmu_Erase();
void TSC_Fls_17_Pmu_SchM_Exit_Fls_17_Pmu_Erase();
void TSC_Fls_17_Pmu_SchM_Enter_Fls_17_Pmu_Init();
void TSC_Fls_17_Pmu_SchM_Exit_Fls_17_Pmu_Init();
void TSC_Fls_17_Pmu_SchM_Enter_Fls_17_Pmu_Main();
void TSC_Fls_17_Pmu_SchM_Exit_Fls_17_Pmu_Main();
void TSC_Fls_17_Pmu_SchM_Enter_Fls_17_Pmu_ResumeErase();
void TSC_Fls_17_Pmu_SchM_Exit_Fls_17_Pmu_ResumeErase();
void TSC_Fls_17_Pmu_SchM_Enter_Fls_17_Pmu_Write();
void TSC_Fls_17_Pmu_SchM_Exit_Fls_17_Pmu_Write();
