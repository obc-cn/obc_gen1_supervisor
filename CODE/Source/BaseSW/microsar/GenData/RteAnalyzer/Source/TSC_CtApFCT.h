/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApFCT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApFCT_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw *data);
Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApFCT_Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(IdtOutputTempAftsales data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(IdtOutputTempAftsales data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(IdtOutputTempAftsales data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(IdtOutputTempAftsales data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(IdtOutputTempAftsales data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(IdtOutputTempAftsales data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC1FaultSCG(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC1FaultSCP(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC2FaultSCG(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC2FaultSCP(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC3FaultSCG(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC3FaultSCP(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempACNFaultSCG(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempACNFaultSCP(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC1FaultSCG(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC1FaultSCP(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC2FaultSCG(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC2FaultSCP(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApFCT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC1(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC2(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC3(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_ACN(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC1(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC2(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC1(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC2(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC3(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_ACN(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC1(void);
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC3(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_ACN(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC3(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_ACN(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC3(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_ACN(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC2(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC3(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_ACN(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC1(void);
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC2(void);
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_AC1(void);
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_AC2(void);
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_AC3(void);
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_ACN(void);
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_DC1(void);
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_DC2(void);
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueAC1_C(void);
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueAC2_C(void);
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueAC3_C(void);
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueACN_C(void);
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueDC1_C(void);
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueDC2_C(void);
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowAC1Plug(void);
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowAC2Plug(void);
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowAC3Plug(void);
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowACNPlug(void);
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowDC1Plug(void);
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowDC2Plug(void);
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC1(void);
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC2(void);
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC3(void);
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_ACN(void);
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC1(void);
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC2(void);
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC1(void);
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC2(void);
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC3(void);
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_ACN(void);
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC1(void);
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC2(void);

/** Per Instance Memories */
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCG(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCP(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCG(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCP(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCG(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCP(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCG(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCP(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCG(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCP(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCG(void);
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCP(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCG(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCP(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCG(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCP(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCG(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCP(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCG(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCP(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCG(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCP(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCG(void);
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCP(void);



