/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLSD.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApLSD.h"
#include "TSC_CtApLSD.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApLSD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
{
  return Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
{
  return Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw *data)
{
  return Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(data);
}




Std_ReturnType TSC_CtApLSD_Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(boolean data)
{
  return Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(boolean data)
{
  return Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(boolean data)
{
  return Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(OBC_ElockState data)
{
  return Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(data);
}

Std_ReturnType TSC_CtApLSD_Rte_Write_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor data)
{
  return Rte_Write_PpOutputELockSensor_DeOutputELockSensor(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApLSD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtELockDebounce  TSC_CtApLSD_Rte_CData_CalELockDebounce(void)
{
  return (IdtELockDebounce ) Rte_CData_CalELockDebounce();
}
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorDriveHigh(void)
{
  return (IdtELockSensorLimit ) Rte_CData_CalELockSensorDriveHigh();
}
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorDriveLow(void)
{
  return (IdtELockSensorLimit ) Rte_CData_CalELockSensorDriveLow();
}
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorLockHigh(void)
{
  return (IdtELockSensorLimit ) Rte_CData_CalELockSensorLockHigh();
}
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorLockLow(void)
{
  return (IdtELockSensorLimit ) Rte_CData_CalELockSensorLockLow();
}
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorUnlockHigh(void)
{
  return (IdtELockSensorLimit ) Rte_CData_CalELockSensorUnlockHigh();
}
IdtELockSensorLimit  TSC_CtApLSD_Rte_CData_CalELockSensorUnlockLow(void)
{
  return (IdtELockSensorLimit ) Rte_CData_CalELockSensorUnlockLow();
}
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCGConfirmTime(void)
{
  return (IdtELockSensorFaultTime ) Rte_CData_CalElockSensorSCGConfirmTime();
}
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCGHealTime(void)
{
  return (IdtELockSensorFaultTime ) Rte_CData_CalElockSensorSCGHealTime();
}
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCPConfirmTime(void)
{
  return (IdtELockSensorFaultTime ) Rte_CData_CalElockSensorSCPConfirmTime();
}
IdtELockSensorFaultTime  TSC_CtApLSD_Rte_CData_CalElockSensorSCPHealTime(void)
{
  return (IdtELockSensorFaultTime ) Rte_CData_CalElockSensorSCPHealTime();
}
IdtELockSensorFaultThreshold  TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCG(void)
{
  return (IdtELockSensorFaultThreshold ) Rte_CData_CalElockSensorThresholdSCG();
}
IdtELockSensorFaultThreshold  TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCP(void)
{
  return (IdtELockSensorFaultThreshold ) Rte_CData_CalElockSensorThresholdSCP();
}
boolean  TSC_CtApLSD_Rte_CData_CalCtrlFlowPlugLock(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowPlugLock();
}

     /* CtApLSD */
      /* CtApLSD */

/** Per Instance Memories */
uint16 *TSC_CtApLSD_Rte_Pim_PimElockCounterSCG(void)
{
  return Rte_Pim_PimElockCounterSCG();
}
uint16 *TSC_CtApLSD_Rte_Pim_PimElockCounterSCP(void)
{
  return Rte_Pim_PimElockCounterSCP();
}
boolean *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCG(void)
{
  return Rte_Pim_PimElockPrefaultSCG();
}
boolean *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCP(void)
{
  return Rte_Pim_PimElockPrefaultSCP();
}



