/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLAD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApLAD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(boolean *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(boolean *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data);
Std_ReturnType TSC_CtApLAD_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockOvercurrent(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean data);
Std_ReturnType TSC_CtApLAD_Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApLAD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** Calibration Component Calibration Parameters */
IdtBatteryVoltMaxTest  TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void);
IdtBatteryVoltMinTest  TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void);
IdtVehStopMaxTest  TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void);

/** SW-C local Calibration Parameters */
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(void);
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(void);
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(void);
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(void);
IdtAfts_ElockActuatorTime1  TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(void);
IdtAfts_ElockActuatorTime2  TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(void);
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(void);
boolean  TSC_CtApLAD_Rte_CData_CalEnableElockActuator(void);

/** Per Instance Memories */
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent(void);
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit(void);
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG(void);
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP(void);
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit(void);
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG(void);
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP(void);
IdtOutputELockSensor *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock(void);
IdtOutputELockSensor *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock(void);
IdtELockSetPoint *TSC_CtApLAD_Rte_Pim_PimElockSetPoint(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent(void);
boolean *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured(void);



