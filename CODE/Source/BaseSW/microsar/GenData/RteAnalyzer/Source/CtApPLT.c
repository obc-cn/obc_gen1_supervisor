/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPLT.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApPLT
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPLT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 * NvM_BlockIdType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApPLT.h"
#include "TSC_CtApPLT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApPLT_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtControlPilotDutyPlantMode: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDCRelayVoltagePlantMode: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtDebounceControlPilotPlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceDCRelayVoltagePlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebouncePhasePlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceProximityPlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtGlobalTimeoutPlantMode: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtMaxTimePlantMode: Integer in interval [0...250]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtPhasePlantmode: Integer in interval [0...220]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtProximityDetectPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityDetectPlantMode: Integer in interval [0...1200]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityVoltageStartingPlantMode: Integer in interval [0...1200]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtRECHARGE_HMI_STATE: Integer in interval [0...255]
 * IdtTimeoutPlantMode: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * NvM_BlockIdType: Integer in interval [1...32767]
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * Rte_DT_IdtPlantModeTestUTPlugin_0: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtPlantModeDTCNumber: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_DTC_10C413 (0U)
 *   PLT_DTC_10C512 (1U)
 *   PLT_DTC_10C613 (2U)
 *   PLT_DTC_10C713 (3U)
 * IdtPlantModeTestInfoDID_Result: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_TEST_NOT_DONE (0U)
 *   PLT_TEST_DONE_WITH_ERROR (1U)
 *   PLT_TEST_DONE_WITHOUT_ERROR (2U)
 * IdtPlantModeTestInfoDID_State: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_CHARGE_MODE (0U)
 *   PLT_TEST_MODE (1U)
 * IdtPlantModeTestInfoDID_Test: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_TEST_NOK (0U)
 *   PLT_TEST_OK (1U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_PosShuntJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Park (0U)
 *   Cx1_Client (1U)
 *   Cx2_Absent (2U)
 *   Cx3_Undetermined (3U)
 *
 * Array Types:
 * ============
 * IdtPlantModeTestUTPlugin: Array with 2 element(s) of type Rte_DT_IdtPlantModeTestUTPlugin_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtTimeoutPlantMode *Rte_Pim_PimGlobalTimeoutPlantMode(void)
 *   IdtTimeoutPlantMode *Rte_Pim_PimTimeoutPlantMode(void)
 *   boolean *Rte_Pim_PimPlantModeControlPilot(void)
 *   boolean *Rte_Pim_PimPlantModeDCRelayVoltage(void)
 *   boolean *Rte_Pim_PimPlantModePhaseVoltage(void)
 *   boolean *Rte_Pim_PimPlantModeProximity(void)
 *   Rte_DT_IdtPlantModeTestUTPlugin_0 *Rte_Pim_PimPlantModeTestUTPlugin(void)
 *     Returnvalue: Rte_DT_IdtPlantModeTestUTPlugin_0* is of type IdtPlantModeTestUTPlugin
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtGlobalTimeoutPlantMode Rte_CData_CalGlobalTimeoutPlantMode(void)
 *   IdtProximityDetectPlantMode Rte_CData_CalMaxProximityDetectPlantMode(void)
 *   IdtProximityDetectPlantMode Rte_CData_CalMinProximityDetectPlantMode(void)
 *   IdtProximityVoltageStartingPlantMode Rte_CData_CalProximityVoltageStartingPlantMode(void)
 *   IdtDebounceControlPilotPlantMode Rte_CData_CalDebounceControlPilotPlantMode(void)
 *   IdtDebounceDCRelayVoltagePlantMode Rte_CData_CalDebounceDCRelayVoltagePlantMode(void)
 *   IdtDebouncePhasePlantMode Rte_CData_CalDebouncePhasePlantMode(void)
 *   IdtDebounceProximityPlantMode Rte_CData_CalDebounceProximityPlantMode(void)
 *   IdtControlPilotDutyPlantMode Rte_CData_CalMaxControlPilotDutyPlantMode(void)
 *   IdtDCRelayVoltagePlantMode Rte_CData_CalMaxDCRelayVoltagePlantMode(void)
 *   IdtPhasePlantmode Rte_CData_CalMaxPhasePlantMode(void)
 *   IdtMaxTimePlantMode Rte_CData_CalMaxTimePlantMode(void)
 *   IdtControlPilotDutyPlantMode Rte_CData_CalMinControlPilotDutyPlantMode(void)
 *   IdtDCRelayVoltagePlantMode Rte_CData_CalMinDCRelayVoltagePlantMode(void)
 *   IdtPhasePlantmode Rte_CData_CalMinPhasePlantMode(void)
 *   boolean Rte_CData_CalInhibitControlPilotDetection(void)
 *   boolean Rte_CData_CalInhibitDCRelayVoltage(void)
 *   boolean Rte_CData_CalInhibitPhaseVoltageDetection(void)
 *   boolean Rte_CData_CalInhibitProximityDetection(void)
 *   Rte_DT_IdtPlantModeTestUTPlugin_0 *Rte_CData_PLTNvMNeed_DefaultValue(void)
 *     Returnvalue: Rte_DT_IdtPlantModeTestUTPlugin_0* is of type IdtPlantModeTestUTPlugin
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtBatteryVoltMaxTest Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
 *   IdtBatteryVoltMinTest Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
 *   IdtVehStopMaxTest Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
 *
 *********************************************************************************************************************/


#define CtApPLT_START_SEC_CODE
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLT_CODE) RCtApPLT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_init
 *********************************************************************************************************************/

  IdtTimeoutPlantMode PimPimGlobalTimeoutPlantMode;
  IdtTimeoutPlantMode PimPimTimeoutPlantMode;
  boolean PimPimPlantModeControlPilot;
  boolean PimPimPlantModeDCRelayVoltage;
  boolean PimPimPlantModePhaseVoltage;
  boolean PimPimPlantModeProximity;
  IdtPlantModeTestUTPlugin PimPimPlantModeTestUTPlugin;

  IdtGlobalTimeoutPlantMode CalGlobalTimeoutPlantMode_data;
  IdtProximityDetectPlantMode CalMaxProximityDetectPlantMode_data;
  IdtProximityDetectPlantMode CalMinProximityDetectPlantMode_data;
  IdtProximityVoltageStartingPlantMode CalProximityVoltageStartingPlantMode_data;
  IdtDebounceControlPilotPlantMode CalDebounceControlPilotPlantMode_data;
  IdtDebounceDCRelayVoltagePlantMode CalDebounceDCRelayVoltagePlantMode_data;
  IdtDebouncePhasePlantMode CalDebouncePhasePlantMode_data;
  IdtDebounceProximityPlantMode CalDebounceProximityPlantMode_data;
  IdtControlPilotDutyPlantMode CalMaxControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMaxDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMaxPhasePlantMode_data;
  IdtMaxTimePlantMode CalMaxTimePlantMode_data;
  IdtControlPilotDutyPlantMode CalMinControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMinDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMinPhasePlantMode_data;
  boolean CalInhibitControlPilotDetection_data;
  boolean CalInhibitDCRelayVoltage_data;
  boolean CalInhibitPhaseVoltageDetection_data;
  boolean CalInhibitProximityDetection_data;
  IdtPlantModeTestUTPlugin PLTNvMNeed_DefaultValue_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimGlobalTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode() = PimPimGlobalTimeoutPlantMode;
  PimPimTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode() = PimPimTimeoutPlantMode;
  PimPimPlantModeControlPilot = *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot() = PimPimPlantModeControlPilot;
  PimPimPlantModeDCRelayVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage() = PimPimPlantModeDCRelayVoltage;
  PimPimPlantModePhaseVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage() = PimPimPlantModePhaseVoltage;
  PimPimPlantModeProximity = *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity() = PimPimPlantModeProximity;

  (void)memcpy(PimPimPlantModeTestUTPlugin, TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), PimPimPlantModeTestUTPlugin, sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalGlobalTimeoutPlantMode_data = TSC_CtApPLT_Rte_CData_CalGlobalTimeoutPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalProximityVoltageStartingPlantMode_data = TSC_CtApPLT_Rte_CData_CalProximityVoltageStartingPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceControlPilotPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceControlPilotPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebouncePhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebouncePhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceProximityPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceProximityPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxTimePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitControlPilotDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitControlPilotDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitDCRelayVoltage_data = TSC_CtApPLT_Rte_CData_CalInhibitDCRelayVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitPhaseVoltageDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitPhaseVoltageDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitProximityDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitProximityDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(PLTNvMNeed_DefaultValue_data, TSC_CtApPLT_Rte_CData_PLTNvMNeed_DefaultValue(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApPLT_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApPLT_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLT_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
 *   Std_ReturnType Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeChargeError(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeGuideManagement(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(boolean data)
 *   Std_ReturnType Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean data)
 *   Std_ReturnType Rte_Write_PpPlantModeState_DePlantModeState(boolean data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(IdtPlantModeTestInfoDID_Result data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(IdtPlantModeTestInfoDID_State data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(IdtPlantModeDTCNumber DTC_Id)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(IdtPlantModeDTCNumber DTC_Id)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLT_CODE) RCtApPLT_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtDutyControlPilot Read_PpDutyControlPilot_DeDutyControlPilot;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  BMS_DCRelayVoltage Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  PFC_VPH12_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
  PFC_VPH23_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V;
  PFC_VPH31_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V;
  VCU_EtatGmpHyb Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
  VCU_ModeEPSRequest Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
  VCU_PosShuntJDD Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD;
  IdtProximityDetectPhysicalValue Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue;
  boolean Read_PpShutdownAuthorization_DeShutdownAuthorization;

  IdtTimeoutPlantMode PimPimGlobalTimeoutPlantMode;
  IdtTimeoutPlantMode PimPimTimeoutPlantMode;
  boolean PimPimPlantModeControlPilot;
  boolean PimPimPlantModeDCRelayVoltage;
  boolean PimPimPlantModePhaseVoltage;
  boolean PimPimPlantModeProximity;
  IdtPlantModeTestUTPlugin PimPimPlantModeTestUTPlugin;

  IdtGlobalTimeoutPlantMode CalGlobalTimeoutPlantMode_data;
  IdtProximityDetectPlantMode CalMaxProximityDetectPlantMode_data;
  IdtProximityDetectPlantMode CalMinProximityDetectPlantMode_data;
  IdtProximityVoltageStartingPlantMode CalProximityVoltageStartingPlantMode_data;
  IdtDebounceControlPilotPlantMode CalDebounceControlPilotPlantMode_data;
  IdtDebounceDCRelayVoltagePlantMode CalDebounceDCRelayVoltagePlantMode_data;
  IdtDebouncePhasePlantMode CalDebouncePhasePlantMode_data;
  IdtDebounceProximityPlantMode CalDebounceProximityPlantMode_data;
  IdtControlPilotDutyPlantMode CalMaxControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMaxDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMaxPhasePlantMode_data;
  IdtMaxTimePlantMode CalMaxTimePlantMode_data;
  IdtControlPilotDutyPlantMode CalMinControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMinDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMinPhasePlantMode_data;
  boolean CalInhibitControlPilotDetection_data;
  boolean CalInhibitDCRelayVoltage_data;
  boolean CalInhibitPhaseVoltageDetection_data;
  boolean CalInhibitProximityDetection_data;
  IdtPlantModeTestUTPlugin PLTNvMNeed_DefaultValue_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimGlobalTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode() = PimPimGlobalTimeoutPlantMode;
  PimPimTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode() = PimPimTimeoutPlantMode;
  PimPimPlantModeControlPilot = *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot() = PimPimPlantModeControlPilot;
  PimPimPlantModeDCRelayVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage() = PimPimPlantModeDCRelayVoltage;
  PimPimPlantModePhaseVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage() = PimPimPlantModePhaseVoltage;
  PimPimPlantModeProximity = *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity() = PimPimPlantModeProximity;

  (void)memcpy(PimPimPlantModeTestUTPlugin, TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), PimPimPlantModeTestUTPlugin, sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalGlobalTimeoutPlantMode_data = TSC_CtApPLT_Rte_CData_CalGlobalTimeoutPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalProximityVoltageStartingPlantMode_data = TSC_CtApPLT_Rte_CData_CalProximityVoltageStartingPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceControlPilotPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceControlPilotPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebouncePhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebouncePhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceProximityPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceProximityPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxTimePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitControlPilotDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitControlPilotDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitDCRelayVoltage_data = TSC_CtApPLT_Rte_CData_CalInhibitDCRelayVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitPhaseVoltageDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitPhaseVoltageDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitProximityDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitProximityDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(PLTNvMNeed_DefaultValue_data, TSC_CtApPLT_Rte_CData_PLTNvMNeed_DefaultValue(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApPLT_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(&Read_PpDutyControlPilot_DeDutyControlPilot); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(&Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(&Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(&Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&Read_PpShutdownAuthorization_DeShutdownAuthorization); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(Rte_InitValue_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeChargeError(Rte_InitValue_PpLedModes_PlantMode_DeChargeError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(Rte_InitValue_PpLedModes_PlantMode_DeChargeInProgress); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(Rte_InitValue_PpLedModes_PlantMode_DeEndOfCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeGuideManagement(Rte_InitValue_PpLedModes_PlantMode_DeGuideManagement); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(Rte_InitValue_PpLedModes_PlantMode_DeProgrammingCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(Rte_InitValue_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeState_DePlantModeState(Rte_InitValue_PpPlantModeState_DePlantModeState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(Rte_InitValue_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_02, uint8 *Out_MODE_TEST, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPLT_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  VCU_EtatGmpHyb Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;

  IdtTimeoutPlantMode PimPimGlobalTimeoutPlantMode;
  IdtTimeoutPlantMode PimPimTimeoutPlantMode;
  boolean PimPimPlantModeControlPilot;
  boolean PimPimPlantModeDCRelayVoltage;
  boolean PimPimPlantModePhaseVoltage;
  boolean PimPimPlantModeProximity;
  IdtPlantModeTestUTPlugin PimPimPlantModeTestUTPlugin;

  IdtGlobalTimeoutPlantMode CalGlobalTimeoutPlantMode_data;
  IdtProximityDetectPlantMode CalMaxProximityDetectPlantMode_data;
  IdtProximityDetectPlantMode CalMinProximityDetectPlantMode_data;
  IdtProximityVoltageStartingPlantMode CalProximityVoltageStartingPlantMode_data;
  IdtDebounceControlPilotPlantMode CalDebounceControlPilotPlantMode_data;
  IdtDebounceDCRelayVoltagePlantMode CalDebounceDCRelayVoltagePlantMode_data;
  IdtDebouncePhasePlantMode CalDebouncePhasePlantMode_data;
  IdtDebounceProximityPlantMode CalDebounceProximityPlantMode_data;
  IdtControlPilotDutyPlantMode CalMaxControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMaxDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMaxPhasePlantMode_data;
  IdtMaxTimePlantMode CalMaxTimePlantMode_data;
  IdtControlPilotDutyPlantMode CalMinControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMinDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMinPhasePlantMode_data;
  boolean CalInhibitControlPilotDetection_data;
  boolean CalInhibitDCRelayVoltage_data;
  boolean CalInhibitPhaseVoltageDetection_data;
  boolean CalInhibitProximityDetection_data;
  IdtPlantModeTestUTPlugin PLTNvMNeed_DefaultValue_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimGlobalTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode() = PimPimGlobalTimeoutPlantMode;
  PimPimTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode() = PimPimTimeoutPlantMode;
  PimPimPlantModeControlPilot = *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot() = PimPimPlantModeControlPilot;
  PimPimPlantModeDCRelayVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage() = PimPimPlantModeDCRelayVoltage;
  PimPimPlantModePhaseVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage() = PimPimPlantModePhaseVoltage;
  PimPimPlantModeProximity = *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity() = PimPimPlantModeProximity;

  (void)memcpy(PimPimPlantModeTestUTPlugin, TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), PimPimPlantModeTestUTPlugin, sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalGlobalTimeoutPlantMode_data = TSC_CtApPLT_Rte_CData_CalGlobalTimeoutPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalProximityVoltageStartingPlantMode_data = TSC_CtApPLT_Rte_CData_CalProximityVoltageStartingPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceControlPilotPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceControlPilotPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebouncePhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebouncePhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceProximityPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceProximityPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxTimePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitControlPilotDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitControlPilotDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitDCRelayVoltage_data = TSC_CtApPLT_Rte_CData_CalInhibitDCRelayVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitPhaseVoltageDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitPhaseVoltageDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitProximityDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitProximityDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(PLTNvMNeed_DefaultValue_data, TSC_CtApPLT_Rte_CData_PLTNvMNeed_DefaultValue(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApPLT_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  return RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, uint8 *Out_RSR_02, uint8 *Out_MODE_TEST, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPLT_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  VCU_EtatGmpHyb Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;

  IdtTimeoutPlantMode PimPimGlobalTimeoutPlantMode;
  IdtTimeoutPlantMode PimPimTimeoutPlantMode;
  boolean PimPimPlantModeControlPilot;
  boolean PimPimPlantModeDCRelayVoltage;
  boolean PimPimPlantModePhaseVoltage;
  boolean PimPimPlantModeProximity;
  IdtPlantModeTestUTPlugin PimPimPlantModeTestUTPlugin;

  IdtGlobalTimeoutPlantMode CalGlobalTimeoutPlantMode_data;
  IdtProximityDetectPlantMode CalMaxProximityDetectPlantMode_data;
  IdtProximityDetectPlantMode CalMinProximityDetectPlantMode_data;
  IdtProximityVoltageStartingPlantMode CalProximityVoltageStartingPlantMode_data;
  IdtDebounceControlPilotPlantMode CalDebounceControlPilotPlantMode_data;
  IdtDebounceDCRelayVoltagePlantMode CalDebounceDCRelayVoltagePlantMode_data;
  IdtDebouncePhasePlantMode CalDebouncePhasePlantMode_data;
  IdtDebounceProximityPlantMode CalDebounceProximityPlantMode_data;
  IdtControlPilotDutyPlantMode CalMaxControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMaxDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMaxPhasePlantMode_data;
  IdtMaxTimePlantMode CalMaxTimePlantMode_data;
  IdtControlPilotDutyPlantMode CalMinControlPilotDutyPlantMode_data;
  IdtDCRelayVoltagePlantMode CalMinDCRelayVoltagePlantMode_data;
  IdtPhasePlantmode CalMinPhasePlantMode_data;
  boolean CalInhibitControlPilotDetection_data;
  boolean CalInhibitDCRelayVoltage_data;
  boolean CalInhibitPhaseVoltageDetection_data;
  boolean CalInhibitProximityDetection_data;
  IdtPlantModeTestUTPlugin PLTNvMNeed_DefaultValue_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimGlobalTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimGlobalTimeoutPlantMode() = PimPimGlobalTimeoutPlantMode;
  PimPimTimeoutPlantMode = *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode();
  *TSC_CtApPLT_Rte_Pim_PimTimeoutPlantMode() = PimPimTimeoutPlantMode;
  PimPimPlantModeControlPilot = *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeControlPilot() = PimPimPlantModeControlPilot;
  PimPimPlantModeDCRelayVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeDCRelayVoltage() = PimPimPlantModeDCRelayVoltage;
  PimPimPlantModePhaseVoltage = *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage();
  *TSC_CtApPLT_Rte_Pim_PimPlantModePhaseVoltage() = PimPimPlantModePhaseVoltage;
  PimPimPlantModeProximity = *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity();
  *TSC_CtApPLT_Rte_Pim_PimPlantModeProximity() = PimPimPlantModeProximity;

  (void)memcpy(PimPimPlantModeTestUTPlugin, TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtApPLT_Rte_Pim_PimPlantModeTestUTPlugin(), PimPimPlantModeTestUTPlugin, sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalGlobalTimeoutPlantMode_data = TSC_CtApPLT_Rte_CData_CalGlobalTimeoutPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinProximityDetectPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinProximityDetectPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalProximityVoltageStartingPlantMode_data = TSC_CtApPLT_Rte_CData_CalProximityVoltageStartingPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceControlPilotPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceControlPilotPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebouncePhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalDebouncePhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceProximityPlantMode_data = TSC_CtApPLT_Rte_CData_CalDebounceProximityPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimePlantMode_data = TSC_CtApPLT_Rte_CData_CalMaxTimePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinControlPilotDutyPlantMode_data = TSC_CtApPLT_Rte_CData_CalMinControlPilotDutyPlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinDCRelayVoltagePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinDCRelayVoltagePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPhasePlantMode_data = TSC_CtApPLT_Rte_CData_CalMinPhasePlantMode(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitControlPilotDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitControlPilotDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitDCRelayVoltage_data = TSC_CtApPLT_Rte_CData_CalInhibitDCRelayVoltage(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitPhaseVoltageDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitPhaseVoltageDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalInhibitProximityDetection_data = TSC_CtApPLT_Rte_CData_CalInhibitProximityDetection(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(PLTNvMNeed_DefaultValue_data, TSC_CtApPLT_Rte_CData_PLTNvMNeed_DefaultValue(), sizeof(IdtPlantModeTestUTPlugin)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApPLT_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApPLT_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApPLT_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLT_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  return RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPLT_STOP_SEC_CODE
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApPLT_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtELockSetPoint Test_IdtELockSetPoint_V_1 = ELOCK_SETPOINT_UNLOCKED;
  IdtELockSetPoint Test_IdtELockSetPoint_V_2 = ELOCK_SETPOINT_LOCKED;

  IdtPlantModeDTCNumber Test_IdtPlantModeDTCNumber_V_1 = PLT_DTC_10C413;
  IdtPlantModeDTCNumber Test_IdtPlantModeDTCNumber_V_2 = PLT_DTC_10C512;
  IdtPlantModeDTCNumber Test_IdtPlantModeDTCNumber_V_3 = PLT_DTC_10C613;
  IdtPlantModeDTCNumber Test_IdtPlantModeDTCNumber_V_4 = PLT_DTC_10C713;

  IdtPlantModeTestInfoDID_Result Test_IdtPlantModeTestInfoDID_Result_V_1 = PLT_TEST_NOT_DONE;
  IdtPlantModeTestInfoDID_Result Test_IdtPlantModeTestInfoDID_Result_V_2 = PLT_TEST_DONE_WITH_ERROR;
  IdtPlantModeTestInfoDID_Result Test_IdtPlantModeTestInfoDID_Result_V_3 = PLT_TEST_DONE_WITHOUT_ERROR;

  IdtPlantModeTestInfoDID_State Test_IdtPlantModeTestInfoDID_State_V_1 = PLT_CHARGE_MODE;
  IdtPlantModeTestInfoDID_State Test_IdtPlantModeTestInfoDID_State_V_2 = PLT_TEST_MODE;

  IdtPlantModeTestInfoDID_Test Test_IdtPlantModeTestInfoDID_Test_V_1 = PLT_TEST_NOK;
  IdtPlantModeTestInfoDID_Test Test_IdtPlantModeTestInfoDID_Test_V_2 = PLT_TEST_OK;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_1 = Cx0_PWT_inactive;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_2 = Cx1_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_3 = Cx2_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_4 = Cx3_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_5 = Cx4_PWT_activation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_6 = Cx5_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_7 = Cx6_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_8 = Cx7_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_9 = Cx8_active_PWT;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_10 = Cx9_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_11 = CxA_PWT_desactivation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_12 = CxB_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_13 = CxC_PWT_in_hold_after_desactivation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_14 = CxD_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_15 = CxE_PWT_at_rest;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_16 = CxF_reserved;

  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_1 = Cx0_OFF;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_2 = Cx1_Active_Drive;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_3 = Cx2_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_4 = Cx3_PI_Charge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_5 = Cx4_PI_Balance;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_6 = Cx5_PI_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_7 = Cx6_Reserved;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_8 = Cx7_Reserved;

  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_1 = Cx0_Park;
  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_2 = Cx1_Client;
  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_3 = Cx2_Absent;
  VCU_PosShuntJDD Test_VCU_PosShuntJDD_V_4 = Cx3_Undetermined;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Qac:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
