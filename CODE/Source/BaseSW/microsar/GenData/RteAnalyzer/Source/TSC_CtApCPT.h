/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApCPT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApCPT_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data);
Std_ReturnType TSC_CtApCPT_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApCPT_Rte_Write_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data);
Std_ReturnType TSC_CtApCPT_Rte_Write_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data);
Std_ReturnType TSC_CtApCPT_Rte_Write_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data);
Std_ReturnType TSC_CtApCPT_Rte_Write_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE data);

/** Client server interfaces */
Std_ReturnType TSC_CtApCPT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** Service interfaces */
Std_ReturnType TSC_CtApCPT_Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_ReadBlock(dtRef_VOID DstPtr);
Std_ReturnType TSC_CtApCPT_Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus);
Std_ReturnType TSC_CtApCPT_Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr);

/** SW-C local Calibration Parameters */
IdtDebounceControlPilot  TSC_CtApCPT_Rte_CData_CalDebounceControlPilot(void);

/** Per Instance Memories */
uint8 *TSC_CtApCPT_Rte_Pim_PimCPT_NvMRamMirror(void);



