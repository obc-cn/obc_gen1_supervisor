/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApTBD.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApTBD
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApTBD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApTBD.h"
#include "TSC_CtApTBD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApTBD_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpdValidFlag: Boolean
 * BMS_AuxBattVolt: Integer in interval [0...4095]
 * BMS_Fault: Integer in interval [0...4095]
 * BMS_RelayOpenReq: Boolean
 * DCDC_OVERTEMP: Boolean
 * DIAG_INTEGRA_ELEC: Boolean
 * EFFAC_DEFAUT_DIAG: Boolean
 * IdtOutputTempAftsales: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * MODE_DIAG: Boolean
 * OBC_DCChargingPlugAConnConf: Boolean
 * SUPV_DTCRegistred: Boolean
 * sint32: Integer in interval [-2147483648...2147483647] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_CC2_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_CC2_invalid_not_connected (1U)
 *   Cx2_CC2_valid_full_connected (2U)
 *   Cx3_CC2_invalid_half_connected (3U)
 * BMS_FastChargeSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_not_in_charging (0U)
 *   Cx1_charging (1U)
 *   Cx2_charging_fault (2U)
 *   Cx3_charging_finished (3U)
 * BMS_SlowChargeSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_not_in_charging (0U)
 *   Cx1_charging (1U)
 *   Cx2_charging_fault (2U)
 *   Cx3_charging_finished (3U)
 * VCU_Keyposition: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Key_OFF (0U)
 *   Cx1_Key_ACC (1U)
 *   Cx2_Key_ON (2U)
 *   Cx3_Key_START (3U)
 *
 *********************************************************************************************************************/


#define CtApTBD_START_SEC_CODE
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApTBD_tbd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(sint32 *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_Fault_BMS_Fault(BMS_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq *data)
 *   Std_ReturnType Rte_Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC *data)
 *   Std_ReturnType Rte_Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_Keyposition_VCU_Keyposition(VCU_Keyposition *data)
 *   Std_ReturnType Rte_Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales(IdtOutputTempAftsales *data)
 *   Std_ReturnType Rte_Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales(IdtOutputTempAftsales *data)
 *   Std_ReturnType Rte_Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales(IdtOutputTempAftsales *data)
 *   Std_ReturnType Rte_Read_PpOutputTempAftsales_DeOutputTempACNAftsales(IdtOutputTempAftsales *data)
 *   Std_ReturnType Rte_Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales(IdtOutputTempAftsales *data)
 *   Std_ReturnType Rte_Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales(IdtOutputTempAftsales *data)
 *   Std_ReturnType Rte_Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt *data)
 *   Std_ReturnType Rte_Read_Ppint_MODE_DIAG_MODE_DIAG(MODE_DIAG *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApTBD_tbd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApTBD_CODE) RCtApTBD_tbd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApTBD_tbd
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  sint32 Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup;
  ABS_VehSpdValidFlag Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag;
  BMS_AuxBattVolt Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt;
  BMS_CC2_connection_Status Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status;
  BMS_FastChargeSt Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt;
  BMS_Fault Read_PpInt_BMS_Fault_BMS_Fault;
  BMS_RelayOpenReq Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq;
  DIAG_INTEGRA_ELEC Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC;
  EFFAC_DEFAUT_DIAG Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG;
  VCU_Keyposition Read_PpInt_VCU_Keyposition_VCU_Keyposition;
  IdtOutputTempAftsales Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales;
  IdtOutputTempAftsales Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales;
  IdtOutputTempAftsales Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales;
  IdtOutputTempAftsales Read_PpOutputTempAftsales_DeOutputTempACNAftsales;
  IdtOutputTempAftsales Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales;
  IdtOutputTempAftsales Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales;
  BMS_SlowChargeSt Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt;
  MODE_DIAG Read_Ppint_MODE_DIAG_MODE_DIAG;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_CtApTBD_Rte_Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(&Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(&Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(&Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(&Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(&Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_BMS_Fault_BMS_Fault(&Read_PpInt_BMS_Fault_BMS_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(&Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(&Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(&Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpInt_VCU_Keyposition_VCU_Keyposition(&Read_PpInt_VCU_Keyposition_VCU_Keyposition); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales(&Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales(&Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales(&Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempACNAftsales(&Read_PpOutputTempAftsales_DeOutputTempACNAftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales(&Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales(&Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt(&Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Read_Ppint_MODE_DIAG_MODE_DIAG(&Read_Ppint_MODE_DIAG_MODE_DIAG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Write_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(Rte_InitValue_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Write_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(Rte_InitValue_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApTBD_Rte_Write_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(Rte_InitValue_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  CtApTBD_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApTBD_STOP_SEC_CODE
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApTBD_TestDefines(void)
{
  /* Enumeration Data Types */

  BMS_CC2_connection_Status Test_BMS_CC2_connection_Status_V_1 = Cx0_invalid_value;
  BMS_CC2_connection_Status Test_BMS_CC2_connection_Status_V_2 = Cx1_CC2_invalid_not_connected;
  BMS_CC2_connection_Status Test_BMS_CC2_connection_Status_V_3 = Cx2_CC2_valid_full_connected;
  BMS_CC2_connection_Status Test_BMS_CC2_connection_Status_V_4 = Cx3_CC2_invalid_half_connected;

  BMS_FastChargeSt Test_BMS_FastChargeSt_V_1 = Cx0_not_in_charging;
  BMS_FastChargeSt Test_BMS_FastChargeSt_V_2 = Cx1_charging;
  BMS_FastChargeSt Test_BMS_FastChargeSt_V_3 = Cx2_charging_fault;
  BMS_FastChargeSt Test_BMS_FastChargeSt_V_4 = Cx3_charging_finished;

  BMS_SlowChargeSt Test_BMS_SlowChargeSt_V_1 = Cx0_not_in_charging;
  BMS_SlowChargeSt Test_BMS_SlowChargeSt_V_2 = Cx1_charging;
  BMS_SlowChargeSt Test_BMS_SlowChargeSt_V_3 = Cx2_charging_fault;
  BMS_SlowChargeSt Test_BMS_SlowChargeSt_V_4 = Cx3_charging_finished;

  VCU_Keyposition Test_VCU_Keyposition_V_1 = Cx0_Key_OFF;
  VCU_Keyposition Test_VCU_Keyposition_V_2 = Cx1_Key_ACC;
  VCU_Keyposition Test_VCU_Keyposition_V_3 = Cx2_Key_ON;
  VCU_Keyposition Test_VCU_Keyposition_V_4 = Cx3_Key_START;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
