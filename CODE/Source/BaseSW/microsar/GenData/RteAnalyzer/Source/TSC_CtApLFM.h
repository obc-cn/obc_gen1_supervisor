/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLFM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApLFM_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data);
Std_ReturnType TSC_CtApLFM_Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(boolean data);
Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(boolean data);
Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(boolean data);
Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(boolean data);
Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCLVError_DeDCLVError(boolean data);
Std_ReturnType TSC_CtApLFM_Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(DCDC_Fault data);
Std_ReturnType TSC_CtApLFM_Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data);

/** Client server interfaces */
Std_ReturnType TSC_CtApLFM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data);

/** Calibration Component Calibration Parameters */
IdtDCLVFaultTimeToRetryDefault  TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void);
IdtDCLVFaultTimeToRetryShortCircuit  TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void);
IdtDebounceOvercurrentDCHV  TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void);

/** SW-C local Calibration Parameters */
IdtThresholdLVOvercurrentHW  TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(void);
IdtABSVehSpdThresholdOV  TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(void);
IdtBatteryVoltageSafetyOVLimit1_ConfirmTime  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(void);
IdtBatteryVoltageSafetyOVLimit1_Threshold  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(void);
IdtBatteryVoltageSafetyOVLimit2_ConfirmTime  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(void);
IdtBatteryVoltageSafetyOVLimit2_Threshold  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(void);
IdtDCLV_MaxNumberRetries  TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(void);
IdtTimeBatteryVoltageSafety  TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(void);

/** Per Instance Memories */
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error(void);
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error(void);



