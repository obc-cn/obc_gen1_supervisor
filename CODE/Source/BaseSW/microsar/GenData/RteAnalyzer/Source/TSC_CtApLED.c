/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLED.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApLED.h"
#include "TSC_CtApLED.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApLED_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeChargeError(boolean *data)
{
  return Rte_Read_PpLedModes_DeChargeError(data);
}

Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeChargeInProgress(boolean *data)
{
  return Rte_Read_PpLedModes_DeChargeInProgress(data);
}

Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeEndOfCharge(boolean *data)
{
  return Rte_Read_PpLedModes_DeEndOfCharge(data);
}

Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeGuideManagement(boolean *data)
{
  return Rte_Read_PpLedModes_DeGuideManagement(data);
}

Std_ReturnType TSC_CtApLED_Rte_Read_PpLedModes_DeProgrammingCharge(boolean *data)
{
  return Rte_Read_PpLedModes_DeProgrammingCharge(data);
}




Std_ReturnType TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl data)
{
  return Rte_Write_PpChLedRGBCtl_DeChLedBrCtl(data);
}

Std_ReturnType TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl data)
{
  return Rte_Write_PpChLedRGBCtl_DeChLedGrCtl(data);
}

Std_ReturnType TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl data)
{
  return Rte_Write_PpChLedRGBCtl_DeChLedRrCtl(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApLED_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApLED_Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(boolean *data)
{
  return Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(data);
}

Std_ReturnType TSC_CtApLED_Rte_Read_PpLockStateError_DeLockStateError(boolean *data)
{
  return Rte_Read_PpLockStateError_DeLockStateError(data);
}




Std_ReturnType TSC_CtApLED_Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl data)
{
  return Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueChargeError(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyBlueChargeError();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueChargeInProgress(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyBlueChargeInProgress();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueEndOfCharge(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyBlueEndOfCharge();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueGuideManagement(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyBlueGuideManagement();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyBlueProgrammingCharge(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyBlueProgrammingCharge();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenChargeError(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyGreenChargeError();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenChargeInProgress(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyGreenChargeInProgress();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenEndOfCharge(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyGreenEndOfCharge();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenGuideManagement(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyGreenGuideManagement();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyGreenProgrammingCharge(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyGreenProgrammingCharge();
}
IdtDutyPlug  TSC_CtApLED_Rte_CData_CalDutyPlug(void)
{
  return (IdtDutyPlug ) Rte_CData_CalDutyPlug();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedChargeError(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyRedChargeError();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedChargeInProgress(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyRedChargeInProgress();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedEndOfCharge(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyRedEndOfCharge();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedGuideManagement(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyRedGuideManagement();
}
IdtDutyLedCharge  TSC_CtApLED_Rte_CData_CalDutyRedProgrammingCharge(void)
{
  return (IdtDutyLedCharge ) Rte_CData_CalDutyRedProgrammingCharge();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffChargeError(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOffChargeError();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffChargeInProgress(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOffChargeInProgress();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffEndOfCharge(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOffEndOfCharge();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffGuideManagement(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOffGuideManagement();
}
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOffPlugA(void)
{
  return (IdtTimePlug ) Rte_CData_CalTimeOffPlugA();
}
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOffPlugB(void)
{
  return (IdtTimePlug ) Rte_CData_CalTimeOffPlugB();
}
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOffPlugC(void)
{
  return (IdtTimePlug ) Rte_CData_CalTimeOffPlugC();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOffProgrammingCharge(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOffProgrammingCharge();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnChargeError(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOnChargeError();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnChargeInProgress(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOnChargeInProgress();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnEndOfCharge(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOnEndOfCharge();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnGuideManagement(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOnGuideManagement();
}
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOnPlugA(void)
{
  return (IdtTimePlug ) Rte_CData_CalTimeOnPlugA();
}
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOnPlugB(void)
{
  return (IdtTimePlug ) Rte_CData_CalTimeOnPlugB();
}
IdtTimePlug  TSC_CtApLED_Rte_CData_CalTimeOnPlugC(void)
{
  return (IdtTimePlug ) Rte_CData_CalTimeOnPlugC();
}
IdtTimeLedCharge  TSC_CtApLED_Rte_CData_CalTimeOnProgrammingCharge(void)
{
  return (IdtTimeLedCharge ) Rte_CData_CalTimeOnProgrammingCharge();
}

     /* CtApLED */
      /* CtApLED */



