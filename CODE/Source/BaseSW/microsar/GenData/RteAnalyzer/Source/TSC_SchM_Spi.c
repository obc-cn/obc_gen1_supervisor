/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Spi.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_Spi.h"
#include "TSC_SchM_Spi.h"
void TSC_Spi_SchM_Enter_Spi_AsyncTransmit(void)
{
  SchM_Enter_Spi_AsyncTransmit();
}
void TSC_Spi_SchM_Exit_Spi_AsyncTransmit(void)
{
  SchM_Exit_Spi_AsyncTransmit();
}
void TSC_Spi_SchM_Enter_Spi_Cancel(void)
{
  SchM_Enter_Spi_Cancel();
}
void TSC_Spi_SchM_Exit_Spi_Cancel(void)
{
  SchM_Exit_Spi_Cancel();
}
void TSC_Spi_SchM_Enter_Spi_DeInit(void)
{
  SchM_Enter_Spi_DeInit();
}
void TSC_Spi_SchM_Exit_Spi_DeInit(void)
{
  SchM_Exit_Spi_DeInit();
}
void TSC_Spi_SchM_Enter_Spi_GetSequenceResult(void)
{
  SchM_Enter_Spi_GetSequenceResult();
}
void TSC_Spi_SchM_Exit_Spi_GetSequenceResult(void)
{
  SchM_Exit_Spi_GetSequenceResult();
}
void TSC_Spi_SchM_Enter_Spi_Init(void)
{
  SchM_Enter_Spi_Init();
}
void TSC_Spi_SchM_Exit_Spi_Init(void)
{
  SchM_Exit_Spi_Init();
}
void TSC_Spi_SchM_Enter_Spi_SyncTransmit(void)
{
  SchM_Enter_Spi_SyncTransmit();
}
void TSC_Spi_SchM_Exit_Spi_SyncTransmit(void)
{
  SchM_Exit_Spi_SyncTransmit();
}
void TSC_Spi_SchM_Enter_Spi_WriteIB(void)
{
  SchM_Enter_Spi_WriteIB();
}
void TSC_Spi_SchM_Exit_Spi_WriteIB(void)
{
  SchM_Exit_Spi_WriteIB();
}
