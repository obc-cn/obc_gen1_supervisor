/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Xcp.c
 *           Config:  OBCP11.dpa
 *       BSW Module:  Xcp
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for BSW Module <Xcp>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "SchM_Xcp.h"
#include "TSC_SchM_Xcp.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * ABS_VehSpdValidFlag: Boolean
 * BMS_AuxBattVolt: Integer in interval [0...4095]
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * BMS_Fault: Integer in interval [0...4095]
 * BMS_HighestChargeCurrentAllow: Integer in interval [0...2047]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * BMS_HighestChargeVoltageAllow: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * BMS_OnBoardChargerEnable: Boolean
 * BMS_QuickChargeConnectorState: Boolean
 * BMS_RelayOpenReq: Boolean
 * BMS_SOC: Integer in interval [0...1023]
 * BMS_Voltage: Integer in interval [0...500]
 * BSI_PostDriveWakeup: Boolean
 * BSI_PreDriveWakeup: Boolean
 * BSI_VCUSynchroGPC: Boolean
 * COUPURE_CONSO_CTP: Boolean
 * COUPURE_CONSO_CTPE2: Boolean
 * CalVDCLinkRequiredMonophasic: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 500
 * ComM_InhibitionStatusType: Integer in interval [0...255]
 * ComM_UserHandleType: Integer in interval [0...65535]
 * CounterType: Integer in interval [0...255]
 * DATA0: Integer in interval [0...255]
 * DATA1: Integer in interval [0...255]
 * DATA2: Integer in interval [0...255]
 * DATA3: Integer in interval [0...255]
 * DATA4: Integer in interval [0...255]
 * DATA5: Integer in interval [0...255]
 * DATA6: Integer in interval [0...255]
 * DATA7: Integer in interval [0...255]
 * DATA_ACQ_JDD_BSI_2: Integer in interval [0...255]
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCDC_HighVoltConnectionAllowed: Boolean
 * DCDC_InputCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCDC_InputVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCDC_OBCDCDCRTPowerLoad: Integer in interval [0...255]
 * DCDC_OVERTEMP: Boolean
 * DCDC_OutputCurrent: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCDC_OutputVoltage: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCDC_Temperature: Integer in interval [0...255]
 * DCDC_VoltageReference: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_NTC_MOD_5: Integer in interval [0...255]
 * DCHV_ADC_NTC_MOD_6: Integer in interval [0...255]
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * DCHV_IOM_ERR_CAP_FAIL_H: Boolean
 * DCHV_IOM_ERR_CAP_FAIL_L: Boolean
 * DCHV_IOM_ERR_OC_IOUT: Boolean
 * DCHV_IOM_ERR_OC_NEG: Boolean
 * DCHV_IOM_ERR_OC_POS: Boolean
 * DCHV_IOM_ERR_OT: Boolean
 * DCHV_IOM_ERR_OT_IN: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD5: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD6: Boolean
 * DCHV_IOM_ERR_OV_VOUT: Boolean
 * DCHV_IOM_ERR_UV_12V: Boolean
 * DCLV_Applied_Derating_Factor: Integer in interval [0...2047]
 * DCLV_Input_Current: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Measured_Voltage: Integer in interval [0...175]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCLV_OC_A_HV_FAULT: Boolean
 * DCLV_OC_A_LV_FAULT: Boolean
 * DCLV_OC_B_HV_FAULT: Boolean
 * DCLV_OC_B_LV_FAULT: Boolean
 * DCLV_OT_FAULT: Boolean
 * DCLV_OV_INT_A_FAULT: Boolean
 * DCLV_OV_INT_B_FAULT: Boolean
 * DCLV_OV_UV_HV_FAULT: Boolean
 * DCLV_PWM_STOP: Boolean
 * DCLV_Power: Integer in interval [0...2500]
 * DCLV_T_L_BUCK: Integer in interval [0...255]
 * DCLV_T_PP_A: Integer in interval [0...255]
 * DCLV_T_PP_B: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_A: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_B: Integer in interval [0...255]
 * DCLV_T_TX_PP: Integer in interval [0...255]
 * DCLV_Temp_Derating_Factor: Integer in interval [0...2047]
 * DCLV_VoltageReference: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * DIAG_INTEGRA_ELEC: Boolean
 * Debug1_1: Integer in interval [0...255]
 * Debug1_2: Integer in interval [0...255]
 * Debug1_3: Integer in interval [0...255]
 * Debug1_4: Integer in interval [0...255]
 * Debug1_5: Integer in interval [0...255]
 * Debug1_6: Integer in interval [0...255]
 * Debug1_7: Integer in interval [0...255]
 * Debug2_0: Integer in interval [0...255]
 * Debug2_1: Integer in interval [0...255]
 * Debug2_2: Integer in interval [0...255]
 * Debug2_3: Integer in interval [0...255]
 * Debug2_4: Integer in interval [0...255]
 * Debug2_5: Integer in interval [0...255]
 * Debug2_6: Integer in interval [0...255]
 * Debug2_7: Integer in interval [0...255]
 * Debug3_0: Integer in interval [0...255]
 * Debug3_1: Integer in interval [0...255]
 * Debug3_2: Integer in interval [0...255]
 * Debug3_3: Integer in interval [0...255]
 * Debug3_4: Integer in interval [0...255]
 * Debug3_5: Integer in interval [0...255]
 * Debug3_6: Integer in interval [0...255]
 * Debug3_7: Integer in interval [0...255]
 * Debug4_0: Integer in interval [0...255]
 * Debug4_1: Integer in interval [0...255]
 * Debug4_2: Integer in interval [0...255]
 * Debug4_3: Integer in interval [0...255]
 * Debug4_4: Integer in interval [0...255]
 * Debug4_5: Integer in interval [0...255]
 * Debug4_6: Integer in interval [0...255]
 * Debug4_7: Integer in interval [0...255]
 * Degug1_0: Integer in interval [0...255]
 * Dem_DTCGroupType: Integer in interval [0...16777215]
 * Dem_DTCStatusMaskType: Integer in interval [0...255]
 * Dem_EventIdType: Integer in interval [1...65535]
 * Dem_RatioIdType: Integer in interval [1...65535]
 * EDITION_CALIB: Integer in interval [0...255]
 * EDITION_SOFT: Integer in interval [0...255]
 * EFFAC_DEFAUT_DIAG: Boolean
 * EVSE_RTAB_STOP_CHARGE: Boolean
 * EcuM_TimeType: Integer in interval [0...4294967295]
 * IdtABSVehSpdThresholdOV: Integer in interval [0...100]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtACTempTripThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtACTempTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtACTempWarningThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtACTempWarningTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtAfts_DelayLed: Integer in interval [0...500]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtAfts_ElockActuatorTime1: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtAfts_ElockActuatorTime2: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtAmbientTemperaturePhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit1_ConfirmTime: Integer in interval [0...100]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit1_Threshold: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit2_ConfirmTime: Integer in interval [0...100]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit2_Threshold: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltageThreshold: Integer in interval [0...200]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltageTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtBulkSocConf: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtCOMLatchMaxDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtCalThresholdUVTripDischarge: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtCalTimeUVTripDischarge: Integer in interval [0...200]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtChLedCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtControlPilotDutyPlantMode: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDCLVFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryShortCircuit: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLVTimeConfirmDerating: Integer in interval [0...60]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtDCLV_MaxNumberRetries: Integer in interval [0...60]
 * IdtDCRelayVoltagePlantMode: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtDCTempTripThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtDCTempTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtDCTempWarningThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtDCTempWarningTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtDebounceADCReference: Integer in interval [1...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtDebounceControlPilot: Integer in interval [0...20]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtDebounceControlPilotPlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceDCRelayVoltagePlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceHoldNetworkCom: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtDebounceOvercurrentDCHV: Integer in interval [0...10]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtDebouncePhasePlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceProxDetect: Integer in interval [0...255]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtDebounceProximityPlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceRCD: Integer in interval [0...16]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtDebounceStateFailure: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateFinished: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateInProgress: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateStopped: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceTempoEndCharge: Integer in interval [0...120]
 *   Unit: [s], Factor: 5, Offset: 0
 * IdtDegMainWkuExtinctionTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDemandMaxCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtDutyCloseRelay: Integer in interval [50...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyLedCharge: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyPlug: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtELockDebounce: Integer in interval [0...255]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtELockSensorFaultThreshold: Integer in interval [0...180]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtELockSensorFaultTime: Integer in interval [0...20]
 *   Unit: [ms], Factor: 50, Offset: 0
 * IdtELockSensorLimit: Integer in interval [0...180]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtEVSEMaximumPowerLimit: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtElockActuatorTime: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtElockFeedbackCurrent: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackLock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackUnlock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockThreshold: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockTimeError: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtEnergyCapacity: Integer in interval [0...255]
 *   Unit: [kWh], Factor: 1, Offset: 0
 * IdtEnforcedMainWkuTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtExtChLedRGBCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtOpPlugLockRaw: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtExtPlgLedrCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtinctionTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtFrameDiagTime: Integer in interval [0...255]
 * IdtFrequencyActivateRelays: Integer in interval [0...40]
 *   Unit: [kHz], Factor: 1, Offset: 0
 * IdtFrequencyPWM: Integer in interval [0...100]
 *   Unit: [Hz], Factor: 20, Offset: 0
 * IdtGlobalTimeoutPlantMode: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtHVVTripThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtHVVTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtHVVWarningThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtHVVWarningTime: Integer in interval [1...2000]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtInputVoltageThreshold: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtInternalPartialWkuMaxDuration: Integer in interval [0...65535]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtLVPTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVVTripThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtLVVTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVVWarningThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtLVVWarningTime: Integer in interval [1...2000]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLedFaultTime: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtLedThresholdSCG: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtLedThresholdSCP: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 7000
 * IdtLinTableGlobalTempElement: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtLinTableGlobalVoltElement: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtMainWkuDevalidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtMainWkuValidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtMasterPartialWkuY1MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY1MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY2MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY2MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY3MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY3MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY4MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY4MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY5MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY5MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY6MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY6MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY7MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY7MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY8MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY8MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxDemmandCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxDemmandVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxDiscoveryCurrent: Integer in interval [0...200]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxDiscoveryVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxEfficiency: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtMaxInputACCurrentEVSE: Integer in interval [0...65535]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * IdtMaxInputVoltage110V: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 100
 * IdtMaxInputVoltage220V: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 100
 * IdtMaxNormalPlugLed: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtMaxOutputDCHVCurrent: Integer in interval [0...200]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtMaxOutputDCHVVoltage: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 450
 * IdtMaxPrechargeCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxPrechargeVDCLinkMono: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 300
 * IdtMaxPrechargeVDCLinkTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 500
 * IdtMaxPrechargeVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxTimeChargeError: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeChargeInProgress: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeDiagTools: Integer in interval [0...1024]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtMaxTimeEndOfCharge: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeGuideManagement: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimePlantMode: Integer in interval [0...250]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtMaxTimeProgrammingCharge: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeShutdown: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMinDCHVOutputVoltage: Integer in interval [0...50]
 *   Unit: [V], Factor: 10, Offset: 0
 * IdtMinEfficiency: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtMinInputVoltage110V: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinInputVoltage220V: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinNormalPlugLed: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtMinPrechargeVDCLinkMono: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinPrechargeVDCLinkTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMsrTempRaw: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtOBCFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryOvertemp: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryVoltageError: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCMaxEfficiency: Integer in interval [0...150]
 *   Factor: 0.01, Offset: 0
 * IdtOBCMinEfficiency: Integer in interval [0...150]
 *   Factor: 0.01, Offset: 0
 * IdtOBCOffsetAllowed: Integer in interval [0...10000]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBCPTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtOBC_DelayEfficiencyDCLV: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_DelayEfficiencyHVMono: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_DelayEfficiencyHVTri: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_MaxNumberRetries: Integer in interval [0...60]
 * IdtOBC_PowerAvailableControlPilot: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBC_PowerAvailableHardware: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBC_PowerMaxCalculated: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * IdtOBC_PowerMaxValue: Integer in interval [0...25]
 *   Unit: [W], Factor: 1000, Offset: 0
 * IdtOffsetAllowed: Integer in interval [0...1000]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOutputTempAftsales: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtPDERATING_OBC: Integer in interval [0...1024]
 * IdtPhasePlantmode: Integer in interval [0...220]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtPlgLedrCtrl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtPlugLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtPlugLedMaxOCDetection: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 5000
 * IdtPlugLedMinOCDetection: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlugLedThresholdSCG: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlugLedThresholdSCP: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 7000
 * IdtProximityDetectPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityDetectPlantMode: Integer in interval [0...1200]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityDetectVoltThreshold: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityVoltageStartingPlantMode: Integer in interval [0...1200]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtRCDLineGndSCConfirmTime: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtRCDLineGndSCRehabilitTime: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtRECHARGE_HMI_STATE: Integer in interval [0...255]
 * IdtRampGradient: Integer in interval [0...100]
 *   Unit: [deg C / s], Factor: 0.1, Offset: 0
 * IdtShutdownPreparationMaxTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtShutdownPreparationMinTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtSignalDiagTime: Integer in interval [0...255]
 * IdtSlavePartialWkuXDevalidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtSlavePartialWkuXLockTime: Integer in interval [0...50000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtSlavePartialWkuXValidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtStopChargeDemmandTimer: Integer in interval [0...600]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtSubstituteValueTemp: Integer in interval [0...240]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtT_INHIB_DIAG_COM: Integer in interval [0...51]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTargetPrechargeCurrent: Integer in interval [0...100]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtTempDerating: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtTempSyncRectPhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtThresholdLVOvercurrentHW: Integer in interval [0...400]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtThresholdNoAC: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtThresholdTempFault: Integer in interval [0...5000]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtTimeBatteryVoltageSafety: Integer in interval [0...255]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeBeforeRunningDiag: Integer in interval [0...51]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeCloseRelay: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeConfirmNoAC: Integer in interval [0...300]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeDebChargePush: Integer in interval [0...15]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeDiagTemp: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeElockFaultDetected: Integer in interval [0...60]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeLedCharge: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeLockDelay: Integer in interval [0...100]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeNomMainWkuDisord: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeNomMainWkuIncst: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeNomMainWkuRehabilit: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePlug: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePushButtonKeepValue: Integer in interval [0...600]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePushInhDelay: Integer in interval [0...1000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeRCDPulse: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtTimeRelaysPrechargeClosed: Integer in interval [0...200]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeWakeupCoolingMax: Integer in interval [0...240]
 *   Unit: [minutes], Factor: 1, Offset: 0
 * IdtTimeWakeupCoolingMin: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupHoldDiscontactorMax: Integer in interval [0...600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupHoldDiscontactorMin: Integer in interval [0...120]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupPiStateInfo: Integer in interval [0...120]
 *   Unit: [s], Factor: 5, Offset: 0
 * IdtTimeoutPlantMode: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTo_DgMux_Prod: Integer in interval [0...51]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtVDCLinkRequiredTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 500
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtVehicleSpeedDegThreshold: Integer in interval [0...255]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtVehicleSpeedThreshold: Integer in interval [0...255]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtVehicleSpeedThresholdDiag: Integer in interval [0...255]
 *   Unit: [km/h], Factor: 0.5, Offset: 0
 * IdtVoltageCorrectionOffset: Integer in interval [-32768...32767]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtVoltageExternalADCReference: Integer in interval [1...350]
 *   Unit: [mV], Factor: 10, Offset: 0
 * MODE_DIAG: Boolean
 * NEW_JDD_OBC_DCDC_BYTE_0: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_1: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_2: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_3: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_4: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_5: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_6: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_7: Integer in interval [0...255]
 * NetworkHandleType: Integer in interval [0...255]
 * NvM_BlockIdType: Integer in interval [1...32767]
 * OBC_CommunicationSt: Boolean
 * OBC_CoolingWakeup: Boolean
 * OBC_DCChargingPlugAConnConf: Boolean
 * OBC_HVBattRechargeWakeup: Boolean
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_HoldDiscontactorWakeup: Boolean
 * OBC_OBCStartSt: Boolean
 * OBC_OBCTemp: Integer in interval [0...255]
 * OBC_OutputCurrent: Integer in interval [0...511]
 * OBC_OutputVoltage: Integer in interval [0...8191]
 * OBC_PIStateInfoWakeup: Boolean
 * OBC_PlugVoltDetection: Boolean
 * OBC_PowerMax: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * OBC_PushChargeType: Boolean
 * OBC_SocketTempL: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * OBC_SocketTempN: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * PFC_IOM_ERR_OC_PH1: Boolean
 * PFC_IOM_ERR_OC_PH2: Boolean
 * PFC_IOM_ERR_OC_PH3: Boolean
 * PFC_IOM_ERR_OC_SHUNT1: Boolean
 * PFC_IOM_ERR_OC_SHUNT2: Boolean
 * PFC_IOM_ERR_OC_SHUNT3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M1: Boolean
 * PFC_IOM_ERR_OT_NTC1_M3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M4: Boolean
 * PFC_IOM_ERR_OV_DCLINK: Boolean
 * PFC_IOM_ERR_OV_VPH12: Boolean
 * PFC_IOM_ERR_OV_VPH23: Boolean
 * PFC_IOM_ERR_OV_VPH31: Boolean
 * PFC_IOM_ERR_UVLO_ISO4: Boolean
 * PFC_IOM_ERR_UVLO_ISO7: Boolean
 * PFC_IOM_ERR_UV_12V: Boolean
 * PFC_IPH1_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH2_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH3_RMS_0A1: Integer in interval [0...1023]
 * PFC_Temp_M1_C: Integer in interval [0...255]
 * PFC_Temp_M3_C: Integer in interval [0...255]
 * PFC_Temp_M4_C: Integer in interval [0...255]
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH1_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH2_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * PFC_VPH3_Freq_Hz: Integer in interval [0...1023]
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * Rte_DT_IdtArrayDGN_EOL_NVM_RamMirror_0: Integer in interval [0...255]
 * Rte_DT_IdtArrayInitAIMVoltRef_0: Integer in interval [0...255]
 * Rte_DT_IdtArrayJDDNvMRamMirror_0: Integer in interval [0...255]
 * Rte_DT_IdtPCOMNvMArray_0: Integer in interval [0...255]
 * Rte_DT_IdtPSHNvMArray_0: Integer in interval [0...255]
 * Rte_DT_IdtPlantModeTestUTPlugin_0: Integer in interval [0...255]
 * SUPV_CoolingWupState: Boolean
 * SUPV_DTCRegistred: Boolean
 * SUPV_HVBattChargeWupState: Boolean
 * SUPV_HoldDiscontactorWupState: Boolean
 * SUPV_PIStateInfoWupState: Boolean
 * SUPV_PostDriveWupState: Boolean
 * SUPV_PreDriveWupState: Boolean
 * SUPV_PrecondElecWupState: Boolean
 * SUPV_RCDLineState: Boolean
 * SUPV_StopDelayedHMIWupState: Boolean
 * SUP_CommandVDCLink_V: Integer in interval [0...1023]
 *   Unit: [V], Factor: 1, Offset: 0
 * TimeInMicrosecondsType: Integer in interval [0...4294967295]
 * VCU_AccPedalPosition: Integer in interval [0...100]
 * VCU_ActivedischargeCommand: Boolean
 * VCU_CDEAccJDD: Boolean
 * VCU_CompteurRazGct: Integer in interval [0...253]
 * VCU_CptTemporel: Integer in interval [0...4294967295]
 * VCU_DCDCVoltageReq: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * VCU_DDEGMVSEEM: Integer in interval [0...100]
 * VCU_DMDActivChiller: Boolean
 * VCU_DMDMeap2SEEM: Integer in interval [0...100]
 * VCU_DiagMuxOnPwt: Boolean
 * VCU_ElecMeterSaturation: Boolean
 * VCU_Kilometrage: Integer in interval [0...16777214]
 * VCU_PrecondElecWakeup: Boolean
 * VCU_StopDelayedHMIWakeup: Boolean
 * VERSION_APPLI: Integer in interval [0...255]
 * VERSION_SOFT: Integer in interval [0...255]
 * VERSION_SYSTEME: Integer in interval [0...255]
 * VERS_DATE2_ANNEE: Integer in interval [0...99]
 * VERS_DATE2_JOUR: Integer in interval [0...31]
 * VERS_DATE2_MOIS: Integer in interval [0...12]
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * WdgM_ModeType: Integer in interval [0...255]
 * WdgM_SupervisedEntityIdType: Integer in interval [0...65535]
 * WdgM_ViolationType: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * sint32: Integer in interval [-2147483648...2147483647] (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_CC2_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 * BMS_FastChargeSt: Enumeration of integer in interval [0...3] with enumerators
 * BMS_MainConnectorState: Enumeration of integer in interval [0...3] with enumerators
 * BMS_SlowChargeSt: Enumeration of integer in interval [0...3] with enumerators
 * BSI_ChargeState: Enumeration of integer in interval [0...3] with enumerators
 * BSI_ChargeTypeStatus: Enumeration of integer in interval [0...3] with enumerators
 * BSI_LockedVehicle: Enumeration of integer in interval [0...3] with enumerators
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 * BSI_VCUHeatPumpWorkingMode: Enumeration of integer in interval [0...31] with enumerators
 * BswM_ESH_Mode: Enumeration of integer in interval [0...255] with enumerators
 * BswM_ESH_RunRequest: Enumeration of integer in interval [0...255] with enumerators
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 * DCDC_ActivedischargeSt: Enumeration of integer in interval [0...255] with enumerators
 * DCDC_Fault: Enumeration of integer in interval [0...255] with enumerators
 * DCDC_FaultLampRequest: Enumeration of integer in interval [0...3] with enumerators
 * DCDC_OBCMainContactorReq: Enumeration of integer in interval [0...255] with enumerators
 * DCDC_OBCQuickChargeContactorReq: Enumeration of integer in interval [0...255] with enumerators
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 * Dcm_CommunicationModeType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_ConfirmationStatusType: Enumeration of integer in interval [0...3] with enumerators
 * Dcm_DiagnosticSessionControlType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_EcuResetType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 * Dcm_ProtocolType: Enumeration of integer in interval [0...254] with enumerators
 * Dcm_RequestKindType: Enumeration of integer in interval [0...2] with enumerators
 * Dcm_SecLevelType: Enumeration of integer in interval [0...1] with enumerators
 * Dcm_SesCtrlType: Enumeration of integer in interval [0...80] with enumerators
 * Dem_DTCFormatType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTCKindType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTCOriginType: Enumeration of integer in interval [0...65535] with enumerators
 * Dem_DTCSeverityType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTRControlType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DebounceResetStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DebouncingStateType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IndicatorStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IumprDenomCondIdType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IumprDenomCondStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IumprReadinessGroupType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_MonitorStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_OperationCycleStateType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 * EcuM_BootTargetType: Enumeration of integer in interval [0...2] with enumerators
 * EcuM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 * EcuM_ShutdownCauseType: Enumeration of integer in interval [0...3] with enumerators
 * EcuM_StateType: Enumeration of integer in interval [0...144] with enumerators
 * EcuM_UserType: Enumeration of integer in interval [0...255] with enumerators
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 * IdtPlantModeDTCNumber: Enumeration of integer in interval [0...255] with enumerators
 * IdtPlantModeTestInfoDID_Result: Enumeration of integer in interval [0...255] with enumerators
 * IdtPlantModeTestInfoDID_State: Enumeration of integer in interval [0...255] with enumerators
 * IdtPlantModeTestInfoDID_Test: Enumeration of integer in interval [0...255] with enumerators
 * NvM_RequestResultType: Enumeration of integer in interval [0...8] with enumerators
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 * OBC_CP_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 * OBC_ElockState: Enumeration of integer in interval [0...3] with enumerators
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 * PFC_PFCStatus: Enumeration of integer in interval [0...15] with enumerators
 * SUPV_ECUElecStateRCD: Enumeration of integer in interval [0...7] with enumerators
 * SUP_RequestDCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 * SUP_RequestPFCStatus: Enumeration of integer in interval [0...15] with enumerators
 * SUP_RequestStatusDCHV: Enumeration of integer in interval [0...15] with enumerators
 * VCU_CDEApcJDD: Enumeration of integer in interval [0...3] with enumerators
 * VCU_DCDCActivation: Enumeration of integer in interval [0...3] with enumerators
 * VCU_EPWT_Status: Enumeration of integer in interval [0...3] with enumerators
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 * VCU_EtatPrincipSev: Enumeration of integer in interval [0...3] with enumerators
 * VCU_EtatReseauElec: Enumeration of integer in interval [0...15] with enumerators
 * VCU_Keyposition: Enumeration of integer in interval [0...3] with enumerators
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 * VCU_PosShuntJDD: Enumeration of integer in interval [0...3] with enumerators
 * WdgMMode: Enumeration of integer in interval [0...255] with enumerators
 * WdgM_GlobalStatusType: Enumeration of integer in interval [0...4] with enumerators
 * WdgM_LocalStatusType: Enumeration of integer in interval [0...4] with enumerators
 *
 * Array Types:
 * ============
 * DataArrayType_uint8_1: Array with 1 element(s) of type uint8
 * DataArrayType_uint8_2: Array with 2 element(s) of type uint8
 * DataArrayType_uint8_4: Array with 4 element(s) of type uint8
 * Dcm_Data10ByteType: Array with 10 element(s) of type uint8
 * Dcm_Data16ByteType: Array with 16 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data20ByteType: Array with 20 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data4095ByteType: Array with 4095 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data76ByteType: Array with 76 element(s) of type uint8
 * Dem_MaxDataValueType: Array with 4 element(s) of type uint8
 * IdtArrayDGN_EOL_NVM_RamMirror: Array with 76 element(s) of type Rte_DT_IdtArrayDGN_EOL_NVM_RamMirror_0
 * IdtArrayInitAIMVoltRef: Array with 3 element(s) of type Rte_DT_IdtArrayInitAIMVoltRef_0
 * IdtArrayJDDNvMRamMirror: Array with 2000 element(s) of type Rte_DT_IdtArrayJDDNvMRamMirror_0
 * IdtLinTableGlobalTemp: Array with 39 element(s) of type IdtLinTableGlobalTempElement
 * IdtLinTableGlobalVolt: Array with 39 element(s) of type IdtLinTableGlobalVoltElement
 * IdtPCOMNvMArray: Array with 16 element(s) of type Rte_DT_IdtPCOMNvMArray_0
 * IdtPSHNvMArray: Array with 4 element(s) of type Rte_DT_IdtPSHNvMArray_0
 * IdtPlantModeTestUTPlugin: Array with 2 element(s) of type Rte_DT_IdtPlantModeTestUTPlugin_0
 *
 * Record Types:
 * =============
 * SG_DC2: Record with elements
 *   DCDC_ActivedischargeSt of type DCDC_ActivedischargeSt
 *   DCDC_InputCurrent of type DCDC_InputCurrent
 *   DCDC_OBCDCDCRTPowerLoad of type DCDC_OBCDCDCRTPowerLoad
 *   DCDC_OBCMainContactorReq of type DCDC_OBCMainContactorReq
 *   DCDC_OBCQuickChargeContactorReq of type DCDC_OBCQuickChargeContactorReq
 *   DCDC_OutputCurrent of type DCDC_OutputCurrent
 *
 *********************************************************************************************************************/


#define XCP_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Schedulable Entity Name: CanXcp_MainFunction
 *
 *********************************************************************************************************************/

FUNC(void, XCP_CODE) CanXcp_MainFunction(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CanXcp_MainFunction
 *********************************************************************************************************************/

 /* PRQA S 3226, 1863 L1 */ /* MD_Rte_Os, MD_Rte_Os */
  TSC_Xcp_SchM_Enter_Xcp_CANXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_CANXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_FRXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_FRXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_FRXCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Exit_Xcp_FRXCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Enter_Xcp_FRXCP_EXCLUSIVE_AREA_2();
  TSC_Xcp_SchM_Exit_Xcp_FRXCP_EXCLUSIVE_AREA_2();
  TSC_Xcp_SchM_Enter_Xcp_TCPIPXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_TCPIPXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_XCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_XCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_XCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Exit_Xcp_XCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Enter_Xcp_XCP_EXCLUSIVE_AREA_2();
  TSC_Xcp_SchM_Exit_Xcp_XCP_EXCLUSIVE_AREA_2();
/* PRQA L:L1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Schedulable Entity Name: Xcp_MainFunction
 *
 *********************************************************************************************************************/

FUNC(void, XCP_CODE) Xcp_MainFunction(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Xcp_MainFunction
 *********************************************************************************************************************/

 /* PRQA S 3226, 1863 L1 */ /* MD_Rte_Os, MD_Rte_Os */
  TSC_Xcp_SchM_Enter_Xcp_CANXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_CANXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_FRXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_FRXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_FRXCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Exit_Xcp_FRXCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Enter_Xcp_FRXCP_EXCLUSIVE_AREA_2();
  TSC_Xcp_SchM_Exit_Xcp_FRXCP_EXCLUSIVE_AREA_2();
  TSC_Xcp_SchM_Enter_Xcp_TCPIPXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_TCPIPXCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_XCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Exit_Xcp_XCP_EXCLUSIVE_AREA_0();
  TSC_Xcp_SchM_Enter_Xcp_XCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Exit_Xcp_XCP_EXCLUSIVE_AREA_1();
  TSC_Xcp_SchM_Enter_Xcp_XCP_EXCLUSIVE_AREA_2();
  TSC_Xcp_SchM_Exit_Xcp_XCP_EXCLUSIVE_AREA_2();
/* PRQA L:L1 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define XCP_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
