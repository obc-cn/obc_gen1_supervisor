/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_EthIf.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_EthIf.h"
#include "TSC_SchM_EthIf.h"
void TSC_EthIf_SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_CTRL_INIT(void)
{
  SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_CTRL_INIT();
}
void TSC_EthIf_SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_CTRL_INIT(void)
{
  SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_CTRL_INIT();
}
void TSC_EthIf_SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_RXTX_STATS(void)
{
  SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_RXTX_STATS();
}
void TSC_EthIf_SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_RXTX_STATS(void)
{
  SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_RXTX_STATS();
}
void TSC_EthIf_SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_SET_CTRL_MODE(void)
{
  SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_SET_CTRL_MODE();
}
void TSC_EthIf_SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_SET_CTRL_MODE(void)
{
  SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_SET_CTRL_MODE();
}
void TSC_EthIf_SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_TX_MIRROR_ELEMENT(void)
{
  SchM_Enter_EthIf_ETHIF_EXCLUSIVE_AREA_TX_MIRROR_ELEMENT();
}
void TSC_EthIf_SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_TX_MIRROR_ELEMENT(void)
{
  SchM_Exit_EthIf_ETHIF_EXCLUSIVE_AREA_TX_MIRROR_ELEMENT();
}
