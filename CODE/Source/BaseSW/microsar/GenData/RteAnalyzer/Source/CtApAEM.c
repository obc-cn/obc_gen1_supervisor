/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApAEM.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApAEM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApAEM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApAEM.h"
#include "TSC_CtApAEM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApAEM_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * BSI_PostDriveWakeup: Boolean
 * BSI_PreDriveWakeup: Boolean
 * IdtCOMLatchMaxDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtDegMainWkuExtinctionTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtEnforcedMainWkuTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtExtinctionTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtInternalPartialWkuMaxDuration: Integer in interval [0...65535]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMainWkuDevalidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtMainWkuValidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtMasterPartialWkuY1MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY1MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY2MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY2MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY3MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY3MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY4MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY4MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY5MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY5MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY6MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY6MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY7MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY7MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY8MaxDuration: Integer in interval [0...4000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMasterPartialWkuY8MinDuration: Integer in interval [0...1024]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtRCDLineGndSCConfirmTime: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtRCDLineGndSCRehabilitTime: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtShutdownPreparationMaxTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtShutdownPreparationMinTime: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtSlavePartialWkuXDevalidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtSlavePartialWkuXLockTime: Integer in interval [0...50000]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtSlavePartialWkuXValidTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtTimeNomMainWkuDisord: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeNomMainWkuIncst: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeNomMainWkuRehabilit: Integer in interval [0...650]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeRCDPulse: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtVehicleSpeedDegThreshold: Integer in interval [0...255]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtVehicleSpeedThreshold: Integer in interval [0...255]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * OBC_CoolingWakeup: Boolean
 * OBC_HVBattRechargeWakeup: Boolean
 * OBC_HoldDiscontactorWakeup: Boolean
 * OBC_PIStateInfoWakeup: Boolean
 * SUPV_PostDriveWupState: Boolean
 * SUPV_PreDriveWupState: Boolean
 * SUPV_PrecondElecWupState: Boolean
 * SUPV_RCDLineState: Boolean
 * SUPV_StopDelayedHMIWupState: Boolean
 * VCU_PrecondElecWakeup: Boolean
 * VCU_StopDelayedHMIWakeup: Boolean
 * boolean: Boolean (standard type)
 * sint32: Integer in interval [-2147483648...2147483647] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_RCD (0U)
 *   Cx1_No_main_wake_up_request (1U)
 *   Cx2_Main_wake_up_request (2U)
 *   Cx3_Not_valid (3U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * SUPV_ECUElecStateRCD: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Partial_wakeup (0U)
 *   Cx1_Internal_wakeup (1U)
 *   Cx2_Transitory (2U)
 *   Cx3_Nominal_main_wakeup (3U)
 *   Cx4_Degraded_main_wakeup (4U)
 *   Cx5_Shutdown_preparation (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDegMainWkuExtinctionTime Rte_CData_UCE_tiDegMainWkuDeac_C(void)
 *   IdtTimeNomMainWkuDisord Rte_CData_UCE_tiMainDisrdDet_C(void)
 *   IdtTimeNomMainWkuIncst Rte_CData_UCE_tiMainIncstDet_C(void)
 *   IdtEnforcedMainWkuTime Rte_CData_UCE_tiMainTransForc_C(void)
 *   IdtTimeNomMainWkuRehabilit Rte_CData_UCE_tiMainWkuReh_C(void)
 *   IdtCOMLatchMaxDuration Rte_CData_UCE_tiMaxTiComLatch_C(void)
 *   IdtInternalPartialWkuMaxDuration Rte_CData_UCE_tiMaxTiIntPtlWku_C(void)
 *   IdtMasterPartialWkuY1MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(void)
 *   IdtMasterPartialWkuY2MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(void)
 *   IdtMasterPartialWkuY3MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(void)
 *   IdtMasterPartialWkuY4MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(void)
 *   IdtMasterPartialWkuY5MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(void)
 *   IdtMasterPartialWkuY6MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(void)
 *   IdtMasterPartialWkuY7MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(void)
 *   IdtMasterPartialWkuY8MaxDuration Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(void)
 *   IdtShutdownPreparationMaxTime Rte_CData_UCE_tiMaxTiShutDownPrep_C(void)
 *   IdtMasterPartialWkuY1MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(void)
 *   IdtMasterPartialWkuY2MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(void)
 *   IdtMasterPartialWkuY3MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(void)
 *   IdtMasterPartialWkuY4MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(void)
 *   IdtMasterPartialWkuY5MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(void)
 *   IdtMasterPartialWkuY6MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(void)
 *   IdtMasterPartialWkuY7MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(void)
 *   IdtMasterPartialWkuY8MinDuration Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(void)
 *   IdtShutdownPreparationMinTime Rte_CData_UCE_tiMinTiShutDownPrep_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX10Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX11Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX12Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX13Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX14Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX15Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX16Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX1Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX2Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX3Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX4Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX5Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX6Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX7Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX8Lock_C(void)
 *   IdtSlavePartialWkuXLockTime Rte_CData_UCE_tiPtlWkuX9Lock_C(void)
 *   IdtTimeRCDPulse Rte_CData_UCE_tiRCDLineCmdAcv_C(void)
 *   IdtRCDLineGndSCConfirmTime Rte_CData_UCE_tiRCDLineScgDet_C(void)
 *   IdtRCDLineGndSCRehabilitTime Rte_CData_UCE_tiRCDLineScgReh_C(void)
 *   IdtExtinctionTime Rte_CData_UCE_tiTransitoryDeac_C(void)
 *   IdtVehicleSpeedThreshold Rte_CData_UCE_spdThdDegDeac_C(void)
 *   IdtVehicleSpeedDegThreshold Rte_CData_UCE_spdThdNomDeac_C(void)
 *   IdtMainWkuValidTime Rte_CData_UCE_tiMainWkuAcv_C(void)
 *   IdtMainWkuDevalidTime Rte_CData_UCE_tiNomMainWkuDeac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX10Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX10Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX11Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX11Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX12Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX12Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX13Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX13Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX14Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX14Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX15Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX15Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX16Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX16Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX1Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX1Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX2Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX2Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX3Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX3Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX4Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX4Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX5Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX5Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX6Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX6Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX7Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX7Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX8Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX8Deac_C(void)
 *   IdtSlavePartialWkuXValidTime Rte_CData_UCE_tiPtlWkuX9Acv_C(void)
 *   IdtSlavePartialWkuXDevalidTime Rte_CData_UCE_tiPtlWkuX9Deac_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX10_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX11_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX12_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX13_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX14_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX15_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX16_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX1_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX2_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX3_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX4_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX5_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX6_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX7_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX8_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuX9_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY1_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY2_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY3_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY4_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY5_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY6_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY7_C(void)
 *   boolean Rte_CData_UCE_bInhPtlWkuY8_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(void)
 *   boolean Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(void)
 *   boolean Rte_CData_UCE_noUCETyp_C(void)
 *
 *********************************************************************************************************************/


#define CtApAEM_START_SEC_CODE
#include "CtApAEM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CtApAEM_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CtApAEM_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApAEM_CODE) CtApAEM_Init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CtApAEM_Init
 *********************************************************************************************************************/

  IdtDegMainWkuExtinctionTime UCE_tiDegMainWkuDeac_C_data;
  IdtTimeNomMainWkuDisord UCE_tiMainDisrdDet_C_data;
  IdtTimeNomMainWkuIncst UCE_tiMainIncstDet_C_data;
  IdtEnforcedMainWkuTime UCE_tiMainTransForc_C_data;
  IdtTimeNomMainWkuRehabilit UCE_tiMainWkuReh_C_data;
  IdtCOMLatchMaxDuration UCE_tiMaxTiComLatch_C_data;
  IdtInternalPartialWkuMaxDuration UCE_tiMaxTiIntPtlWku_C_data;
  IdtMasterPartialWkuY1MaxDuration UCE_tiMaxTiMstPtlWkuY1_C_data;
  IdtMasterPartialWkuY2MaxDuration UCE_tiMaxTiMstPtlWkuY2_C_data;
  IdtMasterPartialWkuY3MaxDuration UCE_tiMaxTiMstPtlWkuY3_C_data;
  IdtMasterPartialWkuY4MaxDuration UCE_tiMaxTiMstPtlWkuY4_C_data;
  IdtMasterPartialWkuY5MaxDuration UCE_tiMaxTiMstPtlWkuY5_C_data;
  IdtMasterPartialWkuY6MaxDuration UCE_tiMaxTiMstPtlWkuY6_C_data;
  IdtMasterPartialWkuY7MaxDuration UCE_tiMaxTiMstPtlWkuY7_C_data;
  IdtMasterPartialWkuY8MaxDuration UCE_tiMaxTiMstPtlWkuY8_C_data;
  IdtShutdownPreparationMaxTime UCE_tiMaxTiShutDownPrep_C_data;
  IdtMasterPartialWkuY1MinDuration UCE_tiMinTiMstPtlWkuY1_C_data;
  IdtMasterPartialWkuY2MinDuration UCE_tiMinTiMstPtlWkuY2_C_data;
  IdtMasterPartialWkuY3MinDuration UCE_tiMinTiMstPtlWkuY3_C_data;
  IdtMasterPartialWkuY4MinDuration UCE_tiMinTiMstPtlWkuY4_C_data;
  IdtMasterPartialWkuY5MinDuration UCE_tiMinTiMstPtlWkuY5_C_data;
  IdtMasterPartialWkuY6MinDuration UCE_tiMinTiMstPtlWkuY6_C_data;
  IdtMasterPartialWkuY7MinDuration UCE_tiMinTiMstPtlWkuY7_C_data;
  IdtMasterPartialWkuY8MinDuration UCE_tiMinTiMstPtlWkuY8_C_data;
  IdtShutdownPreparationMinTime UCE_tiMinTiShutDownPrep_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX10Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX11Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX12Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX13Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX14Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX15Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX16Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX1Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX2Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX3Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX4Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX5Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX6Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX7Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX8Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX9Lock_C_data;
  IdtTimeRCDPulse UCE_tiRCDLineCmdAcv_C_data;
  IdtRCDLineGndSCConfirmTime UCE_tiRCDLineScgDet_C_data;
  IdtRCDLineGndSCRehabilitTime UCE_tiRCDLineScgReh_C_data;
  IdtExtinctionTime UCE_tiTransitoryDeac_C_data;
  IdtVehicleSpeedThreshold UCE_spdThdDegDeac_C_data;
  IdtVehicleSpeedDegThreshold UCE_spdThdNomDeac_C_data;
  IdtMainWkuValidTime UCE_tiMainWkuAcv_C_data;
  IdtMainWkuDevalidTime UCE_tiNomMainWkuDeac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX10Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX10Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX11Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX11Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX12Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX12Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX13Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX13Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX14Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX14Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX15Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX15Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX16Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX16Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX1Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX1Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX2Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX2Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX3Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX3Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX4Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX4Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX5Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX5Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX6Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX6Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX7Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX7Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX8Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX8Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX9Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX9Deac_C_data;
  boolean UCE_bInhPtlWkuX10_C_data;
  boolean UCE_bInhPtlWkuX11_C_data;
  boolean UCE_bInhPtlWkuX12_C_data;
  boolean UCE_bInhPtlWkuX13_C_data;
  boolean UCE_bInhPtlWkuX14_C_data;
  boolean UCE_bInhPtlWkuX15_C_data;
  boolean UCE_bInhPtlWkuX16_C_data;
  boolean UCE_bInhPtlWkuX1_C_data;
  boolean UCE_bInhPtlWkuX2_C_data;
  boolean UCE_bInhPtlWkuX3_C_data;
  boolean UCE_bInhPtlWkuX4_C_data;
  boolean UCE_bInhPtlWkuX5_C_data;
  boolean UCE_bInhPtlWkuX6_C_data;
  boolean UCE_bInhPtlWkuX7_C_data;
  boolean UCE_bInhPtlWkuX8_C_data;
  boolean UCE_bInhPtlWkuX9_C_data;
  boolean UCE_bInhPtlWkuY1_C_data;
  boolean UCE_bInhPtlWkuY2_C_data;
  boolean UCE_bInhPtlWkuY3_C_data;
  boolean UCE_bInhPtlWkuY4_C_data;
  boolean UCE_bInhPtlWkuY5_C_data;
  boolean UCE_bInhPtlWkuY6_C_data;
  boolean UCE_bInhPtlWkuY7_C_data;
  boolean UCE_bInhPtlWkuY8_C_data;
  boolean UCE_bSlavePtlWkuX10AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX11AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX12AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX13AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX14AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX15AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX16AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX1AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX2AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX3AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX4AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX5AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX6AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX7AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX8AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX9AcvMod_C_data;
  boolean UCE_noUCETyp_C_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  UCE_tiDegMainWkuDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiDegMainWkuDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainDisrdDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainDisrdDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainIncstDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainIncstDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainTransForc_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainTransForc_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainWkuReh_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainWkuReh_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiComLatch_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiComLatch_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiIntPtlWku_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiIntPtlWku_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiShutDownPrep_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiShutDownPrep_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiShutDownPrep_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiShutDownPrep_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineCmdAcv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineCmdAcv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineScgDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineScgReh_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgReh_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiTransitoryDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiTransitoryDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_spdThdDegDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_spdThdDegDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_spdThdNomDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_spdThdNomDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainWkuAcv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainWkuAcv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiNomMainWkuDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiNomMainWkuDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX10_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX10_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX11_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX11_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX12_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX12_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX13_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX13_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX14_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX14_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX15_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX15_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX16_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX16_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX1_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX2_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX3_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX4_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX5_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX6_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX7_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX8_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX9_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX9_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX10AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX11AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX12AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX13AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX14AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX15AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX16AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX1AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX2AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX3AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX4AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX5AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX6AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX7AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX8AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX9AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_noUCETyp_C_data = TSC_CtApAEM_Rte_CData_UCE_noUCETyp_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApAEM_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApAEM_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApAEM_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApAEM_CODE) RCtApAEM_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApAEM_task10msA
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtDegMainWkuExtinctionTime UCE_tiDegMainWkuDeac_C_data;
  IdtTimeNomMainWkuDisord UCE_tiMainDisrdDet_C_data;
  IdtTimeNomMainWkuIncst UCE_tiMainIncstDet_C_data;
  IdtEnforcedMainWkuTime UCE_tiMainTransForc_C_data;
  IdtTimeNomMainWkuRehabilit UCE_tiMainWkuReh_C_data;
  IdtCOMLatchMaxDuration UCE_tiMaxTiComLatch_C_data;
  IdtInternalPartialWkuMaxDuration UCE_tiMaxTiIntPtlWku_C_data;
  IdtMasterPartialWkuY1MaxDuration UCE_tiMaxTiMstPtlWkuY1_C_data;
  IdtMasterPartialWkuY2MaxDuration UCE_tiMaxTiMstPtlWkuY2_C_data;
  IdtMasterPartialWkuY3MaxDuration UCE_tiMaxTiMstPtlWkuY3_C_data;
  IdtMasterPartialWkuY4MaxDuration UCE_tiMaxTiMstPtlWkuY4_C_data;
  IdtMasterPartialWkuY5MaxDuration UCE_tiMaxTiMstPtlWkuY5_C_data;
  IdtMasterPartialWkuY6MaxDuration UCE_tiMaxTiMstPtlWkuY6_C_data;
  IdtMasterPartialWkuY7MaxDuration UCE_tiMaxTiMstPtlWkuY7_C_data;
  IdtMasterPartialWkuY8MaxDuration UCE_tiMaxTiMstPtlWkuY8_C_data;
  IdtShutdownPreparationMaxTime UCE_tiMaxTiShutDownPrep_C_data;
  IdtMasterPartialWkuY1MinDuration UCE_tiMinTiMstPtlWkuY1_C_data;
  IdtMasterPartialWkuY2MinDuration UCE_tiMinTiMstPtlWkuY2_C_data;
  IdtMasterPartialWkuY3MinDuration UCE_tiMinTiMstPtlWkuY3_C_data;
  IdtMasterPartialWkuY4MinDuration UCE_tiMinTiMstPtlWkuY4_C_data;
  IdtMasterPartialWkuY5MinDuration UCE_tiMinTiMstPtlWkuY5_C_data;
  IdtMasterPartialWkuY6MinDuration UCE_tiMinTiMstPtlWkuY6_C_data;
  IdtMasterPartialWkuY7MinDuration UCE_tiMinTiMstPtlWkuY7_C_data;
  IdtMasterPartialWkuY8MinDuration UCE_tiMinTiMstPtlWkuY8_C_data;
  IdtShutdownPreparationMinTime UCE_tiMinTiShutDownPrep_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX10Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX11Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX12Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX13Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX14Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX15Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX16Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX1Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX2Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX3Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX4Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX5Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX6Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX7Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX8Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX9Lock_C_data;
  IdtTimeRCDPulse UCE_tiRCDLineCmdAcv_C_data;
  IdtRCDLineGndSCConfirmTime UCE_tiRCDLineScgDet_C_data;
  IdtRCDLineGndSCRehabilitTime UCE_tiRCDLineScgReh_C_data;
  IdtExtinctionTime UCE_tiTransitoryDeac_C_data;
  IdtVehicleSpeedThreshold UCE_spdThdDegDeac_C_data;
  IdtVehicleSpeedDegThreshold UCE_spdThdNomDeac_C_data;
  IdtMainWkuValidTime UCE_tiMainWkuAcv_C_data;
  IdtMainWkuDevalidTime UCE_tiNomMainWkuDeac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX10Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX10Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX11Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX11Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX12Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX12Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX13Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX13Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX14Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX14Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX15Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX15Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX16Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX16Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX1Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX1Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX2Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX2Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX3Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX3Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX4Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX4Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX5Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX5Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX6Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX6Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX7Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX7Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX8Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX8Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX9Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX9Deac_C_data;
  boolean UCE_bInhPtlWkuX10_C_data;
  boolean UCE_bInhPtlWkuX11_C_data;
  boolean UCE_bInhPtlWkuX12_C_data;
  boolean UCE_bInhPtlWkuX13_C_data;
  boolean UCE_bInhPtlWkuX14_C_data;
  boolean UCE_bInhPtlWkuX15_C_data;
  boolean UCE_bInhPtlWkuX16_C_data;
  boolean UCE_bInhPtlWkuX1_C_data;
  boolean UCE_bInhPtlWkuX2_C_data;
  boolean UCE_bInhPtlWkuX3_C_data;
  boolean UCE_bInhPtlWkuX4_C_data;
  boolean UCE_bInhPtlWkuX5_C_data;
  boolean UCE_bInhPtlWkuX6_C_data;
  boolean UCE_bInhPtlWkuX7_C_data;
  boolean UCE_bInhPtlWkuX8_C_data;
  boolean UCE_bInhPtlWkuX9_C_data;
  boolean UCE_bInhPtlWkuY1_C_data;
  boolean UCE_bInhPtlWkuY2_C_data;
  boolean UCE_bInhPtlWkuY3_C_data;
  boolean UCE_bInhPtlWkuY4_C_data;
  boolean UCE_bInhPtlWkuY5_C_data;
  boolean UCE_bInhPtlWkuY6_C_data;
  boolean UCE_bInhPtlWkuY7_C_data;
  boolean UCE_bInhPtlWkuY8_C_data;
  boolean UCE_bSlavePtlWkuX10AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX11AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX12AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX13AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX14AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX15AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX16AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX1AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX2AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX3AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX4AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX5AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX6AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX7AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX8AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX9AcvMod_C_data;
  boolean UCE_noUCETyp_C_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  UCE_tiDegMainWkuDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiDegMainWkuDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainDisrdDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainDisrdDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainIncstDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainIncstDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainTransForc_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainTransForc_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainWkuReh_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainWkuReh_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiComLatch_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiComLatch_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiIntPtlWku_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiIntPtlWku_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiShutDownPrep_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiShutDownPrep_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiShutDownPrep_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiShutDownPrep_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineCmdAcv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineCmdAcv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineScgDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineScgReh_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgReh_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiTransitoryDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiTransitoryDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_spdThdDegDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_spdThdDegDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_spdThdNomDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_spdThdNomDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainWkuAcv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainWkuAcv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiNomMainWkuDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiNomMainWkuDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX10_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX10_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX11_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX11_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX12_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX12_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX13_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX13_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX14_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX14_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX15_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX15_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX16_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX16_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX1_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX2_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX3_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX4_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX5_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX6_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX7_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX8_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX9_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX9_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX10AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX11AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX12AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX13AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX14AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX15AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX16AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX1AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX2AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX3AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX4AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX5AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX6AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX7AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX8AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX9AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_noUCETyp_C_data = TSC_CtApAEM_Rte_CData_UCE_noUCETyp_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApAEM_Rte_Write_PpAppRCDECUState_DeAppRCDECUState(Rte_InitValue_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApAEM_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(boolean *data)
 *   Std_ReturnType Rte_Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(boolean *data)
 *   Std_ReturnType Rte_Read_PpDiagToolsRequest_DeDiagToolsRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(boolean *data)
 *   Std_ReturnType Rte_Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data)
 *   Std_ReturnType Rte_Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpCANComRequest_DeCANComRequest(sint32 data)
 *   Std_ReturnType Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(sint32 data)
 *   Std_ReturnType Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(boolean data)
 *   Std_ReturnType Rte_Write_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(boolean data)
 *   Std_ReturnType Rte_Write_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(boolean data)
 *   Std_ReturnType Rte_Write_PpFaultRCDLineSC_DeFaultRCDLineSC(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data)
 *   Std_ReturnType Rte_Write_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(boolean data)
 *   Std_ReturnType Rte_Write_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(boolean data)
 *   Std_ReturnType Rte_Write_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(boolean data)
 *   Std_ReturnType Rte_Write_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpShutdownAuthorization_DeShutdownAuthorization(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApAEM_task10msB_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApAEM_CODE) RCtApAEM_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApAEM_task10msB
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC;
  boolean Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP;
  boolean Read_PpDiagToolsRequest_DeDiagToolsRequest;
  boolean Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest;
  boolean Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP;
  boolean Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  BSI_MainWakeup Read_PpInt_BSI_MainWakeup_BSI_MainWakeup;
  BSI_PostDriveWakeup Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup;
  BSI_PreDriveWakeup Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup;
  SUPV_RCDLineState Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState;
  VCU_PrecondElecWakeup Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup;
  VCU_StopDelayedHMIWakeup Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup;
  boolean Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP;

  IdtDegMainWkuExtinctionTime UCE_tiDegMainWkuDeac_C_data;
  IdtTimeNomMainWkuDisord UCE_tiMainDisrdDet_C_data;
  IdtTimeNomMainWkuIncst UCE_tiMainIncstDet_C_data;
  IdtEnforcedMainWkuTime UCE_tiMainTransForc_C_data;
  IdtTimeNomMainWkuRehabilit UCE_tiMainWkuReh_C_data;
  IdtCOMLatchMaxDuration UCE_tiMaxTiComLatch_C_data;
  IdtInternalPartialWkuMaxDuration UCE_tiMaxTiIntPtlWku_C_data;
  IdtMasterPartialWkuY1MaxDuration UCE_tiMaxTiMstPtlWkuY1_C_data;
  IdtMasterPartialWkuY2MaxDuration UCE_tiMaxTiMstPtlWkuY2_C_data;
  IdtMasterPartialWkuY3MaxDuration UCE_tiMaxTiMstPtlWkuY3_C_data;
  IdtMasterPartialWkuY4MaxDuration UCE_tiMaxTiMstPtlWkuY4_C_data;
  IdtMasterPartialWkuY5MaxDuration UCE_tiMaxTiMstPtlWkuY5_C_data;
  IdtMasterPartialWkuY6MaxDuration UCE_tiMaxTiMstPtlWkuY6_C_data;
  IdtMasterPartialWkuY7MaxDuration UCE_tiMaxTiMstPtlWkuY7_C_data;
  IdtMasterPartialWkuY8MaxDuration UCE_tiMaxTiMstPtlWkuY8_C_data;
  IdtShutdownPreparationMaxTime UCE_tiMaxTiShutDownPrep_C_data;
  IdtMasterPartialWkuY1MinDuration UCE_tiMinTiMstPtlWkuY1_C_data;
  IdtMasterPartialWkuY2MinDuration UCE_tiMinTiMstPtlWkuY2_C_data;
  IdtMasterPartialWkuY3MinDuration UCE_tiMinTiMstPtlWkuY3_C_data;
  IdtMasterPartialWkuY4MinDuration UCE_tiMinTiMstPtlWkuY4_C_data;
  IdtMasterPartialWkuY5MinDuration UCE_tiMinTiMstPtlWkuY5_C_data;
  IdtMasterPartialWkuY6MinDuration UCE_tiMinTiMstPtlWkuY6_C_data;
  IdtMasterPartialWkuY7MinDuration UCE_tiMinTiMstPtlWkuY7_C_data;
  IdtMasterPartialWkuY8MinDuration UCE_tiMinTiMstPtlWkuY8_C_data;
  IdtShutdownPreparationMinTime UCE_tiMinTiShutDownPrep_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX10Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX11Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX12Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX13Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX14Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX15Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX16Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX1Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX2Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX3Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX4Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX5Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX6Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX7Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX8Lock_C_data;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX9Lock_C_data;
  IdtTimeRCDPulse UCE_tiRCDLineCmdAcv_C_data;
  IdtRCDLineGndSCConfirmTime UCE_tiRCDLineScgDet_C_data;
  IdtRCDLineGndSCRehabilitTime UCE_tiRCDLineScgReh_C_data;
  IdtExtinctionTime UCE_tiTransitoryDeac_C_data;
  IdtVehicleSpeedThreshold UCE_spdThdDegDeac_C_data;
  IdtVehicleSpeedDegThreshold UCE_spdThdNomDeac_C_data;
  IdtMainWkuValidTime UCE_tiMainWkuAcv_C_data;
  IdtMainWkuDevalidTime UCE_tiNomMainWkuDeac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX10Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX10Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX11Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX11Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX12Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX12Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX13Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX13Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX14Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX14Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX15Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX15Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX16Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX16Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX1Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX1Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX2Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX2Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX3Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX3Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX4Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX4Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX5Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX5Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX6Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX6Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX7Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX7Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX8Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX8Deac_C_data;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX9Acv_C_data;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX9Deac_C_data;
  boolean UCE_bInhPtlWkuX10_C_data;
  boolean UCE_bInhPtlWkuX11_C_data;
  boolean UCE_bInhPtlWkuX12_C_data;
  boolean UCE_bInhPtlWkuX13_C_data;
  boolean UCE_bInhPtlWkuX14_C_data;
  boolean UCE_bInhPtlWkuX15_C_data;
  boolean UCE_bInhPtlWkuX16_C_data;
  boolean UCE_bInhPtlWkuX1_C_data;
  boolean UCE_bInhPtlWkuX2_C_data;
  boolean UCE_bInhPtlWkuX3_C_data;
  boolean UCE_bInhPtlWkuX4_C_data;
  boolean UCE_bInhPtlWkuX5_C_data;
  boolean UCE_bInhPtlWkuX6_C_data;
  boolean UCE_bInhPtlWkuX7_C_data;
  boolean UCE_bInhPtlWkuX8_C_data;
  boolean UCE_bInhPtlWkuX9_C_data;
  boolean UCE_bInhPtlWkuY1_C_data;
  boolean UCE_bInhPtlWkuY2_C_data;
  boolean UCE_bInhPtlWkuY3_C_data;
  boolean UCE_bInhPtlWkuY4_C_data;
  boolean UCE_bInhPtlWkuY5_C_data;
  boolean UCE_bInhPtlWkuY6_C_data;
  boolean UCE_bInhPtlWkuY7_C_data;
  boolean UCE_bInhPtlWkuY8_C_data;
  boolean UCE_bSlavePtlWkuX10AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX11AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX12AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX13AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX14AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX15AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX16AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX1AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX2AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX3AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX4AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX5AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX6AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX7AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX8AcvMod_C_data;
  boolean UCE_bSlavePtlWkuX9AcvMod_C_data;
  boolean UCE_noUCETyp_C_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  UCE_tiDegMainWkuDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiDegMainWkuDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainDisrdDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainDisrdDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainIncstDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainIncstDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainTransForc_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainTransForc_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainWkuReh_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainWkuReh_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiComLatch_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiComLatch_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiIntPtlWku_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiIntPtlWku_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiMstPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMaxTiShutDownPrep_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMaxTiShutDownPrep_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiMstPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMinTiShutDownPrep_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMinTiShutDownPrep_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Lock_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Lock_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineCmdAcv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineCmdAcv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineScgDet_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgDet_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiRCDLineScgReh_C_data = TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgReh_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiTransitoryDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiTransitoryDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_spdThdDegDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_spdThdDegDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_spdThdNomDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_spdThdNomDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiMainWkuAcv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiMainWkuAcv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiNomMainWkuDeac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiNomMainWkuDeac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX10Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX11Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX12Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX13Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX14Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX15Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX16Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX1Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX2Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX3Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX4Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX5Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX6Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX7Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX8Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Acv_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Acv_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_tiPtlWkuX9Deac_C_data = TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Deac_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX10_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX10_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX11_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX11_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX12_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX12_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX13_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX13_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX14_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX14_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX15_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX15_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX16_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX16_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX1_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX2_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX3_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX4_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX5_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX6_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX7_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX8_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuX9_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX9_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY1_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY1_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY2_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY2_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY3_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY3_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY4_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY4_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY5_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY5_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY6_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY6_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY7_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY7_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bInhPtlWkuY8_C_data = TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY8_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX10AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX11AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX12AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX13AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX14AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX15AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX16AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX1AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX2AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX3AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX4AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX5AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX6AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX7AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX8AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_bSlavePtlWkuX9AcvMod_C_data = TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  UCE_noUCETyp_C_data = TSC_CtApAEM_Rte_CData_UCE_noUCETyp_C(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApAEM_Rte_Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(&Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(&Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpDiagToolsRequest_DeDiagToolsRequest(&Read_PpDiagToolsRequest_DeDiagToolsRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(&Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(&Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(&Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&Read_PpInt_BSI_MainWakeup_BSI_MainWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(&Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(&Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(&Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(&Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(&Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpCANComRequest_DeCANComRequest(Rte_InitValue_PpCANComRequest_DeCANComRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(Rte_InitValue_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(Rte_InitValue_PpECU_WakeupMain_DeECU_WakeupMain); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(Rte_InitValue_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(Rte_InitValue_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpFaultRCDLineSC_DeFaultRCDLineSC(Rte_InitValue_PpFaultRCDLineSC_DeFaultRCDLineSC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(Rte_InitValue_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(Rte_InitValue_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(Rte_InitValue_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(Rte_InitValue_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(Rte_InitValue_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(Rte_InitValue_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(Rte_InitValue_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(Rte_InitValue_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(Rte_InitValue_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(Rte_InitValue_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(Rte_InitValue_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(Rte_InitValue_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(Rte_InitValue_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApAEM_Rte_Write_PpShutdownAuthorization_DeShutdownAuthorization(Rte_InitValue_PpShutdownAuthorization_DeShutdownAuthorization); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApAEM_STOP_SEC_CODE
#include "CtApAEM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApAEM_TestDefines(void)
{
  /* Enumeration Data Types */

  BSI_MainWakeup Test_BSI_MainWakeup_V_1 = Cx0_Invalid_RCD;
  BSI_MainWakeup Test_BSI_MainWakeup_V_2 = Cx1_No_main_wake_up_request;
  BSI_MainWakeup Test_BSI_MainWakeup_V_3 = Cx2_Main_wake_up_request;
  BSI_MainWakeup Test_BSI_MainWakeup_V_4 = Cx3_Not_valid;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_1 = Cx0_Partial_wakeup;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_2 = Cx1_Internal_wakeup;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_3 = Cx2_Transitory;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_4 = Cx3_Nominal_main_wakeup;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_5 = Cx4_Degraded_main_wakeup;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_6 = Cx5_Shutdown_preparation;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_7 = Cx6_Reserved;
  SUPV_ECUElecStateRCD Test_SUPV_ECUElecStateRCD_V_8 = Cx7_Reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
