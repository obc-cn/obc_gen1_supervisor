/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Os_Cfg.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Os definitions
 *********************************************************************************************************************/

#ifndef _OS_CFG_H_
# define _OS_CFG_H_

/* Os definitions */

/* Tasks */
# define ASILB_MAIN_TASK (0U)
# define ASILB_init_task (1U)
# define Default_BSW_Async_QM_Task (2U)
# define Default_BSW_Async_Task (3U)
# define QM_MAIN_TASK (4U)
# define QM_init_task (5U)

/* Counters */
# define SystemTimer (0U)

/* Alarms */
# define Rte_Al_TE_ASILB_MAIN_TASK_1_100ms (0U)
# define Rte_Al_TE_ASILB_MAIN_TASK_1_10ms (1U)
# define Rte_Al_TE_ASILB_MAIN_TASK_2_10ms (2U)
# define Rte_Al_TE_ASILB_MAIN_TASK_3_10ms (3U)
# define Rte_Al_TE_ASILB_MAIN_TASK_4_10ms (4U)
# define Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX (5U)
# define Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX (6U)
# define Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms (7U)
# define Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms (8U)
# define Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms (9U)
# define Rte_Al_TE2_Default_BSW_Async_Task_7_10ms (10U)
# define Rte_Al_TE_CpApAEM_RCtApAEM_task10msA (11U)
# define Rte_Al_TE_QM_MAIN_TASK_2_100ms (12U)
# define Rte_Al_TE_QM_MAIN_TASK_2_10ms (13U)
# define Rte_Al_TE_QM_MAIN_TASK_3_100ms (14U)
# define Rte_Al_TE_QM_MAIN_TASK_3_10ms (15U)

/* Events */
# define Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_1_5ms (0x04)
# define Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_7_10ms (0x01)
# define Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_8_20ms (0x02)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_100ms (0x02)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_10ms (0x01)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_2_10ms (0x20)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_3_10ms (0x04)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_4_10ms (0x10)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_2_100ms (0x04)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_2_10ms (0x02)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_3_100ms (0x10)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_3_10ms (0x01)
# define Rte_Ev_Run_CpApAEM_RCtApAEM_task10msA (0x08)
# define Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msRX (0x08)
# define Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msTX (0x40)

/* Spinlocks */

/* Resources */

/* ScheduleTables */

/* Cores */
# include "usrostyp.h"

/* Trusted Functions */


#endif /* _OS_CFG_H_ */
