/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApFCT.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApFCT.h"
#include "TSC_CtApFCT.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApFCT_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
{
  return Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
{
  return Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw *data)
{
  return Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(data);
}




Std_ReturnType TSC_CtApFCT_Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data)
{
  return Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(IdtOutputTempAftsales data)
{
  return Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(IdtOutputTempAftsales data)
{
  return Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(IdtOutputTempAftsales data)
{
  return Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(IdtOutputTempAftsales data)
{
  return Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(IdtOutputTempAftsales data)
{
  return Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(IdtOutputTempAftsales data)
{
  return Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas data)
{
  return Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas data)
{
  return Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas data)
{
  return Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas data)
{
  return Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas data)
{
  return Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas data)
{
  return Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC1FaultSCG(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempAC1FaultSCG(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC1FaultSCP(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempAC1FaultSCP(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC2FaultSCG(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempAC2FaultSCG(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC2FaultSCP(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempAC2FaultSCP(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC3FaultSCG(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempAC3FaultSCG(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempAC3FaultSCP(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempAC3FaultSCP(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempACNFaultSCG(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempACNFaultSCG(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempACNFaultSCP(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempACNFaultSCP(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC1FaultSCG(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempDC1FaultSCG(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC1FaultSCP(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempDC1FaultSCP(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC2FaultSCG(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempDC2FaultSCG(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempFaults_DeTempDC2FaultSCP(boolean data)
{
  return Rte_Write_PpTempFaults_DeTempDC2FaultSCP(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(boolean data)
{
  return Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(boolean data)
{
  return Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(boolean data)
{
  return Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(boolean data)
{
  return Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(boolean data)
{
  return Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(data);
}

Std_ReturnType TSC_CtApFCT_Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(boolean data)
{
  return Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApFCT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC1(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCG_AC1();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC2(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCG_AC2();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_AC3(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCG_AC3();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_ACN(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCG_ACN();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC1(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCG_DC1();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCG_DC2(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCG_DC2();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC1(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCP_AC1();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC2(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCP_AC2();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_AC3(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCP_AC3();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_ACN(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCP_ACN();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC1(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCP_DC1();
}
IdtThresholdTempFault  TSC_CtApFCT_Rte_CData_CalThresholdSCP_DC2(void)
{
  return (IdtThresholdTempFault ) Rte_CData_CalThresholdSCP_DC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCG_AC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCG_AC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_AC3(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCG_AC3();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_ACN(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCG_ACN();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCG_DC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCG_DC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCG_DC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCP_AC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCP_AC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_AC3(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCP_AC3();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_ACN(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCP_ACN();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCP_DC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeConfSCP_DC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeConfSCP_DC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCG_AC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCG_AC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_AC3(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCG_AC3();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_ACN(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCG_ACN();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCG_DC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCG_DC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCG_DC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCP_AC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCP_AC2();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_AC3(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCP_AC3();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_ACN(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCP_ACN();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC1(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCP_DC1();
}
IdtTimeDiagTemp  TSC_CtApFCT_Rte_CData_CalTimeHealSCP_DC2(void)
{
  return (IdtTimeDiagTemp ) Rte_CData_CalTimeHealSCP_DC2();
}
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_AC1(void)
{
  return (IdtRampGradient ) Rte_CData_CalRampGradient_AC1();
}
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_AC2(void)
{
  return (IdtRampGradient ) Rte_CData_CalRampGradient_AC2();
}
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_AC3(void)
{
  return (IdtRampGradient ) Rte_CData_CalRampGradient_AC3();
}
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_ACN(void)
{
  return (IdtRampGradient ) Rte_CData_CalRampGradient_ACN();
}
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_DC1(void)
{
  return (IdtRampGradient ) Rte_CData_CalRampGradient_DC1();
}
IdtRampGradient  TSC_CtApFCT_Rte_CData_CalRampGradient_DC2(void)
{
  return (IdtRampGradient ) Rte_CData_CalRampGradient_DC2();
}
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueAC1_C(void)
{
  return (IdtSubstituteValueTemp ) Rte_CData_CalSubstituteValueAC1_C();
}
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueAC2_C(void)
{
  return (IdtSubstituteValueTemp ) Rte_CData_CalSubstituteValueAC2_C();
}
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueAC3_C(void)
{
  return (IdtSubstituteValueTemp ) Rte_CData_CalSubstituteValueAC3_C();
}
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueACN_C(void)
{
  return (IdtSubstituteValueTemp ) Rte_CData_CalSubstituteValueACN_C();
}
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueDC1_C(void)
{
  return (IdtSubstituteValueTemp ) Rte_CData_CalSubstituteValueDC1_C();
}
IdtSubstituteValueTemp  TSC_CtApFCT_Rte_CData_CalSubstituteValueDC2_C(void)
{
  return (IdtSubstituteValueTemp ) Rte_CData_CalSubstituteValueDC2_C();
}
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowAC1Plug(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowAC1Plug();
}
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowAC2Plug(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowAC2Plug();
}
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowAC3Plug(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowAC3Plug();
}
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowACNPlug(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowACNPlug();
}
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowDC1Plug(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowDC1Plug();
}
boolean  TSC_CtApFCT_Rte_CData_CalCtrlFlowDC2Plug(void)
{
  return (boolean ) Rte_CData_CalCtrlFlowDC2Plug();
}
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC1(void)
{
  return (IdtLinTableGlobalTemp *) Rte_CData_CalLinTableGlobalTemp_AC1();
}
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC2(void)
{
  return (IdtLinTableGlobalTemp *) Rte_CData_CalLinTableGlobalTemp_AC2();
}
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_AC3(void)
{
  return (IdtLinTableGlobalTemp *) Rte_CData_CalLinTableGlobalTemp_AC3();
}
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_ACN(void)
{
  return (IdtLinTableGlobalTemp *) Rte_CData_CalLinTableGlobalTemp_ACN();
}
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC1(void)
{
  return (IdtLinTableGlobalTemp *) Rte_CData_CalLinTableGlobalTemp_DC1();
}
IdtLinTableGlobalTemp * TSC_CtApFCT_Rte_CData_CalLinTableGlobalTemp_DC2(void)
{
  return (IdtLinTableGlobalTemp *) Rte_CData_CalLinTableGlobalTemp_DC2();
}
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC1(void)
{
  return (IdtLinTableGlobalVolt *) Rte_CData_CalLinTableGlobalVoltage_AC1();
}
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC2(void)
{
  return (IdtLinTableGlobalVolt *) Rte_CData_CalLinTableGlobalVoltage_AC2();
}
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_AC3(void)
{
  return (IdtLinTableGlobalVolt *) Rte_CData_CalLinTableGlobalVoltage_AC3();
}
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_ACN(void)
{
  return (IdtLinTableGlobalVolt *) Rte_CData_CalLinTableGlobalVoltage_ACN();
}
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC1(void)
{
  return (IdtLinTableGlobalVolt *) Rte_CData_CalLinTableGlobalVoltage_DC1();
}
IdtLinTableGlobalVolt * TSC_CtApFCT_Rte_CData_CalLinTableGlobalVoltage_DC2(void)
{
  return (IdtLinTableGlobalVolt *) Rte_CData_CalLinTableGlobalVoltage_DC2();
}

     /* CtApFCT */
      /* CtApFCT */

/** Per Instance Memories */
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCG(void)
{
  return Rte_Pim_PimTempAC1CounterSCG();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC1CounterSCP(void)
{
  return Rte_Pim_PimTempAC1CounterSCP();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCG(void)
{
  return Rte_Pim_PimTempAC2CounterSCG();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC2CounterSCP(void)
{
  return Rte_Pim_PimTempAC2CounterSCP();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCG(void)
{
  return Rte_Pim_PimTempAC3CounterSCG();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempAC3CounterSCP(void)
{
  return Rte_Pim_PimTempAC3CounterSCP();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCG(void)
{
  return Rte_Pim_PimTempACNCounterSCG();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempACNCounterSCP(void)
{
  return Rte_Pim_PimTempACNCounterSCP();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCG(void)
{
  return Rte_Pim_PimTempDC1CounterSCG();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC1CounterSCP(void)
{
  return Rte_Pim_PimTempDC1CounterSCP();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCG(void)
{
  return Rte_Pim_PimTempDC2CounterSCG();
}
uint16 *TSC_CtApFCT_Rte_Pim_PimTempDC2CounterSCP(void)
{
  return Rte_Pim_PimTempDC2CounterSCP();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCG(void)
{
  return Rte_Pim_PimTempAC1PrefaultSCG();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC1PrefaultSCP(void)
{
  return Rte_Pim_PimTempAC1PrefaultSCP();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCG(void)
{
  return Rte_Pim_PimTempAC2PrefaultSCG();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC2PrefaultSCP(void)
{
  return Rte_Pim_PimTempAC2PrefaultSCP();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCG(void)
{
  return Rte_Pim_PimTempAC3PrefaultSCG();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempAC3PrefaultSCP(void)
{
  return Rte_Pim_PimTempAC3PrefaultSCP();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCG(void)
{
  return Rte_Pim_PimTempACNPrefaultSCG();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempACNPrefaultSCP(void)
{
  return Rte_Pim_PimTempACNPrefaultSCP();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCG(void)
{
  return Rte_Pim_PimTempDC1PrefaultSCG();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC1PrefaultSCP(void)
{
  return Rte_Pim_PimTempDC1PrefaultSCP();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCG(void)
{
  return Rte_Pim_PimTempDC2PrefaultSCG();
}
boolean *TSC_CtApFCT_Rte_Pim_PimTempDC2PrefaultSCP(void)
{
  return Rte_Pim_PimTempDC2PrefaultSCP();
}



