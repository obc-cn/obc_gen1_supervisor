/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApOFM.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApOFM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApOFM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApOFM.h"
#include "TSC_CtApOFM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApOFM_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * DCHV_IOM_ERR_CAP_FAIL_H: Boolean
 * DCHV_IOM_ERR_CAP_FAIL_L: Boolean
 * DCHV_IOM_ERR_OC_IOUT: Boolean
 * DCHV_IOM_ERR_OC_NEG: Boolean
 * DCHV_IOM_ERR_OC_POS: Boolean
 * DCHV_IOM_ERR_OT: Boolean
 * DCHV_IOM_ERR_OT_IN: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD5: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD6: Boolean
 * DCHV_IOM_ERR_OV_VOUT: Boolean
 * DCHV_IOM_ERR_UV_12V: Boolean
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtDebounceOvercurrentDCHV: Integer in interval [0...10]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtOBCFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryOvertemp: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryVoltageError: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_MaxNumberRetries: Integer in interval [0...60]
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_PlugVoltDetection: Boolean
 * PFC_IOM_ERR_OC_PH1: Boolean
 * PFC_IOM_ERR_OC_PH2: Boolean
 * PFC_IOM_ERR_OC_PH3: Boolean
 * PFC_IOM_ERR_OC_SHUNT1: Boolean
 * PFC_IOM_ERR_OC_SHUNT2: Boolean
 * PFC_IOM_ERR_OC_SHUNT3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M1: Boolean
 * PFC_IOM_ERR_OT_NTC1_M3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M4: Boolean
 * PFC_IOM_ERR_OV_DCLINK: Boolean
 * PFC_IOM_ERR_OV_VPH12: Boolean
 * PFC_IOM_ERR_OV_VPH23: Boolean
 * PFC_IOM_ERR_OV_VPH31: Boolean
 * PFC_IOM_ERR_UVLO_ISO4: Boolean
 * PFC_IOM_ERR_UVLO_ISO7: Boolean
 * PFC_IOM_ERR_UV_12V: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 *   FAULT_LEVEL_NO_FAULT (0U)
 *   FAULT_LEVEL_1 (1U)
 *   FAULT_LEVEL_2 (2U)
 *   FAULT_LEVEL_3 (3U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 * Array Types:
 * ============
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtFaultLevel *Rte_Pim_PimOBC_AC1PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_AC2PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_AC3PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ACNPlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ActiveDischarge_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_BatteryHVUndervoltage_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ControlPilot_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DC1PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DC2PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVHWFault_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVOvervoltage_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DiagnosticNoACInput_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ElockLocked_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_FaultSatellites_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_OBCInternalCom_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_PFCHWFault_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ProximityLine_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_VCUModeEPSRequest_Error(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtOBC_MaxNumberRetries Rte_CData_CalOBC_MaxNumberRetries(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtOBCFaultTimeToRetryDefault Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(void)
 *   IdtOBCFaultTimeToRetryOvertemp Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(void)
 *   IdtOBCFaultTimeToRetryVoltageError Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(void)
 *   IdtDebounceOvercurrentDCHV Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void)
 *
 *********************************************************************************************************************/


#define CtApOFM_START_SEC_CODE
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_OFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApOFM_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  return RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_OFM_HW_faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_OFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOFM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RCtApOFM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_init
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOFM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean *data)
 *   Std_ReturnType Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(boolean *data)
 *   Std_ReturnType Rte_Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_Fault_OBC_Fault(OBC_Fault data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeInlet_OvertempACSensorFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_HWErrors_Fault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_InternalComFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_OvertemperatureFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RCtApOFM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpActiveDischargeRequest_DeActiveDischargeRequest;
  IdtPOST_Result Read_PpDCDC_POST_Result_DeDCDC_POST_Result;
  boolean Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput;
  boolean Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue;
  DCDC_CurrentReference Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference;
  DCHV_ADC_IOUT Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
  DCHV_ADC_VOUT Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
  DCHV_Command_Current_Reference Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
  DCHV_DCHVStatus Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
  DCHV_IOM_ERR_CAP_FAIL_H Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H;
  DCHV_IOM_ERR_CAP_FAIL_L Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L;
  DCHV_IOM_ERR_OC_IOUT Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT;
  DCHV_IOM_ERR_OC_NEG Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG;
  DCHV_IOM_ERR_OC_POS Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS;
  DCHV_IOM_ERR_OT Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT;
  DCHV_IOM_ERR_OT_IN Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN;
  DCHV_IOM_ERR_OT_NTC_MOD5 Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5;
  DCHV_IOM_ERR_OT_NTC_MOD6 Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6;
  DCHV_IOM_ERR_OV_VOUT Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT;
  DCHV_IOM_ERR_UV_12V Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V;
  DCLV_Input_Voltage Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  OBC_HighVoltConnectionAllowed Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed;
  OBC_PlugVoltDetection Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
  OBC_Status Read_PpInt_OBC_Status_Delayed_OBC_Status;
  PFC_IOM_ERR_OC_PH1 Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1;
  PFC_IOM_ERR_OC_PH2 Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2;
  PFC_IOM_ERR_OC_PH3 Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3;
  PFC_IOM_ERR_OC_SHUNT1 Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1;
  PFC_IOM_ERR_OC_SHUNT2 Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2;
  PFC_IOM_ERR_OC_SHUNT3 Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3;
  PFC_IOM_ERR_OT_NTC1_M1 Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1;
  PFC_IOM_ERR_OT_NTC1_M3 Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3;
  PFC_IOM_ERR_OT_NTC1_M4 Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4;
  PFC_IOM_ERR_OV_DCLINK Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK;
  PFC_IOM_ERR_OV_VPH12 Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12;
  PFC_IOM_ERR_OV_VPH23 Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23;
  PFC_IOM_ERR_OV_VPH31 Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31;
  PFC_IOM_ERR_UVLO_ISO4 Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4;
  PFC_IOM_ERR_UVLO_ISO7 Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7;
  PFC_IOM_ERR_UV_12V Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V;
  VCU_ModeEPSRequest Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
  boolean Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error;
  IdtFaultLevel Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error;
  IdtFaultLevel Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error;
  IdtFaultLevel Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error;
  IdtFaultLevel Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error;
  IdtFaultLevel Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error;
  IdtFaultLevel Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error;
  IdtFaultLevel Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error;
  IdtFaultLevel Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error;
  IdtPOST_Result Read_PpOBC_POST_Result_DeOBC_POST_Result;
  IdtOutputELockSensor Read_PpOutputELockSensor_DeOutputELockSensor;
  boolean Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC1Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC2Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC3Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempACNMeas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempDC1Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempDC2Meas;

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApOFM_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(&Read_PpActiveDischargeRequest_DeActiveDischargeRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&Read_PpDCDC_POST_Result_DeDCDC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(&Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(&Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(&Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(&Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(&Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(&Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(&Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(&Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(&Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(&Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(&Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(&Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(&Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(&Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(&Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(&Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(&Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&Read_PpInt_OBC_Status_Delayed_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(&Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(&Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(&Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(&Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(&Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(&Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(&Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(&Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(&Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(&Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(&Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(&Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(&Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(&Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(&Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(&Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(&Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(&Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(&Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(&Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(&Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(&Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(&Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(&Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(&Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&Read_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&Read_PpOutputELockSensor_DeOutputELockSensor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(&Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&Read_PpOutputTempMeas_DeOutputTempAC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(&Read_PpOutputTempMeas_DeOutputTempAC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(&Read_PpOutputTempMeas_DeOutputTempAC3Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(&Read_PpOutputTempMeas_DeOutputTempACNMeas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(&Read_PpOutputTempMeas_DeOutputTempDC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(&Read_PpOutputTempMeas_DeOutputTempDC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpFaultChargeSoftStop_DeFaultChargeSoftStop(Rte_InitValue_PpFaultChargeSoftStop_DeFaultChargeSoftStop); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpInt_OBC_Fault_OBC_Fault(Rte_InitValue_PpInt_OBC_Fault_OBC_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeInlet_OvertempACSensorFault(Rte_InitValue_PpOBCFaultsList_DeInlet_OvertempACSensorFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(Rte_InitValue_PpOBCFaultsList_DeInlet_OvertempDCSensorFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_HWErrors_Fault(Rte_InitValue_PpOBCFaultsList_DeOBC_HWErrors_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_InternalComFault(Rte_InitValue_PpOBCFaultsList_DeOBC_InternalComFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(Rte_InitValue_PpOBCFaultsList_DeOBC_OvercurrentOutputFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_OvertemperatureFault(Rte_InitValue_PpOBCFaultsList_DeOBC_OvertemperatureFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Write_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(Rte_InitValue_PpOBCFaultsList_DeOBC_OvervoltageOutputFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApOFM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_OFM_Faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_OFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_OFM_Faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_OFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpOFMGetDCHVHWFaults> of PortPrototype <PpOFMGetDCHVHWFaults>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpOFMGetPFCHWFaults> of PortPrototype <PpOFMGetPFCHWFaults>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults
 *********************************************************************************************************************/

  IdtFaultLevel PimPimOBC_AC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_AC3PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ACNPlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_ActiveDischarge_Error;
  IdtFaultLevel PimPimOBC_BatteryHVUndervoltage_Error;
  IdtFaultLevel PimPimOBC_ControlPilot_Error;
  IdtFaultLevel PimPimOBC_DC1PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DC2PlugOvertemp_Error;
  IdtFaultLevel PimPimOBC_DCHVHWFault_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputOvercurrent_Error;
  IdtFaultLevel PimPimOBC_DCHVOutputShortCircuit_Error;
  IdtFaultLevel PimPimOBC_DCHVOvervoltage_Error;
  IdtFaultLevel PimPimOBC_DiagnosticNoACInput_Error;
  IdtFaultLevel PimPimOBC_ElockLocked_Error;
  IdtFaultLevel PimPimOBC_FaultSatellites_Error;
  IdtFaultLevel PimPimOBC_OBCInternalCom_Error;
  IdtFaultLevel PimPimOBC_PFCHWFault_Error;
  IdtFaultLevel PimPimOBC_ProximityLine_Error;
  IdtFaultLevel PimPimOBC_VCUModeEPSRequest_Error;

  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries_data;

  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_AC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC1PlugOvertemp_Error() = PimPimOBC_AC1PlugOvertemp_Error;
  PimPimOBC_AC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC2PlugOvertemp_Error() = PimPimOBC_AC2PlugOvertemp_Error;
  PimPimOBC_AC3PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_AC3PlugOvertemp_Error() = PimPimOBC_AC3PlugOvertemp_Error;
  PimPimOBC_ACNPlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ACNPlugOvertemp_Error() = PimPimOBC_ACNPlugOvertemp_Error;
  PimPimOBC_ActiveDischarge_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ActiveDischarge_Error() = PimPimOBC_ActiveDischarge_Error;
  PimPimOBC_BatteryHVUndervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() = PimPimOBC_BatteryHVUndervoltage_Error;
  PimPimOBC_ControlPilot_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ControlPilot_Error() = PimPimOBC_ControlPilot_Error;
  PimPimOBC_DC1PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC1PlugOvertemp_Error() = PimPimOBC_DC1PlugOvertemp_Error;
  PimPimOBC_DC2PlugOvertemp_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DC2PlugOvertemp_Error() = PimPimOBC_DC2PlugOvertemp_Error;
  PimPimOBC_DCHVHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVHWFault_Error() = PimPimOBC_DCHVHWFault_Error;
  PimPimOBC_DCHVOutputOvercurrent_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() = PimPimOBC_DCHVOutputOvercurrent_Error;
  PimPimOBC_DCHVOutputShortCircuit_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() = PimPimOBC_DCHVOutputShortCircuit_Error;
  PimPimOBC_DCHVOvervoltage_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DCHVOvervoltage_Error() = PimPimOBC_DCHVOvervoltage_Error;
  PimPimOBC_DiagnosticNoACInput_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_DiagnosticNoACInput_Error() = PimPimOBC_DiagnosticNoACInput_Error;
  PimPimOBC_ElockLocked_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ElockLocked_Error() = PimPimOBC_ElockLocked_Error;
  PimPimOBC_FaultSatellites_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_FaultSatellites_Error() = PimPimOBC_FaultSatellites_Error;
  PimPimOBC_OBCInternalCom_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_OBCInternalCom_Error() = PimPimOBC_OBCInternalCom_Error;
  PimPimOBC_PFCHWFault_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_PFCHWFault_Error() = PimPimOBC_PFCHWFault_Error;
  PimPimOBC_ProximityLine_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_ProximityLine_Error() = PimPimOBC_ProximityLine_Error;
  PimPimOBC_VCUModeEPSRequest_Error = *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error();
  *TSC_CtApOFM_Rte_Pim_PimOBC_VCUModeEPSRequest_Error() = PimPimOBC_VCUModeEPSRequest_Error;

  CalOBC_MaxNumberRetries_data = TSC_CtApOFM_Rte_CData_CalOBC_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApOFM_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApOFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApOFM_STOP_SEC_CODE
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApOFM_TestDefines(void)
{
  /* Enumeration Data Types */

  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_1 = Cx0_DCHV_STATUS_STANDBY;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_2 = Cx1_DCHV_STATUS_RUN;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_3 = Cx2_DCHV_STATUS_ERROR;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_4 = Cx3_DCHV_STATUS_ALARM;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_5 = Cx4_DCHV_STATUS_SAFE;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtFaultLevel Test_IdtFaultLevel_V_1 = FAULT_LEVEL_NO_FAULT;
  IdtFaultLevel Test_IdtFaultLevel_V_2 = FAULT_LEVEL_1;
  IdtFaultLevel Test_IdtFaultLevel_V_3 = FAULT_LEVEL_2;
  IdtFaultLevel Test_IdtFaultLevel_V_4 = FAULT_LEVEL_3;

  IdtOutputELockSensor Test_IdtOutputELockSensor_V_1 = ELOCK_LOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_2 = ELOCK_UNLOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_3 = ELOCK_DRIVE_UNDEFINED;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  OBC_ChargingMode Test_OBC_ChargingMode_V_1 = Cx0_no_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_2 = Cx1_slow_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_3 = Cx2_China_fast_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_4 = Cx3_Euro_fast_charging;

  OBC_Fault Test_OBC_Fault_V_1 = OBC_FAULT_NO_FAULT;
  OBC_Fault Test_OBC_Fault_V_2 = OBC_FAULT_LEVEL_1;
  OBC_Fault Test_OBC_Fault_V_3 = OBC_FAULT_LEVEL_2;
  OBC_Fault Test_OBC_Fault_V_4 = OBC_FAULT_LEVEL_3;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;

  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_1 = Cx0_OFF;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_2 = Cx1_Active_Drive;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_3 = Cx2_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_4 = Cx3_PI_Charge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_5 = Cx4_PI_Balance;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_6 = Cx5_PI_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_7 = Cx6_Reserved;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_8 = Cx7_Reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
