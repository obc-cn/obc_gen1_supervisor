/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApILT.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApILT
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApILT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApILT.h"
#include "TSC_CtApILT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApILT_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDebounceStateFailure: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateFinished: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateInProgress: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateStopped: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtElockTimeError: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtMaxTimeChargeError: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeChargeInProgress: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeEndOfCharge: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeGuideManagement: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeProgrammingCharge: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtRECHARGE_HMI_STATE: Integer in interval [0...255]
 * IdtTimePushButtonKeepValue: Integer in interval [0...600]
 *   Unit: [ms], Factor: 100, Offset: 0
 * OBC_PushChargeType: Boolean
 * VCU_ElecMeterSaturation: Boolean
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_ChargeState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_ON (0U)
 *   Cx1_OFF_not_ended (1U)
 *   Cx2_OFF_ended (2U)
 *   Cx3_Unavailable (3U)
 * BSI_ChargeTypeStatus: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_DIFFEREE (1U)
 *   Cx2_IMMEDIATE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxTimeChargeError Rte_CData_CalMaxTimeChargeError(void)
 *   IdtMaxTimeChargeInProgress Rte_CData_CalMaxTimeChargeInProgress(void)
 *   IdtMaxTimeEndOfCharge Rte_CData_CalMaxTimeEndOfCharge(void)
 *   IdtMaxTimeGuideManagement Rte_CData_CalMaxTimeGuideManagement(void)
 *   IdtMaxTimeProgrammingCharge Rte_CData_CalMaxTimeProgrammingCharge(void)
 *   IdtTimePushButtonKeepValue Rte_CData_CalTimePushButtonKeepValue(void)
 *   IdtDebounceStateFailure Rte_CData_CalDebounceStateFailure(void)
 *   IdtDebounceStateFinished Rte_CData_CalDebounceStateFinished(void)
 *   IdtDebounceStateInProgress Rte_CData_CalDebounceStateInProgress(void)
 *   IdtDebounceStateStopped Rte_CData_CalDebounceStateStopped(void)
 *   IdtElockTimeError Rte_CData_CalElockTimeError(void)
 *
 *********************************************************************************************************************/


#define CtApILT_START_SEC_CODE
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_init
 *********************************************************************************************************************/

  IdtMaxTimeChargeError CalMaxTimeChargeError_data;
  IdtMaxTimeChargeInProgress CalMaxTimeChargeInProgress_data;
  IdtMaxTimeEndOfCharge CalMaxTimeEndOfCharge_data;
  IdtMaxTimeGuideManagement CalMaxTimeGuideManagement_data;
  IdtMaxTimeProgrammingCharge CalMaxTimeProgrammingCharge_data;
  IdtTimePushButtonKeepValue CalTimePushButtonKeepValue_data;
  IdtDebounceStateFailure CalDebounceStateFailure_data;
  IdtDebounceStateFinished CalDebounceStateFinished_data;
  IdtDebounceStateInProgress CalDebounceStateInProgress_data;
  IdtDebounceStateStopped CalDebounceStateStopped_data;
  IdtElockTimeError CalElockTimeError_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxTimeChargeError_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeChargeInProgress_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeEndOfCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeGuideManagement_data = TSC_CtApILT_Rte_CData_CalMaxTimeGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeProgrammingCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimePushButtonKeepValue_data = TSC_CtApILT_Rte_CData_CalTimePushButtonKeepValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFailure_data = TSC_CtApILT_Rte_CData_CalDebounceStateFailure(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFinished_data = TSC_CtApILT_Rte_CData_CalDebounceStateFinished(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateInProgress_data = TSC_CtApILT_Rte_CData_CalDebounceStateInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateStopped_data = TSC_CtApILT_Rte_CData_CalDebounceStateStopped(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockTimeError_data = TSC_CtApILT_Rte_CData_CalElockTimeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApILT_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_task100msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_task100msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msA
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpOutputChargePushFil_DeOutputChargePushFil;

  IdtMaxTimeChargeError CalMaxTimeChargeError_data;
  IdtMaxTimeChargeInProgress CalMaxTimeChargeInProgress_data;
  IdtMaxTimeEndOfCharge CalMaxTimeEndOfCharge_data;
  IdtMaxTimeGuideManagement CalMaxTimeGuideManagement_data;
  IdtMaxTimeProgrammingCharge CalMaxTimeProgrammingCharge_data;
  IdtTimePushButtonKeepValue CalTimePushButtonKeepValue_data;
  IdtDebounceStateFailure CalDebounceStateFailure_data;
  IdtDebounceStateFinished CalDebounceStateFinished_data;
  IdtDebounceStateInProgress CalDebounceStateInProgress_data;
  IdtDebounceStateStopped CalDebounceStateStopped_data;
  IdtElockTimeError CalElockTimeError_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxTimeChargeError_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeChargeInProgress_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeEndOfCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeGuideManagement_data = TSC_CtApILT_Rte_CData_CalMaxTimeGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeProgrammingCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimePushButtonKeepValue_data = TSC_CtApILT_Rte_CData_CalTimePushButtonKeepValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFailure_data = TSC_CtApILT_Rte_CData_CalDebounceStateFailure(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFinished_data = TSC_CtApILT_Rte_CData_CalDebounceStateFinished(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateInProgress_data = TSC_CtApILT_Rte_CData_CalDebounceStateInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateStopped_data = TSC_CtApILT_Rte_CData_CalDebounceStateStopped(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockTimeError_data = TSC_CtApILT_Rte_CData_CalElockTimeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApILT_Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(&Read_PpOutputChargePushFil_DeOutputChargePushFil); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType(Rte_InitValue_PpInt_OBC_PushChargeType_OBC_PushChargeType); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_task100msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeChargeError(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeGuideManagement(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(boolean *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *   Std_ReturnType Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeChargeError(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeChargeInProgress(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeEndOfCharge(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeGuideManagement(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeProgrammingCharge(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msB_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_task100msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msB
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  BSI_ChargeState Read_PpInt_BSI_ChargeState_BSI_ChargeState;
  BSI_ChargeTypeStatus Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  OBC_Fault Read_PpInt_OBC_Fault_OBC_Fault;
  VCU_ElecMeterSaturation Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation;
  VCU_ModeEPSRequest Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
  boolean Read_PpLedModes_PlantMode_DeChargeError;
  boolean Read_PpLedModes_PlantMode_DeChargeInProgress;
  boolean Read_PpLedModes_PlantMode_DeEndOfCharge;
  boolean Read_PpLedModes_PlantMode_DeGuideManagement;
  boolean Read_PpLedModes_PlantMode_DeProgrammingCharge;
  boolean Read_PpPlantModeState_DePlantModeState;
  IdtRECHARGE_HMI_STATE Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode;

  IdtMaxTimeChargeError CalMaxTimeChargeError_data;
  IdtMaxTimeChargeInProgress CalMaxTimeChargeInProgress_data;
  IdtMaxTimeEndOfCharge CalMaxTimeEndOfCharge_data;
  IdtMaxTimeGuideManagement CalMaxTimeGuideManagement_data;
  IdtMaxTimeProgrammingCharge CalMaxTimeProgrammingCharge_data;
  IdtTimePushButtonKeepValue CalTimePushButtonKeepValue_data;
  IdtDebounceStateFailure CalDebounceStateFailure_data;
  IdtDebounceStateFinished CalDebounceStateFinished_data;
  IdtDebounceStateInProgress CalDebounceStateInProgress_data;
  IdtDebounceStateStopped CalDebounceStateStopped_data;
  IdtElockTimeError CalElockTimeError_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxTimeChargeError_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeChargeInProgress_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeEndOfCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeGuideManagement_data = TSC_CtApILT_Rte_CData_CalMaxTimeGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeProgrammingCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimePushButtonKeepValue_data = TSC_CtApILT_Rte_CData_CalTimePushButtonKeepValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFailure_data = TSC_CtApILT_Rte_CData_CalDebounceStateFailure(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFinished_data = TSC_CtApILT_Rte_CData_CalDebounceStateFinished(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateInProgress_data = TSC_CtApILT_Rte_CData_CalDebounceStateInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateStopped_data = TSC_CtApILT_Rte_CData_CalDebounceStateStopped(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockTimeError_data = TSC_CtApILT_Rte_CData_CalElockTimeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApILT_Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(&Read_PpInt_BSI_ChargeState_BSI_ChargeState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(&Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpInt_OBC_Fault_OBC_Fault(&Read_PpInt_OBC_Fault_OBC_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(&Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeChargeError(&Read_PpLedModes_PlantMode_DeChargeError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(&Read_PpLedModes_PlantMode_DeChargeInProgress); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(&Read_PpLedModes_PlantMode_DeEndOfCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeGuideManagement(&Read_PpLedModes_PlantMode_DeGuideManagement); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(&Read_PpLedModes_PlantMode_DeProgrammingCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpPlantModeState_DePlantModeState(&Read_PpPlantModeState_DePlantModeState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(&Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(Rte_InitValue_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLedModes_DeChargeError(Rte_InitValue_PpLedModes_DeChargeError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLedModes_DeChargeInProgress(Rte_InitValue_PpLedModes_DeChargeInProgress); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLedModes_DeEndOfCharge(Rte_InitValue_PpLedModes_DeEndOfCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLedModes_DeGuideManagement(Rte_InitValue_PpLedModes_DeGuideManagement); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLedModes_DeProgrammingCharge(Rte_InitValue_PpLedModes_DeProgrammingCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(boolean data)
 *   Std_ReturnType Rte_Write_PpLockStateError_DeLockStateError(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode;
  IdtOutputELockSensor Read_PpOutputELockSensor_DeOutputELockSensor;
  boolean Read_PpPlantModeState_DePlantModeState;

  IdtMaxTimeChargeError CalMaxTimeChargeError_data;
  IdtMaxTimeChargeInProgress CalMaxTimeChargeInProgress_data;
  IdtMaxTimeEndOfCharge CalMaxTimeEndOfCharge_data;
  IdtMaxTimeGuideManagement CalMaxTimeGuideManagement_data;
  IdtMaxTimeProgrammingCharge CalMaxTimeProgrammingCharge_data;
  IdtTimePushButtonKeepValue CalTimePushButtonKeepValue_data;
  IdtDebounceStateFailure CalDebounceStateFailure_data;
  IdtDebounceStateFinished CalDebounceStateFinished_data;
  IdtDebounceStateInProgress CalDebounceStateInProgress_data;
  IdtDebounceStateStopped CalDebounceStateStopped_data;
  IdtElockTimeError CalElockTimeError_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxTimeChargeError_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeChargeInProgress_data = TSC_CtApILT_Rte_CData_CalMaxTimeChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeEndOfCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeGuideManagement_data = TSC_CtApILT_Rte_CData_CalMaxTimeGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeProgrammingCharge_data = TSC_CtApILT_Rte_CData_CalMaxTimeProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimePushButtonKeepValue_data = TSC_CtApILT_Rte_CData_CalTimePushButtonKeepValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFailure_data = TSC_CtApILT_Rte_CData_CalDebounceStateFailure(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateFinished_data = TSC_CtApILT_Rte_CData_CalDebounceStateFinished(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateInProgress_data = TSC_CtApILT_Rte_CData_CalDebounceStateInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceStateStopped_data = TSC_CtApILT_Rte_CData_CalDebounceStateStopped(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockTimeError_data = TSC_CtApILT_Rte_CData_CalElockTimeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApILT_Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(&Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&Read_PpOutputELockSensor_DeOutputELockSensor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Read_PpPlantModeState_DePlantModeState(&Read_PpPlantModeState_DePlantModeState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(Rte_InitValue_PpLockLED2BEPR_DeLockLED2BEPR); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Write_PpLockStateError_DeLockStateError(Rte_InitValue_PpLockStateError_DeLockStateError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApILT_STOP_SEC_CODE
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApILT_TestDefines(void)
{
  /* Enumeration Data Types */

  BSI_ChargeState Test_BSI_ChargeState_V_1 = Cx0_ON;
  BSI_ChargeState Test_BSI_ChargeState_V_2 = Cx1_OFF_not_ended;
  BSI_ChargeState Test_BSI_ChargeState_V_3 = Cx2_OFF_ended;
  BSI_ChargeState Test_BSI_ChargeState_V_4 = Cx3_Unavailable;

  BSI_ChargeTypeStatus Test_BSI_ChargeTypeStatus_V_1 = Cx0_STANDBY;
  BSI_ChargeTypeStatus Test_BSI_ChargeTypeStatus_V_2 = Cx1_DIFFEREE;
  BSI_ChargeTypeStatus Test_BSI_ChargeTypeStatus_V_3 = Cx2_IMMEDIATE;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtOutputELockSensor Test_IdtOutputELockSensor_V_1 = ELOCK_LOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_2 = ELOCK_UNLOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_3 = ELOCK_DRIVE_UNDEFINED;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  OBC_Fault Test_OBC_Fault_V_1 = OBC_FAULT_NO_FAULT;
  OBC_Fault Test_OBC_Fault_V_2 = OBC_FAULT_LEVEL_1;
  OBC_Fault Test_OBC_Fault_V_3 = OBC_FAULT_LEVEL_2;
  OBC_Fault Test_OBC_Fault_V_4 = OBC_FAULT_LEVEL_3;

  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_1 = Cx0_Disconnected;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_2 = Cx1_In_progress;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_3 = Cx2_Failure;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_4 = Cx3_Stopped;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_5 = Cx4_Finished;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_6 = Cx5_Reserved;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_7 = Cx6_Reserved;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_8 = Cx7_Reserved;

  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_1 = Cx0_OFF;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_2 = Cx1_Active_Drive;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_3 = Cx2_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_4 = Cx3_PI_Charge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_5 = Cx4_PI_Balance;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_6 = Cx5_PI_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_7 = Cx6_Reserved;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_8 = Cx7_Reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
