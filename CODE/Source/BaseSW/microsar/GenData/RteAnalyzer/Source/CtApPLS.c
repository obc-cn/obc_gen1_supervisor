/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPLS.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApPLS
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPLS>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * WdgM_CheckpointIdType
 *   
 *
 * WdgM_SupervisedEntityIdType
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * CheckpointReached of Port Interface WdgM_AliveSupervision
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApPLS.h"
#include "TSC_CtApPLS.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApPLS_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_Voltage: Integer in interval [0...500]
 * DCDC_InputVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCDC_OutputCurrent: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCLV_Input_Current: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Measured_Voltage: Integer in interval [0...175]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * IdtACTempTripThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtACTempTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtACTempWarningThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtACTempWarningTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtDCLVFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryShortCircuit: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCTempTripThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtDCTempTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtDCTempWarningThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtDCTempWarningTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtHVVTripThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtHVVTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtHVVWarningThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtHVVWarningTime: Integer in interval [1...2000]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVPTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVVTripThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtLVVTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVVWarningThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtLVVWarningTime: Integer in interval [1...2000]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtMaxEfficiency: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtMinEfficiency: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtOBCFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryOvertemp: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryVoltageError: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCMaxEfficiency: Integer in interval [0...150]
 *   Factor: 0.01, Offset: 0
 * IdtOBCMinEfficiency: Integer in interval [0...150]
 *   Factor: 0.01, Offset: 0
 * IdtOBCOffsetAllowed: Integer in interval [0...10000]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBCPTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtOBC_DelayEfficiencyDCLV: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_DelayEfficiencyHVMono: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_DelayEfficiencyHVTri: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOffsetAllowed: Integer in interval [0...1000]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtVoltageCorrectionOffset: Integer in interval [-32768...32767]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * OBC_OutputCurrent: Integer in interval [0...511]
 * OBC_OutputVoltage: Integer in interval [0...8191]
 * PFC_IPH1_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH2_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH3_RMS_0A1: Integer in interval [0...1023]
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * WdgM_SupervisedEntityIdType: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 *   FAULT_LEVEL_NO_FAULT (0U)
 *   FAULT_LEVEL_1 (1U)
 *   FAULT_LEVEL_2 (2U)
 *   FAULT_LEVEL_3 (3U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtACTempTripTime Rte_CData_CalACTempTripTime(void)
 *   IdtACTempWarningTime Rte_CData_CalACTempWarningTime(void)
 *   IdtDCTempTripTime Rte_CData_CalDCTempTripTime(void)
 *   IdtDCTempWarningTime Rte_CData_CalDCTempWarningTime(void)
 *   IdtHVVTripTime Rte_CData_CalHVVTripTime(void)
 *   IdtHVVWarningTime Rte_CData_CalHVVWarningTime(void)
 *   IdtLVPTripTime Rte_CData_CalLVPTripTime(void)
 *   IdtLVVTripTime Rte_CData_CalLVVTripTime(void)
 *   IdtLVVWarningTime Rte_CData_CalLVVWarningTime(void)
 *   IdtOBCMaxEfficiency Rte_CData_CalOBCMaxEfficiency(void)
 *   IdtOBCOffsetAllowed Rte_CData_CalOBCOffsetAllowed(void)
 *   IdtOBCPTripTime Rte_CData_CalOBCPTripTime(void)
 *   IdtOffsetAllowed Rte_CData_CalOffsetAllowed(void)
 *   IdtACTempTripThreshold Rte_CData_CalACTempTripThreshold(void)
 *   IdtACTempWarningThreshold Rte_CData_CalACTempWarningThreshold(void)
 *   IdtDCTempTripThreshold Rte_CData_CalDCTempTripThreshold(void)
 *   IdtDCTempWarningThreshold Rte_CData_CalDCTempWarningThreshold(void)
 *   IdtHVVTripThreshold Rte_CData_CalHVVTripThreshold(void)
 *   IdtHVVWarningThreshold Rte_CData_CalHVVWarningThreshold(void)
 *   IdtLVVTripThreshold Rte_CData_CalLVVTripThreshold(void)
 *   IdtLVVWarningThreshold Rte_CData_CalLVVWarningThreshold(void)
 *   IdtMaxEfficiency Rte_CData_CalMaxEfficiency(void)
 *   IdtMinEfficiency Rte_CData_CalMinEfficiency(void)
 *   IdtOBCMinEfficiency Rte_CData_CalOBCMinEfficiency(void)
 *   IdtOBC_DelayEfficiencyDCLV Rte_CData_CalOBC_DelayEfficiencyDCLV(void)
 *   IdtOBC_DelayEfficiencyHVMono Rte_CData_CalOBC_DelayEfficiencyHVMono(void)
 *   IdtOBC_DelayEfficiencyHVTri Rte_CData_CalOBC_DelayEfficiencyHVTri(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtDCLVFaultTimeToRetryDefault Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void)
 *   IdtDCLVFaultTimeToRetryShortCircuit Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void)
 *   IdtOBCFaultTimeToRetryDefault Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(void)
 *   IdtOBCFaultTimeToRetryOvertemp Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(void)
 *   IdtOBCFaultTimeToRetryVoltageError Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(void)
 *
 *********************************************************************************************************************/


#define CtApPLS_START_SEC_CODE
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLS_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLS_CODE) RCtApPLS_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_init
 *********************************************************************************************************************/

  IdtACTempTripTime CalACTempTripTime_data;
  IdtACTempWarningTime CalACTempWarningTime_data;
  IdtDCTempTripTime CalDCTempTripTime_data;
  IdtDCTempWarningTime CalDCTempWarningTime_data;
  IdtHVVTripTime CalHVVTripTime_data;
  IdtHVVWarningTime CalHVVWarningTime_data;
  IdtLVPTripTime CalLVPTripTime_data;
  IdtLVVTripTime CalLVVTripTime_data;
  IdtLVVWarningTime CalLVVWarningTime_data;
  IdtOBCMaxEfficiency CalOBCMaxEfficiency_data;
  IdtOBCOffsetAllowed CalOBCOffsetAllowed_data;
  IdtOBCPTripTime CalOBCPTripTime_data;
  IdtOffsetAllowed CalOffsetAllowed_data;
  IdtACTempTripThreshold CalACTempTripThreshold_data;
  IdtACTempWarningThreshold CalACTempWarningThreshold_data;
  IdtDCTempTripThreshold CalDCTempTripThreshold_data;
  IdtDCTempWarningThreshold CalDCTempWarningThreshold_data;
  IdtHVVTripThreshold CalHVVTripThreshold_data;
  IdtHVVWarningThreshold CalHVVWarningThreshold_data;
  IdtLVVTripThreshold CalLVVTripThreshold_data;
  IdtLVVWarningThreshold CalLVVWarningThreshold_data;
  IdtMaxEfficiency CalMaxEfficiency_data;
  IdtMinEfficiency CalMinEfficiency_data;
  IdtOBCMinEfficiency CalOBCMinEfficiency_data;
  IdtOBC_DelayEfficiencyDCLV CalOBC_DelayEfficiencyDCLV_data;
  IdtOBC_DelayEfficiencyHVMono CalOBC_DelayEfficiencyHVMono_data;
  IdtOBC_DelayEfficiencyHVTri CalOBC_DelayEfficiencyHVTri_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalACTempTripTime_data = TSC_CtApPLS_Rte_CData_CalACTempTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalACTempWarningTime_data = TSC_CtApPLS_Rte_CData_CalACTempWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempTripTime_data = TSC_CtApPLS_Rte_CData_CalDCTempTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempWarningTime_data = TSC_CtApPLS_Rte_CData_CalDCTempWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVTripTime_data = TSC_CtApPLS_Rte_CData_CalHVVTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVWarningTime_data = TSC_CtApPLS_Rte_CData_CalHVVWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVPTripTime_data = TSC_CtApPLS_Rte_CData_CalLVPTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVTripTime_data = TSC_CtApPLS_Rte_CData_CalLVVTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVWarningTime_data = TSC_CtApPLS_Rte_CData_CalLVVWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCMaxEfficiency_data = TSC_CtApPLS_Rte_CData_CalOBCMaxEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCOffsetAllowed_data = TSC_CtApPLS_Rte_CData_CalOBCOffsetAllowed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCPTripTime_data = TSC_CtApPLS_Rte_CData_CalOBCPTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOffsetAllowed_data = TSC_CtApPLS_Rte_CData_CalOffsetAllowed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalACTempTripThreshold_data = TSC_CtApPLS_Rte_CData_CalACTempTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalACTempWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalACTempWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempTripThreshold_data = TSC_CtApPLS_Rte_CData_CalDCTempTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalDCTempWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVTripThreshold_data = TSC_CtApPLS_Rte_CData_CalHVVTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalHVVWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVTripThreshold_data = TSC_CtApPLS_Rte_CData_CalLVVTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalLVVWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxEfficiency_data = TSC_CtApPLS_Rte_CData_CalMaxEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinEfficiency_data = TSC_CtApPLS_Rte_CData_CalMinEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCMinEfficiency_data = TSC_CtApPLS_Rte_CData_CalOBCMinEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_DelayEfficiencyDCLV_data = TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyDCLV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_DelayEfficiencyHVMono_data = TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyHVMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_DelayEfficiencyHVTri_data = TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyHVTri(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApPLS_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApPLS_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApPLS_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLS_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCDC_OutputCurrent data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data)
 *   Std_ReturnType Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(IdtVoltageCorrectionOffset data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLS_CODE) RCtApPLS_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtPOST_Result Read_PpDCDC_POST_Result_DeDCDC_POST_Result;
  IdtInputVoltageMode Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed;
  BMS_Voltage Read_PpInt_BMS_Voltage_BMS_Voltage;
  DCDC_Status Read_PpInt_DCDC_Status_Delayed_DCDC_Status;
  DCHV_ADC_IOUT Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
  DCHV_ADC_VOUT Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
  DCHV_DCHVStatus Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
  DCLV_DCLVStatus Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
  DCLV_Input_Current Read_PpInt_DCLV_Input_Current_DCLV_Input_Current;
  DCLV_Input_Voltage Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
  DCLV_Measured_Current Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
  DCLV_Measured_Voltage Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage;
  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  OBC_Status Read_PpInt_OBC_Status_Delayed_OBC_Status;
  PFC_IPH1_RMS_0A1 Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1;
  PFC_VPH12_RMS_V Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
  IdtPOST_Result Read_PpOBC_POST_Result_DeOBC_POST_Result;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC1Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC2Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC3Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempACNMeas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempDC1Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempDC2Meas;
  boolean Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled;

  IdtACTempTripTime CalACTempTripTime_data;
  IdtACTempWarningTime CalACTempWarningTime_data;
  IdtDCTempTripTime CalDCTempTripTime_data;
  IdtDCTempWarningTime CalDCTempWarningTime_data;
  IdtHVVTripTime CalHVVTripTime_data;
  IdtHVVWarningTime CalHVVWarningTime_data;
  IdtLVPTripTime CalLVPTripTime_data;
  IdtLVVTripTime CalLVVTripTime_data;
  IdtLVVWarningTime CalLVVWarningTime_data;
  IdtOBCMaxEfficiency CalOBCMaxEfficiency_data;
  IdtOBCOffsetAllowed CalOBCOffsetAllowed_data;
  IdtOBCPTripTime CalOBCPTripTime_data;
  IdtOffsetAllowed CalOffsetAllowed_data;
  IdtACTempTripThreshold CalACTempTripThreshold_data;
  IdtACTempWarningThreshold CalACTempWarningThreshold_data;
  IdtDCTempTripThreshold CalDCTempTripThreshold_data;
  IdtDCTempWarningThreshold CalDCTempWarningThreshold_data;
  IdtHVVTripThreshold CalHVVTripThreshold_data;
  IdtHVVWarningThreshold CalHVVWarningThreshold_data;
  IdtLVVTripThreshold CalLVVTripThreshold_data;
  IdtLVVWarningThreshold CalLVVWarningThreshold_data;
  IdtMaxEfficiency CalMaxEfficiency_data;
  IdtMinEfficiency CalMinEfficiency_data;
  IdtOBCMinEfficiency CalOBCMinEfficiency_data;
  IdtOBC_DelayEfficiencyDCLV CalOBC_DelayEfficiencyDCLV_data;
  IdtOBC_DelayEfficiencyHVMono CalOBC_DelayEfficiencyHVMono_data;
  IdtOBC_DelayEfficiencyHVTri CalOBC_DelayEfficiencyHVTri_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalACTempTripTime_data = TSC_CtApPLS_Rte_CData_CalACTempTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalACTempWarningTime_data = TSC_CtApPLS_Rte_CData_CalACTempWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempTripTime_data = TSC_CtApPLS_Rte_CData_CalDCTempTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempWarningTime_data = TSC_CtApPLS_Rte_CData_CalDCTempWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVTripTime_data = TSC_CtApPLS_Rte_CData_CalHVVTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVWarningTime_data = TSC_CtApPLS_Rte_CData_CalHVVWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVPTripTime_data = TSC_CtApPLS_Rte_CData_CalLVPTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVTripTime_data = TSC_CtApPLS_Rte_CData_CalLVVTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVWarningTime_data = TSC_CtApPLS_Rte_CData_CalLVVWarningTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCMaxEfficiency_data = TSC_CtApPLS_Rte_CData_CalOBCMaxEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCOffsetAllowed_data = TSC_CtApPLS_Rte_CData_CalOBCOffsetAllowed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCPTripTime_data = TSC_CtApPLS_Rte_CData_CalOBCPTripTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOffsetAllowed_data = TSC_CtApPLS_Rte_CData_CalOffsetAllowed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalACTempTripThreshold_data = TSC_CtApPLS_Rte_CData_CalACTempTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalACTempWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalACTempWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempTripThreshold_data = TSC_CtApPLS_Rte_CData_CalDCTempTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCTempWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalDCTempWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVTripThreshold_data = TSC_CtApPLS_Rte_CData_CalHVVTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalHVVWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalHVVWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVTripThreshold_data = TSC_CtApPLS_Rte_CData_CalLVVTripThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalLVVWarningThreshold_data = TSC_CtApPLS_Rte_CData_CalLVVWarningThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxEfficiency_data = TSC_CtApPLS_Rte_CData_CalMaxEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinEfficiency_data = TSC_CtApPLS_Rte_CData_CalMinEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCMinEfficiency_data = TSC_CtApPLS_Rte_CData_CalOBCMinEfficiency(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_DelayEfficiencyDCLV_data = TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyDCLV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_DelayEfficiencyHVMono_data = TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyHVMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_DelayEfficiencyHVTri_data = TSC_CtApPLS_Rte_CData_CalOBC_DelayEfficiencyHVTri(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApPLS_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApPLS_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault_data = TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp_data = TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError_data = TSC_CtApPLS_Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApPLS_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&Read_PpDCDC_POST_Result_DeDCDC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(&Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(&Read_PpInt_BMS_Voltage_BMS_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&Read_PpInt_DCDC_Status_Delayed_DCDC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(&Read_PpInt_DCLV_Input_Current_DCLV_Input_Current); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(&Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(&Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&Read_PpInt_OBC_Status_Delayed_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(&Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&Read_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&Read_PpOutputTempMeas_DeOutputTempAC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(&Read_PpOutputTempMeas_DeOutputTempAC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(&Read_PpOutputTempMeas_DeOutputTempAC3Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(&Read_PpOutputTempMeas_DeOutputTempACNMeas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(&Read_PpOutputTempMeas_DeOutputTempDC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(&Read_PpOutputTempMeas_DeOutputTempDC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(Rte_InitValue_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(Rte_InitValue_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(Rte_InitValue_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(Rte_InitValue_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(Rte_InitValue_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(Rte_InitValue_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(Rte_InitValue_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(Rte_InitValue_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(Rte_InitValue_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(Rte_InitValue_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(Rte_InitValue_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(Rte_InitValue_PpInt_DCDC_InputVoltage_DCDC_InputVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(Rte_InitValue_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(Rte_InitValue_PpInt_OBC_OutputCurrent_OBC_OutputCurrent); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(Rte_InitValue_PpInt_OBC_OutputVoltage_OBC_OutputVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(Rte_InitValue_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(Rte_InitValue_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(Rte_InitValue_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(Rte_InitValue_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(Rte_InitValue_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(Rte_InitValue_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(Rte_InitValue_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(Rte_InitValue_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(Rte_InitValue_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(Rte_InitValue_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(Rte_InitValue_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(Rte_InitValue_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(Rte_InitValue_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApPLS_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_WdgM_AliveSupervision_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPLS_STOP_SEC_CODE
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApPLS_TestDefines(void)
{
  /* Enumeration Data Types */

  DCDC_Status Test_DCDC_Status_V_1 = Cx0_off_mode;
  DCDC_Status Test_DCDC_Status_V_2 = Cx1_Init_mode;
  DCDC_Status Test_DCDC_Status_V_3 = Cx2_standby_mode;
  DCDC_Status Test_DCDC_Status_V_4 = Cx3_conversion_working_;
  DCDC_Status Test_DCDC_Status_V_5 = Cx4_error_mode;
  DCDC_Status Test_DCDC_Status_V_6 = Cx5_degradation_mode;
  DCDC_Status Test_DCDC_Status_V_7 = Cx6_reserved;
  DCDC_Status Test_DCDC_Status_V_8 = Cx7_invalid;

  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_1 = Cx0_DCHV_STATUS_STANDBY;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_2 = Cx1_DCHV_STATUS_RUN;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_3 = Cx2_DCHV_STATUS_ERROR;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_4 = Cx3_DCHV_STATUS_ALARM;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_5 = Cx4_DCHV_STATUS_SAFE;

  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_1 = Cx0_STANDBY;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_2 = Cx1_RUN;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_3 = Cx2_ERROR;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_4 = Cx3_ALARM;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_5 = Cx4_SAFE;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_6 = Cx5_DERATED;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_7 = Cx6_SHUTDOWN;

  IdtFaultLevel Test_IdtFaultLevel_V_1 = FAULT_LEVEL_NO_FAULT;
  IdtFaultLevel Test_IdtFaultLevel_V_2 = FAULT_LEVEL_1;
  IdtFaultLevel Test_IdtFaultLevel_V_3 = FAULT_LEVEL_2;
  IdtFaultLevel Test_IdtFaultLevel_V_4 = FAULT_LEVEL_3;

  IdtInputVoltageMode Test_IdtInputVoltageMode_V_1 = IVM_NO_INPUT_DETECTED;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_2 = IVM_MONOPHASIC_INPUT;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_3 = IVM_TRIPHASIC_INPUT;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  OBC_ChargingMode Test_OBC_ChargingMode_V_1 = Cx0_no_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_2 = Cx1_slow_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_3 = Cx2_China_fast_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_4 = Cx3_Euro_fast_charging;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
