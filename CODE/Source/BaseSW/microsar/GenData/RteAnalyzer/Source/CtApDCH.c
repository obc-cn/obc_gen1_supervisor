/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApDCH.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApDCH
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApDCH>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApDCH.h"
#include "TSC_CtApDCH.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApDCH_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtCalThresholdUVTripDischarge: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtCalTimeUVTripDischarge: Integer in interval [0...200]
 *   Unit: [ms], Factor: 100, Offset: 0
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * VCU_ActivedischargeCommand: Boolean
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCDC_ActivedischargeSt: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_not_in_active_discharge (0U)
 *   Cx1_active_discharging (1U)
 *   Cx2_discharge_completed (2U)
 *   Cx3_discharge_failure (3U)
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * PFC_PFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 *   Cx6_PFC_STATE_START_TRI (6U)
 *   Cx7_PFC_STATE_START_MONO (7U)
 *   Cx8_PFC_STATE_ERROR (8U)
 *   Cx9_PFC_STATE_DIRECTPWM (9U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtCalThresholdUVTripDischarge Rte_CData_CalThresholdUVTripDischarge(void)
 *   IdtCalTimeUVTripDischarge Rte_CData_CalTimeUVTripDischarge(void)
 *
 *********************************************************************************************************************/


#define CtApDCH_START_SEC_CODE
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDCH_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDCH_CODE) RCtApDCH_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_init
 *********************************************************************************************************************/

  IdtCalThresholdUVTripDischarge CalThresholdUVTripDischarge_data;
  IdtCalTimeUVTripDischarge CalTimeUVTripDischarge_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalThresholdUVTripDischarge_data = TSC_CtApDCH_Rte_CData_CalThresholdUVTripDischarge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeUVTripDischarge_data = TSC_CtApDCH_Rte_CData_CalTimeUVTripDischarge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApDCH_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDCH_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(DCDC_ActivedischargeSt data)
 *   Std_ReturnType Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDCH_CODE) RCtApDCH_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  boolean Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC;
  DCDC_Status Read_PpInt_DCDC_Status_Delayed_DCDC_Status;
  DCHV_ADC_VOUT Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
  DCLV_Input_Voltage Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
  OBC_ACRange Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange;
  OBC_Status Read_PpInt_OBC_Status_Delayed_OBC_Status;
  PFC_PFCStatus Read_PpInt_PFC_PFCStatus_PFC_PFCStatus;
  PFC_Vdclink_V Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
  VCU_ActivedischargeCommand Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand;
  IdtPOST_Result Read_PpOBC_POST_Result_DeOBC_POST_Result;

  IdtCalThresholdUVTripDischarge CalThresholdUVTripDischarge_data;
  IdtCalTimeUVTripDischarge CalTimeUVTripDischarge_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalThresholdUVTripDischarge_data = TSC_CtApDCH_Rte_CData_CalThresholdUVTripDischarge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeUVTripDischarge_data = TSC_CtApDCH_Rte_CData_CalTimeUVTripDischarge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApDCH_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(&Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&Read_PpInt_DCDC_Status_Delayed_DCDC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange(&Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&Read_PpInt_OBC_Status_Delayed_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(&Read_PpInt_PFC_PFCStatus_PFC_PFCStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(&Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(&Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&Read_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest(Rte_InitValue_PpActiveDischargeRequest_DeActiveDischargeRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(Rte_InitValue_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(Rte_InitValue_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDCH_Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(Rte_InitValue_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApDCH_STOP_SEC_CODE
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApDCH_TestDefines(void)
{
  /* Enumeration Data Types */

  DCDC_ActivedischargeSt Test_DCDC_ActivedischargeSt_V_1 = Cx0_not_in_active_discharge;
  DCDC_ActivedischargeSt Test_DCDC_ActivedischargeSt_V_2 = Cx1_active_discharging;
  DCDC_ActivedischargeSt Test_DCDC_ActivedischargeSt_V_3 = Cx2_discharge_completed;
  DCDC_ActivedischargeSt Test_DCDC_ActivedischargeSt_V_4 = Cx3_discharge_failure;

  DCDC_Status Test_DCDC_Status_V_1 = Cx0_off_mode;
  DCDC_Status Test_DCDC_Status_V_2 = Cx1_Init_mode;
  DCDC_Status Test_DCDC_Status_V_3 = Cx2_standby_mode;
  DCDC_Status Test_DCDC_Status_V_4 = Cx3_conversion_working_;
  DCDC_Status Test_DCDC_Status_V_5 = Cx4_error_mode;
  DCDC_Status Test_DCDC_Status_V_6 = Cx5_degradation_mode;
  DCDC_Status Test_DCDC_Status_V_7 = Cx6_reserved;
  DCDC_Status Test_DCDC_Status_V_8 = Cx7_invalid;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  OBC_ACRange Test_OBC_ACRange_V_1 = Cx0_No_AC_10V_;
  OBC_ACRange Test_OBC_ACRange_V_2 = Cx1_110V_85_132V_;
  OBC_ACRange Test_OBC_ACRange_V_3 = Cx2_Invalid;
  OBC_ACRange Test_OBC_ACRange_V_4 = Cx3_220V_170_265V_;
  OBC_ACRange Test_OBC_ACRange_V_5 = Cx4_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_6 = Cx5_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_7 = Cx6_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_8 = Cx7_Reserved;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;

  PFC_PFCStatus Test_PFC_PFCStatus_V_1 = Cx0_PFC_STATE_STANDBY;
  PFC_PFCStatus Test_PFC_PFCStatus_V_2 = Cx1_PFC_STATE_TRI;
  PFC_PFCStatus Test_PFC_PFCStatus_V_3 = Cx2_PFC_STATE_MONO;
  PFC_PFCStatus Test_PFC_PFCStatus_V_4 = Cx3_PFC_STATE_RESET_ERROR;
  PFC_PFCStatus Test_PFC_PFCStatus_V_5 = Cx4_PFC_STATE_DISCHARGE;
  PFC_PFCStatus Test_PFC_PFCStatus_V_6 = Cx5_PFC_STATE_SHUTDOWN;
  PFC_PFCStatus Test_PFC_PFCStatus_V_7 = Cx6_PFC_STATE_START_TRI;
  PFC_PFCStatus Test_PFC_PFCStatus_V_8 = Cx7_PFC_STATE_START_MONO;
  PFC_PFCStatus Test_PFC_PFCStatus_V_9 = Cx8_PFC_STATE_ERROR;
  PFC_PFCStatus Test_PFC_PFCStatus_V_10 = Cx9_PFC_STATE_DIRECTPWM;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
