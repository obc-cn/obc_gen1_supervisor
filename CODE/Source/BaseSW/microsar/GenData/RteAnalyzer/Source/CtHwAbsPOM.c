/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtHwAbsPOM.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtHwAbsPOM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtHwAbsPOM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtHwAbsPOM.h"
#include "TSC_CtHwAbsPOM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtHwAbsPOM_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDutyCloseRelay: Integer in interval [50...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtChLedRGBCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtPlgLedrCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtFrequencyActivateRelays: Integer in interval [0...40]
 *   Unit: [kHz], Factor: 1, Offset: 0
 * IdtFrequencyPWM: Integer in interval [0...100]
 *   Unit: [Hz], Factor: 20, Offset: 0
 * IdtTimeCloseRelay: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDutyCloseRelay Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed(void)
 *   IdtDutyCloseRelay Rte_CData_CalDutyKeepRelayMonoClosed(void)
 *   IdtFrequencyActivateRelays Rte_CData_CalFrequencyActivateRelays(void)
 *   IdtFrequencyPWM Rte_CData_CalFrequencyPWM(void)
 *   IdtTimeCloseRelay Rte_CData_CalTimeCloseOutputPrechargeRelays(void)
 *   IdtTimeCloseRelay Rte_CData_CalTimeCloseRelayMono(void)
 *
 *********************************************************************************************************************/


#define CtHwAbsPOM_START_SEC_CODE
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsPOM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsPOM_CODE) RCtHwAbsPOM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_init
 *********************************************************************************************************************/

  IdtDutyCloseRelay CalDutyKeepOutputPrechargeRelaysClosed_data;
  IdtDutyCloseRelay CalDutyKeepRelayMonoClosed_data;
  IdtFrequencyActivateRelays CalFrequencyActivateRelays_data;
  IdtFrequencyPWM CalFrequencyPWM_data;
  IdtTimeCloseRelay CalTimeCloseOutputPrechargeRelays_data;
  IdtTimeCloseRelay CalTimeCloseRelayMono_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDutyKeepOutputPrechargeRelaysClosed_data = TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyKeepRelayMonoClosed_data = TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepRelayMonoClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalFrequencyActivateRelays_data = TSC_CtHwAbsPOM_Rte_CData_CalFrequencyActivateRelays(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalFrequencyPWM_data = TSC_CtHwAbsPOM_Rte_CData_CalFrequencyPWM(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeCloseOutputPrechargeRelays_data = TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseOutputPrechargeRelays(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeCloseRelayMono_data = TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseRelayMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtHwAbsPOM_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsPOM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl *data)
 *   Std_ReturnType Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl *data)
 *   Std_ReturnType Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl *data)
 *   Std_ReturnType Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl *data)
 *   Std_ReturnType Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputRelayMono_DeOutputRelayMono(boolean *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsPOM_CODE) RCtHwAbsPOM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtExtChLedRGBCtl Read_PpExtChLedRGBCtl_DeExtChLedBrCtl;
  IdtExtChLedRGBCtl Read_PpExtChLedRGBCtl_DeExtChLedGrCtl;
  IdtExtChLedRGBCtl Read_PpExtChLedRGBCtl_DeExtChLedRrCtl;
  IdtExtPlgLedrCtl Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl;
  boolean Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays;
  boolean Read_PpOutputRelayMono_DeOutputRelayMono;

  IdtDutyCloseRelay CalDutyKeepOutputPrechargeRelaysClosed_data;
  IdtDutyCloseRelay CalDutyKeepRelayMonoClosed_data;
  IdtFrequencyActivateRelays CalFrequencyActivateRelays_data;
  IdtFrequencyPWM CalFrequencyPWM_data;
  IdtTimeCloseRelay CalTimeCloseOutputPrechargeRelays_data;
  IdtTimeCloseRelay CalTimeCloseRelayMono_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDutyKeepOutputPrechargeRelaysClosed_data = TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyKeepRelayMonoClosed_data = TSC_CtHwAbsPOM_Rte_CData_CalDutyKeepRelayMonoClosed(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalFrequencyActivateRelays_data = TSC_CtHwAbsPOM_Rte_CData_CalFrequencyActivateRelays(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalFrequencyPWM_data = TSC_CtHwAbsPOM_Rte_CData_CalFrequencyPWM(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeCloseOutputPrechargeRelays_data = TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseOutputPrechargeRelays(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeCloseRelayMono_data = TSC_CtHwAbsPOM_Rte_CData_CalTimeCloseRelayMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(&Read_PpExtChLedRGBCtl_DeExtChLedBrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(&Read_PpExtChLedRGBCtl_DeExtChLedGrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsPOM_Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(&Read_PpExtChLedRGBCtl_DeExtChLedRrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsPOM_Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(&Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsPOM_Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(&Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsPOM_Rte_Read_PpOutputRelayMono_DeOutputRelayMono(&Read_PpOutputRelayMono_DeOutputRelayMono); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsPOM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtHwAbsPOM_STOP_SEC_CODE
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtHwAbsPOM_TestDefines(void)
{
  /* Enumeration Data Types */

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
