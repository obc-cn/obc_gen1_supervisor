/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApRLY.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApRLY
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApRLY>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApRLY.h"
#include "TSC_CtApRLY.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApRLY_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtMaxPrechargeVDCLinkMono: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 300
 * IdtMaxPrechargeVDCLinkTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 500
 * IdtMinPrechargeVDCLinkMono: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinPrechargeVDCLinkTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 0
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxPrechargeVDCLinkMono Rte_CData_CalMaxPrechargeVDCLinkMono(void)
 *   IdtMaxPrechargeVDCLinkTriphasic Rte_CData_CalMaxPrechargeVDCLinkTriphasic(void)
 *   IdtMinPrechargeVDCLinkMono Rte_CData_CalMinPrechargeVDCLinkMono(void)
 *   IdtMinPrechargeVDCLinkTriphasic Rte_CData_CalMinPrechargeVDCLinkTriphasic(void)
 *
 *********************************************************************************************************************/


#define CtApRLY_START_SEC_CODE
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApRLY_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApRLY_CODE) RCtApRLY_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_init
 *********************************************************************************************************************/

  IdtMaxPrechargeVDCLinkMono CalMaxPrechargeVDCLinkMono_data;
  IdtMaxPrechargeVDCLinkTriphasic CalMaxPrechargeVDCLinkTriphasic_data;
  IdtMinPrechargeVDCLinkMono CalMinPrechargeVDCLinkMono_data;
  IdtMinPrechargeVDCLinkTriphasic CalMinPrechargeVDCLinkTriphasic_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxPrechargeVDCLinkMono_data = TSC_CtApRLY_Rte_CData_CalMaxPrechargeVDCLinkMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVDCLinkTriphasic_data = TSC_CtApRLY_Rte_CData_CalMaxPrechargeVDCLinkTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPrechargeVDCLinkMono_data = TSC_CtApRLY_Rte_CData_CalMinPrechargeVDCLinkMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPrechargeVDCLinkTriphasic_data = TSC_CtApRLY_Rte_CData_CalMinPrechargeVDCLinkTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApRLY_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApRLY_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpStopConditions_DeStopConditions(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputRelayMono_DeOutputRelayMono(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApRLY_CODE) RCtApRLY_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
  IdtInputVoltageMode Read_PpInputVoltageMode_DeInputVoltageMode;
  OBC_ACRange Read_PpInt_OBC_ACRange_OBC_ACRange;
  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  OBC_InputVoltageSt Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
  OBC_Status Read_PpInt_OBC_Status_OBC_Status;
  PFC_VPH12_Peak_V Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
  PFC_Vdclink_V Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
  boolean Read_PpStopConditions_DeStopConditions;

  IdtMaxPrechargeVDCLinkMono CalMaxPrechargeVDCLinkMono_data;
  IdtMaxPrechargeVDCLinkTriphasic CalMaxPrechargeVDCLinkTriphasic_data;
  IdtMinPrechargeVDCLinkMono CalMinPrechargeVDCLinkMono_data;
  IdtMinPrechargeVDCLinkTriphasic CalMinPrechargeVDCLinkTriphasic_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalMaxPrechargeVDCLinkMono_data = TSC_CtApRLY_Rte_CData_CalMaxPrechargeVDCLinkMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxPrechargeVDCLinkTriphasic_data = TSC_CtApRLY_Rte_CData_CalMaxPrechargeVDCLinkTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPrechargeVDCLinkMono_data = TSC_CtApRLY_Rte_CData_CalMinPrechargeVDCLinkMono(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMinPrechargeVDCLinkTriphasic_data = TSC_CtApRLY_Rte_CData_CalMinPrechargeVDCLinkTriphasic(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApRLY_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&Read_PpInputVoltageMode_DeInputVoltageMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(&Read_PpInt_OBC_ACRange_OBC_ACRange); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInt_OBC_Status_OBC_Status(&Read_PpInt_OBC_Status_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(&Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(&Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Read_PpStopConditions_DeStopConditions(&Read_PpStopConditions_DeStopConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays(Rte_InitValue_PpOutputPrechargeRelays_DeOutputPrechargeRelays); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Write_PpOutputRelayMono_DeOutputRelayMono(Rte_InitValue_PpOutputRelayMono_DeOutputRelayMono); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApRLY_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApRLY_STOP_SEC_CODE
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApRLY_TestDefines(void)
{
  /* Enumeration Data Types */

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtInputVoltageMode Test_IdtInputVoltageMode_V_1 = IVM_NO_INPUT_DETECTED;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_2 = IVM_MONOPHASIC_INPUT;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_3 = IVM_TRIPHASIC_INPUT;

  OBC_ACRange Test_OBC_ACRange_V_1 = Cx0_No_AC_10V_;
  OBC_ACRange Test_OBC_ACRange_V_2 = Cx1_110V_85_132V_;
  OBC_ACRange Test_OBC_ACRange_V_3 = Cx2_Invalid;
  OBC_ACRange Test_OBC_ACRange_V_4 = Cx3_220V_170_265V_;
  OBC_ACRange Test_OBC_ACRange_V_5 = Cx4_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_6 = Cx5_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_7 = Cx6_Reserved;
  OBC_ACRange Test_OBC_ACRange_V_8 = Cx7_Reserved;

  OBC_ChargingMode Test_OBC_ChargingMode_V_1 = Cx0_no_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_2 = Cx1_slow_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_3 = Cx2_China_fast_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_4 = Cx3_Euro_fast_charging;

  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_1 = Cx0_No_220V_AC;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_2 = Cx1_220V_AC_connected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_3 = Cx2_220V_AC_disconnected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_4 = Cx3_Invalid_value;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
