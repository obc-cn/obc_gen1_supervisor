/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLFM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApLFM.h"
#include "TSC_CtApLFM.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApLFM_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean *data)
{
  return Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel *data)
{
  return Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel *data)
{
  return Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel *data)
{
  return Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data)
{
  return Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
{
  return Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data)
{
  return Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed *data)
{
  return Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
{
  return Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
{
  return Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
{
  return Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
{
  return Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
{
  return Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
{
  return Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
{
  return Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT *data)
{
  return Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP *data)
{
  return Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
{
  return Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
{
  return Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean *data)
{
  return Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(data);
}




Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(boolean data)
{
  return Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(boolean data)
{
  return Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(boolean data)
{
  return Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(boolean data)
{
  return Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Write_PpDCLVError_DeDCLVError(boolean data)
{
  return Rte_Write_PpDCLVError_DeDCLVError(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(DCDC_Fault data)
{
  return Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(data);
}

Std_ReturnType TSC_CtApLFM_Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data)
{
  return Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApLFM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtDCLVFaultTimeToRetryDefault  TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void)
{
  return (IdtDCLVFaultTimeToRetryDefault ) Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault();
}
IdtDCLVFaultTimeToRetryShortCircuit  TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void)
{
  return (IdtDCLVFaultTimeToRetryShortCircuit ) Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit();
}
IdtDebounceOvercurrentDCHV  TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void)
{
  return (IdtDebounceOvercurrentDCHV ) Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV();
}

IdtThresholdLVOvercurrentHW  TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(void)
{
  return (IdtThresholdLVOvercurrentHW ) Rte_CData_CalThresholdLVOvercurrentHW();
}
IdtABSVehSpdThresholdOV  TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(void)
{
  return (IdtABSVehSpdThresholdOV ) Rte_CData_CalABSVehSpdThresholdOV();
}
IdtBatteryVoltageSafetyOVLimit1_ConfirmTime  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(void)
{
  return (IdtBatteryVoltageSafetyOVLimit1_ConfirmTime ) Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime();
}
IdtBatteryVoltageSafetyOVLimit1_Threshold  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(void)
{
  return (IdtBatteryVoltageSafetyOVLimit1_Threshold ) Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold();
}
IdtBatteryVoltageSafetyOVLimit2_ConfirmTime  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(void)
{
  return (IdtBatteryVoltageSafetyOVLimit2_ConfirmTime ) Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime();
}
IdtBatteryVoltageSafetyOVLimit2_Threshold  TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(void)
{
  return (IdtBatteryVoltageSafetyOVLimit2_Threshold ) Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold();
}
IdtDCLV_MaxNumberRetries  TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(void)
{
  return (IdtDCLV_MaxNumberRetries ) Rte_CData_CalDCLV_MaxNumberRetries();
}
IdtTimeBatteryVoltageSafety  TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(void)
{
  return (IdtTimeBatteryVoltageSafety ) Rte_CData_CalTimeBatteryVoltageSafety();
}

     /* CtApLFM */
      /* CtApLFM */

/** Per Instance Memories */
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error(void)
{
  return Rte_Pim_PimDCLVHWFault_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error(void)
{
  return Rte_Pim_PimDCLV_FaultSatellite_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error(void)
{
  return Rte_Pim_PimDCLV_InternalCom_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error(void)
{
  return Rte_Pim_PimDCLV_OutputOvercurrent_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error(void)
{
  return Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error(void)
{
  return Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error(void)
{
  return Rte_Pim_PimDCLV_OutputShortCircuit_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error(void)
{
  return Rte_Pim_PimDCLV_OvervoltageHV_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error(void)
{
  return Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error(void)
{
  return Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
}
IdtFaultLevel *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error(void)
{
  return Rte_Pim_PimDCLV_Overvoltage_Error();
}



