/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApBAT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApBAT_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApBAT_Rte_Write_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(boolean data);
Std_ReturnType TSC_CtApBAT_Rte_Write_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState data);

/** Client server interfaces */
Std_ReturnType TSC_CtApBAT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CalBatteryVoltageOVHealTime(void);
IdtBatteryVoltageThreshold  TSC_CtApBAT_Rte_CData_CalBatteryVoltageThresholdOV(void);
IdtBatteryVoltageThreshold  TSC_CtApBAT_Rte_CData_CalBatteryVoltageThresholdUV(void);
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CalBatteryVoltageUVConfirmTime(void);
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CalBatteryVoltageUVHealTime(void);
IdtBatteryVoltageTime  TSC_CtApBAT_Rte_CData_CarBatteryVoltageOVConfirmTime(void);




