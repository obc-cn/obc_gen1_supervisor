/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApTBD.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApTBD.h"
#include "TSC_CtApTBD.h"








Std_ReturnType TSC_CtApTBD_Rte_Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(sint32 *data)
{
  return Rte_Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag *data)
{
  return Rte_Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt *data)
{
  return Rte_Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status *data)
{
  return Rte_Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt *data)
{
  return Rte_Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_BMS_Fault_BMS_Fault(BMS_Fault *data)
{
  return Rte_Read_PpInt_BMS_Fault_BMS_Fault(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq *data)
{
  return Rte_Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC *data)
{
  return Rte_Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG *data)
{
  return Rte_Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpInt_VCU_Keyposition_VCU_Keyposition(VCU_Keyposition *data)
{
  return Rte_Read_PpInt_VCU_Keyposition_VCU_Keyposition(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales(IdtOutputTempAftsales *data)
{
  return Rte_Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales(IdtOutputTempAftsales *data)
{
  return Rte_Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales(IdtOutputTempAftsales *data)
{
  return Rte_Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempACNAftsales(IdtOutputTempAftsales *data)
{
  return Rte_Read_PpOutputTempAftsales_DeOutputTempACNAftsales(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales(IdtOutputTempAftsales *data)
{
  return Rte_Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales(IdtOutputTempAftsales *data)
{
  return Rte_Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt *data)
{
  return Rte_Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Read_Ppint_MODE_DIAG_MODE_DIAG(MODE_DIAG *data)
{
  return Rte_Read_Ppint_MODE_DIAG_MODE_DIAG(data);
}




Std_ReturnType TSC_CtApTBD_Rte_Write_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data)
{
  return Rte_Write_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Write_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data)
{
  return Rte_Write_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(data);
}

Std_ReturnType TSC_CtApTBD_Rte_Write_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data)
{
  return Rte_Write_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* CtApTBD */
      /* CtApTBD */



