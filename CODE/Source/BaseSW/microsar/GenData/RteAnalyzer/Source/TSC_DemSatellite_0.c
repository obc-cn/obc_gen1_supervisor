/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DemSatellite_0.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DemSatellite_0.h"
#include "TSC_DemSatellite_0.h"















     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D407_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D407_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D40C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D40C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D49C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D49C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D4CA_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D4CA_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D5CF_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D5CF_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D5D1_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D5D1_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D805_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D805_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D806_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D806_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D807_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D807_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D808_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D808_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D809_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D809_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D80C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D80C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D822_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D822_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D824_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D824_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D825_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D825_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D827_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D827_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D828_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D828_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D829_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D829_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D82B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82B_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D82C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D82D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82D_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D82E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82E_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D82F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D82F_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D831_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D831_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D83B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83B_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D83C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D83D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83D_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D83E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83E_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D83F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D83F_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D840_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D840_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D843_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D843_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D844_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D844_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D845_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D845_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D846_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D846_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D84A_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84A_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D84B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84B_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D84C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D84D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84D_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D84E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84E_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D84F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D84F_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D850_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D850_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D851_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D851_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D852_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D852_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D853_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D853_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D854_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D854_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D855_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D855_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D8E9_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D8E9_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_D8EB_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_D8EB_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE60_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE60_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE61_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE61_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE62_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE62_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE63_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE63_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE64_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE64_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE65_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE65_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE66_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE66_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE67_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE67_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE68_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE68_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE69_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE69_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE6A_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6A_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE6B_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6B_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE6C_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6C_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE6D_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6D_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE6E_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6E_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE6F_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE6F_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE70_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE70_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE71_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE71_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE72_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE72_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_FE73_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_FE73_ReadData(Data);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* DemSatellite_0 */
      /* DemSatellite_0 */



