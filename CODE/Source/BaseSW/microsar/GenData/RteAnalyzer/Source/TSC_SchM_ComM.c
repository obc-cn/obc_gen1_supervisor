/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_ComM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_ComM.h"
#include "TSC_SchM_ComM.h"
void TSC_ComM_SchM_Enter_ComM_COMM_EXCLUSIVE_AREA_0(void)
{
  SchM_Enter_ComM_COMM_EXCLUSIVE_AREA_0();
}
void TSC_ComM_SchM_Exit_ComM_COMM_EXCLUSIVE_AREA_0(void)
{
  SchM_Exit_ComM_COMM_EXCLUSIVE_AREA_0();
}
void TSC_ComM_SchM_Enter_ComM_COMM_EXCLUSIVE_AREA_1(void)
{
  SchM_Enter_ComM_COMM_EXCLUSIVE_AREA_1();
}
void TSC_ComM_SchM_Exit_ComM_COMM_EXCLUSIVE_AREA_1(void)
{
  SchM_Exit_ComM_COMM_EXCLUSIVE_AREA_1();
}
