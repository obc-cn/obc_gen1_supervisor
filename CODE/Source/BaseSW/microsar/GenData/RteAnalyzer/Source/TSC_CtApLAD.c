/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApLAD.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApLAD.h"
#include "TSC_CtApLAD.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApLAD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
{
  return Rte_Read_PpAppRCDECUState_DeAppRCDECUState(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
{
  return Rte_Read_PpBatteryVolt_DeBatteryVolt(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
{
  return Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(boolean *data)
{
  return Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(boolean *data)
{
  return Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent *data)
{
  return Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock *data)
{
  return Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock *data)
{
  return Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
{
  return Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint *data)
{
  return Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
{
  return Rte_Read_PpHWEditionDetected_DeHWEditionDetected(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
{
  return Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
{
  return Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
{
  return Rte_Read_PpOutputELockSensor_DeOutputELockSensor(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
{
  return Rte_Read_PpPlantModeState_DePlantModeState(data);
}




Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockOvercurrent(boolean data)
{
  return Rte_Write_PpELockFaults_DeOutputElockOvercurrent(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(boolean data)
{
  return Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean data)
{
  return Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(data);
}

Std_ReturnType TSC_CtApLAD_Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean data)
{
  return Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApLAD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtBatteryVoltMaxTest  TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
{
  return (IdtBatteryVoltMaxTest ) Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
}
IdtBatteryVoltMinTest  TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
{
  return (IdtBatteryVoltMinTest ) Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
}
IdtVehStopMaxTest  TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
{
  return (IdtVehStopMaxTest ) Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();
}

IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(void)
{
  return (IdtElockThreshold ) Rte_CData_CalElockThresholdNoCurrent();
}
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(void)
{
  return (IdtElockThreshold ) Rte_CData_CalElockThresholdOvercurrent();
}
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(void)
{
  return (IdtElockThreshold ) Rte_CData_CalElockThresholdSCG();
}
IdtElockThreshold  TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(void)
{
  return (IdtElockThreshold ) Rte_CData_CalElockThresholdSCP();
}
IdtAfts_ElockActuatorTime1  TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(void)
{
  return (IdtAfts_ElockActuatorTime1 ) Rte_CData_CalAfts_ElockActuatorTime1();
}
IdtAfts_ElockActuatorTime2  TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(void)
{
  return (IdtAfts_ElockActuatorTime2 ) Rte_CData_CalAfts_ElockActuatorTime2();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockHOpenCircuitConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockHOpenCircuitHealTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockHSCGConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockHSCGHealTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockHSCPConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockHSCPHealTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLOpenCircuitConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLOpenCircuitHealTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLOvercurrentConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLSCGConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLSCGHealTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLSCPConfirmTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockLSCPHealTime();
}
IdtElockActuatorTime  TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(void)
{
  return (IdtElockActuatorTime ) Rte_CData_CalOutputElockOvercurrentHealTime();
}
boolean  TSC_CtApLAD_Rte_CData_CalEnableElockActuator(void)
{
  return (boolean ) Rte_CData_CalEnableElockActuator();
}

     /* CtApLAD */
      /* CtApLAD */

/** Per Instance Memories */
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent(void)
{
  return Rte_Pim_PimElockActuatorCounterOverCurrent();
}
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit(void)
{
  return Rte_Pim_PimElockActuatorHCounterOpenCircuit();
}
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG(void)
{
  return Rte_Pim_PimElockActuatorHCounterSCG();
}
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP(void)
{
  return Rte_Pim_PimElockActuatorHCounterSCP();
}
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit(void)
{
  return Rte_Pim_PimElockActuatorLCounterOpenCircuit();
}
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG(void)
{
  return Rte_Pim_PimElockActuatorLCounterSCG();
}
uint16 *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP(void)
{
  return Rte_Pim_PimElockActuatorLCounterSCP();
}
IdtOutputELockSensor *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock(void)
{
  return Rte_Pim_PimElockSensor_PreviousToLock();
}
IdtOutputELockSensor *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock(void)
{
  return Rte_Pim_PimElockSensor_PreviousToUnlock();
}
IdtELockSetPoint *TSC_CtApLAD_Rte_Pim_PimElockSetPoint(void)
{
  return Rte_Pim_PimElockSetPoint();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit(void)
{
  return Rte_Pim_PimElockActuatorHPrefaultOpenCircuit();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG(void)
{
  return Rte_Pim_PimElockActuatorHPrefaultSCG();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP(void)
{
  return Rte_Pim_PimElockActuatorHPrefaultSCP();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit(void)
{
  return Rte_Pim_PimElockActuatorLPrefaultOpenCircuit();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG(void)
{
  return Rte_Pim_PimElockActuatorLPrefaultSCG();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP(void)
{
  return Rte_Pim_PimElockActuatorLPrefaultSCP();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent(void)
{
  return Rte_Pim_PimElockActuatorPrefaultOverCurrent();
}
boolean *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured(void)
{
  return Rte_Pim_PimElockNotCurrentMeasured();
}



