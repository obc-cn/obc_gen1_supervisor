/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtHwAbsAIM.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtHwAbsAIM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtHwAbsAIM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * NvM_BlockIdType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtHwAbsAIM.h"
#include "TSC_CtHwAbsAIM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtHwAbsAIM_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtAmbientTemperaturePhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtDebounceADCReference: Integer in interval [1...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtElockFeedbackCurrent: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackLock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackUnlock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtExtOpPlugLockRaw: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtMsrTempRaw: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtPlugLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtProximityDetectPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtTempSyncRectPhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtVoltageExternalADCReference: Integer in interval [1...350]
 *   Unit: [mV], Factor: 10, Offset: 0
 * NvM_BlockIdType: Integer in interval [1...32767]
 * Rte_DT_IdtArrayInitAIMVoltRef_0: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 *   HW_EDITION_UNKNOWN (0U)
 *   HW_EDITION_D21 (1U)
 *   HW_EDITION_D31 (2U)
 *
 * Array Types:
 * ============
 * IdtArrayInitAIMVoltRef: Array with 3 element(s) of type Rte_DT_IdtArrayInitAIMVoltRef_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   Rte_DT_IdtArrayInitAIMVoltRef_0 *Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(void)
 *     Returnvalue: Rte_DT_IdtArrayInitAIMVoltRef_0* is of type IdtArrayInitAIMVoltRef
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtVoltageExternalADCReference Rte_CData_CalVoltageExternalADCReferenceWave1(void)
 *   IdtVoltageExternalADCReference Rte_CData_CalVoltageExternalADCReferenceWave2(void)
 *   IdtDebounceADCReference Rte_CData_CalDebounceADCReference(void)
 *   Rte_DT_IdtArrayInitAIMVoltRef_0 *Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(void)
 *     Returnvalue: Rte_DT_IdtArrayInitAIMVoltRef_0* is of type IdtArrayInitAIMVoltRef
 *
 *********************************************************************************************************************/


#define CtHwAbsAIM_START_SEC_CODE
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsAIM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_init
 *********************************************************************************************************************/

  IdtArrayInitAIMVoltRef PimAIMVoltRefNvBlockNeed_MirrorBlock;

  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave1_data;
  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave2_data;
  IdtDebounceADCReference CalDebounceADCReference_data;
  IdtArrayInitAIMVoltRef AIMVoltRefNvBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimAIMVoltRefNvBlockNeed_MirrorBlock, TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(), sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(), PimAIMVoltRefNvBlockNeed_MirrorBlock, sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalVoltageExternalADCReferenceWave1_data = TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVoltageExternalADCReferenceWave2_data = TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceADCReference_data = TSC_CtHwAbsAIM_Rte_CData_CalDebounceADCReference(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(AIMVoltRefNvBlockNeed_DefaultValue_data, TSC_CtHwAbsAIM_Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(), sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  CtHwAbsAIM_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsAIM_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue data)
 *   Std_ReturnType Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtArrayInitAIMVoltRef PimAIMVoltRefNvBlockNeed_MirrorBlock;

  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave1_data;
  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave2_data;
  IdtDebounceADCReference CalDebounceADCReference_data;
  IdtArrayInitAIMVoltRef AIMVoltRefNvBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimAIMVoltRefNvBlockNeed_MirrorBlock, TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(), sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(), PimAIMVoltRefNvBlockNeed_MirrorBlock, sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalVoltageExternalADCReferenceWave1_data = TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVoltageExternalADCReferenceWave2_data = TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceADCReference_data = TSC_CtHwAbsAIM_Rte_CData_CalDebounceADCReference(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(AIMVoltRefNvBlockNeed_DefaultValue_data, TSC_CtHwAbsAIM_Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(), sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(Rte_InitValue_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(Rte_InitValue_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(Rte_InitValue_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(Rte_InitValue_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw(Rte_InitValue_PpMsrTempRaw_DeMsrTempAC1Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw(Rte_InitValue_PpMsrTempRaw_DeMsrTempAC2Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw(Rte_InitValue_PpMsrTempRaw_DeMsrTempAC3Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw(Rte_InitValue_PpMsrTempRaw_DeMsrTempACNRaw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw(Rte_InitValue_PpMsrTempRaw_DeMsrTempDC1Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw(Rte_InitValue_PpMsrTempRaw_DeMsrTempDC2Raw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(Rte_InitValue_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsAIM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt data)
 *   Std_ReturnType Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent data)
 *   Std_ReturnType Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock data)
 *   Std_ReturnType Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock data)
 *   Std_ReturnType Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw data)
 *   Std_ReturnType Rte_Write_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected data)
 *   Std_ReturnType Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK
 *   Std_ReturnType Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtArrayInitAIMVoltRef PimAIMVoltRefNvBlockNeed_MirrorBlock;

  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave1_data;
  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave2_data;
  IdtDebounceADCReference CalDebounceADCReference_data;
  IdtArrayInitAIMVoltRef AIMVoltRefNvBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/


  (void)memcpy(PimAIMVoltRefNvBlockNeed_MirrorBlock, TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(), sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */
  (void)memcpy(TSC_CtHwAbsAIM_Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(), PimAIMVoltRefNvBlockNeed_MirrorBlock, sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */


  CalVoltageExternalADCReferenceWave1_data = TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalVoltageExternalADCReferenceWave2_data = TSC_CtHwAbsAIM_Rte_CData_CalVoltageExternalADCReferenceWave2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceADCReference_data = TSC_CtHwAbsAIM_Rte_CData_CalDebounceADCReference(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  (void)memcpy(AIMVoltRefNvBlockNeed_DefaultValue_data, TSC_CtHwAbsAIM_Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(), sizeof(IdtArrayInitAIMVoltRef)); /* PRQA S 0315, 0316, 3226, 1495 */ /* MD_Rte_0315, MD_Rte_0316, MD_Rte_3226, MD_Rte_Qac */

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpBatteryVolt_DeBatteryVolt(Rte_InitValue_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent(Rte_InitValue_PpElockFeedbackCurrent_DeElockFeedbackCurrent); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock(Rte_InitValue_PpElockFeedbackLock_DeElockFeedbackLock); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock(Rte_InitValue_PpElockFeedbackUnlock_DeElockFeedbackUnlock); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(Rte_InitValue_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpHWEditionDetected_DeHWEditionDetected(Rte_InitValue_PpHWEditionDetected_DeHWEditionDetected); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(Rte_InitValue_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(Rte_InitValue_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtHwAbsAIM_Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtHwAbsAIM_STOP_SEC_CODE
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtHwAbsAIM_TestDefines(void)
{
  /* Enumeration Data Types */

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtHWEditionDetected Test_IdtHWEditionDetected_V_1 = HW_EDITION_UNKNOWN;
  IdtHWEditionDetected Test_IdtHWEditionDetected_V_2 = HW_EDITION_D21;
  IdtHWEditionDetected Test_IdtHWEditionDetected_V_3 = HW_EDITION_D31;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Qac:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
