/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLSD.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApLSD
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLSD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApLSD.h"
#include "TSC_CtApLSD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApLSD_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtELockDebounce: Integer in interval [0...255]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtELockSensorFaultThreshold: Integer in interval [0...180]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtELockSensorFaultTime: Integer in interval [0...20]
 *   Unit: [ms], Factor: 50, Offset: 0
 * IdtELockSensorLimit: Integer in interval [0...180]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtExtOpPlugLockRaw: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * OBC_ElockState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Elock_unlocked (0U)
 *   Cx1_Elock_locked (1U)
 *   Cx2_E_lock_lock_fail (2U)
 *   Cx3_E_Lock_unlock_fail (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimElockCounterSCG(void)
 *   uint16 *Rte_Pim_PimElockCounterSCP(void)
 *   boolean *Rte_Pim_PimElockPrefaultSCG(void)
 *   boolean *Rte_Pim_PimElockPrefaultSCP(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtELockDebounce Rte_CData_CalELockDebounce(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorDriveHigh(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorDriveLow(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorLockHigh(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorLockLow(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorUnlockHigh(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorUnlockLow(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCGConfirmTime(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCGHealTime(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCPConfirmTime(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCPHealTime(void)
 *   IdtELockSensorFaultThreshold Rte_CData_CalElockSensorThresholdSCG(void)
 *   IdtELockSensorFaultThreshold Rte_CData_CalElockSensorThresholdSCP(void)
 *   boolean Rte_CData_CalCtrlFlowPlugLock(void)
 *
 *********************************************************************************************************************/


#define CtApLSD_START_SEC_CODE
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLSD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLSD_CODE) RCtApLSD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_init
 *********************************************************************************************************************/

  uint16 PimPimElockCounterSCG;
  uint16 PimPimElockCounterSCP;
  boolean PimPimElockPrefaultSCG;
  boolean PimPimElockPrefaultSCP;

  IdtELockDebounce CalELockDebounce_data;
  IdtELockSensorLimit CalELockSensorDriveHigh_data;
  IdtELockSensorLimit CalELockSensorDriveLow_data;
  IdtELockSensorLimit CalELockSensorLockHigh_data;
  IdtELockSensorLimit CalELockSensorLockLow_data;
  IdtELockSensorLimit CalELockSensorUnlockHigh_data;
  IdtELockSensorLimit CalELockSensorUnlockLow_data;
  IdtELockSensorFaultTime CalElockSensorSCGConfirmTime_data;
  IdtELockSensorFaultTime CalElockSensorSCGHealTime_data;
  IdtELockSensorFaultTime CalElockSensorSCPConfirmTime_data;
  IdtELockSensorFaultTime CalElockSensorSCPHealTime_data;
  IdtELockSensorFaultThreshold CalElockSensorThresholdSCG_data;
  IdtELockSensorFaultThreshold CalElockSensorThresholdSCP_data;
  boolean CalCtrlFlowPlugLock_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockCounterSCG = *TSC_CtApLSD_Rte_Pim_PimElockCounterSCG();
  *TSC_CtApLSD_Rte_Pim_PimElockCounterSCG() = PimPimElockCounterSCG;
  PimPimElockCounterSCP = *TSC_CtApLSD_Rte_Pim_PimElockCounterSCP();
  *TSC_CtApLSD_Rte_Pim_PimElockCounterSCP() = PimPimElockCounterSCP;
  PimPimElockPrefaultSCG = *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCG();
  *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCG() = PimPimElockPrefaultSCG;
  PimPimElockPrefaultSCP = *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCP();
  *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCP() = PimPimElockPrefaultSCP;

  CalELockDebounce_data = TSC_CtApLSD_Rte_CData_CalELockDebounce(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorDriveHigh_data = TSC_CtApLSD_Rte_CData_CalELockSensorDriveHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorDriveLow_data = TSC_CtApLSD_Rte_CData_CalELockSensorDriveLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorLockHigh_data = TSC_CtApLSD_Rte_CData_CalELockSensorLockHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorLockLow_data = TSC_CtApLSD_Rte_CData_CalELockSensorLockLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorUnlockHigh_data = TSC_CtApLSD_Rte_CData_CalELockSensorUnlockHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorUnlockLow_data = TSC_CtApLSD_Rte_CData_CalELockSensorUnlockLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCGConfirmTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCGHealTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCPConfirmTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCPHealTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorThresholdSCG_data = TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorThresholdSCP_data = TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowPlugLock_data = TSC_CtApLSD_Rte_CData_CalCtrlFlowPlugLock(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApLSD_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLSD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(OBC_ElockState data)
 *   Std_ReturnType Rte_Write_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLSD_CODE) RCtApLSD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtBatteryVoltageState Read_PpBatteryVoltageState_DeBatteryVoltageState;
  IdtELockSetPoint Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode;
  IdtExtOpPlugLockRaw Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw;

  uint16 PimPimElockCounterSCG;
  uint16 PimPimElockCounterSCP;
  boolean PimPimElockPrefaultSCG;
  boolean PimPimElockPrefaultSCP;

  IdtELockDebounce CalELockDebounce_data;
  IdtELockSensorLimit CalELockSensorDriveHigh_data;
  IdtELockSensorLimit CalELockSensorDriveLow_data;
  IdtELockSensorLimit CalELockSensorLockHigh_data;
  IdtELockSensorLimit CalELockSensorLockLow_data;
  IdtELockSensorLimit CalELockSensorUnlockHigh_data;
  IdtELockSensorLimit CalELockSensorUnlockLow_data;
  IdtELockSensorFaultTime CalElockSensorSCGConfirmTime_data;
  IdtELockSensorFaultTime CalElockSensorSCGHealTime_data;
  IdtELockSensorFaultTime CalElockSensorSCPConfirmTime_data;
  IdtELockSensorFaultTime CalElockSensorSCPHealTime_data;
  IdtELockSensorFaultThreshold CalElockSensorThresholdSCG_data;
  IdtELockSensorFaultThreshold CalElockSensorThresholdSCP_data;
  boolean CalCtrlFlowPlugLock_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockCounterSCG = *TSC_CtApLSD_Rte_Pim_PimElockCounterSCG();
  *TSC_CtApLSD_Rte_Pim_PimElockCounterSCG() = PimPimElockCounterSCG;
  PimPimElockCounterSCP = *TSC_CtApLSD_Rte_Pim_PimElockCounterSCP();
  *TSC_CtApLSD_Rte_Pim_PimElockCounterSCP() = PimPimElockCounterSCP;
  PimPimElockPrefaultSCG = *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCG();
  *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCG() = PimPimElockPrefaultSCG;
  PimPimElockPrefaultSCP = *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCP();
  *TSC_CtApLSD_Rte_Pim_PimElockPrefaultSCP() = PimPimElockPrefaultSCP;

  CalELockDebounce_data = TSC_CtApLSD_Rte_CData_CalELockDebounce(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorDriveHigh_data = TSC_CtApLSD_Rte_CData_CalELockSensorDriveHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorDriveLow_data = TSC_CtApLSD_Rte_CData_CalELockSensorDriveLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorLockHigh_data = TSC_CtApLSD_Rte_CData_CalELockSensorLockHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorLockLow_data = TSC_CtApLSD_Rte_CData_CalELockSensorLockLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorUnlockHigh_data = TSC_CtApLSD_Rte_CData_CalELockSensorUnlockHigh(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalELockSensorUnlockLow_data = TSC_CtApLSD_Rte_CData_CalELockSensorUnlockLow(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCGConfirmTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCGHealTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCPConfirmTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorSCPHealTime_data = TSC_CtApLSD_Rte_CData_CalElockSensorSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorThresholdSCG_data = TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockSensorThresholdSCP_data = TSC_CtApLSD_Rte_CData_CalElockSensorThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalCtrlFlowPlugLock_data = TSC_CtApLSD_Rte_CData_CalCtrlFlowPlugLock(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLSD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&Read_PpBatteryVoltageState_DeBatteryVoltageState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(&Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(&Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(Rte_InitValue_PpELockSensorFaults_DeELockSensorFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(Rte_InitValue_PpELockSensorFaults_DeELockSensorFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(Rte_InitValue_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(Rte_InitValue_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Write_PpOutputELockSensor_DeOutputELockSensor(Rte_InitValue_PpOutputELockSensor_DeOutputELockSensor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLSD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLSD_STOP_SEC_CODE
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApLSD_TestDefines(void)
{
  /* Enumeration Data Types */

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_1 = BAT_VALID_RANGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_2 = BAT_OVERVOLTAGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_3 = BAT_UNDERVOLTAGE;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtELockSetPoint Test_IdtELockSetPoint_V_1 = ELOCK_SETPOINT_UNLOCKED;
  IdtELockSetPoint Test_IdtELockSetPoint_V_2 = ELOCK_SETPOINT_LOCKED;

  IdtOutputELockSensor Test_IdtOutputELockSensor_V_1 = ELOCK_LOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_2 = ELOCK_UNLOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_3 = ELOCK_DRIVE_UNDEFINED;

  OBC_ElockState Test_OBC_ElockState_V_1 = Cx0_Elock_unlocked;
  OBC_ElockState Test_OBC_ElockState_V_2 = Cx1_Elock_locked;
  OBC_ElockState Test_OBC_ElockState_V_3 = Cx2_E_lock_lock_fail;
  OBC_ElockState Test_OBC_ElockState_V_4 = Cx3_E_Lock_unlock_fail;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
