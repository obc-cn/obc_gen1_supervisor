/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApDER.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApDER
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApDER>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApDER.h"
#include "TSC_CtApDER.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCDC_Temperature: Integer in interval [0...255]
 * DCHV_ADC_NTC_MOD_5: Integer in interval [0...255]
 * DCHV_ADC_NTC_MOD_6: Integer in interval [0...255]
 * DCLV_T_PP_A: Integer in interval [0...255]
 * DCLV_T_PP_B: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_A: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_B: Integer in interval [0...255]
 * DCLV_Temp_Derating_Factor: Integer in interval [0...2047]
 * IdtAmbientTemperaturePhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtPDERATING_OBC: Integer in interval [0...1024]
 * IdtTempDerating: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtTempSyncRectPhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * OBC_OBCTemp: Integer in interval [0...255]
 * PFC_Temp_M1_C: Integer in interval [0...255]
 * PFC_Temp_M4_C: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtTempDerating Rte_CData_CalDCDC_FinishLinear(void)
 *   IdtTempDerating Rte_CData_CalDCDC_MaxDeviation(void)
 *   IdtTempDerating Rte_CData_CalDCDC_MinDeviation(void)
 *   IdtTempDerating Rte_CData_CalDCDC_StartLinear(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBC_FinishLinear(void)
 *   IdtTempDerating Rte_CData_CalOBC_MaxDeviation(void)
 *   IdtTempDerating Rte_CData_CalOBC_MinDeviation(void)
 *   IdtTempDerating Rte_CData_CalOBC_StartLinear(void)
 *
 *********************************************************************************************************************/


#define CtApDER_START_SEC_CODE
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDER_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDER_CODE) RCtApDER_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_init
 *********************************************************************************************************************/

  IdtTempDerating CalDCDC_FinishLinear_data;
  IdtTempDerating CalDCDC_MaxDeviation_data;
  IdtTempDerating CalDCDC_MinDeviation_data;
  IdtTempDerating CalDCDC_StartLinear_data;
  IdtTempDerating CalDCLVHighTempEndDerating_AmbTemp_data;
  IdtTempDerating CalDCLVHighTempEndDerating_Buck_A_data;
  IdtTempDerating CalDCLVHighTempEndDerating_Buck_B_data;
  IdtTempDerating CalDCLVHighTempEndDerating_PushPull_A_data;
  IdtTempDerating CalDCLVHighTempEndDerating_PushPull_B_data;
  IdtTempDerating CalDCLVHighTempEndDerating_SyncRect_data;
  IdtTempDerating CalDCLVHighTempStartDerating_AmbTemp_data;
  IdtTempDerating CalDCLVHighTempStartDerating_Buck_A_data;
  IdtTempDerating CalDCLVHighTempStartDerating_Buck_B_data;
  IdtTempDerating CalDCLVHighTempStartDerating_PushPull_A_data;
  IdtTempDerating CalDCLVHighTempStartDerating_PushPull_B_data;
  IdtTempDerating CalDCLVHighTempStartDerating_SyncRect_data;
  IdtTempDerating CalDCLVLowTempEndDerating_AmbTemp_data;
  IdtTempDerating CalDCLVLowTempEndDerating_Buck_A_data;
  IdtTempDerating CalDCLVLowTempEndDerating_Buck_B_data;
  IdtTempDerating CalDCLVLowTempEndDerating_PushPull_A_data;
  IdtTempDerating CalDCLVLowTempEndDerating_PushPull_B_data;
  IdtTempDerating CalDCLVLowTempEndDerating_SyncRect_data;
  IdtTempDerating CalDCLVLowTempStartDerating_AmbTemp_data;
  IdtTempDerating CalDCLVLowTempStartDerating_Buck_A_data;
  IdtTempDerating CalDCLVLowTempStartDerating_Buck_B_data;
  IdtTempDerating CalDCLVLowTempStartDerating_PushPull_A_data;
  IdtTempDerating CalDCLVLowTempStartDerating_PushPull_B_data;
  IdtTempDerating CalDCLVLowTempStartDerating_SyncRect_data;
  IdtTempDerating CalOBCHighTempEndDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCHighTempEndDerating_DCHV_M5_data;
  IdtTempDerating CalOBCHighTempEndDerating_DCHV_M6_data;
  IdtTempDerating CalOBCHighTempEndDerating_PFC_M1_data;
  IdtTempDerating CalOBCHighTempEndDerating_PFC_M4_data;
  IdtTempDerating CalOBCHighTempStartDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCHighTempStartDerating_DCHV_M5_data;
  IdtTempDerating CalOBCHighTempStartDerating_DCHV_M6_data;
  IdtTempDerating CalOBCHighTempStartDerating_PFC_M1_data;
  IdtTempDerating CalOBCHighTempStartDerating_PFC_M4_data;
  IdtTempDerating CalOBCLowTempEndDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCLowTempEndDerating_DCHV_M5_data;
  IdtTempDerating CalOBCLowTempEndDerating_DCHV_M6_data;
  IdtTempDerating CalOBCLowTempEndDerating_PFC_M1_data;
  IdtTempDerating CalOBCLowTempEndDerating_PFC_M4_data;
  IdtTempDerating CalOBCLowTempStartDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCLowTempStartDerating_DCHV_M5_data;
  IdtTempDerating CalOBCLowTempStartDerating_DCHV_M6_data;
  IdtTempDerating CalOBCLowTempStartDerating_PFC_M1_data;
  IdtTempDerating CalOBCLowTempStartDerating_PFC_M4_data;
  IdtTempDerating CalOBC_FinishLinear_data;
  IdtTempDerating CalOBC_MaxDeviation_data;
  IdtTempDerating CalOBC_MinDeviation_data;
  IdtTempDerating CalOBC_StartLinear_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDCDC_FinishLinear_data = TSC_CtApDER_Rte_CData_CalDCDC_FinishLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCDC_MaxDeviation_data = TSC_CtApDER_Rte_CData_CalDCDC_MaxDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCDC_MinDeviation_data = TSC_CtApDER_Rte_CData_CalDCDC_MinDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCDC_StartLinear_data = TSC_CtApDER_Rte_CData_CalDCDC_StartLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_FinishLinear_data = TSC_CtApDER_Rte_CData_CalOBC_FinishLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_MaxDeviation_data = TSC_CtApDER_Rte_CData_CalOBC_MaxDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_MinDeviation_data = TSC_CtApDER_Rte_CData_CalOBC_MinDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_StartLinear_data = TSC_CtApDER_Rte_CData_CalOBC_StartLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDER_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data)
 *   Std_ReturnType Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean data)
 *   Std_ReturnType Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data)
 *   Std_ReturnType Rte_Write_PpOBCDerating_DeOBCDerating(boolean data)
 *   Std_ReturnType Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDER_CODE) RCtApDER_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAmbientTemperaturePhysicalValue Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue;
  DCHV_ADC_NTC_MOD_5 Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5;
  DCHV_ADC_NTC_MOD_6 Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6;
  DCLV_T_PP_A Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A;
  DCLV_T_PP_B Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B;
  DCLV_T_SW_BUCK_A Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A;
  DCLV_T_SW_BUCK_B Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B;
  PFC_Temp_M1_C Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C;
  PFC_Temp_M4_C Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C;
  IdtTempSyncRectPhysicalValue Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue;

  IdtTempDerating CalDCDC_FinishLinear_data;
  IdtTempDerating CalDCDC_MaxDeviation_data;
  IdtTempDerating CalDCDC_MinDeviation_data;
  IdtTempDerating CalDCDC_StartLinear_data;
  IdtTempDerating CalDCLVHighTempEndDerating_AmbTemp_data;
  IdtTempDerating CalDCLVHighTempEndDerating_Buck_A_data;
  IdtTempDerating CalDCLVHighTempEndDerating_Buck_B_data;
  IdtTempDerating CalDCLVHighTempEndDerating_PushPull_A_data;
  IdtTempDerating CalDCLVHighTempEndDerating_PushPull_B_data;
  IdtTempDerating CalDCLVHighTempEndDerating_SyncRect_data;
  IdtTempDerating CalDCLVHighTempStartDerating_AmbTemp_data;
  IdtTempDerating CalDCLVHighTempStartDerating_Buck_A_data;
  IdtTempDerating CalDCLVHighTempStartDerating_Buck_B_data;
  IdtTempDerating CalDCLVHighTempStartDerating_PushPull_A_data;
  IdtTempDerating CalDCLVHighTempStartDerating_PushPull_B_data;
  IdtTempDerating CalDCLVHighTempStartDerating_SyncRect_data;
  IdtTempDerating CalDCLVLowTempEndDerating_AmbTemp_data;
  IdtTempDerating CalDCLVLowTempEndDerating_Buck_A_data;
  IdtTempDerating CalDCLVLowTempEndDerating_Buck_B_data;
  IdtTempDerating CalDCLVLowTempEndDerating_PushPull_A_data;
  IdtTempDerating CalDCLVLowTempEndDerating_PushPull_B_data;
  IdtTempDerating CalDCLVLowTempEndDerating_SyncRect_data;
  IdtTempDerating CalDCLVLowTempStartDerating_AmbTemp_data;
  IdtTempDerating CalDCLVLowTempStartDerating_Buck_A_data;
  IdtTempDerating CalDCLVLowTempStartDerating_Buck_B_data;
  IdtTempDerating CalDCLVLowTempStartDerating_PushPull_A_data;
  IdtTempDerating CalDCLVLowTempStartDerating_PushPull_B_data;
  IdtTempDerating CalDCLVLowTempStartDerating_SyncRect_data;
  IdtTempDerating CalOBCHighTempEndDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCHighTempEndDerating_DCHV_M5_data;
  IdtTempDerating CalOBCHighTempEndDerating_DCHV_M6_data;
  IdtTempDerating CalOBCHighTempEndDerating_PFC_M1_data;
  IdtTempDerating CalOBCHighTempEndDerating_PFC_M4_data;
  IdtTempDerating CalOBCHighTempStartDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCHighTempStartDerating_DCHV_M5_data;
  IdtTempDerating CalOBCHighTempStartDerating_DCHV_M6_data;
  IdtTempDerating CalOBCHighTempStartDerating_PFC_M1_data;
  IdtTempDerating CalOBCHighTempStartDerating_PFC_M4_data;
  IdtTempDerating CalOBCLowTempEndDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCLowTempEndDerating_DCHV_M5_data;
  IdtTempDerating CalOBCLowTempEndDerating_DCHV_M6_data;
  IdtTempDerating CalOBCLowTempEndDerating_PFC_M1_data;
  IdtTempDerating CalOBCLowTempEndDerating_PFC_M4_data;
  IdtTempDerating CalOBCLowTempStartDerating_AmbTempOBC_data;
  IdtTempDerating CalOBCLowTempStartDerating_DCHV_M5_data;
  IdtTempDerating CalOBCLowTempStartDerating_DCHV_M6_data;
  IdtTempDerating CalOBCLowTempStartDerating_PFC_M1_data;
  IdtTempDerating CalOBCLowTempStartDerating_PFC_M4_data;
  IdtTempDerating CalOBC_FinishLinear_data;
  IdtTempDerating CalOBC_MaxDeviation_data;
  IdtTempDerating CalOBC_MinDeviation_data;
  IdtTempDerating CalOBC_StartLinear_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDCDC_FinishLinear_data = TSC_CtApDER_Rte_CData_CalDCDC_FinishLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCDC_MaxDeviation_data = TSC_CtApDER_Rte_CData_CalDCDC_MaxDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCDC_MinDeviation_data = TSC_CtApDER_Rte_CData_CalDCDC_MinDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCDC_StartLinear_data = TSC_CtApDER_Rte_CData_CalDCDC_StartLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempEndDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVHighTempStartDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempEndDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_AmbTemp_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_AmbTemp(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_Buck_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_Buck_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_PushPull_A_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_A(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_PushPull_B_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_B(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLVLowTempStartDerating_SyncRect_data = TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_SyncRect(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempEndDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCHighTempStartDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempEndDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_AmbTempOBC_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_DCHV_M5_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M5(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_DCHV_M6_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M6(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_PFC_M1_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBCLowTempStartDerating_PFC_M4_data = TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M4(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_FinishLinear_data = TSC_CtApDER_Rte_CData_CalOBC_FinishLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_MaxDeviation_data = TSC_CtApDER_Rte_CData_CalOBC_MaxDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_MinDeviation_data = TSC_CtApDER_Rte_CData_CalOBC_MinDeviation(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOBC_StartLinear_data = TSC_CtApDER_Rte_CData_CalOBC_StartLinear(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApDER_Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(&Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(&Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(&Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(&Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(&Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(&Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(&Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(&Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(&Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(&Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(Rte_InitValue_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(Rte_InitValue_PpImplausibilityTempBuck_DeImplausibilityTempBuck); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(Rte_InitValue_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(Rte_InitValue_PpInt_DCDC_Temperature_DCDC_Temperature); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(Rte_InitValue_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp(Rte_InitValue_PpInt_OBC_OBCTemp_OBC_OBCTemp); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpOBCDerating_DeOBCDerating(Rte_InitValue_PpOBCDerating_DeOBCDerating); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApDER_Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(Rte_InitValue_PpPDERATING_OBC_DePDERATING_OBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApDER_STOP_SEC_CODE
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
