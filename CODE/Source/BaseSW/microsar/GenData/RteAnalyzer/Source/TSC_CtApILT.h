/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApILT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApILT_Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeChargeError(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeGuideManagement(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data);
Std_ReturnType TSC_CtApILT_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApILT_Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeChargeError(boolean data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeChargeInProgress(boolean data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeEndOfCharge(boolean data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeGuideManagement(boolean data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLedModes_DeProgrammingCharge(boolean data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(boolean data);
Std_ReturnType TSC_CtApILT_Rte_Write_PpLockStateError_DeLockStateError(boolean data);

/** Client server interfaces */
Std_ReturnType TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);
Std_ReturnType TSC_CtApILT_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue);

/** SW-C local Calibration Parameters */
IdtMaxTimeChargeError  TSC_CtApILT_Rte_CData_CalMaxTimeChargeError(void);
IdtMaxTimeChargeInProgress  TSC_CtApILT_Rte_CData_CalMaxTimeChargeInProgress(void);
IdtMaxTimeEndOfCharge  TSC_CtApILT_Rte_CData_CalMaxTimeEndOfCharge(void);
IdtMaxTimeGuideManagement  TSC_CtApILT_Rte_CData_CalMaxTimeGuideManagement(void);
IdtMaxTimeProgrammingCharge  TSC_CtApILT_Rte_CData_CalMaxTimeProgrammingCharge(void);
IdtTimePushButtonKeepValue  TSC_CtApILT_Rte_CData_CalTimePushButtonKeepValue(void);
IdtDebounceStateFailure  TSC_CtApILT_Rte_CData_CalDebounceStateFailure(void);
IdtDebounceStateFinished  TSC_CtApILT_Rte_CData_CalDebounceStateFinished(void);
IdtDebounceStateInProgress  TSC_CtApILT_Rte_CData_CalDebounceStateInProgress(void);
IdtDebounceStateStopped  TSC_CtApILT_Rte_CData_CalDebounceStateStopped(void);
IdtElockTimeError  TSC_CtApILT_Rte_CData_CalElockTimeError(void);




