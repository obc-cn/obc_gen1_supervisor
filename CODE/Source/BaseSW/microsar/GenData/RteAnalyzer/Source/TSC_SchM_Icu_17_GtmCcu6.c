/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Icu_17_GtmCcu6.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_Icu_17_GtmCcu6.h"
#include "TSC_SchM_Icu_17_GtmCcu6.h"
void TSC_Icu_17_GtmCcu6_SchM_Enter_Icu_17_GtmCcu6_Ccu6IenUpdate(void)
{
  SchM_Enter_Icu_17_GtmCcu6_Ccu6IenUpdate();
}
void TSC_Icu_17_GtmCcu6_SchM_Exit_Icu_17_GtmCcu6_Ccu6IenUpdate(void)
{
  SchM_Exit_Icu_17_GtmCcu6_Ccu6IenUpdate();
}
void TSC_Icu_17_GtmCcu6_SchM_Enter_Icu_17_GtmCcu6_CcuInterruptHandle(void)
{
  SchM_Enter_Icu_17_GtmCcu6_CcuInterruptHandle();
}
void TSC_Icu_17_GtmCcu6_SchM_Exit_Icu_17_GtmCcu6_CcuInterruptHandle(void)
{
  SchM_Exit_Icu_17_GtmCcu6_CcuInterruptHandle();
}
void TSC_Icu_17_GtmCcu6_SchM_Enter_Icu_17_GtmCcu6_CcuVariableupdate(void)
{
  SchM_Enter_Icu_17_GtmCcu6_CcuVariableupdate();
}
void TSC_Icu_17_GtmCcu6_SchM_Exit_Icu_17_GtmCcu6_CcuVariableupdate(void)
{
  SchM_Exit_Icu_17_GtmCcu6_CcuVariableupdate();
}
void TSC_Icu_17_GtmCcu6_SchM_Enter_Icu_17_GtmCcu6_EnableNotification(void)
{
  SchM_Enter_Icu_17_GtmCcu6_EnableNotification();
}
void TSC_Icu_17_GtmCcu6_SchM_Exit_Icu_17_GtmCcu6_EnableNotification(void)
{
  SchM_Exit_Icu_17_GtmCcu6_EnableNotification();
}
void TSC_Icu_17_GtmCcu6_SchM_Enter_Icu_17_GtmCcu6_EnableWakeup(void)
{
  SchM_Enter_Icu_17_GtmCcu6_EnableWakeup();
}
void TSC_Icu_17_GtmCcu6_SchM_Exit_Icu_17_GtmCcu6_EnableWakeup(void)
{
  SchM_Exit_Icu_17_GtmCcu6_EnableWakeup();
}
void TSC_Icu_17_GtmCcu6_SchM_Enter_Icu_17_GtmCcu6_ResetEdgeCount(void)
{
  SchM_Enter_Icu_17_GtmCcu6_ResetEdgeCount();
}
void TSC_Icu_17_GtmCcu6_SchM_Exit_Icu_17_GtmCcu6_ResetEdgeCount(void)
{
  SchM_Exit_Icu_17_GtmCcu6_ResetEdgeCount();
}
