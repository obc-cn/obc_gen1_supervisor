/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Com_Cfg.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Com Signal definitions
 *********************************************************************************************************************/

#ifndef _COM_CFG_H_
# define _COM_CFG_H_


# define ComConf_ComSignal_ABS_VehSpdValidFlag_oVCU_PCANInfo_oE_CAN_badcc650_Rx 0

# define ComConf_ComSignal_ABS_VehSpd_oVCU_PCANInfo_oE_CAN_c4f9375f_Rx 1

# define ComConf_ComSignal_BMS_AuxBattVolt_oBMS3_oE_CAN_3f0258cb_Rx 2

# define ComConf_ComSignal_BMS_CC2_connection_Status_oBMS8_oE_CAN_ce1fe0ec_Rx 3

# define ComConf_ComSignal_BMS_DCRelayVoltage_oBMS9_oE_CAN_c2a98509_Rx 4

# define ComConf_ComSignal_BMS_FastChargeSt_oBMS6_oE_CAN_21564382_Rx 5

# define ComConf_ComSignal_BMS_Fault_oBMS5_oE_CAN_87a5f6c7_Rx 6

# define ComConf_ComSignal_BMS_HighestChargeCurrentAllow_oBMS6_oE_CAN_060c33c4_Rx 7

# define ComConf_ComSignal_BMS_HighestChargeVoltageAllow_oBMS6_oE_CAN_c465fe5e_Rx 8

# define ComConf_ComSignal_BMS_MainConnectorState_oBMS1_oE_CAN_db099b33_Rx 9

# define ComConf_ComSignal_BMS_OnBoardChargerEnable_oBMS6_oE_CAN_b5381f70_Rx 10

# define ComConf_ComSignal_BMS_QuickChargeConnectorState_oBMS1_oE_CAN_940005e5_Rx 11

# define ComConf_ComSignal_BMS_RelayOpenReq_oBMS1_oE_CAN_9e155f98_Rx 12

# define ComConf_ComSignal_BMS_SOC_oBMS1_oE_CAN_4aa6784d_Rx 13

# define ComConf_ComSignal_BMS_SlowChargeSt_oBMS6_oE_CAN_25b6b066_Rx 14

# define ComConf_ComSignal_BMS_Voltage_oBMS1_oE_CAN_2c5227ff_Rx 15

# define ComConf_ComSignal_BSI_ChargeState_oBSIInfo_oE_CAN_be52c881_Rx 16

# define ComConf_ComSignal_BSI_ChargeTypeStatus_oBSIInfo_oE_CAN_f023c7ab_Rx 17

# define ComConf_ComSignal_BSI_LockedVehicle_oBSIInfo_oE_CAN_bee231f7_Rx 18

# define ComConf_ComSignal_BSI_MainWakeup_oBSIInfo_oE_CAN_3acf26ac_Rx 19

# define ComConf_ComSignal_BSI_PostDriveWakeup_oBSIInfo_oE_CAN_c679787c_Rx 20

# define ComConf_ComSignal_BSI_PreDriveWakeup_oBSIInfo_oE_CAN_10f8dc31_Rx 21

# define ComConf_ComSignal_BSI_VCUHeatPumpWorkingMode_oBSIInfo_oE_CAN_56dcb551_Rx 22

# define ComConf_ComSignal_BSI_VCUSynchroGPC_oBSIInfo_oE_CAN_8a27e8bd_Rx 23

# define ComConf_ComSignal_COUPURE_CONSO_CTPE2_oVCU_PCANInfo_oE_CAN_4e44e208_Rx 24

# define ComConf_ComSignal_COUPURE_CONSO_CTP_oVCU_PCANInfo_oE_CAN_1a153f59_Rx 25

# define ComConf_ComSignal_DATA0_oProgTool_SupEnterBoot_oInt_CAN_d0fc95c5_Rx 26

# define ComConf_ComSignal_DATA1_oProgTool_SupEnterBoot_oInt_CAN_e60e0536_Rx 27

# define ComConf_ComSignal_DATA2_oProgTool_SupEnterBoot_oInt_CAN_bd19b423_Rx 28

# define ComConf_ComSignal_DATA3_oProgTool_SupEnterBoot_oInt_CAN_8beb24d0_Rx 29

# define ComConf_ComSignal_DATA4_oProgTool_SupEnterBoot_oInt_CAN_0b36d609_Rx 30

# define ComConf_ComSignal_DATA5_oProgTool_SupEnterBoot_oInt_CAN_3dc446fa_Rx 31

# define ComConf_ComSignal_DATA6_oProgTool_SupEnterBoot_oInt_CAN_66d3f7ef_Rx 32

# define ComConf_ComSignal_DATA7_oProgTool_SupEnterBoot_oInt_CAN_5021671c_Rx 33

# define ComConf_ComSignal_DATA_ACQ_JDD_BSI_2_oNEW_JDD_oE_CAN_16efcc6e_Rx 34

# define ComConf_ComSignal_DCDC_CurrentReference_oSUP_CommandToDCHV_oInt_CAN_e1358897_Tx 35

# define ComConf_ComSignal_DCDC_FaultLampRequest_oDC1_oE_CAN_66ccec8a_Tx 36

# define ComConf_ComSignal_DCDC_Fault_oDC1_oE_CAN_80e733c8_Tx 37

# define ComConf_ComSignal_DCDC_HighVoltConnectionAllowed_oDC1_oE_CAN_7634e78c_Tx 38

# define ComConf_ComSignal_DCDC_InputVoltage_oDC1_oE_CAN_dc52df4d_Tx 39

# define ComConf_ComSignal_DCDC_OVERTEMP_oDC1_oE_CAN_9db3c9a4_Tx 40

# define ComConf_ComSignal_DCDC_OutputVoltage_oDC1_oE_CAN_b16c6aa0_Tx 41

# define ComConf_ComSignal_DCDC_Status_oDC1_oE_CAN_0062049f_Tx 42

# define ComConf_ComSignal_DCDC_Temperature_oDC1_oE_CAN_ebbc428a_Tx 43

# define ComConf_ComSignal_DCDC_VoltageReference_oSUP_CommandToDCHV_oInt_CAN_154ec40d_Tx 44

# define ComConf_ComSignal_DCHV_ADC_IOUT_oDCHV_Status1_oInt_CAN_db9652d6_Rx 45

# define ComConf_ComSignal_DCHV_ADC_NTC_MOD_5_oDCHV_Status2_oInt_CAN_46287fbe_Rx 46

# define ComConf_ComSignal_DCHV_ADC_NTC_MOD_6_oDCHV_Status2_oInt_CAN_1e36d696_Rx 47

# define ComConf_ComSignal_DCHV_ADC_VOUT_oDCHV_Status1_oInt_CAN_95b5698f_Rx 48

# define ComConf_ComSignal_DCHV_Command_Current_Reference_oDCHV_Status1_oInt_CAN_205fcb72_Rx 49

# define ComConf_ComSignal_DCHV_DCHVStatus_oDCHV_Status1_oInt_CAN_4479f96a_Rx 50

# define ComConf_ComSignal_DCHV_IOM_ERR_CAP_FAIL_H_oDCHV_Fault_oInt_CAN_a3babff0_Rx 51

# define ComConf_ComSignal_DCHV_IOM_ERR_CAP_FAIL_L_oDCHV_Fault_oInt_CAN_ff1b2cf0_Rx 52

# define ComConf_ComSignal_DCHV_IOM_ERR_OC_IOUT_oDCHV_Fault_oInt_CAN_2f168ffb_Rx 53

# define ComConf_ComSignal_DCHV_IOM_ERR_OC_NEG_oDCHV_Fault_oInt_CAN_d091e3ea_Rx 54

# define ComConf_ComSignal_DCHV_IOM_ERR_OC_POS_oDCHV_Fault_oInt_CAN_d5b3bbb7_Rx 55

# define ComConf_ComSignal_DCHV_IOM_ERR_OT_IN_oDCHV_Fault_oInt_CAN_5338fe6d_Rx 56

# define ComConf_ComSignal_DCHV_IOM_ERR_OT_NTC_MOD5_oDCHV_Fault_oInt_CAN_05da4c61_Rx 57

# define ComConf_ComSignal_DCHV_IOM_ERR_OT_NTC_MOD6_oDCHV_Fault_oInt_CAN_3ca2e121_Rx 58

# define ComConf_ComSignal_DCHV_IOM_ERR_OT_oDCHV_Fault_oInt_CAN_33753ff7_Rx 59

# define ComConf_ComSignal_DCHV_IOM_ERR_OV_VOUT_oDCHV_Fault_oInt_CAN_c4e31cdb_Rx 60

# define ComConf_ComSignal_DCHV_IOM_ERR_UV_12V_oDCHV_Fault_oInt_CAN_7ceb9676_Rx 61

# define ComConf_ComSignal_DCLV_Applied_Derating_Factor_oDCLV_Status3_oInt_CAN_8199edb4_Rx 62

# define ComConf_ComSignal_DCLV_DCLVStatus_oDCLV_Status1_oInt_CAN_aede0f5b_Rx 63

# define ComConf_ComSignal_DCLV_Input_Current_oDCLV_Status3_oInt_CAN_053924dc_Rx 64

# define ComConf_ComSignal_DCLV_Input_Voltage_oDCLV_Status3_oInt_CAN_a46268d5_Rx 65

# define ComConf_ComSignal_DCLV_Measured_Current_oDCLV_Status1_oInt_CAN_4e0e970b_Rx 66

# define ComConf_ComSignal_DCLV_Measured_Voltage_oDCLV_Status1_oInt_CAN_ef55db02_Rx 67

# define ComConf_ComSignal_DCLV_OC_A_HV_FAULT_oDCLV_Fault_oInt_CAN_7ab3f331_Rx 68

# define ComConf_ComSignal_DCLV_OC_A_LV_FAULT_oDCLV_Fault_oInt_CAN_2ddd91e0_Rx 69

# define ComConf_ComSignal_DCLV_OC_B_HV_FAULT_oDCLV_Fault_oInt_CAN_699bca42_Rx 70

# define ComConf_ComSignal_DCLV_OC_B_LV_FAULT_oDCLV_Fault_oInt_CAN_3ef5a893_Rx 71

# define ComConf_ComSignal_DCLV_OT_FAULT_oDCLV_Fault_oInt_CAN_334ae1fe_Rx 72

# define ComConf_ComSignal_DCLV_OV_INT_A_FAULT_oDCLV_Fault_oInt_CAN_3c3a442a_Rx 73

# define ComConf_ComSignal_DCLV_OV_INT_B_FAULT_oDCLV_Fault_oInt_CAN_3ee4430d_Rx 74

# define ComConf_ComSignal_DCLV_OV_UV_HV_FAULT_oDCLV_Fault_oInt_CAN_3d935207_Rx 75

# define ComConf_ComSignal_DCLV_PWM_STOP_oDCLV_Fault_oInt_CAN_13fc246e_Rx 76

# define ComConf_ComSignal_DCLV_Power_oDCLV_Status1_oInt_CAN_74948da2_Rx 77

# define ComConf_ComSignal_DCLV_T_L_BUCK_oDCLV_Status2_oInt_CAN_f4d254b0_Rx 78

# define ComConf_ComSignal_DCLV_T_PP_A_oDCLV_Status2_oInt_CAN_33203dc6_Rx 79

# define ComConf_ComSignal_DCLV_T_PP_B_oDCLV_Status2_oInt_CAN_6b3e94ee_Rx 80

# define ComConf_ComSignal_DCLV_T_SW_BUCK_A_oDCLV_Status2_oInt_CAN_f172fb4d_Rx 81

# define ComConf_ComSignal_DCLV_T_SW_BUCK_B_oDCLV_Status2_oInt_CAN_a96c5265_Rx 82

# define ComConf_ComSignal_DCLV_T_TX_PP_oDCLV_Status2_oInt_CAN_19721921_Rx 83

# define ComConf_ComSignal_DCLV_Temp_Derating_Factor_oSUP_CommandToDCLV_oInt_CAN_18f05833_Tx 84

# define ComConf_ComSignal_DCLV_VoltageReference_oSUP_CommandToDCLV_oInt_CAN_927eb38f_Tx 85

# define ComConf_ComSignal_DIAG_INTEGRA_ELEC_oELECTRON_BSI_oE_CAN_fb8cbe6b_Rx 86

# define ComConf_ComSignal_Debug1_1_oDebug1_oInt_CAN_c4f96638_Tx 87

# define ComConf_ComSignal_Debug1_2_oDebug1_oInt_CAN_2e7fbb5a_Tx 88

# define ComConf_ComSignal_Debug1_3_oDebug1_oInt_CAN_c12d0dbb_Tx 89

# define ComConf_ComSignal_Debug1_4_oDebug1_oInt_CAN_200307df_Tx 90

# define ComConf_ComSignal_Debug1_5_oDebug1_oInt_CAN_cf51b13e_Tx 91

# define ComConf_ComSignal_Debug1_6_oDebug1_oInt_CAN_25d76c5c_Tx 92

# define ComConf_ComSignal_Debug1_7_oDebug1_oInt_CAN_ca85dabd_Tx 93

# define ComConf_ComSignal_Debug2_0_oDebug2_oInt_CAN_6f352610_Tx 94

# define ComConf_ComSignal_Debug2_1_oDebug2_oInt_CAN_806790f1_Tx 95

# define ComConf_ComSignal_Debug2_2_oDebug2_oInt_CAN_6ae14d93_Tx 96

# define ComConf_ComSignal_Debug2_3_oDebug2_oInt_CAN_85b3fb72_Tx 97

# define ComConf_ComSignal_Debug2_4_oDebug2_oInt_CAN_649df116_Tx 98

# define ComConf_ComSignal_Debug2_5_oDebug2_oInt_CAN_8bcf47f7_Tx 99

# define ComConf_ComSignal_Debug2_6_oDebug2_oInt_CAN_61499a95_Tx 100

# define ComConf_ComSignal_Debug2_7_oDebug2_oInt_CAN_8e1b2c74_Tx 101

# define ComConf_ComSignal_Debug3_0_oDebug3_oInt_CAN_e5907668_Tx 102

# define ComConf_ComSignal_Debug3_1_oDebug3_oInt_CAN_0ac2c089_Tx 103

# define ComConf_ComSignal_Debug3_2_oDebug3_oInt_CAN_e0441deb_Tx 104

# define ComConf_ComSignal_Debug3_3_oDebug3_oInt_CAN_0f16ab0a_Tx 105

# define ComConf_ComSignal_Debug3_4_oDebug3_oInt_CAN_ee38a16e_Tx 106

# define ComConf_ComSignal_Debug3_5_oDebug3_oInt_CAN_016a178f_Tx 107

# define ComConf_ComSignal_Debug3_6_oDebug3_oInt_CAN_ebeccaed_Tx 108

# define ComConf_ComSignal_Debug3_7_oDebug3_oInt_CAN_04be7c0c_Tx 109

# define ComConf_ComSignal_Debug4_0_oDebug4_oInt_CAN_e608cb82_Tx 110

# define ComConf_ComSignal_Debug4_1_oDebug4_oInt_CAN_095a7d63_Tx 111

# define ComConf_ComSignal_Debug4_2_oDebug4_oInt_CAN_e3dca001_Tx 112

# define ComConf_ComSignal_Debug4_3_oDebug4_oInt_CAN_0c8e16e0_Tx 113

# define ComConf_ComSignal_Debug4_4_oDebug4_oInt_CAN_eda01c84_Tx 114

# define ComConf_ComSignal_Debug4_5_oDebug4_oInt_CAN_02f2aa65_Tx 115

# define ComConf_ComSignal_Debug4_6_oDebug4_oInt_CAN_e8747707_Tx 116

# define ComConf_ComSignal_Debug4_7_oDebug4_oInt_CAN_0726c1e6_Tx 117

# define ComConf_ComSignal_Degug1_0_oDebug1_oInt_CAN_b0849b9e_Tx 118

# define ComConf_ComSignal_EDITION_CALIB_oVERS_OBC_DCDC_oE_CAN_20fa071e_Tx 119

# define ComConf_ComSignal_EDITION_SOFT_oVERS_OBC_DCDC_oE_CAN_bde5b057_Tx 120

# define ComConf_ComSignal_EFFAC_DEFAUT_DIAG_oELECTRON_BSI_oE_CAN_83a2b5c6_Rx 121

# define ComConf_ComSignal_EVSE_RTAB_STOP_CHARGE_oOBC1_oE_CAN_01a50620_Tx 122

# define ComConf_ComSignal_MODE_DIAG_oELECTRON_BSI_oE_CAN_f76ac80c_Rx 123

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_0_oNEW_JDD_OBC_DCDC_oE_CAN_bfd944fb_Tx 124

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_1_oNEW_JDD_OBC_DCDC_oE_CAN_58c4e26c_Tx 125

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_2_oNEW_JDD_OBC_DCDC_oE_CAN_aa930f94_Tx 126

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_3_oNEW_JDD_OBC_DCDC_oE_CAN_4d8ea903_Tx 127

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_4_oNEW_JDD_OBC_DCDC_oE_CAN_954dd225_Tx 128

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_5_oNEW_JDD_OBC_DCDC_oE_CAN_725074b2_Tx 129

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_6_oNEW_JDD_OBC_DCDC_oE_CAN_8007994a_Tx 130

# define ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_7_oNEW_JDD_OBC_DCDC_oE_CAN_671a3fdd_Tx 131

# define ComConf_ComSignal_OBC_ACRange_oOBC2_oE_CAN_1149ad47_Tx 132

# define ComConf_ComSignal_OBC_CP_connection_Status_oOBC1_oE_CAN_bef9ca9d_Tx 133

# define ComConf_ComSignal_OBC_ChargingConnectionConfirmati_oOBC2_oE_CAN_e178031d_Tx 134

# define ComConf_ComSignal_OBC_ChargingMode_oOBC1_oE_CAN_ce17c9da_Tx 135

# define ComConf_ComSignal_OBC_CommunicationSt_oOBC2_oE_CAN_e0b8e211_Tx 136

# define ComConf_ComSignal_OBC_CoolingWakeup_oOBC3_oE_CAN_91759696_Tx 137

# define ComConf_ComSignal_OBC_DCChargingPlugAConnConf_oOBC2_oE_CAN_ef0cc399_Tx 138

# define ComConf_ComSignal_OBC_ElockState_oOBC1_oE_CAN_d48080ac_Tx 139

# define ComConf_ComSignal_OBC_Fault_oOBC1_oE_CAN_cba9590d_Tx 140

# define ComConf_ComSignal_OBC_HVBattRechargeWakeup_oOBC3_oE_CAN_2290bde4_Tx 141

# define ComConf_ComSignal_OBC_HighVoltConnectionAllowed_oOBC1_oE_CAN_b82322d0_Tx 142

# define ComConf_ComSignal_OBC_HoldDiscontactorWakeup_oOBC3_oE_CAN_0a4fcf18_Tx 143

# define ComConf_ComSignal_OBC_InputVoltageSt_oOBC2_oE_CAN_f750717a_Tx 144

# define ComConf_ComSignal_OBC_OBCStartSt_oOBC2_oE_CAN_5b2bb089_Tx 145

# define ComConf_ComSignal_OBC_OBCTemp_oOBC2_oE_CAN_0c3bcf20_Tx 146

# define ComConf_ComSignal_OBC_OutputCurrent_oOBC2_oE_CAN_c3e935dd_Tx 147

# define ComConf_ComSignal_OBC_OutputVoltage_oOBC2_oE_CAN_afb611f2_Tx 148

# define ComConf_ComSignal_OBC_PIStateInfoWakeup_oOBC3_oE_CAN_b3c75a3a_Tx 149

# define ComConf_ComSignal_OBC_PlugVoltDetection_oOBC2_oE_CAN_3eae826f_Tx 150

# define ComConf_ComSignal_OBC_PowerMax_oOBC2_oE_CAN_cb72170d_Tx 151

# define ComConf_ComSignal_OBC_PushChargeType_oOBC4_oE_CAN_f9102f70_Tx 152

# define ComConf_ComSignal_OBC_RechargeHMIState_oOBC4_oE_CAN_2e6d800d_Tx 153

# define ComConf_ComSignal_OBC_SocketTempL_oOBC1_oE_CAN_72ea0018_Tx 154

# define ComConf_ComSignal_OBC_SocketTempN_oOBC1_oE_CAN_9384c4b5_Tx 155

# define ComConf_ComSignal_OBC_Status_oOBC1_oE_CAN_ef77955c_Tx 156

# define ComConf_ComSignal_PFC_IOM_ERR_OC_PH1_oPFC_Fault_oInt_CAN_a122859f_Rx 157

# define ComConf_ComSignal_PFC_IOM_ERR_OC_PH2_oPFC_Fault_oInt_CAN_da3c077c_Rx 158

# define ComConf_ComSignal_PFC_IOM_ERR_OC_PH3_oPFC_Fault_oInt_CAN_45e684e2_Rx 159

# define ComConf_ComSignal_PFC_IOM_ERR_OC_SHUNT1_oPFC_Fault_oInt_CAN_25f6ba61_Rx 160

# define ComConf_ComSignal_PFC_IOM_ERR_OC_SHUNT2_oPFC_Fault_oInt_CAN_5ee83882_Rx 161

# define ComConf_ComSignal_PFC_IOM_ERR_OC_SHUNT3_oPFC_Fault_oInt_CAN_c132bb1c_Rx 162

# define ComConf_ComSignal_PFC_IOM_ERR_OT_NTC1_M1_oPFC_Fault_oInt_CAN_a0dec855_Rx 163

# define ComConf_ComSignal_PFC_IOM_ERR_OT_NTC1_M3_oPFC_Fault_oInt_CAN_441ac928_Rx 164

# define ComConf_ComSignal_PFC_IOM_ERR_OT_NTC1_M4_oPFC_Fault_oInt_CAN_2dfd4f70_Rx 165

# define ComConf_ComSignal_PFC_IOM_ERR_OV_DCLINK_oPFC_Fault_oInt_CAN_df590e67_Rx 166

# define ComConf_ComSignal_PFC_IOM_ERR_OV_VPH12_oPFC_Fault_oInt_CAN_70e217b6_Rx 167

# define ComConf_ComSignal_PFC_IOM_ERR_OV_VPH23_oPFC_Fault_oInt_CAN_d6403968_Rx 168

# define ComConf_ComSignal_PFC_IOM_ERR_OV_VPH31_oPFC_Fault_oInt_CAN_25ac5cd5_Rx 169

# define ComConf_ComSignal_PFC_IOM_ERR_UVLO_ISO4_oPFC_Fault_oInt_CAN_83f330a8_Rx 170

# define ComConf_ComSignal_PFC_IOM_ERR_UVLO_ISO7_oPFC_Fault_oInt_CAN_f8edb24b_Rx 171

# define ComConf_ComSignal_PFC_IOM_ERR_UV_12V_oPFC_Fault_oInt_CAN_a7954067_Rx 172

# define ComConf_ComSignal_PFC_IPH1_RMS_0A1_oPFC_Status3_oInt_CAN_e736f57d_Rx 173

# define ComConf_ComSignal_PFC_IPH2_RMS_0A1_oPFC_Status3_oInt_CAN_f41ecc0e_Rx 174

# define ComConf_ComSignal_PFC_IPH3_RMS_0A1_oPFC_Status3_oInt_CAN_faf924df_Rx 175

# define ComConf_ComSignal_PFC_PFCStatus_oPFC_Status1_oInt_CAN_4eddb451_Rx 176

# define ComConf_ComSignal_PFC_Temp_M1_C_oPFC_Status1_oInt_CAN_23fc84dd_Rx 177

# define ComConf_ComSignal_PFC_Temp_M3_C_oPFC_Status1_oInt_CAN_b39aa280_Rx 178

# define ComConf_ComSignal_PFC_Temp_M4_C_oPFC_Status1_oInt_CAN_7dca5e28_Rx 179

# define ComConf_ComSignal_PFC_VPH12_Peak_V_oPFC_Status2_oInt_CAN_2da0b0ab_Rx 180

# define ComConf_ComSignal_PFC_VPH12_RMS_V_oPFC_Status3_oInt_CAN_dbb6f081_Rx 181

# define ComConf_ComSignal_PFC_VPH1_Freq_Hz_oPFC_Status5_oInt_CAN_489242f9_Rx 182

# define ComConf_ComSignal_PFC_VPH23_Peak_V_oPFC_Status2_oInt_CAN_02e86ad0_Rx 183

# define ComConf_ComSignal_PFC_VPH23_RMS_V_oPFC_Status3_oInt_CAN_fc100cbd_Rx 184

# define ComConf_ComSignal_PFC_VPH2_Freq_Hz_oPFC_Status5_oInt_CAN_5bba7b8a_Rx 185

# define ComConf_ComSignal_PFC_VPH31_Peak_V_oPFC_Status2_oInt_CAN_74ce4411_Rx 186

# define ComConf_ComSignal_PFC_VPH31_RMS_V_oPFC_Status3_oInt_CAN_067f5dfd_Rx 187

# define ComConf_ComSignal_PFC_VPH3_Freq_Hz_oPFC_Status5_oInt_CAN_555d935b_Rx 188

# define ComConf_ComSignal_PFC_Vdclink_V_oPFC_Status1_oInt_CAN_cb855707_Rx 189

# define ComConf_ComSignalGroup_SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx 190

# define ComConf_ComGroupSignal_DCDC_ActivedischargeSt_oDC2_oE_CAN_5a2849b9_Tx 191

# define ComConf_ComGroupSignal_DCDC_InputCurrent_oDC2_oE_CAN_0b6c1afa_Tx 192

# define ComConf_ComGroupSignal_DCDC_OBCDCDCRTPowerLoad_oDC2_oE_CAN_e771c64f_Tx 193

# define ComConf_ComGroupSignal_DCDC_OBCMainContactorReq_oDC2_oE_CAN_40cd3485_Tx 194

# define ComConf_ComGroupSignal_DCDC_OBCQuickChargeContactorReq_oDC2_oE_CAN_2093b4ef_Tx 195

# define ComConf_ComGroupSignal_DCDC_OutputCurrent_oDC2_oE_CAN_6652af17_Tx 196

# define ComConf_ComSignal_SUPV_CoolingWupState_oSUPV_V2_OBC_DCDC_oE_CAN_addf5c22_Tx 197

# define ComConf_ComSignal_SUPV_DTCRegistred_oSUPV_V2_OBC_DCDC_oE_CAN_04412eaa_Tx 198

# define ComConf_ComSignal_SUPV_ECUElecStateRCD_oSUPV_V2_OBC_DCDC_oE_CAN_ff7122fa_Tx 199

# define ComConf_ComSignal_SUPV_HVBattChargeWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1071a795_Tx 200

# define ComConf_ComSignal_SUPV_HoldDiscontactorWupState_oSUPV_V2_OBC_DCDC_oE_CAN_b0368fb5_Tx 201

# define ComConf_ComSignal_SUPV_PIStateInfoWupState_oSUPV_V2_OBC_DCDC_oE_CAN_a649d590_Tx 202

# define ComConf_ComSignal_SUPV_PostDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_5f77ad32_Tx 203

# define ComConf_ComSignal_SUPV_PreDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_bfe17473_Tx 204

# define ComConf_ComSignal_SUPV_PrecondElecWupState_oSUPV_V2_OBC_DCDC_oE_CAN_4523c42d_Tx 205

# define ComConf_ComSignal_SUPV_RCDLineState_oSUPV_V2_OBC_DCDC_oE_CAN_ab22fee8_Tx 206

# define ComConf_ComSignal_SUPV_StopDelayedHMIWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1dcc6f45_Tx 207

# define ComConf_ComSignal_SUP_CommandVDCLink_V_oSUP_CommandToPFC_oInt_CAN_b6bd9cf9_Tx 208

# define ComConf_ComSignal_SUP_RequestDCLVStatus_oSUP_CommandToDCLV_oInt_CAN_08473e1f_Tx 209

# define ComConf_ComSignal_SUP_RequestPFCStatus_oSUP_CommandToPFC_oInt_CAN_98b04349_Tx 210

# define ComConf_ComSignal_SUP_RequestStatusDCHV_oSUP_CommandToDCHV_oInt_CAN_7bfbedf8_Tx 211

# define ComConf_ComSignal_VCU_AccPedalPosition_oVCU_TU_oE_CAN_a12a1de4_Rx 212

# define ComConf_ComSignal_VCU_ActivedischargeCommand_oCtrlDCDC_oE_CAN_6f6ad9a3_Rx 213

# define ComConf_ComSignal_VCU_CDEAccJDD_oVCU_BSI_Wakeup_oE_CAN_153d1d79_Rx 214

# define ComConf_ComSignal_VCU_CDEApcJDD_oVCU_BSI_Wakeup_oE_CAN_0cbccb8e_Rx 215

# define ComConf_ComSignal_VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_Rx 216

# define ComConf_ComSignal_VCU_CptTemporel_oVCU_oE_CAN_80a510d2_Rx 217

# define ComConf_ComSignal_VCU_DCDCActivation_oCtrlDCDC_oE_CAN_541eed40_Rx 218

# define ComConf_ComSignal_VCU_DCDCVoltageReq_oCtrlDCDC_oE_CAN_2adee191_Rx 219

# define ComConf_ComSignal_VCU_DDEGMVSEEM_oVCU_BSI_Wakeup_oE_CAN_073a8f6a_Rx 220

# define ComConf_ComSignal_VCU_DMDActivChiller_oVCU_BSI_Wakeup_oE_CAN_eb265576_Rx 221

# define ComConf_ComSignal_VCU_DMDMeap2SEEM_oVCU_BSI_Wakeup_oE_CAN_3b64713a_Rx 222

# define ComConf_ComSignal_VCU_DiagMuxOnPwt_oVCU_BSI_Wakeup_oE_CAN_712c6c84_Rx 223

# define ComConf_ComSignal_VCU_EPWT_Status_oParkCommand_oE_CAN_c4868b19_Rx 224

# define ComConf_ComSignal_VCU_ElecMeterSaturation_oVCU3_oE_CAN_c481c141_Rx 225

# define ComConf_ComSignal_VCU_EtatGmpHyb_oVCU_BSI_Wakeup_oE_CAN_0076bb1d_Rx 226

# define ComConf_ComSignal_VCU_EtatPrincipSev_oVCU_BSI_Wakeup_oE_CAN_8c281110_Rx 227

# define ComConf_ComSignal_VCU_EtatReseauElec_oVCU_BSI_Wakeup_oE_CAN_b805a640_Rx 228

# define ComConf_ComSignal_VCU_Keyposition_oVCU2_oE_CAN_7b65e581_Rx 229

# define ComConf_ComSignal_VCU_Kilometrage_oVCU_oE_CAN_90aff34b_Rx 230

# define ComConf_ComSignal_VCU_ModeEPSRequest_oVCU_BSI_Wakeup_oE_CAN_760ae031_Rx 231

# define ComConf_ComSignal_VCU_PosShuntJDD_oVCU_BSI_Wakeup_oE_CAN_0978ac9d_Rx 232

# define ComConf_ComSignal_VCU_PrecondElecWakeup_oVCU_BSI_Wakeup_oE_CAN_6616770e_Rx 233

# define ComConf_ComSignal_VCU_StopDelayedHMIWakeup_oVCU_BSI_Wakeup_oE_CAN_05bb1585_Rx 234

# define ComConf_ComSignal_VERSION_APPLI_oVERS_OBC_DCDC_oE_CAN_1774b9bd_Tx 235

# define ComConf_ComSignal_VERSION_SOFT_oVERS_OBC_DCDC_oE_CAN_dcc20268_Tx 236

# define ComConf_ComSignal_VERSION_SYSTEME_oVERS_OBC_DCDC_oE_CAN_7264d771_Tx 237

# define ComConf_ComSignal_VERS_DATE2_ANNEE_oVERS_OBC_DCDC_oE_CAN_258c1038_Tx 238

# define ComConf_ComSignal_VERS_DATE2_JOUR_oVERS_OBC_DCDC_oE_CAN_526ac526_Tx 239

# define ComConf_ComSignal_VERS_DATE2_MOIS_oVERS_OBC_DCDC_oE_CAN_d5ba8908_Tx 240


#endif /* _COM_CFG_H_ */
