/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApAEM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApAEM_Rte_Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(boolean *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(boolean *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpDiagToolsRequest_DeDiagToolsRequest(boolean *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(boolean *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(boolean *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data);
Std_ReturnType TSC_CtApAEM_Rte_Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApAEM_Rte_Write_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpCANComRequest_DeCANComRequest(sint32 data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(sint32 data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpFaultRCDLineSC_DeFaultRCDLineSC(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(boolean data);
Std_ReturnType TSC_CtApAEM_Rte_Write_PpShutdownAuthorization_DeShutdownAuthorization(boolean data);

/** SW-C local Calibration Parameters */
IdtDegMainWkuExtinctionTime  TSC_CtApAEM_Rte_CData_UCE_tiDegMainWkuDeac_C(void);
IdtTimeNomMainWkuDisord  TSC_CtApAEM_Rte_CData_UCE_tiMainDisrdDet_C(void);
IdtTimeNomMainWkuIncst  TSC_CtApAEM_Rte_CData_UCE_tiMainIncstDet_C(void);
IdtEnforcedMainWkuTime  TSC_CtApAEM_Rte_CData_UCE_tiMainTransForc_C(void);
IdtTimeNomMainWkuRehabilit  TSC_CtApAEM_Rte_CData_UCE_tiMainWkuReh_C(void);
IdtCOMLatchMaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiComLatch_C(void);
IdtInternalPartialWkuMaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiIntPtlWku_C(void);
IdtMasterPartialWkuY1MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C(void);
IdtMasterPartialWkuY2MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(void);
IdtMasterPartialWkuY3MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(void);
IdtMasterPartialWkuY4MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C(void);
IdtMasterPartialWkuY5MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(void);
IdtMasterPartialWkuY6MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(void);
IdtMasterPartialWkuY7MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(void);
IdtMasterPartialWkuY8MaxDuration  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(void);
IdtShutdownPreparationMaxTime  TSC_CtApAEM_Rte_CData_UCE_tiMaxTiShutDownPrep_C(void);
IdtMasterPartialWkuY1MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(void);
IdtMasterPartialWkuY2MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(void);
IdtMasterPartialWkuY3MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(void);
IdtMasterPartialWkuY4MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(void);
IdtMasterPartialWkuY5MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(void);
IdtMasterPartialWkuY6MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(void);
IdtMasterPartialWkuY7MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(void);
IdtMasterPartialWkuY8MinDuration  TSC_CtApAEM_Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(void);
IdtShutdownPreparationMinTime  TSC_CtApAEM_Rte_CData_UCE_tiMinTiShutDownPrep_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Lock_C(void);
IdtSlavePartialWkuXLockTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Lock_C(void);
IdtTimeRCDPulse  TSC_CtApAEM_Rte_CData_UCE_tiRCDLineCmdAcv_C(void);
IdtRCDLineGndSCConfirmTime  TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgDet_C(void);
IdtRCDLineGndSCRehabilitTime  TSC_CtApAEM_Rte_CData_UCE_tiRCDLineScgReh_C(void);
IdtExtinctionTime  TSC_CtApAEM_Rte_CData_UCE_tiTransitoryDeac_C(void);
IdtVehicleSpeedThreshold  TSC_CtApAEM_Rte_CData_UCE_spdThdDegDeac_C(void);
IdtVehicleSpeedDegThreshold  TSC_CtApAEM_Rte_CData_UCE_spdThdNomDeac_C(void);
IdtMainWkuValidTime  TSC_CtApAEM_Rte_CData_UCE_tiMainWkuAcv_C(void);
IdtMainWkuDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiNomMainWkuDeac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX10Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX11Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX12Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX13Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX14Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX15Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX16Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX1Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX2Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX3Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX4Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX5Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX6Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX7Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX8Deac_C(void);
IdtSlavePartialWkuXValidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Acv_C(void);
IdtSlavePartialWkuXDevalidTime  TSC_CtApAEM_Rte_CData_UCE_tiPtlWkuX9Deac_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX10_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX11_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX12_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX13_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX14_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX15_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX16_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX1_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX2_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX3_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX4_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX5_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX6_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX7_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX8_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuX9_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY1_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY2_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY3_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY4_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY5_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY6_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY7_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bInhPtlWkuY8_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(void);
boolean  TSC_CtApAEM_Rte_CData_UCE_noUCETyp_C(void);




