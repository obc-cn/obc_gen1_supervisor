/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLAD.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApLAD
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLAD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApLAD.h"
#include "TSC_CtApLAD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApLAD_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * IdtAfts_ElockActuatorTime1: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtAfts_ElockActuatorTime2: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtElockActuatorTime: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtElockFeedbackCurrent: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackLock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackUnlock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockThreshold: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 *   HW_EDITION_UNKNOWN (0U)
 *   HW_EDITION_D21 (1U)
 *   HW_EDITION_D31 (2U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimElockActuatorCounterOverCurrent(void)
 *   uint16 *Rte_Pim_PimElockActuatorHCounterOpenCircuit(void)
 *   uint16 *Rte_Pim_PimElockActuatorHCounterSCG(void)
 *   uint16 *Rte_Pim_PimElockActuatorHCounterSCP(void)
 *   uint16 *Rte_Pim_PimElockActuatorLCounterOpenCircuit(void)
 *   uint16 *Rte_Pim_PimElockActuatorLCounterSCG(void)
 *   uint16 *Rte_Pim_PimElockActuatorLCounterSCP(void)
 *   IdtOutputELockSensor *Rte_Pim_PimElockSensor_PreviousToLock(void)
 *   IdtOutputELockSensor *Rte_Pim_PimElockSensor_PreviousToUnlock(void)
 *   IdtELockSetPoint *Rte_Pim_PimElockSetPoint(void)
 *   boolean *Rte_Pim_PimElockActuatorHPrefaultOpenCircuit(void)
 *   boolean *Rte_Pim_PimElockActuatorHPrefaultSCG(void)
 *   boolean *Rte_Pim_PimElockActuatorHPrefaultSCP(void)
 *   boolean *Rte_Pim_PimElockActuatorLPrefaultOpenCircuit(void)
 *   boolean *Rte_Pim_PimElockActuatorLPrefaultSCG(void)
 *   boolean *Rte_Pim_PimElockActuatorLPrefaultSCP(void)
 *   boolean *Rte_Pim_PimElockActuatorPrefaultOverCurrent(void)
 *   boolean *Rte_Pim_PimElockNotCurrentMeasured(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtElockThreshold Rte_CData_CalElockThresholdNoCurrent(void)
 *   IdtElockThreshold Rte_CData_CalElockThresholdOvercurrent(void)
 *   IdtElockThreshold Rte_CData_CalElockThresholdSCG(void)
 *   IdtElockThreshold Rte_CData_CalElockThresholdSCP(void)
 *   IdtAfts_ElockActuatorTime1 Rte_CData_CalAfts_ElockActuatorTime1(void)
 *   IdtAfts_ElockActuatorTime2 Rte_CData_CalAfts_ElockActuatorTime2(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHOpenCircuitConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHOpenCircuitHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCGConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCGHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCPConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCPHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLOpenCircuitConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLOpenCircuitHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLOvercurrentConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCGConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCGHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCPConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCPHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockOvercurrentHealTime(void)
 *   boolean Rte_CData_CalEnableElockActuator(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtBatteryVoltMaxTest Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
 *   IdtBatteryVoltMinTest Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
 *   IdtVehStopMaxTest Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
 *
 *********************************************************************************************************************/


#define CtApLAD_START_SEC_CODE
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CtApLAD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CtApLAD_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLAD_CODE) CtApLAD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CtApLAD_init
 *********************************************************************************************************************/

  uint16 PimPimElockActuatorCounterOverCurrent;
  uint16 PimPimElockActuatorHCounterOpenCircuit;
  uint16 PimPimElockActuatorHCounterSCG;
  uint16 PimPimElockActuatorHCounterSCP;
  uint16 PimPimElockActuatorLCounterOpenCircuit;
  uint16 PimPimElockActuatorLCounterSCG;
  uint16 PimPimElockActuatorLCounterSCP;
  IdtOutputELockSensor PimPimElockSensor_PreviousToLock;
  IdtOutputELockSensor PimPimElockSensor_PreviousToUnlock;
  IdtELockSetPoint PimPimElockSetPoint;
  boolean PimPimElockActuatorHPrefaultOpenCircuit;
  boolean PimPimElockActuatorHPrefaultSCG;
  boolean PimPimElockActuatorHPrefaultSCP;
  boolean PimPimElockActuatorLPrefaultOpenCircuit;
  boolean PimPimElockActuatorLPrefaultSCG;
  boolean PimPimElockActuatorLPrefaultSCP;
  boolean PimPimElockActuatorPrefaultOverCurrent;
  boolean PimPimElockNotCurrentMeasured;

  IdtElockThreshold CalElockThresholdNoCurrent_data;
  IdtElockThreshold CalElockThresholdOvercurrent_data;
  IdtElockThreshold CalElockThresholdSCG_data;
  IdtElockThreshold CalElockThresholdSCP_data;
  IdtAfts_ElockActuatorTime1 CalAfts_ElockActuatorTime1_data;
  IdtAfts_ElockActuatorTime2 CalAfts_ElockActuatorTime2_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockLOvercurrentConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockLSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockOvercurrentHealTime_data;
  boolean CalEnableElockActuator_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockActuatorCounterOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent() = PimPimElockActuatorCounterOverCurrent;
  PimPimElockActuatorHCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit() = PimPimElockActuatorHCounterOpenCircuit;
  PimPimElockActuatorHCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG() = PimPimElockActuatorHCounterSCG;
  PimPimElockActuatorHCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP() = PimPimElockActuatorHCounterSCP;
  PimPimElockActuatorLCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit() = PimPimElockActuatorLCounterOpenCircuit;
  PimPimElockActuatorLCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG() = PimPimElockActuatorLCounterSCG;
  PimPimElockActuatorLCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP() = PimPimElockActuatorLCounterSCP;
  PimPimElockSensor_PreviousToLock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock() = PimPimElockSensor_PreviousToLock;
  PimPimElockSensor_PreviousToUnlock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock() = PimPimElockSensor_PreviousToUnlock;
  PimPimElockSetPoint = *TSC_CtApLAD_Rte_Pim_PimElockSetPoint();
  *TSC_CtApLAD_Rte_Pim_PimElockSetPoint() = PimPimElockSetPoint;
  PimPimElockActuatorHPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = PimPimElockActuatorHPrefaultOpenCircuit;
  PimPimElockActuatorHPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG() = PimPimElockActuatorHPrefaultSCG;
  PimPimElockActuatorHPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP() = PimPimElockActuatorHPrefaultSCP;
  PimPimElockActuatorLPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = PimPimElockActuatorLPrefaultOpenCircuit;
  PimPimElockActuatorLPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG() = PimPimElockActuatorLPrefaultSCG;
  PimPimElockActuatorLPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP() = PimPimElockActuatorLPrefaultSCP;
  PimPimElockActuatorPrefaultOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent() = PimPimElockActuatorPrefaultOverCurrent;
  PimPimElockNotCurrentMeasured = *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured();
  *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured() = PimPimElockNotCurrentMeasured;

  CalElockThresholdNoCurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdOvercurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCG_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCP_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime1_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime2_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOvercurrentConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockOvercurrentHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableElockActuator_data = TSC_CtApLAD_Rte_CData_CalEnableElockActuator(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApLAD_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLAD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(boolean *data)
 *   Std_ReturnType Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(boolean *data)
 *   Std_ReturnType Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent *data)
 *   Std_ReturnType Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock *data)
 *   Std_ReturnType Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockOvercurrent(boolean data)
 *   Std_ReturnType Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLAD_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLAD_CODE) RCtApLAD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLAD_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtBatteryVoltageState Read_PpBatteryVoltageState_DeBatteryVoltageState;
  boolean Read_PpELockSensorFaults_DeELockSensorFaultSCG;
  boolean Read_PpELockSensorFaults_DeELockSensorFaultSCP;
  IdtElockFeedbackCurrent Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent;
  IdtElockFeedbackLock Read_PpElockFeedbackLock_DeElockFeedbackLock;
  IdtElockFeedbackUnlock Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock;
  IdtELockSetPoint Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;
  IdtELockSetPoint Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode;
  IdtHWEditionDetected Read_PpHWEditionDetected_DeHWEditionDetected;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  VCU_EtatGmpHyb Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
  IdtOutputELockSensor Read_PpOutputELockSensor_DeOutputELockSensor;
  boolean Read_PpPlantModeState_DePlantModeState;

  uint16 PimPimElockActuatorCounterOverCurrent;
  uint16 PimPimElockActuatorHCounterOpenCircuit;
  uint16 PimPimElockActuatorHCounterSCG;
  uint16 PimPimElockActuatorHCounterSCP;
  uint16 PimPimElockActuatorLCounterOpenCircuit;
  uint16 PimPimElockActuatorLCounterSCG;
  uint16 PimPimElockActuatorLCounterSCP;
  IdtOutputELockSensor PimPimElockSensor_PreviousToLock;
  IdtOutputELockSensor PimPimElockSensor_PreviousToUnlock;
  IdtELockSetPoint PimPimElockSetPoint;
  boolean PimPimElockActuatorHPrefaultOpenCircuit;
  boolean PimPimElockActuatorHPrefaultSCG;
  boolean PimPimElockActuatorHPrefaultSCP;
  boolean PimPimElockActuatorLPrefaultOpenCircuit;
  boolean PimPimElockActuatorLPrefaultSCG;
  boolean PimPimElockActuatorLPrefaultSCP;
  boolean PimPimElockActuatorPrefaultOverCurrent;
  boolean PimPimElockNotCurrentMeasured;

  IdtElockThreshold CalElockThresholdNoCurrent_data;
  IdtElockThreshold CalElockThresholdOvercurrent_data;
  IdtElockThreshold CalElockThresholdSCG_data;
  IdtElockThreshold CalElockThresholdSCP_data;
  IdtAfts_ElockActuatorTime1 CalAfts_ElockActuatorTime1_data;
  IdtAfts_ElockActuatorTime2 CalAfts_ElockActuatorTime2_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockLOvercurrentConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockLSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockOvercurrentHealTime_data;
  boolean CalEnableElockActuator_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockActuatorCounterOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent() = PimPimElockActuatorCounterOverCurrent;
  PimPimElockActuatorHCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit() = PimPimElockActuatorHCounterOpenCircuit;
  PimPimElockActuatorHCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG() = PimPimElockActuatorHCounterSCG;
  PimPimElockActuatorHCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP() = PimPimElockActuatorHCounterSCP;
  PimPimElockActuatorLCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit() = PimPimElockActuatorLCounterOpenCircuit;
  PimPimElockActuatorLCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG() = PimPimElockActuatorLCounterSCG;
  PimPimElockActuatorLCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP() = PimPimElockActuatorLCounterSCP;
  PimPimElockSensor_PreviousToLock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock() = PimPimElockSensor_PreviousToLock;
  PimPimElockSensor_PreviousToUnlock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock() = PimPimElockSensor_PreviousToUnlock;
  PimPimElockSetPoint = *TSC_CtApLAD_Rte_Pim_PimElockSetPoint();
  *TSC_CtApLAD_Rte_Pim_PimElockSetPoint() = PimPimElockSetPoint;
  PimPimElockActuatorHPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = PimPimElockActuatorHPrefaultOpenCircuit;
  PimPimElockActuatorHPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG() = PimPimElockActuatorHPrefaultSCG;
  PimPimElockActuatorHPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP() = PimPimElockActuatorHPrefaultSCP;
  PimPimElockActuatorLPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = PimPimElockActuatorLPrefaultOpenCircuit;
  PimPimElockActuatorLPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG() = PimPimElockActuatorLPrefaultSCG;
  PimPimElockActuatorLPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP() = PimPimElockActuatorLPrefaultSCP;
  PimPimElockActuatorPrefaultOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent() = PimPimElockActuatorPrefaultOverCurrent;
  PimPimElockNotCurrentMeasured = *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured();
  *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured() = PimPimElockNotCurrentMeasured;

  CalElockThresholdNoCurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdOvercurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCG_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCP_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime1_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime2_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOvercurrentConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockOvercurrentHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableElockActuator_data = TSC_CtApLAD_Rte_CData_CalEnableElockActuator(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLAD_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&Read_PpBatteryVoltageState_DeBatteryVoltageState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(&Read_PpELockSensorFaults_DeELockSensorFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(&Read_PpELockSensorFaults_DeELockSensorFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(&Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(&Read_PpElockFeedbackLock_DeElockFeedbackLock); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(&Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(&Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(&Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpHWEditionDetected_DeHWEditionDetected(&Read_PpHWEditionDetected_DeHWEditionDetected); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&Read_PpOutputELockSensor_DeOutputELockSensor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Read_PpPlantModeState_DePlantModeState(&Read_PpPlantModeState_DePlantModeState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(Rte_InitValue_PpELockFaults_DeOutputElockHFaultOpenCircuit); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(Rte_InitValue_PpELockFaults_DeOutputElockHFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(Rte_InitValue_PpELockFaults_DeOutputElockHFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(Rte_InitValue_PpELockFaults_DeOutputElockLFaultOpenCircuit); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(Rte_InitValue_PpELockFaults_DeOutputElockLFaultSCG); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(Rte_InitValue_PpELockFaults_DeOutputElockLFaultSCP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpELockFaults_DeOutputElockOvercurrent(Rte_InitValue_PpELockFaults_DeOutputElockOvercurrent); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(Rte_InitValue_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(Rte_InitValue_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(Rte_InitValue_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLAD_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_recharging_plug_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimElockActuatorCounterOverCurrent;
  uint16 PimPimElockActuatorHCounterOpenCircuit;
  uint16 PimPimElockActuatorHCounterSCG;
  uint16 PimPimElockActuatorHCounterSCP;
  uint16 PimPimElockActuatorLCounterOpenCircuit;
  uint16 PimPimElockActuatorLCounterSCG;
  uint16 PimPimElockActuatorLCounterSCP;
  IdtOutputELockSensor PimPimElockSensor_PreviousToLock;
  IdtOutputELockSensor PimPimElockSensor_PreviousToUnlock;
  IdtELockSetPoint PimPimElockSetPoint;
  boolean PimPimElockActuatorHPrefaultOpenCircuit;
  boolean PimPimElockActuatorHPrefaultSCG;
  boolean PimPimElockActuatorHPrefaultSCP;
  boolean PimPimElockActuatorLPrefaultOpenCircuit;
  boolean PimPimElockActuatorLPrefaultSCG;
  boolean PimPimElockActuatorLPrefaultSCP;
  boolean PimPimElockActuatorPrefaultOverCurrent;
  boolean PimPimElockNotCurrentMeasured;

  IdtElockThreshold CalElockThresholdNoCurrent_data;
  IdtElockThreshold CalElockThresholdOvercurrent_data;
  IdtElockThreshold CalElockThresholdSCG_data;
  IdtElockThreshold CalElockThresholdSCP_data;
  IdtAfts_ElockActuatorTime1 CalAfts_ElockActuatorTime1_data;
  IdtAfts_ElockActuatorTime2 CalAfts_ElockActuatorTime2_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockLOvercurrentConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockLSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockOvercurrentHealTime_data;
  boolean CalEnableElockActuator_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockActuatorCounterOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent() = PimPimElockActuatorCounterOverCurrent;
  PimPimElockActuatorHCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit() = PimPimElockActuatorHCounterOpenCircuit;
  PimPimElockActuatorHCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG() = PimPimElockActuatorHCounterSCG;
  PimPimElockActuatorHCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP() = PimPimElockActuatorHCounterSCP;
  PimPimElockActuatorLCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit() = PimPimElockActuatorLCounterOpenCircuit;
  PimPimElockActuatorLCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG() = PimPimElockActuatorLCounterSCG;
  PimPimElockActuatorLCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP() = PimPimElockActuatorLCounterSCP;
  PimPimElockSensor_PreviousToLock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock() = PimPimElockSensor_PreviousToLock;
  PimPimElockSensor_PreviousToUnlock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock() = PimPimElockSensor_PreviousToUnlock;
  PimPimElockSetPoint = *TSC_CtApLAD_Rte_Pim_PimElockSetPoint();
  *TSC_CtApLAD_Rte_Pim_PimElockSetPoint() = PimPimElockSetPoint;
  PimPimElockActuatorHPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = PimPimElockActuatorHPrefaultOpenCircuit;
  PimPimElockActuatorHPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG() = PimPimElockActuatorHPrefaultSCG;
  PimPimElockActuatorHPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP() = PimPimElockActuatorHPrefaultSCP;
  PimPimElockActuatorLPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = PimPimElockActuatorLPrefaultOpenCircuit;
  PimPimElockActuatorLPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG() = PimPimElockActuatorLPrefaultSCG;
  PimPimElockActuatorLPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP() = PimPimElockActuatorLPrefaultSCP;
  PimPimElockActuatorPrefaultOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent() = PimPimElockActuatorPrefaultOverCurrent;
  PimPimElockNotCurrentMeasured = *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured();
  *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured() = PimPimElockNotCurrentMeasured;

  CalElockThresholdNoCurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdOvercurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCG_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCP_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime1_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime2_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOvercurrentConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockOvercurrentHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableElockActuator_data = TSC_CtApLAD_Rte_CData_CalEnableElockActuator(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Actuator_test_on_recharging_plug_lock_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_recharging_plug_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Start (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimElockActuatorCounterOverCurrent;
  uint16 PimPimElockActuatorHCounterOpenCircuit;
  uint16 PimPimElockActuatorHCounterSCG;
  uint16 PimPimElockActuatorHCounterSCP;
  uint16 PimPimElockActuatorLCounterOpenCircuit;
  uint16 PimPimElockActuatorLCounterSCG;
  uint16 PimPimElockActuatorLCounterSCP;
  IdtOutputELockSensor PimPimElockSensor_PreviousToLock;
  IdtOutputELockSensor PimPimElockSensor_PreviousToUnlock;
  IdtELockSetPoint PimPimElockSetPoint;
  boolean PimPimElockActuatorHPrefaultOpenCircuit;
  boolean PimPimElockActuatorHPrefaultSCG;
  boolean PimPimElockActuatorHPrefaultSCP;
  boolean PimPimElockActuatorLPrefaultOpenCircuit;
  boolean PimPimElockActuatorLPrefaultSCG;
  boolean PimPimElockActuatorLPrefaultSCP;
  boolean PimPimElockActuatorPrefaultOverCurrent;
  boolean PimPimElockNotCurrentMeasured;

  IdtElockThreshold CalElockThresholdNoCurrent_data;
  IdtElockThreshold CalElockThresholdOvercurrent_data;
  IdtElockThreshold CalElockThresholdSCG_data;
  IdtElockThreshold CalElockThresholdSCP_data;
  IdtAfts_ElockActuatorTime1 CalAfts_ElockActuatorTime1_data;
  IdtAfts_ElockActuatorTime2 CalAfts_ElockActuatorTime2_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockLOvercurrentConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockLSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockOvercurrentHealTime_data;
  boolean CalEnableElockActuator_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockActuatorCounterOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent() = PimPimElockActuatorCounterOverCurrent;
  PimPimElockActuatorHCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit() = PimPimElockActuatorHCounterOpenCircuit;
  PimPimElockActuatorHCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG() = PimPimElockActuatorHCounterSCG;
  PimPimElockActuatorHCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP() = PimPimElockActuatorHCounterSCP;
  PimPimElockActuatorLCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit() = PimPimElockActuatorLCounterOpenCircuit;
  PimPimElockActuatorLCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG() = PimPimElockActuatorLCounterSCG;
  PimPimElockActuatorLCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP() = PimPimElockActuatorLCounterSCP;
  PimPimElockSensor_PreviousToLock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock() = PimPimElockSensor_PreviousToLock;
  PimPimElockSensor_PreviousToUnlock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock() = PimPimElockSensor_PreviousToUnlock;
  PimPimElockSetPoint = *TSC_CtApLAD_Rte_Pim_PimElockSetPoint();
  *TSC_CtApLAD_Rte_Pim_PimElockSetPoint() = PimPimElockSetPoint;
  PimPimElockActuatorHPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = PimPimElockActuatorHPrefaultOpenCircuit;
  PimPimElockActuatorHPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG() = PimPimElockActuatorHPrefaultSCG;
  PimPimElockActuatorHPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP() = PimPimElockActuatorHPrefaultSCP;
  PimPimElockActuatorLPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = PimPimElockActuatorLPrefaultOpenCircuit;
  PimPimElockActuatorLPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG() = PimPimElockActuatorLPrefaultSCG;
  PimPimElockActuatorLPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP() = PimPimElockActuatorLPrefaultSCP;
  PimPimElockActuatorPrefaultOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent() = PimPimElockActuatorPrefaultOverCurrent;
  PimPimElockNotCurrentMeasured = *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured();
  *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured() = PimPimElockNotCurrentMeasured;

  CalElockThresholdNoCurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdOvercurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCG_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCP_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime1_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime2_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOvercurrentConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockOvercurrentHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableElockActuator_data = TSC_CtApLAD_Rte_CData_CalEnableElockActuator(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Actuator_test_on_recharging_plug_lock_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_recharging_plug_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Stop (returns application error)
 *********************************************************************************************************************/

  uint16 PimPimElockActuatorCounterOverCurrent;
  uint16 PimPimElockActuatorHCounterOpenCircuit;
  uint16 PimPimElockActuatorHCounterSCG;
  uint16 PimPimElockActuatorHCounterSCP;
  uint16 PimPimElockActuatorLCounterOpenCircuit;
  uint16 PimPimElockActuatorLCounterSCG;
  uint16 PimPimElockActuatorLCounterSCP;
  IdtOutputELockSensor PimPimElockSensor_PreviousToLock;
  IdtOutputELockSensor PimPimElockSensor_PreviousToUnlock;
  IdtELockSetPoint PimPimElockSetPoint;
  boolean PimPimElockActuatorHPrefaultOpenCircuit;
  boolean PimPimElockActuatorHPrefaultSCG;
  boolean PimPimElockActuatorHPrefaultSCP;
  boolean PimPimElockActuatorLPrefaultOpenCircuit;
  boolean PimPimElockActuatorLPrefaultSCG;
  boolean PimPimElockActuatorLPrefaultSCP;
  boolean PimPimElockActuatorPrefaultOverCurrent;
  boolean PimPimElockNotCurrentMeasured;

  IdtElockThreshold CalElockThresholdNoCurrent_data;
  IdtElockThreshold CalElockThresholdOvercurrent_data;
  IdtElockThreshold CalElockThresholdSCG_data;
  IdtElockThreshold CalElockThresholdSCP_data;
  IdtAfts_ElockActuatorTime1 CalAfts_ElockActuatorTime1_data;
  IdtAfts_ElockActuatorTime2 CalAfts_ElockActuatorTime2_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockHSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockHSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLOpenCircuitHealTime_data;
  IdtElockActuatorTime CalOutputElockLOvercurrentConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCGHealTime_data;
  IdtElockActuatorTime CalOutputElockLSCPConfirmTime_data;
  IdtElockActuatorTime CalOutputElockLSCPHealTime_data;
  IdtElockActuatorTime CalOutputElockOvercurrentHealTime_data;
  boolean CalEnableElockActuator_data;

  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest_data;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimElockActuatorCounterOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorCounterOverCurrent() = PimPimElockActuatorCounterOverCurrent;
  PimPimElockActuatorHCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterOpenCircuit() = PimPimElockActuatorHCounterOpenCircuit;
  PimPimElockActuatorHCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCG() = PimPimElockActuatorHCounterSCG;
  PimPimElockActuatorHCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHCounterSCP() = PimPimElockActuatorHCounterSCP;
  PimPimElockActuatorLCounterOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterOpenCircuit() = PimPimElockActuatorLCounterOpenCircuit;
  PimPimElockActuatorLCounterSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCG() = PimPimElockActuatorLCounterSCG;
  PimPimElockActuatorLCounterSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLCounterSCP() = PimPimElockActuatorLCounterSCP;
  PimPimElockSensor_PreviousToLock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToLock() = PimPimElockSensor_PreviousToLock;
  PimPimElockSensor_PreviousToUnlock = *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock();
  *TSC_CtApLAD_Rte_Pim_PimElockSensor_PreviousToUnlock() = PimPimElockSensor_PreviousToUnlock;
  PimPimElockSetPoint = *TSC_CtApLAD_Rte_Pim_PimElockSetPoint();
  *TSC_CtApLAD_Rte_Pim_PimElockSetPoint() = PimPimElockSetPoint;
  PimPimElockActuatorHPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = PimPimElockActuatorHPrefaultOpenCircuit;
  PimPimElockActuatorHPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCG() = PimPimElockActuatorHPrefaultSCG;
  PimPimElockActuatorHPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorHPrefaultSCP() = PimPimElockActuatorHPrefaultSCP;
  PimPimElockActuatorLPrefaultOpenCircuit = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = PimPimElockActuatorLPrefaultOpenCircuit;
  PimPimElockActuatorLPrefaultSCG = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCG() = PimPimElockActuatorLPrefaultSCG;
  PimPimElockActuatorLPrefaultSCP = *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorLPrefaultSCP() = PimPimElockActuatorLPrefaultSCP;
  PimPimElockActuatorPrefaultOverCurrent = *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent();
  *TSC_CtApLAD_Rte_Pim_PimElockActuatorPrefaultOverCurrent() = PimPimElockActuatorPrefaultOverCurrent;
  PimPimElockNotCurrentMeasured = *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured();
  *TSC_CtApLAD_Rte_Pim_PimElockNotCurrentMeasured() = PimPimElockNotCurrentMeasured;

  CalElockThresholdNoCurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdNoCurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdOvercurrent_data = TSC_CtApLAD_Rte_CData_CalElockThresholdOvercurrent(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCG_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCG(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalElockThresholdSCP_data = TSC_CtApLAD_Rte_CData_CalElockThresholdSCP(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime1_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime1(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalAfts_ElockActuatorTime2_data = TSC_CtApLAD_Rte_CData_CalAfts_ElockActuatorTime2(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockHSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockHSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOpenCircuitHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOpenCircuitHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLOvercurrentConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLOvercurrentConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCGHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCGHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPConfirmTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockLSCPHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockLSCPHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalOutputElockOvercurrentHealTime_data = TSC_CtApLAD_Rte_CData_CalOutputElockOvercurrentHealTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalEnableElockActuator_data = TSC_CtApLAD_Rte_CData_CalEnableElockActuator(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpBatteryVoltMaxTest_DeBatteryVoltMaxTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpBatteryVoltMinTest_DeBatteryVoltMinTest_data = TSC_CtApLAD_Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpVehStopMaxTest_DeVehStopMaxTest_data = TSC_CtApLAD_Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLAD_STOP_SEC_CODE
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApLAD_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_1 = BAT_VALID_RANGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_2 = BAT_OVERVOLTAGE;
  IdtBatteryVoltageState Test_IdtBatteryVoltageState_V_3 = BAT_UNDERVOLTAGE;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtELockSetPoint Test_IdtELockSetPoint_V_1 = ELOCK_SETPOINT_UNLOCKED;
  IdtELockSetPoint Test_IdtELockSetPoint_V_2 = ELOCK_SETPOINT_LOCKED;

  IdtHWEditionDetected Test_IdtHWEditionDetected_V_1 = HW_EDITION_UNKNOWN;
  IdtHWEditionDetected Test_IdtHWEditionDetected_V_2 = HW_EDITION_D21;
  IdtHWEditionDetected Test_IdtHWEditionDetected_V_3 = HW_EDITION_D31;

  IdtOutputELockSensor Test_IdtOutputELockSensor_V_1 = ELOCK_LOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_2 = ELOCK_UNLOCKED;
  IdtOutputELockSensor Test_IdtOutputELockSensor_V_3 = ELOCK_DRIVE_UNDEFINED;

  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_1 = Cx0_PWT_inactive;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_2 = Cx1_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_3 = Cx2_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_4 = Cx3_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_5 = Cx4_PWT_activation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_6 = Cx5_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_7 = Cx6_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_8 = Cx7_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_9 = Cx8_active_PWT;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_10 = Cx9_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_11 = CxA_PWT_desactivation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_12 = CxB_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_13 = CxC_PWT_in_hold_after_desactivation;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_14 = CxD_reserved;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_15 = CxE_PWT_at_rest;
  VCU_EtatGmpHyb Test_VCU_EtatGmpHyb_V_16 = CxF_reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
