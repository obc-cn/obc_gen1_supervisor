/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLFM.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApLFM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLFM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApLFM.h"
#include "TSC_CtApLFM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApLFM_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCDC_HighVoltConnectionAllowed: Boolean
 * DCDC_OutputVoltage: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_OC_A_HV_FAULT: Boolean
 * DCLV_OC_A_LV_FAULT: Boolean
 * DCLV_OC_B_HV_FAULT: Boolean
 * DCLV_OC_B_LV_FAULT: Boolean
 * DCLV_OT_FAULT: Boolean
 * DCLV_OV_INT_A_FAULT: Boolean
 * DCLV_OV_INT_B_FAULT: Boolean
 * DCLV_OV_UV_HV_FAULT: Boolean
 * DCLV_PWM_STOP: Boolean
 * IdtABSVehSpdThresholdOV: Integer in interval [0...100]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit1_ConfirmTime: Integer in interval [0...100]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit1_Threshold: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit2_ConfirmTime: Integer in interval [0...100]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit2_Threshold: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryShortCircuit: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLV_MaxNumberRetries: Integer in interval [0...60]
 * IdtDebounceOvercurrentDCHV: Integer in interval [0...10]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtThresholdLVOvercurrentHW: Integer in interval [0...400]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtTimeBatteryVoltageSafety: Integer in interval [0...255]
 *   Unit: [s], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_RCD (0U)
 *   Cx1_No_main_wake_up_request (1U)
 *   Cx2_Main_wake_up_request (2U)
 *   Cx3_Not_valid (3U)
 * DCDC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   DCDC_FAULT_NO_FAULT (0U)
 *   DCDC_FAULT_LEVEL_1 (64U)
 *   DCDC_FAULT_LEVEL_2 (128U)
 *   DCDC_FAULT_LEVEL_3 (192U)
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 *   FAULT_LEVEL_NO_FAULT (0U)
 *   FAULT_LEVEL_1 (1U)
 *   FAULT_LEVEL_2 (2U)
 *   FAULT_LEVEL_3 (3U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 *
 * Array Types:
 * ============
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtFaultLevel *Rte_Pim_PimDCLVHWFault_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_FaultSatellite_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_InternalCom_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputOvercurrent_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputShorcircuitHV_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputShortCircuitLV_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputShortCircuit_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OvervoltageHV_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OvervoltageLimit1_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OvervoltageLimit2_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_Overvoltage_Error(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtThresholdLVOvercurrentHW Rte_CData_CalThresholdLVOvercurrentHW(void)
 *   IdtABSVehSpdThresholdOV Rte_CData_CalABSVehSpdThresholdOV(void)
 *   IdtBatteryVoltageSafetyOVLimit1_ConfirmTime Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(void)
 *   IdtBatteryVoltageSafetyOVLimit1_Threshold Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(void)
 *   IdtBatteryVoltageSafetyOVLimit2_ConfirmTime Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(void)
 *   IdtBatteryVoltageSafetyOVLimit2_Threshold Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(void)
 *   IdtDCLV_MaxNumberRetries Rte_CData_CalDCLV_MaxNumberRetries(void)
 *   IdtTimeBatteryVoltageSafety Rte_CData_CalTimeBatteryVoltageSafety(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtDCLVFaultTimeToRetryDefault Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void)
 *   IdtDCLVFaultTimeToRetryShortCircuit Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void)
 *   IdtDebounceOvercurrentDCHV Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void)
 *
 *********************************************************************************************************************/


#define CtApLFM_START_SEC_CODE
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_LFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApLFM_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  return RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_LFM_HW_faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_LFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLFMGetDCLVHWFaults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpLFMGetDCLVHWFaults> of PortPrototype <PpLFMGetDCLVHWFaults>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RCtApLFMGetDCLVHWFaults(uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFMGetDCLVHWFaults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLFM_CODE) RCtApLFMGetDCLVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFMGetDCLVHWFaults
 *********************************************************************************************************************/

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLFM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLFM_CODE) RCtApLFM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_init
 *********************************************************************************************************************/

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLFM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCLVError_DeDCLVError(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(DCDC_Fault data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLFM_CODE) RCtApLFM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtBatteryVolt Read_PpBatteryVolt_DeBatteryVolt;
  IdtPOST_Result Read_PpDCDC_POST_Result_DeDCDC_POST_Result;
  boolean Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error;
  IdtFaultLevel Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error;
  IdtFaultLevel Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error;
  IdtFaultLevel Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error;
  boolean Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue;
  ABS_VehSpd Read_PpInt_ABS_VehSpd_ABS_VehSpd;
  BSI_MainWakeup Read_PpInt_BSI_MainWakeup_BSI_MainWakeup;
  DCDC_CurrentReference Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference;
  DCDC_HighVoltConnectionAllowed Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed;
  DCDC_Status Read_PpInt_DCDC_Status_Delayed_DCDC_Status;
  DCHV_ADC_IOUT Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
  DCHV_ADC_VOUT Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
  DCHV_Command_Current_Reference Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
  DCHV_DCHVStatus Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
  DCLV_DCLVStatus Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
  DCLV_Input_Voltage Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
  DCLV_Measured_Current Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
  DCLV_OC_A_HV_FAULT Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT;
  DCLV_OC_A_LV_FAULT Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT;
  DCLV_OC_B_HV_FAULT Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT;
  DCLV_OC_B_LV_FAULT Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT;
  DCLV_OT_FAULT Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT;
  DCLV_OV_INT_A_FAULT Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT;
  DCLV_OV_INT_B_FAULT Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT;
  DCLV_OV_UV_HV_FAULT Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT;
  DCLV_PWM_STOP Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  OBC_Status Read_PpInt_OBC_Status_Delayed_OBC_Status;
  IdtPOST_Result Read_PpOBC_POST_Result_DeOBC_POST_Result;
  boolean Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed;

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLFM_Rte_Read_PpBatteryVolt_DeBatteryVolt(&Read_PpBatteryVolt_DeBatteryVolt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&Read_PpDCDC_POST_Result_DeDCDC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(&Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(&Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(&Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(&Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(&Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&Read_PpInt_ABS_VehSpd_ABS_VehSpd); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&Read_PpInt_BSI_MainWakeup_BSI_MainWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(&Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(&Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&Read_PpInt_DCDC_Status_Delayed_DCDC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(&Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(&Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(&Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(&Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(&Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(&Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(&Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(&Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(&Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(&Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(&Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&Read_PpInt_OBC_Status_Delayed_OBC_Status); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&Read_PpOBC_POST_Result_DeOBC_POST_Result); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(&Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(Rte_InitValue_PpDCDCFaultsList_DeDCDC_OvertemperatureFault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(Rte_InitValue_PpDCDCFaultsList_DeDCLV_HWErrors_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(Rte_InitValue_PpDCDCFaultsList_DeDCLV_InternalCom_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(Rte_InitValue_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpDCLVError_DeDCLVError(Rte_InitValue_PpDCLVError_DeDCLVError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(Rte_InitValue_PpInt_DCDC_Fault_DCDC_Fault); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(Rte_InitValue_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLFM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_LFM_Faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_LFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_LFM_Faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_LFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtFaultLevel PimPimDCLVHWFault_Error;
  IdtFaultLevel PimPimDCLV_FaultSatellite_Error;
  IdtFaultLevel PimPimDCLV_InternalCom_Error;
  IdtFaultLevel PimPimDCLV_OutputOvercurrent_Error;
  IdtFaultLevel PimPimDCLV_OutputShorcircuitHV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuitLV_Error;
  IdtFaultLevel PimPimDCLV_OutputShortCircuit_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageHV_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit1_Error;
  IdtFaultLevel PimPimDCLV_OvervoltageLimit2_Error;
  IdtFaultLevel PimPimDCLV_Overvoltage_Error;

  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW_data;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV_data;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold_data;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold_data;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries_data;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety_data;

  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimDCLVHWFault_Error = *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLVHWFault_Error() = PimPimDCLVHWFault_Error;
  PimPimDCLV_FaultSatellite_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_FaultSatellite_Error() = PimPimDCLV_FaultSatellite_Error;
  PimPimDCLV_InternalCom_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_InternalCom_Error() = PimPimDCLV_InternalCom_Error;
  PimPimDCLV_OutputOvercurrent_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputOvercurrent_Error() = PimPimDCLV_OutputOvercurrent_Error;
  PimPimDCLV_OutputShorcircuitHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() = PimPimDCLV_OutputShorcircuitHV_Error;
  PimPimDCLV_OutputShortCircuitLV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() = PimPimDCLV_OutputShortCircuitLV_Error;
  PimPimDCLV_OutputShortCircuit_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OutputShortCircuit_Error() = PimPimDCLV_OutputShortCircuit_Error;
  PimPimDCLV_OvervoltageHV_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageHV_Error() = PimPimDCLV_OvervoltageHV_Error;
  PimPimDCLV_OvervoltageLimit1_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit1_Error() = PimPimDCLV_OvervoltageLimit1_Error;
  PimPimDCLV_OvervoltageLimit2_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_OvervoltageLimit2_Error() = PimPimDCLV_OvervoltageLimit2_Error;
  PimPimDCLV_Overvoltage_Error = *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error();
  *TSC_CtApLFM_Rte_Pim_PimDCLV_Overvoltage_Error() = PimPimDCLV_Overvoltage_Error;

  CalThresholdLVOvercurrentHW_data = TSC_CtApLFM_Rte_CData_CalThresholdLVOvercurrentHW(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalABSVehSpdThresholdOV_data = TSC_CtApLFM_Rte_CData_CalABSVehSpdThresholdOV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit1_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_ConfirmTime_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalBatteryVoltageSafetyOVLimit2_Threshold_data = TSC_CtApLFM_Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDCLV_MaxNumberRetries_data = TSC_CtApLFM_Rte_CData_CalDCLV_MaxNumberRetries(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeBatteryVoltageSafety_data = TSC_CtApLFM_Rte_CData_CalTimeBatteryVoltageSafety(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit_data = TSC_CtApLFM_Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV_data = TSC_CtApLFM_Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLFM_STOP_SEC_CODE
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApLFM_TestDefines(void)
{
  /* Enumeration Data Types */

  BSI_MainWakeup Test_BSI_MainWakeup_V_1 = Cx0_Invalid_RCD;
  BSI_MainWakeup Test_BSI_MainWakeup_V_2 = Cx1_No_main_wake_up_request;
  BSI_MainWakeup Test_BSI_MainWakeup_V_3 = Cx2_Main_wake_up_request;
  BSI_MainWakeup Test_BSI_MainWakeup_V_4 = Cx3_Not_valid;

  DCDC_Fault Test_DCDC_Fault_V_1 = DCDC_FAULT_NO_FAULT;
  DCDC_Fault Test_DCDC_Fault_V_2 = DCDC_FAULT_LEVEL_1;
  DCDC_Fault Test_DCDC_Fault_V_3 = DCDC_FAULT_LEVEL_2;
  DCDC_Fault Test_DCDC_Fault_V_4 = DCDC_FAULT_LEVEL_3;

  DCDC_Status Test_DCDC_Status_V_1 = Cx0_off_mode;
  DCDC_Status Test_DCDC_Status_V_2 = Cx1_Init_mode;
  DCDC_Status Test_DCDC_Status_V_3 = Cx2_standby_mode;
  DCDC_Status Test_DCDC_Status_V_4 = Cx3_conversion_working_;
  DCDC_Status Test_DCDC_Status_V_5 = Cx4_error_mode;
  DCDC_Status Test_DCDC_Status_V_6 = Cx5_degradation_mode;
  DCDC_Status Test_DCDC_Status_V_7 = Cx6_reserved;
  DCDC_Status Test_DCDC_Status_V_8 = Cx7_invalid;

  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_1 = Cx0_DCHV_STATUS_STANDBY;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_2 = Cx1_DCHV_STATUS_RUN;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_3 = Cx2_DCHV_STATUS_ERROR;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_4 = Cx3_DCHV_STATUS_ALARM;
  DCHV_DCHVStatus Test_DCHV_DCHVStatus_V_5 = Cx4_DCHV_STATUS_SAFE;

  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_1 = Cx0_STANDBY;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_2 = Cx1_RUN;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_3 = Cx2_ERROR;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_4 = Cx3_ALARM;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_5 = Cx4_SAFE;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_6 = Cx5_DERATED;
  DCLV_DCLVStatus Test_DCLV_DCLVStatus_V_7 = Cx6_SHUTDOWN;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtFaultLevel Test_IdtFaultLevel_V_1 = FAULT_LEVEL_NO_FAULT;
  IdtFaultLevel Test_IdtFaultLevel_V_2 = FAULT_LEVEL_1;
  IdtFaultLevel Test_IdtFaultLevel_V_3 = FAULT_LEVEL_2;
  IdtFaultLevel Test_IdtFaultLevel_V_4 = FAULT_LEVEL_3;

  IdtPOST_Result Test_IdtPOST_Result_V_1 = POST_ONGOING;
  IdtPOST_Result Test_IdtPOST_Result_V_2 = POST_OK;
  IdtPOST_Result Test_IdtPOST_Result_V_3 = POST_NOK;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  OBC_Status Test_OBC_Status_V_1 = Cx0_off_mode;
  OBC_Status Test_OBC_Status_V_2 = Cx1_Init_mode;
  OBC_Status Test_OBC_Status_V_3 = Cx2_standby_mode;
  OBC_Status Test_OBC_Status_V_4 = Cx3_conversion_working_;
  OBC_Status Test_OBC_Status_V_5 = Cx4_error_mode;
  OBC_Status Test_OBC_Status_V_6 = Cx5_degradation_mode;
  OBC_Status Test_OBC_Status_V_7 = Cx6_reserved;
  OBC_Status Test_OBC_Status_V_8 = Cx7_invalid;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
