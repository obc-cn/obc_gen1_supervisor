/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApMSC.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApMSC
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApMSC>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 * NvM_BlockIdType
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApMSC.h"
#include "TSC_CtApMSC.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApMSC_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * BMS_OnBoardChargerEnable: Boolean
 * DCDC_OBCDCDCRTPowerLoad: Integer in interval [0...255]
 * DCLV_Applied_Derating_Factor: Integer in interval [0...2047]
 * DCLV_Power: Integer in interval [0...2500]
 * IdtEVSEMaximumPowerLimit: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtInputVoltageThreshold: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMaxInputACCurrentEVSE: Integer in interval [0...65535]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * IdtOBC_PowerAvailableControlPilot: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBC_PowerAvailableHardware: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBC_PowerMaxCalculated: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * IdtOBC_PowerMaxValue: Integer in interval [0...25]
 *   Unit: [W], Factor: 1000, Offset: 0
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtPDERATING_OBC: Integer in interval [0...1024]
 * IdtTimeLockDelay: Integer in interval [0...100]
 *   Unit: [ms], Factor: 100, Offset: 0
 * NvM_BlockIdType: Integer in interval [1...32767]
 * OBC_OBCStartSt: Boolean
 * OBC_PowerMax: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * OBC_SocketTempL: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * OBC_SocketTempN: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * VCU_DiagMuxOnPwt: Boolean
 * boolean: Boolean (standard type)
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_MainConnectorState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_contactors_opened (0U)
 *   Cx1_precharge (1U)
 *   Cx2_contactors_closed (2U)
 *   Cx3_Invalid (3U)
 * BSI_LockedVehicle: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_Locked (0U)
 *   Cx1_Locked (1U)
 *   Cx2_Overlocked (2U)
 *   Cx3_Reserved (3U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtOBC_PowerAvailableControlPilot *Rte_Pim_PimOBC_PowerAvailableControlPilot(void)
 *   IdtOBC_PowerAvailableHardware *Rte_Pim_PimOBC_PowerAvailableHardware(void)
 *   uint8 *Rte_Pim_PimMSC_PowerLatchFlag(void)
 *   IdtOBC_PowerMaxCalculated *Rte_Pim_PimOBC_PowerMaxCalculated(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtOBC_PowerMaxValue Rte_CData_CalOBC_PowerMaxValue(void)
 *   IdtTimeLockDelay Rte_CData_CalTimeLockDelay(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtInputVoltageThreshold Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(void)
 *
 *********************************************************************************************************************/


#define CtApMSC_START_SEC_CODE
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApMSC_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  return RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData (returns application error)
 *********************************************************************************************************************/

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApMSC_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApMSC_CODE) RCtApMSC_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_init
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApMSC_Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApMSC_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApMSC_CODE) RCtApMSC_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC1Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC2Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempAC3Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempACNMeas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempDC1Meas;
  IdtOutputTempMeas Read_PpOutputTempMeas_DeOutputTempDC2Meas;

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&Read_PpOutputTempMeas_DeOutputTempAC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(&Read_PpOutputTempMeas_DeOutputTempAC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(&Read_PpOutputTempMeas_DeOutputTempAC3Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(&Read_PpOutputTempMeas_DeOutputTempACNMeas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(&Read_PpOutputTempMeas_DeOutputTempDC1Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(&Read_PpOutputTempMeas_DeOutputTempDC2Meas); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL(Rte_InitValue_PpInt_OBC_SocketTemp_OBC_SocketTempL); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN(Rte_InitValue_PpInt_OBC_SocketTemp_OBC_SocketTempN); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApMSC_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit *data)
 *   Std_ReturnType Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Power_DCLV_Power(DCLV_Power *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data)
 *   Std_ReturnType Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApMSC_CODE) RCtApMSC_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  boolean Read_PpActiveDischargeRequest_DeActiveDischargeRequest;
  IdtEVSEMaximumPowerLimit Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit;
  boolean Read_PpForceElockCloseMode4_DeForceElockCloseMode4;
  IdtInputVoltageMode Read_PpInputVoltageMode_DeInputVoltageMode;
  BMS_DCRelayVoltage Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
  BMS_MainConnectorState Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState;
  BMS_OnBoardChargerEnable Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable;
  BSI_LockedVehicle Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle;
  DCLV_Applied_Derating_Factor Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor;
  DCLV_Power Read_PpInt_DCLV_Power_DCLV_Power;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  OBC_ChargingMode Read_PpInt_OBC_ChargingMode_OBC_ChargingMode;
  OBC_InputVoltageSt Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
  OBC_OBCStartSt Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt;
  PFC_VPH12_RMS_V Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
  IdtMaxInputACCurrentEVSE Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE;
  IdtPDERATING_OBC Read_PpPDERATING_OBC_DePDERATING_OBC;

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApMSC_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(&Read_PpActiveDischargeRequest_DeActiveDischargeRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(&Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(&Read_PpForceElockCloseMode4_DeForceElockCloseMode4); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&Read_PpInputVoltageMode_DeInputVoltageMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(&Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(&Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(&Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(&Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(&Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_DCLV_Power_DCLV_Power(&Read_PpInt_DCLV_Power_DCLV_Power); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&Read_PpInt_OBC_ChargingMode_OBC_ChargingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(&Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(&Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(&Read_PpPDERATING_OBC_DePDERATING_OBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(Rte_InitValue_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(Rte_InitValue_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(Rte_InitValue_PpInt_OBC_PowerMax_OBC_PowerMax); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Powerlatch_Information_Positioning_PIP>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_RoutineStatusRecord_01_02, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults (returns application error)
 *********************************************************************************************************************/

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  return RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Powerlatch_Information_Positioning_PIP_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Powerlatch_Information_Positioning_PIP>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, uint8 *Out_RoutineStatusRecord_01_02, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_Start (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  VCU_DiagMuxOnPwt Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt;

  IdtOBC_PowerAvailableControlPilot PimPimOBC_PowerAvailableControlPilot;
  IdtOBC_PowerAvailableHardware PimPimOBC_PowerAvailableHardware;
  uint8 PimPimMSC_PowerLatchFlag;
  IdtOBC_PowerMaxCalculated PimPimOBC_PowerMaxCalculated;

  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue_data;
  IdtTimeLockDelay CalTimeLockDelay_data;

  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPimOBC_PowerAvailableControlPilot = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableControlPilot() = PimPimOBC_PowerAvailableControlPilot;
  PimPimOBC_PowerAvailableHardware = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerAvailableHardware() = PimPimOBC_PowerAvailableHardware;
  PimPimMSC_PowerLatchFlag = *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag();
  *TSC_CtApMSC_Rte_Pim_PimMSC_PowerLatchFlag() = PimPimMSC_PowerLatchFlag;
  PimPimOBC_PowerMaxCalculated = *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated();
  *TSC_CtApMSC_Rte_Pim_PimOBC_PowerMaxCalculated() = PimPimOBC_PowerMaxCalculated;

  CalOBC_PowerMaxValue_data = TSC_CtApMSC_Rte_CData_CalOBC_PowerMaxValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeLockDelay_data = TSC_CtApMSC_Rte_CData_CalTimeLockDelay(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  PpInputVoltageThreshold_DeInputVoltageThreshold_data = TSC_CtApMSC_Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApMSC_Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApMSC_Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  return RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApMSC_STOP_SEC_CODE
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApMSC_TestDefines(void)
{
  /* Enumeration Data Types */

  BMS_MainConnectorState Test_BMS_MainConnectorState_V_1 = Cx0_contactors_opened;
  BMS_MainConnectorState Test_BMS_MainConnectorState_V_2 = Cx1_precharge;
  BMS_MainConnectorState Test_BMS_MainConnectorState_V_3 = Cx2_contactors_closed;
  BMS_MainConnectorState Test_BMS_MainConnectorState_V_4 = Cx3_Invalid;

  BSI_LockedVehicle Test_BSI_LockedVehicle_V_1 = Cx0_No_Locked;
  BSI_LockedVehicle Test_BSI_LockedVehicle_V_2 = Cx1_Locked;
  BSI_LockedVehicle Test_BSI_LockedVehicle_V_3 = Cx2_Overlocked;
  BSI_LockedVehicle Test_BSI_LockedVehicle_V_4 = Cx3_Reserved;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;

  IdtELockSetPoint Test_IdtELockSetPoint_V_1 = ELOCK_SETPOINT_UNLOCKED;
  IdtELockSetPoint Test_IdtELockSetPoint_V_2 = ELOCK_SETPOINT_LOCKED;

  IdtInputVoltageMode Test_IdtInputVoltageMode_V_1 = IVM_NO_INPUT_DETECTED;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_2 = IVM_MONOPHASIC_INPUT;
  IdtInputVoltageMode Test_IdtInputVoltageMode_V_3 = IVM_TRIPHASIC_INPUT;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  OBC_ChargingMode Test_OBC_ChargingMode_V_1 = Cx0_no_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_2 = Cx1_slow_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_3 = Cx2_China_fast_charging;
  OBC_ChargingMode Test_OBC_ChargingMode_V_4 = Cx3_Euro_fast_charging;

  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_1 = Cx0_No_220V_AC;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_2 = Cx1_220V_AC_connected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_3 = Cx2_220V_AC_disconnected;
  OBC_InputVoltageSt Test_OBC_InputVoltageSt_V_4 = Cx3_Invalid_value;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
