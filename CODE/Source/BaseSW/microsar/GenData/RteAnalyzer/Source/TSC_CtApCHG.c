/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApCHG.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtApCHG.h"
#include "TSC_CtApCHG.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtApCHG_Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean data)
{
  return Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode data)
{
  return Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange data)
{
  return Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status data)
{
  return Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtApCHG_Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
{
  return Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
{
  return Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
{
  return Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpFreqOutRange_DeFreqOutRange(boolean *data)
{
  return Rte_Read_PpFreqOutRange_DeFreqOutRange(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
{
  return Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
{
  return Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
{
  return Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
{
  return Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
{
  return Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState *data)
{
  return Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_SOC_BMS_SOC(BMS_SOC *data)
{
  return Rte_Read_PpInt_BMS_SOC_BMS_SOC(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data)
{
  return Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
{
  return Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status *data)
{
  return Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
{
  return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
{
  return Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
{
  return Rte_Read_PpInt_OBC_Fault_OBC_Fault(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
{
  return Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
{
  return Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz *data)
{
  return Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz *data)
{
  return Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz *data)
{
  return Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data)
{
  return Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
{
  return Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpOBCDerating_DeOBCDerating(boolean *data)
{
  return Rte_Read_PpOBCDerating_DeOBCDerating(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
{
  return Rte_Read_PpOutputELockSensor_DeOutputELockSensor(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean *data)
{
  return Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean *data)
{
  return Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
{
  return Rte_Read_PpPlantModeState_DePlantModeState(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC1FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC1FaultSCG(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC1FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC1FaultSCP(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC2FaultSCG(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC2FaultSCG(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Read_PpTempFaults_DeTempDC2FaultSCP(boolean *data)
{
  return Rte_Read_PpTempFaults_DeTempDC2FaultSCP(data);
}




Std_ReturnType TSC_CtApCHG_Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(boolean data)
{
  return Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit data)
{
  return Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpElockFaultDetected_DeElockFaultDetected(boolean data)
{
  return Rte_Write_PpElockFaultDetected_DeElockFaultDetected(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean data)
{
  return Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode data)
{
  return Rte_Write_PpInputVoltageMode_DeInputVoltageMode(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(DCDC_OBCMainContactorReq data)
{
  return Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(DCDC_OBCQuickChargeContactorReq data)
{
  return Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data)
{
  return Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange data)
{
  return Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data)
{
  return Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data)
{
  return Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpInt_OBC_Status_OBC_Status(OBC_Status data)
{
  return Rte_Write_PpInt_OBC_Status_OBC_Status(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(boolean data)
{
  return Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean data)
{
  return Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean data)
{
  return Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean data)
{
  return Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(data);
}

Std_ReturnType TSC_CtApCHG_Rte_Write_PpStopConditions_DeStopConditions(boolean data)
{
  return Rte_Write_PpStopConditions_DeStopConditions(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtApCHG_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
{
  return Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(debugPinID, debugPinValue);
}
Std_ReturnType TSC_CtApCHG_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */
Std_ReturnType TSC_CtApCHG_Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(ComM_ModeType ComMode)
{
  return Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(ComMode);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





IdtInputVoltageThreshold  TSC_CtApCHG_Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(void)
{
  return (IdtInputVoltageThreshold ) Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold();
}

IdtMaxDemmandVoltage  TSC_CtApCHG_Rte_CData_CalMaxDemmandVoltage(void)
{
  return (IdtMaxDemmandVoltage ) Rte_CData_CalMaxDemmandVoltage();
}
IdtMaxDiscoveryVoltage  TSC_CtApCHG_Rte_CData_CalMaxDiscoveryVoltage(void)
{
  return (IdtMaxDiscoveryVoltage ) Rte_CData_CalMaxDiscoveryVoltage();
}
IdtMaxPrechargeVoltage  TSC_CtApCHG_Rte_CData_CalMaxPrechargeVoltage(void)
{
  return (IdtMaxPrechargeVoltage ) Rte_CData_CalMaxPrechargeVoltage();
}
IdtStopChargeDemmandTimer  TSC_CtApCHG_Rte_CData_CalStopChargeDemmandTimer(void)
{
  return (IdtStopChargeDemmandTimer ) Rte_CData_CalStopChargeDemmandTimer();
}
IdtTimeConfirmNoAC  TSC_CtApCHG_Rte_CData_CalTimeConfirmNoAC(void)
{
  return (IdtTimeConfirmNoAC ) Rte_CData_CalTimeConfirmNoAC();
}
IdtBulkSocConf  TSC_CtApCHG_Rte_CData_CalBulkSocConf(void)
{
  return (IdtBulkSocConf ) Rte_CData_CalBulkSocConf();
}
IdtDemandMaxCurrent  TSC_CtApCHG_Rte_CData_CalDemandMaxCurrent(void)
{
  return (IdtDemandMaxCurrent ) Rte_CData_CalDemandMaxCurrent();
}
IdtEnergyCapacity  TSC_CtApCHG_Rte_CData_CalEnergyCapacity(void)
{
  return (IdtEnergyCapacity ) Rte_CData_CalEnergyCapacity();
}
IdtMaxDemmandCurrent  TSC_CtApCHG_Rte_CData_CalMaxDemmandCurrent(void)
{
  return (IdtMaxDemmandCurrent ) Rte_CData_CalMaxDemmandCurrent();
}
IdtMaxDiscoveryCurrent  TSC_CtApCHG_Rte_CData_CalMaxDiscoveryCurrent(void)
{
  return (IdtMaxDiscoveryCurrent ) Rte_CData_CalMaxDiscoveryCurrent();
}
IdtMaxInputVoltage110V  TSC_CtApCHG_Rte_CData_CalMaxInputVoltage110V(void)
{
  return (IdtMaxInputVoltage110V ) Rte_CData_CalMaxInputVoltage110V();
}
IdtMaxInputVoltage220V  TSC_CtApCHG_Rte_CData_CalMaxInputVoltage220V(void)
{
  return (IdtMaxInputVoltage220V ) Rte_CData_CalMaxInputVoltage220V();
}
IdtMaxPrechargeCurrent  TSC_CtApCHG_Rte_CData_CalMaxPrechargeCurrent(void)
{
  return (IdtMaxPrechargeCurrent ) Rte_CData_CalMaxPrechargeCurrent();
}
IdtMinInputVoltage110V  TSC_CtApCHG_Rte_CData_CalMinInputVoltage110V(void)
{
  return (IdtMinInputVoltage110V ) Rte_CData_CalMinInputVoltage110V();
}
IdtMinInputVoltage220V  TSC_CtApCHG_Rte_CData_CalMinInputVoltage220V(void)
{
  return (IdtMinInputVoltage220V ) Rte_CData_CalMinInputVoltage220V();
}
IdtTargetPrechargeCurrent  TSC_CtApCHG_Rte_CData_CalTargetPrechargeCurrent(void)
{
  return (IdtTargetPrechargeCurrent ) Rte_CData_CalTargetPrechargeCurrent();
}
IdtThresholdNoAC  TSC_CtApCHG_Rte_CData_CalThresholdNoAC(void)
{
  return (IdtThresholdNoAC ) Rte_CData_CalThresholdNoAC();
}
IdtTimeElockFaultDetected  TSC_CtApCHG_Rte_CData_CalTimeElockFaultDetected(void)
{
  return (IdtTimeElockFaultDetected ) Rte_CData_CalTimeElockFaultDetected();
}

     /* CtApCHG */
      /* CtApCHG */



