/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApWUM.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApWUM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApWUM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * ComM_ModeType
 *   
 *
 * ComM_UserHandleType
 *   
 *
 * NvM_BlockIdType
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * ReleaseRUN of Port Interface EcuM_StateRequest
 *   
 *
 * RequestRUN of Port Interface EcuM_StateRequest
 *   
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CtApWUM.h"
#include "TSC_CtApWUM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApWUM_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BSI_PostDriveWakeup: Boolean
 * BSI_PreDriveWakeup: Boolean
 * COUPURE_CONSO_CTP: Boolean
 * COUPURE_CONSO_CTPE2: Boolean
 * ComM_UserHandleType: Integer in interval [0...65535]
 * IdtDebounceHoldNetworkCom: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtDebounceTempoEndCharge: Integer in interval [0...120]
 *   Unit: [s], Factor: 5, Offset: 0
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtMaxTimeDiagTools: Integer in interval [0...1024]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtMaxTimeShutdown: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupCoolingMax: Integer in interval [0...240]
 *   Unit: [minutes], Factor: 1, Offset: 0
 * IdtTimeWakeupCoolingMin: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupHoldDiscontactorMax: Integer in interval [0...600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupHoldDiscontactorMin: Integer in interval [0...120]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupPiStateInfo: Integer in interval [0...120]
 *   Unit: [s], Factor: 5, Offset: 0
 * NvM_BlockIdType: Integer in interval [1...32767]
 * OBC_PlugVoltDetection: Boolean
 * OBC_PushChargeType: Boolean
 * SUPV_CoolingWupState: Boolean
 * SUPV_HVBattChargeWupState: Boolean
 * SUPV_HoldDiscontactorWupState: Boolean
 * SUPV_PIStateInfoWupState: Boolean
 * SUPV_RCDLineState: Boolean
 * VCU_DDEGMVSEEM: Integer in interval [0...100]
 * VCU_DMDActivChiller: Boolean
 * VCU_DMDMeap2SEEM: Integer in interval [0...100]
 * VCU_DiagMuxOnPwt: Boolean
 * VCU_PrecondElecWakeup: Boolean
 * VCU_StopDelayedHMIWakeup: Boolean
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * sint32: Integer in interval [-2147483648...2147483647] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_VCUHeatPumpWorkingMode: Enumeration of integer in interval [0...31] with enumerators
 *   Cx00_Stop_mode (0U)
 *   Cx01_Failure_mode_1 (1U)
 *   Cx02_Failure_mode_2 (2U)
 *   Cx03_Failure_mode_3 (3U)
 *   Cx04_Failure_mode_4 (4U)
 *   Cx05_Failure_mode_5 (5U)
 *   Cx06_Failure_mode_6 (6U)
 *   Cx07_Failure_mode_7 (7U)
 *   Cx08_AC_mode (8U)
 *   Cx09_HP_mode (9U)
 *   Cx0A_Deshum_total_heating_mode (10U)
 *   Cx0B_Deshum_simple_heating_mode (11U)
 *   Cx0C_De_icing_mode_ (12U)
 *   Cx0D_transition_mode (13U)
 *   Cx0E_Failure_mode (14U)
 *   Cx0F_Chiller_alone_mode (15U)
 *   Cx10_AC_chiffler_mode (16U)
 *   Cx11_Deshum_Chiller_mode (17U)
 *   Cx12_Reserved (18U)
 *   Cx13_Water_Heather_mode (19U)
 *   Cx14_Reserved (20U)
 *   Cx15_Reserved (21U)
 *   Cx16_Reserved (22U)
 *   Cx17_Reserved (23U)
 *   Cx18_Reserved (24U)
 *   Cx19_Reserved (25U)
 *   Cx1A_Reserved (26U)
 *   Cx1B_Reserved (27U)
 *   Cx1C_Reserved (28U)
 *   Cx1D_Reserved (29U)
 *   Cx1E_Reserved (30U)
 *   Cx1F_Reserved (31U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint8 *Rte_Pim_NvWUMBlockNeed_MirrorBlock(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxTimeDiagTools Rte_CData_CalMaxTimeDiagTools(void)
 *   IdtTimeWakeupHoldDiscontactorMax Rte_CData_CalTimeWakeupHoldDiscontactorMax(void)
 *   IdtDebounceHoldNetworkCom Rte_CData_CalDebounceHoldNetworkCom(void)
 *   IdtDebounceTempoEndCharge Rte_CData_CalDebounceTempoEndCharge(void)
 *   IdtMaxTimeShutdown Rte_CData_CalMaxTimeShutdown(void)
 *   IdtTimeWakeupCoolingMax Rte_CData_CalTimeWakeupCoolingMax(void)
 *   IdtTimeWakeupCoolingMin Rte_CData_CalTimeWakeupCoolingMin(void)
 *   IdtTimeWakeupHoldDiscontactorMin Rte_CData_CalTimeWakeupHoldDiscontactorMin(void)
 *   IdtTimeWakeupPiStateInfo Rte_CData_CalTimeWakeupPiStateInfo(void)
 *   uint8 Rte_CData_NvWUMBlockNeed_DefaultValue(void)
 *
 *********************************************************************************************************************/


#define CtApWUM_START_SEC_CODE
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpWUM_DiagToolRequestRcvd> of PortPrototype <PpWUM_DiagToolRequestRcvd>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd
 *********************************************************************************************************************/

  uint8 PimNvWUMBlockNeed_MirrorBlock;

  IdtMaxTimeDiagTools CalMaxTimeDiagTools_data;
  IdtTimeWakeupHoldDiscontactorMax CalTimeWakeupHoldDiscontactorMax_data;
  IdtDebounceHoldNetworkCom CalDebounceHoldNetworkCom_data;
  IdtDebounceTempoEndCharge CalDebounceTempoEndCharge_data;
  IdtMaxTimeShutdown CalMaxTimeShutdown_data;
  IdtTimeWakeupCoolingMax CalTimeWakeupCoolingMax_data;
  IdtTimeWakeupCoolingMin CalTimeWakeupCoolingMin_data;
  IdtTimeWakeupHoldDiscontactorMin CalTimeWakeupHoldDiscontactorMin_data;
  IdtTimeWakeupPiStateInfo CalTimeWakeupPiStateInfo_data;
  uint8 NvWUMBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimNvWUMBlockNeed_MirrorBlock = *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock();
  *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock() = PimNvWUMBlockNeed_MirrorBlock;

  CalMaxTimeDiagTools_data = TSC_CtApWUM_Rte_CData_CalMaxTimeDiagTools(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceHoldNetworkCom_data = TSC_CtApWUM_Rte_CData_CalDebounceHoldNetworkCom(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceTempoEndCharge_data = TSC_CtApWUM_Rte_CData_CalDebounceTempoEndCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeShutdown_data = TSC_CtApWUM_Rte_CData_CalMaxTimeShutdown(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupPiStateInfo_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupPiStateInfo(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  NvWUMBlockNeed_DefaultValue_data = TSC_CtApWUM_Rte_CData_NvWUMBlockNeed_DefaultValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApWUM_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApWUM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtApWUM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_init
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  uint8 PimNvWUMBlockNeed_MirrorBlock;

  IdtMaxTimeDiagTools CalMaxTimeDiagTools_data;
  IdtTimeWakeupHoldDiscontactorMax CalTimeWakeupHoldDiscontactorMax_data;
  IdtDebounceHoldNetworkCom CalDebounceHoldNetworkCom_data;
  IdtDebounceTempoEndCharge CalDebounceTempoEndCharge_data;
  IdtMaxTimeShutdown CalMaxTimeShutdown_data;
  IdtTimeWakeupCoolingMax CalTimeWakeupCoolingMax_data;
  IdtTimeWakeupCoolingMin CalTimeWakeupCoolingMin_data;
  IdtTimeWakeupHoldDiscontactorMin CalTimeWakeupHoldDiscontactorMin_data;
  IdtTimeWakeupPiStateInfo CalTimeWakeupPiStateInfo_data;
  uint8 NvWUMBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimNvWUMBlockNeed_MirrorBlock = *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock();
  *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock() = PimNvWUMBlockNeed_MirrorBlock;

  CalMaxTimeDiagTools_data = TSC_CtApWUM_Rte_CData_CalMaxTimeDiagTools(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceHoldNetworkCom_data = TSC_CtApWUM_Rte_CData_CalDebounceHoldNetworkCom(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceTempoEndCharge_data = TSC_CtApWUM_Rte_CData_CalDebounceTempoEndCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeShutdown_data = TSC_CtApWUM_Rte_CData_CalMaxTimeShutdown(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupPiStateInfo_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupPiStateInfo(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  NvWUMBlockNeed_DefaultValue_data = TSC_CtApWUM_Rte_CData_NvWUMBlockNeed_DefaultValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApWUM_Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_MODE_LIMITATION:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_MODE_LIMITATION:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_EcuM_StateRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_EcuM_StateRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApWUM_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP *data)
 *   Std_ReturnType Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(boolean data)
 *   Std_ReturnType Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest(boolean data)
 *   Std_ReturnType Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean data)
 *   Std_ReturnType Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(boolean data)
 *   Std_ReturnType Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data)
 *   Std_ReturnType Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtApWUM_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msA
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  IdtDutyControlPilot Read_PpDutyControlPilot_DeDutyControlPilot;
  BSI_PostDriveWakeup Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup;
  BSI_PreDriveWakeup Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup;
  BSI_VCUHeatPumpWorkingMode Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode;
  COUPURE_CONSO_CTP Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP;
  COUPURE_CONSO_CTPE2 Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2;
  OBC_ChargingConnectionConfirmati Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
  OBC_PlugVoltDetection Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
  OBC_PushChargeType Read_PpInt_OBC_PushChargeType_OBC_PushChargeType;
  OBC_RechargeHMIState Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState;
  SUPV_RCDLineState Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState;
  VCU_DDEGMVSEEM Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM;
  VCU_DMDActivChiller Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller;
  VCU_DMDMeap2SEEM Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM;
  VCU_DiagMuxOnPwt Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt;
  VCU_ModeEPSRequest Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
  VCU_PrecondElecWakeup Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup;
  VCU_StopDelayedHMIWakeup Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup;

  uint8 PimNvWUMBlockNeed_MirrorBlock;

  IdtMaxTimeDiagTools CalMaxTimeDiagTools_data;
  IdtTimeWakeupHoldDiscontactorMax CalTimeWakeupHoldDiscontactorMax_data;
  IdtDebounceHoldNetworkCom CalDebounceHoldNetworkCom_data;
  IdtDebounceTempoEndCharge CalDebounceTempoEndCharge_data;
  IdtMaxTimeShutdown CalMaxTimeShutdown_data;
  IdtTimeWakeupCoolingMax CalTimeWakeupCoolingMax_data;
  IdtTimeWakeupCoolingMin CalTimeWakeupCoolingMin_data;
  IdtTimeWakeupHoldDiscontactorMin CalTimeWakeupHoldDiscontactorMin_data;
  IdtTimeWakeupPiStateInfo CalTimeWakeupPiStateInfo_data;
  uint8 NvWUMBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimNvWUMBlockNeed_MirrorBlock = *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock();
  *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock() = PimNvWUMBlockNeed_MirrorBlock;

  CalMaxTimeDiagTools_data = TSC_CtApWUM_Rte_CData_CalMaxTimeDiagTools(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceHoldNetworkCom_data = TSC_CtApWUM_Rte_CData_CalDebounceHoldNetworkCom(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceTempoEndCharge_data = TSC_CtApWUM_Rte_CData_CalDebounceTempoEndCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeShutdown_data = TSC_CtApWUM_Rte_CData_CalMaxTimeShutdown(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupPiStateInfo_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupPiStateInfo(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  NvWUMBlockNeed_DefaultValue_data = TSC_CtApWUM_Rte_CData_NvWUMBlockNeed_DefaultValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApWUM_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpDutyControlPilot_DeDutyControlPilot(&Read_PpDutyControlPilot_DeDutyControlPilot); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(&Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(&Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(&Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(&Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(&Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(&Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(&Read_PpInt_OBC_PushChargeType_OBC_PushChargeType); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(&Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(&Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(&Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(&Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(&Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(&Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(Rte_InitValue_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(Rte_InitValue_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest(Rte_InitValue_PpDiagToolsRequest_DeDiagToolsRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(Rte_InitValue_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(Rte_InitValue_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(Rte_InitValue_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(Rte_InitValue_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(Rte_InitValue_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(Rte_InitValue_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(Rte_InitValue_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(Rte_InitValue_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs(); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock(NULL_PTR); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_NvMService_AC3_SRBS_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApWUM_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpCANComRequest_DeCANComRequest(sint32 *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msB_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtApWUM_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msB
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  sint32 Read_PpCANComRequest_DeCANComRequest;
  boolean Read_PpShutdownAuthorization_DeShutdownAuthorization;

  uint8 PimNvWUMBlockNeed_MirrorBlock;

  IdtMaxTimeDiagTools CalMaxTimeDiagTools_data;
  IdtTimeWakeupHoldDiscontactorMax CalTimeWakeupHoldDiscontactorMax_data;
  IdtDebounceHoldNetworkCom CalDebounceHoldNetworkCom_data;
  IdtDebounceTempoEndCharge CalDebounceTempoEndCharge_data;
  IdtMaxTimeShutdown CalMaxTimeShutdown_data;
  IdtTimeWakeupCoolingMax CalTimeWakeupCoolingMax_data;
  IdtTimeWakeupCoolingMin CalTimeWakeupCoolingMin_data;
  IdtTimeWakeupHoldDiscontactorMin CalTimeWakeupHoldDiscontactorMin_data;
  IdtTimeWakeupPiStateInfo CalTimeWakeupPiStateInfo_data;
  uint8 NvWUMBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimNvWUMBlockNeed_MirrorBlock = *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock();
  *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock() = PimNvWUMBlockNeed_MirrorBlock;

  CalMaxTimeDiagTools_data = TSC_CtApWUM_Rte_CData_CalMaxTimeDiagTools(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceHoldNetworkCom_data = TSC_CtApWUM_Rte_CData_CalDebounceHoldNetworkCom(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceTempoEndCharge_data = TSC_CtApWUM_Rte_CData_CalDebounceTempoEndCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeShutdown_data = TSC_CtApWUM_Rte_CData_CalMaxTimeShutdown(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupPiStateInfo_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupPiStateInfo(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  NvWUMBlockNeed_DefaultValue_data = TSC_CtApWUM_Rte_CData_NvWUMBlockNeed_DefaultValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApWUM_Rte_Read_PpCANComRequest_DeCANComRequest(&Read_PpCANComRequest_DeCANComRequest); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&Read_PpShutdownAuthorization_DeShutdownAuthorization); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U, 0U, 0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_MODE_LIMITATION:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(0U); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_MODE_LIMITATION:
      fct_error = TRUE;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_EcuM_StateRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApWUM_Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
    case RTE_E_EcuM_StateRequest_E_NOT_OK:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpWUM_ElectronBSI_Frame_Received> of PortPrototype <PpWUM_ElectronBSI_Frame_Received>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received
 *********************************************************************************************************************/

  uint8 PimNvWUMBlockNeed_MirrorBlock;

  IdtMaxTimeDiagTools CalMaxTimeDiagTools_data;
  IdtTimeWakeupHoldDiscontactorMax CalTimeWakeupHoldDiscontactorMax_data;
  IdtDebounceHoldNetworkCom CalDebounceHoldNetworkCom_data;
  IdtDebounceTempoEndCharge CalDebounceTempoEndCharge_data;
  IdtMaxTimeShutdown CalMaxTimeShutdown_data;
  IdtTimeWakeupCoolingMax CalTimeWakeupCoolingMax_data;
  IdtTimeWakeupCoolingMin CalTimeWakeupCoolingMin_data;
  IdtTimeWakeupHoldDiscontactorMin CalTimeWakeupHoldDiscontactorMin_data;
  IdtTimeWakeupPiStateInfo CalTimeWakeupPiStateInfo_data;
  uint8 NvWUMBlockNeed_DefaultValue_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimNvWUMBlockNeed_MirrorBlock = *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock();
  *TSC_CtApWUM_Rte_Pim_NvWUMBlockNeed_MirrorBlock() = PimNvWUMBlockNeed_MirrorBlock;

  CalMaxTimeDiagTools_data = TSC_CtApWUM_Rte_CData_CalMaxTimeDiagTools(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceHoldNetworkCom_data = TSC_CtApWUM_Rte_CData_CalDebounceHoldNetworkCom(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDebounceTempoEndCharge_data = TSC_CtApWUM_Rte_CData_CalDebounceTempoEndCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalMaxTimeShutdown_data = TSC_CtApWUM_Rte_CData_CalMaxTimeShutdown(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMax_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMax(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupCoolingMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupCoolingMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupHoldDiscontactorMin_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupHoldDiscontactorMin(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeWakeupPiStateInfo_data = TSC_CtApWUM_Rte_CData_CalTimeWakeupPiStateInfo(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  NvWUMBlockNeed_DefaultValue_data = TSC_CtApWUM_Rte_CData_NvWUMBlockNeed_DefaultValue(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApWUM_STOP_SEC_CODE
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApWUM_TestDefines(void)
{
  /* Enumeration Data Types */

  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_1 = Cx00_Stop_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_2 = Cx01_Failure_mode_1;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_3 = Cx02_Failure_mode_2;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_4 = Cx03_Failure_mode_3;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_5 = Cx04_Failure_mode_4;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_6 = Cx05_Failure_mode_5;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_7 = Cx06_Failure_mode_6;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_8 = Cx07_Failure_mode_7;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_9 = Cx08_AC_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_10 = Cx09_HP_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_11 = Cx0A_Deshum_total_heating_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_12 = Cx0B_Deshum_simple_heating_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_13 = Cx0C_De_icing_mode_;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_14 = Cx0D_transition_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_15 = Cx0E_Failure_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_16 = Cx0F_Chiller_alone_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_17 = Cx10_AC_chiffler_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_18 = Cx11_Deshum_Chiller_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_19 = Cx12_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_20 = Cx13_Water_Heather_mode;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_21 = Cx14_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_22 = Cx15_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_23 = Cx16_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_24 = Cx17_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_25 = Cx18_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_26 = Cx19_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_27 = Cx1A_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_28 = Cx1B_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_29 = Cx1C_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_30 = Cx1D_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_31 = Cx1E_Reserved;
  BSI_VCUHeatPumpWorkingMode Test_BSI_VCUHeatPumpWorkingMode_V_32 = Cx1F_Reserved;

  ComM_ModeType Test_ComM_ModeType_V_1 = COMM_NO_COMMUNICATION;
  ComM_ModeType Test_ComM_ModeType_V_2 = COMM_SILENT_COMMUNICATION;
  ComM_ModeType Test_ComM_ModeType_V_3 = COMM_FULL_COMMUNICATION;

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_1 = Cx0_invalid_value;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_2 = Cx1_not_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_3 = Cx2_full_connected;
  OBC_ChargingConnectionConfirmati Test_OBC_ChargingConnectionConfirmati_V_4 = Cx3_CC_is_half_connected;

  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_1 = Cx0_Disconnected;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_2 = Cx1_In_progress;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_3 = Cx2_Failure;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_4 = Cx3_Stopped;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_5 = Cx4_Finished;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_6 = Cx5_Reserved;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_7 = Cx6_Reserved;
  OBC_RechargeHMIState Test_OBC_RechargeHMIState_V_8 = Cx7_Reserved;

  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_1 = Cx0_OFF;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_2 = Cx1_Active_Drive;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_3 = Cx2_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_4 = Cx3_PI_Charge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_5 = Cx4_PI_Balance;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_6 = Cx5_PI_Discharge;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_7 = Cx6_Reserved;
  VCU_ModeEPSRequest Test_VCU_ModeEPSRequest_V_8 = Cx7_Reserved;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
