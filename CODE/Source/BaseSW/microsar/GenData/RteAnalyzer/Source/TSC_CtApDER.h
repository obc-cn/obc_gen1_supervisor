/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtApDER.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CtApDER_Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data);
Std_ReturnType TSC_CtApDER_Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CtApDER_Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(boolean data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpOBCDerating_DeOBCDerating(boolean data);
Std_ReturnType TSC_CtApDER_Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC data);

/** SW-C local Calibration Parameters */
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_FinishLinear(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_MaxDeviation(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_MinDeviation(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCDC_StartLinear(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_AmbTemp(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_Buck_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_PushPull_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempEndDerating_SyncRect(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_AmbTemp(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_Buck_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_PushPull_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVHighTempStartDerating_SyncRect(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_AmbTemp(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_Buck_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_PushPull_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempEndDerating_SyncRect(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_AmbTemp(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_Buck_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_A(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_PushPull_B(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalDCLVLowTempStartDerating_SyncRect(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M5(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_DCHV_M6(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M1(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempEndDerating_PFC_M4(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M5(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_DCHV_M6(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M1(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCHighTempStartDerating_PFC_M4(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M5(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_DCHV_M6(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M1(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempEndDerating_PFC_M4(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M5(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_DCHV_M6(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M1(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBCLowTempStartDerating_PFC_M4(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_FinishLinear(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_MaxDeviation(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_MinDeviation(void);
IdtTempDerating  TSC_CtApDER_Rte_CData_CalOBC_StartLinear(void);




