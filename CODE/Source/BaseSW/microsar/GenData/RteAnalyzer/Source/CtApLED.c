/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLED.c
 *           Config:  OBCP11.dpa
 *        SW-C Type:  CtApLED
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLED>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */
/* PRQA S 0857 EOF */ /* MD_MSR_Dir1.1 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2982 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2983 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2880 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApLED.h"
#include "TSC_CtApLED.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CtApLED_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtChLedCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyLedCharge: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyPlug: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtPlgLedrCtrl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtTimeLedCharge: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePlug: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueChargeError(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueChargeInProgress(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueEndOfCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueGuideManagement(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueProgrammingCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenChargeError(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenChargeInProgress(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenEndOfCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenGuideManagement(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenProgrammingCharge(void)
 *   IdtDutyPlug Rte_CData_CalDutyPlug(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedChargeError(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedChargeInProgress(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedEndOfCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedGuideManagement(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedProgrammingCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffChargeError(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffChargeInProgress(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffEndOfCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffGuideManagement(void)
 *   IdtTimePlug Rte_CData_CalTimeOffPlugA(void)
 *   IdtTimePlug Rte_CData_CalTimeOffPlugB(void)
 *   IdtTimePlug Rte_CData_CalTimeOffPlugC(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffProgrammingCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnChargeError(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnChargeInProgress(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnEndOfCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnGuideManagement(void)
 *   IdtTimePlug Rte_CData_CalTimeOnPlugA(void)
 *   IdtTimePlug Rte_CData_CalTimeOnPlugB(void)
 *   IdtTimePlug Rte_CData_CalTimeOnPlugC(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnProgrammingCharge(void)
 *
 *********************************************************************************************************************/


#define CtApLED_START_SEC_CODE
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLED_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLED_CODE) RCtApLED_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_init
 *********************************************************************************************************************/

  IdtDutyLedCharge CalDutyBlueChargeError_data;
  IdtDutyLedCharge CalDutyBlueChargeInProgress_data;
  IdtDutyLedCharge CalDutyBlueEndOfCharge_data;
  IdtDutyLedCharge CalDutyBlueGuideManagement_data;
  IdtDutyLedCharge CalDutyBlueProgrammingCharge_data;
  IdtDutyLedCharge CalDutyGreenChargeError_data;
  IdtDutyLedCharge CalDutyGreenChargeInProgress_data;
  IdtDutyLedCharge CalDutyGreenEndOfCharge_data;
  IdtDutyLedCharge CalDutyGreenGuideManagement_data;
  IdtDutyLedCharge CalDutyGreenProgrammingCharge_data;
  IdtDutyPlug CalDutyPlug_data;
  IdtDutyLedCharge CalDutyRedChargeError_data;
  IdtDutyLedCharge CalDutyRedChargeInProgress_data;
  IdtDutyLedCharge CalDutyRedEndOfCharge_data;
  IdtDutyLedCharge CalDutyRedGuideManagement_data;
  IdtDutyLedCharge CalDutyRedProgrammingCharge_data;
  IdtTimeLedCharge CalTimeOffChargeError_data;
  IdtTimeLedCharge CalTimeOffChargeInProgress_data;
  IdtTimeLedCharge CalTimeOffEndOfCharge_data;
  IdtTimeLedCharge CalTimeOffGuideManagement_data;
  IdtTimePlug CalTimeOffPlugA_data;
  IdtTimePlug CalTimeOffPlugB_data;
  IdtTimePlug CalTimeOffPlugC_data;
  IdtTimeLedCharge CalTimeOffProgrammingCharge_data;
  IdtTimeLedCharge CalTimeOnChargeError_data;
  IdtTimeLedCharge CalTimeOnChargeInProgress_data;
  IdtTimeLedCharge CalTimeOnEndOfCharge_data;
  IdtTimeLedCharge CalTimeOnGuideManagement_data;
  IdtTimePlug CalTimeOnPlugA_data;
  IdtTimePlug CalTimeOnPlugB_data;
  IdtTimePlug CalTimeOnPlugC_data;
  IdtTimeLedCharge CalTimeOnProgrammingCharge_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDutyBlueChargeError_data = TSC_CtApLED_Rte_CData_CalDutyBlueChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyBlueChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyBlueEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyBlueGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyBlueProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenChargeError_data = TSC_CtApLED_Rte_CData_CalDutyGreenChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyGreenChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyGreenEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyGreenGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyGreenProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyPlug_data = TSC_CtApLED_Rte_CData_CalDutyPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedChargeError_data = TSC_CtApLED_Rte_CData_CalDutyRedChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyRedChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyRedEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyRedGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyRedProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffChargeError_data = TSC_CtApLED_Rte_CData_CalTimeOffChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffChargeInProgress_data = TSC_CtApLED_Rte_CData_CalTimeOffChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffEndOfCharge_data = TSC_CtApLED_Rte_CData_CalTimeOffEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffGuideManagement_data = TSC_CtApLED_Rte_CData_CalTimeOffGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugA_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugA(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugB_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugB(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugC_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalTimeOffProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnChargeError_data = TSC_CtApLED_Rte_CData_CalTimeOnChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnChargeInProgress_data = TSC_CtApLED_Rte_CData_CalTimeOnChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnEndOfCharge_data = TSC_CtApLED_Rte_CData_CalTimeOnEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnGuideManagement_data = TSC_CtApLED_Rte_CData_CalTimeOnGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugA_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugA(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugB_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugB(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugC_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalTimeOnProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  CtApLED_TestDefines(); /* PRQA S 2987 */ /* MD_Rte_2987 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLED_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeChargeError(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeChargeInProgress(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeEndOfCharge(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeGuideManagement(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeProgrammingCharge(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl data)
 *   Std_ReturnType Rte_Write_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl data)
 *   Std_ReturnType Rte_Write_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task100ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLED_CODE) RCtApLED_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task100ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  boolean Read_PpLedModes_DeChargeError;
  boolean Read_PpLedModes_DeChargeInProgress;
  boolean Read_PpLedModes_DeEndOfCharge;
  boolean Read_PpLedModes_DeGuideManagement;
  boolean Read_PpLedModes_DeProgrammingCharge;

  IdtDutyLedCharge CalDutyBlueChargeError_data;
  IdtDutyLedCharge CalDutyBlueChargeInProgress_data;
  IdtDutyLedCharge CalDutyBlueEndOfCharge_data;
  IdtDutyLedCharge CalDutyBlueGuideManagement_data;
  IdtDutyLedCharge CalDutyBlueProgrammingCharge_data;
  IdtDutyLedCharge CalDutyGreenChargeError_data;
  IdtDutyLedCharge CalDutyGreenChargeInProgress_data;
  IdtDutyLedCharge CalDutyGreenEndOfCharge_data;
  IdtDutyLedCharge CalDutyGreenGuideManagement_data;
  IdtDutyLedCharge CalDutyGreenProgrammingCharge_data;
  IdtDutyPlug CalDutyPlug_data;
  IdtDutyLedCharge CalDutyRedChargeError_data;
  IdtDutyLedCharge CalDutyRedChargeInProgress_data;
  IdtDutyLedCharge CalDutyRedEndOfCharge_data;
  IdtDutyLedCharge CalDutyRedGuideManagement_data;
  IdtDutyLedCharge CalDutyRedProgrammingCharge_data;
  IdtTimeLedCharge CalTimeOffChargeError_data;
  IdtTimeLedCharge CalTimeOffChargeInProgress_data;
  IdtTimeLedCharge CalTimeOffEndOfCharge_data;
  IdtTimeLedCharge CalTimeOffGuideManagement_data;
  IdtTimePlug CalTimeOffPlugA_data;
  IdtTimePlug CalTimeOffPlugB_data;
  IdtTimePlug CalTimeOffPlugC_data;
  IdtTimeLedCharge CalTimeOffProgrammingCharge_data;
  IdtTimeLedCharge CalTimeOnChargeError_data;
  IdtTimeLedCharge CalTimeOnChargeInProgress_data;
  IdtTimeLedCharge CalTimeOnEndOfCharge_data;
  IdtTimeLedCharge CalTimeOnGuideManagement_data;
  IdtTimePlug CalTimeOnPlugA_data;
  IdtTimePlug CalTimeOnPlugB_data;
  IdtTimePlug CalTimeOnPlugC_data;
  IdtTimeLedCharge CalTimeOnProgrammingCharge_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDutyBlueChargeError_data = TSC_CtApLED_Rte_CData_CalDutyBlueChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyBlueChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyBlueEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyBlueGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyBlueProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenChargeError_data = TSC_CtApLED_Rte_CData_CalDutyGreenChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyGreenChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyGreenEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyGreenGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyGreenProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyPlug_data = TSC_CtApLED_Rte_CData_CalDutyPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedChargeError_data = TSC_CtApLED_Rte_CData_CalDutyRedChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyRedChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyRedEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyRedGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyRedProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffChargeError_data = TSC_CtApLED_Rte_CData_CalTimeOffChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffChargeInProgress_data = TSC_CtApLED_Rte_CData_CalTimeOffChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffEndOfCharge_data = TSC_CtApLED_Rte_CData_CalTimeOffEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffGuideManagement_data = TSC_CtApLED_Rte_CData_CalTimeOffGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugA_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugA(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugB_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugB(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugC_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalTimeOffProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnChargeError_data = TSC_CtApLED_Rte_CData_CalTimeOnChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnChargeInProgress_data = TSC_CtApLED_Rte_CData_CalTimeOnChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnEndOfCharge_data = TSC_CtApLED_Rte_CData_CalTimeOnEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnGuideManagement_data = TSC_CtApLED_Rte_CData_CalTimeOnGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugA_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugA(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugB_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugB(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugC_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalTimeOnProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLED_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLedModes_DeChargeError(&Read_PpLedModes_DeChargeError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLedModes_DeChargeInProgress(&Read_PpLedModes_DeChargeInProgress); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLedModes_DeEndOfCharge(&Read_PpLedModes_DeEndOfCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLedModes_DeGuideManagement(&Read_PpLedModes_DeGuideManagement); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLedModes_DeProgrammingCharge(&Read_PpLedModes_DeProgrammingCharge); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedBrCtl(Rte_InitValue_PpChLedRGBCtl_DeChLedBrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedGrCtl(Rte_InitValue_PpChLedRGBCtl_DeChLedGrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Write_PpChLedRGBCtl_DeChLedRrCtl(Rte_InitValue_PpChLedRGBCtl_DeChLedRrCtl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLED_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(boolean *data)
 *   Std_ReturnType Rte_Read_PpLockStateError_DeLockStateError(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLED_CODE) RCtApLED_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task10ms
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error;

  IdtAppRCDECUState Read_PpAppRCDECUState_DeAppRCDECUState;
  boolean Read_PpLockLED2BEPR_DeLockLED2BEPR;
  boolean Read_PpLockStateError_DeLockStateError;

  IdtDutyLedCharge CalDutyBlueChargeError_data;
  IdtDutyLedCharge CalDutyBlueChargeInProgress_data;
  IdtDutyLedCharge CalDutyBlueEndOfCharge_data;
  IdtDutyLedCharge CalDutyBlueGuideManagement_data;
  IdtDutyLedCharge CalDutyBlueProgrammingCharge_data;
  IdtDutyLedCharge CalDutyGreenChargeError_data;
  IdtDutyLedCharge CalDutyGreenChargeInProgress_data;
  IdtDutyLedCharge CalDutyGreenEndOfCharge_data;
  IdtDutyLedCharge CalDutyGreenGuideManagement_data;
  IdtDutyLedCharge CalDutyGreenProgrammingCharge_data;
  IdtDutyPlug CalDutyPlug_data;
  IdtDutyLedCharge CalDutyRedChargeError_data;
  IdtDutyLedCharge CalDutyRedChargeInProgress_data;
  IdtDutyLedCharge CalDutyRedEndOfCharge_data;
  IdtDutyLedCharge CalDutyRedGuideManagement_data;
  IdtDutyLedCharge CalDutyRedProgrammingCharge_data;
  IdtTimeLedCharge CalTimeOffChargeError_data;
  IdtTimeLedCharge CalTimeOffChargeInProgress_data;
  IdtTimeLedCharge CalTimeOffEndOfCharge_data;
  IdtTimeLedCharge CalTimeOffGuideManagement_data;
  IdtTimePlug CalTimeOffPlugA_data;
  IdtTimePlug CalTimeOffPlugB_data;
  IdtTimePlug CalTimeOffPlugC_data;
  IdtTimeLedCharge CalTimeOffProgrammingCharge_data;
  IdtTimeLedCharge CalTimeOnChargeError_data;
  IdtTimeLedCharge CalTimeOnChargeInProgress_data;
  IdtTimeLedCharge CalTimeOnEndOfCharge_data;
  IdtTimeLedCharge CalTimeOnGuideManagement_data;
  IdtTimePlug CalTimeOnPlugA_data;
  IdtTimePlug CalTimeOnPlugB_data;
  IdtTimePlug CalTimeOnPlugC_data;
  IdtTimeLedCharge CalTimeOnProgrammingCharge_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  CalDutyBlueChargeError_data = TSC_CtApLED_Rte_CData_CalDutyBlueChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyBlueChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyBlueEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyBlueGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyBlueProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyBlueProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenChargeError_data = TSC_CtApLED_Rte_CData_CalDutyGreenChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyGreenChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyGreenEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyGreenGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyGreenProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyGreenProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyPlug_data = TSC_CtApLED_Rte_CData_CalDutyPlug(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedChargeError_data = TSC_CtApLED_Rte_CData_CalDutyRedChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedChargeInProgress_data = TSC_CtApLED_Rte_CData_CalDutyRedChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedEndOfCharge_data = TSC_CtApLED_Rte_CData_CalDutyRedEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedGuideManagement_data = TSC_CtApLED_Rte_CData_CalDutyRedGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalDutyRedProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalDutyRedProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffChargeError_data = TSC_CtApLED_Rte_CData_CalTimeOffChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffChargeInProgress_data = TSC_CtApLED_Rte_CData_CalTimeOffChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffEndOfCharge_data = TSC_CtApLED_Rte_CData_CalTimeOffEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffGuideManagement_data = TSC_CtApLED_Rte_CData_CalTimeOffGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugA_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugA(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugB_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugB(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffPlugC_data = TSC_CtApLED_Rte_CData_CalTimeOffPlugC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOffProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalTimeOffProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnChargeError_data = TSC_CtApLED_Rte_CData_CalTimeOnChargeError(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnChargeInProgress_data = TSC_CtApLED_Rte_CData_CalTimeOnChargeInProgress(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnEndOfCharge_data = TSC_CtApLED_Rte_CData_CalTimeOnEndOfCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnGuideManagement_data = TSC_CtApLED_Rte_CData_CalTimeOnGuideManagement(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugA_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugA(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugB_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugB(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnPlugC_data = TSC_CtApLED_Rte_CData_CalTimeOnPlugC(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */
  CalTimeOnProgrammingCharge_data = TSC_CtApLED_Rte_CData_CalTimeOnProgrammingCharge(); /* PRQA S 0315, 0316 */ /* MD_Rte_0315, MD_Rte_0316 */

  fct_status = TSC_CtApLED_Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&Read_PpAppRCDECUState_DeAppRCDECUState); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(&Read_PpLockLED2BEPR_DeLockLED2BEPR); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Read_PpLockStateError_DeLockStateError(&Read_PpLockStateError_DeLockStateError); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = TRUE;
      break;
    case RTE_E_INVALID:
      fct_error = TRUE;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = TRUE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl(Rte_InitValue_PpPlgLedrCtrl_DePlgLedrCtrl); /* PRQA S 3226, 0315 */ /* MD_Rte_3226, MD_Rte_0315 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
  }

  fct_status = TSC_CtApLED_Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(0U, FALSE); /* PRQA S 0315, 3226 */ /* MD_Rte_0315, MD_Rte_3226 */
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = FALSE;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = TRUE;
      break;
    case RTE_E_TIMEOUT:
      fct_error = TRUE;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLED_STOP_SEC_CODE
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CtApLED_TestDefines(void)
{
  /* Enumeration Data Types */

  IdtAppRCDECUState Test_IdtAppRCDECUState_V_1 = APP_STATE_0;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_2 = APP_STATE_1;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_3 = APP_STATE_2;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_4 = APP_STATE_3;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_5 = APP_STATE_4;
  IdtAppRCDECUState Test_IdtAppRCDECUState_V_6 = APP_STATE_5;

  IdtDebugPortID Test_IdtDebugPortID_V_1 = DEBUG_PORT_ID_0;
  IdtDebugPortID Test_IdtDebugPortID_V_2 = DEBUG_PORT_ID_1;
  IdtDebugPortID Test_IdtDebugPortID_V_3 = DEBUG_PORT_ID_2;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3226:  MISRA rule: Rule13.4
     Reason:     Needed for function like macro to do arithmetic operations in sub macros
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_TestCode:
     Reason:     This justification is used as summary justification for all deviations caused by wrong analysis tool results.
                 The used analysis tool QAC 9.0 sometimes creates wrong messages. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
