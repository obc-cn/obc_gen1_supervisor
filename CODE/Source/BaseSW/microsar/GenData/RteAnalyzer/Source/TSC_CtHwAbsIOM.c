/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CtHwAbsIOM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CtHwAbsIOM.h"
#include "TSC_CtHwAbsIOM.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
{
  return Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data);
}




Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpInputChargePushRaw_DeInputChargePushRaw(boolean data)
{
  return Rte_Write_PpInputChargePushRaw_DeInputChargePushRaw(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean data)
{
  return Rte_Write_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
{
  return Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(frameNum, byteNum, data);
}


     /* Service calls */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
{
  return Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(CPID);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean data)
{
  return Rte_Write_PpExtRCDLineRaw_DeExtRCDLineRaw(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean data)
{
  return Rte_Write_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean data)
{
  return Rte_Write_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean data)
{
  return Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean data)
{
  return Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean *data)
{
  return Rte_Read_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean *data)
{
  return Rte_Read_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(boolean *data)
{
  return Rte_Read_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(boolean *data)
{
  return Rte_Read_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(boolean *data)
{
  return Rte_Read_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(boolean *data)
{
  return Rte_Read_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(data);
}

Std_ReturnType TSC_CtHwAbsIOM_Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean *data)
{
  return Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(data);
}








     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_general_Core0_ActivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID)
{
  return Rte_Call_general_Core0_ActivateSupervisionEntity(SEID);
}
Std_ReturnType TSC_CtHwAbsIOM_Rte_Call_general_Core0_DeactivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID)
{
  return Rte_Call_general_Core0_DeactivateSupervisionEntity(SEID);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* CtHwAbsIOM */
      /* CtHwAbsIOM */



