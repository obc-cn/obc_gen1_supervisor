/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Cfg.h
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#ifndef OS_CFG_H
# define OS_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */

/* Os module declarations */

/* Os kernel module dependencies */

/* Os hal dependencies */
# include "Os_Hal_Cfg.h"


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/*! Configuration major version identification. */
# define OS_CFG_MAJOR_VERSION                    (2u)

/*! Configuration minor version identification. */
# define OS_CFG_MINOR_VERSION                    (37u)

/*! Defines which platform is used. */
# define OS_CFG_PLATFORM_TRICOREAURIX     (STD_ON)

/*! Defines which derivative group is configured. */
# define OS_CFG_DERIVATIVEGROUP_TC23X     (STD_ON)

/*! Defines which derivative is configured. */
# define OS_CFG_DERIVATIVE_TC234     (STD_ON)

/*! Defines which compiler is configured. */
# define OS_CFG_COMPILER_GNU     (STD_ON)

/*! Defines whether access macros to get context related information in the error hook are enabled (STD_ON) or not (STD_OFF). */
# define OS_CFG_ERR_PARAMETERACCESS              (STD_OFF)

/*! Defines whether access macros to get service ID information in the error hook are enabled (STD_ON) or not (STD_OFF). */
# define OS_CFG_ERR_GETSERVICEID                 (STD_OFF)

/*! Defines whether the pre-task hook is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_PRETASKHOOK                      (STD_OFF)

/*! Defines whether the post-task hook is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_POSTTASKHOOK                     (STD_OFF)

/*! Defines whether the OS shall call the panic hook (STD_ON) or not (STD_OFF). */
# define OS_CFG_PANICHOOK                        (STD_ON)

/*! Defines whether the system startup hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_STARTUPHOOK_SYSTEM               (STD_ON)

/*! Defines whether the system shutdown hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_SHUTDOWNHOOK_SYSTEM              (STD_ON)

/*! Defines whether the system error hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_ERRORHOOK_SYSTEM                 (STD_ON)

/*! Defines whether the system protection hook is configured (STD_ON) or not (STD_OFF). */
# define OS_CFG_PROTECTIONHOOK_SYSTEM            (STD_ON)

/*! Defines whether backward compatibility defines are needed (STD_ON) or not (STD_OFF). */
# define OS_CFG_PERIPHERAL_COMPATIBILITY         (STD_ON)

/* OS application modes */
# define DONOTCARE     ((AppModeType)0)
# define OS_APPMODE_NONE     ((AppModeType)0)
# define OSDEFAULTAPPMODE     ((AppModeType)1)
# define OS_APPMODE_ANY     ((AppModeType)255)

/*! Defines whether EVENT is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_EVENT                            (STD_ON)

/* Event masks */
# define Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_1_5ms     ((EventMaskType)1uLL)
# define Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_7_10ms     ((EventMaskType)2uLL)
# define Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_8_20ms     ((EventMaskType)4uLL)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_100ms     ((EventMaskType)1uLL)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_10ms     ((EventMaskType)2uLL)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_2_10ms     ((EventMaskType)4uLL)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_3_10ms     ((EventMaskType)8uLL)
# define Rte_Ev_Cyclic_ASILB_MAIN_TASK_4_10ms     ((EventMaskType)16uLL)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_2_100ms     ((EventMaskType)1uLL)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_2_10ms     ((EventMaskType)2uLL)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_3_100ms     ((EventMaskType)4uLL)
# define Rte_Ev_Cyclic_QM_MAIN_TASK_3_10ms     ((EventMaskType)8uLL)
# define Rte_Ev_Run_CpApAEM_RCtApAEM_task10msA     ((EventMaskType)16uLL)
# define Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msRX     ((EventMaskType)32uLL)
# define Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msTX     ((EventMaskType)64uLL)

/* Software counter timing macros */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Cfg.h
 *********************************************************************************************************************/

