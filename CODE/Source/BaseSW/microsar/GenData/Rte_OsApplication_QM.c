/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_OsApplication_QM.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  RTE implementation file
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0857 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define RTE_CORE
#include "Os.h" /* PRQA S 0828, 0883 */ /* MD_MSR_Dir1.1, MD_Rte_Os */
#include "Rte_Type.h"
#include "Rte_Main.h"

#include "Rte_BswM.h"
#include "Rte_ComM.h"
#include "Rte_CtApAEM.h"
#include "Rte_CtApBAT.h"
#include "Rte_CtApCHG.h"
#include "Rte_CtApCPT.h"
#include "Rte_CtApDCH.h"
#include "Rte_CtApDER.h"
#include "Rte_CtApDGN.h"
#include "Rte_CtApFCL.h"
#include "Rte_CtApFCT.h"
#include "Rte_CtApILT.h"
#include "Rte_CtApJDD.h"
#include "Rte_CtApLAD.h"
#include "Rte_CtApLED.h"
#include "Rte_CtApLFM.h"
#include "Rte_CtApLSD.h"
#include "Rte_CtApLVC.h"
#include "Rte_CtApMSC.h"
#include "Rte_CtApOBC.h"
#include "Rte_CtApOFM.h"
#include "Rte_CtApPCOM.h"
#include "Rte_CtApPLS.h"
#include "Rte_CtApPLT.h"
#include "Rte_CtApPSH.h"
#include "Rte_CtApPXL.h"
#include "Rte_CtApRCD.h"
#include "Rte_CtApRLY.h"
#include "Rte_CtApTBD.h"
#include "Rte_CtApWUM.h"
#include "Rte_CtHwAbsAIM.h"
#include "Rte_CtHwAbsIOM.h"
#include "Rte_CtHwAbsPIM.h"
#include "Rte_CtHwAbsPOM.h"
#include "Rte_Dcm.h"
#include "Rte_DemMaster_0.h"
#include "Rte_DemSatellite_0.h"
#include "Rte_Det.h"
#include "Rte_EcuM.h"
#include "Rte_NvM.h"
#include "Rte_Os_OsCore0_swc.h"
#include "Rte_WdgM_OsApplication_ASILB.h"
#include "SchM_Adc.h"
#include "SchM_BswM.h"
#include "SchM_Can.h"
#include "SchM_CanIf.h"
#include "SchM_CanSM.h"
#include "SchM_CanTp.h"
#include "SchM_CanTrcv_30_Tja1145.h"
#include "SchM_Com.h"
#include "SchM_ComM.h"
#include "SchM_Dcm.h"
#include "SchM_Dem.h"
#include "SchM_Det.h"
#include "SchM_Dio.h"
#include "SchM_EcuM.h"
#include "SchM_EthIf.h"
#include "SchM_EthSM.h"
#include "SchM_EthTrcv_30_Ar7000.h"
#include "SchM_Eth_30_Ar7000.h"
#include "SchM_Exi.h"
#include "SchM_Fee.h"
#include "SchM_Fls_17_Pmu.h"
#include "SchM_Gpt.h"
#include "SchM_Icu_17_GtmCcu6.h"
#include "SchM_Irq.h"
#include "SchM_Mcu.h"
#include "SchM_NvM.h"
#include "SchM_PduR.h"
#include "SchM_Port.h"
#include "SchM_Pwm_17_Gtm.h"
#include "SchM_Scc.h"
#include "SchM_Spi.h"
#include "SchM_TcpIp.h"
#include "SchM_WdgM.h"
#include "SchM_Wdg_30_TLE4278G.h"
#include "SchM_Xcp.h"

#include "Rte_Hook.h"

#include "Com.h"
#if defined(IL_ASRCOM_VERSION)
# define RTE_USE_COM_TXSIGNAL_RDACCESS
#endif

#include "Rte_Cbk.h"

/* AUTOSAR 3.x compatibility */
#if !defined (RTE_LOCAL)
# define RTE_LOCAL static
#endif


/**********************************************************************************************************************
 * API for enable / disable interrupts global
 *********************************************************************************************************************/

#if defined(osDisableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableAllInterrupts() osDisableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_DisableAllInterrupts() DisableAllInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableAllInterrupts() osEnableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_EnableAllInterrupts() EnableAllInterrupts()   /* AUTOSAR OS */
#endif

/**********************************************************************************************************************
 * API for enable / disable interrupts up to the systemLevel
 *********************************************************************************************************************/

#if defined(osDisableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableOSInterrupts() osDisableLevelKM()   /* MICROSAR OS */
#else
# define Rte_DisableOSInterrupts() SuspendOSInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableOSInterrupts() osEnableLevelKM()   /* MICROSAR OS */
#else
# define Rte_EnableOSInterrupts() ResumeOSInterrupts()   /* AUTOSAR OS */
#endif


/**********************************************************************************************************************
 * Buffers for unqueued S/R
 *********************************************************************************************************************/

#define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpCANComRequest_DeCANComRequest = 0;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup = 0;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_CoolingWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_HVBattRechargeWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_HoldDiscontactorWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_PIStateInfoWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_ECUElecStateRCD, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_PostDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_PreDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_PrecondElecWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_StopDelayedHMIWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(EDITION_CALIB, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(EDITION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_CommunicationSt, RTE_VAR_INIT) Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VERSION_APPLI, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VERSION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VERSION_SYSTEME, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VERS_DATE2_ANNEE, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VERS_DATE2_JOUR, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VERS_DATE2_MOIS, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue = TRUE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtExtPlgLedrCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_PushChargeType, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_RechargeHMIState, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeInProgress = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeEndOfCharge = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeGuideManagement = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeProgrammingCharge = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockStateError_DeLockStateError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_0, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_1, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_2, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_3, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_4, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_5, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_6, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(NEW_JDD_OBC_DCDC_BYTE_7, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlgLedrCtrl, RTE_VAR_INIT) Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_OBCDCDCRTPowerLoad, RTE_VAR_INIT) Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_PowerMax, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_SocketTempL, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL = 50U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_SocketTempN, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN = 50U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlantModeTestInfoDID_Result, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlantModeTestInfoDID_State, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtRECHARGE_HMI_STATE, RTE_VAR_INIT) Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPSH_PpOutputChargePushFil_DeOutputChargePushFil = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_RCDLineState, RTE_VAR_INIT) Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_OVERTEMP, RTE_VAR_INIT) Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_DCChargingPlugAConnConf, RTE_VAR_INIT) Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_DTCRegistred, RTE_VAR_INIT) Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_CoolingWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_HVBattChargeWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_HoldDiscontactorWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUPV_PIStateInfoWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP = FALSE;
/* PRQA L:L1 */

#define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Per-Instance Memory
 *********************************************************************************************************************/

#define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorCounterOverCurrent; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterOpenCircuit; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterOpenCircuit; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtOBC_PowerAvailableControlPilot, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerAvailableControlPilot; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtOBC_PowerAvailableHardware, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerAvailableHardware; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtOutputELockSensor, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSensor_PreviousToLock; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtOutputELockSensor, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSensor_PreviousToUnlock; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtELockSetPoint, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSetPoint; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimMSC_PowerLatchFlag; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtOBC_PowerMaxCalculated, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerMaxCalculated; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtTimeoutPlantMode, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimGlobalTimeoutPlantMode; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtTimeoutPlantMode, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimTimeoutPlantMode; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApWUM_NvWUMBlockNeed_MirrorBlock; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultOC; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultOpenCircuit; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorPrefaultOverCurrent; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockNotCurrentMeasured; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeControlPilot; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeDCRelayVoltage; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModePhaseVoltage; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeProximity; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtArrayDGN_EOL_NVM_RamMirror, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApDGN_PimDGN_EOL_NVM_RamMirror; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtArrayJDDNvMRamMirror, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApJDD_PimJDD_NvMRamMirror; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtPlantModeTestUTPlugin, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeTestUTPlugin; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtPSHNvMArray, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPSH_NvPSHBlockNeed_MirrorBlock; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */

#define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
/**********************************************************************************************************************
 * Defines for Rte_ComSendSignalProxy
 *********************************************************************************************************************/
#define RTE_COM_SENDSIGNALPROXY_NOCHANGE       (0U)
#define RTE_COM_SENDSIGNALPROXY_SEND           (1U)
#define RTE_COM_SENDSIGNALPROXY_INVALIDATE     (2U)


#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) Rte_MemClr(P2VAR(void, AUTOMATIC, RTE_VAR_NOINIT) ptr, uint32_least num);
FUNC(void, RTE_CODE) Rte_MemCpy(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num); /* PRQA S 1505, 3408 */ /* MD_MSR_Rule8.7, MD_Rte_3408 */
FUNC(void, RTE_CODE) Rte_MemCpy32(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num); /* PRQA S 1505, 3408 */ /* MD_MSR_Rule8.7, MD_Rte_3408 */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Helper functions for mode management
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_BswM_ESH_Mode(BswM_ESH_Mode mode); /* PRQA S 3408 */ /* MD_Rte_3408 */
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_Dcm_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType mode); /* PRQA S 3408 */ /* MD_Rte_3408 */
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_Dcm_DcmEcuReset(Dcm_EcuResetType mode); /* PRQA S 3408 */ /* MD_Rte_3408 */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Timer handling
 *********************************************************************************************************************/

#if defined OS_US2TICKS_SystemTimer
# define RTE_USEC_SystemTimer OS_US2TICKS_SystemTimer
#else
# define RTE_USEC_SystemTimer(val) ((TickType)RTE_CONST_USEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_MS2TICKS_SystemTimer
# define RTE_MSEC_SystemTimer OS_MS2TICKS_SystemTimer
#else
# define RTE_MSEC_SystemTimer(val) ((TickType)RTE_CONST_MSEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_SEC2TICKS_SystemTimer
# define RTE_SEC_SystemTimer OS_SEC2TICKS_SystemTimer
#else
# define RTE_SEC_SystemTimer(val)  ((TickType)RTE_CONST_SEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#define RTE_CONST_MSEC_SystemTimer_0 (0UL)
#define RTE_CONST_MSEC_SystemTimer_1 (100000UL)
#define RTE_CONST_MSEC_SystemTimer_10 (1000000UL)
#define RTE_CONST_MSEC_SystemTimer_100 (10000000UL)
#define RTE_CONST_MSEC_SystemTimer_2 (200000UL)
#define RTE_CONST_MSEC_SystemTimer_20 (2000000UL)
#define RTE_CONST_MSEC_SystemTimer_3 (300000UL)
#define RTE_CONST_MSEC_SystemTimer_4 (400000UL)
#define RTE_CONST_MSEC_SystemTimer_5 (500000UL)
#define RTE_CONST_MSEC_SystemTimer_7 (700000UL)
#define RTE_CONST_MSEC_SystemTimer_8 (800000UL)


/**********************************************************************************************************************
 * Internal definitions
 *********************************************************************************************************************/

#define RTE_TASK_TIMEOUT_EVENT_MASK   ((EventMaskType)0x01)
#define RTE_TASK_WAITPOINT_EVENT_MASK ((EventMaskType)0x02)

/**********************************************************************************************************************
 * RTE life cycle API
 *********************************************************************************************************************/

#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


FUNC(void, RTE_CODE) Rte_InitMemory_OsApplication_QM(void)
{
  /* set default values for internal data */
  Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState = 2U;
  Rte_CpApAEM_PpCANComRequest_DeCANComRequest = 0;
  Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup = 0;
  Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain = FALSE;
  Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd = FALSE;
  Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst = FALSE;
  Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC = FALSE;
  Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup = FALSE;
  Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup = FALSE;
  Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup = FALSE;
  Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup = FALSE;
  Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD = 2U;
  Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState = FALSE;
  Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState = FALSE;
  Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState = FALSE;
  Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState = FALSE;
  Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd = FALSE;
  Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst = FALSE;
  Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC = FALSE;
  Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue = FALSE;
  Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization = FALSE;
  Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB = 0U;
  Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT = 0U;
  Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt = FALSE;
  Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI = 0U;
  Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT = 0U;
  Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME = 0U;
  Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE = 0U;
  Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR = 0U;
  Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS = 0U;
  Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue = TRUE;
  Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl = 0U;
  Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl = 0U;
  Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl = 0U;
  Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl = 0U;
  Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG = FALSE;
  Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP = FALSE;
  Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions = FALSE;
  Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions = FALSE;
  Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions = FALSE;
  Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions = FALSE;
  Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC = FALSE;
  Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG = FALSE;
  Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP = FALSE;
  Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType = FALSE;
  Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState = 0U;
  Rte_CpApILT_PpLedModes_DeChargeError = FALSE;
  Rte_CpApILT_PpLedModes_DeChargeInProgress = FALSE;
  Rte_CpApILT_PpLedModes_DeEndOfCharge = FALSE;
  Rte_CpApILT_PpLedModes_DeGuideManagement = FALSE;
  Rte_CpApILT_PpLedModes_DeProgrammingCharge = FALSE;
  Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR = FALSE;
  Rte_CpApILT_PpLockStateError_DeLockStateError = FALSE;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 = 0U;
  Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 = 0U;
  Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP = FALSE;
  Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent = FALSE;
  Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions = FALSE;
  Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue = FALSE;
  Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue = FALSE;
  Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl = 0U;
  Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl = 0U;
  Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl = 0U;
  Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl = 0U;
  Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode = 0U;
  Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad = 0U;
  Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax = 0U;
  Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL = 50U;
  Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN = 50U;
  Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode = 0U;
  Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError = FALSE;
  Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress = FALSE;
  Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge = FALSE;
  Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement = FALSE;
  Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge = FALSE;
  Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode = FALSE;
  Rte_CpApPLT_PpPlantModeState_DePlantModeState = FALSE;
  Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result = 0U;
  Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State = 0U;
  Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase = 0U;
  Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines = 0U;
  Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty = 0U;
  Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage = 0U;
  Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode = 0U;
  Rte_CpApPSH_PpOutputChargePushFil_DeOutputChargePushFil = FALSE;
  Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState = FALSE;
  Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays = FALSE;
  Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono = FALSE;
  Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP = FALSE;
  Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf = FALSE;
  Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred = FALSE;
  Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC = FALSE;
  Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP = FALSE;
  Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest = FALSE;
  Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest = FALSE;
  Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP = FALSE;
  Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP = FALSE;
  Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState = FALSE;
  Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState = FALSE;
  Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState = FALSE;
  Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState = FALSE;
  Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP = FALSE;

}


/**********************************************************************************************************************
 * Mode reading API (Rte_Mode)
 *********************************************************************************************************************/

FUNC(uint8, RTE_CODE) Rte_Mode_CtApDGN_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void) /* PRQA S 3408 */ /* MD_Rte_3408 */
{
  uint8 curMode;
  curMode = Rte_ModeMachine_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl;
  return curMode;
}


/**********************************************************************************************************************
 * Task bodies for RTE controlled tasks
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Task:     QM_MAIN_TASK
 * Priority: 495
 * Schedule: FULL
 *********************************************************************************************************************/
TASK(QM_MAIN_TASK) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{
  EventMaskType ev;

  for(;;)
  {
    (void)WaitEvent(Rte_Ev_Cyclic_QM_MAIN_TASK_2_100ms | Rte_Ev_Cyclic_QM_MAIN_TASK_2_10ms | Rte_Ev_Cyclic_QM_MAIN_TASK_3_100ms | Rte_Ev_Cyclic_QM_MAIN_TASK_3_10ms | Rte_Ev_Run_CpApAEM_RCtApAEM_task10msA); /* PRQA S 3417 */ /* MD_Rte_Os */
    (void)GetEvent(QM_MAIN_TASK, &ev); /* PRQA S 3417 */ /* MD_Rte_Os */
    (void)ClearEvent(ev & (Rte_Ev_Cyclic_QM_MAIN_TASK_2_100ms | Rte_Ev_Cyclic_QM_MAIN_TASK_2_10ms | Rte_Ev_Cyclic_QM_MAIN_TASK_3_100ms | Rte_Ev_Cyclic_QM_MAIN_TASK_3_10ms | Rte_Ev_Run_CpApAEM_RCtApAEM_task10msA)); /* PRQA S 3417 */ /* MD_Rte_Os */

    if ((ev & Rte_Ev_Run_CpApAEM_RCtApAEM_task10msA) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApAEM_task10msA(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_QM_MAIN_TASK_2_10ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApRCD_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApMSC_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApRLY_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_QM_MAIN_TASK_2_100ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApMSC_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApPSH_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApILT_task100msA(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApPLT_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApILT_task100msB(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_QM_MAIN_TASK_3_10ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApILT_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApWUM_task10msA(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApLED_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApAEM_task10msB(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApFCL_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApLAD_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApWUM_task10msB(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApJDD_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApDGN_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_QM_MAIN_TASK_3_100ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApLED_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApFCL_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }
  }
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 * Task:     QM_init_task
 * Priority: 998
 * Schedule: FULL
 *********************************************************************************************************************/
TASK(QM_init_task) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{

  /* call runnable */
  RCtApPSH_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApRCD_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApPLT_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApILT_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApMSC_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApRLY_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  CtApLAD_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApLED_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApFCL_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  CtApAEM_Init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApJDD_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApDGN_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApWUM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApTBD_tbd(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  (void)TerminateTask(); /* PRQA S 3417 */ /* MD_Rte_Os */
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_1514:  MISRA rule: Rule8.9
     Reason:     Because of external definition, misra does not see the call.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_1533:  MISRA rule: Rule8.9
     Reason:     Object is referenced by more than one function in different configurations.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3408:  MISRA rule: Rule8.4
     Reason:     For the purpose of monitoring during calibration or debugging it is necessary to use non-static declarations.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
