# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
#   \verbatim
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   \endverbatim
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#              File:  Rte_check.mak
#            Config:  OBCP11.dpa
#       ECU-Project:  OBCP11
# 
#         Generator:  MICROSAR RTE Generator Version 4.21.0
#                     RTE Core Version 1.21.0
#           License:  CBD1900270
# 
#       Description:  GNU MAKEFILE (checks)
# *********************************************************************************************************************
