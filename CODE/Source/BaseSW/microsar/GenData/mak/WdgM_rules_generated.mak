#######################################################################################################################
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
#   \verbatim
# 
#                  This software is copyright protected and proprietary to Vector Informatik GmbH.
#                  Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                  All other rights remain with Vector Informatik GmbH.
#   \endverbatim
#   -------------------------------------------------------------------------------------------------------------------
#   LICENSE
#   -------------------------------------------------------------------------------------------------------------------
#             Module: WdgM
#            Program: MSR_Vector_SLP4
#           Customer: MAHLE Electronics S.A.
#        Expiry Date: Not restricted
#   Ordered Derivat.: SAK-TC234L AC
#     License Scope : The usage is restricted to CBD1900270_D04
# 
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#               File: mak/WdgM_rules_generated.mak
#    Generation Time: 2020-11-23 11:39:45
#            Project: OBCP11 - Version 1.0
#           Delivery: CBD1900270_D04
#       Tool Version: DaVinci Configurator  5.20.55 SP4
# 
# 
#######################################################################################################################

GENERATED_SOURCE_FILES  += $(GENDATA_DIR)\WdgM_StatusNotifications_Core0.c

