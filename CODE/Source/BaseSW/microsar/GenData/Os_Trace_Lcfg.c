/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Trace_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:50
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_TRACE_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Trace_Lcfg.h"
#include "Os_Trace.h"

/* Os kernel module dependencies */
#include "Os_Common_Types.h"

/* Os hal dependencies */


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define OS_START_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Dynamic trace data: OsCore0 */
VAR(Os_TraceCoreType, OS_VAR_NOINIT) OsCfg_Trace_OsCore0_Dyn;

/*! Dynamic trace data: ASILB_MAIN_TASK */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_ASILB_MAIN_TASK_Dyn;

/*! Dynamic trace data: ASILB_init_task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_ASILB_init_task_Dyn;

/*! Dynamic trace data: Default_BSW_Async_QM_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Default_BSW_Async_QM_Task_Dyn;

/*! Dynamic trace data: Default_BSW_Async_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Default_BSW_Async_Task_Dyn;

/*! Dynamic trace data: Default_BSW_Sync_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Default_BSW_Sync_Task_Dyn;

/*! Dynamic trace data: Default_Init_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Default_Init_Task_Dyn;

/*! Dynamic trace data: Default_RTE_Mode_switch_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Default_RTE_Mode_switch_Task_Dyn;

/*! Dynamic trace data: IdleTask_OsCore0 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_IdleTask_OsCore0_Dyn;

/*! Dynamic trace data: QM_MAIN_TASK */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_QM_MAIN_TASK_Dyn;

/*! Dynamic trace data: QM_init_task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_QM_init_task_Dyn;

/*! Dynamic trace data: CanIsr_1 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_Dyn;

/*! Dynamic trace data: CanIsr_2 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_Dyn;

/*! Dynamic trace data: CounterIsr_SystemTimer */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CounterIsr_SystemTimer_Dyn;

/*! Dynamic trace data: DMACH10SR_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DMACH10SR_ISR_Dyn;

/*! Dynamic trace data: DMACH11SR_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DMACH11SR_ISR_Dyn;

/*! Dynamic trace data: GTMTOM1SR0_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_GTMTOM1SR0_ISR_Dyn;

/*! Dynamic trace data: PLC_Interrupt */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_PLC_Interrupt_Dyn;

/*! Dynamic trace data: QSPI1ERR_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_QSPI1ERR_ISR_Dyn;

/*! Dynamic trace data: QSPI1PT_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_QSPI1PT_ISR_Dyn;

/*! Dynamic trace data: QSPI1UD_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_QSPI1UD_ISR_Dyn;

#define OS_STOP_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Trace configuration data: ASILB_MAIN_TASK */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASILB_MAIN_TASK =
{
  /* .Dyn  = */ &OsCfg_Trace_ASILB_MAIN_TASK_Dyn,
  /* .Id   = */ Os_TraceId_ASILB_MAIN_TASK,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: ASILB_init_task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASILB_init_task =
{
  /* .Dyn  = */ &OsCfg_Trace_ASILB_init_task_Dyn,
  /* .Id   = */ Os_TraceId_ASILB_init_task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: Default_BSW_Async_QM_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_BSW_Async_QM_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_Default_BSW_Async_QM_Task_Dyn,
  /* .Id   = */ Os_TraceId_Default_BSW_Async_QM_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: Default_BSW_Async_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_BSW_Async_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_Default_BSW_Async_Task_Dyn,
  /* .Id   = */ Os_TraceId_Default_BSW_Async_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: Default_BSW_Sync_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_BSW_Sync_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_Default_BSW_Sync_Task_Dyn,
  /* .Id   = */ Os_TraceId_Default_BSW_Sync_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: Default_Init_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_Init_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_Default_Init_Task_Dyn,
  /* .Id   = */ Os_TraceId_Default_Init_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: Default_RTE_Mode_switch_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_RTE_Mode_switch_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_Default_RTE_Mode_switch_Task_Dyn,
  /* .Id   = */ Os_TraceId_Default_RTE_Mode_switch_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: IdleTask_OsCore0 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_IdleTask_OsCore0 =
{
  /* .Dyn  = */ &OsCfg_Trace_IdleTask_OsCore0_Dyn,
  /* .Id   = */ Os_TraceId_IdleTask_OsCore0,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: QM_MAIN_TASK */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QM_MAIN_TASK =
{
  /* .Dyn  = */ &OsCfg_Trace_QM_MAIN_TASK_Dyn,
  /* .Id   = */ Os_TraceId_QM_MAIN_TASK,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: QM_init_task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QM_init_task =
{
  /* .Dyn  = */ &OsCfg_Trace_QM_init_task_Dyn,
  /* .Id   = */ Os_TraceId_QM_init_task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: CanIsr_1 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CounterIsr_SystemTimer */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CounterIsr_SystemTimer =
{
  /* .Dyn  = */ &OsCfg_Trace_CounterIsr_SystemTimer_Dyn,
  /* .Id   = */ Os_TraceId_CounterIsr_SystemTimer,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DMACH10SR_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DMACH10SR_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DMACH10SR_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DMACH10SR_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DMACH11SR_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DMACH11SR_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DMACH11SR_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DMACH11SR_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: GTMTOM1SR0_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_GTMTOM1SR0_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_GTMTOM1SR0_ISR_Dyn,
  /* .Id   = */ Os_TraceId_GTMTOM1SR0_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: PLC_Interrupt */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_PLC_Interrupt =
{
  /* .Dyn  = */ &OsCfg_Trace_PLC_Interrupt_Dyn,
  /* .Id   = */ Os_TraceId_PLC_Interrupt,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: QSPI1ERR_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QSPI1ERR_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_QSPI1ERR_ISR_Dyn,
  /* .Id   = */ Os_TraceId_QSPI1ERR_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: QSPI1PT_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QSPI1PT_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_QSPI1PT_ISR_Dyn,
  /* .Id   = */ Os_TraceId_QSPI1PT_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: QSPI1UD_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QSPI1UD_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_QSPI1UD_ISR_Dyn,
  /* .Id   = */ Os_TraceId_QSPI1UD_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Trace_Lcfg.c
 *********************************************************************************************************************/
