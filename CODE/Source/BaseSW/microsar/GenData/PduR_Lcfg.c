/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: PduR
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: PduR_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#define PDUR_LCFG_SOURCE

/**********************************************************************************************************************
 * MISRA / PCLINT JUSTIFICATION
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "PduR.h"
#include "PduR_Fm.h"
#include "PduR_IFQ.h"
#include "PduR_Sm.h"
#include "PduR_RmIf.h"
#include "PduR_RmTp.h"
#include "PduR_RmTp_TxInst.h"
#include "SchM_PduR.h"
#if(PDUR_DEV_ERROR_REPORT == STD_ON)
# include "Det.h"
#endif

/* Include headers with callbacks */
#include "PduR_CanIf.h"
#include "PduR_Com.h"
#include "PduR_CanTp.h"
#include "PduR_Dcm.h"


/**********************************************************************************************************************
 * LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
/* Exclusive Area Wrapper functions */
static FUNC(void, PDUR_CODE) PduR_SchM_Enter_PduR_PDUR_EXCLUSIVE_AREA_0(void);
static FUNC(void, PDUR_CODE) PduR_SchM_Exit_PduR_PDUR_EXCLUSIVE_AREA_0(void);
  
/**********************************************************************************************************************
 * LOCAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * GLOBAL DATA
 *********************************************************************************************************************/
/* \trace SPEC-663, SPEC-661 */

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  PduR_ExclusiveAreaRom
**********************************************************************************************************************/
/** 
  \var    PduR_ExclusiveAreaRom
  \brief  PduR Exclusive Area Locks
  \details
  Element    Description
  Lock       Lock function
  Unlock     Unlock function
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_ExclusiveAreaRomType, PDUR_CONST) PduR_ExclusiveAreaRom[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Lock                                        Unlock                                           Referable Keys */
  { /*     0 */ PduR_SchM_Enter_PduR_PDUR_EXCLUSIVE_AREA_0, PduR_SchM_Exit_PduR_PDUR_EXCLUSIVE_AREA_0 }   /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRLock_PduRExclusiveArea] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_MmRom
**********************************************************************************************************************/
/** 
  \var    PduR_MmRom
  \brief  Module manager: Contains all function pointers of the bordering modules.
  \details
  Element                         Description
  DestApplicationManagerRomIdx    the index of the 1:1 relation pointing to PduR_DestApplicationManagerRom
  MaskedBits                      contains bitcoded the boolean data of PduR_LoIfOfMmRom, PduR_LoTpOfMmRom, PduR_RmGDestRomUsedOfMmRom, PduR_UpIfOfMmRom, PduR_UpTpOfMmRom
  RmGDestRomEndIdx                the end index of the 0:n relation pointing to PduR_RmGDestRom
  RmGDestRomStartIdx              the start index of the 0:n relation pointing to PduR_RmGDestRom
  UpTpCopyRxDataFctPtr            Transport protocol CopyRxData function pointers
  UpTpCopyTxDataFctPtr            Transport protocol CopyTxData function pointers
  UpIfRxIndicationFctPtr          Upper layer communication interface Rx indication function pointers.
  UpIfTxConfirmationFctPtr        Upper layer communication interface Tx confimation function pointers
  UpTpStartOfReceptionFctPtr      Transport protocol StartOfReception function pointers
  UpTpTpRxIndicationFctPtr        Transport protocol TpRxIndication function pointers
  UpTpTpTxConfirmationFctPtr      Transport protocol TpTxConfimation function pointers
  LoIfTransmitFctPtr              Lower layer If transmit function pointers
  LoTpTransmitFctPtr              Lower layer Tp transmit function pointers
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_MmRomType, PDUR_CONST) PduR_MmRom[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    DestApplicationManagerRomIdx  MaskedBits  RmGDestRomEndIdx  RmGDestRomStartIdx  UpTpCopyRxDataFctPtr  UpTpCopyTxDataFctPtr  UpIfRxIndicationFctPtr  UpIfTxConfirmationFctPtr  UpTpStartOfReceptionFctPtr  UpTpTpRxIndicationFctPtr  UpTpTpTxConfirmationFctPtr  LoIfTransmitFctPtr  LoTpTransmitFctPtr        Comment                        Referable Keys */
  { /*     0 */                           0u,      0x14u,              17u,                 0u, NULL_PTR            , NULL_PTR            , NULL_PTR              , NULL_PTR                , NULL_PTR                  , NULL_PTR                , NULL_PTR                  , CanIf_Transmit    , NULL_PTR           },  /* [BswModule: CanIf] */  /* [/ActiveEcuC/PduR/CanIf, /ActiveEcuC/PduR] */
  { /*     1 */                           0u,      0x0Cu,              18u,                17u, NULL_PTR            , NULL_PTR            , NULL_PTR              , NULL_PTR                , NULL_PTR                  , NULL_PTR                , NULL_PTR                  , NULL_PTR          , CanTp_Transmit     },  /* [BswModule: CanTp] */  /* [/ActiveEcuC/PduR/CanTp, /ActiveEcuC/PduR] */
  { /*     2 */                           0u,      0x06u,              51u,                18u, NULL_PTR            , NULL_PTR            , Com_RxIndication      , Com_TxConfirmation      , NULL_PTR                  , NULL_PTR                , NULL_PTR                  , NULL_PTR          , NULL_PTR           },  /* [BswModule: Com]   */  /* [/ActiveEcuC/PduR/Com, /ActiveEcuC/PduR] */
  { /*     3 */                           0u,      0x05u,              53u,                51u, Dcm_CopyRxData      , Dcm_CopyTxData      , NULL_PTR              , NULL_PTR                , Dcm_StartOfReception      , Dcm_TpRxIndication      , Dcm_TpTxConfirmation      , NULL_PTR          , NULL_PTR           }   /* [BswModule: Dcm]   */  /* [/ActiveEcuC/PduR/Dcm, /ActiveEcuC/PduR] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmDestRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmDestRom
  \brief  PduR RoutiongPathManager destPdu Table
  \details
  Element          Description
  RmGDestRomIdx    the index of the 1:1 relation pointing to PduR_RmGDestRom
  RmSrcRomIdx      the index of the 1:1 relation pointing to PduR_RmSrcRom
  RoutingType      Type of the Routing (API Forwarding, Gateway).
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_RmDestRomType, PDUR_CONST) PduR_RmDestRom[53] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RmGDestRomIdx  RmSrcRomIdx  RoutingType                                                 Comment                                                                           Referable Keys */
  { /*     0 */            9u,         44u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: OBC3_oE_CAN_947c3ff2_Tx]                                */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC3_oE_CAN_cbd8979f_Tx/PduRSrcPdu_0f56ae96, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC3_oE_CAN_cbd8979f_Tx/OBC3_oE_CAN_947c3ff2_Tx] */
  { /*     1 */            0u,         35u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DC1_oE_CAN_f87e7579_Tx]                                 */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC1_oE_CAN_fcac9ee4_Tx/PduRSrcPdu_1224da83, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC1_oE_CAN_fcac9ee4_Tx/DC1_oE_CAN_f87e7579_Tx] */
  { /*     2 */           37u,         19u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: PFC_Status2_oInt_CAN_6fc7830f_Rx_13c131e4_Rx]           */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status2_oInt_CAN_6fc7830f_Rx/PduRSrcPdu_13c131e4, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status2_oInt_CAN_6fc7830f_Rx/PFC_Status2_oInt_CAN_6fc7830f_Rx_13c131e4_Rx] */
  { /*     3 */           49u,         31u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VCU_TU_oE_CAN_bad58dcd_Rx_141e34ce_Rx]                  */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_TU_oE_CAN_bad58dcd_Rx/PduRSrcPdu_141e34ce, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_TU_oE_CAN_bad58dcd_Rx/VCU_TU_oE_CAN_bad58dcd_Rx_141e34ce_Rx] */
  { /*     4 */           51u,         33u, PDUR_TP_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: REQ_DIAG_ON_CAN_oE_CAN_0be358b0_Rx_1707827c_Rx]         */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REQ_DIAG_ON_CAN_oE_CAN_0be358b0_Rx/PduRSrcPdu_1707827c, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REQ_DIAG_ON_CAN_oE_CAN_0be358b0_Rx/REQ_DIAG_ON_CAN_oE_CAN_0be358b0_Rx_1707827c_Rx] */
  { /*     5 */           13u,         49u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx]                 */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx/PduRSrcPdu_17d4e376, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx/SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx] */
  { /*     6 */           33u,         15u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: ELECTRON_BSI_oE_CAN_61376617_Rx_182f4c9b_Rx]            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ELECTRON_BSI_oE_CAN_61376617_Rx/PduRSrcPdu_182f4c9b, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ELECTRON_BSI_oE_CAN_61376617_Rx/ELECTRON_BSI_oE_CAN_61376617_Rx_182f4c9b_Rx] */
  { /*     7 */           46u,         28u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VCU3_oE_CAN_e6c752c2_Rx_2def7208_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU3_oE_CAN_e6c752c2_Rx/PduRSrcPdu_2def7208, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU3_oE_CAN_e6c752c2_Rx/VCU3_oE_CAN_e6c752c2_Rx_2def7208_Rx] */
  { /*     8 */            6u,         41u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx/PduRSrcPdu_2e76203d, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx/NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx] */
  { /*     9 */           25u,          7u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: CtrlDCDC_oE_CAN_8127de91_Rx_2ed93c0e_Rx]                */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/CtrlDCDC_oE_CAN_8127de91_Rx/PduRSrcPdu_2ed93c0e, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/CtrlDCDC_oE_CAN_8127de91_Rx/CtrlDCDC_oE_CAN_8127de91_Rx_2ed93c0e_Rx] */
  { /*    10 */           10u,         45u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: OBC4_oE_CAN_e2c5a75c_Tx]                                */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC4_oE_CAN_c11d9e86_Tx/PduRSrcPdu_31565810, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC4_oE_CAN_c11d9e86_Tx/OBC4_oE_CAN_e2c5a75c_Tx] */
  { /*    11 */           29u,         11u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCLV_Fault_oInt_CAN_cb576812_Rx_3259f79e_Rx]            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Fault_oInt_CAN_cb576812_Rx/PduRSrcPdu_3259f79e, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Fault_oInt_CAN_cb576812_Rx/DCLV_Fault_oInt_CAN_cb576812_Rx_3259f79e_Rx] */
  { /*    12 */           14u,         50u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx]                 */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx/PduRSrcPdu_36c239fa, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx/SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx] */
  { /*    13 */           35u,         17u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: PFC_Fault_oInt_CAN_63f22496_Rx_3c5c1cb6_Rx]             */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Fault_oInt_CAN_63f22496_Rx/PduRSrcPdu_3c5c1cb6, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Fault_oInt_CAN_63f22496_Rx/PFC_Fault_oInt_CAN_63f22496_Rx_3c5c1cb6_Rx] */
  { /*    14 */           23u,          5u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BMS9_oE_CAN_7cc3a2ea_Rx_3c9c33f4_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS9_oE_CAN_7cc3a2ea_Rx/PduRSrcPdu_3c9c33f4, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS9_oE_CAN_7cc3a2ea_Rx/BMS9_oE_CAN_7cc3a2ea_Rx_3c9c33f4_Rx] */
  { /*    15 */           36u,         18u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: PFC_Status1_oInt_CAN_dc53aecc_Rx_4db707a4_Rx]           */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status1_oInt_CAN_dc53aecc_Rx/PduRSrcPdu_4db707a4, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status1_oInt_CAN_dc53aecc_Rx/PFC_Status1_oInt_CAN_dc53aecc_Rx_4db707a4_Rx] */
  { /*    16 */           41u,         23u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: ParkCommand_oE_CAN_16da6a87_Rx_50e0f311_Rx]             */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ParkCommand_oE_CAN_16da6a87_Rx/PduRSrcPdu_50e0f311, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ParkCommand_oE_CAN_16da6a87_Rx/ParkCommand_oE_CAN_16da6a87_Rx_50e0f311_Rx] */
  { /*    17 */            8u,         43u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: OBC2_oE_CAN_0f0fd526_Tx]                                */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC2_oE_CAN_07729701_Tx/PduRSrcPdu_558557e8, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC2_oE_CAN_07729701_Tx/OBC2_oE_CAN_0f0fd526_Tx] */
  { /*    18 */            7u,         42u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: OBC1_oE_CAN_79eaec1b_Tx]                                */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC1_oE_CAN_89fd90e2_Tx/PduRSrcPdu_5705bb4d, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC1_oE_CAN_89fd90e2_Tx/OBC1_oE_CAN_79eaec1b_Tx] */
  { /*    19 */           52u,         34u, PDUR_TP_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: UDS_ON_CAN_oE_CAN_a9f0043e_Rx_59769882_Rx]              */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/UDS_ON_CAN_oE_CAN_a9f0043e_Rx/PduRSrcPdu_59769882, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/UDS_ON_CAN_oE_CAN_a9f0043e_Rx/UDS_ON_CAN_oE_CAN_a9f0043e_Rx_59769882_Rx] */
  { /*    20 */           20u,          2u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BMS5_oE_CAN_7c26405f_Rx_5b82c527_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS5_oE_CAN_7c26405f_Rx/PduRSrcPdu_5b82c527, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS5_oE_CAN_7c26405f_Rx/BMS5_oE_CAN_7c26405f_Rx_5b82c527_Rx] */
  { /*    21 */           32u,         14u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCLV_Status3_oInt_CAN_21ce601f_Rx_6f865e62_Rx]          */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status3_oInt_CAN_21ce601f_Rx/PduRSrcPdu_6f865e62, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status3_oInt_CAN_21ce601f_Rx/DCLV_Status3_oInt_CAN_21ce601f_Rx_6f865e62_Rx] */
  { /*    22 */           47u,         29u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VCU_BSI_Wakeup_oE_CAN_79c55f53_Rx_771f59ac_Rx]          */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_BSI_Wakeup_oE_CAN_79c55f53_Rx/PduRSrcPdu_771f59ac, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_BSI_Wakeup_oE_CAN_79c55f53_Rx/VCU_BSI_Wakeup_oE_CAN_79c55f53_Rx_771f59ac_Rx] */
  { /*    23 */           44u,         26u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: ReqToOBC_oE_CAN_45858201_Rx_7e41dab9_Rx]                */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ReqToOBC_oE_CAN_45858201_Rx/PduRSrcPdu_7e41dab9, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ReqToOBC_oE_CAN_45858201_Rx/ReqToOBC_oE_CAN_45858201_Rx_7e41dab9_Rx] */
  { /*    24 */           31u,         13u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCLV_Status2_oInt_CAN_4f427b5e_Rx_820710d6_Rx]          */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status2_oInt_CAN_4f427b5e_Rx/PduRSrcPdu_820710d6, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status2_oInt_CAN_4f427b5e_Rx/DCLV_Status2_oInt_CAN_4f427b5e_Rx_820710d6_Rx] */
  { /*    25 */           39u,         21u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: PFC_Status4_oInt_CAN_d39edec8_Rx_8463d87d_Rx]           */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status4_oInt_CAN_d39edec8_Rx/PduRSrcPdu_8463d87d, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status4_oInt_CAN_d39edec8_Rx/PFC_Status4_oInt_CAN_d39edec8_Rx_8463d87d_Rx] */
  { /*    26 */           24u,          6u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BSIInfo_oE_CAN_0ff52381_Rx_882b1898_Rx]                 */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BSIInfo_oE_CAN_0ff52381_Rx/PduRSrcPdu_882b1898, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BSIInfo_oE_CAN_0ff52381_Rx/BSIInfo_oE_CAN_0ff52381_Rx_882b1898_Rx] */
  { /*    27 */           16u,         52u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VERS_OBC_DCDC_oE_CAN_336be5e5_Tx]                       */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx/PduRSrcPdu_8aa49178, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx/VERS_OBC_DCDC_oE_CAN_336be5e5_Tx] */
  { /*    28 */           27u,          9u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCHV_Status1_oInt_CAN_c4860e0a_Rx_91da6bff_Rx]          */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Status1_oInt_CAN_c4860e0a_Rx/PduRSrcPdu_91da6bff, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Status1_oInt_CAN_c4860e0a_Rx/DCHV_Status1_oInt_CAN_c4860e0a_Rx_91da6bff_Rx] */
  { /*    29 */           38u,         20u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: PFC_Status3_oInt_CAN_014b984e_Rx_97e42112_Rx]           */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status3_oInt_CAN_014b984e_Rx/PduRSrcPdu_97e42112, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status3_oInt_CAN_014b984e_Rx/PFC_Status3_oInt_CAN_014b984e_Rx_97e42112_Rx] */
  { /*    30 */           42u,         24u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: ProgTool_SupEnterBoot_oInt_CAN_39bdfe09_Rx_9b0aaf50_Rx] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ProgTool_SupEnterBoot_oInt_CAN_39bdfe09_Rx/PduRSrcPdu_9b0aaf50, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ProgTool_SupEnterBoot_oInt_CAN_39bdfe09_Rx/ProgTool_SupEnterBoot_oInt_CAN_39bdfe09_Rx_9b0aaf50_Rx] */
  { /*    31 */           28u,         10u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCHV_Status2_oInt_CAN_771223c9_Rx_9f59032e_Rx]          */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Status2_oInt_CAN_771223c9_Rx/PduRSrcPdu_9f59032e, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Status2_oInt_CAN_771223c9_Rx/DCHV_Status2_oInt_CAN_771223c9_Rx_9f59032e_Rx] */
  { /*    32 */           34u,         16u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: NEW_JDD_oE_CAN_4540bd6c_Rx_a0d1622f_Rx]                 */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_oE_CAN_4540bd6c_Rx/PduRSrcPdu_a0d1622f, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_oE_CAN_4540bd6c_Rx/NEW_JDD_oE_CAN_4540bd6c_Rx_a0d1622f_Rx] */
  { /*    33 */           12u,         48u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx/PduRSrcPdu_a0eb6b35, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx/SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx] */
  { /*    34 */           11u,         46u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: OBCTxDiag_oE_CAN_794ae2be_Tx]                           */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBCTxDiag_oE_CAN_98fb5e1b_Tx/PduRSrcPdu_aa4ba89d, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBCTxDiag_oE_CAN_98fb5e1b_Tx/OBCTxDiag_oE_CAN_794ae2be_Tx] */
  { /*    35 */           26u,          8u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCHV_Fault_oInt_CAN_595e2cca_Rx_aaa6ebec_Rx]            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Fault_oInt_CAN_595e2cca_Rx/PduRSrcPdu_aaa6ebec, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Fault_oInt_CAN_595e2cca_Rx/DCHV_Fault_oInt_CAN_595e2cca_Rx_aaa6ebec_Rx] */
  { /*    36 */            5u,         40u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: Debug4_oInt_CAN_5edfb5ff_Tx]                            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug4_oInt_CAN_f1561a72_Tx/PduRSrcPdu_af12cdd2, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug4_oInt_CAN_f1561a72_Tx/Debug4_oInt_CAN_5edfb5ff_Tx] */
  { /*    37 */           50u,         32u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VCU_oE_CAN_377cc071_Rx_b485c90c_Rx]                     */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_oE_CAN_377cc071_Rx/PduRSrcPdu_b485c90c, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_oE_CAN_377cc071_Rx/VCU_oE_CAN_377cc071_Rx_b485c90c_Rx] */
  { /*    38 */           43u,         25u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: ReqToECANFunction_oE_CAN_abe814c4_Rx_b756064f_Rx]       */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ReqToECANFunction_oE_CAN_abe814c4_Rx/PduRSrcPdu_b756064f, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ReqToECANFunction_oE_CAN_abe814c4_Rx/ReqToECANFunction_oE_CAN_abe814c4_Rx_b756064f_Rx] */
  { /*    39 */           22u,          4u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BMS8_oE_CAN_e7b0483e_Rx_b865118f_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS8_oE_CAN_e7b0483e_Rx/PduRSrcPdu_b865118f, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS8_oE_CAN_e7b0483e_Rx/BMS8_oE_CAN_e7b0483e_Rx_b865118f_Rx] */
  { /*    40 */            2u,         37u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: Debug1_oInt_CAN_5112c5fb_Tx]                            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug1_oInt_CAN_177fd136_Tx/PduRSrcPdu_c451a6c2, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug1_oInt_CAN_177fd136_Tx/Debug1_oInt_CAN_5112c5fb_Tx] */
  { /*    41 */           45u,         27u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VCU2_oE_CAN_7db4b816_Rx_c61797dd_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU2_oE_CAN_7db4b816_Rx/PduRSrcPdu_c61797dd, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU2_oE_CAN_7db4b816_Rx/VCU2_oE_CAN_7db4b816_Rx_c61797dd_Rx] */
  { /*    42 */           48u,         30u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: VCU_PCANInfo_oE_CAN_1242d2f8_Rx_c80fbc21_Rx]            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_PCANInfo_oE_CAN_1242d2f8_Rx/PduRSrcPdu_c80fbc21, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_PCANInfo_oE_CAN_1242d2f8_Rx/VCU_PCANInfo_oE_CAN_1242d2f8_Rx_c80fbc21_Rx] */
  { /*    43 */           18u,          0u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BMS1_oE_CAN_7c7ae1cc_Rx_ce686a11_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS1_oE_CAN_7c7ae1cc_Rx/PduRSrcPdu_ce686a11, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS1_oE_CAN_7c7ae1cc_Rx/BMS1_oE_CAN_7c7ae1cc_Rx_ce686a11_Rx] */
  { /*    44 */            3u,         38u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: Debug2_oInt_CAN_e286e838_Tx]                            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug2_oInt_CAN_fc486a35_Tx/PduRSrcPdu_cf0367a1, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug2_oInt_CAN_fc486a35_Tx/Debug2_oInt_CAN_e286e838_Tx] */
  { /*    45 */            1u,         36u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DC2_oE_CAN_8e9b4c44_Tx]                                 */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC2_oE_CAN_72239907_Tx/PduRSrcPdu_d598d177, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC2_oE_CAN_72239907_Tx/DC2_oE_CAN_8e9b4c44_Tx] */
  { /*    46 */           19u,          1u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BMS3_oE_CAN_91ec3225_Rx_d8341745_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS3_oE_CAN_91ec3225_Rx/PduRSrcPdu_d8341745, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS3_oE_CAN_91ec3225_Rx/BMS3_oE_CAN_91ec3225_Rx_d8341745_Rx] */
  { /*    47 */           21u,          3u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: BMS6_oE_CAN_0ac37962_Rx_e104bf72_Rx]                    */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS6_oE_CAN_0ac37962_Rx/PduRSrcPdu_e104bf72, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS6_oE_CAN_0ac37962_Rx/BMS6_oE_CAN_0ac37962_Rx_e104bf72_Rx] */
  { /*    48 */            4u,         39u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: Debug3_oInt_CAN_8c0af379_Tx]                            */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug3_oInt_CAN_138a010b_Tx/PduRSrcPdu_e2c32671, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug3_oInt_CAN_138a010b_Tx/Debug3_oInt_CAN_8c0af379_Tx] */
  { /*    49 */           17u,         47u, PDUR_TP_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: REP_DIAG_ON_CAN_oE_CAN_c47d4f78_Tx]                     */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REP_DIAG_ON_CAN_oE_CAN_81c96575_Tx/PduRSrcPdu_f0e620d6, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REP_DIAG_ON_CAN_oE_CAN_81c96575_Tx/REP_DIAG_ON_CAN_oE_CAN_c47d4f78_Tx] */
    /* Index    RmGDestRomIdx  RmSrcRomIdx  RoutingType                                                 Comment                                                                           Referable Keys */
  { /*    50 */           30u,         12u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: DCLV_Status1_oInt_CAN_fcd6569d_Rx_f6508c17_Rx]          */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status1_oInt_CAN_fcd6569d_Rx/PduRSrcPdu_f6508c17, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status1_oInt_CAN_fcd6569d_Rx/DCLV_Status1_oInt_CAN_fcd6569d_Rx_f6508c17_Rx] */
  { /*    51 */           15u,         51u, PDUR_IF_UNBUFFERED_TX_API_FWD_ROUTINGTYPEOFRMDESTROM },  /* [PduRDestPdu: SUP_CommandToPFC_oInt_CAN_11d75aca_Tx]                  */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx/PduRSrcPdu_f7c07c35, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx/SUP_CommandToPFC_oInt_CAN_11d75aca_Tx] */
  { /*    52 */           40u,         22u, PDUR_IF_UNBUFFERED_RX_API_FWD_ROUTINGTYPEOFRMDESTROM }   /* [PduRDestPdu: PFC_Status5_oInt_CAN_bd12c589_Rx_fa5b1cce_Rx]           */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status5_oInt_CAN_bd12c589_Rx/PduRSrcPdu_fa5b1cce, /ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status5_oInt_CAN_bd12c589_Rx/PFC_Status5_oInt_CAN_bd12c589_Rx_fa5b1cce_Rx] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmGDestRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmGDestRom
  \brief  PduR RoutiongPathManager global destPdu Table
  \details
  Element                  Description
  DestHnd                  handle to be used as parameter for the StartOfReception, CopyRxData, Transmit or RxIndication function call.
  Direction                Direction of this Pdu: Rx or Tx
  MmRomIdx                 the index of the 1:1 relation pointing to PduR_MmRom
  PduRDestPduProcessing    Is Processing Type of destination PDU.
  RmDestRomIdx             the index of the 1:1 relation pointing to PduR_RmDestRom
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_RmGDestRomType, PDUR_CONST) PduR_RmGDestRom[53] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    DestHnd                                                               Direction                      MmRomIdx  PduRDestPduProcessing                             RmDestRomIdx        Comment                                                                       Referable Keys */
  { /*     0 */                       CanIfConf_CanIfTxPduCfg_DC1_oE_CAN_0bfdcb0d_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           1u },  /* [Globale PduRDestPdu: DC1_oE_CAN_f87e7579_Tx]                     */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DC1_oE_CAN_f87e7579_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     1 */                       CanIfConf_CanIfTxPduCfg_DC2_oE_CAN_1927a4ae_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          45u },  /* [Globale PduRDestPdu: DC2_oE_CAN_8e9b4c44_Tx]                     */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DC2_oE_CAN_8e9b4c44_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     2 */                  CanIfConf_CanIfTxPduCfg_Debug1_oInt_CAN_1d48e037_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          40u },  /* [Globale PduRDestPdu: Debug1_oInt_CAN_5112c5fb_Tx]                */  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug1_oInt_CAN_5112c5fb_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     3 */                  CanIfConf_CanIfTxPduCfg_Debug2_oInt_CAN_bc7a3afe_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          44u },  /* [Globale PduRDestPdu: Debug2_oInt_CAN_e286e838_Tx]                */  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug2_oInt_CAN_e286e838_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     4 */                  CanIfConf_CanIfTxPduCfg_Debug3_oInt_CAN_6a448e86_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          48u },  /* [Globale PduRDestPdu: Debug3_oInt_CAN_8c0af379_Tx]                */  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug3_oInt_CAN_8c0af379_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     5 */                  CanIfConf_CanIfTxPduCfg_Debug4_oInt_CAN_256e892d_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          36u },  /* [Globale PduRDestPdu: Debug4_oInt_CAN_5edfb5ff_Tx]                */  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug4_oInt_CAN_5edfb5ff_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     6 */          CanIfConf_CanIfTxPduCfg_NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           8u },  /* [Globale PduRDestPdu: NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx]        */  /* [/ActiveEcuC/EcuC/EcucPduCollection/NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     7 */                      CanIfConf_CanIfTxPduCfg_OBC1_oE_CAN_c97caff2_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          18u },  /* [Globale PduRDestPdu: OBC1_oE_CAN_79eaec1b_Tx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC1_oE_CAN_79eaec1b_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     8 */                      CanIfConf_CanIfTxPduCfg_OBC2_oE_CAN_349585b6_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          17u },  /* [Globale PduRDestPdu: OBC2_oE_CAN_0f0fd526_Tx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC2_oE_CAN_0f0fd526_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*     9 */                      CanIfConf_CanIfTxPduCfg_OBC3_oE_CAN_d6e261b5_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           0u },  /* [Globale PduRDestPdu: OBC3_oE_CAN_947c3ff2_Tx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC3_oE_CAN_947c3ff2_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    10 */                      CanIfConf_CanIfTxPduCfg_OBC4_oE_CAN_1436d77f_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          10u },  /* [Globale PduRDestPdu: OBC4_oE_CAN_e2c5a75c_Tx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC4_oE_CAN_e2c5a75c_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    11 */                 CanIfConf_CanIfTxPduCfg_OBCTxDiag_oE_CAN_d6e7f536_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          34u },  /* [Globale PduRDestPdu: OBCTxDiag_oE_CAN_794ae2be_Tx]               */  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBCTxDiag_oE_CAN_794ae2be_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    12 */          CanIfConf_CanIfTxPduCfg_SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          33u },  /* [Globale PduRDestPdu: SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx]        */  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    13 */       CanIfConf_CanIfTxPduCfg_SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           5u },  /* [Globale PduRDestPdu: SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx]     */  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    14 */       CanIfConf_CanIfTxPduCfg_SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          12u },  /* [Globale PduRDestPdu: SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx]     */  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    15 */        CanIfConf_CanIfTxPduCfg_SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          51u },  /* [Globale PduRDestPdu: SUP_CommandToPFC_oInt_CAN_11d75aca_Tx]      */  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUP_CommandToPFC_oInt_CAN_11d75aca_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    16 */             CanIfConf_CanIfTxPduCfg_VERS_OBC_DCDC_oE_CAN_86d6c928_Tx, PDUR_TX_DIRECTIONOFRMGDESTROM,       0u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          27u },  /* [Globale PduRDestPdu: VERS_OBC_DCDC_oE_CAN_336be5e5_Tx]           */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VERS_OBC_DCDC_oE_CAN_336be5e5_Tx, /ActiveEcuC/PduR/CanIf] */
  { /*    17 */                           CanTpConf_CanTpTxNSdu_CanTpTxNSdu_b7d3e822, PDUR_TX_DIRECTIONOFRMGDESTROM,       1u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          49u },  /* [Globale PduRDestPdu: REP_DIAG_ON_CAN_oE_CAN_c47d4f78_Tx]         */  /* [/ActiveEcuC/EcuC/EcucPduCollection/REP_DIAG_ON_CAN_oE_CAN_c47d4f78_Tx, /ActiveEcuC/PduR/CanTp] */
  { /*    18 */                              ComConf_ComIPdu_BMS1_oE_CAN_1fa2c419_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          43u },  /* [Globale PduRDestPdu: BMS1_oE_CAN_1fa2c419_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BMS1_oE_CAN_1fa2c419_Rx, /ActiveEcuC/PduR/Com] */
  { /*    19 */                              ComConf_ComIPdu_BMS3_oE_CAN_5d87c364_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          46u },  /* [Globale PduRDestPdu: BMS3_oE_CAN_5d87c364_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BMS3_oE_CAN_5d87c364_Rx, /ActiveEcuC/PduR/Com] */
  { /*    20 */                              ComConf_ComIPdu_BMS5_oE_CAN_9be8cae3_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          20u },  /* [Globale PduRDestPdu: BMS5_oE_CAN_9be8cae3_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BMS5_oE_CAN_9be8cae3_Rx, /ActiveEcuC/PduR/Com] */
  { /*    21 */                              ComConf_ComIPdu_BMS6_oE_CAN_1567cd00_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          47u },  /* [Globale PduRDestPdu: BMS6_oE_CAN_1567cd00_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BMS6_oE_CAN_1567cd00_Rx, /ActiveEcuC/PduR/Com] */
  { /*    22 */                              ComConf_ComIPdu_BMS8_oE_CAN_00eddf32_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          39u },  /* [Globale PduRDestPdu: BMS8_oE_CAN_00eddf32_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BMS8_oE_CAN_00eddf32_Rx, /ActiveEcuC/PduR/Com] */
  { /*    23 */                              ComConf_ComIPdu_BMS9_oE_CAN_cc47dfac_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          14u },  /* [Globale PduRDestPdu: BMS9_oE_CAN_cc47dfac_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BMS9_oE_CAN_cc47dfac_Rx, /ActiveEcuC/PduR/Com] */
  { /*    24 */                           ComConf_ComIPdu_BSIInfo_oE_CAN_b362dec3_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          26u },  /* [Globale PduRDestPdu: BSIInfo_oE_CAN_b362dec3_Rx]                 */  /* [/ActiveEcuC/EcuC/EcucPduCollection/BSIInfo_oE_CAN_b362dec3_Rx, /ActiveEcuC/PduR/Com] */
  { /*    25 */                          ComConf_ComIPdu_CtrlDCDC_oE_CAN_6ce88e20_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           9u },  /* [Globale PduRDestPdu: CtrlDCDC_oE_CAN_6ce88e20_Rx]                */  /* [/ActiveEcuC/EcuC/EcucPduCollection/CtrlDCDC_oE_CAN_6ce88e20_Rx, /ActiveEcuC/PduR/Com] */
  { /*    26 */                      ComConf_ComIPdu_DCHV_Fault_oInt_CAN_d7c47a53_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          35u },  /* [Globale PduRDestPdu: DCHV_Fault_oInt_CAN_d7c47a53_Rx]            */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCHV_Fault_oInt_CAN_d7c47a53_Rx, /ActiveEcuC/PduR/Com] */
  { /*    27 */                    ComConf_ComIPdu_DCHV_Status1_oInt_CAN_6c1b5ec3_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          28u },  /* [Globale PduRDestPdu: DCHV_Status1_oInt_CAN_6c1b5ec3_Rx]          */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx, /ActiveEcuC/PduR/Com] */
  { /*    28 */                    ComConf_ComIPdu_DCHV_Status2_oInt_CAN_872ce5c0_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          31u },  /* [Globale PduRDestPdu: DCHV_Status2_oInt_CAN_872ce5c0_Rx]          */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCHV_Status2_oInt_CAN_872ce5c0_Rx, /ActiveEcuC/PduR/Com] */
  { /*    29 */                      ComConf_ComIPdu_DCLV_Fault_oInt_CAN_a4cc5d9c_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          11u },  /* [Globale PduRDestPdu: DCLV_Fault_oInt_CAN_a4cc5d9c_Rx]            */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx, /ActiveEcuC/PduR/Com] */
  { /*    30 */                    ComConf_ComIPdu_DCLV_Status1_oInt_CAN_85735321_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          50u },  /* [Globale PduRDestPdu: DCLV_Status1_oInt_CAN_85735321_Rx]          */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCLV_Status1_oInt_CAN_85735321_Rx, /ActiveEcuC/PduR/Com] */
  { /*    31 */                    ComConf_ComIPdu_DCLV_Status2_oInt_CAN_6e44e822_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          24u },  /* [Globale PduRDestPdu: DCLV_Status2_oInt_CAN_6e44e822_Rx]          */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCLV_Status2_oInt_CAN_6e44e822_Rx, /ActiveEcuC/PduR/Com] */
  { /*    32 */                    ComConf_ComIPdu_DCLV_Status3_oInt_CAN_8186831c_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          21u },  /* [Globale PduRDestPdu: DCLV_Status3_oInt_CAN_8186831c_Rx]          */  /* [/ActiveEcuC/EcuC/EcucPduCollection/DCLV_Status3_oInt_CAN_8186831c_Rx, /ActiveEcuC/PduR/Com] */
  { /*    33 */                      ComConf_ComIPdu_ELECTRON_BSI_oE_CAN_ab0b72ff_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           6u },  /* [Globale PduRDestPdu: ELECTRON_BSI_oE_CAN_ab0b72ff_Rx]            */  /* [/ActiveEcuC/EcuC/EcucPduCollection/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx, /ActiveEcuC/PduR/Com] */
  { /*    34 */                           ComConf_ComIPdu_NEW_JDD_oE_CAN_60b2710c_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          32u },  /* [Globale PduRDestPdu: NEW_JDD_oE_CAN_60b2710c_Rx]                 */  /* [/ActiveEcuC/EcuC/EcucPduCollection/NEW_JDD_oE_CAN_60b2710c_Rx, /ActiveEcuC/PduR/Com] */
  { /*    35 */                       ComConf_ComIPdu_PFC_Fault_oInt_CAN_0a8001a9_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          13u },  /* [Globale PduRDestPdu: PFC_Fault_oInt_CAN_0a8001a9_Rx]             */  /* [/ActiveEcuC/EcuC/EcucPduCollection/PFC_Fault_oInt_CAN_0a8001a9_Rx, /ActiveEcuC/PduR/Com] */
  { /*    36 */                     ComConf_ComIPdu_PFC_Status1_oInt_CAN_459f2204_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          15u },  /* [Globale PduRDestPdu: PFC_Status1_oInt_CAN_459f2204_Rx]           */  /* [/ActiveEcuC/EcuC/EcucPduCollection/PFC_Status1_oInt_CAN_459f2204_Rx, /ActiveEcuC/PduR/Com] */
  { /*    37 */                     ComConf_ComIPdu_PFC_Status2_oInt_CAN_aea89907_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           2u },  /* [Globale PduRDestPdu: PFC_Status2_oInt_CAN_aea89907_Rx]           */  /* [/ActiveEcuC/EcuC/EcucPduCollection/PFC_Status2_oInt_CAN_aea89907_Rx, /ActiveEcuC/PduR/Com] */
  { /*    38 */                     ComConf_ComIPdu_PFC_Status3_oInt_CAN_416af239_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          29u },  /* [Globale PduRDestPdu: PFC_Status3_oInt_CAN_416af239_Rx]           */  /* [/ActiveEcuC/EcuC/EcucPduCollection/PFC_Status3_oInt_CAN_416af239_Rx, /ActiveEcuC/PduR/Com] */
  { /*    39 */                     ComConf_ComIPdu_PFC_Status4_oInt_CAN_a3b6e940_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          25u },  /* [Globale PduRDestPdu: PFC_Status4_oInt_CAN_a3b6e940_Rx]           */  /* [/ActiveEcuC/EcuC/EcucPduCollection/PFC_Status4_oInt_CAN_a3b6e940_Rx, /ActiveEcuC/PduR/Com] */
  { /*    40 */                     ComConf_ComIPdu_PFC_Status5_oInt_CAN_4c74827e_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          52u },  /* [Globale PduRDestPdu: PFC_Status5_oInt_CAN_4c74827e_Rx]           */  /* [/ActiveEcuC/EcuC/EcucPduCollection/PFC_Status5_oInt_CAN_4c74827e_Rx, /ActiveEcuC/PduR/Com] */
  { /*    41 */                       ComConf_ComIPdu_ParkCommand_oE_CAN_5e7110cb_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          16u },  /* [Globale PduRDestPdu: ParkCommand_oE_CAN_5e7110cb_Rx]             */  /* [/ActiveEcuC/EcuC/EcucPduCollection/ParkCommand_oE_CAN_5e7110cb_Rx, /ActiveEcuC/PduR/Com] */
  { /*    42 */           ComConf_ComIPdu_ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          30u },  /* [Globale PduRDestPdu: ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */  /* [/ActiveEcuC/EcuC/EcucPduCollection/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx, /ActiveEcuC/PduR/Com] */
  { /*    43 */                 ComConf_ComIPdu_ReqToECANFunction_oE_CAN_01f67ab2_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          38u },  /* [Globale PduRDestPdu: ReqToECANFunction_oE_CAN_01f67ab2_Rx]       */  /* [/ActiveEcuC/EcuC/EcucPduCollection/ReqToECANFunction_oE_CAN_01f67ab2_Rx, /ActiveEcuC/PduR/Com] */
  { /*    44 */                          ComConf_ComIPdu_ReqToOBC_oE_CAN_09b8aa3e_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          23u },  /* [Globale PduRDestPdu: ReqToOBC_oE_CAN_09b8aa3e_Rx]                */  /* [/ActiveEcuC/EcuC/EcucPduCollection/ReqToOBC_oE_CAN_09b8aa3e_Rx, /ActiveEcuC/PduR/Com] */
  { /*    45 */                              ComConf_ComIPdu_VCU2_oE_CAN_e2d13d8a_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          41u },  /* [Globale PduRDestPdu: VCU2_oE_CAN_e2d13d8a_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VCU2_oE_CAN_e2d13d8a_Rx, /ActiveEcuC/PduR/Com] */
  { /*    46 */                              ComConf_ComIPdu_VCU3_oE_CAN_2e7b3d14_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           7u },  /* [Globale PduRDestPdu: VCU3_oE_CAN_2e7b3d14_Rx]                    */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VCU3_oE_CAN_2e7b3d14_Rx, /ActiveEcuC/PduR/Com] */
  { /*    47 */                    ComConf_ComIPdu_VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          22u },  /* [Globale PduRDestPdu: VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx]          */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx, /ActiveEcuC/PduR/Com] */
  { /*    48 */                      ComConf_ComIPdu_VCU_PCANInfo_oE_CAN_788ea84c_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          42u },  /* [Globale PduRDestPdu: VCU_PCANInfo_oE_CAN_788ea84c_Rx]            */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VCU_PCANInfo_oE_CAN_788ea84c_Rx, /ActiveEcuC/PduR/Com] */
  { /*    49 */                            ComConf_ComIPdu_VCU_TU_oE_CAN_e21002f7_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           3u },  /* [Globale PduRDestPdu: VCU_TU_oE_CAN_e21002f7_Rx]                  */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VCU_TU_oE_CAN_e21002f7_Rx, /ActiveEcuC/PduR/Com] */
    /* Index    DestHnd                                                               Direction                      MmRomIdx  PduRDestPduProcessing                             RmDestRomIdx        Comment                                                                       Referable Keys */
  { /*    50 */                               ComConf_ComIPdu_VCU_oE_CAN_8b566170_Rx, PDUR_RX_DIRECTIONOFRMGDESTROM,       2u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          37u },  /* [Globale PduRDestPdu: VCU_oE_CAN_8b566170_Rx]                     */  /* [/ActiveEcuC/EcuC/EcucPduCollection/VCU_oE_CAN_8b566170_Rx, /ActiveEcuC/PduR/Com] */
  { /*    51 */ DcmConf_DcmDslProtocolRx_REQ_DIAG_ON_CAN_oE_CAN_e4ae5e33_Rx_d856e5e3, PDUR_RX_DIRECTIONOFRMGDESTROM,       3u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,           4u },  /* [Globale PduRDestPdu: REQ_DIAG_ON_CAN_oE_CAN_e4ae5e33_Rx]         */  /* [/ActiveEcuC/EcuC/EcucPduCollection/REQ_DIAG_ON_CAN_oE_CAN_e4ae5e33_Rx, /ActiveEcuC/PduR/Dcm] */
  { /*    52 */      DcmConf_DcmDslProtocolRx_UDS_ON_CAN_oE_CAN_544a8d55_Rx_392b7c7f, PDUR_RX_DIRECTIONOFRMGDESTROM,       3u, PDUR_IMMEDIATE_PDURDESTPDUPROCESSINGOFRMGDESTROM,          19u }   /* [Globale PduRDestPdu: UDS_ON_CAN_oE_CAN_544a8d55_Rx]              */  /* [/ActiveEcuC/EcuC/EcucPduCollection/UDS_ON_CAN_oE_CAN_544a8d55_Rx, /ActiveEcuC/PduR/Dcm] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmSrcRom
**********************************************************************************************************************/
/** 
  \var    PduR_RmSrcRom
  \brief  PduR RoutiongManager SrcPdu Table
  \details
  Element                    Description
  TxConfirmationSupported
  MmRomIdx                   the index of the 1:1 relation pointing to PduR_MmRom
  RmDestRomLength            the number of relations pointing to PduR_RmDestRom
  RmDestRomStartIdx          the start index of the 1:n relation pointing to PduR_RmDestRom
  SrcHnd                     handle to be used as parameter for the TxConfirmation or TriggerTransmit function call.
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_RmSrcRomType, PDUR_CONST) PduR_RmSrcRom[53] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxConfirmationSupported  MmRomIdx  RmDestRomLength  RmDestRomStartIdx  SrcHnd                                                                      Comment                                       Referable Keys */
  { /*     0 */                   FALSE,       0u,              1u,               43u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_ce686a11] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS1_oE_CAN_7c7ae1cc_Rx/PduRSrcPdu_ce686a11] */
  { /*     1 */                   FALSE,       0u,              1u,               46u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_d8341745] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS3_oE_CAN_91ec3225_Rx/PduRSrcPdu_d8341745] */
  { /*     2 */                   FALSE,       0u,              1u,               20u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_5b82c527] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS5_oE_CAN_7c26405f_Rx/PduRSrcPdu_5b82c527] */
  { /*     3 */                   FALSE,       0u,              1u,               47u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_e104bf72] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS6_oE_CAN_0ac37962_Rx/PduRSrcPdu_e104bf72] */
  { /*     4 */                   FALSE,       0u,              1u,               39u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_b865118f] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS8_oE_CAN_e7b0483e_Rx/PduRSrcPdu_b865118f] */
  { /*     5 */                   FALSE,       0u,              1u,               14u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_3c9c33f4] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BMS9_oE_CAN_7cc3a2ea_Rx/PduRSrcPdu_3c9c33f4] */
  { /*     6 */                   FALSE,       0u,              1u,               26u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_882b1898] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/BSIInfo_oE_CAN_0ff52381_Rx/PduRSrcPdu_882b1898] */
  { /*     7 */                   FALSE,       0u,              1u,                9u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_2ed93c0e] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/CtrlDCDC_oE_CAN_8127de91_Rx/PduRSrcPdu_2ed93c0e] */
  { /*     8 */                   FALSE,       0u,              1u,               35u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_aaa6ebec] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Fault_oInt_CAN_595e2cca_Rx/PduRSrcPdu_aaa6ebec] */
  { /*     9 */                   FALSE,       0u,              1u,               28u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_91da6bff] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Status1_oInt_CAN_c4860e0a_Rx/PduRSrcPdu_91da6bff] */
  { /*    10 */                   FALSE,       0u,              1u,               31u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_9f59032e] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCHV_Status2_oInt_CAN_771223c9_Rx/PduRSrcPdu_9f59032e] */
  { /*    11 */                   FALSE,       0u,              1u,               11u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_3259f79e] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Fault_oInt_CAN_cb576812_Rx/PduRSrcPdu_3259f79e] */
  { /*    12 */                   FALSE,       0u,              1u,               50u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_f6508c17] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status1_oInt_CAN_fcd6569d_Rx/PduRSrcPdu_f6508c17] */
  { /*    13 */                   FALSE,       0u,              1u,               24u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_820710d6] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status2_oInt_CAN_4f427b5e_Rx/PduRSrcPdu_820710d6] */
  { /*    14 */                   FALSE,       0u,              1u,               21u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_6f865e62] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DCLV_Status3_oInt_CAN_21ce601f_Rx/PduRSrcPdu_6f865e62] */
  { /*    15 */                   FALSE,       0u,              1u,                6u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_182f4c9b] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ELECTRON_BSI_oE_CAN_61376617_Rx/PduRSrcPdu_182f4c9b] */
  { /*    16 */                   FALSE,       0u,              1u,               32u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_a0d1622f] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_oE_CAN_4540bd6c_Rx/PduRSrcPdu_a0d1622f] */
  { /*    17 */                   FALSE,       0u,              1u,               13u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_3c5c1cb6] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Fault_oInt_CAN_63f22496_Rx/PduRSrcPdu_3c5c1cb6] */
  { /*    18 */                   FALSE,       0u,              1u,               15u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_4db707a4] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status1_oInt_CAN_dc53aecc_Rx/PduRSrcPdu_4db707a4] */
  { /*    19 */                   FALSE,       0u,              1u,                2u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_13c131e4] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status2_oInt_CAN_6fc7830f_Rx/PduRSrcPdu_13c131e4] */
  { /*    20 */                   FALSE,       0u,              1u,               29u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_97e42112] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status3_oInt_CAN_014b984e_Rx/PduRSrcPdu_97e42112] */
  { /*    21 */                   FALSE,       0u,              1u,               25u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_8463d87d] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status4_oInt_CAN_d39edec8_Rx/PduRSrcPdu_8463d87d] */
  { /*    22 */                   FALSE,       0u,              1u,               52u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_fa5b1cce] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/PFC_Status5_oInt_CAN_bd12c589_Rx/PduRSrcPdu_fa5b1cce] */
  { /*    23 */                   FALSE,       0u,              1u,               16u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_50e0f311] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ParkCommand_oE_CAN_16da6a87_Rx/PduRSrcPdu_50e0f311] */
  { /*    24 */                   FALSE,       0u,              1u,               30u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_9b0aaf50] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ProgTool_SupEnterBoot_oInt_CAN_39bdfe09_Rx/PduRSrcPdu_9b0aaf50] */
  { /*    25 */                   FALSE,       0u,              1u,               38u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_b756064f] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ReqToECANFunction_oE_CAN_abe814c4_Rx/PduRSrcPdu_b756064f] */
  { /*    26 */                   FALSE,       0u,              1u,               23u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_7e41dab9] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/ReqToOBC_oE_CAN_45858201_Rx/PduRSrcPdu_7e41dab9] */
  { /*    27 */                   FALSE,       0u,              1u,               41u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_c61797dd] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU2_oE_CAN_7db4b816_Rx/PduRSrcPdu_c61797dd] */
  { /*    28 */                   FALSE,       0u,              1u,                7u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_2def7208] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU3_oE_CAN_e6c752c2_Rx/PduRSrcPdu_2def7208] */
  { /*    29 */                   FALSE,       0u,              1u,               22u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_771f59ac] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_BSI_Wakeup_oE_CAN_79c55f53_Rx/PduRSrcPdu_771f59ac] */
  { /*    30 */                   FALSE,       0u,              1u,               42u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_c80fbc21] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_PCANInfo_oE_CAN_1242d2f8_Rx/PduRSrcPdu_c80fbc21] */
  { /*    31 */                   FALSE,       0u,              1u,                3u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_141e34ce] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_TU_oE_CAN_bad58dcd_Rx/PduRSrcPdu_141e34ce] */
  { /*    32 */                   FALSE,       0u,              1u,               37u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_b485c90c] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VCU_oE_CAN_377cc071_Rx/PduRSrcPdu_b485c90c] */
  { /*    33 */                   FALSE,       1u,              1u,                4u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_1707827c] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REQ_DIAG_ON_CAN_oE_CAN_0be358b0_Rx/PduRSrcPdu_1707827c] */
  { /*    34 */                   FALSE,       1u,              1u,               19u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_59769882] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/UDS_ON_CAN_oE_CAN_a9f0043e_Rx/PduRSrcPdu_59769882] */
  { /*    35 */                    TRUE,       2u,              1u,                1u,                               ComConf_ComIPdu_DC1_oE_CAN_fcac9ee4_Tx },  /* [PduRSrcPdu: PduRSrcPdu_1224da83] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC1_oE_CAN_fcac9ee4_Tx/PduRSrcPdu_1224da83] */
  { /*    36 */                    TRUE,       2u,              1u,               45u,                               ComConf_ComIPdu_DC2_oE_CAN_72239907_Tx },  /* [PduRSrcPdu: PduRSrcPdu_d598d177] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC2_oE_CAN_72239907_Tx/PduRSrcPdu_d598d177] */
  { /*    37 */                   FALSE,       2u,              1u,               40u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_c451a6c2] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug1_oInt_CAN_177fd136_Tx/PduRSrcPdu_c451a6c2] */
  { /*    38 */                   FALSE,       2u,              1u,               44u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_cf0367a1] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug2_oInt_CAN_fc486a35_Tx/PduRSrcPdu_cf0367a1] */
  { /*    39 */                   FALSE,       2u,              1u,               48u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_e2c32671] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug3_oInt_CAN_138a010b_Tx/PduRSrcPdu_e2c32671] */
  { /*    40 */                   FALSE,       2u,              1u,               36u,                                             PDUR_NO_SRCHNDOFRMSRCROM },  /* [PduRSrcPdu: PduRSrcPdu_af12cdd2] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug4_oInt_CAN_f1561a72_Tx/PduRSrcPdu_af12cdd2] */
  { /*    41 */                    TRUE,       2u,              1u,                8u,                  ComConf_ComIPdu_NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx },  /* [PduRSrcPdu: PduRSrcPdu_2e76203d] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx/PduRSrcPdu_2e76203d] */
  { /*    42 */                    TRUE,       2u,              1u,               18u,                              ComConf_ComIPdu_OBC1_oE_CAN_89fd90e2_Tx },  /* [PduRSrcPdu: PduRSrcPdu_5705bb4d] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC1_oE_CAN_89fd90e2_Tx/PduRSrcPdu_5705bb4d] */
  { /*    43 */                    TRUE,       2u,              1u,               17u,                              ComConf_ComIPdu_OBC2_oE_CAN_07729701_Tx },  /* [PduRSrcPdu: PduRSrcPdu_558557e8] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC2_oE_CAN_07729701_Tx/PduRSrcPdu_558557e8] */
  { /*    44 */                    TRUE,       2u,              1u,                0u,                              ComConf_ComIPdu_OBC3_oE_CAN_cbd8979f_Tx },  /* [PduRSrcPdu: PduRSrcPdu_0f56ae96] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC3_oE_CAN_cbd8979f_Tx/PduRSrcPdu_0f56ae96] */
  { /*    45 */                    TRUE,       2u,              1u,               10u,                              ComConf_ComIPdu_OBC4_oE_CAN_c11d9e86_Tx },  /* [PduRSrcPdu: PduRSrcPdu_31565810] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC4_oE_CAN_c11d9e86_Tx/PduRSrcPdu_31565810] */
  { /*    46 */                    TRUE,       2u,              1u,               34u,                         ComConf_ComIPdu_OBCTxDiag_oE_CAN_98fb5e1b_Tx },  /* [PduRSrcPdu: PduRSrcPdu_aa4ba89d] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBCTxDiag_oE_CAN_98fb5e1b_Tx/PduRSrcPdu_aa4ba89d] */
  { /*    47 */                   FALSE,       3u,              1u,               49u, DcmConf_DcmDslProtocolTx_REP_DIAG_ON_CAN_oE_CAN_81c96575_Tx_eb2739d1 },  /* [PduRSrcPdu: PduRSrcPdu_f0e620d6] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REP_DIAG_ON_CAN_oE_CAN_81c96575_Tx/PduRSrcPdu_f0e620d6] */
  { /*    48 */                    TRUE,       2u,              1u,               33u,                  ComConf_ComIPdu_SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx },  /* [PduRSrcPdu: PduRSrcPdu_a0eb6b35] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx/PduRSrcPdu_a0eb6b35] */
  { /*    49 */                    TRUE,       2u,              1u,                5u,               ComConf_ComIPdu_SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx },  /* [PduRSrcPdu: PduRSrcPdu_17d4e376] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx/PduRSrcPdu_17d4e376] */
    /* Index    TxConfirmationSupported  MmRomIdx  RmDestRomLength  RmDestRomStartIdx  SrcHnd                                                                      Comment                                       Referable Keys */
  { /*    50 */                    TRUE,       2u,              1u,               12u,               ComConf_ComIPdu_SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx },  /* [PduRSrcPdu: PduRSrcPdu_36c239fa] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx/PduRSrcPdu_36c239fa] */
  { /*    51 */                    TRUE,       2u,              1u,               51u,                ComConf_ComIPdu_SUP_CommandToPFC_oInt_CAN_d9411c97_Tx },  /* [PduRSrcPdu: PduRSrcPdu_f7c07c35] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx/PduRSrcPdu_f7c07c35] */
  { /*    52 */                    TRUE,       2u,              1u,               27u,                     ComConf_ComIPdu_VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx }   /* [PduRSrcPdu: PduRSrcPdu_8aa49178] */  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx/PduRSrcPdu_8aa49178] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_RmTransmitFctPtr
**********************************************************************************************************************/
/** 
  \var    PduR_RmTransmitFctPtr
  \brief  Internal routing manager Transmit functions.
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_RmTransmitFctPtrType, PDUR_CONST) PduR_RmTransmitFctPtr[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RmTransmitFctPtr                  Referable Keys */
  /*     0 */ PduR_RmIf_RoutePdu           ,  /* [PduR_RmIf_RoutePdu] */
  /*     1 */ PduR_RmTp_Transmit_MultiDest    /* [PduR_RmTp_Transmit_MultiDest] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Tx2Lo
**********************************************************************************************************************/
/** 
  \var    PduR_Tx2Lo
  \brief  Contains all informations to route a Pdu from a upper layer to a lower layer module, or to cancel a transmission
  \details
  Element                Description
  RmTransmitFctPtrIdx    the index of the 1:1 relation pointing to PduR_RmTransmitFctPtr
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_Tx2LoType, PDUR_CONST) PduR_Tx2Lo[18] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RmTransmitFctPtrIdx        Referable Keys */
  { /*     0 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC1_oE_CAN_fcac9ee4_Tx/PduRSrcPdu_1224da83] */
  { /*     1 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/DC2_oE_CAN_72239907_Tx/PduRSrcPdu_d598d177] */
  { /*     2 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug1_oInt_CAN_177fd136_Tx/PduRSrcPdu_c451a6c2] */
  { /*     3 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug2_oInt_CAN_fc486a35_Tx/PduRSrcPdu_cf0367a1] */
  { /*     4 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug3_oInt_CAN_138a010b_Tx/PduRSrcPdu_e2c32671] */
  { /*     5 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/Debug4_oInt_CAN_f1561a72_Tx/PduRSrcPdu_af12cdd2] */
  { /*     6 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx/PduRSrcPdu_2e76203d] */
  { /*     7 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC1_oE_CAN_89fd90e2_Tx/PduRSrcPdu_5705bb4d] */
  { /*     8 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC2_oE_CAN_07729701_Tx/PduRSrcPdu_558557e8] */
  { /*     9 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC3_oE_CAN_cbd8979f_Tx/PduRSrcPdu_0f56ae96] */
  { /*    10 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBC4_oE_CAN_c11d9e86_Tx/PduRSrcPdu_31565810] */
  { /*    11 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/OBCTxDiag_oE_CAN_98fb5e1b_Tx/PduRSrcPdu_aa4ba89d] */
  { /*    12 */                  1u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/REP_DIAG_ON_CAN_oE_CAN_81c96575_Tx/PduRSrcPdu_f0e620d6] */
  { /*    13 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx/PduRSrcPdu_a0eb6b35] */
  { /*    14 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx/PduRSrcPdu_17d4e376] */
  { /*    15 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx/PduRSrcPdu_36c239fa] */
  { /*    16 */                  0u },  /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx/PduRSrcPdu_f7c07c35] */
  { /*    17 */                  0u }   /* [/ActiveEcuC/PduR/PduRRoutingTables/PduRRoutingTable/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx/PduRSrcPdu_8aa49178] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_TxIf2Up
**********************************************************************************************************************/
/** 
  \var    PduR_TxIf2Up
  \brief  This table contains all routing information to perform the Tx handling of an interface routing. Used in the &lt;LLIf&gt;_TriggerTransmit and &lt;LLIf&gt;_TxConfirmation
  \details
  Element               Description
  TxConfirmationUsed    True, if any of the source PduRDestPdus uses a TxConfirmation.
*/ 
#define PDUR_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(PduR_TxIf2UpType, PDUR_CONST) PduR_TxIf2Up[17] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxConfirmationUsed        Referable Keys */
  { /*     0 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/DC1_oE_CAN_f87e7579_Tx] */
  { /*     1 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/DC2_oE_CAN_8e9b4c44_Tx] */
  { /*     2 */              FALSE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug1_oInt_CAN_5112c5fb_Tx] */
  { /*     3 */              FALSE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug2_oInt_CAN_e286e838_Tx] */
  { /*     4 */              FALSE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug3_oInt_CAN_8c0af379_Tx] */
  { /*     5 */              FALSE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/Debug4_oInt_CAN_5edfb5ff_Tx] */
  { /*     6 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx] */
  { /*     7 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC1_oE_CAN_79eaec1b_Tx] */
  { /*     8 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC2_oE_CAN_0f0fd526_Tx] */
  { /*     9 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC3_oE_CAN_947c3ff2_Tx] */
  { /*    10 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBC4_oE_CAN_e2c5a75c_Tx] */
  { /*    11 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/OBCTxDiag_oE_CAN_794ae2be_Tx] */
  { /*    12 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx] */
  { /*    13 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx] */
  { /*    14 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx] */
  { /*    15 */               TRUE },  /* [/ActiveEcuC/EcuC/EcucPduCollection/SUP_CommandToPFC_oInt_CAN_11d75aca_Tx] */
  { /*    16 */               TRUE }   /* [/ActiveEcuC/EcuC/EcucPduCollection/VERS_OBC_DCDC_oE_CAN_336be5e5_Tx] */
};
#define PDUR_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  PduR_Initialized
**********************************************************************************************************************/
/** 
  \var    PduR_Initialized
  \brief  Initialization state of PduR. TRUE, if PduR_Init() has been called, else FALSE
*/ 
#define PDUR_START_SEC_VAR_ZERO_INIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(PduR_InitializedType, PDUR_VAR_ZERO_INIT) PduR_Initialized = FALSE;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define PDUR_STOP_SEC_VAR_ZERO_INIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/


#define PDUR_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h"    /* PRQA S 5087 */        /* MD_MSR_MemMap */

#if(PDUR_USE_INIT_POINTER == STD_ON)
P2CONST(PduR_PBConfigType, PDUR_VAR_ZERO_INIT, PDUR_PBCFG) PduR_ConfigDataPtr = (const PduR_PBConfigType *) NULL_PTR;
#endif

#define PDUR_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "MemMap.h"    /* PRQA S 5087 */        /* MD_MSR_MemMap */
  
/**********************************************************************************************************************
 * LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/
#define PDUR_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "MemMap.h"

/* Exclusive Area Wrapper functions */

/**********************************************************************************************************************
 * PduR_SchM_Enter_PduR_PDUR_EXCLUSIVE_AREA_0
 *********************************************************************************************************************/
/*!
 * \internal
 * -  enter the EA  
 * \endinternal
 *********************************************************************************************************************/
static FUNC(void, PDUR_CODE) PduR_SchM_Enter_PduR_PDUR_EXCLUSIVE_AREA_0(void)
{
  SchM_Enter_PduR_PDUR_EXCLUSIVE_AREA_0();
}


/**********************************************************************************************************************
 * PduR_SchM_Exit_PduR_PDUR_EXCLUSIVE_AREA_0
 *********************************************************************************************************************/
/*!
 * \internal
 * -  exit the EA  
 * \endinternal
 *********************************************************************************************************************/
static FUNC(void, PDUR_CODE) PduR_SchM_Exit_PduR_PDUR_EXCLUSIVE_AREA_0(void)
{
  SchM_Exit_PduR_PDUR_EXCLUSIVE_AREA_0();
}


/* Upper Layer Interface APIs */

/**********************************************************************************************************************
 * PduR_ComTransmit
 *********************************************************************************************************************/
/*!
 * \internal
 * - call upper layer Transmit function. 
 * \endinternal
 *********************************************************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE) PduR_ComTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info) /* COV_PDUR_WRAPPER_FUNC */
{
  Std_ReturnType retVal = E_NOT_OK;        /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
#if (PDUR_TX2LO == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  retVal = PduR_UpTransmit(id, info); /* SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY */
#else
  PduR_Det_ReportError(PDUR_FCT_TX, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(info);   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
}


/**********************************************************************************************************************
 * PduR_DcmTransmit
 *********************************************************************************************************************/
/*!
 * \internal
 * - call upper layer Transmit function. 
 * \endinternal
 *********************************************************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE) PduR_DcmTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info) /* COV_PDUR_WRAPPER_FUNC */
{
  Std_ReturnType retVal = E_NOT_OK;        /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
#if (PDUR_TX2LO == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  retVal = PduR_UpTransmit(id, info); /* SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY */
#else
  PduR_Det_ReportError(PDUR_FCT_TX, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(info);   /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
}


/* Communication Interface APIs */

/**********************************************************************************************************************
 * PduR_CanIfRxIndication
 *********************************************************************************************************************/
/*!
 * \internal
 * -  call internal general IfRxIndication function.  
 * \endinternal
 *********************************************************************************************************************/
FUNC(void, PDUR_CODE) PduR_CanIfRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info) /* COV_PDUR_WRAPPER_FUNC */
{
#if (PDUR_RXIF2DEST == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  PduR_LoIfRxIndication(RxPduId, info);  /* SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY */
#else
  PduR_Det_ReportError(PDUR_FCT_IFRXIND, PDUR_E_PDU_ID_INVALID);
#endif
  PDUR_DUMMY_STATEMENT(RxPduId);        /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(info);   		/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}


/**********************************************************************************************************************
 * PduR_CanIfTxConfirmation
 *********************************************************************************************************************/
/*!
 * \internal
 * - call internal general communication interface TxConfirmation function.
 * \endinternal
 *********************************************************************************************************************/
FUNC(void, PDUR_CODE) PduR_CanIfTxConfirmation(PduIdType TxPduId) /* COV_PDUR_WRAPPER_FUNC */
{
#if (PDUR_TXCONFIRMATIONUSEDOFTXIF2UP == STD_ON)
  PduR_LoIfTxConfirmation(TxPduId);
#endif
  PDUR_DUMMY_STATEMENT(TxPduId);        /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}




/* Transport Protocol APIs */

/**********************************************************************************************************************
 * PduR_CanTpStartOfReception
 *********************************************************************************************************************/
 /*!
 * \internal
 * - call transport protocoll StartOfReception function.  
 * \endinternal
 *********************************************************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE) PduR_CanTpStartOfReception(PduIdType id, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info, PduLengthType TpSduLength, P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr) /* COV_PDUR_WRAPPER_FUNC */
{
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;        /* PRQA S 2981 */ /* MD_MSR_RetVal */

#if (PDUR_RXTP2DEST == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  retVal = PduR_LoTpStartOfReception(id, info, TpSduLength, bufferSizePtr); /* SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY */
#else
  PduR_Det_ReportError(PDUR_FCT_SOR, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     		/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(info);   		/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(TpSduLength);    /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(bufferSizePtr);  /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */

  return retVal;
}


/**********************************************************************************************************************
 * PduR_CanTpCopyRxData
 *********************************************************************************************************************/
/*!
 * \internal
 * - call internal general CopyRxData.
 * \endinternal
 *********************************************************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE) PduR_CanTpCopyRxData(PduIdType id, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info, P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr) /* COV_PDUR_WRAPPER_FUNC */
{
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;        /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
#if (PDUR_RXTP2DEST == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  retVal = PduR_LoTpCopyRxData(id, info, bufferSizePtr); /* SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY */
#else
  PduR_Det_ReportError(PDUR_FCT_CPYRX, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     		/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(info);  			/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(bufferSizePtr);  /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  
  return retVal;
}


/**********************************************************************************************************************
 * PduR_CanTpCopyTxData
 *********************************************************************************************************************/
/*!
 * \internal
 * - call internal general CopyTxData.
 * \endinternal
 *********************************************************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE) PduR_CanTpCopyTxData(PduIdType id, P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info, P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry, P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr) /* COV_PDUR_WRAPPER_FUNC */
{
  BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;        /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
#if(PDUR_TXTP2SRC == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  retVal = PduR_LoTpCopyTxData(id, info, retry, availableDataPtr); /* SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY */
#else
  PduR_Det_ReportError(PDUR_FCT_CPYTX, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     				/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(info);  					/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(retry);  				/* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(availableDataPtr);       /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  
  return retVal;
}


/**********************************************************************************************************************
 * PduR_CanTpRxIndication
 *********************************************************************************************************************/
/*!
 * \internal
 * - call Tp RxIndication function.
 * \endinternal
 *********************************************************************************************************************/
FUNC(void, PDUR_CODE) PduR_CanTpRxIndication(PduIdType id, Std_ReturnType result) /* COV_PDUR_WRAPPER_FUNC */
{
#if (PDUR_RXTP2DEST == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  PduR_LoTpRxIndication(id, result);
#else
  PduR_Det_ReportError(PDUR_FCT_TPRXIND, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(result); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}


/**********************************************************************************************************************
 * PduR_CanTpTxConfirmation
 *********************************************************************************************************************/
/*!
 * \internal
 * - call Tp TxConfirmation function 
 * \endinternal
 *********************************************************************************************************************/
FUNC(void, PDUR_CODE) PduR_CanTpTxConfirmation(PduIdType id, Std_ReturnType result) /* COV_PDUR_WRAPPER_FUNC */
{
#if (PDUR_TXTP2SRC == STD_ON) /* COV_PDUR_RX_OR_TX_ONLY_CONFIG */
  PduR_LoTpTxConfirmation(id, result);
#else
  PduR_Det_ReportError(PDUR_FCT_TPTXCFM, PDUR_E_PDU_ID_INVALID);
#endif

  PDUR_DUMMY_STATEMENT(id);     /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
  PDUR_DUMMY_STATEMENT(result); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */ /* lint -e{438} */
}


/* CancelReceive global without UseTag API */

/* ChangeParameter global without UseTag  API */

/* Communication Interface / Transport Protocol APIs */

/* Communication Interface / Transport Protocol APIs */

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/


#define PDUR_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "MemMap.h"

/* SBSW_JUSTIFICATION_BEGIN

  \ID SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ID_AND_PTR
    \DESCRIPTION    The API is called with an Id and a pointer.
                    The API call is forwarded using a function pointer which is read using ComStackLib macros.
                    
    \COUNTERMEASURE \N The function pointer is read using the passed Id (and ComStackLib macros). 
                       The Id is a SNV provided by the lower layer. 
                       It is checked at runtime if it is in the valid range.
                       The pointer is provided by the lower layer and is assumed to be valid. It is passed as is to the corresponding function. 

  \ID SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ID_ONLY
    \DESCRIPTION    The API is called with an Id.
                    The API call is forwarded using a function pointer which is read using ComStackLib macros.
                    
    \COUNTERMEASURE \R The function pointer is read using the passed Id (and ComStackLib macros). 
                       The Id is a SNV provided by the lower layer. 
                       It is checked at runtime if it is in the valid range.
                       
  \ID SBSW_PDUR_EXTERNAL_API_CALL_FORWARDING_ONLY
    \DESCRIPTION    The API call is forwarded to an internal function.
    \COUNTERMEASURE \N The parameter are checked in the called function.

 SBSW_JUSTIFICATION_END */
 
 /* COV_JUSTIFICATION_BEGIN
 
  \ID COV_PDUR_WRAPPER_FUNC
    \ACCEPT X
    \REASON Each neighbouring module call the same internal function. The call is covered by an other module. 
    
  \ID COV_PDUR_RX_OR_TX_ONLY_CONFIG
    \ACCEPT TX
    \REASON The API is mandatory if this Bsw module exists. In case of Rx or Tx only configs the API call is not forwarded to the internal function. 
 
 COV_JUSTIFICATION_END */
 

/**********************************************************************************************************************
 * END OF FILE: PduR_Lcfg.c
 *********************************************************************************************************************/

