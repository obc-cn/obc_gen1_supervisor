/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EthTrcv_Ar7000
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EthTrcv_30_Ar7000_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


#define ETHTRCV_30_AR7000_LCFG_SOURCE

/* START of Checksum exclude for - EthTrcv_LinktimeCRC */

/* PRQA S 0810 EOF */ /* MD_MSR_1.1_810 */
/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
#include "EthTrcv_30_Ar7000_Lcfg.h"

/**********************************************************************************************************************
 *  RAM VARIABLES
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  ROM VARIABLES
 *********************************************************************************************************************/
/* START of Checksum include for - EthTrcv_LinktimeCRC */


#define ETHTRCV_30_AR7000_START_SEC_CONST_8BIT
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_HwSubIndex[1] =
{
  ETHTRCV_DONT_CARE
};

CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_HwIndex[1] =
{
  EthConf_EthCtrlConfig_EthCtrlConfig
};

CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Speed[1] =
{
  ETHTRCV_BAUD_RATE_100MBIT
};

CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_MiiMode[1] =
{
  ETHTRCV_DONT_CARE
};

CONST(boolean, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_AutoNegotiation[1] =
{
  FALSE
};

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_8BIT
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

CONST(EthTrcv_30_Ar7000_ConfigType, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Config = 0;

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define ETHTRCV_30_AR7000_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  DYNAMIC FUNCTIONS
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  VARIANT DEPENDEND
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  ROM VARIABLES
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"
#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"
/**********************************************************************************************************************
 *  RAM VARIABLES
 *********************************************************************************************************************/

#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


/* END of Checksum include for - EthTrcv_LinktimeCRC */


/* END of Checksum exclude for - EthTrcv_LinktimeCRC */

/**********************************************************************************************************************
 *  MISRA DEVIATIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_Lcfg.c
 *********************************************************************************************************************/

