/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Alarm_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_ALARM_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Alarm_Lcfg.h"
#include "Os_Alarm.h"

/* Os kernel module dependencies */
#include "Os_Application_Lcfg.h"
#include "Os_Cfg.h"
#include "Os_Common.h"
#include "Os_Counter_Lcfg.h"
#include "Os_Counter.h"
#include "Os_Task_Lcfg.h"
#include "Os_Timer.h"

/* Os hal dependencies */


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Dynamic alarm data: Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE2_Default_BSW_Async_Task_7_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_Task_7_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_ASILB_MAIN_TASK_1_100ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_100ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_ASILB_MAIN_TASK_1_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_ASILB_MAIN_TASK_2_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_2_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_ASILB_MAIN_TASK_3_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_3_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_ASILB_MAIN_TASK_4_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_4_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_CpApAEM_RCtApAEM_task10msA */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_CpApAEM_RCtApAEM_task10msA_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_QM_MAIN_TASK_2_100ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_100ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_QM_MAIN_TASK_2_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_10ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_QM_MAIN_TASK_3_100ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_100ms_Dyn;

/*! Dynamic alarm data: Rte_Al_TE_QM_MAIN_TASK_3_10ms */
OS_LOCAL VAR(Os_AlarmType, OS_VAR_NOINIT) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_10ms_Dyn;

#define OS_STOP_SEC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Alarm configuration data: Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_BSW_QM
  },
  /* .Task  = */ &OsCfg_Task_Default_BSW_Async_QM_Task,
  /* .Mask  = */ Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_1_5ms
};

/*! Alarm configuration data: Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_BSW_QM
  },
  /* .Task  = */ &OsCfg_Task_Default_BSW_Async_QM_Task,
  /* .Mask  = */ Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_7_10ms
};

/*! Alarm configuration data: Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_BSW_QM
  },
  /* .Task  = */ &OsCfg_Task_Default_BSW_Async_QM_Task,
  /* .Mask  = */ Rte_Ev_Cyclic2_Default_BSW_Async_QM_Task_8_20ms
};

/*! Alarm configuration data: Rte_Al_TE2_Default_BSW_Async_Task_7_10ms */
CONST(Os_AlarmActivateTaskConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_Task_7_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_Task_7_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionActivateTask
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_SystemApplication_OsCore0
  },
  /* .Task  = */ &OsCfg_Task_Default_BSW_Async_Task
};

/*! Alarm configuration data: Rte_Al_TE_ASILB_MAIN_TASK_1_100ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_100ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_100ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_100ms
};

/*! Alarm configuration data: Rte_Al_TE_ASILB_MAIN_TASK_1_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_10ms
};

/*! Alarm configuration data: Rte_Al_TE_ASILB_MAIN_TASK_2_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_2_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_2_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_ASILB_MAIN_TASK_2_10ms
};

/*! Alarm configuration data: Rte_Al_TE_ASILB_MAIN_TASK_3_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_3_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_3_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_ASILB_MAIN_TASK_3_10ms
};

/*! Alarm configuration data: Rte_Al_TE_ASILB_MAIN_TASK_4_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_4_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_4_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_ASILB_MAIN_TASK_4_10ms
};

/*! Alarm configuration data: Rte_Al_TE_CpApAEM_RCtApAEM_task10msA */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_CpApAEM_RCtApAEM_task10msA =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_CpApAEM_RCtApAEM_task10msA_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(OsApplication_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_QM
  },
  /* .Task  = */ &OsCfg_Task_QM_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Run_CpApAEM_RCtApAEM_task10msA
};

/*! Alarm configuration data: Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msRX
};

/*! Alarm configuration data: Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_ASILB) | OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_ASILB
  },
  /* .Task  = */ &OsCfg_Task_ASILB_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msTX
};

/*! Alarm configuration data: Rte_Al_TE_QM_MAIN_TASK_2_100ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_100ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_100ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(OsApplication_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_QM
  },
  /* .Task  = */ &OsCfg_Task_QM_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_QM_MAIN_TASK_2_100ms
};

/*! Alarm configuration data: Rte_Al_TE_QM_MAIN_TASK_2_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(OsApplication_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_QM
  },
  /* .Task  = */ &OsCfg_Task_QM_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_QM_MAIN_TASK_2_10ms
};

/*! Alarm configuration data: Rte_Al_TE_QM_MAIN_TASK_3_100ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_100ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_100ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(OsApplication_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_QM
  },
  /* .Task  = */ &OsCfg_Task_QM_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_QM_MAIN_TASK_3_100ms
};

/*! Alarm configuration data: Rte_Al_TE_QM_MAIN_TASK_3_10ms */
CONST(Os_AlarmSetEventConfigType, OS_CONST) OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_10ms =
{
  /* .Alarm = */
  {
    /* .Job                   = */
    {
      /* .Dyn      = */ OS_ALARM_CASTDYN_ALARM_2_JOB(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_10ms_Dyn),
      /* .Counter  = */ OS_COUNTER_CASTCONFIG_TIMERHRT_2_COUNTER(OsCfg_Counter_SystemTimer),
      /* .Callback = */ Os_AlarmActionSetEvent
    },
    /* .Autostart             = */
    {
      /* .AlarmTime        = */ 0uL, /* 0.0 sec */
      /* .Cycle            = */ 0uL, /* 0.0 sec */
      /* .ApplicationModes = */ OS_APPMODE_NONE,
      /* .AlarmMode        = */ OS_ALARMMODE_ABSOLUTE
    },
    /* .AccessingApplications = */ (OS_APPID2MASK(OsApplication_BSW_QM) | OS_APPID2MASK(OsApplication_QM) | OS_APPID2MASK(SystemApplication_OsCore0)),
    /* .OwnerApplication      = */ &OsCfg_App_OsApplication_QM
  },
  /* .Task  = */ &OsCfg_Task_QM_MAIN_TASK,
  /* .Mask  = */ Rte_Ev_Cyclic_QM_MAIN_TASK_3_10ms
};

#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Object reference table for alarms. */
CONSTP2CONST(Os_AlarmConfigType, OS_CONST, OS_CONST) OsCfg_AlarmRefs[OS_ALARMID_COUNT + 1] =  /* PRQA S 4521 */ /* MD_Os_Rule10.1_4521 */
{
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms),
  OS_ALARM_CASTCONFIG_ALARMACTIVATETASK_2_ALARM(OsCfg_Alarm_Rte_Al_TE2_Default_BSW_Async_Task_7_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_100ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_1_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_2_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_3_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_ASILB_MAIN_TASK_4_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_CpApAEM_RCtApAEM_task10msA),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_100ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_2_10ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_100ms),
  OS_ALARM_CASTCONFIG_ALARMSETEVENT_2_ALARM(OsCfg_Alarm_Rte_Al_TE_QM_MAIN_TASK_3_10ms),
  NULL_PTR
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Alarm_Lcfg.c
 *********************************************************************************************************************/
