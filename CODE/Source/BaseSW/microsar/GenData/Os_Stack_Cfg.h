/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Stack_Cfg.h
 *   Generation Time: 2020-08-19 13:07:50
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#ifndef OS_STACK_CFG_H
# define OS_STACK_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/*! Defines whether stack monitoring is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_STACKMONITORING                  (STD_ON)

/*! Defines whether stack measurement is active (STD_ON) or not (STD_OFF). */
# define OS_CFG_STACKMEASUREMENT                 (STD_OFF)

/* Configured stack sizes (Total: 29184 Byte) */
# define OS_CFG_SIZE_ASILB_MAIN_TASK_STACK     (1024uL)
# define OS_CFG_SIZE_DEFAULT_BSW_ASYNC_QM_TASK_STACK     (2048uL)
# define OS_CFG_SIZE_OSCORE0_ERROR_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_INIT_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_ISR_CORE_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_KERNEL_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_PROTECTION_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_SHUTDOWN_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_STARTUP_STACK     (1024uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO10_STACK     (4096uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO1000_STACK     (4096uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO40_STACK     (512uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO4294967295_STACK     (4096uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO502_STACK     (4096uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO998_STACK     (512uL)
# define OS_CFG_SIZE_OSCORE0_TASK_PRIO999_STACK     (512uL)
# define OS_CFG_SIZE_QM_MAIN_TASK_STACK     (1024uL)


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/


#endif /* OS_STACK_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Stack_Cfg.h
 *********************************************************************************************************************/
