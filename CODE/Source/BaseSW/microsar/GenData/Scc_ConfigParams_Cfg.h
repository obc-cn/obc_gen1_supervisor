/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Scc
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Scc_ConfigParams_Cfg.h
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined(SCC_CONFIG_PARAMS_CFG_H)
#define SCC_CONFIG_PARAMS_CFG_H

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  MISRA & PC-lint
 *********************************************************************************************************************/
/* PRQA S 3453,3458 FUNCTION_LIKE_MACROS */ /* MD_MSR_19.4,MD_MSR_19.7 */

/**********************************************************************************************************************
 * DIAGNOSTIC PARAMETER VALUES
 *********************************************************************************************************************/
 
 /* General Timer ConfigValue */ 
#define Scc_ConfigValue_Timer_General_IPAddressWaitTimeout                     0u 
#define Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolRetries             49u 
#define Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTlsRetries          25u 
#define Scc_ConfigValue_Timer_General_SECCDiscoveryProtocolTimeout             50u 
#define Scc_ConfigValue_Timer_General_TransportLayerTimeout                    0u 
#define Scc_ConfigValue_Timer_General_SupportedAppProtocolMessageTimeout       400u 


 /* ISO Timer ConfigValue */
#define Scc_ConfigValue_Timer_ISO_SequencePerformanceTimeout                   8000u 
#define Scc_ConfigValue_Timer_ISO_CommunicationSetupTimeout                    4000u 
#define Scc_ConfigValue_Timer_ISO_AuthorizationMessageTimeout                  400u 
#define Scc_ConfigValue_Timer_ISO_CableCheckMessageTimeout                     400u 
#define Scc_ConfigValue_Timer_ISO_ChargeParameterDiscoveryMessageTimeout       400u 
#define Scc_ConfigValue_Timer_ISO_CurrentDemandMessageTimeout                  72u 
#define Scc_ConfigValue_Timer_ISO_PaymentServiceSelectionMessageTimeout        400u 
#define Scc_ConfigValue_Timer_ISO_PowerDeliveryMessageTimeout                  1000u 
#define Scc_ConfigValue_Timer_ISO_PreChargeMessageTimeout                      400u 
#define Scc_ConfigValue_Timer_ISO_ServiceDetailMessageTimeout                  1000u 
#define Scc_ConfigValue_Timer_ISO_ServiceDiscoveryMessageTimeout               400u 
#define Scc_ConfigValue_Timer_ISO_SessionSetupMessageTimeout                   400u 
#define Scc_ConfigValue_Timer_ISO_SessionStopMessageTimeout                    400u 
#define Scc_ConfigValue_Timer_ISO_WeldingDetectionMessageTimeout               400u 


 /* DIN Timer ConfigValue */
#define Scc_ConfigValue_Timer_DIN_SequencePerformanceTimeout                   11800u 
#define Scc_ConfigValue_Timer_DIN_CommunicationSetupTimeout                    4000u 
#define Scc_ConfigValue_Timer_DIN_ReadyToChargeTimeout                         30000u 
#define Scc_ConfigValue_Timer_DIN_ContractAuthenticationMessageTimeout         400u 
#define Scc_ConfigValue_Timer_DIN_CableCheckMessageTimeout                     400u 
#define Scc_ConfigValue_Timer_DIN_ChargeParameterDiscoveryMessageTimeout       400u 
#define Scc_ConfigValue_Timer_DIN_CurrentDemandMessageTimeout                  72u 
#define Scc_ConfigValue_Timer_DIN_ServicePaymentSelectionMessageTimeout        400u 
#define Scc_ConfigValue_Timer_DIN_PowerDeliveryMessageTimeout                  400u 
#define Scc_ConfigValue_Timer_DIN_PreChargeMessageTimeout                      400u 
#define Scc_ConfigValue_Timer_DIN_ServiceDiscoveryMessageTimeout               400u 
#define Scc_ConfigValue_Timer_DIN_SessionSetupMessageTimeout                   400u 
#define Scc_ConfigValue_Timer_DIN_SessionStopMessageTimeout                    400u 
#define Scc_ConfigValue_Timer_DIN_WeldingDetectionMessageTimeout               400u 


 /* State Machine ConfigValue */
#define Scc_ConfigValue_StateM_AcceptUnsecureConnection                        0u 
#define Scc_ConfigValue_StateM_ReconnectRetries                                0u 
#define Scc_ConfigValue_StateM_ReconnectDelay                                  1u 
#define Scc_ConfigValue_StateM_RequestInternetDetails                          1u 
#define Scc_ConfigValue_StateM_AuthorizationNextReqDelay                       100u 
#define Scc_ConfigValue_StateM_ChargeParameterDiscoveryNextReqDelay            100u 
#define Scc_ConfigValue_StateM_AuthorizationOngoingTimeout                     12000u 
#define Scc_ConfigValue_StateM_ChargeParameterDiscoveryOngoingTimeout          12000u 
#define Scc_ConfigValue_StateM_CableCheckOngoingTimeout                        8000u 
#define Scc_ConfigValue_StateM_PreChargeOngoingTimeout                         1400u 
#define Scc_ConfigValue_StateM_CableCheckNextReqDelay                          40u 
#define Scc_ConfigValue_StateM_PreChargeNextReqDelay                           40u 
#define Scc_ConfigValue_StateM_CurrentDemandNextReqDelay                       20u 
#define Scc_ConfigValue_StateM_WeldingDetectionNextReqDelay                    40u 
#define Scc_ConfigValue_StateM_SLACStartMode                                   Appl_SccDynCP_StateM_SLACStartMode() 
#define Scc_ConfigValue_StateM_QCAIdleTimer                                    Appl_SccDynCP_StateM_QCAIdleTimer() 


/**********************************************************************************************************************
 * DIAGNOSTIC PARAMETER TYPES
 *********************************************************************************************************************/

#define SCC_CFG_TYPE_STATIC  1u
#define SCC_CFG_TYPE_DYNAMIC 2u
#define SCC_CFG_TYPE_EXTERN  3u


 /* General Timer ConfigType */
#define Scc_ConfigType_Timer_General_IPAddressWaitTimeout                      SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_General_SECCDiscoveryProtocolRetries              SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTlsRetries           SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_General_SECCDiscoveryProtocolTimeout              SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_General_TransportLayerTimeout                     SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_General_SupportedAppProtocolMessageTimeout        SCC_CFG_TYPE_STATIC 


 /* ISO Timer ConfigType */
#define Scc_ConfigType_Timer_ISO_SequencePerformanceTimeout                    SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_CommunicationSetupTimeout                     SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_AuthorizationMessageTimeout                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_CableCheckMessageTimeout                      SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_ChargeParameterDiscoveryMessageTimeout        SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_CurrentDemandMessageTimeout                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_PaymentServiceSelectionMessageTimeout         SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_PowerDeliveryMessageTimeout                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_PreChargeMessageTimeout                       SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_ServiceDetailMessageTimeout                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_ServiceDiscoveryMessageTimeout                SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_SessionSetupMessageTimeout                    SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_SessionStopMessageTimeout                     SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_ISO_WeldingDetectionMessageTimeout                SCC_CFG_TYPE_STATIC 


 /* DIN Timer ConfigType */
#define Scc_ConfigType_Timer_DIN_SequencePerformanceTimeout                    SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_CommunicationSetupTimeout                     SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_ReadyToChargeTimeout                          SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_ContractAuthenticationMessageTimeout          SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_CableCheckMessageTimeout                      SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_ChargeParameterDiscoveryMessageTimeout        SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_CurrentDemandMessageTimeout                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_ServicePaymentSelectionMessageTimeout         SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_PowerDeliveryMessageTimeout                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_PreChargeMessageTimeout                       SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_ServiceDiscoveryMessageTimeout                SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_SessionSetupMessageTimeout                    SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_SessionStopMessageTimeout                     SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_Timer_DIN_WeldingDetectionMessageTimeout                SCC_CFG_TYPE_STATIC 



 /* State Machine ConfigType */
#define Scc_ConfigType_StateM_AcceptUnsecureConnection                         SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_ReconnectRetries                                 SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_ReconnectDelay                                   SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_RequestInternetDetails                           SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_AuthorizationNextReqDelay                        SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_ChargeParameterDiscoveryNextReqDelay             SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_AuthorizationOngoingTimeout                      SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_ChargeParameterDiscoveryOngoingTimeout           SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_CableCheckOngoingTimeout                         SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_PreChargeOngoingTimeout                          SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_CableCheckNextReqDelay                           SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_PreChargeNextReqDelay                            SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_CurrentDemandNextReqDelay                        SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_WeldingDetectionNextReqDelay                     SCC_CFG_TYPE_STATIC 
#define Scc_ConfigType_StateM_SLACStartMode                                    SCC_CFG_TYPE_EXTERN 
#define Scc_ConfigType_StateM_QCAIdleTimer                                     SCC_CFG_TYPE_EXTERN 

 
/**********************************************************************************************************************
 *  MISRA & PC-lint
 *********************************************************************************************************************/
/* PRQA L:FUNCTION_LIKE_MACROS */

#endif /* SCC_CONFIG_PARAMS_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Scc_ConfigParams_Cfg.h
 *********************************************************************************************************************/
 











