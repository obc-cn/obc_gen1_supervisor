/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Com
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Com_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:52
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
  MISRA / PClint JUSTIFICATIONS
**********************************************************************************************************************/
/* PRQA  S 1881 EOF */ /* MD_MSR_AutosarBoolean */
/* PRQA  S 1882 EOF */ /* MD_MSR_AutosarBoolean */

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#define V_IL_ASRCOMCFG5_LCFG_SOURCE

#include "Com.h"

#include "Com_Lcfg.h"

#include "Appl_Cbk.h"

#include "SchM_Com.h"

/**********************************************************************************************************************
  LOCAL CONSTANT MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  Com_CbkTxAckDefFuncPtr
**********************************************************************************************************************/
/** 
  \var    Com_CbkTxAckDefFuncPtr
  \brief  Function pointer table for deferred Tx confirmation notification functions.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComCbkTxAckDefType, COM_CONST) Com_CbkTxAckDefFuncPtr[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     CbkTxAckDefFuncPtr        Referable Keys */
  /*     0 */ DC2_SendNotification    /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_CbkTxTOutFuncPtr
**********************************************************************************************************************/
/** 
  \var    Com_CbkTxTOutFuncPtr
  \brief  Function pointer table containing configured Tx timeout notifications for signals and signal groups.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComCbkTxTOutType, COM_CONST) Com_CbkTxTOutFuncPtr[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     CbkTxTOutFuncPtr             Referable Keys */
  /*     0 */ DC2_TimeoutNotification    /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_ConstValueArrayBased
**********************************************************************************************************************/
/** 
  \var    Com_ConstValueArrayBased
  \brief  Optimized array of commonly used values like initial or invalid values. (UINT8_N, UINT8_DYN)
*/ 
#define COM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_ConstValueArrayBasedType, COM_CONST) Com_ConstValueArrayBased[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ConstValueArrayBased      Referable Keys */
  /*     0 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     1 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     2 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     3 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     4 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     5 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     6 */                 0x00u,  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
  /*     7 */                 0x00u   /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_RxInitValue, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_RxInitValue] */
};
#define COM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_ConstValueUInt16
**********************************************************************************************************************/
/** 
  \var    Com_ConstValueUInt16
  \brief  Optimized array of commonly used values like initial or invalid values. (UINT16)
*/ 
#define COM_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_ConstValueUInt16Type, COM_CONST) Com_ConstValueUInt16[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ConstValueUInt16      Referable Keys */
  /*     0 */           0x0000u   /* [/ActiveEcuC/Com/ComConfig/ABS_VehSpd_oVCU_PCANInfo_oE_CAN_c4f9375f_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_AuxBattVolt_oBMS3_oE_CAN_3f0258cb_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_DCRelayVoltage_oBMS9_oE_CAN_c2a98509_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_Fault_oBMS5_oE_CAN_87a5f6c7_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_HighestChargeCurrentAllow_oBMS6_oE_CAN_060c33c4_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_HighestChargeVoltageAllow_oBMS6_oE_CAN_c465fe5e_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_SOC_oBMS1_oE_CAN_4aa6784d_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_Voltage_oBMS1_oE_CAN_2c5227ff_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_ADC_2_5V_oDCHV_Status2_oInt_CAN_79e30624_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_ADC_IOUT_oDCHV_Status1_oInt_CAN_db9652d6_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_ADC_VOUT_oDCHV_Status1_oInt_CAN_95b5698f_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_Command_Current_Reference_oDCHV_Status1_oInt_CAN_205fcb72_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_Command_Voltage_Reference_oDCHV_Status1_oInt_CAN_0d7c27e1_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Applied_Derating_Factor_oDCLV_Status3_oInt_CAN_8199edb4_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Command_Current_Reference_oDCLV_Status1_oInt_CAN_572e045e_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Input_Voltage_oDCLV_Status3_oInt_CAN_a46268d5_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Measured_Current_oDCLV_Status1_oInt_CAN_4e0e970b_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Power_oDCLV_Status1_oInt_CAN_74948da2_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_CommandVdclink_V_oPFC_Status1_oInt_CAN_d9828b27_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH1_Freq_Hz_oPFC_Status5_oInt_CAN_6dc86329_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH1_Mean_0A1_oPFC_Status4_oInt_CAN_587b772d_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH1_Peak_0A1_oPFC_Status2_oInt_CAN_dfd13a45_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH1_RMS_0A1_oPFC_Status3_oInt_CAN_e736f57d_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH2_Freq_Hz_oPFC_Status5_oInt_CAN_7ee05a5a_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH2_Mean_0A1_oPFC_Status4_oInt_CAN_91647f92_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH2_Peak_0A1_oPFC_Status2_oInt_CAN_16ce32fa_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH2_RMS_0A1_oPFC_Status3_oInt_CAN_f41ecc0e_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH3_Freq_Hz_oPFC_Status5_oInt_CAN_7007b28b_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH3_Mean_0A1_oPFC_Status4_oInt_CAN_60be7a38_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH3_Peak_0A1_oPFC_Status2_oInt_CAN_e7143750_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IPH3_RMS_0A1_oPFC_Status3_oInt_CAN_faf924df_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH12_Mean_V_oPFC_Status4_oInt_CAN_6f1a2a48_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH12_Peak_V_oPFC_Status2_oInt_CAN_2da0b0ab_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH12_RMS_V_oPFC_Status3_oInt_CAN_dbb6f081_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH1_Freq_Hz_oPFC_Status5_oInt_CAN_489242f9_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH23_Mean_V_oPFC_Status4_oInt_CAN_4052f033_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH23_Peak_V_oPFC_Status2_oInt_CAN_02e86ad0_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH23_RMS_V_oPFC_Status3_oInt_CAN_fc100cbd_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH2_Freq_Hz_oPFC_Status5_oInt_CAN_5bba7b8a_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH31_Mean_V_oPFC_Status4_oInt_CAN_3674def2_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH31_Peak_V_oPFC_Status2_oInt_CAN_74ce4411_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH31_RMS_V_oPFC_Status3_oInt_CAN_067f5dfd_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_VPH3_Freq_Hz_oPFC_Status5_oInt_CAN_555d935b_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_Vdclink_V_oPFC_Status1_oInt_CAN_cb855707_RxInitValue] */
};
#define COM_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_ConstValueUInt32
**********************************************************************************************************************/
/** 
  \var    Com_ConstValueUInt32
  \brief  Optimized array of commonly used values like initial or invalid values. (UINT32)
*/ 
#define COM_START_SEC_CONST_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_ConstValueUInt32Type, COM_CONST) Com_ConstValueUInt32[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ConstValueUInt32      Referable Keys */
  /*     0 */       0x00000000u   /* [/ActiveEcuC/Com/ComConfig/VCU_CptTemporel_oVCU_oE_CAN_80a510d2_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_Kilometrage_oVCU_oE_CAN_90aff34b_RxInitValue] */
};
#define COM_STOP_SEC_CONST_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_ConstValueUInt8
**********************************************************************************************************************/
/** 
  \var    Com_ConstValueUInt8
  \brief  Optimized array of commonly used values like initial or invalid values. (BOOLEAN, UINT8)
*/ 
#define COM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_ConstValueUInt8Type, COM_CONST) Com_ConstValueUInt8[5] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ConstValueUInt8      Referable Keys */
  /*     0 */            0x08u,  /* [/ActiveEcuC/Com/ComConfig/PFC_PFCStatus_oPFC_Status1_oInt_CAN_4eddb451_RxInitValue] */
  /*     1 */            0x02u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_DCHVStatus_oDCHV_Status1_oInt_CAN_4479f96a_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_DCLVStatus_oDCLV_Status1_oInt_CAN_aede0f5b_RxInitValue] */
  /*     2 */            0x03u,  /* [/ActiveEcuC/Com/ComConfig/BSI_ChargeState_oBSIInfo_oE_CAN_be52c881_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_PosShuntJDD_oVCU_BSI_Wakeup_oE_CAN_0978ac9d_RxInitValue] */
  /*     3 */            0x01u,  /* [/ActiveEcuC/Com/ComConfig/BMS_CC2_connection_Status_oBMS8_oE_CAN_ce1fe0ec_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_DCDCActivation_oCtrlDCDC_oE_CAN_541eed40_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_EPWT_Status_oParkCommand_oE_CAN_c4868b19_RxInitValue] */
  /*     4 */            0x00u   /* [/ActiveEcuC/Com/ComConfig/ABS_VehSpdValidFlag_oVCU_PCANInfo_oE_CAN_badcc650_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_FastChargeSt_oBMS6_oE_CAN_21564382_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_MainConnectorState_oBMS1_oE_CAN_db099b33_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_OnBoardChargerEnable_oBMS6_oE_CAN_b5381f70_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_QuickChargeConnectorState_oBMS1_oE_CAN_940005e5_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_RelayOpenReq_oBMS1_oE_CAN_9e155f98_RxInitValue, /ActiveEcuC/Com/ComConfig/BMS_SlowChargeSt_oBMS6_oE_CAN_25b6b066_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_ChargeTypeStatus_oBSIInfo_oE_CAN_f023c7ab_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_LockedVehicle_oBSIInfo_oE_CAN_bee231f7_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_MainWakeup_oBSIInfo_oE_CAN_3acf26ac_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_PostDriveWakeup_oBSIInfo_oE_CAN_c679787c_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_PreDriveWakeup_oBSIInfo_oE_CAN_10f8dc31_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_VCUHeatPumpWorkingMode_oBSIInfo_oE_CAN_56dcb551_RxInitValue, /ActiveEcuC/Com/ComConfig/BSI_VCUSynchroGPC_oBSIInfo_oE_CAN_8a27e8bd_RxInitValue, /ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTPE2_oVCU_PCANInfo_oE_CAN_4e44e208_RxInitValue, /ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTP_oVCU_PCANInfo_oE_CAN_1a153f59_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA0_oProgTool_SupEnterBoot_oInt_CAN_d0fc95c5_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA1_oProgTool_SupEnterBoot_oInt_CAN_e60e0536_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA2_oProgTool_SupEnterBoot_oInt_CAN_bd19b423_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA3_oProgTool_SupEnterBoot_oInt_CAN_8beb24d0_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA4_oProgTool_SupEnterBoot_oInt_CAN_0b36d609_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA5_oProgTool_SupEnterBoot_oInt_CAN_3dc446fa_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA6_oProgTool_SupEnterBoot_oInt_CAN_66d3f7ef_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA7_oProgTool_SupEnterBoot_oInt_CAN_5021671c_RxInitValue, /ActiveEcuC/Com/ComConfig/DATA_ACQ_JDD_BSI_2_oNEW_JDD_oE_CAN_16efcc6e_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_5_oDCHV_Status2_oInt_CAN_46287fbe_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_6_oDCHV_Status2_oInt_CAN_1e36d696_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_H_oDCHV_Fault_oInt_CAN_a3babff0_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_L_oDCHV_Fault_oInt_CAN_ff1b2cf0_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_IOUT_oDCHV_Fault_oInt_CAN_2f168ffb_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_NEG_oDCHV_Fault_oInt_CAN_d091e3ea_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_POS_oDCHV_Fault_oInt_CAN_d5b3bbb7_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_IN_oDCHV_Fault_oInt_CAN_5338fe6d_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD5_oDCHV_Fault_oInt_CAN_05da4c61_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD6_oDCHV_Fault_oInt_CAN_3ca2e121_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_oDCHV_Fault_oInt_CAN_33753ff7_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OV_VOUT_oDCHV_Fault_oInt_CAN_c4e31cdb_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_UV_12V_oDCHV_Fault_oInt_CAN_7ceb9676_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4C7_AC_oDCHV_Fault_oInt_CAN_033d492e_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4E5_AC_oDCHV_Status1_oInt_CAN_91fed45b_RxInitValue, /ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4FA_AC_oDCHV_Status2_oInt_CAN_596855d0_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Command_Voltage_Reference_oDCLV_Status1_oInt_CAN_7a0de8cd_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Input_Current_oDCLV_Status3_oInt_CAN_053924dc_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_Measured_Voltage_oDCLV_Status1_oInt_CAN_ef55db02_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OC_A_HV_FAULT_oDCLV_Fault_oInt_CAN_7ab3f331_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OC_A_LV_FAULT_oDCLV_Fault_oInt_CAN_2ddd91e0_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OC_B_HV_FAULT_oDCLV_Fault_oInt_CAN_699bca42_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OC_B_LV_FAULT_oDCLV_Fault_oInt_CAN_3ef5a893_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OT_FAULT_oDCLV_Fault_oInt_CAN_334ae1fe_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OV_INT_A_FAULT_oDCLV_Fault_oInt_CAN_3c3a442a_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OV_INT_B_FAULT_oDCLV_Fault_oInt_CAN_3ee4430d_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_OV_UV_HV_FAULT_oDCLV_Fault_oInt_CAN_3d935207_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_PWM_STOP_oDCLV_Fault_oInt_CAN_13fc246e_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_496_AC_oDCLV_Fault_oInt_CAN_974bbf25_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_497_AC_oDCLV_Status1_oInt_CAN_90fced42_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_498_AC_oDCLV_Status2_oInt_CAN_6b9cc64d_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_499_AC_oDCLV_Status3_oInt_CAN_ead2b632_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OC_ALARM_oDCLV_Fault_oInt_CAN_d37c4d73_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OV_ALARM_oDCLV_Fault_oInt_CAN_cf4195ca_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_SW_HV_UV_ALARM_oDCLV_Fault_oInt_CAN_215a6f25_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OC_ALARM_oDCLV_Fault_oInt_CAN_a2f65759_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OV_ALARM_oDCLV_Fault_oInt_CAN_becb8fe0_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_T_L_BUCK_oDCLV_Status2_oInt_CAN_f4d254b0_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_T_PP_A_oDCLV_Status2_oInt_CAN_33203dc6_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_T_PP_B_oDCLV_Status2_oInt_CAN_6b3e94ee_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_A_oDCLV_Status2_oInt_CAN_f172fb4d_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_B_oDCLV_Status2_oInt_CAN_a96c5265_RxInitValue, /ActiveEcuC/Com/ComConfig/DCLV_T_TX_PP_oDCLV_Status2_oInt_CAN_19721921_RxInitValue, /ActiveEcuC/Com/ComConfig/DIAG_INTEGRA_ELEC_oELECTRON_BSI_oE_CAN_fb8cbe6b_RxInitValue, /ActiveEcuC/Com/ComConfig/EFFAC_DEFAUT_DIAG_oELECTRON_BSI_oE_CAN_83a2b5c6_RxInitValue, /ActiveEcuC/Com/ComConfig/MODE_DIAG_oELECTRON_BSI_oE_CAN_f76ac80c_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH1_oPFC_Fault_oInt_CAN_a122859f_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH2_oPFC_Fault_oInt_CAN_da3c077c_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH3_oPFC_Fault_oInt_CAN_45e684e2_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT1_oPFC_Fault_oInt_CAN_25f6ba61_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT2_oPFC_Fault_oInt_CAN_5ee83882_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT3_oPFC_Fault_oInt_CAN_c132bb1c_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M1_oPFC_Fault_oInt_CAN_a0dec855_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M3_oPFC_Fault_oInt_CAN_441ac928_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M4_oPFC_Fault_oInt_CAN_2dfd4f70_RxInitValue, 
              /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_DCLINK_oPFC_Fault_oInt_CAN_df590e67_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH12_oPFC_Fault_oInt_CAN_70e217b6_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH23_oPFC_Fault_oInt_CAN_d6403968_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH31_oPFC_Fault_oInt_CAN_25ac5cd5_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO4_oPFC_Fault_oInt_CAN_83f330a8_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO7_oPFC_Fault_oInt_CAN_f8edb24b_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UV_12V_oPFC_Fault_oInt_CAN_a7954067_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_420_AC_oPFC_Fault_oInt_CAN_571a9c7f_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_444_AC_oPFC_Status1_oInt_CAN_8a8e39e7_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_455_AC_oPFC_Status2_oInt_CAN_e8283f32_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_466_AC_oPFC_Status3_oInt_CAN_46299437_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_477_AC_oPFC_Status4_oInt_CAN_2d643298_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_488_AC_oPFC_Status5_oInt_CAN_5f1b9f30_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_Temp_M1_C_oPFC_Status1_oInt_CAN_23fc84dd_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_Temp_M3_C_oPFC_Status1_oInt_CAN_b39aa280_RxInitValue, /ActiveEcuC/Com/ComConfig/PFC_Temp_M4_C_oPFC_Status1_oInt_CAN_7dca5e28_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_AccPedalPosition_oVCU_TU_oE_CAN_a12a1de4_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_ActivedischargeCommand_oCtrlDCDC_oE_CAN_6f6ad9a3_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_CDEAccJDD_oVCU_BSI_Wakeup_oE_CAN_153d1d79_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_CDEApcJDD_oVCU_BSI_Wakeup_oE_CAN_0cbccb8e_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_DCDCVoltageReq_oCtrlDCDC_oE_CAN_2adee191_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_DDEGMVSEEM_oVCU_BSI_Wakeup_oE_CAN_073a8f6a_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_DMDActivChiller_oVCU_BSI_Wakeup_oE_CAN_eb265576_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_DMDMeap2SEEM_oVCU_BSI_Wakeup_oE_CAN_3b64713a_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_DiagMuxOnPwt_oVCU_BSI_Wakeup_oE_CAN_712c6c84_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_ElecMeterSaturation_oVCU3_oE_CAN_c481c141_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_EtatGmpHyb_oVCU_BSI_Wakeup_oE_CAN_0076bb1d_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_EtatPrincipSev_oVCU_BSI_Wakeup_oE_CAN_8c281110_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_EtatReseauElec_oVCU_BSI_Wakeup_oE_CAN_b805a640_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_FrameChksum17B_oVCU_PCANInfo_oE_CAN_657e6685_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_Keyposition_oVCU2_oE_CAN_7b65e581_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_ModeEPSRequest_oVCU_BSI_Wakeup_oE_CAN_760ae031_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_PrecondElecWakeup_oVCU_BSI_Wakeup_oE_CAN_6616770e_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_RollingCounter17B_oVCU_PCANInfo_oE_CAN_57322517_RxInitValue, /ActiveEcuC/Com/ComConfig/VCU_StopDelayedHMIWakeup_oVCU_BSI_Wakeup_oE_CAN_05bb1585_RxInitValue] */
};
#define COM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_PduGrpVector
**********************************************************************************************************************/
/** 
  \var    Com_PduGrpVector
  \brief  Contains an I-PDU-Group vector for each I-PDU, mapping the I-PDU to the corresponding I-PDU-Groups.
*/ 
#define COM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_PduGrpVectorType, COM_CONST) Com_PduGrpVector[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     PduGrpVector      Referable Keys */
  /*     0 */         0x08u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*     1 */         0x04u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*     2 */         0x02u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*     3 */         0x01u   /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx, /ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx, /ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx, /ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx, /ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx, /ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx, /ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx, /ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx, /ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx, /ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx, /ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx, /ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx, /ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx, /ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx, /ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
};
#define COM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxAccessInfo
**********************************************************************************************************************/
/** 
  \var    Com_RxAccessInfo
  \brief  Contains all signal layout information necessary for signal access within an I-PDU.
  \details
  Element                                  Description
  InitValueUsed                            TRUE, if the 0:1 relation has minimum 1 relation pointing to Com_ConstValueUInt8,Com_ConstValueUInt16,Com_ConstValueUInt32,Com_ConstValueUInt64,Com_ConstValueSInt8,Com_ConstValueSInt16,Com_ConstValueSInt32,Com_ConstValueSInt64,Com_ConstValueFloat32,Com_ConstValueFloat64
  RxSigBufferArrayBasedBufferUsed          TRUE, if the 0:n relation has 1 relation pointing to Com_RxSigBufferArrayBased
  ApplType                                 Application data type.
  BitLength                                Bit length of the signal or group signal.
  BitPosition                              Little endian bit position of the signal or group signal within the I-PDU.
  BufferIdx                                the index of the 0:1 relation pointing to Com_RxSigBufferUInt8,Com_RxSigBufferUInt16,Com_RxSigBufferUInt32,Com_RxSigBufferUInt64,Com_RxSigBufferZeroBit,Com_RxSigBufferSInt8,Com_RxSigBufferSInt16,Com_RxSigBufferSInt32,Com_RxSigBufferSInt64,Com_RxSigBufferFloat32,Com_RxSigBufferFloat64
  BusAcc                                   BUS access algorithm for signal or group signal packing / un-packing.
  ByteLength                               Byte length of the signal or group signal.
  BytePosition                             Little endian byte position of the signal or group signal within the I-PDU.
  ConstValueArrayBasedInitValueEndIdx      the end index of the 0:n relation pointing to Com_ConstValueArrayBased
  ConstValueArrayBasedInitValueStartIdx    the start index of the 0:n relation pointing to Com_ConstValueArrayBased
  InitValueIdx                             the index of the 0:1 relation pointing to Com_ConstValueUInt8,Com_ConstValueUInt16,Com_ConstValueUInt32,Com_ConstValueUInt64,Com_ConstValueSInt8,Com_ConstValueSInt16,Com_ConstValueSInt32,Com_ConstValueSInt64,Com_ConstValueFloat32,Com_ConstValueFloat64
  RxPduInfoIdx                             the index of the 1:1 relation pointing to Com_RxPduInfo
  RxSigBufferArrayBasedBufferEndIdx        the end index of the 0:n relation pointing to Com_RxSigBufferArrayBased
  RxSigBufferArrayBasedBufferStartIdx      the start index of the 0:n relation pointing to Com_RxSigBufferArrayBased
  StartByteInPduPosition                   Start Byte position of the signal or group signal within the I-PDU.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_RxAccessInfoType, COM_CONST) Com_RxAccessInfo[172] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    InitValueUsed  RxSigBufferArrayBasedBufferUsed  ApplType                            BitLength  BitPosition  BufferIdx                       BusAcc                                 ByteLength  BytePosition  ConstValueArrayBasedInitValueEndIdx                       ConstValueArrayBasedInitValueStartIdx                       InitValueIdx                       RxPduInfoIdx  RxSigBufferArrayBasedBufferEndIdx                       RxSigBufferArrayBasedBufferStartIdx                       StartByteInPduPosition        Referable Keys */
  { /*     0 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         23u,                             0u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          30u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/ABS_VehSpdValidFlag_oVCU_PCANInfo_oE_CAN_badcc650_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*     1 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       16u,          8u,                             0u,     COM_NBYTE_SW_BUSACCOFRXACCESSINFO,         2u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          30u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/ABS_VehSpd_oVCU_PCANInfo_oE_CAN_c4f9375f_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*     2 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       12u,         24u,                             1u, COM_NBITNBYTE_SW_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           1u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/BMS_AuxBattVolt_oBMS3_oE_CAN_3f0258cb_Rx, /ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  { /*     3 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,         16u,                             1u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                3u,           4u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/BMS_CC2_connection_Status_oBMS8_oE_CAN_ce1fe0ec_Rx, /ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  { /*     4 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,        9u,         15u,                             2u, COM_NBITNBYTE_SW_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           5u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BMS_DCRelayVoltage_oBMS9_oE_CAN_c2a98509_Rx, /ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  { /*     5 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,         54u,                             2u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           3u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/BMS_FastChargeSt_oBMS6_oE_CAN_21564382_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*     6 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       12u,         12u,                             3u, COM_NBITNBYTE_SW_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           2u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BMS_Fault_oBMS5_oE_CAN_87a5f6c7_Rx, /ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  { /*     7 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       12u,         56u,                             4u, COM_NBITNBYTE_SW_BUSACCOFRXACCESSINFO,         1u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           3u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/BMS_HighestChargeCurrentAllow_oBMS6_oE_CAN_060c33c4_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*     8 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       16u,          8u,                             5u,     COM_NBYTE_SW_BUSACCOFRXACCESSINFO,         2u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           3u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BMS_HighestChargeVoltageAllow_oBMS6_oE_CAN_c465fe5e_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*     9 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,         11u,                             3u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           0u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/BMS_MainConnectorState_oBMS1_oE_CAN_db099b33_Rx, /ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*    10 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         31u,                             4u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           3u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/BMS_OnBoardChargerEnable_oBMS6_oE_CAN_b5381f70_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*    11 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         10u,                             5u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           0u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/BMS_QuickChargeConnectorState_oBMS1_oE_CAN_940005e5_Rx, /ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*    12 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         38u,                             6u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           0u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/BMS_RelayOpenReq_oBMS1_oE_CAN_9e155f98_Rx, /ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*    13 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         14u,                             6u, COM_NBITNBYTE_SW_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           0u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BMS_SOC_oBMS1_oE_CAN_4aa6784d_Rx, /ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*    14 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,         29u,                             7u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           3u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/BMS_SlowChargeSt_oBMS6_oE_CAN_25b6b066_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*    15 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,        9u,         39u,                             7u, COM_NBITNBYTE_SW_BUSACCOFRXACCESSINFO,         1u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           0u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/BMS_Voltage_oBMS1_oE_CAN_2c5227ff_Rx, /ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*    16 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,         14u,                             8u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                2u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/BSI_ChargeState_oBSIInfo_oE_CAN_be52c881_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    17 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          2u,                             9u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BSI_ChargeTypeStatus_oBSIInfo_oE_CAN_f023c7ab_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    18 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          4u,                            10u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BSI_LockedVehicle_oBSIInfo_oE_CAN_bee231f7_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    19 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          0u,                            11u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BSI_MainWakeup_oBSIInfo_oE_CAN_3acf26ac_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    20 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          7u,                            12u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BSI_PostDriveWakeup_oBSIInfo_oE_CAN_c679787c_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    21 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          6u,                            13u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/BSI_PreDriveWakeup_oBSIInfo_oE_CAN_10f8dc31_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    22 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        5u,          8u,                            14u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/BSI_VCUHeatPumpWorkingMode_oBSIInfo_oE_CAN_56dcb551_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    23 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         13u,                            15u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           6u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/BSI_VCUSynchroGPC_oBSIInfo_oE_CAN_8a27e8bd_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    24 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         36u,                            16u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          30u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTPE2_oVCU_PCANInfo_oE_CAN_4e44e208_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*    25 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         35u,                            17u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          30u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTP_oVCU_PCANInfo_oE_CAN_1a153f59_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*    26 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          0u,                            18u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DATA0_oProgTool_SupEnterBoot_oInt_CAN_d0fc95c5_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    27 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          8u,                            19u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DATA1_oProgTool_SupEnterBoot_oInt_CAN_e60e0536_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    28 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         16u,                            20u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DATA2_oProgTool_SupEnterBoot_oInt_CAN_bd19b423_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    29 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         24u,                            21u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/DATA3_oProgTool_SupEnterBoot_oInt_CAN_8beb24d0_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    30 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         32u,                            22u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/DATA4_oProgTool_SupEnterBoot_oInt_CAN_0b36d609_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    31 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         40u,                            23u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/DATA5_oProgTool_SupEnterBoot_oInt_CAN_3dc446fa_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    32 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         48u,                            24u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/DATA6_oProgTool_SupEnterBoot_oInt_CAN_66d3f7ef_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    33 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         56u,                            25u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          24u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DATA7_oProgTool_SupEnterBoot_oInt_CAN_5021671c_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*    34 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          0u,                            26u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          16u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DATA_ACQ_JDD_BSI_2_oNEW_JDD_oE_CAN_16efcc6e_Rx, /ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx] */
  { /*    35 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         26u,                             8u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          10u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_2_5V_oDCHV_Status2_oInt_CAN_79e30624_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    36 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       12u,         44u,                             9u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           9u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_IOUT_oDCHV_Status1_oInt_CAN_db9652d6_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    37 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          0u,                            27u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          10u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_5_oDCHV_Status2_oInt_CAN_46287fbe_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    38 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          8u,                            28u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          10u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_6_oDCHV_Status2_oInt_CAN_1e36d696_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    39 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       16u,         16u,                            10u,        COM_NBYTE_BUSACCOFRXACCESSINFO,         2u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           9u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_VOUT_oDCHV_Status1_oInt_CAN_95b5698f_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    40 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       11u,         32u,                            11u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           9u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Command_Current_Reference_oDCHV_Status1_oInt_CAN_205fcb72_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    41 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       16u,          0u,                            12u,        COM_NBYTE_BUSACCOFRXACCESSINFO,         2u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,           9u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Command_Voltage_Reference_oDCHV_Status1_oInt_CAN_0d7c27e1_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    42 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         56u,                            29u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                1u,           9u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_DCHVStatus_oDCHV_Status1_oInt_CAN_4479f96a_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    43 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         10u,                            30u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_H_oDCHV_Fault_oInt_CAN_a3babff0_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    44 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         11u,                            31u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_L_oDCHV_Fault_oInt_CAN_ff1b2cf0_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    45 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          0u,                            32u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_IOUT_oDCHV_Fault_oInt_CAN_2f168ffb_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    46 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          9u,                            33u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_NEG_oDCHV_Fault_oInt_CAN_d091e3ea_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    47 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          8u,                            34u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_POS_oDCHV_Fault_oInt_CAN_d5b3bbb7_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    48 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          5u,                            35u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_IN_oDCHV_Fault_oInt_CAN_5338fe6d_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    49 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          3u,                            36u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD5_oDCHV_Fault_oInt_CAN_05da4c61_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
    /* Index    InitValueUsed  RxSigBufferArrayBasedBufferUsed  ApplType                            BitLength  BitPosition  BufferIdx                       BusAcc                                 ByteLength  BytePosition  ConstValueArrayBasedInitValueEndIdx                       ConstValueArrayBasedInitValueStartIdx                       InitValueIdx                       RxPduInfoIdx  RxSigBufferArrayBasedBufferEndIdx                       RxSigBufferArrayBasedBufferStartIdx                       StartByteInPduPosition        Referable Keys */
  { /*    50 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          4u,                            37u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD6_oDCHV_Fault_oInt_CAN_3ca2e121_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    51 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          2u,                            38u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_oDCHV_Fault_oInt_CAN_33753ff7_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    52 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          1u,                            39u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OV_VOUT_oDCHV_Fault_oInt_CAN_c4e31cdb_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    53 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          6u,                            40u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_UV_12V_oDCHV_Fault_oInt_CAN_7ceb9676_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    54 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            41u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           8u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4C7_AC_oDCHV_Fault_oInt_CAN_033d492e_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    55 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            42u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           9u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4E5_AC_oDCHV_Status1_oInt_CAN_91fed45b_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    56 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            43u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          10u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4FA_AC_oDCHV_Status2_oInt_CAN_596855d0_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    57 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       11u,         24u,                            13u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          14u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Applied_Derating_Factor_oDCLV_Status3_oInt_CAN_8199edb4_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    58 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       16u,          0u,                            14u,        COM_NBYTE_BUSACCOFRXACCESSINFO,         2u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Command_Current_Reference_oDCLV_Status1_oInt_CAN_572e045e_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    59 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        7u,         32u,                            44u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Command_Voltage_Reference_oDCLV_Status1_oInt_CAN_7a0de8cd_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    60 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         28u,                            45u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                1u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_DCLVStatus_oDCLV_Status1_oInt_CAN_aede0f5b_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    61 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          0u,                            46u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          14u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Input_Current_oDCLV_Status3_oInt_CAN_053924dc_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    62 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,          8u,                            15u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          14u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Input_Voltage_oDCLV_Status3_oInt_CAN_a46268d5_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    63 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       12u,         16u,                            16u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Measured_Current_oDCLV_Status1_oInt_CAN_4e0e970b_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    64 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         40u,                            47u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Measured_Voltage_oDCLV_Status1_oInt_CAN_ef55db02_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    65 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          3u,                            48u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_A_HV_FAULT_oDCLV_Fault_oInt_CAN_7ab3f331_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    66 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          8u,                            49u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_A_LV_FAULT_oDCLV_Fault_oInt_CAN_2ddd91e0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    67 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          4u,                            50u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_B_HV_FAULT_oDCLV_Fault_oInt_CAN_699bca42_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    68 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          9u,                            51u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_B_LV_FAULT_oDCLV_Fault_oInt_CAN_3ef5a893_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    69 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          2u,                            52u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OT_FAULT_oDCLV_Fault_oInt_CAN_334ae1fe_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    70 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          5u,                            53u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OV_INT_A_FAULT_oDCLV_Fault_oInt_CAN_3c3a442a_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    71 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          6u,                            54u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OV_INT_B_FAULT_oDCLV_Fault_oInt_CAN_3ee4430d_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    72 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          7u,                            55u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_OV_UV_HV_FAULT_oDCLV_Fault_oInt_CAN_3d935207_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    73 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          0u,                            56u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_PWM_STOP_oDCLV_Fault_oInt_CAN_13fc246e_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    74 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       12u,         48u,                            17u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Power_oDCLV_Status1_oInt_CAN_74948da2_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    75 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            57u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_496_AC_oDCLV_Fault_oInt_CAN_974bbf25_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    76 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            58u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          12u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_497_AC_oDCLV_Status1_oInt_CAN_90fced42_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    77 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            59u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_498_AC_oDCLV_Status2_oInt_CAN_6b9cc64d_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    78 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            60u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          14u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_499_AC_oDCLV_Status3_oInt_CAN_ead2b632_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    79 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         15u,                            61u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OC_ALARM_oDCLV_Fault_oInt_CAN_d37c4d73_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    80 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         14u,                            62u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OV_ALARM_oDCLV_Fault_oInt_CAN_cf4195ca_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    81 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         18u,                            63u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_HV_UV_ALARM_oDCLV_Fault_oInt_CAN_215a6f25_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    82 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         17u,                            64u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OC_ALARM_oDCLV_Fault_oInt_CAN_a2f65759_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    83 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         16u,                            65u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          11u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OV_ALARM_oDCLV_Fault_oInt_CAN_becb8fe0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    84 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         32u,                            66u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_L_BUCK_oDCLV_Status2_oInt_CAN_f4d254b0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    85 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         16u,                            67u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_PP_A_oDCLV_Status2_oInt_CAN_33203dc6_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    86 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         24u,                            68u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_PP_B_oDCLV_Status2_oInt_CAN_6b3e94ee_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    87 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          0u,                            69u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_A_oDCLV_Status2_oInt_CAN_f172fb4d_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    88 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,          8u,                            70u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_B_oDCLV_Status2_oInt_CAN_a96c5265_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    89 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         40u,                            71u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          13u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_TX_PP_oDCLV_Status2_oInt_CAN_19721921_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    90 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          7u,                            72u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          15u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/DIAG_INTEGRA_ELEC_oELECTRON_BSI_oE_CAN_fb8cbe6b_Rx, /ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  { /*    91 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          0u,                            73u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          15u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/EFFAC_DEFAUT_DIAG_oELECTRON_BSI_oE_CAN_83a2b5c6_Rx, /ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  { /*    92 */         FALSE,                            TRUE, COM_UINT8_N_APPLTYPEOFRXACCESSINFO,       64u,          0u, COM_NO_BUFFERIDXOFRXACCESSINFO,  COM_ARRAY_BASED_BUSACCOFRXACCESSINFO,         8u,           0u,                                                       8u,                                                         0u, COM_NO_INITVALUEIDXOFRXACCESSINFO,          25u,                                                     8u,                                                       0u,                     0u },  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx, /ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  { /*    93 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          6u,                            74u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          15u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/MODE_DIAG_oELECTRON_BSI_oE_CAN_f76ac80c_Rx, /ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  { /*    94 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,          4u,                            18u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_CommandVdclink_V_oPFC_Status1_oInt_CAN_d9828b27_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*    95 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         12u,                            75u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH1_oPFC_Fault_oInt_CAN_a122859f_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    96 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         11u,                            76u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH2_oPFC_Fault_oInt_CAN_da3c077c_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    97 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         10u,                            77u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH3_oPFC_Fault_oInt_CAN_45e684e2_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    98 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         13u,                            78u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT1_oPFC_Fault_oInt_CAN_25f6ba61_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    99 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         14u,                            79u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT2_oPFC_Fault_oInt_CAN_5ee83882_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
    /* Index    InitValueUsed  RxSigBufferArrayBasedBufferUsed  ApplType                            BitLength  BitPosition  BufferIdx                       BusAcc                                 ByteLength  BytePosition  ConstValueArrayBasedInitValueEndIdx                       ConstValueArrayBasedInitValueStartIdx                       InitValueIdx                       RxPduInfoIdx  RxSigBufferArrayBasedBufferEndIdx                       RxSigBufferArrayBasedBufferStartIdx                       StartByteInPduPosition        Referable Keys */
  { /*   100 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         15u,                            80u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT3_oPFC_Fault_oInt_CAN_c132bb1c_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   101 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          5u,                            81u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M1_oPFC_Fault_oInt_CAN_a0dec855_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   102 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          4u,                            82u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M3_oPFC_Fault_oInt_CAN_441ac928_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   103 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          3u,                            83u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M4_oPFC_Fault_oInt_CAN_2dfd4f70_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   104 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          6u,                            84u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_DCLINK_oPFC_Fault_oInt_CAN_df590e67_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   105 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          7u,                            85u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH12_oPFC_Fault_oInt_CAN_70e217b6_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   106 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          9u,                            86u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH23_oPFC_Fault_oInt_CAN_d6403968_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   107 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          8u,                            87u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH31_oPFC_Fault_oInt_CAN_25ac5cd5_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   108 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          2u,                            88u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO4_oPFC_Fault_oInt_CAN_83f330a8_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   109 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          0u,                            89u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO7_oPFC_Fault_oInt_CAN_f8edb24b_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   110 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          1u,                            90u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UV_12V_oPFC_Fault_oInt_CAN_a7954067_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   111 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         30u,                            19u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_Freq_Hz_oPFC_Status5_oInt_CAN_6dc86329_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   112 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         30u,                            20u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_Mean_0A1_oPFC_Status4_oInt_CAN_587b772d_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   113 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         30u,                            21u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_Peak_0A1_oPFC_Status2_oInt_CAN_dfd13a45_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   114 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         30u,                            22u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_RMS_0A1_oPFC_Status3_oInt_CAN_e736f57d_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   115 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         40u,                            23u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_Freq_Hz_oPFC_Status5_oInt_CAN_7ee05a5a_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   116 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         40u,                            24u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_Mean_0A1_oPFC_Status4_oInt_CAN_91647f92_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   117 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         40u,                            25u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_Peak_0A1_oPFC_Status2_oInt_CAN_16ce32fa_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   118 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         40u,                            26u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_RMS_0A1_oPFC_Status3_oInt_CAN_f41ecc0e_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   119 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         50u,                            27u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_Freq_Hz_oPFC_Status5_oInt_CAN_7007b28b_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   120 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         50u,                            28u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_Mean_0A1_oPFC_Status4_oInt_CAN_60be7a38_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   121 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         50u,                            29u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_Peak_0A1_oPFC_Status2_oInt_CAN_e7143750_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   122 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         50u,                            30u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     6u },  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_RMS_0A1_oPFC_Status3_oInt_CAN_faf924df_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   123 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,          0u,                            91u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_PFCStatus_oPFC_Status1_oInt_CAN_4eddb451_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   124 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            92u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          17u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_420_AC_oPFC_Fault_oInt_CAN_571a9c7f_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   125 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            93u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_444_AC_oPFC_Status1_oInt_CAN_8a8e39e7_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   126 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            94u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_455_AC_oPFC_Status2_oInt_CAN_e8283f32_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   127 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            95u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_466_AC_oPFC_Status3_oInt_CAN_46299437_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   128 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            96u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_477_AC_oPFC_Status4_oInt_CAN_2d643298_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   129 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                            97u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_488_AC_oPFC_Status5_oInt_CAN_5f1b9f30_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   130 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         24u,                            98u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Temp_M1_C_oPFC_Status1_oInt_CAN_23fc84dd_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   131 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         32u,                            99u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Temp_M3_C_oPFC_Status1_oInt_CAN_b39aa280_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   132 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         40u,                           100u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           5u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Temp_M4_C_oPFC_Status1_oInt_CAN_7dca5e28_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   133 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,          0u,                            31u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH1_Freq_Hz_oPFC_Status5_oInt_CAN_489242f9_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   134 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         10u,                            32u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH2_Freq_Hz_oPFC_Status5_oInt_CAN_5bba7b8a_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   135 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         20u,                            33u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          22u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH3_Freq_Hz_oPFC_Status5_oInt_CAN_555d935b_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   136 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,          0u,                            34u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH12_Mean_V_oPFC_Status4_oInt_CAN_6f1a2a48_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   137 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,          0u,                            35u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH12_Peak_V_oPFC_Status2_oInt_CAN_2da0b0ab_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   138 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,          0u,                            36u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH12_RMS_V_oPFC_Status3_oInt_CAN_dbb6f081_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   139 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         10u,                            37u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH23_Mean_V_oPFC_Status4_oInt_CAN_4052f033_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   140 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         10u,                            38u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH23_Peak_V_oPFC_Status2_oInt_CAN_02e86ad0_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   141 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         10u,                            39u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH23_RMS_V_oPFC_Status3_oInt_CAN_fc100cbd_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   142 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         20u,                            40u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          21u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH31_Mean_V_oPFC_Status4_oInt_CAN_3674def2_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   143 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         20u,                            41u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          19u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH31_Peak_V_oPFC_Status2_oInt_CAN_74ce4411_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   144 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         20u,                            42u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          20u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH31_RMS_V_oPFC_Status3_oInt_CAN_067f5dfd_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   145 */          TRUE,                           FALSE,  COM_UINT16_APPLTYPEOFRXACCESSINFO,       10u,         14u,                            43u,    COM_NBITNBYTE_BUSACCOFRXACCESSINFO,         1u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          18u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Vdclink_V_oPFC_Status1_oInt_CAN_cb855707_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   146 */         FALSE,                            TRUE, COM_UINT8_N_APPLTYPEOFRXACCESSINFO,       64u,          0u, COM_NO_BUFFERIDXOFRXACCESSINFO,  COM_ARRAY_BASED_BUSACCOFRXACCESSINFO,         8u,           0u,                                                       8u,                                                         0u, COM_NO_INITVALUEIDXOFRXACCESSINFO,          26u,                                                    16u,                                                       8u,                     0u },  /* [/ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx, /ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  { /*   147 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         24u,                           101u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          31u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/VCU_AccPedalPosition_oVCU_TU_oE_CAN_a12a1de4_Rx, /ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */
  { /*   148 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         13u,                           102u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           7u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_ActivedischargeCommand_oCtrlDCDC_oE_CAN_6f6ad9a3_Rx, /ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  { /*   149 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          0u,                           103u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_CDEAccJDD_oVCU_BSI_Wakeup_oE_CAN_153d1d79_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
    /* Index    InitValueUsed  RxSigBufferArrayBasedBufferUsed  ApplType                            BitLength  BitPosition  BufferIdx                       BusAcc                                 ByteLength  BytePosition  ConstValueArrayBasedInitValueEndIdx                       ConstValueArrayBasedInitValueStartIdx                       InitValueIdx                       RxPduInfoIdx  RxSigBufferArrayBasedBufferEndIdx                       RxSigBufferArrayBasedBufferStartIdx                       StartByteInPduPosition        Referable Keys */
  { /*   150 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          1u,                           104u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_CDEApcJDD_oVCU_BSI_Wakeup_oE_CAN_0cbccb8e_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   151 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         56u,                           105u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          32u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_Rx, /ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  { /*   152 */          TRUE,                           FALSE,  COM_UINT32_APPLTYPEOFRXACCESSINFO,       32u,         24u,                             0u,     COM_NBYTE_SW_BUSACCOFRXACCESSINFO,         4u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          32u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_CptTemporel_oVCU_oE_CAN_80a510d2_Rx, /ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  { /*   153 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,         14u,                           106u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                3u,           7u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_DCDCActivation_oCtrlDCDC_oE_CAN_541eed40_Rx, /ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  { /*   154 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        7u,          1u,                           107u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,           7u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_DCDCVoltageReq_oCtrlDCDC_oE_CAN_2adee191_Rx, /ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  { /*   155 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        8u,         16u,                           108u,         COM_BYTE_BUSACCOFRXACCESSINFO,         1u,           2u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     2u },  /* [/ActiveEcuC/Com/ComConfig/VCU_DDEGMVSEEM_oVCU_BSI_Wakeup_oE_CAN_073a8f6a_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   156 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         35u,                           109u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_DMDActivChiller_oVCU_BSI_Wakeup_oE_CAN_eb265576_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   157 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        7u,         24u,                           110u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u },  /* [/ActiveEcuC/Com/ComConfig/VCU_DMDMeap2SEEM_oVCU_BSI_Wakeup_oE_CAN_3b64713a_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   158 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         34u,                           111u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_DiagMuxOnPwt_oVCU_BSI_Wakeup_oE_CAN_712c6c84_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   159 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          3u,                           112u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                3u,          23u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_EPWT_Status_oParkCommand_oE_CAN_c4868b19_Rx, /ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  { /*   160 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,          7u,                           113u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          28u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_ElecMeterSaturation_oVCU3_oE_CAN_c481c141_Rx, /ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  { /*   161 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,          8u,                           114u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_EtatGmpHyb_oVCU_BSI_Wakeup_oE_CAN_0076bb1d_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   162 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          6u,                           115u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_EtatPrincipSev_oVCU_BSI_Wakeup_oE_CAN_8c281110_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   163 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         12u,                           116u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           1u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_EtatReseauElec_oVCU_BSI_Wakeup_oE_CAN_b805a640_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   164 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         56u,                           117u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          30u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/VCU_FrameChksum17B_oVCU_PCANInfo_oE_CAN_657e6685_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   165 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          6u,                           118u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          27u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_Keyposition_oVCU2_oE_CAN_7b65e581_Rx, /ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  { /*   166 */          TRUE,                           FALSE,  COM_UINT32_APPLTYPEOFRXACCESSINFO,       24u,         48u,                             1u,     COM_NBYTE_SW_BUSACCOFRXACCESSINFO,         3u,           6u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                0u,          32u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_Kilometrage_oVCU_oE_CAN_90aff34b_Rx, /ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  { /*   167 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        3u,         37u,                           119u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_ModeEPSRequest_oVCU_BSI_Wakeup_oE_CAN_760ae031_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   168 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        2u,          3u,                           120u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           0u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                2u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     0u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PosShuntJDD_oVCU_BSI_Wakeup_oE_CAN_0978ac9d_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   169 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         36u,                           121u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           4u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PrecondElecWakeup_oVCU_BSI_Wakeup_oE_CAN_6616770e_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   170 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        4u,         60u,                           122u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           7u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          30u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     7u },  /* [/ActiveEcuC/Com/ComConfig/VCU_RollingCounter17B_oVCU_PCANInfo_oE_CAN_57322517_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   171 */          TRUE,                           FALSE,   COM_UINT8_APPLTYPEOFRXACCESSINFO,        1u,         31u,                           123u,         COM_NBIT_BUSACCOFRXACCESSINFO,         0u,           3u, COM_NO_CONSTVALUEARRAYBASEDINITVALUEENDIDXOFRXACCESSINFO, COM_NO_CONSTVALUEARRAYBASEDINITVALUESTARTIDXOFRXACCESSINFO,                                4u,          29u, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERENDIDXOFRXACCESSINFO, COM_NO_RXSIGBUFFERARRAYBASEDBUFFERSTARTIDXOFRXACCESSINFO,                     3u }   /* [/ActiveEcuC/Com/ComConfig/VCU_StopDelayedHMIWakeup_oVCU_BSI_Wakeup_oE_CAN_05bb1585_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxAccessInfoInd
**********************************************************************************************************************/
/** 
  \var    Com_RxAccessInfoInd
  \brief  the indexes of the 1:1 sorted relation pointing to Com_RxAccessInfo
*/ 
#define COM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_RxAccessInfoIndType, COM_CONST) Com_RxAccessInfoInd[172] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RxAccessInfoInd      Referable Keys */
  /*     0 */               9u,  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     1 */              11u,  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     2 */              12u,  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     3 */              13u,  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     4 */              15u,  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     5 */               2u,  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  /*     6 */               6u,  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  /*     7 */               5u,  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*     8 */               7u,  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*     9 */               8u,  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*    10 */              10u,  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*    11 */              14u,  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*    12 */               3u,  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  /*    13 */               4u,  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  /*    14 */              16u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    15 */              17u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    16 */              18u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    17 */              19u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    18 */              20u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    19 */              21u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    20 */              22u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    21 */              23u,  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  /*    22 */             148u,  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*    23 */             153u,  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*    24 */             154u,  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*    25 */              43u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    26 */              44u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    27 */              45u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    28 */              46u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    29 */              47u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    30 */              48u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    31 */              49u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    32 */              50u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    33 */              51u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    34 */              52u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    35 */              53u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    36 */              54u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    37 */              36u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    38 */              39u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    39 */              40u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    40 */              41u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    41 */              42u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    42 */              55u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    43 */              35u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    44 */              37u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    45 */              38u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    46 */              56u,  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    47 */              65u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    48 */              66u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    49 */              67u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /* Index     RxAccessInfoInd      Referable Keys */
  /*    50 */              68u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    51 */              69u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    52 */              70u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    53 */              71u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    54 */              72u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    55 */              73u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    56 */              75u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    57 */              79u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    58 */              80u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    59 */              81u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    60 */              82u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    61 */              83u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    62 */              58u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    63 */              59u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    64 */              60u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    65 */              63u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    66 */              64u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    67 */              74u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    68 */              76u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    69 */              77u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    70 */              84u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    71 */              85u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    72 */              86u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    73 */              87u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    74 */              88u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    75 */              89u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    76 */              57u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*    77 */              61u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*    78 */              62u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*    79 */              78u,  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*    80 */              90u,  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  /*    81 */              91u,  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  /*    82 */              93u,  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  /*    83 */              34u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx] */
  /*    84 */              95u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    85 */              96u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    86 */              97u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    87 */              98u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    88 */              99u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    89 */             100u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    90 */             101u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    91 */             102u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    92 */             103u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    93 */             104u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    94 */             105u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    95 */             106u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    96 */             107u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    97 */             108u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    98 */             109u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    99 */             110u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /* Index     RxAccessInfoInd      Referable Keys */
  /*   100 */             124u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*   101 */              94u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   102 */             123u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   103 */             125u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   104 */             130u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   105 */             131u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   106 */             132u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   107 */             145u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   108 */             113u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   109 */             117u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   110 */             121u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   111 */             126u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   112 */             137u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   113 */             140u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   114 */             143u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   115 */             114u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   116 */             118u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   117 */             122u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   118 */             127u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   119 */             138u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   120 */             141u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   121 */             144u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   122 */             112u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   123 */             116u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   124 */             120u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   125 */             128u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   126 */             136u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   127 */             139u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   128 */             142u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   129 */             111u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   130 */             115u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   131 */             119u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   132 */             129u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   133 */             133u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   134 */             134u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   135 */             135u,  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   136 */             159u,  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  /*   137 */              26u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   138 */              27u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   139 */              28u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   140 */              29u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   141 */              30u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   142 */              31u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   143 */              32u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   144 */              33u,  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   145 */              92u,  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  /*   146 */             146u,  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  /*   147 */             165u,  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  /*   148 */             160u,  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  /*   149 */             149u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /* Index     RxAccessInfoInd      Referable Keys */
  /*   150 */             150u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   151 */             155u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   152 */             156u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   153 */             157u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   154 */             158u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   155 */             161u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   156 */             162u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   157 */             163u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   158 */             167u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   159 */             168u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   160 */             169u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   161 */             171u,  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   162 */               0u,  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   163 */               1u,  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   164 */              24u,  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   165 */              25u,  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   166 */             164u,  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   167 */             170u,  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   168 */             147u,  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */
  /*   169 */             151u,  /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  /*   170 */             152u,  /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  /*   171 */             166u   /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
};
#define COM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxPduCalloutFuncPtr
**********************************************************************************************************************/
/** 
  \var    Com_RxPduCalloutFuncPtr
  \brief  Rx I-PDU callout function pointer table.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComRxPduCalloutType, COM_CONST) Com_RxPduCalloutFuncPtr[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RxPduCalloutFuncPtr         Referable Keys */
  /*     0 */ Com_ECANRxPduCallout   ,  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx, /ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx, /ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx, /ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx, /ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx, /ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx, /ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx, /ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx, /ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx, /ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx, /ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx, /ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx, /ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx, /ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx, /ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx, /ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx, /ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  /*     1 */ Com_IntCANRxPduCallout    /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx, /ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx, /ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx, /ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxPduGrpInfo
**********************************************************************************************************************/
/** 
  \var    Com_RxPduGrpInfo
  \brief  Contains all I-PDU-Group relevant information for Rx I-PDUs.
  \details
  Element                 Description
  PduGrpVectorStartIdx    the start index of the 0:n relation pointing to Com_PduGrpVector
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_RxPduGrpInfoType, COM_CONST) Com_RxPduGrpInfo[33] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    PduGrpVectorStartIdx */
  { /*     0 */                   3u },
  { /*     1 */                   3u },
  { /*     2 */                   3u },
  { /*     3 */                   3u },
  { /*     4 */                   3u },
  { /*     5 */                   3u },
  { /*     6 */                   3u },
  { /*     7 */                   3u },
  { /*     8 */                   1u },
  { /*     9 */                   1u },
  { /*    10 */                   1u },
  { /*    11 */                   1u },
  { /*    12 */                   1u },
  { /*    13 */                   1u },
  { /*    14 */                   1u },
  { /*    15 */                   3u },
  { /*    16 */                   3u },
  { /*    17 */                   1u },
  { /*    18 */                   1u },
  { /*    19 */                   1u },
  { /*    20 */                   1u },
  { /*    21 */                   1u },
  { /*    22 */                   1u },
  { /*    23 */                   3u },
  { /*    24 */                   1u },
  { /*    25 */                   3u },
  { /*    26 */                   3u },
  { /*    27 */                   3u },
  { /*    28 */                   3u },
  { /*    29 */                   3u },
  { /*    30 */                   3u },
  { /*    31 */                   3u },
  { /*    32 */                   3u }
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxPduInfo
**********************************************************************************************************************/
/** 
  \var    Com_RxPduInfo
  \brief  Contains all relevant common information for Rx I-PDUs.
  \details
  Element                   Description
  RxDefPduBufferUsed        TRUE, if the 0:n relation has 1 relation pointing to Com_RxDefPduBuffer
  RxSigInfoUsed             TRUE, if the 0:n relation has 1 relation pointing to Com_RxSigInfo
  HandleRxPduDeferredIdx    the index of the 0:1 relation pointing to Com_HandleRxPduDeferred
  RxDefPduBufferEndIdx      the end index of the 0:n relation pointing to Com_RxDefPduBuffer
  RxDefPduBufferStartIdx    the start index of the 0:n relation pointing to Com_RxDefPduBuffer
  RxPduCalloutFuncPtrIdx    the index of the 0:1 relation pointing to Com_RxPduCalloutFuncPtr
  RxSigInfoEndIdx           the end index of the 0:n relation pointing to Com_RxSigInfo
  RxSigInfoStartIdx         the start index of the 0:n relation pointing to Com_RxSigInfo
  SignalProcessing          Defines whether rx Pdu is processed in DEFERRED or IMMEDIATE fashion.
  Type                      Defines whether rx Pdu is a NORMAL or TP IPdu.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_RxPduInfoType, COM_CONST) Com_RxPduInfo[33] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxDefPduBufferUsed  RxSigInfoUsed  HandleRxPduDeferredIdx                    RxDefPduBufferEndIdx                    RxDefPduBufferStartIdx                    RxPduCalloutFuncPtrIdx                    RxSigInfoEndIdx  RxSigInfoStartIdx  SignalProcessing                           Type                              Referable Keys */
  { /*     0 */               TRUE,          TRUE,                                       0u,                                     8u,                                       0u,                                       0u,              5u,                0u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     1 */               TRUE,          TRUE,                                       1u,                                    16u,                                       8u,                                       0u,              6u,                5u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     2 */               TRUE,          TRUE,                                       2u,                                    24u,                                      16u,                                       0u,              7u,                6u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     3 */               TRUE,          TRUE,                                       3u,                                    32u,                                      24u,                                       0u,             12u,                7u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     4 */               TRUE,          TRUE,                                       4u,                                    40u,                                      32u,                                       0u,             13u,               12u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     5 */               TRUE,          TRUE,                                       5u,                                    48u,                                      40u,                                       0u,             14u,               13u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     6 */              FALSE,          TRUE, COM_NO_HANDLERXPDUDEFERREDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERENDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERSTARTIDXOFRXPDUINFO,                                       0u,             22u,               14u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     7 */               TRUE,          TRUE,                                       6u,                                    56u,                                      48u,                                       0u,             25u,               22u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     8 */               TRUE,          TRUE,                                       7u,                                    64u,                                      56u,                                       1u,             37u,               25u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     9 */               TRUE,          TRUE,                                       8u,                                    72u,                                      64u,                                       1u,             43u,               37u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    10 */               TRUE,          TRUE,                                       9u,                                    80u,                                      72u,                                       1u,             47u,               43u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    11 */               TRUE,          TRUE,                                      10u,                                    88u,                                      80u,                                       1u,             62u,               47u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    12 */               TRUE,          TRUE,                                      11u,                                    96u,                                      88u,                                       1u,             69u,               62u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    13 */               TRUE,          TRUE,                                      12u,                                   104u,                                      96u,                                       1u,             76u,               69u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    14 */               TRUE,          TRUE,                                      13u,                                   112u,                                     104u,                                       1u,             80u,               76u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    15 */              FALSE,          TRUE, COM_NO_HANDLERXPDUDEFERREDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERENDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERSTARTIDXOFRXPDUINFO,                                       0u,             83u,               80u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    16 */              FALSE,          TRUE, COM_NO_HANDLERXPDUDEFERREDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERENDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERSTARTIDXOFRXPDUINFO,                                       0u,             84u,               83u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    17 */               TRUE,          TRUE,                                      14u,                                   120u,                                     112u,                                       1u,            101u,               84u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    18 */               TRUE,          TRUE,                                      15u,                                   128u,                                     120u,                                       1u,            108u,              101u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    19 */               TRUE,          TRUE,                                      16u,                                   136u,                                     128u,                                       1u,            115u,              108u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    20 */               TRUE,          TRUE,                                      17u,                                   144u,                                     136u,                                       1u,            122u,              115u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    21 */               TRUE,          TRUE,                                      18u,                                   152u,                                     144u,                                       1u,            129u,              122u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    22 */               TRUE,          TRUE,                                      19u,                                   160u,                                     152u,                                       1u,            136u,              129u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    23 */               TRUE,          TRUE,                                      20u,                                   168u,                                     160u,                                       0u,            137u,              136u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    24 */               TRUE,          TRUE,                                      21u,                                   176u,                                     168u,                                       1u,            145u,              137u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*    25 */               TRUE,          TRUE,                                      22u,                                   184u,                                     176u, COM_NO_RXPDUCALLOUTFUNCPTRIDXOFRXPDUINFO,            146u,              145u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    26 */               TRUE,          TRUE,                                      23u,                                   192u,                                     184u, COM_NO_RXPDUCALLOUTFUNCPTRIDXOFRXPDUINFO,            147u,              146u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    27 */               TRUE,          TRUE,                                      24u,                                   200u,                                     192u,                                       0u,            148u,              147u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    28 */               TRUE,          TRUE,                                      25u,                                   208u,                                     200u,                                       0u,            149u,              148u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    29 */               TRUE,          TRUE,                                      26u,                                   216u,                                     208u,                                       0u,            162u,              149u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    30 */               TRUE,          TRUE,                                      27u,                                   224u,                                     216u,                                       0u,            168u,              162u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    31 */               TRUE,          TRUE,                                      28u,                                   232u,                                     224u,                                       0u,            169u,              168u,  COM_DEFERRED_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*    32 */              FALSE,          TRUE, COM_NO_HANDLERXPDUDEFERREDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERENDIDXOFRXPDUINFO, COM_NO_RXDEFPDUBUFFERSTARTIDXOFRXPDUINFO,                                       0u,            172u,              169u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXPDUINFO, COM_NORMAL_TYPEOFRXPDUINFO }   /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxSigInfo
**********************************************************************************************************************/
/** 
  \var    Com_RxSigInfo
  \brief  Contains all relevant information for Rx signals.
  \details
  Element             Description
  RxAccessInfoIdx     the index of the 1:1 relation pointing to Com_RxAccessInfo
  SignalProcessing
  ValidDlc            Minimum length of PDU required to completely receive the signal or signal group.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_RxSigInfoType, COM_CONST) Com_RxSigInfo[172] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxAccessInfoIdx  SignalProcessing                           ValidDlc        Referable Keys */
  { /*     0 */              9u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*     1 */             11u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*     2 */             12u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*     3 */             13u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*     4 */             15u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  { /*     5 */              2u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  { /*     6 */              6u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  { /*     7 */              5u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*     8 */              7u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*     9 */              8u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*    10 */             10u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*    11 */             14u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  { /*    12 */              3u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  { /*    13 */              4u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  { /*    14 */             16u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    15 */             17u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    16 */             18u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    17 */             19u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    18 */             20u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    19 */             21u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    20 */             22u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    21 */             23u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx] */
  { /*    22 */            148u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  { /*    23 */            153u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  { /*    24 */            154u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  { /*    25 */             43u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    26 */             44u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    27 */             45u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    28 */             46u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    29 */             47u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    30 */             48u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    31 */             49u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    32 */             50u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    33 */             51u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    34 */             52u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    35 */             53u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    36 */             54u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  { /*    37 */             36u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    38 */             39u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    39 */             40u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       6u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    40 */             41u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    41 */             42u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    42 */             55u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  { /*    43 */             35u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    44 */             37u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    45 */             38u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    46 */             56u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  { /*    47 */             65u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    48 */             66u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    49 */             67u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
    /* Index    RxAccessInfoIdx  SignalProcessing                           ValidDlc        Referable Keys */
  { /*    50 */             68u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    51 */             69u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    52 */             70u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    53 */             71u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    54 */             72u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    55 */             73u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    56 */             75u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    57 */             79u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    58 */             80u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    59 */             81u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    60 */             82u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    61 */             83u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  { /*    62 */             58u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    63 */             59u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    64 */             60u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    65 */             63u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    66 */             64u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       6u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    67 */             74u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    68 */             76u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  { /*    69 */             77u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    70 */             84u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    71 */             85u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    72 */             86u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    73 */             87u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    74 */             88u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    75 */             89u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       6u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  { /*    76 */             57u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    77 */             61u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    78 */             62u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    79 */             78u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  { /*    80 */             90u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  { /*    81 */             91u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  { /*    82 */             93u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx] */
  { /*    83 */             34u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx] */
  { /*    84 */             95u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    85 */             96u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    86 */             97u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    87 */             98u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    88 */             99u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    89 */            100u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    90 */            101u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    91 */            102u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    92 */            103u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    93 */            104u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    94 */            105u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    95 */            106u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    96 */            107u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    97 */            108u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    98 */            109u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*    99 */            110u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
    /* Index    RxAccessInfoIdx  SignalProcessing                           ValidDlc        Referable Keys */
  { /*   100 */            124u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  { /*   101 */             94u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   102 */            123u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   103 */            125u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   104 */            130u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   105 */            131u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   106 */            132u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       6u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   107 */            145u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  { /*   108 */            113u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   109 */            117u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   110 */            121u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   111 */            126u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   112 */            137u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   113 */            140u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   114 */            143u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  { /*   115 */            114u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   116 */            118u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   117 */            122u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   118 */            127u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   119 */            138u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   120 */            141u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   121 */            144u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  { /*   122 */            112u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   123 */            116u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   124 */            120u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   125 */            128u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   126 */            136u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   127 */            139u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   128 */            142u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  { /*   129 */            111u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   130 */            115u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   131 */            119u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   132 */            129u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   133 */            133u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   134 */            134u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   135 */            135u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  { /*   136 */            159u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  { /*   137 */             26u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   138 */             27u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   139 */             28u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   140 */             29u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   141 */             30u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   142 */             31u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       6u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   143 */             32u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       7u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   144 */             33u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  { /*   145 */             92u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  { /*   146 */            146u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  { /*   147 */            165u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  { /*   148 */            160u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  { /*   149 */            149u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
    /* Index    RxAccessInfoIdx  SignalProcessing                           ValidDlc        Referable Keys */
  { /*   150 */            150u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   151 */            155u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   152 */            156u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   153 */            157u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   154 */            158u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   155 */            161u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   156 */            162u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   157 */            163u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   158 */            167u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   159 */            168u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       1u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   160 */            169u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   161 */            171u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  { /*   162 */              0u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       3u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   163 */              1u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       2u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   164 */             24u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   165 */             25u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       5u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   166 */            164u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   167 */            170u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  { /*   168 */            147u,  COM_DEFERRED_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */
  { /*   169 */            151u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       8u },  /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  { /*   170 */            152u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       4u },  /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
  { /*   171 */            166u, COM_IMMEDIATE_SIGNALPROCESSINGOFRXSIGINFO,       7u }   /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxModeFalse
**********************************************************************************************************************/
/** 
  \var    Com_TxModeFalse
  \brief  Contains all relevant information for transmission mode false.
  \details
  Element       Description
  Periodic      TRUE if transmission mode contains a cyclic part.
  TimeOffset    Initial time offset factor for cyclic transmission.
  TimePeriod    Cycle time factor.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxModeFalseType, COM_CONST) Com_TxModeFalse[12] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Periodic  TimeOffset  TimePeriod        Referable Keys */
  { /*     0 */     TRUE,         3u,         2u },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  { /*     1 */     TRUE,         5u,       200u },  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*     2 */     TRUE,         4u,       200u },  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  { /*     3 */     TRUE,         7u,        10u },  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*     4 */     TRUE,         3u,        20u },  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*     5 */     TRUE,         1u,        20u },  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*     6 */    FALSE,         1u,         0u },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*     7 */     TRUE,         8u,        20u },  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*     8 */     TRUE,         6u,        20u },  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*     9 */     TRUE,         4u,        20u },  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    10 */     TRUE,         2u,         2u },  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*    11 */     TRUE,         5u,        20u }   /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxModeInfo
**********************************************************************************************************************/
/** 
  \var    Com_TxModeInfo
  \brief  Contains all relevant information for transmission mode handling.
  \details
  Element          Description
  InitMode         Initial transmission mode selector of the Tx I-PDU.
  MinimumDelay     Minimum delay factor of the Tx I-PDU.
  TxModeTrueIdx    the index of the 1:1 relation pointing to Com_TxModeTrue
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxModeInfoType, COM_CONST) Com_TxModeInfo[17] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    InitMode  MinimumDelay  TxModeTrueIdx        Referable Keys */
  { /*     0 */     TRUE,           3u,           11u },  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     1 */     TRUE,           2u,           10u },  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*     2 */     TRUE,           0u,           10u },  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*     3 */     TRUE,           0u,            9u },  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*     4 */     TRUE,           0u,            8u },  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*     5 */     TRUE,           0u,            7u },  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*     6 */     TRUE,           3u,            6u },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*     7 */     TRUE,           3u,            5u },  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*     8 */     TRUE,           3u,            4u },  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*     9 */     TRUE,           3u,            3u },  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*    10 */     TRUE,           3u,            2u },  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  { /*    11 */     TRUE,           3u,            6u },  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  { /*    12 */     TRUE,           3u,            1u },  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    13 */     TRUE,           2u,           10u },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*    14 */     TRUE,           2u,            0u },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  { /*    15 */     TRUE,           2u,            0u },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  { /*    16 */     TRUE,           3u,            6u }   /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxModeTrue
**********************************************************************************************************************/
/** 
  \var    Com_TxModeTrue
  \brief  Contains all relevant information for transmission mode true.
  \details
  Element       Description
  Periodic      TRUE if transmission mode contains a cyclic part.
  TimeOffset    Initial time offset factor for cyclic transmission.
  TimePeriod    Cycle time factor.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxModeTrueType, COM_CONST) Com_TxModeTrue[12] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Periodic  TimeOffset  TimePeriod        Referable Keys */
  { /*     0 */     TRUE,         3u,         2u },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  { /*     1 */     TRUE,         5u,       200u },  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*     2 */     TRUE,         4u,       200u },  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  { /*     3 */     TRUE,         7u,        10u },  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*     4 */     TRUE,         3u,        20u },  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*     5 */     TRUE,         1u,        20u },  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*     6 */    FALSE,         1u,         0u },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*     7 */     TRUE,         8u,        20u },  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*     8 */     TRUE,         6u,        20u },  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*     9 */     TRUE,         4u,        20u },  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    10 */     TRUE,         2u,         2u },  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*    11 */     TRUE,         5u,        20u }   /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxPduCalloutFuncPtr
**********************************************************************************************************************/
/** 
  \var    Com_TxPduCalloutFuncPtr
  \brief  Tx I-PDU callout function pointer table.
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComTxPduCalloutType, COM_CONST) Com_TxPduCalloutFuncPtr[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     TxPduCalloutFuncPtr         Referable Keys */
  /*     0 */ Com_ECANTxPduCallout   ,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*     1 */ Com_IntCANTxPduCallout    /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxPduGrpInfo
**********************************************************************************************************************/
/** 
  \var    Com_TxPduGrpInfo
  \brief  Contains all I-PDU-Group relevant information for Tx I-PDUs.
  \details
  Element                 Description
  PduGrpVectorStartIdx    the start index of the 0:n relation pointing to Com_PduGrpVector
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxPduGrpInfoType, COM_CONST) Com_TxPduGrpInfo[17] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    PduGrpVectorStartIdx */
  { /*     0 */                   2u },
  { /*     1 */                   2u },
  { /*     2 */                   0u },
  { /*     3 */                   0u },
  { /*     4 */                   0u },
  { /*     5 */                   0u },
  { /*     6 */                   2u },
  { /*     7 */                   2u },
  { /*     8 */                   2u },
  { /*     9 */                   2u },
  { /*    10 */                   2u },
  { /*    11 */                   2u },
  { /*    12 */                   2u },
  { /*    13 */                   0u },
  { /*    14 */                   0u },
  { /*    15 */                   0u },
  { /*    16 */                   2u }
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxPduInfo
**********************************************************************************************************************/
/** 
  \var    Com_TxPduInfo
  \brief  Contains all relevant information for Tx I-PDUs.
  \details
  Element                          Description
  TxPduInitValueUsed               TRUE, if the 0:n relation has 1 relation pointing to Com_TxPduInitValue
  TxSigGrpInfoIndUsed              TRUE, if the 0:n relation has 1 relation pointing to Com_TxSigGrpInfoInd
  CbkTxAckDefFuncPtrIndEndIdx      the end index of the 0:n relation pointing to Com_CbkTxAckDefFuncPtrInd
  CbkTxAckDefFuncPtrIndStartIdx    the start index of the 0:n relation pointing to Com_CbkTxAckDefFuncPtrInd
  ExternalId                       External ID used to call PduR_ComTransmit().
  TxBufferLength                   the number of relations pointing to Com_TxBuffer
  TxPduCalloutFuncPtrIdx           the index of the 0:1 relation pointing to Com_TxPduCalloutFuncPtr
  TxPduInitValueEndIdx             the end index of the 0:n relation pointing to Com_TxPduInitValue
  TxPduInitValueStartIdx           the start index of the 0:n relation pointing to Com_TxPduInitValue
  TxSigGrpInfoIndEndIdx            the end index of the 0:n relation pointing to Com_TxSigGrpInfoInd
  TxSigGrpInfoIndStartIdx          the start index of the 0:n relation pointing to Com_TxSigGrpInfoInd
  TxTOutInfoIdx                    the index of the 0:1 relation pointing to Com_TxTOutInfo
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxPduInfoType, COM_CONST) Com_TxPduInfo[17] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxPduInitValueUsed  TxSigGrpInfoIndUsed  CbkTxAckDefFuncPtrIndEndIdx                    CbkTxAckDefFuncPtrIndStartIdx                    ExternalId                               TxBufferLength  TxPduCalloutFuncPtrIdx                    TxPduInitValueEndIdx  TxPduInitValueStartIdx  TxSigGrpInfoIndEndIdx                    TxSigGrpInfoIndStartIdx                    TxTOutInfoIdx                          Referable Keys */
  { /*     0 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_1224da83,             8u,                                       0u,                   8u,                     0u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     1 */               TRUE,                TRUE,                                            1u,                                              0u, PduRConf_PduRSrcPdu_PduRSrcPdu_d598d177,             8u,                                       0u,                  16u,                     8u,                                      1u,                                        0u,                              0u },  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     2 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_c451a6c2,             8u, COM_NO_TXPDUCALLOUTFUNCPTRIDXOFTXPDUINFO,                  24u,                    16u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     3 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_cf0367a1,             8u, COM_NO_TXPDUCALLOUTFUNCPTRIDXOFTXPDUINFO,                  32u,                    24u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     4 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_e2c32671,             8u, COM_NO_TXPDUCALLOUTFUNCPTRIDXOFTXPDUINFO,                  40u,                    32u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     5 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_af12cdd2,             8u, COM_NO_TXPDUCALLOUTFUNCPTRIDXOFTXPDUINFO,                  48u,                    40u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     6 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_2e76203d,             8u,                                       0u,                  56u,                    48u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     7 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_5705bb4d,             8u,                                       0u,                  64u,                    56u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     8 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_558557e8,             8u,                                       0u,                  72u,                    64u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     9 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_0f56ae96,             8u,                                       0u,                  80u,                    72u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*    10 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_31565810,             8u,                                       0u,                  88u,                    80u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*    11 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_aa4ba89d,             8u, COM_NO_TXPDUCALLOUTFUNCPTRIDXOFTXPDUINFO,                  96u,                    88u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*    12 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_a0eb6b35,             8u,                                       0u,                 104u,                    96u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*    13 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_17d4e376,             8u,                                       1u,                 112u,                   104u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*    14 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_36c239fa,             8u,                                       1u,                 120u,                   112u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*    15 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_f7c07c35,             8u,                                       1u,                 128u,                   120u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*    16 */               TRUE,               FALSE, COM_NO_CBKTXACKDEFFUNCPTRINDENDIDXOFTXPDUINFO, COM_NO_CBKTXACKDEFFUNCPTRINDSTARTIDXOFTXPDUINFO, PduRConf_PduRSrcPdu_PduRSrcPdu_8aa49178,             8u,                                       0u,                 136u,                   128u, COM_NO_TXSIGGRPINFOINDENDIDXOFTXPDUINFO, COM_NO_TXSIGGRPINFOINDSTARTIDXOFTXPDUINFO, COM_NO_TXTOUTINFOIDXOFTXPDUINFO }   /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxPduInitValue
**********************************************************************************************************************/
/** 
  \var    Com_TxPduInitValue
  \brief  Initial values used for Tx I-PDU buffer initialization.
*/ 
#define COM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxPduInitValueType, COM_CONST) Com_TxPduInitValue[136] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     TxPduInitValue      Referable Keys */
  /*     0 */           0x28u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     1 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     2 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     3 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     4 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     5 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     6 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     7 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     8 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*     9 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    10 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    11 */           0x0Au,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    12 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    13 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    14 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    15 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    16 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    17 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    18 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    19 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    20 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    21 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    22 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    23 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  /*    24 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    25 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    26 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    27 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    28 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    29 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    30 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    31 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  /*    32 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    33 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    34 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    35 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    36 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    37 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    38 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    39 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  /*    40 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    41 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    42 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    43 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    44 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    45 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    46 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    47 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  /*    48 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    49 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /* Index     TxPduInitValue      Referable Keys */
  /*    50 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    51 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    52 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    53 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    54 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    55 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  /*    56 */           0x32u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    57 */           0x32u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    58 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    59 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    60 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    61 */           0x0Au,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    62 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    63 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    64 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    65 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    66 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    67 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    68 */           0x64u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    69 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    70 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    71 */           0x02u,  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  /*    72 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    73 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    74 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    75 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    76 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    77 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    78 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    79 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    80 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    81 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    82 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    83 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    84 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    85 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    86 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    87 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    88 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    89 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    90 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    91 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    92 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    93 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    94 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    95 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  /*    96 */           0x28u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*    97 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*    98 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*    99 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /* Index     TxPduInitValue      Referable Keys */
  /*   100 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   101 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   102 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   103 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   104 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   105 */           0x34u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   106 */           0x08u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   107 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   108 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   109 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   110 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   111 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   112 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   113 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   114 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   115 */           0x04u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   116 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   117 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   118 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   119 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   120 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   121 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   122 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   123 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   124 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   125 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   126 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   127 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   128 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   129 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   130 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   131 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   132 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   133 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   134 */           0x00u,  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  /*   135 */           0x00u   /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
};
#define COM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxSigGrpInfo
**********************************************************************************************************************/
/** 
  \var    Com_TxSigGrpInfo
  \brief  Contains all relevant information for Tx Signal Groups.
  \details
  Element             Description
  TxSigGrpMaskUsed    TRUE, if the 0:n relation has 1 relation pointing to Com_TxSigGrpMask
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxSigGrpInfoType, COM_CONST) Com_TxSigGrpInfo[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxSigGrpMaskUsed        Referable Keys */
  { /*     0 */             TRUE }   /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_ActivedischargeSt_oDC2_oE_CAN_5a2849b9_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_InputCurrent_oDC2_oE_CAN_0b6c1afa_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCMainContactorReq_oDC2_oE_CAN_40cd3485_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCQuickChargeContactorReq_oDC2_oE_CAN_2093b4ef_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OutputCurrent_oDC2_oE_CAN_6652af17_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCDCDCRTPowerLoad_oDC2_oE_CAN_e771c64f_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxSigGrpMask
**********************************************************************************************************************/
/** 
  \var    Com_TxSigGrpMask
  \brief  Signal group mask needed to copy interlaced signal groups to the Tx PDU buffer.
*/ 
#define COM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxSigGrpMaskType, COM_CONST) Com_TxSigGrpMask[7] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     TxSigGrpMask      Referable Keys */
  /*     0 */         0xFEu,  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*     1 */         0x00u,  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*     2 */         0x00u,  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*     3 */         0x3Fu,  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*     4 */         0xFFu,  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*     5 */         0xFFu,  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*     6 */         0xF0u   /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
};
#define COM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxSigInfo
**********************************************************************************************************************/
/** 
  \var    Com_TxSigInfo
  \brief  Contains all relevant information for Tx signals and group signals.
  \details
  Element                   Description
  Triggered                 TRUE if signal or group signal has any 'TRIGGERED_*' transfer property.
  ApplType                  Application data type.
  BitLength                 Bit length of the signal or group signal.
  BitPosition               Little endian bit position of the signal or group signal within the I-PDU.
  BusAcc                    BUS access algorithm for signal or group signal packing / un-packing.
  ByteLength                Byte length of the signal or group signal.
  BytePosition              Little endian byte position of the signal or group signal within the I-PDU.
  StartByteInPduPosition    Start Byte position of the signal or group signal within the I-PDU.
  TxBufferEndIdx            the end index of the 0:n relation pointing to Com_TxBuffer
  TxBufferStartIdx          the start index of the 0:n relation pointing to Com_TxBuffer
  TxPduInfoIdx              the index of the 1:1 relation pointing to Com_TxPduInfo
  TxSigGrpInfoIdx           the index of the 0:1 relation pointing to Com_TxSigGrpInfo
*/ 
#define COM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Com_TxSigInfoType, COM_CONST) Com_TxSigInfo[117] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Triggered  ApplType                         BitLength  BitPosition  BusAcc                              ByteLength  BytePosition  StartByteInPduPosition  TxBufferEndIdx  TxBufferStartIdx  TxPduInfoIdx  TxSigGrpInfoIdx                          Referable Keys */
  { /*     0 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,       11u,         24u,    COM_NBITNBYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,           109u,             107u,          13u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_CurrentReference_oSUP_CommandToDCHV_oInt_CAN_e1358897_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*     1 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         24u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,             4u,               3u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_FaultLampRequest_oDC1_oE_CAN_66ccec8a_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     2 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,             5u,               4u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_Fault_oDC1_oE_CAN_80e733c8_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     3 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         26u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,             4u,               3u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_HighVoltConnectionAllowed_oDC1_oE_CAN_7634e78c_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     4 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,        9u,         63u, COM_NBITNBYTE_SW_BUSACCOFTXSIGINFO,         1u,           7u,                     6u,             8u,               6u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_InputVoltage_oDC1_oE_CAN_dc52df4d_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     5 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         27u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,             4u,               3u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_OVERTEMP_oDC1_oE_CAN_9db3c9a4_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     6 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,             2u,               1u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_OutputVoltage_oDC1_oE_CAN_b16c6aa0_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     7 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        3u,         60u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,             8u,               7u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_Status_oDC1_oE_CAN_0062049f_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     8 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,             1u,               0u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_Temperature_oDC1_oE_CAN_ebbc428a_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*     9 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,       16u,          8u,        COM_NBYTE_BUSACCOFTXSIGINFO,         2u,           1u,                     1u,           107u,             105u,          13u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCDC_VoltageReference_oSUP_CommandToDCHV_oInt_CAN_154ec40d_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*    10 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,       11u,         16u,    COM_NBITNBYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,           116u,             114u,          14u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCLV_Temp_Derating_Factor_oSUP_CommandToDCLV_oInt_CAN_18f05833_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  { /*    11 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        7u,          8u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           1u,                     1u,           114u,             113u,          14u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/DCLV_VoltageReference_oSUP_CommandToDCLV_oInt_CAN_927eb38f_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  { /*    12 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,            18u,              17u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_1_oDebug1_oInt_CAN_c4f96638_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    13 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         16u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,            19u,              18u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_2_oDebug1_oInt_CAN_2e7fbb5a_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    14 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         24u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,            20u,              19u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_3_oDebug1_oInt_CAN_c12d0dbb_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    15 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            21u,              20u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_4_oDebug1_oInt_CAN_200307df_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    16 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,            22u,              21u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_5_oDebug1_oInt_CAN_cf51b13e_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    17 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         48u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           6u,                     6u,            23u,              22u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_6_oDebug1_oInt_CAN_25d76c5c_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    18 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         56u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           7u,                     7u,            24u,              23u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug1_7_oDebug1_oInt_CAN_ca85dabd_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    19 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,            25u,              24u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_0_oDebug2_oInt_CAN_6f352610_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    20 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,            26u,              25u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_1_oDebug2_oInt_CAN_806790f1_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    21 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         16u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,            27u,              26u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_2_oDebug2_oInt_CAN_6ae14d93_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    22 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         24u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,            28u,              27u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_3_oDebug2_oInt_CAN_85b3fb72_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    23 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            29u,              28u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_4_oDebug2_oInt_CAN_649df116_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    24 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,            30u,              29u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_5_oDebug2_oInt_CAN_8bcf47f7_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    25 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         48u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           6u,                     6u,            31u,              30u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_6_oDebug2_oInt_CAN_61499a95_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    26 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         56u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           7u,                     7u,            32u,              31u,           3u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug2_7_oDebug2_oInt_CAN_8e1b2c74_Tx, /ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx] */
  { /*    27 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,            33u,              32u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_0_oDebug3_oInt_CAN_e5907668_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    28 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,            34u,              33u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_1_oDebug3_oInt_CAN_0ac2c089_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    29 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         16u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,            35u,              34u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_2_oDebug3_oInt_CAN_e0441deb_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    30 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         24u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,            36u,              35u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_3_oDebug3_oInt_CAN_0f16ab0a_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    31 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            37u,              36u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_4_oDebug3_oInt_CAN_ee38a16e_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    32 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,            38u,              37u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_5_oDebug3_oInt_CAN_016a178f_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    33 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         48u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           6u,                     6u,            39u,              38u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_6_oDebug3_oInt_CAN_ebeccaed_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    34 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         56u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           7u,                     7u,            40u,              39u,           4u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug3_7_oDebug3_oInt_CAN_04be7c0c_Tx, /ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx] */
  { /*    35 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,            41u,              40u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_0_oDebug4_oInt_CAN_e608cb82_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    36 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,            42u,              41u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_1_oDebug4_oInt_CAN_095a7d63_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    37 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         16u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,            43u,              42u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_2_oDebug4_oInt_CAN_e3dca001_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    38 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         24u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,            44u,              43u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_3_oDebug4_oInt_CAN_0c8e16e0_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    39 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            45u,              44u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_4_oDebug4_oInt_CAN_eda01c84_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    40 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,            46u,              45u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_5_oDebug4_oInt_CAN_02f2aa65_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    41 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         48u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           6u,                     6u,            47u,              46u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_6_oDebug4_oInt_CAN_e8747707_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    42 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         56u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           7u,                     7u,            48u,              47u,           5u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Debug4_7_oDebug4_oInt_CAN_0726c1e6_Tx, /ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx] */
  { /*    43 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,            17u,              16u,           2u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/Degug1_0_oDebug1_oInt_CAN_b0849b9e_Tx, /ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx] */
  { /*    44 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         56u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           7u,                     7u,           136u,             135u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/EDITION_CALIB_oVERS_OBC_DCDC_oE_CAN_20fa071e_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*    45 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         48u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           6u,                     6u,           135u,             134u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/EDITION_SOFT_oVERS_OBC_DCDC_oE_CAN_bde5b057_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*    46 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         23u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           2u,                     2u,            59u,              58u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/EVSE_RTAB_STOP_CHARGE_oOBC1_oE_CAN_01a50620_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    47 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,            49u,              48u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_0_oNEW_JDD_OBC_DCDC_oE_CAN_bfd944fb_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    48 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,            50u,              49u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_1_oNEW_JDD_OBC_DCDC_oE_CAN_58c4e26c_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    49 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         16u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,            51u,              50u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_2_oNEW_JDD_OBC_DCDC_oE_CAN_aa930f94_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
    /* Index    Triggered  ApplType                         BitLength  BitPosition  BusAcc                              ByteLength  BytePosition  StartByteInPduPosition  TxBufferEndIdx  TxBufferStartIdx  TxPduInfoIdx  TxSigGrpInfoIdx                          Referable Keys */
  { /*    50 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         24u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,            52u,              51u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_3_oNEW_JDD_OBC_DCDC_oE_CAN_4d8ea903_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    51 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            53u,              52u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_4_oNEW_JDD_OBC_DCDC_oE_CAN_954dd225_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    52 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,            54u,              53u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_5_oNEW_JDD_OBC_DCDC_oE_CAN_725074b2_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    53 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         48u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           6u,                     6u,            55u,              54u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_6_oNEW_JDD_OBC_DCDC_oE_CAN_8007994a_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    54 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         56u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           7u,                     7u,            56u,              55u,           6u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_7_oNEW_JDD_OBC_DCDC_oE_CAN_671a3fdd_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx] */
  { /*    55 */      TRUE, COM_UINT8_N_APPLTYPEOFTXSIGINFO,       64u,          0u,  COM_ARRAY_BASED_BUSACCOFTXSIGINFO,         8u,           0u,                     0u,            96u,              88u,          11u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiagData_oOBCTxDiag_oE_CAN_e994d172_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx] */
  { /*    56 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        3u,         24u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,            68u,              67u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_ACRange_oOBC2_oE_CAN_1149ad47_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    57 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         43u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           5u,                     5u,            62u,              61u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_CP_connection_Status_oOBC1_oE_CAN_bef9ca9d_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    58 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         57u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,            72u,              71u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_ChargingConnectionConfirmati_oOBC2_oE_CAN_e178031d_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    59 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         41u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           5u,                     5u,            62u,              61u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_ChargingMode_oOBC1_oE_CAN_ce17c9da_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    60 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         27u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,            68u,              67u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_CommunicationSt_oOBC2_oE_CAN_e0b8e211_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    61 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          3u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            73u,              72u,           9u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_CoolingWakeup_oOBC3_oE_CAN_91759696_Tx, /ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*    62 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         10u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           1u,                     1u,            66u,              65u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_DCChargingPlugAConnConf_oOBC2_oE_CAN_ef0cc399_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    63 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         16u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           2u,                     2u,            59u,              58u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_ElockState_oOBC1_oE_CAN_d48080ac_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    64 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            61u,              60u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_Fault_oOBC1_oE_CAN_cba9590d_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    65 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         48u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           6u,                     6u,            71u,              70u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_FrameChksum3A2_oOBC2_oE_CAN_0180e848_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    66 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         56u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,            64u,              63u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_FrameChksum3A3_oOBC1_oE_CAN_12000edd_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    67 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         28u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,             4u,               3u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_FrameChksum345_oDC1_oE_CAN_abd913d1_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*    68 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          0u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            73u,              72u,           9u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_HVBattRechargeWakeup_oOBC3_oE_CAN_2290bde4_Tx, /ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*    69 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         22u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           2u,                     2u,            59u,              58u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_HighVoltConnectionAllowed_oOBC1_oE_CAN_b82322d0_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    70 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          2u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            73u,              72u,           9u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_HoldDiscontactorWakeup_oOBC3_oE_CAN_0a4fcf18_Tx, /ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*    71 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         28u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,            68u,              67u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_InputVoltageSt_oOBC2_oE_CAN_f750717a_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    72 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         56u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,            72u,              71u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_OBCStartSt_oOBC2_oE_CAN_5b2bb089_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    73 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,            69u,              68u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_OBCTemp_oOBC2_oE_CAN_0c3bcf20_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    74 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,        9u,         31u, COM_NBITNBYTE_SW_BUSACCOFTXSIGINFO,         1u,           3u,                     2u,            68u,              66u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_OutputCurrent_oOBC2_oE_CAN_c3e935dd_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    75 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,       13u,         11u, COM_NBITNBYTE_SW_BUSACCOFTXSIGINFO,         1u,           1u,                     0u,            66u,              64u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_OutputVoltage_oOBC2_oE_CAN_afb611f2_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    76 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          1u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            73u,              72u,           9u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_PIStateInfoWakeup_oOBC3_oE_CAN_b3c75a3a_Tx, /ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  { /*    77 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         30u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,            68u,              67u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_PlugVoltDetection_oOBC2_oE_CAN_3eae826f_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    78 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,            70u,              69u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_PowerMax_oOBC2_oE_CAN_cb72170d_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    79 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          4u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            81u,              80u,          10u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_PushChargeType_oOBC4_oE_CAN_f9102f70_Tx, /ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  { /*    80 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        3u,          5u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            81u,              80u,          10u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_RechargeHMIState_oOBC4_oE_CAN_2e6d800d_Tx, /ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  { /*    81 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         52u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           6u,                     6u,            71u,              70u,           8u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_RollingCounter3A2_oOBC2_oE_CAN_ba360476_Tx, /ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx] */
  { /*    82 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         60u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,            64u,              63u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_RollingCounter3A3_oOBC1_oE_CAN_a9b6e2e3_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    83 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         56u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,             8u,               7u,           0u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_RollingCounter345_oDC1_oE_CAN_16722e64_Tx, /ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  { /*    84 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,            57u,              56u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_SocketTempL_oOBC1_oE_CAN_72ea0018_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    85 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,            58u,              57u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_SocketTempN_oOBC1_oE_CAN_9384c4b5_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    86 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        3u,         45u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           5u,                     5u,            62u,              61u,           7u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/OBC_Status_oOBC1_oE_CAN_ef77955c_Tx, /ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  { /*    87 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         28u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,           140u,             139u,           1u,                                0u },  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_ActivedischargeSt_oDC2_oE_CAN_5a2849b9_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*    88 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,           141u,             140u,           1u,                                0u },  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_InputCurrent_oDC2_oE_CAN_0b6c1afa_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*    89 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        7u,          1u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,           137u,             136u,           1u,                                0u },  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCDCDCRTPowerLoad_oDC2_oE_CAN_e771c64f_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*    90 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         26u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,           140u,             139u,           1u,                                0u },  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCMainContactorReq_oDC2_oE_CAN_40cd3485_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*    91 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        2u,         24u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           3u,                     3u,           140u,             139u,           1u,                                0u },  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCQuickChargeContactorReq_oDC2_oE_CAN_2093b4ef_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*    92 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,       12u,         52u, COM_NBITNBYTE_SW_BUSACCOFTXSIGINFO,         1u,           6u,                     5u,           143u,             141u,           1u,                                0u },  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OutputCurrent_oDC2_oE_CAN_6652af17_Tx, /ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  { /*    93 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         61u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           104u,             103u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_CoolingWupState_oSUPV_V2_OBC_DCDC_oE_CAN_addf5c22_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    94 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          7u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            97u,              96u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_DTCRegistred_oSUPV_V2_OBC_DCDC_oE_CAN_04412eaa_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    95 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        3u,          4u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            97u,              96u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_ECUElecStateRCD_oSUPV_V2_OBC_DCDC_oE_CAN_ff7122fa_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    96 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         49u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           6u,                     6u,           103u,             102u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_HVBattChargeWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1071a795_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    97 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         58u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           104u,             103u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_HoldDiscontactorWupState_oSUPV_V2_OBC_DCDC_oE_CAN_b0368fb5_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    98 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         48u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           6u,                     6u,           103u,             102u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_PIStateInfoWupState_oSUPV_V2_OBC_DCDC_oE_CAN_a649d590_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*    99 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         59u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           104u,             103u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_PostDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_5f77ad32_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
    /* Index    Triggered  ApplType                         BitLength  BitPosition  BusAcc                              ByteLength  BytePosition  StartByteInPduPosition  TxBufferEndIdx  TxBufferStartIdx  TxPduInfoIdx  TxSigGrpInfoIdx                          Referable Keys */
  { /*   100 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         60u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           104u,             103u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_PreDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_bfe17473_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*   101 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         57u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           104u,             103u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_PrecondElecWupState_oSUPV_V2_OBC_DCDC_oE_CAN_4523c42d_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*   102 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,          3u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,            97u,              96u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_RCDLineState_oSUPV_V2_OBC_DCDC_oE_CAN_ab22fee8_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*   103 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        1u,         50u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           6u,                     6u,           103u,             102u,          12u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUPV_StopDelayedHMIWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1dcc6f45_Tx, /ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  { /*   104 */     FALSE,  COM_UINT16_APPLTYPEOFTXSIGINFO,       10u,          4u,    COM_NBITNBYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,           122u,             120u,          15u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandVDCLink_V_oSUP_CommandToPFC_oInt_CAN_b6bd9cf9_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  { /*   105 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,          0u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,           113u,             112u,          14u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_RequestDCLVStatus_oSUP_CommandToDCLV_oInt_CAN_08473e1f_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  { /*   106 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,          0u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,           121u,             120u,          15u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_RequestPFCStatus_oSUP_CommandToPFC_oInt_CAN_98b04349_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  { /*   107 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,          0u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           0u,                     0u,           105u,             104u,          13u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_RequestStatusDCHV_oSUP_CommandToDCHV_oInt_CAN_7bfbedf8_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*   108 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         60u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           112u,             111u,          13u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_RollingCounter_4A3_AC_oSUP_CommandToDCHV_oInt_CAN_cc39974d_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  { /*   109 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         60u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           128u,             127u,          15u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_RollingCounter_402_AC_oSUP_CommandToPFC_oInt_CAN_64ec921b_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  { /*   110 */     FALSE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        4u,         60u,         COM_NBIT_BUSACCOFTXSIGINFO,         0u,           7u,                     7u,           120u,             119u,          14u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/SUP_RollingCounter_495_AC_oSUP_CommandToDCLV_oInt_CAN_39a872f4_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  { /*   111 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         32u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           4u,                     4u,           133u,             132u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/VERSION_APPLI_oVERS_OBC_DCDC_oE_CAN_1774b9bd_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*   112 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         40u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           5u,                     5u,           134u,             133u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/VERSION_SOFT_oVERS_OBC_DCDC_oE_CAN_dcc20268_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*   113 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          0u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           0u,                     0u,           129u,             128u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/VERSION_SYSTEME_oVERS_OBC_DCDC_oE_CAN_7264d771_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*   114 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         24u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           3u,                     3u,           132u,             131u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/VERS_DATE2_ANNEE_oVERS_OBC_DCDC_oE_CAN_258c1038_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*   115 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,          8u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           1u,                     1u,           130u,             129u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO },  /* [/ActiveEcuC/Com/ComConfig/VERS_DATE2_JOUR_oVERS_OBC_DCDC_oE_CAN_526ac526_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
  { /*   116 */      TRUE,   COM_UINT8_APPLTYPEOFTXSIGINFO,        8u,         16u,         COM_BYTE_BUSACCOFTXSIGINFO,         1u,           2u,                     2u,           131u,             130u,          16u, COM_NO_TXSIGGRPINFOIDXOFTXSIGINFO }   /* [/ActiveEcuC/Com/ComConfig/VERS_DATE2_MOIS_oVERS_OBC_DCDC_oE_CAN_d5ba8908_Tx, /ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx] */
};
#define COM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_CurrentTxMode
**********************************************************************************************************************/
/** 
  \var    Com_CurrentTxMode
  \brief  Current transmission mode state of all Tx I-PDUs.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_CurrentTxModeType, COM_VAR_NOINIT) Com_CurrentTxMode[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_CycleTimeCnt
**********************************************************************************************************************/
/** 
  \var    Com_CycleTimeCnt
  \brief  Current counter value of cyclic transmission.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_CycleTimeCntType, COM_VAR_NOINIT) Com_CycleTimeCnt[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_CyclicSendRequest
**********************************************************************************************************************/
/** 
  \var    Com_CyclicSendRequest
  \brief  Cyclic send request flag used to indicate cyclic transmission mode for all Tx I-PDU.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_CyclicSendRequestType, COM_VAR_NOINIT) Com_CyclicSendRequest[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_DelayTimeCnt
**********************************************************************************************************************/
/** 
  \var    Com_DelayTimeCnt
  \brief  Current counter value of minimum delay counter.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_DelayTimeCntType, COM_VAR_NOINIT) Com_DelayTimeCnt[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_HandleRxPduDeferred
**********************************************************************************************************************/
/** 
  \var    Com_HandleRxPduDeferred
  \brief  Array indicating received Rx I-PDUs to be processed deferred within the next call of Com_MainfunctionRx().
*/ 
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_HandleRxPduDeferredUType, COM_VAR_NOINIT) Com_HandleRxPduDeferred;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*    17 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*    18 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*    19 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*    20 */  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  /*    21 */  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*    22 */  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  /*    23 */  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  /*    24 */  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  /*    25 */  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  /*    26 */  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*    27 */  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*    28 */  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */

#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_HandleTxPduDeferred
**********************************************************************************************************************/
/** 
  \var    Com_HandleTxPduDeferred
  \brief  Flag array used for deferred Tx confirmation handling.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_HandleTxPduDeferredType, COM_VAR_NOINIT) Com_HandleTxPduDeferred[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_Initialized
**********************************************************************************************************************/
/** 
  \var    Com_Initialized
  \brief  Initialization state of Com. TRUE, if Com_Init() has been called, else FALSE.
*/ 
#define COM_START_SEC_VAR_ZERO_INIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_InitializedType, COM_VAR_ZERO_INIT) Com_Initialized = FALSE;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_ZERO_INIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxDeadlineMonitoringISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxDeadlineMonitoringISRLockCounterType, COM_VAR_NOINIT) Com_RxDeadlineMonitoringISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxDefPduBuffer
**********************************************************************************************************************/
/** 
  \var    Com_RxDefPduBuffer
  \brief  Rx I-PDU buffer for deferred signal processing.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxDefPduBufferUType, COM_VAR_NOINIT) Com_RxDefPduBuffer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  /*    23 */  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx] */
  /*    24 */  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*    31 */  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx] */
  /*    32 */  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  /*    39 */  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx] */
  /*    40 */  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  /*    47 */  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx] */
  /*    48 */  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*    55 */  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx] */
  /*    56 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    63 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx] */
  /*    64 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    71 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx] */
  /*    72 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    79 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx] */
  /*    80 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    87 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx] */
  /*    88 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    95 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx] */
  /*    96 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*   103 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx] */
  /*   104 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*   111 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx] */
  /*   112 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*   119 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx] */
  /*   120 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   127 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx] */
  /*   128 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /*   135 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx] */
  /* Index        Referable Keys */
  /*   136 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   143 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx] */
  /*   144 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   151 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx] */
  /*   152 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   159 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx] */
  /*   160 */  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  /*   167 */  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx] */
  /*   168 */  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   175 */  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx] */
  /*   176 */  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  /*   183 */  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx] */
  /*   184 */  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  /*   191 */  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx] */
  /*   192 */  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  /*   199 */  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx] */
  /*   200 */  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  /*   207 */  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx] */
  /*   208 */  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   215 */  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx] */
  /*   216 */  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   223 */  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx] */
  /*   224 */  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */
  /*   231 */  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxDeferredProcessingISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxDeferredProcessingISRLockCounterType, COM_VAR_NOINIT) Com_RxDeferredProcessingISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxDlMonDivisorCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxDlMonDivisorCounterType, COM_VAR_NOINIT) Com_RxDlMonDivisorCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxIPduGroupISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxIPduGroupISRLockCounterType, COM_VAR_NOINIT) Com_RxIPduGroupISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxPduGrpActive
**********************************************************************************************************************/
/** 
  \var    Com_RxPduGrpActive
  \brief  Rx I-PDU based state (started/stopped) of the corresponding I-PDU-Group.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxPduGrpActiveType, COM_VAR_NOINIT) Com_RxPduGrpActive[33];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/BMS1_oE_CAN_1fa2c419_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/BMS3_oE_CAN_5d87c364_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/BMS5_oE_CAN_9be8cae3_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/BMS6_oE_CAN_1567cd00_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/BMS8_oE_CAN_00eddf32_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/BMS9_oE_CAN_cc47dfac_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/BSIInfo_oE_CAN_b362dec3_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/CtrlDCDC_oE_CAN_6ce88e20_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Fault_oInt_CAN_d7c47a53_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status1_oInt_CAN_6c1b5ec3_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Status2_oInt_CAN_872ce5c0_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Fault_oInt_CAN_a4cc5d9c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status1_oInt_CAN_85735321_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status2_oInt_CAN_6e44e822_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Status3_oInt_CAN_8186831c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/ELECTRON_BSI_oE_CAN_ab0b72ff_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_oE_CAN_60b2710c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    17 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Fault_oInt_CAN_0a8001a9_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    18 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status1_oInt_CAN_459f2204_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    19 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status2_oInt_CAN_aea89907_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    20 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status3_oInt_CAN_416af239_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    21 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status4_oInt_CAN_a3b6e940_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    22 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Status5_oInt_CAN_4c74827e_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    23 */  /* [/ActiveEcuC/Com/ComConfig/ParkCommand_oE_CAN_5e7110cb_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    24 */  /* [/ActiveEcuC/Com/ComConfig/ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    25 */  /* [/ActiveEcuC/Com/ComConfig/ReqToECANFunction_oE_CAN_01f67ab2_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    26 */  /* [/ActiveEcuC/Com/ComConfig/ReqToOBC_oE_CAN_09b8aa3e_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    27 */  /* [/ActiveEcuC/Com/ComConfig/VCU2_oE_CAN_e2d13d8a_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    28 */  /* [/ActiveEcuC/Com/ComConfig/VCU3_oE_CAN_2e7b3d14_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    29 */  /* [/ActiveEcuC/Com/ComConfig/VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    30 */  /* [/ActiveEcuC/Com/ComConfig/VCU_PCANInfo_oE_CAN_788ea84c_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    31 */  /* [/ActiveEcuC/Com/ComConfig/VCU_TU_oE_CAN_e21002f7_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  /*    32 */  /* [/ActiveEcuC/Com/ComConfig/VCU_oE_CAN_8b566170_Rx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Rx_473dcfe4] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxSigBufferArrayBased
**********************************************************************************************************************/
/** 
  \var    Com_RxSigBufferArrayBased
  \brief  Rx Signal and Group Signal Buffer. (UINT8_N, UINT8_DYN)
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxSigBufferArrayBasedType, COM_VAR_NOINIT) Com_RxSigBufferArrayBased[16];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx, /ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx_RxSignalBufferRouting] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx, /ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx_RxSignalBufferRouting] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx, /ActiveEcuC/Com/ComConfig/FunctionReqDiagData_oReqToECANFunction_oE_CAN_bb1485e7_Rx_RxSignalBufferRouting] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx_RxSignalBufferRouting] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx_RxSignalBufferRouting] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx, /ActiveEcuC/Com/ComConfig/ReqDiagDataToOBC_oReqToOBC_oE_CAN_e4611b21_Rx_RxSignalBufferRouting] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxSigBufferUInt16
**********************************************************************************************************************/
/** 
  \var    Com_RxSigBufferUInt16
  \brief  Rx Signal and Group Signal Buffer. (UINT16)
*/ 
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxSigBufferUInt16Type, COM_VAR_NOINIT) Com_RxSigBufferUInt16[44];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/ABS_VehSpd_oVCU_PCANInfo_oE_CAN_c4f9375f_Rx, /ActiveEcuC/Com/ComConfig/ABS_VehSpd_oVCU_PCANInfo_oE_CAN_c4f9375f_Rx_RxSignalBufferRouting] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/BMS_AuxBattVolt_oBMS3_oE_CAN_3f0258cb_Rx, /ActiveEcuC/Com/ComConfig/BMS_AuxBattVolt_oBMS3_oE_CAN_3f0258cb_Rx_RxSignalBufferRouting] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/BMS_DCRelayVoltage_oBMS9_oE_CAN_c2a98509_Rx, /ActiveEcuC/Com/ComConfig/BMS_DCRelayVoltage_oBMS9_oE_CAN_c2a98509_Rx_RxSignalBufferRouting] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/BMS_Fault_oBMS5_oE_CAN_87a5f6c7_Rx, /ActiveEcuC/Com/ComConfig/BMS_Fault_oBMS5_oE_CAN_87a5f6c7_Rx_RxSignalBufferRouting] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/BMS_HighestChargeCurrentAllow_oBMS6_oE_CAN_060c33c4_Rx, /ActiveEcuC/Com/ComConfig/BMS_HighestChargeCurrentAllow_oBMS6_oE_CAN_060c33c4_Rx_RxSignalBufferRouting] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/BMS_HighestChargeVoltageAllow_oBMS6_oE_CAN_c465fe5e_Rx, /ActiveEcuC/Com/ComConfig/BMS_HighestChargeVoltageAllow_oBMS6_oE_CAN_c465fe5e_Rx_RxSignalBufferRouting] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/BMS_SOC_oBMS1_oE_CAN_4aa6784d_Rx, /ActiveEcuC/Com/ComConfig/BMS_SOC_oBMS1_oE_CAN_4aa6784d_Rx_RxSignalBufferRouting] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/BMS_Voltage_oBMS1_oE_CAN_2c5227ff_Rx, /ActiveEcuC/Com/ComConfig/BMS_Voltage_oBMS1_oE_CAN_2c5227ff_Rx_RxSignalBufferRouting] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_2_5V_oDCHV_Status2_oInt_CAN_79e30624_Rx, /ActiveEcuC/Com/ComConfig/DCHV_ADC_2_5V_oDCHV_Status2_oInt_CAN_79e30624_Rx_RxSignalBufferRouting] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_IOUT_oDCHV_Status1_oInt_CAN_db9652d6_Rx, /ActiveEcuC/Com/ComConfig/DCHV_ADC_IOUT_oDCHV_Status1_oInt_CAN_db9652d6_Rx_RxSignalBufferRouting] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_VOUT_oDCHV_Status1_oInt_CAN_95b5698f_Rx, /ActiveEcuC/Com/ComConfig/DCHV_ADC_VOUT_oDCHV_Status1_oInt_CAN_95b5698f_Rx_RxSignalBufferRouting] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Command_Current_Reference_oDCHV_Status1_oInt_CAN_205fcb72_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Command_Current_Reference_oDCHV_Status1_oInt_CAN_205fcb72_Rx_RxSignalBufferRouting] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_Command_Voltage_Reference_oDCHV_Status1_oInt_CAN_0d7c27e1_Rx, /ActiveEcuC/Com/ComConfig/DCHV_Command_Voltage_Reference_oDCHV_Status1_oInt_CAN_0d7c27e1_Rx_RxSignalBufferRouting] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Applied_Derating_Factor_oDCLV_Status3_oInt_CAN_8199edb4_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Applied_Derating_Factor_oDCLV_Status3_oInt_CAN_8199edb4_Rx_RxSignalBufferRouting] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Command_Current_Reference_oDCLV_Status1_oInt_CAN_572e045e_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Command_Current_Reference_oDCLV_Status1_oInt_CAN_572e045e_Rx_RxSignalBufferRouting] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Input_Voltage_oDCLV_Status3_oInt_CAN_a46268d5_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Input_Voltage_oDCLV_Status3_oInt_CAN_a46268d5_Rx_RxSignalBufferRouting] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Measured_Current_oDCLV_Status1_oInt_CAN_4e0e970b_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Measured_Current_oDCLV_Status1_oInt_CAN_4e0e970b_Rx_RxSignalBufferRouting] */
  /*    17 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Power_oDCLV_Status1_oInt_CAN_74948da2_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Power_oDCLV_Status1_oInt_CAN_74948da2_Rx_RxSignalBufferRouting] */
  /*    18 */  /* [/ActiveEcuC/Com/ComConfig/PFC_CommandVdclink_V_oPFC_Status1_oInt_CAN_d9828b27_Rx, /ActiveEcuC/Com/ComConfig/PFC_CommandVdclink_V_oPFC_Status1_oInt_CAN_d9828b27_Rx_RxSignalBufferRouting] */
  /*    19 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_Freq_Hz_oPFC_Status5_oInt_CAN_6dc86329_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH1_Freq_Hz_oPFC_Status5_oInt_CAN_6dc86329_Rx_RxSignalBufferRouting] */
  /*    20 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_Mean_0A1_oPFC_Status4_oInt_CAN_587b772d_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH1_Mean_0A1_oPFC_Status4_oInt_CAN_587b772d_Rx_RxSignalBufferRouting] */
  /*    21 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_Peak_0A1_oPFC_Status2_oInt_CAN_dfd13a45_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH1_Peak_0A1_oPFC_Status2_oInt_CAN_dfd13a45_Rx_RxSignalBufferRouting] */
  /*    22 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH1_RMS_0A1_oPFC_Status3_oInt_CAN_e736f57d_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH1_RMS_0A1_oPFC_Status3_oInt_CAN_e736f57d_Rx_RxSignalBufferRouting] */
  /*    23 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_Freq_Hz_oPFC_Status5_oInt_CAN_7ee05a5a_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH2_Freq_Hz_oPFC_Status5_oInt_CAN_7ee05a5a_Rx_RxSignalBufferRouting] */
  /*    24 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_Mean_0A1_oPFC_Status4_oInt_CAN_91647f92_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH2_Mean_0A1_oPFC_Status4_oInt_CAN_91647f92_Rx_RxSignalBufferRouting] */
  /*    25 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_Peak_0A1_oPFC_Status2_oInt_CAN_16ce32fa_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH2_Peak_0A1_oPFC_Status2_oInt_CAN_16ce32fa_Rx_RxSignalBufferRouting] */
  /*    26 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH2_RMS_0A1_oPFC_Status3_oInt_CAN_f41ecc0e_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH2_RMS_0A1_oPFC_Status3_oInt_CAN_f41ecc0e_Rx_RxSignalBufferRouting] */
  /*    27 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_Freq_Hz_oPFC_Status5_oInt_CAN_7007b28b_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH3_Freq_Hz_oPFC_Status5_oInt_CAN_7007b28b_Rx_RxSignalBufferRouting] */
  /*    28 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_Mean_0A1_oPFC_Status4_oInt_CAN_60be7a38_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH3_Mean_0A1_oPFC_Status4_oInt_CAN_60be7a38_Rx_RxSignalBufferRouting] */
  /*    29 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_Peak_0A1_oPFC_Status2_oInt_CAN_e7143750_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH3_Peak_0A1_oPFC_Status2_oInt_CAN_e7143750_Rx_RxSignalBufferRouting] */
  /*    30 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IPH3_RMS_0A1_oPFC_Status3_oInt_CAN_faf924df_Rx, /ActiveEcuC/Com/ComConfig/PFC_IPH3_RMS_0A1_oPFC_Status3_oInt_CAN_faf924df_Rx_RxSignalBufferRouting] */
  /*    31 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH1_Freq_Hz_oPFC_Status5_oInt_CAN_489242f9_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH1_Freq_Hz_oPFC_Status5_oInt_CAN_489242f9_Rx_RxSignalBufferRouting] */
  /*    32 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH2_Freq_Hz_oPFC_Status5_oInt_CAN_5bba7b8a_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH2_Freq_Hz_oPFC_Status5_oInt_CAN_5bba7b8a_Rx_RxSignalBufferRouting] */
  /*    33 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH3_Freq_Hz_oPFC_Status5_oInt_CAN_555d935b_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH3_Freq_Hz_oPFC_Status5_oInt_CAN_555d935b_Rx_RxSignalBufferRouting] */
  /*    34 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH12_Mean_V_oPFC_Status4_oInt_CAN_6f1a2a48_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH12_Mean_V_oPFC_Status4_oInt_CAN_6f1a2a48_Rx_RxSignalBufferRouting] */
  /*    35 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH12_Peak_V_oPFC_Status2_oInt_CAN_2da0b0ab_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH12_Peak_V_oPFC_Status2_oInt_CAN_2da0b0ab_Rx_RxSignalBufferRouting] */
  /*    36 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH12_RMS_V_oPFC_Status3_oInt_CAN_dbb6f081_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH12_RMS_V_oPFC_Status3_oInt_CAN_dbb6f081_Rx_RxSignalBufferRouting] */
  /*    37 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH23_Mean_V_oPFC_Status4_oInt_CAN_4052f033_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH23_Mean_V_oPFC_Status4_oInt_CAN_4052f033_Rx_RxSignalBufferRouting] */
  /*    38 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH23_Peak_V_oPFC_Status2_oInt_CAN_02e86ad0_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH23_Peak_V_oPFC_Status2_oInt_CAN_02e86ad0_Rx_RxSignalBufferRouting] */
  /*    39 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH23_RMS_V_oPFC_Status3_oInt_CAN_fc100cbd_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH23_RMS_V_oPFC_Status3_oInt_CAN_fc100cbd_Rx_RxSignalBufferRouting] */
  /*    40 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH31_Mean_V_oPFC_Status4_oInt_CAN_3674def2_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH31_Mean_V_oPFC_Status4_oInt_CAN_3674def2_Rx_RxSignalBufferRouting] */
  /*    41 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH31_Peak_V_oPFC_Status2_oInt_CAN_74ce4411_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH31_Peak_V_oPFC_Status2_oInt_CAN_74ce4411_Rx_RxSignalBufferRouting] */
  /*    42 */  /* [/ActiveEcuC/Com/ComConfig/PFC_VPH31_RMS_V_oPFC_Status3_oInt_CAN_067f5dfd_Rx, /ActiveEcuC/Com/ComConfig/PFC_VPH31_RMS_V_oPFC_Status3_oInt_CAN_067f5dfd_Rx_RxSignalBufferRouting] */
  /*    43 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Vdclink_V_oPFC_Status1_oInt_CAN_cb855707_Rx, /ActiveEcuC/Com/ComConfig/PFC_Vdclink_V_oPFC_Status1_oInt_CAN_cb855707_Rx_RxSignalBufferRouting] */

#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxSigBufferUInt32
**********************************************************************************************************************/
/** 
  \var    Com_RxSigBufferUInt32
  \brief  Rx Signal and Group Signal Buffer. (UINT32)
*/ 
#define COM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxSigBufferUInt32Type, COM_VAR_NOINIT) Com_RxSigBufferUInt32[2];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/VCU_CptTemporel_oVCU_oE_CAN_80a510d2_Rx, /ActiveEcuC/Com/ComConfig/VCU_CptTemporel_oVCU_oE_CAN_80a510d2_Rx_RxSignalBufferRouting] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/VCU_Kilometrage_oVCU_oE_CAN_90aff34b_Rx, /ActiveEcuC/Com/ComConfig/VCU_Kilometrage_oVCU_oE_CAN_90aff34b_Rx_RxSignalBufferRouting] */

#define COM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_RxSigBufferUInt8
**********************************************************************************************************************/
/** 
  \var    Com_RxSigBufferUInt8
  \brief  Rx Signal and Group Signal Buffer. (BOOLEAN, UINT8)
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_RxSigBufferUInt8Type, COM_VAR_NOINIT) Com_RxSigBufferUInt8[124];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/ABS_VehSpdValidFlag_oVCU_PCANInfo_oE_CAN_badcc650_Rx, /ActiveEcuC/Com/ComConfig/ABS_VehSpdValidFlag_oVCU_PCANInfo_oE_CAN_badcc650_Rx_RxSignalBufferRouting] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/BMS_CC2_connection_Status_oBMS8_oE_CAN_ce1fe0ec_Rx, /ActiveEcuC/Com/ComConfig/BMS_CC2_connection_Status_oBMS8_oE_CAN_ce1fe0ec_Rx_RxSignalBufferRouting] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/BMS_FastChargeSt_oBMS6_oE_CAN_21564382_Rx, /ActiveEcuC/Com/ComConfig/BMS_FastChargeSt_oBMS6_oE_CAN_21564382_Rx_RxSignalBufferRouting] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/BMS_MainConnectorState_oBMS1_oE_CAN_db099b33_Rx, /ActiveEcuC/Com/ComConfig/BMS_MainConnectorState_oBMS1_oE_CAN_db099b33_Rx_RxSignalBufferRouting] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/BMS_OnBoardChargerEnable_oBMS6_oE_CAN_b5381f70_Rx, /ActiveEcuC/Com/ComConfig/BMS_OnBoardChargerEnable_oBMS6_oE_CAN_b5381f70_Rx_RxSignalBufferRouting] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/BMS_QuickChargeConnectorState_oBMS1_oE_CAN_940005e5_Rx, /ActiveEcuC/Com/ComConfig/BMS_QuickChargeConnectorState_oBMS1_oE_CAN_940005e5_Rx_RxSignalBufferRouting] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/BMS_RelayOpenReq_oBMS1_oE_CAN_9e155f98_Rx, /ActiveEcuC/Com/ComConfig/BMS_RelayOpenReq_oBMS1_oE_CAN_9e155f98_Rx_RxSignalBufferRouting] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/BMS_SlowChargeSt_oBMS6_oE_CAN_25b6b066_Rx, /ActiveEcuC/Com/ComConfig/BMS_SlowChargeSt_oBMS6_oE_CAN_25b6b066_Rx_RxSignalBufferRouting] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/BSI_ChargeState_oBSIInfo_oE_CAN_be52c881_Rx, /ActiveEcuC/Com/ComConfig/BSI_ChargeState_oBSIInfo_oE_CAN_be52c881_Rx_RxSignalBufferRouting] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/BSI_ChargeTypeStatus_oBSIInfo_oE_CAN_f023c7ab_Rx, /ActiveEcuC/Com/ComConfig/BSI_ChargeTypeStatus_oBSIInfo_oE_CAN_f023c7ab_Rx_RxSignalBufferRouting] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/BSI_LockedVehicle_oBSIInfo_oE_CAN_bee231f7_Rx, /ActiveEcuC/Com/ComConfig/BSI_LockedVehicle_oBSIInfo_oE_CAN_bee231f7_Rx_RxSignalBufferRouting] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/BSI_MainWakeup_oBSIInfo_oE_CAN_3acf26ac_Rx, /ActiveEcuC/Com/ComConfig/BSI_MainWakeup_oBSIInfo_oE_CAN_3acf26ac_Rx_RxSignalBufferRouting] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/BSI_PostDriveWakeup_oBSIInfo_oE_CAN_c679787c_Rx, /ActiveEcuC/Com/ComConfig/BSI_PostDriveWakeup_oBSIInfo_oE_CAN_c679787c_Rx_RxSignalBufferRouting] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/BSI_PreDriveWakeup_oBSIInfo_oE_CAN_10f8dc31_Rx, /ActiveEcuC/Com/ComConfig/BSI_PreDriveWakeup_oBSIInfo_oE_CAN_10f8dc31_Rx_RxSignalBufferRouting] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/BSI_VCUHeatPumpWorkingMode_oBSIInfo_oE_CAN_56dcb551_Rx, /ActiveEcuC/Com/ComConfig/BSI_VCUHeatPumpWorkingMode_oBSIInfo_oE_CAN_56dcb551_Rx_RxSignalBufferRouting] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/BSI_VCUSynchroGPC_oBSIInfo_oE_CAN_8a27e8bd_Rx, /ActiveEcuC/Com/ComConfig/BSI_VCUSynchroGPC_oBSIInfo_oE_CAN_8a27e8bd_Rx_RxSignalBufferRouting] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTPE2_oVCU_PCANInfo_oE_CAN_4e44e208_Rx, /ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTPE2_oVCU_PCANInfo_oE_CAN_4e44e208_Rx_RxSignalBufferRouting] */
  /*    17 */  /* [/ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTP_oVCU_PCANInfo_oE_CAN_1a153f59_Rx, /ActiveEcuC/Com/ComConfig/COUPURE_CONSO_CTP_oVCU_PCANInfo_oE_CAN_1a153f59_Rx_RxSignalBufferRouting] */
  /*    18 */  /* [/ActiveEcuC/Com/ComConfig/DATA0_oProgTool_SupEnterBoot_oInt_CAN_d0fc95c5_Rx, /ActiveEcuC/Com/ComConfig/DATA0_oProgTool_SupEnterBoot_oInt_CAN_d0fc95c5_Rx_RxSignalBufferRouting] */
  /*    19 */  /* [/ActiveEcuC/Com/ComConfig/DATA1_oProgTool_SupEnterBoot_oInt_CAN_e60e0536_Rx, /ActiveEcuC/Com/ComConfig/DATA1_oProgTool_SupEnterBoot_oInt_CAN_e60e0536_Rx_RxSignalBufferRouting] */
  /*    20 */  /* [/ActiveEcuC/Com/ComConfig/DATA2_oProgTool_SupEnterBoot_oInt_CAN_bd19b423_Rx, /ActiveEcuC/Com/ComConfig/DATA2_oProgTool_SupEnterBoot_oInt_CAN_bd19b423_Rx_RxSignalBufferRouting] */
  /*    21 */  /* [/ActiveEcuC/Com/ComConfig/DATA3_oProgTool_SupEnterBoot_oInt_CAN_8beb24d0_Rx, /ActiveEcuC/Com/ComConfig/DATA3_oProgTool_SupEnterBoot_oInt_CAN_8beb24d0_Rx_RxSignalBufferRouting] */
  /*    22 */  /* [/ActiveEcuC/Com/ComConfig/DATA4_oProgTool_SupEnterBoot_oInt_CAN_0b36d609_Rx, /ActiveEcuC/Com/ComConfig/DATA4_oProgTool_SupEnterBoot_oInt_CAN_0b36d609_Rx_RxSignalBufferRouting] */
  /*    23 */  /* [/ActiveEcuC/Com/ComConfig/DATA5_oProgTool_SupEnterBoot_oInt_CAN_3dc446fa_Rx, /ActiveEcuC/Com/ComConfig/DATA5_oProgTool_SupEnterBoot_oInt_CAN_3dc446fa_Rx_RxSignalBufferRouting] */
  /*    24 */  /* [/ActiveEcuC/Com/ComConfig/DATA6_oProgTool_SupEnterBoot_oInt_CAN_66d3f7ef_Rx, /ActiveEcuC/Com/ComConfig/DATA6_oProgTool_SupEnterBoot_oInt_CAN_66d3f7ef_Rx_RxSignalBufferRouting] */
  /*    25 */  /* [/ActiveEcuC/Com/ComConfig/DATA7_oProgTool_SupEnterBoot_oInt_CAN_5021671c_Rx, /ActiveEcuC/Com/ComConfig/DATA7_oProgTool_SupEnterBoot_oInt_CAN_5021671c_Rx_RxSignalBufferRouting] */
  /*    26 */  /* [/ActiveEcuC/Com/ComConfig/DATA_ACQ_JDD_BSI_2_oNEW_JDD_oE_CAN_16efcc6e_Rx, /ActiveEcuC/Com/ComConfig/DATA_ACQ_JDD_BSI_2_oNEW_JDD_oE_CAN_16efcc6e_Rx_RxSignalBufferRouting] */
  /*    27 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_5_oDCHV_Status2_oInt_CAN_46287fbe_Rx, /ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_5_oDCHV_Status2_oInt_CAN_46287fbe_Rx_RxSignalBufferRouting] */
  /*    28 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_6_oDCHV_Status2_oInt_CAN_1e36d696_Rx, /ActiveEcuC/Com/ComConfig/DCHV_ADC_NTC_MOD_6_oDCHV_Status2_oInt_CAN_1e36d696_Rx_RxSignalBufferRouting] */
  /*    29 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_DCHVStatus_oDCHV_Status1_oInt_CAN_4479f96a_Rx, /ActiveEcuC/Com/ComConfig/DCHV_DCHVStatus_oDCHV_Status1_oInt_CAN_4479f96a_Rx_RxSignalBufferRouting] */
  /*    30 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_H_oDCHV_Fault_oInt_CAN_a3babff0_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_H_oDCHV_Fault_oInt_CAN_a3babff0_Rx_RxSignalBufferRouting] */
  /*    31 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_L_oDCHV_Fault_oInt_CAN_ff1b2cf0_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_CAP_FAIL_L_oDCHV_Fault_oInt_CAN_ff1b2cf0_Rx_RxSignalBufferRouting] */
  /*    32 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_IOUT_oDCHV_Fault_oInt_CAN_2f168ffb_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_IOUT_oDCHV_Fault_oInt_CAN_2f168ffb_Rx_RxSignalBufferRouting] */
  /*    33 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_NEG_oDCHV_Fault_oInt_CAN_d091e3ea_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_NEG_oDCHV_Fault_oInt_CAN_d091e3ea_Rx_RxSignalBufferRouting] */
  /*    34 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_POS_oDCHV_Fault_oInt_CAN_d5b3bbb7_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OC_POS_oDCHV_Fault_oInt_CAN_d5b3bbb7_Rx_RxSignalBufferRouting] */
  /*    35 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_IN_oDCHV_Fault_oInt_CAN_5338fe6d_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_IN_oDCHV_Fault_oInt_CAN_5338fe6d_Rx_RxSignalBufferRouting] */
  /*    36 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD5_oDCHV_Fault_oInt_CAN_05da4c61_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD5_oDCHV_Fault_oInt_CAN_05da4c61_Rx_RxSignalBufferRouting] */
  /*    37 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD6_oDCHV_Fault_oInt_CAN_3ca2e121_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_NTC_MOD6_oDCHV_Fault_oInt_CAN_3ca2e121_Rx_RxSignalBufferRouting] */
  /*    38 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_oDCHV_Fault_oInt_CAN_33753ff7_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OT_oDCHV_Fault_oInt_CAN_33753ff7_Rx_RxSignalBufferRouting] */
  /*    39 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OV_VOUT_oDCHV_Fault_oInt_CAN_c4e31cdb_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_OV_VOUT_oDCHV_Fault_oInt_CAN_c4e31cdb_Rx_RxSignalBufferRouting] */
  /*    40 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_UV_12V_oDCHV_Fault_oInt_CAN_7ceb9676_Rx, /ActiveEcuC/Com/ComConfig/DCHV_IOM_ERR_UV_12V_oDCHV_Fault_oInt_CAN_7ceb9676_Rx_RxSignalBufferRouting] */
  /*    41 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4C7_AC_oDCHV_Fault_oInt_CAN_033d492e_Rx, /ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4C7_AC_oDCHV_Fault_oInt_CAN_033d492e_Rx_RxSignalBufferRouting] */
  /*    42 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4E5_AC_oDCHV_Status1_oInt_CAN_91fed45b_Rx, /ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4E5_AC_oDCHV_Status1_oInt_CAN_91fed45b_Rx_RxSignalBufferRouting] */
  /*    43 */  /* [/ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4FA_AC_oDCHV_Status2_oInt_CAN_596855d0_Rx, /ActiveEcuC/Com/ComConfig/DCHV_RollingCounter_4FA_AC_oDCHV_Status2_oInt_CAN_596855d0_Rx_RxSignalBufferRouting] */
  /*    44 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Command_Voltage_Reference_oDCLV_Status1_oInt_CAN_7a0de8cd_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Command_Voltage_Reference_oDCLV_Status1_oInt_CAN_7a0de8cd_Rx_RxSignalBufferRouting] */
  /*    45 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_DCLVStatus_oDCLV_Status1_oInt_CAN_aede0f5b_Rx, /ActiveEcuC/Com/ComConfig/DCLV_DCLVStatus_oDCLV_Status1_oInt_CAN_aede0f5b_Rx_RxSignalBufferRouting] */
  /*    46 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Input_Current_oDCLV_Status3_oInt_CAN_053924dc_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Input_Current_oDCLV_Status3_oInt_CAN_053924dc_Rx_RxSignalBufferRouting] */
  /*    47 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_Measured_Voltage_oDCLV_Status1_oInt_CAN_ef55db02_Rx, /ActiveEcuC/Com/ComConfig/DCLV_Measured_Voltage_oDCLV_Status1_oInt_CAN_ef55db02_Rx_RxSignalBufferRouting] */
  /*    48 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_A_HV_FAULT_oDCLV_Fault_oInt_CAN_7ab3f331_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OC_A_HV_FAULT_oDCLV_Fault_oInt_CAN_7ab3f331_Rx_RxSignalBufferRouting] */
  /*    49 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_A_LV_FAULT_oDCLV_Fault_oInt_CAN_2ddd91e0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OC_A_LV_FAULT_oDCLV_Fault_oInt_CAN_2ddd91e0_Rx_RxSignalBufferRouting] */
  /* Index        Referable Keys */
  /*    50 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_B_HV_FAULT_oDCLV_Fault_oInt_CAN_699bca42_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OC_B_HV_FAULT_oDCLV_Fault_oInt_CAN_699bca42_Rx_RxSignalBufferRouting] */
  /*    51 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OC_B_LV_FAULT_oDCLV_Fault_oInt_CAN_3ef5a893_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OC_B_LV_FAULT_oDCLV_Fault_oInt_CAN_3ef5a893_Rx_RxSignalBufferRouting] */
  /*    52 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OT_FAULT_oDCLV_Fault_oInt_CAN_334ae1fe_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OT_FAULT_oDCLV_Fault_oInt_CAN_334ae1fe_Rx_RxSignalBufferRouting] */
  /*    53 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OV_INT_A_FAULT_oDCLV_Fault_oInt_CAN_3c3a442a_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OV_INT_A_FAULT_oDCLV_Fault_oInt_CAN_3c3a442a_Rx_RxSignalBufferRouting] */
  /*    54 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OV_INT_B_FAULT_oDCLV_Fault_oInt_CAN_3ee4430d_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OV_INT_B_FAULT_oDCLV_Fault_oInt_CAN_3ee4430d_Rx_RxSignalBufferRouting] */
  /*    55 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_OV_UV_HV_FAULT_oDCLV_Fault_oInt_CAN_3d935207_Rx, /ActiveEcuC/Com/ComConfig/DCLV_OV_UV_HV_FAULT_oDCLV_Fault_oInt_CAN_3d935207_Rx_RxSignalBufferRouting] */
  /*    56 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_PWM_STOP_oDCLV_Fault_oInt_CAN_13fc246e_Rx, /ActiveEcuC/Com/ComConfig/DCLV_PWM_STOP_oDCLV_Fault_oInt_CAN_13fc246e_Rx_RxSignalBufferRouting] */
  /*    57 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_496_AC_oDCLV_Fault_oInt_CAN_974bbf25_Rx, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_496_AC_oDCLV_Fault_oInt_CAN_974bbf25_Rx_RxSignalBufferRouting] */
  /*    58 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_497_AC_oDCLV_Status1_oInt_CAN_90fced42_Rx, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_497_AC_oDCLV_Status1_oInt_CAN_90fced42_Rx_RxSignalBufferRouting] */
  /*    59 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_498_AC_oDCLV_Status2_oInt_CAN_6b9cc64d_Rx, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_498_AC_oDCLV_Status2_oInt_CAN_6b9cc64d_Rx_RxSignalBufferRouting] */
  /*    60 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_499_AC_oDCLV_Status3_oInt_CAN_ead2b632_Rx, /ActiveEcuC/Com/ComConfig/DCLV_RollingCounter_499_AC_oDCLV_Status3_oInt_CAN_ead2b632_Rx_RxSignalBufferRouting] */
  /*    61 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OC_ALARM_oDCLV_Fault_oInt_CAN_d37c4d73_Rx, /ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OC_ALARM_oDCLV_Fault_oInt_CAN_d37c4d73_Rx_RxSignalBufferRouting] */
  /*    62 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OV_ALARM_oDCLV_Fault_oInt_CAN_cf4195ca_Rx, /ActiveEcuC/Com/ComConfig/DCLV_SW_HV_OV_ALARM_oDCLV_Fault_oInt_CAN_cf4195ca_Rx_RxSignalBufferRouting] */
  /*    63 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_HV_UV_ALARM_oDCLV_Fault_oInt_CAN_215a6f25_Rx, /ActiveEcuC/Com/ComConfig/DCLV_SW_HV_UV_ALARM_oDCLV_Fault_oInt_CAN_215a6f25_Rx_RxSignalBufferRouting] */
  /*    64 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OC_ALARM_oDCLV_Fault_oInt_CAN_a2f65759_Rx, /ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OC_ALARM_oDCLV_Fault_oInt_CAN_a2f65759_Rx_RxSignalBufferRouting] */
  /*    65 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OV_ALARM_oDCLV_Fault_oInt_CAN_becb8fe0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_SW_LV_OV_ALARM_oDCLV_Fault_oInt_CAN_becb8fe0_Rx_RxSignalBufferRouting] */
  /*    66 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_L_BUCK_oDCLV_Status2_oInt_CAN_f4d254b0_Rx, /ActiveEcuC/Com/ComConfig/DCLV_T_L_BUCK_oDCLV_Status2_oInt_CAN_f4d254b0_Rx_RxSignalBufferRouting] */
  /*    67 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_PP_A_oDCLV_Status2_oInt_CAN_33203dc6_Rx, /ActiveEcuC/Com/ComConfig/DCLV_T_PP_A_oDCLV_Status2_oInt_CAN_33203dc6_Rx_RxSignalBufferRouting] */
  /*    68 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_PP_B_oDCLV_Status2_oInt_CAN_6b3e94ee_Rx, /ActiveEcuC/Com/ComConfig/DCLV_T_PP_B_oDCLV_Status2_oInt_CAN_6b3e94ee_Rx_RxSignalBufferRouting] */
  /*    69 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_A_oDCLV_Status2_oInt_CAN_f172fb4d_Rx, /ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_A_oDCLV_Status2_oInt_CAN_f172fb4d_Rx_RxSignalBufferRouting] */
  /*    70 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_B_oDCLV_Status2_oInt_CAN_a96c5265_Rx, /ActiveEcuC/Com/ComConfig/DCLV_T_SW_BUCK_B_oDCLV_Status2_oInt_CAN_a96c5265_Rx_RxSignalBufferRouting] */
  /*    71 */  /* [/ActiveEcuC/Com/ComConfig/DCLV_T_TX_PP_oDCLV_Status2_oInt_CAN_19721921_Rx, /ActiveEcuC/Com/ComConfig/DCLV_T_TX_PP_oDCLV_Status2_oInt_CAN_19721921_Rx_RxSignalBufferRouting] */
  /*    72 */  /* [/ActiveEcuC/Com/ComConfig/DIAG_INTEGRA_ELEC_oELECTRON_BSI_oE_CAN_fb8cbe6b_Rx, /ActiveEcuC/Com/ComConfig/DIAG_INTEGRA_ELEC_oELECTRON_BSI_oE_CAN_fb8cbe6b_Rx_RxSignalBufferRouting] */
  /*    73 */  /* [/ActiveEcuC/Com/ComConfig/EFFAC_DEFAUT_DIAG_oELECTRON_BSI_oE_CAN_83a2b5c6_Rx, /ActiveEcuC/Com/ComConfig/EFFAC_DEFAUT_DIAG_oELECTRON_BSI_oE_CAN_83a2b5c6_Rx_RxSignalBufferRouting] */
  /*    74 */  /* [/ActiveEcuC/Com/ComConfig/MODE_DIAG_oELECTRON_BSI_oE_CAN_f76ac80c_Rx, /ActiveEcuC/Com/ComConfig/MODE_DIAG_oELECTRON_BSI_oE_CAN_f76ac80c_Rx_RxSignalBufferRouting] */
  /*    75 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH1_oPFC_Fault_oInt_CAN_a122859f_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH1_oPFC_Fault_oInt_CAN_a122859f_Rx_RxSignalBufferRouting] */
  /*    76 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH2_oPFC_Fault_oInt_CAN_da3c077c_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH2_oPFC_Fault_oInt_CAN_da3c077c_Rx_RxSignalBufferRouting] */
  /*    77 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH3_oPFC_Fault_oInt_CAN_45e684e2_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_PH3_oPFC_Fault_oInt_CAN_45e684e2_Rx_RxSignalBufferRouting] */
  /*    78 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT1_oPFC_Fault_oInt_CAN_25f6ba61_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT1_oPFC_Fault_oInt_CAN_25f6ba61_Rx_RxSignalBufferRouting] */
  /*    79 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT2_oPFC_Fault_oInt_CAN_5ee83882_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT2_oPFC_Fault_oInt_CAN_5ee83882_Rx_RxSignalBufferRouting] */
  /*    80 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT3_oPFC_Fault_oInt_CAN_c132bb1c_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OC_SHUNT3_oPFC_Fault_oInt_CAN_c132bb1c_Rx_RxSignalBufferRouting] */
  /*    81 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M1_oPFC_Fault_oInt_CAN_a0dec855_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M1_oPFC_Fault_oInt_CAN_a0dec855_Rx_RxSignalBufferRouting] */
  /*    82 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M3_oPFC_Fault_oInt_CAN_441ac928_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M3_oPFC_Fault_oInt_CAN_441ac928_Rx_RxSignalBufferRouting] */
  /*    83 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M4_oPFC_Fault_oInt_CAN_2dfd4f70_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OT_NTC1_M4_oPFC_Fault_oInt_CAN_2dfd4f70_Rx_RxSignalBufferRouting] */
  /*    84 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_DCLINK_oPFC_Fault_oInt_CAN_df590e67_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_DCLINK_oPFC_Fault_oInt_CAN_df590e67_Rx_RxSignalBufferRouting] */
  /*    85 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH12_oPFC_Fault_oInt_CAN_70e217b6_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH12_oPFC_Fault_oInt_CAN_70e217b6_Rx_RxSignalBufferRouting] */
  /*    86 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH23_oPFC_Fault_oInt_CAN_d6403968_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH23_oPFC_Fault_oInt_CAN_d6403968_Rx_RxSignalBufferRouting] */
  /*    87 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH31_oPFC_Fault_oInt_CAN_25ac5cd5_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_OV_VPH31_oPFC_Fault_oInt_CAN_25ac5cd5_Rx_RxSignalBufferRouting] */
  /*    88 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO4_oPFC_Fault_oInt_CAN_83f330a8_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO4_oPFC_Fault_oInt_CAN_83f330a8_Rx_RxSignalBufferRouting] */
  /*    89 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO7_oPFC_Fault_oInt_CAN_f8edb24b_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UVLO_ISO7_oPFC_Fault_oInt_CAN_f8edb24b_Rx_RxSignalBufferRouting] */
  /*    90 */  /* [/ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UV_12V_oPFC_Fault_oInt_CAN_a7954067_Rx, /ActiveEcuC/Com/ComConfig/PFC_IOM_ERR_UV_12V_oPFC_Fault_oInt_CAN_a7954067_Rx_RxSignalBufferRouting] */
  /*    91 */  /* [/ActiveEcuC/Com/ComConfig/PFC_PFCStatus_oPFC_Status1_oInt_CAN_4eddb451_Rx, /ActiveEcuC/Com/ComConfig/PFC_PFCStatus_oPFC_Status1_oInt_CAN_4eddb451_Rx_RxSignalBufferRouting] */
  /*    92 */  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_420_AC_oPFC_Fault_oInt_CAN_571a9c7f_Rx, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_420_AC_oPFC_Fault_oInt_CAN_571a9c7f_Rx_RxSignalBufferRouting] */
  /*    93 */  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_444_AC_oPFC_Status1_oInt_CAN_8a8e39e7_Rx, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_444_AC_oPFC_Status1_oInt_CAN_8a8e39e7_Rx_RxSignalBufferRouting] */
  /*    94 */  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_455_AC_oPFC_Status2_oInt_CAN_e8283f32_Rx, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_455_AC_oPFC_Status2_oInt_CAN_e8283f32_Rx_RxSignalBufferRouting] */
  /*    95 */  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_466_AC_oPFC_Status3_oInt_CAN_46299437_Rx, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_466_AC_oPFC_Status3_oInt_CAN_46299437_Rx_RxSignalBufferRouting] */
  /*    96 */  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_477_AC_oPFC_Status4_oInt_CAN_2d643298_Rx, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_477_AC_oPFC_Status4_oInt_CAN_2d643298_Rx_RxSignalBufferRouting] */
  /*    97 */  /* [/ActiveEcuC/Com/ComConfig/PFC_RollingCounter_488_AC_oPFC_Status5_oInt_CAN_5f1b9f30_Rx, /ActiveEcuC/Com/ComConfig/PFC_RollingCounter_488_AC_oPFC_Status5_oInt_CAN_5f1b9f30_Rx_RxSignalBufferRouting] */
  /*    98 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Temp_M1_C_oPFC_Status1_oInt_CAN_23fc84dd_Rx, /ActiveEcuC/Com/ComConfig/PFC_Temp_M1_C_oPFC_Status1_oInt_CAN_23fc84dd_Rx_RxSignalBufferRouting] */
  /*    99 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Temp_M3_C_oPFC_Status1_oInt_CAN_b39aa280_Rx, /ActiveEcuC/Com/ComConfig/PFC_Temp_M3_C_oPFC_Status1_oInt_CAN_b39aa280_Rx_RxSignalBufferRouting] */
  /* Index        Referable Keys */
  /*   100 */  /* [/ActiveEcuC/Com/ComConfig/PFC_Temp_M4_C_oPFC_Status1_oInt_CAN_7dca5e28_Rx, /ActiveEcuC/Com/ComConfig/PFC_Temp_M4_C_oPFC_Status1_oInt_CAN_7dca5e28_Rx_RxSignalBufferRouting] */
  /*   101 */  /* [/ActiveEcuC/Com/ComConfig/VCU_AccPedalPosition_oVCU_TU_oE_CAN_a12a1de4_Rx, /ActiveEcuC/Com/ComConfig/VCU_AccPedalPosition_oVCU_TU_oE_CAN_a12a1de4_Rx_RxSignalBufferRouting] */
  /*   102 */  /* [/ActiveEcuC/Com/ComConfig/VCU_ActivedischargeCommand_oCtrlDCDC_oE_CAN_6f6ad9a3_Rx, /ActiveEcuC/Com/ComConfig/VCU_ActivedischargeCommand_oCtrlDCDC_oE_CAN_6f6ad9a3_Rx_RxSignalBufferRouting] */
  /*   103 */  /* [/ActiveEcuC/Com/ComConfig/VCU_CDEAccJDD_oVCU_BSI_Wakeup_oE_CAN_153d1d79_Rx, /ActiveEcuC/Com/ComConfig/VCU_CDEAccJDD_oVCU_BSI_Wakeup_oE_CAN_153d1d79_Rx_RxSignalBufferRouting] */
  /*   104 */  /* [/ActiveEcuC/Com/ComConfig/VCU_CDEApcJDD_oVCU_BSI_Wakeup_oE_CAN_0cbccb8e_Rx, /ActiveEcuC/Com/ComConfig/VCU_CDEApcJDD_oVCU_BSI_Wakeup_oE_CAN_0cbccb8e_Rx_RxSignalBufferRouting] */
  /*   105 */  /* [/ActiveEcuC/Com/ComConfig/VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_Rx, /ActiveEcuC/Com/ComConfig/VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_Rx_RxSignalBufferRouting] */
  /*   106 */  /* [/ActiveEcuC/Com/ComConfig/VCU_DCDCActivation_oCtrlDCDC_oE_CAN_541eed40_Rx, /ActiveEcuC/Com/ComConfig/VCU_DCDCActivation_oCtrlDCDC_oE_CAN_541eed40_Rx_RxSignalBufferRouting] */
  /*   107 */  /* [/ActiveEcuC/Com/ComConfig/VCU_DCDCVoltageReq_oCtrlDCDC_oE_CAN_2adee191_Rx, /ActiveEcuC/Com/ComConfig/VCU_DCDCVoltageReq_oCtrlDCDC_oE_CAN_2adee191_Rx_RxSignalBufferRouting] */
  /*   108 */  /* [/ActiveEcuC/Com/ComConfig/VCU_DDEGMVSEEM_oVCU_BSI_Wakeup_oE_CAN_073a8f6a_Rx, /ActiveEcuC/Com/ComConfig/VCU_DDEGMVSEEM_oVCU_BSI_Wakeup_oE_CAN_073a8f6a_Rx_RxSignalBufferRouting] */
  /*   109 */  /* [/ActiveEcuC/Com/ComConfig/VCU_DMDActivChiller_oVCU_BSI_Wakeup_oE_CAN_eb265576_Rx, /ActiveEcuC/Com/ComConfig/VCU_DMDActivChiller_oVCU_BSI_Wakeup_oE_CAN_eb265576_Rx_RxSignalBufferRouting] */
  /*   110 */  /* [/ActiveEcuC/Com/ComConfig/VCU_DMDMeap2SEEM_oVCU_BSI_Wakeup_oE_CAN_3b64713a_Rx, /ActiveEcuC/Com/ComConfig/VCU_DMDMeap2SEEM_oVCU_BSI_Wakeup_oE_CAN_3b64713a_Rx_RxSignalBufferRouting] */
  /*   111 */  /* [/ActiveEcuC/Com/ComConfig/VCU_DiagMuxOnPwt_oVCU_BSI_Wakeup_oE_CAN_712c6c84_Rx, /ActiveEcuC/Com/ComConfig/VCU_DiagMuxOnPwt_oVCU_BSI_Wakeup_oE_CAN_712c6c84_Rx_RxSignalBufferRouting] */
  /*   112 */  /* [/ActiveEcuC/Com/ComConfig/VCU_EPWT_Status_oParkCommand_oE_CAN_c4868b19_Rx, /ActiveEcuC/Com/ComConfig/VCU_EPWT_Status_oParkCommand_oE_CAN_c4868b19_Rx_RxSignalBufferRouting] */
  /*   113 */  /* [/ActiveEcuC/Com/ComConfig/VCU_ElecMeterSaturation_oVCU3_oE_CAN_c481c141_Rx, /ActiveEcuC/Com/ComConfig/VCU_ElecMeterSaturation_oVCU3_oE_CAN_c481c141_Rx_RxSignalBufferRouting] */
  /*   114 */  /* [/ActiveEcuC/Com/ComConfig/VCU_EtatGmpHyb_oVCU_BSI_Wakeup_oE_CAN_0076bb1d_Rx, /ActiveEcuC/Com/ComConfig/VCU_EtatGmpHyb_oVCU_BSI_Wakeup_oE_CAN_0076bb1d_Rx_RxSignalBufferRouting] */
  /*   115 */  /* [/ActiveEcuC/Com/ComConfig/VCU_EtatPrincipSev_oVCU_BSI_Wakeup_oE_CAN_8c281110_Rx, /ActiveEcuC/Com/ComConfig/VCU_EtatPrincipSev_oVCU_BSI_Wakeup_oE_CAN_8c281110_Rx_RxSignalBufferRouting] */
  /*   116 */  /* [/ActiveEcuC/Com/ComConfig/VCU_EtatReseauElec_oVCU_BSI_Wakeup_oE_CAN_b805a640_Rx, /ActiveEcuC/Com/ComConfig/VCU_EtatReseauElec_oVCU_BSI_Wakeup_oE_CAN_b805a640_Rx_RxSignalBufferRouting] */
  /*   117 */  /* [/ActiveEcuC/Com/ComConfig/VCU_FrameChksum17B_oVCU_PCANInfo_oE_CAN_657e6685_Rx, /ActiveEcuC/Com/ComConfig/VCU_FrameChksum17B_oVCU_PCANInfo_oE_CAN_657e6685_Rx_RxSignalBufferRouting] */
  /*   118 */  /* [/ActiveEcuC/Com/ComConfig/VCU_Keyposition_oVCU2_oE_CAN_7b65e581_Rx, /ActiveEcuC/Com/ComConfig/VCU_Keyposition_oVCU2_oE_CAN_7b65e581_Rx_RxSignalBufferRouting] */
  /*   119 */  /* [/ActiveEcuC/Com/ComConfig/VCU_ModeEPSRequest_oVCU_BSI_Wakeup_oE_CAN_760ae031_Rx, /ActiveEcuC/Com/ComConfig/VCU_ModeEPSRequest_oVCU_BSI_Wakeup_oE_CAN_760ae031_Rx_RxSignalBufferRouting] */
  /*   120 */  /* [/ActiveEcuC/Com/ComConfig/VCU_PosShuntJDD_oVCU_BSI_Wakeup_oE_CAN_0978ac9d_Rx, /ActiveEcuC/Com/ComConfig/VCU_PosShuntJDD_oVCU_BSI_Wakeup_oE_CAN_0978ac9d_Rx_RxSignalBufferRouting] */
  /*   121 */  /* [/ActiveEcuC/Com/ComConfig/VCU_PrecondElecWakeup_oVCU_BSI_Wakeup_oE_CAN_6616770e_Rx, /ActiveEcuC/Com/ComConfig/VCU_PrecondElecWakeup_oVCU_BSI_Wakeup_oE_CAN_6616770e_Rx_RxSignalBufferRouting] */
  /*   122 */  /* [/ActiveEcuC/Com/ComConfig/VCU_RollingCounter17B_oVCU_PCANInfo_oE_CAN_57322517_Rx, /ActiveEcuC/Com/ComConfig/VCU_RollingCounter17B_oVCU_PCANInfo_oE_CAN_57322517_Rx_RxSignalBufferRouting] */
  /*   123 */  /* [/ActiveEcuC/Com/ComConfig/VCU_StopDelayedHMIWakeup_oVCU_BSI_Wakeup_oE_CAN_05bb1585_Rx, /ActiveEcuC/Com/ComConfig/VCU_StopDelayedHMIWakeup_oVCU_BSI_Wakeup_oE_CAN_05bb1585_Rx_RxSignalBufferRouting] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_SigGrpEventFlag
**********************************************************************************************************************/
/** 
  \var    Com_SigGrpEventFlag
  \brief  Flag is set if a group signal write access caused a triggered event.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_SigGrpEventFlagType, COM_VAR_NOINIT) Com_SigGrpEventFlag[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TmpRxBuffer
**********************************************************************************************************************/
/** 
  \var    Com_TmpRxBuffer
  \brief  Temporary buffer for Rx UINT8_N and UINT8_DYN signals.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TmpRxBufferType, COM_VAR_NOINIT) Com_TmpRxBuffer[8];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TransmitRequest
**********************************************************************************************************************/
/** 
  \var    Com_TransmitRequest
  \brief  Transmit request flag used for decoupled Tx I-PDU tranmission.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TransmitRequestType, COM_VAR_NOINIT) Com_TransmitRequest[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxBuffer
**********************************************************************************************************************/
/** 
  \var    Com_TxBuffer
  \brief  Shared uint8 buffer for Tx I-PDUs and ComSignalGroup shadow buffer.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxBufferType, COM_VAR_NOINIT) Com_TxBuffer[143];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DCDC_Temperature_oDC1_oE_CAN_ebbc428a_Tx] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DCDC_OutputVoltage_oDC1_oE_CAN_b16c6aa0_Tx] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DCDC_FaultLampRequest_oDC1_oE_CAN_66ccec8a_Tx, /ActiveEcuC/Com/ComConfig/DCDC_HighVoltConnectionAllowed_oDC1_oE_CAN_7634e78c_Tx, /ActiveEcuC/Com/ComConfig/DCDC_OVERTEMP_oDC1_oE_CAN_9db3c9a4_Tx, /ActiveEcuC/Com/ComConfig/OBC_FrameChksum345_oDC1_oE_CAN_abd913d1_Tx] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DCDC_Fault_oDC1_oE_CAN_80e733c8_Tx] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DCDC_InputVoltage_oDC1_oE_CAN_dc52df4d_Tx] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/DCDC_InputVoltage_oDC1_oE_CAN_dc52df4d_Tx, /ActiveEcuC/Com/ComConfig/DCDC_Status_oDC1_oE_CAN_0062049f_Tx, /ActiveEcuC/Com/ComConfig/OBC_RollingCounter345_oDC1_oE_CAN_16722e64_Tx] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_TxSigGrpInTxIPDU] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_TxSigGrpInTxIPDU] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_TxSigGrpInTxIPDU] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Degug1_0_oDebug1_oInt_CAN_b0849b9e_Tx] */
  /*    17 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_1_oDebug1_oInt_CAN_c4f96638_Tx] */
  /*    18 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_2_oDebug1_oInt_CAN_2e7fbb5a_Tx] */
  /*    19 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_3_oDebug1_oInt_CAN_c12d0dbb_Tx] */
  /*    20 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_4_oDebug1_oInt_CAN_200307df_Tx] */
  /*    21 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_5_oDebug1_oInt_CAN_cf51b13e_Tx] */
  /*    22 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_6_oDebug1_oInt_CAN_25d76c5c_Tx] */
  /*    23 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/Debug1_7_oDebug1_oInt_CAN_ca85dabd_Tx] */
  /*    24 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_0_oDebug2_oInt_CAN_6f352610_Tx] */
  /*    25 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_1_oDebug2_oInt_CAN_806790f1_Tx] */
  /*    26 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_2_oDebug2_oInt_CAN_6ae14d93_Tx] */
  /*    27 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_3_oDebug2_oInt_CAN_85b3fb72_Tx] */
  /*    28 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_4_oDebug2_oInt_CAN_649df116_Tx] */
  /*    29 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_5_oDebug2_oInt_CAN_8bcf47f7_Tx] */
  /*    30 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_6_oDebug2_oInt_CAN_61499a95_Tx] */
  /*    31 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/Debug2_7_oDebug2_oInt_CAN_8e1b2c74_Tx] */
  /*    32 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_0_oDebug3_oInt_CAN_e5907668_Tx] */
  /*    33 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_1_oDebug3_oInt_CAN_0ac2c089_Tx] */
  /*    34 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_2_oDebug3_oInt_CAN_e0441deb_Tx] */
  /*    35 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_3_oDebug3_oInt_CAN_0f16ab0a_Tx] */
  /*    36 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_4_oDebug3_oInt_CAN_ee38a16e_Tx] */
  /*    37 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_5_oDebug3_oInt_CAN_016a178f_Tx] */
  /*    38 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_6_oDebug3_oInt_CAN_ebeccaed_Tx] */
  /*    39 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/Debug3_7_oDebug3_oInt_CAN_04be7c0c_Tx] */
  /*    40 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_0_oDebug4_oInt_CAN_e608cb82_Tx] */
  /*    41 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_1_oDebug4_oInt_CAN_095a7d63_Tx] */
  /*    42 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_2_oDebug4_oInt_CAN_e3dca001_Tx] */
  /*    43 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_3_oDebug4_oInt_CAN_0c8e16e0_Tx] */
  /*    44 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_4_oDebug4_oInt_CAN_eda01c84_Tx] */
  /*    45 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_5_oDebug4_oInt_CAN_02f2aa65_Tx] */
  /*    46 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_6_oDebug4_oInt_CAN_e8747707_Tx] */
  /*    47 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/Debug4_7_oDebug4_oInt_CAN_0726c1e6_Tx] */
  /*    48 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_0_oNEW_JDD_OBC_DCDC_oE_CAN_bfd944fb_Tx] */
  /*    49 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_1_oNEW_JDD_OBC_DCDC_oE_CAN_58c4e26c_Tx] */
  /*    50 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_2_oNEW_JDD_OBC_DCDC_oE_CAN_aa930f94_Tx] */
  /*    51 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_3_oNEW_JDD_OBC_DCDC_oE_CAN_4d8ea903_Tx] */
  /*    52 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_4_oNEW_JDD_OBC_DCDC_oE_CAN_954dd225_Tx] */
  /*    53 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_5_oNEW_JDD_OBC_DCDC_oE_CAN_725074b2_Tx] */
  /* Index        Referable Keys */
  /*    54 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_6_oNEW_JDD_OBC_DCDC_oE_CAN_8007994a_Tx] */
  /*    55 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_BYTE_7_oNEW_JDD_OBC_DCDC_oE_CAN_671a3fdd_Tx] */
  /*    56 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_SocketTempL_oOBC1_oE_CAN_72ea0018_Tx] */
  /*    57 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_SocketTempN_oOBC1_oE_CAN_9384c4b5_Tx] */
  /*    58 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/EVSE_RTAB_STOP_CHARGE_oOBC1_oE_CAN_01a50620_Tx, /ActiveEcuC/Com/ComConfig/OBC_ElockState_oOBC1_oE_CAN_d48080ac_Tx, /ActiveEcuC/Com/ComConfig/OBC_HighVoltConnectionAllowed_oOBC1_oE_CAN_b82322d0_Tx] */
  /*    59 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    60 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_Fault_oOBC1_oE_CAN_cba9590d_Tx] */
  /*    61 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_CP_connection_Status_oOBC1_oE_CAN_bef9ca9d_Tx, /ActiveEcuC/Com/ComConfig/OBC_ChargingMode_oOBC1_oE_CAN_ce17c9da_Tx, /ActiveEcuC/Com/ComConfig/OBC_Status_oOBC1_oE_CAN_ef77955c_Tx] */
  /*    62 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx] */
  /*    63 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_FrameChksum3A3_oOBC1_oE_CAN_12000edd_Tx, /ActiveEcuC/Com/ComConfig/OBC_RollingCounter3A3_oOBC1_oE_CAN_a9b6e2e3_Tx] */
  /*    64 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_OutputVoltage_oOBC2_oE_CAN_afb611f2_Tx] */
  /*    65 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCChargingPlugAConnConf_oOBC2_oE_CAN_ef0cc399_Tx, /ActiveEcuC/Com/ComConfig/OBC_OutputVoltage_oOBC2_oE_CAN_afb611f2_Tx] */
  /*    66 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_OutputCurrent_oOBC2_oE_CAN_c3e935dd_Tx] */
  /*    67 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_ACRange_oOBC2_oE_CAN_1149ad47_Tx, /ActiveEcuC/Com/ComConfig/OBC_CommunicationSt_oOBC2_oE_CAN_e0b8e211_Tx, /ActiveEcuC/Com/ComConfig/OBC_InputVoltageSt_oOBC2_oE_CAN_f750717a_Tx, /ActiveEcuC/Com/ComConfig/OBC_OutputCurrent_oOBC2_oE_CAN_c3e935dd_Tx, /ActiveEcuC/Com/ComConfig/OBC_PlugVoltDetection_oOBC2_oE_CAN_3eae826f_Tx] */
  /*    68 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_OBCTemp_oOBC2_oE_CAN_0c3bcf20_Tx] */
  /*    69 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_PowerMax_oOBC2_oE_CAN_cb72170d_Tx] */
  /*    70 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_FrameChksum3A2_oOBC2_oE_CAN_0180e848_Tx, /ActiveEcuC/Com/ComConfig/OBC_RollingCounter3A2_oOBC2_oE_CAN_ba360476_Tx] */
  /*    71 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_ChargingConnectionConfirmati_oOBC2_oE_CAN_e178031d_Tx, /ActiveEcuC/Com/ComConfig/OBC_OBCStartSt_oOBC2_oE_CAN_5b2bb089_Tx] */
  /*    72 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_CoolingWakeup_oOBC3_oE_CAN_91759696_Tx, /ActiveEcuC/Com/ComConfig/OBC_HVBattRechargeWakeup_oOBC3_oE_CAN_2290bde4_Tx, /ActiveEcuC/Com/ComConfig/OBC_HoldDiscontactorWakeup_oOBC3_oE_CAN_0a4fcf18_Tx, /ActiveEcuC/Com/ComConfig/OBC_PIStateInfoWakeup_oOBC3_oE_CAN_b3c75a3a_Tx] */
  /*    73 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    79 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx] */
  /*    80 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_PushChargeType_oOBC4_oE_CAN_f9102f70_Tx, /ActiveEcuC/Com/ComConfig/OBC_RechargeHMIState_oOBC4_oE_CAN_2e6d800d_Tx] */
  /*    81 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    87 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx] */
  /*    88 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiagData_oOBCTxDiag_oE_CAN_e994d172_Tx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiagData_oOBCTxDiag_oE_CAN_e994d172_Tx] */
  /*    95 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBCTxDiagData_oOBCTxDiag_oE_CAN_e994d172_Tx] */
  /*    96 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/SUPV_DTCRegistred_oSUPV_V2_OBC_DCDC_oE_CAN_04412eaa_Tx, /ActiveEcuC/Com/ComConfig/SUPV_ECUElecStateRCD_oSUPV_V2_OBC_DCDC_oE_CAN_ff7122fa_Tx, /ActiveEcuC/Com/ComConfig/SUPV_RCDLineState_oSUPV_V2_OBC_DCDC_oE_CAN_ab22fee8_Tx] */
  /*    97 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   101 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx] */
  /*   102 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/SUPV_HVBattChargeWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1071a795_Tx, /ActiveEcuC/Com/ComConfig/SUPV_PIStateInfoWupState_oSUPV_V2_OBC_DCDC_oE_CAN_a649d590_Tx, /ActiveEcuC/Com/ComConfig/SUPV_StopDelayedHMIWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1dcc6f45_Tx] */
  /*   103 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/SUPV_CoolingWupState_oSUPV_V2_OBC_DCDC_oE_CAN_addf5c22_Tx, /ActiveEcuC/Com/ComConfig/SUPV_HoldDiscontactorWupState_oSUPV_V2_OBC_DCDC_oE_CAN_b0368fb5_Tx, /ActiveEcuC/Com/ComConfig/SUPV_PostDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_5f77ad32_Tx, /ActiveEcuC/Com/ComConfig/SUPV_PreDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_bfe17473_Tx, /ActiveEcuC/Com/ComConfig/SUPV_PrecondElecWupState_oSUPV_V2_OBC_DCDC_oE_CAN_4523c42d_Tx] */
  /*   104 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/SUP_RequestStatusDCHV_oSUP_CommandToDCHV_oInt_CAN_7bfbedf8_Tx] */
  /*   105 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/DCDC_VoltageReference_oSUP_CommandToDCHV_oInt_CAN_154ec40d_Tx] */
  /*   106 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/DCDC_VoltageReference_oSUP_CommandToDCHV_oInt_CAN_154ec40d_Tx] */
  /*   107 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/DCDC_CurrentReference_oSUP_CommandToDCHV_oInt_CAN_e1358897_Tx] */
  /*   108 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/DCDC_CurrentReference_oSUP_CommandToDCHV_oInt_CAN_e1358897_Tx] */
  /*   109 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   110 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx] */
  /*   111 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/SUP_RollingCounter_4A3_AC_oSUP_CommandToDCHV_oInt_CAN_cc39974d_Tx] */
  /*   112 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/SUP_RequestDCLVStatus_oSUP_CommandToDCLV_oInt_CAN_08473e1f_Tx] */
  /*   113 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/DCLV_VoltageReference_oSUP_CommandToDCLV_oInt_CAN_927eb38f_Tx] */
  /*   114 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/DCLV_Temp_Derating_Factor_oSUP_CommandToDCLV_oInt_CAN_18f05833_Tx] */
  /*   115 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/DCLV_Temp_Derating_Factor_oSUP_CommandToDCLV_oInt_CAN_18f05833_Tx] */
  /*   116 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /*   118 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx] */
  /* Index        Referable Keys */
  /*   119 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/SUP_RollingCounter_495_AC_oSUP_CommandToDCLV_oInt_CAN_39a872f4_Tx] */
  /*   120 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandVDCLink_V_oSUP_CommandToPFC_oInt_CAN_b6bd9cf9_Tx, /ActiveEcuC/Com/ComConfig/SUP_RequestPFCStatus_oSUP_CommandToPFC_oInt_CAN_98b04349_Tx] */
  /*   121 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/SUP_CommandVDCLink_V_oSUP_CommandToPFC_oInt_CAN_b6bd9cf9_Tx] */
  /*   122 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   ... */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   126 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx] */
  /*   127 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/SUP_RollingCounter_402_AC_oSUP_CommandToPFC_oInt_CAN_64ec921b_Tx] */
  /*   128 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/VERSION_SYSTEME_oVERS_OBC_DCDC_oE_CAN_7264d771_Tx] */
  /*   129 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/VERS_DATE2_JOUR_oVERS_OBC_DCDC_oE_CAN_526ac526_Tx] */
  /*   130 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/VERS_DATE2_MOIS_oVERS_OBC_DCDC_oE_CAN_d5ba8908_Tx] */
  /*   131 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/VERS_DATE2_ANNEE_oVERS_OBC_DCDC_oE_CAN_258c1038_Tx] */
  /*   132 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/VERSION_APPLI_oVERS_OBC_DCDC_oE_CAN_1774b9bd_Tx] */
  /*   133 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/VERSION_SOFT_oVERS_OBC_DCDC_oE_CAN_dcc20268_Tx] */
  /*   134 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/EDITION_SOFT_oVERS_OBC_DCDC_oE_CAN_bde5b057_Tx] */
  /*   135 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/EDITION_CALIB_oVERS_OBC_DCDC_oE_CAN_20fa071e_Tx] */
  /*   136 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCDCDCRTPowerLoad_oDC2_oE_CAN_e771c64f_Tx] */
  /*   137 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*   138 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx] */
  /*   139 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_ActivedischargeSt_oDC2_oE_CAN_5a2849b9_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCMainContactorReq_oDC2_oE_CAN_40cd3485_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OBCQuickChargeContactorReq_oDC2_oE_CAN_2093b4ef_Tx] */
  /*   140 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_InputCurrent_oDC2_oE_CAN_0b6c1afa_Tx] */
  /*   141 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OutputCurrent_oDC2_oE_CAN_6652af17_Tx] */
  /*   142 */  /* [/ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx, /ActiveEcuC/Com/ComConfig/SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx/DCDC_OutputCurrent_oDC2_oE_CAN_6652af17_Tx] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxCyclicProcessingISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxCyclicProcessingISRLockCounterType, COM_VAR_NOINIT) Com_TxCyclicProcessingISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxDeadlineMonitoringISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxDeadlineMonitoringISRLockCounterType, COM_VAR_NOINIT) Com_TxDeadlineMonitoringISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxDlMonDivisorCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxDlMonDivisorCounterType, COM_VAR_NOINIT) Com_TxDlMonDivisorCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxIPduGroupISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxIPduGroupISRLockCounterType, COM_VAR_NOINIT) Com_TxIPduGroupISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxPduGrpActive
**********************************************************************************************************************/
/** 
  \var    Com_TxPduGrpActive
  \brief  Tx I-PDU based state (started/stopped) of the corresponding I-PDU-Group.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxPduGrpActiveType, COM_VAR_NOINIT) Com_TxPduGrpActive[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxProcessingISRLockCounter
**********************************************************************************************************************/
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxProcessingISRLockCounterType, COM_VAR_NOINIT) Com_TxProcessingISRLockCounter;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxSduLength
**********************************************************************************************************************/
/** 
  \var    Com_TxSduLength
  \brief  This var Array contains the Com Ipdu Length.
*/ 
#define COM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxSduLengthType, COM_VAR_NOINIT) Com_TxSduLength[17];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC1_oE_CAN_fcac9ee4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     1 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     2 */  /* [/ActiveEcuC/Com/ComConfig/Debug1_oInt_CAN_177fd136_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     3 */  /* [/ActiveEcuC/Com/ComConfig/Debug2_oInt_CAN_fc486a35_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     4 */  /* [/ActiveEcuC/Com/ComConfig/Debug3_oInt_CAN_138a010b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     5 */  /* [/ActiveEcuC/Com/ComConfig/Debug4_oInt_CAN_f1561a72_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*     6 */  /* [/ActiveEcuC/Com/ComConfig/NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     7 */  /* [/ActiveEcuC/Com/ComConfig/OBC1_oE_CAN_89fd90e2_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     8 */  /* [/ActiveEcuC/Com/ComConfig/OBC2_oE_CAN_07729701_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*     9 */  /* [/ActiveEcuC/Com/ComConfig/OBC3_oE_CAN_cbd8979f_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    10 */  /* [/ActiveEcuC/Com/ComConfig/OBC4_oE_CAN_c11d9e86_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    11 */  /* [/ActiveEcuC/Com/ComConfig/OBCTxDiag_oE_CAN_98fb5e1b_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    12 */  /* [/ActiveEcuC/Com/ComConfig/SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */
  /*    13 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    14 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    15 */  /* [/ActiveEcuC/Com/ComConfig/SUP_CommandToPFC_oInt_CAN_d9411c97_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    16 */  /* [/ActiveEcuC/Com/ComConfig/VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx, /ActiveEcuC/Com/ComConfig/OBC_DCDC_oE_CAN_Tx_11676862] */

#define COM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Com_TxTOutCnt
**********************************************************************************************************************/
/** 
  \var    Com_TxTOutCnt
  \brief  This array holds timeout counters for all Tx I-PDUs with a configured I-PDU based deadline monitoring.
*/ 
#define COM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Com_TxTOutCntUType, COM_VAR_NOINIT) Com_TxTOutCnt;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Com/ComConfig/DC2_oE_CAN_72239907_Tx] */

#define COM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL FUNCTION PROTOTYPES
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL FUNCTIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/






/**********************************************************************************************************************
  END OF FILE: Com_Lcfg.c
**********************************************************************************************************************/

