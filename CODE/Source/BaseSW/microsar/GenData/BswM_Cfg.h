/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: BswM
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswM_Cfg.h
 *   Generation Time: 2020-08-19 13:07:46
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


#if !defined(BSWM_CFG_H)
#define BSWM_CFG_H

/* -----------------------------------------------------------------------------
    &&&~ INCLUDE
 ----------------------------------------------------------------------------- */
#include "Std_Types.h"
#include "ComStack_Types.h" 
#include "BswM_ComM.h"
#include "BswM_CanSM.h"
#include "BswM_EthIf.h"
#include "BswM_EthSM.h"
#include "BswM_Dcm.h"
#include "BswM_EcuM.h"
#include "BswM_NvM.h"
#include "Rte_BswM_Type.h"
#include "Com_Types.h" 
#include "EcuM_Generated_Types.h" 
#include "NvM_Pre_Init.h" 






/* -----------------------------------------------------------------------------
    &&&~ GENERAL DEFINES
 ----------------------------------------------------------------------------- */
#ifndef BSWM_DEV_ERROR_DETECT
#define BSWM_DEV_ERROR_DETECT STD_OFF
#endif
#ifndef BSWM_DEV_ERROR_REPORT
#define BSWM_DEV_ERROR_REPORT STD_OFF
#endif
#ifndef BSWM_USE_DUMMY_STATEMENT
#define BSWM_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef BSWM_DUMMY_STATEMENT
#define BSWM_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef BSWM_DUMMY_STATEMENT_CONST
#define BSWM_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef BSWM_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define BSWM_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef BSWM_ATOMIC_VARIABLE_ACCESS
#define BSWM_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef BSWM_PROCESSOR_TC234
#define BSWM_PROCESSOR_TC234
#endif
#ifndef BSWM_COMP_GNU
#define BSWM_COMP_GNU
#endif
#ifndef BSWM_GEN_GENERATOR_MSR
#define BSWM_GEN_GENERATOR_MSR
#endif
#ifndef BSWM_CPUTYPE_BITORDER_LSB2MSB
#define BSWM_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef BSWM_CONFIGURATION_VARIANT_PRECOMPILE
#define BSWM_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef BSWM_CONFIGURATION_VARIANT_LINKTIME
#define BSWM_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef BSWM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define BSWM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef BSWM_CONFIGURATION_VARIANT
#define BSWM_CONFIGURATION_VARIANT BSWM_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef BSWM_POSTBUILD_VARIANT_SUPPORT
#define BSWM_POSTBUILD_VARIANT_SUPPORT STD_ON
#endif


#if !defined (BSWM_DUMMY_STATEMENT)
# define BSWM_DUMMY_STATEMENT(statement) (void)statement
#endif

/* -----------------------------------------------------------------------------
    &&&~ CONFIGURATION DEFINES
 ----------------------------------------------------------------------------- */

/* START of Checksum include for */
/* START of Checksum include for - SysService_Asr4BswMCfg5PrecompileCRC */

#define BSWM_MODE_CHECK                      STD_ON
#define BSWM_ENABLE_CANSM                    STD_ON
#define BSWM_ENABLE_FRSM                     STD_OFF
#define BSWM_ENABLE_LINSM                    STD_OFF
#define BSWM_ENABLE_ETHIF                    STD_ON
#define BSWM_ENABLE_ETHSM                    STD_ON
#define BSWM_ENABLE_LINTP                    STD_OFF
#define BSWM_ENABLE_DCM                      STD_ON
#define BSWM_ENABLE_NVM                      STD_ON
#define BSWM_ENABLE_ECUM                     STD_ON
#define BSWM_ENABLE_COMM                     STD_ON
#define BSWM_ENABLE_J1939DCM                 STD_OFF
#define BSWM_ENABLE_J1939NM                  STD_OFF
#define BSWM_ENABLE_SD                       STD_OFF
#define BSWM_ENABLE_NM                       STD_OFF
#define BSWM_ENABLE_PDUR                     STD_OFF
#define BSWM_ENABLE_WDGM                     STD_ON
#define BSWM_ENABLE_RULE_CONTROL             STD_OFF
#define BSWM_VERSION_INFO_API                STD_OFF
#define BSWM_COMM_PNC_SUPPORT                STD_OFF
#define BSWM_COMM_INITIATE_RESET             STD_OFF
#define BSWM_CHANNEL_COUNT                   3u
#define BSWM_WAKEUP_SOURCE_COUNT             7u
#define BSWM_IPDU_GROUP_CONTROL              STD_ON
#define BSWM_ECUM_MODE_HANDLING              STD_ON
#define BSWM_IPDUGROUPVECTORSIZE             1u
#define BSWM_MULTIPARTITION                  STD_OFF



/* END of Checksum include for - SysService_Asr4BswMCfg5PrecompileCRC */

/* END of Checksum include for */

/* -----------------------------------------------------------------------------
    &&&~ RULE DEFINES
 ----------------------------------------------------------------------------- */
#define BswMConf_BswMRule_BswMRule_ProgReset (0) 
#define BswMConf_BswMRule_CC_CN_E_CAN_7f812c72_RX (1) 
#define BswMConf_BswMRule_CC_CN_E_CAN_7f812c72_RX_DM (2) 
#define BswMConf_BswMRule_CC_CN_E_CAN_7f812c72_TX (3) 
#define BswMConf_BswMRule_CC_CN_Int_CAN_b597612f_RX (4) 
#define BswMConf_BswMRule_CC_CN_Int_CAN_b597612f_RX_DM (5) 
#define BswMConf_BswMRule_CC_CN_Int_CAN_b597612f_TX (6) 
#define BswMConf_BswMRule_ESH_InitToWakeup (7) 
#define BswMConf_BswMRule_ESH_PostRun (8) 
#define BswMConf_BswMRule_ESH_PostRunNested (9) 
#define BswMConf_BswMRule_ESH_PostRunToPrepNested (10) 
#define BswMConf_BswMRule_ESH_PrepToWait (11) 
#define BswMConf_BswMRule_ESH_RunToPostRun (12) 
#define BswMConf_BswMRule_ESH_WaitToShutdown (13) 
#define BswMConf_BswMRule_ESH_WaitToWakeup (14) 
#define BswMConf_BswMRule_ESH_WakeupToPrep (15) 
#define BswMConf_BswMRule_ESH_WakeupToRun (16) 


/* -----------------------------------------------------------------------------
    &&&~ GENERIC DEFINES
 ----------------------------------------------------------------------------- */
#define BSWM_GENERIC_ESH_State 230u 

#define BSWM_GENERICVALUE_ESH_State_ESH_INIT          0x0000u 
#define BSWM_GENERICVALUE_ESH_State_ESH_POST_RUN      0x0002u 
#define BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN 0x0003u 
#define BSWM_GENERICVALUE_ESH_State_ESH_RUN           0x0001u 
#define BSWM_GENERICVALUE_ESH_State_ESH_SHUTDOWN      0x0005u 
#define BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM  0x0004u 
#define BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP        0x0006u 


/* -----------------------------------------------------------------------------
    &&&~ TIMER DEFINES
 ----------------------------------------------------------------------------- */
#define BSWM_TMR_ESH_NvM_CancelWriteAllTimer_Left 0u 
#define BSWM_TMR_ESH_NvM_WriteAllTimer_Left       1u 
#define BSWM_TMR_ESH_SelfRunRequestTimer_Left     2u 



/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCDataSwitches  BswM Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define BSWM_ACTIONITEMS                                                                            STD_ON
#define BSWM_ABORTOFACTIONITEMS                                                                     STD_OFF  /**< Deactivateable: 'BswM_ActionItems.Abort' Reason: 'the value of BswM_AbortOfActionItems is always 'false' due to this, the array is deactivated.' */
#define BSWM_ACTIONSIDXOFACTIONITEMS                                                                STD_ON
#define BSWM_PARAMETERIDXOFACTIONITEMS                                                              STD_ON
#define BSWM_PARAMETERUSEDOFACTIONITEMS                                                             STD_ON
#define BSWM_REPORTOFACTIONITEMS                                                                    STD_OFF  /**< Deactivateable: 'BswM_ActionItems.Report' Reason: 'No Dem event is configured.' */
#define BSWM_ACTIONLISTPRIORITYQUEUE                                                                STD_OFF  /**< Deactivateable: 'BswM_ActionListPriorityQueue' Reason: 'Action List Queue Search Algorithm is not equal to PRIORITY_QUEUE' */
#define BSWM_ACTIONLISTQUEUE                                                                        STD_ON
#define BSWM_ACTIONLISTS                                                                            STD_ON
#define BSWM_ACTIONITEMSENDIDXOFACTIONLISTS                                                         STD_ON
#define BSWM_ACTIONITEMSSTARTIDXOFACTIONLISTS                                                       STD_ON
#define BSWM_ACTIONITEMSUSEDOFACTIONLISTS                                                           STD_ON
#define BSWM_CONDITIONALOFACTIONLISTS                                                               STD_ON
#define BSWM_MASKEDBITSOFACTIONLISTS                                                                STD_ON
#define BSWM_ACTIONS                                                                                STD_ON
#define BSWM_CANSMCHANNELMAPPING                                                                    STD_ON
#define BSWM_EXTERNALIDOFCANSMCHANNELMAPPING                                                        STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                               STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                             STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFCANSMCHANNELMAPPING                                                 STD_ON
#define BSWM_INITVALUEOFCANSMCHANNELMAPPING                                                         STD_ON
#define BSWM_CANSMCHANNELSTATE                                                                      STD_ON
#define BSWM_COMDMCONTROLPARAMETERS                                                                 STD_ON
#define BSWM_COMDMCONTROLSUBPARAMETERSENDIDXOFCOMDMCONTROLPARAMETERS                                STD_ON
#define BSWM_COMDMCONTROLSUBPARAMETERSSTARTIDXOFCOMDMCONTROLPARAMETERS                              STD_ON
#define BSWM_COMDMCONTROLSUBPARAMETERSUSEDOFCOMDMCONTROLPARAMETERS                                  STD_ON
#define BSWM_COMDMCONTROLSUBPARAMETERS                                                              STD_ON
#define BSWM_BITVALOFCOMDMCONTROLSUBPARAMETERS                                                      STD_ON
#define BSWM_IPDUGROUPIDOFCOMDMCONTROLSUBPARAMETERS                                                 STD_ON
#define BSWM_COMDMHANDLINGDISABLEPARAMETERS                                                         STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingDisableParameters' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_IPDUGROUPIDOFCOMDMHANDLINGDISABLEPARAMETERS                                            STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingDisableParameters.IpduGroupId' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGENABLEPARAMETERS                                                          STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingEnableParameters' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_IPDUGROUPIDOFCOMDMHANDLINGENABLEPARAMETERS                                             STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingEnableParameters.IpduGroupId' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGPARAMETERS                                                                STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGDISABLEPARAMETERSENDIDXOFCOMDMHANDLINGPARAMETERS                          STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters.ComDMHandlingDisableParametersEndIdx' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGDISABLEPARAMETERSSTARTIDXOFCOMDMHANDLINGPARAMETERS                        STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters.ComDMHandlingDisableParametersStartIdx' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGDISABLEPARAMETERSUSEDOFCOMDMHANDLINGPARAMETERS                            STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters.ComDMHandlingDisableParametersUsed' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGENABLEPARAMETERSENDIDXOFCOMDMHANDLINGPARAMETERS                           STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters.ComDMHandlingEnableParametersEndIdx' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGENABLEPARAMETERSSTARTIDXOFCOMDMHANDLINGPARAMETERS                         STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters.ComDMHandlingEnableParametersStartIdx' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMDMHANDLINGENABLEPARAMETERSUSEDOFCOMDMHANDLINGPARAMETERS                             STD_OFF  /**< Deactivateable: 'BswM_ComDMHandlingParameters.ComDMHandlingEnableParametersUsed' Reason: 'Action of type 'ComDMHandling' not in configuration.' */
#define BSWM_COMMALLOWCOMPARAMETERS                                                                 STD_OFF  /**< Deactivateable: 'BswM_ComMAllowComParameters' Reason: 'Action of type 'ComMAllowCom' not in configuration.' */
#define BSWM_ALLOWEDOFCOMMALLOWCOMPARAMETERS                                                        STD_OFF  /**< Deactivateable: 'BswM_ComMAllowComParameters.Allowed' Reason: 'Action of type 'ComMAllowCom' not in configuration.' */
#define BSWM_CHANNELOFCOMMALLOWCOMPARAMETERS                                                        STD_OFF  /**< Deactivateable: 'BswM_ComMAllowComParameters.Channel' Reason: 'Action of type 'ComMAllowCom' not in configuration.' */
#define BSWM_COMMCHANNELMAPPING                                                                     STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping' Reason: 'No Mode Request for BswMComMIndication configured.' */
#define BSWM_EXTERNALIDOFCOMMCHANNELMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ExternalId' Reason: 'No Mode Request for BswMComMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFCOMMCHANNELMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMComMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFCOMMCHANNELMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMComMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFCOMMCHANNELMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMComMIndication configured.' */
#define BSWM_INITVALUEOFCOMMCHANNELMAPPING                                                          STD_OFF  /**< Deactivateable: 'BswM_ComMChannelMapping.InitValue' Reason: 'No Mode Request for BswMComMIndication configured.' */
#define BSWM_COMMCHANNELSTATE                                                                       STD_OFF  /**< Deactivateable: 'BswM_ComMChannelState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_COMMINITIATERESETMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFCOMMINITIATERESETMAPPING                                          STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFCOMMINITIATERESETMAPPING                                        STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFCOMMINITIATERESETMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_INITVALUEOFCOMMINITIATERESETMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetMapping.InitValue' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_COMMINITIATERESETSTATE                                                                 STD_OFF  /**< Deactivateable: 'BswM_ComMInitiateResetState' Reason: 'No Mode Request for BswMComMInitiateReset configured.' */
#define BSWM_COMMMODELIMITATIONPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_ComMModeLimitationParameters' Reason: 'Action of type 'ComMModeLimitation' not in configuration.' */
#define BSWM_CHANNELOFCOMMMODELIMITATIONPARAMETERS                                                  STD_OFF  /**< Deactivateable: 'BswM_ComMModeLimitationParameters.Channel' Reason: 'Action of type 'ComMModeLimitation' not in configuration.' */
#define BSWM_STATUSOFCOMMMODELIMITATIONPARAMETERS                                                   STD_OFF  /**< Deactivateable: 'BswM_ComMModeLimitationParameters.Status' Reason: 'Action of type 'ComMModeLimitation' not in configuration.' */
#define BSWM_COMMMODESWITCHPARAMETERS                                                               STD_OFF  /**< Deactivateable: 'BswM_ComMModeSwitchParameters' Reason: 'Action of type 'ComMModeSwitch' not in configuration.' */
#define BSWM_COMMODEOFCOMMMODESWITCHPARAMETERS                                                      STD_OFF  /**< Deactivateable: 'BswM_ComMModeSwitchParameters.ComMode' Reason: 'Action of type 'ComMModeSwitch' not in configuration.' */
#define BSWM_USEROFCOMMMODESWITCHPARAMETERS                                                         STD_OFF  /**< Deactivateable: 'BswM_ComMModeSwitchParameters.User' Reason: 'Action of type 'ComMModeSwitch' not in configuration.' */
#define BSWM_COMMPNCMAPPING                                                                         STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_EXTERNALIDOFCOMMPNCMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ExternalId' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFCOMMPNCMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFCOMMPNCMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFCOMMPNCMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_INITVALUEOFCOMMPNCMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_ComMPncMapping.InitValue' Reason: 'No Mode Request for BswMComMPncRequest configured.' */
#define BSWM_COMMPNCSTATE                                                                           STD_OFF  /**< Deactivateable: 'BswM_ComMPncState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_COMPDUGROUPHANDLINGPARAMETERS                                                          STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTARTPARAMETERSENDIDXOFCOMPDUGROUPHANDLINGPARAMETERS                STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters.ComPduGroupHandlingStartParametersEndIdx' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTARTPARAMETERSSTARTIDXOFCOMPDUGROUPHANDLINGPARAMETERS              STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters.ComPduGroupHandlingStartParametersStartIdx' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTARTPARAMETERSUSEDOFCOMPDUGROUPHANDLINGPARAMETERS                  STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters.ComPduGroupHandlingStartParametersUsed' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTOPPARAMETERSENDIDXOFCOMPDUGROUPHANDLINGPARAMETERS                 STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters.ComPduGroupHandlingStopParametersEndIdx' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTOPPARAMETERSSTARTIDXOFCOMPDUGROUPHANDLINGPARAMETERS               STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters.ComPduGroupHandlingStopParametersStartIdx' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTOPPARAMETERSUSEDOFCOMPDUGROUPHANDLINGPARAMETERS                   STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingParameters.ComPduGroupHandlingStopParametersUsed' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTARTPARAMETERS                                                     STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingStartParameters' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_INITIALIZEOFCOMPDUGROUPHANDLINGSTARTPARAMETERS                                         STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingStartParameters.Initialize' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_IPDUGROUPIDOFCOMPDUGROUPHANDLINGSTARTPARAMETERS                                        STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingStartParameters.IpduGroupId' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPHANDLINGSTOPPARAMETERS                                                      STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingStopParameters' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_IPDUGROUPIDOFCOMPDUGROUPHANDLINGSTOPPARAMETERS                                         STD_OFF  /**< Deactivateable: 'BswM_ComPduGroupHandlingStopParameters.IpduGroupId' Reason: 'Action of type 'ComPduGroupHandling' not in configuration.' */
#define BSWM_COMPDUGROUPSWITCHPARAMETERS                                                            STD_ON
#define BSWM_COMPDUGROUPSWITCHSUBPARAMETERSENDIDXOFCOMPDUGROUPSWITCHPARAMETERS                      STD_ON
#define BSWM_COMPDUGROUPSWITCHSUBPARAMETERSSTARTIDXOFCOMPDUGROUPSWITCHPARAMETERS                    STD_ON
#define BSWM_COMPDUGROUPSWITCHSUBPARAMETERSUSEDOFCOMPDUGROUPSWITCHPARAMETERS                        STD_ON
#define BSWM_CONTROLOFCOMPDUGROUPSWITCHPARAMETERS                                                   STD_ON
#define BSWM_COMPDUGROUPSWITCHSUBPARAMETERS                                                         STD_ON
#define BSWM_BITVALOFCOMPDUGROUPSWITCHSUBPARAMETERS                                                 STD_ON
#define BSWM_IPDUGROUPIDOFCOMPDUGROUPSWITCHSUBPARAMETERS                                            STD_ON
#define BSWM_COMSWITCHIPDUMODEPARAMETERS                                                            STD_OFF  /**< Deactivateable: 'BswM_ComSwitchIPduModeParameters' Reason: 'Action of type 'ComSwitchIPduMode' not in configuration.' */
#define BSWM_MODEOFCOMSWITCHIPDUMODEPARAMETERS                                                      STD_OFF  /**< Deactivateable: 'BswM_ComSwitchIPduModeParameters.Mode' Reason: 'Action of type 'ComSwitchIPduMode' not in configuration.' */
#define BSWM_PDUIDOFCOMSWITCHIPDUMODEPARAMETERS                                                     STD_OFF  /**< Deactivateable: 'BswM_ComSwitchIPduModeParameters.PduId' Reason: 'Action of type 'ComSwitchIPduMode' not in configuration.' */
#define BSWM_COMTRIGGERIPDUSENDPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_ComTriggerIPduSendParameters' Reason: 'Action of type 'ComTriggerIPduSend' not in configuration.' */
#define BSWM_COMTRIGGERIPDUSENDSUBPARAMETERSENDIDXOFCOMTRIGGERIPDUSENDPARAMETERS                    STD_OFF  /**< Deactivateable: 'BswM_ComTriggerIPduSendParameters.ComTriggerIPduSendSubParametersEndIdx' Reason: 'Action of type 'ComTriggerIPduSend' not in configuration.' */
#define BSWM_COMTRIGGERIPDUSENDSUBPARAMETERSSTARTIDXOFCOMTRIGGERIPDUSENDPARAMETERS                  STD_OFF  /**< Deactivateable: 'BswM_ComTriggerIPduSendParameters.ComTriggerIPduSendSubParametersStartIdx' Reason: 'Action of type 'ComTriggerIPduSend' not in configuration.' */
#define BSWM_COMTRIGGERIPDUSENDSUBPARAMETERSUSEDOFCOMTRIGGERIPDUSENDPARAMETERS                      STD_OFF  /**< Deactivateable: 'BswM_ComTriggerIPduSendParameters.ComTriggerIPduSendSubParametersUsed' Reason: 'Action of type 'ComTriggerIPduSend' not in configuration.' */
#define BSWM_COMTRIGGERIPDUSENDSUBPARAMETERS                                                        STD_OFF  /**< Deactivateable: 'BswM_ComTriggerIPduSendSubParameters' Reason: 'Action of type 'ComTriggerIPduSend' not in configuration.' */
#define BSWM_PDUIDOFCOMTRIGGERIPDUSENDSUBPARAMETERS                                                 STD_OFF  /**< Deactivateable: 'BswM_ComTriggerIPduSendSubParameters.PduId' Reason: 'Action of type 'ComTriggerIPduSend' not in configuration.' */
#define BSWM_DCMAPPLUPDATEMAPPING                                                                   STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFDCMAPPLUPDATEMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFDCMAPPLUPDATEMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFDCMAPPLUPDATEMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_INITVALUEOFDCMAPPLUPDATEMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateMapping.InitValue' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_DCMAPPLUPDATESTATE                                                                     STD_OFF  /**< Deactivateable: 'BswM_DcmApplUpdateState' Reason: 'No Mode Request for BswMDcmApplicationUpdatedIndication configured.' */
#define BSWM_DCMCOMMAPPING                                                                          STD_OFF  /**< Deactivateable: 'BswM_DcmComMapping' Reason: 'No Mode Request for BswMDcmComModeRequest configured.' */
#define BSWM_EXTERNALIDOFDCMCOMMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_DcmComMapping.ExternalId' Reason: 'No Mode Request for BswMDcmComModeRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFDCMCOMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_DcmComMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMDcmComModeRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFDCMCOMMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_DcmComMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMDcmComModeRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFDCMCOMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_DcmComMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMDcmComModeRequest configured.' */
#define BSWM_INITVALUEOFDCMCOMMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_DcmComMapping.InitValue' Reason: 'No Mode Request for BswMDcmComModeRequest configured.' */
#define BSWM_DCMCOMSTATE                                                                            STD_OFF  /**< Deactivateable: 'BswM_DcmComState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_DEFERREDRULES                                                                          STD_ON
#define BSWM_RULESIDXOFDEFERREDRULES                                                                STD_ON
#define BSWM_ECUMMODEMAPPING                                                                        STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFECUMMODEMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfEcuMModeMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFECUMMODEMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfEcuMModeMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFECUMMODEMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_EcuMModeMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfEcuMModeMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_INITVALUEOFECUMMODEMAPPING                                                             STD_ON
#define BSWM_ECUMMODESTATE                                                                          STD_ON
#define BSWM_ECUMRUNREQUESTMAPPING                                                                  STD_ON
#define BSWM_EXTERNALIDOFECUMRUNREQUESTMAPPING                                                      STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFECUMRUNREQUESTMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfEcuMRunRequestMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFECUMRUNREQUESTMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfEcuMRunRequestMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFECUMRUNREQUESTMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_EcuMRunRequestMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfEcuMRunRequestMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_INITVALUEOFECUMRUNREQUESTMAPPING                                                       STD_ON
#define BSWM_ECUMRUNREQUESTSTATE                                                                    STD_ON
#define BSWM_ECUMSELECTSHUTDOWNTARGETPARAMETERS                                                     STD_ON
#define BSWM_RESETSLEEPMODEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                                     STD_ON
#define BSWM_TARGETSTATEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                                        STD_ON
#define BSWM_ECUMSTATESWITCHPARAMETERS                                                              STD_ON
#define BSWM_TARGETSTATEOFECUMSTATESWITCHPARAMETERS                                                 STD_ON
#define BSWM_ECUMWAKEUPMAPPING                                                                      STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_EXTERNALIDOFECUMWAKEUPMAPPING                                                          STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ExternalId' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFECUMWAKEUPMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFECUMWAKEUPMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFECUMWAKEUPMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_INITVALUEOFECUMWAKEUPMAPPING                                                           STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupMapping.InitValue' Reason: 'No Mode Request for BswMEcuMWakeupSource configured.' */
#define BSWM_ECUMWAKEUPSTATE                                                                        STD_OFF  /**< Deactivateable: 'BswM_EcuMWakeupState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_ETHIFPORTMAPPING                                                                       STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_EXTERNALIDOFETHIFPORTMAPPING                                                           STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ExternalId' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFETHIFPORTMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFETHIFPORTMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFETHIFPORTMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_INITVALUEOFETHIFPORTMAPPING                                                            STD_OFF  /**< Deactivateable: 'BswM_EthIfPortMapping.InitValue' Reason: 'No Mode Request for BswMEthIfPortGroupLinkStateChg configured.' */
#define BSWM_ETHIFPORTSTATE                                                                         STD_OFF  /**< Deactivateable: 'BswM_EthIfPortState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_ETHSMMAPPING                                                                           STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_EXTERNALIDOFETHSMMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ExternalId' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFETHSMMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFETHSMMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFETHSMMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_INITVALUEOFETHSMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_EthSMMapping.InitValue' Reason: 'No Mode Request for BswMEthSMIndication configured.' */
#define BSWM_ETHSMSTATE                                                                             STD_OFF  /**< Deactivateable: 'BswM_EthSMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_EXPRESSIONS                                                                            STD_ON
#define BSWM_FINALMAGICNUMBER                                                                       STD_OFF  /**< Deactivateable: 'BswM_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_FORCEDACTIONLISTPRIORITY                                                               STD_ON
#define BSWM_FRSMMAPPING                                                                            STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_EXTERNALIDOFFRSMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ExternalId' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFFRSMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFFRSMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFFRSMMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_INITVALUEOFFRSMMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_FrSMMapping.InitValue' Reason: 'No Mode Request for BswMFrSMIndication configured.' */
#define BSWM_FRSMSTATE                                                                              STD_OFF  /**< Deactivateable: 'BswM_FrSMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_GENERICMAPPING                                                                         STD_ON
#define BSWM_EXTERNALIDOFGENERICMAPPING                                                             STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                                    STD_ON
#define BSWM_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                                  STD_ON
#define BSWM_IMMEDIATEUSERUSEDOFGENERICMAPPING                                                      STD_ON
#define BSWM_INITVALUEOFGENERICMAPPING                                                              STD_ON
#define BSWM_GENERICMODEPARAMETERS                                                                  STD_ON
#define BSWM_MODEOFGENERICMODEPARAMETERS                                                            STD_ON
#define BSWM_USEROFGENERICMODEPARAMETERS                                                            STD_ON
#define BSWM_GENERICMODEREFPARAMETERS                                                               STD_OFF  /**< Deactivateable: 'BswM_GenericModeRefParameters' Reason: 'Action of type 'GenericModeRef' not in configuration.' */
#define BSWM_MODEOFGENERICMODEREFPARAMETERS                                                         STD_OFF  /**< Deactivateable: 'BswM_GenericModeRefParameters.Mode' Reason: 'Action of type 'GenericModeRef' not in configuration.' */
#define BSWM_USEROFGENERICMODEREFPARAMETERS                                                         STD_OFF  /**< Deactivateable: 'BswM_GenericModeRefParameters.User' Reason: 'Action of type 'GenericModeRef' not in configuration.' */
#define BSWM_GENERICSTATE                                                                           STD_ON
#define BSWM_IMMEDIATEUSER                                                                          STD_ON
#define BSWM_FORCEDOFIMMEDIATEUSER                                                                  STD_OFF  /**< Deactivateable: 'BswM_ImmediateUser.Forced' Reason: 'the value of BswM_ForcedOfImmediateUser is always 'false' due to this, the array is deactivated.' */
#define BSWM_MASKEDBITSOFIMMEDIATEUSER                                                              STD_ON
#define BSWM_ONINITOFIMMEDIATEUSER                                                                  STD_ON
#define BSWM_RULESINDENDIDXOFIMMEDIATEUSER                                                          STD_ON
#define BSWM_RULESINDSTARTIDXOFIMMEDIATEUSER                                                        STD_ON
#define BSWM_RULESINDUSEDOFIMMEDIATEUSER                                                            STD_ON
#define BSWM_INITACTIONLISTS                                                                        STD_ON
#define BSWM_INITDATAHASHCODE                                                                       STD_OFF  /**< Deactivateable: 'BswM_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_INITGENVARANDINITAL                                                                    STD_ON
#define BSWM_INITVALUES                                                                             STD_ON
#define BSWM_INITIALIZED                                                                            STD_ON
#define BSWM_J1939DCMMAPPING                                                                        STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFJ1939DCMMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFJ1939DCMMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFJ1939DCMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_INITVALUEOFJ1939DCMMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_J1939DcmMapping.InitValue' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_J1939DCMSTATE                                                                          STD_OFF  /**< Deactivateable: 'BswM_J1939DcmState' Reason: 'No Mode Request for BswMJ1939DcmBroadcastStatus configured.' */
#define BSWM_J1939DCMSTATEPARAMETERS                                                                STD_OFF  /**< Deactivateable: 'BswM_J1939DcmStateParameters' Reason: 'Action of type 'J1939DcmState' not in configuration.' */
#define BSWM_NETWORKOFJ1939DCMSTATEPARAMETERS                                                       STD_OFF  /**< Deactivateable: 'BswM_J1939DcmStateParameters.Network' Reason: 'Action of type 'J1939DcmState' not in configuration.' */
#define BSWM_NODEOFJ1939DCMSTATEPARAMETERS                                                          STD_OFF  /**< Deactivateable: 'BswM_J1939DcmStateParameters.Node' Reason: 'Action of type 'J1939DcmState' not in configuration.' */
#define BSWM_STATEOFJ1939DCMSTATEPARAMETERS                                                         STD_OFF  /**< Deactivateable: 'BswM_J1939DcmStateParameters.State' Reason: 'Action of type 'J1939DcmState' not in configuration.' */
#define BSWM_J1939NMMAPPING                                                                         STD_OFF  /**< Deactivateable: 'BswM_J1939NmMapping' Reason: 'No Mode Request for BswMJ1939NmIndication configured.' */
#define BSWM_EXTERNALIDOFJ1939NMMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_J1939NmMapping.ExternalId' Reason: 'No Mode Request for BswMJ1939NmIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFJ1939NMMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_J1939NmMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMJ1939NmIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFJ1939NMMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_J1939NmMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMJ1939NmIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFJ1939NMMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_J1939NmMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMJ1939NmIndication configured.' */
#define BSWM_INITVALUEOFJ1939NMMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_J1939NmMapping.InitValue' Reason: 'No Mode Request for BswMJ1939NmIndication configured.' */
#define BSWM_J1939NMSTATE                                                                           STD_OFF  /**< Deactivateable: 'BswM_J1939NmState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_J1939RMSTATEPARAMETERS                                                                 STD_OFF  /**< Deactivateable: 'BswM_J1939RmStateParameters' Reason: 'Action of type 'J1939RmState' not in configuration.' */
#define BSWM_NETWORKOFJ1939RMSTATEPARAMETERS                                                        STD_OFF  /**< Deactivateable: 'BswM_J1939RmStateParameters.Network' Reason: 'Action of type 'J1939RmState' not in configuration.' */
#define BSWM_NODEOFJ1939RMSTATEPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_J1939RmStateParameters.Node' Reason: 'Action of type 'J1939RmState' not in configuration.' */
#define BSWM_STATEOFJ1939RMSTATEPARAMETERS                                                          STD_OFF  /**< Deactivateable: 'BswM_J1939RmStateParameters.State' Reason: 'Action of type 'J1939RmState' not in configuration.' */
#define BSWM_LENGTHOFACTIONLISTPRIORITYQUEUE                                                        STD_OFF  /**< Deactivateable: 'BswM_LengthOfActionListPriorityQueue' Reason: 'ActionListPriorityQueue not enabled' */
#define BSWM_LINSMMAPPING                                                                           STD_OFF  /**< Deactivateable: 'BswM_LinSMMapping' Reason: 'No Mode Request for BswMLinSMIndication configured.' */
#define BSWM_EXTERNALIDOFLINSMMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_LinSMMapping.ExternalId' Reason: 'No Mode Request for BswMLinSMIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFLINSMMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_LinSMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMLinSMIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINSMMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_LinSMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMLinSMIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFLINSMMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_LinSMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMLinSMIndication configured.' */
#define BSWM_INITVALUEOFLINSMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_LinSMMapping.InitValue' Reason: 'No Mode Request for BswMLinSMIndication configured.' */
#define BSWM_LINSMSTATE                                                                             STD_OFF  /**< Deactivateable: 'BswM_LinSMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_LINSCHEDULEENDMAPPING                                                                  STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndMapping' Reason: 'No Mode Request for BswMLinScheduleEndNotification configured.' */
#define BSWM_EXTERNALIDOFLINSCHEDULEENDMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndMapping.ExternalId' Reason: 'No Mode Request for BswMLinScheduleEndNotification configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFLINSCHEDULEENDMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMLinScheduleEndNotification configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEENDMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMLinScheduleEndNotification configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFLINSCHEDULEENDMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMLinScheduleEndNotification configured.' */
#define BSWM_INITVALUEOFLINSCHEDULEENDMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndMapping.InitValue' Reason: 'No Mode Request for BswMLinScheduleEndNotification configured.' */
#define BSWM_LINSCHEDULEENDSTATE                                                                    STD_OFF  /**< Deactivateable: 'BswM_LinScheduleEndState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_LINSCHEDULEMAPPING                                                                     STD_OFF  /**< Deactivateable: 'BswM_LinScheduleMapping' Reason: 'No Mode Request for BswMLinScheduleIndication configured.' */
#define BSWM_EXTERNALIDOFLINSCHEDULEMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_LinScheduleMapping.ExternalId' Reason: 'No Mode Request for BswMLinScheduleIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFLINSCHEDULEMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_LinScheduleMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMLinScheduleIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINSCHEDULEMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_LinScheduleMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMLinScheduleIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFLINSCHEDULEMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_LinScheduleMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMLinScheduleIndication configured.' */
#define BSWM_INITVALUEOFLINSCHEDULEMAPPING                                                          STD_OFF  /**< Deactivateable: 'BswM_LinScheduleMapping.InitValue' Reason: 'No Mode Request for BswMLinScheduleIndication configured.' */
#define BSWM_LINSCHEDULEREQUESTPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_LinScheduleRequestParameters' Reason: 'Action of type 'LinScheduleRequest' not in configuration.' */
#define BSWM_NETWORKOFLINSCHEDULEREQUESTPARAMETERS                                                  STD_OFF  /**< Deactivateable: 'BswM_LinScheduleRequestParameters.Network' Reason: 'Action of type 'LinScheduleRequest' not in configuration.' */
#define BSWM_SCHEDULEOFLINSCHEDULEREQUESTPARAMETERS                                                 STD_OFF  /**< Deactivateable: 'BswM_LinScheduleRequestParameters.Schedule' Reason: 'Action of type 'LinScheduleRequest' not in configuration.' */
#define BSWM_LINSCHEDULESTATE                                                                       STD_OFF  /**< Deactivateable: 'BswM_LinScheduleState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_LINTPMAPPING                                                                           STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_EXTERNALIDOFLINTPMAPPING                                                               STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ExternalId' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFLINTPMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFLINTPMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFLINTPMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_INITVALUEOFLINTPMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_LinTPMapping.InitValue' Reason: 'No Mode Request for BswMLinTpModeRequest configured.' */
#define BSWM_LINTPSTATE                                                                             STD_OFF  /**< Deactivateable: 'BswM_LinTPState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_MODENOTIFICATIONFCT                                                                    STD_ON
#define BSWM_MODENOTIFICATIONMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define BSWM_IMMEDIATEUSERENDIDXOFMODENOTIFICATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeNotificationMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFMODENOTIFICATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeNotificationMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFMODENOTIFICATIONMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_ModeNotificationMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeNotificationMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_MODEREQUESTMAPPING                                                                     STD_OFF  /**< Deactivateable: 'BswM_ModeRequestMapping' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define BSWM_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_ModeRequestMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeRequestMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_ModeRequestMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeRequestMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFMODEREQUESTMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_ModeRequestMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfModeRequestMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_MODEREQUESTQUEUE                                                                       STD_ON
#define BSWM_NMCONTROLPARAMETERS                                                                    STD_OFF  /**< Deactivateable: 'BswM_NmControlParameters' Reason: 'Action of type 'NmControl' not in configuration.' */
#define BSWM_CHANNELOFNMCONTROLPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_NmControlParameters.Channel' Reason: 'Action of type 'NmControl' not in configuration.' */
#define BSWM_CONTROLOFNMCONTROLPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_NmControlParameters.Control' Reason: 'Action of type 'NmControl' not in configuration.' */
#define BSWM_NMMAPPING                                                                              STD_OFF  /**< Deactivateable: 'BswM_NmMapping' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_EXTERNALIDOFNMMAPPING                                                                  STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ExternalId' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFNMMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFNMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFNMMAPPING                                                           STD_OFF  /**< Deactivateable: 'BswM_NmMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_INITVALUEOFNMMAPPING                                                                   STD_OFF  /**< Deactivateable: 'BswM_NmMapping.InitValue' Reason: 'No Mode Request for BswMNmIndication configured.' */
#define BSWM_NMSTATE                                                                                STD_OFF  /**< Deactivateable: 'BswM_NmState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_NVMBLOCKMAPPING                                                                        STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_EXTERNALIDOFNVMBLOCKMAPPING                                                            STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ExternalId' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFNVMBLOCKMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFNVMBLOCKMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFNVMBLOCKMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_INITVALUEOFNVMBLOCKMAPPING                                                             STD_OFF  /**< Deactivateable: 'BswM_NvMBlockMapping.InitValue' Reason: 'No Mode Request for BswMNvMRequest configured.' */
#define BSWM_NVMBLOCKSTATE                                                                          STD_OFF  /**< Deactivateable: 'BswM_NvMBlockState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_NVMJOBMAPPING                                                                          STD_ON
#define BSWM_EXTERNALIDOFNVMJOBMAPPING                                                              STD_ON
#define BSWM_IMMEDIATEUSERENDIDXOFNVMJOBMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_NvMJobMapping.ImmediateUserEndIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfNvMJobMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFNVMJOBMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_NvMJobMapping.ImmediateUserStartIdx' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfNvMJobMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_IMMEDIATEUSERUSEDOFNVMJOBMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_NvMJobMapping.ImmediateUserUsed' Reason: 'the optional indirection is deactivated because ImmediateUserUsedOfNvMJobMapping is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define BSWM_INITVALUEOFNVMJOBMAPPING                                                               STD_ON
#define BSWM_NVMJOBSTATE                                                                            STD_ON
#define BSWM_PARTITIONIDENTIFIERS                                                                   STD_ON
#define BSWM_PCPARTITIONCONFIGIDXOFPARTITIONIDENTIFIERS                                             STD_ON
#define BSWM_PARTITIONSNVOFPARTITIONIDENTIFIERS                                                     STD_ON
#define BSWM_PDURRXINDICATIONMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_EXTERNALIDOFPDURRXINDICATIONMAPPING                                                    STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURRXINDICATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURRXINDICATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURRXINDICATIONMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_INITVALUEOFPDURRXINDICATIONMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.InitValue' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_PDURRXINDICATIONSTATEIDXOFPDURRXINDICATIONMAPPING                                      STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationMapping.PduRRxIndicationStateIdx' Reason: 'No Mode Request for BswMPduRRxIndication configured.' */
#define BSWM_PDURRXINDICATIONSTATE                                                                  STD_OFF  /**< Deactivateable: 'BswM_PduRRxIndicationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTPRXINDICATIONMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_EXTERNALIDOFPDURTPRXINDICATIONMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTPRXINDICATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTPRXINDICATIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTPRXINDICATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_INITVALUEOFPDURTPRXINDICATIONMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.InitValue' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_PDURTPRXINDICATIONSTATEIDXOFPDURTPRXINDICATIONMAPPING                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationMapping.PduRTpRxIndicationStateIdx' Reason: 'No Mode Request for BswMPduRTpRxIndication configured.' */
#define BSWM_PDURTPRXINDICATIONSTATE                                                                STD_OFF  /**< Deactivateable: 'BswM_PduRTpRxIndicationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTPSTARTOFRECEPTIONMAPPING                                                          STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_EXTERNALIDOFPDURTPSTARTOFRECEPTIONMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTPSTARTOFRECEPTIONMAPPING                                     STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTPSTARTOFRECEPTIONMAPPING                                   STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTPSTARTOFRECEPTIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_INITVALUEOFPDURTPSTARTOFRECEPTIONMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.InitValue' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_PDURTPSTARTOFRECEPTIONSTATEIDXOFPDURTPSTARTOFRECEPTIONMAPPING                          STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionMapping.PduRTpStartOfReceptionStateIdx' Reason: 'No Mode Request for BswMPduRTpStartOfReception configured.' */
#define BSWM_PDURTPSTARTOFRECEPTIONSTATE                                                            STD_OFF  /**< Deactivateable: 'BswM_PduRTpStartOfReceptionState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTPTXCONFIRMATIONMAPPING                                                            STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_EXTERNALIDOFPDURTPTXCONFIRMATIONMAPPING                                                STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTPTXCONFIRMATIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTPTXCONFIRMATIONMAPPING                                     STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTPTXCONFIRMATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_INITVALUEOFPDURTPTXCONFIRMATIONMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.InitValue' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_PDURTPTXCONFIRMATIONSTATEIDXOFPDURTPTXCONFIRMATIONMAPPING                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationMapping.PduRTpTxConfirmationStateIdx' Reason: 'No Mode Request for BswMPduRTpTxConfirmation configured.' */
#define BSWM_PDURTPTXCONFIRMATIONSTATE                                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTpTxConfirmationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTRANSMITMAPPING                                                                    STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_EXTERNALIDOFPDURTRANSMITMAPPING                                                        STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTRANSMITMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTRANSMITMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTRANSMITMAPPING                                                 STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_INITVALUEOFPDURTRANSMITMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.InitValue' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_PDURTRANSMITSTATEIDXOFPDURTRANSMITMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitMapping.PduRTransmitStateIdx' Reason: 'No Mode Request for BswMPduRTransmit configured.' */
#define BSWM_PDURTRANSMITSTATE                                                                      STD_OFF  /**< Deactivateable: 'BswM_PduRTransmitState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDURTXCONFIRMATIONMAPPING                                                              STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_EXTERNALIDOFPDURTXCONFIRMATIONMAPPING                                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ExternalId' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFPDURTXCONFIRMATIONMAPPING                                         STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFPDURTXCONFIRMATIONMAPPING                                       STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFPDURTXCONFIRMATIONMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_INITVALUEOFPDURTXCONFIRMATIONMAPPING                                                   STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.InitValue' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_PDURTXCONFIRMATIONSTATEIDXOFPDURTXCONFIRMATIONMAPPING                                  STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationMapping.PduRTxConfirmationStateIdx' Reason: 'No Mode Request for BswMPduRTxConfirmation configured.' */
#define BSWM_PDURTXCONFIRMATIONSTATE                                                                STD_OFF  /**< Deactivateable: 'BswM_PduRTxConfirmationState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PDUROUTERCONTROLPARAMETERS                                                             STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlParameters' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_PDUROUTERCONTROLSUBPARAMETERSENDIDXOFPDUROUTERCONTROLPARAMETERS                        STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlParameters.PduRouterControlSubParametersEndIdx' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_PDUROUTERCONTROLSUBPARAMETERSSTARTIDXOFPDUROUTERCONTROLPARAMETERS                      STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlParameters.PduRouterControlSubParametersStartIdx' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_PDUROUTERCONTROLSUBPARAMETERSUSEDOFPDUROUTERCONTROLPARAMETERS                          STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlParameters.PduRouterControlSubParametersUsed' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_ROUTINGOFPDUROUTERCONTROLPARAMETERS                                                    STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlParameters.Routing' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_PDUROUTERCONTROLSUBPARAMETERS                                                          STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlSubParameters' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_IDOFPDUROUTERCONTROLSUBPARAMETERS                                                      STD_OFF  /**< Deactivateable: 'BswM_PduRouterControlSubParameters.Id' Reason: 'Action of type 'PduRouterControl' not in configuration.' */
#define BSWM_QUEUESEMAPHORE                                                                         STD_ON
#define BSWM_QUEUEWRITTEN                                                                           STD_ON
#define BSWM_RULECONTROLPARAMETERS                                                                  STD_OFF  /**< Deactivateable: 'BswM_RuleControlParameters' Reason: 'Action of type 'RuleControl' not in configuration.' */
#define BSWM_RULEIDOFRULECONTROLPARAMETERS                                                          STD_OFF  /**< Deactivateable: 'BswM_RuleControlParameters.RuleId' Reason: 'Action of type 'RuleControl' not in configuration.' */
#define BSWM_STATEOFRULECONTROLPARAMETERS                                                           STD_OFF  /**< Deactivateable: 'BswM_RuleControlParameters.State' Reason: 'Action of type 'RuleControl' not in configuration.' */
#define BSWM_RULESTATES                                                                             STD_ON
#define BSWM_RULES                                                                                  STD_ON
#define BSWM_ACTIONLISTSFALSEIDXOFRULES                                                             STD_ON
#define BSWM_ACTIONLISTSFALSEUSEDOFRULES                                                            STD_ON
#define BSWM_ACTIONLISTSTRUEIDXOFRULES                                                              STD_ON
#define BSWM_ACTIONLISTSTRUEUSEDOFRULES                                                             STD_ON
#define BSWM_EXPRESSIONSIDXOFRULES                                                                  STD_ON
#define BSWM_IDOFRULES                                                                              STD_ON
#define BSWM_INITOFRULES                                                                            STD_ON
#define BSWM_MASKEDBITSOFRULES                                                                      STD_ON
#define BSWM_RULESTATESIDXOFRULES                                                                   STD_ON
#define BSWM_RULESIND                                                                               STD_ON
#define BSWM_SDCLIENTPARAMETERS                                                                     STD_OFF  /**< Deactivateable: 'BswM_SdClientParameters' Reason: 'Action of type 'SdClient' not in configuration.' */
#define BSWM_IDOFSDCLIENTPARAMETERS                                                                 STD_OFF  /**< Deactivateable: 'BswM_SdClientParameters.Id' Reason: 'Action of type 'SdClient' not in configuration.' */
#define BSWM_STATEOFSDCLIENTPARAMETERS                                                              STD_OFF  /**< Deactivateable: 'BswM_SdClientParameters.State' Reason: 'Action of type 'SdClient' not in configuration.' */
#define BSWM_SDCLIENTSERVICEMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_EXTERNALIDOFSDCLIENTSERVICEMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ExternalId' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFSDCLIENTSERVICEMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFSDCLIENTSERVICEMAPPING                                          STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFSDCLIENTSERVICEMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_INITVALUEOFSDCLIENTSERVICEMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceMapping.InitValue' Reason: 'No Mode Request for BswMSdClientServiceCurrentState configured.' */
#define BSWM_SDCLIENTSERVICESTATE                                                                   STD_OFF  /**< Deactivateable: 'BswM_SdClientServiceState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_SDCONSUMEDEVENTMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_EXTERNALIDOFSDCONSUMEDEVENTMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ExternalId' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFSDCONSUMEDEVENTMAPPING                                            STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFSDCONSUMEDEVENTMAPPING                                          STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFSDCONSUMEDEVENTMAPPING                                              STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_INITVALUEOFSDCONSUMEDEVENTMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventMapping.InitValue' Reason: 'No Mode Request for BswMSdConsumedEventGroupCurrentState configured.' */
#define BSWM_SDCONSUMEDEVENTSTATE                                                                   STD_OFF  /**< Deactivateable: 'BswM_SdConsumedEventState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_SDCONSUMEDPARAMETERS                                                                   STD_OFF  /**< Deactivateable: 'BswM_SdConsumedParameters' Reason: 'Action of type 'SdConsumed' not in configuration.' */
#define BSWM_IDOFSDCONSUMEDPARAMETERS                                                               STD_OFF  /**< Deactivateable: 'BswM_SdConsumedParameters.Id' Reason: 'Action of type 'SdConsumed' not in configuration.' */
#define BSWM_STATEOFSDCONSUMEDPARAMETERS                                                            STD_OFF  /**< Deactivateable: 'BswM_SdConsumedParameters.State' Reason: 'Action of type 'SdConsumed' not in configuration.' */
#define BSWM_SDEVENTHANDLERMAPPING                                                                  STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_EXTERNALIDOFSDEVENTHANDLERMAPPING                                                      STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ExternalId' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFSDEVENTHANDLERMAPPING                                             STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFSDEVENTHANDLERMAPPING                                           STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFSDEVENTHANDLERMAPPING                                               STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_INITVALUEOFSDEVENTHANDLERMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerMapping.InitValue' Reason: 'No Mode Request for BswMSdEventHandlerCurrentState configured.' */
#define BSWM_SDEVENTHANDLERSTATE                                                                    STD_OFF  /**< Deactivateable: 'BswM_SdEventHandlerState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_SDSERVERPARAMETERS                                                                     STD_OFF  /**< Deactivateable: 'BswM_SdServerParameters' Reason: 'Action of type 'SdServer' not in configuration.' */
#define BSWM_IDOFSDSERVERPARAMETERS                                                                 STD_OFF  /**< Deactivateable: 'BswM_SdServerParameters.Id' Reason: 'Action of type 'SdServer' not in configuration.' */
#define BSWM_STATEOFSDSERVERPARAMETERS                                                              STD_OFF  /**< Deactivateable: 'BswM_SdServerParameters.State' Reason: 'Action of type 'SdServer' not in configuration.' */
#define BSWM_SIZEOFACTIONITEMS                                                                      STD_ON
#define BSWM_SIZEOFACTIONLISTQUEUE                                                                  STD_ON
#define BSWM_SIZEOFACTIONLISTS                                                                      STD_ON
#define BSWM_SIZEOFACTIONS                                                                          STD_ON
#define BSWM_SIZEOFCANSMCHANNELMAPPING                                                              STD_ON
#define BSWM_SIZEOFCANSMCHANNELSTATE                                                                STD_ON
#define BSWM_SIZEOFCOMDMCONTROLPARAMETERS                                                           STD_ON
#define BSWM_SIZEOFCOMDMCONTROLSUBPARAMETERS                                                        STD_ON
#define BSWM_SIZEOFCOMPDUGROUPSWITCHPARAMETERS                                                      STD_ON
#define BSWM_SIZEOFCOMPDUGROUPSWITCHSUBPARAMETERS                                                   STD_ON
#define BSWM_SIZEOFDEFERREDRULES                                                                    STD_ON
#define BSWM_SIZEOFECUMMODEMAPPING                                                                  STD_ON
#define BSWM_SIZEOFECUMRUNREQUESTMAPPING                                                            STD_ON
#define BSWM_SIZEOFECUMRUNREQUESTSTATE                                                              STD_ON
#define BSWM_SIZEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                                               STD_ON
#define BSWM_SIZEOFECUMSTATESWITCHPARAMETERS                                                        STD_ON
#define BSWM_SIZEOFEXPRESSIONS                                                                      STD_ON
#define BSWM_SIZEOFGENERICMAPPING                                                                   STD_ON
#define BSWM_SIZEOFGENERICMODEPARAMETERS                                                            STD_ON
#define BSWM_SIZEOFGENERICSTATE                                                                     STD_ON
#define BSWM_SIZEOFIMMEDIATEUSER                                                                    STD_ON
#define BSWM_SIZEOFINITACTIONLISTS                                                                  STD_ON
#define BSWM_SIZEOFINITGENVARANDINITAL                                                              STD_ON
#define BSWM_SIZEOFINITVALUES                                                                       STD_ON
#define BSWM_SIZEOFMODENOTIFICATIONFCT                                                              STD_ON
#define BSWM_SIZEOFMODEREQUESTQUEUE                                                                 STD_ON
#define BSWM_SIZEOFNVMJOBMAPPING                                                                    STD_ON
#define BSWM_SIZEOFNVMJOBSTATE                                                                      STD_ON
#define BSWM_SIZEOFPARTITIONIDENTIFIERS                                                             STD_ON
#define BSWM_SIZEOFRULESTATES                                                                       STD_ON
#define BSWM_SIZEOFRULES                                                                            STD_ON
#define BSWM_SIZEOFRULESIND                                                                         STD_ON
#define BSWM_SIZEOFSWCMODEREQUESTUPDATEFCT                                                          STD_ON
#define BSWM_SIZEOFTIMERCONTROLPARAMETERS                                                           STD_ON
#define BSWM_SIZEOFTIMERSTATE                                                                       STD_ON
#define BSWM_SIZEOFTIMERVALUE                                                                       STD_ON
#define BSWM_SWCMODEREQUESTUPDATEFCT                                                                STD_ON
#define BSWM_TIMERCONTROLPARAMETERS                                                                 STD_ON
#define BSWM_TIMEROFTIMERCONTROLPARAMETERS                                                          STD_ON
#define BSWM_VALUEOFTIMERCONTROLPARAMETERS                                                          STD_ON
#define BSWM_TIMERSTATE                                                                             STD_ON
#define BSWM_TIMERVALUE                                                                             STD_ON
#define BSWM_WDGMMAPPING                                                                            STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_EXTERNALIDOFWDGMMAPPING                                                                STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ExternalId' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_IMMEDIATEUSERENDIDXOFWDGMMAPPING                                                       STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ImmediateUserEndIdx' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_IMMEDIATEUSERSTARTIDXOFWDGMMAPPING                                                     STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ImmediateUserStartIdx' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_IMMEDIATEUSERUSEDOFWDGMMAPPING                                                         STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.ImmediateUserUsed' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_INITVALUEOFWDGMMAPPING                                                                 STD_OFF  /**< Deactivateable: 'BswM_WdgMMapping.InitValue' Reason: 'No Mode Request for BswMWdgMRequestPartitionReset configured.' */
#define BSWM_WDGMSTATE                                                                              STD_OFF  /**< Deactivateable: 'BswM_WdgMState' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define BSWM_PCCONFIG                                                                               STD_ON
#define BSWM_FINALMAGICNUMBEROFPCCONFIG                                                             STD_OFF  /**< Deactivateable: 'BswM_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_INITDATAHASHCODEOFPCCONFIG                                                             STD_OFF  /**< Deactivateable: 'BswM_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define BSWM_PCPARTITIONCONFIGOFPCCONFIG                                                            STD_ON
#define BSWM_PARTITIONIDENTIFIERSOFPCCONFIG                                                         STD_ON
#define BSWM_SIZEOFPARTITIONIDENTIFIERSOFPCCONFIG                                                   STD_ON
#define BSWM_PCPARTITIONCONFIG                                                                      STD_ON
#define BSWM_ACTIONITEMSOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_ACTIONLISTQUEUEOFPCPARTITIONCONFIG                                                     STD_ON
#define BSWM_ACTIONLISTSOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_ACTIONSOFPCPARTITIONCONFIG                                                             STD_ON
#define BSWM_CANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_CANSMCHANNELSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_COMDMCONTROLPARAMETERSOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_COMDMCONTROLSUBPARAMETERSOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_COMPDUGROUPSWITCHPARAMETERSOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_COMPDUGROUPSWITCHSUBPARAMETERSOFPCPARTITIONCONFIG                                      STD_ON
#define BSWM_DEFERREDRULESOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_ECUMMODEMAPPINGOFPCPARTITIONCONFIG                                                     STD_ON
#define BSWM_ECUMMODESTATEOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_ECUMRUNREQUESTMAPPINGOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_ECUMRUNREQUESTSTATEOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ECUMSELECTSHUTDOWNTARGETPARAMETERSOFPCPARTITIONCONFIG                                  STD_ON
#define BSWM_ECUMSTATESWITCHPARAMETERSOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_EXPRESSIONSOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_FORCEDACTIONLISTPRIORITYOFPCPARTITIONCONFIG                                            STD_ON
#define BSWM_GENERICMAPPINGOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_GENERICMODEPARAMETERSOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_GENERICSTATEOFPCPARTITIONCONFIG                                                        STD_ON
#define BSWM_IMMEDIATEUSEROFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_INITACTIONLISTSOFPCPARTITIONCONFIG                                                     STD_ON
#define BSWM_INITGENVARANDINITALOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_INITVALUESOFPCPARTITIONCONFIG                                                          STD_ON
#define BSWM_INITIALIZEDOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_MODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_MODEREQUESTQUEUEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_NVMJOBMAPPINGOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_NVMJOBSTATEOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_QUEUESEMAPHOREOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_QUEUEWRITTENOFPCPARTITIONCONFIG                                                        STD_ON
#define BSWM_RULESTATESOFPCPARTITIONCONFIG                                                          STD_ON
#define BSWM_RULESINDOFPCPARTITIONCONFIG                                                            STD_ON
#define BSWM_RULESOFPCPARTITIONCONFIG                                                               STD_ON
#define BSWM_SIZEOFACTIONITEMSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFACTIONLISTQUEUEOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_SIZEOFACTIONLISTSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFACTIONSOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_SIZEOFCANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFCANSMCHANNELSTATEOFPCPARTITIONCONFIG                                             STD_ON
#define BSWM_SIZEOFCOMDMCONTROLPARAMETERSOFPCPARTITIONCONFIG                                        STD_ON
#define BSWM_SIZEOFCOMDMCONTROLSUBPARAMETERSOFPCPARTITIONCONFIG                                     STD_ON
#define BSWM_SIZEOFCOMPDUGROUPSWITCHPARAMETERSOFPCPARTITIONCONFIG                                   STD_ON
#define BSWM_SIZEOFCOMPDUGROUPSWITCHSUBPARAMETERSOFPCPARTITIONCONFIG                                STD_ON
#define BSWM_SIZEOFDEFERREDRULESOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFECUMMODEMAPPINGOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_SIZEOFECUMRUNREQUESTMAPPINGOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_SIZEOFECUMRUNREQUESTSTATEOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFECUMSELECTSHUTDOWNTARGETPARAMETERSOFPCPARTITIONCONFIG                            STD_ON
#define BSWM_SIZEOFECUMSTATESWITCHPARAMETERSOFPCPARTITIONCONFIG                                     STD_ON
#define BSWM_SIZEOFEXPRESSIONSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFGENERICMAPPINGOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_SIZEOFGENERICMODEPARAMETERSOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_SIZEOFGENERICSTATEOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_SIZEOFIMMEDIATEUSEROFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFINITACTIONLISTSOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_SIZEOFINITGENVARANDINITALOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFINITVALUESOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SIZEOFMODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_SIZEOFMODEREQUESTQUEUEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_SIZEOFNVMJOBMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_SIZEOFNVMJOBSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_SIZEOFRULESTATESOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SIZEOFRULESINDOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_SIZEOFRULESOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_SIZEOFSWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                       STD_ON
#define BSWM_SIZEOFTIMERCONTROLPARAMETERSOFPCPARTITIONCONFIG                                        STD_ON
#define BSWM_SIZEOFTIMERSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SIZEOFTIMERVALUEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_SWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                             STD_ON
#define BSWM_TIMERCONTROLPARAMETERSOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_TIMERSTATEOFPCPARTITIONCONFIG                                                          STD_ON
#define BSWM_TIMERVALUEOFPCPARTITIONCONFIG                                                          STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCMinNumericValueDefines  BswM Min Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the minimum value in numerical based data.
  \{
*/ 
#define BSWM_MIN_ACTIONLISTQUEUE                                                                    0u
#define BSWM_MIN_ECUMMODESTATE                                                                      0u
#define BSWM_MIN_FORCEDACTIONLISTPRIORITY                                                           0u
#define BSWM_MIN_MODEREQUESTQUEUE                                                                   0u
#define BSWM_MIN_QUEUESEMAPHORE                                                                     0u
#define BSWM_MIN_RULESTATES                                                                         0u
#define BSWM_MIN_TIMERSTATE                                                                         0u
#define BSWM_MIN_TIMERVALUE                                                                         0u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCMaxNumericValueDefines  BswM Max Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the maximum value in numerical based data.
  \{
*/ 
#define BSWM_MAX_ACTIONLISTQUEUE                                                                    255u
#define BSWM_MAX_ECUMMODESTATE                                                                      255u
#define BSWM_MAX_FORCEDACTIONLISTPRIORITY                                                           255u
#define BSWM_MAX_MODEREQUESTQUEUE                                                                   255u
#define BSWM_MAX_QUEUESEMAPHORE                                                                     255u
#define BSWM_MAX_RULESTATES                                                                         255u
#define BSWM_MAX_TIMERSTATE                                                                         255u
#define BSWM_MAX_TIMERVALUE                                                                         4294967295u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCNoReferenceDefines  BswM No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define BSWM_NO_PARAMETERIDXOFACTIONITEMS                                                           255u
#define BSWM_NO_ACTIONITEMSENDIDXOFACTIONLISTS                                                      255u
#define BSWM_NO_ACTIONITEMSSTARTIDXOFACTIONLISTS                                                    255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                            255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                          255u
#define BSWM_NO_COMDMCONTROLSUBPARAMETERSENDIDXOFCOMDMCONTROLPARAMETERS                             255u
#define BSWM_NO_COMDMCONTROLSUBPARAMETERSSTARTIDXOFCOMDMCONTROLPARAMETERS                           255u
#define BSWM_NO_COMPDUGROUPSWITCHSUBPARAMETERSENDIDXOFCOMPDUGROUPSWITCHPARAMETERS                   255u
#define BSWM_NO_COMPDUGROUPSWITCHSUBPARAMETERSSTARTIDXOFCOMPDUGROUPSWITCHPARAMETERS                 255u
#define BSWM_NO_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                                 255u
#define BSWM_NO_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                               255u
#define BSWM_NO_RULESINDENDIDXOFIMMEDIATEUSER                                                       255u
#define BSWM_NO_RULESINDSTARTIDXOFIMMEDIATEUSER                                                     255u
#define BSWM_NO_ACTIONLISTSFALSEIDXOFRULES                                                          255u
#define BSWM_NO_ACTIONLISTSTRUEIDXOFRULES                                                           255u
#define BSWM_NO_IDOFRULES                                                                           255u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCMaskedBitDefines  BswM Masked Bit Defines (PRE_COMPILE)
  \brief  These defines are masks to extract packed boolean data.
  \{
*/ 
#define BSWM_ACTIONITEMSUSEDOFACTIONLISTS_MASK                                                      0x02u
#define BSWM_CONDITIONALOFACTIONLISTS_MASK                                                          0x01u
#define BSWM_ONINITOFIMMEDIATEUSER_MASK                                                             0x02u
#define BSWM_RULESINDUSEDOFIMMEDIATEUSER_MASK                                                       0x01u
#define BSWM_ACTIONLISTSFALSEUSEDOFRULES_MASK                                                       0x02u
#define BSWM_ACTIONLISTSTRUEUSEDOFRULES_MASK                                                        0x01u
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCIsReducedToDefineDefines  BswM Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define BSWM_ISDEF_ACTIONSIDXOFACTIONITEMS                                                          STD_OFF
#define BSWM_ISDEF_PARAMETERIDXOFACTIONITEMS                                                        STD_OFF
#define BSWM_ISDEF_PARAMETERUSEDOFACTIONITEMS                                                       STD_OFF
#define BSWM_ISDEF_ACTIONITEMSENDIDXOFACTIONLISTS                                                   STD_OFF
#define BSWM_ISDEF_ACTIONITEMSSTARTIDXOFACTIONLISTS                                                 STD_OFF
#define BSWM_ISDEF_ACTIONITEMSUSEDOFACTIONLISTS                                                     STD_OFF
#define BSWM_ISDEF_CONDITIONALOFACTIONLISTS                                                         STD_OFF
#define BSWM_ISDEF_MASKEDBITSOFACTIONLISTS                                                          STD_OFF
#define BSWM_ISDEF_ACTIONS                                                                          STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFCANSMCHANNELMAPPING                                                  STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                         STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                       STD_OFF
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFCANSMCHANNELMAPPING                                           STD_ON
#define BSWM_ISDEF_INITVALUEOFCANSMCHANNELMAPPING                                                   STD_ON
#define BSWM_ISDEF_COMDMCONTROLSUBPARAMETERSENDIDXOFCOMDMCONTROLPARAMETERS                          STD_OFF
#define BSWM_ISDEF_COMDMCONTROLSUBPARAMETERSSTARTIDXOFCOMDMCONTROLPARAMETERS                        STD_OFF
#define BSWM_ISDEF_COMDMCONTROLSUBPARAMETERSUSEDOFCOMDMCONTROLPARAMETERS                            STD_ON
#define BSWM_ISDEF_BITVALOFCOMDMCONTROLSUBPARAMETERS                                                STD_OFF
#define BSWM_ISDEF_IPDUGROUPIDOFCOMDMCONTROLSUBPARAMETERS                                           STD_OFF
#define BSWM_ISDEF_COMPDUGROUPSWITCHSUBPARAMETERSENDIDXOFCOMPDUGROUPSWITCHPARAMETERS                STD_OFF
#define BSWM_ISDEF_COMPDUGROUPSWITCHSUBPARAMETERSSTARTIDXOFCOMPDUGROUPSWITCHPARAMETERS              STD_OFF
#define BSWM_ISDEF_COMPDUGROUPSWITCHSUBPARAMETERSUSEDOFCOMPDUGROUPSWITCHPARAMETERS                  STD_ON
#define BSWM_ISDEF_CONTROLOFCOMPDUGROUPSWITCHPARAMETERS                                             STD_ON
#define BSWM_ISDEF_BITVALOFCOMPDUGROUPSWITCHSUBPARAMETERS                                           STD_OFF
#define BSWM_ISDEF_IPDUGROUPIDOFCOMPDUGROUPSWITCHSUBPARAMETERS                                      STD_OFF
#define BSWM_ISDEF_RULESIDXOFDEFERREDRULES                                                          STD_OFF
#define BSWM_ISDEF_INITVALUEOFECUMMODEMAPPING                                                       STD_ON
#define BSWM_ISDEF_EXTERNALIDOFECUMRUNREQUESTMAPPING                                                STD_OFF
#define BSWM_ISDEF_INITVALUEOFECUMRUNREQUESTMAPPING                                                 STD_ON
#define BSWM_ISDEF_RESETSLEEPMODEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                               STD_ON
#define BSWM_ISDEF_TARGETSTATEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                                  STD_ON
#define BSWM_ISDEF_TARGETSTATEOFECUMSTATESWITCHPARAMETERS                                           STD_OFF
#define BSWM_ISDEF_EXPRESSIONS                                                                      STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFGENERICMAPPING                                                       STD_ON
#define BSWM_ISDEF_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                              STD_ON
#define BSWM_ISDEF_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                            STD_ON
#define BSWM_ISDEF_IMMEDIATEUSERUSEDOFGENERICMAPPING                                                STD_ON
#define BSWM_ISDEF_INITVALUEOFGENERICMAPPING                                                        STD_ON
#define BSWM_ISDEF_MODEOFGENERICMODEPARAMETERS                                                      STD_OFF
#define BSWM_ISDEF_USEROFGENERICMODEPARAMETERS                                                      STD_ON
#define BSWM_ISDEF_MASKEDBITSOFIMMEDIATEUSER                                                        STD_OFF
#define BSWM_ISDEF_ONINITOFIMMEDIATEUSER                                                            STD_OFF
#define BSWM_ISDEF_RULESINDENDIDXOFIMMEDIATEUSER                                                    STD_OFF
#define BSWM_ISDEF_RULESINDSTARTIDXOFIMMEDIATEUSER                                                  STD_OFF
#define BSWM_ISDEF_RULESINDUSEDOFIMMEDIATEUSER                                                      STD_ON
#define BSWM_ISDEF_INITACTIONLISTS                                                                  STD_ON
#define BSWM_ISDEF_INITGENVARANDINITAL                                                              STD_OFF
#define BSWM_ISDEF_INITVALUES                                                                       STD_ON
#define BSWM_ISDEF_MODENOTIFICATIONFCT                                                              STD_OFF
#define BSWM_ISDEF_EXTERNALIDOFNVMJOBMAPPING                                                        STD_ON
#define BSWM_ISDEF_INITVALUEOFNVMJOBMAPPING                                                         STD_ON
#define BSWM_ISDEF_PCPARTITIONCONFIGIDXOFPARTITIONIDENTIFIERS                                       STD_OFF
#define BSWM_ISDEF_PARTITIONSNVOFPARTITIONIDENTIFIERS                                               STD_OFF
#define BSWM_ISDEF_ACTIONLISTSFALSEIDXOFRULES                                                       STD_OFF
#define BSWM_ISDEF_ACTIONLISTSFALSEUSEDOFRULES                                                      STD_OFF
#define BSWM_ISDEF_ACTIONLISTSTRUEIDXOFRULES                                                        STD_OFF
#define BSWM_ISDEF_ACTIONLISTSTRUEUSEDOFRULES                                                       STD_ON
#define BSWM_ISDEF_EXPRESSIONSIDXOFRULES                                                            STD_OFF
#define BSWM_ISDEF_IDOFRULES                                                                        STD_OFF
#define BSWM_ISDEF_INITOFRULES                                                                      STD_ON
#define BSWM_ISDEF_MASKEDBITSOFRULES                                                                STD_OFF
#define BSWM_ISDEF_RULESTATESIDXOFRULES                                                             STD_OFF
#define BSWM_ISDEF_RULESIND                                                                         STD_OFF
#define BSWM_ISDEF_SWCMODEREQUESTUPDATEFCT                                                          STD_OFF
#define BSWM_ISDEF_TIMEROFTIMERCONTROLPARAMETERS                                                    STD_OFF
#define BSWM_ISDEF_VALUEOFTIMERCONTROLPARAMETERS                                                    STD_OFF
#define BSWM_ISDEF_PCPARTITIONCONFIGOFPCCONFIG                                                      STD_ON
#define BSWM_ISDEF_PARTITIONIDENTIFIERSOFPCCONFIG                                                   STD_ON
#define BSWM_ISDEF_ACTIONITEMSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_ACTIONLISTQUEUEOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_ISDEF_ACTIONLISTSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_ACTIONSOFPCPARTITIONCONFIG                                                       STD_ON
#define BSWM_ISDEF_CANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_CANSMCHANNELSTATEOFPCPARTITIONCONFIG                                             STD_ON
#define BSWM_ISDEF_COMDMCONTROLPARAMETERSOFPCPARTITIONCONFIG                                        STD_ON
#define BSWM_ISDEF_COMDMCONTROLSUBPARAMETERSOFPCPARTITIONCONFIG                                     STD_ON
#define BSWM_ISDEF_COMPDUGROUPSWITCHPARAMETERSOFPCPARTITIONCONFIG                                   STD_ON
#define BSWM_ISDEF_COMPDUGROUPSWITCHSUBPARAMETERSOFPCPARTITIONCONFIG                                STD_ON
#define BSWM_ISDEF_DEFERREDRULESOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_ECUMMODEMAPPINGOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_ISDEF_ECUMMODESTATEOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_ECUMRUNREQUESTMAPPINGOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_ISDEF_ECUMRUNREQUESTSTATEOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_ECUMSELECTSHUTDOWNTARGETPARAMETERSOFPCPARTITIONCONFIG                            STD_ON
#define BSWM_ISDEF_ECUMSTATESWITCHPARAMETERSOFPCPARTITIONCONFIG                                     STD_ON
#define BSWM_ISDEF_EXPRESSIONSOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_FORCEDACTIONLISTPRIORITYOFPCPARTITIONCONFIG                                      STD_ON
#define BSWM_ISDEF_GENERICMAPPINGOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_ISDEF_GENERICMODEPARAMETERSOFPCPARTITIONCONFIG                                         STD_ON
#define BSWM_ISDEF_GENERICSTATEOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_ISDEF_IMMEDIATEUSEROFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_INITACTIONLISTSOFPCPARTITIONCONFIG                                               STD_ON
#define BSWM_ISDEF_INITGENVARANDINITALOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_INITVALUESOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_ISDEF_INITIALIZEDOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_MODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                           STD_ON
#define BSWM_ISDEF_MODEREQUESTQUEUEOFPCPARTITIONCONFIG                                              STD_ON
#define BSWM_ISDEF_NVMJOBMAPPINGOFPCPARTITIONCONFIG                                                 STD_ON
#define BSWM_ISDEF_NVMJOBSTATEOFPCPARTITIONCONFIG                                                   STD_ON
#define BSWM_ISDEF_QUEUESEMAPHOREOFPCPARTITIONCONFIG                                                STD_ON
#define BSWM_ISDEF_QUEUEWRITTENOFPCPARTITIONCONFIG                                                  STD_ON
#define BSWM_ISDEF_RULESTATESOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_ISDEF_RULESINDOFPCPARTITIONCONFIG                                                      STD_ON
#define BSWM_ISDEF_RULESOFPCPARTITIONCONFIG                                                         STD_ON
#define BSWM_ISDEF_SWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                       STD_ON
#define BSWM_ISDEF_TIMERCONTROLPARAMETERSOFPCPARTITIONCONFIG                                        STD_ON
#define BSWM_ISDEF_TIMERSTATEOFPCPARTITIONCONFIG                                                    STD_ON
#define BSWM_ISDEF_TIMERVALUEOFPCPARTITIONCONFIG                                                    STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCEqualsAlwaysToDefines  BswM Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define BSWM_EQ2_ACTIONSIDXOFACTIONITEMS                                                            
#define BSWM_EQ2_PARAMETERIDXOFACTIONITEMS                                                          
#define BSWM_EQ2_PARAMETERUSEDOFACTIONITEMS                                                         
#define BSWM_EQ2_ACTIONITEMSENDIDXOFACTIONLISTS                                                     
#define BSWM_EQ2_ACTIONITEMSSTARTIDXOFACTIONLISTS                                                   
#define BSWM_EQ2_ACTIONITEMSUSEDOFACTIONLISTS                                                       
#define BSWM_EQ2_CONDITIONALOFACTIONLISTS                                                           
#define BSWM_EQ2_MASKEDBITSOFACTIONLISTS                                                            
#define BSWM_EQ2_ACTIONS                                                                            
#define BSWM_EQ2_EXTERNALIDOFCANSMCHANNELMAPPING                                                    
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFCANSMCHANNELMAPPING                                           
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFCANSMCHANNELMAPPING                                         
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFCANSMCHANNELMAPPING                                             TRUE
#define BSWM_EQ2_INITVALUEOFCANSMCHANNELMAPPING                                                     CANSM_BSWM_NO_COMMUNICATION
#define BSWM_EQ2_COMDMCONTROLSUBPARAMETERSENDIDXOFCOMDMCONTROLPARAMETERS                            
#define BSWM_EQ2_COMDMCONTROLSUBPARAMETERSSTARTIDXOFCOMDMCONTROLPARAMETERS                          
#define BSWM_EQ2_COMDMCONTROLSUBPARAMETERSUSEDOFCOMDMCONTROLPARAMETERS                              TRUE
#define BSWM_EQ2_BITVALOFCOMDMCONTROLSUBPARAMETERS                                                  
#define BSWM_EQ2_IPDUGROUPIDOFCOMDMCONTROLSUBPARAMETERS                                             
#define BSWM_EQ2_COMPDUGROUPSWITCHSUBPARAMETERSENDIDXOFCOMPDUGROUPSWITCHPARAMETERS                  
#define BSWM_EQ2_COMPDUGROUPSWITCHSUBPARAMETERSSTARTIDXOFCOMPDUGROUPSWITCHPARAMETERS                
#define BSWM_EQ2_COMPDUGROUPSWITCHSUBPARAMETERSUSEDOFCOMPDUGROUPSWITCHPARAMETERS                    TRUE
#define BSWM_EQ2_CONTROLOFCOMPDUGROUPSWITCHPARAMETERS                                               BSWM_GROUPCONTROL_NORMAL
#define BSWM_EQ2_BITVALOFCOMPDUGROUPSWITCHSUBPARAMETERS                                             
#define BSWM_EQ2_IPDUGROUPIDOFCOMPDUGROUPSWITCHSUBPARAMETERS                                        
#define BSWM_EQ2_RULESIDXOFDEFERREDRULES                                                            
#define BSWM_EQ2_INITVALUEOFECUMMODEMAPPING                                                         ECUM_STATE_OFF
#define BSWM_EQ2_EXTERNALIDOFECUMRUNREQUESTMAPPING                                                  
#define BSWM_EQ2_INITVALUEOFECUMRUNREQUESTMAPPING                                                   ECUM_RUNSTATUS_RELEASED
#define BSWM_EQ2_RESETSLEEPMODEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                                 EcuMConf_EcuMResetMode_ECUM_RESET_MCU
#define BSWM_EQ2_TARGETSTATEOFECUMSELECTSHUTDOWNTARGETPARAMETERS                                    ECUM_STATE_RESET
#define BSWM_EQ2_TARGETSTATEOFECUMSTATESWITCHPARAMETERS                                             
#define BSWM_EQ2_EXPRESSIONS                                                                        
#define BSWM_EQ2_EXTERNALIDOFGENERICMAPPING                                                         BSWM_GENERIC_ESH_State
#define BSWM_EQ2_IMMEDIATEUSERENDIDXOFGENERICMAPPING                                                3u
#define BSWM_EQ2_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING                                              2u
#define BSWM_EQ2_IMMEDIATEUSERUSEDOFGENERICMAPPING                                                  TRUE
#define BSWM_EQ2_INITVALUEOFGENERICMAPPING                                                          BSWM_GENERICVALUE_ESH_State_ESH_INIT
#define BSWM_EQ2_MODEOFGENERICMODEPARAMETERS                                                        
#define BSWM_EQ2_USEROFGENERICMODEPARAMETERS                                                        BSWM_GENERIC_ESH_State
#define BSWM_EQ2_MASKEDBITSOFIMMEDIATEUSER                                                          
#define BSWM_EQ2_ONINITOFIMMEDIATEUSER                                                              
#define BSWM_EQ2_RULESINDENDIDXOFIMMEDIATEUSER                                                      
#define BSWM_EQ2_RULESINDSTARTIDXOFIMMEDIATEUSER                                                    
#define BSWM_EQ2_RULESINDUSEDOFIMMEDIATEUSER                                                        TRUE
#define BSWM_EQ2_INITACTIONLISTS                                                                    0u
#define BSWM_EQ2_INITGENVARANDINITAL                                                                
#define BSWM_EQ2_INITVALUES                                                                         BSWM_GENERICVALUE_ESH_State_ESH_INIT
#define BSWM_EQ2_MODENOTIFICATIONFCT                                                                
#define BSWM_EQ2_EXTERNALIDOFNVMJOBMAPPING                                                          NVM_SERVICE_ID_WRITEALL
#define BSWM_EQ2_INITVALUEOFNVMJOBMAPPING                                                           NVM_REQ_OK
#define BSWM_EQ2_PCPARTITIONCONFIGIDXOFPARTITIONIDENTIFIERS                                         
#define BSWM_EQ2_PARTITIONSNVOFPARTITIONIDENTIFIERS                                                 
#define BSWM_EQ2_ACTIONLISTSFALSEIDXOFRULES                                                         
#define BSWM_EQ2_ACTIONLISTSFALSEUSEDOFRULES                                                        
#define BSWM_EQ2_ACTIONLISTSTRUEIDXOFRULES                                                          
#define BSWM_EQ2_ACTIONLISTSTRUEUSEDOFRULES                                                         TRUE
#define BSWM_EQ2_EXPRESSIONSIDXOFRULES                                                              
#define BSWM_EQ2_IDOFRULES                                                                          
#define BSWM_EQ2_INITOFRULES                                                                        BSWM_FALSE
#define BSWM_EQ2_MASKEDBITSOFRULES                                                                  
#define BSWM_EQ2_RULESTATESIDXOFRULES                                                               
#define BSWM_EQ2_RULESIND                                                                           
#define BSWM_EQ2_SWCMODEREQUESTUPDATEFCT                                                            
#define BSWM_EQ2_TIMEROFTIMERCONTROLPARAMETERS                                                      
#define BSWM_EQ2_VALUEOFTIMERCONTROLPARAMETERS                                                      
#define BSWM_EQ2_PCPARTITIONCONFIGOFPCCONFIG                                                        BswM_PCPartitionConfigLeft
#define BSWM_EQ2_PARTITIONIDENTIFIERSOFPCCONFIG                                                     BswM_PartitionIdentifiers
#define BSWM_EQ2_ACTIONITEMSOFPCPARTITIONCONFIG                                                     BswM_ActionItems
#define BSWM_EQ2_ACTIONLISTQUEUEOFPCPARTITIONCONFIG                                                 BswM_ActionListQueue.raw
#define BSWM_EQ2_ACTIONLISTSOFPCPARTITIONCONFIG                                                     BswM_ActionLists
#define BSWM_EQ2_ACTIONSOFPCPARTITIONCONFIG                                                         BswM_Actions
#define BSWM_EQ2_CANSMCHANNELMAPPINGOFPCPARTITIONCONFIG                                             BswM_CanSMChannelMapping
#define BSWM_EQ2_CANSMCHANNELSTATEOFPCPARTITIONCONFIG                                               BswM_CanSMChannelState
#define BSWM_EQ2_COMDMCONTROLPARAMETERSOFPCPARTITIONCONFIG                                          BswM_ComDMControlParameters
#define BSWM_EQ2_COMDMCONTROLSUBPARAMETERSOFPCPARTITIONCONFIG                                       BswM_ComDMControlSubParameters
#define BSWM_EQ2_COMPDUGROUPSWITCHPARAMETERSOFPCPARTITIONCONFIG                                     BswM_ComPduGroupSwitchParameters
#define BSWM_EQ2_COMPDUGROUPSWITCHSUBPARAMETERSOFPCPARTITIONCONFIG                                  BswM_ComPduGroupSwitchSubParameters
#define BSWM_EQ2_DEFERREDRULESOFPCPARTITIONCONFIG                                                   BswM_DeferredRules
#define BSWM_EQ2_ECUMMODEMAPPINGOFPCPARTITIONCONFIG                                                 BswM_EcuMModeMapping
#define BSWM_EQ2_ECUMMODESTATEOFPCPARTITIONCONFIG                                                   (&(BswM_EcuMModeState))
#define BSWM_EQ2_ECUMRUNREQUESTMAPPINGOFPCPARTITIONCONFIG                                           BswM_EcuMRunRequestMapping
#define BSWM_EQ2_ECUMRUNREQUESTSTATEOFPCPARTITIONCONFIG                                             BswM_EcuMRunRequestState
#define BSWM_EQ2_ECUMSELECTSHUTDOWNTARGETPARAMETERSOFPCPARTITIONCONFIG                              BswM_EcuMSelectShutdownTargetParameters
#define BSWM_EQ2_ECUMSTATESWITCHPARAMETERSOFPCPARTITIONCONFIG                                       BswM_EcuMStateSwitchParameters
#define BSWM_EQ2_EXPRESSIONSOFPCPARTITIONCONFIG                                                     BswM_Expressions
#define BSWM_EQ2_FORCEDACTIONLISTPRIORITYOFPCPARTITIONCONFIG                                        (&(BswM_ForcedActionListPriority))
#define BSWM_EQ2_GENERICMAPPINGOFPCPARTITIONCONFIG                                                  BswM_GenericMapping
#define BSWM_EQ2_GENERICMODEPARAMETERSOFPCPARTITIONCONFIG                                           BswM_GenericModeParameters
#define BSWM_EQ2_GENERICSTATEOFPCPARTITIONCONFIG                                                    BswM_GenericState
#define BSWM_EQ2_IMMEDIATEUSEROFPCPARTITIONCONFIG                                                   BswM_ImmediateUser
#define BSWM_EQ2_INITACTIONLISTSOFPCPARTITIONCONFIG                                                 BswM_InitActionLists
#define BSWM_EQ2_INITGENVARANDINITALOFPCPARTITIONCONFIG                                             BswM_InitGenVarAndInitAL
#define BSWM_EQ2_INITVALUESOFPCPARTITIONCONFIG                                                      BswM_InitValues
#define BSWM_EQ2_INITIALIZEDOFPCPARTITIONCONFIG                                                     (&(BswM_Initialized))
#define BSWM_EQ2_MODENOTIFICATIONFCTOFPCPARTITIONCONFIG                                             BswM_ModeNotificationFct
#define BSWM_EQ2_MODEREQUESTQUEUEOFPCPARTITIONCONFIG                                                BswM_ModeRequestQueue
#define BSWM_EQ2_NVMJOBMAPPINGOFPCPARTITIONCONFIG                                                   BswM_NvMJobMapping
#define BSWM_EQ2_NVMJOBSTATEOFPCPARTITIONCONFIG                                                     BswM_NvMJobState
#define BSWM_EQ2_QUEUESEMAPHOREOFPCPARTITIONCONFIG                                                  (&(BswM_QueueSemaphore))
#define BSWM_EQ2_QUEUEWRITTENOFPCPARTITIONCONFIG                                                    (&(BswM_QueueWritten))
#define BSWM_EQ2_RULESTATESOFPCPARTITIONCONFIG                                                      BswM_RuleStates.raw
#define BSWM_EQ2_RULESINDOFPCPARTITIONCONFIG                                                        BswM_RulesInd
#define BSWM_EQ2_RULESOFPCPARTITIONCONFIG                                                           BswM_Rules
#define BSWM_EQ2_SWCMODEREQUESTUPDATEFCTOFPCPARTITIONCONFIG                                         BswM_SwcModeRequestUpdateFct
#define BSWM_EQ2_TIMERCONTROLPARAMETERSOFPCPARTITIONCONFIG                                          BswM_TimerControlParameters
#define BSWM_EQ2_TIMERSTATEOFPCPARTITIONCONFIG                                                      BswM_TimerState.raw
#define BSWM_EQ2_TIMERVALUEOFPCPARTITIONCONFIG                                                      BswM_TimerValue.raw
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCSymbolicInitializationPointers  BswM Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define BswM_Config_Left_Ptr                                                                        NULL_PTR  /**< symbolic identifier which shall be used to initialize 'BswM' */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCInitializationSymbols  BswM Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define BswM_Config_Left                                                                            NULL_PTR  /**< symbolic identifier which could be used to initialize 'BswM */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCGeneral  BswM General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define BSWM_CHECK_INIT_POINTER                                                                     STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define BSWM_FINAL_MAGIC_NUMBER                                                                     0x2A1Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of BswM */
#define BSWM_INDIVIDUAL_POSTBUILD                                                                   STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'BswM' is not configured to be postbuild capable. */
#define BSWM_INIT_DATA                                                                              BSWM_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define BSWM_INIT_DATA_HASH_CODE                                                                    -1519484662  /**< the precompile constant to validate the initialization structure at initialization time of BswM with a hashcode. The seed value is '0x2A1Eu' */
#define BSWM_USE_ECUM_BSW_ERROR_HOOK                                                                STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define BSWM_USE_INIT_POINTER                                                                       STD_OFF  /**< STD_ON if the init pointer BswM shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  BswMPBDataSwitches  BswM Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define BSWM_PBCONFIG                                                                               STD_OFF  /**< Deactivateable: 'BswM_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_LTCONFIGIDXOFPBCONFIG                                                                  STD_OFF  /**< Deactivateable: 'BswM_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_PBPARTITIONCONFIGOFPBCONFIG                                                            STD_OFF  /**< Deactivateable: 'BswM_PBConfig.PBPartitionConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_PCCONFIGIDXOFPBCONFIG                                                                  STD_OFF  /**< Deactivateable: 'BswM_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define BSWM_PBPARTITIONCONFIG                                                                      STD_OFF  /**< Deactivateable: 'BswM_PBPartitionConfigLeft' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/* PRQA S 0639, 0779 PRECOMPILEGLOBALDATATYPES */ /* MD_MSR_1.1_639, MD_MSR_Rule5.2_0779 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCIterableTypes  BswM Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate BswM_ActionItems */
typedef uint8_least BswM_ActionItemsIterType;

/**   \brief  type used to iterate BswM_ActionLists */
typedef uint8_least BswM_ActionListsIterType;

/**   \brief  type used to iterate BswM_Actions */
typedef uint8_least BswM_ActionsIterType;

/**   \brief  type used to iterate BswM_CanSMChannelMapping */
typedef uint8_least BswM_CanSMChannelMappingIterType;

/**   \brief  type used to iterate BswM_CanSMChannelState */
typedef uint8_least BswM_CanSMChannelStateIterType;

/**   \brief  type used to iterate BswM_ComDMControlParameters */
typedef uint8_least BswM_ComDMControlParametersIterType;

/**   \brief  type used to iterate BswM_ComDMControlSubParameters */
typedef uint8_least BswM_ComDMControlSubParametersIterType;

/**   \brief  type used to iterate BswM_ComPduGroupSwitchParameters */
typedef uint8_least BswM_ComPduGroupSwitchParametersIterType;

/**   \brief  type used to iterate BswM_ComPduGroupSwitchSubParameters */
typedef uint8_least BswM_ComPduGroupSwitchSubParametersIterType;

/**   \brief  type used to iterate BswM_DeferredRules */
typedef uint8_least BswM_DeferredRulesIterType;

/**   \brief  type used to iterate BswM_EcuMModeMapping */
typedef uint8_least BswM_EcuMModeMappingIterType;

/**   \brief  type used to iterate BswM_EcuMRunRequestMapping */
typedef uint8_least BswM_EcuMRunRequestMappingIterType;

/**   \brief  type used to iterate BswM_EcuMRunRequestState */
typedef uint8_least BswM_EcuMRunRequestStateIterType;

/**   \brief  type used to iterate BswM_EcuMSelectShutdownTargetParameters */
typedef uint8_least BswM_EcuMSelectShutdownTargetParametersIterType;

/**   \brief  type used to iterate BswM_EcuMStateSwitchParameters */
typedef uint8_least BswM_EcuMStateSwitchParametersIterType;

/**   \brief  type used to iterate BswM_Expressions */
typedef uint8_least BswM_ExpressionsIterType;

/**   \brief  type used to iterate BswM_GenericMapping */
typedef uint8_least BswM_GenericMappingIterType;

/**   \brief  type used to iterate BswM_GenericModeParameters */
typedef uint8_least BswM_GenericModeParametersIterType;

/**   \brief  type used to iterate BswM_GenericState */
typedef uint8_least BswM_GenericStateIterType;

/**   \brief  type used to iterate BswM_ImmediateUser */
typedef uint8_least BswM_ImmediateUserIterType;

/**   \brief  type used to iterate BswM_InitActionLists */
typedef uint8_least BswM_InitActionListsIterType;

/**   \brief  type used to iterate BswM_InitGenVarAndInitAL */
typedef uint8_least BswM_InitGenVarAndInitALIterType;

/**   \brief  type used to iterate BswM_InitValues */
typedef uint8_least BswM_InitValuesIterType;

/**   \brief  type used to iterate BswM_ModeNotificationFct */
typedef uint8_least BswM_ModeNotificationFctIterType;

/**   \brief  type used to iterate BswM_NvMJobMapping */
typedef uint8_least BswM_NvMJobMappingIterType;

/**   \brief  type used to iterate BswM_NvMJobState */
typedef uint8_least BswM_NvMJobStateIterType;

/**   \brief  type used to iterate BswM_PartitionIdentifiers */
typedef uint8_least BswM_PartitionIdentifiersIterType;

/**   \brief  type used to iterate BswM_RuleStates */
typedef uint8_least BswM_RuleStatesIterType;

/**   \brief  type used to iterate BswM_Rules */
typedef uint8_least BswM_RulesIterType;

/**   \brief  type used to iterate BswM_RulesInd */
typedef uint8_least BswM_RulesIndIterType;

/**   \brief  type used to iterate BswM_SwcModeRequestUpdateFct */
typedef uint8_least BswM_SwcModeRequestUpdateFctIterType;

/**   \brief  type used to iterate BswM_TimerControlParameters */
typedef uint8_least BswM_TimerControlParametersIterType;

/**   \brief  type used to iterate BswM_TimerState */
typedef uint8_least BswM_TimerStateIterType;

/**   \brief  type used to iterate BswM_TimerValue */
typedef uint8_least BswM_TimerValueIterType;

/**   \brief  type used to iterate BswM_PCPartitionConfig */
typedef uint8_least BswM_PCPartitionConfigIterType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCIterableTypesWithSizeRelations  BswM Iterable Types With Size Relations (PRE_COMPILE)
  \brief  These type definitions are used to iterate over a VAR based array with the same iterator as the related CONST array.
  \{
*/ 
/**   \brief  type used to iterate BswM_ActionListQueue */
typedef BswM_ActionListsIterType BswM_ActionListQueueIterType;

/**   \brief  type used to iterate BswM_ModeRequestQueue */
typedef BswM_ImmediateUserIterType BswM_ModeRequestQueueIterType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCValueTypes  BswM Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for BswM_ActionsIdxOfActionItems */
typedef uint8 BswM_ActionsIdxOfActionItemsType;

/**   \brief  value based type definition for BswM_ParameterIdxOfActionItems */
typedef uint8 BswM_ParameterIdxOfActionItemsType;

/**   \brief  value based type definition for BswM_ParameterUsedOfActionItems */
typedef boolean BswM_ParameterUsedOfActionItemsType;

/**   \brief  value based type definition for BswM_ActionListQueue */
typedef uint8 BswM_ActionListQueueType;

/**   \brief  value based type definition for BswM_ActionItemsEndIdxOfActionLists */
typedef uint8 BswM_ActionItemsEndIdxOfActionListsType;

/**   \brief  value based type definition for BswM_ActionItemsStartIdxOfActionLists */
typedef uint8 BswM_ActionItemsStartIdxOfActionListsType;

/**   \brief  value based type definition for BswM_ActionItemsUsedOfActionLists */
typedef boolean BswM_ActionItemsUsedOfActionListsType;

/**   \brief  value based type definition for BswM_ConditionalOfActionLists */
typedef boolean BswM_ConditionalOfActionListsType;

/**   \brief  value based type definition for BswM_MaskedBitsOfActionLists */
typedef uint8 BswM_MaskedBitsOfActionListsType;

/**   \brief  value based type definition for BswM_ExternalIdOfCanSMChannelMapping */
typedef uint32 BswM_ExternalIdOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfCanSMChannelMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfCanSMChannelMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfCanSMChannelMapping */
typedef boolean BswM_ImmediateUserUsedOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_ComDMControlSubParametersEndIdxOfComDMControlParameters */
typedef uint8 BswM_ComDMControlSubParametersEndIdxOfComDMControlParametersType;

/**   \brief  value based type definition for BswM_ComDMControlSubParametersStartIdxOfComDMControlParameters */
typedef uint8 BswM_ComDMControlSubParametersStartIdxOfComDMControlParametersType;

/**   \brief  value based type definition for BswM_ComDMControlSubParametersUsedOfComDMControlParameters */
typedef boolean BswM_ComDMControlSubParametersUsedOfComDMControlParametersType;

/**   \brief  value based type definition for BswM_ComPduGroupSwitchSubParametersEndIdxOfComPduGroupSwitchParameters */
typedef uint8 BswM_ComPduGroupSwitchSubParametersEndIdxOfComPduGroupSwitchParametersType;

/**   \brief  value based type definition for BswM_ComPduGroupSwitchSubParametersStartIdxOfComPduGroupSwitchParameters */
typedef uint8 BswM_ComPduGroupSwitchSubParametersStartIdxOfComPduGroupSwitchParametersType;

/**   \brief  value based type definition for BswM_ComPduGroupSwitchSubParametersUsedOfComPduGroupSwitchParameters */
typedef boolean BswM_ComPduGroupSwitchSubParametersUsedOfComPduGroupSwitchParametersType;

/**   \brief  value based type definition for BswM_ControlOfComPduGroupSwitchParameters */
typedef uint8 BswM_ControlOfComPduGroupSwitchParametersType;

/**   \brief  value based type definition for BswM_RulesIdxOfDeferredRules */
typedef uint8 BswM_RulesIdxOfDeferredRulesType;

/**   \brief  value based type definition for BswM_InitValueOfEcuMModeMapping */
typedef uint8 BswM_InitValueOfEcuMModeMappingType;

/**   \brief  value based type definition for BswM_EcuMModeState */
typedef uint8 BswM_EcuMModeStateType;

/**   \brief  value based type definition for BswM_ExternalIdOfEcuMRunRequestMapping */
typedef uint32 BswM_ExternalIdOfEcuMRunRequestMappingType;

/**   \brief  value based type definition for BswM_ForcedActionListPriority */
typedef uint8 BswM_ForcedActionListPriorityType;

/**   \brief  value based type definition for BswM_ExternalIdOfGenericMapping */
typedef uint32 BswM_ExternalIdOfGenericMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserEndIdxOfGenericMapping */
typedef uint8 BswM_ImmediateUserEndIdxOfGenericMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserStartIdxOfGenericMapping */
typedef uint8 BswM_ImmediateUserStartIdxOfGenericMappingType;

/**   \brief  value based type definition for BswM_ImmediateUserUsedOfGenericMapping */
typedef boolean BswM_ImmediateUserUsedOfGenericMappingType;

/**   \brief  value based type definition for BswM_MaskedBitsOfImmediateUser */
typedef uint8 BswM_MaskedBitsOfImmediateUserType;

/**   \brief  value based type definition for BswM_OnInitOfImmediateUser */
typedef boolean BswM_OnInitOfImmediateUserType;

/**   \brief  value based type definition for BswM_RulesIndEndIdxOfImmediateUser */
typedef uint8 BswM_RulesIndEndIdxOfImmediateUserType;

/**   \brief  value based type definition for BswM_RulesIndStartIdxOfImmediateUser */
typedef uint8 BswM_RulesIndStartIdxOfImmediateUserType;

/**   \brief  value based type definition for BswM_RulesIndUsedOfImmediateUser */
typedef boolean BswM_RulesIndUsedOfImmediateUserType;

/**   \brief  value based type definition for BswM_InitActionLists */
typedef uint8 BswM_InitActionListsType;

/**   \brief  value based type definition for BswM_InitValues */
typedef uint16 BswM_InitValuesType;

/**   \brief  value based type definition for BswM_Initialized */
typedef boolean BswM_InitializedType;

/**   \brief  value based type definition for BswM_ModeRequestQueue */
typedef uint8 BswM_ModeRequestQueueType;

/**   \brief  value based type definition for BswM_ExternalIdOfNvMJobMapping */
typedef uint32 BswM_ExternalIdOfNvMJobMappingType;

/**   \brief  value based type definition for BswM_PCPartitionConfigIdxOfPartitionIdentifiers */
typedef uint8 BswM_PCPartitionConfigIdxOfPartitionIdentifiersType;

/**   \brief  value based type definition for BswM_PartitionSNVOfPartitionIdentifiers */
typedef uint32 BswM_PartitionSNVOfPartitionIdentifiersType;

/**   \brief  value based type definition for BswM_QueueSemaphore */
typedef uint8 BswM_QueueSemaphoreType;

/**   \brief  value based type definition for BswM_QueueWritten */
typedef boolean BswM_QueueWrittenType;

/**   \brief  value based type definition for BswM_RuleStates */
typedef uint8 BswM_RuleStatesType;

/**   \brief  value based type definition for BswM_ActionListsFalseIdxOfRules */
typedef uint8 BswM_ActionListsFalseIdxOfRulesType;

/**   \brief  value based type definition for BswM_ActionListsFalseUsedOfRules */
typedef boolean BswM_ActionListsFalseUsedOfRulesType;

/**   \brief  value based type definition for BswM_ActionListsTrueIdxOfRules */
typedef uint8 BswM_ActionListsTrueIdxOfRulesType;

/**   \brief  value based type definition for BswM_ActionListsTrueUsedOfRules */
typedef boolean BswM_ActionListsTrueUsedOfRulesType;

/**   \brief  value based type definition for BswM_ExpressionsIdxOfRules */
typedef uint8 BswM_ExpressionsIdxOfRulesType;

/**   \brief  value based type definition for BswM_IdOfRules */
typedef uint8 BswM_IdOfRulesType;

/**   \brief  value based type definition for BswM_InitOfRules */
typedef uint8 BswM_InitOfRulesType;

/**   \brief  value based type definition for BswM_MaskedBitsOfRules */
typedef uint8 BswM_MaskedBitsOfRulesType;

/**   \brief  value based type definition for BswM_RuleStatesIdxOfRules */
typedef uint8 BswM_RuleStatesIdxOfRulesType;

/**   \brief  value based type definition for BswM_RulesInd */
typedef uint8 BswM_RulesIndType;

/**   \brief  value based type definition for BswM_SizeOfActionItems */
typedef uint8 BswM_SizeOfActionItemsType;

/**   \brief  value based type definition for BswM_SizeOfActionListQueue */
typedef uint8 BswM_SizeOfActionListQueueType;

/**   \brief  value based type definition for BswM_SizeOfActionLists */
typedef uint8 BswM_SizeOfActionListsType;

/**   \brief  value based type definition for BswM_SizeOfActions */
typedef uint8 BswM_SizeOfActionsType;

/**   \brief  value based type definition for BswM_SizeOfCanSMChannelMapping */
typedef uint8 BswM_SizeOfCanSMChannelMappingType;

/**   \brief  value based type definition for BswM_SizeOfCanSMChannelState */
typedef uint8 BswM_SizeOfCanSMChannelStateType;

/**   \brief  value based type definition for BswM_SizeOfComDMControlParameters */
typedef uint8 BswM_SizeOfComDMControlParametersType;

/**   \brief  value based type definition for BswM_SizeOfComDMControlSubParameters */
typedef uint8 BswM_SizeOfComDMControlSubParametersType;

/**   \brief  value based type definition for BswM_SizeOfComPduGroupSwitchParameters */
typedef uint8 BswM_SizeOfComPduGroupSwitchParametersType;

/**   \brief  value based type definition for BswM_SizeOfComPduGroupSwitchSubParameters */
typedef uint8 BswM_SizeOfComPduGroupSwitchSubParametersType;

/**   \brief  value based type definition for BswM_SizeOfDeferredRules */
typedef uint8 BswM_SizeOfDeferredRulesType;

/**   \brief  value based type definition for BswM_SizeOfEcuMModeMapping */
typedef uint8 BswM_SizeOfEcuMModeMappingType;

/**   \brief  value based type definition for BswM_SizeOfEcuMRunRequestMapping */
typedef uint8 BswM_SizeOfEcuMRunRequestMappingType;

/**   \brief  value based type definition for BswM_SizeOfEcuMRunRequestState */
typedef uint8 BswM_SizeOfEcuMRunRequestStateType;

/**   \brief  value based type definition for BswM_SizeOfEcuMSelectShutdownTargetParameters */
typedef uint8 BswM_SizeOfEcuMSelectShutdownTargetParametersType;

/**   \brief  value based type definition for BswM_SizeOfEcuMStateSwitchParameters */
typedef uint8 BswM_SizeOfEcuMStateSwitchParametersType;

/**   \brief  value based type definition for BswM_SizeOfExpressions */
typedef uint8 BswM_SizeOfExpressionsType;

/**   \brief  value based type definition for BswM_SizeOfGenericMapping */
typedef uint8 BswM_SizeOfGenericMappingType;

/**   \brief  value based type definition for BswM_SizeOfGenericModeParameters */
typedef uint8 BswM_SizeOfGenericModeParametersType;

/**   \brief  value based type definition for BswM_SizeOfGenericState */
typedef uint8 BswM_SizeOfGenericStateType;

/**   \brief  value based type definition for BswM_SizeOfImmediateUser */
typedef uint8 BswM_SizeOfImmediateUserType;

/**   \brief  value based type definition for BswM_SizeOfInitActionLists */
typedef uint8 BswM_SizeOfInitActionListsType;

/**   \brief  value based type definition for BswM_SizeOfInitGenVarAndInitAL */
typedef uint8 BswM_SizeOfInitGenVarAndInitALType;

/**   \brief  value based type definition for BswM_SizeOfInitValues */
typedef uint8 BswM_SizeOfInitValuesType;

/**   \brief  value based type definition for BswM_SizeOfModeNotificationFct */
typedef uint8 BswM_SizeOfModeNotificationFctType;

/**   \brief  value based type definition for BswM_SizeOfModeRequestQueue */
typedef uint8 BswM_SizeOfModeRequestQueueType;

/**   \brief  value based type definition for BswM_SizeOfNvMJobMapping */
typedef uint8 BswM_SizeOfNvMJobMappingType;

/**   \brief  value based type definition for BswM_SizeOfNvMJobState */
typedef uint8 BswM_SizeOfNvMJobStateType;

/**   \brief  value based type definition for BswM_SizeOfPartitionIdentifiers */
typedef uint8 BswM_SizeOfPartitionIdentifiersType;

/**   \brief  value based type definition for BswM_SizeOfRuleStates */
typedef uint8 BswM_SizeOfRuleStatesType;

/**   \brief  value based type definition for BswM_SizeOfRules */
typedef uint8 BswM_SizeOfRulesType;

/**   \brief  value based type definition for BswM_SizeOfRulesInd */
typedef uint8 BswM_SizeOfRulesIndType;

/**   \brief  value based type definition for BswM_SizeOfSwcModeRequestUpdateFct */
typedef uint8 BswM_SizeOfSwcModeRequestUpdateFctType;

/**   \brief  value based type definition for BswM_SizeOfTimerControlParameters */
typedef uint8 BswM_SizeOfTimerControlParametersType;

/**   \brief  value based type definition for BswM_SizeOfTimerState */
typedef uint8 BswM_SizeOfTimerStateType;

/**   \brief  value based type definition for BswM_SizeOfTimerValue */
typedef uint8 BswM_SizeOfTimerValueType;

/**   \brief  value based type definition for BswM_TimerState */
typedef uint8 BswM_TimerStateType;

/**   \brief  value based type definition for BswM_TimerValue */
typedef uint32 BswM_TimerValueType;

/** 
  \}
*/ 


/* PRQA L:PRECOMPILEGLOBALDATATYPES */

typedef uint8 BswM_UserDomainType; /* user domain: CanSM, LinSM... */

/* It is possible to configure any user number from 0 to 65535 */
typedef uint16 BswM_UserType;
typedef uint16 BswM_ModeType;
typedef uint8 BswM_HandleType;
typedef P2FUNC (void, BSWM_CODE, BswM_InitGenVarAndInitALType)(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
typedef P2FUNC (uint8, BSWM_CODE, BswM_ExpressionFuncType)(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
typedef P2FUNC (Std_ReturnType, BSWM_CODE, BswM_ActionFuncType)(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);

typedef P2FUNC (void, BSWM_CODE, BswM_PartitionFunctionType)(void);

/* PRQA S 0639, 0779 PRECOMPILEGLOBALDATATYPES */ /* MD_MSR_1.1_639, MD_MSR_Rule5.2_0779 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCStructTypes  BswM Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in BswM_ActionItems */
typedef struct sBswM_ActionItemsType
{
  BswM_ActionsIdxOfActionItemsType ActionsIdxOfActionItems;  /**< the index of the 1:1 relation pointing to BswM_Actions */
  BswM_ParameterIdxOfActionItemsType ParameterIdxOfActionItems;  /**< the index of the 0:1 relation pointing to BswM_ComMAllowComParameters,BswM_ComMModeLimitationParameters,BswM_ComMModeSwitchParameters,BswM_ComDMControlParameters,BswM_EcuMSelectShutdownTargetParameters,BswM_EcuMStateSwitchParameters,BswM_LinScheduleRequestParameters,BswM_NmControlParameters,BswM_ComSwitchIPduModeParameters,BswM_ComPduGroupSwitchParameters,BswM_ComPduGroupHandlingParameters,BswM_ComDMHandlingParameters,BswM_ComTriggerIPduSendParameters,BswM_PduRouterControlParameters,BswM_TimerControlParameters,BswM_GenericModeParameters,BswM_GenericModeRefParameters,BswM_J1939DcmStateParameters,BswM_J1939RmStateParameters,BswM_SdClientParameters,BswM_SdConsumedParameters,BswM_SdServerParameters,BswM_RuleControlParameters,BswM_ActionLists,BswM_Rules */
} BswM_ActionItemsType;

/**   \brief  type used in BswM_ActionLists */
typedef struct sBswM_ActionListsType
{
  BswM_ActionItemsEndIdxOfActionListsType ActionItemsEndIdxOfActionLists;  /**< the end index of the 0:n relation pointing to BswM_ActionItems */
  BswM_ActionItemsStartIdxOfActionListsType ActionItemsStartIdxOfActionLists;  /**< the start index of the 0:n relation pointing to BswM_ActionItems */
  BswM_MaskedBitsOfActionListsType MaskedBitsOfActionLists;  /**< contains bitcoded the boolean data of BswM_ActionItemsUsedOfActionLists, BswM_ConditionalOfActionLists */
} BswM_ActionListsType;

/**   \brief  type used in BswM_CanSMChannelMapping */
typedef struct sBswM_CanSMChannelMappingType
{
  BswM_ExternalIdOfCanSMChannelMappingType ExternalIdOfCanSMChannelMapping;  /**< External id of BswMCanSMIndication. */
  BswM_ImmediateUserEndIdxOfCanSMChannelMappingType ImmediateUserEndIdxOfCanSMChannelMapping;  /**< the end index of the 0:n relation pointing to BswM_ImmediateUser */
  BswM_ImmediateUserStartIdxOfCanSMChannelMappingType ImmediateUserStartIdxOfCanSMChannelMapping;  /**< the start index of the 0:n relation pointing to BswM_ImmediateUser */
} BswM_CanSMChannelMappingType;

/**   \brief  type used in BswM_ComDMControlParameters */
typedef struct sBswM_ComDMControlParametersType
{
  BswM_ComDMControlSubParametersEndIdxOfComDMControlParametersType ComDMControlSubParametersEndIdxOfComDMControlParameters;  /**< the end index of the 0:n relation pointing to BswM_ComDMControlSubParameters */
  BswM_ComDMControlSubParametersStartIdxOfComDMControlParametersType ComDMControlSubParametersStartIdxOfComDMControlParameters;  /**< the start index of the 0:n relation pointing to BswM_ComDMControlSubParameters */
} BswM_ComDMControlParametersType;

/**   \brief  type used in BswM_ComDMControlSubParameters */
typedef struct sBswM_ComDMControlSubParametersType
{
  Com_IpduGroupIdType IpduGroupIdOfComDMControlSubParameters;
  boolean BitValOfComDMControlSubParameters;
} BswM_ComDMControlSubParametersType;

/**   \brief  type used in BswM_ComPduGroupSwitchParameters */
typedef struct sBswM_ComPduGroupSwitchParametersType
{
  BswM_ComPduGroupSwitchSubParametersEndIdxOfComPduGroupSwitchParametersType ComPduGroupSwitchSubParametersEndIdxOfComPduGroupSwitchParameters;  /**< the end index of the 0:n relation pointing to BswM_ComPduGroupSwitchSubParameters */
  BswM_ComPduGroupSwitchSubParametersStartIdxOfComPduGroupSwitchParametersType ComPduGroupSwitchSubParametersStartIdxOfComPduGroupSwitchParameters;  /**< the start index of the 0:n relation pointing to BswM_ComPduGroupSwitchSubParameters */
} BswM_ComPduGroupSwitchParametersType;

/**   \brief  type used in BswM_ComPduGroupSwitchSubParameters */
typedef struct sBswM_ComPduGroupSwitchSubParametersType
{
  Com_IpduGroupIdType IpduGroupIdOfComPduGroupSwitchSubParameters;
  boolean BitValOfComPduGroupSwitchSubParameters;
} BswM_ComPduGroupSwitchSubParametersType;

/**   \brief  type used in BswM_DeferredRules */
typedef struct sBswM_DeferredRulesType
{
  BswM_RulesIdxOfDeferredRulesType RulesIdxOfDeferredRules;  /**< the index of the 1:1 relation pointing to BswM_Rules */
} BswM_DeferredRulesType;

/**   \brief  type used in BswM_EcuMModeMapping */
typedef struct sBswM_EcuMModeMappingType
{
  uint8 BswM_EcuMModeMappingNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_EcuMModeMappingType;

/**   \brief  type used in BswM_EcuMRunRequestMapping */
typedef struct sBswM_EcuMRunRequestMappingType
{
  BswM_ExternalIdOfEcuMRunRequestMappingType ExternalIdOfEcuMRunRequestMapping;  /**< External id of BswMEcuMRUNRequestIndication. */
} BswM_EcuMRunRequestMappingType;

/**   \brief  type used in BswM_EcuMSelectShutdownTargetParameters */
typedef struct sBswM_EcuMSelectShutdownTargetParametersType
{
  uint8 BswM_EcuMSelectShutdownTargetParametersNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_EcuMSelectShutdownTargetParametersType;

/**   \brief  type used in BswM_EcuMStateSwitchParameters */
typedef struct sBswM_EcuMStateSwitchParametersType
{
  EcuM_StateType TargetStateOfEcuMStateSwitchParameters;
} BswM_EcuMStateSwitchParametersType;

/**   \brief  type used in BswM_GenericMapping */
typedef struct sBswM_GenericMappingType
{
  uint8 BswM_GenericMappingNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_GenericMappingType;

/**   \brief  type used in BswM_GenericModeParameters */
typedef struct sBswM_GenericModeParametersType
{
  BswM_ModeType ModeOfGenericModeParameters;
} BswM_GenericModeParametersType;

/**   \brief  type used in BswM_ImmediateUser */
typedef struct sBswM_ImmediateUserType
{
  BswM_MaskedBitsOfImmediateUserType MaskedBitsOfImmediateUser;  /**< contains bitcoded the boolean data of BswM_OnInitOfImmediateUser, BswM_RulesIndUsedOfImmediateUser */
  BswM_RulesIndEndIdxOfImmediateUserType RulesIndEndIdxOfImmediateUser;  /**< the end index of the 0:n relation pointing to BswM_RulesInd */
  BswM_RulesIndStartIdxOfImmediateUserType RulesIndStartIdxOfImmediateUser;  /**< the start index of the 0:n relation pointing to BswM_RulesInd */
} BswM_ImmediateUserType;

/**   \brief  type used in BswM_NvMJobMapping */
typedef struct sBswM_NvMJobMappingType
{
  uint8 BswM_NvMJobMappingNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_NvMJobMappingType;

/**   \brief  type used in BswM_PartitionIdentifiers */
typedef struct sBswM_PartitionIdentifiersType
{
  BswM_PartitionSNVOfPartitionIdentifiersType PartitionSNVOfPartitionIdentifiers;
  BswM_PCPartitionConfigIdxOfPartitionIdentifiersType PCPartitionConfigIdxOfPartitionIdentifiers;  /**< the index of the 1:1 relation pointing to BswM_PCPartitionConfig */
} BswM_PartitionIdentifiersType;

/**   \brief  type used in BswM_Rules */
typedef struct sBswM_RulesType
{
  BswM_ActionListsFalseIdxOfRulesType ActionListsFalseIdxOfRules;  /**< the index of the 0:1 relation pointing to BswM_ActionLists */
  BswM_ActionListsTrueIdxOfRulesType ActionListsTrueIdxOfRules;  /**< the index of the 0:1 relation pointing to BswM_ActionLists */
  BswM_ExpressionsIdxOfRulesType ExpressionsIdxOfRules;  /**< the index of the 1:1 relation pointing to BswM_Expressions */
  BswM_IdOfRulesType IdOfRules;  /**< External id of rule. */
  BswM_MaskedBitsOfRulesType MaskedBitsOfRules;  /**< contains bitcoded the boolean data of BswM_ActionListsFalseUsedOfRules, BswM_ActionListsTrueUsedOfRules */
  BswM_RuleStatesIdxOfRulesType RuleStatesIdxOfRules;  /**< the index of the 1:1 relation pointing to BswM_RuleStates */
} BswM_RulesType;

/**   \brief  type used in BswM_TimerControlParameters */
typedef struct sBswM_TimerControlParametersType
{
  uint32 ValueOfTimerControlParameters;
  uint8 TimerOfTimerControlParameters;
} BswM_TimerControlParametersType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCSymbolicStructTypes  BswM Symbolic Struct Types (PRE_COMPILE)
  \brief  These structs are used in unions to have a symbol based data representation style.
  \{
*/ 
/**   \brief  type to be used as symbolic data element access to BswM_ActionListQueue in the predefined variant Left in the partition context  */
typedef struct BswM_ActionListQueueLeftStructSTag
{
  BswM_ActionListQueueType AL_INIT_AL_Initialize;
  BswM_ActionListQueueType AL_ESH_AL_RunToPostRun;
  BswM_ActionListQueueType AL_ESH_AL_WaitForNvMToShutdown;
  BswM_ActionListQueueType AL_ESH_AL_WakeupToPrep;
  BswM_ActionListQueueType AL_ESH_AL_WaitForNvMWakeup;
  BswM_ActionListQueueType AL_ESH_AL_WakeupToRun;
  BswM_ActionListQueueType AL_ESH_AL_InitToWakeup;
  BswM_ActionListQueueType AL_ESH_AL_PostRunToPrepShutdown;
  BswM_ActionListQueueType AL_ESH_AL_ESH_PostRunToPrepCheck;
  BswM_ActionListQueueType AL_ESH_AL_PostRunToRun;
  BswM_ActionListQueueType AL_ESH_AL_ExitPostRun;
  BswM_ActionListQueueType AL_ESH_AL_PrepShutdownToWaitForNvM;
  BswM_ActionListQueueType AL_CC_AL_CN_Int_CAN_b597612f_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_Int_CAN_b597612f_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_E_CAN_7f812c72_RX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_E_CAN_7f812c72_RX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_E_CAN_7f812c72_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_E_CAN_7f812c72_Enable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_Int_CAN_b597612f_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_Int_CAN_b597612f_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_E_CAN_7f812c72_TX_Disable;
  BswM_ActionListQueueType AL_CC_AL_CN_E_CAN_7f812c72_TX_EnableNoinit;
  BswM_ActionListQueueType AL_CC_AL_CN_Int_CAN_b597612f_Disable_DM;
  BswM_ActionListQueueType AL_CC_AL_CN_Int_CAN_b597612f_Enable_DM;
  BswM_ActionListQueueType AL_BswMActionList_ProgReset;
  BswM_ActionListQueueType AL_BswMActionList_ProgResetFail;
} BswM_ActionListQueueLeftStructSType;

/**   \brief  type to be used as symbolic data element access to BswM_RuleStates in the predefined variant Left in the partition context  */
typedef struct BswM_RuleStatesLeftStructSTag
{
  BswM_RuleStatesType R_ESH_RunToPostRun;
  BswM_RuleStatesType R_ESH_WaitToShutdown;
  BswM_RuleStatesType R_ESH_WakeupToPrep;
  BswM_RuleStatesType R_ESH_WaitToWakeup;
  BswM_RuleStatesType R_ESH_WakeupToRun;
  BswM_RuleStatesType R_ESH_InitToWakeup;
  BswM_RuleStatesType R_ESH_PostRunToPrepNested;
  BswM_RuleStatesType R_ESH_PostRunNested;
  BswM_RuleStatesType R_ESH_PostRun;
  BswM_RuleStatesType R_ESH_PrepToWait;
  BswM_RuleStatesType R_CC_CN_Int_CAN_b597612f_RX;
  BswM_RuleStatesType R_CC_CN_E_CAN_7f812c72_RX;
  BswM_RuleStatesType R_CC_CN_E_CAN_7f812c72_RX_DM;
  BswM_RuleStatesType R_CC_CN_Int_CAN_b597612f_TX;
  BswM_RuleStatesType R_CC_CN_E_CAN_7f812c72_TX;
  BswM_RuleStatesType R_CC_CN_Int_CAN_b597612f_RX_DM;
  BswM_RuleStatesType R_BswMRule_ProgReset;
} BswM_RuleStatesLeftStructSType;

/**   \brief  type to be used as symbolic data element access to BswM_TimerState in the predefined variant Left in the partition context  */
typedef struct BswM_TimerStateLeftStructSTag
{
  BswM_TimerStateType MRP_ESH_NvM_CancelWriteAllTimer;
  BswM_TimerStateType MRP_ESH_NvM_WriteAllTimer;
  BswM_TimerStateType MRP_ESH_SelfRunRequestTimer;
} BswM_TimerStateLeftStructSType;

/**   \brief  type to be used as symbolic data element access to BswM_TimerValue in the predefined variant Left in the partition context  */
typedef struct BswM_TimerValueLeftStructSTag
{
  BswM_TimerValueType MRP_ESH_NvM_CancelWriteAllTimer;
  BswM_TimerValueType MRP_ESH_NvM_WriteAllTimer;
  BswM_TimerValueType MRP_ESH_SelfRunRequestTimer;
} BswM_TimerValueLeftStructSType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCUnionIndexAndSymbolTypes  BswM Union Index And Symbol Types (PRE_COMPILE)
  \brief  These unions are used to access arrays in an index and symbol based style.
  \{
*/ 
/**   \brief  type to access BswM_ActionListQueue in an index and symbol based style. */
typedef union BswM_ActionListQueueUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_ActionListQueueType raw[26];
  BswM_ActionListQueueLeftStructSType strLeft;
} BswM_ActionListQueueUType;

/**   \brief  type to access BswM_RuleStates in an index and symbol based style. */
typedef union BswM_RuleStatesUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_RuleStatesType raw[17];
  BswM_RuleStatesLeftStructSType strLeft;
} BswM_RuleStatesUType;

/**   \brief  type to access BswM_TimerState in an index and symbol based style. */
typedef union BswM_TimerStateUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_TimerStateType raw[3];
  BswM_TimerStateLeftStructSType strLeft;
} BswM_TimerStateUType;

/**   \brief  type to access BswM_TimerValue in an index and symbol based style. */
typedef union BswM_TimerValueUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  BswM_TimerValueType raw[3];
  BswM_TimerValueLeftStructSType strLeft;
} BswM_TimerValueUType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCRootPointerTypes  BswM Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to BswM_ActionItems */
typedef P2CONST(BswM_ActionItemsType, TYPEDEF, BSWM_CONST) BswM_ActionItemsPtrType;

/**   \brief  type used to point to BswM_ActionListQueue */
typedef P2VAR(BswM_ActionListQueueType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ActionListQueuePtrType;

/**   \brief  type used to point to BswM_ActionLists */
typedef P2CONST(BswM_ActionListsType, TYPEDEF, BSWM_CONST) BswM_ActionListsPtrType;

/**   \brief  type used to point to BswM_Actions */
typedef P2CONST(BswM_ActionFuncType, TYPEDEF, BSWM_CONST) BswM_ActionsPtrType;

/**   \brief  type used to point to BswM_CanSMChannelMapping */
typedef P2CONST(BswM_CanSMChannelMappingType, TYPEDEF, BSWM_CONST) BswM_CanSMChannelMappingPtrType;

/**   \brief  type used to point to BswM_CanSMChannelState */
typedef P2VAR(CanSM_BswMCurrentStateType, TYPEDEF, BSWM_VAR_NOINIT) BswM_CanSMChannelStatePtrType;

/**   \brief  type used to point to BswM_ComDMControlParameters */
typedef P2CONST(BswM_ComDMControlParametersType, TYPEDEF, BSWM_CONST) BswM_ComDMControlParametersPtrType;

/**   \brief  type used to point to BswM_ComDMControlSubParameters */
typedef P2CONST(BswM_ComDMControlSubParametersType, TYPEDEF, BSWM_CONST) BswM_ComDMControlSubParametersPtrType;

/**   \brief  type used to point to BswM_ComPduGroupSwitchParameters */
typedef P2CONST(BswM_ComPduGroupSwitchParametersType, TYPEDEF, BSWM_CONST) BswM_ComPduGroupSwitchParametersPtrType;

/**   \brief  type used to point to BswM_ComPduGroupSwitchSubParameters */
typedef P2CONST(BswM_ComPduGroupSwitchSubParametersType, TYPEDEF, BSWM_CONST) BswM_ComPduGroupSwitchSubParametersPtrType;

/**   \brief  type used to point to BswM_DeferredRules */
typedef P2CONST(BswM_DeferredRulesType, TYPEDEF, BSWM_CONST) BswM_DeferredRulesPtrType;

/**   \brief  type used to point to BswM_EcuMModeMapping */
typedef P2CONST(BswM_EcuMModeMappingType, TYPEDEF, BSWM_CONST) BswM_EcuMModeMappingPtrType;

/**   \brief  type used to point to BswM_EcuMModeState */
typedef P2VAR(BswM_EcuMModeStateType, TYPEDEF, BSWM_VAR_NOINIT) BswM_EcuMModeStatePtrType;

/**   \brief  type used to point to BswM_EcuMRunRequestMapping */
typedef P2CONST(BswM_EcuMRunRequestMappingType, TYPEDEF, BSWM_CONST) BswM_EcuMRunRequestMappingPtrType;

/**   \brief  type used to point to BswM_EcuMRunRequestState */
typedef P2VAR(EcuM_RunStatusType, TYPEDEF, BSWM_VAR_NOINIT) BswM_EcuMRunRequestStatePtrType;

/**   \brief  type used to point to BswM_EcuMSelectShutdownTargetParameters */
typedef P2CONST(BswM_EcuMSelectShutdownTargetParametersType, TYPEDEF, BSWM_CONST) BswM_EcuMSelectShutdownTargetParametersPtrType;

/**   \brief  type used to point to BswM_EcuMStateSwitchParameters */
typedef P2CONST(BswM_EcuMStateSwitchParametersType, TYPEDEF, BSWM_CONST) BswM_EcuMStateSwitchParametersPtrType;

/**   \brief  type used to point to BswM_Expressions */
typedef P2CONST(BswM_ExpressionFuncType, TYPEDEF, BSWM_CONST) BswM_ExpressionsPtrType;

/**   \brief  type used to point to BswM_ForcedActionListPriority */
typedef P2VAR(BswM_ForcedActionListPriorityType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ForcedActionListPriorityPtrType;

/**   \brief  type used to point to BswM_GenericMapping */
typedef P2CONST(BswM_GenericMappingType, TYPEDEF, BSWM_CONST) BswM_GenericMappingPtrType;

/**   \brief  type used to point to BswM_GenericModeParameters */
typedef P2CONST(BswM_GenericModeParametersType, TYPEDEF, BSWM_CONST) BswM_GenericModeParametersPtrType;

/**   \brief  type used to point to BswM_GenericState */
typedef P2VAR(BswM_ModeType, TYPEDEF, BSWM_VAR_NOINIT) BswM_GenericStatePtrType;

/**   \brief  type used to point to BswM_ImmediateUser */
typedef P2CONST(BswM_ImmediateUserType, TYPEDEF, BSWM_CONST) BswM_ImmediateUserPtrType;

/**   \brief  type used to point to BswM_InitActionLists */
typedef P2CONST(BswM_InitActionListsType, TYPEDEF, BSWM_CONST) BswM_InitActionListsPtrType;

/**   \brief  type used to point to BswM_InitGenVarAndInitAL */
typedef P2CONST(BswM_InitGenVarAndInitALType, TYPEDEF, BSWM_CONST) BswM_InitGenVarAndInitALPtrType;

/**   \brief  type used to point to BswM_InitValues */
typedef P2CONST(BswM_InitValuesType, TYPEDEF, BSWM_CONST) BswM_InitValuesPtrType;

/**   \brief  type used to point to BswM_Initialized */
typedef P2VAR(BswM_InitializedType, TYPEDEF, BSWM_VAR_NOINIT) BswM_InitializedPtrType;

/**   \brief  type used to point to BswM_ModeNotificationFct */
typedef P2CONST(BswM_PartitionFunctionType, TYPEDEF, BSWM_CONST) BswM_ModeNotificationFctPtrType;

/**   \brief  type used to point to BswM_ModeRequestQueue */
typedef P2VAR(BswM_ModeRequestQueueType, TYPEDEF, BSWM_VAR_NOINIT) BswM_ModeRequestQueuePtrType;

/**   \brief  type used to point to BswM_NvMJobMapping */
typedef P2CONST(BswM_NvMJobMappingType, TYPEDEF, BSWM_CONST) BswM_NvMJobMappingPtrType;

/**   \brief  type used to point to BswM_NvMJobState */
typedef P2VAR(NvM_RequestResultType, TYPEDEF, BSWM_VAR_NOINIT) BswM_NvMJobStatePtrType;

/**   \brief  type used to point to BswM_PartitionIdentifiers */
typedef P2CONST(BswM_PartitionIdentifiersType, TYPEDEF, BSWM_CONST) BswM_PartitionIdentifiersPtrType;

/**   \brief  type used to point to BswM_QueueSemaphore */
typedef P2VAR(BswM_QueueSemaphoreType, TYPEDEF, BSWM_VAR_NOINIT) BswM_QueueSemaphorePtrType;

/**   \brief  type used to point to BswM_QueueWritten */
typedef P2VAR(BswM_QueueWrittenType, TYPEDEF, BSWM_VAR_NOINIT) BswM_QueueWrittenPtrType;

/**   \brief  type used to point to BswM_RuleStates */
typedef P2VAR(BswM_RuleStatesType, TYPEDEF, BSWM_VAR_NOINIT) BswM_RuleStatesPtrType;

/**   \brief  type used to point to BswM_Rules */
typedef P2CONST(BswM_RulesType, TYPEDEF, BSWM_CONST) BswM_RulesPtrType;

/**   \brief  type used to point to BswM_RulesInd */
typedef P2CONST(BswM_RulesIndType, TYPEDEF, BSWM_CONST) BswM_RulesIndPtrType;

/**   \brief  type used to point to BswM_SwcModeRequestUpdateFct */
typedef P2CONST(BswM_PartitionFunctionType, TYPEDEF, BSWM_CONST) BswM_SwcModeRequestUpdateFctPtrType;

/**   \brief  type used to point to BswM_TimerControlParameters */
typedef P2CONST(BswM_TimerControlParametersType, TYPEDEF, BSWM_CONST) BswM_TimerControlParametersPtrType;

/**   \brief  type used to point to BswM_TimerState */
typedef P2VAR(BswM_TimerStateType, TYPEDEF, BSWM_VAR_NOINIT) BswM_TimerStatePtrType;

/**   \brief  type used to point to BswM_TimerValue */
typedef P2VAR(BswM_TimerValueType, TYPEDEF, BSWM_VAR_NOINIT) BswM_TimerValuePtrType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCPartitionRootPointer  BswM Partition Root Pointer (PRE_COMPILE)
  \brief  This type definitions are used for partition specific instance.
  \{
*/ 
/**   \brief  type used in BswM_PCPartitionConfig */
typedef struct sBswM_PCPartitionConfigType
{
  uint8 BswM_PCPartitionConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_PCPartitionConfigType;

/**   \brief  type used to point to BswM_PCPartitionConfig */
typedef P2CONST(BswM_PCPartitionConfigType, TYPEDEF, BSWM_CONST) BswM_PCPartitionConfigPtrType;

/** 
  \}
*/ 

/** 
  \defgroup  BswMPCRootValueTypes  BswM Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in BswM_PCConfig */
typedef struct sBswM_PCConfigType
{
  uint8 BswM_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} BswM_PCConfigType;

typedef BswM_PCConfigType BswM_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/* PRQA L:PRECOMPILEGLOBALDATATYPES */





/* PRQA S 0639, 0779 POSTBUILDGLOBALDATATYPES */ /* MD_MSR_1.1_639, MD_MSR_Rule5.2_0779 */ 
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/* PRQA L:POSTBUILDGLOBALDATATYPES */

/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/* PRQA S 3449, 3451 EXTERNDECLARATIONS */ /* MD_BSWM_3449, MD_BSWM_3451 */ 

/* PRQA L:EXTERNDECLARATIONS */



/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  BswM_OsApplicationToBswM
**********************************************************************************************************************/


#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

extern VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode;
extern VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_RunRequest_0_requestedMode;
extern VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_RunRequest_1_requestedMode;
extern VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_PostRunRequest_0_requestedMode;
extern VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_PostRunRequest_1_requestedMode;
extern VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode;
extern VAR(Rte_ModeType_DcmEcuReset, BSWM_VAR_NOINIT) BswM_Mode_Notification_ResetsModeNotification_DcmEcuReset;


extern VAR(boolean, BSWM_VAR_NOINIT) BswM_PreInitialized;
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
#define BSWM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* -----------------------------------------------------------------------------
&&&~ USER CALLOUT DECLARATIONS
----------------------------------------------------------------------------- */
/* PRQA S 0777 CALLOUTDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */
extern FUNC(void, BSWM_CODE) BswM_AL_SetProgrammableInterrupts(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterPostRun(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterPrepShutdown(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterRun(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterShutdown(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterWaitForNvm(void);
extern FUNC(void, BSWM_CODE) BswM_ESH_OnEnterWakeup(void);
extern FUNC(void, BSWM_CODE) BswM_INIT_NvMReadAll(void);
extern FUNC(void, BSWM_CODE) NvM_Call_PreInit(void);
/* PRQA L:CALLOUTDECLARATIONS */

/* -----------------------------------------------------------------------------
&&&~ USER CALLOUT DECLARATIONS
----------------------------------------------------------------------------- */
#define BSWM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

#endif /* BSWM_CFG_H */

