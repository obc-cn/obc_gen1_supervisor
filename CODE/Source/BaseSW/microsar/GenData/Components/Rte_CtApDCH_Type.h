/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApDCH_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApDCH>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPDCH_TYPE_H
# define RTE_CTAPDCH_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_not_in_active_discharge
#   define Cx0_not_in_active_discharge (0U)
#  endif

#  ifndef Cx1_active_discharging
#   define Cx1_active_discharging (1U)
#  endif

#  ifndef Cx2_discharge_completed
#   define Cx2_discharge_completed (2U)
#  endif

#  ifndef Cx3_discharge_failure
#   define Cx3_discharge_failure (3U)
#  endif

#  ifndef Cx0_off_mode
#   define Cx0_off_mode (0U)
#  endif

#  ifndef Cx1_Init_mode
#   define Cx1_Init_mode (1U)
#  endif

#  ifndef Cx2_standby_mode
#   define Cx2_standby_mode (2U)
#  endif

#  ifndef Cx3_conversion_working_
#   define Cx3_conversion_working_ (3U)
#  endif

#  ifndef Cx4_error_mode
#   define Cx4_error_mode (4U)
#  endif

#  ifndef Cx5_degradation_mode
#   define Cx5_degradation_mode (5U)
#  endif

#  ifndef Cx6_reserved
#   define Cx6_reserved (6U)
#  endif

#  ifndef Cx7_invalid
#   define Cx7_invalid (7U)
#  endif

#  ifndef POST_ONGOING
#   define POST_ONGOING (0U)
#  endif

#  ifndef POST_OK
#   define POST_OK (1U)
#  endif

#  ifndef POST_NOK
#   define POST_NOK (2U)
#  endif

#  ifndef Cx0_No_AC_10V_
#   define Cx0_No_AC_10V_ (0U)
#  endif

#  ifndef Cx1_110V_85_132V_
#   define Cx1_110V_85_132V_ (1U)
#  endif

#  ifndef Cx2_Invalid
#   define Cx2_Invalid (2U)
#  endif

#  ifndef Cx3_220V_170_265V_
#   define Cx3_220V_170_265V_ (3U)
#  endif

#  ifndef Cx4_Reserved
#   define Cx4_Reserved (4U)
#  endif

#  ifndef Cx5_Reserved
#   define Cx5_Reserved (5U)
#  endif

#  ifndef Cx6_Reserved
#   define Cx6_Reserved (6U)
#  endif

#  ifndef Cx7_Reserved
#   define Cx7_Reserved (7U)
#  endif

#  ifndef Cx0_PFC_STATE_STANDBY
#   define Cx0_PFC_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_PFC_STATE_TRI
#   define Cx1_PFC_STATE_TRI (1U)
#  endif

#  ifndef Cx2_PFC_STATE_MONO
#   define Cx2_PFC_STATE_MONO (2U)
#  endif

#  ifndef Cx3_PFC_STATE_RESET_ERROR
#   define Cx3_PFC_STATE_RESET_ERROR (3U)
#  endif

#  ifndef Cx4_PFC_STATE_DISCHARGE
#   define Cx4_PFC_STATE_DISCHARGE (4U)
#  endif

#  ifndef Cx5_PFC_STATE_SHUTDOWN
#   define Cx5_PFC_STATE_SHUTDOWN (5U)
#  endif

#  ifndef Cx6_PFC_STATE_START_TRI
#   define Cx6_PFC_STATE_START_TRI (6U)
#  endif

#  ifndef Cx7_PFC_STATE_START_MONO
#   define Cx7_PFC_STATE_START_MONO (7U)
#  endif

#  ifndef Cx8_PFC_STATE_ERROR
#   define Cx8_PFC_STATE_ERROR (8U)
#  endif

#  ifndef Cx9_PFC_STATE_DIRECTPWM
#   define Cx9_PFC_STATE_DIRECTPWM (9U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPDCH_TYPE_H */
