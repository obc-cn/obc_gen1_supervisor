/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApOBC.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApOBC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPOBC_H
# define RTE_CTAPOBC_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApOBC_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC;
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference;
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference;
extern VAR(DCDC_VoltageReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference;
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed;
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed;
extern VAR(SUP_CommandVDCLink_V, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V;
extern VAR(SUP_RequestPFCStatus, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus;
extern VAR(SUP_RequestStatusDCHV, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode;
extern VAR(BMS_HighestChargeCurrentAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow;
extern VAR(BMS_HighestChargeVoltageAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow;
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCHV_Command_Current_Reference, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
extern VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status;
extern VAR(PFC_IPH1_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1;
extern VAR(PFC_IPH2_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1;
extern VAR(PFC_IPH3_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1;
extern VAR(PFC_PFCStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus;
extern VAR(PFC_Vdclink_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
extern VAR(IdtMaxInputACCurrentEVSE, RTE_VAR_INIT) Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpStopConditions_DeStopConditions;
extern VAR(IdtVoltageCorrectionOffset, RTE_VAR_INIT) Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(OBC_PowerMax, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC (FALSE)
#  define Rte_InitValue_PpInt_DCDC_CurrentReference_DCDC_CurrentReference (0U)
#  define Rte_InitValue_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference (0U)
#  define Rte_InitValue_PpInt_DCDC_VoltageReference_DCDC_VoltageReference (2100U)
#  define Rte_InitValue_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed (FALSE)
#  define Rte_InitValue_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed (FALSE)
#  define Rte_InitValue_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V (0U)
#  define Rte_InitValue_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus (0U)
#  define Rte_InitValue_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV (0U)
#  define Rte_InitValue_PpOBC_POST_Result_DeOBC_POST_Result (0U)
#  define Rte_InitValue_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault (FALSE)
#  define Rte_InitValue_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue (FALSE)
#  define Rte_InitValue_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue (TRUE)
#  define Rte_InitValue_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop Rte_Read_CtApOBC_PpFaultChargeSoftStop_DeFaultChargeSoftStop
#  define Rte_Read_CtApOBC_PpFaultChargeSoftStop_DeFaultChargeSoftStop(data) (*(data) = Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue Rte_Read_CtApOBC_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue
#  define Rte_Read_CtApOBC_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInputVoltageMode_DeInputVoltageMode Rte_Read_CtApOBC_PpInputVoltageMode_DeInputVoltageMode
#  define Rte_Read_CtApOBC_PpInputVoltageMode_DeInputVoltageMode(data) (*(data) = Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow Rte_Read_CtApOBC_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow
#  define Rte_Read_CtApOBC_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow Rte_Read_CtApOBC_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow
#  define Rte_Read_CtApOBC_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Read_CtApOBC_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Read_CtApOBC_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApOBC_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApOBC_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference Rte_Read_CtApOBC_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference
#  define Rte_Read_CtApOBC_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus Rte_Read_CtApOBC_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus
#  define Rte_Read_CtApOBC_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApOBC_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApOBC_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt Rte_Read_CtApOBC_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Read_CtApOBC_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax Rte_Read_CtApOBC_PpInt_OBC_PowerMax_OBC_PowerMax
#  define Rte_Read_CtApOBC_PpInt_OBC_PowerMax_OBC_PowerMax(data) (*(data) = Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_OBC_Status Rte_Read_CtApOBC_PpInt_OBC_Status_OBC_Status
#  define Rte_Read_CtApOBC_PpInt_OBC_Status_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 Rte_Read_CtApOBC_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1
#  define Rte_Read_CtApOBC_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 Rte_Read_CtApOBC_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1
#  define Rte_Read_CtApOBC_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 Rte_Read_CtApOBC_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1
#  define Rte_Read_CtApOBC_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus Rte_Read_CtApOBC_PpInt_PFC_PFCStatus_PFC_PFCStatus
#  define Rte_Read_CtApOBC_PpInt_PFC_PFCStatus_PFC_PFCStatus(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V Rte_Read_CtApOBC_PpInt_PFC_Vdclink_V_PFC_Vdclink_V
#  define Rte_Read_CtApOBC_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE Rte_Read_CtApOBC_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE
#  define Rte_Read_CtApOBC_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(data) (*(data) = Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue Rte_Read_CtApOBC_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue
#  define Rte_Read_CtApOBC_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(data) (*(data) = Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays Rte_Read_CtApOBC_PpOutputPrechargeRelays_DeOutputPrechargeRelays
#  define Rte_Read_CtApOBC_PpOutputPrechargeRelays_DeOutputPrechargeRelays(data) (*(data) = Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled Rte_Read_CtApOBC_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled
#  define Rte_Read_CtApOBC_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data) (*(data) = Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpRequestHWStopOBC_DeRequestHWStopOBC Rte_Read_CtApOBC_PpRequestHWStopOBC_DeRequestHWStopOBC
#  define Rte_Read_CtApOBC_PpRequestHWStopOBC_DeRequestHWStopOBC(data) (*(data) = Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpStopConditions_DeStopConditions Rte_Read_CtApOBC_PpStopConditions_DeStopConditions
#  define Rte_Read_CtApOBC_PpStopConditions_DeStopConditions(data) (*(data) = Rte_CpApCHG_PpStopConditions_DeStopConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset Rte_Read_CtApOBC_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset
#  define Rte_Read_CtApOBC_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(data) (*(data) = Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC Rte_Write_CtApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC
#  define Rte_Write_CtApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(data) (Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_CurrentReference_DCDC_CurrentReference Rte_Write_CtApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference
#  define Rte_Write_CtApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(data) (Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference Rte_Write_CtApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference
#  define Rte_Write_CtApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(data) (Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_VoltageReference_DCDC_VoltageReference Rte_Write_CtApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference
#  define Rte_Write_CtApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(data) (Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed Rte_Write_CtApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed
#  define Rte_Write_CtApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(data) (Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed Rte_Write_CtApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed
#  define Rte_Write_CtApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(data) (Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V Rte_Write_CtApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V
#  define Rte_Write_CtApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(data) (Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus Rte_Write_CtApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus
#  define Rte_Write_CtApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(data) (Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV Rte_Write_CtApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV
#  define Rte_Write_CtApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(data) (Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_POST_Result_DeOBC_POST_Result Rte_Write_CtApOBC_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Write_CtApOBC_PpOBC_POST_Result_DeOBC_POST_Result(data) (Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault Rte_Write_CtApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault
#  define Rte_Write_CtApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(data) (Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue Rte_Write_CtApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue
#  define Rte_Write_CtApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(data) (Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue Rte_Write_CtApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue
#  define Rte_Write_CtApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(data) (Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue Rte_Write_CtApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue
#  define Rte_Write_CtApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(data) (Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_WDGM_OSAPPLICATION_ASILB_APPL_CODE) WdgM_CheckpointReached(WdgM_SupervisedEntityIdType parg0, WdgM_CheckpointIdType CPID); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(arg1) (WdgM_CheckpointReached((WdgM_SupervisedEntityIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalMaxOutputDCHVCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOBC.CalMaxOutputDCHVCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxOutputDCHVVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOBC.CalMaxOutputDCHVVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinDCHVOutputVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOBC.CalMinDCHVOutputVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeRelaysPrechargeClosed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOBC.CalTimeRelaysPrechargeClosed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalVDCLinkRequiredMonophasic() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOBC.CalVDCLinkRequiredMonophasic) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalVDCLinkRequiredTriphasic() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOBC.CalVDCLinkRequiredTriphasic) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOBC_PimOBCShutdownPathFSP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOBC_PimOBCShutdownPathGPIO;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBCShutdownPathFSP() \
  (&Rte_CpApOBC_PimOBCShutdownPathFSP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBCShutdownPathGPIO() \
  (&Rte_CpApOBC_PimOBCShutdownPathGPIO)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApOBC_START_SEC_CODE
# include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApOBC_init RCtApOBC_init
#  define RTE_RUNNABLE_RCtApOBC_task10msA RCtApOBC_task10msA
#  define RTE_RUNNABLE_RCtApOBC_task10msB RCtApOBC_task10msB
# endif

FUNC(void, CtApOBC_CODE) RCtApOBC_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApOBC_CODE) RCtApOBC_task10msA(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApOBC_CODE) RCtApOBC_task10msB(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApOBC_STOP_SEC_CODE
# include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_WdgM_AliveSupervision_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPOBC_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
