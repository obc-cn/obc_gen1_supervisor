/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApDGN.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApDGN>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPDGN_H
# define RTE_CTAPDGN_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApDGN_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest;
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions;
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput;
extern VAR(IdtDutyControlPilot, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions;
extern VAR(IdtExtOpPlugLockRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode;
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
extern VAR(BMS_MainConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState;
extern VAR(BMS_SOC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC;
extern VAR(BSI_ChargeState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState;
extern VAR(BSI_VCUSynchroGPC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC;
extern VAR(DCDC_InputCurrent, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent;
extern VAR(DCDC_InputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage;
extern VAR(DCDC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent;
extern VAR(DCDC_OutputVoltage, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status;
extern VAR(DCDC_Temperature, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature;
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
extern VAR(DCHV_ADC_NTC_MOD_5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5;
extern VAR(DCHV_ADC_NTC_MOD_6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCLV_T_L_BUCK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK;
extern VAR(DCLV_T_PP_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A;
extern VAR(DCLV_T_PP_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B;
extern VAR(DCLV_T_SW_BUCK_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A;
extern VAR(DCLV_T_SW_BUCK_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B;
extern VAR(DCLV_T_TX_PP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_ElockState, RTE_VAR_INIT) Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState;
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
extern VAR(OBC_OutputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status;
extern VAR(PFC_IPH1_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1;
extern VAR(PFC_IPH2_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1;
extern VAR(PFC_IPH3_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1;
extern VAR(PFC_Temp_M1_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C;
extern VAR(PFC_Temp_M3_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C;
extern VAR(PFC_Temp_M4_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C;
extern VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
extern VAR(PFC_VPH23_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V;
extern VAR(PFC_VPH31_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V;
extern VAR(SUP_RequestDCLVStatus, RTE_VAR_INIT) Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus;
extern VAR(VCU_AccPedalPosition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition;
extern VAR(VCU_CptTemporel, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel;
extern VAR(VCU_DCDCActivation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation;
extern VAR(VCU_DiagMuxOnPwt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt;
extern VAR(VCU_EPWT_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status;
extern VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
extern VAR(VCU_Kilometrage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage;
extern VAR(IdtMaxInputACCurrentEVSE, RTE_VAR_INIT) Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas;
extern VAR(IdtProximityDetectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(EDITION_CALIB, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB;
extern VAR(EDITION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT;
extern VAR(OBC_CommunicationSt, RTE_VAR_INIT) Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt;
extern VAR(VERSION_APPLI, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI;
extern VAR(VERSION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT;
extern VAR(VERSION_SYSTEME, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME;
extern VAR(VERS_DATE2_ANNEE, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE;
extern VAR(VERS_DATE2_JOUR, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR;
extern VAR(VERS_DATE2_MOIS, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS;
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpCANComRequest_DeCANComRequest;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions;
extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl;
extern VAR(IdtExtPlgLedrCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC;
extern VAR(DCDC_OBCDCDCRTPowerLoad, RTE_VAR_INIT) Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad;
extern VAR(DCDC_OVERTEMP, RTE_VAR_INIT) Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP;
extern VAR(OBC_PushChargeType, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType;
extern VAR(OBC_RechargeHMIState, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState;
extern VAR(OBC_SocketTempL, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL;
extern VAR(OBC_SocketTempN, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN;
extern VAR(SUPV_RCDLineState, RTE_VAR_INIT) Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono;
extern VAR(IdtPlantModeTestInfoDID_Result, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result;
extern VAR(IdtPlantModeTestInfoDID_State, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage;
extern VAR(IdtPlgLedrCtrl, RTE_VAR_INIT) Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpInt_EDITION_CALIB_EDITION_CALIB (0U)
#  define Rte_InitValue_PpInt_EDITION_SOFT_EDITION_SOFT (0U)
#  define Rte_InitValue_PpInt_OBC_CommunicationSt_OBC_CommunicationSt (FALSE)
#  define Rte_InitValue_PpInt_VERSION_APPLI_VERSION_APPLI (0U)
#  define Rte_InitValue_PpInt_VERSION_SOFT_VERSION_SOFT (0U)
#  define Rte_InitValue_PpInt_VERSION_SYSTEME_VERSION_SYSTEME (0U)
#  define Rte_InitValue_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE (0U)
#  define Rte_InitValue_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR (0U)
#  define Rte_InitValue_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS (0U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_Mode_CtApDGN_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void);

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault Rte_Read_CtApDGN_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault
#  define Rte_Read_CtApDGN_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(data) (*(data) = Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest Rte_Read_CtApDGN_PpActiveDischargeRequest_DeActiveDischargeRequest
#  define Rte_Read_CtApDGN_PpActiveDischargeRequest_DeActiveDischargeRequest(data) (*(data) = Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApDGN_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApDGN_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions Rte_Read_CtApDGN_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions
#  define Rte_Read_CtApDGN_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(data) (*(data) = Rte_CpApBAT_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState Rte_Read_CtApDGN_PpBatteryVoltageState_DeBatteryVoltageState
#  define Rte_Read_CtApDGN_PpBatteryVoltageState_DeBatteryVoltageState(data) (*(data) = Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions Rte_Read_CtApDGN_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions
#  define Rte_Read_CtApDGN_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(data) (*(data) = Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBusOFFEnableConditions_DeBusOFFEnableConditions Rte_Read_CtApDGN_PpBusOFFEnableConditions_DeBusOFFEnableConditions
#  define Rte_Read_CtApDGN_PpBusOFFEnableConditions_DeBusOFFEnableConditions(data) (*(data) = Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBusOffCANFault_DeBusOffCANFault Rte_Read_CtApDGN_PpBusOffCANFault_DeBusOffCANFault
#  define Rte_Read_CtApDGN_PpBusOffCANFault_DeBusOffCANFault(data) (*(data) = Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpCANComRequest_DeCANComRequest Rte_Read_CtApDGN_PpCANComRequest_DeCANComRequest
#  define Rte_Read_CtApDGN_PpCANComRequest_DeCANComRequest(data) (*(data) = Rte_CpApAEM_PpCANComRequest_DeCANComRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpChLedRGBCtl_DeChLedBrCtl Rte_Read_CtApDGN_PpChLedRGBCtl_DeChLedBrCtl
#  define Rte_Read_CtApDGN_PpChLedRGBCtl_DeChLedBrCtl(data) (*(data) = Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpChLedRGBCtl_DeChLedGrCtl Rte_Read_CtApDGN_PpChLedRGBCtl_DeChLedGrCtl
#  define Rte_Read_CtApDGN_PpChLedRGBCtl_DeChLedGrCtl(data) (*(data) = Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpChLedRGBCtl_DeChLedRrCtl Rte_Read_CtApDGN_PpChLedRGBCtl_DeChLedRrCtl
#  define Rte_Read_CtApDGN_PpChLedRGBCtl_DeChLedRrCtl(data) (*(data) = Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo Rte_Read_CtApDGN_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo
#  define Rte_Read_CtApDGN_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(data) (*(data) = Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU Rte_Read_CtApDGN_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU
#  define Rte_Read_CtApDGN_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(data) (*(data) = Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU Rte_Read_CtApDGN_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU
#  define Rte_Read_CtApDGN_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(data) (*(data) = Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpControlPilotFreqError_DeControlPilotFreqError Rte_Read_CtApDGN_PpControlPilotFreqError_DeControlPilotFreqError
#  define Rte_Read_CtApDGN_PpControlPilotFreqError_DeControlPilotFreqError(data) (*(data) = Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDCFaultsList_DeDCDC_OvertemperatureFault Rte_Read_CtApDGN_PpDCDCFaultsList_DeDCDC_OvertemperatureFault
#  define Rte_Read_CtApDGN_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(data) (*(data) = Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDCFaultsList_DeDCLV_HWErrors_Fault Rte_Read_CtApDGN_PpDCDCFaultsList_DeDCLV_HWErrors_Fault
#  define Rte_Read_CtApDGN_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(data) (*(data) = Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDCFaultsList_DeDCLV_InternalCom_Fault Rte_Read_CtApDGN_PpDCDCFaultsList_DeDCLV_InternalCom_Fault
#  define Rte_Read_CtApDGN_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(data) (*(data) = Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result Rte_Read_CtApDGN_PpDCDC_POST_Result_DeDCDC_POST_Result
#  define Rte_Read_CtApDGN_PpDCDC_POST_Result_DeDCDC_POST_Result(data) (*(data) = Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error Rte_Read_CtApDGN_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error
#  define Rte_Read_CtApDGN_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(data) (*(data) = Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault Rte_Read_CtApDGN_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault
#  define Rte_Read_CtApDGN_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(data) (*(data) = Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault Rte_Read_CtApDGN_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault
#  define Rte_Read_CtApDGN_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(data) (*(data) = Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552 Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU
#  define Rte_Read_CtApDGN_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault
#  define Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(data) (*(data) = Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault
#  define Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(data) (*(data) = Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault
#  define Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(data) (*(data) = Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault
#  define Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(data) (*(data) = Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault
#  define Rte_Read_CtApDGN_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(data) (*(data) = Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault Rte_Read_CtApDGN_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault
#  define Rte_Read_CtApDGN_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(data) (*(data) = Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault
#  define Rte_Read_CtApDGN_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(data) (*(data) = Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput Rte_Read_CtApDGN_PpDiagnosticNoACInput_DeDiagnosticNoACInput
#  define Rte_Read_CtApDGN_PpDiagnosticNoACInput_DeDiagnosticNoACInput(data) (*(data) = Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDutyControlPilot_DeDutyControlPilot Rte_Read_CtApDGN_PpDutyControlPilot_DeDutyControlPilot
#  define Rte_Read_CtApDGN_PpDutyControlPilot_DeDutyControlPilot(data) (*(data) = Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockHFaultOpenCircuit Rte_Read_CtApDGN_PpELockFaults_DeOutputElockHFaultOpenCircuit
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockHFaultOpenCircuit(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockHFaultSCG Rte_Read_CtApDGN_PpELockFaults_DeOutputElockHFaultSCG
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockHFaultSCG(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockHFaultSCP Rte_Read_CtApDGN_PpELockFaults_DeOutputElockHFaultSCP
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockHFaultSCP(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockLFaultOpenCircuit Rte_Read_CtApDGN_PpELockFaults_DeOutputElockLFaultOpenCircuit
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockLFaultOpenCircuit(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockLFaultSCG Rte_Read_CtApDGN_PpELockFaults_DeOutputElockLFaultSCG
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockLFaultSCG(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockLFaultSCP Rte_Read_CtApDGN_PpELockFaults_DeOutputElockLFaultSCP
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockLFaultSCP(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockFaults_DeOutputElockOvercurrent Rte_Read_CtApDGN_PpELockFaults_DeOutputElockOvercurrent
#  define Rte_Read_CtApDGN_PpELockFaults_DeOutputElockOvercurrent(data) (*(data) = Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG Rte_Read_CtApDGN_PpELockSensorFaults_DeELockSensorFaultSCG
#  define Rte_Read_CtApDGN_PpELockSensorFaults_DeELockSensorFaultSCG(data) (*(data) = Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP Rte_Read_CtApDGN_PpELockSensorFaults_DeELockSensorFaultSCP
#  define Rte_Read_CtApDGN_PpELockSensorFaults_DeELockSensorFaultSCP(data) (*(data) = Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions Rte_Read_CtApDGN_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions
#  define Rte_Read_CtApDGN_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(data) (*(data) = Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockFaultDetected_DeElockFaultDetected Rte_Read_CtApDGN_PpElockFaultDetected_DeElockFaultDetected
#  define Rte_Read_CtApDGN_PpElockFaultDetected_DeElockFaultDetected(data) (*(data) = Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions Rte_Read_CtApDGN_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions
#  define Rte_Read_CtApDGN_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(data) (*(data) = Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode Rte_Read_CtApDGN_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode
#  define Rte_Read_CtApDGN_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data) (*(data) = Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl Rte_Read_CtApDGN_PpExtChLedRGBCtl_DeExtChLedBrCtl
#  define Rte_Read_CtApDGN_PpExtChLedRGBCtl_DeExtChLedBrCtl(data) (*(data) = Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl Rte_Read_CtApDGN_PpExtChLedRGBCtl_DeExtChLedGrCtl
#  define Rte_Read_CtApDGN_PpExtChLedRGBCtl_DeExtChLedGrCtl(data) (*(data) = Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl Rte_Read_CtApDGN_PpExtChLedRGBCtl_DeExtChLedRrCtl
#  define Rte_Read_CtApDGN_PpExtChLedRGBCtl_DeExtChLedRrCtl(data) (*(data) = Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw Rte_Read_CtApDGN_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw
#  define Rte_Read_CtApDGN_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(data) (*(data) = Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl Rte_Read_CtApDGN_PpExtPlgLedrCtl_DeExtPlgLedrCtl
#  define Rte_Read_CtApDGN_PpExtPlgLedrCtl_DeExtPlgLedrCtl(data) (*(data) = Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw Rte_Read_CtApDGN_PpExtRCDLineRaw_DeExtRCDLineRaw
#  define Rte_Read_CtApDGN_PpExtRCDLineRaw_DeExtRCDLineRaw(data) (*(data) = Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd Rte_Read_CtApDGN_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd
#  define Rte_Read_CtApDGN_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(data) (*(data) = Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst Rte_Read_CtApDGN_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst
#  define Rte_Read_CtApDGN_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(data) (*(data) = Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFaultRCDLineSC_DeFaultRCDLineSC Rte_Read_CtApDGN_PpFaultRCDLineSC_DeFaultRCDLineSC
#  define Rte_Read_CtApDGN_PpFaultRCDLineSC_DeFaultRCDLineSC(data) (*(data) = Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw Rte_Read_CtApDGN_PpInputChargePushRaw_DeInputChargePushRaw
#  define Rte_Read_CtApDGN_PpInputChargePushRaw_DeInputChargePushRaw(data) (*(data) = Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInputVoltageMode_DeInputVoltageMode Rte_Read_CtApDGN_PpInputVoltageMode_DeInputVoltageMode
#  define Rte_Read_CtApDGN_PpInputVoltageMode_DeInputVoltageMode(data) (*(data) = Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Read_CtApDGN_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApDGN_PpInt_ABS_VehSpd_ABS_VehSpd(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage Rte_Read_CtApDGN_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage
#  define Rte_Read_CtApDGN_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState Rte_Read_CtApDGN_PpInt_BMS_MainConnectorState_BMS_MainConnectorState
#  define Rte_Read_CtApDGN_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_SOC_BMS_SOC Rte_Read_CtApDGN_PpInt_BMS_SOC_BMS_SOC
#  define Rte_Read_CtApDGN_PpInt_BMS_SOC_BMS_SOC(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState Rte_Read_CtApDGN_PpInt_BSI_ChargeState_BSI_ChargeState
#  define Rte_Read_CtApDGN_PpInt_BSI_ChargeState_BSI_ChargeState(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC Rte_Read_CtApDGN_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC
#  define Rte_Read_CtApDGN_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent Rte_Read_CtApDGN_PpInt_DCDC_InputCurrent_DCDC_InputCurrent
#  define Rte_Read_CtApDGN_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage Rte_Read_CtApDGN_PpInt_DCDC_InputVoltage_DCDC_InputVoltage
#  define Rte_Read_CtApDGN_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(data) (*(data) = Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad Rte_Read_CtApDGN_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad
#  define Rte_Read_CtApDGN_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(data) (*(data) = Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP Rte_Read_CtApDGN_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP
#  define Rte_Read_CtApDGN_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(data) (*(data) = Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent Rte_Read_CtApDGN_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent
#  define Rte_Read_CtApDGN_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(data) (*(data) = Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage Rte_Read_CtApDGN_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage
#  define Rte_Read_CtApDGN_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(data) (*(data) = Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Status_DCDC_Status Rte_Read_CtApDGN_PpInt_DCDC_Status_DCDC_Status
#  define Rte_Read_CtApDGN_PpInt_DCDC_Status_DCDC_Status(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature Rte_Read_CtApDGN_PpInt_DCDC_Temperature_DCDC_Temperature
#  define Rte_Read_CtApDGN_PpInt_DCDC_Temperature_DCDC_Temperature(data) (*(data) = Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Read_CtApDGN_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Read_CtApDGN_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 Rte_Read_CtApDGN_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5
#  define Rte_Read_CtApDGN_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 Rte_Read_CtApDGN_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6
#  define Rte_Read_CtApDGN_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApDGN_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApDGN_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK Rte_Read_CtApDGN_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK
#  define Rte_Read_CtApDGN_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A Rte_Read_CtApDGN_PpInt_DCLV_T_PP_A_DCLV_T_PP_A
#  define Rte_Read_CtApDGN_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B Rte_Read_CtApDGN_PpInt_DCLV_T_PP_B_DCLV_T_PP_B
#  define Rte_Read_CtApDGN_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A Rte_Read_CtApDGN_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A
#  define Rte_Read_CtApDGN_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B Rte_Read_CtApDGN_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B
#  define Rte_Read_CtApDGN_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP Rte_Read_CtApDGN_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP
#  define Rte_Read_CtApDGN_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApDGN_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApDGN_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApDGN_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApDGN_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ElockState_OBC_ElockState Rte_Read_CtApDGN_PpInt_OBC_ElockState_OBC_ElockState
#  define Rte_Read_CtApDGN_PpInt_OBC_ElockState_OBC_ElockState(data) (*(data) = Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt Rte_Read_CtApDGN_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Read_CtApDGN_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage Rte_Read_CtApDGN_PpInt_OBC_OutputVoltage_OBC_OutputVoltage
#  define Rte_Read_CtApDGN_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(data) (*(data) = Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType Rte_Read_CtApDGN_PpInt_OBC_PushChargeType_OBC_PushChargeType
#  define Rte_Read_CtApDGN_PpInt_OBC_PushChargeType_OBC_PushChargeType(data) (*(data) = Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState Rte_Read_CtApDGN_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState
#  define Rte_Read_CtApDGN_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data) (*(data) = Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL Rte_Read_CtApDGN_PpInt_OBC_SocketTemp_OBC_SocketTempL
#  define Rte_Read_CtApDGN_PpInt_OBC_SocketTemp_OBC_SocketTempL(data) (*(data) = Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN Rte_Read_CtApDGN_PpInt_OBC_SocketTemp_OBC_SocketTempN
#  define Rte_Read_CtApDGN_PpInt_OBC_SocketTemp_OBC_SocketTempN(data) (*(data) = Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_OBC_Status Rte_Read_CtApDGN_PpInt_OBC_Status_OBC_Status
#  define Rte_Read_CtApDGN_PpInt_OBC_Status_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 Rte_Read_CtApDGN_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1
#  define Rte_Read_CtApDGN_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 Rte_Read_CtApDGN_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1
#  define Rte_Read_CtApDGN_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 Rte_Read_CtApDGN_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1
#  define Rte_Read_CtApDGN_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C Rte_Read_CtApDGN_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C
#  define Rte_Read_CtApDGN_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C Rte_Read_CtApDGN_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C
#  define Rte_Read_CtApDGN_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C Rte_Read_CtApDGN_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C
#  define Rte_Read_CtApDGN_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V Rte_Read_CtApDGN_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V
#  define Rte_Read_CtApDGN_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V Rte_Read_CtApDGN_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V
#  define Rte_Read_CtApDGN_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V Rte_Read_CtApDGN_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V
#  define Rte_Read_CtApDGN_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState Rte_Read_CtApDGN_PpInt_SUPV_RCDLineState_SUPV_RCDLineState
#  define Rte_Read_CtApDGN_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data) (*(data) = Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus Rte_Read_CtApDGN_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus
#  define Rte_Read_CtApDGN_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data) (*(data) = Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition Rte_Read_CtApDGN_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition
#  define Rte_Read_CtApDGN_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel Rte_Read_CtApDGN_PpInt_VCU_CptTemporel_VCU_CptTemporel
#  define Rte_Read_CtApDGN_PpInt_VCU_CptTemporel_VCU_CptTemporel(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation Rte_Read_CtApDGN_PpInt_VCU_DCDCActivation_VCU_DCDCActivation
#  define Rte_Read_CtApDGN_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt Rte_Read_CtApDGN_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt
#  define Rte_Read_CtApDGN_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EPWT_Status_VCU_EPWT_Status Rte_Read_CtApDGN_PpInt_VCU_EPWT_Status_VCU_EPWT_Status
#  define Rte_Read_CtApDGN_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb Rte_Read_CtApDGN_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb
#  define Rte_Read_CtApDGN_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage Rte_Read_CtApDGN_PpInt_VCU_Kilometrage_VCU_Kilometrage
#  define Rte_Read_CtApDGN_PpInt_VCU_Kilometrage_VCU_Kilometrage(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedBlueFaultOC Rte_Read_CtApDGN_PpLedFaults_DeLedBlueFaultOC
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedBlueFaultOC(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedBlueFaultSCG Rte_Read_CtApDGN_PpLedFaults_DeLedBlueFaultSCG
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedBlueFaultSCG(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedBlueFaultSCP Rte_Read_CtApDGN_PpLedFaults_DeLedBlueFaultSCP
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedBlueFaultSCP(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedGreenFaultOC Rte_Read_CtApDGN_PpLedFaults_DeLedGreenFaultOC
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedGreenFaultOC(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedGreenFaultSCG Rte_Read_CtApDGN_PpLedFaults_DeLedGreenFaultSCG
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedGreenFaultSCG(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedGreenFaultSCP Rte_Read_CtApDGN_PpLedFaults_DeLedGreenFaultSCP
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedGreenFaultSCP(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedRedFaultOC Rte_Read_CtApDGN_PpLedFaults_DeLedRedFaultOC
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedRedFaultOC(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedRedFaultSCG Rte_Read_CtApDGN_PpLedFaults_DeLedRedFaultSCG
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedRedFaultSCG(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedFaults_DeLedRedFaultSCP Rte_Read_CtApDGN_PpLedFaults_DeLedRedFaultSCP
#  define Rte_Read_CtApDGN_PpLedFaults_DeLedRedFaultSCP(data) (*(data) = Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedMonitoringConditions_DeBlueLedMonitoringConditions Rte_Read_CtApDGN_PpLedMonitoringConditions_DeBlueLedMonitoringConditions
#  define Rte_Read_CtApDGN_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(data) (*(data) = Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedMonitoringConditions_DeGreenLedMonitoringConditions Rte_Read_CtApDGN_PpLedMonitoringConditions_DeGreenLedMonitoringConditions
#  define Rte_Read_CtApDGN_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(data) (*(data) = Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedMonitoringConditions_DePlugLedMonitoringConditions Rte_Read_CtApDGN_PpLedMonitoringConditions_DePlugLedMonitoringConditions
#  define Rte_Read_CtApDGN_PpLedMonitoringConditions_DePlugLedMonitoringConditions(data) (*(data) = Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedMonitoringConditions_DeRedLedMonitoringConditions Rte_Read_CtApDGN_PpLedMonitoringConditions_DeRedLedMonitoringConditions
#  define Rte_Read_CtApDGN_PpLedMonitoringConditions_DeRedLedMonitoringConditions(data) (*(data) = Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE Rte_Read_CtApDGN_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE
#  define Rte_Read_CtApDGN_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(data) (*(data) = Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd Rte_Read_CtApDGN_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd
#  define Rte_Read_CtApDGN_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(data) (*(data) = Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst Rte_Read_CtApDGN_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst
#  define Rte_Read_CtApDGN_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(data) (*(data) = Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC Rte_Read_CtApDGN_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC
#  define Rte_Read_CtApDGN_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(data) (*(data) = Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempAC1Raw
#  define Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempAC1Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempAC2Raw
#  define Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempAC2Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempAC3Raw
#  define Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempAC3Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempACNRaw
#  define Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempACNRaw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempDC1Raw
#  define Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempDC1Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempDC2Raw
#  define Rte_Read_CtApDGN_PpMsrTempRaw_DeMsrTempDC2Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeInlet_OvertempACSensorFault Rte_Read_CtApDGN_PpOBCFaultsList_DeInlet_OvertempACSensorFault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeInlet_OvertempACSensorFault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeInlet_OvertempDCSensorFault Rte_Read_CtApDGN_PpOBCFaultsList_DeInlet_OvertempDCSensorFault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeOBC_HWErrors_Fault Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_HWErrors_Fault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_HWErrors_Fault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeOBC_InternalComFault Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_InternalComFault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_InternalComFault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeOBC_OvercurrentOutputFault Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_OvercurrentOutputFault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeOBC_OvertemperatureFault Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_OvertemperatureFault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_OvertemperatureFault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFaultsList_DeOBC_OvervoltageOutputFault Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_OvervoltageOutputFault
#  define Rte_Read_CtApDGN_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(data) (*(data) = Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error Rte_Read_CtApDGN_PpOBCFramesReception_Error_DeOBCFramesReception_Error
#  define Rte_Read_CtApDGN_PpOBCFramesReception_Error_DeOBCFramesReception_Error(data) (*(data) = Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result Rte_Read_CtApDGN_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Read_CtApDGN_PpOBC_POST_Result_DeOBC_POST_Result(data) (*(data) = Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault Rte_Read_CtApDGN_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault
#  define Rte_Read_CtApDGN_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(data) (*(data) = Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue Rte_Read_CtApDGN_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue
#  define Rte_Read_CtApDGN_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(data) (*(data) = Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue Rte_Read_CtApDGN_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue
#  define Rte_Read_CtApDGN_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(data) (*(data) = Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputRelayMono_DeOutputRelayMono Rte_Read_CtApDGN_PpOutputRelayMono_DeOutputRelayMono
#  define Rte_Read_CtApDGN_PpOutputRelayMono_DeOutputRelayMono(data) (*(data) = Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempAC1Meas
#  define Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempAC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempAC2Meas
#  define Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempAC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempAC3Meas
#  define Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempAC3Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempACNMeas
#  define Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempACNMeas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempDC1Meas
#  define Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempDC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempDC2Meas
#  define Rte_Read_CtApDGN_PpOutputTempMeas_DeOutputTempDC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result
#  define Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(data) (*(data) = Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State
#  define Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(data) (*(data) = Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase
#  define Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(data) (*(data) = Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines
#  define Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(data) (*(data) = Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty
#  define Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(data) (*(data) = Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage
#  define Rte_Read_CtApDGN_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(data) (*(data) = Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl Rte_Read_CtApDGN_PpPlgLedrCtrl_DePlgLedrCtrl
#  define Rte_Read_CtApDGN_PpPlgLedrCtrl_DePlgLedrCtrl(data) (*(data) = Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlugLedFaults_DeLedPlugFaultOC Rte_Read_CtApDGN_PpPlugLedFaults_DeLedPlugFaultOC
#  define Rte_Read_CtApDGN_PpPlugLedFaults_DeLedPlugFaultOC(data) (*(data) = Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlugLedFaults_DeLedPlugFaultSCG Rte_Read_CtApDGN_PpPlugLedFaults_DeLedPlugFaultSCG
#  define Rte_Read_CtApDGN_PpPlugLedFaults_DeLedPlugFaultSCG(data) (*(data) = Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlugLedFaults_DeLedPlugFaultSCP Rte_Read_CtApDGN_PpPlugLedFaults_DeLedPlugFaultSCP
#  define Rte_Read_CtApDGN_PpPlugLedFaults_DeLedPlugFaultSCP(data) (*(data) = Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue Rte_Read_CtApDGN_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue
#  define Rte_Read_CtApDGN_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue Rte_Read_CtApDGN_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue
#  define Rte_Read_CtApDGN_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(data) (*(data) = Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempAC1FaultSCG Rte_Read_CtApDGN_PpTempFaults_DeTempAC1FaultSCG
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempAC1FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempAC1FaultSCP Rte_Read_CtApDGN_PpTempFaults_DeTempAC1FaultSCP
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempAC1FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempAC2FaultSCG Rte_Read_CtApDGN_PpTempFaults_DeTempAC2FaultSCG
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempAC2FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempAC2FaultSCP Rte_Read_CtApDGN_PpTempFaults_DeTempAC2FaultSCP
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempAC2FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempAC3FaultSCG Rte_Read_CtApDGN_PpTempFaults_DeTempAC3FaultSCG
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempAC3FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempAC3FaultSCP Rte_Read_CtApDGN_PpTempFaults_DeTempAC3FaultSCP
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempAC3FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempACNFaultSCG Rte_Read_CtApDGN_PpTempFaults_DeTempACNFaultSCG
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempACNFaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempACNFaultSCP Rte_Read_CtApDGN_PpTempFaults_DeTempACNFaultSCP
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempACNFaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC1FaultSCG Rte_Read_CtApDGN_PpTempFaults_DeTempDC1FaultSCG
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempDC1FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC1FaultSCP Rte_Read_CtApDGN_PpTempFaults_DeTempDC1FaultSCP
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempDC1FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC2FaultSCG Rte_Read_CtApDGN_PpTempFaults_DeTempDC2FaultSCG
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempDC2FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC2FaultSCP Rte_Read_CtApDGN_PpTempFaults_DeTempDC2FaultSCP
#  define Rte_Read_CtApDGN_PpTempFaults_DeTempDC2FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempMonitoringConditions_DeAC1TempMonitoringConditions Rte_Read_CtApDGN_PpTempMonitoringConditions_DeAC1TempMonitoringConditions
#  define Rte_Read_CtApDGN_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(data) (*(data) = Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempMonitoringConditions_DeAC2TempMonitoringConditions Rte_Read_CtApDGN_PpTempMonitoringConditions_DeAC2TempMonitoringConditions
#  define Rte_Read_CtApDGN_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(data) (*(data) = Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempMonitoringConditions_DeAC3TempMonitoringConditions Rte_Read_CtApDGN_PpTempMonitoringConditions_DeAC3TempMonitoringConditions
#  define Rte_Read_CtApDGN_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(data) (*(data) = Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempMonitoringConditions_DeACNTempMonitoringConditions Rte_Read_CtApDGN_PpTempMonitoringConditions_DeACNTempMonitoringConditions
#  define Rte_Read_CtApDGN_PpTempMonitoringConditions_DeACNTempMonitoringConditions(data) (*(data) = Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempMonitoringConditions_DeDC1TempMonitoringConditions Rte_Read_CtApDGN_PpTempMonitoringConditions_DeDC1TempMonitoringConditions
#  define Rte_Read_CtApDGN_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(data) (*(data) = Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempMonitoringConditions_DeDC2TempMonitoringConditions Rte_Read_CtApDGN_PpTempMonitoringConditions_DeDC2TempMonitoringConditions
#  define Rte_Read_CtApDGN_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(data) (*(data) = Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpInt_EDITION_CALIB_EDITION_CALIB Rte_Write_CtApDGN_PpInt_EDITION_CALIB_EDITION_CALIB
#  define Rte_Write_CtApDGN_PpInt_EDITION_CALIB_EDITION_CALIB(data) (Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_EDITION_SOFT_EDITION_SOFT Rte_Write_CtApDGN_PpInt_EDITION_SOFT_EDITION_SOFT
#  define Rte_Write_CtApDGN_PpInt_EDITION_SOFT_EDITION_SOFT(data) (Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_CommunicationSt_OBC_CommunicationSt Rte_Write_CtApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt
#  define Rte_Write_CtApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(data) (Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VERSION_APPLI_VERSION_APPLI Rte_Write_CtApDGN_PpInt_VERSION_APPLI_VERSION_APPLI
#  define Rte_Write_CtApDGN_PpInt_VERSION_APPLI_VERSION_APPLI(data) (Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VERSION_SOFT_VERSION_SOFT Rte_Write_CtApDGN_PpInt_VERSION_SOFT_VERSION_SOFT
#  define Rte_Write_CtApDGN_PpInt_VERSION_SOFT_VERSION_SOFT(data) (Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VERSION_SYSTEME_VERSION_SYSTEME Rte_Write_CtApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME
#  define Rte_Write_CtApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(data) (Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE Rte_Write_CtApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE
#  define Rte_Write_CtApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(data) (Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR Rte_Write_CtApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR
#  define Rte_Write_CtApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(data) (Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS Rte_Write_CtApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS
#  define Rte_Write_CtApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(data) (Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Mode_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl Rte_Mode_CtApDGN_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMMASTER_0_APPL_CODE) Dem_ClearDTC(uint8 parg0); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_ClearDTC_DemClient_CDD_ClearDTC() (Dem_ClearDTC((uint8)0)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMMASTER_0_APPL_CODE) Dem_SelectDTC(uint8 parg0, uint32 DTC, Dem_DTCFormatType DTCFormat, Dem_DTCOriginType DTCOrigin); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_ClearDTC_DemClient_CDD_SelectDTC(arg1, arg2, arg3) (Dem_SelectDTC((uint8)0, arg1, arg2, arg3)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMMASTER_0_APPL_CODE) Dem_SetEnableCondition(uint8 parg0, boolean ConditionFulfilled); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_EnableCond_DemEnableConditionDTC_0x10C413_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableConditionDTC_0x10C512_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableConditionDTC_0x10C613_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)3, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableConditionDTC_0x10C713_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)4, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableConditionDTC_0x12e912_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)5, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableConditionDTC_0x12f316_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)6, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x056216_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)55, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x056317_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)56, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x060251_SetEnableCondition(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x061055_SetEnableCondition(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a0804_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)8, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a084b_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)7, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x0a9464_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x0af864_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)10, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x0cf464_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x108093_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)57, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120a11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)42, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120a12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)43, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120b11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120b12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)45, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120c64_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)46, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120c98_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)47, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120d64_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)48, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x120d98_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)49, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d711_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)16, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d712_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)17, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d713_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)18, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d811_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)19, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d812_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)20, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d813_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)21, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d911_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)22, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d912_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)23, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12d913_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)24, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)25, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)26, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12da13_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)27, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12db12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)28, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12dc11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)29, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12dd12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)30, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12de11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)31, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12df13_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)32, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e012_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)33, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e111_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)34, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e213_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)35, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e319_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)36, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e712_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)37, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12e811_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)38, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12ea11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)39, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x12f917_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x13e919_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)41, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x160a51_SetEnableCondition(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x166C64_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)50, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x16e600_SetEnableCondition(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x179e11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)51, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x179e12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)52, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x179f11_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)53, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x179f12_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)54, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a0064_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a7104_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)14, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a714b_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)12, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0x1a7172_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)15, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xc07988_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)58, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xc08913_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)59, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xd18787_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)62, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xd1a087_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)60, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xd20781_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)63, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xd2a081_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)61, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xd38782_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)64, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xd38783_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)65, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00081_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)66, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00087_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)67, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00214_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)68, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EnableCond_DemEnableCondition_DTC_0xe00362_SetEnableCondition(arg1) (Dem_SetEnableCondition((uint8)69, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_Event_DTC_0x056216_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)47, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x056317_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)48, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x060251_SetEventStatus(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x061055_SetEventStatus(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x0a0804_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x0a084b_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x0a9464_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)3, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x0af864_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)4, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x0cf464_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)66, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x108093_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)49, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x10c413_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)67, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x10c512_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)68, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x10c613_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)69, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x10c713_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)70, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120a11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)35, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120a12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)36, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120b11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)37, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120b12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)38, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120c64_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)39, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120c98_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120d64_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)41, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x120d98_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)42, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d711_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d712_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)10, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d713_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d811_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)12, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d812_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d813_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)14, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d911_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)15, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d912_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)16, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12d913_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)17, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12da11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)18, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12da12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)19, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12da13_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)20, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12db12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)21, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12dc11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)22, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12dd12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)23, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12de11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)24, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12df13_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)25, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e012_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)26, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e111_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)27, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e213_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)28, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e319_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)29, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e712_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)30, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e811_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)31, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12e912_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)32, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12ea11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)33, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12f316_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)71, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x12f917_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)34, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x13e919_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)64, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x160a51_SetEventStatus(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x166c64_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)65, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x16e600_SetEventStatus(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x179e11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)43, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x179e12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x179f11_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)45, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x179f12_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)46, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x1a0064_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)6, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x1a7104_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)7, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x1a714b_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)5, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0x1a7172_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)8, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xc07988_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)50, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xc08913_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)51, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xd18787_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)54, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xd1a087_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)52, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xd20781_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)55, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xd2a081_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)53, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xd38782_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)56, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xd38783_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)57, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xe00081_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)58, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xe00087_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)59, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xe00214_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)60, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_Event_DTC_0xe00362_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)61, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMMASTER_0_APPL_CODE) Dem_GetOperationCycleState(uint8 parg0, P2VAR(Dem_OperationCycleStateType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) CycleState); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_OpCycle_DemAgingCycle_GetOperationCycleState(arg1) (Dem_GetOperationCycleState((uint8)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMMASTER_0_APPL_CODE) Dem_SetOperationCycleState(uint8 parg0, Dem_OperationCycleStateType CycleState); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_OpCycle_DemAgingCycle_SetOperationCycleState(arg1) (Dem_SetOperationCycleState((uint8)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_OpCycle_DemDrivingCycle_GetOperationCycleState(arg1) (Dem_GetOperationCycleState((uint8)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_OpCycle_DemDrivingCycle_SetOperationCycleState(arg1) (Dem_SetOperationCycleState((uint8)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_OpCycle_PowerCycle_GetOperationCycleState(arg1) (Dem_GetOperationCycleState((uint8)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(arg1) (Dem_SetOperationCycleState((uint8)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_CpApDGN_DGN_EOL_NVM_Needs_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)45, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_EraseNvBlock(NvM_BlockIdType parg0); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_EthMACAdressDataBlock_EraseBlock() (NvM_EraseNvBlock((NvM_BlockIdType)13)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_GetErrorStatus(NvM_BlockIdType parg0, P2VAR(NvM_RequestResultType, AUTOMATIC, RTE_NVM_APPL_VAR) ErrorStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_EthMACAdressDataBlock_GetErrorStatus(arg1) (NvM_GetErrorStatus((NvM_BlockIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_InvalidateNvBlock(NvM_BlockIdType parg0); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_EthMACAdressDataBlock_InvalidateNvBlock() (NvM_InvalidateNvBlock((NvM_BlockIdType)13)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_ReadBlock(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_EthMACAdressDataBlock_ReadBlock(arg1) (NvM_ReadBlock((NvM_BlockIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_RestoreBlockDefaults(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_EthMACAdressDataBlock_RestoreBlockDefaults(arg1) (NvM_RestoreBlockDefaults((NvM_BlockIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_SetRamBlockStatus(NvM_BlockIdType parg0, boolean RamBlockStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_EthMACAdressDataBlock_SetRamBlockStatus(arg1) (NvM_SetRamBlockStatus((NvM_BlockIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_PS_EthMACAdressDataBlock_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPLFM_APPL_CODE) RCtApLFMGetDCLVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpLFMGetDCLVHWFaults_OpLFMGetDCLVHWFaults(arg1) (RCtApLFMGetDCLVHWFaults(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPOFM_APPL_CODE) RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(arg1) (RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPOFM_APPL_CODE) RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(arg1) (RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx() (PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPWUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPWUM_APPL_CODE) PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPWUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd() (PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalVehicleSpeedThresholdDiag() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDGN.CalVehicleSpeedThresholdDiag) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtArrayDGN_EOL_NVM_RamMirror, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApDGN_PimDGN_EOL_NVM_RamMirror;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_PimDGN_EOL_NVM_RamMirror() (&((*RtePim_PimDGN_EOL_NVM_RamMirror())[0]))
#  else
#   define Rte_Pim_PimDGN_EOL_NVM_RamMirror() RtePim_PimDGN_EOL_NVM_RamMirror()
#  endif
#  define RtePim_PimDGN_EOL_NVM_RamMirror() \
  (&Rte_CpApDGN_PimDGN_EOL_NVM_RamMirror)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApDGN_START_SEC_CODE
# include "CtApDGN_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D407_ReadData CBReadData_DemDataClass_D407_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D40C_ReadData CBReadData_DemDataClass_D40C_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D49C_ReadData CBReadData_DemDataClass_D49C_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D4CA_ReadData CBReadData_DemDataClass_D4CA_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D5CF_ReadData CBReadData_DemDataClass_D5CF_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D5D1_ReadData CBReadData_DemDataClass_D5D1_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D805_ReadData CBReadData_DemDataClass_D805_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D806_ReadData CBReadData_DemDataClass_D806_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D807_ReadData CBReadData_DemDataClass_D807_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D808_ReadData CBReadData_DemDataClass_D808_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D809_ReadData CBReadData_DemDataClass_D809_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D80C_ReadData CBReadData_DemDataClass_D80C_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D822_ReadData CBReadData_DemDataClass_D822_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D824_ReadData CBReadData_DemDataClass_D824_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D825_ReadData CBReadData_DemDataClass_D825_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D827_ReadData CBReadData_DemDataClass_D827_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D828_ReadData CBReadData_DemDataClass_D828_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D829_ReadData CBReadData_DemDataClass_D829_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D82B_ReadData CBReadData_DemDataClass_D82B_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D82C_ReadData CBReadData_DemDataClass_D82C_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D82D_ReadData CBReadData_DemDataClass_D82D_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D82E_ReadData CBReadData_DemDataClass_D82E_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D82F_ReadData CBReadData_DemDataClass_D82F_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D831_ReadData CBReadData_DemDataClass_D831_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D83B_ReadData CBReadData_DemDataClass_D83B_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D83C_ReadData CBReadData_DemDataClass_D83C_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D83D_ReadData CBReadData_DemDataClass_D83D_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D83E_ReadData CBReadData_DemDataClass_D83E_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D83F_ReadData CBReadData_DemDataClass_D83F_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D840_ReadData CBReadData_DemDataClass_D840_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D843_ReadData CBReadData_DemDataClass_D843_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D844_ReadData CBReadData_DemDataClass_D844_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D845_ReadData CBReadData_DemDataClass_D845_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D846_ReadData CBReadData_DemDataClass_D846_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D84A_ReadData CBReadData_DemDataClass_D84A_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D84B_ReadData CBReadData_DemDataClass_D84B_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D84C_ReadData CBReadData_DemDataClass_D84C_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D84D_ReadData CBReadData_DemDataClass_D84D_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D84E_ReadData CBReadData_DemDataClass_D84E_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D84F_ReadData CBReadData_DemDataClass_D84F_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D850_ReadData CBReadData_DemDataClass_D850_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D851_ReadData CBReadData_DemDataClass_D851_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D852_ReadData CBReadData_DemDataClass_D852_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D853_ReadData CBReadData_DemDataClass_D853_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D854_ReadData CBReadData_DemDataClass_D854_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D855_ReadData CBReadData_DemDataClass_D855_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D8E9_ReadData CBReadData_DemDataClass_D8E9_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_D8EB_ReadData CBReadData_DemDataClass_D8EB_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_FE71_ReadData CBReadData_DemDataClass_FE71_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_FE72_ReadData CBReadData_DemDataClass_FE72_ReadData
#  define RTE_RUNNABLE_CBReadData_DemDataClass_FE73_ReadData CBReadData_DemDataClass_FE73_ReadData
#  define RTE_RUNNABLE_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData
#  define RTE_RUNNABLE_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData
#  define RTE_RUNNABLE_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData
#  define RTE_RUNNABLE_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData
#  define RTE_RUNNABLE_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData
#  define RTE_RUNNABLE_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Mahle_EOL_information_EOLData_ReadData DataServices_Mahle_EOL_information_EOLData_ReadData
#  define RTE_RUNNABLE_DataServices_Mahle_EOL_information_EOLData_WriteData DataServices_Mahle_EOL_information_EOLData_WriteData
#  define RTE_RUNNABLE_DataServices_PLC_MAC_address_MAC_ConditionCheckRead DataServices_PLC_MAC_address_MAC_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_PLC_MAC_address_MAC_ReadData DataServices_PLC_MAC_address_MAC_ReadData
#  define RTE_RUNNABLE_DataServices_PLC_MAC_address_MAC_WriteData DataServices_PLC_MAC_address_MAC_WriteData
#  define RTE_RUNNABLE_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData
#  define RTE_RUNNABLE_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData
#  define RTE_RUNNABLE_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData
#  define RTE_RUNNABLE_DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData
#  define RTE_RUNNABLE_DataServices_SW_version_Major_version_ConditionCheckRead DataServices_SW_version_Major_version_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_SW_version_Major_version_ReadData DataServices_SW_version_Major_version_ReadData
#  define RTE_RUNNABLE_DataServices_SW_version_Minor_version_ConditionCheckRead DataServices_SW_version_Minor_version_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_SW_version_Minor_version_ReadData DataServices_SW_version_Minor_version_ReadData
#  define RTE_RUNNABLE_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData
#  define RTE_RUNNABLE_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData
#  define RTE_RUNNABLE_DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData
#  define RTE_RUNNABLE_DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData
#  define RTE_RUNNABLE_DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData
#  define RTE_RUNNABLE_DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE60_ReadData RCBReadData_DemDataClass_FE60_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE61_ReadData RCBReadData_DemDataClass_FE61_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE62_ReadData RCBReadData_DemDataClass_FE62_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE63_ReadData RCBReadData_DemDataClass_FE63_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE64_ReadData RCBReadData_DemDataClass_FE64_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE65_ReadData RCBReadData_DemDataClass_FE65_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE66_ReadData RCBReadData_DemDataClass_FE66_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE67_ReadData RCBReadData_DemDataClass_FE67_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE68_ReadData RCBReadData_DemDataClass_FE68_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE69_ReadData RCBReadData_DemDataClass_FE69_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE6A_ReadData RCBReadData_DemDataClass_FE6A_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE6B_ReadData RCBReadData_DemDataClass_FE6B_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE6C_ReadData RCBReadData_DemDataClass_FE6C_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE6D_ReadData RCBReadData_DemDataClass_FE6D_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE6E_ReadData RCBReadData_DemDataClass_FE6E_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE6F_ReadData RCBReadData_DemDataClass_FE6F_ReadData
#  define RTE_RUNNABLE_RCBReadData_DemDataClass_FE70_ReadData RCBReadData_DemDataClass_FE70_ReadData
#  define RTE_RUNNABLE_RCtApDGN_AppCleanDTCs RCtApDGN_AppCleanDTCs
#  define RTE_RUNNABLE_RCtApDGN_init RCtApDGN_init
#  define RTE_RUNNABLE_RCtApDGN_task10ms RCtApDGN_task10ms
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Reserved_ReadData RDataServices_Authentification_Zone_ZA_Reserved_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData
#  define RTE_RUNNABLE_RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData
#  define RTE_RUNNABLE_RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData
#  define RTE_RUNNABLE_RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData
#  define RTE_RUNNABLE_RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ConditionCheckRead RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData
#  define RTE_RUNNABLE_RDataServices_Etat_du_convertisseur_de_courant_DCDC_ConditionCheckRead RDataServices_Etat_du_convertisseur_de_courant_DCDC_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData
#  define RTE_RUNNABLE_RDataServices_Etat_proximity_ST_DECT_CONNECT_ConditionCheckRead RDataServices_Etat_proximity_ST_DECT_CONNECT_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData
#  define RTE_RUNNABLE_RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData
#  define RTE_RUNNABLE_RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ConditionCheckRead RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData
#  define RTE_RUNNABLE_RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData
#  define RTE_RUNNABLE_RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData
#  define RTE_RUNNABLE_RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ConditionCheckRead RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData
#  define RTE_RUNNABLE_RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ConditionCheckRead RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData
#  define RTE_RUNNABLE_RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData
#  define RTE_RUNNABLE_RPlantModeDTCReset RPlantModeDTCReset
#  define RTE_RUNNABLE_RPlantModeDTCSet RPlantModeDTCSet
#  define RTE_RUNNABLE_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation
#  define RTE_RUNNABLE_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D407_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D407_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D40C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D40C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D49C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D49C_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D4CA_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D4CA_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D5CF_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D5CF_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D5D1_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D5D1_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D805_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D805_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D806_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D806_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D807_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D807_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D808_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D808_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D809_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D809_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D80C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D80C_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D822_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D822_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D824_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D824_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D825_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D825_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D827_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D827_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D828_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D828_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D829_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D829_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82B_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D82F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D831_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D831_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83B_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D83F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D840_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D840_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D843_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D843_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D844_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D844_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D845_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D845_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D846_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D846_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84A_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84A_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84B_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D84F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D850_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D850_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D851_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D851_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D852_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D852_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D853_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D853_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D854_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D854_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D855_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D855_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D8E9_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D8E9_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D8EB_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_D8EB_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_FE71_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_FE71_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_FE72_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_FE72_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_FE73_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) CBReadData_DemDataClass_FE73_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data10ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data20ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength(Dcm_OpStatusType OpStatus, P2VAR(uint16, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) DataLength); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data16ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Mahle_EOL_information_EOLData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Mahle_EOL_information_EOLData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data76ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Mahle_EOL_information_EOLData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Mahle_EOL_information_EOLData_WriteData(P2CONST(Dcm_Data76ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_PLC_MAC_address_MAC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_PLC_MAC_address_MAC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_PLC_MAC_address_MAC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data6ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_PLC_MAC_address_MAC_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_PLC_MAC_address_MAC_WriteData(P2CONST(Dcm_Data6ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SW_version_Major_version_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SW_version_Major_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SW_version_Major_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SW_version_Minor_version_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SW_version_Minor_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_SW_version_Minor_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE60_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE60_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE61_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE61_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE62_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE62_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE63_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE63_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE64_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE64_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE65_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE65_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE66_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE66_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE67_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE67_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE68_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE68_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE69_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE69_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6A_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6A_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6B_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE6F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE70_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RCBReadData_DemDataClass_FE70_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApDGN_CODE) RCtApDGN_AppCleanDTCs(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApDGN_CODE) RCtApDGN_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApDGN_CODE) RCtApDGN_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Reserved_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_du_convertisseur_de_courant_DCDC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_proximity_ST_DECT_CONNECT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApDGN_CODE) RPlantModeDTCReset(IdtPlantModeDTCNumber DTC_Id); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApDGN_CODE) RPlantModeDTCSet(IdtPlantModeDTCNumber DTC_Id); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, CtApDGN_CODE) ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApDGN_CODE) ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(uint8 SID, P2CONST(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApDGN_CODE) ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(uint8 SID, P2CONST(Dcm_Data4095ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif

# define CtApDGN_STOP_SEC_CODE
# include "CtApDGN_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CSDataServices_DemDataClass_D407_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D40C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D49C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D4CA_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D5CF_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D5D1_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D805_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D806_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D807_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D808_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D809_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D80C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D822_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D824_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D825_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D827_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D828_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D829_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D831_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D840_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D843_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D844_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D845_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D846_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84A_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D850_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D851_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D852_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D853_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D854_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D855_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D8E9_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D8EB_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE60_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE61_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE62_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE63_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE64_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE65_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE66_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE67_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE68_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE69_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6A_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE70_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE71_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE72_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE73_E_NOT_OK (1U)

#  define RTE_E_ClearDTC_DEM_CLEAR_BUSY (5U)

#  define RTE_E_ClearDTC_DEM_CLEAR_FAILED (7U)

#  define RTE_E_ClearDTC_DEM_CLEAR_MEMORY_ERROR (6U)

#  define RTE_E_ClearDTC_DEM_PENDING (4U)

#  define RTE_E_ClearDTC_DEM_WRONG_DTC (8U)

#  define RTE_E_ClearDTC_DEM_WRONG_DTCORIGIN (9U)

#  define RTE_E_ClearDTC_E_NOT_OK (1U)

#  define RTE_E_ClearDTC_E_OK (0U)

#  define RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_E_NOT_OK (1U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_E_NOT_OK (1U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_E_NOT_OK (1U)

#  define RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_E_NOT_OK (1U)

#  define RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_E_NOT_OK (1U)

#  define RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_E_NOT_OK (1U)

#  define RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK (1U)

#  define RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_E_NOT_OK (1U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_E_NOT_OK (1U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_E_NOT_OK (1U)

#  define RTE_E_DataServices_SW_version_Major_version_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_SW_version_Major_version_E_NOT_OK (1U)

#  define RTE_E_DataServices_SW_version_Minor_version_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_SW_version_Minor_version_E_NOT_OK (1U)

#  define RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_E_NOT_OK (1U)

#  define RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_E_NOT_OK (1U)

#  define RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_E_NOT_OK (1U)

#  define RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_E_NOT_OK (1U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_E_NOT_OK (1U)

#  define RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_EnableCondition_E_NOT_OK (1U)

#  define RTE_E_NvMService_AC3_SRBS_E_NOT_OK (1U)

#  define RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK (1U)

#  define RTE_E_OperationCycle_E_NOT_OK (1U)

#  define RTE_E_OperationCycle_E_OK (0U)

#  define RTE_E_ServiceRequestNotification_E_NOT_OK (1U)

#  define RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED (8U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPDGN_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
