/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApMSC.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApMSC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPMSC_H
# define RTE_CTAPMSC_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApMSC_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest;
extern VAR(IdtEVSEMaximumPowerLimit, RTE_VAR_INIT) Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode;
extern VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
extern VAR(BMS_MainConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState;
extern VAR(BMS_OnBoardChargerEnable, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable;
extern VAR(BSI_LockedVehicle, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle;
extern VAR(DCLV_Applied_Derating_Factor, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor;
extern VAR(DCLV_Power, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
extern VAR(OBC_OBCStartSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt;
extern VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
extern VAR(VCU_DiagMuxOnPwt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt;
extern VAR(IdtMaxInputACCurrentEVSE, RTE_VAR_INIT) Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas;
extern VAR(IdtPDERATING_OBC, RTE_VAR_INIT) Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;
extern VAR(DCDC_OBCDCDCRTPowerLoad, RTE_VAR_INIT) Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad;
extern VAR(OBC_PowerMax, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax;
extern VAR(OBC_SocketTempL, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL;
extern VAR(OBC_SocketTempN, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode (0U)
#  define Rte_InitValue_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad (0U)
#  define Rte_InitValue_PpInt_OBC_PowerMax_OBC_PowerMax (0U)
#  define Rte_InitValue_PpInt_OBC_SocketTemp_OBC_SocketTempL (50U)
#  define Rte_InitValue_PpInt_OBC_SocketTemp_OBC_SocketTempN (50U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest Rte_Read_CtApMSC_PpActiveDischargeRequest_DeActiveDischargeRequest
#  define Rte_Read_CtApMSC_PpActiveDischargeRequest_DeActiveDischargeRequest(data) (*(data) = Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit Rte_Read_CtApMSC_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit
#  define Rte_Read_CtApMSC_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(data) (*(data) = Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4 Rte_Read_CtApMSC_PpForceElockCloseMode4_DeForceElockCloseMode4
#  define Rte_Read_CtApMSC_PpForceElockCloseMode4_DeForceElockCloseMode4(data) (*(data) = Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInputVoltageMode_DeInputVoltageMode Rte_Read_CtApMSC_PpInputVoltageMode_DeInputVoltageMode
#  define Rte_Read_CtApMSC_PpInputVoltageMode_DeInputVoltageMode(data) (*(data) = Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage Rte_Read_CtApMSC_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage
#  define Rte_Read_CtApMSC_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState Rte_Read_CtApMSC_PpInt_BMS_MainConnectorState_BMS_MainConnectorState
#  define Rte_Read_CtApMSC_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable Rte_Read_CtApMSC_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable
#  define Rte_Read_CtApMSC_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle Rte_Read_CtApMSC_PpInt_BSI_LockedVehicle_BSI_LockedVehicle
#  define Rte_Read_CtApMSC_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor Rte_Read_CtApMSC_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor
#  define Rte_Read_CtApMSC_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Power_DCLV_Power Rte_Read_CtApMSC_PpInt_DCLV_Power_DCLV_Power
#  define Rte_Read_CtApMSC_PpInt_DCLV_Power_DCLV_Power(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApMSC_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApMSC_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApMSC_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApMSC_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt Rte_Read_CtApMSC_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Read_CtApMSC_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt Rte_Read_CtApMSC_PpInt_OBC_OBCStartSt_OBC_OBCStartSt
#  define Rte_Read_CtApMSC_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V Rte_Read_CtApMSC_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V
#  define Rte_Read_CtApMSC_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt Rte_Read_CtApMSC_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt
#  define Rte_Read_CtApMSC_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE Rte_Read_CtApMSC_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE
#  define Rte_Read_CtApMSC_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(data) (*(data) = Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempAC1Meas
#  define Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempAC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempAC2Meas
#  define Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempAC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempAC3Meas
#  define Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempAC3Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempACNMeas
#  define Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempACNMeas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempDC1Meas
#  define Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempDC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempDC2Meas
#  define Rte_Read_CtApMSC_PpOutputTempMeas_DeOutputTempDC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPDERATING_OBC_DePDERATING_OBC Rte_Read_CtApMSC_PpPDERATING_OBC_DePDERATING_OBC
#  define Rte_Read_CtApMSC_PpPDERATING_OBC_DePDERATING_OBC(data) (*(data) = Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode Rte_Write_CtApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode
#  define Rte_Write_CtApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data) (Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad Rte_Write_CtApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad
#  define Rte_Write_CtApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(data) (Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax Rte_Write_CtApMSC_PpInt_OBC_PowerMax_OBC_PowerMax
#  define Rte_Write_CtApMSC_PpInt_OBC_PowerMax_OBC_PowerMax(data) (Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL Rte_Write_CtApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL
#  define Rte_Write_CtApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL(data) (Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN Rte_Write_CtApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN
#  define Rte_Write_CtApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN(data) (Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)41, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalOBC_PowerMaxValue() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApMSC.CalOBC_PowerMaxValue) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeLockDelay() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApMSC.CalTimeLockDelay) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpInputVoltageThreshold_DeInputVoltageThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtOBC_PowerAvailableControlPilot, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerAvailableControlPilot;
extern VAR(IdtOBC_PowerAvailableHardware, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerAvailableHardware;
extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimMSC_PowerLatchFlag;
extern VAR(IdtOBC_PowerMaxCalculated, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerMaxCalculated;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_PowerAvailableControlPilot() \
  (&Rte_CpApMSC_PimOBC_PowerAvailableControlPilot)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_PowerAvailableHardware() \
  (&Rte_CpApMSC_PimOBC_PowerAvailableHardware)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimMSC_PowerLatchFlag() \
  (&Rte_CpApMSC_PimMSC_PowerLatchFlag)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_PowerMaxCalculated() \
  (&Rte_CpApMSC_PimOBC_PowerMaxCalculated)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApMSC_START_SEC_CODE
# include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData
#  define RTE_RUNNABLE_RCtApMSC_init RCtApMSC_init
#  define RTE_RUNNABLE_RCtApMSC_task100ms RCtApMSC_task100ms
#  define RTE_RUNNABLE_RCtApMSC_task10ms RCtApMSC_task10ms
#  define RTE_RUNNABLE_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults
#  define RTE_RUNNABLE_RoutineServices_Powerlatch_Information_Positioning_PIP_Start RoutineServices_Powerlatch_Information_Positioning_PIP_Start
# endif

FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApMSC_CODE) RCtApMSC_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApMSC_CODE) RCtApMSC_task100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApMSC_CODE) RCtApMSC_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, CtApMSC_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, CtApMSC_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApMSC_STOP_SEC_CODE
# include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK (1U)

#  define RTE_E_NvMService_AC3_SRBS_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPMSC_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
