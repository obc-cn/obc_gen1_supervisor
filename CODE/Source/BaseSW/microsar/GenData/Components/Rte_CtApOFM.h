/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApOFM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApOFM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPOFM_H
# define RTE_CTAPOFM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApOFM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
extern VAR(OBC_Fault, RTE_VAR_INIT) Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue;
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference;
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCHV_Command_Current_Reference, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
extern VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
extern VAR(DCHV_IOM_ERR_CAP_FAIL_H, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H;
extern VAR(DCHV_IOM_ERR_CAP_FAIL_L, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L;
extern VAR(DCHV_IOM_ERR_OC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT;
extern VAR(DCHV_IOM_ERR_OC_NEG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG;
extern VAR(DCHV_IOM_ERR_OC_POS, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS;
extern VAR(DCHV_IOM_ERR_OT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT;
extern VAR(DCHV_IOM_ERR_OT_IN, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN;
extern VAR(DCHV_IOM_ERR_OT_NTC_MOD5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5;
extern VAR(DCHV_IOM_ERR_OT_NTC_MOD6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6;
extern VAR(DCHV_IOM_ERR_OV_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT;
extern VAR(DCHV_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V;
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed;
extern VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status;
extern VAR(PFC_IOM_ERR_OC_PH1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1;
extern VAR(PFC_IOM_ERR_OC_PH2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2;
extern VAR(PFC_IOM_ERR_OC_PH3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3;
extern VAR(PFC_IOM_ERR_OC_SHUNT1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1;
extern VAR(PFC_IOM_ERR_OC_SHUNT2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2;
extern VAR(PFC_IOM_ERR_OC_SHUNT3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3;
extern VAR(PFC_IOM_ERR_OT_NTC1_M1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1;
extern VAR(PFC_IOM_ERR_OT_NTC1_M3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3;
extern VAR(PFC_IOM_ERR_OT_NTC1_M4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4;
extern VAR(PFC_IOM_ERR_OV_DCLINK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK;
extern VAR(PFC_IOM_ERR_OV_VPH12, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12;
extern VAR(PFC_IOM_ERR_OV_VPH23, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23;
extern VAR(PFC_IOM_ERR_OV_VPH31, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31;
extern VAR(PFC_IOM_ERR_UVLO_ISO4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4;
extern VAR(PFC_IOM_ERR_UVLO_ISO7, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7;
extern VAR(PFC_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V;
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;
extern VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpFaultChargeSoftStop_DeFaultChargeSoftStop (FALSE)
#  define Rte_InitValue_PpInt_OBC_Fault_OBC_Fault (0U)
#  define Rte_InitValue_PpOBCFaultsList_DeInlet_OvertempACSensorFault (FALSE)
#  define Rte_InitValue_PpOBCFaultsList_DeInlet_OvertempDCSensorFault (FALSE)
#  define Rte_InitValue_PpOBCFaultsList_DeOBC_HWErrors_Fault (FALSE)
#  define Rte_InitValue_PpOBCFaultsList_DeOBC_InternalComFault (FALSE)
#  define Rte_InitValue_PpOBCFaultsList_DeOBC_OvercurrentOutputFault (FALSE)
#  define Rte_InitValue_PpOBCFaultsList_DeOBC_OvertemperatureFault (FALSE)
#  define Rte_InitValue_PpOBCFaultsList_DeOBC_OvervoltageOutputFault (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest Rte_Read_CtApOFM_PpActiveDischargeRequest_DeActiveDischargeRequest
#  define Rte_Read_CtApOFM_PpActiveDischargeRequest_DeActiveDischargeRequest(data) (*(data) = Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result Rte_Read_CtApOFM_PpDCDC_POST_Result_DeDCDC_POST_Result
#  define Rte_Read_CtApOFM_PpDCDC_POST_Result_DeDCDC_POST_Result(data) (*(data) = Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput Rte_Read_CtApOFM_PpDiagnosticNoACInput_DeDiagnosticNoACInput
#  define Rte_Read_CtApOFM_PpDiagnosticNoACInput_DeDiagnosticNoACInput(data) (*(data) = Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue Rte_Read_CtApOFM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue
#  define Rte_Read_CtApOFM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference Rte_Read_CtApOFM_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference
#  define Rte_Read_CtApOFM_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(data) (*(data) = Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Read_CtApOFM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Read_CtApOFM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApOFM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApOFM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference Rte_Read_CtApOFM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference
#  define Rte_Read_CtApOFM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus Rte_Read_CtApOFM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus
#  define Rte_Read_CtApOFM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V
#  define Rte_Read_CtApOFM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage Rte_Read_CtApOFM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Read_CtApOFM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApOFM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApOFM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApOFM_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApOFM_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed Rte_Read_CtApOFM_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed
#  define Rte_Read_CtApOFM_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(data) (*(data) = Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection Rte_Read_CtApOFM_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection
#  define Rte_Read_CtApOFM_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data) (*(data) = Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status Rte_Read_CtApOFM_PpInt_OBC_Status_Delayed_OBC_Status
#  define Rte_Read_CtApOFM_PpInt_OBC_Status_Delayed_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V
#  define Rte_Read_CtApOFM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Read_CtApOFM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Read_CtApOFM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error Rte_Read_CtApOFM_PpOBCFramesReception_Error_DeOBCFramesReception_Error
#  define Rte_Read_CtApOFM_PpOBCFramesReception_Error_DeOBCFramesReception_Error(data) (*(data) = Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error Rte_Read_CtApOFM_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error
#  define Rte_Read_CtApOFM_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(data) (*(data) = Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error Rte_Read_CtApOFM_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error
#  define Rte_Read_CtApOFM_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(data) (*(data) = Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error Rte_Read_CtApOFM_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error
#  define Rte_Read_CtApOFM_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(data) (*(data) = Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error Rte_Read_CtApOFM_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error
#  define Rte_Read_CtApOFM_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(data) (*(data) = Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error Rte_Read_CtApOFM_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error
#  define Rte_Read_CtApOFM_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(data) (*(data) = Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error Rte_Read_CtApOFM_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error
#  define Rte_Read_CtApOFM_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(data) (*(data) = Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error Rte_Read_CtApOFM_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error
#  define Rte_Read_CtApOFM_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(data) (*(data) = Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error Rte_Read_CtApOFM_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error
#  define Rte_Read_CtApOFM_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(data) (*(data) = Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result Rte_Read_CtApOFM_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Read_CtApOFM_PpOBC_POST_Result_DeOBC_POST_Result(data) (*(data) = Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputELockSensor_DeOutputELockSensor Rte_Read_CtApOFM_PpOutputELockSensor_DeOutputELockSensor
#  define Rte_Read_CtApOFM_PpOutputELockSensor_DeOutputELockSensor(data) (*(data) = Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue Rte_Read_CtApOFM_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue
#  define Rte_Read_CtApOFM_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(data) (*(data) = Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempAC1Meas
#  define Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempAC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempAC2Meas
#  define Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempAC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempAC3Meas
#  define Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempAC3Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempACNMeas
#  define Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempACNMeas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempDC1Meas
#  define Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempDC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempDC2Meas
#  define Rte_Read_CtApOFM_PpOutputTempMeas_DeOutputTempDC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpFaultChargeSoftStop_DeFaultChargeSoftStop Rte_Write_CtApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop
#  define Rte_Write_CtApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop(data) (Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_Fault_OBC_Fault Rte_Write_CtApOFM_PpInt_OBC_Fault_OBC_Fault
#  define Rte_Write_CtApOFM_PpInt_OBC_Fault_OBC_Fault(data) (Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeInlet_OvertempACSensorFault Rte_Write_CtApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault(data) (Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeInlet_OvertempDCSensorFault Rte_Write_CtApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(data) (Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeOBC_HWErrors_Fault Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault(data) (Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeOBC_InternalComFault Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_InternalComFault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_InternalComFault(data) (Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeOBC_OvercurrentOutputFault Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(data) (Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeOBC_OvertemperatureFault Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault(data) (Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCFaultsList_DeOBC_OvervoltageOutputFault Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault
#  define Rte_Write_CtApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(data) (Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalOBC_MaxNumberRetries() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApOFM.CalOBC_MaxNumberRetries) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC1PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC2PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC3PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ACNPlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ActiveDischarge_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_BatteryHVUndervoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ControlPilot_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DC1PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DC2PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVHWFault_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOutputOvercurrent_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOutputShortCircuit_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOvervoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DiagnosticNoACInput_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ElockLocked_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_FaultSatellites_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_OBCInternalCom_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_PFCHWFault_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ProximityLine_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_VCUModeEPSRequest_Error;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_AC1PlugOvertemp_Error() \
  (&Rte_CpApOFM_PimOBC_AC1PlugOvertemp_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_AC2PlugOvertemp_Error() \
  (&Rte_CpApOFM_PimOBC_AC2PlugOvertemp_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_AC3PlugOvertemp_Error() \
  (&Rte_CpApOFM_PimOBC_AC3PlugOvertemp_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_ACNPlugOvertemp_Error() \
  (&Rte_CpApOFM_PimOBC_ACNPlugOvertemp_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_ActiveDischarge_Error() \
  (&Rte_CpApOFM_PimOBC_ActiveDischarge_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_BatteryHVUndervoltage_Error() \
  (&Rte_CpApOFM_PimOBC_BatteryHVUndervoltage_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_ControlPilot_Error() \
  (&Rte_CpApOFM_PimOBC_ControlPilot_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DC1PlugOvertemp_Error() \
  (&Rte_CpApOFM_PimOBC_DC1PlugOvertemp_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DC2PlugOvertemp_Error() \
  (&Rte_CpApOFM_PimOBC_DC2PlugOvertemp_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DCHVHWFault_Error() \
  (&Rte_CpApOFM_PimOBC_DCHVHWFault_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error() \
  (&Rte_CpApOFM_PimOBC_DCHVOutputOvercurrent_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error() \
  (&Rte_CpApOFM_PimOBC_DCHVOutputShortCircuit_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DCHVOvervoltage_Error() \
  (&Rte_CpApOFM_PimOBC_DCHVOvervoltage_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_DiagnosticNoACInput_Error() \
  (&Rte_CpApOFM_PimOBC_DiagnosticNoACInput_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_ElockLocked_Error() \
  (&Rte_CpApOFM_PimOBC_ElockLocked_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_FaultSatellites_Error() \
  (&Rte_CpApOFM_PimOBC_FaultSatellites_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_OBCInternalCom_Error() \
  (&Rte_CpApOFM_PimOBC_OBCInternalCom_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_PFCHWFault_Error() \
  (&Rte_CpApOFM_PimOBC_PFCHWFault_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_ProximityLine_Error() \
  (&Rte_CpApOFM_PimOBC_ProximityLine_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimOBC_VCUModeEPSRequest_Error() \
  (&Rte_CpApOFM_PimOBC_VCUModeEPSRequest_Error)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApOFM_START_SEC_CODE
# include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_OFM_HW_faults_DataRecord_ReadData DataServices_OFM_HW_faults_DataRecord_ReadData
#  define RTE_RUNNABLE_RCtApOFM_init RCtApOFM_init
#  define RTE_RUNNABLE_RCtApOFM_task10ms RCtApOFM_task10ms
#  define RTE_RUNNABLE_RDataServices_OFM_Faults_DataRecord_ConditionCheckRead RDataServices_OFM_Faults_DataRecord_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_OFM_Faults_DataRecord_ReadData RDataServices_OFM_Faults_DataRecord_ReadData
#  define RTE_RUNNABLE_RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults
#  define RTE_RUNNABLE_RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults
# endif

FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApOFM_CODE) RCtApOFM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApOFM_CODE) RCtApOFM_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApOFM_CODE) RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApOFM_CODE) RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApOFM_STOP_SEC_CODE
# include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPOFM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
