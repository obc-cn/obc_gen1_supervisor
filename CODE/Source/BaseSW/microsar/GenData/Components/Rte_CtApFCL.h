/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApFCL.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApFCL>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPFCL_H
# define RTE_CTAPFCL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApFCL_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result;
extern VAR(IdtHWEditionDetected, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected;
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue;
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue;
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue;
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;
extern VAR(IdtPlugLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl;
extern VAR(IdtExtPlgLedrCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP;
extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl;
extern VAR(IdtPlgLedrCtrl, RTE_VAR_INIT) Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue (TRUE)
#  define Rte_InitValue_PpExtChLedRGBCtl_DeExtChLedBrCtl (0U)
#  define Rte_InitValue_PpExtChLedRGBCtl_DeExtChLedGrCtl (0U)
#  define Rte_InitValue_PpExtChLedRGBCtl_DeExtChLedRrCtl (0U)
#  define Rte_InitValue_PpExtPlgLedrCtl_DeExtPlgLedrCtl (0U)
#  define Rte_InitValue_PpLedFaults_DeLedBlueFaultOC (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedBlueFaultSCG (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedBlueFaultSCP (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedGreenFaultOC (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedGreenFaultSCG (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedGreenFaultSCP (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedRedFaultOC (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedRedFaultSCG (FALSE)
#  define Rte_InitValue_PpLedFaults_DeLedRedFaultSCP (FALSE)
#  define Rte_InitValue_PpLedMonitoringConditions_DeBlueLedMonitoringConditions (FALSE)
#  define Rte_InitValue_PpLedMonitoringConditions_DeGreenLedMonitoringConditions (FALSE)
#  define Rte_InitValue_PpLedMonitoringConditions_DePlugLedMonitoringConditions (FALSE)
#  define Rte_InitValue_PpLedMonitoringConditions_DeRedLedMonitoringConditions (FALSE)
#  define Rte_InitValue_PpPlugLedFaults_DeLedPlugFaultOC (FALSE)
#  define Rte_InitValue_PpPlugLedFaults_DeLedPlugFaultSCG (FALSE)
#  define Rte_InitValue_PpPlugLedFaults_DeLedPlugFaultSCP (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApFCL_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApFCL_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApFCL_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApFCL_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState Rte_Read_CtApFCL_PpBatteryVoltageState_DeBatteryVoltageState
#  define Rte_Read_CtApFCL_PpBatteryVoltageState_DeBatteryVoltageState(data) (*(data) = Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpChLedRGBCtl_DeChLedBrCtl Rte_Read_CtApFCL_PpChLedRGBCtl_DeChLedBrCtl
#  define Rte_Read_CtApFCL_PpChLedRGBCtl_DeChLedBrCtl(data) (*(data) = Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpChLedRGBCtl_DeChLedGrCtl Rte_Read_CtApFCL_PpChLedRGBCtl_DeChLedGrCtl
#  define Rte_Read_CtApFCL_PpChLedRGBCtl_DeChLedGrCtl(data) (*(data) = Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpChLedRGBCtl_DeChLedRrCtl Rte_Read_CtApFCL_PpChLedRGBCtl_DeChLedRrCtl
#  define Rte_Read_CtApFCL_PpChLedRGBCtl_DeChLedRrCtl(data) (*(data) = Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result Rte_Read_CtApFCL_PpDCDC_POST_Result_DeDCDC_POST_Result
#  define Rte_Read_CtApFCL_PpDCDC_POST_Result_DeDCDC_POST_Result(data) (*(data) = Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpHWEditionDetected_DeHWEditionDetected Rte_Read_CtApFCL_PpHWEditionDetected_DeHWEditionDetected
#  define Rte_Read_CtApFCL_PpHWEditionDetected_DeHWEditionDetected(data) (*(data) = Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Read_CtApFCL_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApFCL_PpInt_ABS_VehSpd_ABS_VehSpd(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb Rte_Read_CtApFCL_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb
#  define Rte_Read_CtApFCL_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue Rte_Read_CtApFCL_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue
#  define Rte_Read_CtApFCL_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue Rte_Read_CtApFCL_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue
#  define Rte_Read_CtApFCL_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue Rte_Read_CtApFCL_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue
#  define Rte_Read_CtApFCL_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue Rte_Read_CtApFCL_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue
#  define Rte_Read_CtApFCL_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result Rte_Read_CtApFCL_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Read_CtApFCL_PpOBC_POST_Result_DeOBC_POST_Result(data) (*(data) = Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl Rte_Read_CtApFCL_PpPlgLedrCtrl_DePlgLedrCtrl
#  define Rte_Read_CtApFCL_PpPlgLedrCtrl_DePlgLedrCtrl(data) (*(data) = Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue Rte_Read_CtApFCL_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue
#  define Rte_Read_CtApFCL_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue Rte_Write_CtApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue
#  define Rte_Write_CtApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(data) (Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl Rte_Write_CtApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl
#  define Rte_Write_CtApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl(data) (Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl Rte_Write_CtApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl
#  define Rte_Write_CtApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl(data) (Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl Rte_Write_CtApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl
#  define Rte_Write_CtApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl(data) (Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl Rte_Write_CtApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl
#  define Rte_Write_CtApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl(data) (Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedBlueFaultOC Rte_Write_CtApFCL_PpLedFaults_DeLedBlueFaultOC
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedBlueFaultOC(data) (Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedBlueFaultSCG Rte_Write_CtApFCL_PpLedFaults_DeLedBlueFaultSCG
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedBlueFaultSCG(data) (Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedBlueFaultSCP Rte_Write_CtApFCL_PpLedFaults_DeLedBlueFaultSCP
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedBlueFaultSCP(data) (Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedGreenFaultOC Rte_Write_CtApFCL_PpLedFaults_DeLedGreenFaultOC
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedGreenFaultOC(data) (Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedGreenFaultSCG Rte_Write_CtApFCL_PpLedFaults_DeLedGreenFaultSCG
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedGreenFaultSCG(data) (Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedGreenFaultSCP Rte_Write_CtApFCL_PpLedFaults_DeLedGreenFaultSCP
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedGreenFaultSCP(data) (Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedRedFaultOC Rte_Write_CtApFCL_PpLedFaults_DeLedRedFaultOC
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedRedFaultOC(data) (Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedRedFaultSCG Rte_Write_CtApFCL_PpLedFaults_DeLedRedFaultSCG
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedRedFaultSCG(data) (Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedFaults_DeLedRedFaultSCP Rte_Write_CtApFCL_PpLedFaults_DeLedRedFaultSCP
#  define Rte_Write_CtApFCL_PpLedFaults_DeLedRedFaultSCP(data) (Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions Rte_Write_CtApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions
#  define Rte_Write_CtApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(data) (Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions Rte_Write_CtApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions
#  define Rte_Write_CtApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(data) (Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions Rte_Write_CtApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions
#  define Rte_Write_CtApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions(data) (Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions Rte_Write_CtApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions
#  define Rte_Write_CtApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions(data) (Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC Rte_Write_CtApFCL_PpPlugLedFaults_DeLedPlugFaultOC
#  define Rte_Write_CtApFCL_PpPlugLedFaults_DeLedPlugFaultOC(data) (Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG Rte_Write_CtApFCL_PpPlugLedFaults_DeLedPlugFaultSCG
#  define Rte_Write_CtApFCL_PpPlugLedFaults_DeLedPlugFaultSCG(data) (Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP Rte_Write_CtApFCL_PpPlugLedFaults_DeLedPlugFaultSCP
#  define Rte_Write_CtApFCL_PpPlugLedFaults_DeLedPlugFaultSCP(data) (Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalAfts_DelayLedBlueOff() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedBlueOff) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedBlueOn() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedBlueOn) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedGreenOff() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedGreenOff) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedGreenOn() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedGreenOn) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedPlugOff() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedPlugOff) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedPlugOn() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedPlugOn) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedRedOff() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedRedOff) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_DelayLedRedOn() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalAfts_DelayLedRedOn) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueThresholdSCG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueThresholdSCG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueThresholdSCP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueThresholdSCP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenThresholdSCG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenThresholdSCG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenThresholdSCP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenThresholdSCP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedThresholdSCG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedThresholdSCG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedThresholdSCP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedThresholdSCP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxNormalPlugLed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalMaxNormalPlugLed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinNormalPlugLed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalMinNormalPlugLed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalPlugLedMaxOCDetection() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalPlugLedMaxOCDetection) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalPlugLedMinOCDetection() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalPlugLedMinOCDetection) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalPlugLedThresholdSCG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalPlugLedThresholdSCG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalPlugLedThresholdSCP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalPlugLedThresholdSCP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueOCConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueOCConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueOCHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueOCHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedBlueSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedBlueSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenOCConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenOCConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenOCHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenOCHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedGreenSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedGreenSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedPlugOCConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedPlugOCConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedPlugOCHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedPlugOCHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedPlugSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedPlugSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedPlugSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedPlugSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedPlugSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedPlugSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedPlugSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedPlugSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedOCConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedOCConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedOCHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedOCHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLedRedSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalLedRedSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalEnableLedBlue() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalEnableLedBlue) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalEnableLedGreen() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalEnableLedGreen) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalEnableLedPlug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalEnableLedPlug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalEnableLedRed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCL.CalEnableLedRed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpBatteryVoltMaxTest_DeBatteryVoltMaxTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpBatteryVoltMinTest_DeBatteryVoltMinTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpVehStopMaxTest_DeVehStopMaxTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultSCP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedBlueCounterOC() \
  (&Rte_CpApFCL_PimLedBlueCounterOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedBlueCounterSCG() \
  (&Rte_CpApFCL_PimLedBlueCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedBlueCounterSCP() \
  (&Rte_CpApFCL_PimLedBlueCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedGreenCounterOC() \
  (&Rte_CpApFCL_PimLedGreenCounterOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedGreenCounterSCG() \
  (&Rte_CpApFCL_PimLedGreenCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedGreenCounterSCP() \
  (&Rte_CpApFCL_PimLedGreenCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedPlugCounterOC() \
  (&Rte_CpApFCL_PimLedPlugCounterOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedPlugCounterSCG() \
  (&Rte_CpApFCL_PimLedPlugCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedPlugCounterSCP() \
  (&Rte_CpApFCL_PimLedPlugCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedRedCounterOC() \
  (&Rte_CpApFCL_PimLedRedCounterOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedRedCounterSCG() \
  (&Rte_CpApFCL_PimLedRedCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedRedCounterSCP() \
  (&Rte_CpApFCL_PimLedRedCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedBluePrefaultOC() \
  (&Rte_CpApFCL_PimLedBluePrefaultOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedBluePrefaultSCG() \
  (&Rte_CpApFCL_PimLedBluePrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedBluePrefaultSCP() \
  (&Rte_CpApFCL_PimLedBluePrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedGreenPrefaultOC() \
  (&Rte_CpApFCL_PimLedGreenPrefaultOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedGreenPrefaultSCG() \
  (&Rte_CpApFCL_PimLedGreenPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedGreenPrefaultSCP() \
  (&Rte_CpApFCL_PimLedGreenPrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedPlugPrefaultOC() \
  (&Rte_CpApFCL_PimLedPlugPrefaultOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedPlugPrefaultSCG() \
  (&Rte_CpApFCL_PimLedPlugPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedPlugPrefaultSCP() \
  (&Rte_CpApFCL_PimLedPlugPrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedRedPrefaultOC() \
  (&Rte_CpApFCL_PimLedRedPrefaultOC)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedRedPrefaultSCG() \
  (&Rte_CpApFCL_PimLedRedPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimLedRedPrefaultSCP() \
  (&Rte_CpApFCL_PimLedRedPrefaultSCP)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApFCL_START_SEC_CODE
# include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApFCL_init RCtApFCL_init
#  define RTE_RUNNABLE_RCtApFCL_task100ms RCtApFCL_task100ms
#  define RTE_RUNNABLE_RCtApFCL_task10ms RCtApFCL_task10ms
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start
#  define RTE_RUNNABLE_RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop
# endif

FUNC(void, CtApFCL_CODE) RCtApFCL_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApFCL_CODE) RCtApFCL_task100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApFCL_CODE) RCtApFCL_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApFCL_STOP_SEC_CODE
# include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPFCL_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
