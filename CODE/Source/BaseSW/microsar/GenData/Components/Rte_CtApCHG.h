/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApCHG.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApCHG>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPCHG_H
# define RTE_CTAPCHG_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApCHG_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput;
extern VAR(IdtEVSEMaximumPowerLimit, RTE_VAR_INIT) Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed;
extern VAR(DCDC_OBCMainContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq;
extern VAR(DCDC_OBCQuickChargeContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq;
extern VAR(EVSE_RTAB_STOP_CHARGE, RTE_VAR_INIT) Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE;
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange;
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange;
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
extern VAR(OBC_OBCStartSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpStopConditions_DeStopConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange;
extern VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
extern VAR(BMS_HighestChargeCurrentAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow;
extern VAR(BMS_HighestChargeVoltageAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow;
extern VAR(BMS_MainConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState;
extern VAR(BMS_OnBoardChargerEnable, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable;
extern VAR(BMS_QuickChargeConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState;
extern VAR(BMS_SOC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC;
extern VAR(BMS_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage;
extern VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
extern VAR(OBC_CP_connection_Status, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_Fault, RTE_VAR_INIT) Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault;
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed;
extern VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
extern VAR(PFC_VPH1_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz;
extern VAR(PFC_VPH2_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz;
extern VAR(PFC_VPH3_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz;
extern VAR(PFC_VPH12_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
extern VAR(PFC_VPH23_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V;
extern VAR(PFC_VPH31_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V;
extern VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
extern VAR(PFC_VPH23_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V;
extern VAR(PFC_VPH31_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V;
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpOBCDerating_DeOBCDerating;
extern VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpControlPilotFreqError_DeControlPilotFreqError (FALSE)
#  define Rte_InitValue_PpDiagnosticNoACInput_DeDiagnosticNoACInput (FALSE)
#  define Rte_InitValue_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit (0U)
#  define Rte_InitValue_PpElockFaultDetected_DeElockFaultDetected (FALSE)
#  define Rte_InitValue_PpForceElockCloseMode4_DeForceElockCloseMode4 (FALSE)
#  define Rte_InitValue_PpInputVoltageMode_DeInputVoltageMode (0U)
#  define Rte_InitValue_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed (0U)
#  define Rte_InitValue_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq (2U)
#  define Rte_InitValue_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq (2U)
#  define Rte_InitValue_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE (FALSE)
#  define Rte_InitValue_PpInt_OBC_ACRange_OBC_ACRange (0U)
#  define Rte_InitValue_PpInt_OBC_ACRange_Delayed_OBC_ACRange (0U)
#  define Rte_InitValue_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt (0U)
#  define Rte_InitValue_PpInt_OBC_OBCStartSt_OBC_OBCStartSt (FALSE)
#  define Rte_InitValue_PpInt_OBC_Status_OBC_Status (0U)
#  define Rte_InitValue_PpInt_OBC_Status_Delayed_OBC_Status (0U)
#  define Rte_InitValue_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress (FALSE)
#  define Rte_InitValue_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue (FALSE)
#  define Rte_InitValue_PpRequestHWStopOBC_DeRequestHWStopOBC (FALSE)
#  define Rte_InitValue_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue (TRUE)
#  define Rte_InitValue_PpStopConditions_DeStopConditions (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest Rte_Read_CtApCHG_PpActiveDischargeRequest_DeActiveDischargeRequest
#  define Rte_Read_CtApCHG_PpActiveDischargeRequest_DeActiveDischargeRequest(data) (*(data) = Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode Rte_Read_CtApCHG_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode
#  define Rte_Read_CtApCHG_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data) (*(data) = Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop Rte_Read_CtApCHG_PpFaultChargeSoftStop_DeFaultChargeSoftStop
#  define Rte_Read_CtApCHG_PpFaultChargeSoftStop_DeFaultChargeSoftStop(data) (*(data) = Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFreqOutRange_DeFreqOutRange Rte_Read_CtApCHG_PpFreqOutRange_DeFreqOutRange
#  define Rte_Read_CtApCHG_PpFreqOutRange_DeFreqOutRange(data) (*(data) = Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage Rte_Read_CtApCHG_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage
#  define Rte_Read_CtApCHG_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow Rte_Read_CtApCHG_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow
#  define Rte_Read_CtApCHG_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow Rte_Read_CtApCHG_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow
#  define Rte_Read_CtApCHG_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState Rte_Read_CtApCHG_PpInt_BMS_MainConnectorState_BMS_MainConnectorState
#  define Rte_Read_CtApCHG_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable Rte_Read_CtApCHG_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable
#  define Rte_Read_CtApCHG_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState Rte_Read_CtApCHG_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState
#  define Rte_Read_CtApCHG_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_SOC_BMS_SOC Rte_Read_CtApCHG_PpInt_BMS_SOC_BMS_SOC
#  define Rte_Read_CtApCHG_PpInt_BMS_SOC_BMS_SOC(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_Voltage_BMS_Voltage Rte_Read_CtApCHG_PpInt_BMS_Voltage_BMS_Voltage
#  define Rte_Read_CtApCHG_PpInt_BMS_Voltage_BMS_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus Rte_Read_CtApCHG_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus
#  define Rte_Read_CtApCHG_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status Rte_Read_CtApCHG_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status
#  define Rte_Read_CtApCHG_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(data) (*(data) = Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApCHG_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApCHG_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApCHG_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApCHG_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Fault_OBC_Fault Rte_Read_CtApCHG_PpInt_OBC_Fault_OBC_Fault
#  define Rte_Read_CtApCHG_PpInt_OBC_Fault_OBC_Fault(data) (*(data) = Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed Rte_Read_CtApCHG_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed
#  define Rte_Read_CtApCHG_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(data) (*(data) = Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection Rte_Read_CtApCHG_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection
#  define Rte_Read_CtApCHG_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data) (*(data) = Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz Rte_Read_CtApCHG_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz Rte_Read_CtApCHG_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz Rte_Read_CtApCHG_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V Rte_Read_CtApCHG_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V Rte_Read_CtApCHG_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V Rte_Read_CtApCHG_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V Rte_Read_CtApCHG_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V Rte_Read_CtApCHG_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V Rte_Read_CtApCHG_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V
#  define Rte_Read_CtApCHG_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Read_CtApCHG_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Read_CtApCHG_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBCDerating_DeOBCDerating Rte_Read_CtApCHG_PpOBCDerating_DeOBCDerating
#  define Rte_Read_CtApCHG_PpOBCDerating_DeOBCDerating(data) (*(data) = Rte_CpApDER_PpOBCDerating_DeOBCDerating, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputELockSensor_DeOutputELockSensor Rte_Read_CtApCHG_PpOutputELockSensor_DeOutputELockSensor
#  define Rte_Read_CtApCHG_PpOutputELockSensor_DeOutputELockSensor(data) (*(data) = Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue Rte_Read_CtApCHG_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue
#  define Rte_Read_CtApCHG_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue Rte_Read_CtApCHG_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue
#  define Rte_Read_CtApCHG_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeState_DePlantModeState Rte_Read_CtApCHG_PpPlantModeState_DePlantModeState
#  define Rte_Read_CtApCHG_PpPlantModeState_DePlantModeState(data) (*(data) = Rte_CpApPLT_PpPlantModeState_DePlantModeState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC1FaultSCG Rte_Read_CtApCHG_PpTempFaults_DeTempDC1FaultSCG
#  define Rte_Read_CtApCHG_PpTempFaults_DeTempDC1FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC1FaultSCP Rte_Read_CtApCHG_PpTempFaults_DeTempDC1FaultSCP
#  define Rte_Read_CtApCHG_PpTempFaults_DeTempDC1FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC2FaultSCG Rte_Read_CtApCHG_PpTempFaults_DeTempDC2FaultSCG
#  define Rte_Read_CtApCHG_PpTempFaults_DeTempDC2FaultSCG(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempFaults_DeTempDC2FaultSCP Rte_Read_CtApCHG_PpTempFaults_DeTempDC2FaultSCP
#  define Rte_Read_CtApCHG_PpTempFaults_DeTempDC2FaultSCP(data) (*(data) = Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError Rte_Write_CtApCHG_PpControlPilotFreqError_DeControlPilotFreqError
#  define Rte_Write_CtApCHG_PpControlPilotFreqError_DeControlPilotFreqError(data) (Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput Rte_Write_CtApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput
#  define Rte_Write_CtApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput(data) (Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit Rte_Write_CtApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit
#  define Rte_Write_CtApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(data) (Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElockFaultDetected_DeElockFaultDetected Rte_Write_CtApCHG_PpElockFaultDetected_DeElockFaultDetected
#  define Rte_Write_CtApCHG_PpElockFaultDetected_DeElockFaultDetected(data) (Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4 Rte_Write_CtApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4
#  define Rte_Write_CtApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4(data) (Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInputVoltageMode_DeInputVoltageMode Rte_Write_CtApCHG_PpInputVoltageMode_DeInputVoltageMode
#  define Rte_Write_CtApCHG_PpInputVoltageMode_DeInputVoltageMode(data) (Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed Rte_Write_CtApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed
#  define Rte_Write_CtApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(data) (Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq Rte_Write_CtApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq
#  define Rte_Write_CtApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(data) (Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq Rte_Write_CtApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq
#  define Rte_Write_CtApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(data) (Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE Rte_Write_CtApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE
#  define Rte_Write_CtApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(data) (Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_ACRange_OBC_ACRange Rte_Write_CtApCHG_PpInt_OBC_ACRange_OBC_ACRange
#  define Rte_Write_CtApCHG_PpInt_OBC_ACRange_OBC_ACRange(data) (Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange Rte_Write_CtApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange
#  define Rte_Write_CtApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange(data) (Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt Rte_Write_CtApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Write_CtApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data) (Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt Rte_Write_CtApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt
#  define Rte_Write_CtApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(data) (Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_Status_OBC_Status Rte_Write_CtApCHG_PpInt_OBC_Status_OBC_Status
#  define Rte_Write_CtApCHG_PpInt_OBC_Status_OBC_Status(data) (Rte_CpApCHG_PpInt_OBC_Status_OBC_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status Rte_Write_CtApCHG_PpInt_OBC_Status_Delayed_OBC_Status
#  define Rte_Write_CtApCHG_PpInt_OBC_Status_Delayed_OBC_Status(data) (Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress Rte_Write_CtApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress
#  define Rte_Write_CtApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(data) (Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue Rte_Write_CtApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue
#  define Rte_Write_CtApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(data) (Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC Rte_Write_CtApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC
#  define Rte_Write_CtApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC(data) (Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue Rte_Write_CtApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue
#  define Rte_Write_CtApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(data) (Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpStopConditions_DeStopConditions Rte_Write_CtApCHG_PpStopConditions_DeStopConditions
#  define Rte_Write_CtApCHG_PpStopConditions_DeStopConditions(data) (Rte_CpApCHG_PpStopConditions_DeStopConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_COMM_APPL_CODE) ComM_RequestComMode(ComM_UserHandleType parg0, ComM_ModeType ComMode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(arg1) (ComM_RequestComMode((ComM_UserHandleType)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalMaxDemmandVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxDemmandVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxDiscoveryVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxDiscoveryVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxPrechargeVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxPrechargeVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalStopChargeDemmandTimer() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalStopChargeDemmandTimer) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfirmNoAC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalTimeConfirmNoAC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalBulkSocConf() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalBulkSocConf) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDemandMaxCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalDemandMaxCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalEnergyCapacity() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalEnergyCapacity) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxDemmandCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxDemmandCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxDiscoveryCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxDiscoveryCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxInputVoltage110V() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxInputVoltage110V) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxInputVoltage220V() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxInputVoltage220V) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxPrechargeCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMaxPrechargeCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinInputVoltage110V() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMinInputVoltage110V) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinInputVoltage220V() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalMinInputVoltage220V) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTargetPrechargeCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalTargetPrechargeCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdNoAC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalThresholdNoAC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeElockFaultDetected() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCHG.CalTimeElockFaultDetected) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpInputVoltageThreshold_DeInputVoltageThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApCHG_START_SEC_CODE
# include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_V2G_States_Debug_CHGDebugData_ReadData DataServices_V2G_States_Debug_CHGDebugData_ReadData
#  define RTE_RUNNABLE_RCtApCHG_init RCtApCHG_init
#  define RTE_RUNNABLE_RCtApCHG_task10msA RCtApCHG_task10msA
#  define RTE_RUNNABLE_RCtApCHG_task10msB RCtApCHG_task10msB
#  define RTE_RUNNABLE_RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_V2G_States_Debug_CHGInternalState_ReadData RDataServices_V2G_States_Debug_CHGInternalState_ReadData
#  define RTE_RUNNABLE_RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_V2G_States_Debug_SCCStateMachine_ReadData RDataServices_V2G_States_Debug_SCCStateMachine_ReadData
# endif

FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApCHG_CODE) RCtApCHG_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApCHG_CODE) RCtApCHG_task10msA(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApCHG_CODE) RCtApCHG_task10msB(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif

# define CtApCHG_STOP_SEC_CODE
# include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_ComM_UserRequest_E_MODE_LIMITATION (2U)

#  define RTE_E_ComM_UserRequest_E_NOT_OK (1U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK (1U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK (1U)

#  define RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPCHG_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
