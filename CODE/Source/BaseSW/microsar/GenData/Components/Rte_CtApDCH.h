/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApDCH.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApDCH>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPDCH_H
# define RTE_CTAPDCH_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApDCH_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest;
extern VAR(DCDC_ActivedischargeSt, RTE_VAR_INIT) Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue;
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status;
extern VAR(PFC_PFCStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus;
extern VAR(PFC_Vdclink_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
extern VAR(VCU_ActivedischargeCommand, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpActiveDischargeRequest_DeActiveDischargeRequest (FALSE)
#  define Rte_InitValue_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt (0U)
#  define Rte_InitValue_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue (FALSE)
#  define Rte_InitValue_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApDCH_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApDCH_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC Rte_Read_CtApDCH_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC
#  define Rte_Read_CtApDCH_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status Rte_Read_CtApDCH_PpInt_DCDC_Status_Delayed_DCDC_Status
#  define Rte_Read_CtApDCH_PpInt_DCDC_Status_Delayed_DCDC_Status(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApDCH_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApDCH_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage Rte_Read_CtApDCH_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Read_CtApDCH_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange Rte_Read_CtApDCH_PpInt_OBC_ACRange_Delayed_OBC_ACRange
#  define Rte_Read_CtApDCH_PpInt_OBC_ACRange_Delayed_OBC_ACRange(data) (*(data) = Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status Rte_Read_CtApDCH_PpInt_OBC_Status_Delayed_OBC_Status
#  define Rte_Read_CtApDCH_PpInt_OBC_Status_Delayed_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus Rte_Read_CtApDCH_PpInt_PFC_PFCStatus_PFC_PFCStatus
#  define Rte_Read_CtApDCH_PpInt_PFC_PFCStatus_PFC_PFCStatus(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V Rte_Read_CtApDCH_PpInt_PFC_Vdclink_V_PFC_Vdclink_V
#  define Rte_Read_CtApDCH_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand Rte_Read_CtApDCH_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand
#  define Rte_Read_CtApDCH_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result Rte_Read_CtApDCH_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Read_CtApDCH_PpOBC_POST_Result_DeOBC_POST_Result(data) (*(data) = Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest Rte_Write_CtApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest
#  define Rte_Write_CtApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest(data) (Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt Rte_Write_CtApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt
#  define Rte_Write_CtApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(data) (Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue Rte_Write_CtApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue
#  define Rte_Write_CtApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(data) (Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue Rte_Write_CtApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue
#  define Rte_Write_CtApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(data) (Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalThresholdUVTripDischarge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDCH.CalThresholdUVTripDischarge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeUVTripDischarge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDCH.CalTimeUVTripDischarge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApDCH_START_SEC_CODE
# include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApDCH_init RCtApDCH_init
#  define RTE_RUNNABLE_RCtApDCH_task10ms RCtApDCH_task10ms
# endif

FUNC(void, CtApDCH_CODE) RCtApDCH_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApDCH_CODE) RCtApDCH_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApDCH_STOP_SEC_CODE
# include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPDCH_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
