/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApWUM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApWUM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPWUM_H
# define RTE_CTAPWUM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApWUM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtDutyControlPilot, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot;
extern VAR(BSI_PostDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup;
extern VAR(BSI_PreDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup;
extern VAR(BSI_VCUHeatPumpWorkingMode, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode;
extern VAR(COUPURE_CONSO_CTP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP;
extern VAR(COUPURE_CONSO_CTPE2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
extern VAR(VCU_DDEGMVSEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM;
extern VAR(VCU_DMDActivChiller, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller;
extern VAR(VCU_DMDMeap2SEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM;
extern VAR(VCU_DiagMuxOnPwt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt;
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
extern VAR(VCU_PrecondElecWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup;
extern VAR(VCU_StopDelayedHMIWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP;
extern VAR(SUPV_CoolingWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState;
extern VAR(SUPV_HVBattChargeWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState;
extern VAR(SUPV_HoldDiscontactorWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState;
extern VAR(SUPV_PIStateInfoWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP;
extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpCANComRequest_DeCANComRequest;
extern VAR(OBC_PushChargeType, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType;
extern VAR(OBC_RechargeHMIState, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState;
extern VAR(SUPV_RCDLineState, RTE_VAR_INIT) Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC (FALSE)
#  define Rte_InitValue_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP (FALSE)
#  define Rte_InitValue_PpDiagToolsRequest_DeDiagToolsRequest (FALSE)
#  define Rte_InitValue_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest (FALSE)
#  define Rte_InitValue_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP (FALSE)
#  define Rte_InitValue_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP (FALSE)
#  define Rte_InitValue_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState (FALSE)
#  define Rte_InitValue_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState (FALSE)
#  define Rte_InitValue_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState (FALSE)
#  define Rte_InitValue_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState (FALSE)
#  define Rte_InitValue_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApWUM_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApWUM_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpCANComRequest_DeCANComRequest Rte_Read_CtApWUM_PpCANComRequest_DeCANComRequest
#  define Rte_Read_CtApWUM_PpCANComRequest_DeCANComRequest(data) (*(data) = Rte_CpApAEM_PpCANComRequest_DeCANComRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDutyControlPilot_DeDutyControlPilot Rte_Read_CtApWUM_PpDutyControlPilot_DeDutyControlPilot
#  define Rte_Read_CtApWUM_PpDutyControlPilot_DeDutyControlPilot(data) (*(data) = Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup Rte_Read_CtApWUM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup
#  define Rte_Read_CtApWUM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup Rte_Read_CtApWUM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup
#  define Rte_Read_CtApWUM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode Rte_Read_CtApWUM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode
#  define Rte_Read_CtApWUM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP Rte_Read_CtApWUM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP
#  define Rte_Read_CtApWUM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(data) (*(data) = Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 Rte_Read_CtApWUM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2
#  define Rte_Read_CtApWUM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(data) (*(data) = Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApWUM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApWUM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection Rte_Read_CtApWUM_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection
#  define Rte_Read_CtApWUM_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data) (*(data) = Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType Rte_Read_CtApWUM_PpInt_OBC_PushChargeType_OBC_PushChargeType
#  define Rte_Read_CtApWUM_PpInt_OBC_PushChargeType_OBC_PushChargeType(data) (*(data) = Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState Rte_Read_CtApWUM_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState
#  define Rte_Read_CtApWUM_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data) (*(data) = Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState Rte_Read_CtApWUM_PpInt_SUPV_RCDLineState_SUPV_RCDLineState
#  define Rte_Read_CtApWUM_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data) (*(data) = Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM Rte_Read_CtApWUM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM
#  define Rte_Read_CtApWUM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller Rte_Read_CtApWUM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller
#  define Rte_Read_CtApWUM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM Rte_Read_CtApWUM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM
#  define Rte_Read_CtApWUM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt Rte_Read_CtApWUM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt
#  define Rte_Read_CtApWUM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Read_CtApWUM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Read_CtApWUM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup Rte_Read_CtApWUM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup
#  define Rte_Read_CtApWUM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup Rte_Read_CtApWUM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup
#  define Rte_Read_CtApWUM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization Rte_Read_CtApWUM_PpShutdownAuthorization_DeShutdownAuthorization
#  define Rte_Read_CtApWUM_PpShutdownAuthorization_DeShutdownAuthorization(data) (*(data) = Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC Rte_Write_CtApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC
#  define Rte_Write_CtApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(data) (Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP Rte_Write_CtApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP
#  define Rte_Write_CtApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(data) (Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest Rte_Write_CtApWUM_PpDiagToolsRequest_DeDiagToolsRequest
#  define Rte_Write_CtApWUM_PpDiagToolsRequest_DeDiagToolsRequest(data) (Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest Rte_Write_CtApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest
#  define Rte_Write_CtApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(data) (Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP Rte_Write_CtApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP
#  define Rte_Write_CtApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(data) (Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP Rte_Write_CtApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP
#  define Rte_Write_CtApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(data) (Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState Rte_Write_CtApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState
#  define Rte_Write_CtApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(data) (Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState Rte_Write_CtApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState
#  define Rte_Write_CtApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(data) (Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState Rte_Write_CtApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState
#  define Rte_Write_CtApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(data) (Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState Rte_Write_CtApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState
#  define Rte_Write_CtApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(data) (Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP Rte_Write_CtApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP
#  define Rte_Write_CtApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(data) (Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_ReadBlock(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(arg1) (NvM_ReadBlock((NvM_BlockIdType)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_COMM_APPL_CODE) ComM_RequestComMode(ComM_UserHandleType parg0, ComM_ModeType ComMode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(arg1) (ComM_RequestComMode((ComM_UserHandleType)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(arg1) (ComM_RequestComMode((ComM_UserHandleType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPDGN_APPL_CODE) RCtApDGN_AppCleanDTCs(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs() (RCtApDGN_AppCleanDTCs(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalMaxTimeDiagTools() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalMaxTimeDiagTools) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeWakeupHoldDiscontactorMax() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalTimeWakeupHoldDiscontactorMax) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceHoldNetworkCom() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalDebounceHoldNetworkCom) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceTempoEndCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalDebounceTempoEndCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxTimeShutdown() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalMaxTimeShutdown) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeWakeupCoolingMax() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalTimeWakeupCoolingMax) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeWakeupCoolingMin() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalTimeWakeupCoolingMin) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeWakeupHoldDiscontactorMin() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalTimeWakeupHoldDiscontactorMin) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeWakeupPiStateInfo() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApWUM.CalTimeWakeupPiStateInfo) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_NvWUMBlockNeed_DefaultValue() (Rte_CpApWUM_NvWUMBlockNeed_MirrorBlock) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApWUM_NvWUMBlockNeed_MirrorBlock;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_NvWUMBlockNeed_MirrorBlock() \
  (&Rte_CpApWUM_NvWUMBlockNeed_MirrorBlock)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApWUM_START_SEC_CODE
# include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd
#  define RTE_RUNNABLE_RCtApWUM_init RCtApWUM_init
#  define RTE_RUNNABLE_RCtApWUM_task10msA RCtApWUM_task10msA
#  define RTE_RUNNABLE_RCtApWUM_task10msB RCtApWUM_task10msB
#  define RTE_RUNNABLE_RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received
# endif

FUNC(void, CtApWUM_CODE) PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApWUM_CODE) RCtApWUM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApWUM_CODE) RCtApWUM_task10msA(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApWUM_CODE) RCtApWUM_task10msB(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApWUM_CODE) RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApWUM_STOP_SEC_CODE
# include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_ComM_UserRequest_E_MODE_LIMITATION (2U)

#  define RTE_E_ComM_UserRequest_E_NOT_OK (1U)

#  define RTE_E_EcuM_StateRequest_E_NOT_OK (1U)

#  define RTE_E_NvMService_AC3_SRBS_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPWUM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
