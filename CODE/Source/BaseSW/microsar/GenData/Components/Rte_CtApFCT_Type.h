/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApFCT_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApFCT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPFCT_TYPE_H
# define RTE_CTAPFCT_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_Invalid_RCD
#   define Cx0_Invalid_RCD (0U)
#  endif

#  ifndef Cx1_No_main_wake_up_request
#   define Cx1_No_main_wake_up_request (1U)
#  endif

#  ifndef Cx2_Main_wake_up_request
#   define Cx2_Main_wake_up_request (2U)
#  endif

#  ifndef Cx3_Not_valid
#   define Cx3_Not_valid (3U)
#  endif

#  ifndef Cx0_No_fault
#   define Cx0_No_fault (0U)
#  endif

#  ifndef Cx1_Warning_limited_performance
#   define Cx1_Warning_limited_performance (1U)
#  endif

#  ifndef Cx2_Short_time_fault_Need_reset
#   define Cx2_Short_time_fault_Need_reset (2U)
#  endif

#  ifndef Cx3_Permernant_fault_need_replaced
#   define Cx3_Permernant_fault_need_replaced (3U)
#  endif

#  ifndef APP_STATE_0
#   define APP_STATE_0 (0U)
#  endif

#  ifndef APP_STATE_1
#   define APP_STATE_1 (1U)
#  endif

#  ifndef APP_STATE_2
#   define APP_STATE_2 (2U)
#  endif

#  ifndef APP_STATE_3
#   define APP_STATE_3 (3U)
#  endif

#  ifndef APP_STATE_4
#   define APP_STATE_4 (4U)
#  endif

#  ifndef APP_STATE_5
#   define APP_STATE_5 (5U)
#  endif

#  ifndef BAT_VALID_RANGE
#   define BAT_VALID_RANGE (0U)
#  endif

#  ifndef BAT_OVERVOLTAGE
#   define BAT_OVERVOLTAGE (1U)
#  endif

#  ifndef BAT_UNDERVOLTAGE
#   define BAT_UNDERVOLTAGE (2U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPFCT_TYPE_H */
