/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtHwAbsAIM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtHwAbsAIM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTHWABSAIM_H
# define RTE_CTHWABSAIM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtHwAbsAIM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtAmbientTemperaturePhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue;
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtElockFeedbackCurrent, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent;
extern VAR(IdtElockFeedbackLock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock;
extern VAR(IdtElockFeedbackUnlock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock;
extern VAR(IdtExtOpPlugLockRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw;
extern VAR(IdtHWEditionDetected, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected;
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue;
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue;
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw;
extern VAR(IdtPlugLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue;
extern VAR(IdtProximityDetectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue;
extern VAR(IdtTempSyncRectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue (65U)
#  define Rte_InitValue_PpBatteryVolt_DeBatteryVolt (255U)
#  define Rte_InitValue_PpElockFeedbackCurrent_DeElockFeedbackCurrent (0U)
#  define Rte_InitValue_PpElockFeedbackLock_DeElockFeedbackLock (0U)
#  define Rte_InitValue_PpElockFeedbackUnlock_DeElockFeedbackUnlock (0U)
#  define Rte_InitValue_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw (255U)
#  define Rte_InitValue_PpHWEditionDetected_DeHWEditionDetected (0U)
#  define Rte_InitValue_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue (0U)
#  define Rte_InitValue_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue (0U)
#  define Rte_InitValue_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue (0U)
#  define Rte_InitValue_PpMsrTempRaw_DeMsrTempAC1Raw (0U)
#  define Rte_InitValue_PpMsrTempRaw_DeMsrTempAC2Raw (0U)
#  define Rte_InitValue_PpMsrTempRaw_DeMsrTempAC3Raw (0U)
#  define Rte_InitValue_PpMsrTempRaw_DeMsrTempACNRaw (0U)
#  define Rte_InitValue_PpMsrTempRaw_DeMsrTempDC1Raw (0U)
#  define Rte_InitValue_PpMsrTempRaw_DeMsrTempDC2Raw (0U)
#  define Rte_InitValue_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue (0U)
#  define Rte_InitValue_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue (65535U)
#  define Rte_InitValue_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue (65U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue Rte_Write_CtHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(data) (Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpBatteryVolt_DeBatteryVolt Rte_Write_CtHwAbsAIM_PpBatteryVolt_DeBatteryVolt
#  define Rte_Write_CtHwAbsAIM_PpBatteryVolt_DeBatteryVolt(data) (Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent Rte_Write_CtHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent
#  define Rte_Write_CtHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent(data) (Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock Rte_Write_CtHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock
#  define Rte_Write_CtHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock(data) (Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock Rte_Write_CtHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock
#  define Rte_Write_CtHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock(data) (Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw Rte_Write_CtHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw
#  define Rte_Write_CtHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(data) (Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpHWEditionDetected_DeHWEditionDetected Rte_Write_CtHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected
#  define Rte_Write_CtHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected(data) (Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue Rte_Write_CtHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(data) (Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue Rte_Write_CtHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(data) (Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue Rte_Write_CtHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(data) (Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw
#  define Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw(data) (Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw
#  define Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw(data) (Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw
#  define Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw(data) (Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw
#  define Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw(data) (Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw
#  define Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw(data) (Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw
#  define Rte_Write_CtHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw(data) (Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue Rte_Write_CtHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(data) (Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue Rte_Write_CtHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data) (Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue Rte_Write_CtHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue
#  define Rte_Write_CtHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(data) (Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_ReadBlock(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(arg1) (NvM_ReadBlock((NvM_BlockIdType)47, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)47, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalVoltageExternalADCReferenceWave1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsAIM.CalVoltageExternalADCReferenceWave1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalVoltageExternalADCReferenceWave2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsAIM.CalVoltageExternalADCReferenceWave2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceADCReference() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsAIM.CalDebounceADCReference) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue() (&(Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue() ((P2CONST(IdtArrayInitAIMVoltRef, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtArrayInitAIMVoltRef, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock() (&((*RtePim_AIMVoltRefNvBlockNeed_MirrorBlock())[0]))
#  else
#   define Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock() RtePim_AIMVoltRefNvBlockNeed_MirrorBlock()
#  endif
#  define RtePim_AIMVoltRefNvBlockNeed_MirrorBlock() \
  (&Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtHwAbsAIM_START_SEC_CODE
# include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtHwAbsAIM_init RCtHwAbsAIM_init
#  define RTE_RUNNABLE_RCtHwAbsAIM_task100ms RCtHwAbsAIM_task100ms
#  define RTE_RUNNABLE_RCtHwAbsAIM_task10ms RCtHwAbsAIM_task10ms
# endif

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_task100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtHwAbsAIM_STOP_SEC_CODE
# include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTHWABSAIM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
