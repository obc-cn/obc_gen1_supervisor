/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLSD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApLSD>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLSD_H
# define RTE_CTAPLSD_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApLSD_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions;
extern VAR(OBC_ElockState, RTE_VAR_INIT) Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState;
extern VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor;
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState;
extern VAR(IdtExtOpPlugLockRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpELockSensorFaults_DeELockSensorFaultSCG (FALSE)
#  define Rte_InitValue_PpELockSensorFaults_DeELockSensorFaultSCP (FALSE)
#  define Rte_InitValue_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions (FALSE)
#  define Rte_InitValue_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState (0U)
#  define Rte_InitValue_PpOutputELockSensor_DeOutputELockSensor (2U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApLSD_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApLSD_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApLSD_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApLSD_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState Rte_Read_CtApLSD_PpBatteryVoltageState_DeBatteryVoltageState
#  define Rte_Read_CtApLSD_PpBatteryVoltageState_DeBatteryVoltageState(data) (*(data) = Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode Rte_Read_CtApLSD_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode
#  define Rte_Read_CtApLSD_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(data) (*(data) = Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw Rte_Read_CtApLSD_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw
#  define Rte_Read_CtApLSD_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(data) (*(data) = Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG Rte_Write_CtApLSD_PpELockSensorFaults_DeELockSensorFaultSCG
#  define Rte_Write_CtApLSD_PpELockSensorFaults_DeELockSensorFaultSCG(data) (Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP Rte_Write_CtApLSD_PpELockSensorFaults_DeELockSensorFaultSCP
#  define Rte_Write_CtApLSD_PpELockSensorFaults_DeELockSensorFaultSCP(data) (Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions Rte_Write_CtApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions
#  define Rte_Write_CtApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(data) (Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState Rte_Write_CtApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState
#  define Rte_Write_CtApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(data) (Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputELockSensor_DeOutputELockSensor Rte_Write_CtApLSD_PpOutputELockSensor_DeOutputELockSensor
#  define Rte_Write_CtApLSD_PpOutputELockSensor_DeOutputELockSensor(data) (Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalELockDebounce() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockDebounce) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalELockSensorDriveHigh() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockSensorDriveHigh) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalELockSensorDriveLow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockSensorDriveLow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalELockSensorLockHigh() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockSensorLockHigh) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalELockSensorLockLow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockSensorLockLow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalELockSensorUnlockHigh() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockSensorUnlockHigh) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalELockSensorUnlockLow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalELockSensorUnlockLow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockSensorSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalElockSensorSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockSensorSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalElockSensorSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockSensorSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalElockSensorSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockSensorSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalElockSensorSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockSensorThresholdSCG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalElockSensorThresholdSCG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockSensorThresholdSCP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalElockSensorThresholdSCP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowPlugLock() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLSD.CalCtrlFlowPlugLock) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockCounterSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockPrefaultSCP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockCounterSCG() \
  (&Rte_CpApLSD_PimElockCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockCounterSCP() \
  (&Rte_CpApLSD_PimElockCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockPrefaultSCG() \
  (&Rte_CpApLSD_PimElockPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockPrefaultSCP() \
  (&Rte_CpApLSD_PimElockPrefaultSCP)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApLSD_START_SEC_CODE
# include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApLSD_init RCtApLSD_init
#  define RTE_RUNNABLE_RCtApLSD_task10ms RCtApLSD_task10ms
# endif

FUNC(void, CtApLSD_CODE) RCtApLSD_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLSD_CODE) RCtApLSD_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApLSD_STOP_SEC_CODE
# include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLSD_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
