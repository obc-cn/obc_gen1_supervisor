/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApPCOM_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApPCOM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPPCOM_TYPE_H
# define RTE_CTAPPCOM_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_invalid_value
#   define Cx0_invalid_value (0U)
#  endif

#  ifndef Cx1_CC2_invalid_not_connected
#   define Cx1_CC2_invalid_not_connected (1U)
#  endif

#  ifndef Cx2_CC2_valid_full_connected
#   define Cx2_CC2_valid_full_connected (2U)
#  endif

#  ifndef Cx3_CC2_invalid_half_connected
#   define Cx3_CC2_invalid_half_connected (3U)
#  endif

#  ifndef Cx0_not_in_charging
#   define Cx0_not_in_charging (0U)
#  endif

#  ifndef Cx1_charging
#   define Cx1_charging (1U)
#  endif

#  ifndef Cx2_charging_fault
#   define Cx2_charging_fault (2U)
#  endif

#  ifndef Cx3_charging_finished
#   define Cx3_charging_finished (3U)
#  endif

#  ifndef Cx0_contactors_opened
#   define Cx0_contactors_opened (0U)
#  endif

#  ifndef Cx1_precharge
#   define Cx1_precharge (1U)
#  endif

#  ifndef Cx2_contactors_closed
#   define Cx2_contactors_closed (2U)
#  endif

#  ifndef Cx3_Invalid
#   define Cx3_Invalid (3U)
#  endif

#  ifndef Cx0_ON
#   define Cx0_ON (0U)
#  endif

#  ifndef Cx1_OFF_not_ended
#   define Cx1_OFF_not_ended (1U)
#  endif

#  ifndef Cx2_OFF_ended
#   define Cx2_OFF_ended (2U)
#  endif

#  ifndef Cx3_Unavailable
#   define Cx3_Unavailable (3U)
#  endif

#  ifndef Cx0_STANDBY
#   define Cx0_STANDBY (0U)
#  endif

#  ifndef Cx1_DIFFEREE
#   define Cx1_DIFFEREE (1U)
#  endif

#  ifndef Cx2_IMMEDIATE
#   define Cx2_IMMEDIATE (2U)
#  endif

#  ifndef Cx0_No_Locked
#   define Cx0_No_Locked (0U)
#  endif

#  ifndef Cx1_Locked
#   define Cx1_Locked (1U)
#  endif

#  ifndef Cx2_Overlocked
#   define Cx2_Overlocked (2U)
#  endif

#  ifndef Cx3_Reserved
#   define Cx3_Reserved (3U)
#  endif

#  ifndef Cx0_Invalid_RCD
#   define Cx0_Invalid_RCD (0U)
#  endif

#  ifndef Cx1_No_main_wake_up_request
#   define Cx1_No_main_wake_up_request (1U)
#  endif

#  ifndef Cx2_Main_wake_up_request
#   define Cx2_Main_wake_up_request (2U)
#  endif

#  ifndef Cx3_Not_valid
#   define Cx3_Not_valid (3U)
#  endif

#  ifndef Cx00_Stop_mode
#   define Cx00_Stop_mode (0U)
#  endif

#  ifndef Cx01_Failure_mode_1
#   define Cx01_Failure_mode_1 (1U)
#  endif

#  ifndef Cx02_Failure_mode_2
#   define Cx02_Failure_mode_2 (2U)
#  endif

#  ifndef Cx03_Failure_mode_3
#   define Cx03_Failure_mode_3 (3U)
#  endif

#  ifndef Cx04_Failure_mode_4
#   define Cx04_Failure_mode_4 (4U)
#  endif

#  ifndef Cx05_Failure_mode_5
#   define Cx05_Failure_mode_5 (5U)
#  endif

#  ifndef Cx06_Failure_mode_6
#   define Cx06_Failure_mode_6 (6U)
#  endif

#  ifndef Cx07_Failure_mode_7
#   define Cx07_Failure_mode_7 (7U)
#  endif

#  ifndef Cx08_AC_mode
#   define Cx08_AC_mode (8U)
#  endif

#  ifndef Cx09_HP_mode
#   define Cx09_HP_mode (9U)
#  endif

#  ifndef Cx0A_Deshum_total_heating_mode
#   define Cx0A_Deshum_total_heating_mode (10U)
#  endif

#  ifndef Cx0B_Deshum_simple_heating_mode
#   define Cx0B_Deshum_simple_heating_mode (11U)
#  endif

#  ifndef Cx0C_De_icing_mode_
#   define Cx0C_De_icing_mode_ (12U)
#  endif

#  ifndef Cx0D_transition_mode
#   define Cx0D_transition_mode (13U)
#  endif

#  ifndef Cx0E_Failure_mode
#   define Cx0E_Failure_mode (14U)
#  endif

#  ifndef Cx0F_Chiller_alone_mode
#   define Cx0F_Chiller_alone_mode (15U)
#  endif

#  ifndef Cx10_AC_chiffler_mode
#   define Cx10_AC_chiffler_mode (16U)
#  endif

#  ifndef Cx11_Deshum_Chiller_mode
#   define Cx11_Deshum_Chiller_mode (17U)
#  endif

#  ifndef Cx12_Reserved
#   define Cx12_Reserved (18U)
#  endif

#  ifndef Cx13_Water_Heather_mode
#   define Cx13_Water_Heather_mode (19U)
#  endif

#  ifndef Cx14_Reserved
#   define Cx14_Reserved (20U)
#  endif

#  ifndef Cx15_Reserved
#   define Cx15_Reserved (21U)
#  endif

#  ifndef Cx16_Reserved
#   define Cx16_Reserved (22U)
#  endif

#  ifndef Cx17_Reserved
#   define Cx17_Reserved (23U)
#  endif

#  ifndef Cx18_Reserved
#   define Cx18_Reserved (24U)
#  endif

#  ifndef Cx19_Reserved
#   define Cx19_Reserved (25U)
#  endif

#  ifndef Cx1A_Reserved
#   define Cx1A_Reserved (26U)
#  endif

#  ifndef Cx1B_Reserved
#   define Cx1B_Reserved (27U)
#  endif

#  ifndef Cx1C_Reserved
#   define Cx1C_Reserved (28U)
#  endif

#  ifndef Cx1D_Reserved
#   define Cx1D_Reserved (29U)
#  endif

#  ifndef Cx1E_Reserved
#   define Cx1E_Reserved (30U)
#  endif

#  ifndef Cx1F_Reserved
#   define Cx1F_Reserved (31U)
#  endif

#  ifndef COMM_NO_COMMUNICATION
#   define COMM_NO_COMMUNICATION (0U)
#  endif

#  ifndef COMM_SILENT_COMMUNICATION
#   define COMM_SILENT_COMMUNICATION (1U)
#  endif

#  ifndef COMM_FULL_COMMUNICATION
#   define COMM_FULL_COMMUNICATION (2U)
#  endif

#  ifndef Cx0_not_in_active_discharge
#   define Cx0_not_in_active_discharge (0U)
#  endif

#  ifndef Cx1_active_discharging
#   define Cx1_active_discharging (1U)
#  endif

#  ifndef Cx2_discharge_completed
#   define Cx2_discharge_completed (2U)
#  endif

#  ifndef Cx3_discharge_failure
#   define Cx3_discharge_failure (3U)
#  endif

#  ifndef DCDC_FAULT_NO_FAULT
#   define DCDC_FAULT_NO_FAULT (0U)
#  endif

#  ifndef DCDC_FAULT_LEVEL_1
#   define DCDC_FAULT_LEVEL_1 (64U)
#  endif

#  ifndef DCDC_FAULT_LEVEL_2
#   define DCDC_FAULT_LEVEL_2 (128U)
#  endif

#  ifndef DCDC_FAULT_LEVEL_3
#   define DCDC_FAULT_LEVEL_3 (192U)
#  endif

#  ifndef Cx0_No_fault
#   define Cx0_No_fault (0U)
#  endif

#  ifndef Cx1_Warning_limited_performance
#   define Cx1_Warning_limited_performance (1U)
#  endif

#  ifndef Cx2_Short_time_fault_Need_reset
#   define Cx2_Short_time_fault_Need_reset (2U)
#  endif

#  ifndef Cx3_Permernant_fault_need_replaced
#   define Cx3_Permernant_fault_need_replaced (3U)
#  endif

#  ifndef Cx0_Request_to_Open_the_Contactors
#   define Cx0_Request_to_Open_the_Contactors (0U)
#  endif

#  ifndef Cx1_Request_to_Close_the_Contactors
#   define Cx1_Request_to_Close_the_Contactors (1U)
#  endif

#  ifndef Cx2_No_request
#   define Cx2_No_request (2U)
#  endif

#  ifndef Cx0_off_mode
#   define Cx0_off_mode (0U)
#  endif

#  ifndef Cx1_Init_mode
#   define Cx1_Init_mode (1U)
#  endif

#  ifndef Cx2_standby_mode
#   define Cx2_standby_mode (2U)
#  endif

#  ifndef Cx3_conversion_working_
#   define Cx3_conversion_working_ (3U)
#  endif

#  ifndef Cx4_error_mode
#   define Cx4_error_mode (4U)
#  endif

#  ifndef Cx5_degradation_mode
#   define Cx5_degradation_mode (5U)
#  endif

#  ifndef Cx6_reserved
#   define Cx6_reserved (6U)
#  endif

#  ifndef Cx7_invalid
#   define Cx7_invalid (7U)
#  endif

#  ifndef Cx0_DCHV_STATUS_STANDBY
#   define Cx0_DCHV_STATUS_STANDBY (0U)
#  endif

#  ifndef Cx1_DCHV_STATUS_RUN
#   define Cx1_DCHV_STATUS_RUN (1U)
#  endif

#  ifndef Cx2_DCHV_STATUS_ERROR
#   define Cx2_DCHV_STATUS_ERROR (2U)
#  endif

#  ifndef Cx3_DCHV_STATUS_ALARM
#   define Cx3_DCHV_STATUS_ALARM (3U)
#  endif

#  ifndef Cx4_DCHV_STATUS_SAFE
#   define Cx4_DCHV_STATUS_SAFE (4U)
#  endif

#  ifndef Cx1_RUN
#   define Cx1_RUN (1U)
#  endif

#  ifndef Cx2_ERROR
#   define Cx2_ERROR (2U)
#  endif

#  ifndef Cx3_ALARM
#   define Cx3_ALARM (3U)
#  endif

#  ifndef Cx4_SAFE
#   define Cx4_SAFE (4U)
#  endif

#  ifndef Cx5_DERATED
#   define Cx5_DERATED (5U)
#  endif

#  ifndef Cx6_SHUTDOWN
#   define Cx6_SHUTDOWN (6U)
#  endif

#  ifndef DCM_E_POSITIVERESPONSE
#   define DCM_E_POSITIVERESPONSE (0U)
#  endif

#  ifndef DCM_E_GENERALREJECT
#   define DCM_E_GENERALREJECT (16U)
#  endif

#  ifndef DCM_E_SERVICENOTSUPPORTED
#   define DCM_E_SERVICENOTSUPPORTED (17U)
#  endif

#  ifndef DCM_E_SUBFUNCTIONNOTSUPPORTED
#   define DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
#  endif

#  ifndef DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT
#   define DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
#  endif

#  ifndef DCM_E_RESPONSETOOLONG
#   define DCM_E_RESPONSETOOLONG (20U)
#  endif

#  ifndef DCM_E_BUSYREPEATREQUEST
#   define DCM_E_BUSYREPEATREQUEST (33U)
#  endif

#  ifndef DCM_E_CONDITIONSNOTCORRECT
#   define DCM_E_CONDITIONSNOTCORRECT (34U)
#  endif

#  ifndef DCM_E_REQUESTSEQUENCEERROR
#   define DCM_E_REQUESTSEQUENCEERROR (36U)
#  endif

#  ifndef DCM_E_NORESPONSEFROMSUBNETCOMPONENT
#   define DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
#  endif

#  ifndef DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION
#   define DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
#  endif

#  ifndef DCM_E_REQUESTOUTOFRANGE
#   define DCM_E_REQUESTOUTOFRANGE (49U)
#  endif

#  ifndef DCM_E_SECURITYACCESSDENIED
#   define DCM_E_SECURITYACCESSDENIED (51U)
#  endif

#  ifndef DCM_E_INVALIDKEY
#   define DCM_E_INVALIDKEY (53U)
#  endif

#  ifndef DCM_E_EXCEEDNUMBEROFATTEMPTS
#   define DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
#  endif

#  ifndef DCM_E_REQUIREDTIMEDELAYNOTEXPIRED
#   define DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
#  endif

#  ifndef DCM_E_UPLOADDOWNLOADNOTACCEPTED
#   define DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
#  endif

#  ifndef DCM_E_TRANSFERDATASUSPENDED
#   define DCM_E_TRANSFERDATASUSPENDED (113U)
#  endif

#  ifndef DCM_E_GENERALPROGRAMMINGFAILURE
#   define DCM_E_GENERALPROGRAMMINGFAILURE (114U)
#  endif

#  ifndef DCM_E_WRONGBLOCKSEQUENCECOUNTER
#   define DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
#  endif

#  ifndef DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING
#   define DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
#  endif

#  ifndef DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION
#   define DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
#  endif

#  ifndef DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION
#   define DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
#  endif

#  ifndef DCM_E_RPMTOOHIGH
#   define DCM_E_RPMTOOHIGH (129U)
#  endif

#  ifndef DCM_E_RPMTOOLOW
#   define DCM_E_RPMTOOLOW (130U)
#  endif

#  ifndef DCM_E_ENGINEISRUNNING
#   define DCM_E_ENGINEISRUNNING (131U)
#  endif

#  ifndef DCM_E_ENGINEISNOTRUNNING
#   define DCM_E_ENGINEISNOTRUNNING (132U)
#  endif

#  ifndef DCM_E_ENGINERUNTIMETOOLOW
#   define DCM_E_ENGINERUNTIMETOOLOW (133U)
#  endif

#  ifndef DCM_E_TEMPERATURETOOHIGH
#   define DCM_E_TEMPERATURETOOHIGH (134U)
#  endif

#  ifndef DCM_E_TEMPERATURETOOLOW
#   define DCM_E_TEMPERATURETOOLOW (135U)
#  endif

#  ifndef DCM_E_VEHICLESPEEDTOOHIGH
#   define DCM_E_VEHICLESPEEDTOOHIGH (136U)
#  endif

#  ifndef DCM_E_VEHICLESPEEDTOOLOW
#   define DCM_E_VEHICLESPEEDTOOLOW (137U)
#  endif

#  ifndef DCM_E_THROTTLE_PEDALTOOHIGH
#   define DCM_E_THROTTLE_PEDALTOOHIGH (138U)
#  endif

#  ifndef DCM_E_THROTTLE_PEDALTOOLOW
#   define DCM_E_THROTTLE_PEDALTOOLOW (139U)
#  endif

#  ifndef DCM_E_TRANSMISSIONRANGENOTINNEUTRAL
#   define DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
#  endif

#  ifndef DCM_E_TRANSMISSIONRANGENOTINGEAR
#   define DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
#  endif

#  ifndef DCM_E_BRAKESWITCH_NOTCLOSED
#   define DCM_E_BRAKESWITCH_NOTCLOSED (143U)
#  endif

#  ifndef DCM_E_SHIFTERLEVERNOTINPARK
#   define DCM_E_SHIFTERLEVERNOTINPARK (144U)
#  endif

#  ifndef DCM_E_TORQUECONVERTERCLUTCHLOCKED
#   define DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
#  endif

#  ifndef DCM_E_VOLTAGETOOHIGH
#   define DCM_E_VOLTAGETOOHIGH (146U)
#  endif

#  ifndef DCM_E_VOLTAGETOOLOW
#   define DCM_E_VOLTAGETOOLOW (147U)
#  endif

#  ifndef DCM_E_VMSCNC_0
#   define DCM_E_VMSCNC_0 (240U)
#  endif

#  ifndef DCM_E_VMSCNC_1
#   define DCM_E_VMSCNC_1 (241U)
#  endif

#  ifndef DCM_E_VMSCNC_2
#   define DCM_E_VMSCNC_2 (242U)
#  endif

#  ifndef DCM_E_VMSCNC_3
#   define DCM_E_VMSCNC_3 (243U)
#  endif

#  ifndef DCM_E_VMSCNC_4
#   define DCM_E_VMSCNC_4 (244U)
#  endif

#  ifndef DCM_E_VMSCNC_5
#   define DCM_E_VMSCNC_5 (245U)
#  endif

#  ifndef DCM_E_VMSCNC_6
#   define DCM_E_VMSCNC_6 (246U)
#  endif

#  ifndef DCM_E_VMSCNC_7
#   define DCM_E_VMSCNC_7 (247U)
#  endif

#  ifndef DCM_E_VMSCNC_8
#   define DCM_E_VMSCNC_8 (248U)
#  endif

#  ifndef DCM_E_VMSCNC_9
#   define DCM_E_VMSCNC_9 (249U)
#  endif

#  ifndef DCM_E_VMSCNC_A
#   define DCM_E_VMSCNC_A (250U)
#  endif

#  ifndef DCM_E_VMSCNC_B
#   define DCM_E_VMSCNC_B (251U)
#  endif

#  ifndef DCM_E_VMSCNC_C
#   define DCM_E_VMSCNC_C (252U)
#  endif

#  ifndef DCM_E_VMSCNC_D
#   define DCM_E_VMSCNC_D (253U)
#  endif

#  ifndef DCM_E_VMSCNC_E
#   define DCM_E_VMSCNC_E (254U)
#  endif

#  ifndef DCM_INITIAL
#   define DCM_INITIAL (0U)
#  endif

#  ifndef DCM_PENDING
#   define DCM_PENDING (1U)
#  endif

#  ifndef DCM_CANCEL
#   define DCM_CANCEL (2U)
#  endif

#  ifndef DCM_FORCE_RCRRP_OK
#   define DCM_FORCE_RCRRP_OK (3U)
#  endif

#  ifndef DCM_FORCE_RCRRP_NOT_OK
#   define DCM_FORCE_RCRRP_NOT_OK (64U)
#  endif

#  ifndef DEM_UDS_STATUS_TF
#   define DEM_UDS_STATUS_TF (1U)
#  endif

#  ifndef DEM_UDS_STATUS_TF_BflMask
#   define DEM_UDS_STATUS_TF_BflMask 1U
#  endif

#  ifndef DEM_UDS_STATUS_TF_BflPn
#   define DEM_UDS_STATUS_TF_BflPn 0U
#  endif

#  ifndef DEM_UDS_STATUS_TF_BflLn
#   define DEM_UDS_STATUS_TF_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_TFTOC
#   define DEM_UDS_STATUS_TFTOC (2U)
#  endif

#  ifndef DEM_UDS_STATUS_TFTOC_BflMask
#   define DEM_UDS_STATUS_TFTOC_BflMask 2U
#  endif

#  ifndef DEM_UDS_STATUS_TFTOC_BflPn
#   define DEM_UDS_STATUS_TFTOC_BflPn 1U
#  endif

#  ifndef DEM_UDS_STATUS_TFTOC_BflLn
#   define DEM_UDS_STATUS_TFTOC_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_PDTC
#   define DEM_UDS_STATUS_PDTC (4U)
#  endif

#  ifndef DEM_UDS_STATUS_PDTC_BflMask
#   define DEM_UDS_STATUS_PDTC_BflMask 4U
#  endif

#  ifndef DEM_UDS_STATUS_PDTC_BflPn
#   define DEM_UDS_STATUS_PDTC_BflPn 2U
#  endif

#  ifndef DEM_UDS_STATUS_PDTC_BflLn
#   define DEM_UDS_STATUS_PDTC_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_CDTC
#   define DEM_UDS_STATUS_CDTC (8U)
#  endif

#  ifndef DEM_UDS_STATUS_CDTC_BflMask
#   define DEM_UDS_STATUS_CDTC_BflMask 8U
#  endif

#  ifndef DEM_UDS_STATUS_CDTC_BflPn
#   define DEM_UDS_STATUS_CDTC_BflPn 3U
#  endif

#  ifndef DEM_UDS_STATUS_CDTC_BflLn
#   define DEM_UDS_STATUS_CDTC_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_TNCSLC
#   define DEM_UDS_STATUS_TNCSLC (16U)
#  endif

#  ifndef DEM_UDS_STATUS_TNCSLC_BflMask
#   define DEM_UDS_STATUS_TNCSLC_BflMask 16U
#  endif

#  ifndef DEM_UDS_STATUS_TNCSLC_BflPn
#   define DEM_UDS_STATUS_TNCSLC_BflPn 4U
#  endif

#  ifndef DEM_UDS_STATUS_TNCSLC_BflLn
#   define DEM_UDS_STATUS_TNCSLC_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_TFSLC
#   define DEM_UDS_STATUS_TFSLC (32U)
#  endif

#  ifndef DEM_UDS_STATUS_TFSLC_BflMask
#   define DEM_UDS_STATUS_TFSLC_BflMask 32U
#  endif

#  ifndef DEM_UDS_STATUS_TFSLC_BflPn
#   define DEM_UDS_STATUS_TFSLC_BflPn 5U
#  endif

#  ifndef DEM_UDS_STATUS_TFSLC_BflLn
#   define DEM_UDS_STATUS_TFSLC_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_TNCTOC
#   define DEM_UDS_STATUS_TNCTOC (64U)
#  endif

#  ifndef DEM_UDS_STATUS_TNCTOC_BflMask
#   define DEM_UDS_STATUS_TNCTOC_BflMask 64U
#  endif

#  ifndef DEM_UDS_STATUS_TNCTOC_BflPn
#   define DEM_UDS_STATUS_TNCTOC_BflPn 6U
#  endif

#  ifndef DEM_UDS_STATUS_TNCTOC_BflLn
#   define DEM_UDS_STATUS_TNCTOC_BflLn 1U
#  endif

#  ifndef DEM_UDS_STATUS_WIR
#   define DEM_UDS_STATUS_WIR (128U)
#  endif

#  ifndef DEM_UDS_STATUS_WIR_BflMask
#   define DEM_UDS_STATUS_WIR_BflMask 128U
#  endif

#  ifndef DEM_UDS_STATUS_WIR_BflPn
#   define DEM_UDS_STATUS_WIR_BflPn 7U
#  endif

#  ifndef DEM_UDS_STATUS_WIR_BflLn
#   define DEM_UDS_STATUS_WIR_BflLn 1U
#  endif

#  ifndef BAT_VALID_RANGE
#   define BAT_VALID_RANGE (0U)
#  endif

#  ifndef BAT_OVERVOLTAGE
#   define BAT_OVERVOLTAGE (1U)
#  endif

#  ifndef BAT_UNDERVOLTAGE
#   define BAT_UNDERVOLTAGE (2U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

#  ifndef Cx0_No_AC_10V_
#   define Cx0_No_AC_10V_ (0U)
#  endif

#  ifndef Cx1_110V_85_132V_
#   define Cx1_110V_85_132V_ (1U)
#  endif

#  ifndef Cx2_Invalid
#   define Cx2_Invalid (2U)
#  endif

#  ifndef Cx3_220V_170_265V_
#   define Cx3_220V_170_265V_ (3U)
#  endif

#  ifndef Cx4_Reserved
#   define Cx4_Reserved (4U)
#  endif

#  ifndef Cx5_Reserved
#   define Cx5_Reserved (5U)
#  endif

#  ifndef Cx6_Reserved
#   define Cx6_Reserved (6U)
#  endif

#  ifndef Cx7_Reserved
#   define Cx7_Reserved (7U)
#  endif

#  ifndef Cx0_Invalid_value
#   define Cx0_Invalid_value (0U)
#  endif

#  ifndef Cx1_CP_invalid_not_connected
#   define Cx1_CP_invalid_not_connected (1U)
#  endif

#  ifndef Cx2_CP_valid_full_connected
#   define Cx2_CP_valid_full_connected (2U)
#  endif

#  ifndef Cx3_CP_invalid_half_connected
#   define Cx3_CP_invalid_half_connected (3U)
#  endif

#  ifndef Cx1_not_connected
#   define Cx1_not_connected (1U)
#  endif

#  ifndef Cx2_full_connected
#   define Cx2_full_connected (2U)
#  endif

#  ifndef Cx3_CC_is_half_connected
#   define Cx3_CC_is_half_connected (3U)
#  endif

#  ifndef Cx0_no_charging
#   define Cx0_no_charging (0U)
#  endif

#  ifndef Cx1_slow_charging
#   define Cx1_slow_charging (1U)
#  endif

#  ifndef Cx2_China_fast_charging
#   define Cx2_China_fast_charging (2U)
#  endif

#  ifndef Cx3_Euro_fast_charging
#   define Cx3_Euro_fast_charging (3U)
#  endif

#  ifndef Cx0_Elock_unlocked
#   define Cx0_Elock_unlocked (0U)
#  endif

#  ifndef Cx1_Elock_locked
#   define Cx1_Elock_locked (1U)
#  endif

#  ifndef Cx2_E_lock_lock_fail
#   define Cx2_E_lock_lock_fail (2U)
#  endif

#  ifndef Cx3_E_Lock_unlock_fail
#   define Cx3_E_Lock_unlock_fail (3U)
#  endif

#  ifndef OBC_FAULT_NO_FAULT
#   define OBC_FAULT_NO_FAULT (0U)
#  endif

#  ifndef OBC_FAULT_LEVEL_1
#   define OBC_FAULT_LEVEL_1 (64U)
#  endif

#  ifndef OBC_FAULT_LEVEL_2
#   define OBC_FAULT_LEVEL_2 (128U)
#  endif

#  ifndef OBC_FAULT_LEVEL_3
#   define OBC_FAULT_LEVEL_3 (192U)
#  endif

#  ifndef Cx0_No_220V_AC
#   define Cx0_No_220V_AC (0U)
#  endif

#  ifndef Cx1_220V_AC_connected
#   define Cx1_220V_AC_connected (1U)
#  endif

#  ifndef Cx2_220V_AC_disconnected
#   define Cx2_220V_AC_disconnected (2U)
#  endif

#  ifndef Cx3_Invalid_value
#   define Cx3_Invalid_value (3U)
#  endif

#  ifndef Cx0_Disconnected
#   define Cx0_Disconnected (0U)
#  endif

#  ifndef Cx1_In_progress
#   define Cx1_In_progress (1U)
#  endif

#  ifndef Cx2_Failure
#   define Cx2_Failure (2U)
#  endif

#  ifndef Cx3_Stopped
#   define Cx3_Stopped (3U)
#  endif

#  ifndef Cx4_Finished
#   define Cx4_Finished (4U)
#  endif

#  ifndef Cx0_PFC_STATE_STANDBY
#   define Cx0_PFC_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_PFC_STATE_TRI
#   define Cx1_PFC_STATE_TRI (1U)
#  endif

#  ifndef Cx2_PFC_STATE_MONO
#   define Cx2_PFC_STATE_MONO (2U)
#  endif

#  ifndef Cx3_PFC_STATE_RESET_ERROR
#   define Cx3_PFC_STATE_RESET_ERROR (3U)
#  endif

#  ifndef Cx4_PFC_STATE_DISCHARGE
#   define Cx4_PFC_STATE_DISCHARGE (4U)
#  endif

#  ifndef Cx5_PFC_STATE_SHUTDOWN
#   define Cx5_PFC_STATE_SHUTDOWN (5U)
#  endif

#  ifndef Cx6_PFC_STATE_START_TRI
#   define Cx6_PFC_STATE_START_TRI (6U)
#  endif

#  ifndef Cx7_PFC_STATE_START_MONO
#   define Cx7_PFC_STATE_START_MONO (7U)
#  endif

#  ifndef Cx8_PFC_STATE_ERROR
#   define Cx8_PFC_STATE_ERROR (8U)
#  endif

#  ifndef Cx9_PFC_STATE_DIRECTPWM
#   define Cx9_PFC_STATE_DIRECTPWM (9U)
#  endif

#  ifndef Cx0_Partial_wakeup
#   define Cx0_Partial_wakeup (0U)
#  endif

#  ifndef Cx1_Internal_wakeup
#   define Cx1_Internal_wakeup (1U)
#  endif

#  ifndef Cx2_Transitory
#   define Cx2_Transitory (2U)
#  endif

#  ifndef Cx3_Nominal_main_wakeup
#   define Cx3_Nominal_main_wakeup (3U)
#  endif

#  ifndef Cx4_Degraded_main_wakeup
#   define Cx4_Degraded_main_wakeup (4U)
#  endif

#  ifndef Cx5_Shutdown_preparation
#   define Cx5_Shutdown_preparation (5U)
#  endif

#  ifndef Cx0_DCLV_STATE_STANDBY
#   define Cx0_DCLV_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_DCLV_STATE_RUN
#   define Cx1_DCLV_STATE_RUN (1U)
#  endif

#  ifndef Cx2_DCLV_STATE_RESET_ERROR
#   define Cx2_DCLV_STATE_RESET_ERROR (2U)
#  endif

#  ifndef Cx3_DCLV_STATE_DISCHARGE
#   define Cx3_DCLV_STATE_DISCHARGE (3U)
#  endif

#  ifndef Cx4_DCLV_STATE_SHUTDOWN
#   define Cx4_DCLV_STATE_SHUTDOWN (4U)
#  endif

#  ifndef Cx0_DCHV_STATE_STANDBY
#   define Cx0_DCHV_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_DCHV_STATE_RUN
#   define Cx1_DCHV_STATE_RUN (1U)
#  endif

#  ifndef Cx2_DCHV_STATE_RESET_ERROR
#   define Cx2_DCHV_STATE_RESET_ERROR (2U)
#  endif

#  ifndef Cx3_DCHV_STATE_DISCHARGE
#   define Cx3_DCHV_STATE_DISCHARGE (3U)
#  endif

#  ifndef Cx4_DCHV_STATE_SHUTDOWN
#   define Cx4_DCHV_STATE_SHUTDOWN (4U)
#  endif

#  ifndef Cx0_APC_unchanged
#   define Cx0_APC_unchanged (0U)
#  endif

#  ifndef Cx1_APC_unchanged
#   define Cx1_APC_unchanged (1U)
#  endif

#  ifndef Cx2_APC_OFF
#   define Cx2_APC_OFF (2U)
#  endif

#  ifndef Cx3_APC_ON_
#   define Cx3_APC_ON_ (3U)
#  endif

#  ifndef Cx0_Not_used
#   define Cx0_Not_used (0U)
#  endif

#  ifndef Cx1_DCDC_Off
#   define Cx1_DCDC_Off (1U)
#  endif

#  ifndef Cx2_DCDC_On
#   define Cx2_DCDC_On (2U)
#  endif

#  ifndef Cx3_Unavailable_Value
#   define Cx3_Unavailable_Value (3U)
#  endif

#  ifndef Cx0_EPWT_invalid
#   define Cx0_EPWT_invalid (0U)
#  endif

#  ifndef Cx1_inactive
#   define Cx1_inactive (1U)
#  endif

#  ifndef Cx2_active_no_speed_
#   define Cx2_active_no_speed_ (2U)
#  endif

#  ifndef Cx3_EPWT_is_running_with_speed
#   define Cx3_EPWT_is_running_with_speed (3U)
#  endif

#  ifndef Cx0_PWT_inactive
#   define Cx0_PWT_inactive (0U)
#  endif

#  ifndef Cx1_reserved
#   define Cx1_reserved (1U)
#  endif

#  ifndef Cx2_reserved
#   define Cx2_reserved (2U)
#  endif

#  ifndef Cx3_reserved
#   define Cx3_reserved (3U)
#  endif

#  ifndef Cx4_PWT_activation
#   define Cx4_PWT_activation (4U)
#  endif

#  ifndef Cx5_reserved
#   define Cx5_reserved (5U)
#  endif

#  ifndef Cx7_reserved
#   define Cx7_reserved (7U)
#  endif

#  ifndef Cx8_active_PWT
#   define Cx8_active_PWT (8U)
#  endif

#  ifndef Cx9_reserved
#   define Cx9_reserved (9U)
#  endif

#  ifndef CxA_PWT_desactivation
#   define CxA_PWT_desactivation (10U)
#  endif

#  ifndef CxB_reserved
#   define CxB_reserved (11U)
#  endif

#  ifndef CxC_PWT_in_hold_after_desactivation
#   define CxC_PWT_in_hold_after_desactivation (12U)
#  endif

#  ifndef CxD_reserved
#   define CxD_reserved (13U)
#  endif

#  ifndef CxE_PWT_at_rest
#   define CxE_PWT_at_rest (14U)
#  endif

#  ifndef CxF_reserved
#   define CxF_reserved (15U)
#  endif

#  ifndef Cx0_Stop
#   define Cx0_Stop (0U)
#  endif

#  ifndef Cx1_Contact
#   define Cx1_Contact (1U)
#  endif

#  ifndef Cx2_Start
#   define Cx2_Start (2U)
#  endif

#  ifndef Cx0_TI_NOMINAL
#   define Cx0_TI_NOMINAL (0U)
#  endif

#  ifndef Cx1_TI_DEGRADE
#   define Cx1_TI_DEGRADE (1U)
#  endif

#  ifndef Cx2_TI_DISPO_DEM
#   define Cx2_TI_DISPO_DEM (2U)
#  endif

#  ifndef Cx3_DEMARRAGE
#   define Cx3_DEMARRAGE (3U)
#  endif

#  ifndef Cx4_REDEMARRAGE
#   define Cx4_REDEMARRAGE (4U)
#  endif

#  ifndef Cx5_TA_NOMINAL
#   define Cx5_TA_NOMINAL (5U)
#  endif

#  ifndef Cx6_TA_DEGRADE
#   define Cx6_TA_DEGRADE (6U)
#  endif

#  ifndef Cx7_TA_SECU
#   define Cx7_TA_SECU (7U)
#  endif

#  ifndef Cx8_Reserved
#   define Cx8_Reserved (8U)
#  endif

#  ifndef Cx9_Reserved
#   define Cx9_Reserved (9U)
#  endif

#  ifndef CxA_Reserved
#   define CxA_Reserved (10U)
#  endif

#  ifndef CxB_Reserved
#   define CxB_Reserved (11U)
#  endif

#  ifndef CxC_Reserved
#   define CxC_Reserved (12U)
#  endif

#  ifndef CxD_Reserved
#   define CxD_Reserved (13U)
#  endif

#  ifndef CxE_Reserved
#   define CxE_Reserved (14U)
#  endif

#  ifndef CxF_Reserved
#   define CxF_Reserved (15U)
#  endif

#  ifndef Cx0_Key_OFF
#   define Cx0_Key_OFF (0U)
#  endif

#  ifndef Cx1_Key_ACC
#   define Cx1_Key_ACC (1U)
#  endif

#  ifndef Cx2_Key_ON
#   define Cx2_Key_ON (2U)
#  endif

#  ifndef Cx3_Key_START
#   define Cx3_Key_START (3U)
#  endif

#  ifndef Cx0_OFF
#   define Cx0_OFF (0U)
#  endif

#  ifndef Cx1_Active_Drive
#   define Cx1_Active_Drive (1U)
#  endif

#  ifndef Cx2_Discharge
#   define Cx2_Discharge (2U)
#  endif

#  ifndef Cx3_PI_Charge
#   define Cx3_PI_Charge (3U)
#  endif

#  ifndef Cx4_PI_Balance
#   define Cx4_PI_Balance (4U)
#  endif

#  ifndef Cx5_PI_Discharge
#   define Cx5_PI_Discharge (5U)
#  endif

#  ifndef Cx0_Park
#   define Cx0_Park (0U)
#  endif

#  ifndef Cx1_Client
#   define Cx1_Client (1U)
#  endif

#  ifndef Cx2_Absent
#   define Cx2_Absent (2U)
#  endif

#  ifndef Cx3_Undetermined
#   define Cx3_Undetermined (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPPCOM_TYPE_H */
