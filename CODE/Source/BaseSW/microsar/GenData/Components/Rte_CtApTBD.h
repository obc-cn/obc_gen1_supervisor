/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApTBD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApTBD>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPTBD_H
# define RTE_CTAPTBD_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApTBD_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(ABS_VehSpdValidFlag, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag;
extern VAR(BMS_AuxBattVolt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt;
extern VAR(BMS_CC2_connection_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status;
extern VAR(BMS_FastChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt;
extern VAR(BMS_Fault, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault;
extern VAR(BMS_RelayOpenReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq;
extern VAR(DIAG_INTEGRA_ELEC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC;
extern VAR(EFFAC_DEFAUT_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG;
extern VAR(VCU_Keyposition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales;
extern VAR(BMS_SlowChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt;
extern VAR(MODE_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(DCDC_OVERTEMP, RTE_VAR_INIT) Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP;
extern VAR(OBC_DCChargingPlugAConnConf, RTE_VAR_INIT) Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf;
extern VAR(SUPV_DTCRegistred, RTE_VAR_INIT) Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred;
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP (FALSE)
#  define Rte_InitValue_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf (FALSE)
#  define Rte_InitValue_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup Rte_Read_CtApTBD_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup
#  define Rte_Read_CtApTBD_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(data) (*(data) = Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag Rte_Read_CtApTBD_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag
#  define Rte_Read_CtApTBD_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt Rte_Read_CtApTBD_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt
#  define Rte_Read_CtApTBD_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status Rte_Read_CtApTBD_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status
#  define Rte_Read_CtApTBD_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_FastChargeSt_BMS_FastChargeSt Rte_Read_CtApTBD_PpInt_BMS_FastChargeSt_BMS_FastChargeSt
#  define Rte_Read_CtApTBD_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_Fault_BMS_Fault Rte_Read_CtApTBD_PpInt_BMS_Fault_BMS_Fault
#  define Rte_Read_CtApTBD_PpInt_BMS_Fault_BMS_Fault(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq Rte_Read_CtApTBD_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq
#  define Rte_Read_CtApTBD_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC Rte_Read_CtApTBD_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC
#  define Rte_Read_CtApTBD_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(data) (*(data) = Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG Rte_Read_CtApTBD_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG
#  define Rte_Read_CtApTBD_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(data) (*(data) = Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_Keyposition_VCU_Keyposition Rte_Read_CtApTBD_PpInt_VCU_Keyposition_VCU_Keyposition
#  define Rte_Read_CtApTBD_PpInt_VCU_Keyposition_VCU_Keyposition(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempAftsales_DeOutputTempAC1Aftsales Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempAC1Aftsales
#  define Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempAC1Aftsales(data) (*(data) = Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempAftsales_DeOutputTempAC2Aftsales Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempAC2Aftsales
#  define Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempAC2Aftsales(data) (*(data) = Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempAftsales_DeOutputTempAC3Aftsales Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempAC3Aftsales
#  define Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempAC3Aftsales(data) (*(data) = Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempAftsales_DeOutputTempACNAftsales Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempACNAftsales
#  define Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempACNAftsales(data) (*(data) = Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempAftsales_DeOutputTempDC1Aftsales Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempDC1Aftsales
#  define Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempDC1Aftsales(data) (*(data) = Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempAftsales_DeOutputTempDC2Aftsales Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempDC2Aftsales
#  define Rte_Read_CtApTBD_PpOutputTempAftsales_DeOutputTempDC2Aftsales(data) (*(data) = Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt Rte_Read_CtApTBD_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt
#  define Rte_Read_CtApTBD_Ppint_BMS_SlowChargeSt_BMS_SlowChargeSt(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_Ppint_MODE_DIAG_MODE_DIAG Rte_Read_CtApTBD_Ppint_MODE_DIAG_MODE_DIAG
#  define Rte_Read_CtApTBD_Ppint_MODE_DIAG_MODE_DIAG(data) (*(data) = Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP Rte_Write_CtApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP
#  define Rte_Write_CtApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(data) (Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf Rte_Write_CtApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf
#  define Rte_Write_CtApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(data) (Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred Rte_Write_CtApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred
#  define Rte_Write_CtApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(data) (Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


# endif /* !defined(RTE_CORE) */


# define CtApTBD_START_SEC_CODE
# include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApTBD_tbd RCtApTBD_tbd
# endif

FUNC(void, CtApTBD_CODE) RCtApTBD_tbd(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApTBD_STOP_SEC_CODE
# include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPTBD_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
