/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApWUM_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApWUM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPWUM_TYPE_H
# define RTE_CTAPWUM_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx00_Stop_mode
#   define Cx00_Stop_mode (0U)
#  endif

#  ifndef Cx01_Failure_mode_1
#   define Cx01_Failure_mode_1 (1U)
#  endif

#  ifndef Cx02_Failure_mode_2
#   define Cx02_Failure_mode_2 (2U)
#  endif

#  ifndef Cx03_Failure_mode_3
#   define Cx03_Failure_mode_3 (3U)
#  endif

#  ifndef Cx04_Failure_mode_4
#   define Cx04_Failure_mode_4 (4U)
#  endif

#  ifndef Cx05_Failure_mode_5
#   define Cx05_Failure_mode_5 (5U)
#  endif

#  ifndef Cx06_Failure_mode_6
#   define Cx06_Failure_mode_6 (6U)
#  endif

#  ifndef Cx07_Failure_mode_7
#   define Cx07_Failure_mode_7 (7U)
#  endif

#  ifndef Cx08_AC_mode
#   define Cx08_AC_mode (8U)
#  endif

#  ifndef Cx09_HP_mode
#   define Cx09_HP_mode (9U)
#  endif

#  ifndef Cx0A_Deshum_total_heating_mode
#   define Cx0A_Deshum_total_heating_mode (10U)
#  endif

#  ifndef Cx0B_Deshum_simple_heating_mode
#   define Cx0B_Deshum_simple_heating_mode (11U)
#  endif

#  ifndef Cx0C_De_icing_mode_
#   define Cx0C_De_icing_mode_ (12U)
#  endif

#  ifndef Cx0D_transition_mode
#   define Cx0D_transition_mode (13U)
#  endif

#  ifndef Cx0E_Failure_mode
#   define Cx0E_Failure_mode (14U)
#  endif

#  ifndef Cx0F_Chiller_alone_mode
#   define Cx0F_Chiller_alone_mode (15U)
#  endif

#  ifndef Cx10_AC_chiffler_mode
#   define Cx10_AC_chiffler_mode (16U)
#  endif

#  ifndef Cx11_Deshum_Chiller_mode
#   define Cx11_Deshum_Chiller_mode (17U)
#  endif

#  ifndef Cx12_Reserved
#   define Cx12_Reserved (18U)
#  endif

#  ifndef Cx13_Water_Heather_mode
#   define Cx13_Water_Heather_mode (19U)
#  endif

#  ifndef Cx14_Reserved
#   define Cx14_Reserved (20U)
#  endif

#  ifndef Cx15_Reserved
#   define Cx15_Reserved (21U)
#  endif

#  ifndef Cx16_Reserved
#   define Cx16_Reserved (22U)
#  endif

#  ifndef Cx17_Reserved
#   define Cx17_Reserved (23U)
#  endif

#  ifndef Cx18_Reserved
#   define Cx18_Reserved (24U)
#  endif

#  ifndef Cx19_Reserved
#   define Cx19_Reserved (25U)
#  endif

#  ifndef Cx1A_Reserved
#   define Cx1A_Reserved (26U)
#  endif

#  ifndef Cx1B_Reserved
#   define Cx1B_Reserved (27U)
#  endif

#  ifndef Cx1C_Reserved
#   define Cx1C_Reserved (28U)
#  endif

#  ifndef Cx1D_Reserved
#   define Cx1D_Reserved (29U)
#  endif

#  ifndef Cx1E_Reserved
#   define Cx1E_Reserved (30U)
#  endif

#  ifndef Cx1F_Reserved
#   define Cx1F_Reserved (31U)
#  endif

#  ifndef COMM_NO_COMMUNICATION
#   define COMM_NO_COMMUNICATION (0U)
#  endif

#  ifndef COMM_SILENT_COMMUNICATION
#   define COMM_SILENT_COMMUNICATION (1U)
#  endif

#  ifndef COMM_FULL_COMMUNICATION
#   define COMM_FULL_COMMUNICATION (2U)
#  endif

#  ifndef APP_STATE_0
#   define APP_STATE_0 (0U)
#  endif

#  ifndef APP_STATE_1
#   define APP_STATE_1 (1U)
#  endif

#  ifndef APP_STATE_2
#   define APP_STATE_2 (2U)
#  endif

#  ifndef APP_STATE_3
#   define APP_STATE_3 (3U)
#  endif

#  ifndef APP_STATE_4
#   define APP_STATE_4 (4U)
#  endif

#  ifndef APP_STATE_5
#   define APP_STATE_5 (5U)
#  endif

#  ifndef Cx0_invalid_value
#   define Cx0_invalid_value (0U)
#  endif

#  ifndef Cx1_not_connected
#   define Cx1_not_connected (1U)
#  endif

#  ifndef Cx2_full_connected
#   define Cx2_full_connected (2U)
#  endif

#  ifndef Cx3_CC_is_half_connected
#   define Cx3_CC_is_half_connected (3U)
#  endif

#  ifndef Cx0_Disconnected
#   define Cx0_Disconnected (0U)
#  endif

#  ifndef Cx1_In_progress
#   define Cx1_In_progress (1U)
#  endif

#  ifndef Cx2_Failure
#   define Cx2_Failure (2U)
#  endif

#  ifndef Cx3_Stopped
#   define Cx3_Stopped (3U)
#  endif

#  ifndef Cx4_Finished
#   define Cx4_Finished (4U)
#  endif

#  ifndef Cx5_Reserved
#   define Cx5_Reserved (5U)
#  endif

#  ifndef Cx6_Reserved
#   define Cx6_Reserved (6U)
#  endif

#  ifndef Cx7_Reserved
#   define Cx7_Reserved (7U)
#  endif

#  ifndef Cx0_OFF
#   define Cx0_OFF (0U)
#  endif

#  ifndef Cx1_Active_Drive
#   define Cx1_Active_Drive (1U)
#  endif

#  ifndef Cx2_Discharge
#   define Cx2_Discharge (2U)
#  endif

#  ifndef Cx3_PI_Charge
#   define Cx3_PI_Charge (3U)
#  endif

#  ifndef Cx4_PI_Balance
#   define Cx4_PI_Balance (4U)
#  endif

#  ifndef Cx5_PI_Discharge
#   define Cx5_PI_Discharge (5U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPWUM_TYPE_H */
