/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApJDD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApJDD>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPJDD_H
# define RTE_CTAPJDD_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApJDD_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup;
extern VAR(DATA_ACQ_JDD_BSI_2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2;
extern VAR(VCU_CDEAccJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD;
extern VAR(VCU_CDEApcJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD;
extern VAR(VCU_CompteurRazGct, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct;
extern VAR(VCU_CptTemporel, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel;
extern VAR(VCU_EtatPrincipSev, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev;
extern VAR(VCU_EtatReseauElec, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec;
extern VAR(VCU_Kilometrage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage;
extern VAR(VCU_PosShuntJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(NEW_JDD_OBC_DCDC_BYTE_0, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_1, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_2, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_3, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_4, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_5, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_6, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_7, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError;
extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 (0U)
#  define Rte_InitValue_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 (0U)
#  define Rte_InitValue_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault Rte_Read_CtApJDD_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault
#  define Rte_Read_CtApJDD_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(data) (*(data) = Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApJDD_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApJDD_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBusOffCANFault_DeBusOffCANFault Rte_Read_CtApJDD_PpBusOffCANFault_DeBusOffCANFault
#  define Rte_Read_CtApJDD_PpBusOffCANFault_DeBusOffCANFault(data) (*(data) = Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup Rte_Read_CtApJDD_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup
#  define Rte_Read_CtApJDD_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(data) (*(data) = Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest Rte_Read_CtApJDD_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest
#  define Rte_Read_CtApJDD_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(data) (*(data) = Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 Rte_Read_CtApJDD_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2
#  define Rte_Read_CtApJDD_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(data) (*(data) = Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD Rte_Read_CtApJDD_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD
#  define Rte_Read_CtApJDD_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD Rte_Read_CtApJDD_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD
#  define Rte_Read_CtApJDD_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct Rte_Read_CtApJDD_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct
#  define Rte_Read_CtApJDD_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel Rte_Read_CtApJDD_PpInt_VCU_CptTemporel_VCU_CptTemporel
#  define Rte_Read_CtApJDD_PpInt_VCU_CptTemporel_VCU_CptTemporel(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev Rte_Read_CtApJDD_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev
#  define Rte_Read_CtApJDD_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec Rte_Read_CtApJDD_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec
#  define Rte_Read_CtApJDD_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage Rte_Read_CtApJDD_PpInt_VCU_Kilometrage_VCU_Kilometrage
#  define Rte_Read_CtApJDD_PpInt_VCU_Kilometrage_VCU_Kilometrage(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD Rte_Read_CtApJDD_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD
#  define Rte_Read_CtApJDD_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization Rte_Read_CtApJDD_PpShutdownAuthorization_DeShutdownAuthorization
#  define Rte_Read_CtApJDD_PpShutdownAuthorization_DeShutdownAuthorization(data) (*(data) = Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct Rte_Read_CtApJDD_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct
#  define Rte_Read_CtApJDD_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel Rte_Read_CtApJDD_Pp_VCU_CptTemporel_VCU_CptTemporel
#  define Rte_Read_CtApJDD_Pp_VCU_CptTemporel_VCU_CptTemporel(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_CptTemporel_oVCU_oE_CAN_80a510d2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7
#  define Rte_Write_CtApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(data) (Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError Rte_Write_CtApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError
#  define Rte_Write_CtApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(data) (Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_GetEventFailed(Dem_EventIdType parg0, P2VAR(boolean, AUTOMATIC, RTE_DEMSATELLITE_0_APPL_VAR) EventFailed); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_EvtInfo_DTC_0x056216_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)47, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_GetEventUdsStatus(Dem_EventIdType parg0, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMSATELLITE_0_APPL_VAR) UDSStatusByte); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_EvtInfo_DTC_0x056216_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)47, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x056317_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)48, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x056317_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)48, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0a0804_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0a0804_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)2, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0a084b_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0a084b_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0a9464_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)3, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0a9464_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)3, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0af864_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)4, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0af864_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)4, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0cf464_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)66, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x0cf464_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)66, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x108093_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)49, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x108093_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)49, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c413_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)67, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c413_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)67, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c512_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)68, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c512_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)68, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c613_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)69, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c613_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)69, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c713_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)70, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x10c713_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)70, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120a11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)35, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120a11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)35, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120a12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)36, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120a12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)36, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120b11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)37, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120b11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)37, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120b12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)38, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120b12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)38, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120c64_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)39, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120c64_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)39, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120c98_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120c98_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120d64_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)41, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120d64_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)41, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120d98_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)42, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x120d98_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)42, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d711_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d711_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d712_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)10, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d712_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)10, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d713_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d713_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)11, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d811_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)12, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d811_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)12, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d812_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d812_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)13, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d813_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)14, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d813_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)14, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d911_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)15, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d911_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)15, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d912_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)16, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d912_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)16, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d913_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)17, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12d913_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)17, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12da11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)18, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12da11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)18, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12da12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)19, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12da12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)19, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12da13_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)20, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12da13_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)20, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12db12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)21, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12db12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)21, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12dc11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)22, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12dc11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)22, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12dd12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)23, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12dd12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)23, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12de11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)24, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12de11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)24, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12df13_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)25, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12df13_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)25, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e012_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)26, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e012_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)26, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e111_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)27, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e111_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)27, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e213_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)28, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e213_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)28, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e319_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)29, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e319_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)29, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e712_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)30, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e712_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)30, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e811_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)31, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e811_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)31, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e912_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)32, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12e912_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)32, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12ea11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)33, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12ea11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)33, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12f316_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)71, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12f316_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)71, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12f917_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)34, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x12f917_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)34, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x13e919_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)64, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x13e919_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)64, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x166c64_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)65, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x166c64_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)65, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179e11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)43, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179e11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)43, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179e12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179e12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179f11_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)45, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179f11_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)45, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179f12_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)46, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x179f12_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)46, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a0064_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)6, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a0064_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)6, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a7104_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)7, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a7104_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)7, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a714b_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)5, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a714b_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)5, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a7172_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)8, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0x1a7172_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)8, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)50, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)50, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)51, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)51, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd18787_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)54, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd18787_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)54, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd1a087_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)52, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd1a087_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)52, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd20781_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)55, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd20781_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)55, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd2a081_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)53, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd2a081_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)53, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd38782_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)56, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd38782_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)56, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd38783_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)57, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xd38783_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)57, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00081_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)58, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00081_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)58, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00087_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)59, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00087_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)59, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00214_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)60, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00214_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)60, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00362_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)61, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xe00362_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)61, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_ReadBlock(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_ReadBlock(arg1) (NvM_ReadBlock((NvM_BlockIdType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_SetRamBlockStatus(NvM_BlockIdType parg0, boolean RamBlockStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(arg1) (NvM_SetRamBlockStatus((NvM_BlockIdType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx() (PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalEnableJDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApJDD.CalEnableJDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtArrayJDDNvMRamMirror, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApJDD_PimJDD_NvMRamMirror;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_PimJDD_NvMRamMirror() (&((*RtePim_PimJDD_NvMRamMirror())[0]))
#  else
#   define Rte_Pim_PimJDD_NvMRamMirror() RtePim_PimJDD_NvMRamMirror()
#  endif
#  define RtePim_PimJDD_NvMRamMirror() \
  (&Rte_CpApJDD_PimJDD_NvMRamMirror)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApJDD_START_SEC_CODE
# include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd
#  define RTE_RUNNABLE_PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd
#  define RTE_RUNNABLE_RCtApJDD_init RCtApJDD_init
#  define RTE_RUNNABLE_RCtApJDD_task10ms RCtApJDD_task10ms
# endif

FUNC(void, CtApJDD_CODE) PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApJDD_CODE) PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApJDD_CODE) RCtApJDD_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApJDD_CODE) RCtApJDD_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApJDD_STOP_SEC_CODE
# include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticInfo_E_NOT_OK (1U)

#  define RTE_E_NvMService_AC3_SRBS_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPJDD_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
