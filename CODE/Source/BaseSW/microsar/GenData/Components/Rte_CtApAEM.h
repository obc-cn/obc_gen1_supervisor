/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApAEM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApAEM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPAEM_H
# define RTE_CTAPAEM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApAEM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(BSI_MainWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup;
extern VAR(BSI_PostDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup;
extern VAR(BSI_PreDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup;
extern VAR(VCU_PrecondElecWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup;
extern VAR(VCU_StopDelayedHMIWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpCANComRequest_DeCANComRequest;
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC;
extern VAR(OBC_CoolingWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup;
extern VAR(OBC_HVBattRechargeWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup;
extern VAR(OBC_HoldDiscontactorWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup;
extern VAR(OBC_PIStateInfoWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup;
extern VAR(SUPV_ECUElecStateRCD, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD;
extern VAR(SUPV_PostDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState;
extern VAR(SUPV_PreDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState;
extern VAR(SUPV_PrecondElecWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState;
extern VAR(SUPV_StopDelayedHMIWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP;
extern VAR(SUPV_RCDLineState, RTE_VAR_INIT) Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpAppRCDECUState_DeAppRCDECUState (2U)
#  define Rte_InitValue_PpCANComRequest_DeCANComRequest (0)
#  define Rte_InitValue_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup (0)
#  define Rte_InitValue_PpECU_WakeupMain_DeECU_WakeupMain (FALSE)
#  define Rte_InitValue_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd (FALSE)
#  define Rte_InitValue_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst (FALSE)
#  define Rte_InitValue_PpFaultRCDLineSC_DeFaultRCDLineSC (FALSE)
#  define Rte_InitValue_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup (FALSE)
#  define Rte_InitValue_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup (FALSE)
#  define Rte_InitValue_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup (FALSE)
#  define Rte_InitValue_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup (FALSE)
#  define Rte_InitValue_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD (2U)
#  define Rte_InitValue_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState (FALSE)
#  define Rte_InitValue_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState (FALSE)
#  define Rte_InitValue_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState (FALSE)
#  define Rte_InitValue_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState (FALSE)
#  define Rte_InitValue_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd (FALSE)
#  define Rte_InitValue_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst (FALSE)
#  define Rte_InitValue_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC (FALSE)
#  define Rte_InitValue_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue (FALSE)
#  define Rte_InitValue_PpShutdownAuthorization_DeShutdownAuthorization (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC Rte_Read_CtApAEM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC
#  define Rte_Read_CtApAEM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(data) (*(data) = Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP Rte_Read_CtApAEM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP
#  define Rte_Read_CtApAEM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(data) (*(data) = Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDiagToolsRequest_DeDiagToolsRequest Rte_Read_CtApAEM_PpDiagToolsRequest_DeDiagToolsRequest
#  define Rte_Read_CtApAEM_PpDiagToolsRequest_DeDiagToolsRequest(data) (*(data) = Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest Rte_Read_CtApAEM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest
#  define Rte_Read_CtApAEM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(data) (*(data) = Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP Rte_Read_CtApAEM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP
#  define Rte_Read_CtApAEM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(data) (*(data) = Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP Rte_Read_CtApAEM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP
#  define Rte_Read_CtApAEM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(data) (*(data) = Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Read_CtApAEM_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApAEM_PpInt_ABS_VehSpd_ABS_VehSpd(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup Rte_Read_CtApAEM_PpInt_BSI_MainWakeup_BSI_MainWakeup
#  define Rte_Read_CtApAEM_PpInt_BSI_MainWakeup_BSI_MainWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup Rte_Read_CtApAEM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup
#  define Rte_Read_CtApAEM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup Rte_Read_CtApAEM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup
#  define Rte_Read_CtApAEM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState Rte_Read_CtApAEM_PpInt_SUPV_RCDLineState_SUPV_RCDLineState
#  define Rte_Read_CtApAEM_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data) (*(data) = Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup Rte_Read_CtApAEM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup
#  define Rte_Read_CtApAEM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup Rte_Read_CtApAEM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup
#  define Rte_Read_CtApAEM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP Rte_Read_CtApAEM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP
#  define Rte_Read_CtApAEM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(data) (*(data) = Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpAppRCDECUState_DeAppRCDECUState Rte_Write_CtApAEM_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Write_CtApAEM_PpAppRCDECUState_DeAppRCDECUState(data) (Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpCANComRequest_DeCANComRequest Rte_Write_CtApAEM_PpCANComRequest_DeCANComRequest
#  define Rte_Write_CtApAEM_PpCANComRequest_DeCANComRequest(data) (Rte_CpApAEM_PpCANComRequest_DeCANComRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup Rte_Write_CtApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup
#  define Rte_Write_CtApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(data) (Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain Rte_Write_CtApAEM_PpECU_WakeupMain_DeECU_WakeupMain
#  define Rte_Write_CtApAEM_PpECU_WakeupMain_DeECU_WakeupMain(data) (Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd Rte_Write_CtApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd
#  define Rte_Write_CtApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd(data) (Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst Rte_Write_CtApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst
#  define Rte_Write_CtApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst(data) (Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpFaultRCDLineSC_DeFaultRCDLineSC Rte_Write_CtApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC
#  define Rte_Write_CtApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC(data) (Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup Rte_Write_CtApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup
#  define Rte_Write_CtApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(data) (Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup Rte_Write_CtApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup
#  define Rte_Write_CtApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(data) (Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup Rte_Write_CtApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup
#  define Rte_Write_CtApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(data) (Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup Rte_Write_CtApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup
#  define Rte_Write_CtApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(data) (Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD Rte_Write_CtApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD
#  define Rte_Write_CtApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(data) (Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState Rte_Write_CtApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState
#  define Rte_Write_CtApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(data) (Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState Rte_Write_CtApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState
#  define Rte_Write_CtApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(data) (Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState Rte_Write_CtApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState
#  define Rte_Write_CtApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(data) (Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState Rte_Write_CtApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState
#  define Rte_Write_CtApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(data) (Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd Rte_Write_CtApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd
#  define Rte_Write_CtApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd(data) (Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst Rte_Write_CtApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst
#  define Rte_Write_CtApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst(data) (Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC Rte_Write_CtApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC
#  define Rte_Write_CtApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC(data) (Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue Rte_Write_CtApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue
#  define Rte_Write_CtApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(data) (Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpShutdownAuthorization_DeShutdownAuthorization Rte_Write_CtApAEM_PpShutdownAuthorization_DeShutdownAuthorization
#  define Rte_Write_CtApAEM_PpShutdownAuthorization_DeShutdownAuthorization(data) (Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_UCE_tiDegMainWkuDeac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiDegMainWkuDeac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMainDisrdDet_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMainDisrdDet_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMainIncstDet_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMainIncstDet_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMainTransForc_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMainTransForc_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMainWkuReh_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMainWkuReh_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiComLatch_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiComLatch_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiIntPtlWku_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiIntPtlWku_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY1_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY1_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY2_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY3_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY4_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY4_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY5_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY6_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY7_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiMstPtlWkuY8_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMaxTiShutDownPrep_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMaxTiShutDownPrep_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY1_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY1_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY2_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY2_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY3_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY3_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY4_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY4_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY5_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY5_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY6_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY6_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY7_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY7_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiMstPtlWkuY8_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiMstPtlWkuY8_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMinTiShutDownPrep_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMinTiShutDownPrep_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX10Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX10Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX11Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX11Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX12Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX12Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX13Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX13Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX14Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX14Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX15Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX15Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX16Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX16Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX1Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX1Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX2Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX2Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX3Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX3Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX4Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX4Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX5Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX5Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX6Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX6Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX7Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX7Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX8Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX8Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX9Lock_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX9Lock_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiRCDLineCmdAcv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiRCDLineCmdAcv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiRCDLineScgDet_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiRCDLineScgDet_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiRCDLineScgReh_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiRCDLineScgReh_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiTransitoryDeac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiTransitoryDeac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_spdThdDegDeac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_spdThdDegDeac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_spdThdNomDeac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_spdThdNomDeac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiMainWkuAcv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiMainWkuAcv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiNomMainWkuDeac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiNomMainWkuDeac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX10Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX10Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX10Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX10Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX11Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX11Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX11Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX11Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX12Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX12Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX12Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX12Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX13Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX13Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX13Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX13Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX14Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX14Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX14Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX14Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX15Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX15Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX15Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX15Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX16Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX16Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX16Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX16Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX1Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX1Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX1Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX1Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX2Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX2Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX2Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX2Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX3Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX3Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX3Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX3Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX4Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX4Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX4Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX4Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX5Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX5Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX5Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX5Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX6Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX6Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX6Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX6Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX7Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX7Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX7Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX7Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX8Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX8Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX8Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX8Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX9Acv_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX9Acv_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_tiPtlWkuX9Deac_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_tiPtlWkuX9Deac_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX10_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX10_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX11_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX11_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX12_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX12_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX13_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX13_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX14_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX14_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX15_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX15_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX16_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX16_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX1_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX1_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX2_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX2_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX3_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX3_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX4_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX4_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX5_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX5_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX6_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX6_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX7_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX7_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX8_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX8_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuX9_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuX9_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY1_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY1_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY2_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY2_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY3_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY3_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY4_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY4_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY5_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY5_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY6_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY6_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY7_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY7_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bInhPtlWkuY8_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bInhPtlWkuY8_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX10AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX11AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX12AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX13AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX14AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX15AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX16AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX1AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX2AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX3AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX4AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX5AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX6AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX7AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX8AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_bSlavePtlWkuX9AcvMod_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_UCE_noUCETyp_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApAEM.UCE_noUCETyp_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApAEM_START_SEC_CODE
# include "CtApAEM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CtApAEM_Init CtApAEM_Init
#  define RTE_RUNNABLE_RCtApAEM_task10msA RCtApAEM_task10msA
#  define RTE_RUNNABLE_RCtApAEM_task10msB RCtApAEM_task10msB
# endif

FUNC(void, CtApAEM_CODE) CtApAEM_Init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApAEM_CODE) RCtApAEM_task10msA(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApAEM_CODE) RCtApAEM_task10msB(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApAEM_STOP_SEC_CODE
# include "CtApAEM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPAEM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
