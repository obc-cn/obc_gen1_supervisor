/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLED.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApLED>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLED_H
# define RTE_CTAPLED_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApLED_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl;
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl;
extern VAR(IdtPlgLedrCtrl, RTE_VAR_INIT) Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl;
extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeInProgress;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeEndOfCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeGuideManagement;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeProgrammingCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockStateError_DeLockStateError;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpChLedRGBCtl_DeChLedBrCtl (0U)
#  define Rte_InitValue_PpChLedRGBCtl_DeChLedGrCtl (0U)
#  define Rte_InitValue_PpChLedRGBCtl_DeChLedRrCtl (0U)
#  define Rte_InitValue_PpPlgLedrCtrl_DePlgLedrCtrl (0U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApLED_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApLED_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_DeChargeError Rte_Read_CtApLED_PpLedModes_DeChargeError
#  define Rte_Read_CtApLED_PpLedModes_DeChargeError(data) (*(data) = Rte_CpApILT_PpLedModes_DeChargeError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_DeChargeInProgress Rte_Read_CtApLED_PpLedModes_DeChargeInProgress
#  define Rte_Read_CtApLED_PpLedModes_DeChargeInProgress(data) (*(data) = Rte_CpApILT_PpLedModes_DeChargeInProgress, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_DeEndOfCharge Rte_Read_CtApLED_PpLedModes_DeEndOfCharge
#  define Rte_Read_CtApLED_PpLedModes_DeEndOfCharge(data) (*(data) = Rte_CpApILT_PpLedModes_DeEndOfCharge, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_DeGuideManagement Rte_Read_CtApLED_PpLedModes_DeGuideManagement
#  define Rte_Read_CtApLED_PpLedModes_DeGuideManagement(data) (*(data) = Rte_CpApILT_PpLedModes_DeGuideManagement, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_DeProgrammingCharge Rte_Read_CtApLED_PpLedModes_DeProgrammingCharge
#  define Rte_Read_CtApLED_PpLedModes_DeProgrammingCharge(data) (*(data) = Rte_CpApILT_PpLedModes_DeProgrammingCharge, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR Rte_Read_CtApLED_PpLockLED2BEPR_DeLockLED2BEPR
#  define Rte_Read_CtApLED_PpLockLED2BEPR_DeLockLED2BEPR(data) (*(data) = Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLockStateError_DeLockStateError Rte_Read_CtApLED_PpLockStateError_DeLockStateError
#  define Rte_Read_CtApLED_PpLockStateError_DeLockStateError(data) (*(data) = Rte_CpApILT_PpLockStateError_DeLockStateError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpChLedRGBCtl_DeChLedBrCtl Rte_Write_CtApLED_PpChLedRGBCtl_DeChLedBrCtl
#  define Rte_Write_CtApLED_PpChLedRGBCtl_DeChLedBrCtl(data) (Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpChLedRGBCtl_DeChLedGrCtl Rte_Write_CtApLED_PpChLedRGBCtl_DeChLedGrCtl
#  define Rte_Write_CtApLED_PpChLedRGBCtl_DeChLedGrCtl(data) (Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpChLedRGBCtl_DeChLedRrCtl Rte_Write_CtApLED_PpChLedRGBCtl_DeChLedRrCtl
#  define Rte_Write_CtApLED_PpChLedRGBCtl_DeChLedRrCtl(data) (Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl Rte_Write_CtApLED_PpPlgLedrCtrl_DePlgLedrCtrl
#  define Rte_Write_CtApLED_PpPlgLedrCtrl_DePlgLedrCtrl(data) (Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalDutyBlueChargeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyBlueChargeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyBlueChargeInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyBlueChargeInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyBlueEndOfCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyBlueEndOfCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyBlueGuideManagement() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyBlueGuideManagement) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyBlueProgrammingCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyBlueProgrammingCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyGreenChargeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyGreenChargeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyGreenChargeInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyGreenChargeInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyGreenEndOfCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyGreenEndOfCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyGreenGuideManagement() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyGreenGuideManagement) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyGreenProgrammingCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyGreenProgrammingCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyPlug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyPlug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyRedChargeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyRedChargeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyRedChargeInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyRedChargeInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyRedEndOfCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyRedEndOfCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyRedGuideManagement() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyRedGuideManagement) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyRedProgrammingCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalDutyRedProgrammingCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffChargeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffChargeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffChargeInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffChargeInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffEndOfCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffEndOfCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffGuideManagement() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffGuideManagement) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffPlugA() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffPlugA) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffPlugB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffPlugB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffPlugC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffPlugC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOffProgrammingCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOffProgrammingCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnChargeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnChargeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnChargeInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnChargeInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnEndOfCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnEndOfCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnGuideManagement() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnGuideManagement) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnPlugA() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnPlugA) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnPlugB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnPlugB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnPlugC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnPlugC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeOnProgrammingCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLED.CalTimeOnProgrammingCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApLED_START_SEC_CODE
# include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApLED_init RCtApLED_init
#  define RTE_RUNNABLE_RCtApLED_task100ms RCtApLED_task100ms
#  define RTE_RUNNABLE_RCtApLED_task10ms RCtApLED_task10ms
# endif

FUNC(void, CtApLED_CODE) RCtApLED_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLED_CODE) RCtApLED_task100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLED_CODE) RCtApLED_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApLED_STOP_SEC_CODE
# include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLED_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
