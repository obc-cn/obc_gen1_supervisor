/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApDER.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApDER>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPDER_H
# define RTE_CTAPDER_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApDER_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull;
extern VAR(DCDC_Temperature, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature;
extern VAR(DCLV_Temp_Derating_Factor, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor;
extern VAR(OBC_OBCTemp, RTE_VAR_INIT) Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpOBCDerating_DeOBCDerating;
extern VAR(IdtPDERATING_OBC, RTE_VAR_INIT) Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC;
extern VAR(IdtAmbientTemperaturePhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue;
extern VAR(DCHV_ADC_NTC_MOD_5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5;
extern VAR(DCHV_ADC_NTC_MOD_6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6;
extern VAR(DCLV_T_PP_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A;
extern VAR(DCLV_T_PP_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B;
extern VAR(DCLV_T_SW_BUCK_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A;
extern VAR(DCLV_T_SW_BUCK_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B;
extern VAR(PFC_Temp_M1_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C;
extern VAR(PFC_Temp_M4_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C;
extern VAR(IdtTempSyncRectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError (FALSE)
#  define Rte_InitValue_PpImplausibilityTempBuck_DeImplausibilityTempBuck (FALSE)
#  define Rte_InitValue_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull (FALSE)
#  define Rte_InitValue_PpInt_DCDC_Temperature_DCDC_Temperature (40U)
#  define Rte_InitValue_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor (0x400U)
#  define Rte_InitValue_PpInt_OBC_OBCTemp_OBC_OBCTemp (100U)
#  define Rte_InitValue_PpOBCDerating_DeOBCDerating (FALSE)
#  define Rte_InitValue_PpPDERATING_OBC_DePDERATING_OBC (11000U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue Rte_Read_CtApDER_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue
#  define Rte_Read_CtApDER_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 Rte_Read_CtApDER_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5
#  define Rte_Read_CtApDER_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 Rte_Read_CtApDER_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6
#  define Rte_Read_CtApDER_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A Rte_Read_CtApDER_PpInt_DCLV_T_PP_A_DCLV_T_PP_A
#  define Rte_Read_CtApDER_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B Rte_Read_CtApDER_PpInt_DCLV_T_PP_B_DCLV_T_PP_B
#  define Rte_Read_CtApDER_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A Rte_Read_CtApDER_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A
#  define Rte_Read_CtApDER_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B Rte_Read_CtApDER_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B
#  define Rte_Read_CtApDER_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C Rte_Read_CtApDER_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C
#  define Rte_Read_CtApDER_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C Rte_Read_CtApDER_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C
#  define Rte_Read_CtApDER_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue Rte_Read_CtApDER_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue
#  define Rte_Read_CtApDER_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError Rte_Write_CtApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError
#  define Rte_Write_CtApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(data) (Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck Rte_Write_CtApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck
#  define Rte_Write_CtApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck(data) (Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull Rte_Write_CtApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull
#  define Rte_Write_CtApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(data) (Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature Rte_Write_CtApDER_PpInt_DCDC_Temperature_DCDC_Temperature
#  define Rte_Write_CtApDER_PpInt_DCDC_Temperature_DCDC_Temperature(data) (Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor Rte_Write_CtApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor
#  define Rte_Write_CtApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(data) (Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp Rte_Write_CtApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp
#  define Rte_Write_CtApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp(data) (Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBCDerating_DeOBCDerating Rte_Write_CtApDER_PpOBCDerating_DeOBCDerating
#  define Rte_Write_CtApDER_PpOBCDerating_DeOBCDerating(data) (Rte_CpApDER_PpOBCDerating_DeOBCDerating = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPDERATING_OBC_DePDERATING_OBC Rte_Write_CtApDER_PpPDERATING_OBC_DePDERATING_OBC
#  define Rte_Write_CtApDER_PpPDERATING_OBC_DePDERATING_OBC(data) (Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalDCDC_FinishLinear() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCDC_FinishLinear) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCDC_MaxDeviation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCDC_MaxDeviation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCDC_MinDeviation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCDC_MinDeviation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCDC_StartLinear() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCDC_StartLinear) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempEndDerating_AmbTemp() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempEndDerating_AmbTemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempEndDerating_Buck_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempEndDerating_Buck_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempEndDerating_Buck_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempEndDerating_Buck_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempEndDerating_PushPull_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempEndDerating_PushPull_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempEndDerating_PushPull_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempEndDerating_PushPull_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempEndDerating_SyncRect() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempEndDerating_SyncRect) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempStartDerating_AmbTemp() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempStartDerating_AmbTemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempStartDerating_Buck_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempStartDerating_Buck_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempStartDerating_Buck_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempStartDerating_Buck_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempStartDerating_PushPull_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempStartDerating_PushPull_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempStartDerating_PushPull_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempStartDerating_PushPull_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVHighTempStartDerating_SyncRect() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVHighTempStartDerating_SyncRect) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempEndDerating_AmbTemp() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempEndDerating_AmbTemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempEndDerating_Buck_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempEndDerating_Buck_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempEndDerating_Buck_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempEndDerating_Buck_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempEndDerating_PushPull_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempEndDerating_PushPull_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempEndDerating_PushPull_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempEndDerating_PushPull_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempEndDerating_SyncRect() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempEndDerating_SyncRect) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempStartDerating_AmbTemp() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempStartDerating_AmbTemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempStartDerating_Buck_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempStartDerating_Buck_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempStartDerating_Buck_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempStartDerating_Buck_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempStartDerating_PushPull_A() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempStartDerating_PushPull_A) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempStartDerating_PushPull_B() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempStartDerating_PushPull_B) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVLowTempStartDerating_SyncRect() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalDCLVLowTempStartDerating_SyncRect) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempEndDerating_AmbTempOBC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempEndDerating_DCHV_M5() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempEndDerating_DCHV_M5) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempEndDerating_DCHV_M6() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempEndDerating_DCHV_M6) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempEndDerating_PFC_M1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempEndDerating_PFC_M1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempEndDerating_PFC_M4() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempEndDerating_PFC_M4) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempStartDerating_AmbTempOBC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempStartDerating_DCHV_M5() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempStartDerating_DCHV_M5) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempStartDerating_DCHV_M6() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempStartDerating_DCHV_M6) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempStartDerating_PFC_M1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempStartDerating_PFC_M1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCHighTempStartDerating_PFC_M4() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCHighTempStartDerating_PFC_M4) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempEndDerating_AmbTempOBC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempEndDerating_DCHV_M5() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempEndDerating_DCHV_M5) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempEndDerating_DCHV_M6() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempEndDerating_DCHV_M6) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempEndDerating_PFC_M1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempEndDerating_PFC_M1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempEndDerating_PFC_M4() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempEndDerating_PFC_M4) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempStartDerating_AmbTempOBC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempStartDerating_DCHV_M5() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempStartDerating_DCHV_M5) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempStartDerating_DCHV_M6() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempStartDerating_DCHV_M6) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempStartDerating_PFC_M1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempStartDerating_PFC_M1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCLowTempStartDerating_PFC_M4() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBCLowTempStartDerating_PFC_M4) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_FinishLinear() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBC_FinishLinear) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_MaxDeviation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBC_MaxDeviation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_MinDeviation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBC_MinDeviation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_StartLinear() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApDER.CalOBC_StartLinear) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApDER_START_SEC_CODE
# include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApDER_init RCtApDER_init
#  define RTE_RUNNABLE_RCtApDER_task10ms RCtApDER_task10ms
# endif

FUNC(void, CtApDER_CODE) RCtApDER_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApDER_CODE) RCtApDER_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApDER_STOP_SEC_CODE
# include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPDER_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
