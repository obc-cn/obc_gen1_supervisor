/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLAD.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApLAD>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLAD_H
# define RTE_CTAPLAD_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApLAD_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP;
extern VAR(IdtElockFeedbackCurrent, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent;
extern VAR(IdtElockFeedbackLock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock;
extern VAR(IdtElockFeedbackUnlock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock;
extern VAR(IdtHWEditionDetected, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected;
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
extern VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue;
extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;
extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode;
extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpELockFaults_DeOutputElockHFaultOpenCircuit (FALSE)
#  define Rte_InitValue_PpELockFaults_DeOutputElockHFaultSCG (FALSE)
#  define Rte_InitValue_PpELockFaults_DeOutputElockHFaultSCP (FALSE)
#  define Rte_InitValue_PpELockFaults_DeOutputElockLFaultOpenCircuit (FALSE)
#  define Rte_InitValue_PpELockFaults_DeOutputElockLFaultSCG (FALSE)
#  define Rte_InitValue_PpELockFaults_DeOutputElockLFaultSCP (FALSE)
#  define Rte_InitValue_PpELockFaults_DeOutputElockOvercurrent (FALSE)
#  define Rte_InitValue_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions (FALSE)
#  define Rte_InitValue_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue (FALSE)
#  define Rte_InitValue_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApLAD_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApLAD_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApLAD_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApLAD_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState Rte_Read_CtApLAD_PpBatteryVoltageState_DeBatteryVoltageState
#  define Rte_Read_CtApLAD_PpBatteryVoltageState_DeBatteryVoltageState(data) (*(data) = Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG Rte_Read_CtApLAD_PpELockSensorFaults_DeELockSensorFaultSCG
#  define Rte_Read_CtApLAD_PpELockSensorFaults_DeELockSensorFaultSCG(data) (*(data) = Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP Rte_Read_CtApLAD_PpELockSensorFaults_DeELockSensorFaultSCP
#  define Rte_Read_CtApLAD_PpELockSensorFaults_DeELockSensorFaultSCP(data) (*(data) = Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent Rte_Read_CtApLAD_PpElockFeedbackCurrent_DeElockFeedbackCurrent
#  define Rte_Read_CtApLAD_PpElockFeedbackCurrent_DeElockFeedbackCurrent(data) (*(data) = Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock Rte_Read_CtApLAD_PpElockFeedbackLock_DeElockFeedbackLock
#  define Rte_Read_CtApLAD_PpElockFeedbackLock_DeElockFeedbackLock(data) (*(data) = Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock Rte_Read_CtApLAD_PpElockFeedbackUnlock_DeElockFeedbackUnlock
#  define Rte_Read_CtApLAD_PpElockFeedbackUnlock_DeElockFeedbackUnlock(data) (*(data) = Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode Rte_Read_CtApLAD_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode
#  define Rte_Read_CtApLAD_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(data) (*(data) = Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode Rte_Read_CtApLAD_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode
#  define Rte_Read_CtApLAD_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(data) (*(data) = Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpHWEditionDetected_DeHWEditionDetected Rte_Read_CtApLAD_PpHWEditionDetected_DeHWEditionDetected
#  define Rte_Read_CtApLAD_PpHWEditionDetected_DeHWEditionDetected(data) (*(data) = Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Read_CtApLAD_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApLAD_PpInt_ABS_VehSpd_ABS_VehSpd(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb Rte_Read_CtApLAD_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb
#  define Rte_Read_CtApLAD_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputELockSensor_DeOutputELockSensor Rte_Read_CtApLAD_PpOutputELockSensor_DeOutputELockSensor
#  define Rte_Read_CtApLAD_PpOutputELockSensor_DeOutputELockSensor(data) (*(data) = Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeState_DePlantModeState Rte_Read_CtApLAD_PpPlantModeState_DePlantModeState
#  define Rte_Read_CtApLAD_PpPlantModeState_DePlantModeState(data) (*(data) = Rte_CpApPLT_PpPlantModeState_DePlantModeState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit Rte_Write_CtApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockFaults_DeOutputElockHFaultSCG Rte_Write_CtApLAD_PpELockFaults_DeOutputElockHFaultSCG
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockHFaultSCG(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockFaults_DeOutputElockHFaultSCP Rte_Write_CtApLAD_PpELockFaults_DeOutputElockHFaultSCP
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockHFaultSCP(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit Rte_Write_CtApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockFaults_DeOutputElockLFaultSCG Rte_Write_CtApLAD_PpELockFaults_DeOutputElockLFaultSCG
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockLFaultSCG(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockFaults_DeOutputElockLFaultSCP Rte_Write_CtApLAD_PpELockFaults_DeOutputElockLFaultSCP
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockLFaultSCP(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpELockFaults_DeOutputElockOvercurrent Rte_Write_CtApLAD_PpELockFaults_DeOutputElockOvercurrent
#  define Rte_Write_CtApLAD_PpELockFaults_DeOutputElockOvercurrent(data) (Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions Rte_Write_CtApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions
#  define Rte_Write_CtApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(data) (Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue Rte_Write_CtApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue
#  define Rte_Write_CtApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(data) (Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue Rte_Write_CtApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue
#  define Rte_Write_CtApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(data) (Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalElockThresholdNoCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalElockThresholdNoCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockThresholdOvercurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalElockThresholdOvercurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockThresholdSCG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalElockThresholdSCG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockThresholdSCP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalElockThresholdSCP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_ElockActuatorTime1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalAfts_ElockActuatorTime1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalAfts_ElockActuatorTime2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalAfts_ElockActuatorTime2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockHOpenCircuitConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockHOpenCircuitConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockHOpenCircuitHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockHOpenCircuitHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockHSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockHSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockHSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockHSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockHSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockHSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockHSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockHSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLOpenCircuitConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLOpenCircuitConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLOpenCircuitHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLOpenCircuitHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLOvercurrentConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLOvercurrentConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLSCGConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLSCGConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLSCGHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLSCGHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLSCPConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLSCPConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockLSCPHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockLSCPHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOutputElockOvercurrentHealTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalOutputElockOvercurrentHealTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalEnableElockActuator() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLAD.CalEnableElockActuator) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpBatteryVoltMaxTest_DeBatteryVoltMaxTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpBatteryVoltMinTest_DeBatteryVoltMinTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpVehStopMaxTest_DeVehStopMaxTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorCounterOverCurrent;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterOpenCircuit;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterOpenCircuit;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterSCP;
extern VAR(IdtOutputELockSensor, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSensor_PreviousToLock;
extern VAR(IdtOutputELockSensor, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSensor_PreviousToUnlock;
extern VAR(IdtELockSetPoint, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSetPoint;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultOpenCircuit;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorPrefaultOverCurrent;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockNotCurrentMeasured;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorCounterOverCurrent() \
  (&Rte_CpApLAD_PimElockActuatorCounterOverCurrent)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorHCounterOpenCircuit() \
  (&Rte_CpApLAD_PimElockActuatorHCounterOpenCircuit)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorHCounterSCG() \
  (&Rte_CpApLAD_PimElockActuatorHCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorHCounterSCP() \
  (&Rte_CpApLAD_PimElockActuatorHCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorLCounterOpenCircuit() \
  (&Rte_CpApLAD_PimElockActuatorLCounterOpenCircuit)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorLCounterSCG() \
  (&Rte_CpApLAD_PimElockActuatorLCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorLCounterSCP() \
  (&Rte_CpApLAD_PimElockActuatorLCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockSensor_PreviousToLock() \
  (&Rte_CpApLAD_PimElockSensor_PreviousToLock)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockSensor_PreviousToUnlock() \
  (&Rte_CpApLAD_PimElockSensor_PreviousToUnlock)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockSetPoint() \
  (&Rte_CpApLAD_PimElockSetPoint)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() \
  (&Rte_CpApLAD_PimElockActuatorHPrefaultOpenCircuit)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorHPrefaultSCG() \
  (&Rte_CpApLAD_PimElockActuatorHPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorHPrefaultSCP() \
  (&Rte_CpApLAD_PimElockActuatorHPrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() \
  (&Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorLPrefaultSCG() \
  (&Rte_CpApLAD_PimElockActuatorLPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorLPrefaultSCP() \
  (&Rte_CpApLAD_PimElockActuatorLPrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockActuatorPrefaultOverCurrent() \
  (&Rte_CpApLAD_PimElockActuatorPrefaultOverCurrent)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimElockNotCurrentMeasured() \
  (&Rte_CpApLAD_PimElockNotCurrentMeasured)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApLAD_START_SEC_CODE
# include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CtApLAD_init CtApLAD_init
#  define RTE_RUNNABLE_RCtApLAD_task10ms RCtApLAD_task10ms
#  define RTE_RUNNABLE_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults
#  define RTE_RUNNABLE_RoutineServices_Actuator_test_on_recharging_plug_lock_Start RoutineServices_Actuator_test_on_recharging_plug_lock_Start
#  define RTE_RUNNABLE_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop RoutineServices_Actuator_test_on_recharging_plug_lock_Stop
# endif

FUNC(void, CtApLAD_CODE) CtApLAD_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLAD_CODE) RCtApLAD_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApLAD_STOP_SEC_CODE
# include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLAD_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
