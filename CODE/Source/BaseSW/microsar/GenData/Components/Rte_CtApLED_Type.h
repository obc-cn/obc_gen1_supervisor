/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLED_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApLED>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLED_TYPE_H
# define RTE_CTAPLED_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef APP_STATE_0
#   define APP_STATE_0 (0U)
#  endif

#  ifndef APP_STATE_1
#   define APP_STATE_1 (1U)
#  endif

#  ifndef APP_STATE_2
#   define APP_STATE_2 (2U)
#  endif

#  ifndef APP_STATE_3
#   define APP_STATE_3 (3U)
#  endif

#  ifndef APP_STATE_4
#   define APP_STATE_4 (4U)
#  endif

#  ifndef APP_STATE_5
#   define APP_STATE_5 (5U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLED_TYPE_H */
