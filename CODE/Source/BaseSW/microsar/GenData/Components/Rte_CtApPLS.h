/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApPLS.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApPLS>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPPLS_H
# define RTE_CTAPPLS_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApPLS_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault;
extern VAR(DCDC_InputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage;
extern VAR(DCDC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent;
extern VAR(OBC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent;
extern VAR(OBC_OutputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError;
extern VAR(IdtVoltageCorrectionOffset, RTE_VAR_INIT) Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset;
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed;
extern VAR(BMS_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status;
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
extern VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
extern VAR(DCLV_Input_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current;
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
extern VAR(DCLV_Measured_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
extern VAR(DCLV_Measured_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status;
extern VAR(PFC_IPH1_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1;
extern VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError (FALSE)
#  define Rte_InitValue_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError (FALSE)
#  define Rte_InitValue_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError (FALSE)
#  define Rte_InitValue_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error (0U)
#  define Rte_InitValue_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error (0U)
#  define Rte_InitValue_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error (0U)
#  define Rte_InitValue_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault (FALSE)
#  define Rte_InitValue_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault (FALSE)
#  define Rte_InitValue_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault (FALSE)
#  define Rte_InitValue_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault (FALSE)
#  define Rte_InitValue_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault (FALSE)
#  define Rte_InitValue_PpInt_DCDC_InputVoltage_DCDC_InputVoltage (0U)
#  define Rte_InitValue_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent (0U)
#  define Rte_InitValue_PpInt_OBC_OutputCurrent_OBC_OutputCurrent (0U)
#  define Rte_InitValue_PpInt_OBC_OutputVoltage_OBC_OutputVoltage (0U)
#  define Rte_InitValue_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error (0U)
#  define Rte_InitValue_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error (0U)
#  define Rte_InitValue_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error (0U)
#  define Rte_InitValue_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error (0U)
#  define Rte_InitValue_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error (0U)
#  define Rte_InitValue_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error (0U)
#  define Rte_InitValue_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error (0U)
#  define Rte_InitValue_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError (FALSE)
#  define Rte_InitValue_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error (0U)
#  define Rte_InitValue_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError (FALSE)
#  define Rte_InitValue_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError (FALSE)
#  define Rte_InitValue_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError (FALSE)
#  define Rte_InitValue_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset (0)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApPLS_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApPLS_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result Rte_Read_CtApPLS_PpDCDC_POST_Result_DeDCDC_POST_Result
#  define Rte_Read_CtApPLS_PpDCDC_POST_Result_DeDCDC_POST_Result(data) (*(data) = Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed Rte_Read_CtApPLS_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed
#  define Rte_Read_CtApPLS_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(data) (*(data) = Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_Voltage_BMS_Voltage Rte_Read_CtApPLS_PpInt_BMS_Voltage_BMS_Voltage
#  define Rte_Read_CtApPLS_PpInt_BMS_Voltage_BMS_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status Rte_Read_CtApPLS_PpInt_DCDC_Status_Delayed_DCDC_Status
#  define Rte_Read_CtApPLS_PpInt_DCDC_Status_Delayed_DCDC_Status(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Read_CtApPLS_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Read_CtApPLS_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApPLS_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApPLS_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus Rte_Read_CtApPLS_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus
#  define Rte_Read_CtApPLS_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus Rte_Read_CtApPLS_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus
#  define Rte_Read_CtApPLS_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current Rte_Read_CtApPLS_PpInt_DCLV_Input_Current_DCLV_Input_Current
#  define Rte_Read_CtApPLS_PpInt_DCLV_Input_Current_DCLV_Input_Current(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage Rte_Read_CtApPLS_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Read_CtApPLS_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current Rte_Read_CtApPLS_PpInt_DCLV_Measured_Current_DCLV_Measured_Current
#  define Rte_Read_CtApPLS_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage Rte_Read_CtApPLS_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage
#  define Rte_Read_CtApPLS_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApPLS_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApPLS_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status Rte_Read_CtApPLS_PpInt_OBC_Status_Delayed_OBC_Status
#  define Rte_Read_CtApPLS_PpInt_OBC_Status_Delayed_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 Rte_Read_CtApPLS_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1
#  define Rte_Read_CtApPLS_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V Rte_Read_CtApPLS_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V
#  define Rte_Read_CtApPLS_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result Rte_Read_CtApPLS_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Read_CtApPLS_PpOBC_POST_Result_DeOBC_POST_Result(data) (*(data) = Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempAC1Meas
#  define Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempAC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempAC2Meas
#  define Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempAC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempAC3Meas
#  define Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempAC3Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempACNMeas
#  define Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempACNMeas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempDC1Meas
#  define Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempDC1Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempDC2Meas
#  define Rte_Read_CtApPLS_PpOutputTempMeas_DeOutputTempDC2Meas(data) (*(data) = Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled Rte_Read_CtApPLS_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled
#  define Rte_Read_CtApPLS_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data) (*(data) = Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError Rte_Write_CtApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError
#  define Rte_Write_CtApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(data) (Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError Rte_Write_CtApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError
#  define Rte_Write_CtApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(data) (Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError Rte_Write_CtApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError
#  define Rte_Write_CtApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(data) (Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error Rte_Write_CtApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error
#  define Rte_Write_CtApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(data) (Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error Rte_Write_CtApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error
#  define Rte_Write_CtApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(data) (Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error Rte_Write_CtApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error
#  define Rte_Write_CtApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(data) (Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault
#  define Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(data) (Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault
#  define Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(data) (Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault
#  define Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(data) (Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault
#  define Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(data) (Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault
#  define Rte_Write_CtApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(data) (Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage Rte_Write_CtApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage
#  define Rte_Write_CtApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(data) (Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent Rte_Write_CtApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent
#  define Rte_Write_CtApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(data) (Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent Rte_Write_CtApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent
#  define Rte_Write_CtApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(data) (Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage Rte_Write_CtApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage
#  define Rte_Write_CtApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(data) (Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error Rte_Write_CtApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error
#  define Rte_Write_CtApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(data) (Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error Rte_Write_CtApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error
#  define Rte_Write_CtApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(data) (Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error Rte_Write_CtApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error
#  define Rte_Write_CtApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(data) (Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error Rte_Write_CtApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error
#  define Rte_Write_CtApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(data) (Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error Rte_Write_CtApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error
#  define Rte_Write_CtApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(data) (Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error Rte_Write_CtApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error
#  define Rte_Write_CtApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(data) (Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error Rte_Write_CtApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error
#  define Rte_Write_CtApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(data) (Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError Rte_Write_CtApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError
#  define Rte_Write_CtApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(data) (Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error Rte_Write_CtApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error
#  define Rte_Write_CtApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(data) (Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError Rte_Write_CtApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError
#  define Rte_Write_CtApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(data) (Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError Rte_Write_CtApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError
#  define Rte_Write_CtApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(data) (Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError Rte_Write_CtApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError
#  define Rte_Write_CtApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(data) (Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset Rte_Write_CtApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset
#  define Rte_Write_CtApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(data) (Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_WDGM_OSAPPLICATION_ASILB_APPL_CODE) WdgM_CheckpointReached(WdgM_SupervisedEntityIdType parg0, WdgM_CheckpointIdType CPID); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(arg1) (WdgM_CheckpointReached((WdgM_SupervisedEntityIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalACTempTripTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalACTempTripTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalACTempWarningTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalACTempWarningTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCTempTripTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalDCTempTripTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCTempWarningTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalDCTempWarningTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalHVVTripTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalHVVTripTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalHVVWarningTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalHVVWarningTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLVPTripTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalLVPTripTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLVVTripTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalLVVTripTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLVVWarningTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalLVVWarningTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCMaxEfficiency() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBCMaxEfficiency) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCOffsetAllowed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBCOffsetAllowed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCPTripTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBCPTripTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOffsetAllowed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOffsetAllowed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalACTempTripThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalACTempTripThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalACTempWarningThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalACTempWarningThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCTempTripThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalDCTempTripThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCTempWarningThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalDCTempWarningThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalHVVTripThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalHVVTripThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalHVVWarningThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalHVVWarningThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLVVTripThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalLVVTripThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalLVVWarningThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalLVVWarningThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxEfficiency() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalMaxEfficiency) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinEfficiency() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalMinEfficiency) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBCMinEfficiency() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBCMinEfficiency) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_DelayEfficiencyDCLV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBC_DelayEfficiencyDCLV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_DelayEfficiencyHVMono() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBC_DelayEfficiencyHVMono) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalOBC_DelayEfficiencyHVTri() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLS.CalOBC_DelayEfficiencyHVTri) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApPLS_START_SEC_CODE
# include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApPLS_init RCtApPLS_init
#  define RTE_RUNNABLE_RCtApPLS_task10ms RCtApPLS_task10ms
# endif

FUNC(void, CtApPLS_CODE) RCtApPLS_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApPLS_CODE) RCtApPLS_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApPLS_STOP_SEC_CODE
# include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_WdgM_AliveSupervision_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPPLS_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
