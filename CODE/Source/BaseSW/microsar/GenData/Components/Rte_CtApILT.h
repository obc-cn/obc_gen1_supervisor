/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApILT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApILT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPILT_H
# define RTE_CTAPILT_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApILT_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(BSI_ChargeState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState;
extern VAR(BSI_ChargeTypeStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_Fault, RTE_VAR_INIT) Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault;
extern VAR(VCU_ElecMeterSaturation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation;
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
extern VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(OBC_PushChargeType, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType;
extern VAR(OBC_RechargeHMIState, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeInProgress;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeEndOfCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeGuideManagement;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeProgrammingCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockStateError_DeLockStateError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPSH_PpOutputChargePushFil_DeOutputChargePushFil;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState;
extern VAR(IdtRECHARGE_HMI_STATE, RTE_VAR_INIT) Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpInt_OBC_PushChargeType_OBC_PushChargeType (FALSE)
#  define Rte_InitValue_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState (0U)
#  define Rte_InitValue_PpLedModes_DeChargeError (FALSE)
#  define Rte_InitValue_PpLedModes_DeChargeInProgress (FALSE)
#  define Rte_InitValue_PpLedModes_DeEndOfCharge (FALSE)
#  define Rte_InitValue_PpLedModes_DeGuideManagement (FALSE)
#  define Rte_InitValue_PpLedModes_DeProgrammingCharge (FALSE)
#  define Rte_InitValue_PpLockLED2BEPR_DeLockLED2BEPR (FALSE)
#  define Rte_InitValue_PpLockStateError_DeLockStateError (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState Rte_Read_CtApILT_PpInt_BSI_ChargeState_BSI_ChargeState
#  define Rte_Read_CtApILT_PpInt_BSI_ChargeState_BSI_ChargeState(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus Rte_Read_CtApILT_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus
#  define Rte_Read_CtApILT_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApILT_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApILT_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Fault_OBC_Fault Rte_Read_CtApILT_PpInt_OBC_Fault_OBC_Fault
#  define Rte_Read_CtApILT_PpInt_OBC_Fault_OBC_Fault(data) (*(data) = Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation Rte_Read_CtApILT_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation
#  define Rte_Read_CtApILT_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Read_CtApILT_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Read_CtApILT_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_PlantMode_DeChargeError Rte_Read_CtApILT_PpLedModes_PlantMode_DeChargeError
#  define Rte_Read_CtApILT_PpLedModes_PlantMode_DeChargeError(data) (*(data) = Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_PlantMode_DeChargeInProgress Rte_Read_CtApILT_PpLedModes_PlantMode_DeChargeInProgress
#  define Rte_Read_CtApILT_PpLedModes_PlantMode_DeChargeInProgress(data) (*(data) = Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_PlantMode_DeEndOfCharge Rte_Read_CtApILT_PpLedModes_PlantMode_DeEndOfCharge
#  define Rte_Read_CtApILT_PpLedModes_PlantMode_DeEndOfCharge(data) (*(data) = Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_PlantMode_DeGuideManagement Rte_Read_CtApILT_PpLedModes_PlantMode_DeGuideManagement
#  define Rte_Read_CtApILT_PpLedModes_PlantMode_DeGuideManagement(data) (*(data) = Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge Rte_Read_CtApILT_PpLedModes_PlantMode_DeProgrammingCharge
#  define Rte_Read_CtApILT_PpLedModes_PlantMode_DeProgrammingCharge(data) (*(data) = Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode Rte_Read_CtApILT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode
#  define Rte_Read_CtApILT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(data) (*(data) = Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil Rte_Read_CtApILT_PpOutputChargePushFil_DeOutputChargePushFil
#  define Rte_Read_CtApILT_PpOutputChargePushFil_DeOutputChargePushFil(data) (*(data) = Rte_CpApPSH_PpOutputChargePushFil_DeOutputChargePushFil, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputELockSensor_DeOutputELockSensor Rte_Read_CtApILT_PpOutputELockSensor_DeOutputELockSensor
#  define Rte_Read_CtApILT_PpOutputELockSensor_DeOutputELockSensor(data) (*(data) = Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeState_DePlantModeState Rte_Read_CtApILT_PpPlantModeState_DePlantModeState
#  define Rte_Read_CtApILT_PpPlantModeState_DePlantModeState(data) (*(data) = Rte_CpApPLT_PpPlantModeState_DePlantModeState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode Rte_Read_CtApILT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode
#  define Rte_Read_CtApILT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(data) (*(data) = Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType Rte_Write_CtApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType
#  define Rte_Write_CtApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType(data) (Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState Rte_Write_CtApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState
#  define Rte_Write_CtApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data) (Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_DeChargeError Rte_Write_CtApILT_PpLedModes_DeChargeError
#  define Rte_Write_CtApILT_PpLedModes_DeChargeError(data) (Rte_CpApILT_PpLedModes_DeChargeError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_DeChargeInProgress Rte_Write_CtApILT_PpLedModes_DeChargeInProgress
#  define Rte_Write_CtApILT_PpLedModes_DeChargeInProgress(data) (Rte_CpApILT_PpLedModes_DeChargeInProgress = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_DeEndOfCharge Rte_Write_CtApILT_PpLedModes_DeEndOfCharge
#  define Rte_Write_CtApILT_PpLedModes_DeEndOfCharge(data) (Rte_CpApILT_PpLedModes_DeEndOfCharge = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_DeGuideManagement Rte_Write_CtApILT_PpLedModes_DeGuideManagement
#  define Rte_Write_CtApILT_PpLedModes_DeGuideManagement(data) (Rte_CpApILT_PpLedModes_DeGuideManagement = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_DeProgrammingCharge Rte_Write_CtApILT_PpLedModes_DeProgrammingCharge
#  define Rte_Write_CtApILT_PpLedModes_DeProgrammingCharge(data) (Rte_CpApILT_PpLedModes_DeProgrammingCharge = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR Rte_Write_CtApILT_PpLockLED2BEPR_DeLockLED2BEPR
#  define Rte_Write_CtApILT_PpLockLED2BEPR_DeLockLED2BEPR(data) (Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLockStateError_DeLockStateError Rte_Write_CtApILT_PpLockStateError_DeLockStateError
#  define Rte_Write_CtApILT_PpLockStateError_DeLockStateError(data) (Rte_CpApILT_PpLockStateError_DeLockStateError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalMaxTimeChargeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalMaxTimeChargeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxTimeChargeInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalMaxTimeChargeInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxTimeEndOfCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalMaxTimeEndOfCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxTimeGuideManagement() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalMaxTimeGuideManagement) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxTimeProgrammingCharge() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalMaxTimeProgrammingCharge) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimePushButtonKeepValue() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalTimePushButtonKeepValue) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceStateFailure() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalDebounceStateFailure) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceStateFinished() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalDebounceStateFinished) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceStateInProgress() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalDebounceStateInProgress) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceStateStopped() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalDebounceStateStopped) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalElockTimeError() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApILT.CalElockTimeError) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApILT_START_SEC_CODE
# include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApILT_init RCtApILT_init
#  define RTE_RUNNABLE_RCtApILT_task100msA RCtApILT_task100msA
#  define RTE_RUNNABLE_RCtApILT_task100msB RCtApILT_task100msB
#  define RTE_RUNNABLE_RCtApILT_task10ms RCtApILT_task10ms
# endif

FUNC(void, CtApILT_CODE) RCtApILT_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApILT_CODE) RCtApILT_task100msA(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApILT_CODE) RCtApILT_task100msB(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApILT_CODE) RCtApILT_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApILT_STOP_SEC_CODE
# include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPILT_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
