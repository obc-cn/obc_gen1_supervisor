/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DemMaster_0.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <DemMaster_0>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_DEMMASTER_0_H
# define RTE_DEMMASTER_0_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DemMaster_0_Type.h"
# include "Rte_DataHandleType.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter(P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter
#  define Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter Rte_Call_DemMaster_0_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D407_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D407_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D407_ReadData CBReadData_DemDataClass_D407_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D40C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D40C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D40C_ReadData CBReadData_DemDataClass_D40C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D49C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D49C_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D49C_ReadData CBReadData_DemDataClass_D49C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D4CA_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D4CA_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D4CA_ReadData CBReadData_DemDataClass_D4CA_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D5CF_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D5CF_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D5CF_ReadData CBReadData_DemDataClass_D5CF_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D5D1_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D5D1_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D5D1_ReadData CBReadData_DemDataClass_D5D1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D805_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D805_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D805_ReadData CBReadData_DemDataClass_D805_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D806_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D806_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D806_ReadData CBReadData_DemDataClass_D806_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D807_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D807_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D807_ReadData CBReadData_DemDataClass_D807_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D808_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D808_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D808_ReadData CBReadData_DemDataClass_D808_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D809_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D809_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D809_ReadData CBReadData_DemDataClass_D809_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D80C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D80C_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D80C_ReadData CBReadData_DemDataClass_D80C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D822_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D822_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D822_ReadData CBReadData_DemDataClass_D822_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D824_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D824_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D824_ReadData CBReadData_DemDataClass_D824_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D825_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D825_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D825_ReadData CBReadData_DemDataClass_D825_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D827_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D827_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D827_ReadData CBReadData_DemDataClass_D827_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D828_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D828_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D828_ReadData CBReadData_DemDataClass_D828_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D829_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D829_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D829_ReadData CBReadData_DemDataClass_D829_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82B_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D82B_ReadData CBReadData_DemDataClass_D82B_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D82C_ReadData CBReadData_DemDataClass_D82C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D82D_ReadData CBReadData_DemDataClass_D82D_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D82E_ReadData CBReadData_DemDataClass_D82E_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D82F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D82F_ReadData CBReadData_DemDataClass_D82F_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D831_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D831_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D831_ReadData CBReadData_DemDataClass_D831_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83B_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D83B_ReadData CBReadData_DemDataClass_D83B_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D83C_ReadData CBReadData_DemDataClass_D83C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D83D_ReadData CBReadData_DemDataClass_D83D_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D83E_ReadData CBReadData_DemDataClass_D83E_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D83F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D83F_ReadData CBReadData_DemDataClass_D83F_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D840_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D840_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D840_ReadData CBReadData_DemDataClass_D840_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D843_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D843_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D843_ReadData CBReadData_DemDataClass_D843_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D844_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D844_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D844_ReadData CBReadData_DemDataClass_D844_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D845_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D845_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D845_ReadData CBReadData_DemDataClass_D845_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D846_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D846_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D846_ReadData CBReadData_DemDataClass_D846_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84A_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84A_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D84A_ReadData CBReadData_DemDataClass_D84A_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84B_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D84B_ReadData CBReadData_DemDataClass_D84B_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D84C_ReadData CBReadData_DemDataClass_D84C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D84D_ReadData CBReadData_DemDataClass_D84D_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D84E_ReadData CBReadData_DemDataClass_D84E_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D84F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D84F_ReadData CBReadData_DemDataClass_D84F_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D850_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D850_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D850_ReadData CBReadData_DemDataClass_D850_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D851_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D851_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D851_ReadData CBReadData_DemDataClass_D851_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D852_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D852_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D852_ReadData CBReadData_DemDataClass_D852_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D853_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D853_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D853_ReadData CBReadData_DemDataClass_D853_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D854_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D854_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D854_ReadData CBReadData_DemDataClass_D854_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D855_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D855_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D855_ReadData CBReadData_DemDataClass_D855_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D8E9_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D8E9_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D8E9_ReadData CBReadData_DemDataClass_D8E9_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D8EB_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_D8EB_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_D8EB_ReadData CBReadData_DemDataClass_D8EB_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE60_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE60_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE60_ReadData RCBReadData_DemDataClass_FE60_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE61_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE61_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE61_ReadData RCBReadData_DemDataClass_FE61_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE62_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE62_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE62_ReadData RCBReadData_DemDataClass_FE62_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE63_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE63_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE63_ReadData RCBReadData_DemDataClass_FE63_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE64_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE64_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE64_ReadData RCBReadData_DemDataClass_FE64_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE65_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE65_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE65_ReadData RCBReadData_DemDataClass_FE65_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE66_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE66_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE66_ReadData RCBReadData_DemDataClass_FE66_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE67_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE67_ReadData(P2VAR(DataArrayType_uint8_4, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE67_ReadData RCBReadData_DemDataClass_FE67_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE68_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE68_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE68_ReadData RCBReadData_DemDataClass_FE68_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE69_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE69_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE69_ReadData RCBReadData_DemDataClass_FE69_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6A_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6A_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE6A_ReadData RCBReadData_DemDataClass_FE6A_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6B_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6B_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE6B_ReadData RCBReadData_DemDataClass_FE6B_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6C_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6C_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE6C_ReadData RCBReadData_DemDataClass_FE6C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6D_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6D_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE6D_ReadData RCBReadData_DemDataClass_FE6D_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6E_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6E_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE6E_ReadData RCBReadData_DemDataClass_FE6E_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6F_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE6F_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE6F_ReadData RCBReadData_DemDataClass_FE6F_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE70_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RCBReadData_DemDataClass_FE70_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE70_ReadData RCBReadData_DemDataClass_FE70_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_FE71_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_FE71_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE71_ReadData CBReadData_DemDataClass_FE71_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_FE72_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_FE72_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE72_ReadData CBReadData_DemDataClass_FE72_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_FE73_ReadData(P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) CBReadData_DemDataClass_FE73_ReadData(P2VAR(DataArrayType_uint8_2, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_CBReadData_DemDataClass_FE73_ReadData CBReadData_DemDataClass_FE73_ReadData


# endif /* !defined(RTE_CORE) */


# define DemMaster_0_START_SEC_CODE
# include "DemMaster_0_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_ClearDTC Dem_ClearDTC
#  define RTE_RUNNABLE_Dem_MasterMainFunction Dem_MasterMainFunction
#  define RTE_RUNNABLE_GetDTCOfEvent Dem_GetDTCOfEvent
#  define RTE_RUNNABLE_GetDTCStatusAvailabilityMask Dem_GetDTCStatusAvailabilityMask
#  define RTE_RUNNABLE_GetDebouncingOfEvent Dem_GetDebouncingOfEvent
#  define RTE_RUNNABLE_GetEventEnableCondition Dem_GetEventEnableCondition
#  define RTE_RUNNABLE_GetEventExtendedDataRecordEx Dem_GetEventExtendedDataRecordEx
#  define RTE_RUNNABLE_GetEventFailed Dem_GetEventFailed
#  define RTE_RUNNABLE_GetEventFreezeFrameDataEx Dem_GetEventFreezeFrameDataEx
#  define RTE_RUNNABLE_GetEventMemoryOverflow Dem_GetEventMemoryOverflow
#  define RTE_RUNNABLE_GetEventStatus Dem_GetEventUdsStatus
#  define RTE_RUNNABLE_GetEventTested Dem_GetEventTested
#  define RTE_RUNNABLE_GetEventUdsStatus Dem_GetEventUdsStatus
#  define RTE_RUNNABLE_GetFaultDetectionCounter Dem_GetFaultDetectionCounter
#  define RTE_RUNNABLE_GetMonitorStatus Dem_GetMonitorStatus
#  define RTE_RUNNABLE_GetNumberOfEventMemoryEntries Dem_GetNumberOfEventMemoryEntries
#  define RTE_RUNNABLE_GetOperationCycleState Dem_GetOperationCycleState
#  define RTE_RUNNABLE_PostRunRequested Dem_PostRunRequested
#  define RTE_RUNNABLE_SelectDTC Dem_SelectDTC
#  define RTE_RUNNABLE_SetEnableCondition Dem_SetEnableCondition
#  define RTE_RUNNABLE_SetOperationCycleState Dem_SetOperationCycleState
# endif

FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_ClearDTC(uint8 parg0); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, DemMaster_0_CODE) Dem_MasterMainFunction(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetDTCOfEvent(Dem_EventIdType EventId, Dem_DTCFormatType DTCFormat, P2VAR(uint32, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DTCOfEvent); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetDTCStatusAvailabilityMask(uint8 ClientId, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DTCStatusMask); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetDebouncingOfEvent(Dem_EventIdType EventId, P2VAR(Dem_DebouncingStateType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DebouncingState); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventEnableCondition(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) ConditionFullfilled); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventExtendedDataRecordEx(Dem_EventIdType EventId, uint8 RecordNumber, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventExtendedDataRecordEx(Dem_EventIdType EventId, uint8 RecordNumber, P2VAR(Dem_MaxDataValueType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventFailed(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) EventFailed); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventFreezeFrameDataEx(Dem_EventIdType EventId, uint8 RecordNumber, uint16 DataId, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventFreezeFrameDataEx(Dem_EventIdType EventId, uint8 RecordNumber, uint16 DataId, P2VAR(Dem_MaxDataValueType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) DestBuffer, P2VAR(uint16, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) BufSize); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventMemoryOverflow(uint8 parg0, Dem_DTCOriginType parg1, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) OverflowIndication); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventUdsStatus(Dem_EventIdType EventId, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) UDSStatusByte); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventTested(Dem_EventIdType EventId, P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) EventTested); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetEventUdsStatus(Dem_EventIdType EventId, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) UDSStatusByte); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetFaultDetectionCounter(Dem_EventIdType EventId, P2VAR(sint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) FaultDetectionCounter); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetMonitorStatus(Dem_EventIdType EventId, P2VAR(Dem_MonitorStatusType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) MonitorStatus); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetNumberOfEventMemoryEntries(uint8 parg0, Dem_DTCOriginType parg1, P2VAR(uint8, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) NumberOfEventMemoryEntries); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_GetOperationCycleState(uint8 parg0, P2VAR(Dem_OperationCycleStateType, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) CycleState); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_PostRunRequested(P2VAR(boolean, AUTOMATIC, RTE_DEMMASTER_0_APPL_VAR) IsRequested); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_SelectDTC(uint8 parg0, uint32 DTC, Dem_DTCFormatType DTCFormat, Dem_DTCOriginType DTCOrigin); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_SetEnableCondition(uint8 parg0, boolean ConditionFulfilled); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, DemMaster_0_CODE) Dem_SetOperationCycleState(uint8 parg0, Dem_OperationCycleStateType CycleState); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define DemMaster_0_STOP_SEC_CODE
# include "DemMaster_0_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CSDataServices_DemDataClass_D407_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D40C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D49C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D4CA_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D5CF_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D5D1_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D805_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D806_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D807_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D808_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D809_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D80C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D822_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D824_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D825_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D827_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D828_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D829_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D82F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D831_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D83F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D840_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D843_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D844_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D845_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D846_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84A_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D84F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D850_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D851_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D852_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D853_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D854_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D855_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D8E9_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_D8EB_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE60_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE61_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE62_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE63_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE64_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE65_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE66_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE67_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE68_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE69_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6A_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6B_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6C_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6D_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6E_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE6F_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE70_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE71_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE72_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_DemDataClass_FE73_E_NOT_OK (1U)

#  define RTE_E_CallbackGetFaultDetectCounter_E_NOT_OK (1U)

#  define RTE_E_ClearDTC_DEM_CLEAR_BUSY (5U)

#  define RTE_E_ClearDTC_DEM_CLEAR_FAILED (7U)

#  define RTE_E_ClearDTC_DEM_CLEAR_MEMORY_ERROR (6U)

#  define RTE_E_ClearDTC_DEM_PENDING (4U)

#  define RTE_E_ClearDTC_DEM_WRONG_DTC (8U)

#  define RTE_E_ClearDTC_DEM_WRONG_DTCORIGIN (9U)

#  define RTE_E_ClearDTC_E_NOT_OK (1U)

#  define RTE_E_ClearDTC_E_OK (0U)

#  define RTE_E_DemServices_E_NOT_OK (1U)

#  define RTE_E_EnableCondition_E_NOT_OK (1U)

#  define RTE_E_EvMemOverflowIndication_E_NOT_OK (1U)

#  define RTE_E_GeneralDiagnosticInfo_DEM_BUFFER_TOO_SMALL (21U)

#  define RTE_E_GeneralDiagnosticInfo_DEM_E_NO_DTC_AVAILABLE (10U)

#  define RTE_E_GeneralDiagnosticInfo_DEM_E_NO_FDC_AVAILABLE (14U)

#  define RTE_E_GeneralDiagnosticInfo_DEM_NO_SUCH_ELEMENT (48U)

#  define RTE_E_GeneralDiagnosticInfo_E_NOT_OK (1U)

#  define RTE_E_OperationCycle_E_NOT_OK (1U)

#  define RTE_E_OperationCycle_E_OK (0U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_DEMMASTER_0_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_1330:  MISRA rule: Rule8.3
     Reason:     The RTE Generator uses default names for parameter identifiers of port defined arguments of service modules.
                 Therefore the parameter identifiers in the function declaration differs from those of the implementation of the BSW module.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
