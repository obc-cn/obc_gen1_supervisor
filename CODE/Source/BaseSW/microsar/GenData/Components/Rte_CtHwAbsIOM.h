/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtHwAbsIOM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtHwAbsIOM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTHWABSIOM_H
# define RTE_CTHWABSIOM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtHwAbsIOM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpExtRCDLineRaw_DeExtRCDLineRaw (FALSE)
#  define Rte_InitValue_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue (FALSE)
#  define Rte_InitValue_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue (FALSE)
#  define Rte_InitValue_PpInputChargePushRaw_DeInputChargePushRaw (FALSE)
#  define Rte_InitValue_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue (FALSE)
#  define Rte_InitValue_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue (FALSE)
#  define Rte_InitValue_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue Rte_Read_CtHwAbsIOM_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue(data) (*(data) = Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue Rte_Read_CtHwAbsIOM_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(data) (*(data) = Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV Rte_Read_CtHwAbsIOM_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV
#  define Rte_Read_CtHwAbsIOM_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(data) (*(data) = Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC Rte_Read_CtHwAbsIOM_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC
#  define Rte_Read_CtHwAbsIOM_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(data) (*(data) = Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(data) (*(data) = Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(data) (*(data) = Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(data) (*(data) = Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(data) (*(data) = Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(data) (*(data) = Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(data) (*(data) = Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(data) (*(data) = Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue Rte_Read_CtHwAbsIOM_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(data) (*(data) = Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled Rte_Read_CtHwAbsIOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled
#  define Rte_Read_CtHwAbsIOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data) (*(data) = Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue Rte_Read_CtHwAbsIOM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(data) (*(data) = Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue Rte_Read_CtHwAbsIOM_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue
#  define Rte_Read_CtHwAbsIOM_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(data) (*(data) = Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpExtRCDLineRaw_DeExtRCDLineRaw Rte_Write_CtHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw
#  define Rte_Write_CtHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw(data) (Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue Rte_Write_CtHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue
#  define Rte_Write_CtHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(data) (Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue Rte_Write_CtHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue
#  define Rte_Write_CtHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(data) (Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInputChargePushRaw_DeInputChargePushRaw Rte_Write_CtHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw
#  define Rte_Write_CtHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw(data) (Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue Rte_Write_CtHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue
#  define Rte_Write_CtHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(data) (Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue Rte_Write_CtHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue
#  define Rte_Write_CtHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(data) (Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue Rte_Write_CtHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue
#  define Rte_Write_CtHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(data) (Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_WDGM_OSAPPLICATION_ASILB_APPL_CODE) WdgM_CheckpointReached(WdgM_SupervisedEntityIdType parg0, WdgM_CheckpointIdType CPID); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(arg1) (WdgM_CheckpointReached((WdgM_SupervisedEntityIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_WDGM_OSAPPLICATION_ASILB_APPL_CODE) WdgM_ActivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_general_Core0_ActivateSupervisionEntity WdgM_ActivateSupervisionEntity
#  define RTE_START_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_WDGM_OSAPPLICATION_ASILB_APPL_CODE) WdgM_DeactivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_general_Core0_DeactivateSupervisionEntity WdgM_DeactivateSupervisionEntity


# endif /* !defined(RTE_CORE) */


# define CtHwAbsIOM_START_SEC_CODE
# include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtHwAbsIOM_init RCtHwAbsIOM_init
#  define RTE_RUNNABLE_RCtHwAbsIOM_inputTask100ms RCtHwAbsIOM_inputTask100ms
#  define RTE_RUNNABLE_RCtHwAbsIOM_inputTask10ms RCtHwAbsIOM_inputTask10ms
#  define RTE_RUNNABLE_RCtHwAbsIOM_outputTask10ms RCtHwAbsIOM_outputTask10ms
#  define RTE_RUNNABLE_RPpSetDebugPinValue_OpSetDebugPinValue RPpSetDebugPinValue_OpSetDebugPinValue
# endif

FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_inputTask100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_inputTask10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_outputTask10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsIOM_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtHwAbsIOM_STOP_SEC_CODE
# include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_WdgM_AliveSupervision_E_NOT_OK (1U)

#  define RTE_E_WdgM_General_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTHWABSIOM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
