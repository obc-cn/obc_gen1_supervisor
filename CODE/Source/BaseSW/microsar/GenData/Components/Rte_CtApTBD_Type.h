/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApTBD_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApTBD>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPTBD_TYPE_H
# define RTE_CTAPTBD_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_invalid_value
#   define Cx0_invalid_value (0U)
#  endif

#  ifndef Cx1_CC2_invalid_not_connected
#   define Cx1_CC2_invalid_not_connected (1U)
#  endif

#  ifndef Cx2_CC2_valid_full_connected
#   define Cx2_CC2_valid_full_connected (2U)
#  endif

#  ifndef Cx3_CC2_invalid_half_connected
#   define Cx3_CC2_invalid_half_connected (3U)
#  endif

#  ifndef Cx0_not_in_charging
#   define Cx0_not_in_charging (0U)
#  endif

#  ifndef Cx1_charging
#   define Cx1_charging (1U)
#  endif

#  ifndef Cx2_charging_fault
#   define Cx2_charging_fault (2U)
#  endif

#  ifndef Cx3_charging_finished
#   define Cx3_charging_finished (3U)
#  endif

#  ifndef Cx0_Key_OFF
#   define Cx0_Key_OFF (0U)
#  endif

#  ifndef Cx1_Key_ACC
#   define Cx1_Key_ACC (1U)
#  endif

#  ifndef Cx2_Key_ON
#   define Cx2_Key_ON (2U)
#  endif

#  ifndef Cx3_Key_START
#   define Cx3_Key_START (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPTBD_TYPE_H */
