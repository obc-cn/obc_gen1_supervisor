/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApCPT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApCPT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPCPT_H
# define RTE_CTAPCPT_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApCPT_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(OBC_CP_connection_Status, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
extern VAR(IdtMaxInputACCurrentEVSE, RTE_VAR_INIT) Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE;
extern VAR(IdtDutyControlPilot, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status (1U)
#  define Rte_InitValue_PpInt_OBC_ChargingMode_OBC_ChargingMode (1U)
#  define Rte_InitValue_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection (FALSE)
#  define Rte_InitValue_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE (0U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpDutyControlPilot_DeDutyControlPilot Rte_Read_CtApCPT_PpDutyControlPilot_DeDutyControlPilot
#  define Rte_Read_CtApCPT_PpDutyControlPilot_DeDutyControlPilot(data) (*(data) = Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization Rte_Read_CtApCPT_PpShutdownAuthorization_DeShutdownAuthorization
#  define Rte_Read_CtApCPT_PpShutdownAuthorization_DeShutdownAuthorization(data) (*(data) = Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status Rte_Write_CtApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status
#  define Rte_Write_CtApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(data) (Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Write_CtApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Write_CtApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection Rte_Write_CtApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection
#  define Rte_Write_CtApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data) (Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE Rte_Write_CtApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE
#  define Rte_Write_CtApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(data) (Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_ReadBlock(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_ReadBlock(arg1) (NvM_ReadBlock((NvM_BlockIdType)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_SetRamBlockStatus(NvM_BlockIdType parg0, boolean RamBlockStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_SetRamBlockStatus(arg1) (NvM_SetRamBlockStatus((NvM_BlockIdType)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)44, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalDebounceControlPilot() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApCPT.CalDebounceControlPilot) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApCPT_PimCPT_NvMRamMirror;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCPT_NvMRamMirror() \
  (&Rte_CpApCPT_PimCPT_NvMRamMirror)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApCPT_START_SEC_CODE
# include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApCPT_init RCtApCPT_init
#  define RTE_RUNNABLE_RCtApCPT_task10ms RCtApCPT_task10ms
# endif

FUNC(void, CtApCPT_CODE) RCtApCPT_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApCPT_CODE) RCtApCPT_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApCPT_STOP_SEC_CODE
# include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_NvMService_AC3_SRBS_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPCPT_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
