/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApCPT_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApCPT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPCPT_TYPE_H
# define RTE_CTAPCPT_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

#  ifndef Cx0_Invalid_value
#   define Cx0_Invalid_value (0U)
#  endif

#  ifndef Cx1_CP_invalid_not_connected
#   define Cx1_CP_invalid_not_connected (1U)
#  endif

#  ifndef Cx2_CP_valid_full_connected
#   define Cx2_CP_valid_full_connected (2U)
#  endif

#  ifndef Cx3_CP_invalid_half_connected
#   define Cx3_CP_invalid_half_connected (3U)
#  endif

#  ifndef Cx0_no_charging
#   define Cx0_no_charging (0U)
#  endif

#  ifndef Cx1_slow_charging
#   define Cx1_slow_charging (1U)
#  endif

#  ifndef Cx2_China_fast_charging
#   define Cx2_China_fast_charging (2U)
#  endif

#  ifndef Cx3_Euro_fast_charging
#   define Cx3_Euro_fast_charging (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPCPT_TYPE_H */
