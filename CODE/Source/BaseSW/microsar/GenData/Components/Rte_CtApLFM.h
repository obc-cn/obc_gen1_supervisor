/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLFM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApLFM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLFM_H
# define RTE_CTAPLFM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApLFM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCLVError_DeDCLVError;
extern VAR(DCDC_Fault, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault;
extern VAR(DCDC_OutputVoltage, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage;
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue;
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(BSI_MainWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup;
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference;
extern VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status;
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCHV_Command_Current_Reference, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
extern VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
extern VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
extern VAR(DCLV_Measured_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
extern VAR(DCLV_OC_A_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT;
extern VAR(DCLV_OC_A_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT;
extern VAR(DCLV_OC_B_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT;
extern VAR(DCLV_OC_B_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT;
extern VAR(DCLV_OT_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT;
extern VAR(DCLV_OV_INT_A_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT;
extern VAR(DCLV_OV_INT_B_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT;
extern VAR(DCLV_OV_UV_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT;
extern VAR(DCLV_PWM_STOP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status;
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpDCDCFaultsList_DeDCDC_OvertemperatureFault (FALSE)
#  define Rte_InitValue_PpDCDCFaultsList_DeDCLV_HWErrors_Fault (FALSE)
#  define Rte_InitValue_PpDCDCFaultsList_DeDCLV_InternalCom_Fault (FALSE)
#  define Rte_InitValue_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError (FALSE)
#  define Rte_InitValue_PpDCLVError_DeDCLVError (FALSE)
#  define Rte_InitValue_PpInt_DCDC_Fault_DCDC_Fault (0U)
#  define Rte_InitValue_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage (0U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApLFM_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApLFM_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result Rte_Read_CtApLFM_PpDCDC_POST_Result_DeDCDC_POST_Result
#  define Rte_Read_CtApLFM_PpDCDC_POST_Result_DeDCDC_POST_Result(data) (*(data) = Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error Rte_Read_CtApLFM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error
#  define Rte_Read_CtApLFM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(data) (*(data) = Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error Rte_Read_CtApLFM_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error
#  define Rte_Read_CtApLFM_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(data) (*(data) = Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error Rte_Read_CtApLFM_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error
#  define Rte_Read_CtApLFM_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(data) (*(data) = Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error Rte_Read_CtApLFM_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error
#  define Rte_Read_CtApLFM_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(data) (*(data) = Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue Rte_Read_CtApLFM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue
#  define Rte_Read_CtApLFM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Read_CtApLFM_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApLFM_PpInt_ABS_VehSpd_ABS_VehSpd(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup Rte_Read_CtApLFM_PpInt_BSI_MainWakeup_BSI_MainWakeup
#  define Rte_Read_CtApLFM_PpInt_BSI_MainWakeup_BSI_MainWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference Rte_Read_CtApLFM_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference
#  define Rte_Read_CtApLFM_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(data) (*(data) = Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed Rte_Read_CtApLFM_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed
#  define Rte_Read_CtApLFM_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status Rte_Read_CtApLFM_PpInt_DCDC_Status_Delayed_DCDC_Status
#  define Rte_Read_CtApLFM_PpInt_DCDC_Status_Delayed_DCDC_Status(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Read_CtApLFM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Read_CtApLFM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApLFM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApLFM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference Rte_Read_CtApLFM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference
#  define Rte_Read_CtApLFM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus Rte_Read_CtApLFM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus
#  define Rte_Read_CtApLFM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus Rte_Read_CtApLFM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus
#  define Rte_Read_CtApLFM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage Rte_Read_CtApLFM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Read_CtApLFM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current Rte_Read_CtApLFM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current
#  define Rte_Read_CtApLFM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT Rte_Read_CtApLFM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT
#  define Rte_Read_CtApLFM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP Rte_Read_CtApLFM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP
#  define Rte_Read_CtApLFM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApLFM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApLFM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status Rte_Read_CtApLFM_PpInt_OBC_Status_Delayed_OBC_Status
#  define Rte_Read_CtApLFM_PpInt_OBC_Status_Delayed_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result Rte_Read_CtApLFM_PpOBC_POST_Result_DeOBC_POST_Result
#  define Rte_Read_CtApLFM_PpOBC_POST_Result_DeOBC_POST_Result(data) (*(data) = Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed Rte_Read_CtApLFM_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed
#  define Rte_Read_CtApLFM_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(data) (*(data) = Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault Rte_Write_CtApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault
#  define Rte_Write_CtApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(data) (Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault Rte_Write_CtApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault
#  define Rte_Write_CtApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(data) (Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault Rte_Write_CtApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault
#  define Rte_Write_CtApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(data) (Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError Rte_Write_CtApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError
#  define Rte_Write_CtApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(data) (Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCLVError_DeDCLVError Rte_Write_CtApLFM_PpDCLVError_DeDCLVError
#  define Rte_Write_CtApLFM_PpDCLVError_DeDCLVError(data) (Rte_CpApLFM_PpDCLVError_DeDCLVError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_Fault_DCDC_Fault Rte_Write_CtApLFM_PpInt_DCDC_Fault_DCDC_Fault
#  define Rte_Write_CtApLFM_PpInt_DCDC_Fault_DCDC_Fault(data) (Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage Rte_Write_CtApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage
#  define Rte_Write_CtApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(data) (Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalThresholdLVOvercurrentHW() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalThresholdLVOvercurrentHW) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalABSVehSpdThresholdOV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalABSVehSpdThresholdOV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalBatteryVoltageSafetyOVLimit1_ConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalBatteryVoltageSafetyOVLimit1_Threshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalBatteryVoltageSafetyOVLimit2_ConfirmTime) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalBatteryVoltageSafetyOVLimit2_Threshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLV_MaxNumberRetries() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalDCLV_MaxNumberRetries) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeBatteryVoltageSafety() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLFM.CalTimeBatteryVoltageSafety) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLVHWFault_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_FaultSatellite_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_InternalCom_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputOvercurrent_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShorcircuitHV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShortCircuitLV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShortCircuit_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageHV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageLimit1_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageLimit2_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_Overvoltage_Error;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLVHWFault_Error() \
  (&Rte_CpApLFM_PimDCLVHWFault_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_FaultSatellite_Error() \
  (&Rte_CpApLFM_PimDCLV_FaultSatellite_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_InternalCom_Error() \
  (&Rte_CpApLFM_PimDCLV_InternalCom_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OutputOvercurrent_Error() \
  (&Rte_CpApLFM_PimDCLV_OutputOvercurrent_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OutputShorcircuitHV_Error() \
  (&Rte_CpApLFM_PimDCLV_OutputShorcircuitHV_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OutputShortCircuitLV_Error() \
  (&Rte_CpApLFM_PimDCLV_OutputShortCircuitLV_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OutputShortCircuit_Error() \
  (&Rte_CpApLFM_PimDCLV_OutputShortCircuit_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OvervoltageHV_Error() \
  (&Rte_CpApLFM_PimDCLV_OvervoltageHV_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OvervoltageLimit1_Error() \
  (&Rte_CpApLFM_PimDCLV_OvervoltageLimit1_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_OvervoltageLimit2_Error() \
  (&Rte_CpApLFM_PimDCLV_OvervoltageLimit2_Error)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLV_Overvoltage_Error() \
  (&Rte_CpApLFM_PimDCLV_Overvoltage_Error)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApLFM_START_SEC_CODE
# include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_LFM_HW_faults_DataRecord_ReadData DataServices_LFM_HW_faults_DataRecord_ReadData
#  define RTE_RUNNABLE_RCtApLFMGetDCLVHWFaults RCtApLFMGetDCLVHWFaults
#  define RTE_RUNNABLE_RCtApLFM_init RCtApLFM_init
#  define RTE_RUNNABLE_RCtApLFM_task10ms RCtApLFM_task10ms
#  define RTE_RUNNABLE_RDataServices_LFM_Faults_DataRecord_ConditionCheckRead RDataServices_LFM_Faults_DataRecord_ConditionCheckRead
#  define RTE_RUNNABLE_RDataServices_LFM_Faults_DataRecord_ReadData RDataServices_LFM_Faults_DataRecord_ReadData
# endif

FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApLFM_CODE) RCtApLFMGetDCLVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApLFM_CODE) RCtApLFM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLFM_CODE) RCtApLFM_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif

# define CtApLFM_STOP_SEC_CODE
# include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLFM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
