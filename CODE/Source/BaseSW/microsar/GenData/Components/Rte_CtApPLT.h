/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApPLT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApPLT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPPLT_H
# define RTE_CTAPPLT_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApPLT_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt;
extern VAR(IdtDutyControlPilot, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot;
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(PFC_VPH12_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
extern VAR(PFC_VPH23_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V;
extern VAR(PFC_VPH31_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V;
extern VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
extern VAR(VCU_PosShuntJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD;
extern VAR(IdtProximityDetectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState;
extern VAR(IdtPlantModeTestInfoDID_Result, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result;
extern VAR(IdtPlantModeTestInfoDID_State, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty;
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage;
extern VAR(IdtRECHARGE_HMI_STATE, RTE_VAR_INIT) Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode (0U)
#  define Rte_InitValue_PpLedModes_PlantMode_DeChargeError (FALSE)
#  define Rte_InitValue_PpLedModes_PlantMode_DeChargeInProgress (FALSE)
#  define Rte_InitValue_PpLedModes_PlantMode_DeEndOfCharge (FALSE)
#  define Rte_InitValue_PpLedModes_PlantMode_DeGuideManagement (FALSE)
#  define Rte_InitValue_PpLedModes_PlantMode_DeProgrammingCharge (FALSE)
#  define Rte_InitValue_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode (FALSE)
#  define Rte_InitValue_PpPlantModeState_DePlantModeState (FALSE)
#  define Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result (0U)
#  define Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State (0U)
#  define Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase (0U)
#  define Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines (0U)
#  define Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty (0U)
#  define Rte_InitValue_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage (0U)
#  define Rte_InitValue_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode (0U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpBatteryVolt_DeBatteryVolt Rte_Read_CtApPLT_PpBatteryVolt_DeBatteryVolt
#  define Rte_Read_CtApPLT_PpBatteryVolt_DeBatteryVolt(data) (*(data) = Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDutyControlPilot_DeDutyControlPilot Rte_Read_CtApPLT_PpDutyControlPilot_DeDutyControlPilot
#  define Rte_Read_CtApPLT_PpDutyControlPilot_DeDutyControlPilot(data) (*(data) = Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Read_CtApPLT_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApPLT_PpInt_ABS_VehSpd_ABS_VehSpd(data) (*(data) = Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage Rte_Read_CtApPLT_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage
#  define Rte_Read_CtApPLT_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data) (*(data) = Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApPLT_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApPLT_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V Rte_Read_CtApPLT_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V
#  define Rte_Read_CtApPLT_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V Rte_Read_CtApPLT_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V
#  define Rte_Read_CtApPLT_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V Rte_Read_CtApPLT_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V
#  define Rte_Read_CtApPLT_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb Rte_Read_CtApPLT_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb
#  define Rte_Read_CtApPLT_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Read_CtApPLT_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Read_CtApPLT_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD Rte_Read_CtApPLT_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD
#  define Rte_Read_CtApPLT_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue Rte_Read_CtApPLT_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue
#  define Rte_Read_CtApPLT_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(data) (*(data) = Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization Rte_Read_CtApPLT_PpShutdownAuthorization_DeShutdownAuthorization
#  define Rte_Read_CtApPLT_PpShutdownAuthorization_DeShutdownAuthorization(data) (*(data) = Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode Rte_Write_CtApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode
#  define Rte_Write_CtApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(data) (Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_PlantMode_DeChargeError Rte_Write_CtApPLT_PpLedModes_PlantMode_DeChargeError
#  define Rte_Write_CtApPLT_PpLedModes_PlantMode_DeChargeError(data) (Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_PlantMode_DeChargeInProgress Rte_Write_CtApPLT_PpLedModes_PlantMode_DeChargeInProgress
#  define Rte_Write_CtApPLT_PpLedModes_PlantMode_DeChargeInProgress(data) (Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_PlantMode_DeEndOfCharge Rte_Write_CtApPLT_PpLedModes_PlantMode_DeEndOfCharge
#  define Rte_Write_CtApPLT_PpLedModes_PlantMode_DeEndOfCharge(data) (Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_PlantMode_DeGuideManagement Rte_Write_CtApPLT_PpLedModes_PlantMode_DeGuideManagement
#  define Rte_Write_CtApPLT_PpLedModes_PlantMode_DeGuideManagement(data) (Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge Rte_Write_CtApPLT_PpLedModes_PlantMode_DeProgrammingCharge
#  define Rte_Write_CtApPLT_PpLedModes_PlantMode_DeProgrammingCharge(data) (Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode Rte_Write_CtApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode
#  define Rte_Write_CtApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(data) (Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeState_DePlantModeState Rte_Write_CtApPLT_PpPlantModeState_DePlantModeState
#  define Rte_Write_CtApPLT_PpPlantModeState_DePlantModeState(data) (Rte_CpApPLT_PpPlantModeState_DePlantModeState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result
#  define Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(data) (Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State
#  define Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(data) (Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase
#  define Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(data) (Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines
#  define Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(data) (Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty
#  define Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(data) (Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage
#  define Rte_Write_CtApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(data) (Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode Rte_Write_CtApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode
#  define Rte_Write_CtApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(data) (Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_SetRamBlockStatus(NvM_BlockIdType parg0, boolean RamBlockStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(arg1) (NvM_SetRamBlockStatus((NvM_BlockIdType)46, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPDGN_APPL_CODE) RPlantModeDTCReset(IdtPlantModeDTCNumber DTC_Id); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(arg1) (RPlantModeDTCReset(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPDGN_APPL_CODE) RPlantModeDTCSet(IdtPlantModeDTCNumber DTC_Id); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(arg1) (RPlantModeDTCSet(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalGlobalTimeoutPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalGlobalTimeoutPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxProximityDetectPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMaxProximityDetectPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinProximityDetectPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMinProximityDetectPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalProximityVoltageStartingPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalProximityVoltageStartingPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceControlPilotPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalDebounceControlPilotPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceDCRelayVoltagePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalDebounceDCRelayVoltagePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebouncePhasePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalDebouncePhasePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDebounceProximityPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalDebounceProximityPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxControlPilotDutyPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMaxControlPilotDutyPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxDCRelayVoltagePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMaxDCRelayVoltagePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxPhasePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMaxPhasePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxTimePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMaxTimePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinControlPilotDutyPlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMinControlPilotDutyPlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinDCRelayVoltagePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMinDCRelayVoltagePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinPhasePlantMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalMinPhasePlantMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalInhibitControlPilotDetection() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalInhibitControlPilotDetection) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalInhibitDCRelayVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalInhibitDCRelayVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalInhibitPhaseVoltageDetection() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalInhibitPhaseVoltageDetection) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalInhibitProximityDetection() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPLT.CalInhibitProximityDetection) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_PLTNvMNeed_DefaultValue() (&(Rte_CpApPLT_PimPlantModeTestUTPlugin[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_PLTNvMNeed_DefaultValue() ((P2CONST(IdtPlantModeTestUTPlugin, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CpApPLT_PimPlantModeTestUTPlugin) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpBatteryVoltMaxTest_DeBatteryVoltMaxTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpBatteryVoltMinTest_DeBatteryVoltMinTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest() (Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP.Rte_Calprm_CpCalibration.PpVehStopMaxTest_DeVehStopMaxTest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtTimeoutPlantMode, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimGlobalTimeoutPlantMode;
extern VAR(IdtTimeoutPlantMode, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimTimeoutPlantMode;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeControlPilot;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeDCRelayVoltage;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModePhaseVoltage;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeProximity;
extern VAR(IdtPlantModeTestUTPlugin, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeTestUTPlugin;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimGlobalTimeoutPlantMode() \
  (&Rte_CpApPLT_PimGlobalTimeoutPlantMode)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTimeoutPlantMode() \
  (&Rte_CpApPLT_PimTimeoutPlantMode)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimPlantModeControlPilot() \
  (&Rte_CpApPLT_PimPlantModeControlPilot)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimPlantModeDCRelayVoltage() \
  (&Rte_CpApPLT_PimPlantModeDCRelayVoltage)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimPlantModePhaseVoltage() \
  (&Rte_CpApPLT_PimPlantModePhaseVoltage)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimPlantModeProximity() \
  (&Rte_CpApPLT_PimPlantModeProximity)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_PimPlantModeTestUTPlugin() (&((*RtePim_PimPlantModeTestUTPlugin())[0]))
#  else
#   define Rte_Pim_PimPlantModeTestUTPlugin() RtePim_PimPlantModeTestUTPlugin()
#  endif
#  define RtePim_PimPlantModeTestUTPlugin() \
  (&Rte_CpApPLT_PimPlantModeTestUTPlugin)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApPLT_START_SEC_CODE
# include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApPLT_init RCtApPLT_init
#  define RTE_RUNNABLE_RCtApPLT_task100ms RCtApPLT_task100ms
#  define RTE_RUNNABLE_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults
#  define RTE_RUNNABLE_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start
# endif

FUNC(void, CtApPLT_CODE) RCtApPLT_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApPLT_CODE) RCtApPLT_task100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, CtApPLT_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, CtApPLT_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApPLT_STOP_SEC_CODE
# include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPPLT_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
