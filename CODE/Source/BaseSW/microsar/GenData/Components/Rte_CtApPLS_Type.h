/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApPLS_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApPLS>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPPLS_TYPE_H
# define RTE_CTAPPLS_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_off_mode
#   define Cx0_off_mode (0U)
#  endif

#  ifndef Cx1_Init_mode
#   define Cx1_Init_mode (1U)
#  endif

#  ifndef Cx2_standby_mode
#   define Cx2_standby_mode (2U)
#  endif

#  ifndef Cx3_conversion_working_
#   define Cx3_conversion_working_ (3U)
#  endif

#  ifndef Cx4_error_mode
#   define Cx4_error_mode (4U)
#  endif

#  ifndef Cx5_degradation_mode
#   define Cx5_degradation_mode (5U)
#  endif

#  ifndef Cx6_reserved
#   define Cx6_reserved (6U)
#  endif

#  ifndef Cx7_invalid
#   define Cx7_invalid (7U)
#  endif

#  ifndef Cx0_DCHV_STATUS_STANDBY
#   define Cx0_DCHV_STATUS_STANDBY (0U)
#  endif

#  ifndef Cx1_DCHV_STATUS_RUN
#   define Cx1_DCHV_STATUS_RUN (1U)
#  endif

#  ifndef Cx2_DCHV_STATUS_ERROR
#   define Cx2_DCHV_STATUS_ERROR (2U)
#  endif

#  ifndef Cx3_DCHV_STATUS_ALARM
#   define Cx3_DCHV_STATUS_ALARM (3U)
#  endif

#  ifndef Cx4_DCHV_STATUS_SAFE
#   define Cx4_DCHV_STATUS_SAFE (4U)
#  endif

#  ifndef Cx0_STANDBY
#   define Cx0_STANDBY (0U)
#  endif

#  ifndef Cx1_RUN
#   define Cx1_RUN (1U)
#  endif

#  ifndef Cx2_ERROR
#   define Cx2_ERROR (2U)
#  endif

#  ifndef Cx3_ALARM
#   define Cx3_ALARM (3U)
#  endif

#  ifndef Cx4_SAFE
#   define Cx4_SAFE (4U)
#  endif

#  ifndef Cx5_DERATED
#   define Cx5_DERATED (5U)
#  endif

#  ifndef Cx6_SHUTDOWN
#   define Cx6_SHUTDOWN (6U)
#  endif

#  ifndef FAULT_LEVEL_NO_FAULT
#   define FAULT_LEVEL_NO_FAULT (0U)
#  endif

#  ifndef FAULT_LEVEL_1
#   define FAULT_LEVEL_1 (1U)
#  endif

#  ifndef FAULT_LEVEL_2
#   define FAULT_LEVEL_2 (2U)
#  endif

#  ifndef FAULT_LEVEL_3
#   define FAULT_LEVEL_3 (3U)
#  endif

#  ifndef IVM_NO_INPUT_DETECTED
#   define IVM_NO_INPUT_DETECTED (0U)
#  endif

#  ifndef IVM_MONOPHASIC_INPUT
#   define IVM_MONOPHASIC_INPUT (1U)
#  endif

#  ifndef IVM_TRIPHASIC_INPUT
#   define IVM_TRIPHASIC_INPUT (2U)
#  endif

#  ifndef POST_ONGOING
#   define POST_ONGOING (0U)
#  endif

#  ifndef POST_OK
#   define POST_OK (1U)
#  endif

#  ifndef POST_NOK
#   define POST_NOK (2U)
#  endif

#  ifndef Cx0_no_charging
#   define Cx0_no_charging (0U)
#  endif

#  ifndef Cx1_slow_charging
#   define Cx1_slow_charging (1U)
#  endif

#  ifndef Cx2_China_fast_charging
#   define Cx2_China_fast_charging (2U)
#  endif

#  ifndef Cx3_Euro_fast_charging
#   define Cx3_Euro_fast_charging (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPPLS_TYPE_H */
