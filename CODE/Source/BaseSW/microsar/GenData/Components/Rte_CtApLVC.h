/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLVC.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApLVC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLVC_H
# define RTE_CTAPLVC_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApLVC_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV;
extern VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed;
extern VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed;
extern VAR(DCDC_InputCurrent, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status;
extern VAR(DCLV_VoltageReference, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference;
extern VAR(SUP_RequestDCLVStatus, RTE_VAR_INIT) Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCLVError_DeDCLVError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull;
extern VAR(DCLV_Applied_Derating_Factor, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor;
extern VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
extern VAR(DCLV_Input_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current;
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
extern VAR(DCLV_Measured_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
extern VAR(DCLV_Measured_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage;
extern VAR(VCU_DCDCActivation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation;
extern VAR(VCU_DCDCVoltageReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpDCDC_POST_Result_DeDCDC_POST_Result (0U)
#  define Rte_InitValue_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault (FALSE)
#  define Rte_InitValue_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV (FALSE)
#  define Rte_InitValue_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed (FALSE)
#  define Rte_InitValue_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed (FALSE)
#  define Rte_InitValue_PpInt_DCDC_InputCurrent_DCDC_InputCurrent (0U)
#  define Rte_InitValue_PpInt_DCDC_Status_DCDC_Status (0U)
#  define Rte_InitValue_PpInt_DCDC_Status_Delayed_DCDC_Status (0U)
#  define Rte_InitValue_PpInt_DCLV_VoltageReference_DCLV_VoltageReference (0U)
#  define Rte_InitValue_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus (0U)
#  define Rte_InitValue_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue (FALSE)
#  define Rte_InitValue_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue (FALSE)
#  define Rte_InitValue_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest Rte_Read_CtApLVC_PpActiveDischargeRequest_DeActiveDischargeRequest
#  define Rte_Read_CtApLVC_PpActiveDischargeRequest_DeActiveDischargeRequest(data) (*(data) = Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLVError_DeDCLVError Rte_Read_CtApLVC_PpDCLVError_DeDCLVError
#  define Rte_Read_CtApLVC_PpDCLVError_DeDCLVError(data) (*(data) = Rte_CpApLFM_PpDCLVError_DeDCLVError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue Rte_Read_CtApLVC_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue
#  define Rte_Read_CtApLVC_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(data) (*(data) = Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck Rte_Read_CtApLVC_PpImplausibilityTempBuck_DeImplausibilityTempBuck
#  define Rte_Read_CtApLVC_PpImplausibilityTempBuck_DeImplausibilityTempBuck(data) (*(data) = Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull Rte_Read_CtApLVC_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull
#  define Rte_Read_CtApLVC_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(data) (*(data) = Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor Rte_Read_CtApLVC_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor
#  define Rte_Read_CtApLVC_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus Rte_Read_CtApLVC_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus
#  define Rte_Read_CtApLVC_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current Rte_Read_CtApLVC_PpInt_DCLV_Input_Current_DCLV_Input_Current
#  define Rte_Read_CtApLVC_PpInt_DCLV_Input_Current_DCLV_Input_Current(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage Rte_Read_CtApLVC_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Read_CtApLVC_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current Rte_Read_CtApLVC_PpInt_DCLV_Measured_Current_DCLV_Measured_Current
#  define Rte_Read_CtApLVC_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage Rte_Read_CtApLVC_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage
#  define Rte_Read_CtApLVC_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(data) (*(data) = Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation Rte_Read_CtApLVC_PpInt_VCU_DCDCActivation_VCU_DCDCActivation
#  define Rte_Read_CtApLVC_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq Rte_Read_CtApLVC_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq
#  define Rte_Read_CtApLVC_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(data) (*(data) = Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result Rte_Write_CtApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result
#  define Rte_Write_CtApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result(data) (Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault Rte_Write_CtApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault
#  define Rte_Write_CtApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(data) (Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV Rte_Write_CtApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV
#  define Rte_Write_CtApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(data) (Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed Rte_Write_CtApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed
#  define Rte_Write_CtApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(data) (Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed Rte_Write_CtApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed
#  define Rte_Write_CtApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(data) (Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent Rte_Write_CtApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent
#  define Rte_Write_CtApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(data) (Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_Status_DCDC_Status Rte_Write_CtApLVC_PpInt_DCDC_Status_DCDC_Status
#  define Rte_Write_CtApLVC_PpInt_DCDC_Status_DCDC_Status(data) (Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status Rte_Write_CtApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status
#  define Rte_Write_CtApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status(data) (Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference Rte_Write_CtApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference
#  define Rte_Write_CtApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(data) (Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus Rte_Write_CtApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus
#  define Rte_Write_CtApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data) (Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue Rte_Write_CtApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue
#  define Rte_Write_CtApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(data) (Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue Rte_Write_CtApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue
#  define Rte_Write_CtApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(data) (Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed Rte_Write_CtApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed
#  define Rte_Write_CtApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(data) (Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalDCLVDeratingThreshold() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLVC.CalDCLVDeratingThreshold) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDCLVTimeConfirmDerating() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApLVC.CalDCLVTimeConfirmDerating) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLVC_PimDCLVShutdownPathFSP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLVC_PimDCLVShutdownPathGPIO;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLVShutdownPathFSP() \
  (&Rte_CpApLVC_PimDCLVShutdownPathFSP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDCLVShutdownPathGPIO() \
  (&Rte_CpApLVC_PimDCLVShutdownPathGPIO)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApLVC_START_SEC_CODE
# include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApLVC_init RCtApLVC_init
#  define RTE_RUNNABLE_RCtApLVC_task10msA RCtApLVC_task10msA
#  define RTE_RUNNABLE_RCtApLVC_task10msB RCtApLVC_task10msB
# endif

FUNC(void, CtApLVC_CODE) RCtApLVC_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLVC_CODE) RCtApLVC_task10msA(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApLVC_CODE) RCtApLVC_task10msB(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApLVC_STOP_SEC_CODE
# include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLVC_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
