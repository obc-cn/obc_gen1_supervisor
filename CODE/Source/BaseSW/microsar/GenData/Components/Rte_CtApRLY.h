/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApRLY.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApRLY>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPRLY_H
# define RTE_CTAPRLY_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApRLY_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop;
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode;
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status;
extern VAR(PFC_VPH12_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
extern VAR(PFC_Vdclink_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpStopConditions_DeStopConditions;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpOutputPrechargeRelays_DeOutputPrechargeRelays (FALSE)
#  define Rte_InitValue_PpOutputRelayMono_DeOutputRelayMono (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop Rte_Read_CtApRLY_PpFaultChargeSoftStop_DeFaultChargeSoftStop
#  define Rte_Read_CtApRLY_PpFaultChargeSoftStop_DeFaultChargeSoftStop(data) (*(data) = Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInputVoltageMode_DeInputVoltageMode Rte_Read_CtApRLY_PpInputVoltageMode_DeInputVoltageMode
#  define Rte_Read_CtApRLY_PpInputVoltageMode_DeInputVoltageMode(data) (*(data) = Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ACRange_OBC_ACRange Rte_Read_CtApRLY_PpInt_OBC_ACRange_OBC_ACRange
#  define Rte_Read_CtApRLY_PpInt_OBC_ACRange_OBC_ACRange(data) (*(data) = Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApRLY_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApRLY_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt Rte_Read_CtApRLY_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Read_CtApRLY_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_OBC_Status Rte_Read_CtApRLY_PpInt_OBC_Status_OBC_Status
#  define Rte_Read_CtApRLY_PpInt_OBC_Status_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V Rte_Read_CtApRLY_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V
#  define Rte_Read_CtApRLY_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V Rte_Read_CtApRLY_PpInt_PFC_Vdclink_V_PFC_Vdclink_V
#  define Rte_Read_CtApRLY_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data) (*(data) = Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpStopConditions_DeStopConditions Rte_Read_CtApRLY_PpStopConditions_DeStopConditions
#  define Rte_Read_CtApRLY_PpStopConditions_DeStopConditions(data) (*(data) = Rte_CpApCHG_PpStopConditions_DeStopConditions, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays Rte_Write_CtApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays
#  define Rte_Write_CtApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays(data) (Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputRelayMono_DeOutputRelayMono Rte_Write_CtApRLY_PpOutputRelayMono_DeOutputRelayMono
#  define Rte_Write_CtApRLY_PpOutputRelayMono_DeOutputRelayMono(data) (Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPPCOM_APPL_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(arg1, arg2, arg3) (RSetIntCANDebugSignal(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalMaxPrechargeVDCLinkMono() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApRLY.CalMaxPrechargeVDCLinkMono) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMaxPrechargeVDCLinkTriphasic() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApRLY.CalMaxPrechargeVDCLinkTriphasic) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinPrechargeVDCLinkMono() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApRLY.CalMinPrechargeVDCLinkMono) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalMinPrechargeVDCLinkTriphasic() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApRLY.CalMinPrechargeVDCLinkTriphasic) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtApRLY_START_SEC_CODE
# include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApRLY_init RCtApRLY_init
#  define RTE_RUNNABLE_RCtApRLY_task10ms RCtApRLY_task10ms
# endif

FUNC(void, CtApRLY_CODE) RCtApRLY_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApRLY_CODE) RCtApRLY_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApRLY_STOP_SEC_CODE
# include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPRLY_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
