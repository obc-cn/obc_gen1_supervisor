/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApILT_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApILT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPILT_TYPE_H
# define RTE_CTAPILT_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_ON
#   define Cx0_ON (0U)
#  endif

#  ifndef Cx1_OFF_not_ended
#   define Cx1_OFF_not_ended (1U)
#  endif

#  ifndef Cx2_OFF_ended
#   define Cx2_OFF_ended (2U)
#  endif

#  ifndef Cx3_Unavailable
#   define Cx3_Unavailable (3U)
#  endif

#  ifndef Cx0_STANDBY
#   define Cx0_STANDBY (0U)
#  endif

#  ifndef Cx1_DIFFEREE
#   define Cx1_DIFFEREE (1U)
#  endif

#  ifndef Cx2_IMMEDIATE
#   define Cx2_IMMEDIATE (2U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

#  ifndef ELOCK_LOCKED
#   define ELOCK_LOCKED (0U)
#  endif

#  ifndef ELOCK_UNLOCKED
#   define ELOCK_UNLOCKED (1U)
#  endif

#  ifndef ELOCK_DRIVE_UNDEFINED
#   define ELOCK_DRIVE_UNDEFINED (2U)
#  endif

#  ifndef Cx0_invalid_value
#   define Cx0_invalid_value (0U)
#  endif

#  ifndef Cx1_not_connected
#   define Cx1_not_connected (1U)
#  endif

#  ifndef Cx2_full_connected
#   define Cx2_full_connected (2U)
#  endif

#  ifndef Cx3_CC_is_half_connected
#   define Cx3_CC_is_half_connected (3U)
#  endif

#  ifndef OBC_FAULT_NO_FAULT
#   define OBC_FAULT_NO_FAULT (0U)
#  endif

#  ifndef OBC_FAULT_LEVEL_1
#   define OBC_FAULT_LEVEL_1 (64U)
#  endif

#  ifndef OBC_FAULT_LEVEL_2
#   define OBC_FAULT_LEVEL_2 (128U)
#  endif

#  ifndef OBC_FAULT_LEVEL_3
#   define OBC_FAULT_LEVEL_3 (192U)
#  endif

#  ifndef Cx0_Disconnected
#   define Cx0_Disconnected (0U)
#  endif

#  ifndef Cx1_In_progress
#   define Cx1_In_progress (1U)
#  endif

#  ifndef Cx2_Failure
#   define Cx2_Failure (2U)
#  endif

#  ifndef Cx3_Stopped
#   define Cx3_Stopped (3U)
#  endif

#  ifndef Cx4_Finished
#   define Cx4_Finished (4U)
#  endif

#  ifndef Cx5_Reserved
#   define Cx5_Reserved (5U)
#  endif

#  ifndef Cx6_Reserved
#   define Cx6_Reserved (6U)
#  endif

#  ifndef Cx7_Reserved
#   define Cx7_Reserved (7U)
#  endif

#  ifndef Cx0_OFF
#   define Cx0_OFF (0U)
#  endif

#  ifndef Cx1_Active_Drive
#   define Cx1_Active_Drive (1U)
#  endif

#  ifndef Cx2_Discharge
#   define Cx2_Discharge (2U)
#  endif

#  ifndef Cx3_PI_Charge
#   define Cx3_PI_Charge (3U)
#  endif

#  ifndef Cx4_PI_Balance
#   define Cx4_PI_Balance (4U)
#  endif

#  ifndef Cx5_PI_Discharge
#   define Cx5_PI_Discharge (5U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPILT_TYPE_H */
