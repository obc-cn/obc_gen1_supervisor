/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApFCT.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApFCT>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPFCT_H
# define RTE_CTAPFCT_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApFCT_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(DCDC_FaultLampRequest, RTE_VAR_INIT) Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales;
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas;
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions;
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState;
extern VAR(BSI_MainWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw;
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest (0U)
#  define Rte_InitValue_PpOutputTempAftsales_DeOutputTempAC1Aftsales (0U)
#  define Rte_InitValue_PpOutputTempAftsales_DeOutputTempAC2Aftsales (0U)
#  define Rte_InitValue_PpOutputTempAftsales_DeOutputTempAC3Aftsales (0U)
#  define Rte_InitValue_PpOutputTempAftsales_DeOutputTempACNAftsales (0U)
#  define Rte_InitValue_PpOutputTempAftsales_DeOutputTempDC1Aftsales (0U)
#  define Rte_InitValue_PpOutputTempAftsales_DeOutputTempDC2Aftsales (0U)
#  define Rte_InitValue_PpOutputTempMeas_DeOutputTempAC1Meas (400U)
#  define Rte_InitValue_PpOutputTempMeas_DeOutputTempAC2Meas (400U)
#  define Rte_InitValue_PpOutputTempMeas_DeOutputTempAC3Meas (400U)
#  define Rte_InitValue_PpOutputTempMeas_DeOutputTempACNMeas (400U)
#  define Rte_InitValue_PpOutputTempMeas_DeOutputTempDC1Meas (400U)
#  define Rte_InitValue_PpOutputTempMeas_DeOutputTempDC2Meas (400U)
#  define Rte_InitValue_PpTempFaults_DeTempAC1FaultSCG (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempAC1FaultSCP (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempAC2FaultSCG (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempAC2FaultSCP (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempAC3FaultSCG (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempAC3FaultSCP (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempACNFaultSCG (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempACNFaultSCP (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempDC1FaultSCG (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempDC1FaultSCP (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempDC2FaultSCG (FALSE)
#  define Rte_InitValue_PpTempFaults_DeTempDC2FaultSCP (FALSE)
#  define Rte_InitValue_PpTempMonitoringConditions_DeAC1TempMonitoringConditions (FALSE)
#  define Rte_InitValue_PpTempMonitoringConditions_DeAC2TempMonitoringConditions (FALSE)
#  define Rte_InitValue_PpTempMonitoringConditions_DeAC3TempMonitoringConditions (FALSE)
#  define Rte_InitValue_PpTempMonitoringConditions_DeACNTempMonitoringConditions (FALSE)
#  define Rte_InitValue_PpTempMonitoringConditions_DeDC1TempMonitoringConditions (FALSE)
#  define Rte_InitValue_PpTempMonitoringConditions_DeDC2TempMonitoringConditions (FALSE)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpAppRCDECUState_DeAppRCDECUState Rte_Read_CtApFCT_PpAppRCDECUState_DeAppRCDECUState
#  define Rte_Read_CtApFCT_PpAppRCDECUState_DeAppRCDECUState(data) (*(data) = Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState Rte_Read_CtApFCT_PpBatteryVoltageState_DeBatteryVoltageState
#  define Rte_Read_CtApFCT_PpBatteryVoltageState_DeBatteryVoltageState(data) (*(data) = Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup Rte_Read_CtApFCT_PpInt_BSI_MainWakeup_BSI_MainWakeup
#  define Rte_Read_CtApFCT_PpInt_BSI_MainWakeup_BSI_MainWakeup(data) (*(data) = Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempAC1Raw
#  define Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempAC1Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempAC2Raw
#  define Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempAC2Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempAC3Raw
#  define Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempAC3Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempACNRaw
#  define Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempACNRaw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempDC1Raw
#  define Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempDC1Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempDC2Raw
#  define Rte_Read_CtApFCT_PpMsrTempRaw_DeMsrTempDC2Raw(data) (*(data) = Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest Rte_Write_CtApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest
#  define Rte_Write_CtApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(data) (Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales
#  define Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales(data) (Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales
#  define Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales(data) (Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales
#  define Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales(data) (Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales
#  define Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales(data) (Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales
#  define Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales(data) (Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales
#  define Rte_Write_CtApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales(data) (Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempAC1Meas
#  define Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempAC1Meas(data) (Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempAC2Meas
#  define Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempAC2Meas(data) (Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempAC3Meas
#  define Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempAC3Meas(data) (Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempACNMeas
#  define Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempACNMeas(data) (Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempDC1Meas
#  define Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempDC1Meas(data) (Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempDC2Meas
#  define Rte_Write_CtApFCT_PpOutputTempMeas_DeOutputTempDC2Meas(data) (Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempAC1FaultSCG Rte_Write_CtApFCT_PpTempFaults_DeTempAC1FaultSCG
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempAC1FaultSCG(data) (Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempAC1FaultSCP Rte_Write_CtApFCT_PpTempFaults_DeTempAC1FaultSCP
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempAC1FaultSCP(data) (Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempAC2FaultSCG Rte_Write_CtApFCT_PpTempFaults_DeTempAC2FaultSCG
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempAC2FaultSCG(data) (Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempAC2FaultSCP Rte_Write_CtApFCT_PpTempFaults_DeTempAC2FaultSCP
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempAC2FaultSCP(data) (Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempAC3FaultSCG Rte_Write_CtApFCT_PpTempFaults_DeTempAC3FaultSCG
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempAC3FaultSCG(data) (Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempAC3FaultSCP Rte_Write_CtApFCT_PpTempFaults_DeTempAC3FaultSCP
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempAC3FaultSCP(data) (Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempACNFaultSCG Rte_Write_CtApFCT_PpTempFaults_DeTempACNFaultSCG
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempACNFaultSCG(data) (Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempACNFaultSCP Rte_Write_CtApFCT_PpTempFaults_DeTempACNFaultSCP
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempACNFaultSCP(data) (Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempDC1FaultSCG Rte_Write_CtApFCT_PpTempFaults_DeTempDC1FaultSCG
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempDC1FaultSCG(data) (Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempDC1FaultSCP Rte_Write_CtApFCT_PpTempFaults_DeTempDC1FaultSCP
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempDC1FaultSCP(data) (Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempDC2FaultSCG Rte_Write_CtApFCT_PpTempFaults_DeTempDC2FaultSCG
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempDC2FaultSCG(data) (Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempFaults_DeTempDC2FaultSCP Rte_Write_CtApFCT_PpTempFaults_DeTempDC2FaultSCP
#  define Rte_Write_CtApFCT_PpTempFaults_DeTempDC2FaultSCP(data) (Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions Rte_Write_CtApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions
#  define Rte_Write_CtApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(data) (Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions Rte_Write_CtApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions
#  define Rte_Write_CtApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(data) (Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions Rte_Write_CtApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions
#  define Rte_Write_CtApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(data) (Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions Rte_Write_CtApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions
#  define Rte_Write_CtApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions(data) (Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions Rte_Write_CtApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions
#  define Rte_Write_CtApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(data) (Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions Rte_Write_CtApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions
#  define Rte_Write_CtApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(data) (Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalThresholdSCG_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCG_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCG_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCG_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCG_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCG_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCG_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCG_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCG_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCG_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCG_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCG_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCP_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCP_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCP_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCP_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCP_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCP_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCP_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCP_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCP_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCP_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalThresholdSCP_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalThresholdSCP_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCG_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCG_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCG_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCG_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCG_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCG_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCG_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCG_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCG_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCG_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCG_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCG_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCP_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCP_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCP_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCP_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCP_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCP_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCP_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCP_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCP_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCP_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeConfSCP_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeConfSCP_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCG_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCG_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCG_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCG_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCG_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCG_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCG_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCG_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCG_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCG_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCG_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCG_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCP_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCP_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCP_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCP_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCP_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCP_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCP_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCP_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCP_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCP_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeHealSCP_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalTimeHealSCP_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalRampGradient_AC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalRampGradient_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalRampGradient_AC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalRampGradient_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalRampGradient_AC3() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalRampGradient_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalRampGradient_ACN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalRampGradient_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalRampGradient_DC1() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalRampGradient_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalRampGradient_DC2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalRampGradient_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSubstituteValueAC1_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalSubstituteValueAC1_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSubstituteValueAC2_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalSubstituteValueAC2_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSubstituteValueAC3_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalSubstituteValueAC3_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSubstituteValueACN_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalSubstituteValueACN_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSubstituteValueDC1_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalSubstituteValueDC1_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSubstituteValueDC2_C() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalSubstituteValueDC2_C) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowAC1Plug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalCtrlFlowAC1Plug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowAC2Plug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalCtrlFlowAC2Plug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowAC3Plug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalCtrlFlowAC3Plug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowACNPlug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalCtrlFlowACNPlug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowDC1Plug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalCtrlFlowDC1Plug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalCtrlFlowDC2Plug() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalCtrlFlowDC2Plug) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalTemp_AC1() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_AC1[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalTemp_AC1() ((P2CONST(IdtLinTableGlobalTemp, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalTemp_AC2() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_AC2[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalTemp_AC2() ((P2CONST(IdtLinTableGlobalTemp, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalTemp_AC3() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_AC3[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalTemp_AC3() ((P2CONST(IdtLinTableGlobalTemp, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalTemp_ACN() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_ACN[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalTemp_ACN() ((P2CONST(IdtLinTableGlobalTemp, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalTemp_DC1() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_DC1[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalTemp_DC1() ((P2CONST(IdtLinTableGlobalTemp, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalTemp_DC2() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_DC2[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalTemp_DC2() ((P2CONST(IdtLinTableGlobalTemp, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalTemp_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalVoltage_AC1() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_AC1[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalVoltage_AC1() ((P2CONST(IdtLinTableGlobalVolt, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_AC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalVoltage_AC2() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_AC2[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalVoltage_AC2() ((P2CONST(IdtLinTableGlobalVolt, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_AC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalVoltage_AC3() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_AC3[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalVoltage_AC3() ((P2CONST(IdtLinTableGlobalVolt, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_AC3) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalVoltage_ACN() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_ACN[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalVoltage_ACN() ((P2CONST(IdtLinTableGlobalVolt, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_ACN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalVoltage_DC1() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_DC1[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalVoltage_DC1() ((P2CONST(IdtLinTableGlobalVolt, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_DC1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_CData_CalLinTableGlobalVoltage_DC2() (&(Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_DC2[0])) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  else
#   define Rte_CData_CalLinTableGlobalVoltage_DC2() ((P2CONST(IdtLinTableGlobalVolt, AUTOMATIC, RTE_CONST_DEFAULT_RTE_CDATA_GROUP))&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApFCT.CalLinTableGlobalVoltage_DC2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2CounterSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2PrefaultSCP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC1CounterSCG() \
  (&Rte_CpApFCT_PimTempAC1CounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC1CounterSCP() \
  (&Rte_CpApFCT_PimTempAC1CounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC2CounterSCG() \
  (&Rte_CpApFCT_PimTempAC2CounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC2CounterSCP() \
  (&Rte_CpApFCT_PimTempAC2CounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC3CounterSCG() \
  (&Rte_CpApFCT_PimTempAC3CounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC3CounterSCP() \
  (&Rte_CpApFCT_PimTempAC3CounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempACNCounterSCG() \
  (&Rte_CpApFCT_PimTempACNCounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempACNCounterSCP() \
  (&Rte_CpApFCT_PimTempACNCounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC1CounterSCG() \
  (&Rte_CpApFCT_PimTempDC1CounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC1CounterSCP() \
  (&Rte_CpApFCT_PimTempDC1CounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC2CounterSCG() \
  (&Rte_CpApFCT_PimTempDC2CounterSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC2CounterSCP() \
  (&Rte_CpApFCT_PimTempDC2CounterSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC1PrefaultSCG() \
  (&Rte_CpApFCT_PimTempAC1PrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC1PrefaultSCP() \
  (&Rte_CpApFCT_PimTempAC1PrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC2PrefaultSCG() \
  (&Rte_CpApFCT_PimTempAC2PrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC2PrefaultSCP() \
  (&Rte_CpApFCT_PimTempAC2PrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC3PrefaultSCG() \
  (&Rte_CpApFCT_PimTempAC3PrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempAC3PrefaultSCP() \
  (&Rte_CpApFCT_PimTempAC3PrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempACNPrefaultSCG() \
  (&Rte_CpApFCT_PimTempACNPrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempACNPrefaultSCP() \
  (&Rte_CpApFCT_PimTempACNPrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC1PrefaultSCG() \
  (&Rte_CpApFCT_PimTempDC1PrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC1PrefaultSCP() \
  (&Rte_CpApFCT_PimTempDC1PrefaultSCP)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC2PrefaultSCG() \
  (&Rte_CpApFCT_PimTempDC2PrefaultSCG)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimTempDC2PrefaultSCP() \
  (&Rte_CpApFCT_PimTempDC2PrefaultSCP)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApFCT_START_SEC_CODE
# include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtApFCT_init RCtApFCT_init
#  define RTE_RUNNABLE_RCtApFCT_task100ms RCtApFCT_task100ms
# endif

FUNC(void, CtApFCT_CODE) RCtApFCT_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApFCT_CODE) RCtApFCT_task100ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtApFCT_STOP_SEC_CODE
# include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPFCT_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
