/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApLVC_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApLVC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPLVC_TYPE_H
# define RTE_CTAPLVC_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_off_mode
#   define Cx0_off_mode (0U)
#  endif

#  ifndef Cx1_Init_mode
#   define Cx1_Init_mode (1U)
#  endif

#  ifndef Cx2_standby_mode
#   define Cx2_standby_mode (2U)
#  endif

#  ifndef Cx3_conversion_working_
#   define Cx3_conversion_working_ (3U)
#  endif

#  ifndef Cx4_error_mode
#   define Cx4_error_mode (4U)
#  endif

#  ifndef Cx5_degradation_mode
#   define Cx5_degradation_mode (5U)
#  endif

#  ifndef Cx6_reserved
#   define Cx6_reserved (6U)
#  endif

#  ifndef Cx7_invalid
#   define Cx7_invalid (7U)
#  endif

#  ifndef Cx0_STANDBY
#   define Cx0_STANDBY (0U)
#  endif

#  ifndef Cx1_RUN
#   define Cx1_RUN (1U)
#  endif

#  ifndef Cx2_ERROR
#   define Cx2_ERROR (2U)
#  endif

#  ifndef Cx3_ALARM
#   define Cx3_ALARM (3U)
#  endif

#  ifndef Cx4_SAFE
#   define Cx4_SAFE (4U)
#  endif

#  ifndef Cx5_DERATED
#   define Cx5_DERATED (5U)
#  endif

#  ifndef Cx6_SHUTDOWN
#   define Cx6_SHUTDOWN (6U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

#  ifndef POST_ONGOING
#   define POST_ONGOING (0U)
#  endif

#  ifndef POST_OK
#   define POST_OK (1U)
#  endif

#  ifndef POST_NOK
#   define POST_NOK (2U)
#  endif

#  ifndef Cx0_DCLV_STATE_STANDBY
#   define Cx0_DCLV_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_DCLV_STATE_RUN
#   define Cx1_DCLV_STATE_RUN (1U)
#  endif

#  ifndef Cx2_DCLV_STATE_RESET_ERROR
#   define Cx2_DCLV_STATE_RESET_ERROR (2U)
#  endif

#  ifndef Cx3_DCLV_STATE_DISCHARGE
#   define Cx3_DCLV_STATE_DISCHARGE (3U)
#  endif

#  ifndef Cx4_DCLV_STATE_SHUTDOWN
#   define Cx4_DCLV_STATE_SHUTDOWN (4U)
#  endif

#  ifndef Cx0_Not_used
#   define Cx0_Not_used (0U)
#  endif

#  ifndef Cx1_DCDC_Off
#   define Cx1_DCDC_Off (1U)
#  endif

#  ifndef Cx2_DCDC_On
#   define Cx2_DCDC_On (2U)
#  endif

#  ifndef Cx3_Unavailable_Value
#   define Cx3_Unavailable_Value (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPLVC_TYPE_H */
