/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtHwAbsPOM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtHwAbsPOM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTHWABSPOM_H
# define RTE_CTHWABSPOM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtHwAbsPOM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl;
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl;
extern VAR(IdtExtPlgLedrCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl Rte_Read_CtHwAbsPOM_PpExtChLedRGBCtl_DeExtChLedBrCtl
#  define Rte_Read_CtHwAbsPOM_PpExtChLedRGBCtl_DeExtChLedBrCtl(data) (*(data) = Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl Rte_Read_CtHwAbsPOM_PpExtChLedRGBCtl_DeExtChLedGrCtl
#  define Rte_Read_CtHwAbsPOM_PpExtChLedRGBCtl_DeExtChLedGrCtl(data) (*(data) = Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl Rte_Read_CtHwAbsPOM_PpExtChLedRGBCtl_DeExtChLedRrCtl
#  define Rte_Read_CtHwAbsPOM_PpExtChLedRGBCtl_DeExtChLedRrCtl(data) (*(data) = Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl Rte_Read_CtHwAbsPOM_PpExtPlgLedrCtl_DeExtPlgLedrCtl
#  define Rte_Read_CtHwAbsPOM_PpExtPlgLedrCtl_DeExtPlgLedrCtl(data) (*(data) = Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays Rte_Read_CtHwAbsPOM_PpOutputPrechargeRelays_DeOutputPrechargeRelays
#  define Rte_Read_CtHwAbsPOM_PpOutputPrechargeRelays_DeOutputPrechargeRelays(data) (*(data) = Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOutputRelayMono_DeOutputRelayMono Rte_Read_CtHwAbsPOM_PpOutputRelayMono_DeOutputRelayMono
#  define Rte_Read_CtHwAbsPOM_PpOutputRelayMono_DeOutputRelayMono(data) (*(data) = Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsPOM.CalDutyKeepOutputPrechargeRelaysClosed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalDutyKeepRelayMonoClosed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsPOM.CalDutyKeepRelayMonoClosed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrequencyActivateRelays() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsPOM.CalFrequencyActivateRelays) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrequencyPWM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsPOM.CalFrequencyPWM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeCloseOutputPrechargeRelays() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsPOM.CalTimeCloseOutputPrechargeRelays) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeCloseRelayMono() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtHwAbsPOM.CalTimeCloseRelayMono) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

# endif /* !defined(RTE_CORE) */


# define CtHwAbsPOM_START_SEC_CODE
# include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RCtHwAbsPOM_init RCtHwAbsPOM_init
#  define RTE_RUNNABLE_RCtHwAbsPOM_task10ms RCtHwAbsPOM_task10ms
# endif

FUNC(void, CtHwAbsPOM_CODE) RCtHwAbsPOM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtHwAbsPOM_CODE) RCtHwAbsPOM_task10ms(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */

# define CtHwAbsPOM_STOP_SEC_CODE
# include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTHWABSPOM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
