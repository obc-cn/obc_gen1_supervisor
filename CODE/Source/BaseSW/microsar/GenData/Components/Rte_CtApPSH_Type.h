/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApPSH_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApPSH>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPPSH_TYPE_H
# define RTE_CTAPPSH_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_No_Locked
#   define Cx0_No_Locked (0U)
#  endif

#  ifndef Cx1_Locked
#   define Cx1_Locked (1U)
#  endif

#  ifndef Cx2_Overlocked
#   define Cx2_Overlocked (2U)
#  endif

#  ifndef Cx3_Reserved
#   define Cx3_Reserved (3U)
#  endif

#  ifndef APP_STATE_0
#   define APP_STATE_0 (0U)
#  endif

#  ifndef APP_STATE_1
#   define APP_STATE_1 (1U)
#  endif

#  ifndef APP_STATE_2
#   define APP_STATE_2 (2U)
#  endif

#  ifndef APP_STATE_3
#   define APP_STATE_3 (3U)
#  endif

#  ifndef APP_STATE_4
#   define APP_STATE_4 (4U)
#  endif

#  ifndef APP_STATE_5
#   define APP_STATE_5 (5U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

#  ifndef Cx0_Disconnected
#   define Cx0_Disconnected (0U)
#  endif

#  ifndef Cx1_In_progress
#   define Cx1_In_progress (1U)
#  endif

#  ifndef Cx2_Failure
#   define Cx2_Failure (2U)
#  endif

#  ifndef Cx3_Stopped
#   define Cx3_Stopped (3U)
#  endif

#  ifndef Cx4_Finished
#   define Cx4_Finished (4U)
#  endif

#  ifndef Cx5_Reserved
#   define Cx5_Reserved (5U)
#  endif

#  ifndef Cx6_Reserved
#   define Cx6_Reserved (6U)
#  endif

#  ifndef Cx7_Reserved
#   define Cx7_Reserved (7U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPPSH_TYPE_H */
