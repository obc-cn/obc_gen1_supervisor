/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApAEM_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApAEM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPAEM_TYPE_H
# define RTE_CTAPAEM_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_Invalid_RCD
#   define Cx0_Invalid_RCD (0U)
#  endif

#  ifndef Cx1_No_main_wake_up_request
#   define Cx1_No_main_wake_up_request (1U)
#  endif

#  ifndef Cx2_Main_wake_up_request
#   define Cx2_Main_wake_up_request (2U)
#  endif

#  ifndef Cx3_Not_valid
#   define Cx3_Not_valid (3U)
#  endif

#  ifndef APP_STATE_0
#   define APP_STATE_0 (0U)
#  endif

#  ifndef APP_STATE_1
#   define APP_STATE_1 (1U)
#  endif

#  ifndef APP_STATE_2
#   define APP_STATE_2 (2U)
#  endif

#  ifndef APP_STATE_3
#   define APP_STATE_3 (3U)
#  endif

#  ifndef APP_STATE_4
#   define APP_STATE_4 (4U)
#  endif

#  ifndef APP_STATE_5
#   define APP_STATE_5 (5U)
#  endif

#  ifndef Cx0_Partial_wakeup
#   define Cx0_Partial_wakeup (0U)
#  endif

#  ifndef Cx1_Internal_wakeup
#   define Cx1_Internal_wakeup (1U)
#  endif

#  ifndef Cx2_Transitory
#   define Cx2_Transitory (2U)
#  endif

#  ifndef Cx3_Nominal_main_wakeup
#   define Cx3_Nominal_main_wakeup (3U)
#  endif

#  ifndef Cx4_Degraded_main_wakeup
#   define Cx4_Degraded_main_wakeup (4U)
#  endif

#  ifndef Cx5_Shutdown_preparation
#   define Cx5_Shutdown_preparation (5U)
#  endif

#  ifndef Cx6_Reserved
#   define Cx6_Reserved (6U)
#  endif

#  ifndef Cx7_Reserved
#   define Cx7_Reserved (7U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPAEM_TYPE_H */
