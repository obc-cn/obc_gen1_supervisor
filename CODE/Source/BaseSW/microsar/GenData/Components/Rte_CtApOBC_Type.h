/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApOBC_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application types header file for SW-C <CtApOBC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPOBC_TYPE_H
# define RTE_CTAPOBC_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Cx0_DCHV_STATUS_STANDBY
#   define Cx0_DCHV_STATUS_STANDBY (0U)
#  endif

#  ifndef Cx1_DCHV_STATUS_RUN
#   define Cx1_DCHV_STATUS_RUN (1U)
#  endif

#  ifndef Cx2_DCHV_STATUS_ERROR
#   define Cx2_DCHV_STATUS_ERROR (2U)
#  endif

#  ifndef Cx3_DCHV_STATUS_ALARM
#   define Cx3_DCHV_STATUS_ALARM (3U)
#  endif

#  ifndef Cx4_DCHV_STATUS_SAFE
#   define Cx4_DCHV_STATUS_SAFE (4U)
#  endif

#  ifndef DEBUG_PORT_ID_0
#   define DEBUG_PORT_ID_0 (0U)
#  endif

#  ifndef DEBUG_PORT_ID_1
#   define DEBUG_PORT_ID_1 (1U)
#  endif

#  ifndef DEBUG_PORT_ID_2
#   define DEBUG_PORT_ID_2 (2U)
#  endif

#  ifndef IVM_NO_INPUT_DETECTED
#   define IVM_NO_INPUT_DETECTED (0U)
#  endif

#  ifndef IVM_MONOPHASIC_INPUT
#   define IVM_MONOPHASIC_INPUT (1U)
#  endif

#  ifndef IVM_TRIPHASIC_INPUT
#   define IVM_TRIPHASIC_INPUT (2U)
#  endif

#  ifndef POST_ONGOING
#   define POST_ONGOING (0U)
#  endif

#  ifndef POST_OK
#   define POST_OK (1U)
#  endif

#  ifndef POST_NOK
#   define POST_NOK (2U)
#  endif

#  ifndef Cx0_no_charging
#   define Cx0_no_charging (0U)
#  endif

#  ifndef Cx1_slow_charging
#   define Cx1_slow_charging (1U)
#  endif

#  ifndef Cx2_China_fast_charging
#   define Cx2_China_fast_charging (2U)
#  endif

#  ifndef Cx3_Euro_fast_charging
#   define Cx3_Euro_fast_charging (3U)
#  endif

#  ifndef Cx0_No_220V_AC
#   define Cx0_No_220V_AC (0U)
#  endif

#  ifndef Cx1_220V_AC_connected
#   define Cx1_220V_AC_connected (1U)
#  endif

#  ifndef Cx2_220V_AC_disconnected
#   define Cx2_220V_AC_disconnected (2U)
#  endif

#  ifndef Cx3_Invalid_value
#   define Cx3_Invalid_value (3U)
#  endif

#  ifndef Cx0_off_mode
#   define Cx0_off_mode (0U)
#  endif

#  ifndef Cx1_Init_mode
#   define Cx1_Init_mode (1U)
#  endif

#  ifndef Cx2_standby_mode
#   define Cx2_standby_mode (2U)
#  endif

#  ifndef Cx3_conversion_working_
#   define Cx3_conversion_working_ (3U)
#  endif

#  ifndef Cx4_error_mode
#   define Cx4_error_mode (4U)
#  endif

#  ifndef Cx5_degradation_mode
#   define Cx5_degradation_mode (5U)
#  endif

#  ifndef Cx6_reserved
#   define Cx6_reserved (6U)
#  endif

#  ifndef Cx7_invalid
#   define Cx7_invalid (7U)
#  endif

#  ifndef Cx0_PFC_STATE_STANDBY
#   define Cx0_PFC_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_PFC_STATE_TRI
#   define Cx1_PFC_STATE_TRI (1U)
#  endif

#  ifndef Cx2_PFC_STATE_MONO
#   define Cx2_PFC_STATE_MONO (2U)
#  endif

#  ifndef Cx3_PFC_STATE_RESET_ERROR
#   define Cx3_PFC_STATE_RESET_ERROR (3U)
#  endif

#  ifndef Cx4_PFC_STATE_DISCHARGE
#   define Cx4_PFC_STATE_DISCHARGE (4U)
#  endif

#  ifndef Cx5_PFC_STATE_SHUTDOWN
#   define Cx5_PFC_STATE_SHUTDOWN (5U)
#  endif

#  ifndef Cx6_PFC_STATE_START_TRI
#   define Cx6_PFC_STATE_START_TRI (6U)
#  endif

#  ifndef Cx7_PFC_STATE_START_MONO
#   define Cx7_PFC_STATE_START_MONO (7U)
#  endif

#  ifndef Cx8_PFC_STATE_ERROR
#   define Cx8_PFC_STATE_ERROR (8U)
#  endif

#  ifndef Cx9_PFC_STATE_DIRECTPWM
#   define Cx9_PFC_STATE_DIRECTPWM (9U)
#  endif

#  ifndef Cx0_DCHV_STATE_STANDBY
#   define Cx0_DCHV_STATE_STANDBY (0U)
#  endif

#  ifndef Cx1_DCHV_STATE_RUN
#   define Cx1_DCHV_STATE_RUN (1U)
#  endif

#  ifndef Cx2_DCHV_STATE_RESET_ERROR
#   define Cx2_DCHV_STATE_RESET_ERROR (2U)
#  endif

#  ifndef Cx3_DCHV_STATE_DISCHARGE
#   define Cx3_DCHV_STATE_DISCHARGE (3U)
#  endif

#  ifndef Cx4_DCHV_STATE_SHUTDOWN
#   define Cx4_DCHV_STATE_SHUTDOWN (4U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPOBC_TYPE_H */
