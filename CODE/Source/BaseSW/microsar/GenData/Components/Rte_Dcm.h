/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Dcm.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <Dcm>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_DCM_H
# define RTE_DCM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Dcm_Type.h"
# include "Rte_DataHandleType.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType nextMode);
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmEcuReset_DcmEcuReset(Dcm_EcuResetType nextMode);
FUNC(Std_ReturnType, RTE_CODE) Rte_SwitchAck_Dcm_DcmEcuReset_DcmEcuReset(void);
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_1_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_1_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_2_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_2_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_3_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_3_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_4_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_4_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_DataServices_New_Identification_Parameters_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_RoutineServices_check_Memory_CM_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_DCM_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_RoutineServices_check_Memory_CM_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_DCM_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_RoutineServices_check_Memory_CM_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_DCM_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_RoutineServices_check_Memory_CM_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_DCM_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_RoutineServices_erase_Memory_EM_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Out_routineInfo_FF00, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_RoutineServices_erase_Memory_EM_Start(uint8 In_routineControlOptionRecord_FF00, uint16 In_RCEOR_SGN_OTL, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Out_routineInfo_FF00, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_SecurityAccess_UnlockedL1_CompareKey(P2CONST(uint8, AUTOMATIC, RTE_DCM_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_SecurityAccess_UnlockedL1_CompareKey(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DCM_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_SecurityAccess_UnlockedL1_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_DCM_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Call_Dcm_SecurityAccess_UnlockedL1_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DCM_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DCM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Switch_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Switch_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl Rte_Switch_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl
#  define Rte_Switch_DcmEcuReset_DcmEcuReset Rte_Switch_Dcm_DcmEcuReset_DcmEcuReset


/**********************************************************************************************************************
 * Rte_Feedback_<p>_<m> (mode switch acknowledge)
 *********************************************************************************************************************/
#  define Rte_SwitchAck_DcmEcuReset_DcmEcuReset Rte_SwitchAck_Dcm_DcmEcuReset_DcmEcuReset


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData RDataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData RDataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData RDataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData RDataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData RDataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData RDataServices_Authentification_Zone_ZA_Reserved_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData RDataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData RDataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData RDataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData RDataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data10ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData DataServices_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData DataServices_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData RDataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data20ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength(Dcm_OpStatusType OpStatus, P2VAR(uint16, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) DataLength); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data16ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_proximity_ST_DECT_CONNECT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead RDataServices_Etat_proximity_ST_DECT_CONNECT_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData RDataServices_Etat_proximity_ST_DECT_CONNECT_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData RDataServices_Etat_des_discontacteurs_de_la_batterie_traction_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_du_convertisseur_de_courant_DCDC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead RDataServices_Etat_du_convertisseur_de_courant_DCDC_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData RDataServices_Etat_du_convertisseur_de_courant_DCDC_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData RDataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData
#  define RTE_START_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPLFM_APPL_CODE) RDataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead RDataServices_LFM_Faults_DataRecord_ConditionCheckRead
#  define RTE_START_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPLFM_APPL_CODE) RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPLFM_APPL_CODE) RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_LFM_Faults_DataRecord_ReadData RDataServices_LFM_Faults_DataRecord_ReadData
#  define RTE_START_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPLFM_APPL_CODE) DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead
#  define RTE_START_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPLFM_APPL_CODE) DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPLFM_APPL_CODE) DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPLFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_LFM_HW_faults_DataRecord_ReadData DataServices_LFM_HW_faults_DataRecord_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Mahle_EOL_information_EOLData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Mahle_EOL_information_EOLData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data76ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Mahle_EOL_information_EOLData_ReadData DataServices_Mahle_EOL_information_EOLData_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Mahle_EOL_information_EOLData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Mahle_EOL_information_EOLData_WriteData(P2CONST(Dcm_Data76ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Mahle_EOL_information_EOLData_WriteData DataServices_Mahle_EOL_information_EOLData_WriteData
#  define Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead Rte_Call_Dcm_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead
#  define Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData Rte_Call_Dcm_DataServices_New_Identification_Parameters_1_DataRecord_ReadData
#  define Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead Rte_Call_Dcm_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead
#  define Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData Rte_Call_Dcm_DataServices_New_Identification_Parameters_2_DataRecord_ReadData
#  define Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead Rte_Call_Dcm_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead
#  define Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData Rte_Call_Dcm_DataServices_New_Identification_Parameters_3_DataRecord_ReadData
#  define Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead Rte_Call_Dcm_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead
#  define Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData Rte_Call_Dcm_DataServices_New_Identification_Parameters_4_DataRecord_ReadData
#  define Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead Rte_Call_Dcm_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead
#  define Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ReadData Rte_Call_Dcm_DataServices_New_Identification_Parameters_DataRecord_ReadData
#  define RTE_START_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPOFM_APPL_CODE) RDataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead RDataServices_OFM_Faults_DataRecord_ConditionCheckRead
#  define RTE_START_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPOFM_APPL_CODE) RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPOFM_APPL_CODE) RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_OFM_Faults_DataRecord_ReadData RDataServices_OFM_Faults_DataRecord_ReadData
#  define RTE_START_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPOFM_APPL_CODE) DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead
#  define RTE_START_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPOFM_APPL_CODE) DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPOFM_APPL_CODE) DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPOFM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_OFM_HW_faults_DataRecord_ReadData DataServices_OFM_HW_faults_DataRecord_ReadData
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead
#  define RTE_START_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPPCOM_APPL_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPPCOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData DataServices_PCOM_debug_signals_PCOMDebug_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_PLC_MAC_address_MAC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead DataServices_PLC_MAC_address_MAC_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_PLC_MAC_address_MAC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_PLC_MAC_address_MAC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data6ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_PLC_MAC_address_MAC_ReadData DataServices_PLC_MAC_address_MAC_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_PLC_MAC_address_MAC_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_PLC_MAC_address_MAC_WriteData(P2CONST(Dcm_Data6ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_PLC_MAC_address_MAC_WriteData DataServices_PLC_MAC_address_MAC_WriteData
#  define RTE_START_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPMSC_APPL_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead
#  define RTE_START_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPMSC_APPL_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPMSC_APPL_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData RDataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData RDataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData RDataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData RDataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData RDataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData RDataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData RDataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData RDataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData RDataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData RDataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData RDataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData RDataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData RDataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData RDataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData RDataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData RDataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData RDataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData RDataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData RDataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData RDataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData RDataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData RDataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData RDataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData RDataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData RDataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData RDataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData RDataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData RDataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData RDataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData RDataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData RDataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData RDataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData RDataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData RDataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData RDataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData RDataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData RDataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData RDataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData RDataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData RDataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData RDataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData RDataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SW_version_Major_version_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_SW_version_Major_version_ConditionCheckRead DataServices_SW_version_Major_version_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SW_version_Major_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SW_version_Major_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_SW_version_Major_version_ReadData DataServices_SW_version_Major_version_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SW_version_Minor_version_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_SW_version_Minor_version_ConditionCheckRead DataServices_SW_version_Minor_version_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SW_version_Minor_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SW_version_Minor_version_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_SW_version_Minor_version_ReadData DataServices_SW_version_Minor_version_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData DataServices_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData RDataServices_Temperature_du_convertisseur_de_courant_DCDC_484F075F_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData RDataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData RDataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData DataServices_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData DataServices_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData RDataServices_Tension_temperature_de_la_broche_1_rapi_6B35C481_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData RDataServices_Tension_temperature_de_la_broche_2_rapi_85133EA1_ReadData
#  define RTE_START_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead
#  define RTE_START_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData DataServices_V2G_States_Debug_CHGDebugData_ReadData
#  define RTE_START_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead
#  define RTE_START_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData RDataServices_V2G_States_Debug_CHGInternalState_ReadData
#  define RTE_START_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead
#  define RTE_START_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPCHG_APPL_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPCHG_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData RDataServices_V2G_States_Debug_SCCStateMachine_ReadData
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) Data); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData RDataServices_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop
#  define RTE_START_SEC_CTAPLAD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPLAD_APPL_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPLAD_APPL_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPLAD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults
#  define RTE_START_SEC_CTAPLAD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPLAD_APPL_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPLAD_APPL_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPLAD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start RoutineServices_Actuator_test_on_recharging_plug_lock_Start
#  define RTE_START_SEC_CTAPLAD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPLAD_APPL_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPLAD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop RoutineServices_Actuator_test_on_recharging_plug_lock_Stop
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start
#  define RTE_START_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPFCL_APPL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPFCL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop
#  define RTE_START_SEC_CTAPPLT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPPLT_APPL_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPLT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults
#  define RTE_START_SEC_CTAPPLT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPPLT_APPL_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPPLT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start
#  define RTE_START_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPMSC_APPL_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults
#  define RTE_START_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPMSC_APPL_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPMSC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start RoutineServices_Powerlatch_Information_Positioning_PIP_Start
#  define Rte_Call_RoutineServices_check_Memory_CM_RequestResults Rte_Call_Dcm_RoutineServices_check_Memory_CM_RequestResults
#  define Rte_Call_RoutineServices_check_Memory_CM_Start Rte_Call_Dcm_RoutineServices_check_Memory_CM_Start
#  define Rte_Call_RoutineServices_erase_Memory_EM_RequestResults Rte_Call_Dcm_RoutineServices_erase_Memory_EM_RequestResults
#  define Rte_Call_RoutineServices_erase_Memory_EM_Start Rte_Call_Dcm_RoutineServices_erase_Memory_EM_Start
#  define Rte_Call_SecurityAccess_UnlockedL1_CompareKey Rte_Call_Dcm_SecurityAccess_UnlockedL1_CompareKey
#  define Rte_Call_SecurityAccess_UnlockedL1_GetSeed Rte_Call_Dcm_SecurityAccess_UnlockedL1_GetSeed
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation
#  define RTE_START_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(uint8 SID, P2CONST(uint8, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  else
FUNC(Std_ReturnType, RTE_CTAPDGN_APPL_CODE) ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(uint8 SID, P2CONST(Dcm_Data4095ByteType, AUTOMATIC, RTE_CTAPDGN_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPDGN_APPL_VAR) ErrorCode); /* PRQA S 1330, 3451, 0786, 3449, 0624 */ /* MD_Rte_1330, MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  endif
#  define RTE_STOP_SEC_CTAPDGN_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication


# endif /* !defined(RTE_CORE) */


# define Dcm_START_SEC_CODE
# include "Dcm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Dcm_MainFunction Dcm_MainFunction
#  define RTE_RUNNABLE_GetActiveProtocol Dcm_GetActiveProtocol
#  define RTE_RUNNABLE_GetRequestKind Dcm_GetRequestKind
#  define RTE_RUNNABLE_GetSecurityLevel Dcm_GetSecurityLevel
#  define RTE_RUNNABLE_GetSesCtrlType Dcm_GetSesCtrlType
#  define RTE_RUNNABLE_ResetToDefaultSession Dcm_ResetToDefaultSession
#  define RTE_RUNNABLE_SetActiveDiagnostic Dcm_SetActiveDiagnostic
# endif

FUNC(void, Dcm_CODE) Dcm_MainFunction(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetActiveProtocol(P2VAR(Dcm_ProtocolType, AUTOMATIC, RTE_DCM_APPL_VAR) ActiveProtocol); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetRequestKind(uint16 TesterSourceAddress, P2VAR(Dcm_RequestKindType, AUTOMATIC, RTE_DCM_APPL_VAR) RequestKind); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetSecurityLevel(P2VAR(Dcm_SecLevelType, AUTOMATIC, RTE_DCM_APPL_VAR) SecLevel); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetSesCtrlType(P2VAR(Dcm_SesCtrlType, AUTOMATIC, RTE_DCM_APPL_VAR) SesCtrlType); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_ResetToDefaultSession(void); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_SetActiveDiagnostic(boolean active); /* PRQA S 3451, 0786, 3449, 0624 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define Dcm_STOP_SEC_CODE
# include "Dcm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DCMServices_E_NOT_OK (1U)

#  define RTE_E_DCMServices_E_OK (0U)

#  define RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_E_NOT_OK (1U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_E_NOT_OK (1U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_E_NOT_OK (1U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK (1U)

#  define RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_E_NOT_OK (1U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_E_NOT_OK (1U)

#  define RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_E_NOT_OK (1U)

#  define RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_E_NOT_OK (1U)

#  define RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_E_NOT_OK (1U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_E_NOT_OK (1U)

#  define RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK (1U)

#  define RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_Mahle_EOL_information_EOLData_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Mahle_EOL_information_EOLData_E_NOT_OK (1U)

#  define RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_New_Identification_Parameters_1_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_New_Identification_Parameters_2_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_New_Identification_Parameters_3_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_New_Identification_Parameters_4_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_New_Identification_Parameters_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_New_Identification_Parameters_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK (1U)

#  define RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK (1U)

#  define RTE_E_DataServices_PLC_MAC_address_MAC_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_PLC_MAC_address_MAC_E_NOT_OK (1U)

#  define RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_E_NOT_OK (1U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_E_NOT_OK (1U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_E_NOT_OK (1U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_E_NOT_OK (1U)

#  define RTE_E_DataServices_SW_version_Major_version_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_SW_version_Major_version_E_NOT_OK (1U)

#  define RTE_E_DataServices_SW_version_Minor_version_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_SW_version_Minor_version_E_NOT_OK (1U)

#  define RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_E_NOT_OK (1U)

#  define RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_E_NOT_OK (1U)

#  define RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_E_NOT_OK (1U)

#  define RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_E_NOT_OK (1U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_E_NOT_OK (1U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_E_NOT_OK (1U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK (1U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK (1U)

#  define RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK (1U)

#  define RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_check_Memory_CM_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_check_Memory_CM_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_check_Memory_CM_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_erase_Memory_EM_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_erase_Memory_EM_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_erase_Memory_EM_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_UnlockedL1_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_UnlockedL1_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_UnlockedL1_E_NOT_OK (1U)

#  define RTE_E_ServiceRequestNotification_E_NOT_OK (1U)

#  define RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED (8U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_DCM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_1330:  MISRA rule: Rule8.3
     Reason:     The RTE Generator uses default names for parameter identifiers of port defined arguments of service modules.
                 Therefore the parameter identifiers in the function declaration differs from those of the implementation of the BSW module.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
