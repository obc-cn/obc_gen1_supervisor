/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CtApPCOM.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Application header file for SW-C <CtApPCOM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_CTAPPCOM_H
# define RTE_CTAPPCOM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CtApPCOM_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault;
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd;
extern VAR(ABS_VehSpdValidFlag, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag;
extern VAR(BMS_AuxBattVolt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt;
extern VAR(BMS_CC2_connection_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status;
extern VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage;
extern VAR(BMS_FastChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt;
extern VAR(BMS_Fault, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault;
extern VAR(BMS_HighestChargeCurrentAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow;
extern VAR(BMS_HighestChargeVoltageAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow;
extern VAR(BMS_MainConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState;
extern VAR(BMS_OnBoardChargerEnable, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable;
extern VAR(BMS_QuickChargeConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState;
extern VAR(BMS_RelayOpenReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq;
extern VAR(BMS_SOC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC;
extern VAR(BMS_SlowChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt;
extern VAR(BMS_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage;
extern VAR(BSI_ChargeState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState;
extern VAR(BSI_ChargeTypeStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus;
extern VAR(BSI_LockedVehicle, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle;
extern VAR(BSI_MainWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup;
extern VAR(BSI_PostDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup;
extern VAR(BSI_PreDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup;
extern VAR(BSI_VCUHeatPumpWorkingMode, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode;
extern VAR(BSI_VCUSynchroGPC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC;
extern VAR(COUPURE_CONSO_CTP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP;
extern VAR(COUPURE_CONSO_CTPE2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2;
extern VAR(DATA_ACQ_JDD_BSI_2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2;
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT;
extern VAR(DCHV_ADC_NTC_MOD_5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5;
extern VAR(DCHV_ADC_NTC_MOD_6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6;
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT;
extern VAR(DCHV_Command_Current_Reference, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference;
extern VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus;
extern VAR(DCHV_IOM_ERR_CAP_FAIL_H, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H;
extern VAR(DCHV_IOM_ERR_CAP_FAIL_L, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L;
extern VAR(DCHV_IOM_ERR_OC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT;
extern VAR(DCHV_IOM_ERR_OC_NEG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG;
extern VAR(DCHV_IOM_ERR_OC_POS, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS;
extern VAR(DCHV_IOM_ERR_OT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT;
extern VAR(DCHV_IOM_ERR_OT_IN, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN;
extern VAR(DCHV_IOM_ERR_OT_NTC_MOD5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5;
extern VAR(DCHV_IOM_ERR_OT_NTC_MOD6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6;
extern VAR(DCHV_IOM_ERR_OV_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT;
extern VAR(DCHV_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V;
extern VAR(DCLV_Applied_Derating_Factor, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor;
extern VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus;
extern VAR(DCLV_Input_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current;
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage;
extern VAR(DCLV_Measured_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current;
extern VAR(DCLV_Measured_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage;
extern VAR(DCLV_OC_A_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT;
extern VAR(DCLV_OC_A_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT;
extern VAR(DCLV_OC_B_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT;
extern VAR(DCLV_OC_B_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT;
extern VAR(DCLV_OT_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT;
extern VAR(DCLV_OV_INT_A_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT;
extern VAR(DCLV_OV_INT_B_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT;
extern VAR(DCLV_OV_UV_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT;
extern VAR(DCLV_PWM_STOP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP;
extern VAR(DCLV_Power, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power;
extern VAR(DCLV_T_L_BUCK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK;
extern VAR(DCLV_T_PP_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A;
extern VAR(DCLV_T_PP_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B;
extern VAR(DCLV_T_SW_BUCK_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A;
extern VAR(DCLV_T_SW_BUCK_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B;
extern VAR(DCLV_T_TX_PP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP;
extern VAR(DIAG_INTEGRA_ELEC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC;
extern VAR(EFFAC_DEFAUT_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG;
extern VAR(MODE_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG;
extern VAR(PFC_IOM_ERR_OC_PH1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1;
extern VAR(PFC_IOM_ERR_OC_PH2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2;
extern VAR(PFC_IOM_ERR_OC_PH3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3;
extern VAR(PFC_IOM_ERR_OC_SHUNT1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1;
extern VAR(PFC_IOM_ERR_OC_SHUNT2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2;
extern VAR(PFC_IOM_ERR_OC_SHUNT3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3;
extern VAR(PFC_IOM_ERR_OT_NTC1_M1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1;
extern VAR(PFC_IOM_ERR_OT_NTC1_M3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3;
extern VAR(PFC_IOM_ERR_OT_NTC1_M4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4;
extern VAR(PFC_IOM_ERR_OV_DCLINK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK;
extern VAR(PFC_IOM_ERR_OV_VPH12, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12;
extern VAR(PFC_IOM_ERR_OV_VPH23, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23;
extern VAR(PFC_IOM_ERR_OV_VPH31, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31;
extern VAR(PFC_IOM_ERR_UVLO_ISO4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4;
extern VAR(PFC_IOM_ERR_UVLO_ISO7, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7;
extern VAR(PFC_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V;
extern VAR(PFC_IPH1_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1;
extern VAR(PFC_IPH2_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1;
extern VAR(PFC_IPH3_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1;
extern VAR(PFC_PFCStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus;
extern VAR(PFC_Temp_M1_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C;
extern VAR(PFC_Temp_M3_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C;
extern VAR(PFC_Temp_M4_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C;
extern VAR(PFC_VPH1_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz;
extern VAR(PFC_VPH2_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz;
extern VAR(PFC_VPH3_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz;
extern VAR(PFC_VPH12_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V;
extern VAR(PFC_VPH23_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V;
extern VAR(PFC_VPH31_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V;
extern VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V;
extern VAR(PFC_VPH23_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V;
extern VAR(PFC_VPH31_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V;
extern VAR(PFC_Vdclink_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V;
extern VAR(VCU_AccPedalPosition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition;
extern VAR(VCU_ActivedischargeCommand, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand;
extern VAR(VCU_CDEAccJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD;
extern VAR(VCU_CDEApcJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD;
extern VAR(VCU_CompteurRazGct, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct;
extern VAR(VCU_CptTemporel, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel;
extern VAR(VCU_DCDCActivation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation;
extern VAR(VCU_DCDCVoltageReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq;
extern VAR(VCU_DDEGMVSEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM;
extern VAR(VCU_DMDActivChiller, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller;
extern VAR(VCU_DMDMeap2SEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM;
extern VAR(VCU_DiagMuxOnPwt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt;
extern VAR(VCU_EPWT_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status;
extern VAR(VCU_ElecMeterSaturation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation;
extern VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
extern VAR(VCU_EtatPrincipSev, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev;
extern VAR(VCU_EtatReseauElec, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec;
extern VAR(VCU_Keyposition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition;
extern VAR(VCU_Kilometrage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage;
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest;
extern VAR(VCU_PosShuntJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD;
extern VAR(VCU_PrecondElecWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup;
extern VAR(VCU_StopDelayedHMIWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled;
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError;
extern VAR(DCDC_ActivedischargeSt, RTE_VAR_INIT) Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt;
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference;
extern VAR(DCDC_Fault, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault;
extern VAR(DCDC_FaultLampRequest, RTE_VAR_INIT) Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest;
extern VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed;
extern VAR(DCDC_InputCurrent, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent;
extern VAR(DCDC_InputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage;
extern VAR(DCDC_OBCMainContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq;
extern VAR(DCDC_OBCQuickChargeContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq;
extern VAR(DCDC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent;
extern VAR(DCDC_OutputVoltage, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage;
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status;
extern VAR(DCDC_Temperature, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature;
extern VAR(DCDC_VoltageReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference;
extern VAR(DCLV_Temp_Derating_Factor, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor;
extern VAR(DCLV_VoltageReference, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference;
extern VAR(EVSE_RTAB_STOP_CHARGE, RTE_VAR_INIT) Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE;
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange;
extern VAR(OBC_CP_connection_Status, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status;
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati;
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode;
extern VAR(OBC_ElockState, RTE_VAR_INIT) Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState;
extern VAR(OBC_Fault, RTE_VAR_INIT) Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault;
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed;
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt;
extern VAR(OBC_OBCStartSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt;
extern VAR(OBC_OBCTemp, RTE_VAR_INIT) Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp;
extern VAR(OBC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent;
extern VAR(OBC_OutputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage;
extern VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection;
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status;
extern VAR(SUP_CommandVDCLink_V, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V;
extern VAR(SUP_RequestDCLVStatus, RTE_VAR_INIT) Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus;
extern VAR(SUP_RequestPFCStatus, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus;
extern VAR(SUP_RequestStatusDCHV, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress;

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpCANComRequest_DeCANComRequest;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain;
extern VAR(DCDC_OBCDCDCRTPowerLoad, RTE_VAR_INIT) Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad;
extern VAR(DCDC_OVERTEMP, RTE_VAR_INIT) Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP;
extern VAR(EDITION_CALIB, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB;
extern VAR(EDITION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_0, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_1, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_2, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_3, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_4, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_5, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_6, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6;
extern VAR(NEW_JDD_OBC_DCDC_BYTE_7, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7;
extern VAR(OBC_CommunicationSt, RTE_VAR_INIT) Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt;
extern VAR(OBC_CoolingWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup;
extern VAR(OBC_DCChargingPlugAConnConf, RTE_VAR_INIT) Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf;
extern VAR(OBC_HVBattRechargeWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup;
extern VAR(OBC_HoldDiscontactorWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup;
extern VAR(OBC_PIStateInfoWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup;
extern VAR(OBC_PowerMax, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax;
extern VAR(OBC_PushChargeType, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType;
extern VAR(OBC_RechargeHMIState, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState;
extern VAR(OBC_SocketTempL, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL;
extern VAR(OBC_SocketTempN, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN;
extern VAR(SUPV_CoolingWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState;
extern VAR(SUPV_DTCRegistred, RTE_VAR_INIT) Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred;
extern VAR(SUPV_ECUElecStateRCD, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD;
extern VAR(SUPV_HVBattChargeWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState;
extern VAR(SUPV_HoldDiscontactorWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState;
extern VAR(SUPV_PIStateInfoWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState;
extern VAR(SUPV_PostDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState;
extern VAR(SUPV_PreDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState;
extern VAR(SUPV_PrecondElecWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState;
extern VAR(SUPV_RCDLineState, RTE_VAR_INIT) Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState;
extern VAR(SUPV_StopDelayedHMIWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState;
extern VAR(VERSION_APPLI, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI;
extern VAR(VERSION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT;
extern VAR(VERSION_SYSTEME, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME;
extern VAR(VERS_DATE2_ANNEE, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE;
extern VAR(VERS_DATE2_JOUR, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR;
extern VAR(VERS_DATE2_MOIS, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError;
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization;

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Debug1_1_Debug1_1 (0U)
#  define Rte_InitValue_Debug1_2_Debug1_2 (0U)
#  define Rte_InitValue_Debug1_3_Debug1_3 (0U)
#  define Rte_InitValue_Debug1_4_Debug1_4 (0U)
#  define Rte_InitValue_Debug1_5_Debug1_5 (0U)
#  define Rte_InitValue_Debug1_6_Debug1_6 (0U)
#  define Rte_InitValue_Debug1_7_Debug1_7 (0U)
#  define Rte_InitValue_Debug2_0_Debug2_0 (0U)
#  define Rte_InitValue_Debug2_1_Debug2_1 (0U)
#  define Rte_InitValue_Debug2_2_Debug2_2 (0U)
#  define Rte_InitValue_Debug2_3_Debug2_3 (0U)
#  define Rte_InitValue_Debug2_4_Debug2_4 (0U)
#  define Rte_InitValue_Debug2_5_Debug2_5 (0U)
#  define Rte_InitValue_Debug2_6_Debug2_6 (0U)
#  define Rte_InitValue_Debug2_7_Debug2_7 (0U)
#  define Rte_InitValue_Debug3_0_Debug3_0 (0U)
#  define Rte_InitValue_Debug3_1_Debug3_1 (0U)
#  define Rte_InitValue_Debug3_2_Debug3_2 (0U)
#  define Rte_InitValue_Debug3_3_Debug3_3 (0U)
#  define Rte_InitValue_Debug3_4_Debug3_4 (0U)
#  define Rte_InitValue_Debug3_5_Debug3_5 (0U)
#  define Rte_InitValue_Debug3_6_Debug3_6 (0U)
#  define Rte_InitValue_Debug3_7_Debug3_7 (0U)
#  define Rte_InitValue_Debug4_0_Debug4_0 (0U)
#  define Rte_InitValue_Debug4_1_Debug4_1 (0U)
#  define Rte_InitValue_Debug4_2_Debug4_2 (0U)
#  define Rte_InitValue_Debug4_3_Debug4_3 (0U)
#  define Rte_InitValue_Debug4_4_Debug4_4 (0U)
#  define Rte_InitValue_Debug4_5_Debug4_5 (0U)
#  define Rte_InitValue_Debug4_6_Debug4_6 (0U)
#  define Rte_InitValue_Debug4_7_Debug4_7 (0U)
#  define Rte_InitValue_Degug1_0_Degug1_0 (0U)
#  define Rte_InitValue_PpABS_VehSpd_ABS_VehSpd (0U)
#  define Rte_InitValue_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag (FALSE)
#  define Rte_InitValue_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault (FALSE)
#  define Rte_InitValue_PpBMS_AuxBattVolt_BMS_AuxBattVolt (0U)
#  define Rte_InitValue_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status (1U)
#  define Rte_InitValue_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage (0U)
#  define Rte_InitValue_PpBMS_FastChargeSt_BMS_FastChargeSt (0U)
#  define Rte_InitValue_PpBMS_Fault_BMS_Fault (0U)
#  define Rte_InitValue_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow (0U)
#  define Rte_InitValue_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow (0U)
#  define Rte_InitValue_PpBMS_MainConnectorState_BMS_MainConnectorState (0U)
#  define Rte_InitValue_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable (FALSE)
#  define Rte_InitValue_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState (FALSE)
#  define Rte_InitValue_PpBMS_RelayOpenReq_BMS_RelayOpenReq (FALSE)
#  define Rte_InitValue_PpBMS_SOC_BMS_SOC (0U)
#  define Rte_InitValue_PpBMS_SlowChargeSt_BMS_SlowChargeSt (0U)
#  define Rte_InitValue_PpBMS_Voltage_BMS_Voltage (0U)
#  define Rte_InitValue_PpBSI_ChargeState_BSI_ChargeState (3U)
#  define Rte_InitValue_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus (0U)
#  define Rte_InitValue_PpBSI_LockedVehicle_BSI_LockedVehicle (0U)
#  define Rte_InitValue_PpBSI_MainWakeup_BSI_MainWakeup (0U)
#  define Rte_InitValue_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup (FALSE)
#  define Rte_InitValue_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup (FALSE)
#  define Rte_InitValue_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode (0U)
#  define Rte_InitValue_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC (FALSE)
#  define Rte_InitValue_PpBootEnterData_DATA0 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA1 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA2 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA3 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA4 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA5 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA6 (0U)
#  define Rte_InitValue_PpBootEnterData_DATA7 (0U)
#  define Rte_InitValue_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions (FALSE)
#  define Rte_InitValue_PpBusOFFEnableConditions_DeBusOFFEnableConditions (FALSE)
#  define Rte_InitValue_PpBusOffCANFault_DeBusOffCANFault (FALSE)
#  define Rte_InitValue_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP (FALSE)
#  define Rte_InitValue_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 (FALSE)
#  define Rte_InitValue_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo (FALSE)
#  define Rte_InitValue_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU (FALSE)
#  define Rte_InitValue_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU (FALSE)
#  define Rte_InitValue_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 (0U)
#  define Rte_InitValue_PpDCDC_CurrentReference_DCDC_CurrentReference (0U)
#  define Rte_InitValue_PpDCDC_Fault_DCDC_Fault (0U)
#  define Rte_InitValue_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest (0U)
#  define Rte_InitValue_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed (FALSE)
#  define Rte_InitValue_PpDCDC_InputVoltage_DCDC_InputVoltage (0U)
#  define Rte_InitValue_PpDCDC_OVERTEMP_DCDC_OVERTEMP (FALSE)
#  define Rte_InitValue_PpDCDC_OutputVoltage_DCDC_OutputVoltage (0U)
#  define Rte_InitValue_PpDCDC_Status_DCDC_Status (0U)
#  define Rte_InitValue_PpDCDC_Temperature_DCDC_Temperature (40U)
#  define Rte_InitValue_PpDCDC_VoltageReference_DCDC_VoltageReference (2100U)
#  define Rte_InitValue_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT (0U)
#  define Rte_InitValue_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 (0U)
#  define Rte_InitValue_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 (0U)
#  define Rte_InitValue_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT (0U)
#  define Rte_InitValue_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference (0U)
#  define Rte_InitValue_PpDCHV_DCHVStatus_DCHV_DCHVStatus (0U)
#  define Rte_InitValue_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT (FALSE)
#  define Rte_InitValue_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V (FALSE)
#  define Rte_InitValue_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error (FALSE)
#  define Rte_InitValue_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor (0U)
#  define Rte_InitValue_PpDCLV_DCLVStatus_DCLV_DCLVStatus (0U)
#  define Rte_InitValue_PpDCLV_Input_Current_DCLV_Input_Current (0U)
#  define Rte_InitValue_PpDCLV_Input_Voltage_DCLV_Input_Voltage (0U)
#  define Rte_InitValue_PpDCLV_Measured_Current_DCLV_Measured_Current (0U)
#  define Rte_InitValue_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage (0U)
#  define Rte_InitValue_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OT_FAULT_DCLV_OT_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT (FALSE)
#  define Rte_InitValue_PpDCLV_PWM_STOP_DCLV_PWM_STOP (FALSE)
#  define Rte_InitValue_PpDCLV_Power_DCLV_Power (0U)
#  define Rte_InitValue_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK (0U)
#  define Rte_InitValue_PpDCLV_T_PP_A_DCLV_T_PP_A (0U)
#  define Rte_InitValue_PpDCLV_T_PP_B_DCLV_T_PP_B (0U)
#  define Rte_InitValue_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A (0U)
#  define Rte_InitValue_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B (0U)
#  define Rte_InitValue_PpDCLV_T_TX_PP_DCLV_T_TX_PP (0U)
#  define Rte_InitValue_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor (1024U)
#  define Rte_InitValue_PpDCLV_VoltageReference_DCLV_VoltageReference (0U)
#  define Rte_InitValue_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC (FALSE)
#  define Rte_InitValue_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552 (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo (FALSE)
#  define Rte_InitValue_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU (FALSE)
#  define Rte_InitValue_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault (FALSE)
#  define Rte_InitValue_PpEDITION_CALIB_EDITION_CALIB (0U)
#  define Rte_InitValue_PpEDITION_SOFT_EDITION_SOFT (0U)
#  define Rte_InitValue_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG (FALSE)
#  define Rte_InitValue_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE (FALSE)
#  define Rte_InitValue_PpInt_ABS_VehSpd_ABS_VehSpd (0U)
#  define Rte_InitValue_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag (FALSE)
#  define Rte_InitValue_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt (0U)
#  define Rte_InitValue_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status (0U)
#  define Rte_InitValue_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage (0U)
#  define Rte_InitValue_PpInt_BMS_FastChargeSt_BMS_FastChargeSt (0U)
#  define Rte_InitValue_PpInt_BMS_Fault_BMS_Fault (0U)
#  define Rte_InitValue_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow (0U)
#  define Rte_InitValue_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow (0U)
#  define Rte_InitValue_PpInt_BMS_MainConnectorState_BMS_MainConnectorState (0U)
#  define Rte_InitValue_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable (FALSE)
#  define Rte_InitValue_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState (FALSE)
#  define Rte_InitValue_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq (FALSE)
#  define Rte_InitValue_PpInt_BMS_SOC_BMS_SOC (0U)
#  define Rte_InitValue_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt (0U)
#  define Rte_InitValue_PpInt_BMS_Voltage_BMS_Voltage (0U)
#  define Rte_InitValue_PpInt_BSI_ChargeState_BSI_ChargeState (3U)
#  define Rte_InitValue_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus (0U)
#  define Rte_InitValue_PpInt_BSI_LockedVehicle_BSI_LockedVehicle (0U)
#  define Rte_InitValue_PpInt_BSI_MainWakeup_BSI_MainWakeup (0U)
#  define Rte_InitValue_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup (FALSE)
#  define Rte_InitValue_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup (FALSE)
#  define Rte_InitValue_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode (0U)
#  define Rte_InitValue_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC (FALSE)
#  define Rte_InitValue_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP (FALSE)
#  define Rte_InitValue_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 (FALSE)
#  define Rte_InitValue_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 (0U)
#  define Rte_InitValue_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT (0U)
#  define Rte_InitValue_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 (0U)
#  define Rte_InitValue_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 (65U)
#  define Rte_InitValue_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT (0U)
#  define Rte_InitValue_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference (0U)
#  define Rte_InitValue_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus (2U)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT (FALSE)
#  define Rte_InitValue_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V (FALSE)
#  define Rte_InitValue_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor (0U)
#  define Rte_InitValue_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus (2U)
#  define Rte_InitValue_PpInt_DCLV_Input_Current_DCLV_Input_Current (0U)
#  define Rte_InitValue_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage (0U)
#  define Rte_InitValue_PpInt_DCLV_Measured_Current_DCLV_Measured_Current (0U)
#  define Rte_InitValue_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage (0U)
#  define Rte_InitValue_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT (FALSE)
#  define Rte_InitValue_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP (FALSE)
#  define Rte_InitValue_PpInt_DCLV_Power_DCLV_Power (0U)
#  define Rte_InitValue_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK (0U)
#  define Rte_InitValue_PpInt_DCLV_T_PP_A_DCLV_T_PP_A (65U)
#  define Rte_InitValue_PpInt_DCLV_T_PP_B_DCLV_T_PP_B (65U)
#  define Rte_InitValue_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A (65U)
#  define Rte_InitValue_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B (65U)
#  define Rte_InitValue_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP (0U)
#  define Rte_InitValue_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC (FALSE)
#  define Rte_InitValue_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG (FALSE)
#  define Rte_InitValue_PpInt_MODE_DIAG_MODE_DIAG (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 (FALSE)
#  define Rte_InitValue_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V (FALSE)
#  define Rte_InitValue_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 (0U)
#  define Rte_InitValue_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 (0U)
#  define Rte_InitValue_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 (0U)
#  define Rte_InitValue_PpInt_PFC_PFCStatus_PFC_PFCStatus (8U)
#  define Rte_InitValue_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C (75U)
#  define Rte_InitValue_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C (0U)
#  define Rte_InitValue_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C (75U)
#  define Rte_InitValue_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V (0U)
#  define Rte_InitValue_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V (0U)
#  define Rte_InitValue_PpInt_PFC_Vdclink_V_PFC_Vdclink_V (0U)
#  define Rte_InitValue_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition (0U)
#  define Rte_InitValue_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand (FALSE)
#  define Rte_InitValue_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD (FALSE)
#  define Rte_InitValue_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD (0U)
#  define Rte_InitValue_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct (0U)
#  define Rte_InitValue_PpInt_VCU_CptTemporel_VCU_CptTemporel (0U)
#  define Rte_InitValue_PpInt_VCU_DCDCActivation_VCU_DCDCActivation (1U)
#  define Rte_InitValue_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq (0U)
#  define Rte_InitValue_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM (0U)
#  define Rte_InitValue_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller (FALSE)
#  define Rte_InitValue_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM (0U)
#  define Rte_InitValue_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt (FALSE)
#  define Rte_InitValue_PpInt_VCU_EPWT_Status_VCU_EPWT_Status (0U)
#  define Rte_InitValue_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation (FALSE)
#  define Rte_InitValue_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb (0U)
#  define Rte_InitValue_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev (0U)
#  define Rte_InitValue_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec (0U)
#  define Rte_InitValue_PpInt_VCU_Keyposition_VCU_Keyposition (0U)
#  define Rte_InitValue_PpInt_VCU_Kilometrage_VCU_Kilometrage (0U)
#  define Rte_InitValue_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest (0U)
#  define Rte_InitValue_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD (3U)
#  define Rte_InitValue_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup (FALSE)
#  define Rte_InitValue_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup (FALSE)
#  define Rte_InitValue_PpMODE_DIAG_MODE_DIAG (FALSE)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 (0U)
#  define Rte_InitValue_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 (0U)
#  define Rte_InitValue_PpOBCFramesReception_Error_DeOBCFramesReception_Error (FALSE)
#  define Rte_InitValue_PpOBC_ACRange_OBC_ACRange (0U)
#  define Rte_InitValue_PpOBC_CP_connection_Status_OBC_CP_connection_Status (0U)
#  define Rte_InitValue_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati (0U)
#  define Rte_InitValue_PpOBC_ChargingMode_OBC_ChargingMode (1U)
#  define Rte_InitValue_PpOBC_CommunicationSt_OBC_CommunicationSt (FALSE)
#  define Rte_InitValue_PpOBC_CoolingWakeup_OBC_CoolingWakeup (FALSE)
#  define Rte_InitValue_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf (FALSE)
#  define Rte_InitValue_PpOBC_ElockState_OBC_ElockState (0U)
#  define Rte_InitValue_PpOBC_Fault_OBC_Fault (0U)
#  define Rte_InitValue_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup (FALSE)
#  define Rte_InitValue_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed (FALSE)
#  define Rte_InitValue_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup (FALSE)
#  define Rte_InitValue_PpOBC_InputVoltageSt_OBC_InputVoltageSt (0U)
#  define Rte_InitValue_PpOBC_OBCStartSt_OBC_OBCStartSt (FALSE)
#  define Rte_InitValue_PpOBC_OBCTemp_OBC_OBCTemp (100U)
#  define Rte_InitValue_PpOBC_OutputCurrent_OBC_OutputCurrent (0U)
#  define Rte_InitValue_PpOBC_OutputVoltage_OBC_OutputVoltage (0U)
#  define Rte_InitValue_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup (FALSE)
#  define Rte_InitValue_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection (FALSE)
#  define Rte_InitValue_PpOBC_PowerMax_OBC_PowerMax (0U)
#  define Rte_InitValue_PpOBC_PushChargeType_OBC_PushChargeType (FALSE)
#  define Rte_InitValue_PpOBC_RechargeHMIState_OBC_RechargeHMIState (0U)
#  define Rte_InitValue_PpOBC_SocketTemp_OBC_SocketTempL (50U)
#  define Rte_InitValue_PpOBC_SocketTemp_OBC_SocketTempN (50U)
#  define Rte_InitValue_PpOBC_Status_OBC_Status (0U)
#  define Rte_InitValue_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 (FALSE)
#  define Rte_InitValue_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V (FALSE)
#  define Rte_InitValue_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 (0U)
#  define Rte_InitValue_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 (0U)
#  define Rte_InitValue_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 (0U)
#  define Rte_InitValue_PpPFC_PFCStatus_PFC_PFCStatus (0U)
#  define Rte_InitValue_PpPFC_Temp_M1_C_PFC_Temp_M1_C (0U)
#  define Rte_InitValue_PpPFC_Temp_M3_C_PFC_Temp_M3_C (0U)
#  define Rte_InitValue_PpPFC_Temp_M4_C_PFC_Temp_M4_C (0U)
#  define Rte_InitValue_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V (0U)
#  define Rte_InitValue_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V (0U)
#  define Rte_InitValue_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V (0U)
#  define Rte_InitValue_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz (0U)
#  define Rte_InitValue_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz (0U)
#  define Rte_InitValue_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz (0U)
#  define Rte_InitValue_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V (0U)
#  define Rte_InitValue_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V (0U)
#  define Rte_InitValue_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V (0U)
#  define Rte_InitValue_PpPFC_Vdclink_V_PFC_Vdclink_V (0U)
#  define Rte_InitValue_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled (FALSE)
#  define Rte_InitValue_PpSUPV_CoolingWupState_SUPV_CoolingWupState (FALSE)
#  define Rte_InitValue_PpSUPV_DTCRegistred_SUPV_DTCRegistred (FALSE)
#  define Rte_InitValue_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD (2U)
#  define Rte_InitValue_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState (FALSE)
#  define Rte_InitValue_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState (FALSE)
#  define Rte_InitValue_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState (FALSE)
#  define Rte_InitValue_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState (FALSE)
#  define Rte_InitValue_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState (FALSE)
#  define Rte_InitValue_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState (FALSE)
#  define Rte_InitValue_PpSUPV_RCDLineState_SUPV_RCDLineState (FALSE)
#  define Rte_InitValue_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState (FALSE)
#  define Rte_InitValue_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V (0U)
#  define Rte_InitValue_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus (0U)
#  define Rte_InitValue_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus (0U)
#  define Rte_InitValue_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV (0U)
#  define Rte_InitValue_PpVCU_AccPedalPosition_VCU_AccPedalPosition (0U)
#  define Rte_InitValue_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand (FALSE)
#  define Rte_InitValue_PpVCU_CDEAccJDD_VCU_CDEAccJDD (FALSE)
#  define Rte_InitValue_PpVCU_CDEApcJDD_VCU_CDEApcJDD (0U)
#  define Rte_InitValue_PpVCU_CompteurRazGct_VCU_CompteurRazGct (0U)
#  define Rte_InitValue_PpVCU_CptTemporel_VCU_CptTemporel (0U)
#  define Rte_InitValue_PpVCU_DCDCActivation_VCU_DCDCActivation (1U)
#  define Rte_InitValue_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq (28U)
#  define Rte_InitValue_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM (0U)
#  define Rte_InitValue_PpVCU_DMDActivChiller_VCU_DMDActivChiller (FALSE)
#  define Rte_InitValue_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM (0U)
#  define Rte_InitValue_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt (FALSE)
#  define Rte_InitValue_PpVCU_EPWT_Status_VCU_EPWT_Status (1U)
#  define Rte_InitValue_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation (FALSE)
#  define Rte_InitValue_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb (0U)
#  define Rte_InitValue_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev (0U)
#  define Rte_InitValue_PpVCU_EtatReseauElec_VCU_EtatReseauElec (0U)
#  define Rte_InitValue_PpVCU_Keyposition_VCU_Keyposition (0U)
#  define Rte_InitValue_PpVCU_Kilometrage_VCU_Kilometrage (0U)
#  define Rte_InitValue_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest (0U)
#  define Rte_InitValue_PpVCU_PosShuntJDD_VCU_PosShuntJDD (3U)
#  define Rte_InitValue_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup (FALSE)
#  define Rte_InitValue_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup (FALSE)
#  define Rte_InitValue_PpVERSION_APPLI_VERSION_APPLI (0U)
#  define Rte_InitValue_PpVERSION_SOFT_VERSION_SOFT (0U)
#  define Rte_InitValue_PpVERSION_SYSTEME_VERSION_SYSTEME (0U)
#  define Rte_InitValue_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE (0U)
#  define Rte_InitValue_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR (0U)
#  define Rte_InitValue_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS (0U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_1_Debug1_1(Debug1_1 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_2_Debug1_2(Debug1_2 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_3_Debug1_3(Debug1_3 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_4_Debug1_4(Debug1_4 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_5_Debug1_5(Debug1_5 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_6_Debug1_6(Debug1_6 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_7_Debug1_7(Debug1_7 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_0_Debug2_0(Debug2_0 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_1_Debug2_1(Debug2_1 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_2_Debug2_2(Debug2_2 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_3_Debug2_3(Debug2_3 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_4_Debug2_4(Debug2_4 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_5_Debug2_5(Debug2_5 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_6_Debug2_6(Debug2_6 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_7_Debug2_7(Debug2_7 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_0_Debug3_0(Debug3_0 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_1_Debug3_1(Debug3_1 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_2_Debug3_2(Debug3_2 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_3_Debug3_3(Debug3_3 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_4_Debug3_4(Debug3_4 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_5_Debug3_5(Debug3_5 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_6_Debug3_6(Debug3_6 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_7_Debug3_7(Debug3_7 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_0_Debug4_0(Debug4_0 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_1_Debug4_1(Debug4_1 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_2_Debug4_2(Debug4_2 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_3_Debug4_3(Debug4_3 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_4_Debug4_4(Debug4_4 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_5_Debug4_5(Debug4_5 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_6_Debug4_6(Debug4_6 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_7_Debug4_7(Debug4_7 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Degug1_0_Degug1_0(Degug1_0 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_Fault_DCDC_Fault(DCDC_Fault data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_Status_DCDC_Status(DCDC_Status data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_Temperature_DCDC_Temperature(DCDC_Temperature data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpEDITION_CALIB_EDITION_CALIB(EDITION_CALIB data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpEDITION_SOFT_EDITION_SOFT(EDITION_SOFT data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ACRange_OBC_ACRange(OBC_ACRange data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ElockState_OBC_ElockState(OBC_ElockState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_Fault_OBC_Fault(OBC_Fault data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PowerMax_OBC_PowerMax(OBC_PowerMax data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_Status_OBC_Status(OBC_Status data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSG_DC2_SG_DC2(P2CONST(SG_DC2, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERSION_APPLI_VERSION_APPLI(VERSION_APPLI data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERSION_SOFT_VERSION_SOFT(VERSION_SOFT data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PpABS_VehSpd_ABS_VehSpd Rte_Read_CtApPCOM_PpABS_VehSpd_ABS_VehSpd
#  define Rte_Read_CtApPCOM_PpABS_VehSpd_ABS_VehSpd(data) (Com_ReceiveSignal(ComConf_ComSignal_ABS_VehSpd_oVCU_PCANInfo_oE_CAN_c4f9375f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag Rte_Read_CtApPCOM_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag
#  define Rte_Read_CtApPCOM_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag(data) (Com_ReceiveSignal(ComConf_ComSignal_ABS_VehSpdValidFlag_oVCU_PCANInfo_oE_CAN_badcc650_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_AuxBattVolt_BMS_AuxBattVolt Rte_Read_CtApPCOM_PpBMS_AuxBattVolt_BMS_AuxBattVolt
#  define Rte_Read_CtApPCOM_PpBMS_AuxBattVolt_BMS_AuxBattVolt(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_AuxBattVolt_oBMS3_oE_CAN_3f0258cb_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status Rte_Read_CtApPCOM_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status
#  define Rte_Read_CtApPCOM_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_CC2_connection_Status_oBMS8_oE_CAN_ce1fe0ec_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage Rte_Read_CtApPCOM_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage
#  define Rte_Read_CtApPCOM_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_DCRelayVoltage_oBMS9_oE_CAN_c2a98509_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_FastChargeSt_BMS_FastChargeSt Rte_Read_CtApPCOM_PpBMS_FastChargeSt_BMS_FastChargeSt
#  define Rte_Read_CtApPCOM_PpBMS_FastChargeSt_BMS_FastChargeSt(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_FastChargeSt_oBMS6_oE_CAN_21564382_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_Fault_BMS_Fault Rte_Read_CtApPCOM_PpBMS_Fault_BMS_Fault
#  define Rte_Read_CtApPCOM_PpBMS_Fault_BMS_Fault(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_Fault_oBMS5_oE_CAN_87a5f6c7_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow Rte_Read_CtApPCOM_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow
#  define Rte_Read_CtApPCOM_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_HighestChargeCurrentAllow_oBMS6_oE_CAN_060c33c4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow Rte_Read_CtApPCOM_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow
#  define Rte_Read_CtApPCOM_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_HighestChargeVoltageAllow_oBMS6_oE_CAN_c465fe5e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_MainConnectorState_BMS_MainConnectorState Rte_Read_CtApPCOM_PpBMS_MainConnectorState_BMS_MainConnectorState
#  define Rte_Read_CtApPCOM_PpBMS_MainConnectorState_BMS_MainConnectorState(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_MainConnectorState_oBMS1_oE_CAN_db099b33_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable Rte_Read_CtApPCOM_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable
#  define Rte_Read_CtApPCOM_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_OnBoardChargerEnable_oBMS6_oE_CAN_b5381f70_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState Rte_Read_CtApPCOM_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState
#  define Rte_Read_CtApPCOM_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_QuickChargeConnectorState_oBMS1_oE_CAN_940005e5_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_RelayOpenReq_BMS_RelayOpenReq Rte_Read_CtApPCOM_PpBMS_RelayOpenReq_BMS_RelayOpenReq
#  define Rte_Read_CtApPCOM_PpBMS_RelayOpenReq_BMS_RelayOpenReq(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_RelayOpenReq_oBMS1_oE_CAN_9e155f98_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_SOC_BMS_SOC Rte_Read_CtApPCOM_PpBMS_SOC_BMS_SOC
#  define Rte_Read_CtApPCOM_PpBMS_SOC_BMS_SOC(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_SOC_oBMS1_oE_CAN_4aa6784d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_SlowChargeSt_BMS_SlowChargeSt Rte_Read_CtApPCOM_PpBMS_SlowChargeSt_BMS_SlowChargeSt
#  define Rte_Read_CtApPCOM_PpBMS_SlowChargeSt_BMS_SlowChargeSt(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_SlowChargeSt_oBMS6_oE_CAN_25b6b066_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBMS_Voltage_BMS_Voltage Rte_Read_CtApPCOM_PpBMS_Voltage_BMS_Voltage
#  define Rte_Read_CtApPCOM_PpBMS_Voltage_BMS_Voltage(data) (Com_ReceiveSignal(ComConf_ComSignal_BMS_Voltage_oBMS1_oE_CAN_2c5227ff_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_ChargeState_BSI_ChargeState Rte_Read_CtApPCOM_PpBSI_ChargeState_BSI_ChargeState
#  define Rte_Read_CtApPCOM_PpBSI_ChargeState_BSI_ChargeState(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_ChargeState_oBSIInfo_oE_CAN_be52c881_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus Rte_Read_CtApPCOM_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus
#  define Rte_Read_CtApPCOM_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_ChargeTypeStatus_oBSIInfo_oE_CAN_f023c7ab_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_LockedVehicle_BSI_LockedVehicle Rte_Read_CtApPCOM_PpBSI_LockedVehicle_BSI_LockedVehicle
#  define Rte_Read_CtApPCOM_PpBSI_LockedVehicle_BSI_LockedVehicle(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_LockedVehicle_oBSIInfo_oE_CAN_bee231f7_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_MainWakeup_BSI_MainWakeup Rte_Read_CtApPCOM_PpBSI_MainWakeup_BSI_MainWakeup
#  define Rte_Read_CtApPCOM_PpBSI_MainWakeup_BSI_MainWakeup(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_MainWakeup_oBSIInfo_oE_CAN_3acf26ac_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup Rte_Read_CtApPCOM_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup
#  define Rte_Read_CtApPCOM_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_PostDriveWakeup_oBSIInfo_oE_CAN_c679787c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup Rte_Read_CtApPCOM_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup
#  define Rte_Read_CtApPCOM_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_PreDriveWakeup_oBSIInfo_oE_CAN_10f8dc31_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode Rte_Read_CtApPCOM_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode
#  define Rte_Read_CtApPCOM_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_VCUHeatPumpWorkingMode_oBSIInfo_oE_CAN_56dcb551_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC Rte_Read_CtApPCOM_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC
#  define Rte_Read_CtApPCOM_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC(data) (Com_ReceiveSignal(ComConf_ComSignal_BSI_VCUSynchroGPC_oBSIInfo_oE_CAN_8a27e8bd_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState Rte_Read_CtApPCOM_PpBatteryVoltageState_DeBatteryVoltageState
#  define Rte_Read_CtApPCOM_PpBatteryVoltageState_DeBatteryVoltageState(data) (*(data) = Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA0 Rte_Read_CtApPCOM_PpBootEnterData_DATA0
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA0(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA0_oProgTool_SupEnterBoot_oInt_CAN_d0fc95c5_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA1 Rte_Read_CtApPCOM_PpBootEnterData_DATA1
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA1(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA1_oProgTool_SupEnterBoot_oInt_CAN_e60e0536_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA2 Rte_Read_CtApPCOM_PpBootEnterData_DATA2
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA2(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA2_oProgTool_SupEnterBoot_oInt_CAN_bd19b423_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA3 Rte_Read_CtApPCOM_PpBootEnterData_DATA3
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA3(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA3_oProgTool_SupEnterBoot_oInt_CAN_8beb24d0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA4 Rte_Read_CtApPCOM_PpBootEnterData_DATA4
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA4(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA4_oProgTool_SupEnterBoot_oInt_CAN_0b36d609_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA5 Rte_Read_CtApPCOM_PpBootEnterData_DATA5
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA5(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA5_oProgTool_SupEnterBoot_oInt_CAN_3dc446fa_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA6 Rte_Read_CtApPCOM_PpBootEnterData_DATA6
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA6(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA6_oProgTool_SupEnterBoot_oInt_CAN_66d3f7ef_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpBootEnterData_DATA7 Rte_Read_CtApPCOM_PpBootEnterData_DATA7
#  define Rte_Read_CtApPCOM_PpBootEnterData_DATA7(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA7_oProgTool_SupEnterBoot_oInt_CAN_5021671c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpCANComRequest_DeCANComRequest Rte_Read_CtApPCOM_PpCANComRequest_DeCANComRequest
#  define Rte_Read_CtApPCOM_PpCANComRequest_DeCANComRequest(data) (*(data) = Rte_CpApAEM_PpCANComRequest_DeCANComRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP Rte_Read_CtApPCOM_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP
#  define Rte_Read_CtApPCOM_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP(data) (Com_ReceiveSignal(ComConf_ComSignal_COUPURE_CONSO_CTP_oVCU_PCANInfo_oE_CAN_1a153f59_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 Rte_Read_CtApPCOM_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2
#  define Rte_Read_CtApPCOM_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(data) (Com_ReceiveSignal(ComConf_ComSignal_COUPURE_CONSO_CTPE2_oVCU_PCANInfo_oE_CAN_4e44e208_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 Rte_Read_CtApPCOM_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2
#  define Rte_Read_CtApPCOM_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(data) (Com_ReceiveSignal(ComConf_ComSignal_DATA_ACQ_JDD_BSI_2_oNEW_JDD_oE_CAN_16efcc6e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError Rte_Read_CtApPCOM_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError
#  define Rte_Read_CtApPCOM_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(data) (*(data) = Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError Rte_Read_CtApPCOM_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError
#  define Rte_Read_CtApPCOM_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(data) (*(data) = Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError Rte_Read_CtApPCOM_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError
#  define Rte_Read_CtApPCOM_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(data) (*(data) = Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError Rte_Read_CtApPCOM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError
#  define Rte_Read_CtApPCOM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(data) (*(data) = Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError Rte_Read_CtApPCOM_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError
#  define Rte_Read_CtApPCOM_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(data) (*(data) = Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Read_CtApPCOM_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Read_CtApPCOM_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_ADC_IOUT_oDCHV_Status1_oInt_CAN_db9652d6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 Rte_Read_CtApPCOM_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5
#  define Rte_Read_CtApPCOM_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_ADC_NTC_MOD_5_oDCHV_Status2_oInt_CAN_46287fbe_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 Rte_Read_CtApPCOM_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6
#  define Rte_Read_CtApPCOM_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_ADC_NTC_MOD_6_oDCHV_Status2_oInt_CAN_1e36d696_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Read_CtApPCOM_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Read_CtApPCOM_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_ADC_VOUT_oDCHV_Status1_oInt_CAN_95b5698f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference Rte_Read_CtApPCOM_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference
#  define Rte_Read_CtApPCOM_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_Command_Current_Reference_oDCHV_Status1_oInt_CAN_205fcb72_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_DCHVStatus_DCHV_DCHVStatus Rte_Read_CtApPCOM_PpDCHV_DCHVStatus_DCHV_DCHVStatus
#  define Rte_Read_CtApPCOM_PpDCHV_DCHVStatus_DCHV_DCHVStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_DCHVStatus_oDCHV_Status1_oInt_CAN_4479f96a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_CAP_FAIL_H_oDCHV_Fault_oInt_CAN_a3babff0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_CAP_FAIL_L_oDCHV_Fault_oInt_CAN_ff1b2cf0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OC_IOUT_oDCHV_Fault_oInt_CAN_2f168ffb_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OC_NEG_oDCHV_Fault_oInt_CAN_d091e3ea_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OC_POS_oDCHV_Fault_oInt_CAN_d5b3bbb7_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OT_oDCHV_Fault_oInt_CAN_33753ff7_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OT_IN_oDCHV_Fault_oInt_CAN_5338fe6d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OT_NTC_MOD5_oDCHV_Fault_oInt_CAN_05da4c61_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OT_NTC_MOD6_oDCHV_Fault_oInt_CAN_3ca2e121_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_OV_VOUT_oDCHV_Fault_oInt_CAN_c4e31cdb_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V
#  define Rte_Read_CtApPCOM_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(data) (Com_ReceiveSignal(ComConf_ComSignal_DCHV_IOM_ERR_UV_12V_oDCHV_Fault_oInt_CAN_7ceb9676_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor Rte_Read_CtApPCOM_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor
#  define Rte_Read_CtApPCOM_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_Applied_Derating_Factor_oDCLV_Status3_oInt_CAN_8199edb4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_DCLVStatus_DCLV_DCLVStatus Rte_Read_CtApPCOM_PpDCLV_DCLVStatus_DCLV_DCLVStatus
#  define Rte_Read_CtApPCOM_PpDCLV_DCLVStatus_DCLV_DCLVStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_DCLVStatus_oDCLV_Status1_oInt_CAN_aede0f5b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_Input_Current_DCLV_Input_Current Rte_Read_CtApPCOM_PpDCLV_Input_Current_DCLV_Input_Current
#  define Rte_Read_CtApPCOM_PpDCLV_Input_Current_DCLV_Input_Current(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_Input_Current_oDCLV_Status3_oInt_CAN_053924dc_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_Input_Voltage_DCLV_Input_Voltage Rte_Read_CtApPCOM_PpDCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Read_CtApPCOM_PpDCLV_Input_Voltage_DCLV_Input_Voltage(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_Input_Voltage_oDCLV_Status3_oInt_CAN_a46268d5_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_Measured_Current_DCLV_Measured_Current Rte_Read_CtApPCOM_PpDCLV_Measured_Current_DCLV_Measured_Current
#  define Rte_Read_CtApPCOM_PpDCLV_Measured_Current_DCLV_Measured_Current(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_Measured_Current_oDCLV_Status1_oInt_CAN_4e0e970b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage Rte_Read_CtApPCOM_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage
#  define Rte_Read_CtApPCOM_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_Measured_Voltage_oDCLV_Status1_oInt_CAN_ef55db02_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT Rte_Read_CtApPCOM_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OC_A_HV_FAULT_oDCLV_Fault_oInt_CAN_7ab3f331_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT Rte_Read_CtApPCOM_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OC_A_LV_FAULT_oDCLV_Fault_oInt_CAN_2ddd91e0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT Rte_Read_CtApPCOM_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OC_B_HV_FAULT_oDCLV_Fault_oInt_CAN_699bca42_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT Rte_Read_CtApPCOM_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OC_B_LV_FAULT_oDCLV_Fault_oInt_CAN_3ef5a893_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OT_FAULT_DCLV_OT_FAULT Rte_Read_CtApPCOM_PpDCLV_OT_FAULT_DCLV_OT_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OT_FAULT_DCLV_OT_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OT_FAULT_oDCLV_Fault_oInt_CAN_334ae1fe_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT Rte_Read_CtApPCOM_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OV_INT_A_FAULT_oDCLV_Fault_oInt_CAN_3c3a442a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT Rte_Read_CtApPCOM_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OV_INT_B_FAULT_oDCLV_Fault_oInt_CAN_3ee4430d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT Rte_Read_CtApPCOM_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT
#  define Rte_Read_CtApPCOM_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_OV_UV_HV_FAULT_oDCLV_Fault_oInt_CAN_3d935207_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_PWM_STOP_DCLV_PWM_STOP Rte_Read_CtApPCOM_PpDCLV_PWM_STOP_DCLV_PWM_STOP
#  define Rte_Read_CtApPCOM_PpDCLV_PWM_STOP_DCLV_PWM_STOP(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_PWM_STOP_oDCLV_Fault_oInt_CAN_13fc246e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_Power_DCLV_Power Rte_Read_CtApPCOM_PpDCLV_Power_DCLV_Power
#  define Rte_Read_CtApPCOM_PpDCLV_Power_DCLV_Power(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_Power_oDCLV_Status1_oInt_CAN_74948da2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK Rte_Read_CtApPCOM_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK
#  define Rte_Read_CtApPCOM_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_T_L_BUCK_oDCLV_Status2_oInt_CAN_f4d254b0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_T_PP_A_DCLV_T_PP_A Rte_Read_CtApPCOM_PpDCLV_T_PP_A_DCLV_T_PP_A
#  define Rte_Read_CtApPCOM_PpDCLV_T_PP_A_DCLV_T_PP_A(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_T_PP_A_oDCLV_Status2_oInt_CAN_33203dc6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_T_PP_B_DCLV_T_PP_B Rte_Read_CtApPCOM_PpDCLV_T_PP_B_DCLV_T_PP_B
#  define Rte_Read_CtApPCOM_PpDCLV_T_PP_B_DCLV_T_PP_B(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_T_PP_B_oDCLV_Status2_oInt_CAN_6b3e94ee_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A Rte_Read_CtApPCOM_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A
#  define Rte_Read_CtApPCOM_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_T_SW_BUCK_A_oDCLV_Status2_oInt_CAN_f172fb4d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B Rte_Read_CtApPCOM_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B
#  define Rte_Read_CtApPCOM_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_T_SW_BUCK_B_oDCLV_Status2_oInt_CAN_a96c5265_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDCLV_T_TX_PP_DCLV_T_TX_PP Rte_Read_CtApPCOM_PpDCLV_T_TX_PP_DCLV_T_TX_PP
#  define Rte_Read_CtApPCOM_PpDCLV_T_TX_PP_DCLV_T_TX_PP(data) (Com_ReceiveSignal(ComConf_ComSignal_DCLV_T_TX_PP_oDCLV_Status2_oInt_CAN_19721921_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC Rte_Read_CtApPCOM_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC
#  define Rte_Read_CtApPCOM_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(data) (Com_ReceiveSignal(ComConf_ComSignal_DIAG_INTEGRA_ELEC_oELECTRON_BSI_oE_CAN_fb8cbe6b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpECU_WakeupMain_DeECU_WakeupMain Rte_Read_CtApPCOM_PpECU_WakeupMain_DeECU_WakeupMain
#  define Rte_Read_CtApPCOM_PpECU_WakeupMain_DeECU_WakeupMain(data) (*(data) = Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG Rte_Read_CtApPCOM_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG
#  define Rte_Read_CtApPCOM_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(data) (Com_ReceiveSignal(ComConf_ComSignal_EFFAC_DEFAUT_DIAG_oELECTRON_BSI_oE_CAN_83a2b5c6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt Rte_Read_CtApPCOM_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt
#  define Rte_Read_CtApPCOM_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(data) (*(data) = Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_CurrentReference_DCDC_CurrentReference Rte_Read_CtApPCOM_PpInt_DCDC_CurrentReference_DCDC_CurrentReference
#  define Rte_Read_CtApPCOM_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(data) (*(data) = Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Fault_DCDC_Fault Rte_Read_CtApPCOM_PpInt_DCDC_Fault_DCDC_Fault
#  define Rte_Read_CtApPCOM_PpInt_DCDC_Fault_DCDC_Fault(data) (*(data) = Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest Rte_Read_CtApPCOM_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest
#  define Rte_Read_CtApPCOM_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(data) (*(data) = Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed Rte_Read_CtApPCOM_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed
#  define Rte_Read_CtApPCOM_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent Rte_Read_CtApPCOM_PpInt_DCDC_InputCurrent_DCDC_InputCurrent
#  define Rte_Read_CtApPCOM_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage Rte_Read_CtApPCOM_PpInt_DCDC_InputVoltage_DCDC_InputVoltage
#  define Rte_Read_CtApPCOM_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(data) (*(data) = Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad Rte_Read_CtApPCOM_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad
#  define Rte_Read_CtApPCOM_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(data) (*(data) = Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq Rte_Read_CtApPCOM_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq
#  define Rte_Read_CtApPCOM_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(data) (*(data) = Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq Rte_Read_CtApPCOM_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq
#  define Rte_Read_CtApPCOM_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(data) (*(data) = Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP Rte_Read_CtApPCOM_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP
#  define Rte_Read_CtApPCOM_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(data) (*(data) = Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent Rte_Read_CtApPCOM_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent
#  define Rte_Read_CtApPCOM_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(data) (*(data) = Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage Rte_Read_CtApPCOM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage
#  define Rte_Read_CtApPCOM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(data) (*(data) = Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Status_DCDC_Status Rte_Read_CtApPCOM_PpInt_DCDC_Status_DCDC_Status
#  define Rte_Read_CtApPCOM_PpInt_DCDC_Status_DCDC_Status(data) (*(data) = Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature Rte_Read_CtApPCOM_PpInt_DCDC_Temperature_DCDC_Temperature
#  define Rte_Read_CtApPCOM_PpInt_DCDC_Temperature_DCDC_Temperature(data) (*(data) = Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCDC_VoltageReference_DCDC_VoltageReference Rte_Read_CtApPCOM_PpInt_DCDC_VoltageReference_DCDC_VoltageReference
#  define Rte_Read_CtApPCOM_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(data) (*(data) = Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor Rte_Read_CtApPCOM_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor
#  define Rte_Read_CtApPCOM_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(data) (*(data) = Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_DCLV_VoltageReference_DCLV_VoltageReference Rte_Read_CtApPCOM_PpInt_DCLV_VoltageReference_DCLV_VoltageReference
#  define Rte_Read_CtApPCOM_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(data) (*(data) = Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_EDITION_CALIB_EDITION_CALIB Rte_Read_CtApPCOM_PpInt_EDITION_CALIB_EDITION_CALIB
#  define Rte_Read_CtApPCOM_PpInt_EDITION_CALIB_EDITION_CALIB(data) (*(data) = Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_EDITION_SOFT_EDITION_SOFT Rte_Read_CtApPCOM_PpInt_EDITION_SOFT_EDITION_SOFT
#  define Rte_Read_CtApPCOM_PpInt_EDITION_SOFT_EDITION_SOFT(data) (*(data) = Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE Rte_Read_CtApPCOM_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE
#  define Rte_Read_CtApPCOM_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(data) (*(data) = Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7
#  define Rte_Read_CtApPCOM_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(data) (*(data) = Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ACRange_OBC_ACRange Rte_Read_CtApPCOM_PpInt_OBC_ACRange_OBC_ACRange
#  define Rte_Read_CtApPCOM_PpInt_OBC_ACRange_OBC_ACRange(data) (*(data) = Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status Rte_Read_CtApPCOM_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status
#  define Rte_Read_CtApPCOM_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(data) (*(data) = Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Read_CtApPCOM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Read_CtApPCOM_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(data) (*(data) = Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode Rte_Read_CtApPCOM_PpInt_OBC_ChargingMode_OBC_ChargingMode
#  define Rte_Read_CtApPCOM_PpInt_OBC_ChargingMode_OBC_ChargingMode(data) (*(data) = Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_CommunicationSt_OBC_CommunicationSt Rte_Read_CtApPCOM_PpInt_OBC_CommunicationSt_OBC_CommunicationSt
#  define Rte_Read_CtApPCOM_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(data) (*(data) = Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup Rte_Read_CtApPCOM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup
#  define Rte_Read_CtApPCOM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(data) (*(data) = Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf Rte_Read_CtApPCOM_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf
#  define Rte_Read_CtApPCOM_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(data) (*(data) = Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_ElockState_OBC_ElockState Rte_Read_CtApPCOM_PpInt_OBC_ElockState_OBC_ElockState
#  define Rte_Read_CtApPCOM_PpInt_OBC_ElockState_OBC_ElockState(data) (*(data) = Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Fault_OBC_Fault Rte_Read_CtApPCOM_PpInt_OBC_Fault_OBC_Fault
#  define Rte_Read_CtApPCOM_PpInt_OBC_Fault_OBC_Fault(data) (*(data) = Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup Rte_Read_CtApPCOM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup
#  define Rte_Read_CtApPCOM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(data) (*(data) = Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed Rte_Read_CtApPCOM_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed
#  define Rte_Read_CtApPCOM_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(data) (*(data) = Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup Rte_Read_CtApPCOM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup
#  define Rte_Read_CtApPCOM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(data) (*(data) = Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt Rte_Read_CtApPCOM_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Read_CtApPCOM_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt Rte_Read_CtApPCOM_PpInt_OBC_OBCStartSt_OBC_OBCStartSt
#  define Rte_Read_CtApPCOM_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(data) (*(data) = Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_OBCTemp_OBC_OBCTemp Rte_Read_CtApPCOM_PpInt_OBC_OBCTemp_OBC_OBCTemp
#  define Rte_Read_CtApPCOM_PpInt_OBC_OBCTemp_OBC_OBCTemp(data) (*(data) = Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_OutputCurrent_OBC_OutputCurrent Rte_Read_CtApPCOM_PpInt_OBC_OutputCurrent_OBC_OutputCurrent
#  define Rte_Read_CtApPCOM_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(data) (*(data) = Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage Rte_Read_CtApPCOM_PpInt_OBC_OutputVoltage_OBC_OutputVoltage
#  define Rte_Read_CtApPCOM_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(data) (*(data) = Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup Rte_Read_CtApPCOM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup
#  define Rte_Read_CtApPCOM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(data) (*(data) = Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection Rte_Read_CtApPCOM_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection
#  define Rte_Read_CtApPCOM_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(data) (*(data) = Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax Rte_Read_CtApPCOM_PpInt_OBC_PowerMax_OBC_PowerMax
#  define Rte_Read_CtApPCOM_PpInt_OBC_PowerMax_OBC_PowerMax(data) (*(data) = Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType Rte_Read_CtApPCOM_PpInt_OBC_PushChargeType_OBC_PushChargeType
#  define Rte_Read_CtApPCOM_PpInt_OBC_PushChargeType_OBC_PushChargeType(data) (*(data) = Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState Rte_Read_CtApPCOM_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState
#  define Rte_Read_CtApPCOM_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(data) (*(data) = Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL Rte_Read_CtApPCOM_PpInt_OBC_SocketTemp_OBC_SocketTempL
#  define Rte_Read_CtApPCOM_PpInt_OBC_SocketTemp_OBC_SocketTempL(data) (*(data) = Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN Rte_Read_CtApPCOM_PpInt_OBC_SocketTemp_OBC_SocketTempN
#  define Rte_Read_CtApPCOM_PpInt_OBC_SocketTemp_OBC_SocketTempN(data) (*(data) = Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_OBC_Status_OBC_Status Rte_Read_CtApPCOM_PpInt_OBC_Status_OBC_Status
#  define Rte_Read_CtApPCOM_PpInt_OBC_Status_OBC_Status(data) (*(data) = Rte_CpApCHG_PpInt_OBC_Status_OBC_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState Rte_Read_CtApPCOM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(data) (*(data) = Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred Rte_Read_CtApPCOM_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred
#  define Rte_Read_CtApPCOM_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(data) (*(data) = Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD Rte_Read_CtApPCOM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD
#  define Rte_Read_CtApPCOM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(data) (*(data) = Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState Rte_Read_CtApPCOM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(data) (*(data) = Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState Rte_Read_CtApPCOM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(data) (*(data) = Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState Rte_Read_CtApPCOM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(data) (*(data) = Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState Rte_Read_CtApPCOM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(data) (*(data) = Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState Rte_Read_CtApPCOM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(data) (*(data) = Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState Rte_Read_CtApPCOM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(data) (*(data) = Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState Rte_Read_CtApPCOM_PpInt_SUPV_RCDLineState_SUPV_RCDLineState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(data) (*(data) = Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState Rte_Read_CtApPCOM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState
#  define Rte_Read_CtApPCOM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(data) (*(data) = Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V Rte_Read_CtApPCOM_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V
#  define Rte_Read_CtApPCOM_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(data) (*(data) = Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus Rte_Read_CtApPCOM_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus
#  define Rte_Read_CtApPCOM_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(data) (*(data) = Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus Rte_Read_CtApPCOM_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus
#  define Rte_Read_CtApPCOM_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(data) (*(data) = Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV Rte_Read_CtApPCOM_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV
#  define Rte_Read_CtApPCOM_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(data) (*(data) = Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VERSION_APPLI_VERSION_APPLI Rte_Read_CtApPCOM_PpInt_VERSION_APPLI_VERSION_APPLI
#  define Rte_Read_CtApPCOM_PpInt_VERSION_APPLI_VERSION_APPLI(data) (*(data) = Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VERSION_SOFT_VERSION_SOFT Rte_Read_CtApPCOM_PpInt_VERSION_SOFT_VERSION_SOFT
#  define Rte_Read_CtApPCOM_PpInt_VERSION_SOFT_VERSION_SOFT(data) (*(data) = Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VERSION_SYSTEME_VERSION_SYSTEME Rte_Read_CtApPCOM_PpInt_VERSION_SYSTEME_VERSION_SYSTEME
#  define Rte_Read_CtApPCOM_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(data) (*(data) = Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE Rte_Read_CtApPCOM_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE
#  define Rte_Read_CtApPCOM_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(data) (*(data) = Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR Rte_Read_CtApPCOM_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR
#  define Rte_Read_CtApPCOM_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(data) (*(data) = Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS Rte_Read_CtApPCOM_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS
#  define Rte_Read_CtApPCOM_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(data) (*(data) = Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpMODE_DIAG_MODE_DIAG Rte_Read_CtApPCOM_PpMODE_DIAG_MODE_DIAG
#  define Rte_Read_CtApPCOM_PpMODE_DIAG_MODE_DIAG(data) (Com_ReceiveSignal(ComConf_ComSignal_MODE_DIAG_oELECTRON_BSI_oE_CAN_f76ac80c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError Rte_Read_CtApPCOM_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError
#  define Rte_Read_CtApPCOM_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(data) (*(data) = Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError Rte_Read_CtApPCOM_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError
#  define Rte_Read_CtApPCOM_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(data) (*(data) = Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError Rte_Read_CtApPCOM_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError
#  define Rte_Read_CtApPCOM_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(data) (*(data) = Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError Rte_Read_CtApPCOM_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError
#  define Rte_Read_CtApPCOM_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(data) (*(data) = Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OC_PH1_oPFC_Fault_oInt_CAN_a122859f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OC_PH2_oPFC_Fault_oInt_CAN_da3c077c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OC_PH3_oPFC_Fault_oInt_CAN_45e684e2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OC_SHUNT1_oPFC_Fault_oInt_CAN_25f6ba61_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OC_SHUNT2_oPFC_Fault_oInt_CAN_5ee83882_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OC_SHUNT3_oPFC_Fault_oInt_CAN_c132bb1c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OT_NTC1_M1_oPFC_Fault_oInt_CAN_a0dec855_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OT_NTC1_M3_oPFC_Fault_oInt_CAN_441ac928_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OT_NTC1_M4_oPFC_Fault_oInt_CAN_2dfd4f70_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OV_DCLINK_oPFC_Fault_oInt_CAN_df590e67_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OV_VPH12_oPFC_Fault_oInt_CAN_70e217b6_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OV_VPH23_oPFC_Fault_oInt_CAN_d6403968_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_OV_VPH31_oPFC_Fault_oInt_CAN_25ac5cd5_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_UVLO_ISO4_oPFC_Fault_oInt_CAN_83f330a8_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 Rte_Read_CtApPCOM_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_UVLO_ISO7_oPFC_Fault_oInt_CAN_f8edb24b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V Rte_Read_CtApPCOM_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V
#  define Rte_Read_CtApPCOM_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IOM_ERR_UV_12V_oPFC_Fault_oInt_CAN_a7954067_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 Rte_Read_CtApPCOM_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1
#  define Rte_Read_CtApPCOM_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IPH1_RMS_0A1_oPFC_Status3_oInt_CAN_e736f57d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 Rte_Read_CtApPCOM_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1
#  define Rte_Read_CtApPCOM_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IPH2_RMS_0A1_oPFC_Status3_oInt_CAN_f41ecc0e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 Rte_Read_CtApPCOM_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1
#  define Rte_Read_CtApPCOM_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_IPH3_RMS_0A1_oPFC_Status3_oInt_CAN_faf924df_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_PFCStatus_PFC_PFCStatus Rte_Read_CtApPCOM_PpPFC_PFCStatus_PFC_PFCStatus
#  define Rte_Read_CtApPCOM_PpPFC_PFCStatus_PFC_PFCStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_PFCStatus_oPFC_Status1_oInt_CAN_4eddb451_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_Temp_M1_C_PFC_Temp_M1_C Rte_Read_CtApPCOM_PpPFC_Temp_M1_C_PFC_Temp_M1_C
#  define Rte_Read_CtApPCOM_PpPFC_Temp_M1_C_PFC_Temp_M1_C(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_Temp_M1_C_oPFC_Status1_oInt_CAN_23fc84dd_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_Temp_M3_C_PFC_Temp_M3_C Rte_Read_CtApPCOM_PpPFC_Temp_M3_C_PFC_Temp_M3_C
#  define Rte_Read_CtApPCOM_PpPFC_Temp_M3_C_PFC_Temp_M3_C(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_Temp_M3_C_oPFC_Status1_oInt_CAN_b39aa280_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_Temp_M4_C_PFC_Temp_M4_C Rte_Read_CtApPCOM_PpPFC_Temp_M4_C_PFC_Temp_M4_C
#  define Rte_Read_CtApPCOM_PpPFC_Temp_M4_C_PFC_Temp_M4_C(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_Temp_M4_C_oPFC_Status1_oInt_CAN_7dca5e28_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V Rte_Read_CtApPCOM_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V
#  define Rte_Read_CtApPCOM_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH12_RMS_V_oPFC_Status3_oInt_CAN_dbb6f081_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V Rte_Read_CtApPCOM_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V
#  define Rte_Read_CtApPCOM_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH23_RMS_V_oPFC_Status3_oInt_CAN_fc100cbd_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V Rte_Read_CtApPCOM_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V
#  define Rte_Read_CtApPCOM_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH31_RMS_V_oPFC_Status3_oInt_CAN_067f5dfd_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz Rte_Read_CtApPCOM_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz
#  define Rte_Read_CtApPCOM_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH1_Freq_Hz_oPFC_Status5_oInt_CAN_489242f9_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz Rte_Read_CtApPCOM_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz
#  define Rte_Read_CtApPCOM_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH2_Freq_Hz_oPFC_Status5_oInt_CAN_5bba7b8a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz Rte_Read_CtApPCOM_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz
#  define Rte_Read_CtApPCOM_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH3_Freq_Hz_oPFC_Status5_oInt_CAN_555d935b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V Rte_Read_CtApPCOM_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V
#  define Rte_Read_CtApPCOM_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH12_Peak_V_oPFC_Status2_oInt_CAN_2da0b0ab_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V Rte_Read_CtApPCOM_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V
#  define Rte_Read_CtApPCOM_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH23_Peak_V_oPFC_Status2_oInt_CAN_02e86ad0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V Rte_Read_CtApPCOM_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V
#  define Rte_Read_CtApPCOM_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_VPH31_Peak_V_oPFC_Status2_oInt_CAN_74ce4411_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPFC_Vdclink_V_PFC_Vdclink_V Rte_Read_CtApPCOM_PpPFC_Vdclink_V_PFC_Vdclink_V
#  define Rte_Read_CtApPCOM_PpPFC_Vdclink_V_PFC_Vdclink_V(data) (Com_ReceiveSignal(ComConf_ComSignal_PFC_Vdclink_V_oPFC_Status1_oInt_CAN_cb855707_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress Rte_Read_CtApPCOM_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress
#  define Rte_Read_CtApPCOM_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(data) (*(data) = Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpPlantModeState_DePlantModeState Rte_Read_CtApPCOM_PpPlantModeState_DePlantModeState
#  define Rte_Read_CtApPCOM_PpPlantModeState_DePlantModeState(data) (*(data) = Rte_CpApPLT_PpPlantModeState_DePlantModeState, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError Rte_Read_CtApPCOM_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError
#  define Rte_Read_CtApPCOM_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(data) (*(data) = Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization Rte_Read_CtApPCOM_PpShutdownAuthorization_DeShutdownAuthorization
#  define Rte_Read_CtApPCOM_PpShutdownAuthorization_DeShutdownAuthorization(data) (*(data) = Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_AccPedalPosition_VCU_AccPedalPosition Rte_Read_CtApPCOM_PpVCU_AccPedalPosition_VCU_AccPedalPosition
#  define Rte_Read_CtApPCOM_PpVCU_AccPedalPosition_VCU_AccPedalPosition(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_AccPedalPosition_oVCU_TU_oE_CAN_a12a1de4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand Rte_Read_CtApPCOM_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand
#  define Rte_Read_CtApPCOM_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_ActivedischargeCommand_oCtrlDCDC_oE_CAN_6f6ad9a3_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_CDEAccJDD_VCU_CDEAccJDD Rte_Read_CtApPCOM_PpVCU_CDEAccJDD_VCU_CDEAccJDD
#  define Rte_Read_CtApPCOM_PpVCU_CDEAccJDD_VCU_CDEAccJDD(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_CDEAccJDD_oVCU_BSI_Wakeup_oE_CAN_153d1d79_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_CDEApcJDD_VCU_CDEApcJDD Rte_Read_CtApPCOM_PpVCU_CDEApcJDD_VCU_CDEApcJDD
#  define Rte_Read_CtApPCOM_PpVCU_CDEApcJDD_VCU_CDEApcJDD(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_CDEApcJDD_oVCU_BSI_Wakeup_oE_CAN_0cbccb8e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_CompteurRazGct_VCU_CompteurRazGct Rte_Read_CtApPCOM_PpVCU_CompteurRazGct_VCU_CompteurRazGct
#  define Rte_Read_CtApPCOM_PpVCU_CompteurRazGct_VCU_CompteurRazGct(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_CompteurRazGct_oVCU_oE_CAN_34a3bca9_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_CptTemporel_VCU_CptTemporel Rte_Read_CtApPCOM_PpVCU_CptTemporel_VCU_CptTemporel
#  define Rte_Read_CtApPCOM_PpVCU_CptTemporel_VCU_CptTemporel(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_CptTemporel_oVCU_oE_CAN_80a510d2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_DCDCActivation_VCU_DCDCActivation Rte_Read_CtApPCOM_PpVCU_DCDCActivation_VCU_DCDCActivation
#  define Rte_Read_CtApPCOM_PpVCU_DCDCActivation_VCU_DCDCActivation(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_DCDCActivation_oCtrlDCDC_oE_CAN_541eed40_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq Rte_Read_CtApPCOM_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq
#  define Rte_Read_CtApPCOM_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_DCDCVoltageReq_oCtrlDCDC_oE_CAN_2adee191_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM Rte_Read_CtApPCOM_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM
#  define Rte_Read_CtApPCOM_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_DDEGMVSEEM_oVCU_BSI_Wakeup_oE_CAN_073a8f6a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_DMDActivChiller_VCU_DMDActivChiller Rte_Read_CtApPCOM_PpVCU_DMDActivChiller_VCU_DMDActivChiller
#  define Rte_Read_CtApPCOM_PpVCU_DMDActivChiller_VCU_DMDActivChiller(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_DMDActivChiller_oVCU_BSI_Wakeup_oE_CAN_eb265576_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM Rte_Read_CtApPCOM_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM
#  define Rte_Read_CtApPCOM_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_DMDMeap2SEEM_oVCU_BSI_Wakeup_oE_CAN_3b64713a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt Rte_Read_CtApPCOM_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt
#  define Rte_Read_CtApPCOM_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_DiagMuxOnPwt_oVCU_BSI_Wakeup_oE_CAN_712c6c84_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_EPWT_Status_VCU_EPWT_Status Rte_Read_CtApPCOM_PpVCU_EPWT_Status_VCU_EPWT_Status
#  define Rte_Read_CtApPCOM_PpVCU_EPWT_Status_VCU_EPWT_Status(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_EPWT_Status_oParkCommand_oE_CAN_c4868b19_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation Rte_Read_CtApPCOM_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation
#  define Rte_Read_CtApPCOM_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_ElecMeterSaturation_oVCU3_oE_CAN_c481c141_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb Rte_Read_CtApPCOM_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb
#  define Rte_Read_CtApPCOM_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_EtatGmpHyb_oVCU_BSI_Wakeup_oE_CAN_0076bb1d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev Rte_Read_CtApPCOM_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev
#  define Rte_Read_CtApPCOM_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_EtatPrincipSev_oVCU_BSI_Wakeup_oE_CAN_8c281110_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_EtatReseauElec_VCU_EtatReseauElec Rte_Read_CtApPCOM_PpVCU_EtatReseauElec_VCU_EtatReseauElec
#  define Rte_Read_CtApPCOM_PpVCU_EtatReseauElec_VCU_EtatReseauElec(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_EtatReseauElec_oVCU_BSI_Wakeup_oE_CAN_b805a640_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_Keyposition_VCU_Keyposition Rte_Read_CtApPCOM_PpVCU_Keyposition_VCU_Keyposition
#  define Rte_Read_CtApPCOM_PpVCU_Keyposition_VCU_Keyposition(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_Keyposition_oVCU2_oE_CAN_7b65e581_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_Kilometrage_VCU_Kilometrage Rte_Read_CtApPCOM_PpVCU_Kilometrage_VCU_Kilometrage
#  define Rte_Read_CtApPCOM_PpVCU_Kilometrage_VCU_Kilometrage(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_Kilometrage_oVCU_oE_CAN_90aff34b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Read_CtApPCOM_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Read_CtApPCOM_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_ModeEPSRequest_oVCU_BSI_Wakeup_oE_CAN_760ae031_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_PosShuntJDD_VCU_PosShuntJDD Rte_Read_CtApPCOM_PpVCU_PosShuntJDD_VCU_PosShuntJDD
#  define Rte_Read_CtApPCOM_PpVCU_PosShuntJDD_VCU_PosShuntJDD(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_PosShuntJDD_oVCU_BSI_Wakeup_oE_CAN_0978ac9d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup Rte_Read_CtApPCOM_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup
#  define Rte_Read_CtApPCOM_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_PrecondElecWakeup_oVCU_BSI_Wakeup_oE_CAN_6616770e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Read_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup Rte_Read_CtApPCOM_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup
#  define Rte_Read_CtApPCOM_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data) (Com_ReceiveSignal(ComConf_ComSignal_VCU_StopDelayedHMIWakeup_oVCU_BSI_Wakeup_oE_CAN_05bb1585_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_Debug1_1_Debug1_1 Rte_Write_CtApPCOM_Debug1_1_Debug1_1
#  define Rte_Write_Debug1_2_Debug1_2 Rte_Write_CtApPCOM_Debug1_2_Debug1_2
#  define Rte_Write_Debug1_3_Debug1_3 Rte_Write_CtApPCOM_Debug1_3_Debug1_3
#  define Rte_Write_Debug1_4_Debug1_4 Rte_Write_CtApPCOM_Debug1_4_Debug1_4
#  define Rte_Write_Debug1_5_Debug1_5 Rte_Write_CtApPCOM_Debug1_5_Debug1_5
#  define Rte_Write_Debug1_6_Debug1_6 Rte_Write_CtApPCOM_Debug1_6_Debug1_6
#  define Rte_Write_Debug1_7_Debug1_7 Rte_Write_CtApPCOM_Debug1_7_Debug1_7
#  define Rte_Write_Debug2_0_Debug2_0 Rte_Write_CtApPCOM_Debug2_0_Debug2_0
#  define Rte_Write_Debug2_1_Debug2_1 Rte_Write_CtApPCOM_Debug2_1_Debug2_1
#  define Rte_Write_Debug2_2_Debug2_2 Rte_Write_CtApPCOM_Debug2_2_Debug2_2
#  define Rte_Write_Debug2_3_Debug2_3 Rte_Write_CtApPCOM_Debug2_3_Debug2_3
#  define Rte_Write_Debug2_4_Debug2_4 Rte_Write_CtApPCOM_Debug2_4_Debug2_4
#  define Rte_Write_Debug2_5_Debug2_5 Rte_Write_CtApPCOM_Debug2_5_Debug2_5
#  define Rte_Write_Debug2_6_Debug2_6 Rte_Write_CtApPCOM_Debug2_6_Debug2_6
#  define Rte_Write_Debug2_7_Debug2_7 Rte_Write_CtApPCOM_Debug2_7_Debug2_7
#  define Rte_Write_Debug3_0_Debug3_0 Rte_Write_CtApPCOM_Debug3_0_Debug3_0
#  define Rte_Write_Debug3_1_Debug3_1 Rte_Write_CtApPCOM_Debug3_1_Debug3_1
#  define Rte_Write_Debug3_2_Debug3_2 Rte_Write_CtApPCOM_Debug3_2_Debug3_2
#  define Rte_Write_Debug3_3_Debug3_3 Rte_Write_CtApPCOM_Debug3_3_Debug3_3
#  define Rte_Write_Debug3_4_Debug3_4 Rte_Write_CtApPCOM_Debug3_4_Debug3_4
#  define Rte_Write_Debug3_5_Debug3_5 Rte_Write_CtApPCOM_Debug3_5_Debug3_5
#  define Rte_Write_Debug3_6_Debug3_6 Rte_Write_CtApPCOM_Debug3_6_Debug3_6
#  define Rte_Write_Debug3_7_Debug3_7 Rte_Write_CtApPCOM_Debug3_7_Debug3_7
#  define Rte_Write_Debug4_0_Debug4_0 Rte_Write_CtApPCOM_Debug4_0_Debug4_0
#  define Rte_Write_Debug4_1_Debug4_1 Rte_Write_CtApPCOM_Debug4_1_Debug4_1
#  define Rte_Write_Debug4_2_Debug4_2 Rte_Write_CtApPCOM_Debug4_2_Debug4_2
#  define Rte_Write_Debug4_3_Debug4_3 Rte_Write_CtApPCOM_Debug4_3_Debug4_3
#  define Rte_Write_Debug4_4_Debug4_4 Rte_Write_CtApPCOM_Debug4_4_Debug4_4
#  define Rte_Write_Debug4_5_Debug4_5 Rte_Write_CtApPCOM_Debug4_5_Debug4_5
#  define Rte_Write_Debug4_6_Debug4_6 Rte_Write_CtApPCOM_Debug4_6_Debug4_6
#  define Rte_Write_Debug4_7_Debug4_7 Rte_Write_CtApPCOM_Debug4_7_Debug4_7
#  define Rte_Write_Degug1_0_Degug1_0 Rte_Write_CtApPCOM_Degug1_0_Degug1_0
#  define Rte_Write_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault Rte_Write_CtApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault
#  define Rte_Write_CtApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(data) (Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions Rte_Write_CtApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions
#  define Rte_Write_CtApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(data) (Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpBusOFFEnableConditions_DeBusOFFEnableConditions Rte_Write_CtApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions
#  define Rte_Write_CtApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions(data) (Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpBusOffCANFault_DeBusOffCANFault Rte_Write_CtApPCOM_PpBusOffCANFault_DeBusOffCANFault
#  define Rte_Write_CtApPCOM_PpBusOffCANFault_DeBusOffCANFault(data) (Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo Rte_Write_CtApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo
#  define Rte_Write_CtApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(data) (Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU Rte_Write_CtApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU
#  define Rte_Write_CtApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(data) (Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU Rte_Write_CtApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU
#  define Rte_Write_CtApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(data) (Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCDC_CurrentReference_DCDC_CurrentReference Rte_Write_CtApPCOM_PpDCDC_CurrentReference_DCDC_CurrentReference
#  define Rte_Write_PpDCDC_Fault_DCDC_Fault Rte_Write_CtApPCOM_PpDCDC_Fault_DCDC_Fault
#  define Rte_Write_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest Rte_Write_CtApPCOM_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest
#  define Rte_Write_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed Rte_Write_CtApPCOM_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed
#  define Rte_Write_PpDCDC_InputVoltage_DCDC_InputVoltage Rte_Write_CtApPCOM_PpDCDC_InputVoltage_DCDC_InputVoltage
#  define Rte_Write_PpDCDC_OVERTEMP_DCDC_OVERTEMP Rte_Write_CtApPCOM_PpDCDC_OVERTEMP_DCDC_OVERTEMP
#  define Rte_Write_PpDCDC_OutputVoltage_DCDC_OutputVoltage Rte_Write_CtApPCOM_PpDCDC_OutputVoltage_DCDC_OutputVoltage
#  define Rte_Write_PpDCDC_Status_DCDC_Status Rte_Write_CtApPCOM_PpDCDC_Status_DCDC_Status
#  define Rte_Write_PpDCDC_Temperature_DCDC_Temperature Rte_Write_CtApPCOM_PpDCDC_Temperature_DCDC_Temperature
#  define Rte_Write_PpDCDC_VoltageReference_DCDC_VoltageReference Rte_Write_CtApPCOM_PpDCDC_VoltageReference_DCDC_VoltageReference
#  define Rte_Write_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error Rte_Write_CtApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error
#  define Rte_Write_CtApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(data) (Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor Rte_Write_CtApPCOM_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor
#  define Rte_Write_PpDCLV_VoltageReference_DCLV_VoltageReference Rte_Write_CtApPCOM_PpDCLV_VoltageReference_DCLV_VoltageReference
#  define Rte_Write_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault Rte_Write_CtApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault
#  define Rte_Write_CtApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(data) (Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552 Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU
#  define Rte_Write_CtApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(data) (Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault Rte_Write_CtApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault
#  define Rte_Write_CtApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(data) (Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(data) (Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault
#  define Rte_Write_CtApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(data) (Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpEDITION_CALIB_EDITION_CALIB Rte_Write_CtApPCOM_PpEDITION_CALIB_EDITION_CALIB
#  define Rte_Write_PpEDITION_SOFT_EDITION_SOFT Rte_Write_CtApPCOM_PpEDITION_SOFT_EDITION_SOFT
#  define Rte_Write_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE Rte_Write_CtApPCOM_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE
#  define Rte_Write_PpInt_ABS_VehSpd_ABS_VehSpd Rte_Write_CtApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd
#  define Rte_Write_CtApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd(data) (Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag Rte_Write_CtApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag
#  define Rte_Write_CtApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(data) (Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt Rte_Write_CtApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt
#  define Rte_Write_CtApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(data) (Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status Rte_Write_CtApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status
#  define Rte_Write_CtApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(data) (Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage Rte_Write_CtApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage
#  define Rte_Write_CtApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(data) (Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_FastChargeSt_BMS_FastChargeSt Rte_Write_CtApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt
#  define Rte_Write_CtApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(data) (Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_Fault_BMS_Fault Rte_Write_CtApPCOM_PpInt_BMS_Fault_BMS_Fault
#  define Rte_Write_CtApPCOM_PpInt_BMS_Fault_BMS_Fault(data) (Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow Rte_Write_CtApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow
#  define Rte_Write_CtApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(data) (Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow Rte_Write_CtApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow
#  define Rte_Write_CtApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(data) (Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_MainConnectorState_BMS_MainConnectorState Rte_Write_CtApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState
#  define Rte_Write_CtApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(data) (Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable Rte_Write_CtApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable
#  define Rte_Write_CtApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(data) (Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState Rte_Write_CtApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState
#  define Rte_Write_CtApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(data) (Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq Rte_Write_CtApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq
#  define Rte_Write_CtApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(data) (Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_SOC_BMS_SOC Rte_Write_CtApPCOM_PpInt_BMS_SOC_BMS_SOC
#  define Rte_Write_CtApPCOM_PpInt_BMS_SOC_BMS_SOC(data) (Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt Rte_Write_CtApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt
#  define Rte_Write_CtApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt(data) (Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BMS_Voltage_BMS_Voltage Rte_Write_CtApPCOM_PpInt_BMS_Voltage_BMS_Voltage
#  define Rte_Write_CtApPCOM_PpInt_BMS_Voltage_BMS_Voltage(data) (Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_ChargeState_BSI_ChargeState Rte_Write_CtApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState
#  define Rte_Write_CtApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState(data) (Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus Rte_Write_CtApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus
#  define Rte_Write_CtApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(data) (Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_LockedVehicle_BSI_LockedVehicle Rte_Write_CtApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle
#  define Rte_Write_CtApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(data) (Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_MainWakeup_BSI_MainWakeup Rte_Write_CtApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup
#  define Rte_Write_CtApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup(data) (Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup Rte_Write_CtApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup
#  define Rte_Write_CtApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(data) (Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup Rte_Write_CtApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup
#  define Rte_Write_CtApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(data) (Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode Rte_Write_CtApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode
#  define Rte_Write_CtApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(data) (Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC Rte_Write_CtApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC
#  define Rte_Write_CtApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(data) (Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP Rte_Write_CtApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP
#  define Rte_Write_CtApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(data) (Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 Rte_Write_CtApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2
#  define Rte_Write_CtApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(data) (Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 Rte_Write_CtApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2
#  define Rte_Write_CtApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(data) (Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT Rte_Write_CtApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT
#  define Rte_Write_CtApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(data) (Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 Rte_Write_CtApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5
#  define Rte_Write_CtApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(data) (Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 Rte_Write_CtApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6
#  define Rte_Write_CtApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(data) (Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT Rte_Write_CtApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT
#  define Rte_Write_CtApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(data) (Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference Rte_Write_CtApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference
#  define Rte_Write_CtApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(data) (Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus Rte_Write_CtApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus
#  define Rte_Write_CtApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(data) (Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V
#  define Rte_Write_CtApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(data) (Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor Rte_Write_CtApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor
#  define Rte_Write_CtApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(data) (Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus Rte_Write_CtApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus
#  define Rte_Write_CtApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(data) (Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Input_Current_DCLV_Input_Current Rte_Write_CtApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current
#  define Rte_Write_CtApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current(data) (Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage Rte_Write_CtApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage
#  define Rte_Write_CtApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(data) (Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Measured_Current_DCLV_Measured_Current Rte_Write_CtApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current
#  define Rte_Write_CtApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(data) (Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage Rte_Write_CtApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage
#  define Rte_Write_CtApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(data) (Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT Rte_Write_CtApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT
#  define Rte_Write_CtApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(data) (Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP Rte_Write_CtApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP
#  define Rte_Write_CtApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(data) (Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_Power_DCLV_Power Rte_Write_CtApPCOM_PpInt_DCLV_Power_DCLV_Power
#  define Rte_Write_CtApPCOM_PpInt_DCLV_Power_DCLV_Power(data) (Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK Rte_Write_CtApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK
#  define Rte_Write_CtApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(data) (Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_T_PP_A_DCLV_T_PP_A Rte_Write_CtApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A
#  define Rte_Write_CtApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(data) (Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_T_PP_B_DCLV_T_PP_B Rte_Write_CtApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B
#  define Rte_Write_CtApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(data) (Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A Rte_Write_CtApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A
#  define Rte_Write_CtApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(data) (Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B Rte_Write_CtApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B
#  define Rte_Write_CtApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(data) (Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP Rte_Write_CtApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP
#  define Rte_Write_CtApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(data) (Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC Rte_Write_CtApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC
#  define Rte_Write_CtApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(data) (Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG Rte_Write_CtApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG
#  define Rte_Write_CtApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(data) (Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_MODE_DIAG_MODE_DIAG Rte_Write_CtApPCOM_PpInt_MODE_DIAG_MODE_DIAG
#  define Rte_Write_CtApPCOM_PpInt_MODE_DIAG_MODE_DIAG(data) (Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V
#  define Rte_Write_CtApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(data) (Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 Rte_Write_CtApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1
#  define Rte_Write_CtApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(data) (Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 Rte_Write_CtApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1
#  define Rte_Write_CtApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(data) (Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 Rte_Write_CtApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1
#  define Rte_Write_CtApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(data) (Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_PFCStatus_PFC_PFCStatus Rte_Write_CtApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus
#  define Rte_Write_CtApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus(data) (Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C Rte_Write_CtApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C
#  define Rte_Write_CtApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(data) (Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C Rte_Write_CtApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C
#  define Rte_Write_CtApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(data) (Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C Rte_Write_CtApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C
#  define Rte_Write_CtApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(data) (Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz Rte_Write_CtApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(data) (Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz Rte_Write_CtApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(data) (Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz Rte_Write_CtApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(data) (Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V Rte_Write_CtApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(data) (Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V Rte_Write_CtApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(data) (Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V Rte_Write_CtApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(data) (Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V Rte_Write_CtApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(data) (Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V Rte_Write_CtApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(data) (Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V Rte_Write_CtApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(data) (Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_PFC_Vdclink_V_PFC_Vdclink_V Rte_Write_CtApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V
#  define Rte_Write_CtApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(data) (Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition Rte_Write_CtApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition
#  define Rte_Write_CtApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(data) (Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand Rte_Write_CtApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand
#  define Rte_Write_CtApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(data) (Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD Rte_Write_CtApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD
#  define Rte_Write_CtApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(data) (Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD Rte_Write_CtApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD
#  define Rte_Write_CtApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(data) (Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct Rte_Write_CtApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct
#  define Rte_Write_CtApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(data) (Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_CptTemporel_VCU_CptTemporel Rte_Write_CtApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel
#  define Rte_Write_CtApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel(data) (Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_DCDCActivation_VCU_DCDCActivation Rte_Write_CtApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation
#  define Rte_Write_CtApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(data) (Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq Rte_Write_CtApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq
#  define Rte_Write_CtApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(data) (Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM Rte_Write_CtApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM
#  define Rte_Write_CtApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(data) (Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller Rte_Write_CtApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller
#  define Rte_Write_CtApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(data) (Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM Rte_Write_CtApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM
#  define Rte_Write_CtApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(data) (Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt Rte_Write_CtApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt
#  define Rte_Write_CtApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(data) (Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_EPWT_Status_VCU_EPWT_Status Rte_Write_CtApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status
#  define Rte_Write_CtApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(data) (Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation Rte_Write_CtApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation
#  define Rte_Write_CtApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(data) (Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb Rte_Write_CtApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb
#  define Rte_Write_CtApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(data) (Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev Rte_Write_CtApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev
#  define Rte_Write_CtApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(data) (Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec Rte_Write_CtApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec
#  define Rte_Write_CtApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(data) (Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_Keyposition_VCU_Keyposition Rte_Write_CtApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition
#  define Rte_Write_CtApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition(data) (Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_Kilometrage_VCU_Kilometrage Rte_Write_CtApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage
#  define Rte_Write_CtApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage(data) (Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest Rte_Write_CtApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest
#  define Rte_Write_CtApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(data) (Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD Rte_Write_CtApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD
#  define Rte_Write_CtApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(data) (Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup Rte_Write_CtApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup
#  define Rte_Write_CtApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(data) (Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup Rte_Write_CtApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup
#  define Rte_Write_CtApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(data) (Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6
#  define Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7 Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7
#  define Rte_Write_PpOBCFramesReception_Error_DeOBCFramesReception_Error Rte_Write_CtApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error
#  define Rte_Write_CtApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error(data) (Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpOBC_ACRange_OBC_ACRange Rte_Write_CtApPCOM_PpOBC_ACRange_OBC_ACRange
#  define Rte_Write_PpOBC_CP_connection_Status_OBC_CP_connection_Status Rte_Write_CtApPCOM_PpOBC_CP_connection_Status_OBC_CP_connection_Status
#  define Rte_Write_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati Rte_Write_CtApPCOM_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati
#  define Rte_Write_PpOBC_ChargingMode_OBC_ChargingMode Rte_Write_CtApPCOM_PpOBC_ChargingMode_OBC_ChargingMode
#  define Rte_Write_PpOBC_CommunicationSt_OBC_CommunicationSt Rte_Write_CtApPCOM_PpOBC_CommunicationSt_OBC_CommunicationSt
#  define Rte_Write_PpOBC_CoolingWakeup_OBC_CoolingWakeup Rte_Write_CtApPCOM_PpOBC_CoolingWakeup_OBC_CoolingWakeup
#  define Rte_Write_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf Rte_Write_CtApPCOM_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf
#  define Rte_Write_PpOBC_ElockState_OBC_ElockState Rte_Write_CtApPCOM_PpOBC_ElockState_OBC_ElockState
#  define Rte_Write_PpOBC_Fault_OBC_Fault Rte_Write_CtApPCOM_PpOBC_Fault_OBC_Fault
#  define Rte_Write_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup Rte_Write_CtApPCOM_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup
#  define Rte_Write_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed Rte_Write_CtApPCOM_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed
#  define Rte_Write_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup Rte_Write_CtApPCOM_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup
#  define Rte_Write_PpOBC_InputVoltageSt_OBC_InputVoltageSt Rte_Write_CtApPCOM_PpOBC_InputVoltageSt_OBC_InputVoltageSt
#  define Rte_Write_PpOBC_OBCStartSt_OBC_OBCStartSt Rte_Write_CtApPCOM_PpOBC_OBCStartSt_OBC_OBCStartSt
#  define Rte_Write_PpOBC_OBCTemp_OBC_OBCTemp Rte_Write_CtApPCOM_PpOBC_OBCTemp_OBC_OBCTemp
#  define Rte_Write_PpOBC_OutputCurrent_OBC_OutputCurrent Rte_Write_CtApPCOM_PpOBC_OutputCurrent_OBC_OutputCurrent
#  define Rte_Write_PpOBC_OutputVoltage_OBC_OutputVoltage Rte_Write_CtApPCOM_PpOBC_OutputVoltage_OBC_OutputVoltage
#  define Rte_Write_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup Rte_Write_CtApPCOM_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup
#  define Rte_Write_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection Rte_Write_CtApPCOM_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection
#  define Rte_Write_PpOBC_PowerMax_OBC_PowerMax Rte_Write_CtApPCOM_PpOBC_PowerMax_OBC_PowerMax
#  define Rte_Write_PpOBC_PushChargeType_OBC_PushChargeType Rte_Write_CtApPCOM_PpOBC_PushChargeType_OBC_PushChargeType
#  define Rte_Write_PpOBC_RechargeHMIState_OBC_RechargeHMIState Rte_Write_CtApPCOM_PpOBC_RechargeHMIState_OBC_RechargeHMIState
#  define Rte_Write_PpOBC_SocketTemp_OBC_SocketTempL Rte_Write_CtApPCOM_PpOBC_SocketTemp_OBC_SocketTempL
#  define Rte_Write_PpOBC_SocketTemp_OBC_SocketTempN Rte_Write_CtApPCOM_PpOBC_SocketTemp_OBC_SocketTempN
#  define Rte_Write_PpOBC_Status_OBC_Status Rte_Write_CtApPCOM_PpOBC_Status_OBC_Status
#  define Rte_Write_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled Rte_Write_CtApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled
#  define Rte_Write_CtApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(data) (Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Write_PpSG_DC2_SG_DC2 Rte_Write_CtApPCOM_PpSG_DC2_SG_DC2
#  define Rte_Write_PpSUPV_CoolingWupState_SUPV_CoolingWupState Rte_Write_CtApPCOM_PpSUPV_CoolingWupState_SUPV_CoolingWupState
#  define Rte_Write_PpSUPV_DTCRegistred_SUPV_DTCRegistred Rte_Write_CtApPCOM_PpSUPV_DTCRegistred_SUPV_DTCRegistred
#  define Rte_Write_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD Rte_Write_CtApPCOM_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD
#  define Rte_Write_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState Rte_Write_CtApPCOM_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState
#  define Rte_Write_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState Rte_Write_CtApPCOM_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState
#  define Rte_Write_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState Rte_Write_CtApPCOM_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState
#  define Rte_Write_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState Rte_Write_CtApPCOM_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState
#  define Rte_Write_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState Rte_Write_CtApPCOM_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState
#  define Rte_Write_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState Rte_Write_CtApPCOM_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState
#  define Rte_Write_PpSUPV_RCDLineState_SUPV_RCDLineState Rte_Write_CtApPCOM_PpSUPV_RCDLineState_SUPV_RCDLineState
#  define Rte_Write_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState Rte_Write_CtApPCOM_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState
#  define Rte_Write_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V Rte_Write_CtApPCOM_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V
#  define Rte_Write_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus Rte_Write_CtApPCOM_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus
#  define Rte_Write_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus Rte_Write_CtApPCOM_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus
#  define Rte_Write_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV Rte_Write_CtApPCOM_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV
#  define Rte_Write_PpVERSION_APPLI_VERSION_APPLI Rte_Write_CtApPCOM_PpVERSION_APPLI_VERSION_APPLI
#  define Rte_Write_PpVERSION_SOFT_VERSION_SOFT Rte_Write_CtApPCOM_PpVERSION_SOFT_VERSION_SOFT
#  define Rte_Write_PpVERSION_SYSTEME_VERSION_SYSTEME Rte_Write_CtApPCOM_PpVERSION_SYSTEME_VERSION_SYSTEME
#  define Rte_Write_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE Rte_Write_CtApPCOM_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE
#  define Rte_Write_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR Rte_Write_CtApPCOM_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR
#  define Rte_Write_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS Rte_Write_CtApPCOM_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_GetEventFailed(Dem_EventIdType parg0, P2VAR(boolean, AUTOMATIC, RTE_DEMSATELLITE_0_APPL_VAR) EventFailed); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)50, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_GetEventUdsStatus(Dem_EventIdType parg0, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMSATELLITE_0_APPL_VAR) UDSStatusByte); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)50, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)51, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)51, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_ReadBlock(NvM_BlockIdType parg0, dtRef_VOID DstPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_ReadBlock(arg1) (NvM_ReadBlock((NvM_BlockIdType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_SetRamBlockStatus(NvM_BlockIdType parg0, boolean RamBlockStatus); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_SetRamBlockStatus(arg1) (NvM_SetRamBlockStatus((NvM_BlockIdType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_NVM_APPL_CODE) NvM_WriteBlock(NvM_BlockIdType parg0, dtRef_const_VOID SrcPtr); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_NVM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_WriteBlock(arg1) (NvM_WriteBlock((NvM_BlockIdType)9, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_COMM_APPL_CODE) ComM_GetCurrentComMode(ComM_UserHandleType parg0, P2VAR(ComM_ModeType, AUTOMATIC, RTE_COMM_APPL_VAR) ComMode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpComMUserNeed_ECANUserRequest_GetCurrentComMode(arg1) (ComM_GetCurrentComMode((ComM_UserHandleType)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_COMM_APPL_CODE) ComM_RequestComMode(ComM_UserHandleType parg0, ComM_ModeType ComMode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_COMM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(arg1) (ComM_RequestComMode((ComM_UserHandleType)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(arg1) (ComM_RequestComMode((ComM_UserHandleType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_ECUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_ECUM_APPL_CODE) EcuM_ReleaseRUN(EcuM_UserType parg0); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_ECUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN() (EcuM_ReleaseRUN((EcuM_UserType)0)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_ECUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_ECUM_APPL_CODE) EcuM_RequestRUN(EcuM_UserType parg0); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_ECUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN() (EcuM_RequestRUN((EcuM_UserType)0)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPJDD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPJDD_APPL_CODE) PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPJDD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd() (PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPJDD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPJDD_APPL_CODE) PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPJDD_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd() (PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTHWABSIOM_APPL_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTHWABSIOM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2) (RPpSetDebugPinValue_OpSetDebugPinValue(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_CTAPWUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(void, RTE_CTAPWUM_APPL_CODE) RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_CTAPWUM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_PpWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(arg1) (RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define RTE_START_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
FUNC(Std_ReturnType, RTE_WDGM_OSAPPLICATION_ASILB_APPL_CODE) WdgM_CheckpointReached(WdgM_SupervisedEntityIdType parg0, WdgM_CheckpointIdType CPID); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
#  define RTE_STOP_SEC_WDGM_OSAPPLICATION_ASILB_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define Rte_Call_alive_WdgMSupervisedEntity_CheckpointReached(arg1) (WdgM_CheckpointReached((WdgM_SupervisedEntityIdType)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(arg1) (WdgM_CheckpointReached((WdgM_SupervisedEntityIdType)1, arg1)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */


/**********************************************************************************************************************
 * Rte_CData (SW-C local calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT

#   define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

#  define Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_REFERENCE_HORAIRE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_REFERENCE_HORAIRE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_Initial_Value_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_Initial_Value_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_0F0_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_0F0_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_0F0_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_0F0_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_125_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_125_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_125_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_125_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_127_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_127_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_127_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_127_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_129_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_129_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_129_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_129_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_17B_ChkSumConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_17B_ChkSumConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_17B_ChkSumHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_17B_ChkSumHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_17B_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_17B_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_17B_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_17B_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_17B_RCConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_17B_RCConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_17B_RCHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_17B_RCHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_27A_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_27A_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_27A_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_27A_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_31B_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_31B_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_31B_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_31B_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_31E_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_31E_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_31E_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_31E_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_359_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_359_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_359_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_359_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_361_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_361_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_361_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_361_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_372_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_372_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_372_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_372_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_37E_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_37E_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_37E_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_37E_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_382_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_382_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_382_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_382_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_486_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_486_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_486_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_486_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_552_LostFrameConfirm() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_552_LostFrameConfirm) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalFrame_552_LostFrameHeal() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalFrame_552_LostFrameHeal) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS5_359_Initial_Value_BMS_Fault() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS5_359_Initial_Value_BMS_Fault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Fault() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_Fault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Temperature() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC1_345_Initial_Value_DCDC_Temperature) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_NOMBRE_TRAMES() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_NOMBRE_TRAMES) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Fault() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_Fault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC1_3A3_Initial_Value_OBC_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalT_INHIB_DIAG_COM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalT_INHIB_DIAG_COM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeBeforeRunningDiagBSIInfo() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalTimeBeforeRunningDiagBSIInfo) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeBeforeRunningDiagEVCU() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalTimeBeforeRunningDiagEVCU) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTimeBeforeRunningDiagTBMU() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalTimeBeforeRunningDiagTBMU) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalTo_DgMux_Prod() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalTo_DgMux_Prod) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

#  define Rte_CData_CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition() (Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP.Rte_Calprm_CtApPCOM.CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS1_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS3_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS5_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS6_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS8_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS9_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSIInfo_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCtrlDCDC_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimParkCommand_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU2_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU3_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_552_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_PCANInfo_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_TU_LostFrameCounter;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS1_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS3_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS5_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS6_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS8_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS9_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSIInfo_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCtrlDCDC_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimParkCommand_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU2_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU3_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_552_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_PCANInfo_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_TU_LostFramePrefault;
extern VAR(IdtPCOMNvMArray, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_NvPCOMBlockNeed_MirrorBlock;

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  endif

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS1_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBMS1_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS3_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBMS3_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS5_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBMS5_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS6_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBMS6_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS8_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBMS8_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS9_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBMS9_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBSIInfo_LostFrameCounter() \
  (&Rte_CpApPCOM_PimBSIInfo_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCtrlDCDC_LostFrameCounter() \
  (&Rte_CpApPCOM_PimCtrlDCDC_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimParkCommand_LostFrameCounter() \
  (&Rte_CpApPCOM_PimParkCommand_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU2_LostFrameCounter() \
  (&Rte_CpApPCOM_PimVCU2_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU3_LostFrameCounter() \
  (&Rte_CpApPCOM_PimVCU3_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_552_LostFrameCounter() \
  (&Rte_CpApPCOM_PimVCU_552_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_BSI_Wakeup_LostFrameCounter() \
  (&Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_PCANInfo_LostFrameCounter() \
  (&Rte_CpApPCOM_PimVCU_PCANInfo_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_TU_LostFrameCounter() \
  (&Rte_CpApPCOM_PimVCU_TU_LostFrameCounter)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimABS_VehSpd_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimABS_VehSpd_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_SOC_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_SOC_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_SOC_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_SOC_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_Voltage_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_Voltage_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimKILOMETRAGE_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimKILOMETRAGE_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm() \
  (&Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal() \
  (&Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimABS_VehSpd_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimABS_VehSpd_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS1_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBMS1_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS3_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBMS3_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS5_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBMS5_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS6_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBMS6_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS8_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBMS8_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS9_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBMS9_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_MainConnectorState_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_SOC_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimBMS_SOC_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_SOC_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimBMS_SOC_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_Voltage_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBMS_Voltage_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimBMS_Voltage_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBSIInfo_LostFramePrefault() \
  (&Rte_CpApPCOM_PimBSIInfo_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCPT_TEMPOREL_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimCtrlDCDC_LostFramePrefault() \
  (&Rte_CpApPCOM_PimCtrlDCDC_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDDE_GMV_SEEM_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_GMP_HYB_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimKILOMETRAGE_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimKILOMETRAGE_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimParkCommand_LostFramePrefault() \
  (&Rte_CpApPCOM_PimParkCommand_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU2_LostFramePrefault() \
  (&Rte_CpApPCOM_PimVCU2_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU3_LostFramePrefault() \
  (&Rte_CpApPCOM_PimVCU3_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_552_LostFramePrefault() \
  (&Rte_CpApPCOM_PimVCU_552_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_AccPedalPosition_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_BSI_Wakeup_LostFramePrefault() \
  (&Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCActivation_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_EPWT_Status_InvalidValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault() \
  (&Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_PCANInfo_LostFramePrefault() \
  (&Rte_CpApPCOM_PimVCU_PCANInfo_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  define Rte_Pim_PimVCU_TU_LostFramePrefault() \
  (&Rte_CpApPCOM_PimVCU_TU_LostFramePrefault)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_FctLikeMacro */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_NvPCOMBlockNeed_MirrorBlock() (&((*RtePim_NvPCOMBlockNeed_MirrorBlock())[0]))
#  else
#   define Rte_Pim_NvPCOMBlockNeed_MirrorBlock() RtePim_NvPCOMBlockNeed_MirrorBlock()
#  endif
#  define RtePim_NvPCOMBlockNeed_MirrorBlock() \
  (&Rte_CpApPCOM_NvPCOMBlockNeed_MirrorBlock)
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define CtApPCOM_START_SEC_CODE
# include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData
#  define RTE_RUNNABLE_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData
#  define RTE_RUNNABLE_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead
#  define RTE_RUNNABLE_DataServices_PCOM_debug_signals_PCOMDebug_ReadData DataServices_PCOM_debug_signals_PCOMDebug_ReadData
#  define RTE_RUNNABLE_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx
#  define RTE_RUNNABLE_PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx
#  define RTE_RUNNABLE_RCtApPCOM_init RCtApPCOM_init
#  define RTE_RUNNABLE_RCtApPCOM_task5msRX RCtApPCOM_task5msRX
#  define RTE_RUNNABLE_RCtApPCOM_task5msTX RCtApPCOM_task5msTX
#  define RTE_RUNNABLE_RSetIntCANDebugSignal RSetIntCANDebugSignal
# endif

FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# else
FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
# endif
FUNC(void, CtApPCOM_CODE) PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApPCOM_CODE) PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx(void); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */
FUNC(void, CtApPCOM_CODE) RCtApPCOM_init(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApPCOM_CODE) RCtApPCOM_task5msRX(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApPCOM_CODE) RCtApPCOM_task5msTX(void); /* PRQA S 3451, 0786, 3449 */ /* MD_Rte_3451, MD_Rte_0786, MD_Rte_3449 */
FUNC(void, CtApPCOM_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data); /* PRQA S 0786, 3449, 0624 */ /* MD_Rte_0786, MD_Rte_3449, MD_Rte_0624 */

# define CtApPCOM_STOP_SEC_CODE
# include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_ComM_UserRequest_E_MODE_LIMITATION (2U)

#  define RTE_E_ComM_UserRequest_E_NOT_OK (1U)

#  define RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK (1U)

#  define RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK (1U)

#  define RTE_E_DiagnosticInfo_E_NOT_OK (1U)

#  define RTE_E_EcuM_StateRequest_E_NOT_OK (1U)

#  define RTE_E_NvMService_AC3_SRBS_E_NOT_OK (1U)

#  define RTE_E_WdgM_AliveSupervision_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* RTE_CTAPPCOM_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0786:  MISRA rule: Rule5.5
     Reason:     Same macro and idintifier names in first 63 characters are required to meet AUTOSAR spec.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
