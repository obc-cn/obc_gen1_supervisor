/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: PduR
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: PduR_Cfg.h
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined (PDUR_CFG_H)
# define PDUR_CFG_H

/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "PduR_Types.h"

/**********************************************************************************************************************
 * GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
#ifndef PDUR_USE_DUMMY_STATEMENT
#define PDUR_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef PDUR_DUMMY_STATEMENT
#define PDUR_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef PDUR_DUMMY_STATEMENT_CONST
#define PDUR_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef PDUR_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define PDUR_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef PDUR_ATOMIC_VARIABLE_ACCESS
#define PDUR_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef PDUR_PROCESSOR_TC234
#define PDUR_PROCESSOR_TC234
#endif
#ifndef PDUR_COMP_GNU
#define PDUR_COMP_GNU
#endif
#ifndef PDUR_GEN_GENERATOR_MSR
#define PDUR_GEN_GENERATOR_MSR
#endif
#ifndef PDUR_CPUTYPE_BITORDER_LSB2MSB
#define PDUR_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef PDUR_CONFIGURATION_VARIANT_PRECOMPILE
#define PDUR_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef PDUR_CONFIGURATION_VARIANT_LINKTIME
#define PDUR_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef PDUR_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define PDUR_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef PDUR_CONFIGURATION_VARIANT
#define PDUR_CONFIGURATION_VARIANT PDUR_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef PDUR_POSTBUILD_VARIANT_SUPPORT
#define PDUR_POSTBUILD_VARIANT_SUPPORT STD_ON
#endif



#define PDUR_DEV_ERROR_DETECT STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRSafeBswChecks] || /ActiveEcuC/PduR/PduRGeneral[0:PduRDevErrorDetect] */
#define PDUR_DEV_ERROR_REPORT STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRDevErrorDetect] */

#define PDUR_METADATA_SUPPORT STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRMetaDataSupport] */
#define PDUR_VERSION_INFO_API STD_OFF  /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRVersionInfoApi] */

#define PDUR_ERROR_NOTIFICATION STD_OFF

#define PDUR_MAIN_FUNCTION STD_OFF

#define PDUR_MULTICORE STD_OFF /**< /ActiveEcuC/PduR/PduRGeneral[0:PduRSupportMulticore] */

#define PduR_PBConfigIdType uint32

 
 /*  DET Error define list  */ 
#define PDUR_FCT_CANIFRXIND 0x01u 
#define PDUR_FCT_CANIFTX 0x09u 
#define PDUR_FCT_CANIFTXCFM 0x02u 
#define PDUR_FCT_COMTX 0x89u 
#define PDUR_FCT_CANTPRXIND 0x05u 
#define PDUR_FCT_CANTPTX 0x09u 
#define PDUR_FCT_CANTPTXCFM 0x08u 
#define PDUR_FCT_CANTPSOR 0x06u 
#define PDUR_FCT_CANTPCPYRX 0x04u 
#define PDUR_FCT_CANTPCPYTX 0x07u 
#define PDUR_FCT_DCMTX 0x99u 
 /*   PduR_CanIfIfRxIndication  PduR_CanIfTransmit  PduR_CanIfTxConfirmation  PduR_ComTransmit  PduR_CanTpTpRxIndication  PduR_CanTpTransmit  PduR_CanTpTxConfirmation  PduR_CanTpStartOfReception  PduR_CanTpCopyRxData  PduR_CanTpCopyTxData  PduR_DcmTransmit  */ 



/**
 * \defgroup PduRHandleIdsIfRxDest Handle IDs of handle space IfRxDest.
 * \brief Communication interface Rx destination PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_BMS1_oE_CAN_7c7ae1cc_Rx_ce686a11_Rx      0u
#define PduRConf_PduRDestPdu_BMS3_oE_CAN_91ec3225_Rx_d8341745_Rx      1u
#define PduRConf_PduRDestPdu_BMS5_oE_CAN_7c26405f_Rx_5b82c527_Rx      2u
#define PduRConf_PduRDestPdu_BMS6_oE_CAN_0ac37962_Rx_e104bf72_Rx      3u
#define PduRConf_PduRDestPdu_BMS8_oE_CAN_e7b0483e_Rx_b865118f_Rx      4u
#define PduRConf_PduRDestPdu_BMS9_oE_CAN_7cc3a2ea_Rx_3c9c33f4_Rx      5u
#define PduRConf_PduRDestPdu_BSIInfo_oE_CAN_0ff52381_Rx_882b1898_Rx   6u
#define PduRConf_PduRDestPdu_CtrlDCDC_oE_CAN_8127de91_Rx_2ed93c0e_Rx  7u
#define PduRConf_PduRDestPdu_DCHV_Fault_oInt_CAN_595e2cca_Rx_aaa6ebec_Rx 8u
#define PduRConf_PduRDestPdu_DCHV_Status1_oInt_CAN_c4860e0a_Rx_91da6bff_Rx 9u
#define PduRConf_PduRDestPdu_DCHV_Status2_oInt_CAN_771223c9_Rx_9f59032e_Rx 10u
#define PduRConf_PduRDestPdu_DCLV_Fault_oInt_CAN_cb576812_Rx_3259f79e_Rx 11u
#define PduRConf_PduRDestPdu_DCLV_Status1_oInt_CAN_fcd6569d_Rx_f6508c17_Rx 12u
#define PduRConf_PduRDestPdu_DCLV_Status2_oInt_CAN_4f427b5e_Rx_820710d6_Rx 13u
#define PduRConf_PduRDestPdu_DCLV_Status3_oInt_CAN_21ce601f_Rx_6f865e62_Rx 14u
#define PduRConf_PduRDestPdu_ELECTRON_BSI_oE_CAN_61376617_Rx_182f4c9b_Rx 15u
#define PduRConf_PduRDestPdu_NEW_JDD_oE_CAN_4540bd6c_Rx_a0d1622f_Rx   16u
#define PduRConf_PduRDestPdu_PFC_Fault_oInt_CAN_63f22496_Rx_3c5c1cb6_Rx 17u
#define PduRConf_PduRDestPdu_PFC_Status1_oInt_CAN_dc53aecc_Rx_4db707a4_Rx 18u
#define PduRConf_PduRDestPdu_PFC_Status2_oInt_CAN_6fc7830f_Rx_13c131e4_Rx 19u
#define PduRConf_PduRDestPdu_PFC_Status3_oInt_CAN_014b984e_Rx_97e42112_Rx 20u
#define PduRConf_PduRDestPdu_PFC_Status4_oInt_CAN_d39edec8_Rx_8463d87d_Rx 21u
#define PduRConf_PduRDestPdu_PFC_Status5_oInt_CAN_bd12c589_Rx_fa5b1cce_Rx 22u
#define PduRConf_PduRDestPdu_ParkCommand_oE_CAN_16da6a87_Rx_50e0f311_Rx 23u
#define PduRConf_PduRDestPdu_ProgTool_SupEnterBoot_oInt_CAN_39bdfe09_Rx_9b0aaf50_Rx 24u
#define PduRConf_PduRDestPdu_ReqToECANFunction_oE_CAN_abe814c4_Rx_b756064f_Rx 25u
#define PduRConf_PduRDestPdu_ReqToOBC_oE_CAN_45858201_Rx_7e41dab9_Rx  26u
#define PduRConf_PduRDestPdu_VCU2_oE_CAN_7db4b816_Rx_c61797dd_Rx      27u
#define PduRConf_PduRDestPdu_VCU3_oE_CAN_e6c752c2_Rx_2def7208_Rx      28u
#define PduRConf_PduRDestPdu_VCU_BSI_Wakeup_oE_CAN_79c55f53_Rx_771f59ac_Rx 29u
#define PduRConf_PduRDestPdu_VCU_PCANInfo_oE_CAN_1242d2f8_Rx_c80fbc21_Rx 30u
#define PduRConf_PduRDestPdu_VCU_TU_oE_CAN_bad58dcd_Rx_141e34ce_Rx    31u
#define PduRConf_PduRDestPdu_VCU_oE_CAN_377cc071_Rx_b485c90c_Rx       32u
/**\} */

/**
 * \defgroup PduRHandleIdsIfRxSrc Handle IDs of handle space IfRxSrc.
 * \brief Communication interface Rx source PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2def7208                       28u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2ed93c0e                       7u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3c5c1cb6                       17u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3c9c33f4                       5u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_4db707a4                       18u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5b82c527                       2u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_6f865e62                       14u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_7e41dab9                       26u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9b0aaf50                       24u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_9f59032e                       10u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_13c131e4                       19u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_50e0f311                       23u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_91da6bff                       9u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_97e42112                       20u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_141e34ce                       31u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_182f4c9b                       15u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_771f59ac                       29u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_882b1898                       6u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_3259f79e                       11u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8463d87d                       21u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_820710d6                       13u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0d1622f                       16u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_aaa6ebec                       8u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b485c90c                       32u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b756064f                       25u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_b865118f                       4u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c80fbc21                       30u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c61797dd                       27u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_ce686a11                       0u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d8341745                       1u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e104bf72                       3u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f6508c17                       12u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_fa5b1cce                       22u
/**\} */

/**
 * \defgroup PduRHandleIdsIfTpTxSrc Handle IDs of handle space IfTpTxSrc.
 * \brief Communication interface and transport protocol Tx PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRSrcPdu_PduRSrcPdu_0f56ae96                       9u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_2e76203d                       6u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_8aa49178                       17u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_17d4e376                       14u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_36c239fa                       15u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1224da83                       0u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_5705bb4d                       7u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_558557e8                       8u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_31565810                       10u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_a0eb6b35                       13u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_aa4ba89d                       11u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_af12cdd2                       5u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_c451a6c2                       2u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_cf0367a1                       3u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_d598d177                       1u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_e2c32671                       4u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f0e620d6                       12u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_f7c07c35                       16u
/**\} */

/**
 * \defgroup PduRHandleIdsIfTxDest Handle IDs of handle space IfTxDest.
 * \brief Communication interface Tx destination PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_DC1_oE_CAN_f87e7579_Tx                   0u
#define PduRConf_PduRDestPdu_DC2_oE_CAN_8e9b4c44_Tx                   1u
#define PduRConf_PduRDestPdu_Debug1_oInt_CAN_5112c5fb_Tx              2u
#define PduRConf_PduRDestPdu_Debug2_oInt_CAN_e286e838_Tx              3u
#define PduRConf_PduRDestPdu_Debug3_oInt_CAN_8c0af379_Tx              4u
#define PduRConf_PduRDestPdu_Debug4_oInt_CAN_5edfb5ff_Tx              5u
#define PduRConf_PduRDestPdu_NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx      6u
#define PduRConf_PduRDestPdu_OBC1_oE_CAN_79eaec1b_Tx                  7u
#define PduRConf_PduRDestPdu_OBC2_oE_CAN_0f0fd526_Tx                  8u
#define PduRConf_PduRDestPdu_OBC3_oE_CAN_947c3ff2_Tx                  9u
#define PduRConf_PduRDestPdu_OBC4_oE_CAN_e2c5a75c_Tx                  10u
#define PduRConf_PduRDestPdu_OBCTxDiag_oE_CAN_794ae2be_Tx             11u
#define PduRConf_PduRDestPdu_SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx      12u
#define PduRConf_PduRDestPdu_SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx   13u
#define PduRConf_PduRDestPdu_SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx   14u
#define PduRConf_PduRDestPdu_SUP_CommandToPFC_oInt_CAN_11d75aca_Tx    15u
#define PduRConf_PduRDestPdu_VERS_OBC_DCDC_oE_CAN_336be5e5_Tx         16u
/**\} */

/**
 * \defgroup PduRHandleIdsTpRxDest Handle IDs of handle space TpRxDest.
 * \brief Transport protocol Rx destination PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_REQ_DIAG_ON_CAN_oE_CAN_0be358b0_Rx_1707827c_Rx 0u
#define PduRConf_PduRDestPdu_UDS_ON_CAN_oE_CAN_a9f0043e_Rx_59769882_Rx 1u
/**\} */

/**
 * \defgroup PduRHandleIdsTpRxSrc Handle IDs of handle space TpRxSrc.
 * \brief Transport protocol Rx source PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRSrcPdu_PduRSrcPdu_1707827c                       0u
#define PduRConf_PduRSrcPdu_PduRSrcPdu_59769882                       1u
/**\} */

/**
 * \defgroup PduRHandleIdsTpTxDest Handle IDs of handle space TpTxDest.
 * \brief Transport protocol Tx PDUs
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define PduRConf_PduRDestPdu_REP_DIAG_ON_CAN_oE_CAN_c47d4f78_Tx       0u
/**\} */


/* User Config File Start */

/* User Config File End */


/**********************************************************************************************************************
 * GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

#endif  /* PDUR_CFG_H */
/**********************************************************************************************************************
 * END OF FILE: PduR_Cfg.h
 *********************************************************************************************************************/

