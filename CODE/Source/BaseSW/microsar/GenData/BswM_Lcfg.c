/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: BswM
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswM_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:51
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#define BSWM_LCFG_SOURCE

/* -----------------------------------------------------------------------------
    &&&~ MISRA JUSTIFICATION
 ----------------------------------------------------------------------------- */
/* PRQA S 0785, 0786 EOF */ /* MD_CSL_DistinctIdentifiers */

/* -----------------------------------------------------------------------------
    &&&~ INCLUDE
 ----------------------------------------------------------------------------- */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
#include "BswM.h"
#include "BswM_Private_Cfg.h"
#include "SchM_BswM.h"

#if !defined (BSWM_LOCAL)
# define BSWM_LOCAL static
#endif

#if !defined (BSWM_LOCAL_INLINE) /* COV_BSWM_LOCAL_INLINE */
# define BSWM_LOCAL_INLINE LOCAL_INLINE
#endif



#define BSWM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* -----------------------------------------------------------------------------
    &&&~ LOCAL FUNCTION DECLARATIONS
 ----------------------------------------------------------------------------- */
/**********************************************************************************************************************
 *  BswM_Action_RuleHandler()
 **********************************************************************************************************************/
/*!
 * \brief       Executes a rule.
 * \details     Arbitrates a rule and executes corresponding action list.
 * \param[in]   handleId       Id of the rule to execute.
 * \param[in]   partitionIdx   Current partition context.
 * \return      E_OK      No action list was executed or corresponding action list was completely executed.
 * \return      E_NOT_OK  Action list was aborted because an action failed.
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_RuleHandler(BswM_HandleType handleId,
                                                                   BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);


/**********************************************************************************************************************
 *  BswM_UpdateTimer()
 **********************************************************************************************************************/
/*!
 * \brief       Updates a timer.
 * \details     Set timer value of passed timerId to passed value and calculates the new state.
 * \param[in]   timerId        Id of the timer to update.
 * \param[in]   value          New value of the timer.
 * \param[in]   partitionIdx   Current partition context. 
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_UpdateTimer(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx,
                                                         BswM_SizeOfTimerValueType timerId,
                                                         BswM_TimerValueType value);

/*! \addtogroup    BswMGeneratedFunctions BswM Generated Functions
 * \{
 */
/* PRQA S 0779 FUNCTIONDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */

/**********************************************************************************************************************
 *  Init
 *********************************************************************************************************************/
/*! \defgroup    Init
 * \{
 */
/**********************************************************************************************************************
 *  BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
/*!
 * \brief       Initializes BswM.
 * \details     Part of the BswM_Init. Initializes all generated variables and executes action lists for initialization.
 * \pre         -
 * \reentrant   FALSE
 * \synchronous TRUE
 * \note        May only be called by BswM_Init.
 */
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
/*! \} */ /* End of group Init */

/**********************************************************************************************************************
 *  Common
 *********************************************************************************************************************/
/*! \defgroup    Common
 * \{
 */
/**********************************************************************************************************************
 *  BswM_ModeNotificationFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
/*!
 * \brief       Switch Modes of RTE and writes RTE values.
 * \details     Forwards a BswM Switch Action to the RTE and writes value of RteRequestPorts to RTE.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \note        May only be called by BswM_MainFunction.
 */
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_ModeNotificationFct_BSWM_SINGLEPARTITION(void);
/*! \} */ /* End of group Common */

/**********************************************************************************************************************
 *  SwcModeRequestUpdate
 *********************************************************************************************************************/
/*! \defgroup    SwcModeRequestUpdate
 * \{
 */
/**********************************************************************************************************************
 *  BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
/*!
 * \brief       Reads port values from RTE.
 * \details     Gets the current value of SwcModeRequest Ports and SwcNotification Ports from RTE.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \note        May only be called by BswM_MainFunction.
 */
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION(void);
/*! \} */ /* End of group SwcModeRequestUpdate */

/**********************************************************************************************************************
 *  BswMActionFunctions
 *********************************************************************************************************************/
/*! \defgroup    BswMActionFunctions
 * \{
 */

/*!
 * \{
 * \brief       Executes an action.
 * \details     Generated function which executes the configured action.
 * \param[IN]   handleId      ID of the parameter set which shall be used for calling the action.
 * \param[IN]   partitionIdx  Current partition context
 * \return      E_OK      Action was successfully executed.
 * \return      E_NOT_OK  Execution of Action failed
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Can_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanIf_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Com_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_PduR_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanSM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_ComM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Rte_Start_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EnableInterrupts_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_NvM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_EcuMClearValidatedWakeupEvents_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterPostRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchPostRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterShutdown_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_EcuMGoToSelectedShutdownTarget_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterPrepShutdown_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchShutdown_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_CancelNvMWriteAll_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterWakeup_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchWakeup_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_NvMWriteAll_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterWaitForNvm_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_NvM_Pre_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanTp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Dcm_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanTrcv_30_Tja1145_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Fee_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Xcp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Dem_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanXcp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_NvMReadAll_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_WdgM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EthTrcv_30_Ar7000_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Eth_30_Ar7000_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EthIf_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EthSM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_TcpIp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Exi_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Scc_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
/*! \} */ /* End of sharing description for BswMActionFunctions */
/*! \} */ /* End of group BswMActionFunctions */

/**********************************************************************************************************************
 *  BswMExpressionFunctions
 *********************************************************************************************************************/
/*! \defgroup    BswMExpressionFunctions
 * \{
 */

/*!
 * \{
 * \brief       Evaluates a logical expression.
 * \details     Generated function which evaluates the configured logical expression.
 * \return      1  If logical expression evaluates to true
 * \return      0  If logical expression evaluates to false
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunToPostRunTransition_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunReached_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoRunAndNoKillAllRequest_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoRunReasons_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunReleased_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WaitForNvMToShutdown_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WriteNotPending_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoWakeupOrKillAll_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WakeupToPrepShutdown_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_CancelNotPending_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WaitForNvMToWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_ValidWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WakeupToRun_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_InitToWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRunToPrep_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRunReleased_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRunToRun_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunRequestsOrWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunRequested_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRun_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PrepShutdownToWaitForNvM_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_RX_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_DM_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_TX_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_EcuMStateSleepOrShutdown_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_BswMLogicalExpression_ProgReset_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
/*! \} */ /* End of sharing description for BswMExpressionFunctions */
#define BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_TX_Left BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_DM_Left
#define BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_RX_DM_Left BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_TX_Left
/*! \} */ /* End of group BswMExpressionFunctions */
/* PRQA L:FUNCTIONDECLARATIONS */
/*! \} */ /* End of group BswMGeneratedFunctions */

#define BSWM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


/* -----------------------------------------------------------------------------
    &&&~ LOCAL VARIABLE DECLARATIONS
 ----------------------------------------------------------------------------- */

/* PRQA S 0779 VARIABLEDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */ 


#define BSWM_START_SEC_VAR_NOINIT_8BIT
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

VAR(uint8, BSWM_VAR_NOINIT) BswM_PduGroupControlInvocation;

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* PRQA S 3218 3 */ /* MD_BswM_3218 */
VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComIPduGroupState;
VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComRxIPduGroupDMState;

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* PRQA L:VARIABLEDECLARATIONS */

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA PROTOTYPES
**********************************************************************************************************************/



/**********************************************************************************************************************
 *  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/


/* PRQA S 0310 GLOBALDATADECLARATIONS */ /* MD_BSWM_0310 */ 
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  BswM_ActionItems
**********************************************************************************************************************/
/** 
  \var    BswM_ActionItems
  \details
  Element         Description
  ActionsIdx      the index of the 1:1 relation pointing to BswM_Actions
  ParameterIdx    the index of the 0:1 relation pointing to BswM_ComMAllowComParameters,BswM_ComMModeLimitationParameters,BswM_ComMModeSwitchParameters,BswM_ComDMControlParameters,BswM_EcuMSelectShutdownTargetParameters,BswM_EcuMStateSwitchParameters,BswM_LinScheduleRequestParameters,BswM_NmControlParameters,BswM_ComSwitchIPduModeParameters,BswM_ComPduGroupSwitchParameters,BswM_ComPduGroupHandlingParameters,BswM_ComDMHandlingParameters,BswM_ComTriggerIPduSendParameters,BswM_PduRouterControlParameters,BswM_TimerControlParameters,BswM_GenericModeParameters,BswM_GenericModeRefParameters,BswM_J1939DcmStateParameters,BswM_J1939RmStateParameters,BswM_SdClientParameters,BswM_SdConsumedParameters,BswM_SdServerParameters,BswM_RuleControlParameters,BswM_ActionLists,BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ActionItemsType, BSWM_CONST) BswM_ActionItems[87] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ActionsIdx  ParameterIdx                             Referable Keys */
  { /*     0 */        11u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     1 */        24u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     2 */        14u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     3 */        22u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     4 */        13u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     5 */        27u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     6 */        16u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     7 */         4u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     8 */        20u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*     9 */         8u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    10 */        26u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    11 */        15u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    12 */        25u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    13 */        23u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    14 */         5u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    15 */         3u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    16 */        12u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    17 */         9u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    18 */        10u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    19 */        17u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    20 */        21u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    21 */        19u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    22 */        18u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    23 */         7u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    24 */         6u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_INIT_AL_Initialize] */
  { /*    25 */        40u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_RunToPostRun] */
  { /*    26 */        37u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_RunToPostRun] */
  { /*    27 */         1u,                                0u },  /* [AL_ESH_AL_RunToPostRun] */
  { /*    28 */        31u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_RunToPostRun] */
  { /*    29 */        41u,                                0u },  /* [AL_ESH_AL_RunToPostRun] */
  { /*    30 */        42u,                                0u },  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  { /*    31 */        34u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  { /*    32 */        39u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  { /*    33 */        36u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WakeupToPrep] */
  { /*    34 */         1u,                                1u },  /* [AL_ESH_AL_WakeupToPrep] */
  { /*    35 */        29u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WakeupToPrep] */
  { /*    36 */        41u,                                1u },  /* [AL_ESH_AL_WakeupToPrep] */
  { /*    37 */        42u,                                0u },  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*    38 */        42u,                                1u },  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*    39 */        43u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*    40 */        32u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*    41 */        28u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*    42 */        41u,                                2u },  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*    43 */        42u,                                2u },  /* [AL_ESH_AL_WakeupToRun] */
  { /*    44 */        42u,                                3u },  /* [AL_ESH_AL_WakeupToRun] */
  { /*    45 */        35u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WakeupToRun] */
  { /*    46 */         1u,                                2u },  /* [AL_ESH_AL_WakeupToRun] */
  { /*    47 */        30u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_WakeupToRun] */
  { /*    48 */        41u,                                3u },  /* [AL_ESH_AL_WakeupToRun] */
  { /*    49 */        32u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_InitToWakeup] */
    /* Index    ActionsIdx  ParameterIdx                             Referable Keys */
  { /*    50 */        28u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_InitToWakeup] */
  { /*    51 */        41u,                                2u },  /* [AL_ESH_AL_InitToWakeup] */
  { /*    52 */        36u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  { /*    53 */         1u,                                1u },  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  { /*    54 */        29u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  { /*    55 */        41u,                                1u },  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  { /*    56 */         0u,                                6u },  /* [AL_ESH_AL_ESH_PostRunToPrepCheck] */
  { /*    57 */        42u,                                3u },  /* [AL_ESH_AL_PostRunToRun] */
  { /*    58 */        35u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_PostRunToRun] */
  { /*    59 */         1u,                                2u },  /* [AL_ESH_AL_PostRunToRun] */
  { /*    60 */        30u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_PostRunToRun] */
  { /*    61 */        41u,                                3u },  /* [AL_ESH_AL_PostRunToRun] */
  { /*    62 */         0u,                                7u },  /* [AL_ESH_AL_ExitPostRun] */
  { /*    63 */        42u,                                4u },  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  { /*    64 */        38u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  { /*    65 */        33u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  { /*    66 */        41u,                                4u },  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  { /*    67 */        44u,                                0u },  /* [AL_CC_AL_CN_Int_CAN_b597612f_RX_Disable] */
  { /*    68 */        44u,                                1u },  /* [AL_CC_AL_CN_Int_CAN_b597612f_RX_EnableNoinit] */
  { /*    69 */        44u,                                2u },  /* [AL_CC_AL_CN_E_CAN_7f812c72_RX_Disable] */
  { /*    70 */        44u,                                3u },  /* [AL_CC_AL_CN_E_CAN_7f812c72_RX_EnableNoinit] */
  { /*    71 */        45u,                                0u },  /* [AL_CC_AL_CN_E_CAN_7f812c72_Disable_DM] */
  { /*    72 */        45u,                                1u },  /* [AL_CC_AL_CN_E_CAN_7f812c72_Enable_DM] */
  { /*    73 */        44u,                                4u },  /* [AL_CC_AL_CN_Int_CAN_b597612f_TX_Disable] */
  { /*    74 */        44u,                                5u },  /* [AL_CC_AL_CN_Int_CAN_b597612f_TX_EnableNoinit] */
  { /*    75 */        44u,                                6u },  /* [AL_CC_AL_CN_E_CAN_7f812c72_TX_Disable] */
  { /*    76 */        44u,                                7u },  /* [AL_CC_AL_CN_E_CAN_7f812c72_TX_EnableNoinit] */
  { /*    77 */        45u,                                2u },  /* [AL_CC_AL_CN_Int_CAN_b597612f_Disable_DM] */
  { /*    78 */        45u,                                3u },  /* [AL_CC_AL_CN_Int_CAN_b597612f_Enable_DM] */
  { /*    79 */        42u,                                4u },  /* [AL_BswMActionList_ProgReset] */
  { /*    80 */        38u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_BswMActionList_ProgReset] */
  { /*    81 */        33u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_BswMActionList_ProgReset] */
  { /*    82 */        41u,                                4u },  /* [AL_BswMActionList_ProgReset] */
  { /*    83 */        42u,                                0u },  /* [AL_BswMActionList_ProgReset] */
  { /*    84 */        34u, BSWM_NO_PARAMETERIDXOFACTIONITEMS },  /* [AL_BswMActionList_ProgReset] */
  { /*    85 */        46u,                                0u },  /* [AL_BswMActionList_ProgReset] */
  { /*    86 */        39u, BSWM_NO_PARAMETERIDXOFACTIONITEMS }   /* [AL_BswMActionList_ProgReset] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ActionLists
**********************************************************************************************************************/
/** 
  \var    BswM_ActionLists
  \details
  Element                Description
  ActionItemsEndIdx      the end index of the 0:n relation pointing to BswM_ActionItems
  ActionItemsStartIdx    the start index of the 0:n relation pointing to BswM_ActionItems
  MaskedBits             contains bitcoded the boolean data of BswM_ActionItemsUsedOfActionLists, BswM_ConditionalOfActionLists
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ActionListsType, BSWM_CONST) BswM_ActionLists[26] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ActionItemsEndIdx                       ActionItemsStartIdx                       MaskedBits        Comment                   Referable Keys */
  { /*     0 */                                    25u,                                       0u,      0x03u },  /* [Priority: 0] */  /* [AL_INIT_AL_Initialize] */
  { /*     1 */                                    30u,                                      25u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_RunToPostRun] */
  { /*     2 */                                    33u,                                      30u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  { /*     3 */                                    37u,                                      33u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_WakeupToPrep] */
  { /*     4 */                                    43u,                                      37u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*     5 */                                    49u,                                      43u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_WakeupToRun] */
  { /*     6 */                                    52u,                                      49u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_InitToWakeup] */
  { /*     7 */                                    56u,                                      52u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  { /*     8 */                                    57u,                                      56u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_ESH_PostRunToPrepCheck] */
  { /*     9 */                                    62u,                                      57u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_PostRunToRun] */
  { /*    10 */                                    63u,                                      62u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_ExitPostRun] */
  { /*    11 */                                    67u,                                      63u,      0x03u },  /* [Priority: 0] */  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  { /*    12 */                                    68u,                                      67u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Int_CAN_b597612f_RX_Disable] */
  { /*    13 */                                    69u,                                      68u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Int_CAN_b597612f_RX_EnableNoinit] */
  { /*    14 */                                    70u,                                      69u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_E_CAN_7f812c72_RX_Disable] */
  { /*    15 */                                    71u,                                      70u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_E_CAN_7f812c72_RX_EnableNoinit] */
  { /*    16 */                                    72u,                                      71u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_E_CAN_7f812c72_Disable_DM] */
  { /*    17 */                                    73u,                                      72u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_E_CAN_7f812c72_Enable_DM] */
  { /*    18 */                                    74u,                                      73u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Int_CAN_b597612f_TX_Disable] */
  { /*    19 */                                    75u,                                      74u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Int_CAN_b597612f_TX_EnableNoinit] */
  { /*    20 */                                    76u,                                      75u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_E_CAN_7f812c72_TX_Disable] */
  { /*    21 */                                    77u,                                      76u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_E_CAN_7f812c72_TX_EnableNoinit] */
  { /*    22 */                                    78u,                                      77u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Int_CAN_b597612f_Disable_DM] */
  { /*    23 */                                    79u,                                      78u,      0x02u },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Int_CAN_b597612f_Enable_DM] */
  { /*    24 */                                    87u,                                      79u,      0x02u },  /* [Priority: 0] */  /* [AL_BswMActionList_ProgReset] */
  { /*    25 */ BSWM_NO_ACTIONITEMSENDIDXOFACTIONLISTS, BSWM_NO_ACTIONITEMSSTARTIDXOFACTIONLISTS,      0x00u }   /* [Priority: 0] */  /* [AL_BswMActionList_ProgResetFail] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Actions
**********************************************************************************************************************/
/** 
  \var    BswM_Actions
  \brief  Holds pointer to all action functions.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ActionFuncType, BSWM_CONST) BswM_Actions[47] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     Actions                                                         Referable Keys */
  /*     0 */ BswM_Action_RuleHandler                                    ,  /* [R_ESH_PostRunNested, R_ESH_PostRunToPrepNested] */
  /*     1 */ BswM_Action_EcuMStateSwitch                                ,  /* [A_SwitchEcuMStateTo_POST_RUN, A_SwitchEcuMStateTo_RUN, A_SwitchEcuMStateTo_SHUTDOWN] */
  /*     2 */ BswM_Action_NvM_Pre_Init_Left                              ,  /* [A_NvM_Pre_Init] */
  /*     3 */ BswM_Action_INIT_Action_Xcp_Init_Left                      ,  /* [A_INIT_Action_Xcp_Init] */
  /*     4 */ BswM_Action_INIT_Action_WdgM_Init_Left                     ,  /* [A_INIT_Action_WdgM_Init] */
  /*     5 */ BswM_Action_INIT_Action_TcpIp_Init_Left                    ,  /* [A_INIT_Action_TcpIp_Init] */
  /*     6 */ BswM_Action_INIT_Action_Scc_Init_Left                      ,  /* [A_INIT_Action_Scc_Init] */
  /*     7 */ BswM_Action_INIT_Action_Rte_Start_Left                     ,  /* [A_INIT_Action_Rte_Start] */
  /*     8 */ BswM_Action_INIT_Action_PduR_Init_Left                     ,  /* [A_INIT_Action_PduR_Init] */
  /*     9 */ BswM_Action_INIT_Action_NvM_Init_Left                      ,  /* [A_INIT_Action_NvM_Init] */
  /*    10 */ BswM_Action_INIT_Action_NvMReadAll_Left                    ,  /* [A_INIT_Action_NvMReadAll] */
  /*    11 */ BswM_Action_INIT_Action_Fee_Init_Left                      ,  /* [A_INIT_Action_Fee_Init] */
  /*    12 */ BswM_Action_INIT_Action_Exi_Init_Left                      ,  /* [A_INIT_Action_Exi_Init] */
  /*    13 */ BswM_Action_INIT_Action_Eth_30_Ar7000_Init_Left            ,  /* [A_INIT_Action_Eth_30_Ar7000_Init] */
  /*    14 */ BswM_Action_INIT_Action_EthTrcv_30_Ar7000_Init_Left        ,  /* [A_INIT_Action_EthTrcv_30_Ar7000_Init] */
  /*    15 */ BswM_Action_INIT_Action_EthSM_Init_Left                    ,  /* [A_INIT_Action_EthSM_Init] */
  /*    16 */ BswM_Action_INIT_Action_EthIf_Init_Left                    ,  /* [A_INIT_Action_EthIf_Init] */
  /*    17 */ BswM_Action_INIT_Action_EnableInterrupts_Left              ,  /* [A_INIT_Action_EnableInterrupts] */
  /*    18 */ BswM_Action_INIT_Action_Dem_Init_Left                      ,  /* [A_INIT_Action_Dem_Init] */
  /*    19 */ BswM_Action_INIT_Action_Dcm_Init_Left                      ,  /* [A_INIT_Action_Dcm_Init] */
  /*    20 */ BswM_Action_INIT_Action_Com_Init_Left                      ,  /* [A_INIT_Action_Com_Init] */
  /*    21 */ BswM_Action_INIT_Action_ComM_Init_Left                     ,  /* [A_INIT_Action_ComM_Init] */
  /*    22 */ BswM_Action_INIT_Action_Can_Init_Left                      ,  /* [A_INIT_Action_Can_Init] */
  /*    23 */ BswM_Action_INIT_Action_CanXcp_Init_Left                   ,  /* [A_INIT_Action_CanXcp_Init] */
  /*    24 */ BswM_Action_INIT_Action_CanTrcv_30_Tja1145_Init_Left       ,  /* [A_INIT_Action_CanTrcv_30_Tja1145_Init] */
  /*    25 */ BswM_Action_INIT_Action_CanTp_Init_Left                    ,  /* [A_INIT_Action_CanTp_Init] */
  /*    26 */ BswM_Action_INIT_Action_CanSM_Init_Left                    ,  /* [A_INIT_Action_CanSM_Init] */
  /*    27 */ BswM_Action_INIT_Action_CanIf_Init_Left                    ,  /* [A_INIT_Action_CanIf_Init] */
  /*    28 */ BswM_Action_ESH_Action_SwitchWakeup_Left                   ,  /* [A_ESH_Action_SwitchWakeup] */
  /*    29 */ BswM_Action_ESH_Action_SwitchShutdown_Left                 ,  /* [A_ESH_Action_SwitchShutdown] */
  /*    30 */ BswM_Action_ESH_Action_SwitchRun_Left                      ,  /* [A_ESH_Action_SwitchRun] */
  /*    31 */ BswM_Action_ESH_Action_SwitchPostRun_Left                  ,  /* [A_ESH_Action_SwitchPostRun] */
  /*    32 */ BswM_Action_ESH_Action_OnEnterWakeup_Left                  ,  /* [A_ESH_Action_OnEnterWakeup] */
  /*    33 */ BswM_Action_ESH_Action_OnEnterWaitForNvm_Left              ,  /* [A_ESH_Action_OnEnterWaitForNvm] */
  /*    34 */ BswM_Action_ESH_Action_OnEnterShutdown_Left                ,  /* [A_ESH_Action_OnEnterShutdown] */
  /*    35 */ BswM_Action_ESH_Action_OnEnterRun_Left                     ,  /* [A_ESH_Action_OnEnterRun] */
  /*    36 */ BswM_Action_ESH_Action_OnEnterPrepShutdown_Left            ,  /* [A_ESH_Action_OnEnterPrepShutdown] */
  /*    37 */ BswM_Action_ESH_Action_OnEnterPostRun_Left                 ,  /* [A_ESH_Action_OnEnterPostRun] */
  /*    38 */ BswM_Action_ESH_Action_NvMWriteAll_Left                    ,  /* [A_ESH_Action_NvMWriteAll] */
  /*    39 */ BswM_Action_ESH_Action_EcuMGoToSelectedShutdownTarget_Left ,  /* [A_ESH_Action_EcuMGoToSelectedShutdownTarget] */
  /*    40 */ BswM_Action_ESH_Action_EcuMClearValidatedWakeupEvents_Left ,  /* [A_ESH_Action_EcuMClearValidatedWakeupEvents] */
  /*    41 */ BswM_Action_GenericMode                                    ,  /* [A_ESH_Action_ESH_PostRun, A_ESH_Action_ESH_PrepShutdown, A_ESH_Action_ESH_Run, A_ESH_Action_ESH_WaitForNvm, A_ESH_Action_ESH_Wakeup] */
  /*    42 */ BswM_Action_TimerControl                                   ,  /* [A_ESH_Action_CancelWriteAllTimer_Start, A_ESH_Action_CancelWriteAllTimer_Stop, A_ESH_Action_SelfRunRequestTimer_Start, A_ESH_Action_WriteAllTimer_Start, A_ESH_Action_WriteAllTimer_Stop] */
  /*    43 */ BswM_Action_ESH_Action_CancelNvMWriteAll_Left              ,  /* [A_ESH_Action_CancelNvMWriteAll] */
  /*    44 */ BswM_Action_ComPduGroupSwitch                              ,  /* [A_CC_DisablePDUGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4, A_CC_DisablePDUGroup_OBC_DCDC_oE_CAN_Tx_11676862, A_CC_DisablePDUGroup_OBC_DCDC_oInt_CAN_Rx_795f7144, A_CC_DisablePDUGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2, A_CC_EnablePDUGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4, A_CC_EnablePDUGroup_OBC_DCDC_oE_CAN_Tx_11676862, A_CC_EnablePDUGroup_OBC_DCDC_oInt_CAN_Rx_795f7144, A_CC_EnablePDUGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  /*    45 */ BswM_Action_ComDMControl                                   ,  /* [A_CC_DisableDM_OBC_DCDC_oE_CAN_Rx_473dcfe4, A_CC_DisableDM_OBC_DCDC_oInt_CAN_Rx_795f7144, A_CC_EnableDM_OBC_DCDC_oE_CAN_Rx_473dcfe4, A_CC_EnableDM_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  /*    46 */ BswM_Action_EcuMSelectShutdownTarget                          /* [A_BswMAction_ProgReset] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_CanSMChannelMapping
**********************************************************************************************************************/
/** 
  \var    BswM_CanSMChannelMapping
  \brief  Maps the external id of BswMCanSMIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMCanSMIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_CanSMChannelMappingType, BSWM_CONST) BswM_CanSMChannelMapping[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                                ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_E_CAN_7f812c72  ,                  1u,                    0u },  /* [CANSM_CHANNEL_0, MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  { /*     1 */ ComMConf_ComMChannel_CN_Int_CAN_b597612f,                  2u,                    1u }   /* [CANSM_CHANNEL_1, MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComDMControlParameters
**********************************************************************************************************************/
/** 
  \var    BswM_ComDMControlParameters
  \details
  Element                              Description
  ComDMControlSubParametersEndIdx      the end index of the 0:n relation pointing to BswM_ComDMControlSubParameters
  ComDMControlSubParametersStartIdx    the start index of the 0:n relation pointing to BswM_ComDMControlSubParameters
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ComDMControlParametersType, BSWM_CONST) BswM_ComDMControlParameters[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ComDMControlSubParametersEndIdx  ComDMControlSubParametersStartIdx        Referable Keys */
  { /*     0 */                              1u,                                0u },  /* [A_CC_DisableDM_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     1 */                              2u,                                1u },  /* [A_CC_EnableDM_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     2 */                              3u,                                2u },  /* [A_CC_DisableDM_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     3 */                              4u,                                3u }   /* [A_CC_EnableDM_OBC_DCDC_oInt_CAN_Rx_795f7144] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComDMControlSubParameters
**********************************************************************************************************************/
/** 
  \var    BswM_ComDMControlSubParameters
  \details
  Element        Description
  IpduGroupId
  BitVal     
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ComDMControlSubParametersType, BSWM_CONST) BswM_ComDMControlSubParameters[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IpduGroupId                                         BitVal        Referable Keys */
  { /*     0 */ ComConf_ComIPduGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4  , FALSE  },  /* [A_CC_DisableDM_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     1 */ ComConf_ComIPduGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4  , TRUE   },  /* [A_CC_EnableDM_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     2 */ ComConf_ComIPduGroup_OBC_DCDC_oInt_CAN_Rx_795f7144, FALSE  },  /* [A_CC_DisableDM_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     3 */ ComConf_ComIPduGroup_OBC_DCDC_oInt_CAN_Rx_795f7144, TRUE   }   /* [A_CC_EnableDM_OBC_DCDC_oInt_CAN_Rx_795f7144] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComPduGroupSwitchParameters
**********************************************************************************************************************/
/** 
  \var    BswM_ComPduGroupSwitchParameters
  \details
  Element                                   Description
  ComPduGroupSwitchSubParametersEndIdx      the end index of the 0:n relation pointing to BswM_ComPduGroupSwitchSubParameters
  ComPduGroupSwitchSubParametersStartIdx    the start index of the 0:n relation pointing to BswM_ComPduGroupSwitchSubParameters
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ComPduGroupSwitchParametersType, BSWM_CONST) BswM_ComPduGroupSwitchParameters[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ComPduGroupSwitchSubParametersEndIdx  ComPduGroupSwitchSubParametersStartIdx        Referable Keys */
  { /*     0 */                                   1u,                                     0u },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     1 */                                   2u,                                     1u },  /* [A_CC_EnablePDUGroup_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     2 */                                   3u,                                     2u },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     3 */                                   4u,                                     3u },  /* [A_CC_EnablePDUGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     4 */                                   5u,                                     4u },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     5 */                                   6u,                                     5u },  /* [A_CC_EnablePDUGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     6 */                                   7u,                                     6u },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     7 */                                   8u,                                     7u }   /* [A_CC_EnablePDUGroup_OBC_DCDC_oE_CAN_Tx_11676862] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComPduGroupSwitchSubParameters
**********************************************************************************************************************/
/** 
  \var    BswM_ComPduGroupSwitchSubParameters
  \details
  Element        Description
  IpduGroupId
  BitVal     
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ComPduGroupSwitchSubParametersType, BSWM_CONST) BswM_ComPduGroupSwitchSubParameters[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IpduGroupId                                         BitVal        Referable Keys */
  { /*     0 */ ComConf_ComIPduGroup_OBC_DCDC_oInt_CAN_Rx_795f7144, FALSE  },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     1 */ ComConf_ComIPduGroup_OBC_DCDC_oInt_CAN_Rx_795f7144, TRUE   },  /* [A_CC_EnablePDUGroup_OBC_DCDC_oInt_CAN_Rx_795f7144] */
  { /*     2 */ ComConf_ComIPduGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4  , FALSE  },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     3 */ ComConf_ComIPduGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4  , TRUE   },  /* [A_CC_EnablePDUGroup_OBC_DCDC_oE_CAN_Rx_473dcfe4] */
  { /*     4 */ ComConf_ComIPduGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2, FALSE  },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     5 */ ComConf_ComIPduGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2, TRUE   },  /* [A_CC_EnablePDUGroup_OBC_DCDC_oInt_CAN_Tx_2f05d6c2] */
  { /*     6 */ ComConf_ComIPduGroup_OBC_DCDC_oE_CAN_Tx_11676862  , FALSE  },  /* [A_CC_DisablePDUGroup_OBC_DCDC_oE_CAN_Tx_11676862] */
  { /*     7 */ ComConf_ComIPduGroup_OBC_DCDC_oE_CAN_Tx_11676862  , TRUE   }   /* [A_CC_EnablePDUGroup_OBC_DCDC_oE_CAN_Tx_11676862] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DeferredRules
**********************************************************************************************************************/
/** 
  \var    BswM_DeferredRules
  \details
  Element     Description
  RulesIdx    the index of the 1:1 relation pointing to BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_DeferredRulesType, BSWM_CONST) BswM_DeferredRules[9] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RulesIdx        Referable Keys */
  { /*     0 */       0u },  /* [R_ESH_RunToPostRun] */
  { /*     1 */       1u },  /* [R_ESH_WaitToShutdown] */
  { /*     2 */       2u },  /* [R_ESH_WakeupToPrep] */
  { /*     3 */       3u },  /* [R_ESH_WaitToWakeup] */
  { /*     4 */       4u },  /* [R_ESH_WakeupToRun] */
  { /*     5 */       5u },  /* [R_ESH_InitToWakeup] */
  { /*     6 */       8u },  /* [R_ESH_PostRun] */
  { /*     7 */       9u },  /* [R_ESH_PrepToWait] */
  { /*     8 */      16u }   /* [R_BswMRule_ProgReset] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_EcuMRunRequestMapping
**********************************************************************************************************************/
/** 
  \var    BswM_EcuMRunRequestMapping
  \brief  Maps the external id of BswMEcuMRUNRequestIndication to an internal id and references immediate request ports.
  \details
  Element       Description
  ExternalId    External id of BswMEcuMRUNRequestIndication.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_EcuMRunRequestMappingType, BSWM_CONST) BswM_EcuMRunRequestMapping[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                     Referable Keys */
  { /*     0 */ ECUM_STATE_APP_POST_RUN },  /* [ECUM_RUNREQUEST_0, MRP_ESH_EcuMRequest_POSTRUN] */
  { /*     1 */ ECUM_STATE_APP_RUN      }   /* [ECUM_RUNREQUEST_1, MRP_ESH_EcuMRequest_RUN] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_EcuMStateSwitchParameters
**********************************************************************************************************************/
/** 
  \var    BswM_EcuMStateSwitchParameters
  \details
  Element        Description
  TargetState
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_EcuMStateSwitchParametersType, BSWM_CONST) BswM_EcuMStateSwitchParameters[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TargetState                    Referable Keys */
  { /*     0 */ ECUM_STATE_APP_POST_RUN },  /* [A_SwitchEcuMStateTo_POST_RUN] */
  { /*     1 */ ECUM_STATE_SHUTDOWN     },  /* [A_SwitchEcuMStateTo_SHUTDOWN] */
  { /*     2 */ ECUM_STATE_APP_RUN      }   /* [A_SwitchEcuMStateTo_RUN] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Expressions
**********************************************************************************************************************/
/** 
  \var    BswM_Expressions
  \brief  Holds pointer to all expression functions.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ExpressionFuncType, BSWM_CONST) BswM_Expressions[28] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     Expressions                                           Referable Keys */
  /*     0 */ BswM_ExpFct_ESH_LE_WriteNotPending_Left          ,  /* [LE_ESH_LE_WriteNotPending] */
  /*     1 */ BswM_ExpFct_ESH_LE_WakeupToRun_Left              ,  /* [LE_ESH_LE_WakeupToRun] */
  /*     2 */ BswM_ExpFct_ESH_LE_WakeupToPrepShutdown_Left     ,  /* [LE_ESH_LE_WakeupToPrepShutdown] */
  /*     3 */ BswM_ExpFct_ESH_LE_WaitForNvMToWakeup_Left       ,  /* [LE_ESH_LE_WaitForNvMToWakeup] */
  /*     4 */ BswM_ExpFct_ESH_LE_WaitForNvMToShutdown_Left     ,  /* [LE_ESH_LE_WaitForNvMToShutdown] */
  /*     5 */ BswM_ExpFct_ESH_LE_ValidWakeup_Left              ,  /* [LE_ESH_LE_ValidWakeup] */
  /*     6 */ BswM_ExpFct_ESH_LE_RunToPostRunTransition_Left   ,  /* [LE_ESH_LE_RunToPostRunTransition] */
  /*     7 */ BswM_ExpFct_ESH_LE_RunRequestsOrWakeup_Left      ,  /* [LE_ESH_LE_RunRequestsOrWakeup] */
  /*     8 */ BswM_ExpFct_ESH_LE_RunRequested_Left             ,  /* [LE_ESH_LE_RunRequested] */
  /*     9 */ BswM_ExpFct_ESH_LE_RunReleased_Left              ,  /* [LE_ESH_LE_RunReleased] */
  /*    10 */ BswM_ExpFct_ESH_LE_RunReached_Left               ,  /* [LE_ESH_LE_RunReached] */
  /*    11 */ BswM_ExpFct_ESH_LE_PrepShutdownToWaitForNvM_Left ,  /* [LE_ESH_LE_PrepShutdownToWaitForNvM] */
  /*    12 */ BswM_ExpFct_ESH_LE_PostRunToRun_Left             ,  /* [LE_ESH_LE_PostRunToRun] */
  /*    13 */ BswM_ExpFct_ESH_LE_PostRunToPrep_Left            ,  /* [LE_ESH_LE_PostRunToPrep] */
  /*    14 */ BswM_ExpFct_ESH_LE_PostRunReleased_Left          ,  /* [LE_ESH_LE_PostRunReleased] */
  /*    15 */ BswM_ExpFct_ESH_LE_PostRun_Left                  ,  /* [LE_ESH_LE_PostRun] */
  /*    16 */ BswM_ExpFct_ESH_LE_NoWakeupOrKillAll_Left        ,  /* [LE_ESH_LE_NoWakeupOrKillAll] */
  /*    17 */ BswM_ExpFct_ESH_LE_NoWakeup_Left                 ,  /* [LE_ESH_LE_NoWakeup] */
  /*    18 */ BswM_ExpFct_ESH_LE_NoRunReasons_Left             ,  /* [LE_ESH_LE_NoRunReasons] */
  /*    19 */ BswM_ExpFct_ESH_LE_NoRunAndNoKillAllRequest_Left ,  /* [LE_ESH_LE_NoRunAndNoKillAllRequest] */
  /*    20 */ BswM_ExpFct_ESH_LE_InitToWakeup_Left             ,  /* [LE_ESH_LE_InitToWakeup] */
  /*    21 */ BswM_ExpFct_ESH_LE_EcuMStateSleepOrShutdown_Left ,  /* [LE_ESH_LE_EcuMStateSleepOrShutdown] */
  /*    22 */ BswM_ExpFct_ESH_LE_CancelNotPending_Left         ,  /* [LE_ESH_LE_CancelNotPending] */
  /*    23 */ BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_TX_Left    ,  /* [LE_CC_LE_CN_Int_CAN_b597612f_RX_DM, LE_CC_LE_CN_Int_CAN_b597612f_TX] */
  /*    24 */ BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_RX_Left    ,  /* [LE_CC_LE_CN_Int_CAN_b597612f_RX] */
  /*    25 */ BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_DM_Left   ,  /* [LE_CC_LE_CN_E_CAN_7f812c72_RX_DM, LE_CC_LE_CN_E_CAN_7f812c72_TX] */
  /*    26 */ BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_Left      ,  /* [LE_CC_LE_CN_E_CAN_7f812c72_RX] */
  /*    27 */ BswM_ExpFct_BswMLogicalExpression_ProgReset_Left    /* [LE_BswMLogicalExpression_ProgReset] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_GenericModeParameters
**********************************************************************************************************************/
/** 
  \var    BswM_GenericModeParameters
  \details
  Element    Description
  Mode   
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_GenericModeParametersType, BSWM_CONST) BswM_GenericModeParameters[5] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Mode                                                 Referable Keys */
  { /*     0 */ BSWM_GENERICVALUE_ESH_State_ESH_POST_RUN      },  /* [A_ESH_Action_ESH_PostRun] */
  { /*     1 */ BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN },  /* [A_ESH_Action_ESH_PrepShutdown] */
  { /*     2 */ BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP        },  /* [A_ESH_Action_ESH_Wakeup] */
  { /*     3 */ BSWM_GENERICVALUE_ESH_State_ESH_RUN           },  /* [A_ESH_Action_ESH_Run] */
  { /*     4 */ BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM  }   /* [A_ESH_Action_ESH_WaitForNvm] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ImmediateUser
**********************************************************************************************************************/
/** 
  \var    BswM_ImmediateUser
  \brief  Contains all immediate request ports.
  \details
  Element             Description
  MaskedBits          contains bitcoded the boolean data of BswM_OnInitOfImmediateUser, BswM_RulesIndUsedOfImmediateUser
  RulesIndEndIdx      the end index of the 0:n relation pointing to BswM_RulesInd
  RulesIndStartIdx    the start index of the 0:n relation pointing to BswM_RulesInd
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ImmediateUserType, BSWM_CONST) BswM_ImmediateUser[3] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MaskedBits  RulesIndEndIdx  RulesIndStartIdx        Comment                                                    Referable Keys */
  { /*     0 */      0x01u,             3u,               0u },  /* [Name: CC_CanSMIndication_CN_E_CAN_7f812c72]   */  /* [MRP_CC_CanSMIndication_CN_E_CAN_7f812c72, CANSM_CHANNEL_0] */
  { /*     1 */      0x01u,             6u,               3u },  /* [Name: CC_CanSMIndication_CN_Int_CAN_b597612f] */  /* [MRP_CC_CanSMIndication_CN_Int_CAN_b597612f, CANSM_CHANNEL_1] */
  { /*     2 */      0x03u,            14u,               6u }   /* [Name: ESH_State]                              */  /* [MRP_ESH_State, GENERIC_0] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_InitGenVarAndInitAL
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_InitGenVarAndInitALType, BSWM_CONST) BswM_InitGenVarAndInitAL[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     InitGenVarAndInitAL                            */
  /*     0 */ BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION 
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeNotificationFct
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_PartitionFunctionType, BSWM_CONST) BswM_ModeNotificationFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ModeNotificationFct                            */
  /*     0 */ BswM_ModeNotificationFct_BSWM_SINGLEPARTITION 
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_PartitionIdentifiers
**********************************************************************************************************************/
/** 
  \var    BswM_PartitionIdentifiers
  \brief  the partition contex in Config_Left
  \details
  Element                 Description
  PartitionSNV        
  PCPartitionConfigIdx    the index of the 1:1 relation pointing to BswM_PCPartitionConfig
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_PartitionIdentifiersType, BSWM_CONST) BswM_PartitionIdentifiers[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    PartitionSNV          PCPartitionConfigIdx */
  { /*     0 */ BSWM_SINGLEPARTITION,                   0u }
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Rules
**********************************************************************************************************************/
/** 
  \var    BswM_Rules
  \details
  Element                Description
  ActionListsFalseIdx    the index of the 0:1 relation pointing to BswM_ActionLists
  ActionListsTrueIdx     the index of the 0:1 relation pointing to BswM_ActionLists
  ExpressionsIdx         the index of the 1:1 relation pointing to BswM_Expressions
  Id                     External id of rule.
  MaskedBits             contains bitcoded the boolean data of BswM_ActionListsFalseUsedOfRules, BswM_ActionListsTrueUsedOfRules
  RuleStatesIdx          the index of the 1:1 relation pointing to BswM_RuleStates
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_RulesType, BSWM_CONST) BswM_Rules[17] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ActionListsFalseIdx                 ActionListsTrueIdx  ExpressionsIdx  Id   MaskedBits  RuleStatesIdx        Referable Keys */
  { /*     0 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 1u,             6u, 12u,      0x01u,            0u },  /* [R_ESH_RunToPostRun, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_ESH_EcuMState, MRP_ESH_RunRequest_0, MRP_ESH_RunRequest_1, MRP_ESH_EcuMRequest_RUN, MRP_ESH_SelfRunRequestTimer] */
  { /*     1 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 2u,             4u, 13u,      0x01u,            1u },  /* [R_ESH_WaitToShutdown, MRP_ESH_State, MRP_ESH_NvMIndication, MRP_ESH_NvM_WriteAllTimer, MRP_ESH_EcuM_GetValidatedWakeupEvents] */
  { /*     2 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 3u,             2u, 15u,      0x01u,            2u },  /* [R_ESH_WakeupToPrep, MRP_ESH_State, MRP_ESH_NvMIndication, MRP_ESH_NvM_CancelWriteAllTimer, MRP_ESH_EcuM_GetPendingWakeupEvents, MRP_ESH_EcuM_GetValidatedWakeupEvents, MRP_ESH_ModeNotification] */
  { /*     3 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 4u,             3u, 14u,      0x01u,            3u },  /* [R_ESH_WaitToWakeup, MRP_ESH_State, MRP_ESH_EcuM_GetValidatedWakeupEvents] */
  { /*     4 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 5u,             1u, 16u,      0x01u,            4u },  /* [R_ESH_WakeupToRun, MRP_ESH_State, MRP_ESH_EcuM_GetValidatedWakeupEvents, MRP_ESH_NvMIndication, MRP_ESH_NvM_CancelWriteAllTimer, MRP_ESH_ModeNotification] */
  { /*     5 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 6u,            20u,  7u,      0x01u,            5u },  /* [R_ESH_InitToWakeup, MRP_ESH_State, MRP_ESH_EcuMState] */
  { /*     6 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                 7u,            13u, 10u,      0x01u,            6u },  /* [R_ESH_PostRunToPrepNested] */
  { /*     7 */                                 8u,                 9u,            12u,  9u,      0x03u,            7u },  /* [R_ESH_PostRunNested] */
  { /*     8 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                10u,            15u,  8u,      0x01u,            8u },  /* [R_ESH_PostRun, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_ESH_EcuMState] */
  { /*     9 */ BSWM_NO_ACTIONLISTSFALSEIDXOFRULES,                11u,            11u, 11u,      0x01u,            9u },  /* [R_ESH_PrepToWait, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_ESH_EcuMState] */
  { /*    10 */                                12u,                13u,            24u,  4u,      0x03u,           10u },  /* [R_CC_CN_Int_CAN_b597612f_RX, MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
  { /*    11 */                                14u,                15u,            26u,  1u,      0x03u,           11u },  /* [R_CC_CN_E_CAN_7f812c72_RX, MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  { /*    12 */                                16u,                17u,            25u,  2u,      0x03u,           12u },  /* [R_CC_CN_E_CAN_7f812c72_RX_DM, MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  { /*    13 */                                18u,                19u,            23u,  6u,      0x03u,           13u },  /* [R_CC_CN_Int_CAN_b597612f_TX, MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
  { /*    14 */                                20u,                21u,            25u,  3u,      0x03u,           14u },  /* [R_CC_CN_E_CAN_7f812c72_TX, MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  { /*    15 */                                22u,                23u,            23u,  5u,      0x03u,           15u },  /* [R_CC_CN_Int_CAN_b597612f_RX_DM, MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
  { /*    16 */                                24u,                25u,            27u,  0u,      0x03u,           16u }   /* [R_BswMRule_ProgReset, MRP_ResetsModeNotification] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_RulesInd
**********************************************************************************************************************/
/** 
  \var    BswM_RulesInd
  \brief  the indexes of the 1:1 sorted relation pointing to BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_RulesIndType, BSWM_CONST) BswM_RulesInd[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RulesInd      Referable Keys */
  /*     0 */       11u,  /* [MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  /*     1 */       12u,  /* [MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  /*     2 */       14u,  /* [MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  /*     3 */       10u,  /* [MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
  /*     4 */       13u,  /* [MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
  /*     5 */       15u,  /* [MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */
  /*     6 */        0u,  /* [MRP_ESH_State] */
  /*     7 */        1u,  /* [MRP_ESH_State] */
  /*     8 */        2u,  /* [MRP_ESH_State] */
  /*     9 */        3u,  /* [MRP_ESH_State] */
  /*    10 */        4u,  /* [MRP_ESH_State] */
  /*    11 */        5u,  /* [MRP_ESH_State] */
  /*    12 */        8u,  /* [MRP_ESH_State] */
  /*    13 */        9u   /* [MRP_ESH_State] */
};
#define BSWM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_SwcModeRequestUpdateFct
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_PartitionFunctionType, BSWM_CONST) BswM_SwcModeRequestUpdateFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     SwcModeRequestUpdateFct                            */
  /*     0 */ BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION 
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerControlParameters
**********************************************************************************************************************/
/** 
  \var    BswM_TimerControlParameters
  \details
  Element    Description
  Value  
  Timer  
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_TimerControlParametersType, BSWM_CONST) BswM_TimerControlParameters[5] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Value   Timer                                            Referable Keys */
  { /*     0 */ 0u    , BSWM_TMR_ESH_NvM_WriteAllTimer_Left       },  /* [A_ESH_Action_WriteAllTimer_Stop] */
  { /*     1 */ 6000uL, BSWM_TMR_ESH_NvM_CancelWriteAllTimer_Left },  /* [A_ESH_Action_CancelWriteAllTimer_Start] */
  { /*     2 */ 0u    , BSWM_TMR_ESH_NvM_CancelWriteAllTimer_Left },  /* [A_ESH_Action_CancelWriteAllTimer_Stop] */
  { /*     3 */ 500uL , BSWM_TMR_ESH_SelfRunRequestTimer_Left     },  /* [A_ESH_Action_SelfRunRequestTimer_Start] */
  { /*     4 */ 6000uL, BSWM_TMR_ESH_NvM_WriteAllTimer_Left       }   /* [A_ESH_Action_WriteAllTimer_Start] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ActionListQueue
**********************************************************************************************************************/
/** 
  \var    BswM_ActionListQueue
  \brief  Variable to store action lists which shall be executed.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ActionListQueueUType, BSWM_VAR_NOINIT) BswM_ActionListQueue;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [AL_INIT_AL_Initialize] */
  /*     1 */  /* [AL_ESH_AL_RunToPostRun] */
  /*     2 */  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  /*     3 */  /* [AL_ESH_AL_WakeupToPrep] */
  /*     4 */  /* [AL_ESH_AL_WaitForNvMWakeup] */
  /*     5 */  /* [AL_ESH_AL_WakeupToRun] */
  /*     6 */  /* [AL_ESH_AL_InitToWakeup] */
  /*     7 */  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  /*     8 */  /* [AL_ESH_AL_ESH_PostRunToPrepCheck] */
  /*     9 */  /* [AL_ESH_AL_PostRunToRun] */
  /*    10 */  /* [AL_ESH_AL_ExitPostRun] */
  /*    11 */  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  /*    12 */  /* [AL_CC_AL_CN_Int_CAN_b597612f_RX_Disable] */
  /*    13 */  /* [AL_CC_AL_CN_Int_CAN_b597612f_RX_EnableNoinit] */
  /*    14 */  /* [AL_CC_AL_CN_E_CAN_7f812c72_RX_Disable] */
  /*    15 */  /* [AL_CC_AL_CN_E_CAN_7f812c72_RX_EnableNoinit] */
  /*    16 */  /* [AL_CC_AL_CN_E_CAN_7f812c72_Disable_DM] */
  /*    17 */  /* [AL_CC_AL_CN_E_CAN_7f812c72_Enable_DM] */
  /*    18 */  /* [AL_CC_AL_CN_Int_CAN_b597612f_TX_Disable] */
  /*    19 */  /* [AL_CC_AL_CN_Int_CAN_b597612f_TX_EnableNoinit] */
  /*    20 */  /* [AL_CC_AL_CN_E_CAN_7f812c72_TX_Disable] */
  /*    21 */  /* [AL_CC_AL_CN_E_CAN_7f812c72_TX_EnableNoinit] */
  /*    22 */  /* [AL_CC_AL_CN_Int_CAN_b597612f_Disable_DM] */
  /*    23 */  /* [AL_CC_AL_CN_Int_CAN_b597612f_Enable_DM] */
  /*    24 */  /* [AL_BswMActionList_ProgReset] */
  /*    25 */  /* [AL_BswMActionList_ProgResetFail] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_CanSMChannelState
**********************************************************************************************************************/
/** 
  \var    BswM_CanSMChannelState
  \brief  Variable to store current mode of BswMCanSMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanSM_BswMCurrentStateType, BSWM_VAR_NOINIT) BswM_CanSMChannelState[2];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [CANSM_CHANNEL_0, MRP_CC_CanSMIndication_CN_E_CAN_7f812c72] */
  /*     1 */  /* [CANSM_CHANNEL_1, MRP_CC_CanSMIndication_CN_Int_CAN_b597612f] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_EcuMModeState
**********************************************************************************************************************/
/** 
  \var    BswM_EcuMModeState
  \brief  Variable to store the current mode of the BswMEcuMIndication mode request port.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_EcuMModeStateType, BSWM_VAR_NOINIT) BswM_EcuMModeState;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_EcuMRunRequestState
**********************************************************************************************************************/
/** 
  \var    BswM_EcuMRunRequestState
  \brief  Variable to store current mode of BswMEcuMRUNRequestIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(EcuM_RunStatusType, BSWM_VAR_NOINIT) BswM_EcuMRunRequestState[2];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [ECUM_RUNREQUEST_0, MRP_ESH_EcuMRequest_POSTRUN] */
  /*     1 */  /* [ECUM_RUNREQUEST_1, MRP_ESH_EcuMRequest_RUN] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ForcedActionListPriority
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ForcedActionListPriorityType, BSWM_VAR_NOINIT) BswM_ForcedActionListPriority;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_GenericState
**********************************************************************************************************************/
/** 
  \var    BswM_GenericState
  \brief  Variable to store current mode of BswMGenericRequest mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ModeType, BSWM_VAR_NOINIT) BswM_GenericState[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [GENERIC_0, MRP_ESH_State] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Initialized
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_InitializedType, BSWM_VAR_NOINIT) BswM_Initialized;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeRequestQueue
**********************************************************************************************************************/
/** 
  \var    BswM_ModeRequestQueue
  \brief  Variable to store an immediate mode request which must be queued because of a current active arbitration.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ModeRequestQueueType, BSWM_VAR_NOINIT) BswM_ModeRequestQueue[3];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [MRP_CC_CanSMIndication_CN_E_CAN_7f812c72, CANSM_CHANNEL_0] */
  /*     1 */  /* [MRP_CC_CanSMIndication_CN_Int_CAN_b597612f, CANSM_CHANNEL_1] */
  /*     2 */  /* [MRP_ESH_State, GENERIC_0] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_NvMJobState
**********************************************************************************************************************/
/** 
  \var    BswM_NvMJobState
  \brief  Variable to store current mode of BswMNvMJobModeIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(NvM_RequestResultType, BSWM_VAR_NOINIT) BswM_NvMJobState[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [NVM_JOB_0, MRP_ESH_NvMIndication] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_QueueSemaphore
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_QueueSemaphoreType, BSWM_VAR_NOINIT) BswM_QueueSemaphore;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_QueueWritten
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_QueueWrittenType, BSWM_VAR_NOINIT) BswM_QueueWritten;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_RuleStates
**********************************************************************************************************************/
/** 
  \var    BswM_RuleStates
  \brief  Stores the last execution state of the rule.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_RuleStatesUType, BSWM_VAR_NOINIT) BswM_RuleStates;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [R_ESH_RunToPostRun] */
  /*     1 */  /* [R_ESH_WaitToShutdown] */
  /*     2 */  /* [R_ESH_WakeupToPrep] */
  /*     3 */  /* [R_ESH_WaitToWakeup] */
  /*     4 */  /* [R_ESH_WakeupToRun] */
  /*     5 */  /* [R_ESH_InitToWakeup] */
  /*     6 */  /* [R_ESH_PostRunToPrepNested] */
  /*     7 */  /* [R_ESH_PostRunNested] */
  /*     8 */  /* [R_ESH_PostRun] */
  /*     9 */  /* [R_ESH_PrepToWait] */
  /*    10 */  /* [R_CC_CN_Int_CAN_b597612f_RX] */
  /*    11 */  /* [R_CC_CN_E_CAN_7f812c72_RX] */
  /*    12 */  /* [R_CC_CN_E_CAN_7f812c72_RX_DM] */
  /*    13 */  /* [R_CC_CN_Int_CAN_b597612f_TX] */
  /*    14 */  /* [R_CC_CN_E_CAN_7f812c72_TX] */
  /*    15 */  /* [R_CC_CN_Int_CAN_b597612f_RX_DM] */
  /*    16 */  /* [R_BswMRule_ProgReset] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerState
**********************************************************************************************************************/
/** 
  \var    BswM_TimerState
  \brief  Variable to store current state of BswMTimer (STARTED, STOPPER OR EXPIRED).
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_TimerStateUType, BSWM_VAR_NOINIT) BswM_TimerState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [MRP_ESH_NvM_CancelWriteAllTimer] */
  /*     1 */  /* [MRP_ESH_NvM_WriteAllTimer] */
  /*     2 */  /* [MRP_ESH_SelfRunRequestTimer] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerValue
**********************************************************************************************************************/
/** 
  \var    BswM_TimerValue
  \brief  Variable to store current timer values.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_TimerValueUType, BSWM_VAR_NOINIT) BswM_TimerValue;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [MRP_ESH_NvM_CancelWriteAllTimer] */
  /*     1 */  /* [MRP_ESH_NvM_WriteAllTimer] */
  /*     2 */  /* [MRP_ESH_SelfRunRequestTimer] */

#define BSWM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/* PRQA L:GLOBALDATADECLARATIONS */

#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode;

VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_RunRequest_0_requestedMode;
VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_RunRequest_1_requestedMode;
VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_PostRunRequest_0_requestedMode;
VAR(BswM_ESH_RunRequest, BSWM_VAR_NOINIT) Request_ESH_PostRunRequest_1_requestedMode;
VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode;
VAR(Rte_ModeType_DcmEcuReset, BSWM_VAR_NOINIT) BswM_Mode_Notification_ResetsModeNotification_DcmEcuReset;


VAR(boolean, BSWM_VAR_NOINIT) BswM_PreInitialized;
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


#define BSWM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* -----------------------------------------------------------------------------
    &&&~ FUNCTIONS
 ----------------------------------------------------------------------------- */
 
/**********************************************************************************************************************
 *  BswM_ExecuteIpduGroupControl()
 **********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_ExecuteIpduGroupControl(void)
{
  Com_IpduGroupVector ipduGroupState;
  Com_IpduGroupVector dmState;
  uint16 iCnt;
  uint8 controlInvocation = BSWM_GROUPCONTROL_IDLE;

  SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  if(BswM_PduGroupControlInvocation != BSWM_GROUPCONTROL_IDLE)
  {
    if((BswM_PduGroupControlInvocation & BSWM_GROUPCONTROL_NORMAL) != 0u)
    {
      iCnt = BSWM_IPDUGROUPVECTORSIZE;
      while(iCnt-- > (uint16)0x0000) /* PRQA S 3440 */ /* MD_BswM_3440 */
      {
        ipduGroupState[iCnt] = BswM_ComIPduGroupState[iCnt]; /* SBSW_BSWM_SETIPDUGROUPVECTOR */
      }
    }
    if((BswM_PduGroupControlInvocation & BSWM_GROUPCONTROL_DM) != 0u)
    {
      iCnt = BSWM_IPDUGROUPVECTORSIZE;
      while(iCnt-- > (uint16)0x0000) /* PRQA S 3440 */ /* MD_BswM_3440 */
      {
        dmState[iCnt] = BswM_ComRxIPduGroupDMState[iCnt]; /* SBSW_BSWM_SETIPDUGROUPVECTOR */
      }
    }
    controlInvocation = BswM_PduGroupControlInvocation;
    BswM_PduGroupControlInvocation = BSWM_GROUPCONTROL_IDLE;
  }
  SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

  if(controlInvocation != BSWM_GROUPCONTROL_IDLE)
  {
    if((controlInvocation & BSWM_GROUPCONTROL_NORMAL) != 0u)
    {
      Com_IpduGroupControl(ipduGroupState, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    }
    if((controlInvocation & BSWM_GROUPCONTROL_DM) != 0u)
    {
      Com_ReceptionDMControl(dmState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    }
  }
} /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/**********************************************************************************************************************
 *  BswM_Action_RuleHandler()
 **********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_RuleHandler(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType actionListIndex;
  Std_ReturnType retVal = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
#if ( BSWM_DEV_ERROR_DETECT == STD_ON )
  if (handleId < BswM_GetSizeOfRules(partitionIdx))
#endif
  {
    SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    actionListIndex = BswM_ArbitrateRule(handleId, partitionIdx);
    SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    if(actionListIndex < BswM_GetSizeOfActionLists(partitionIdx))
    {
      retVal = BswM_Action_ActionListHandler(actionListIndex, partitionIdx);
    }
    else
    {
      retVal = E_OK;
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  
  return retVal;
} 


/**********************************************************************************************************************
 *  BswM_UpdateTimer()
 **********************************************************************************************************************/
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_UpdateTimer(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx,
                                                              BswM_SizeOfTimerValueType timerId,
                                                              BswM_TimerValueType value)
{
  if (timerId < BswM_GetSizeOfTimerValue(partitionIdx))
  {
      SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      BswM_SetTimerValue(timerId, value, partitionIdx); /* SBSW_BSWM_SETTIMER */
      BswM_SetTimerState(timerId, (BswM_TimerStateType)((value != 0u) ? BSWM_TIMER_STARTED : BSWM_TIMER_STOPPED), partitionIdx); /* SBSW_BSWM_SETTIMER */
      SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}

/**********************************************************************************************************************
 *  BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  uint16 uIndex;

  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = 0xFFu;
  Request_ESH_RunRequest_0_requestedMode = RELEASED;
  Request_ESH_RunRequest_1_requestedMode = RELEASED;
  Request_ESH_PostRunRequest_0_requestedMode = RELEASED;
  Request_ESH_PostRunRequest_1_requestedMode = RELEASED;
  BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_STARTUP;
  BswM_Mode_Notification_ResetsModeNotification_DcmEcuReset = RTE_MODE_DcmEcuReset_NONE;
  BswM_PduGroupControlInvocation = BSWM_GROUPCONTROL_IDLE;

  /* PRQA S 3109 COMCLEARIPDU */ /* MD_BswM_3109 */
  Com_ClearIpduGroupVector(BswM_ComIPduGroupState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  Com_ClearIpduGroupVector(BswM_ComRxIPduGroupDMState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMCLEARIPDU */

  for(uIndex = 0u; uIndex < BswM_GetSizeOfInitActionLists(partitionIdx); uIndex++) /* COV_BSWM_INITACTIONLISTS XF */ 
  {
    (void)BswM_Action_ActionListHandler((BswM_HandleType)BswM_GetInitActionLists(uIndex, partitionIdx), partitionIdx);
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}

/**********************************************************************************************************************
 *  BswM_ModeNotificationFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_ModeNotificationFct_BSWM_SINGLEPARTITION(void)
{
  if(BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode != 0xFFu)
  {
    if(Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode) == RTE_E_OK)
    {
      BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = 0xFFu;
    }
  }
}

/**********************************************************************************************************************
 *  BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION(void)
{
  uint32 mode;
  mode = Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode();
  if (mode != RTE_TRANSITION_ESH_Mode)
  {
    BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode = (Rte_ModeType_ESH_Mode)mode;
  }
  mode = Rte_Mode_Notification_ResetsModeNotification_DcmEcuReset();
  if (mode != RTE_TRANSITION_DcmEcuReset)
  {
    BswM_Mode_Notification_ResetsModeNotification_DcmEcuReset = (Rte_ModeType_DcmEcuReset)mode;
  }
  (void)Rte_Read_Request_ESH_RunRequest_0_requestedMode(&Request_ESH_RunRequest_0_requestedMode); /* SBSW_BSWM_RTE_READ */
  (void)Rte_Read_Request_ESH_RunRequest_1_requestedMode(&Request_ESH_RunRequest_1_requestedMode); /* SBSW_BSWM_RTE_READ */
  (void)Rte_Read_Request_ESH_PostRunRequest_0_requestedMode(&Request_ESH_PostRunRequest_0_requestedMode); /* SBSW_BSWM_RTE_READ */
  (void)Rte_Read_Request_ESH_PostRunRequest_1_requestedMode(&Request_ESH_PostRunRequest_1_requestedMode); /* SBSW_BSWM_RTE_READ */
}

/**********************************************************************************************************************
 *  BswMActionFunctions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Can_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Can_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Can_Init(Can_Config_Left_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_CanIf_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanIf_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanIf_Init(CanIf_Config_Left_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Com_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Com_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Com_Init(Com_Config_Left_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_PduR_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_PduR_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  PduR_Init(PduR_Config_Left_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_CanSM_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanSM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanSM_Init(CanSM_Config_Left_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_ComM_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_ComM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  ComM_Init(ComM_Config_Left_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Rte_Start_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Rte_Start_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Rte_Start();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_EnableInterrupts_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EnableInterrupts_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_AL_SetProgrammableInterrupts();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_NvM_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_NvM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_Init();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_EcuMClearValidatedWakeupEvents_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_EcuMClearValidatedWakeupEvents_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  EcuM_ClearValidatedWakeupEvent(ECUM_WKSOURCE_ALL_SOURCES);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_OnEnterPostRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterPostRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_ESH_OnEnterPostRun();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_SwitchPostRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchPostRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_POSTRUN;
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_GenericMode
 *********************************************************************************************************************/
FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_GenericMode(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BswM_RequestMode(BswM_GetUserOfGenericModeParameters(handleId, partitionIdx), BswM_GetModeOfGenericModeParameters(handleId, partitionIdx));
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_TimerControl
 *********************************************************************************************************************/
FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_TimerControl(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BswM_UpdateTimer(partitionIdx, BswM_GetTimerOfTimerControlParameters(handleId, partitionIdx), BswM_GetValueOfTimerControlParameters(handleId, partitionIdx));
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_OnEnterShutdown_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterShutdown_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_ESH_OnEnterShutdown();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_EcuMGoToSelectedShutdownTarget_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_EcuMGoToSelectedShutdownTarget_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  (void)EcuM_GoToSelectedShutdownTarget();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_OnEnterPrepShutdown_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterPrepShutdown_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_ESH_OnEnterPrepShutdown();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_SwitchShutdown_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchShutdown_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_SHUTDOWN;
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_CancelNvMWriteAll_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_CancelNvMWriteAll_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_CancelWriteAll();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_OnEnterWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterWakeup_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_ESH_OnEnterWakeup();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_SwitchWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchWakeup_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_WAKEUP;
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_OnEnterRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_ESH_OnEnterRun();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_SwitchRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_SwitchRun_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_RUN;
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_NvMWriteAll_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_NvMWriteAll_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_WriteAll();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ESH_Action_OnEnterWaitForNvm_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ESH_Action_OnEnterWaitForNvm_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_ESH_OnEnterWaitForNvm();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_NvM_Pre_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_NvM_Pre_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  NvM_Call_PreInit();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ComPduGroupSwitch
 *********************************************************************************************************************/
FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ComPduGroupSwitch(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BswM_SizeOfComPduGroupSwitchSubParametersType idx;
  BswM_SizeOfComPduGroupSwitchSubParametersType idxControl;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  for(idx = BswM_GetComPduGroupSwitchSubParametersStartIdxOfComPduGroupSwitchParameters(handleId, partitionIdx); idx < BswM_GetComPduGroupSwitchSubParametersEndIdxOfComPduGroupSwitchParameters(handleId, partitionIdx); idx++)
  {
    BswM_SetIpduGroup(BswM_GetIpduGroupIdOfComPduGroupSwitchSubParameters(idx, partitionIdx), BswM_GetBitValOfComPduGroupSwitchSubParameters(idx, partitionIdx)); /* PRQA S 3109 */ /* MD_BswM_3109 */ /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    BswM_SetIpduDMGroup(BswM_GetIpduGroupIdOfComPduGroupSwitchSubParameters(idx, partitionIdx), BswM_GetBitValOfComPduGroupSwitchSubParameters(idx, partitionIdx)); /* PRQA S 3109 */ /* MD_BswM_3109 */ /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  if(BswM_GetControlOfComPduGroupSwitchParameters(handleId, partitionIdx) != BSWM_GROUPCONTROL_NORMAL)
  {
    for(idxControl = BswM_GetComPduGroupSwitchSubParametersStartIdxOfComPduGroupSwitchParameters(handleId, partitionIdx); idxControl < BswM_GetComPduGroupSwitchSubParametersEndIdxOfComPduGroupSwitchParameters(handleId, partitionIdx); idxControl++)
    {
      BswM_SetIpduReinitGroup(BswM_GetIpduGroupIdOfComPduGroupSwitchSubParameters(idxControl, partitionIdx), BswM_GetBitValOfComPduGroupSwitchSubParameters(idxControl, partitionIdx)); /* PRQA S 3109 */ /* MD_BswM_3109 */ /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BswM_MarkPduGroupControlInvocation(BswM_GetControlOfComPduGroupSwitchParameters(handleId, partitionIdx));
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_ComDMControl
 *********************************************************************************************************************/
FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ComDMControl(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BswM_SizeOfComDMControlSubParametersType idx;
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 3112, 3199 */ /* MD_MSR_14.2 */ /*lint -e{438} */
  
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  for(idx = BswM_GetComDMControlSubParametersStartIdxOfComDMControlParameters(handleId, partitionIdx); idx < BswM_GetComDMControlSubParametersEndIdxOfComDMControlParameters(handleId, partitionIdx); idx++)
  {
    BswM_SetIpduDMGroup(BswM_GetIpduGroupIdOfComDMControlSubParameters(idx, partitionIdx), BswM_GetBitValOfComDMControlSubParameters(idx, partitionIdx)); /* PRQA S 3109 */ /* MD_BswM_3109 */ /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BswM_MarkDmControlInvocation();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_CanTp_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanTp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanTp_Init(CanTp_Config_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Dcm_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Dcm_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Dcm_Init(NULL_PTR);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_CanTrcv_30_Tja1145_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanTrcv_30_Tja1145_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanTrcv_30_Tja1145_Init(CanTrcv_30_Tja1145_Config_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Fee_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Fee_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Fee_Init();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Xcp_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Xcp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Xcp_Init();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Dem_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Dem_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Dem_Init(Dem_Config_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_CanXcp_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_CanXcp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanXcp_Init(NULL_PTR);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_NvMReadAll_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_NvMReadAll_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  BswM_INIT_NvMReadAll();
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_EcuMStateSwitch
 *********************************************************************************************************************/
FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_EcuMStateSwitch(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  EcuM_SetState(BswM_GetTargetStateOfEcuMStateSwitchParameters(handleId, partitionIdx));
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_EcuMSelectShutdownTarget
 *********************************************************************************************************************/
FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_EcuMSelectShutdownTarget(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return EcuM_SelectShutdownTarget(BswM_GetTargetStateOfEcuMSelectShutdownTargetParameters(handleId, partitionIdx), BswM_GetResetSleepModeOfEcuMSelectShutdownTargetParameters(handleId, partitionIdx));
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_WdgM_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_WdgM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  WdgM_Init(&WdgMConfig_Mode0_core0);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_EthTrcv_30_Ar7000_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EthTrcv_30_Ar7000_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  EthTrcv_30_Ar7000_Init(NULL_PTR);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Eth_30_Ar7000_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Eth_30_Ar7000_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Eth_30_Ar7000_Init(NULL_PTR);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_EthIf_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EthIf_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  EthIf_Init(EthIf_Config_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_EthSM_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_EthSM_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  EthSM_Init(EthSM_Config_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_TcpIp_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_TcpIp_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  TcpIp_Init(TcpIp_Config_Ptr);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Exi_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Exi_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Exi_Init(NULL_PTR);
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswM_Action_INIT_Action_Scc_Init_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_INIT_Action_Scc_Init_Left(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(handleId); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */

  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Scc_Init();
  /*lint -restore */
  return E_OK;
}

/**********************************************************************************************************************
 *  BswMExpressionFunctions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_RunToPostRunTransition_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunToPostRunTransition_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_ExpFct_ESH_LE_RunReached_Left(partitionIdx) != 0u) && (BswM_ExpFct_ESH_LE_NoRunAndNoKillAllRequest_Left(partitionIdx) != 0u)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_RunReached_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunReached_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_RUN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_RUN) && (BswM_GetEcuMModeState(0u) == ECUM_STATE_APP_RUN)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_NoRunAndNoKillAllRequest_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoRunAndNoKillAllRequest_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_ExpFct_ESH_LE_NoRunReasons_Left(partitionIdx) != 0u) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_NoRunReasons_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoRunReasons_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_ExpFct_ESH_LE_RunReleased_Left(partitionIdx) != 0u) && (BswM_GetEcuMRunRequestState(1, 0u) == ECUM_RUNSTATUS_RELEASED) && (BswM_GetTimerState(2, 0u) == BSWM_TIMER_EXPIRED)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_RunReleased_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunReleased_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((Request_ESH_RunRequest_0_requestedMode == RELEASED) && (Request_ESH_RunRequest_1_requestedMode == RELEASED)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_WaitForNvMToShutdown_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WaitForNvMToShutdown_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM) && (BswM_ExpFct_ESH_LE_WriteNotPending_Left(partitionIdx) != 0u) && (BswM_ExpFct_ESH_LE_NoWakeupOrKillAll_Left(partitionIdx) != 0u)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_WriteNotPending_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WriteNotPending_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_GetNvMJobState(0, 0u) != NVM_REQ_PENDING) || (BswM_GetTimerState(1, 0u) != BSWM_TIMER_STARTED)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_NoWakeupOrKillAll_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoWakeupOrKillAll_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_ExpFct_ESH_LE_NoWakeup_Left(partitionIdx) != 0u) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_NoWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_NoWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((EcuM_GetValidatedWakeupEvents() == 0u) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_WakeupToPrepShutdown_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WakeupToPrepShutdown_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP) && (BswM_ExpFct_ESH_LE_CancelNotPending_Left(partitionIdx) != 0u) && (EcuM_GetPendingWakeupEvents() == 0u) && (BswM_ExpFct_ESH_LE_NoWakeup_Left(partitionIdx) != 0u) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_WAKEUP)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_CancelNotPending_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_CancelNotPending_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_GetNvMJobState(0, 0u) != NVM_REQ_PENDING) || (BswM_GetTimerState(0, 0u) != BSWM_TIMER_STARTED)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_WaitForNvMToWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WaitForNvMToWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM) && (BswM_ExpFct_ESH_LE_ValidWakeup_Left(partitionIdx) != 0u)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_ValidWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_ValidWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((EcuM_GetValidatedWakeupEvents() != 0u) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_WakeupToRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_WakeupToRun_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP) && (BswM_ExpFct_ESH_LE_ValidWakeup_Left(partitionIdx) != 0u) && (BswM_ExpFct_ESH_LE_CancelNotPending_Left(partitionIdx) != 0u) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_WAKEUP)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_InitToWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_InitToWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_INIT) && (BswM_GetEcuMModeState(0u) == ECUM_STATE_STARTUP)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_PostRunToPrep_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRunToPrep_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_ExpFct_ESH_LE_PostRunReleased_Left(partitionIdx) != 0u) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_PostRunReleased_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRunReleased_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((Request_ESH_PostRunRequest_0_requestedMode == RELEASED) && (Request_ESH_PostRunRequest_1_requestedMode == RELEASED) && (BswM_GetEcuMRunRequestState(0, 0u) == ECUM_RUNSTATUS_RELEASED)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_PostRunToRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRunToRun_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_ExpFct_ESH_LE_RunRequestsOrWakeup_Left(partitionIdx) != 0u) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_RunRequestsOrWakeup_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunRequestsOrWakeup_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_ExpFct_ESH_LE_RunRequested_Left(partitionIdx) != 0u) || (BswM_GetEcuMRunRequestState(1, 0u) == ECUM_RUNSTATUS_REQUESTED) || (BswM_ExpFct_ESH_LE_ValidWakeup_Left(partitionIdx) != 0u)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_RunRequested_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_RunRequested_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((Request_ESH_RunRequest_0_requestedMode == REQUESTED) || (Request_ESH_RunRequest_1_requestedMode == REQUESTED)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_PostRun_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PostRun_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_POST_RUN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_POSTRUN) && (BswM_GetEcuMModeState(0u) == ECUM_STATE_APP_POST_RUN)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_PrepShutdownToWaitForNvM_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_PrepShutdownToWaitForNvM_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
  return (uint8)(((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_SHUTDOWN) && (BswM_ExpFct_ESH_LE_EcuMStateSleepOrShutdown_Left(partitionIdx) != 0u)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_RX_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_RX_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_GetCanSMChannelState(1, 0u) != CANSM_BSWM_NO_COMMUNICATION) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_GetCanSMChannelState(0, 0u) != CANSM_BSWM_NO_COMMUNICATION) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_DM_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_E_CAN_7f812c72_RX_DM_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_GetCanSMChannelState(0, 0u) == CANSM_BSWM_FULL_COMMUNICATION) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_TX_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_CC_LE_CN_Int_CAN_b597612f_TX_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_GetCanSMChannelState(1, 0u) == CANSM_BSWM_FULL_COMMUNICATION) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_ESH_LE_EcuMStateSleepOrShutdown_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_ESH_LE_EcuMStateSleepOrShutdown_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)(((BswM_GetEcuMModeState(0u) == ECUM_STATE_SHUTDOWN) || (BswM_GetEcuMModeState(0u) == ECUM_STATE_SLEEP)) ? 1u : 0u);
}

/**********************************************************************************************************************
 *  BswM_ExpFct_BswMLogicalExpression_ProgReset_Left
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(uint8, BSWM_CODE) BswM_ExpFct_BswMLogicalExpression_ProgReset_Left(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return (uint8)((BswM_Mode_Notification_ResetsModeNotification_DcmEcuReset == RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER) ? 1u : 0u);
}


#define BSWM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

