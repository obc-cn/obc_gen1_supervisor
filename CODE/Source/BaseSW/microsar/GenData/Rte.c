/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  RTE implementation file
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0857 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define RTE_CORE
#include "Os.h" /* PRQA S 0828, 0883 */ /* MD_MSR_Dir1.1, MD_Rte_Os */
#include "Rte_Type.h"
#include "Rte_Main.h"

#include "Rte_BswM.h"
#include "Rte_ComM.h"
#include "Rte_CtApAEM.h"
#include "Rte_CtApBAT.h"
#include "Rte_CtApCHG.h"
#include "Rte_CtApCPT.h"
#include "Rte_CtApDCH.h"
#include "Rte_CtApDER.h"
#include "Rte_CtApDGN.h"
#include "Rte_CtApFCL.h"
#include "Rte_CtApFCT.h"
#include "Rte_CtApILT.h"
#include "Rte_CtApJDD.h"
#include "Rte_CtApLAD.h"
#include "Rte_CtApLED.h"
#include "Rte_CtApLFM.h"
#include "Rte_CtApLSD.h"
#include "Rte_CtApLVC.h"
#include "Rte_CtApMSC.h"
#include "Rte_CtApOBC.h"
#include "Rte_CtApOFM.h"
#include "Rte_CtApPCOM.h"
#include "Rte_CtApPLS.h"
#include "Rte_CtApPLT.h"
#include "Rte_CtApPSH.h"
#include "Rte_CtApPXL.h"
#include "Rte_CtApRCD.h"
#include "Rte_CtApRLY.h"
#include "Rte_CtApTBD.h"
#include "Rte_CtApWUM.h"
#include "Rte_CtHwAbsAIM.h"
#include "Rte_CtHwAbsIOM.h"
#include "Rte_CtHwAbsPIM.h"
#include "Rte_CtHwAbsPOM.h"
#include "Rte_Dcm.h"
#include "Rte_DemMaster_0.h"
#include "Rte_DemSatellite_0.h"
#include "Rte_Det.h"
#include "Rte_EcuM.h"
#include "Rte_NvM.h"
#include "Rte_Os_OsCore0_swc.h"
#include "Rte_WdgM_OsApplication_ASILB.h"
#include "SchM_Adc.h"
#include "SchM_BswM.h"
#include "SchM_Can.h"
#include "SchM_CanIf.h"
#include "SchM_CanSM.h"
#include "SchM_CanTp.h"
#include "SchM_CanTrcv_30_Tja1145.h"
#include "SchM_Com.h"
#include "SchM_ComM.h"
#include "SchM_Dcm.h"
#include "SchM_Dem.h"
#include "SchM_Det.h"
#include "SchM_Dio.h"
#include "SchM_EcuM.h"
#include "SchM_EthIf.h"
#include "SchM_EthSM.h"
#include "SchM_EthTrcv_30_Ar7000.h"
#include "SchM_Eth_30_Ar7000.h"
#include "SchM_Exi.h"
#include "SchM_Fee.h"
#include "SchM_Fls_17_Pmu.h"
#include "SchM_Gpt.h"
#include "SchM_Icu_17_GtmCcu6.h"
#include "SchM_Irq.h"
#include "SchM_Mcu.h"
#include "SchM_NvM.h"
#include "SchM_PduR.h"
#include "SchM_Port.h"
#include "SchM_Pwm_17_Gtm.h"
#include "SchM_Scc.h"
#include "SchM_Spi.h"
#include "SchM_TcpIp.h"
#include "SchM_WdgM.h"
#include "SchM_Wdg_30_TLE4278G.h"
#include "SchM_Xcp.h"

#include "Rte_Hook.h"

#include "Com.h"
#if defined(IL_ASRCOM_VERSION)
# define RTE_USE_COM_TXSIGNAL_RDACCESS
#endif

#include "Rte_Cbk.h"

/* AUTOSAR 3.x compatibility */
#if !defined (RTE_LOCAL)
# define RTE_LOCAL static
#endif


/**********************************************************************************************************************
 * API for enable / disable interrupts global
 *********************************************************************************************************************/

#if defined(osDisableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableAllInterrupts() osDisableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_DisableAllInterrupts() DisableAllInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableAllInterrupts() osEnableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_EnableAllInterrupts() EnableAllInterrupts()   /* AUTOSAR OS */
#endif

/**********************************************************************************************************************
 * API for enable / disable interrupts up to the systemLevel
 *********************************************************************************************************************/

#if defined(osDisableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableOSInterrupts() osDisableLevelKM()   /* MICROSAR OS */
#else
# define Rte_DisableOSInterrupts() SuspendOSInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableOSInterrupts() osEnableLevelKM()   /* MICROSAR OS */
#else
# define Rte_EnableOSInterrupts() ResumeOSInterrupts()   /* AUTOSAR OS */
#endif
#define RTE_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

CONST(SchM_ConfigType, RTE_CONST) Rte_Config_Left = { /* PRQA S 3408, 1533 */ /* MD_Rte_3408, MD_Rte_1533 */
  0U
};


#define RTE_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

P2CONST(SchM_ConfigType, AUTOMATIC, RTE_CONST) Rte_VarCfgPtr;

#define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Rte Init State Variable
 *********************************************************************************************************************/

#define RTE_START_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

volatile VAR(uint8, RTE_VAR_ZERO_INIT) Rte_InitState = RTE_STATE_UNINIT; /* PRQA S 3408 */ /* MD_Rte_3408 */
volatile VAR(uint8, RTE_VAR_ZERO_INIT) Rte_StartTiming_InitState = RTE_STATE_UNINIT; /* PRQA S 0850, 3408, 1514 */ /* MD_MSR_MacroArgumentEmpty, MD_Rte_3408, MD_Rte_1514 */

#define RTE_STOP_SEC_VAR_ZERO_INIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Constants
 *********************************************************************************************************************/

#define RTE_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 1514, 1533 L1 */ /* MD_Rte_1514, MD_Rte_1533 */
CONST(SG_DC2, RTE_CONST) Rte_C_SG_DC2_0 = {
  0U, 0U, 0U, 2U, 2U, 0U
};
/* PRQA L:L1 */

#define RTE_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Calibration Parameters (SW-C local and calibration component calibration parameters)
 *********************************************************************************************************************/

#define RTE_START_SEC_CONST_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 3408, 1514 L1 */ /* MD_Rte_3408, MD_Rte_1514 */
CONST(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_CONST_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmRom_DEFAULT_RTE_CALPRM_GROUP = {
  {
    150U,
    100U,
    300U,
    100U,
    50U,
    3000U,
    30U,
    3U,
    60U,
    5U
  }
};
/* PRQA L:L1 */

#define RTE_STOP_SEC_CONST_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#define RTE_START_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 3408, 1514 L1 */ /* MD_Rte_3408, MD_Rte_1514 */
CONST(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmRom_DEFAULT_RTE_CDATA_GROUP = {
  {
    300U,
    10U,
    10U,
    50U,
    1U,
    60U,
    3600U,
    0U,
    60U,
    600U,
    0U,
    360U,
    360U,
    360U,
    360U,
    10U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    360U,
    360U,
    360U,
    360U,
    360U,
    360U,
    360U,
    360U,
    360U,
    360U,
    3600U,
    360U,
    360U,
    360U,
    360U,
    360U,
    100U,
    20U,
    100U,
    50U,
    10U,
    10U,
    0U,
    4U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    0U,
    8U,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    TRUE,
    TRUE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    TRUE
  },
  {
    5U,
    160U,
    80U,
    5U,
    5U,
    5U
  },
  {
    270U,
    270U,
    270U,
    20U,
    180U,
    80U,
    200U,
    50U,
    200U,
    200U,
    32U,
    165U,
    200U,
    85U,
    170U,
    2U,
    20U,
    3U
  },
  {
    5U
  },
  {
    65U,
    10U
  },
  {
    125U,
    40U,
    40U,
    65U,
    125U,
    125U,
    125U,
    125U,
    125U,
    125U,
    115U,
    115U,
    115U,
    115U,
    115U,
    115U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    135U,
    150U,
    125U,
    125U,
    125U,
    125U,
    140U,
    115U,
    115U,
    115U,
    0U,
    0U,
    0U,
    0U,
    0U,
    5U,
    5U,
    5U,
    5U,
    5U,
    125U,
    44U,
    40U,
    65U
  },
  {
    4U
  },
  {
    50U,
    50U,
    50U,
    50U,
    50U,
    50U,
    50U,
    50U,
    270U,
    32U,
    270U,
    32U,
    270U,
    32U,
    650U,
    260U,
    720U,
    700U,
    260U,
    160U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    10U,
    TRUE,
    TRUE,
    TRUE,
    TRUE
  },
  {
    99U,
    99U,
    99U,
    99U,
    99U,
    99U,
    3234U,
    3234U,
    3234U,
    3234U,
    3234U,
    3234U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    30U,
    1U,
    1U,
    1U,
    1U,
    1U,
    1U,
    60U,
    60U,
    60U,
    60U,
    60U,
    60U,
    TRUE,
    TRUE,
    TRUE,
    TRUE,
    TRUE,
    TRUE,
    {
      0U, 5U, 10U, 15U, 20U, 25U, 30U, 35U, 40U, 45U, 50U, 55U, 60U, 65U, 70U, 75U, 80U, 85U, 90U, 95U, 100U, 105U, 110U, 
      115U, 120U, 125U, 130U, 135U, 140U, 145U, 150U, 155U, 160U, 165U, 170U, 175U, 180U, 185U, 190U
    },
    {
      0U, 5U, 10U, 15U, 20U, 25U, 30U, 35U, 40U, 45U, 50U, 55U, 60U, 65U, 70U, 75U, 80U, 85U, 90U, 95U, 100U, 105U, 110U, 
      115U, 120U, 125U, 130U, 135U, 140U, 145U, 150U, 155U, 160U, 165U, 170U, 175U, 180U, 185U, 190U
    },
    {
      0U, 5U, 10U, 15U, 20U, 25U, 30U, 35U, 40U, 45U, 50U, 55U, 60U, 65U, 70U, 75U, 80U, 85U, 90U, 95U, 100U, 105U, 110U, 
      115U, 120U, 125U, 130U, 135U, 140U, 145U, 150U, 155U, 160U, 165U, 170U, 175U, 180U, 185U, 190U
    },
    {
      0U, 5U, 10U, 15U, 20U, 25U, 30U, 35U, 40U, 45U, 50U, 55U, 60U, 65U, 70U, 75U, 80U, 85U, 90U, 95U, 100U, 105U, 110U, 
      115U, 120U, 125U, 130U, 135U, 140U, 145U, 150U, 155U, 160U, 165U, 170U, 175U, 180U, 185U, 190U
    },
    {
      0U, 5U, 10U, 15U, 20U, 25U, 30U, 35U, 40U, 45U, 50U, 55U, 60U, 65U, 70U, 75U, 80U, 85U, 90U, 95U, 100U, 105U, 110U, 
      115U, 120U, 125U, 130U, 135U, 140U, 145U, 150U, 155U, 160U, 165U, 170U, 175U, 180U, 185U, 190U
    },
    {
      0U, 5U, 10U, 15U, 20U, 25U, 30U, 35U, 40U, 45U, 50U, 55U, 60U, 65U, 70U, 75U, 80U, 85U, 90U, 95U, 100U, 105U, 110U, 
      115U, 120U, 125U, 130U, 135U, 140U, 145U, 150U, 155U, 160U, 165U, 170U, 175U, 180U, 185U, 190U
    },
    {
      3222U, 3197U, 3164U, 3124U, 3075U, 3015U, 2943U, 2860U, 2763U, 2654U, 2534U, 2402U, 2262U, 2115U, 1965U, 1813U, 
      1663U, 1516U, 1376U, 1243U, 1119U, 1004U, 899U, 803U, 717U, 639U, 570U, 508U, 453U, 404U, 361U, 323U, 289U, 259U, 
      233U, 209U, 189U, 170U, 154U
    },
    {
      3222U, 3197U, 3164U, 3124U, 3075U, 3015U, 2943U, 2860U, 2763U, 2654U, 2534U, 2402U, 2262U, 2115U, 1965U, 1813U, 
      1663U, 1516U, 1376U, 1243U, 1119U, 1004U, 899U, 803U, 717U, 639U, 570U, 508U, 453U, 404U, 361U, 323U, 289U, 259U, 
      233U, 209U, 189U, 170U, 154U
    },
    {
      3222U, 3197U, 3164U, 3124U, 3075U, 3015U, 2943U, 2860U, 2763U, 2654U, 2534U, 2402U, 2262U, 2115U, 1965U, 1813U, 
      1663U, 1516U, 1376U, 1243U, 1119U, 1004U, 899U, 803U, 717U, 639U, 570U, 508U, 453U, 404U, 361U, 323U, 289U, 259U, 
      233U, 209U, 189U, 170U, 154U
    },
    {
      3222U, 3197U, 3164U, 3124U, 3075U, 3015U, 2943U, 2860U, 2763U, 2654U, 2534U, 2402U, 2262U, 2115U, 1965U, 1813U, 
      1663U, 1516U, 1376U, 1243U, 1119U, 1004U, 899U, 803U, 717U, 639U, 570U, 508U, 453U, 404U, 361U, 323U, 289U, 259U, 
      233U, 209U, 189U, 170U, 154U
    },
    {
      3222U, 3197U, 3164U, 3124U, 3075U, 3015U, 2943U, 2860U, 2763U, 2654U, 2534U, 2402U, 2262U, 2115U, 1965U, 1813U, 
      1663U, 1516U, 1376U, 1243U, 1119U, 1004U, 899U, 803U, 717U, 639U, 570U, 508U, 453U, 404U, 361U, 323U, 289U, 259U, 
      233U, 209U, 189U, 170U, 154U
    },
    {
      3222U, 3197U, 3164U, 3124U, 3075U, 3015U, 2943U, 2860U, 2763U, 2654U, 2534U, 2402U, 2262U, 2115U, 1965U, 1813U, 
      1663U, 1516U, 1376U, 1243U, 1119U, 1004U, 899U, 803U, 717U, 639U, 570U, 508U, 453U, 404U, 361U, 323U, 289U, 259U, 
      233U, 209U, 189U, 170U, 154U
    }
  },
  {
    300U,
    0U,
    300U,
    300U,
    300U,
    30U,
    5U,
    10U,
    2U,
    5U,
    5U
  },
  {
    TRUE
  },
  {
    100U,
    273U,
    10U,
    189U,
    10U,
    10U,
    5U,
    5U,
    5U,
    5U,
    5U,
    5U,
    5U,
    5U,
    2U,
    5U,
    5U,
    5U,
    5U,
    2U,
    TRUE
  },
  {
    0U,
    0U,
    0U,
    33U,
    100U,
    0U,
    100U,
    100U,
    66U,
    0U,
    100U,
    100U,
    0U,
    0U,
    66U,
    0U,
    0U,
    5U,
    0U,
    0U,
    5U,
    0U,
    0U,
    0U,
    10U,
    5U,
    10U,
    10U,
    5U,
    10U,
    0U,
    10U
  },
  {
    350U,
    5U,
    3U,
    180U,
    30U,
    160U,
    1U,
    60U
  },
  {
    15U,
    104U,
    96U,
    118U,
    104U,
    80U,
    65U,
    5U,
    5U,
    5U,
    5U,
    12U,
    117U,
    TRUE
  },
  {
    819U,
    5U
  },
  {
    12U,
    15U
  },
  {
    30U,
    50U,
    20U,
    20U,
    100U,
    150U
  },
  {
    1U
  },
  {
    0xFFFFFFFEU,
    0U,
    0U,
    0xFFFFFFFEU,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    20U,
    40U,
    20U,
    40U,
    20U,
    40U,
    20U,
    40U,
    20U,
    40U,
    10U,
    40U,
    20U,
    40U,
    10U,
    20U,
    10U,
    20U,
    10U,
    20U,
    10U,
    20U,
    10U,
    20U,
    10U,
    20U,
    10U,
    20U,
    10U,
    20U,
    5U,
    10U,
    5U,
    10U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    3U,
    3U,
    3U,
    3U,
    3U,
    6U,
    6U,
    6U,
    6U,
    6U,
    3U,
    3U,
    6U,
    6U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    3U,
    3U,
    6U,
    6U,
    1U,
    1U,
    3U,
    6U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    3U,
    0U,
    0U,
    3U,
    3U,
    6U,
    6U,
    0U,
    1U,
    0U,
    0U,
    1U,
    0U,
    3U,
    3U,
    3U,
    6U,
    6U,
    6U,
    0U,
    0x00U,
    0U,
    0U,
    0U,
    0x00U,
    0x28U,
    0U,
    0U,
    0U,
    2U,
    2U,
    0U,
    0U,
    0U,
    2U,
    0U,
    1U,
    1U,
    0U,
    0U,
    0U,
    0x32U,
    0x32U,
    0U,
    0U,
    1U,
    0U,
    0U,
    0U,
    0U,
    0x64U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    1U,
    1U,
    3U,
    6U,
    0U,
    0U,
    2U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    1U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0xFEU,
    3U,
    3U,
    3U,
    6U,
    6U,
    6U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    3U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    3U,
    0U,
    0U,
    3U,
    3U,
    3U,
    3U,
    3U,
    3U,
    3U,
    6U,
    6U,
    6U,
    6U,
    6U,
    6U,
    6U,
    0U,
    0U,
    0U,
    0U,
    0U,
    0U,
    3U,
    6U,
    0U,
    0U,
    3U,
    3U,
    6U,
    6U,
    10U,
    20U,
    20U,
    20U,
    10U,
    TRUE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    FALSE,
    TRUE,
    FALSE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    TRUE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    FALSE
  },
  {
    250U,
    250U,
    250U,
    250U,
    250U,
    1000U,
    250U,
    250U,
    1000U,
    110U,
    1100U,
    250U,
    800U,
    100U,
    50U,
    100U,
    50U,
    10U,
    5U,
    33U,
    25U,
    140U,
    50U,
    20U,
    50U,
    50U,
    50U
  },
  {
    300U,
    215U,
    50U,
    220U,
    5U,
    5U,
    5U,
    5U,
    60U,
    30U,
    30U,
    150U,
    40U,
    15U,
    15U,
    FALSE,
    FALSE,
    FALSE,
    FALSE
  },
  {
    55U,
    3U,
    3U,
    TRUE
  },
  {
    215U,
    19U,
    335U,
    251U,
    100U,
    5U,
    5U
  },
  {
    3U,
    3U,
    TRUE
  },
  {
    85U,
    160U,
    100U,
    190U
  },
  {
    500U,
    600U,
    5U,
    12U,
    5U,
    150U,
    60U,
    5U,
    12U
  },
  {
    232U,
    250U,
    5U
  },
  {
    70U,
    70U,
    20U,
    10U,
    5U,
    5U
  }
};
/* PRQA L:L1 */
/* PRQA S 3408, 1514, 1533 L1 */ /* MD_Rte_3408, MD_Rte_1514, MD_Rte_1533 */
CONST(uint8, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtApWUM_NvWUMBlockNeed_DefaultValue = 1U;
/* PRQA L:L1 */
/* PRQA S 3408, 1514, 1533 L1 */ /* MD_Rte_3408, MD_Rte_1514, MD_Rte_1533 */
CONST(IdtPlantModeTestUTPlugin, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtApPLT_PLTNvMNeed_DefaultValue = {
  0U, 0U
};
/* PRQA L:L1 */
/* PRQA S 3408, 1514, 1533 L1 */ /* MD_Rte_3408, MD_Rte_1514, MD_Rte_1533 */
CONST(IdtArrayInitAIMVoltRef, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtHwAbsAIM_AIMVoltRefNvBlockNeed_DefaultValue = {
  255U, 255U, 255U
};
/* PRQA L:L1 */

#define RTE_STOP_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP; /* PRQA S 3408, 1514 */ /* MD_Rte_3408, MD_Rte_1514 */

#define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP; /* PRQA S 3408, 1514 */ /* MD_Rte_3408, MD_Rte_1514 */

#define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Defines for Rte_ComSendSignalProxy
 *********************************************************************************************************************/
#define RTE_COM_SENDSIGNALPROXY_NOCHANGE       (0U)
#define RTE_COM_SENDSIGNALPROXY_SEND           (1U)
#define RTE_COM_SENDSIGNALPROXY_INVALIDATE     (2U)


#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) Rte_MemClr(P2VAR(void, AUTOMATIC, RTE_VAR_NOINIT) ptr, uint32_least num);
FUNC(void, RTE_CODE) Rte_MemCpy(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num); /* PRQA S 1505, 3408 */ /* MD_MSR_Rule8.7, MD_Rte_3408 */
FUNC(void, RTE_CODE) Rte_MemCpy32(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num); /* PRQA S 1505, 3408 */ /* MD_MSR_Rule8.7, MD_Rte_3408 */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Helper functions for mode management
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_BswM_ESH_Mode(BswM_ESH_Mode mode) /* PRQA S 3408 */ /* MD_Rte_3408 */
{
  uint8 ret;

  if (mode == 0U)
  {
    ret = 3U;
  }
  else if (mode == 1U)
  {
    ret = 1U;
  }
  else if (mode == 2U)
  {
    ret = 0U;
  }
  else if (mode == 3U)
  {
    ret = 4U;
  }
  else if (mode == 4U)
  {
    ret = 2U;
  }
  else
  {
    ret = 5U;
  }

  return ret;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_Dcm_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType mode) /* PRQA S 3408 */ /* MD_Rte_3408 */
{
  uint8 ret;

  if (mode == 1U)
  {
    ret = 0U;
  }
  else if (mode == 2U)
  {
    ret = 2U;
  }
  else if (mode == 3U)
  {
    ret = 1U;
  }
  else if (mode == 80U)
  {
    ret = 3U;
  }
  else
  {
    ret = 4U;
  }

  return ret;
}

FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_Dcm_DcmEcuReset(Dcm_EcuResetType mode) /* PRQA S 3408 */ /* MD_Rte_3408 */
{
  uint8 ret;

  if (mode == 0U)
  {
    ret = 5U;
  }
  else if (mode == 1U)
  {
    ret = 1U;
  }
  else if (mode == 2U)
  {
    ret = 4U;
  }
  else if (mode == 3U)
  {
    ret = 6U;
  }
  else if (mode == 4U)
  {
    ret = 2U;
  }
  else if (mode == 5U)
  {
    ret = 3U;
  }
  else if (mode == 6U)
  {
    ret = 0U;
  }
  else
  {
    ret = 7U;
  }

  return ret;
} /* PRQA S 6080 */ /* MD_MSR_STMIF */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Timer handling
 *********************************************************************************************************************/

#if defined OS_US2TICKS_SystemTimer
# define RTE_USEC_SystemTimer OS_US2TICKS_SystemTimer
#else
# define RTE_USEC_SystemTimer(val) ((TickType)RTE_CONST_USEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_MS2TICKS_SystemTimer
# define RTE_MSEC_SystemTimer OS_MS2TICKS_SystemTimer
#else
# define RTE_MSEC_SystemTimer(val) ((TickType)RTE_CONST_MSEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_SEC2TICKS_SystemTimer
# define RTE_SEC_SystemTimer OS_SEC2TICKS_SystemTimer
#else
# define RTE_SEC_SystemTimer(val)  ((TickType)RTE_CONST_SEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#define RTE_CONST_MSEC_SystemTimer_0 (0UL)
#define RTE_CONST_MSEC_SystemTimer_1 (100000UL)
#define RTE_CONST_MSEC_SystemTimer_10 (1000000UL)
#define RTE_CONST_MSEC_SystemTimer_100 (10000000UL)
#define RTE_CONST_MSEC_SystemTimer_2 (200000UL)
#define RTE_CONST_MSEC_SystemTimer_20 (2000000UL)
#define RTE_CONST_MSEC_SystemTimer_3 (300000UL)
#define RTE_CONST_MSEC_SystemTimer_4 (400000UL)
#define RTE_CONST_MSEC_SystemTimer_5 (500000UL)
#define RTE_CONST_MSEC_SystemTimer_7 (700000UL)
#define RTE_CONST_MSEC_SystemTimer_8 (800000UL)


/**********************************************************************************************************************
 * Internal definitions
 *********************************************************************************************************************/

#define RTE_TASK_TIMEOUT_EVENT_MASK   ((EventMaskType)0x01)
#define RTE_TASK_WAITPOINT_EVENT_MASK ((EventMaskType)0x02)

/**********************************************************************************************************************
 * RTE life cycle API
 *********************************************************************************************************************/

#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) Rte_MemCpy(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num) /* PRQA S 3408, 1505 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
{
  P2CONST(uint8, AUTOMATIC, RTE_APPL_DATA) src = (P2CONST(uint8, AUTOMATIC, RTE_APPL_DATA)) source; /* PRQA S 0316 */ /* MD_Rte_0316 */
  P2VAR(uint8, AUTOMATIC, RTE_APPL_VAR) dst = (P2VAR(uint8, AUTOMATIC, RTE_APPL_VAR)) destination; /* PRQA S 0316 */ /* MD_Rte_0316 */
  uint32_least i;
  for (i = 0; i < num; i++)
  {
    dst[i] = src[i];
  }
}

#define RTE_MEMCPY32ALIGN (sizeof(uint32) - 1U)

FUNC(void, RTE_CODE) Rte_MemCpy32(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num)
{
  P2CONST(uint32, AUTOMATIC, RTE_APPL_DATA) asrc = (P2CONST(uint32, AUTOMATIC, RTE_APPL_DATA)) source; /* PRQA S 0316 */ /* MD_Rte_0316 */
  P2VAR(uint32, AUTOMATIC, RTE_APPL_VAR) adst = (P2VAR(uint32, AUTOMATIC, RTE_APPL_VAR)) destination; /* PRQA S 0316 */ /* MD_Rte_0316 */
  P2CONST(uint8, AUTOMATIC, RTE_APPL_DATA) src = (P2CONST(uint8, AUTOMATIC, RTE_APPL_DATA)) source; /* PRQA S 0316 */ /* MD_Rte_0316 */
  P2VAR(uint8, AUTOMATIC, RTE_APPL_VAR) dst = (P2VAR(uint8, AUTOMATIC, RTE_APPL_VAR)) destination; /* PRQA S 0316 */ /* MD_Rte_0316 */
  uint32_least i = 0;

  if (num >= 16U)
  {
    if (((((uint32)src) & RTE_MEMCPY32ALIGN) == 0U) && ((((uint32)dst) & RTE_MEMCPY32ALIGN) == 0U)) /* PRQA S 0306 */ /* MD_Rte_0306 */
    {
      uint32_least asize = num / sizeof(uint32);
      uint32_least rem = num & RTE_MEMCPY32ALIGN;
      for (i = 0; i < (asize - 3U); i += 4U)
      {
        adst[i] = asrc[i];
        adst[i+1U] = asrc[i+1U];
        adst[i+2U] = asrc[i+2U];
        adst[i+3U] = asrc[i+3U];
      }

      while (i < asize)
      {
        adst[i] = asrc[i];
        ++i;
      }
      i = num - rem;
    }
    else
    {
      for (i = 0; (i + 15U) < num; i += 16U)
      {
        dst[i] = src[i];
        dst[i+1U] = src[i+1U];
        dst[i+2U] = src[i+2U];
        dst[i+3U] = src[i+3U];
        dst[i+4U] = src[i+4U];
        dst[i+5U] = src[i+5U];
        dst[i+6U] = src[i+6U];
        dst[i+7U] = src[i+7U];
        dst[i+8U] = src[i+8U];
        dst[i+9U] = src[i+9U];
        dst[i+10U] = src[i+10U];
        dst[i+11U] = src[i+11U];
        dst[i+12U] = src[i+12U];
        dst[i+13U] = src[i+13U];
        dst[i+14U] = src[i+14U];
        dst[i+15U] = src[i+15U];
      }
    }

  }
  while (i < num)
  {
    dst[i] = src[i];
    ++i;
  }
}

FUNC(void, RTE_CODE) Rte_MemClr(P2VAR(void, AUTOMATIC, RTE_VAR_NOINIT) ptr, uint32_least num)
{
  P2VAR(uint8, AUTOMATIC, RTE_VAR_NOINIT) dst = (P2VAR(uint8, AUTOMATIC, RTE_VAR_NOINIT))ptr; /* PRQA S 0316 */ /* MD_Rte_0316 */
  uint32_least i;
  for (i = 0; i < num; i++)
  {
    dst[i] = 0;
  }
}

FUNC(void, RTE_CODE) SchM_Init(P2CONST(SchM_ConfigType, AUTOMATIC, RTE_CONST) ConfigPtr)
{
  Rte_VarCfgPtr = ConfigPtr;
  /* activate the tasks */
  (void)ActivateTask(Default_BSW_Async_QM_Task); /* PRQA S 3417 */ /* MD_Rte_Os */

  /* activate the alarms used for TimingEvents */
  (void)SetRelAlarm(Rte_Al_TE2_Default_BSW_Async_Task_7_10ms, RTE_MSEC_SystemTimer(7) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms, RTE_MSEC_SystemTimer(1) + (TickType)1, RTE_MSEC_SystemTimer(5)); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms, RTE_MSEC_SystemTimer(7) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms, RTE_MSEC_SystemTimer(8) + (TickType)1, RTE_MSEC_SystemTimer(20)); /* PRQA S 3417 */ /* MD_Rte_Os */

  Rte_InitState = RTE_STATE_SCHM_INIT;
}

FUNC(Std_ReturnType, RTE_CODE) Rte_Start(void)
{
  /* initialize RAM copy for calibration parameters */
  Rte_MemCpy32(&Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP, &Rte_CalprmRom_DEFAULT_RTE_CALPRM_GROUP, sizeof(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type)); /* PRQA S 0314, 0315, 0316 */ /* MD_Rte_0314, MD_Rte_0315, MD_Rte_0316 */
  Rte_MemCpy32(&Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP, &Rte_CalprmRom_DEFAULT_RTE_CDATA_GROUP, sizeof(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type)); /* PRQA S 0314, 0315, 0316 */ /* MD_Rte_0314, MD_Rte_0315, MD_Rte_0316 */

  /* activate the tasks */
  (void)ActivateTask(ASILB_MAIN_TASK); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)ActivateTask(ASILB_init_task); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)ActivateTask(QM_MAIN_TASK); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)ActivateTask(QM_init_task); /* PRQA S 3417 */ /* MD_Rte_Os */

  /* activate the alarms used for TimingEvents */
  (void)SetRelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_1_100ms, RTE_MSEC_SystemTimer(1) + (TickType)1, RTE_MSEC_SystemTimer(100)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_1_10ms, RTE_MSEC_SystemTimer(1) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_2_10ms, RTE_MSEC_SystemTimer(2) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_3_10ms, RTE_MSEC_SystemTimer(3) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_4_10ms, RTE_MSEC_SystemTimer(4) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX, RTE_MSEC_SystemTimer(0) + (TickType)1, RTE_MSEC_SystemTimer(5)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX, RTE_MSEC_SystemTimer(4) + (TickType)1, RTE_MSEC_SystemTimer(5)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_CpApAEM_RCtApAEM_task10msA, RTE_MSEC_SystemTimer(0) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_QM_MAIN_TASK_2_100ms, RTE_MSEC_SystemTimer(2) + (TickType)1, RTE_MSEC_SystemTimer(100)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_QM_MAIN_TASK_2_10ms, RTE_MSEC_SystemTimer(2) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_QM_MAIN_TASK_3_100ms, RTE_MSEC_SystemTimer(3) + (TickType)1, RTE_MSEC_SystemTimer(100)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */
  (void)SetRelAlarm(Rte_Al_TE_QM_MAIN_TASK_3_10ms, RTE_MSEC_SystemTimer(3) + (TickType)1, RTE_MSEC_SystemTimer(10)); /* PRQA S 3417, 1840 */ /* MD_Rte_Os, MD_Rte_Os */

  Rte_StartTiming_InitState = RTE_STATE_INIT;
  Rte_InitState = RTE_STATE_INIT;

  return RTE_E_OK;
} /* PRQA S 6050 */ /* MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Stop(void)
{
  Rte_StartTiming_InitState = RTE_STATE_UNINIT;
  /* deactivate alarms */
  (void)CancelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_1_100ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_1_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_2_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_3_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_ASILB_MAIN_TASK_4_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_CpApAEM_RCtApAEM_task10msA); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_QM_MAIN_TASK_2_100ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_QM_MAIN_TASK_2_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_QM_MAIN_TASK_3_100ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE_QM_MAIN_TASK_3_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */

  Rte_InitState = RTE_STATE_SCHM_INIT;

  return RTE_E_OK;
}

FUNC(void, RTE_CODE) SchM_Deinit(void)
{
  /* deactivate alarms */
  (void)CancelAlarm(Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms); /* PRQA S 3417 */ /* MD_Rte_Os */
  (void)CancelAlarm(Rte_Al_TE2_Default_BSW_Async_Task_7_10ms); /* PRQA S 3417 */ /* MD_Rte_Os */

  Rte_InitState = RTE_STATE_UNINIT;
}

FUNC(void, RTE_CODE) Rte_InitMemory(void)
{
  Rte_InitState = RTE_STATE_UNINIT;
  Rte_StartTiming_InitState = RTE_STATE_UNINIT;

  Rte_InitMemory_OsApplication_ASILB();
  Rte_InitMemory_OsApplication_BSW_QM();
  Rte_InitMemory_OsApplication_QM();
  Rte_InitMemory_SystemApplication_OsCore0();
}


/**********************************************************************************************************************
 * Exclusive area access
 *********************************************************************************************************************/

FUNC(void, RTE_CODE) SchM_Enter_Adc_DisableHwTrig(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_DisableHwTrig(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_EnableHwTrig(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_EnableHwTrig(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_GetGrpStatus(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_GetGrpStatus(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_GetStreamLastPtr(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_GetStreamLastPtr(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_PopQueue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_PopQueue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_PushQueue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_PushQueue(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_ReadGroup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_ReadGroup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_ScheduleNext(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_ScheduleNext(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_ScheduleStart(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_ScheduleStart(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_ScheduleStop(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_ScheduleStop(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_StartGroup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_StartGroup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Adc_StopGroup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Adc_StopGroup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Pmu_Erase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Pmu_Erase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Pmu_Init(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Pmu_Init(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Pmu_Main(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Pmu_Main(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Pmu_ResumeErase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Pmu_ResumeErase(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Fls_17_Pmu_Write(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Fls_17_Pmu_Write(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_GtmCcu6_Ccu6IenUpdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_GtmCcu6_Ccu6IenUpdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_GtmCcu6_CcuInterruptHandle(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_GtmCcu6_CcuInterruptHandle(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_GtmCcu6_CcuVariableupdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_GtmCcu6_CcuVariableupdate(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_GtmCcu6_EnableNotification(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_GtmCcu6_EnableNotification(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_GtmCcu6_EnableWakeup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_GtmCcu6_EnableWakeup(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Icu_17_GtmCcu6_ResetEdgeCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Icu_17_GtmCcu6_ResetEdgeCount(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Pwm_17_Gtm_StartChannel(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Pwm_17_Gtm_StartChannel(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Pwm_17_Gtm_SyncDuty(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Pwm_17_Gtm_SyncDuty(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_AsyncTransmit(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_AsyncTransmit(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_Cancel(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_Cancel(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_DeInit(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_DeInit(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_GetSequenceResult(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_GetSequenceResult(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_Init(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_Init(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_SyncTransmit(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_SyncTransmit(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}


FUNC(void, RTE_CODE) SchM_Enter_Spi_WriteIB(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  SuspendAllInterrupts();
}

FUNC(void, RTE_CODE) SchM_Exit_Spi_WriteIB(void)
{
  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */
  ResumeAllInterrupts();
}



/**********************************************************************************************************************
 * RTE Schedulable entity for COM-Access from different partitions
 *********************************************************************************************************************/
FUNC(void, RTE_CODE) Rte_ComSendSignalProxyPeriodic(void)
{
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0306:  MISRA rule: Rule11.4
     Reason:     An optimized copy algorithm can be used for aligned data. To check if pointers are aligned, pointers need to be casted to an integer type.
     Risk:       No functional risk. Only the lower 8 bits of the address are checked, therefore all integer types are sufficient.
     Prevention: Not required.

   MD_Rte_0314:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to void is performed.
     Prevention: Not required.

   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_0316:  MISRA rule: Dir1.1
     Reason:     Pointer cast to uint8* because a direct byte access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_1514:  MISRA rule: Rule8.9
     Reason:     Because of external definition, misra does not see the call.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_1533:  MISRA rule: Rule8.9
     Reason:     Object is referenced by more than one function in different configurations.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3408:  MISRA rule: Rule8.4
     Reason:     For the purpose of monitoring during calibration or debugging it is necessary to use non-static declarations.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
