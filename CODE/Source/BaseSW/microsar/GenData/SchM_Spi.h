/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SchM_Spi.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of BSW Scheduler for BSW Module <Spi>
 *********************************************************************************************************************/
#ifndef SCHM_SPI_H
# define SCHM_SPI_H

# ifdef __cplusplus
extern "C" {
# endif  /* __cplusplus */

# include "SchM_Spi_Type.h"

# define SPI_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, SPI_CODE) Spi_MainFunction_Handling(void); /* PRQA S 3451, 3449 */ /* MD_Rte_3451, MD_Rte_3449 */

# define SPI_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) SchM_Enter_Spi_AsyncTransmit(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_AsyncTransmit(void);
FUNC(void, RTE_CODE) SchM_Enter_Spi_Cancel(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_Cancel(void);
FUNC(void, RTE_CODE) SchM_Enter_Spi_DeInit(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_DeInit(void);
FUNC(void, RTE_CODE) SchM_Enter_Spi_GetSequenceResult(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_GetSequenceResult(void);
FUNC(void, RTE_CODE) SchM_Enter_Spi_Init(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_Init(void);
FUNC(void, RTE_CODE) SchM_Enter_Spi_SyncTransmit(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_SyncTransmit(void);
FUNC(void, RTE_CODE) SchM_Enter_Spi_WriteIB(void);
FUNC(void, RTE_CODE) SchM_Exit_Spi_WriteIB(void);

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif  /* __cplusplus */

#endif /* SCHM_SPI_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3449:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_3451:  MISRA rule: Rule8.5
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
