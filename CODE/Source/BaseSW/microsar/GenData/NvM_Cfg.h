/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: NvM
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: NvM_Cfg.h
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

	
/* PRQA S 5087 MemMap */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * PROTECTION AGAINST MULTIPLE INCLUSION
 *********************************************************************************************************************/
/* public section - to be used by NvM itself and its users */
#if (!defined NVM_CFG_H_PUBLIC)
#define NVM_CFG_H_PUBLIC

/**********************************************************************************************************************
 * VERSION IDENTIFICATION
 *********************************************************************************************************************/
#define NVM_CFG_MAJOR_VERSION    (5u)
#define NVM_CFG_MINOR_VERSION    (13u)
#define NVM_CFG_PATCH_VERSION    (0u)

/**********************************************************************************************************************
 * NVM API TYPE INCLUDES
 *********************************************************************************************************************/
#include "Rte_NvM_Type.h"

#include "NvM_Types.h"

/**********************************************************************************************************************
 * API CFG TYPE DEFINITIONS
 *********************************************************************************************************************/
/* Type for an the additional published parameter Compiled Configuration ID
 * (see CompiledConfigurationId in NvM.h)
 */
/* Compiled Config Id Type */
typedef union
{ /* PRQA S 0750 */ /* MD_MSR_Union */
    uint16 Word_u16;
    uint8  Bytes_au8[2u];
} NvM_CompiledConfigIdType;

/**********************************************************************************************************************
 * CFG COMMON PARAMETER
 *********************************************************************************************************************/
/* --------------------  DEVELOPMENT / PRODUCTION MODE -------------------- */
/* switch between Debug- or Production-Mode */
#define NVM_DEV_ERROR_DETECT                  (STD_OFF)

/* Preprocessor switch that is used in NvM_ReadAll() */
#define NVM_DYNAMIC_CONFIGURATION             (STD_OFF)

#define NVM_API_CONFIG_CLASS_1                (1u)
#define NVM_API_CONFIG_CLASS_2                (3u)
#define NVM_API_CONFIG_CLASS_3                (7u)

#define NVM_API_CONFIG_CLASS                  (NVM_API_CONFIG_CLASS_3)

#define NVM_JOB_PRIORISATION                  STD_OFF

/* define compiled Block ID */
#define NVM_COMPILED_CONFIG_ID                (1u)

/* switch for enablinig fast mode during multi block requests */
#define NVM_DRV_MODE_SWITCH                   (STD_ON)

/* switch for enablinig polling mode and disabling notifications */
#define NVM_POLLING_MODE                      (STD_ON)

/* switch for enabling the internal buffer for Crc handling */
#define NVM_CRC_INT_BUFFER                    (STD_ON)

/* number of defined NV blocks */
#define NVM_TOTAL_NUM_OF_NVRAM_BLOCKS         (48u)

/* internal buffer size */
#define NVM_INTERNAL_BUFFER_LENGTH            2000uL

/* version info api switch */
#define NVM_VERSION_INFO_API                  (STD_OFF)

/* switch to enable the ram block status api */
#define NVM_SET_RAM_BLOCK_STATUS_API          (STD_ON)

/* switch that gives the user (EcuM) the possibility to time-out WriteAll cancellation */
#define NVM_KILL_WRITEALL_API                 (STD_ON)

/* enabled or disable the whole repair redundant blocks feature */
#define NVM_REPAIR_REDUNDANT_BLOCKS_API       (STD_ON)

/* NVM does not need this macro. It is intended for underlying modules,
 * relying on its existence
 */
#define NVM_DATASET_SELECTION_BITS            (1u)

/* block offset for DCM blocks */
#define NVM_DCM_BLOCK_OFFSET                  0x8000u

/* returns corresponding DCM BlockId of an original NVRAM Block */
#define NvM_GetDcmBlockId(MyApplBlockId)      ((MyApplBlockId) | NVM_DCM_BLOCK_OFFSET) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/* BlockId's:
 * Note: The numbers of the following list must meet the configured blocks in the NvM_BlockDescriptorTable_at
 *
 * Alignment of the handles of all blocks
 * Id 0 is reserved for multiblock calls
 * Id 1 is reserved for config ID
 */
#define NvMConf___MultiBlockRequest (0u) 
#define NvMConf_NvMBlockDescriptor_NvMConfigBlock (1u) 
#define NvMConf_NvMBlockDescriptor_DemStatusDataBlock (2u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock3 (3u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock2 (4u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock1 (5u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock0 (6u) 
#define NvMConf_NvMBlockDescriptor_DemAdminDataBlock (7u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock8 (8u) 
#define NvMConf_NvMBlockDescriptor_CpApPCOM_NvPCOMBlockNeed (9u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock9 (10u) 
#define NvMConf_NvMBlockDescriptor_CpApWUM_NvWUMBlockNeed (11u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock10 (12u) 
#define NvMConf_NvMBlockDescriptor_EthMACAdressDataBlock (13u) 
#define NvMConf_NvMBlockDescriptor_SCCSessionIDNvMBlockDescriptor (14u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock11 (15u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock12 (16u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock13 (17u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock14 (18u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock15 (19u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock16 (20u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock17 (21u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock18 (22u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock19 (23u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock4 (24u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock20 (25u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock21 (26u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock22 (27u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock5 (28u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock23 (29u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock24 (30u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock25 (31u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock6 (32u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock26 (33u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock27 (34u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock28 (35u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock7 (36u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock29 (37u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock30 (38u) 
#define NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock31 (39u) 
#define NvMConf_NvMBlockDescriptor_CpApJDD_JDDNvBlockNeed (40u) 
#define NvMConf_NvMBlockDescriptor_CpApMSC_NvMSCBlockNeedPowerLatchFlag (41u) 
#define NvMConf_NvMBlockDescriptor_CpApPSH_NvPSHBlockNeed (42u) 
#define NvMConf_NvMBlockDescriptor_CpApPXL_PXLNvBlockNeed (43u) 
#define NvMConf_NvMBlockDescriptor_CpApCPT_CPTNvBlockNeed (44u) 
#define NvMConf_NvMBlockDescriptor_CpApDGN_DGN_EOL_NVM_Needs (45u) 
#define NvMConf_NvMBlockDescriptor_CpApPLT_PLTNvMNeed (46u) 
#define NvMConf_NvMBlockDescriptor_CpHwAbsAIM_AIMVoltRefNvBlockNeed (47u) 


/* CONST_DESCRIPTOR_TABLE contains all block relevant data, including the compiled config ID
 */
#define NVM_START_SEC_CONST_DESCRIPTOR_TABLE
#include "MemMap.h"

/* Additional published parameter because e.g. in case of validate all RAM
 * Blocks it is nice to know the number of Blocks. But take care: this number
 * of Blocks includes Block 0 and Block 1, which are the MultiBlock and the
 * Configuration Block - user Blocks start wiht index 2!
 */
extern CONST(uint16, NVM_PUBLIC_CONST) NvM_NoOfBlockIds_t;

/* Additional published parameter because in case of a clear EEPROM, it is
 * necessary, to write the Configuration Block containing this Compiled
 * Configuration ID to EEPROM
 */
/* Compiled Configuration ID as defined in NvM_Cfg.c */
extern CONST(NvM_CompiledConfigIdType, NVM_PUBLIC_CONST) NvM_CompiledConfigId_t; /* PRQA S 0759 */ /* MD_MSR_Union */

#define NVM_STOP_SEC_CONST_DESCRIPTOR_TABLE
#include "MemMap.h"

/* Component define block (available, if EcuC module is active, otherwise only NVM_DUMMY_STATEMENTs are defined*/
#ifndef NVM_USE_DUMMY_STATEMENT
#define NVM_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef NVM_DUMMY_STATEMENT
#define NVM_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef NVM_DUMMY_STATEMENT_CONST
#define NVM_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef NVM_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define NVM_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef NVM_ATOMIC_VARIABLE_ACCESS
#define NVM_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef NVM_PROCESSOR_TC234
#define NVM_PROCESSOR_TC234
#endif
#ifndef NVM_COMP_GNU
#define NVM_COMP_GNU
#endif
#ifndef NVM_GEN_GENERATOR_MSR
#define NVM_GEN_GENERATOR_MSR
#endif
#ifndef NVM_CPUTYPE_BITORDER_LSB2MSB
#define NVM_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef NVM_CONFIGURATION_VARIANT_PRECOMPILE
#define NVM_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef NVM_CONFIGURATION_VARIANT_LINKTIME
#define NVM_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef NVM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define NVM_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef NVM_CONFIGURATION_VARIANT
#define NVM_CONFIGURATION_VARIANT NVM_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef NVM_POSTBUILD_VARIANT_SUPPORT
#define NVM_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/* ---- end public configuration section ---------------------------------- */
#endif /* NVM_CFG_H_PUBLIC */

/* PRQA L:MemMap */

/**********************************************************************************************************************
 *  END OF FILE: NvM_Cfg.h
 *********************************************************************************************************************/

