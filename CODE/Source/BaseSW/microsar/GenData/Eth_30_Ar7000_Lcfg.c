/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Eth_Ar7000
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Eth_30_Ar7000_Lcfg.c
 *   Generation Time: 2020-11-23 11:39:43
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
   LOCAL MISRA JUSTIFICATION
**********************************************************************************************************************/
/* PRQA S 857 Eth_MD_MSR_1_1_857 */ /* MD_MSR_1.1_857 */

#define ETH_30_AR7000_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
#include "Eth_30_Ar7000_Lcfg.h"


/**********************************************************************************************************************
 *  PRIVATE MACROS
 *********************************************************************************************************************/
#ifndef STATIC
#define STATIC static
#endif /* STATIC */

/**********************************************************************************************************************
 *  RAM VARIABLES
 *********************************************************************************************************************/
#define ETH_30_AR7000_START_SEC_VAR_NOINIT_BUFFER
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */
/* PRQA S 0612 L1 */ /* MD_Eth_0612 */
VAR(Eth_DataType, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_RxBuffer_0_0[768];

VAR(Eth_DataType, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_TxBuffer_0[768];

/* PRQA L:L1 */ /* MD_Eth_0612 */
#define ETH_30_AR7000_STOP_SEC_VAR_NOINIT_BUFFER
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */


#define ETH_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

VAR(uint8, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_TxBufferBusyTable[2];
VAR(uint8, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_UlTxConfState[2];

#define ETH_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


#define ETH_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

VAR(Eth_PhysAddrType, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_PhysSrcAddrs[ETH_30_AR7000_MAX_CTRLS_TOTAL];

#define ETH_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define ETH_30_AR7000_START_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

VAR(uint8, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_RxBufferBusyTable[2];

#define ETH_30_AR7000_STOP_SEC_VAR_NOINIT_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define ETH_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

P2VAR(Eth_30_Ar7000_BaseSpiFlow, ETH_30_AR7000_VAR_NOINIT, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_RingBuffer[17];
VAR(Eth_30_Ar7000_RegAccessSpiFlow, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_RegFlows[10];
VAR(Eth_30_Ar7000_BufAccessSpiFlow, ETH_30_AR7000_VAR_NOINIT) Eth_30_Ar7000_BufFlows[6];

#define ETH_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 *  ROM VARIABLES
 *********************************************************************************************************************/
#define ETH_30_AR7000_START_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

CONST(uint8, ETH_30_AR7000_CONST) Eth_30_Ar7000_RxQueueCntTotal[1] = 
{
  1uL
};

CONST(uint8, ETH_30_AR7000_CONST) Eth_30_Ar7000_TxQueueCntTotal[1] = 
{
  1uL
};



#define ETH_30_AR7000_STOP_SEC_CONST_8BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define ETH_30_AR7000_START_SEC_CONST_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
CONST(uint16, ETH_30_AR7000_CONST) Eth_30_Ar7000_TxBufferLen[2] =
{
  1518u, 
  1518u
};
#define ETH_30_AR7000_STOP_SEC_CONST_16BIT
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define ETH_30_AR7000_START_SEC_CONST_32BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */
CONST(uint32, ETH_30_AR7000_CONST) Eth_30_Ar7000_TxBufferStart[2] =
{
  2uL, 
  1538uL
};

#define ETH_30_AR7000_STOP_SEC_CONST_32BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */
 
 
#define ETH_30_AR7000_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

CONST(Eth_30_Ar7000_RegBaseAddrType, ETH_30_AR7000_CONST)    Eth_30_Ar7000_RegBaseAddr[1] =
{
  0x00000000uL
};


CONST(Eth_30_Ar7000_ConfigType, ETH_30_AR7000_CONST) Eth_30_Ar7000_Config = 0U;
CONST(Eth_PhysAddrType, ETH_30_AR7000_CONST) Eth_30_Ar7000_PhysSrcAddrsRom[ETH_30_AR7000_MAX_CTRLS_TOTAL] =
{
  {0x00U, 0x01U, 0x02U, 0x03U, 0x04U, 0x05U}
};

CONST(Eth_30_Ar7000_AlignedFrameCacheType, ETH_30_AR7000_CONST) Eth_30_Ar7000_Ctrl2TxBufferMap[1] = 
{
  {
    Eth_30_Ar7000_TxBuffer_0
  }
};

CONST(Eth_30_Ar7000_RxRing2BufMapType, ETH_30_AR7000_CONST) Eth_30_Ar7000_RxRing2BufMap_0[1] = {
  {
    {
      Eth_30_Ar7000_RxBuffer_0_0 /*  Buffer Pointers  */ 
    }, 
    1536u /*  Segment Size  */ , 
    1522u /*  Maximum Frame Size  */ , 
    1536u /*  Last Segment Size  */ , 
    2u /*  Segment Number  */ , 
    0u /*  Extended Segment Number  */ 
  }
};



#define ETH_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */







/* PRQA L:Eth_MD_MSR_1_1_857 */

/* module specific MISRA deviations:
 MD_Eth_30_Ar7000_0612:
      Reason:     Ethernet buffers may be larger than 32767 bytes in a total.
      Risk:       There is no risk. If memory is too small the linker complains.
      Prevention: No prevention necessary.
 MD_Eth_30_Ar7000_0750:
      Reason:     Ethernet buffer is accessed by an union. This allows accessing U8, U16 and U32 fields without casts
                  (and misra warnings of type 0310)
      Risk:       Depending on the buffer, an unaligned memory exception may occur on certain platforms.
      Prevention: The configuration tool ensures that all buffers are 4 bytes aligned. Reviewers check that access
                  to U16 fields always 2 bytes aligned and to U32 fields always 4 bytes aligned.
*/

/**********************************************************************************************************************
 *  END OF FILE: Eth_30_Ar7000_Lcfg.c
 *********************************************************************************************************************/
 

