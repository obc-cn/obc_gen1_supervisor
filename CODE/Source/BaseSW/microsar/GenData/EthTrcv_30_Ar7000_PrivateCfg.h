/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EthTrcv_Ar7000
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EthTrcv_30_Ar7000_PrivateCfg.h
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/



#if !defined(ETHTRCV_30_AR7000_PRIVATE_CFG_H)
#define ETHTRCV_30_AR7000_PRIVATE_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/



/**********************************************************************************************************************
 *  ETHERNET CONTROLLER API ABSTRACTION
 *********************************************************************************************************************/
#define ETH_WRITESPI   Eth_30_Ar7000_WriteSpi
#define ETH_READSPI    Eth_30_Ar7000_ReadSpi


/**********************************************************************************************************************
 *  CALLBACK FUNCTIONS
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_APPL_CBK_FW_DOWNLOAD_START              Scc_Cbk_SLAC_FirmwareDownloadStart
#define ETHTRCV_30_AR7000_APPL_CBK_FW_START                       Scc_Cbk_SLAC_FirmwareDownloadComplete

#define ETHTRCV_30_AR7000_APPL_CBK_TRIG_DWLD_REQ                  Appl_TriggerDownloadReqCbk   


/**********************************************************************************************************************
 *  SLAC
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  SLAC - CALLBACK FUNCTIONS
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATE               Scc_Cbk_SLAC_AssociationState
#define ETHTRCV_30_AR7000_APPL_CBK_SLAC_ASSOC_STATUS              Scc_Cbk_SLAC_AssociationStatus
#define ETHTRCV_30_AR7000_APPL_CBK_SLAC_DLINK_READY               Scc_Cbk_SLAC_DLinkReady
#define ETHTRCV_30_AR7000_APPL_CBK_SLAC_GET_RND                   Scc_Cbk_SLAC_GetRandomizedDataBuffer
#define ETHTRCV_30_AR7000_APPL_CBK_SLAC_GET_VALIDATE_TOGGLES      Scc_Cbk_SLAC_GetValidateToggles
#define ETHTRCV_30_AR7000_APPL_CBK_SLAC_TOGGLE_REQ                Scc_Cbk_SLAC_ToggleRequest


#endif /* ETHTRCV_30_AR7000_PRIVATE_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_PrivateCfg.h
 *********************************************************************************************************************/
 
