/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Exi
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Exi_Cfg.h
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined(EXI_CFG_H)
#define EXI_CFG_H

/* PRQA S 0857 EOF */ /* MD_Exi_1.1_0857 */ /* [L] Number of macro definitions exceeds 1024 - program is non-conforming. */

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
#include "Std_Types.h"

/**********************************************************************************************************************
 *  LINKTIME / PRECOMPILE CRC
 *********************************************************************************************************************/
#define EXI_CRC_CHECK                       STD_OFF
#define EXI_PRECOMPILE_CRC                  0x00000000u

/**********************************************************************************************************************
 *  EXI ECUC GLOBAL CONFIGURATION CONTAINER NAME
 *********************************************************************************************************************/
#define ExiConfigSet                        Exi_Config

/**********************************************************************************************************************
 *  GENERAL DEFINES
 *********************************************************************************************************************/
#ifndef EXI_USE_DUMMY_STATEMENT
#define EXI_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef EXI_DUMMY_STATEMENT
#define EXI_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef EXI_DUMMY_STATEMENT_CONST
#define EXI_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef EXI_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define EXI_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef EXI_ATOMIC_VARIABLE_ACCESS
#define EXI_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef EXI_PROCESSOR_TC234
#define EXI_PROCESSOR_TC234
#endif
#ifndef EXI_COMP_GNU
#define EXI_COMP_GNU
#endif
#ifndef EXI_GEN_GENERATOR_MSR
#define EXI_GEN_GENERATOR_MSR
#endif
#ifndef EXI_CPUTYPE_BITORDER_LSB2MSB
#define EXI_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef EXI_CONFIGURATION_VARIANT_PRECOMPILE
#define EXI_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef EXI_CONFIGURATION_VARIANT_LINKTIME
#define EXI_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef EXI_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define EXI_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef EXI_CONFIGURATION_VARIANT
#define EXI_CONFIGURATION_VARIANT EXI_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef EXI_POSTBUILD_VARIANT_SUPPORT
#define EXI_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
 *  DEFINES
 *********************************************************************************************************************/
#define EXI_CONFIG_VARIANT                  EXI_CONFIGURATION_VARIANT

/**********************************************************************************************************************
 *  General configuration
 *********************************************************************************************************************/
#define EXI_VERSION_INFO_API                   STD_OFF
#define EXI_DEV_ERROR_DETECT                   STD_OFF
#define EXI_INTERNAL_DEV_ERROR_DETECT          STD_OFF
#define EXI_ENABLE_SIMPLE_TX_STREAMING_SUPPORT STD_ON
#define EXI_ENABLE_STREAMING_SUPPORT           STD_OFF
#define EXI_ENABLE_PBUF_SUPPORT                STD_ON
#define EXI_ENABLE_EV_MESSAGE_SET              STD_ON
#define EXI_ENABLE_EVSE_MESSAGE_SET            STD_OFF
#define EXI_ENABLE_VSTDLIB                     STD_ON

#define EXI_ENCODE_SCHEMA_FRAGMENT             STD_OFF
#define EXI_ENCODE_SCHEMA_ROOT                 STD_ON
#define EXI_ENCODE_STRING                      STD_ON
#define EXI_DECODE_STRING                      STD_ON
#define EXI_DECODE_ID                          STD_ON
#define EXI_ENCODE_ID                          STD_ON
#define EXI_DECODE_BASE64BINARY                STD_OFF
#define EXI_ENCODE_BASE64BINARY                STD_OFF


/**********************************************************************************************************************
 *  General Max Occurs
 *********************************************************************************************************************/
#define EXI_MAXOCCURS_UNBOUNDED                32u

/**********************************************************************************************************************
 *  General Max Buffer Size
 *********************************************************************************************************************/
#define EXI_MAX_NUM_BYTES_BIGINT               20u
#define EXI_MAX_BUFFER_SIZE_STRING             64u
#define EXI_MAX_BUFFER_SIZE_UNBOUNDED          64u

/**********************************************************************************************************************
 *  Supported App Protocol specific configuration
 *********************************************************************************************************************/
#define EXI_ENABLE_SAP_MESSAGE_SET                 STD_ON

/**********************************************************************************************************************
 *  supportedAppProtocol Schema Set Switches
 *********************************************************************************************************************/
#define EXI_ENCODE_SAP_SCHEMA_ROOT                 STD_ON
#define EXI_ENCODE_SAP_SCHEMA_FRAGMENT             STD_OFF
#define EXI_DECODE_SCHEMA_SET_SAP                  STD_ON
#define EXI_ENCODE_SAP_SUPPORTED_APP_PROTOCOL_REQ  STD_ON
#define EXI_ENCODE_SAP_SUPPORTED_APP_PROTOCOL_RES  STD_OFF
#define EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_RES  STD_ON
#define EXI_DECODE_SAP_SUPPORTED_APP_PROTOCOL_REQ  STD_OFF

#define EXI_ENCODE_SAP_APP_PROTOCOL                STD_ON
#define EXI_ENCODE_SAP_PROTOCOL_NAMESPACE          STD_ON
#define EXI_ENCODE_SAP_RESPONSE_CODE               STD_OFF
#define EXI_DECODE_SAP_RESPONSE_CODE               STD_ON
#define EXI_DECODE_SAP_APP_PROTOCOL                STD_OFF
#define EXI_DECODE_SAP_PROTOCOL_NAMESPACE          STD_OFF

/**********************************************************************************************************************
 *  DIN 70121 specific configuration
 *********************************************************************************************************************/
#define EXI_ENABLE_DIN_MESSAGE_SET            STD_ON
 
/**********************************************************************************************************************
 *  DIN Max Occurs
 *********************************************************************************************************************/
#define EXI_MAXOCCURS_DIN_CERTIFICATE          8u
#define EXI_MAXOCCURS_DIN_CONSUMPTIONCOST      3u
#define EXI_MAXOCCURS_DIN_COST                 3u
#define EXI_MAXOCCURS_DIN_PARAMETER            2u
#define EXI_MAXOCCURS_DIN_PARAMETERSET         4u
#define EXI_MAXOCCURS_DIN_PAYMENTOPTION        2u
#define EXI_MAXOCCURS_DIN_PMAXSCHEDULEENTRY    12u
#define EXI_MAXOCCURS_DIN_PROFILEENTRY         24u
#define EXI_MAXOCCURS_DIN_ROOTCERTIFICATEID    20u
#define EXI_MAXOCCURS_DIN_SALESTARIFFENTRY     12u
#define EXI_MAXOCCURS_DIN_SASCHEDULETUPLE      3u
#define EXI_MAXOCCURS_DIN_SELECTEDSERVICE      3u
#define EXI_MAXOCCURS_DIN_SERVICE              3u

/**********************************************************************************************************************
 *  DIN Max Buffer Size
 *********************************************************************************************************************/
#define EXI_MAX_BUFFER_SIZE_DIN_ATTRIBUTEID    10u
#define EXI_MAX_BUFFER_SIZE_DIN_ATTRIBUTENAME  8u
#define EXI_MAX_BUFFER_SIZE_DIN_GENCHALLENGE   32u

/**********************************************************************************************************************
 *  DIN 70121 Schema Set Switches
 *********************************************************************************************************************/
#define EXI_ENCODE_DIN_SCHEMA_ROOT                    STD_ON
#define EXI_DECODE_SCHEMA_SET_DIN                     STD_ON
#define EXI_ENABLE_DIN_AC_BASIC_MESSAGE_SET           STD_OFF
#define EXI_ENABLE_DIN_DC_BASIC_MESSAGE_SET           STD_ON
#define EXI_ENABLE_DIN_AC_EXTENDED_MESSAGE_SET        STD_OFF
#define EXI_ENCODE_DIN_SCHEMA_FRAGMENT                STD_OFF

#define EXI_ENCODE_DIN_CABLE_CHECK_REQ                STD_ON
#define EXI_ENCODE_DIN_CABLE_CHECK_RES                STD_OFF
#define EXI_ENCODE_DIN_CERTIFICATE_INSTALLATION_REQ   STD_OFF
#define EXI_ENCODE_DIN_CERTIFICATE_INSTALLATION_RES   STD_OFF
#define EXI_ENCODE_DIN_CERTIFICATE_UPDATE_REQ         STD_OFF
#define EXI_ENCODE_DIN_CERTIFICATE_UPDATE_RES         STD_OFF
#define EXI_ENCODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ STD_ON
#define EXI_ENCODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES STD_OFF
#define EXI_ENCODE_DIN_CHARGING_STATUS_REQ            STD_OFF
#define EXI_ENCODE_DIN_CHARGING_STATUS_RES            STD_OFF
#define EXI_ENCODE_DIN_CONTRACT_AUTHENTICATION_REQ    STD_ON
#define EXI_ENCODE_DIN_CONTRACT_AUTHENTICATION_RES    STD_OFF
#define EXI_ENCODE_DIN_CURRENT_DEMAND_REQ             STD_ON
#define EXI_ENCODE_DIN_CURRENT_DEMAND_RES             STD_OFF
#define EXI_ENCODE_DIN_METERING_RECEIPT_REQ           STD_OFF
#define EXI_ENCODE_DIN_METERING_RECEIPT_RES           STD_OFF
#define EXI_ENCODE_DIN_PAYMENT_DETAILS_REQ            STD_OFF
#define EXI_ENCODE_DIN_PAYMENT_DETAILS_RES            STD_OFF
#define EXI_ENCODE_DIN_POWER_DELIVERY_REQ             STD_ON
#define EXI_ENCODE_DIN_POWER_DELIVERY_RES             STD_OFF
#define EXI_ENCODE_DIN_PRE_CHARGE_REQ                 STD_ON
#define EXI_ENCODE_DIN_PRE_CHARGE_RES                 STD_OFF
#define EXI_ENCODE_DIN_SERVICE_DETAIL_REQ             STD_OFF
#define EXI_ENCODE_DIN_SERVICE_DETAIL_RES             STD_OFF
#define EXI_ENCODE_DIN_SERVICE_DISCOVERY_REQ          STD_ON
#define EXI_ENCODE_DIN_SERVICE_DISCOVERY_RES          STD_OFF
#define EXI_ENCODE_DIN_SERVICE_PAYMENT_SELECTION_REQ  STD_ON
#define EXI_ENCODE_DIN_SERVICE_PAYMENT_SELECTION_RES  STD_OFF
#define EXI_ENCODE_DIN_SESSION_SETUP_REQ              STD_ON
#define EXI_ENCODE_DIN_SESSION_SETUP_RES              STD_OFF
#define EXI_ENCODE_DIN_SESSION_STOP_REQ               STD_ON
#define EXI_ENCODE_DIN_SESSION_STOP_RES               STD_OFF
#define EXI_ENCODE_DIN_WELDING_DETECTION_REQ          STD_ON
#define EXI_ENCODE_DIN_WELDING_DETECTION_RES          STD_OFF
#define EXI_ENCODE_DIN_V2G_MESSAGE                    STD_ON


#define EXI_DECODE_DIN_CABLE_CHECK_REQ                STD_OFF
#define EXI_DECODE_DIN_CABLE_CHECK_RES                STD_ON
#define EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_REQ   STD_OFF
#define EXI_DECODE_DIN_CERTIFICATE_INSTALLATION_RES   STD_OFF
#define EXI_DECODE_DIN_CERTIFICATE_UPDATE_REQ         STD_OFF
#define EXI_DECODE_DIN_CERTIFICATE_UPDATE_RES         STD_OFF
#define EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_REQ STD_OFF
#define EXI_DECODE_DIN_CHARGE_PARAMETER_DISCOVERY_RES STD_ON
#define EXI_DECODE_DIN_CHARGING_STATUS_REQ            STD_OFF
#define EXI_DECODE_DIN_CHARGING_STATUS_RES            STD_OFF
#define EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_REQ    STD_OFF
#define EXI_DECODE_DIN_CONTRACT_AUTHENTICATION_RES    STD_ON
#define EXI_DECODE_DIN_CURRENT_DEMAND_REQ             STD_OFF
#define EXI_DECODE_DIN_CURRENT_DEMAND_RES             STD_ON
#define EXI_DECODE_DIN_METERING_RECEIPT_REQ           STD_OFF
#define EXI_DECODE_DIN_METERING_RECEIPT_RES           STD_OFF
#define EXI_DECODE_DIN_PAYMENT_DETAILS_REQ            STD_OFF
#define EXI_DECODE_DIN_PAYMENT_DETAILS_RES            STD_OFF
#define EXI_DECODE_DIN_POWER_DELIVERY_REQ             STD_OFF
#define EXI_DECODE_DIN_POWER_DELIVERY_RES             STD_ON
#define EXI_DECODE_DIN_PRE_CHARGE_REQ                 STD_OFF
#define EXI_DECODE_DIN_PRE_CHARGE_RES                 STD_ON
#define EXI_DECODE_DIN_SERVICE_DETAIL_REQ             STD_OFF
#define EXI_DECODE_DIN_SERVICE_DETAIL_RES             STD_OFF
#define EXI_DECODE_DIN_SERVICE_DISCOVERY_REQ          STD_OFF
#define EXI_DECODE_DIN_SERVICE_DISCOVERY_RES          STD_ON
#define EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_REQ  STD_OFF
#define EXI_DECODE_DIN_SERVICE_PAYMENT_SELECTION_RES  STD_ON
#define EXI_DECODE_DIN_SESSION_SETUP_REQ              STD_OFF
#define EXI_DECODE_DIN_SESSION_SETUP_RES              STD_ON
#define EXI_DECODE_DIN_SESSION_STOP_REQ               STD_OFF
#define EXI_DECODE_DIN_SESSION_STOP_RES               STD_ON
#define EXI_DECODE_DIN_WELDING_DETECTION_REQ          STD_OFF
#define EXI_DECODE_DIN_WELDING_DETECTION_RES          STD_ON
#define EXI_DECODE_DIN_V2G_MESSAGE                    STD_ON


#define EXI_ENCODE_DIN_AC_EVCHARGE_PARAMETER          STD_OFF
#define EXI_ENCODE_DIN_AC_EVSECHARGE_PARAMETER        STD_OFF
#define EXI_ENCODE_DIN_AC_EVSESTATUS                  STD_OFF
#define EXI_ENCODE_DIN_ATTRIBUTE_ID                   STD_ON
#define EXI_ENCODE_DIN_ATTRIBUTE_NAME                 STD_OFF
#define EXI_ENCODE_DIN_ATTRIBUTE_VALUE                STD_OFF
#define EXI_ENCODE_DIN_BODY_BASE                      STD_OFF
#define EXI_ENCODE_DIN_BODY_ELEMENT                   STD_OFF
#define EXI_ENCODE_DIN_BODY                           STD_ON
#define EXI_ENCODE_DIN_CERTIFICATE_CHAIN              STD_OFF
#define EXI_ENCODE_DIN_CERTIFICATE                    STD_OFF
#define EXI_ENCODE_DIN_CHARGING_PROFILE               STD_ON
#define EXI_ENCODE_DIN_CONSUMPTION_COST               STD_OFF
#define EXI_ENCODE_DIN_CONTRACT_ID                    STD_OFF
#define EXI_ENCODE_DIN_COST_KIND                      STD_OFF
#define EXI_ENCODE_DIN_COST                           STD_OFF
#define EXI_ENCODE_DIN_DC_EVCHARGE_PARAMETER          STD_ON
#define EXI_ENCODE_DIN_DC_EVERROR_CODE                STD_ON
#define EXI_ENCODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER  STD_ON
#define EXI_ENCODE_DIN_DC_EVSTATUS                    STD_ON
#define EXI_ENCODE_DIN_DC_EVSECHARGE_PARAMETER        STD_OFF
#define EXI_ENCODE_DIN_DC_EVSESTATUS_CODE             STD_OFF
#define EXI_ENCODE_DIN_DC_EVSESTATUS                  STD_OFF
#define EXI_ENCODE_DIN_D_HPARAMS                      STD_OFF
#define EXI_ENCODE_DIN_ENTRY                          STD_OFF
#define EXI_ENCODE_DIN_EVCC_ID                        STD_ON
#define EXI_ENCODE_DIN_EVCHARGE_PARAMETER             STD_ON
#define EXI_ENCODE_DIN_EVPOWER_DELIVERY_PARAMETER     STD_ON
#define EXI_ENCODE_DIN_EVREQUESTED_ENERGY_TRANSFER    STD_ON
#define EXI_ENCODE_DIN_EVSECHARGE_PARAMETER           STD_OFF
#define EXI_ENCODE_DIN_EVSE_ID                        STD_OFF
#define EXI_ENCODE_DIN_EVSENOTIFICATION               STD_OFF
#define EXI_ENCODE_DIN_EVSEPROCESSING                 STD_OFF
#define EXI_ENCODE_DIN_EVSESTATUS                     STD_OFF
#define EXI_ENCODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER  STD_OFF
#define EXI_ENCODE_DIN_EVSTATUS                       STD_ON
#define EXI_ENCODE_DIN_FAULT_CODE                     STD_ON
#define EXI_ENCODE_DIN_FAULT_MSG                      STD_ON
#define EXI_ENCODE_DIN_GENERIC_ELEMENT                STD_OFF
#define EXI_ENCODE_DIN_GEN_CHALLENGE                  STD_OFF
#define EXI_ENCODE_DIN_INTERVAL                       STD_OFF
#define EXI_ENCODE_DIN_ISOLATION_LEVEL                STD_OFF
#define EXI_ENCODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS   STD_OFF
#define EXI_ENCODE_DIN_MESSAGE_HEADER                 STD_ON
#define EXI_ENCODE_DIN_METER_ID                       STD_OFF
#define EXI_ENCODE_DIN_METER_INFO                     STD_OFF
#define EXI_ENCODE_DIN_NOTIFICATION                   STD_ON
#define EXI_ENCODE_DIN_PARAMETER_SET                  STD_OFF
#define EXI_ENCODE_DIN_PARAMETER                      STD_OFF
#define EXI_ENCODE_DIN_PAYMENT_OPTIONS                STD_OFF
#define EXI_ENCODE_DIN_PAYMENT_OPTION                 STD_ON
#define EXI_ENCODE_DIN_PHYSICAL_VALUE                 STD_ON
#define EXI_ENCODE_DIN_PMAX_SCHEDULE_ENTRY            STD_OFF
#define EXI_ENCODE_DIN_PMAX_SCHEDULE                  STD_OFF
#define EXI_ENCODE_DIN_PRIVATE_KEY                    STD_OFF
#define EXI_ENCODE_DIN_PROFILE_ENTRY                  STD_ON
#define EXI_ENCODE_DIN_RELATIVE_TIME_INTERVAL         STD_OFF
#define EXI_ENCODE_DIN_RESPONSE_CODE                  STD_OFF
#define EXI_ENCODE_DIN_ROOT_CERTIFICATE_ID            STD_OFF
#define EXI_ENCODE_DIN_SALES_TARIFF_ENTRY             STD_OFF
#define EXI_ENCODE_DIN_SALES_TARIFF                   STD_OFF
#define EXI_ENCODE_DIN_SASCHEDULES                    STD_OFF
#define EXI_ENCODE_DIN_SASCHEDULE_LIST                STD_OFF
#define EXI_ENCODE_DIN_SASCHEDULE_TUPLE               STD_OFF
#define EXI_ENCODE_DIN_SELECTED_SERVICE_LIST          STD_ON
#define EXI_ENCODE_DIN_SELECTED_SERVICE               STD_ON
#define EXI_ENCODE_DIN_SERVICE_CATEGORY               STD_ON
#define EXI_ENCODE_DIN_SERVICE_CHARGE                 STD_OFF
#define EXI_ENCODE_DIN_SERVICE_NAME                   STD_OFF
#define EXI_ENCODE_DIN_SERVICE_PARAMETER_LIST         STD_OFF
#define EXI_ENCODE_DIN_SERVICE_SCOPE                  STD_ON
#define EXI_ENCODE_DIN_SERVICE_TAG_LIST               STD_OFF
#define EXI_ENCODE_DIN_SERVICE_TAG                    STD_OFF
#define EXI_ENCODE_DIN_SERVICE                        STD_OFF
#define EXI_ENCODE_DIN_SESSION_ID                     STD_ON
#define EXI_ENCODE_DIN_SIG_METER_READING              STD_OFF
#define EXI_ENCODE_DIN_SUPPORTED_ENERGY_TRANSFER_MODE STD_OFF
#define EXI_ENCODE_DIN_SUB_CERTIFICATES               STD_OFF
#define EXI_ENCODE_DIN_STRING_VALUE                   STD_OFF
#define EXI_ENCODE_DIN_TARIFF_DESCRIPTION             STD_OFF
#define EXI_ENCODE_DIN_TIME_INTERVAL                  STD_OFF
#define EXI_ENCODE_DIN_UNIT_SYMBOL                    STD_ON


#define EXI_DECODE_DIN_AC_EVCHARGE_PARAMETER          STD_OFF
#define EXI_DECODE_DIN_AC_EVSECHARGE_PARAMETER        STD_OFF
#define EXI_DECODE_DIN_AC_EVSESTATUS                  STD_OFF
#define EXI_DECODE_DIN_ATTRIBUTE_NAME                 STD_OFF
#define EXI_DECODE_DIN_ATTRIBUTE_VALUE                STD_OFF
#define EXI_DECODE_DIN_ATTRIBUTE_ID                   STD_ON
#define EXI_DECODE_DIN_BODY_BASE                      STD_OFF
#define EXI_DECODE_DIN_BODY_ELEMENT                   STD_OFF
#define EXI_DECODE_DIN_BODY                           STD_ON
#define EXI_DECODE_DIN_CERTIFICATE_CHAIN              STD_OFF
#define EXI_DECODE_DIN_CERTIFICATE                    STD_OFF
#define EXI_DECODE_DIN_CHARGE_PROGRESS                STD_OFF
#define EXI_DECODE_DIN_CHARGING_PROFILE               STD_OFF
#define EXI_DECODE_DIN_CHARGING_SESSION               STD_OFF
#define EXI_DECODE_DIN_CONSUMPTION_COST               STD_OFF
#define EXI_DECODE_DIN_CONTRACT_ID                    STD_OFF
#define EXI_DECODE_DIN_COST_KIND                      STD_OFF
#define EXI_DECODE_DIN_COST                           STD_OFF
#define EXI_DECODE_DIN_D_HPARAMS                      STD_OFF
#define EXI_DECODE_DIN_DC_EVCHARGE_PARAMETER          STD_OFF
#define EXI_DECODE_DIN_DC_EVERROR_CODE                STD_OFF
#define EXI_DECODE_DIN_DC_EVPOWER_DELIVERY_PARAMETER  STD_OFF
#define EXI_DECODE_DIN_DC_EVSTATUS                    STD_OFF
#define EXI_DECODE_DIN_DC_EVSECHARGE_PARAMETER        STD_ON
#define EXI_DECODE_DIN_DC_EVSESTATUS_CODE             STD_ON
#define EXI_DECODE_DIN_DC_EVSESTATUS                  STD_ON
#define EXI_DECODE_DIN_ENTRY                          STD_OFF
#define EXI_DECODE_DIN_EVCC_ID                        STD_OFF
#define EXI_DECODE_DIN_EVCHARGE_PARAMETER             STD_OFF
#define EXI_DECODE_DIN_EVPOWER_DELIVERY_PARAMETER     STD_OFF
#define EXI_DECODE_DIN_EVREQUESTED_ENERGY_TRANSFER    STD_OFF
#define EXI_DECODE_DIN_EVSECHARGE_PARAMETER           STD_ON
#define EXI_DECODE_DIN_EVSE_ID                        STD_ON
#define EXI_DECODE_DIN_EVSENOTIFICATION               STD_ON
#define EXI_DECODE_DIN_EVSEPROCESSING                 STD_ON
#define EXI_DECODE_DIN_EVSESTATUS                     STD_ON
#define EXI_DECODE_DIN_EVSESUPPORTED_ENERGY_TRANSFER  STD_ON
#define EXI_DECODE_DIN_EVSTATUS                       STD_OFF
#define EXI_DECODE_DIN_FAULT_CODE                     STD_ON
#define EXI_DECODE_DIN_FAULT_MSG                      STD_ON
#define EXI_DECODE_DIN_GENERIC_ELEMENT                STD_OFF
#define EXI_DECODE_DIN_GEN_CHALLENGE                  STD_OFF
#define EXI_DECODE_DIN_INTERVAL                       STD_OFF
#define EXI_DECODE_DIN_ISOLATION_LEVEL                STD_ON
#define EXI_DECODE_DIN_LIST_OF_ROOT_CERTIFICATE_IDS   STD_OFF
#define EXI_DECODE_DIN_MESSAGE_HEADER                 STD_ON
#define EXI_DECODE_DIN_METER_ID                       STD_OFF
#define EXI_DECODE_DIN_METER_INFO                     STD_OFF
#define EXI_DECODE_DIN_NOTIFICATION                   STD_ON
#define EXI_DECODE_DIN_PARAMETER_SET                  STD_OFF
#define EXI_DECODE_DIN_PARAMETER                      STD_OFF
#define EXI_DECODE_DIN_PAYMENT_OPTIONS                STD_ON
#define EXI_DECODE_DIN_PAYMENT_OPTION                 STD_ON
#define EXI_DECODE_DIN_PHYSICAL_VALUE                 STD_ON
#define EXI_DECODE_DIN_PMAX_SCHEDULE_ENTRY            STD_ON
#define EXI_DECODE_DIN_PMAX_SCHEDULE                  STD_ON
#define EXI_DECODE_DIN_PRIVATE_KEY                    STD_OFF
#define EXI_DECODE_DIN_PROFILE_ENTRY                  STD_OFF
#define EXI_DECODE_DIN_RELATIVE_TIME_INTERVAL         STD_ON
#define EXI_DECODE_DIN_RESPONSE_CODE                  STD_ON
#define EXI_DECODE_DIN_ROOT_CERTIFICATE_ID            STD_OFF
#define EXI_DECODE_DIN_SALES_TARIFF_ENTRY             STD_OFF
#define EXI_DECODE_DIN_SALES_TARIFF                   STD_OFF
#define EXI_DECODE_DIN_SASCHEDULES                    STD_ON
#define EXI_DECODE_DIN_SASCHEDULE_LIST                STD_ON
#define EXI_DECODE_DIN_SASCHEDULE_TUPLE               STD_ON
#define EXI_DECODE_DIN_SELECTED_SERVICE_LIST          STD_OFF
#define EXI_DECODE_DIN_SELECTED_SERVICE               STD_OFF
#define EXI_DECODE_DIN_SERVICE_CATEGORY               STD_ON
#define EXI_DECODE_DIN_SERVICE_CHARGE                 STD_ON
#define EXI_DECODE_DIN_SERVICE_NAME                   STD_ON
#define EXI_DECODE_DIN_SERVICE_PARAMETER_LIST         STD_OFF
#define EXI_DECODE_DIN_SERVICE_SCOPE                  STD_OFF
#define EXI_DECODE_DIN_SERVICE_TAG_LIST               STD_ON
#define EXI_DECODE_DIN_SERVICE_TAG                    STD_ON
#define EXI_DECODE_DIN_SERVICE                        STD_ON
#define EXI_DECODE_DIN_SESSION_ID                     STD_ON
#define EXI_DECODE_DIN_SIG_METER_READING              STD_OFF
#define EXI_DECODE_DIN_SUB_CERTIFICATES               STD_OFF
#define EXI_DECODE_DIN_SUPPORTED_ENERGY_TRANSFER_MODE STD_ON
#define EXI_DECODE_DIN_STRING_VALUE                   STD_OFF
#define EXI_DECODE_DIN_TARIFF_DESCRIPTION             STD_OFF
#define EXI_DECODE_DIN_TIME_INTERVAL                  STD_OFF
#define EXI_DECODE_DIN_UNIT_SYMBOL                    STD_ON

/**********************************************************************************************************************
 *  ISO 15118 specific configuration
 *********************************************************************************************************************/
#define EXI_ENABLE_ISO_MESSAGE_SET             STD_ON
#define EXI_ENABLE_ISO_EXT_MESSAGE_SET         STD_OFF
 
/**********************************************************************************************************************
 *  ISO Max Buffer Size
 *********************************************************************************************************************/
#define EXI_MAX_BUFFER_SIZE_ISO_ATTRIBUTEID    10u
#define EXI_MAX_BUFFER_SIZE_ISO_ATTRIBUTENAME  8u

/**********************************************************************************************************************
 *  ISO 15118-2 FDIS Schema Set Switches
 *********************************************************************************************************************/
#define EXI_ENCODE_ISO_SCHEMA_ROOT                              STD_ON
#define EXI_DECODE_SCHEMA_SET_ISO                               STD_ON
#define EXI_ENABLE_ISO_AC_BASIC_MESSAGE_SET                     STD_OFF
#define EXI_ENABLE_ISO_DC_BASIC_MESSAGE_SET                     STD_ON
#define EXI_ENABLE_ISO_AC_EXTENDED_MESSAGE_SET                  STD_OFF
#define EXI_ENABLE_ISO_DC_EXTENDED_MESSAGE_SET                  STD_OFF
#define EXI_ENCODE_ISO_SCHEMA_FRAGMENT                          STD_OFF
                                                                
#define EXI_ENCODE_ISO_AUTHORIZATION_REQ                        STD_ON
#define EXI_ENCODE_ISO_AUTHORIZATION_RES                        STD_OFF
#define EXI_ENCODE_ISO_CABLE_CHECK_REQ                          STD_ON
#define EXI_ENCODE_ISO_CABLE_CHECK_RES                          STD_OFF
#define EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_REQ             STD_OFF
#define EXI_ENCODE_ISO_CERTIFICATE_INSTALLATION_RES             STD_OFF
#define EXI_ENCODE_ISO_CERTIFICATE_UPDATE_REQ                   STD_OFF
#define EXI_ENCODE_ISO_CERTIFICATE_UPDATE_RES                   STD_OFF
#define EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ           STD_ON
#define EXI_ENCODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES           STD_OFF
#define EXI_ENCODE_ISO_CHARGING_STATUS_REQ                      STD_OFF
#define EXI_ENCODE_ISO_CHARGING_STATUS_RES                      STD_OFF
#define EXI_ENCODE_ISO_CURRENT_DEMAND_REQ                       STD_ON
#define EXI_ENCODE_ISO_CURRENT_DEMAND_RES                       STD_OFF
#define EXI_ENCODE_ISO_METERING_RECEIPT_REQ                     STD_ON
#define EXI_ENCODE_ISO_METERING_RECEIPT_RES                     STD_OFF
#define EXI_ENCODE_ISO_PAYMENT_DETAILS_REQ                      STD_OFF
#define EXI_ENCODE_ISO_PAYMENT_DETAILS_RES                      STD_OFF
#define EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_REQ            STD_ON
#define EXI_ENCODE_ISO_PAYMENT_SERVICE_SELECTION_RES            STD_OFF
#define EXI_ENCODE_ISO_POWER_DELIVERY_REQ                       STD_ON
#define EXI_ENCODE_ISO_POWER_DELIVERY_RES                       STD_OFF
#define EXI_ENCODE_ISO_PRE_CHARGE_REQ                           STD_ON
#define EXI_ENCODE_ISO_PRE_CHARGE_RES                           STD_OFF
#define EXI_ENCODE_ISO_SERVICE_DETAIL_REQ                       STD_ON
#define EXI_ENCODE_ISO_SERVICE_DETAIL_RES                       STD_OFF
#define EXI_ENCODE_ISO_SERVICE_DISCOVERY_REQ                    STD_ON
#define EXI_ENCODE_ISO_SERVICE_DISCOVERY_RES                    STD_OFF
#define EXI_ENCODE_ISO_SESSION_SETUP_REQ                        STD_ON
#define EXI_ENCODE_ISO_SESSION_SETUP_RES                        STD_OFF
#define EXI_ENCODE_ISO_SESSION_STOP_REQ                         STD_ON
#define EXI_ENCODE_ISO_SESSION_STOP_RES                         STD_OFF
#define EXI_ENCODE_ISO_WELDING_DETECTION_REQ                    STD_ON
#define EXI_ENCODE_ISO_WELDING_DETECTION_RES                    STD_OFF
#define EXI_ENCODE_ISO_V2G_MESSAGE                              STD_ON
                                                                
                                                                
#define EXI_DECODE_ISO_AUTHORIZATION_REQ                        STD_OFF
#define EXI_DECODE_ISO_AUTHORIZATION_RES                        STD_ON
#define EXI_DECODE_ISO_CABLE_CHECK_REQ                          STD_OFF
#define EXI_DECODE_ISO_CABLE_CHECK_RES                          STD_ON
#define EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_REQ             STD_OFF
#define EXI_DECODE_ISO_CERTIFICATE_INSTALLATION_RES             STD_OFF
#define EXI_DECODE_ISO_CERTIFICATE_UPDATE_REQ                   STD_OFF
#define EXI_DECODE_ISO_CERTIFICATE_UPDATE_RES                   STD_OFF
#define EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_REQ           STD_OFF
#define EXI_DECODE_ISO_CHARGE_PARAMETER_DISCOVERY_RES           STD_ON
#define EXI_DECODE_ISO_CHARGING_STATUS_REQ                      STD_OFF
#define EXI_DECODE_ISO_CHARGING_STATUS_RES                      STD_OFF
#define EXI_DECODE_ISO_CURRENT_DEMAND_REQ                       STD_OFF
#define EXI_DECODE_ISO_CURRENT_DEMAND_RES                       STD_ON
#define EXI_DECODE_ISO_METERING_RECEIPT_REQ                     STD_OFF
#define EXI_DECODE_ISO_METERING_RECEIPT_RES                     STD_OFF
#define EXI_DECODE_ISO_PAYMENT_DETAILS_REQ                      STD_OFF
#define EXI_DECODE_ISO_PAYMENT_DETAILS_RES                      STD_OFF
#define EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_REQ            STD_OFF
#define EXI_DECODE_ISO_PAYMENT_SERVICE_SELECTION_RES            STD_ON
#define EXI_DECODE_ISO_POWER_DELIVERY_REQ                       STD_OFF
#define EXI_DECODE_ISO_POWER_DELIVERY_RES                       STD_ON
#define EXI_DECODE_ISO_PRE_CHARGE_REQ                           STD_OFF
#define EXI_DECODE_ISO_PRE_CHARGE_RES                           STD_ON
#define EXI_DECODE_ISO_SERVICE_DETAIL_REQ                       STD_OFF
#define EXI_DECODE_ISO_SERVICE_DETAIL_RES                       STD_ON
#define EXI_DECODE_ISO_SERVICE_DISCOVERY_REQ                    STD_OFF
#define EXI_DECODE_ISO_SERVICE_DISCOVERY_RES                    STD_ON
#define EXI_DECODE_ISO_SESSION_SETUP_REQ                        STD_OFF
#define EXI_DECODE_ISO_SESSION_SETUP_RES                        STD_ON
#define EXI_DECODE_ISO_SESSION_STOP_REQ                         STD_OFF
#define EXI_DECODE_ISO_SESSION_STOP_RES                         STD_ON
#define EXI_DECODE_ISO_WELDING_DETECTION_REQ                    STD_OFF
#define EXI_DECODE_ISO_WELDING_DETECTION_RES                    STD_ON
#define EXI_DECODE_ISO_V2G_MESSAGE                              STD_ON


#define EXI_ENCODE_ISO_AC_EVCHARGE_PARAMETER                    STD_OFF
#define EXI_ENCODE_ISO_AC_EVSECHARGE_PARAMETER                  STD_OFF
#define EXI_ENCODE_ISO_AC_EVSESTATUS                            STD_OFF
#define EXI_ENCODE_ISO_ATTRIBUTE_ID                             STD_ON
#define EXI_ENCODE_ISO_ATTRIBUTE_NAME                           STD_OFF
#define EXI_ENCODE_ISO_ATTRIBUTE_VALUE                          STD_OFF
#define EXI_ENCODE_ISO_BODY_BASE                                STD_ON
#define EXI_ENCODE_ISO_BODY_ELEMENT                             STD_ON
#define EXI_ENCODE_ISO_BODY                                     STD_ON
#define EXI_ENCODE_ISO_CERTIFICATE_CHAIN                        STD_OFF
#define EXI_ENCODE_ISO_CERTIFICATE                              STD_OFF
#define EXI_ENCODE_ISO_CHARGE_PROGRESS                          STD_ON
#define EXI_ENCODE_ISO_CHARGING_PROFILE                         STD_ON
#define EXI_ENCODE_ISO_CHARGING_SESSION                         STD_ON
#define EXI_ENCODE_ISO_CHARGE_SERVICE                           STD_OFF
#define EXI_ENCODE_ISO_CONSUMPTION_COST                         STD_OFF
#define EXI_ENCODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY STD_OFF
#define EXI_ENCODE_ISO_COST_KIND                                STD_OFF
#define EXI_ENCODE_ISO_COST                                     STD_OFF
#define EXI_ENCODE_ISO_DC_EVCHARGE_PARAMETER                    STD_ON
#define EXI_ENCODE_ISO_DC_EVERROR_CODE                          STD_ON
#define EXI_ENCODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER            STD_ON
#define EXI_ENCODE_ISO_DC_EVSTATUS                              STD_ON
#define EXI_ENCODE_ISO_DC_EVSECHARGE_PARAMETER                  STD_OFF
#define EXI_ENCODE_ISO_DC_EVSESTATUS_CODE                       STD_OFF
#define EXI_ENCODE_ISO_DC_EVSESTATUS                            STD_OFF
#define EXI_ENCODE_ISO_DIFFIE_HELLMAN_PUBLICKEY                 STD_OFF
#define EXI_ENCODE_ISO_EMAID                                    STD_OFF
#define EXI_ENCODE_ISO_EMAID_ELEMENT_FRAGMENT                   STD_OFF
#define EXI_ENCODE_ISO_ENTRY                                    STD_ON
#define EXI_ENCODE_ISO_EVCC_ID                                  STD_ON
#define EXI_ENCODE_ISO_EVCHARGE_PARAMETER                       STD_ON
#define EXI_ENCODE_ISO_EVPOWER_DELIVERY_PARAMETER               STD_ON
#define EXI_ENCODE_ISO_ENERGY_TRANSFER_MODE                     STD_ON
#define EXI_ENCODE_ISO_EVSECHARGE_PARAMETER                     STD_OFF
#define EXI_ENCODE_ISO_EVSE_ID                                  STD_OFF
#define EXI_ENCODE_ISO_EVSENOTIFICATION                         STD_OFF
#define EXI_ENCODE_ISO_EVSEPROCESSING                           STD_OFF
#define EXI_ENCODE_ISO_EVSESTATUS                               STD_OFF
#define EXI_ENCODE_ISO_EVSTATUS                                 STD_ON
#define EXI_ENCODE_ISO_E_MAID                                   STD_OFF
#define EXI_ENCODE_ISO_E_MAID_ELEMENT_FRAGMENT                  STD_OFF
#define EXI_ENCODE_ISO_FAULT_CODE                               STD_ON
#define EXI_ENCODE_ISO_FAULT_MSG                                STD_ON
#define EXI_ENCODE_ISO_GENERIC_ELEMENT                          STD_OFF
#define EXI_ENCODE_ISO_GEN_CHALLENGE                            STD_OFF
#define EXI_ENCODE_ISO_INTERVAL                                 STD_ON
#define EXI_ENCODE_ISO_ISOLATION_LEVEL                          STD_OFF
#define EXI_ENCODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS             STD_OFF
#define EXI_ENCODE_ISO_MESSAGE_HEADER                           STD_ON
#define EXI_ENCODE_ISO_METER_ID                                 STD_ON
#define EXI_ENCODE_ISO_METER_INFO                               STD_ON
#define EXI_ENCODE_ISO_NOTIFICATION                             STD_ON
#define EXI_ENCODE_ISO_PARAMETER_SET                            STD_OFF
#define EXI_ENCODE_ISO_PARAMETER                                STD_OFF
#define EXI_ENCODE_ISO_PAYMENT_OPTION_LIST                      STD_OFF
#define EXI_ENCODE_ISO_PAYMENT_OPTION                           STD_ON
#define EXI_ENCODE_ISO_PHYSICAL_VALUE                           STD_ON
#define EXI_ENCODE_ISO_PMAX_SCHEDULE_ENTRY                      STD_OFF
#define EXI_ENCODE_ISO_PMAX_SCHEDULE                            STD_OFF
#define EXI_ENCODE_ISO_PRIVATE_KEY                              STD_OFF
#define EXI_ENCODE_ISO_PROFILE_ENTRY                            STD_ON
#define EXI_ENCODE_ISO_RELATIVE_TIME_INTERVAL                   STD_OFF
#define EXI_ENCODE_ISO_RESPONSE_CODE                            STD_OFF
#define EXI_ENCODE_ISO_ROOT_CERTIFICATE_ID                      STD_OFF
#define EXI_ENCODE_ISO_SALES_TARIFF_ENTRY                       STD_OFF
#define EXI_ENCODE_ISO_SALES_TARIFF                             STD_OFF
#define EXI_ENCODE_ISO_SASCHEDULES                              STD_OFF
#define EXI_ENCODE_ISO_SASCHEDULE_LIST                          STD_OFF
#define EXI_ENCODE_ISO_SASCHEDULE_TUPLE                         STD_OFF
#define EXI_ENCODE_ISO_SELECTED_SERVICE_LIST                    STD_ON
#define EXI_ENCODE_ISO_SELECTED_SERVICE                         STD_ON
#define EXI_ENCODE_ISO_SERVICE_CATEGORY                         STD_ON
#define EXI_ENCODE_ISO_SERVICE_NAME                             STD_OFF
#define EXI_ENCODE_ISO_SERVICE_LIST                             STD_OFF
#define EXI_ENCODE_ISO_SERVICE_PARAMETER_LIST                   STD_OFF
#define EXI_ENCODE_ISO_SERVICE_SCOPE                            STD_ON
#define EXI_ENCODE_ISO_SERVICE                                  STD_OFF
#define EXI_ENCODE_ISO_SESSION_ID                               STD_ON
#define EXI_ENCODE_ISO_SIG_METER_READING                        STD_ON
#define EXI_ENCODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE           STD_OFF
#define EXI_ENCODE_ISO_SUB_CERTIFICATES                         STD_OFF
#define EXI_ENCODE_ISO_STRING_VALUE                             STD_OFF
#define EXI_ENCODE_ISO_TARIFF_DESCRIPTION                       STD_OFF
#define EXI_ENCODE_ISO_TIME_INTERVAL                            STD_ON
#define EXI_ENCODE_ISO_UNIT_SYMBOL                              STD_ON

#define EXI_DECODE_ISO_AC_EVCHARGE_PARAMETER                    STD_OFF
#define EXI_DECODE_ISO_AC_EVSECHARGE_PARAMETER                  STD_ON
#define EXI_DECODE_ISO_AC_EVSESTATUS                            STD_ON
#define EXI_DECODE_ISO_ATTRIBUTE_NAME                           STD_ON
#define EXI_DECODE_ISO_ATTRIBUTE_VALUE                          STD_ON
#define EXI_DECODE_ISO_ATTRIBUTE_ID                             STD_ON
#define EXI_DECODE_ISO_BODY_BASE                                STD_OFF
#define EXI_DECODE_ISO_BODY_ELEMENT                             STD_ON
#define EXI_DECODE_ISO_BODY                                     STD_ON
#define EXI_DECODE_ISO_CERTIFICATE_CHAIN                        STD_OFF
#define EXI_DECODE_ISO_CERTIFICATE                              STD_OFF
#define EXI_DECODE_ISO_CHARGE_PROGRESS                          STD_OFF
#define EXI_DECODE_ISO_CHARGING_PROFILE                         STD_OFF
#define EXI_DECODE_ISO_CHARGING_SESSION                         STD_OFF
#define EXI_DECODE_ISO_CHARGE_SERVICE                           STD_ON
#define EXI_DECODE_ISO_CONSUMPTION_COST                         STD_ON
#define EXI_DECODE_ISO_CONTRACT_SIGNATURE_ENCRYPTED_PRIVATE_KEY STD_OFF
#define EXI_DECODE_ISO_COST_KIND                                STD_ON
#define EXI_DECODE_ISO_COST                                     STD_ON
#define EXI_DECODE_ISO_DIFFIE_HELLMAN_PUBLICKEY                 STD_OFF
#define EXI_DECODE_ISO_DC_EVCHARGE_PARAMETER                    STD_OFF
#define EXI_DECODE_ISO_DC_EVERROR_CODE                          STD_OFF
#define EXI_DECODE_ISO_DC_EVPOWER_DELIVERY_PARAMETER            STD_OFF
#define EXI_DECODE_ISO_DC_EVSTATUS                              STD_OFF
#define EXI_DECODE_ISO_DC_EVSECHARGE_PARAMETER                  STD_ON
#define EXI_DECODE_ISO_DC_EVSESTATUS_CODE                       STD_ON
#define EXI_DECODE_ISO_DC_EVSESTATUS                            STD_ON
#define EXI_DECODE_ISO_EMAID                                    STD_OFF
#define EXI_DECODE_ISO_ENTRY                                    STD_ON
#define EXI_DECODE_ISO_EVCC_ID                                  STD_OFF
#define EXI_DECODE_ISO_EVCHARGE_PARAMETER                       STD_OFF
#define EXI_DECODE_ISO_EVPOWER_DELIVERY_PARAMETER               STD_OFF
#define EXI_DECODE_ISO_ENERGY_TRANSFER_MODE                     STD_ON
#define EXI_DECODE_ISO_EVSECHARGE_PARAMETER                     STD_ON
#define EXI_DECODE_ISO_EVSE_ID                                  STD_ON
#define EXI_DECODE_ISO_EVSENOTIFICATION                         STD_ON
#define EXI_DECODE_ISO_EVSEPROCESSING                           STD_ON
#define EXI_DECODE_ISO_EVSESTATUS                               STD_ON
#define EXI_DECODE_ISO_EVSTATUS                                 STD_OFF
#define EXI_DECODE_ISO_E_MAID                                   STD_OFF
#define EXI_DECODE_ISO_FAULT_CODE                               STD_ON
#define EXI_DECODE_ISO_FAULT_MSG                                STD_ON
#define EXI_DECODE_ISO_GENERIC_ELEMENT                          STD_OFF
#define EXI_DECODE_ISO_GEN_CHALLENGE                            STD_ON
#define EXI_DECODE_ISO_INTERVAL                                 STD_OFF
#define EXI_DECODE_ISO_ISOLATION_LEVEL                          STD_ON
#define EXI_DECODE_ISO_LIST_OF_ROOT_CERTIFICATE_IDS             STD_OFF
#define EXI_DECODE_ISO_MESSAGE_HEADER                           STD_ON
#define EXI_DECODE_ISO_METER_ID                                 STD_ON
#define EXI_DECODE_ISO_METER_INFO                               STD_ON
#define EXI_DECODE_ISO_NOTIFICATION                             STD_ON
#define EXI_DECODE_ISO_PARAMETER_SET                            STD_ON
#define EXI_DECODE_ISO_PARAMETER                                STD_ON
#define EXI_DECODE_ISO_PAYMENT_OPTION_LIST                      STD_ON
#define EXI_DECODE_ISO_PAYMENT_OPTION                           STD_ON
#define EXI_DECODE_ISO_PHYSICAL_VALUE                           STD_ON
#define EXI_DECODE_ISO_PMAX_SCHEDULE_ENTRY                      STD_ON
#define EXI_DECODE_ISO_PMAX_SCHEDULE                            STD_ON
#define EXI_DECODE_ISO_PRIVATE_KEY                              STD_OFF
#define EXI_DECODE_ISO_PROFILE_ENTRY                            STD_OFF
#define EXI_DECODE_ISO_RELATIVE_TIME_INTERVAL                   STD_ON
#define EXI_DECODE_ISO_RESPONSE_CODE                            STD_ON
#define EXI_DECODE_ISO_ROOT_CERTIFICATE_ID                      STD_OFF
#define EXI_DECODE_ISO_SALES_TARIFF_ENTRY                       STD_ON
#define EXI_DECODE_ISO_SALES_TARIFF                             STD_ON
#define EXI_DECODE_ISO_SASCHEDULES                              STD_ON
#define EXI_DECODE_ISO_SASCHEDULE_LIST                          STD_ON
#define EXI_DECODE_ISO_SASCHEDULE_TUPLE                         STD_ON
#define EXI_DECODE_ISO_SELECTED_SERVICE_LIST                    STD_OFF
#define EXI_DECODE_ISO_SELECTED_SERVICE                         STD_OFF
#define EXI_DECODE_ISO_SERVICE_CATEGORY                         STD_ON
#define EXI_DECODE_ISO_SERVICE_NAME                             STD_ON
#define EXI_DECODE_ISO_SERVICE_LIST                             STD_ON
#define EXI_DECODE_ISO_SERVICE_PARAMETER_LIST                   STD_ON
#define EXI_DECODE_ISO_SERVICE_SCOPE                            STD_ON
#define EXI_DECODE_ISO_SERVICE                                  STD_ON
#define EXI_DECODE_ISO_SESSION_ID                               STD_ON
#define EXI_DECODE_ISO_SIG_METER_READING                        STD_ON
#define EXI_DECODE_ISO_SUB_CERTIFICATES                         STD_OFF
#define EXI_DECODE_ISO_SUPPORTED_ENERGY_TRANSFER_MODE           STD_ON
#define EXI_DECODE_ISO_STRING_VALUE                             STD_OFF
#define EXI_DECODE_ISO_TARIFF_DESCRIPTION                       STD_ON
#define EXI_DECODE_ISO_TIME_INTERVAL                            STD_ON
#define EXI_DECODE_ISO_UNIT_SYMBOL                              STD_ON

/**********************************************************************************************************************
 *  ISO 15118 ED2 DIS specific configuration
 *********************************************************************************************************************/
#define EXI_ENABLE_ISO_ED2_DIS_MESSAGE_SET      STD_OFF
#define EXI_ENCODE_ISO_ED2_DIS_SCHEMA_FRAGMENT  STD_OFF

/**********************************************************************************************************************
 *  XML Signature specific configuration
 *********************************************************************************************************************/
#define EXI_ENABLE_XMLSIG_MESSAGE_SET                  STD_OFF
#define EXI_ENABLE_ENCODE_XMLSIG_MESSAGE_SET           STD_OFF
#define EXI_ENABLE_DECODE_XMLSIG_MESSAGE_SET           STD_ON
#define EXI_ENCODE_XMLSIG_SCHEMA_FRAGMENT              STD_OFF
 
/**********************************************************************************************************************
 *  XML Signature Elements Switches
 *********************************************************************************************************************/
#define EXI_ENCODE_XMLSIG_SCHEMA_ROOT             STD_OFF
#define EXI_DECODE_SCHEMA_SET_XMLSIG              STD_ON

#define EXI_DECODE_XMLSIG_ATTRIBUTE_ALGORITHM     STD_ON
#define EXI_DECODE_XMLSIG_ATTRIBUTE_ENCODING      STD_OFF
#define EXI_DECODE_XMLSIG_ATTRIBUTE_ID            STD_ON
#define EXI_DECODE_XMLSIG_ATTRIBUTE_MIME          STD_OFF
#define EXI_DECODE_XMLSIG_ATTRIBUTE_TARGET        STD_OFF
#define EXI_DECODE_XMLSIG_ATTRIBUTE               STD_ON
#define EXI_DECODE_XMLSIG_ATTRIBUTE_URI           STD_ON
#define EXI_DECODE_XMLSIG_BASE64BINARY            STD_ON
#define EXI_DECODE_XMLSIG_CANONICALIZATION_METHOD STD_ON
#define EXI_DECODE_XMLSIG_CRYPTO_BINARY           STD_OFF
#define EXI_DECODE_XMLSIG_DIGEST_METHOD           STD_ON
#define EXI_DECODE_XMLSIG_DIGEST_VALUE            STD_ON
#define EXI_DECODE_XMLSIG_DSAKEY_VALUE            STD_OFF
#define EXI_DECODE_XMLSIG_KEY_INFO                STD_OFF
#define EXI_DECODE_XMLSIG_KEY_NAME                STD_OFF
#define EXI_DECODE_XMLSIG_KEY_VALUE               STD_OFF
#define EXI_DECODE_XMLSIG_MANIFEST                STD_OFF
#define EXI_DECODE_XMLSIG_MGMT_DATA               STD_OFF
#define EXI_DECODE_XMLSIG_OBJECT                  STD_OFF
#define EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ0     STD_OFF
#define EXI_DECODE_XMLSIG_PGPDATA_CHOICE_SEQ1     STD_OFF
#define EXI_DECODE_XMLSIG_PGPDATA                 STD_OFF
#define EXI_DECODE_XMLSIG_REFERENCE               STD_ON
#define EXI_DECODE_XMLSIG_RETRIEVAL_METHOD        STD_OFF
#define EXI_DECODE_XMLSIG_RSAKEY_VALUE            STD_OFF
#define EXI_DECODE_XMLSIG_SIGNATURE_METHOD        STD_ON
#define EXI_DECODE_XMLSIG_SIGNATURE_PROPERTIES    STD_OFF
#define EXI_DECODE_XMLSIG_SIGNATURE_PROPERTY      STD_OFF
#define EXI_DECODE_XMLSIG_SIGNATURE               STD_ON
#define EXI_DECODE_XMLSIG_SIGNATURE_VALUE         STD_ON
#define EXI_DECODE_XMLSIG_SIGNED_INFO             STD_ON
#define EXI_DECODE_XMLSIG_SPKIDATA                STD_OFF
#define EXI_DECODE_XMLSIG_STRING                  STD_ON
#define EXI_DECODE_XMLSIG_TRANSFORMS              STD_ON
#define EXI_DECODE_XMLSIG_TRANSFORM               STD_ON
#define EXI_DECODE_XMLSIG_X509DATA                STD_OFF
#define EXI_DECODE_XMLSIG_X509ISSUER_SERIAL       STD_OFF

 
/**********************************************************************************************************************
 *  Struct Padding configuration
 *********************************************************************************************************************/
#define EXI_STRUCTURE_PADDING_16BIT_ENABLE        STD_OFF
#define EXI_STRUCTURE_PADDING_32BIT_ENABLE        STD_ON
#define EXI_STRUCTURE_PADDING_BIT_FIELD_SIZE_BYTE 1u
#define EXI_STRUCTURE_PADDING_ENUM_SIZE_BYTE      1u


#endif /* EXI_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Exi_Cfg.h
 *********************************************************************************************************************/
 
