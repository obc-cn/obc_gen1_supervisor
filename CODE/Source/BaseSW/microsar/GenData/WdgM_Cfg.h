/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: WdgM
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: WdgM_Cfg.h
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined (WDGM_CFG_H)
# define WDGM_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
# include "WdgM_Types.h"


/**********************************************************************************************************************
 *  VERSION INFORMATION AND CHECK
 *********************************************************************************************************************/

#define WDGM_CFG_MAJOR_VERSION (2u) 
#define WDGM_CFG_MINOR_VERSION (3u) 
#define WDGM_CFG_PATCH_VERSION (0u) 

/* Check the version of WdgM Types header file */
#if ( (WDGM_TYPES_MAJOR_VERSION != (5u)) \
   || (WDGM_TYPES_MINOR_VERSION != (5u)) )
# error "Version numbers of WdgM_Cfg.h and WdgM_Types.h are inconsistent!"
#endif

# define WDGM_NR_OF_CHECKPOINTS           (8u) 
# define WDGM_NR_OF_ENTITIES              (2u) 
# define WDGM_NR_OF_ALLOWED_CALLERS       (1u) 
# define WDGM_NR_OF_GLOBAL_TRANSITIONS    (0u) 
# define WDGM_NR_OF_LOCAL_TRANSITIONS     (8u) 
# define WDGM_NR_OF_WATCHDOGS_CORE0       (1u) 
# define WDGM_NR_OF_TRIGGER_MODES_CORE0   (1u) 

/* Checkpoints for supervised entity 'WdgMSupervisedEntity' */ 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointCP0 (0u) 
/* Checkpoints for supervised entity 'WdgMSupervisedEntityProgramFlow' */ 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP0 (0u) 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP1 (1u) 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP2 (2u) 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP3 (3u) 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP4 (4u) 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP5 (5u) 
# define WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP6 (6u) 
 

# define WdgMConf_WdgMSupervisedEntity_WdgMSupervisedEntity (0u) 
# define WdgMConf_WdgMSupervisedEntity_WdgMSupervisedEntityProgramFlow (1u) 


#define WDGM_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern CONST(WdgM_ConfigType, WDGM_CONST) WdgMConfig_Mode0_core0; 

#define WDGM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*
 * Status reporting (WdgMStatusReportingMechanism) configured to WDGM_USE_MODE_SWITCH_PORTS
 */

#define WDGM_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
 
/* 
 * Local callback routines for mode switch ports of core 0 
 */ 
/* Local state callback for supervised entity 'WdgMSupervisedEntity' */ 
FUNC(void, WDGM_CODE) WdgM_LocalStatusChangeNotificationSE0(WdgMMode Status); /* PRQA S 0777 */ /* MD_MSR_Rule5.1 */ 
/* Local state callback for supervised entity 'WdgMSupervisedEntityProgramFlow' */ 
FUNC(void, WDGM_CODE) WdgM_LocalStatusChangeNotificationSE1(WdgMMode Status); /* PRQA S 0777 */ /* MD_MSR_Rule5.1 */ 

#define WDGM_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */ 

#endif /* WDGM_CFG_H*/

/**********************************************************************************************************************
 *  END OF FILE: WdgM_Cfg.h
 *********************************************************************************************************************/

