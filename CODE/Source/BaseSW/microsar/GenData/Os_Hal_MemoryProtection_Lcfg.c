/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Hal_MemoryProtection_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:49
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_HAL_MEMORYPROTECTION_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Hal_MemoryProtection_Cfg.h"
#include "Os_Hal_MemoryProtection_Lcfg.h"
#include "Os_Hal_MemoryProtection.h"

/* Os kernel module dependencies */

/* Os hal dependencies */


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL memory protection configuration data: OsCore0 */
static CONST(Os_Hal_MpuRegionRangeConfigType, OS_CONST) OsCfg_Hal_Mp_OsCore0_DataRegions[15];
static CONST(Os_Hal_MpuRegionRangeConfigType, OS_CONST) OsCfg_Hal_Mp_OsCore0_DataRegions[15] =
{
  {
    /* MPU region: Stack region */
    /* .StartAddress  = */ (uint32)0x0uL,  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */
    /* .EndAddress    = */ (uint32)0x0uL  /* PRQA S 0306 */ /* MD_Os_Hal_Rule11.4_0306 */
  },
  {
    /* MPU region: READ_ALL_Core0 */
    /* .StartAddress  = */ (uint32)0x00000000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xFFFFFFF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_StackEnd_To_BeforeASIL_Core0 */
    /* .StartAddress  = */ (uint32)&_OS_CORE0_STACKS_END,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)&_ASIL_Core0_BEFORE_ASIL_SECTION  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_ASILSECTION_Core0 */
    /* .StartAddress  = */ (uint32)&_ASIL_Core0_VAR_BSS_ALL_START,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)&_ASIL_Core0_VAR_ZERO_INIT_ALL_END  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QMSection_To_Segment14_Core0 */
    /* .StartAddress  = */ (uint32)&_QM_Core0_VAR_BSS_ALL_START,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xEFFFFFF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_ASIL_Peripheral */
    /* .StartAddress  = */ (uint32)0xF0000000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xFFFFFFF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_Peripheral_SPI0_AREA */
    /* .StartAddress  = */ (uint32)0xF0001C00uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF0001CF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_Peripheral_CAN_PROTECTED_AREA_ENDINIT */
    /* .StartAddress  = */ (uint32)0xF0018000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF001BFF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_CAN_PROTECTED_AREA_SRC */
    /* .StartAddress  = */ (uint32)0xF0038000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF0039FF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_PORT0_AREA */
    /* .StartAddress  = */ (uint32)0xF003A000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF003A0F8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_PORT22_AREA */
    /* .StartAddress  = */ (uint32)0xF003C200uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF003C2F8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_PORT11_AREA */
    /* .StartAddress  = */ (uint32)0xF003B100uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF003B1F8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_PORT14_AREA */
    /* .StartAddress  = */ (uint32)0xF003B400uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF003B4F8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_Peripheral_SPI1_AREA */
    /* .StartAddress  = */ (uint32)0xF0001D00uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF0001DF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
  {
    /* MPU region: WRITE_QM_Peripheral_DMA_AREA */
    /* .StartAddress  = */ (uint32)0xF0010000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xF0013FF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
};
static CONST(Os_Hal_MpuRegionRangeConfigType, OS_CONST) OsCfg_Hal_Mp_OsCore0_CodeRegions[1];
static CONST(Os_Hal_MpuRegionRangeConfigType, OS_CONST) OsCfg_Hal_Mp_OsCore0_CodeRegions[1] =
{
  {
    /* MPU region: EXC_ALL_Core0 */
    /* .StartAddress  = */ (uint32)0x00000000uL,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
    /* .EndAddress    = */ (uint32)0xFFFFFFF8uL  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */
  },
};
CONST(Os_Hal_MpCoreConfigType, OS_CONST) OsCfg_Hal_Mp_OsCore0 =
{
  /* .MpuDataRegionStartSlot           = */ 0,
  /* .MpuDataRegionCount               = */ 15,
  /* .MpuDataRegions                   = */ OsCfg_Hal_Mp_OsCore0_DataRegions,
  {
  /* Read access bit mask */
  /* MpuDataRegionReadEnablePS0        = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_ENABLE_REGION1 | OS_HAL_COREMPU_DISABLE_REGION2 | OS_HAL_COREMPU_DISABLE_REGION3 | OS_HAL_COREMPU_DISABLE_REGION4 | OS_HAL_COREMPU_DISABLE_REGION5 | OS_HAL_COREMPU_DISABLE_REGION6 | OS_HAL_COREMPU_DISABLE_REGION7 | OS_HAL_COREMPU_DISABLE_REGION8 | OS_HAL_COREMPU_DISABLE_REGION9 | OS_HAL_COREMPU_DISABLE_REGION10 | OS_HAL_COREMPU_DISABLE_REGION11 | OS_HAL_COREMPU_DISABLE_REGION12 | OS_HAL_COREMPU_DISABLE_REGION13 | OS_HAL_COREMPU_DISABLE_REGION14,
  /* MpuDataRegionReadEnablePS1        = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_ENABLE_REGION1 | OS_HAL_COREMPU_DISABLE_REGION2 | OS_HAL_COREMPU_DISABLE_REGION3 | OS_HAL_COREMPU_DISABLE_REGION4 | OS_HAL_COREMPU_DISABLE_REGION5 | OS_HAL_COREMPU_DISABLE_REGION6 | OS_HAL_COREMPU_DISABLE_REGION7 | OS_HAL_COREMPU_DISABLE_REGION8 | OS_HAL_COREMPU_DISABLE_REGION9 | OS_HAL_COREMPU_DISABLE_REGION10 | OS_HAL_COREMPU_DISABLE_REGION11 | OS_HAL_COREMPU_DISABLE_REGION12 | OS_HAL_COREMPU_DISABLE_REGION13 | OS_HAL_COREMPU_DISABLE_REGION14,
  /* MpuDataRegionReadEnablePS2        = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_ENABLE_REGION1 | OS_HAL_COREMPU_DISABLE_REGION2 | OS_HAL_COREMPU_DISABLE_REGION3 | OS_HAL_COREMPU_DISABLE_REGION4 | OS_HAL_COREMPU_DISABLE_REGION5 | OS_HAL_COREMPU_DISABLE_REGION6 | OS_HAL_COREMPU_DISABLE_REGION7 | OS_HAL_COREMPU_DISABLE_REGION8 | OS_HAL_COREMPU_DISABLE_REGION9 | OS_HAL_COREMPU_DISABLE_REGION10 | OS_HAL_COREMPU_DISABLE_REGION11 | OS_HAL_COREMPU_DISABLE_REGION12 | OS_HAL_COREMPU_DISABLE_REGION13 | OS_HAL_COREMPU_DISABLE_REGION14,
  /* MpuDataRegionReadEnablePS3        = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_ENABLE_REGION1 | OS_HAL_COREMPU_DISABLE_REGION2 | OS_HAL_COREMPU_DISABLE_REGION3 | OS_HAL_COREMPU_DISABLE_REGION4 | OS_HAL_COREMPU_DISABLE_REGION5 | OS_HAL_COREMPU_DISABLE_REGION6 | OS_HAL_COREMPU_DISABLE_REGION7 | OS_HAL_COREMPU_DISABLE_REGION8 | OS_HAL_COREMPU_DISABLE_REGION9 | OS_HAL_COREMPU_DISABLE_REGION10 | OS_HAL_COREMPU_DISABLE_REGION11 | OS_HAL_COREMPU_DISABLE_REGION12 | OS_HAL_COREMPU_DISABLE_REGION13 | OS_HAL_COREMPU_DISABLE_REGION14,
  },
  {
  /* Write access bit mask */
  /* MpuDataRegionWriteEnablePS0       = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_DISABLE_REGION1 | OS_HAL_COREMPU_ENABLE_REGION2 | OS_HAL_COREMPU_ENABLE_REGION3 | OS_HAL_COREMPU_ENABLE_REGION4 | OS_HAL_COREMPU_ENABLE_REGION5 | OS_HAL_COREMPU_ENABLE_REGION6 | OS_HAL_COREMPU_ENABLE_REGION7 | OS_HAL_COREMPU_ENABLE_REGION8 | OS_HAL_COREMPU_ENABLE_REGION9 | OS_HAL_COREMPU_ENABLE_REGION10 | OS_HAL_COREMPU_ENABLE_REGION11 | OS_HAL_COREMPU_ENABLE_REGION12 | OS_HAL_COREMPU_ENABLE_REGION13 | OS_HAL_COREMPU_ENABLE_REGION14,
  /* MpuDataRegionWriteEnablePS1       = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_DISABLE_REGION1 | OS_HAL_COREMPU_ENABLE_REGION2 | OS_HAL_COREMPU_DISABLE_REGION3 | OS_HAL_COREMPU_ENABLE_REGION4 | OS_HAL_COREMPU_DISABLE_REGION5 | OS_HAL_COREMPU_ENABLE_REGION6 | OS_HAL_COREMPU_ENABLE_REGION7 | OS_HAL_COREMPU_ENABLE_REGION8 | OS_HAL_COREMPU_ENABLE_REGION9 | OS_HAL_COREMPU_ENABLE_REGION10 | OS_HAL_COREMPU_ENABLE_REGION11 | OS_HAL_COREMPU_ENABLE_REGION12 | OS_HAL_COREMPU_ENABLE_REGION13 | OS_HAL_COREMPU_ENABLE_REGION14,
  /* MpuDataRegionWriteEnablePS2       = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_DISABLE_REGION1 | OS_HAL_COREMPU_ENABLE_REGION2 | OS_HAL_COREMPU_ENABLE_REGION3 | OS_HAL_COREMPU_ENABLE_REGION4 | OS_HAL_COREMPU_ENABLE_REGION5 | OS_HAL_COREMPU_DISABLE_REGION6 | OS_HAL_COREMPU_DISABLE_REGION7 | OS_HAL_COREMPU_DISABLE_REGION8 | OS_HAL_COREMPU_DISABLE_REGION9 | OS_HAL_COREMPU_DISABLE_REGION10 | OS_HAL_COREMPU_DISABLE_REGION11 | OS_HAL_COREMPU_DISABLE_REGION12 | OS_HAL_COREMPU_DISABLE_REGION13 | OS_HAL_COREMPU_DISABLE_REGION14,
  /* MpuDataRegionWriteEnablePS3       = */ OS_HAL_COREMPU_ENABLE_REGION0 | OS_HAL_COREMPU_DISABLE_REGION1 | OS_HAL_COREMPU_DISABLE_REGION2 | OS_HAL_COREMPU_DISABLE_REGION3 | OS_HAL_COREMPU_DISABLE_REGION4 | OS_HAL_COREMPU_DISABLE_REGION5 | OS_HAL_COREMPU_DISABLE_REGION6 | OS_HAL_COREMPU_DISABLE_REGION7 | OS_HAL_COREMPU_DISABLE_REGION8 | OS_HAL_COREMPU_DISABLE_REGION9 | OS_HAL_COREMPU_DISABLE_REGION10 | OS_HAL_COREMPU_DISABLE_REGION11 | OS_HAL_COREMPU_DISABLE_REGION12 | OS_HAL_COREMPU_DISABLE_REGION13 | OS_HAL_COREMPU_DISABLE_REGION14,
  },
  
  /* .MpuCodeRegionStartSlot           = */ 0,
  /* .MpuCodeRegionCount               = */ 1,
  /* .MpuCodeRegions                   = */ OsCfg_Hal_Mp_OsCore0_CodeRegions,
  {
  /* Execution right bit mask */
  /* MpuCodeRegionExecutionEnablePS0   = */ 0uL | OS_HAL_COREMPU_ENABLE_REGION0,
  /* MpuCodeRegionExecutionEnablePS1   = */ 0uL | OS_HAL_COREMPU_ENABLE_REGION0,
  /* MpuCodeRegionExecutionEnablePS2   = */ 0uL | OS_HAL_COREMPU_ENABLE_REGION0,
  /* MpuCodeRegionExecutionEnablePS3   = */ 0uL | OS_HAL_COREMPU_ENABLE_REGION0,
  }
};

/*! HAL memory protection configuration data: OsApplication_ASILB */
CONST(Os_Hal_MpAppConfigType, OS_CONST) OsCfg_Hal_Mp_OsApplication_ASILB =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: OsApplication_BSW_QM */
CONST(Os_Hal_MpAppConfigType, OS_CONST) OsCfg_Hal_Mp_OsApplication_BSW_QM =
{
  /* .ProtectionSet                   = */ 1,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: OsApplication_QM */
CONST(Os_Hal_MpAppConfigType, OS_CONST) OsCfg_Hal_Mp_OsApplication_QM =
{
  /* .ProtectionSet                   = */ 1,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: SystemApplication_OsCore0 */
CONST(Os_Hal_MpAppConfigType, OS_CONST) OsCfg_Hal_Mp_SystemApplication_OsCore0 =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: CanIsr_1 */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_CanIsr_1 =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: CanIsr_2 */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_CanIsr_2 =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: CounterIsr_SystemTimer */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_CounterIsr_SystemTimer =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: DMACH10SR_ISR */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_DMACH10SR_ISR =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: DMACH11SR_ISR */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_DMACH11SR_ISR =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: GTMTOM1SR0_ISR */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_GTMTOM1SR0_ISR =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: PLC_Interrupt */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_PLC_Interrupt =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: QSPI1ERR_ISR */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_QSPI1ERR_ISR =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: QSPI1PT_ISR */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_QSPI1PT_ISR =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: QSPI1UD_ISR */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_QSPI1UD_ISR =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: ASILB_MAIN_TASK */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_ASILB_MAIN_TASK =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: ASILB_init_task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_ASILB_init_task =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: Default_BSW_Async_QM_Task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_Default_BSW_Async_QM_Task =
{
  /* .ProtectionSet                   = */ 1,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: Default_BSW_Async_Task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_Default_BSW_Async_Task =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: Default_BSW_Sync_Task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_Default_BSW_Sync_Task =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: Default_Init_Task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_Default_Init_Task =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: Default_RTE_Mode_switch_Task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_Default_RTE_Mode_switch_Task =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: IdleTask_OsCore0 */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_IdleTask_OsCore0 =
{
  /* .ProtectionSet                   = */ 2,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: QM_MAIN_TASK */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_QM_MAIN_TASK =
{
  /* .ProtectionSet                   = */ 1,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

/*! HAL memory protection configuration data: QM_init_task */
CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_QM_init_task =
{
  /* .ProtectionSet                   = */ 1,
  
  /* .MpuDataRegionStartSlot          = */ 15,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 1,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL,
};

#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL memory protection configuration data: SystemMpu */
CONST(Os_Hal_MpSystemConfigType, OS_CONST) OsCfg_Hal_Mp_SystemMpu =
{
  /* .SysMpuId  = */ 0
};

/*! HAL memory protection configuration data: EmptyThread */

CONST(Os_Hal_MpThreadConfigType, OS_CONST) OsCfg_Hal_Mp_EmptyThread =
{
  /* .ProtectionSet                   = */ 0,
  
  /* .MpuDataRegionStartSlot          = */ 0,
  /* .MpuDataRegionCount              = */ 0,
  /* .MpuDataRegions                  = */ NULL_PTR,
  /* .MpuDataRegionEnableMask         = */ 0uL,
  /* .MpuDataRegionReadEnable         = */ 0uL,
  /* .MpuDataRegionWriteEnable        = */ 0uL,
  
  /* .MpuCodeRegionStartSlot          = */ 0,
  /* .MpuCodeRegionCount              = */ 0,
  /* .MpuCodeRegions                  = */ NULL_PTR,
  /* .MpuCodeRegionEnableMask         = */ 0uL,
  /* .MpuCodeRegionExecutionEnable    = */ 0uL
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Hal_MemoryProtection_Lcfg.c
 *********************************************************************************************************************/
