/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanIf
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanIf_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#define CANIF_LCFG_SOURCE

/**********************************************************************************************************************
  \file  Includes
**********************************************************************************************************************/
/** 
  \brief  Required external files.
*/ 

#include "CanIf_Cfg.h"

 /*  CanTp Header Files  */ 
#include "CanTp_Cfg.h"
#include "CanTp_Cbk.h"
 /*  PduR Header Files  */ 
#include "PduR_Cfg.h"
#include "PduR_CanIf.h"
 /*  Xcp Header Files  */ 
#include "CanXcp.h"
 /*  CanSM Header Files  */ 
#include "CanSM_Cbk.h"

#define CANIF_START_SEC_APPL_CODE

#include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */

/**********************************************************************************************************************
  \var  Prototypes of callback functions
**********************************************************************************************************************/

/** 
  \brief  Tx confirmation functions.
*/



/** 
  \brief  Rx indication functions.
*/





#define CANIF_STOP_SEC_APPL_CODE

#include "MemMap.h" /* PRQA S 5087 */  /* MD_MSR_MemMap */

/**********************************************************************************************************************
  ComStackLib
**********************************************************************************************************************/
/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanIf_BusOffNotificationFctPtr
**********************************************************************************************************************/
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_BusOffNotificationFctType, CANIF_CONST) CanIf_BusOffNotificationFctPtr = CanSM_ControllerBusOff;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_CanIfCtrlId2MappedTxBuffersConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_CanIfCtrlId2MappedTxBuffersConfig
  \brief  CAN controller configuration - mapped Tx-buffer(s).
  \details
  Element                          Description
  MappedTxBuffersConfigEndIdx      the end index of the 1:n relation pointing to CanIf_MappedTxBuffersConfig
  MappedTxBuffersConfigStartIdx    the start index of the 1:n relation pointing to CanIf_MappedTxBuffersConfig
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_CanIfCtrlId2MappedTxBuffersConfigType, CANIF_CONST) CanIf_CanIfCtrlId2MappedTxBuffersConfig[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MappedTxBuffersConfigEndIdx                                                                        MappedTxBuffersConfigStartIdx                                                                              Referable Keys */
  { /*     0 */                          1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,                            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */   },  /* [/ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     1 */                          2u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,                            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */ }   /* [/ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_CtrlModeIndicationFctPtr
**********************************************************************************************************************/
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_CtrlModeIndicationFctType, CANIF_CONST) CanIf_CtrlModeIndicationFctPtr = CanSM_ControllerModeIndication;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_MailBoxConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_MailBoxConfig
  \brief  Mailbox table.
  \details
  Element                 Description
  CtrlStatesIdx           the index of the 1:1 relation pointing to CanIf_CtrlStates
  PduIdFirst              "First" PDU mapped to mailbox.
  PduIdLast               "Last" PDU mapped to mailbox.
  TxBufferCfgIdx          the index of the 0:1 relation pointing to CanIf_TxBufferPrioByCanIdByteQueueConfig
  TxBufferHandlingType
  MailBoxType             Type of mailbox: Rx-/Tx- BasicCAN/FullCAN/unused.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_MailBoxConfigType, CANIF_CONST) CanIf_MailBoxConfig[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    CtrlStatesIdx                                                                        PduIdFirst                            PduIdLast                            TxBufferCfgIdx                                                                              TxBufferHandlingType                     MailBoxType                    Referable Keys */
  { /*     0 */            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,         0u  /* Unused, TxPduId 0 */ ,        0u  /* Unused, TxPduId 11 */,                                     0u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89 */, CANIF_TXBUFFER_HANDLINGTYPE_PRIOBYCANID, CANIF_TxBasicCANMailbox },  /* [/ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx] */
  { /*     1 */            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,         0u  /* RxPduId */           ,       21u  /* RxPduId  */          , CANIF_NO_TXBUFFERCFGIDXOFMAILBOXCONFIG  /* unusedIndex1 */                                , CANIF_TXBUFFER_HANDLINGTYPE_NONE       , CANIF_RxBasicCANMailbox },  /* [/ActiveEcuC/Can/CanConfigSet/CN_E_CAN_5c9dccb3_Rx] */
  { /*     2 */            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,         0u  /* Unused, TxPduId 12 */,        0u  /* Unused, TxPduId 18 */,                                     1u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7 */, CANIF_TXBUFFER_HANDLINGTYPE_PRIOBYCANID, CANIF_TxBasicCANMailbox },  /* [/ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx] */
  { /*     3 */            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,        22u  /* RxPduId */           ,       35u  /* RxPduId  */          , CANIF_NO_TXBUFFERCFGIDXOFMAILBOXCONFIG  /* unusedIndex3 */                                , CANIF_TXBUFFER_HANDLINGTYPE_NONE       , CANIF_RxBasicCANMailbox }   /* [/ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_d9652988_Rx] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_MappedTxBuffersConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_MappedTxBuffersConfig
  \brief  Mapped Tx-buffer(s)
  \details
  Element             Description
  MailBoxConfigIdx    the index of the 1:1 relation pointing to CanIf_MailBoxConfig
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_MappedTxBuffersConfigType, CANIF_CONST) CanIf_MappedTxBuffersConfig[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MailBoxConfigIdx                                                                   Referable Keys */
  { /*     0 */               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */   },  /* [/ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     1 */               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */ }   /* [/ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_RxIndicationFctList
**********************************************************************************************************************/
/** 
  \var    CanIf_RxIndicationFctList
  \brief  Rx indication functions table.
  \details
  Element               Description
  RxIndicationFct       Rx indication function.
  RxIndicationLayout    Layout of Rx indication function.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_RxIndicationFctListType, CANIF_CONST) CanIf_RxIndicationFctList[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxIndicationFct                                               RxIndicationLayout                                                                 Referable Keys */
  { /*     0 */  { (CanIf_SimpleRxIndicationFctType)NULL_PTR }              , CanIf_SimpleRxIndicationLayout    /* PRQA S 0313 */ /* MD_CanIf_Rule11.1 */ },  /* [NULL_PTR] */
  { /*     1 */  { (CanIf_SimpleRxIndicationFctType)CanTp_RxIndication }    , CanIf_AdvancedRxIndicationLayout  /* PRQA S 0313 */ /* MD_CanIf_Rule11.1 */ },  /* [CanTp_RxIndication] */
  { /*     2 */  { (CanIf_SimpleRxIndicationFctType)PduR_CanIfRxIndication }, CanIf_AdvancedRxIndicationLayout  /* PRQA S 0313 */ /* MD_CanIf_Rule11.1 */ },  /* [PduR_CanIfRxIndication] */
  { /*     3 */  { (CanIf_SimpleRxIndicationFctType)Xcp_CanIfRxIndication } , CanIf_AdvancedRxIndicationLayout  /* PRQA S 0313 */ /* MD_CanIf_Rule11.1 */ }   /* [Xcp_CanIfRxIndication] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_RxPduConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_RxPduConfig
  \brief  Rx-PDU configuration table.
  \details
  Element                   Description
  RxPduCanId                Rx-PDU: CAN identifier.
  RxPduMask                 Rx-PDU: CAN identifier mask.
  UpperPduId                PDU ID defined by upper layer.
  RxIndicationFctListIdx    the index of the 1:1 relation pointing to CanIf_RxIndicationFctList
  RxPduDlc                  Rx-PDU length (DLC).
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_RxPduConfigType, CANIF_CONST) CanIf_RxPduConfig[36] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxPduCanId                                                                    RxPduMask                                                    UpperPduId                                  RxIndicationFctListIdx                                RxPduDlc                                     Comment                                                                     Referable Keys */
  { /*     0 */    0x07FFu  /* UDS_ON_CAN_Tp_oE_CAN_3c36c858_Rx, 2.0- or FD-PDU */          ,   0x47FFu  /* UDS_ON_CAN_Tp_oE_CAN_3c36c858_Rx */          , CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5e51c2a7,                     1u  /* CanTp_RxIndication */    ,       0u  /* DLC-Check is disabled */ },  /* [PDU: UDS_ON_CAN_Tp_oE_CAN_3c36c858_Rx, CanId: 0x7ff]           */  /* [CanIfConf_CanIfRxPduCfg_UDS_ON_CAN_Tp_oE_CAN_3c36c858_Rx] */
  { /*     1 */    0x07F5u  /* ReqToOBC_oE_CAN_d5cb541a_Rx, 2.0- or FD-PDU */               ,   0x07FFu  /* ReqToOBC_oE_CAN_d5cb541a_Rx */               , PduRConf_PduRSrcPdu_PduRSrcPdu_7e41dab9   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: ReqToOBC_oE_CAN_d5cb541a_Rx, CanId: 0x7f5]                */  /* [CanIfConf_CanIfRxPduCfg_ReqToOBC_oE_CAN_d5cb541a_Rx] */
  { /*     2 */    0x07DFu  /* ReqToECANFunction_oE_CAN_2b18c310_Rx, 2.0- or FD-PDU */      ,   0x07FFu  /* ReqToECANFunction_oE_CAN_2b18c310_Rx */      , PduRConf_PduRSrcPdu_PduRSrcPdu_b756064f   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: ReqToECANFunction_oE_CAN_2b18c310_Rx, CanId: 0x7df]       */  /* [CanIfConf_CanIfRxPduCfg_ReqToECANFunction_oE_CAN_2b18c310_Rx] */
  { /*     3 */    0x0590u  /* REQ_DIAG_ON_CAN_Tp_oE_CAN_1df6e3bd_Rx, 2.0- or FD-PDU */     ,   0x07FFu  /* REQ_DIAG_ON_CAN_Tp_oE_CAN_1df6e3bd_Rx */     , CanTpConf_CanTpRxNPdu_CanTpRxNPdu_721e246d,                     1u  /* CanTp_RxIndication */    ,       0u  /* DLC-Check is disabled */ },  /* [PDU: REQ_DIAG_ON_CAN_Tp_oE_CAN_1df6e3bd_Rx, CanId: 0x590]      */  /* [CanIfConf_CanIfRxPduCfg_REQ_DIAG_ON_CAN_Tp_oE_CAN_1df6e3bd_Rx] */
  { /*     4 */    0x058Fu  /* XCP_REQ_oE_CAN_b6f2016d_Rx, 2.0- or FD-PDU */                ,   0x07FFu  /* XCP_REQ_oE_CAN_b6f2016d_Rx */                , XcpConf_XcpRxPdu_XcpRxPdu                 ,                     3u  /* Xcp_CanIfRxIndication */ ,       0u  /* DLC-Check is disabled */ },  /* [PDU: XCP_REQ_oE_CAN_b6f2016d_Rx, CanId: 0x58f]                 */  /* [CanIfConf_CanIfRxPduCfg_XCP_REQ_oE_CAN_b6f2016d_Rx] */
  { /*     5 */    0x055Fu  /* NEW_JDD_oE_CAN_40710377_Rx, 2.0- or FD-PDU */                ,   0x07FFu  /* NEW_JDD_oE_CAN_40710377_Rx */                , PduRConf_PduRSrcPdu_PduRSrcPdu_a0d1622f   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: NEW_JDD_oE_CAN_40710377_Rx, CanId: 0x55f]                 */  /* [CanIfConf_CanIfRxPduCfg_NEW_JDD_oE_CAN_40710377_Rx] */
  { /*     6 */    0x0552u  /* VCU_oE_CAN_15dba1c9_Rx, 2.0- or FD-PDU */                    ,   0x07FFu  /* VCU_oE_CAN_15dba1c9_Rx */                    , PduRConf_PduRSrcPdu_PduRSrcPdu_b485c90c   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: VCU_oE_CAN_15dba1c9_Rx, CanId: 0x552]                     */  /* [CanIfConf_CanIfRxPduCfg_VCU_oE_CAN_15dba1c9_Rx] */
  { /*     7 */    0x0486u  /* VCU3_oE_CAN_d93b8aeb_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* VCU3_oE_CAN_d93b8aeb_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_2def7208   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: VCU3_oE_CAN_d93b8aeb_Rx, CanId: 0x486]                    */  /* [CanIfConf_CanIfRxPduCfg_VCU3_oE_CAN_d93b8aeb_Rx] */
  { /*     8 */    0x0382u  /* BSIInfo_oE_CAN_6c029f64_Rx, 2.0- or FD-PDU */                ,   0x07FFu  /* BSIInfo_oE_CAN_6c029f64_Rx */                , PduRConf_PduRSrcPdu_PduRSrcPdu_882b1898   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BSIInfo_oE_CAN_6c029f64_Rx, CanId: 0x382]                 */  /* [CanIfConf_CanIfRxPduCfg_BSIInfo_oE_CAN_6c029f64_Rx] */
  { /*     9 */    0x037Eu  /* VCU_TU_oE_CAN_3e6bd3ba_Rx, 2.0- or FD-PDU */                 ,   0x07FFu  /* VCU_TU_oE_CAN_3e6bd3ba_Rx */                 , PduRConf_PduRSrcPdu_PduRSrcPdu_141e34ce   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: VCU_TU_oE_CAN_3e6bd3ba_Rx, CanId: 0x37e]                  */  /* [CanIfConf_CanIfRxPduCfg_VCU_TU_oE_CAN_3e6bd3ba_Rx] */
  { /*    10 */    0x0372u  /* CtrlDCDC_oE_CAN_ecc1a464_Rx, 2.0- or FD-PDU */               ,   0x07FFu  /* CtrlDCDC_oE_CAN_ecc1a464_Rx */               , PduRConf_PduRSrcPdu_PduRSrcPdu_2ed93c0e   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: CtrlDCDC_oE_CAN_ecc1a464_Rx, CanId: 0x372]                */  /* [CanIfConf_CanIfRxPduCfg_CtrlDCDC_oE_CAN_ecc1a464_Rx] */
  { /*    11 */    0x0361u  /* BMS6_oE_CAN_cd5ebb49_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* BMS6_oE_CAN_cd5ebb49_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_e104bf72   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BMS6_oE_CAN_cd5ebb49_Rx, CanId: 0x361]                    */  /* [CanIfConf_CanIfRxPduCfg_BMS6_oE_CAN_cd5ebb49_Rx] */
  { /*    12 */    0x0359u  /* BMS5_oE_CAN_30b7910d_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* BMS5_oE_CAN_30b7910d_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_5b82c527   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BMS5_oE_CAN_30b7910d_Rx, CanId: 0x359]                    */  /* [CanIfConf_CanIfRxPduCfg_BMS5_oE_CAN_30b7910d_Rx] */
  { /*    13 */    0x031Eu  /* ParkCommand_oE_CAN_903dc1d2_Rx, 2.0- or FD-PDU */            ,   0x07FFu  /* ParkCommand_oE_CAN_903dc1d2_Rx */            , PduRConf_PduRSrcPdu_PduRSrcPdu_50e0f311   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: ParkCommand_oE_CAN_903dc1d2_Rx, CanId: 0x31e]             */  /* [CanIfConf_CanIfRxPduCfg_ParkCommand_oE_CAN_903dc1d2_Rx] */
  { /*    14 */    0x031Bu  /* BMS8_oE_CAN_9386d09c_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* BMS8_oE_CAN_9386d09c_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_b865118f   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BMS8_oE_CAN_9386d09c_Rx, CanId: 0x31b]                    */  /* [CanIfConf_CanIfRxPduCfg_BMS8_oE_CAN_9386d09c_Rx] */
  { /*    15 */    0x027Au  /* VCU_BSI_Wakeup_oE_CAN_3482054c_Rx, 2.0- or FD-PDU */         ,   0x07FFu  /* VCU_BSI_Wakeup_oE_CAN_3482054c_Rx */         , PduRConf_PduRSrcPdu_PduRSrcPdu_771f59ac   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: VCU_BSI_Wakeup_oE_CAN_3482054c_Rx, CanId: 0x27a]          */  /* [CanIfConf_CanIfRxPduCfg_VCU_BSI_Wakeup_oE_CAN_3482054c_Rx] */
  { /*    16 */    0x017Bu  /* VCU_PCANInfo_oE_CAN_30d7a694_Rx, 2.0- or FD-PDU */           ,   0x07FFu  /* VCU_PCANInfo_oE_CAN_30d7a694_Rx */           , PduRConf_PduRSrcPdu_PduRSrcPdu_c80fbc21   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: VCU_PCANInfo_oE_CAN_30d7a694_Rx, CanId: 0x17b]            */  /* [CanIfConf_CanIfRxPduCfg_VCU_PCANInfo_oE_CAN_30d7a694_Rx] */
  { /*    17 */    0x0129u  /* BMS9_oE_CAN_71f1349f_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* BMS9_oE_CAN_71f1349f_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_3c9c33f4   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BMS9_oE_CAN_71f1349f_Rx, CanId: 0x129]                    */  /* [CanIfConf_CanIfRxPduCfg_BMS9_oE_CAN_71f1349f_Rx] */
  { /*    18 */    0x0127u  /* BMS3_oE_CAN_1014c3c4_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* BMS3_oE_CAN_1014c3c4_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_d8341745   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BMS3_oE_CAN_1014c3c4_Rx, CanId: 0x127]                    */  /* [CanIfConf_CanIfRxPduCfg_BMS3_oE_CAN_1014c3c4_Rx] */
  { /*    19 */    0x0125u  /* BMS1_oE_CAN_0f8a0d83_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* BMS1_oE_CAN_0f8a0d83_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_ce686a11   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: BMS1_oE_CAN_0f8a0d83_Rx, CanId: 0x125]                    */  /* [CanIfConf_CanIfRxPduCfg_BMS1_oE_CAN_0f8a0d83_Rx] */
  { /*    20 */    0x00F0u  /* VCU2_oE_CAN_3b4c6ee8_Rx, 2.0- or FD-PDU */                   ,   0x07FFu  /* VCU2_oE_CAN_3b4c6ee8_Rx */                   , PduRConf_PduRSrcPdu_PduRSrcPdu_c61797dd   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: VCU2_oE_CAN_3b4c6ee8_Rx, CanId: 0xf0]                     */  /* [CanIfConf_CanIfRxPduCfg_VCU2_oE_CAN_3b4c6ee8_Rx] */
  { /*    21 */    0x0092u  /* ELECTRON_BSI_oE_CAN_59001af6_Rx, 2.0- or FD-PDU */           ,   0x07FFu  /* ELECTRON_BSI_oE_CAN_59001af6_Rx */           , PduRConf_PduRSrcPdu_PduRSrcPdu_182f4c9b   ,                     2u  /* PduR_CanIfRxIndication */,       0u  /* DLC-Check is disabled */ },  /* [PDU: ELECTRON_BSI_oE_CAN_59001af6_Rx, CanId: 0x92]             */  /* [CanIfConf_CanIfRxPduCfg_ELECTRON_BSI_oE_CAN_59001af6_Rx] */
  { /*    22 */    0x04FAu  /* DCHV_Status2_oInt_CAN_e85b8480_Rx, 2.0- or FD-PDU */         ,   0x47FFu  /* DCHV_Status2_oInt_CAN_e85b8480_Rx */         , PduRConf_PduRSrcPdu_PduRSrcPdu_9f59032e   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCHV_Status2_oInt_CAN_e85b8480_Rx, CanId: 0x4fa]          */  /* [CanIfConf_CanIfRxPduCfg_DCHV_Status2_oInt_CAN_e85b8480_Rx] */
  { /*    23 */    0x04E5u  /* DCHV_Status1_oInt_CAN_3780e05e_Rx, 2.0- or FD-PDU */         ,   0x07FFu  /* DCHV_Status1_oInt_CAN_3780e05e_Rx */         , PduRConf_PduRSrcPdu_PduRSrcPdu_91da6bff   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCHV_Status1_oInt_CAN_3780e05e_Rx, CanId: 0x4e5]          */  /* [CanIfConf_CanIfRxPduCfg_DCHV_Status1_oInt_CAN_3780e05e_Rx] */
  { /*    24 */    0x04C7u  /* DCHV_Fault_oInt_CAN_dd65bb1a_Rx, 2.0- or FD-PDU */           ,   0x07FFu  /* DCHV_Fault_oInt_CAN_dd65bb1a_Rx */           , PduRConf_PduRSrcPdu_PduRSrcPdu_aaa6ebec   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCHV_Fault_oInt_CAN_dd65bb1a_Rx, CanId: 0x4c7]            */  /* [CanIfConf_CanIfRxPduCfg_DCHV_Fault_oInt_CAN_dd65bb1a_Rx] */
  { /*    25 */    0x0499u  /* DCLV_Status3_oInt_CAN_c9adb120_Rx, 2.0- or FD-PDU */         ,   0x07FFu  /* DCLV_Status3_oInt_CAN_c9adb120_Rx */         , PduRConf_PduRSrcPdu_PduRSrcPdu_6f865e62   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCLV_Status3_oInt_CAN_c9adb120_Rx, CanId: 0x499]          */  /* [CanIfConf_CanIfRxPduCfg_DCLV_Status3_oInt_CAN_c9adb120_Rx] */
  { /*    26 */    0x0498u  /* DCLV_Status2_oInt_CAN_831b6d6a_Rx, 2.0- or FD-PDU */         ,   0x07FFu  /* DCLV_Status2_oInt_CAN_831b6d6a_Rx */         , PduRConf_PduRSrcPdu_PduRSrcPdu_820710d6   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCLV_Status2_oInt_CAN_831b6d6a_Rx, CanId: 0x498]          */  /* [CanIfConf_CanIfRxPduCfg_DCLV_Status2_oInt_CAN_831b6d6a_Rx] */
  { /*    27 */    0x0497u  /* DCLV_Status1_oInt_CAN_5cc009b4_Rx, 2.0- or FD-PDU */         ,   0x07FFu  /* DCLV_Status1_oInt_CAN_5cc009b4_Rx */         , PduRConf_PduRSrcPdu_PduRSrcPdu_f6508c17   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCLV_Status1_oInt_CAN_5cc009b4_Rx, CanId: 0x497]          */  /* [CanIfConf_CanIfRxPduCfg_DCLV_Status1_oInt_CAN_5cc009b4_Rx] */
  { /*    28 */    0x0496u  /* DCLV_Fault_oInt_CAN_793c72a5_Rx, 2.0- or FD-PDU */           ,   0x07FFu  /* DCLV_Fault_oInt_CAN_793c72a5_Rx */           , PduRConf_PduRSrcPdu_PduRSrcPdu_3259f79e   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: DCLV_Fault_oInt_CAN_793c72a5_Rx, CanId: 0x496]            */  /* [CanIfConf_CanIfRxPduCfg_DCLV_Fault_oInt_CAN_793c72a5_Rx] */
  { /*    29 */    0x0488u  /* PFC_Status5_oInt_CAN_df906b35_Rx, 2.0- or FD-PDU */          ,   0x07FFu  /* PFC_Status5_oInt_CAN_df906b35_Rx */          , PduRConf_PduRSrcPdu_PduRSrcPdu_fa5b1cce   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: PFC_Status5_oInt_CAN_df906b35_Rx, CanId: 0x488]           */  /* [CanIfConf_CanIfRxPduCfg_PFC_Status5_oInt_CAN_df906b35_Rx] */
  { /*    30 */    0x0477u  /* PFC_Status4_oInt_CAN_c1ee50c6_Rx, 2.0- or FD-PDU */          ,   0x07FFu  /* PFC_Status4_oInt_CAN_c1ee50c6_Rx */          , PduRConf_PduRSrcPdu_PduRSrcPdu_8463d87d   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: PFC_Status4_oInt_CAN_c1ee50c6_Rx, CanId: 0x477]           */  /* [CanIfConf_CanIfRxPduCfg_PFC_Status4_oInt_CAN_c1ee50c6_Rx] */
  { /*    31 */    0x0466u  /* PFC_Status3_oInt_CAN_9a94f31f_Rx, 2.0- or FD-PDU */          ,   0x07FFu  /* PFC_Status3_oInt_CAN_9a94f31f_Rx */          , PduRConf_PduRSrcPdu_PduRSrcPdu_97e42112   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: PFC_Status3_oInt_CAN_9a94f31f_Rx, CanId: 0x466]           */  /* [CanIfConf_CanIfRxPduCfg_PFC_Status3_oInt_CAN_9a94f31f_Rx] */
  { /*    32 */    0x0455u  /* PFC_Status2_oInt_CAN_84eac8ec_Rx, 2.0- or FD-PDU */          ,   0x07FFu  /* PFC_Status2_oInt_CAN_84eac8ec_Rx */          , PduRConf_PduRSrcPdu_PduRSrcPdu_13c131e4   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: PFC_Status2_oInt_CAN_84eac8ec_Rx, CanId: 0x455]           */  /* [CanIfConf_CanIfRxPduCfg_PFC_Status2_oInt_CAN_84eac8ec_Rx] */
  { /*    33 */    0x0444u  /* PFC_Status1_oInt_CAN_a66884f9_Rx, 2.0- or FD-PDU */          ,   0x07FFu  /* PFC_Status1_oInt_CAN_a66884f9_Rx */          , PduRConf_PduRSrcPdu_PduRSrcPdu_4db707a4   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: PFC_Status1_oInt_CAN_a66884f9_Rx, CanId: 0x444]           */  /* [CanIfConf_CanIfRxPduCfg_PFC_Status1_oInt_CAN_a66884f9_Rx] */
  { /*    34 */    0x0420u  /* PFC_Fault_oInt_CAN_11b060ea_Rx, 2.0- or FD-PDU */            ,   0x07FFu  /* PFC_Fault_oInt_CAN_11b060ea_Rx */            , PduRConf_PduRSrcPdu_PduRSrcPdu_3c5c1cb6   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  },  /* [PDU: PFC_Fault_oInt_CAN_11b060ea_Rx, CanId: 0x420]             */  /* [CanIfConf_CanIfRxPduCfg_PFC_Fault_oInt_CAN_11b060ea_Rx] */
  { /*    35 */    0x0400u  /* ProgTool_SupEnterBoot_oInt_CAN_67b8d1eb_Rx, 2.0- or FD-PDU */,   0x07FFu  /* ProgTool_SupEnterBoot_oInt_CAN_67b8d1eb_Rx */, PduRConf_PduRSrcPdu_PduRSrcPdu_9b0aaf50   ,                     2u  /* PduR_CanIfRxIndication */,       8u  /* DLC-Check is enabled */  }   /* [PDU: ProgTool_SupEnterBoot_oInt_CAN_67b8d1eb_Rx, CanId: 0x400] */  /* [CanIfConf_CanIfRxPduCfg_ProgTool_SupEnterBoot_oInt_CAN_67b8d1eb_Rx] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TrcvModeIndicationFctPtr
**********************************************************************************************************************/
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TrcvModeIndicationFctType, CANIF_CONST) CanIf_TrcvModeIndicationFctPtr = CanSM_TransceiverModeIndication;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TrcvToCtrlMap
**********************************************************************************************************************/
/** 
  \var    CanIf_TrcvToCtrlMap
  \brief  Indirection table: logical transceiver index to CAN controller index.
*/ 
#define CANIF_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TrcvToCtrlMapType, CANIF_CONST) CanIf_TrcvToCtrlMap[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     TrcvToCtrlMap                                          */
  /*     0 */             0u  /* CAN controller handle ID (upper) */
};
#define CANIF_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxBufferPrioByCanIdByteQueueConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_TxBufferPrioByCanIdByteQueueConfig
  \brief  Tx-buffer: PRIO_BY_CANID as BYTE_QUEUE
  \details
  Element                                             Description
  TxBufferPrioByCanIdBaseIdx                          the index of the 1:1 relation pointing to CanIf_TxBufferPrioByCanIdBase
  TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdx      the end index of the 1:n relation pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
  TxBufferPrioByCanIdByteQueueMappedTxPdusLength      the number of relations pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
  TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdx    the start index of the 1:n relation pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TxBufferPrioByCanIdByteQueueConfigType, CANIF_CONST) CanIf_TxBufferPrioByCanIdByteQueueConfig[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxBufferPrioByCanIdBaseIdx                                                      TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdx                                                      TxBufferPrioByCanIdByteQueueMappedTxPdusLength                                                      TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdx                                                            Referable Keys */
  { /*     0 */                         0u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89 */,                                            12u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89 */,                                            12u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89 */,                                               0u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89 */ },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     1 */                         1u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7 */,                                            19u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7 */,                                             7u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7 */,                                              12u  /* /ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7 */ }   /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
**********************************************************************************************************************/
/** 
  \var    CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
  \brief  Tx-buffer: PRIO_BY_CANID as BYTE_QUEUE: Mapped Tx-PDUs
  \details
  Element           Description
  TxPduConfigIdx    the index of the 1:1 relation pointing to CanIf_TxPduConfig
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusType, CANIF_CONST) CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus[19] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxPduConfigIdx                                                                                     Referable Keys */
  { /*     0 */             0u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBCTxDiag_oE_CAN_d6e7f536_Tx */           },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     1 */             1u  /* /ActiveEcuC/CanIf/CanIfInitCfg/NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx */    },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     2 */             2u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx */    },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     3 */             3u  /* /ActiveEcuC/CanIf/CanIfInitCfg/XCP_RES_oE_CAN_c5ee7713_Tx */             },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     4 */             4u  /* /ActiveEcuC/CanIf/CanIfInitCfg/REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx */  },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     5 */             5u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC4_oE_CAN_1436d77f_Tx */                },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     6 */             6u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC1_oE_CAN_c97caff2_Tx */                },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     7 */             7u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC2_oE_CAN_349585b6_Tx */                },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     8 */             8u  /* /ActiveEcuC/CanIf/CanIfInitCfg/DC1_oE_CAN_0bfdcb0d_Tx */                 },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*     9 */             9u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC3_oE_CAN_d6e261b5_Tx */                },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*    10 */            10u  /* /ActiveEcuC/CanIf/CanIfInitCfg/VERS_OBC_DCDC_oE_CAN_86d6c928_Tx */       },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*    11 */            11u  /* /ActiveEcuC/CanIf/CanIfInitCfg/DC2_oE_CAN_1927a4ae_Tx */                 },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  { /*    12 */            12u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug4_oInt_CAN_256e892d_Tx */            },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
  { /*    13 */            13u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug3_oInt_CAN_6a448e86_Tx */            },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
  { /*    14 */            14u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug2_oInt_CAN_bc7a3afe_Tx */            },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
  { /*    15 */            15u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug1_oInt_CAN_1d48e037_Tx */            },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
  { /*    16 */            16u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx */ },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
  { /*    17 */            17u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx */ },  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
  { /*    18 */            18u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx */  }   /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxConfirmationFctList
**********************************************************************************************************************/
/** 
  \var    CanIf_TxConfirmationFctList
  \brief  Tx confirmation functions table.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TxConfirmationFctType, CANIF_CONST) CanIf_TxConfirmationFctList[4] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     TxConfirmationFctList                      Referable Keys */
  /*     0 */ (CanIf_TxConfirmationFctType)NULL_PTR ,  /* [NULL_PTR] */
  /*     1 */ CanTp_TxConfirmation                  ,  /* [CanTp_TxConfirmation] */
  /*     2 */ PduR_CanIfTxConfirmation              ,  /* [PduR_CanIfTxConfirmation] */
  /*     3 */ Xcp_CanIfTxConfirmation                  /* [Xcp_CanIfTxConfirmation] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxPduConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_TxPduConfig
  \brief  Tx-PDUs - configuration.
  \details
  Element                     Description
  CanId                       CAN identifier (16bit / 32bit).
  UpperLayerTxPduId           Upper layer handle ID (8bit / 16bit).
  IsTxPduTruncation           TRUE: Truncation of Tx-PDU is enabled, FALSE: Truncation of Tx-PDU is disabled
  CtrlStatesIdx               the index of the 1:1 relation pointing to CanIf_CtrlStates
  MailBoxConfigIdx            the index of the 1:1 relation pointing to CanIf_MailBoxConfig
  TxConfirmationFctListIdx    the index of the 1:1 relation pointing to CanIf_TxConfirmationFctList
  TxPduLength                 Tx-PDU length.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TxPduConfigType, CANIF_CONST) CanIf_TxPduConfig[19] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    CanId    UpperLayerTxPduId                                            IsTxPduTruncation  CtrlStatesIdx                                                                        MailBoxConfigIdx                                                             TxConfirmationFctListIdx                                  TxPduLength        Comment                                                   Referable Keys */
  { /*     0 */ 0x07FDu, PduRConf_PduRDestPdu_OBCTxDiag_oE_CAN_794ae2be_Tx          , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: OBCTxDiag_oE_CAN_d6e7f536_Tx]           */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBCTxDiag_oE_CAN_d6e7f536_Tx] */
  { /*     1 */ 0x05B1u, PduRConf_PduRDestPdu_NEW_JDD_OBC_DCDC_oE_CAN_83c34e71_Tx   , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx]    */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx] */
  { /*     2 */ 0x0591u, PduRConf_PduRDestPdu_SUPV_V2_OBC_DCDC_oE_CAN_50adbf83_Tx   , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx]    */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx] */
  { /*     3 */ 0x0590u, XcpConf_XcpTxPdu_XcpTxPdu                                  , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       3u  /* Xcp_CanIfTxConfirmation */ ,          8u },  /* [PDU: XCP_RES_oE_CAN_c5ee7713_Tx]             */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/XCP_RES_oE_CAN_c5ee7713_Tx] */
  { /*     4 */ 0x058Fu, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_721e246d             , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       1u  /* CanTp_TxConfirmation */    ,          8u },  /* [PDU: REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx]  */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx] */
  { /*     5 */ 0x0439u, PduRConf_PduRDestPdu_OBC4_oE_CAN_e2c5a75c_Tx               , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: OBC4_oE_CAN_1436d77f_Tx]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC4_oE_CAN_1436d77f_Tx] */
  { /*     6 */ 0x03A3u, PduRConf_PduRDestPdu_OBC1_oE_CAN_79eaec1b_Tx               , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: OBC1_oE_CAN_c97caff2_Tx]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC1_oE_CAN_c97caff2_Tx] */
  { /*     7 */ 0x03A2u, PduRConf_PduRDestPdu_OBC2_oE_CAN_0f0fd526_Tx               , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: OBC2_oE_CAN_349585b6_Tx]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC2_oE_CAN_349585b6_Tx] */
  { /*     8 */ 0x0345u, PduRConf_PduRDestPdu_DC1_oE_CAN_f87e7579_Tx                , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: DC1_oE_CAN_0bfdcb0d_Tx]                 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/DC1_oE_CAN_0bfdcb0d_Tx] */
  { /*     9 */ 0x0230u, PduRConf_PduRDestPdu_OBC3_oE_CAN_947c3ff2_Tx               , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: OBC3_oE_CAN_d6e261b5_Tx]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC3_oE_CAN_d6e261b5_Tx] */
  { /*    10 */ 0x00CEu, PduRConf_PduRDestPdu_VERS_OBC_DCDC_oE_CAN_336be5e5_Tx      , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: VERS_OBC_DCDC_oE_CAN_86d6c928_Tx]       */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/VERS_OBC_DCDC_oE_CAN_86d6c928_Tx] */
  { /*    11 */ 0x00C5u, PduRConf_PduRDestPdu_DC2_oE_CAN_8e9b4c44_Tx                , TRUE             ,            0u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2 */  ,               0u  /* /ActiveEcuC/Can/CanConfigSet/CN_E_CAN_7f812c72_Tx */  ,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: DC2_oE_CAN_1927a4ae_Tx]                 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/DC2_oE_CAN_1927a4ae_Tx] */
  { /*    12 */ 0x0503u, PduRConf_PduRDestPdu_Debug4_oInt_CAN_5edfb5ff_Tx           , TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       0u  /* NULL_PTR */                ,          8u },  /* [PDU: Debug4_oInt_CAN_256e892d_Tx]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug4_oInt_CAN_256e892d_Tx] */
  { /*    13 */ 0x0502u, PduRConf_PduRDestPdu_Debug3_oInt_CAN_8c0af379_Tx           , TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       0u  /* NULL_PTR */                ,          8u },  /* [PDU: Debug3_oInt_CAN_6a448e86_Tx]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug3_oInt_CAN_6a448e86_Tx] */
  { /*    14 */ 0x0501u, PduRConf_PduRDestPdu_Debug2_oInt_CAN_e286e838_Tx           , TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       0u  /* NULL_PTR */                ,          8u },  /* [PDU: Debug2_oInt_CAN_bc7a3afe_Tx]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug2_oInt_CAN_bc7a3afe_Tx] */
  { /*    15 */ 0x0500u, PduRConf_PduRDestPdu_Debug1_oInt_CAN_5112c5fb_Tx           , TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       0u  /* NULL_PTR */                ,          8u },  /* [PDU: Debug1_oInt_CAN_1d48e037_Tx]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug1_oInt_CAN_1d48e037_Tx] */
  { /*    16 */ 0x04A3u, PduRConf_PduRDestPdu_SUP_CommandToDCHV_oInt_CAN_4ab042a3_Tx, TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx] */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx] */
  { /*    17 */ 0x0495u, PduRConf_PduRDestPdu_SUP_CommandToDCLV_oInt_CAN_4c67b6d7_Tx, TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       2u  /* PduR_CanIfTxConfirmation */,          8u },  /* [PDU: SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx] */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx] */
  { /*    18 */ 0x0402u, PduRConf_PduRDestPdu_SUP_CommandToPFC_oInt_CAN_11d75aca_Tx , TRUE             ,            1u  /* /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0 */,               2u  /* /ActiveEcuC/Can/CanConfigSet/CN_Int_CAN_b597612f_Tx */,                       2u  /* PduR_CanIfTxConfirmation */,          8u }   /* [PDU: SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx]  */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxPduQueueIndex
**********************************************************************************************************************/
/** 
  \var    CanIf_TxPduQueueIndex
  \brief  Indirection table: Tx-PDU handle ID to corresponding Tx buffer handle ID. NOTE: Only BasicCAN Tx-PDUs have a valid indirection into the Tx buffer.
  \details
  Element       Description
  TxQueueIdx    the index of the 0:1 relation pointing to CanIf_TxQueue
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_TxPduQueueIndexType, CANIF_CONST) CanIf_TxPduQueueIndex[19] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxQueueIdx                                                                                     Comment                                                                             Referable Keys */
  { /*     0 */         0u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBCTxDiag_oE_CAN_d6e7f536_Tx */           },  /* [OBCTxDiag_oE_CAN_d6e7f536_Tx, BasicCAN TxPdu with Tx-buffer]           */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBCTxDiag_oE_CAN_d6e7f536_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     1 */         1u  /* /ActiveEcuC/CanIf/CanIfInitCfg/NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx */    },  /* [NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx, BasicCAN TxPdu with Tx-buffer]    */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     2 */         2u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx */    },  /* [SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx, BasicCAN TxPdu with Tx-buffer]    */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     3 */         3u  /* /ActiveEcuC/CanIf/CanIfInitCfg/XCP_RES_oE_CAN_c5ee7713_Tx */             },  /* [XCP_RES_oE_CAN_c5ee7713_Tx, BasicCAN TxPdu with Tx-buffer]             */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/XCP_RES_oE_CAN_c5ee7713_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     4 */         4u  /* /ActiveEcuC/CanIf/CanIfInitCfg/REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx */  },  /* [REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx, BasicCAN TxPdu with Tx-buffer]  */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     5 */         5u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC4_oE_CAN_1436d77f_Tx */                },  /* [OBC4_oE_CAN_1436d77f_Tx, BasicCAN TxPdu with Tx-buffer]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC4_oE_CAN_1436d77f_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     6 */         6u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC1_oE_CAN_c97caff2_Tx */                },  /* [OBC1_oE_CAN_c97caff2_Tx, BasicCAN TxPdu with Tx-buffer]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC1_oE_CAN_c97caff2_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     7 */         7u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC2_oE_CAN_349585b6_Tx */                },  /* [OBC2_oE_CAN_349585b6_Tx, BasicCAN TxPdu with Tx-buffer]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC2_oE_CAN_349585b6_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     8 */         8u  /* /ActiveEcuC/CanIf/CanIfInitCfg/DC1_oE_CAN_0bfdcb0d_Tx */                 },  /* [DC1_oE_CAN_0bfdcb0d_Tx, BasicCAN TxPdu with Tx-buffer]                 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/DC1_oE_CAN_0bfdcb0d_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*     9 */         9u  /* /ActiveEcuC/CanIf/CanIfInitCfg/OBC3_oE_CAN_d6e261b5_Tx */                },  /* [OBC3_oE_CAN_d6e261b5_Tx, BasicCAN TxPdu with Tx-buffer]                */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC3_oE_CAN_d6e261b5_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*    10 */        10u  /* /ActiveEcuC/CanIf/CanIfInitCfg/VERS_OBC_DCDC_oE_CAN_86d6c928_Tx */       },  /* [VERS_OBC_DCDC_oE_CAN_86d6c928_Tx, BasicCAN TxPdu with Tx-buffer]       */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/VERS_OBC_DCDC_oE_CAN_86d6c928_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*    11 */        11u  /* /ActiveEcuC/CanIf/CanIfInitCfg/DC2_oE_CAN_1927a4ae_Tx */                 },  /* [DC2_oE_CAN_1927a4ae_Tx, BasicCAN TxPdu with Tx-buffer]                 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/DC2_oE_CAN_1927a4ae_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  { /*    12 */        12u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug4_oInt_CAN_256e892d_Tx */            },  /* [Debug4_oInt_CAN_256e892d_Tx, BasicCAN TxPdu with Tx-buffer]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug4_oInt_CAN_256e892d_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
  { /*    13 */        13u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug3_oInt_CAN_6a448e86_Tx */            },  /* [Debug3_oInt_CAN_6a448e86_Tx, BasicCAN TxPdu with Tx-buffer]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug3_oInt_CAN_6a448e86_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
  { /*    14 */        14u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug2_oInt_CAN_bc7a3afe_Tx */            },  /* [Debug2_oInt_CAN_bc7a3afe_Tx, BasicCAN TxPdu with Tx-buffer]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug2_oInt_CAN_bc7a3afe_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
  { /*    15 */        15u  /* /ActiveEcuC/CanIf/CanIfInitCfg/Debug1_oInt_CAN_1d48e037_Tx */            },  /* [Debug1_oInt_CAN_1d48e037_Tx, BasicCAN TxPdu with Tx-buffer]            */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug1_oInt_CAN_1d48e037_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
  { /*    16 */        16u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx */ },  /* [SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx, BasicCAN TxPdu with Tx-buffer] */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
  { /*    17 */        17u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx */ },  /* [SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx, BasicCAN TxPdu with Tx-buffer] */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
  { /*    18 */        18u  /* /ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx */  }   /* [SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx, BasicCAN TxPdu with Tx-buffer]  */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx, /ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */
};
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_UlTxPduId2InternalTxPduId
**********************************************************************************************************************/
/** 
  \var    CanIf_UlTxPduId2InternalTxPduId
  \brief  Indirection table - Upper layer Tx-PduId to internal one.
*/ 
#define CANIF_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanIf_UlTxPduId2InternalTxPduIdType, CANIF_CONST) CanIf_UlTxPduId2InternalTxPduId[19] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     UlTxPduId2InternalTxPduId                                                                                            */
  /*     0 */                         8u  /* CanIfConf_CanIfTxPduCfg_DC1_oE_CAN_0bfdcb0d_Tx, CanId:0x00000345uL */                ,
  /*     1 */                        11u  /* CanIfConf_CanIfTxPduCfg_DC2_oE_CAN_1927a4ae_Tx, CanId:0x000000C5uL */                ,
  /*     2 */                        15u  /* CanIfConf_CanIfTxPduCfg_Debug1_oInt_CAN_1d48e037_Tx, CanId:0x00000500uL */           ,
  /*     3 */                        14u  /* CanIfConf_CanIfTxPduCfg_Debug2_oInt_CAN_bc7a3afe_Tx, CanId:0x00000501uL */           ,
  /*     4 */                        13u  /* CanIfConf_CanIfTxPduCfg_Debug3_oInt_CAN_6a448e86_Tx, CanId:0x00000502uL */           ,
  /*     5 */                        12u  /* CanIfConf_CanIfTxPduCfg_Debug4_oInt_CAN_256e892d_Tx, CanId:0x00000503uL */           ,
  /*     6 */                         1u  /* CanIfConf_CanIfTxPduCfg_NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx, CanId:0x000005B1uL */   ,
  /*     7 */                         6u  /* CanIfConf_CanIfTxPduCfg_OBC1_oE_CAN_c97caff2_Tx, CanId:0x000003A3uL */               ,
  /*     8 */                         7u  /* CanIfConf_CanIfTxPduCfg_OBC2_oE_CAN_349585b6_Tx, CanId:0x000003A2uL */               ,
  /*     9 */                         9u  /* CanIfConf_CanIfTxPduCfg_OBC3_oE_CAN_d6e261b5_Tx, CanId:0x00000230uL */               ,
  /*    10 */                         5u  /* CanIfConf_CanIfTxPduCfg_OBC4_oE_CAN_1436d77f_Tx, CanId:0x00000439uL */               ,
  /*    11 */                         0u  /* CanIfConf_CanIfTxPduCfg_OBCTxDiag_oE_CAN_d6e7f536_Tx, CanId:0x000007FDuL */          ,
  /*    12 */                         4u  /* CanIfConf_CanIfTxPduCfg_REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx, CanId:0x0000058FuL */ ,
  /*    13 */                         2u  /* CanIfConf_CanIfTxPduCfg_SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx, CanId:0x00000591uL */   ,
  /*    14 */                        16u  /* CanIfConf_CanIfTxPduCfg_SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx, CanId:0x000004A3uL */,
  /*    15 */                        17u  /* CanIfConf_CanIfTxPduCfg_SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx, CanId:0x00000495uL */,
  /*    16 */                        18u  /* CanIfConf_CanIfTxPduCfg_SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx, CanId:0x00000402uL */ ,
  /*    17 */                        10u  /* CanIfConf_CanIfTxPduCfg_VERS_OBC_DCDC_oE_CAN_86d6c928_Tx, CanId:0x000000CEuL */      ,
  /*    18 */                         3u  /* CanIfConf_CanIfTxPduCfg_XCP_RES_oE_CAN_c5ee7713_Tx, CanId:0x00000590uL */            
};
#define CANIF_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_CtrlStates
**********************************************************************************************************************/
/** 
  \var    CanIf_CtrlStates
  \details
  Element     Description
  CtrlMode    Controller mode.
  PduMode     PDU mode state.
*/ 
#define CANIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanIf_CtrlStatesUType, CANIF_VAR_NOINIT) CanIf_CtrlStates;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys   Left */
  /*     0 */  /* [/ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_E_CAN_bc2feff2] */
  /*     1 */  /* [/ActiveEcuC/CanIf/CanIfCtrlDrvCfg_7d254554/CT_Int_CAN_8c79a9c0] */

#define CANIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxBufferPrioByCanIdBase
**********************************************************************************************************************/
/** 
  \var    CanIf_TxBufferPrioByCanIdBase
  \brief  Variable declaration - Tx-buffer: PRIO_BY_CANID as byte/bit-queue. Stores at least the QueueCounter.
*/ 
#define CANIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanIf_TxBufferPrioByCanIdBaseUType, CANIF_VAR_NOINIT) CanIf_TxBufferPrioByCanIdBase;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys   Left */
  /*     0 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_869c3a89] */
  /*     1 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/CHNL_5ca432f7] */

#define CANIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxQueue
**********************************************************************************************************************/
/** 
  \var    CanIf_TxQueue
  \brief  Variable declaration - Tx byte queue.
*/ 
#define CANIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanIf_TxQueueUType, CANIF_VAR_NOINIT) CanIf_TxQueue;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys   Left */
  /*     0 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBCTxDiag_oE_CAN_d6e7f536_Tx] */
  /*     1 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/NEW_JDD_OBC_DCDC_oE_CAN_0beb6a5d_Tx] */
  /*     2 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUPV_V2_OBC_DCDC_oE_CAN_211f9423_Tx] */
  /*     3 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/XCP_RES_oE_CAN_c5ee7713_Tx] */
  /*     4 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/REP_DIAG_ON_CAN_Tp_oE_CAN_767bda48_Tx] */
  /*     5 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC4_oE_CAN_1436d77f_Tx] */
  /*     6 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC1_oE_CAN_c97caff2_Tx] */
  /*     7 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC2_oE_CAN_349585b6_Tx] */
  /*     8 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/DC1_oE_CAN_0bfdcb0d_Tx] */
  /*     9 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/OBC3_oE_CAN_d6e261b5_Tx] */
  /*    10 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/VERS_OBC_DCDC_oE_CAN_86d6c928_Tx] */
  /*    11 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/DC2_oE_CAN_1927a4ae_Tx] */
  /*    12 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug4_oInt_CAN_256e892d_Tx] */
  /*    13 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug3_oInt_CAN_6a448e86_Tx] */
  /*    14 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug2_oInt_CAN_bc7a3afe_Tx] */
  /*    15 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/Debug1_oInt_CAN_1d48e037_Tx] */
  /*    16 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCHV_oInt_CAN_9fe346d1_Tx] */
  /*    17 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToDCLV_oInt_CAN_fd19250d_Tx] */
  /*    18 */  /* [/ActiveEcuC/CanIf/CanIfInitCfg/SUP_CommandToPFC_oInt_CAN_1fb3d888_Tx] */

#define CANIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/



