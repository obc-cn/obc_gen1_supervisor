/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Dem
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Dem_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:48
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/



/* configuration, interrupt handling implementations differ from the
 * source identification define used here. The naming
 * schemes for those files can be taken from this list:
 *
 * Dem.c:         DEM_SOURCE
 * Dem_Lcfg.c:    DEM_LCFG_SOURCE
 * Dem_PBcfg.c:   DEM_PBCFG_SOURCE */
#define DEM_LCFG_SOURCE


/**********************************************************************************************************************
  MISRA JUSTIFICATIONS
**********************************************************************************************************************/

/* PRQA S 0810 EOF */ /* MD_MSR_1.1_810 */                                      /* #include "..." causes nesting to exceed 8 levels - program is non-conforming. -- caused by #include'd files. */
/* PRQA S 0828 EOF */ /* MD_MSR_1.1_828 */                                      /* Maximum '#if...' nesting exceeds 8 levels - program is non-conforming -- caused by #include'd files. */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */                                      /* Number of macro definitions exceeds 1024 - program is non-conforming -- caused by #include'd files. */
/* PRQA S 0779 EOF */ /* MD_DEM_5.1 */                                          /* Identifier does not differ in 32 significant characters -- caused by Autosar algorithm for unique symbolic names. */
/* PRQA S 0612 EOF */ /* MD_DEM_1.1_612 */                                      /* The size of an object exceeds 32767 bytes - program is non-conforming -- caused by large user configration. */


/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/

#include "Dem.h"
#include "Os.h"
#if (DEM_CFG_USE_NVM == STD_ON)
# include "NvM.h"                                                               /* get: symbolic names for NvM block IDs */
#endif
#if (DEM_CFG_SUPPORT_J1939 == STD_ON)
# include "J1939Nm.h"                                                           /* get: symbolic names for J1939Nm node IDs */
#endif
#if (DEM_CFG_USE_RTE == STD_ON)
/* DEM used with RTE */
# include "Rte_DemMaster_0.h"
#endif
#include "Dem_AdditionalIncludeCfg.h"                                           /* additional, configuration defined files */

/**********************************************************************************************************************
  LOCAL CONSTANT MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  Dem_Cfg_CallbackGetFdc
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_CallbackGetFdc
  \brief  DemCallbackGetFDC/DemCallbackGetFDCFnc of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceMonitorInternal
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_GetFDCFPtrType, DEM_CONST) Dem_Cfg_CallbackGetFdc[70] = {
  /* Index     CallbackGetFdc                                                       Referable Keys */
  /*     0 */ Rte_Call_CBFaultDetectCtr_DTC_0xe00362_GetFaultDetectionCounter ,  /* [DTC_0xe00362] */
  /*     1 */ Rte_Call_CBFaultDetectCtr_DTC_0xe00214_GetFaultDetectionCounter ,  /* [DTC_0xe00214] */
  /*     2 */ Rte_Call_CBFaultDetectCtr_DTC_0xe00087_GetFaultDetectionCounter ,  /* [DTC_0xe00087] */
  /*     3 */ Rte_Call_CBFaultDetectCtr_DTC_0xe00081_GetFaultDetectionCounter ,  /* [DTC_0xe00081] */
  /*     4 */ Rte_Call_CBFaultDetectCtr_DTC_0xd38783_GetFaultDetectionCounter ,  /* [DTC_0xd38783] */
  /*     5 */ Rte_Call_CBFaultDetectCtr_DTC_0xd38782_GetFaultDetectionCounter ,  /* [DTC_0xd38782] */
  /*     6 */ Rte_Call_CBFaultDetectCtr_DTC_0xd2a081_GetFaultDetectionCounter ,  /* [DTC_0xd2a081] */
  /*     7 */ Rte_Call_CBFaultDetectCtr_DTC_0xd20781_GetFaultDetectionCounter ,  /* [DTC_0xd20781] */
  /*     8 */ Rte_Call_CBFaultDetectCtr_DTC_0xd1a087_GetFaultDetectionCounter ,  /* [DTC_0xd1a087] */
  /*     9 */ Rte_Call_CBFaultDetectCtr_DTC_0xd18787_GetFaultDetectionCounter ,  /* [DTC_0xd18787] */
  /*    10 */ Rte_Call_CBFaultDetectCtr_DTC_0xc08913_GetFaultDetectionCounter ,  /* [DTC_0xc08913] */
  /*    11 */ Rte_Call_CBFaultDetectCtr_DTC_0xc07988_GetFaultDetectionCounter ,  /* [DTC_0xc07988] */
  /*    12 */ Rte_Call_CBFaultDetectCtr_DTC_0x1a7172_GetFaultDetectionCounter ,  /* [DTC_0x1a7172] */
  /*    13 */ Rte_Call_CBFaultDetectCtr_DTC_0x1a714b_GetFaultDetectionCounter ,  /* [DTC_0x1a714b] */
  /*    14 */ Rte_Call_CBFaultDetectCtr_DTC_0x1a7104_GetFaultDetectionCounter ,  /* [DTC_0x1a7104] */
  /*    15 */ Rte_Call_CBFaultDetectCtr_DTC_0x1a0064_GetFaultDetectionCounter ,  /* [DTC_0x1a0064] */
  /*    16 */ Rte_Call_CBFaultDetectCtr_DTC_0x179f12_GetFaultDetectionCounter ,  /* [DTC_0x179f12] */
  /*    17 */ Rte_Call_CBFaultDetectCtr_DTC_0x179f11_GetFaultDetectionCounter ,  /* [DTC_0x179f11] */
  /*    18 */ Rte_Call_CBFaultDetectCtr_DTC_0x179e12_GetFaultDetectionCounter ,  /* [DTC_0x179e12] */
  /*    19 */ Rte_Call_CBFaultDetectCtr_DTC_0x179e11_GetFaultDetectionCounter ,  /* [DTC_0x179e11] */
  /*    20 */ Rte_Call_CBFaultDetectCtr_DTC_0x166c64_GetFaultDetectionCounter ,  /* [DTC_0x166c64] */
  /*    21 */ Rte_Call_CBFaultDetectCtr_DTC_0x13e919_GetFaultDetectionCounter ,  /* [DTC_0x13e919] */
  /*    22 */ Rte_Call_CBFaultDetectCtr_DTC_0x12f917_GetFaultDetectionCounter ,  /* [DTC_0x12f917] */
  /*    23 */ Rte_Call_CBFaultDetectCtr_DTC_0x12f316_GetFaultDetectionCounter ,  /* [DTC_0x12f316] */
  /*    24 */ Rte_Call_CBFaultDetectCtr_DTC_0x12ea11_GetFaultDetectionCounter ,  /* [DTC_0x12ea11] */
  /*    25 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e912_GetFaultDetectionCounter ,  /* [DTC_0x12e912] */
  /*    26 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e811_GetFaultDetectionCounter ,  /* [DTC_0x12e811] */
  /*    27 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e712_GetFaultDetectionCounter ,  /* [DTC_0x12e712] */
  /*    28 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e319_GetFaultDetectionCounter ,  /* [DTC_0x12e319] */
  /*    29 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e213_GetFaultDetectionCounter ,  /* [DTC_0x12e213] */
  /*    30 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e111_GetFaultDetectionCounter ,  /* [DTC_0x12e111] */
  /*    31 */ Rte_Call_CBFaultDetectCtr_DTC_0x12e012_GetFaultDetectionCounter ,  /* [DTC_0x12e012] */
  /*    32 */ Rte_Call_CBFaultDetectCtr_DTC_0x12df13_GetFaultDetectionCounter ,  /* [DTC_0x12df13] */
  /*    33 */ Rte_Call_CBFaultDetectCtr_DTC_0x12de11_GetFaultDetectionCounter ,  /* [DTC_0x12de11] */
  /*    34 */ Rte_Call_CBFaultDetectCtr_DTC_0x12dd12_GetFaultDetectionCounter ,  /* [DTC_0x12dd12] */
  /*    35 */ Rte_Call_CBFaultDetectCtr_DTC_0x12dc11_GetFaultDetectionCounter ,  /* [DTC_0x12dc11] */
  /*    36 */ Rte_Call_CBFaultDetectCtr_DTC_0x12db12_GetFaultDetectionCounter ,  /* [DTC_0x12db12] */
  /*    37 */ Rte_Call_CBFaultDetectCtr_DTC_0x12da13_GetFaultDetectionCounter ,  /* [DTC_0x12da13] */
  /*    38 */ Rte_Call_CBFaultDetectCtr_DTC_0x12da12_GetFaultDetectionCounter ,  /* [DTC_0x12da12] */
  /*    39 */ Rte_Call_CBFaultDetectCtr_DTC_0x12da11_GetFaultDetectionCounter ,  /* [DTC_0x12da11] */
  /*    40 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d913_GetFaultDetectionCounter ,  /* [DTC_0x12d913] */
  /*    41 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d912_GetFaultDetectionCounter ,  /* [DTC_0x12d912] */
  /*    42 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d911_GetFaultDetectionCounter ,  /* [DTC_0x12d911] */
  /*    43 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d813_GetFaultDetectionCounter ,  /* [DTC_0x12d813] */
  /*    44 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d812_GetFaultDetectionCounter ,  /* [DTC_0x12d812] */
  /*    45 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d811_GetFaultDetectionCounter ,  /* [DTC_0x12d811] */
  /*    46 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d713_GetFaultDetectionCounter ,  /* [DTC_0x12d713] */
  /*    47 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d712_GetFaultDetectionCounter ,  /* [DTC_0x12d712] */
  /*    48 */ Rte_Call_CBFaultDetectCtr_DTC_0x12d711_GetFaultDetectionCounter ,  /* [DTC_0x12d711] */
  /*    49 */ Rte_Call_CBFaultDetectCtr_DTC_0x120d98_GetFaultDetectionCounter ,  /* [DTC_0x120d98] */
  /* Index     CallbackGetFdc                                                       Referable Keys */
  /*    50 */ Rte_Call_CBFaultDetectCtr_DTC_0x120d64_GetFaultDetectionCounter ,  /* [DTC_0x120d64] */
  /*    51 */ Rte_Call_CBFaultDetectCtr_DTC_0x120c98_GetFaultDetectionCounter ,  /* [DTC_0x120c98] */
  /*    52 */ Rte_Call_CBFaultDetectCtr_DTC_0x120c64_GetFaultDetectionCounter ,  /* [DTC_0x120c64] */
  /*    53 */ Rte_Call_CBFaultDetectCtr_DTC_0x120b12_GetFaultDetectionCounter ,  /* [DTC_0x120b12] */
  /*    54 */ Rte_Call_CBFaultDetectCtr_DTC_0x120b11_GetFaultDetectionCounter ,  /* [DTC_0x120b11] */
  /*    55 */ Rte_Call_CBFaultDetectCtr_DTC_0x120a12_GetFaultDetectionCounter ,  /* [DTC_0x120a12] */
  /*    56 */ Rte_Call_CBFaultDetectCtr_DTC_0x120a11_GetFaultDetectionCounter ,  /* [DTC_0x120a11] */
  /*    57 */ Rte_Call_CBFaultDetectCtr_DTC_0x10c713_GetFaultDetectionCounter ,  /* [DTC_0x10c713] */
  /*    58 */ Rte_Call_CBFaultDetectCtr_DTC_0x10c613_GetFaultDetectionCounter ,  /* [DTC_0x10c613] */
  /*    59 */ Rte_Call_CBFaultDetectCtr_DTC_0x10c512_GetFaultDetectionCounter ,  /* [DTC_0x10c512] */
  /*    60 */ Rte_Call_CBFaultDetectCtr_DTC_0x10c413_GetFaultDetectionCounter ,  /* [DTC_0x10c413] */
  /*    61 */ Rte_Call_CBFaultDetectCtr_DTC_0x108093_GetFaultDetectionCounter ,  /* [DTC_0x108093] */
  /*    62 */ Rte_Call_CBFaultDetectCtr_DTC_0x0cf464_GetFaultDetectionCounter ,  /* [DTC_0x0cf464] */
  /*    63 */ Rte_Call_CBFaultDetectCtr_DTC_0x0af864_GetFaultDetectionCounter ,  /* [DTC_0x0af864] */
  /*    64 */ Rte_Call_CBFaultDetectCtr_DTC_0x0a9464_GetFaultDetectionCounter ,  /* [DTC_0x0a9464] */
  /*    65 */ Rte_Call_CBFaultDetectCtr_DTC_0x0a084b_GetFaultDetectionCounter ,  /* [DTC_0x0a084b] */
  /*    66 */ Rte_Call_CBFaultDetectCtr_DTC_0x0a0804_GetFaultDetectionCounter ,  /* [DTC_0x0a0804] */
  /*    67 */ Rte_Call_CBFaultDetectCtr_DTC_0x056317_GetFaultDetectionCounter ,  /* [DTC_0x056317] */
  /*    68 */ Rte_Call_CBFaultDetectCtr_DTC_0x056216_GetFaultDetectionCounter ,  /* [DTC_0x056216] */
  /*    69 */ NULL_PTR                                                           /* [#EVENT_INVALID, DTC_dummy1, DTC_dummy2] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_CycleIdTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_CycleIdTable
  \details
  Element         Description
  UsedForAging    DemOperationCycle is target of any DemEventParameter/DemEventClass/DemAgingCycleRef
  OpCycleType     DemOperationCycleType of the DemOperationCycle: [DEM_CFG_OPCYC_IGNITION, DEM_CFG_OPCYC_OBD_DCY, DEM_CFG_OPCYC_OTHER, DEM_CFG_OPCYC_POWER, DEM_CFG_OPCYC_TIME, DEM_CFG_OPCYC_WARMUP, DEM_CFG_OPCYC_IGNITION_HYBRID, DEM_CFG_OPCYC_AGING]
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_CycleIdTableType, DEM_CONST) Dem_Cfg_CycleIdTable[3] = {
    /* Index    UsedForAging  OpCycleType                          Comment */
  { /*     0 */         TRUE, DEM_CFG_OPCYC_IGNITION_HYBRID },  /* [DemConf_DemOperationCycle_DemAgingCycle] */
  { /*     1 */        FALSE, DEM_CFG_OPCYC_IGNITION        },  /* [DemConf_DemOperationCycle_DemDrivingCycle] */
  { /*     2 */        FALSE, DEM_CFG_OPCYC_POWER           }   /* [DemConf_DemOperationCycle_PowerCycle] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataCollectionTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataCollectionTable
  \details
  Element                                Description
  IdNumber                           
  CollectionSize                     
  DataElementTableCol2ElmtIndEndIdx      the end index of the 0:n relation pointing to Dem_Cfg_DataElementTableCol2ElmtInd
  DataElementTableCol2ElmtIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_DataElementTableCol2ElmtInd
  MaskedBits                             contains bitcoded the boolean data of Dem_Cfg_DataElementTableCol2ElmtIndUsedOfDataCollectionTable, Dem_Cfg_UpdateOfDataCollectionTable
  StorageKind                        
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataCollectionTableType, DEM_CONST) Dem_Cfg_DataCollectionTable[69] = {
    /* Index    IdNumber  CollectionSize  DataElementTableCol2ElmtIndEndIdx                                  DataElementTableCol2ElmtIndStartIdx                                  MaskedBits  StorageKind                     Referable Keys */
  { /*     0 */  0x0000u,             0u, DEM_CFG_NO_DATAELEMENTTABLECOL2ELMTINDENDIDXOFDATACOLLECTIONTABLE, DEM_CFG_NO_DATAELEMENTTABLECOL2ELMTINDSTARTIDXOFDATACOLLECTIONTABLE,      0x00u, DEM_CFG_EREC_TYPE_GLOBAL },  /* [#NoDataCollectionConfigured] */
  { /*     1 */  0xD5CFu,             2u,                                                                1u,                                                                  0u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ACPhase2, Ffm:DemFreezeFrameClass_PlantMode] */
  { /*     2 */  0xD5D1u,             2u,                                                                2u,                                                                  1u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ACPhase3, Ffm:DemFreezeFrameClass_PlantMode] */
  { /*     3 */  0xFE73u,             2u,                                                                3u,                                                                  2u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_BMS_DCRelayVoltage, Ffm:DemFreezeFrameClass_PlantMode] */
  { /*     4 */  0xD8EBu,             2u,                                                                4u,                                                                  3u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_BatterySOC, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*     5 */  0xD852u,             1u,                                                                5u,                                                                  4u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ChLEDB, Ffm:DemFreezeFrameClass_LED] */
  { /*     6 */  0xD850u,             1u,                                                                6u,                                                                  5u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ChLEDG, Ffm:DemFreezeFrameClass_LED] */
  { /*     7 */  0xD84Eu,             1u,                                                                7u,                                                                  6u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ChLEDR, Ffm:DemFreezeFrameClass_LED] */
  { /*     8 */  0xD840u,             1u,                                                                8u,                                                                  7u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCDC_HV_OVERVOLT, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LVDC] */
  { /*     9 */  0xD827u,             1u,                                                                9u,                                                                  8u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCDCstatus, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    10 */  0xFE61u,             1u,                                                               10u,                                                                  9u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCHV_ADC_IOUT_CAN_Signal, Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  { /*    11 */  0xFE6Bu,             1u,                                                               11u,                                                                 10u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCHV_ADC_NTC_MOD_5, Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  { /*    12 */  0xFE6Du,             1u,                                                               12u,                                                                 11u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCHV_ADC_NTC_MOD_6, Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  { /*    13 */  0xFE60u,             1u,                                                               13u,                                                                 12u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCHV_ADC_VOUT_CAN_Signal, Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  { /*    14 */  0xFE67u,             4u,                                                               14u,                                                                 13u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCHV_Fault, Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  { /*    15 */  0xFE64u,             4u,                                                               15u,                                                                 14u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_Fault, Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  { /*    16 */  0xFE6Eu,             1u,                                                               16u,                                                                 15u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_T_L_BUCK, Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  { /*    17 */  0xFE6Au,             1u,                                                               17u,                                                                 16u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_T_PP_A, Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  { /*    18 */  0xFE6Cu,             1u,                                                               18u,                                                                 17u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_T_PP_B, Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  { /*    19 */  0xFE65u,             1u,                                                               19u,                                                                 18u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_T_SW_BUCK_A, Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  { /*    20 */  0xFE68u,             1u,                                                               20u,                                                                 19u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_T_SW_BUCK_B, Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  { /*    21 */  0xFE6Fu,             1u,                                                               21u,                                                                 20u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DCLV_T_TX_PP, Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  { /*    22 */  0xD4CAu,             4u,                                                               22u,                                                                 21u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DateForBodyUnit, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    23 */  0xD82Du,             1u,                                                               23u,                                                                 22u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DetcdConnector, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline] */
  { /*    24 */  0xD407u,             1u,                                                               24u,                                                                 23u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DriverRequest, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    25 */  0xFE72u,             1u,                                                               25u,                                                                 24u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_DutyControlPilot, Ffm:DemFreezeFrameClass_PlantMode] */
  { /*    26 */  0xD853u,             1u,                                                               26u,                                                                 25u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ExtChLEDB, Ffm:DemFreezeFrameClass_LED] */
  { /*    27 */  0xD851u,             1u,                                                               27u,                                                                 26u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ExtChLEDG, Ffm:DemFreezeFrameClass_LED] */
  { /*    28 */  0xD84Fu,             1u,                                                               28u,                                                                 27u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ExtChLEDR, Ffm:DemFreezeFrameClass_LED] */
  { /*    29 */  0xD855u,             1u,                                                               29u,                                                                 28u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_Ext_PlgLED, Ffm:DemFreezeFrameClass_LED] */
  { /*    30 */  0xD829u,             1u,                                                               30u,                                                                 29u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_I_DCDC_BT_INP, Ffm:DemFreezeFrameClass_LVDC] */
  { /*    31 */  0xD825u,             1u,                                                               31u,                                                                 30u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_I_DCDC_HT_INP, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    32 */  0xD82Cu,             1u,                                                               32u,                                                                 31u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_I_PLUG, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline] */
  { /*    33 */  0xD82Eu,             1u,                                                               33u,                                                                 32u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_I_PLUG_PILOT_L, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline] */
  { /*    34 */  0xFE70u,             1u,                                                               34u,                                                                 33u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_OBC_Fault, Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  { /*    35 */  0xD84Bu,             1u,                                                               35u,                                                                 34u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_OBC_SocketTempL, Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  { /*    36 */  0xD84Du,             1u,                                                               36u,                                                                 35u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_OBC_SocketTempN, Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  { /*    37 */  0xFE63u,             4u,                                                               37u,                                                                 36u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_PFC_Fault, Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  { /*    38 */  0xFE62u,             1u,                                                               38u,                                                                 37u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_PFC_Temp_M1_C, Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  { /*    39 */  0xFE66u,             1u,                                                               39u,                                                                 38u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_PFC_Temp_M3_C, Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  { /*    40 */  0xFE69u,             1u,                                                               40u,                                                                 39u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_PFC_Temp_M4_C, Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  { /*    41 */  0xD854u,             1u,                                                               41u,                                                                 40u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_PlgLED, Ffm:DemFreezeFrameClass_LED] */
  { /*    42 */  0xD846u,             1u,                                                               42u,                                                                 41u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_PlugLockFlapState, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  { /*    43 */  0xFE71u,             2u,                                                               43u,                                                                 42u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ProximityVoltage, Ffm:DemFreezeFrameClass_PlantMode] */
  { /*    44 */  0xD40Cu,             1u,                                                               44u,                                                                 43u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_RCDLine, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    45 */  0xD83Bu,             1u,                                                               45u,                                                                 44u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_R_DCDC_REAL_LOAD, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_ProximityLline] */
  { /*    46 */  0xD83Fu,             1u,                                                               46u,                                                                 45u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_DCDC_HV_OVER_CUR, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LVDC] */
  { /*    47 */  0xD83Du,             1u,                                                               47u,                                                                 46u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_DCDC_LV_OVER_CUR, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LVDC] */
  { /*    48 */  0xD83Eu,             1u,                                                               48u,                                                                 47u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_DCDC_LV_OVER_VOLT, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LVDC] */
  { /*    49 */  0xD83Cu,             1u,                                                               49u,                                                                 48u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_DCDC_OVER_TEMP, Ffm:DemFreezeFrameClass_HVAC] */
    /* Index    IdNumber  CollectionSize  DataElementTableCol2ElmtIndEndIdx                                  DataElementTableCol2ElmtIndStartIdx                                  MaskedBits  StorageKind                     Referable Keys */
  { /*    50 */  0xD843u,             1u,                                                               50u,                                                                 49u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_LOCK_CTL, Ffm:DemFreezeFrameClass_PlugLock] */
  { /*    51 */  0xD831u,             1u,                                                               51u,                                                                 50u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_PLUG_REQ, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline] */
  { /*    52 */  0xD82Fu,             1u,                                                               52u,                                                                 51u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_PUSH_CHG, Ffm:DemFreezeFrameClass_ProximityLline] */
  { /*    53 */  0xD844u,             1u,                                                               53u,                                                                 52u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_ST_UNLOCK_CTL, Ffm:DemFreezeFrameClass_PlugLock] */
  { /*    54 */  0xD8E9u,             1u,                                                               54u,                                                                 53u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_State_OBC, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    55 */  0xD80Cu,             2u,                                                               55u,                                                                 54u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_SwitchLineRaw, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    56 */  0xD807u,             2u,                                                               56u,                                                                 55u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_T_DC1_PLUG_MES, Ffm:DemFreezeFrameClass_HVDC] */
  { /*    57 */  0xD809u,             2u,                                                               57u,                                                                 56u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_T_DC2_PLUG_RAW, Ffm:DemFreezeFrameClass_HVDC] */
  { /*    58 */  0xD84Au,             1u,                                                               58u,                                                                 57u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_AC1_PLUG_MES, Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  { /*    59 */  0xD84Cu,             1u,                                                               59u,                                                                 58u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_AC2_PLUG_MES, Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  { /*    60 */  0xD822u,             2u,                                                               60u,                                                                 59u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_BAT_MES_OBC, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    61 */  0xD806u,             1u,                                                               61u,                                                                 60u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_DC1_PLUG_MES, Ffm:DemFreezeFrameClass_HVDC] */
  { /*    62 */  0xD808u,             1u,                                                               62u,                                                                 61u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_DC2_PLUG_MES, Ffm:DemFreezeFrameClass_HVDC] */
  { /*    63 */  0xD828u,             1u,                                                               63u,                                                                 62u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_DCDC_BT_INP, Ffm:DemFreezeFrameClass_LVDC] */
  { /*    64 */  0xD824u,             2u,                                                               64u,                                                                 63u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_DCDC_HT_INP, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    65 */  0xD82Bu,             2u,                                                               65u,                                                                 64u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_PLUG, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  { /*    66 */  0xD845u,             2u,                                                               66u,                                                                 65u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_U_PLUG_LOCK, Ffm:DemFreezeFrameClass_PlugLock] */
  { /*    67 */  0xD49Cu,             4u,                                                               67u,                                                                 66u,      0x02u, DEM_CFG_EREC_TYPE_USER   },  /* [#DidDemDidClass_VehicleMileage, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  { /*    68 */  0xD805u,             2u,                                                               68u,                                                                 67u,      0x02u, DEM_CFG_EREC_TYPE_USER   }   /* [#DidDemDidClass_VehicleSpeed, Ffm:DemFreezeFrameClass_DCDC_Overtemperature, Ffm:DemFreezeFrameClass_DCLV_POSTError, Ffm:DemFreezeFrameClass_HVAC, Ffm:DemFreezeFrameClass_HVDC, Ffm:DemFreezeFrameClass_LED, Ffm:DemFreezeFrameClass_LVDC, Ffm:DemFreezeFrameClass_OBC_HWErrors, Ffm:DemFreezeFrameClass_OBC_InputRelays, Ffm:DemFreezeFrameClass_OBC_Overcurrent, Ffm:DemFreezeFrameClass_OBC_Overtemperature, Ffm:DemFreezeFrameClass_OBC_Overvoltage, Ffm:DemFreezeFrameClass_PlantMode, Ffm:DemFreezeFrameClass_PlugLock, Ffm:DemFreezeFrameClass_PlugLockTemperature, Ffm:DemFreezeFrameClass_ProximityLline, Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataCollectionTableFfm2CollInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataCollectionTableFfm2CollInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_DataCollectionTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataCollectionTableFfm2CollIndType, DEM_CONST) Dem_Cfg_DataCollectionTableFfm2CollInd[251] = {
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*     0 */                             44u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     1 */                             54u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     2 */                             55u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     3 */                             22u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     4 */                             31u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     5 */                             68u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     6 */                             64u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     7 */                              4u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     8 */                              9u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*     9 */                             67u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    10 */                             60u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    11 */                             16u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    12 */                             17u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    13 */                             18u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    14 */                             19u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    15 */                             20u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    16 */                             21u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    17 */                             24u,  /* [Ffm:DemFreezeFrameClass_DCDC_Overtemperature] */
  /*    18 */                             44u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    19 */                             54u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    20 */                             55u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    21 */                              4u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    22 */                             64u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    23 */                              9u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    24 */                             67u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    25 */                             68u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    26 */                             31u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    27 */                             60u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    28 */                             22u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    29 */                             24u,  /* [Ffm:DemFreezeFrameClass_DCLV_POSTError] */
  /*    30 */                             31u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    31 */                             32u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    32 */                             33u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    33 */                             45u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    34 */                             49u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    35 */                             54u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    36 */                             64u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    37 */                             65u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    38 */                              9u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    39 */                             67u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    40 */                              4u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    41 */                             60u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    42 */                             22u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    43 */                             24u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    44 */                             68u,  /* [Ffm:DemFreezeFrameClass_HVAC] */
  /*    45 */                              8u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    46 */                             31u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    47 */                             46u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    48 */                             47u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    49 */                             48u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*    50 */                             54u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    51 */                             56u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    52 */                             57u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    53 */                             61u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    54 */                             62u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    55 */                             64u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    56 */                             68u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    57 */                              4u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    58 */                             22u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    59 */                             60u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    60 */                             67u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    61 */                              9u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    62 */                             24u,  /* [Ffm:DemFreezeFrameClass_HVDC] */
  /*    63 */                              5u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    64 */                              6u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    65 */                              7u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    66 */                             23u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    67 */                             29u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    68 */                             26u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    69 */                             27u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    70 */                             28u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    71 */                             41u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    72 */                             42u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    73 */                             31u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    74 */                              4u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    75 */                             22u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    76 */                             67u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    77 */                              9u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    78 */                             64u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    79 */                             60u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    80 */                             68u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    81 */                             24u,  /* [Ffm:DemFreezeFrameClass_LED] */
  /*    82 */                              8u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    83 */                             30u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    84 */                             46u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    85 */                             47u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    86 */                             48u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    87 */                             54u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    88 */                             63u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    89 */                             64u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    90 */                              9u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    91 */                             67u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    92 */                             22u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    93 */                             68u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    94 */                             31u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    95 */                              4u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    96 */                             60u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    97 */                             24u,  /* [Ffm:DemFreezeFrameClass_LVDC] */
  /*    98 */                             44u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*    99 */                             54u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*   100 */                             55u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   101 */                             31u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   102 */                              9u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   103 */                             67u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   104 */                             22u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   105 */                             60u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   106 */                              4u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   107 */                             64u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   108 */                             68u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   109 */                             15u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   110 */                             37u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   111 */                             14u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   112 */                             24u,  /* [Ffm:DemFreezeFrameClass_OBC_HWErrors] */
  /*   113 */                              4u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   114 */                             22u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   115 */                              9u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   116 */                             31u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   117 */                             44u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   118 */                             54u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   119 */                             55u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   120 */                             60u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   121 */                             64u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   122 */                             67u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   123 */                             68u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   124 */                             34u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   125 */                             24u,  /* [Ffm:DemFreezeFrameClass_OBC_InputRelays] */
  /*   126 */                             44u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   127 */                             54u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   128 */                             55u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   129 */                             64u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   130 */                             22u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   131 */                             31u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   132 */                             68u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   133 */                              4u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   134 */                             67u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   135 */                              9u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   136 */                             60u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   137 */                             10u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   138 */                             24u,  /* [Ffm:DemFreezeFrameClass_OBC_Overcurrent] */
  /*   139 */                             44u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   140 */                             54u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   141 */                             55u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   142 */                              9u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   143 */                              4u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   144 */                             68u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   145 */                             64u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   146 */                             67u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   147 */                             60u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   148 */                             22u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   149 */                             31u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*   150 */                             11u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   151 */                             12u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   152 */                             40u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   153 */                             38u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   154 */                             39u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   155 */                             24u,  /* [Ffm:DemFreezeFrameClass_OBC_Overtemperature] */
  /*   156 */                             44u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   157 */                             54u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   158 */                             55u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   159 */                             22u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   160 */                             60u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   161 */                              9u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   162 */                             31u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   163 */                             68u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   164 */                             64u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   165 */                             67u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   166 */                              4u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   167 */                             13u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   168 */                             24u,  /* [Ffm:DemFreezeFrameClass_OBC_Overvoltage] */
  /*   169 */                             67u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   170 */                             22u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   171 */                             24u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   172 */                             68u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   173 */                             31u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   174 */                             64u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   175 */                              4u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   176 */                              9u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   177 */                             60u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   178 */                             65u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   179 */                              1u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   180 */                              2u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   181 */                             43u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   182 */                             25u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   183 */                              3u,  /* [Ffm:DemFreezeFrameClass_PlantMode] */
  /*   184 */                             23u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   185 */                             32u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   186 */                             66u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   187 */                             33u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   188 */                             42u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   189 */                             50u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   190 */                             51u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   191 */                             53u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   192 */                             65u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   193 */                             60u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   194 */                              9u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   195 */                             68u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   196 */                             67u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   197 */                             64u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   198 */                              4u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   199 */                             31u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*   200 */                             22u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   201 */                             24u,  /* [Ffm:DemFreezeFrameClass_PlugLock] */
  /*   202 */                             23u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   203 */                             32u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   204 */                             33u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   205 */                             35u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   206 */                             36u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   207 */                             42u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   208 */                             51u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   209 */                             58u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   210 */                             59u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   211 */                             65u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   212 */                             67u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   213 */                             31u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   214 */                             64u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   215 */                              4u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   216 */                              9u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   217 */                             60u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   218 */                             68u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   219 */                             22u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   220 */                             24u,  /* [Ffm:DemFreezeFrameClass_PlugLockTemperature] */
  /*   221 */                             23u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   222 */                             32u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   223 */                             33u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   224 */                             45u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   225 */                             44u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   226 */                             51u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   227 */                             52u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   228 */                             54u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   229 */                             55u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   230 */                             31u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   231 */                             68u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   232 */                             24u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   233 */                             64u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   234 */                              4u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   235 */                              9u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   236 */                             60u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   237 */                             67u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   238 */                             22u,  /* [Ffm:DemFreezeFrameClass_ProximityLline] */
  /*   239 */                             44u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   240 */                             54u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   241 */                             55u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   242 */                              4u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   243 */                             67u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   244 */                             60u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   245 */                             68u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   246 */                              9u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   247 */                             22u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   248 */                             31u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /*   249 */                             64u,  /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*   250 */                             24u   /* [Ffm:DemFreezeFrameClass_SysSuppl_Comm_ECU] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataElementTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataElementTable
  \details
  Element         Description
  ElementKind     DataElement kind, returned by Dem_Cfg_DataCallbackType()
  ElementSize     Size of data element in Byte.
  ReadDataFunc    C-function for getting the data. Its signature depends on ElementKind: With value(s) DEM_CFG_DATA_FROM_CBK_WITH_EVENTID use: Std_ReturnType (*)(uint8* Buffer, uint16 EventId); - and use: Std_ReturnType (*)(uint8* Buffer); with the other values DEM_CFG_DATA_FROM_CBK, DEM_CFG_DATA_FROM_SR_PORT_BOOLEAN, DEM_CFG_DATA_FROM_SR_PORT_SINT16, DEM_CFG_DATA_FROM_SR_PORT_SINT16_INTEL, DEM_CFG_DATA_FROM_SR_PORT_SINT32, DEM_CFG_DATA_FROM_SR_PORT_SINT32_INTEL, DEM_CFG_DATA_FROM_SR_PORT_SINT8, DEM_CFG_DATA_FROM_SR_PORT_SINT8_N, DEM_CFG_DATA_FROM_SR_PORT_UINT16, DEM_CFG_DATA_FROM_SR_PORT_UINT16_INTEL, DEM_CFG_DATA_FROM_SR_PORT_UINT32, DEM_CFG_DATA_FROM_SR_PORT_UINT32_INTEL, DEM_CFG_DATA_FROM_SR_PORT_UINT8, DEM_CFG_DATA_FROM_SR_PORT_UINT8_N.
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataElementTableType, DEM_CONST) Dem_Cfg_DataElementTable[69] = {
    /* Index    ElementKind                  ElementSize  ReadDataFunc                                                                                                      Referable Keys */
  { /*     0 */ DEM_CFG_DATAELEMENT_INVALID,          0u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                       },  /* [#NoDataElementConfigured] */
  { /*     1 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D407_ReadData },  /* [#DemDataClass_D407, DidDemDidClass_DriverRequest] */
  { /*     2 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D40C_ReadData },  /* [#DemDataClass_D40C, DidDemDidClass_RCDLine] */
  { /*     3 */ DEM_CFG_DATA_FROM_CBK      ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D49C_ReadData },  /* [#DemDataClass_D49C, DidDemDidClass_VehicleMileage] */
  { /*     4 */ DEM_CFG_DATA_FROM_CBK      ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D4CA_ReadData },  /* [#DemDataClass_D4CA, DidDemDidClass_DateForBodyUnit] */
  { /*     5 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D805_ReadData },  /* [#DemDataClass_D805, DidDemDidClass_VehicleSpeed] */
  { /*     6 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D806_ReadData },  /* [#DemDataClass_D806, DidDemDidClass_U_DC1_PLUG_MES] */
  { /*     7 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D807_ReadData },  /* [#DemDataClass_D807, DidDemDidClass_T_DC1_PLUG_MES] */
  { /*     8 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D808_ReadData },  /* [#DemDataClass_D808, DidDemDidClass_U_DC2_PLUG_MES] */
  { /*     9 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D809_ReadData },  /* [#DemDataClass_D809, DidDemDidClass_T_DC2_PLUG_RAW] */
  { /*    10 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D80C_ReadData },  /* [#DemDataClass_D80C, DidDemDidClass_SwitchLineRaw] */
  { /*    11 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D822_ReadData },  /* [#DemDataClass_D822, DidDemDidClass_U_BAT_MES_OBC] */
  { /*    12 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D824_ReadData },  /* [#DemDataClass_D824, DidDemDidClass_U_DCDC_HT_INP] */
  { /*    13 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D825_ReadData },  /* [#DemDataClass_D825, DidDemDidClass_I_DCDC_HT_INP] */
  { /*    14 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D827_ReadData },  /* [#DemDataClass_D827, DidDemDidClass_DCDCstatus] */
  { /*    15 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D828_ReadData },  /* [#DemDataClass_D828, DidDemDidClass_U_DCDC_BT_INP] */
  { /*    16 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D829_ReadData },  /* [#DemDataClass_D829, DidDemDidClass_I_DCDC_BT_INP] */
  { /*    17 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D82B_ReadData },  /* [#DemDataClass_D82B, DidDemDidClass_U_PLUG] */
  { /*    18 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D82C_ReadData },  /* [#DemDataClass_D82C, DidDemDidClass_I_PLUG] */
  { /*    19 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D82D_ReadData },  /* [#DemDataClass_D82D, DidDemDidClass_DetcdConnector] */
  { /*    20 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D82E_ReadData },  /* [#DemDataClass_D82E, DidDemDidClass_I_PLUG_PILOT_L] */
  { /*    21 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D82F_ReadData },  /* [#DemDataClass_D82F, DidDemDidClass_ST_PUSH_CHG] */
  { /*    22 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D831_ReadData },  /* [#DemDataClass_D831, DidDemDidClass_ST_PLUG_REQ] */
  { /*    23 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D83B_ReadData },  /* [#DemDataClass_D83B, DidDemDidClass_R_DCDC_REAL_LOAD] */
  { /*    24 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D83C_ReadData },  /* [#DemDataClass_D83C, DidDemDidClass_ST_DCDC_OVER_TEMP] */
  { /*    25 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D83D_ReadData },  /* [#DemDataClass_D83D, DidDemDidClass_ST_DCDC_LV_OVER_CUR] */
  { /*    26 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D83E_ReadData },  /* [#DemDataClass_D83E, DidDemDidClass_ST_DCDC_LV_OVER_VOLT] */
  { /*    27 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D83F_ReadData },  /* [#DemDataClass_D83F, DidDemDidClass_ST_DCDC_HV_OVER_CUR] */
  { /*    28 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D840_ReadData },  /* [#DemDataClass_D840, DidDemDidClass_DCDC_HV_OVERVOLT] */
  { /*    29 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D843_ReadData },  /* [#DemDataClass_D843, DidDemDidClass_ST_LOCK_CTL] */
  { /*    30 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D844_ReadData },  /* [#DemDataClass_D844, DidDemDidClass_ST_UNLOCK_CTL] */
  { /*    31 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D845_ReadData },  /* [#DemDataClass_D845, DidDemDidClass_U_PLUG_LOCK] */
  { /*    32 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D846_ReadData },  /* [#DemDataClass_D846, DidDemDidClass_PlugLockFlapState] */
  { /*    33 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D84A_ReadData },  /* [#DemDataClass_D84A, DidDemDidClass_U_AC1_PLUG_MES] */
  { /*    34 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D84B_ReadData },  /* [#DemDataClass_D84B, DidDemDidClass_OBC_SocketTempL] */
  { /*    35 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D84C_ReadData },  /* [#DemDataClass_D84C, DidDemDidClass_U_AC2_PLUG_MES] */
  { /*    36 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D84D_ReadData },  /* [#DemDataClass_D84D, DidDemDidClass_OBC_SocketTempN] */
  { /*    37 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D84E_ReadData },  /* [#DemDataClass_D84E, DidDemDidClass_ChLEDR] */
  { /*    38 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D84F_ReadData },  /* [#DemDataClass_D84F, DidDemDidClass_ExtChLEDR] */
  { /*    39 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D850_ReadData },  /* [#DemDataClass_D850, DidDemDidClass_ChLEDG] */
  { /*    40 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D851_ReadData },  /* [#DemDataClass_D851, DidDemDidClass_ExtChLEDG] */
  { /*    41 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D852_ReadData },  /* [#DemDataClass_D852, DidDemDidClass_ChLEDB] */
  { /*    42 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D853_ReadData },  /* [#DemDataClass_D853, DidDemDidClass_ExtChLEDB] */
  { /*    43 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D854_ReadData },  /* [#DemDataClass_D854, DidDemDidClass_PlgLED] */
  { /*    44 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D855_ReadData },  /* [#DemDataClass_D855, DidDemDidClass_Ext_PlgLED] */
  { /*    45 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D8E9_ReadData },  /* [#DemDataClass_D8E9, DidDemDidClass_State_OBC] */
  { /*    46 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D8EB_ReadData },  /* [#DemDataClass_D8EB, DidDemDidClass_BatterySOC] */
  { /*    47 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE60_ReadData },  /* [#DemDataClass_FE60, DidDemDidClass_DCHV_ADC_VOUT_CAN_Signal] */
  { /*    48 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE61_ReadData },  /* [#DemDataClass_FE61, DidDemDidClass_DCHV_ADC_IOUT_CAN_Signal] */
  { /*    49 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE62_ReadData },  /* [#DemDataClass_FE62, DidDemDidClass_PFC_Temp_M1_C] */
    /* Index    ElementKind                  ElementSize  ReadDataFunc                                                                                                      Referable Keys */
  { /*    50 */ DEM_CFG_DATA_FROM_CBK      ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE63_ReadData },  /* [#DemDataClass_FE63, DidDemDidClass_PFC_Fault] */
  { /*    51 */ DEM_CFG_DATA_FROM_CBK      ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE64_ReadData },  /* [#DemDataClass_FE64, DidDemDidClass_DCLV_Fault] */
  { /*    52 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE65_ReadData },  /* [#DemDataClass_FE65, DidDemDidClass_DCLV_T_SW_BUCK_A] */
  { /*    53 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE66_ReadData },  /* [#DemDataClass_FE66, DidDemDidClass_PFC_Temp_M3_C] */
  { /*    54 */ DEM_CFG_DATA_FROM_CBK      ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE67_ReadData },  /* [#DemDataClass_FE67, DidDemDidClass_DCHV_Fault] */
  { /*    55 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE68_ReadData },  /* [#DemDataClass_FE68, DidDemDidClass_DCLV_T_SW_BUCK_B] */
  { /*    56 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE69_ReadData },  /* [#DemDataClass_FE69, DidDemDidClass_PFC_Temp_M4_C] */
  { /*    57 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE6A_ReadData },  /* [#DemDataClass_FE6A, DidDemDidClass_DCLV_T_PP_A] */
  { /*    58 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE6B_ReadData },  /* [#DemDataClass_FE6B, DidDemDidClass_DCHV_ADC_NTC_MOD_5] */
  { /*    59 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE6C_ReadData },  /* [#DemDataClass_FE6C, DidDemDidClass_DCLV_T_PP_B] */
  { /*    60 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE6D_ReadData },  /* [#DemDataClass_FE6D, DidDemDidClass_DCHV_ADC_NTC_MOD_6] */
  { /*    61 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE6E_ReadData },  /* [#DemDataClass_FE6E, DidDemDidClass_DCLV_T_L_BUCK] */
  { /*    62 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE6F_ReadData },  /* [#DemDataClass_FE6F, DidDemDidClass_DCLV_T_TX_PP] */
  { /*    63 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE70_ReadData },  /* [#DemDataClass_FE70, DidDemDidClass_OBC_Fault] */
  { /*    64 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D5CF_ReadData },  /* [#DemDataClass_D5CF, DidDemDidClass_ACPhase2] */
  { /*    65 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_D5D1_ReadData },  /* [#DemDataClass_D5D1, DidDemDidClass_ACPhase3] */
  { /*    66 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE71_ReadData },  /* [#DemDataClass_FE71, DidDemDidClass_ProximityVoltage] */
  { /*    67 */ DEM_CFG_DATA_FROM_CBK      ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE72_ReadData },  /* [#DemDataClass_FE72, DidDemDidClass_DutyControlPilot] */
  { /*    68 */ DEM_CFG_DATA_FROM_CBK      ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_FE73_ReadData }   /* [#DemDataClass_FE73, DidDemDidClass_BMS_DCRelayVoltage] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataElementTableCol2ElmtInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataElementTableCol2ElmtInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_DataElementTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataElementTableCol2ElmtIndType, DEM_CONST) Dem_Cfg_DataElementTableCol2ElmtInd[68] = {
  /* Index     DataElementTableCol2ElmtInd      Referable Keys */
  /*     0 */                          64u,  /* [DidDemDidClass_ACPhase2] */
  /*     1 */                          65u,  /* [DidDemDidClass_ACPhase3] */
  /*     2 */                          68u,  /* [DidDemDidClass_BMS_DCRelayVoltage] */
  /*     3 */                          46u,  /* [DidDemDidClass_BatterySOC] */
  /*     4 */                          41u,  /* [DidDemDidClass_ChLEDB] */
  /*     5 */                          39u,  /* [DidDemDidClass_ChLEDG] */
  /*     6 */                          37u,  /* [DidDemDidClass_ChLEDR] */
  /*     7 */                          28u,  /* [DidDemDidClass_DCDC_HV_OVERVOLT] */
  /*     8 */                          14u,  /* [DidDemDidClass_DCDCstatus] */
  /*     9 */                          48u,  /* [DidDemDidClass_DCHV_ADC_IOUT_CAN_Signal] */
  /*    10 */                          58u,  /* [DidDemDidClass_DCHV_ADC_NTC_MOD_5] */
  /*    11 */                          60u,  /* [DidDemDidClass_DCHV_ADC_NTC_MOD_6] */
  /*    12 */                          47u,  /* [DidDemDidClass_DCHV_ADC_VOUT_CAN_Signal] */
  /*    13 */                          54u,  /* [DidDemDidClass_DCHV_Fault] */
  /*    14 */                          51u,  /* [DidDemDidClass_DCLV_Fault] */
  /*    15 */                          61u,  /* [DidDemDidClass_DCLV_T_L_BUCK] */
  /*    16 */                          57u,  /* [DidDemDidClass_DCLV_T_PP_A] */
  /*    17 */                          59u,  /* [DidDemDidClass_DCLV_T_PP_B] */
  /*    18 */                          52u,  /* [DidDemDidClass_DCLV_T_SW_BUCK_A] */
  /*    19 */                          55u,  /* [DidDemDidClass_DCLV_T_SW_BUCK_B] */
  /*    20 */                          62u,  /* [DidDemDidClass_DCLV_T_TX_PP] */
  /*    21 */                           4u,  /* [DidDemDidClass_DateForBodyUnit] */
  /*    22 */                          19u,  /* [DidDemDidClass_DetcdConnector] */
  /*    23 */                           1u,  /* [DidDemDidClass_DriverRequest] */
  /*    24 */                          67u,  /* [DidDemDidClass_DutyControlPilot] */
  /*    25 */                          42u,  /* [DidDemDidClass_ExtChLEDB] */
  /*    26 */                          40u,  /* [DidDemDidClass_ExtChLEDG] */
  /*    27 */                          38u,  /* [DidDemDidClass_ExtChLEDR] */
  /*    28 */                          44u,  /* [DidDemDidClass_Ext_PlgLED] */
  /*    29 */                          16u,  /* [DidDemDidClass_I_DCDC_BT_INP] */
  /*    30 */                          13u,  /* [DidDemDidClass_I_DCDC_HT_INP] */
  /*    31 */                          18u,  /* [DidDemDidClass_I_PLUG] */
  /*    32 */                          20u,  /* [DidDemDidClass_I_PLUG_PILOT_L] */
  /*    33 */                          63u,  /* [DidDemDidClass_OBC_Fault] */
  /*    34 */                          34u,  /* [DidDemDidClass_OBC_SocketTempL] */
  /*    35 */                          36u,  /* [DidDemDidClass_OBC_SocketTempN] */
  /*    36 */                          50u,  /* [DidDemDidClass_PFC_Fault] */
  /*    37 */                          49u,  /* [DidDemDidClass_PFC_Temp_M1_C] */
  /*    38 */                          53u,  /* [DidDemDidClass_PFC_Temp_M3_C] */
  /*    39 */                          56u,  /* [DidDemDidClass_PFC_Temp_M4_C] */
  /*    40 */                          43u,  /* [DidDemDidClass_PlgLED] */
  /*    41 */                          32u,  /* [DidDemDidClass_PlugLockFlapState] */
  /*    42 */                          66u,  /* [DidDemDidClass_ProximityVoltage] */
  /*    43 */                           2u,  /* [DidDemDidClass_RCDLine] */
  /*    44 */                          23u,  /* [DidDemDidClass_R_DCDC_REAL_LOAD] */
  /*    45 */                          27u,  /* [DidDemDidClass_ST_DCDC_HV_OVER_CUR] */
  /*    46 */                          25u,  /* [DidDemDidClass_ST_DCDC_LV_OVER_CUR] */
  /*    47 */                          26u,  /* [DidDemDidClass_ST_DCDC_LV_OVER_VOLT] */
  /*    48 */                          24u,  /* [DidDemDidClass_ST_DCDC_OVER_TEMP] */
  /*    49 */                          29u,  /* [DidDemDidClass_ST_LOCK_CTL] */
  /* Index     DataElementTableCol2ElmtInd      Referable Keys */
  /*    50 */                          22u,  /* [DidDemDidClass_ST_PLUG_REQ] */
  /*    51 */                          21u,  /* [DidDemDidClass_ST_PUSH_CHG] */
  /*    52 */                          30u,  /* [DidDemDidClass_ST_UNLOCK_CTL] */
  /*    53 */                          45u,  /* [DidDemDidClass_State_OBC] */
  /*    54 */                          10u,  /* [DidDemDidClass_SwitchLineRaw] */
  /*    55 */                           7u,  /* [DidDemDidClass_T_DC1_PLUG_MES] */
  /*    56 */                           9u,  /* [DidDemDidClass_T_DC2_PLUG_RAW] */
  /*    57 */                          33u,  /* [DidDemDidClass_U_AC1_PLUG_MES] */
  /*    58 */                          35u,  /* [DidDemDidClass_U_AC2_PLUG_MES] */
  /*    59 */                          11u,  /* [DidDemDidClass_U_BAT_MES_OBC] */
  /*    60 */                           6u,  /* [DidDemDidClass_U_DC1_PLUG_MES] */
  /*    61 */                           8u,  /* [DidDemDidClass_U_DC2_PLUG_MES] */
  /*    62 */                          15u,  /* [DidDemDidClass_U_DCDC_BT_INP] */
  /*    63 */                          12u,  /* [DidDemDidClass_U_DCDC_HT_INP] */
  /*    64 */                          17u,  /* [DidDemDidClass_U_PLUG] */
  /*    65 */                          31u,  /* [DidDemDidClass_U_PLUG_LOCK] */
  /*    66 */                           3u,  /* [DidDemDidClass_VehicleMileage] */
  /*    67 */                           5u   /* [DidDemDidClass_VehicleSpeed] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DebounceTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DebounceTable
  \details
  Element              Description
  DecrementStepSize    (-1) * DemDebounceCounterDecrementStepSize of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  FailedThreshold      DemDebounceCounterFailedThreshold of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  IncrementStepSize    DemDebounceCounterIncrementStepSize of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  PassedThreshold      DemDebounceCounterPassedThreshold of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  MaskedBits           contains bitcoded the boolean data of Dem_Cfg_DebounceContinuousOfDebounceTable, Dem_Cfg_EventDebounceBehaviorOfDebounceTable, Dem_Cfg_JumpDownOfDebounceTable, Dem_Cfg_JumpUpOfDebounceTable, Dem_Cfg_StorageOfDebounceTable
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DebounceTableType, DEM_CONST) Dem_Cfg_DebounceTable[2] = {
    /* Index    DecrementStepSize  FailedThreshold  IncrementStepSize  PassedThreshold  MaskedBits        Referable Keys */
  { /*     0 */                -1,             127,                 1,            -128,      0x06u },  /* [DTC_0x056216, DTC_0x056317, DTC_0x0a0804, DTC_0x0a084b, DTC_0x0a9464, DTC_0x0af864, DTC_0x0cf464, DTC_0x108093, DTC_0x10c413, DTC_0x10c512, DTC_0x10c613, DTC_0x10c713, DTC_0x120a11, DTC_0x120a12, DTC_0x120b11, DTC_0x120b12, DTC_0x120c64, DTC_0x120c98, DTC_0x120d64, DTC_0x120d98, DTC_0x12d711, DTC_0x12d712, DTC_0x12d713, DTC_0x12d811, DTC_0x12d812, DTC_0x12d813, DTC_0x12d911, DTC_0x12d912, DTC_0x12d913, DTC_0x12da11, DTC_0x12da12, DTC_0x12da13, DTC_0x12db12, DTC_0x12dc11, DTC_0x12dd12, DTC_0x12de11, DTC_0x12df13, DTC_0x12e012, DTC_0x12e111, DTC_0x12e213, DTC_0x12e319, DTC_0x12e712, DTC_0x12e811, DTC_0x12e912, DTC_0x12ea11, DTC_0x12f316, DTC_0x12f917, DTC_0x13e919, DTC_0x166c64, DTC_0x179e11, DTC_0x179e12, DTC_0x179f11, DTC_0x179f12, DTC_0x1a0064, DTC_0x1a7104, DTC_0x1a714b, DTC_0x1a7172, DTC_0xc07988, DTC_0xc08913, DTC_0xd18787, DTC_0xd1a087, DTC_0xd20781, DTC_0xd2a081, DTC_0xd38782, DTC_0xd38783, DTC_0xe00081, DTC_0xe00087, DTC_0xe00214, DTC_0xe00362, DTC_dummy1, DTC_dummy2] */
  { /*     1 */                 0,               0,                 0,               0,      0x00u }   /* [#EVENT_INVALID] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DtcTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DtcTable
  \details
  Element           Description
  UdsDtc        
  FunctionalUnit
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DtcTableType, DEM_CONST) Dem_Cfg_DtcTable[70] = {
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*     0 */ 0x00FFFFFFuL,           255u },  /* [#NoUdsDtcConfigured, #NoObdDtcConfigured, #NoJ1939DtcConfigured] */
  { /*     1 */ 0x000A084BuL,             0u },  /* [DTCClass_DTC_0x0a084b] */
  { /*     2 */ 0x000A0804uL,             0u },  /* [DTCClass_DTC_0x0a0804] */
  { /*     3 */ 0x000A9464uL,             0u },  /* [DTCClass_DTC_0x0a9464] */
  { /*     4 */ 0x000AF864uL,             0u },  /* [DTCClass_DTC_0x0af864] */
  { /*     5 */ 0x001A714BuL,             0u },  /* [DTCClass_DTC_0x1a714b] */
  { /*     6 */ 0x001A0064uL,             0u },  /* [DTCClass_DTC_0x1a0064] */
  { /*     7 */ 0x001A7104uL,             0u },  /* [DTCClass_DTC_0x1a7104] */
  { /*     8 */ 0x001A7172uL,             0u },  /* [DTCClass_DTC_0x1a7172] */
  { /*     9 */ 0x0012D711uL,             0u },  /* [DTCClass_DTC_0x12d711] */
  { /*    10 */ 0x0012D712uL,             0u },  /* [DTCClass_DTC_0x12d712] */
  { /*    11 */ 0x0012D713uL,             0u },  /* [DTCClass_DTC_0x12d713] */
  { /*    12 */ 0x0012D811uL,             0u },  /* [DTCClass_DTC_0x12d811] */
  { /*    13 */ 0x0012D812uL,             0u },  /* [DTCClass_DTC_0x12d812] */
  { /*    14 */ 0x0012D813uL,             0u },  /* [DTCClass_DTC_0x12d813] */
  { /*    15 */ 0x0012D911uL,             0u },  /* [DTCClass_DTC_0x12d911] */
  { /*    16 */ 0x0012D912uL,             0u },  /* [DTCClass_DTC_0x12d912] */
  { /*    17 */ 0x0012D913uL,             0u },  /* [DTCClass_DTC_0x12d913] */
  { /*    18 */ 0x0012DA11uL,             0u },  /* [DTCClass_DTC_0x12da11] */
  { /*    19 */ 0x0012DA12uL,             0u },  /* [DTCClass_DTC_0x12da12] */
  { /*    20 */ 0x0012DA13uL,             0u },  /* [DTCClass_DTC_0x12da13] */
  { /*    21 */ 0x0012DB12uL,             0u },  /* [DTCClass_DTC_0x12db12] */
  { /*    22 */ 0x0012DC11uL,             0u },  /* [DTCClass_DTC_0x12dc11] */
  { /*    23 */ 0x0012DD12uL,             0u },  /* [DTCClass_DTC_0x12dd12] */
  { /*    24 */ 0x0012DE11uL,             0u },  /* [DTCClass_DTC_0x12de11] */
  { /*    25 */ 0x0012DF13uL,             0u },  /* [DTCClass_DTC_0x12df13] */
  { /*    26 */ 0x0012E012uL,             0u },  /* [DTCClass_DTC_0x12e012] */
  { /*    27 */ 0x0012E111uL,             0u },  /* [DTCClass_DTC_0x12e111] */
  { /*    28 */ 0x0012E213uL,             0u },  /* [DTCClass_DTC_0x12e213] */
  { /*    29 */ 0x0012E319uL,             0u },  /* [DTCClass_DTC_0x12e319] */
  { /*    30 */ 0x0012E712uL,             0u },  /* [DTCClass_DTC_0x12e712] */
  { /*    31 */ 0x0012E811uL,             0u },  /* [DTCClass_DTC_0x12e811] */
  { /*    32 */ 0x0012E912uL,             0u },  /* [DTCClass_DTC_0x12e912] */
  { /*    33 */ 0x0012EA11uL,             0u },  /* [DTCClass_DTC_0x12ea11] */
  { /*    34 */ 0x0012F917uL,             0u },  /* [DTCClass_DTC_0x12f917] */
  { /*    35 */ 0x00120A11uL,             0u },  /* [DTCClass_DTC_0x120a11] */
  { /*    36 */ 0x00120A12uL,             0u },  /* [DTCClass_DTC_0x120a12] */
  { /*    37 */ 0x00120B11uL,             0u },  /* [DTCClass_DTC_0x120b11] */
  { /*    38 */ 0x00120B12uL,             0u },  /* [DTCClass_DTC_0x120b12] */
  { /*    39 */ 0x00120C64uL,             0u },  /* [DTCClass_DTC_0x120c64] */
  { /*    40 */ 0x00120C98uL,             0u },  /* [DTCClass_DTC_0x120c98] */
  { /*    41 */ 0x00120D64uL,             0u },  /* [DTCClass_DTC_0x120d64] */
  { /*    42 */ 0x00120D98uL,             0u },  /* [DTCClass_DTC_0x120d98] */
  { /*    43 */ 0x00179E11uL,             0u },  /* [DTCClass_DTC_0x179e11] */
  { /*    44 */ 0x00179E12uL,             0u },  /* [DTCClass_DTC_0x179e12] */
  { /*    45 */ 0x00179F11uL,             0u },  /* [DTCClass_DTC_0x179f11] */
  { /*    46 */ 0x00179F12uL,             0u },  /* [DTCClass_DTC_0x179f12] */
  { /*    47 */ 0x00056216uL,             0u },  /* [DTCClass_DTC_0x056216] */
  { /*    48 */ 0x00056317uL,             0u },  /* [DTCClass_DTC_0x056317] */
  { /*    49 */ 0x00108093uL,             0u },  /* [DTCClass_DTC_0x108093] */
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*    50 */ 0x00C07988uL,             0u },  /* [DTCClass_DTC_0xc07988] */
  { /*    51 */ 0x00C08913uL,             0u },  /* [DTCClass_DTC_0xc08913] */
  { /*    52 */ 0x00D1A087uL,             0u },  /* [DTCClass_DTC_0xd1a087] */
  { /*    53 */ 0x00D2A081uL,             0u },  /* [DTCClass_DTC_0xd2a081] */
  { /*    54 */ 0x00D18787uL,             0u },  /* [DTCClass_DTC_0xd18787] */
  { /*    55 */ 0x00D20781uL,             0u },  /* [DTCClass_DTC_0xd20781] */
  { /*    56 */ 0x00D38782uL,             0u },  /* [DTCClass_DTC_0xd38782] */
  { /*    57 */ 0x00D38783uL,             0u },  /* [DTCClass_DTC_0xd38783] */
  { /*    58 */ 0x00E00081uL,             0u },  /* [DTCClass_DTC_0xe00081] */
  { /*    59 */ 0x00E00087uL,             0u },  /* [DTCClass_DTC_0xe00087] */
  { /*    60 */ 0x00E00214uL,             0u },  /* [DTCClass_DTC_0xe00214] */
  { /*    61 */ 0x00E00362uL,             0u },  /* [DTCClass_DTC_0xe00362] */
  { /*    62 */ 0x0013E919uL,             0u },  /* [DTCClass_DTC_0x13e919] */
  { /*    63 */ 0x00166C64uL,             0u },  /* [DTCClass_DTC_0x166c64] */
  { /*    64 */ 0x000CF464uL,             0u },  /* [DTCClass_DTC_0x0cf464] */
  { /*    65 */ 0x0010C413uL,             0u },  /* [DTCClass_DTC_0x10c413] */
  { /*    66 */ 0x0010C512uL,             0u },  /* [DTCClass_DTC_0x10c512] */
  { /*    67 */ 0x0010C613uL,             0u },  /* [DTCClass_DTC_0x10c613] */
  { /*    68 */ 0x0010C713uL,             0u },  /* [DTCClass_DTC_0x10c713] */
  { /*    69 */ 0x0012F316uL,             0u }   /* [DTCClass_DTC_0x12f316] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionGroupTableInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionGroupTableInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_EnableConditionGroupTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_EnableConditionGroupTableIndType, DEM_CONST) Dem_Cfg_EnableConditionGroupTableInd[139] = {
  /* Index     EnableConditionGroupTableInd      Referable Keys */
  /*     0 */                            0u,  /* [__Internal_ControlDtcSetting] */
  /*     1 */                           51u,  /* [__Internal_ControlDtcSetting] */
  /*     2 */                            8u,  /* [__Internal_ControlDtcSetting] */
  /*     3 */                           50u,  /* [__Internal_ControlDtcSetting] */
  /*     4 */                           67u,  /* [__Internal_ControlDtcSetting] */
  /*     5 */                           69u,  /* [__Internal_ControlDtcSetting] */
  /*     6 */                           66u,  /* [__Internal_ControlDtcSetting] */
  /*     7 */                           38u,  /* [__Internal_ControlDtcSetting] */
  /*     8 */                           58u,  /* [__Internal_ControlDtcSetting] */
  /*     9 */                           40u,  /* [__Internal_ControlDtcSetting] */
  /*    10 */                           63u,  /* [__Internal_ControlDtcSetting] */
  /*    11 */                           59u,  /* [__Internal_ControlDtcSetting] */
  /*    12 */                           43u,  /* [__Internal_ControlDtcSetting] */
  /*    13 */                            3u,  /* [__Internal_ControlDtcSetting] */
  /*    14 */                           17u,  /* [__Internal_ControlDtcSetting] */
  /*    15 */                           31u,  /* [__Internal_ControlDtcSetting] */
  /*    16 */                           30u,  /* [__Internal_ControlDtcSetting] */
  /*    17 */                           12u,  /* [__Internal_ControlDtcSetting] */
  /*    18 */                           33u,  /* [__Internal_ControlDtcSetting] */
  /*    19 */                            7u,  /* [__Internal_ControlDtcSetting] */
  /*    20 */                           35u,  /* [__Internal_ControlDtcSetting] */
  /*    21 */                           37u,  /* [__Internal_ControlDtcSetting] */
  /*    22 */                            1u,  /* [__Internal_ControlDtcSetting] */
  /*    23 */                           10u,  /* [__Internal_ControlDtcSetting] */
  /*    24 */                           61u,  /* [__Internal_ControlDtcSetting] */
  /*    25 */                           21u,  /* [__Internal_ControlDtcSetting] */
  /*    26 */                           22u,  /* [__Internal_ControlDtcSetting] */
  /*    27 */                           23u,  /* [__Internal_ControlDtcSetting] */
  /*    28 */                           27u,  /* [__Internal_ControlDtcSetting] */
  /*    29 */                           28u,  /* [__Internal_ControlDtcSetting] */
  /*    30 */                           29u,  /* [__Internal_ControlDtcSetting] */
  /*    31 */                           15u,  /* [__Internal_ControlDtcSetting] */
  /*    32 */                           56u,  /* [__Internal_ControlDtcSetting] */
  /*    33 */                           45u,  /* [__Internal_ControlDtcSetting] */
  /*    34 */                           16u,  /* [__Internal_ControlDtcSetting] */
  /*    35 */                           20u,  /* [__Internal_ControlDtcSetting] */
  /*    36 */                           52u,  /* [__Internal_ControlDtcSetting] */
  /*    37 */                           62u,  /* [__Internal_ControlDtcSetting] */
  /*    38 */                            6u,  /* [__Internal_ControlDtcSetting] */
  /*    39 */                           53u,  /* [__Internal_ControlDtcSetting] */
  /*    40 */                           55u,  /* [__Internal_ControlDtcSetting] */
  /*    41 */                            4u,  /* [__Internal_ControlDtcSetting] */
  /*    42 */                           39u,  /* [__Internal_ControlDtcSetting] */
  /*    43 */                           60u,  /* [__Internal_ControlDtcSetting] */
  /*    44 */                           48u,  /* [__Internal_ControlDtcSetting] */
  /*    45 */                           54u,  /* [__Internal_ControlDtcSetting] */
  /*    46 */                           42u,  /* [__Internal_ControlDtcSetting] */
  /*    47 */                           44u,  /* [__Internal_ControlDtcSetting] */
  /*    48 */                           41u,  /* [__Internal_ControlDtcSetting] */
  /*    49 */                           19u,  /* [__Internal_ControlDtcSetting] */
  /* Index     EnableConditionGroupTableInd      Referable Keys */
  /*    50 */                           32u,  /* [__Internal_ControlDtcSetting] */
  /*    51 */                           34u,  /* [__Internal_ControlDtcSetting] */
  /*    52 */                           11u,  /* [__Internal_ControlDtcSetting] */
  /*    53 */                           36u,  /* [__Internal_ControlDtcSetting] */
  /*    54 */                            5u,  /* [__Internal_ControlDtcSetting] */
  /*    55 */                           57u,  /* [__Internal_ControlDtcSetting] */
  /*    56 */                            9u,  /* [__Internal_ControlDtcSetting] */
  /*    57 */                            2u,  /* [__Internal_ControlDtcSetting] */
  /*    58 */                           65u,  /* [__Internal_ControlDtcSetting] */
  /*    59 */                           64u,  /* [__Internal_ControlDtcSetting] */
  /*    60 */                           46u,  /* [__Internal_ControlDtcSetting] */
  /*    61 */                           49u,  /* [__Internal_ControlDtcSetting] */
  /*    62 */                           24u,  /* [__Internal_ControlDtcSetting] */
  /*    63 */                           25u,  /* [__Internal_ControlDtcSetting] */
  /*    64 */                           26u,  /* [__Internal_ControlDtcSetting] */
  /*    65 */                           68u,  /* [__Internal_ControlDtcSetting] */
  /*    66 */                           14u,  /* [__Internal_ControlDtcSetting] */
  /*    67 */                           18u,  /* [__Internal_ControlDtcSetting] */
  /*    68 */                           13u,  /* [__Internal_ControlDtcSetting] */
  /*    69 */                           47u,  /* [__Internal_ControlDtcSetting] */
  /*    70 */                            9u,  /* [DemEnableConditionDTC_0x10C413] */
  /*    71 */                           10u,  /* [DemEnableConditionDTC_0x10C512] */
  /*    72 */                           11u,  /* [DemEnableConditionDTC_0x10C613] */
  /*    73 */                           12u,  /* [DemEnableConditionDTC_0x10C713] */
  /*    74 */                           44u,  /* [DemEnableConditionDTC_0x12e912] */
  /*    75 */                           46u,  /* [DemEnableConditionDTC_0x12f316] */
  /*    76 */                            4u,  /* [DemEnableCondition_DTC_0x0a084b] */
  /*    77 */                            3u,  /* [DemEnableCondition_DTC_0x0a0804] */
  /*    78 */                            5u,  /* [DemEnableCondition_DTC_0x0a9464] */
  /*    79 */                            6u,  /* [DemEnableCondition_DTC_0x0af864] */
  /*    80 */                            7u,  /* [DemEnableCondition_DTC_0x0cf464] */
  /*    81 */                           56u,  /* [DemEnableCondition_DTC_0x1a714b] */
  /*    82 */                           54u,  /* [DemEnableCondition_DTC_0x1a0064] */
  /*    83 */                           55u,  /* [DemEnableCondition_DTC_0x1a7104] */
  /*    84 */                           57u,  /* [DemEnableCondition_DTC_0x1a7172] */
  /*    85 */                           21u,  /* [DemEnableCondition_DTC_0x12d711] */
  /*    86 */                           22u,  /* [DemEnableCondition_DTC_0x12d712] */
  /*    87 */                           23u,  /* [DemEnableCondition_DTC_0x12d713] */
  /*    88 */                           24u,  /* [DemEnableCondition_DTC_0x12d811] */
  /*    89 */                           25u,  /* [DemEnableCondition_DTC_0x12d812] */
  /*    90 */                           26u,  /* [DemEnableCondition_DTC_0x12d813] */
  /*    91 */                           27u,  /* [DemEnableCondition_DTC_0x12d911] */
  /*    92 */                           28u,  /* [DemEnableCondition_DTC_0x12d912] */
  /*    93 */                           29u,  /* [DemEnableCondition_DTC_0x12d913] */
  /*    94 */                           30u,  /* [DemEnableCondition_DTC_0x12da11] */
  /*    95 */                           31u,  /* [DemEnableCondition_DTC_0x12da12] */
  /*    96 */                           32u,  /* [DemEnableCondition_DTC_0x12da13] */
  /*    97 */                           33u,  /* [DemEnableCondition_DTC_0x12db12] */
  /*    98 */                           34u,  /* [DemEnableCondition_DTC_0x12dc11] */
  /*    99 */                           35u,  /* [DemEnableCondition_DTC_0x12dd12] */
  /* Index     EnableConditionGroupTableInd      Referable Keys */
  /*   100 */                           36u,  /* [DemEnableCondition_DTC_0x12de11] */
  /*   101 */                           37u,  /* [DemEnableCondition_DTC_0x12df13] */
  /*   102 */                           38u,  /* [DemEnableCondition_DTC_0x12e012] */
  /*   103 */                           39u,  /* [DemEnableCondition_DTC_0x12e111] */
  /*   104 */                           40u,  /* [DemEnableCondition_DTC_0x12e213] */
  /*   105 */                           41u,  /* [DemEnableCondition_DTC_0x12e319] */
  /*   106 */                           42u,  /* [DemEnableCondition_DTC_0x12e712] */
  /*   107 */                           43u,  /* [DemEnableCondition_DTC_0x12e811] */
  /*   108 */                           45u,  /* [DemEnableCondition_DTC_0x12ea11] */
  /*   109 */                           47u,  /* [DemEnableCondition_DTC_0x12f917] */
  /*   110 */                           48u,  /* [DemEnableCondition_DTC_0x13e919] */
  /*   111 */                           13u,  /* [DemEnableCondition_DTC_0x120a11] */
  /*   112 */                           14u,  /* [DemEnableCondition_DTC_0x120a12] */
  /*   113 */                           15u,  /* [DemEnableCondition_DTC_0x120b11] */
  /*   114 */                           16u,  /* [DemEnableCondition_DTC_0x120b12] */
  /*   115 */                           17u,  /* [DemEnableCondition_DTC_0x120c64] */
  /*   116 */                           18u,  /* [DemEnableCondition_DTC_0x120c98] */
  /*   117 */                           19u,  /* [DemEnableCondition_DTC_0x120d64] */
  /*   118 */                           20u,  /* [DemEnableCondition_DTC_0x120d98] */
  /*   119 */                           49u,  /* [DemEnableCondition_DTC_0x166C64] */
  /*   120 */                           50u,  /* [DemEnableCondition_DTC_0x179e11] */
  /*   121 */                           51u,  /* [DemEnableCondition_DTC_0x179e12] */
  /*   122 */                           52u,  /* [DemEnableCondition_DTC_0x179f11] */
  /*   123 */                           53u,  /* [DemEnableCondition_DTC_0x179f12] */
  /*   124 */                            1u,  /* [DemEnableCondition_DTC_0x056216] */
  /*   125 */                            2u,  /* [DemEnableCondition_DTC_0x056317] */
  /*   126 */                            8u,  /* [DemEnableCondition_DTC_0x108093] */
  /*   127 */                           58u,  /* [DemEnableCondition_DTC_0xc07988] */
  /*   128 */                           59u,  /* [DemEnableCondition_DTC_0xc08913] */
  /*   129 */                           61u,  /* [DemEnableCondition_DTC_0xd1a087] */
  /*   130 */                           63u,  /* [DemEnableCondition_DTC_0xd2a081] */
  /*   131 */                           60u,  /* [DemEnableCondition_DTC_0xd18787] */
  /*   132 */                           62u,  /* [DemEnableCondition_DTC_0xd20781] */
  /*   133 */                           64u,  /* [DemEnableCondition_DTC_0xd38782] */
  /*   134 */                           65u,  /* [DemEnableCondition_DTC_0xd38783] */
  /*   135 */                           66u,  /* [DemEnableCondition_DTC_0xe00081] */
  /*   136 */                           67u,  /* [DemEnableCondition_DTC_0xe00087] */
  /*   137 */                           68u,  /* [DemEnableCondition_DTC_0xe00214] */
  /*   138 */                           69u   /* [DemEnableCondition_DTC_0xe00362] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionTable
  \brief  Map each EnableCondition(Id) to the referring EnableConditionGroups - this is reverse direction of the AUTOSAR configuration model.
  \details
  Element                                 Description
  EnableConditionGroupTableIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_EnableConditionGroupTableInd
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_EnableConditionTableType, DEM_CONST) Dem_Cfg_EnableConditionTable[70] = {
    /* Index    EnableConditionGroupTableIndStartIdx */
  { /*     0 */                                   0u },
  { /*     1 */                                  70u },
  { /*     2 */                                  71u },
  { /*     3 */                                  72u },
  { /*     4 */                                  73u },
  { /*     5 */                                  74u },
  { /*     6 */                                  75u },
  { /*     7 */                                  76u },
  { /*     8 */                                  77u },
  { /*     9 */                                  78u },
  { /*    10 */                                  79u },
  { /*    11 */                                  80u },
  { /*    12 */                                  81u },
  { /*    13 */                                  82u },
  { /*    14 */                                  83u },
  { /*    15 */                                  84u },
  { /*    16 */                                  85u },
  { /*    17 */                                  86u },
  { /*    18 */                                  87u },
  { /*    19 */                                  88u },
  { /*    20 */                                  89u },
  { /*    21 */                                  90u },
  { /*    22 */                                  91u },
  { /*    23 */                                  92u },
  { /*    24 */                                  93u },
  { /*    25 */                                  94u },
  { /*    26 */                                  95u },
  { /*    27 */                                  96u },
  { /*    28 */                                  97u },
  { /*    29 */                                  98u },
  { /*    30 */                                  99u },
  { /*    31 */                                 100u },
  { /*    32 */                                 101u },
  { /*    33 */                                 102u },
  { /*    34 */                                 103u },
  { /*    35 */                                 104u },
  { /*    36 */                                 105u },
  { /*    37 */                                 106u },
  { /*    38 */                                 107u },
  { /*    39 */                                 108u },
  { /*    40 */                                 109u },
  { /*    41 */                                 110u },
  { /*    42 */                                 111u },
  { /*    43 */                                 112u },
  { /*    44 */                                 113u },
  { /*    45 */                                 114u },
  { /*    46 */                                 115u },
  { /*    47 */                                 116u },
  { /*    48 */                                 117u },
  { /*    49 */                                 118u },
    /* Index    EnableConditionGroupTableIndStartIdx */
  { /*    50 */                                 119u },
  { /*    51 */                                 120u },
  { /*    52 */                                 121u },
  { /*    53 */                                 122u },
  { /*    54 */                                 123u },
  { /*    55 */                                 124u },
  { /*    56 */                                 125u },
  { /*    57 */                                 126u },
  { /*    58 */                                 127u },
  { /*    59 */                                 128u },
  { /*    60 */                                 129u },
  { /*    61 */                                 130u },
  { /*    62 */                                 131u },
  { /*    63 */                                 132u },
  { /*    64 */                                 133u },
  { /*    65 */                                 134u },
  { /*    66 */                                 135u },
  { /*    67 */                                 136u },
  { /*    68 */                                 137u },
  { /*    69 */                                 138u }
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EventTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EventTable
  \details
  Element                         Description
  AgingCycleCounterThreshold      DemAgingCycleCounterThreshold of the DemEventParameter/DemEventClass, if AgingAllowedOfEventTable==FALSE we use '255' here, too.
  AgingCycleId                    DemOperationCycle (ID) referenced by DemEventParameter/DemEventClass/DemAgingCycleRef
  CallbackGetFdcIdx               the index of the 1:1 relation pointing to Dem_Cfg_CallbackGetFdc
  DtcTableIdx                     the index of the 1:1 relation pointing to Dem_Cfg_DtcTable
  EnableConditionGroupTableIdx    the index of the 1:1 relation pointing to Dem_Cfg_EnableConditionGroupTable
  FreezeFrameNumTableEndIdx       the end index of the 0:n relation pointing to Dem_Cfg_FreezeFrameNumTable
  FreezeFrameNumTableStartIdx     the start index of the 0:n relation pointing to Dem_Cfg_FreezeFrameNumTable
  FreezeFrameTableStdFFIdx        the index of the 1:1 relation pointing to Dem_Cfg_FreezeFrameTable
  MaskedBits                      contains bitcoded the boolean data of Dem_Cfg_AgingAllowedOfEventTable, Dem_Cfg_EventLatchTFOfEventTable, Dem_Cfg_FreezeFrameNumTableUsedOfEventTable
  OperationCycleId                DemOperationCycle (ID) referenced by DemEventParameter/DemEventClass/DemOperationCycleRef
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_EventTableType, DEM_CONST) Dem_Cfg_EventTable[72] = {
    /* Index    AgingCycleCounterThreshold  AgingCycleId                             CallbackGetFdcIdx  DtcTableIdx  EnableConditionGroupTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  MaskedBits  OperationCycleId                                 Referable Keys */
  { /*     0 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               65u,          1u,                           4u,                                               1u,                                                 0u,                       1u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [#EVENT_INVALID, Satellite#0] */
  { /*     1 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               65u,          1u,                           4u,                                               1u,                                                 0u,                       1u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x0a084b, Satellite#0] */
  { /*     2 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               66u,          2u,                           3u,                                               2u,                                                 1u,                       2u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x0a0804, Satellite#0] */
  { /*     3 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               64u,          3u,                           5u,                                               6u,                                                 5u,                       6u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x0a9464, Satellite#0] */
  { /*     4 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               63u,          4u,                           6u,                                               4u,                                                 3u,                       4u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x0af864, Satellite#0] */
  { /*     5 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               13u,          5u,                          56u,                                              10u,                                                 9u,                      10u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x1a714b, Satellite#0] */
  { /*     6 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               15u,          6u,                          54u,                                               6u,                                                 5u,                       6u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x1a0064, Satellite#0] */
  { /*     7 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               14u,          7u,                          55u,                                               7u,                                                 6u,                       7u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x1a7104, Satellite#0] */
  { /*     8 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               12u,          8u,                          57u,                                               8u,                                                 7u,                       8u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x1a7172, Satellite#0] */
  { /*     9 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               48u,          9u,                          21u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d711, Satellite#0] */
  { /*    10 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               47u,         10u,                          22u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d712, Satellite#0] */
  { /*    11 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               46u,         11u,                          23u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d713, Satellite#0] */
  { /*    12 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               45u,         12u,                          24u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d811, Satellite#0] */
  { /*    13 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               44u,         13u,                          25u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d812, Satellite#0] */
  { /*    14 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               43u,         14u,                          26u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d813, Satellite#0] */
  { /*    15 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               42u,         15u,                          27u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d911, Satellite#0] */
  { /*    16 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               41u,         16u,                          28u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d912, Satellite#0] */
  { /*    17 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               40u,         17u,                          29u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12d913, Satellite#0] */
  { /*    18 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               39u,         18u,                          30u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12da11, Satellite#0] */
  { /*    19 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               38u,         19u,                          31u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12da12, Satellite#0] */
  { /*    20 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               37u,         20u,                          32u,                                               5u,                                                 4u,                       5u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12da13, Satellite#0] */
  { /*    21 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               36u,         21u,                          33u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12db12, Satellite#0] */
  { /*    22 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               35u,         22u,                          34u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12dc11, Satellite#0] */
  { /*    23 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               34u,         23u,                          35u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12dd12, Satellite#0] */
  { /*    24 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               33u,         24u,                          36u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12de11, Satellite#0] */
  { /*    25 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               32u,         25u,                          37u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12df13, Satellite#0] */
  { /*    26 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               31u,         26u,                          38u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e012, Satellite#0] */
  { /*    27 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               30u,         27u,                          39u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e111, Satellite#0] */
  { /*    28 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               29u,         28u,                          40u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e213, Satellite#0] */
  { /*    29 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               28u,         29u,                          41u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e319, Satellite#0] */
  { /*    30 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               27u,         30u,                          42u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e712, Satellite#0] */
  { /*    31 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               26u,         31u,                          43u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e811, Satellite#0] */
  { /*    32 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               25u,         32u,                          44u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12e912, Satellite#0] */
  { /*    33 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               24u,         33u,                          45u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12ea11, Satellite#0] */
  { /*    34 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               22u,         34u,                          47u,                                              11u,                                                10u,                      11u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x12f917, Satellite#0] */
  { /*    35 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               56u,         35u,                          13u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120a11, Satellite#0] */
  { /*    36 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               55u,         36u,                          14u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120a12, Satellite#0] */
  { /*    37 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               54u,         37u,                          15u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120b11, Satellite#0] */
  { /*    38 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               53u,         38u,                          16u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120b12, Satellite#0] */
  { /*    39 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               52u,         39u,                          17u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120c64, Satellite#0] */
  { /*    40 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               51u,         40u,                          18u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120c98, Satellite#0] */
  { /*    41 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               50u,         41u,                          19u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120d64, Satellite#0] */
  { /*    42 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               49u,         42u,                          20u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x120d98, Satellite#0] */
  { /*    43 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               19u,         43u,                          50u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x179e11, Satellite#0] */
  { /*    44 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               18u,         44u,                          51u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x179e12, Satellite#0] */
  { /*    45 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               17u,         45u,                          52u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x179f11, Satellite#0] */
  { /*    46 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               16u,         46u,                          53u,                                              14u,                                                13u,                      14u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x179f12, Satellite#0] */
  { /*    47 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               68u,         47u,                           1u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x056216, Satellite#0] */
  { /*    48 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               67u,         48u,                           2u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x056317, Satellite#0] */
  { /*    49 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               61u,         49u,                           8u,                                               4u,                                                 3u,                       4u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x108093, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                             CallbackGetFdcIdx  DtcTableIdx  EnableConditionGroupTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  MaskedBits  OperationCycleId                                 Referable Keys */
  { /*    50 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               11u,         50u,                          58u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xc07988, Satellite#0] */
  { /*    51 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               10u,         51u,                          59u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xc08913, Satellite#0] */
  { /*    52 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                8u,         52u,                          61u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xd1a087, Satellite#0] */
  { /*    53 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                6u,         53u,                          63u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xd2a081, Satellite#0] */
  { /*    54 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                9u,         54u,                          60u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xd18787, Satellite#0] */
  { /*    55 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                7u,         55u,                          62u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xd20781, Satellite#0] */
  { /*    56 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                5u,         56u,                          64u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xd38782, Satellite#0] */
  { /*    57 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                4u,         57u,                          65u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xd38783, Satellite#0] */
  { /*    58 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                3u,         58u,                          66u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xe00081, Satellite#0] */
  { /*    59 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                2u,         59u,                          67u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xe00087, Satellite#0] */
  { /*    60 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                1u,         60u,                          68u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xe00214, Satellite#0] */
  { /*    61 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,                0u,         61u,                          69u,                                              16u,                                                15u,                      16u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0xe00362, Satellite#0] */
  { /*    62 */                       255u, /*no AgingCycle*/ 3U                   ,               69u,          0u,                           0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,      0x00u, DemConf_DemOperationCycle_PowerCycle      },  /* [DTC_dummy2, Satellite#0] */
  { /*    63 */                       255u, /*no AgingCycle*/ 3U                   ,               69u,          0u,                           0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,      0x00u, DemConf_DemOperationCycle_PowerCycle      },  /* [DTC_dummy1, Satellite#0] */
  { /*    64 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               21u,         62u,                          48u,                                               9u,                                                 8u,                       9u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x13e919, Satellite#0] */
  { /*    65 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               20u,         63u,                          49u,                                              13u,                                                12u,                      13u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x166c64, Satellite#0] */
  { /*    66 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               62u,         64u,                           7u,                                              15u,                                                14u,                      15u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x0cf464, Satellite#0] */
  { /*    67 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               60u,         65u,                           9u,                                              12u,                                                11u,                      12u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x10c413, Satellite#0] */
  { /*    68 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               59u,         66u,                          10u,                                              12u,                                                11u,                      12u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x10c512, Satellite#0] */
  { /*    69 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               58u,         67u,                          11u,                                              12u,                                                11u,                      12u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x10c613, Satellite#0] */
  { /*    70 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               57u,         68u,                          12u,                                              12u,                                                11u,                      12u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle },  /* [DTC_0x10c713, Satellite#0] */
  { /*    71 */                        40u, DemConf_DemOperationCycle_DemAgingCycle,               23u,         69u,                          46u,                                               3u,                                                 2u,                       3u,      0x05u, DemConf_DemOperationCycle_DemDrivingCycle }   /* [DTC_0x12f316, Satellite#0] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FreezeFrameNumTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FreezeFrameNumTable
  \details
  Element     Description
  FFNumber
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_FreezeFrameNumTableType, DEM_CONST) Dem_Cfg_FreezeFrameNumTable[16] = {
    /* Index    FFNumber        Referable Keys */
  { /*     0 */      14u },  /* [DemFreezeFrameRecNumClass_DCDC_Overtemperature, #EVENT_INVALID, DTC_0x0a084b] */
  { /*     1 */      13u },  /* [DemFreezeFrameRecNumClass_DCLV_POSTErrors, DTC_0x0a0804] */
  { /*     2 */       6u },  /* [DemFreezeFrameRecNumClass_HVAC, DTC_0x12f316] */
  { /*     3 */       7u },  /* [DemFreezeFrameRecNumClass_HVDC, DTC_0x0af864, DTC_0x108093] */
  { /*     4 */       2u },  /* [DemFreezeFrameRecNumClass_LED, DTC_0x12d711, DTC_0x12d712, DTC_0x12d713, DTC_0x12d811, DTC_0x12d812, DTC_0x12d813, DTC_0x12d911, DTC_0x12d912, DTC_0x12d913, DTC_0x12da11, DTC_0x12da12, DTC_0x12da13] */
  { /*     5 */       8u },  /* [DemFreezeFrameRecNumClass_LVDC, DTC_0x0a9464, DTC_0x1a0064] */
  { /*     6 */      12u },  /* [DemFreezeFrameRecNumClass_OBC_HWErrors, DTC_0x1a7104] */
  { /*     7 */      15u },  /* [DemFreezeFrameRecNumClass_OBC_InputRelays, DTC_0x1a7172] */
  { /*     8 */      10u },  /* [DemFreezeFrameRecNumClass_OBC_Overcurrent, DTC_0x13e919] */
  { /*     9 */      11u },  /* [DemFreezeFrameRecNumClass_OBC_Overtemperature, DTC_0x1a714b] */
  { /*    10 */       9u },  /* [DemFreezeFrameRecNumClass_OBC_Overvoltage, DTC_0x12f917] */
  { /*    11 */      16u },  /* [DemFreezeFrameRecNumClass_PlantMode, DTC_0x10c413, DTC_0x10c512, DTC_0x10c613, DTC_0x10c713] */
  { /*    12 */       3u },  /* [DemFreezeFrameRecNumClass_PlugLock, DTC_0x12db12, DTC_0x12dc11, DTC_0x12dd12, DTC_0x12de11, DTC_0x12df13, DTC_0x12e012, DTC_0x12e111, DTC_0x12e213, DTC_0x12e319, DTC_0x166c64] */
  { /*    13 */       4u },  /* [DemFreezeFrameRecNumClass_PlugLockTemperature, DTC_0x12e712, DTC_0x12e811, DTC_0x12e912, DTC_0x12ea11, DTC_0x120a11, DTC_0x120a12, DTC_0x120b11, DTC_0x120b12, DTC_0x120c64, DTC_0x120c98, DTC_0x120d64, DTC_0x120d98, DTC_0x179e11, DTC_0x179e12, DTC_0x179f11, DTC_0x179f12] */
  { /*    14 */       5u },  /* [DemFreezeFrameRecNumClass_ProximityLline, DTC_0x0cf464] */
  { /*    15 */       1u }   /* [DemFreezeFrameRecNumClass_SysSuppl_Comm_ECU, DTC_0x056216, DTC_0x056317, DTC_0xc07988, DTC_0xc08913, DTC_0xd1a087, DTC_0xd2a081, DTC_0xd18787, DTC_0xd20781, DTC_0xd38782, DTC_0xd38783, DTC_0xe00081, DTC_0xe00087, DTC_0xe00214, DTC_0xe00362] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FreezeFrameTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FreezeFrameTable
  \details
  Element                                   Description
  DataCollectionTableFfm2CollIndEndIdx      the end index of the 0:n relation pointing to Dem_Cfg_DataCollectionTableFfm2CollInd
  DataCollectionTableFfm2CollIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_DataCollectionTableFfm2CollInd
  RecordSize                                Summarized size of did data that is stored in Dem_Cfg_PrimaryEntryType.SnapshotData[][] (i.e. typically without size of dids containing internal data elements).
  RecordSizeUds                             Summarized size of did data, did numbers and snapshot header (i.e. dynamical payload size of the uds response message).
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_FreezeFrameTableType, DEM_CONST) Dem_Cfg_FreezeFrameTable[17] = {
    /* Index    DataCollectionTableFfm2CollIndEndIdx                               DataCollectionTableFfm2CollIndStartIdx                               RecordSize  RecordSizeUds        Referable Keys */
  { /*     0 */ DEM_CFG_NO_DATACOLLECTIONTABLEFFM2COLLINDENDIDXOFFREEZEFRAMETABLE, DEM_CFG_NO_DATACOLLECTIONTABLEFFM2COLLINDSTARTIDXOFFREEZEFRAMETABLE,         0u,            0u },  /* [#NoFreezeFrameConfigured, DTC_dummy2, DTC_dummy1] */
  { /*     1 */                                                               18u,                                                                  0u,        29u,           67u },  /* [#DemFreezeFrameClass_DCDC_Overtemperature, #EVENT_INVALID, DTC_0x0a084b] */
  { /*     2 */                                                               30u,                                                                 18u,        23u,           49u },  /* [#DemFreezeFrameClass_DCLV_POSTError, DTC_0x0a0804] */
  { /*     3 */                                                               45u,                                                                 30u,        26u,           58u },  /* [#DemFreezeFrameClass_HVAC, DTC_0x12f316] */
  { /*     4 */                                                               63u,                                                                 45u,        30u,           68u },  /* [#DemFreezeFrameClass_HVDC, DTC_0x0af864, DTC_0x108093] */
  { /*     5 */                                                               82u,                                                                 63u,        29u,           69u },  /* [#DemFreezeFrameClass_LED, DTC_0x12d711, DTC_0x12d712, DTC_0x12d713, DTC_0x12d811, DTC_0x12d812, DTC_0x12d813, DTC_0x12d911, DTC_0x12d912, DTC_0x12d913, DTC_0x12da11, DTC_0x12da12, DTC_0x12da13] */
  { /*     6 */                                                               98u,                                                                 82u,        26u,           60u },  /* [#DemFreezeFrameClass_LVDC, DTC_0x0a9464, DTC_0x1a0064] */
  { /*     7 */                                                              113u,                                                                 98u,        35u,           67u },  /* [#DemFreezeFrameClass_OBC_HWErrors, DTC_0x1a7104] */
  { /*     8 */                                                              126u,                                                                113u,        24u,           52u },  /* [#DemFreezeFrameClass_OBC_InputRelays, DTC_0x1a7172] */
  { /*     9 */                                                              139u,                                                                126u,        24u,           52u },  /* [#DemFreezeFrameClass_OBC_Overcurrent, DTC_0x13e919] */
  { /*    10 */                                                              156u,                                                                139u,        28u,           64u },  /* [#DemFreezeFrameClass_OBC_Overtemperature, DTC_0x1a714b] */
  { /*    11 */                                                              169u,                                                                156u,        24u,           52u },  /* [#DemFreezeFrameClass_OBC_Overvoltage, DTC_0x12f917] */
  { /*    12 */                                                              184u,                                                                169u,        30u,           62u },  /* [#DemFreezeFrameClass_PlantMode, DTC_0x10c413, DTC_0x10c512, DTC_0x10c613, DTC_0x10c713] */
  { /*    13 */                                                              202u,                                                                184u,        30u,           68u },  /* [#DemFreezeFrameClass_PlugLock, DTC_0x12db12, DTC_0x12dc11, DTC_0x12dd12, DTC_0x12de11, DTC_0x12df13, DTC_0x12e012, DTC_0x12e111, DTC_0x12e213, DTC_0x12e319, DTC_0x166c64] */
  { /*    14 */                                                              221u,                                                                202u,        30u,           70u },  /* [#DemFreezeFrameClass_PlugLockTemperature, DTC_0x12e712, DTC_0x12e811, DTC_0x12e912, DTC_0x12ea11, DTC_0x120a11, DTC_0x120a12, DTC_0x120b11, DTC_0x120b12, DTC_0x120c64, DTC_0x120c98, DTC_0x120d64, DTC_0x120d98, DTC_0x179e11, DTC_0x179e12, DTC_0x179f11, DTC_0x179f12] */
  { /*    15 */                                                              239u,                                                                221u,        29u,           67u },  /* [#DemFreezeFrameClass_ProximityLline, DTC_0x0cf464] */
  { /*    16 */                                                              251u,                                                                239u,        23u,           49u }   /* [#DemFreezeFrameClass_SysSuppl_Comm_ECU, DTC_0x056216, DTC_0x056317, DTC_0xc07988, DTC_0xc08913, DTC_0xd1a087, DTC_0xd2a081, DTC_0xd18787, DTC_0xd20781, DTC_0xd38782, DTC_0xd38783, DTC_0xe00081, DTC_0xe00087, DTC_0xe00214, DTC_0xe00362] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryBlockId
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryBlockId
  \brief  The array contains these items: Admin, Status, 32 * Primary
*/ 
#define DEM_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_MemoryBlockIdType, DEM_CONST) Dem_Cfg_MemoryBlockId[34] = {
  /* Index     MemoryBlockId                                                           Comment */
  /*     0 */ NvMConf_NvMBlockDescriptor_DemAdminDataBlock /*NvMBlockId=7*/      ,  /* [Dem_AdminData] */
  /*     1 */ NvMConf_NvMBlockDescriptor_DemStatusDataBlock /*NvMBlockId=2*/     ,  /* [Dem_StatusData] */
  /*     2 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock0 /*NvMBlockId=6*/   ,  /* [Dem_PrimaryEntry0] */
  /*     3 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock1 /*NvMBlockId=5*/   ,  /* [Dem_PrimaryEntry1] */
  /*     4 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock2 /*NvMBlockId=4*/   ,  /* [Dem_PrimaryEntry2] */
  /*     5 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock3 /*NvMBlockId=3*/   ,  /* [Dem_PrimaryEntry3] */
  /*     6 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock4 /*NvMBlockId=24*/  ,  /* [Dem_PrimaryEntry4] */
  /*     7 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock5 /*NvMBlockId=28*/  ,  /* [Dem_PrimaryEntry5] */
  /*     8 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock6 /*NvMBlockId=32*/  ,  /* [Dem_PrimaryEntry6] */
  /*     9 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock7 /*NvMBlockId=36*/  ,  /* [Dem_PrimaryEntry7] */
  /*    10 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock8 /*NvMBlockId=8*/   ,  /* [Dem_PrimaryEntry8] */
  /*    11 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock9 /*NvMBlockId=10*/  ,  /* [Dem_PrimaryEntry9] */
  /*    12 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock10 /*NvMBlockId=12*/ ,  /* [Dem_PrimaryEntry10] */
  /*    13 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock11 /*NvMBlockId=15*/ ,  /* [Dem_PrimaryEntry11] */
  /*    14 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock12 /*NvMBlockId=16*/ ,  /* [Dem_PrimaryEntry12] */
  /*    15 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock13 /*NvMBlockId=17*/ ,  /* [Dem_PrimaryEntry13] */
  /*    16 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock14 /*NvMBlockId=18*/ ,  /* [Dem_PrimaryEntry14] */
  /*    17 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock15 /*NvMBlockId=19*/ ,  /* [Dem_PrimaryEntry15] */
  /*    18 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock16 /*NvMBlockId=20*/ ,  /* [Dem_PrimaryEntry16] */
  /*    19 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock17 /*NvMBlockId=21*/ ,  /* [Dem_PrimaryEntry17] */
  /*    20 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock18 /*NvMBlockId=22*/ ,  /* [Dem_PrimaryEntry18] */
  /*    21 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock19 /*NvMBlockId=23*/ ,  /* [Dem_PrimaryEntry19] */
  /*    22 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock20 /*NvMBlockId=25*/ ,  /* [Dem_PrimaryEntry20] */
  /*    23 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock21 /*NvMBlockId=26*/ ,  /* [Dem_PrimaryEntry21] */
  /*    24 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock22 /*NvMBlockId=27*/ ,  /* [Dem_PrimaryEntry22] */
  /*    25 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock23 /*NvMBlockId=29*/ ,  /* [Dem_PrimaryEntry23] */
  /*    26 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock24 /*NvMBlockId=30*/ ,  /* [Dem_PrimaryEntry24] */
  /*    27 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock25 /*NvMBlockId=31*/ ,  /* [Dem_PrimaryEntry25] */
  /*    28 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock26 /*NvMBlockId=33*/ ,  /* [Dem_PrimaryEntry26] */
  /*    29 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock27 /*NvMBlockId=34*/ ,  /* [Dem_PrimaryEntry27] */
  /*    30 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock28 /*NvMBlockId=35*/ ,  /* [Dem_PrimaryEntry28] */
  /*    31 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock29 /*NvMBlockId=37*/ ,  /* [Dem_PrimaryEntry29] */
  /*    32 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock30 /*NvMBlockId=38*/ ,  /* [Dem_PrimaryEntry30] */
  /*    33 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock31 /*NvMBlockId=39*/    /* [Dem_PrimaryEntry31] */
};
#define DEM_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryBlockIdToMemoryEntryId
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryBlockIdToMemoryEntryId
  \brief  The array contains these items: Admin, Status, 32 * Primary
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_MemoryEntryHandleType, DEM_CONST) Dem_Cfg_MemoryBlockIdToMemoryEntryId[34] = {
  /* Index     MemoryBlockIdToMemoryEntryId        Comment */
  /*     0 */ DEM_MEMORYENTRY_HANDLE_INVALID ,  /* [Dem_AdminData] */
  /*     1 */ DEM_MEMORYENTRY_HANDLE_INVALID ,  /* [Dem_StatusData] */
  /*     2 */ 0u                             ,  /* [Dem_PrimaryEntry0] */
  /*     3 */ 1u                             ,  /* [Dem_PrimaryEntry1] */
  /*     4 */ 2u                             ,  /* [Dem_PrimaryEntry2] */
  /*     5 */ 3u                             ,  /* [Dem_PrimaryEntry3] */
  /*     6 */ 4u                             ,  /* [Dem_PrimaryEntry4] */
  /*     7 */ 5u                             ,  /* [Dem_PrimaryEntry5] */
  /*     8 */ 6u                             ,  /* [Dem_PrimaryEntry6] */
  /*     9 */ 7u                             ,  /* [Dem_PrimaryEntry7] */
  /*    10 */ 8u                             ,  /* [Dem_PrimaryEntry8] */
  /*    11 */ 9u                             ,  /* [Dem_PrimaryEntry9] */
  /*    12 */ 10u                            ,  /* [Dem_PrimaryEntry10] */
  /*    13 */ 11u                            ,  /* [Dem_PrimaryEntry11] */
  /*    14 */ 12u                            ,  /* [Dem_PrimaryEntry12] */
  /*    15 */ 13u                            ,  /* [Dem_PrimaryEntry13] */
  /*    16 */ 14u                            ,  /* [Dem_PrimaryEntry14] */
  /*    17 */ 15u                            ,  /* [Dem_PrimaryEntry15] */
  /*    18 */ 16u                            ,  /* [Dem_PrimaryEntry16] */
  /*    19 */ 17u                            ,  /* [Dem_PrimaryEntry17] */
  /*    20 */ 18u                            ,  /* [Dem_PrimaryEntry18] */
  /*    21 */ 19u                            ,  /* [Dem_PrimaryEntry19] */
  /*    22 */ 20u                            ,  /* [Dem_PrimaryEntry20] */
  /*    23 */ 21u                            ,  /* [Dem_PrimaryEntry21] */
  /*    24 */ 22u                            ,  /* [Dem_PrimaryEntry22] */
  /*    25 */ 23u                            ,  /* [Dem_PrimaryEntry23] */
  /*    26 */ 24u                            ,  /* [Dem_PrimaryEntry24] */
  /*    27 */ 25u                            ,  /* [Dem_PrimaryEntry25] */
  /*    28 */ 26u                            ,  /* [Dem_PrimaryEntry26] */
  /*    29 */ 27u                            ,  /* [Dem_PrimaryEntry27] */
  /*    30 */ 28u                            ,  /* [Dem_PrimaryEntry28] */
  /*    31 */ 29u                            ,  /* [Dem_PrimaryEntry29] */
  /*    32 */ 30u                            ,  /* [Dem_PrimaryEntry30] */
  /*    33 */ 31u                               /* [Dem_PrimaryEntry31] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryDataPtr
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryDataPtr
  \brief  The array contains these items: Admin, Status, 32 * Primary
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_NvDataPtrType, DEM_CONST) Dem_Cfg_MemoryDataPtr[34] = {
  /* Index     MemoryDataPtr                                                                                   Comment */
  /*     0 */ (Dem_NvDataPtrType) &Dem_Cfg_GetAdminData()         /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_AdminData] */
  /*     1 */ (Dem_NvDataPtrType) &Dem_Cfg_GetStatusData()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_StatusData] */
  /*     2 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_0()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry0] */
  /*     3 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_1()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry1] */
  /*     4 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_2()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry2] */
  /*     5 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_3()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry3] */
  /*     6 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_4()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry4] */
  /*     7 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_5()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry5] */
  /*     8 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_6()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry6] */
  /*     9 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_7()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry7] */
  /*    10 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_8()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry8] */
  /*    11 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_9()    /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry9] */
  /*    12 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_10()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry10] */
  /*    13 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_11()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry11] */
  /*    14 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_12()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry12] */
  /*    15 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_13()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry13] */
  /*    16 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_14()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry14] */
  /*    17 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_15()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry15] */
  /*    18 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_16()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry16] */
  /*    19 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_17()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry17] */
  /*    20 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_18()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry18] */
  /*    21 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_19()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry19] */
  /*    22 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_20()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry20] */
  /*    23 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_21()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry21] */
  /*    24 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_22()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry22] */
  /*    25 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_23()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry23] */
  /*    26 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_24()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry24] */
  /*    27 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_25()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry25] */
  /*    28 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_26()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry26] */
  /*    29 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_27()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry27] */
  /*    30 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_28()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry28] */
  /*    31 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_29()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry29] */
  /*    32 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_30()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry30] */
  /*    33 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_31()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */   /* [Dem_PrimaryEntry31] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryDataSize
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryDataSize
  \brief  The array contains these items: Admin, Status, 32 * Primary
*/ 
#define DEM_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_MemoryDataSizeType, DEM_CONST) Dem_Cfg_MemoryDataSize[34] = {
  /* Index     MemoryDataSize                                                         Comment */
  /*     0 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetAdminData())       ,  /* [Dem_AdminData] */
  /*     1 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetStatusData())      ,  /* [Dem_StatusData] */
  /*     2 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_0())  ,  /* [Dem_PrimaryEntry0] */
  /*     3 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_1())  ,  /* [Dem_PrimaryEntry1] */
  /*     4 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_2())  ,  /* [Dem_PrimaryEntry2] */
  /*     5 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_3())  ,  /* [Dem_PrimaryEntry3] */
  /*     6 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_4())  ,  /* [Dem_PrimaryEntry4] */
  /*     7 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_5())  ,  /* [Dem_PrimaryEntry5] */
  /*     8 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_6())  ,  /* [Dem_PrimaryEntry6] */
  /*     9 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_7())  ,  /* [Dem_PrimaryEntry7] */
  /*    10 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_8())  ,  /* [Dem_PrimaryEntry8] */
  /*    11 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_9())  ,  /* [Dem_PrimaryEntry9] */
  /*    12 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_10()) ,  /* [Dem_PrimaryEntry10] */
  /*    13 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_11()) ,  /* [Dem_PrimaryEntry11] */
  /*    14 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_12()) ,  /* [Dem_PrimaryEntry12] */
  /*    15 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_13()) ,  /* [Dem_PrimaryEntry13] */
  /*    16 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_14()) ,  /* [Dem_PrimaryEntry14] */
  /*    17 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_15()) ,  /* [Dem_PrimaryEntry15] */
  /*    18 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_16()) ,  /* [Dem_PrimaryEntry16] */
  /*    19 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_17()) ,  /* [Dem_PrimaryEntry17] */
  /*    20 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_18()) ,  /* [Dem_PrimaryEntry18] */
  /*    21 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_19()) ,  /* [Dem_PrimaryEntry19] */
  /*    22 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_20()) ,  /* [Dem_PrimaryEntry20] */
  /*    23 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_21()) ,  /* [Dem_PrimaryEntry21] */
  /*    24 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_22()) ,  /* [Dem_PrimaryEntry22] */
  /*    25 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_23()) ,  /* [Dem_PrimaryEntry23] */
  /*    26 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_24()) ,  /* [Dem_PrimaryEntry24] */
  /*    27 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_25()) ,  /* [Dem_PrimaryEntry25] */
  /*    28 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_26()) ,  /* [Dem_PrimaryEntry26] */
  /*    29 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_27()) ,  /* [Dem_PrimaryEntry27] */
  /*    30 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_28()) ,  /* [Dem_PrimaryEntry28] */
  /*    31 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_29()) ,  /* [Dem_PrimaryEntry29] */
  /*    32 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_30()) ,  /* [Dem_PrimaryEntry30] */
  /*    33 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_31())    /* [Dem_PrimaryEntry31] */
};
#define DEM_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryEntry
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryEntry
  \brief  The array contains these items: 32 * Primary, 2 * ReadoutBuffer; size = DEM_CFG_GLOBAL_PRIMARY_SIZE + DEM_CFG_GLOBAL_SECONDARY_SIZE + DEM_CFG_NUMBER_OF_READOUTBUFFERS * DEM_CFG_NUMBER_OF_SUBEVENT_DATA_READOUTBUFFERS
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_SharedMemoryEntryPtrType, DEM_CONST) Dem_Cfg_MemoryEntry[34] = {
  /* Index     MemoryEntry                                                                                          Comment */
  /*     0 */ &Dem_Cfg_GetPrimaryEntry_0()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry0] */
  /*     1 */ &Dem_Cfg_GetPrimaryEntry_1()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry1] */
  /*     2 */ &Dem_Cfg_GetPrimaryEntry_2()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry2] */
  /*     3 */ &Dem_Cfg_GetPrimaryEntry_3()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry3] */
  /*     4 */ &Dem_Cfg_GetPrimaryEntry_4()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry4] */
  /*     5 */ &Dem_Cfg_GetPrimaryEntry_5()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry5] */
  /*     6 */ &Dem_Cfg_GetPrimaryEntry_6()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry6] */
  /*     7 */ &Dem_Cfg_GetPrimaryEntry_7()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry7] */
  /*     8 */ &Dem_Cfg_GetPrimaryEntry_8()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry8] */
  /*     9 */ &Dem_Cfg_GetPrimaryEntry_9()                             /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry9] */
  /*    10 */ &Dem_Cfg_GetPrimaryEntry_10()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry10] */
  /*    11 */ &Dem_Cfg_GetPrimaryEntry_11()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry11] */
  /*    12 */ &Dem_Cfg_GetPrimaryEntry_12()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry12] */
  /*    13 */ &Dem_Cfg_GetPrimaryEntry_13()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry13] */
  /*    14 */ &Dem_Cfg_GetPrimaryEntry_14()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry14] */
  /*    15 */ &Dem_Cfg_GetPrimaryEntry_15()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry15] */
  /*    16 */ &Dem_Cfg_GetPrimaryEntry_16()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry16] */
  /*    17 */ &Dem_Cfg_GetPrimaryEntry_17()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry17] */
  /*    18 */ &Dem_Cfg_GetPrimaryEntry_18()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry18] */
  /*    19 */ &Dem_Cfg_GetPrimaryEntry_19()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry19] */
  /*    20 */ &Dem_Cfg_GetPrimaryEntry_20()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry20] */
  /*    21 */ &Dem_Cfg_GetPrimaryEntry_21()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry21] */
  /*    22 */ &Dem_Cfg_GetPrimaryEntry_22()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry22] */
  /*    23 */ &Dem_Cfg_GetPrimaryEntry_23()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry23] */
  /*    24 */ &Dem_Cfg_GetPrimaryEntry_24()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry24] */
  /*    25 */ &Dem_Cfg_GetPrimaryEntry_25()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry25] */
  /*    26 */ &Dem_Cfg_GetPrimaryEntry_26()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry26] */
  /*    27 */ &Dem_Cfg_GetPrimaryEntry_27()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry27] */
  /*    28 */ &Dem_Cfg_GetPrimaryEntry_28()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry28] */
  /*    29 */ &Dem_Cfg_GetPrimaryEntry_29()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry29] */
  /*    30 */ &Dem_Cfg_GetPrimaryEntry_30()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry30] */
  /*    31 */ &Dem_Cfg_GetPrimaryEntry_31()                            /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry31] */
  /*    32 */ &Dem_Cfg_GetReadoutBuffer(0).ReadOutBufferData[0].Data   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_Cfg_ReadoutBuffer[0].ReadOutBufferData[0].Data] */
  /*    33 */ &Dem_Cfg_GetReadoutBuffer(1).ReadOutBufferData[0].Data   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */   /* [Dem_Cfg_ReadoutBuffer[1].ReadOutBufferData[0].Data] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryEntryInit
**********************************************************************************************************************/
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_PrimaryEntryType, DEM_CONST) Dem_Cfg_MemoryEntryInit = { 0 };
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_TimeSeriesEntryInit
**********************************************************************************************************************/
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_TimeSeriesEntryType, DEM_CONST) Dem_Cfg_TimeSeriesEntryInit = { 0 };
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_AdminData
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_AdminDataType, DEM_NVM_DATA_NOINIT) Dem_Cfg_AdminData;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ClearDTCTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_ClearDTCTable
  \brief  size = DEM_CFG_NUMBER_OF_CLEARDTCS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_ClearDTC_DataType, DEM_VAR_NOINIT) Dem_Cfg_ClearDTCTable[2];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_CommitBuffer
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_CommitBufferType, DEM_VAR_NOINIT) Dem_Cfg_CommitBuffer;  /* PRQA S 0759 */ /* MD_MSR_18.4 */
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DTCSelectorTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DTCSelectorTable
  \brief  size = DEM_CFG_NUMBER_OF_DTCSELECTORS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_DTCSelector_DataType, DEM_VAR_NOINIT) Dem_Cfg_DTCSelectorTable[2];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionGroupCounter
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionGroupCounter
  \brief  (DEM_CFG_SUPPORT_ENABLE_CONDITIONS == STD_ON) or there are internal EnableConditions. Table index: Condition group number. Table value: count of conditions in state 'enable'.
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_EnableConditionGroupCounterType, DEM_VAR_NOINIT) Dem_Cfg_EnableConditionGroupCounter[70];
  /* Index        Referable Keys */
  /*     0 */  /* [##NoEnableConditionGroupConfigured, __Internal_ControlDtcSetting] */
  /*     1 */  /* [#DemEnableConditionGroup_DTC_0x056216, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x056216] */
  /*     2 */  /* [#DemEnableConditionGroup_DTC_0x056317, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x056317] */
  /*     3 */  /* [#DemEnableConditionGroup_DTC_0x0a0804, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0a0804] */
  /*     4 */  /* [#DemEnableConditionGroup_DTC_0x0a084b, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0a084b] */
  /*     5 */  /* [#DemEnableConditionGroup_DTC_0x0a9464, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0a9464] */
  /*     6 */  /* [#DemEnableConditionGroup_DTC_0x0af864, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0af864] */
  /*     7 */  /* [#DemEnableConditionGroup_DTC_0x0cf464, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0cf464] */
  /*     8 */  /* [#DemEnableConditionGroup_DTC_0x108093, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x108093] */
  /*     9 */  /* [#DemEnableConditionGroup_DTC_0x10C413, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C413] */
  /*    10 */  /* [#DemEnableConditionGroup_DTC_0x10C512, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C512] */
  /*    11 */  /* [#DemEnableConditionGroup_DTC_0x10C613, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C613] */
  /*    12 */  /* [#DemEnableConditionGroup_DTC_0x10C713, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C713] */
  /*    13 */  /* [#DemEnableConditionGroup_DTC_0x120a11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120a11] */
  /*    14 */  /* [#DemEnableConditionGroup_DTC_0x120a12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120a12] */
  /*    15 */  /* [#DemEnableConditionGroup_DTC_0x120b11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120b11] */
  /*    16 */  /* [#DemEnableConditionGroup_DTC_0x120b12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120b12] */
  /*    17 */  /* [#DemEnableConditionGroup_DTC_0x120c64, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120c64] */
  /*    18 */  /* [#DemEnableConditionGroup_DTC_0x120c98, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120c98] */
  /*    19 */  /* [#DemEnableConditionGroup_DTC_0x120d64, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120d64] */
  /*    20 */  /* [#DemEnableConditionGroup_DTC_0x120d98, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120d98] */
  /*    21 */  /* [#DemEnableConditionGroup_DTC_0x12d711, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d711] */
  /*    22 */  /* [#DemEnableConditionGroup_DTC_0x12d712, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d712] */
  /*    23 */  /* [#DemEnableConditionGroup_DTC_0x12d713, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d713] */
  /*    24 */  /* [#DemEnableConditionGroup_DTC_0x12d811, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d811] */
  /*    25 */  /* [#DemEnableConditionGroup_DTC_0x12d812, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d812] */
  /*    26 */  /* [#DemEnableConditionGroup_DTC_0x12d813, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d813] */
  /*    27 */  /* [#DemEnableConditionGroup_DTC_0x12d911, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d911] */
  /*    28 */  /* [#DemEnableConditionGroup_DTC_0x12d912, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d912] */
  /*    29 */  /* [#DemEnableConditionGroup_DTC_0x12d913, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d913] */
  /*    30 */  /* [#DemEnableConditionGroup_DTC_0x12da11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12da11] */
  /*    31 */  /* [#DemEnableConditionGroup_DTC_0x12da12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12da12] */
  /*    32 */  /* [#DemEnableConditionGroup_DTC_0x12da13, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12da13] */
  /*    33 */  /* [#DemEnableConditionGroup_DTC_0x12db12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12db12] */
  /*    34 */  /* [#DemEnableConditionGroup_DTC_0x12dc11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12dc11] */
  /*    35 */  /* [#DemEnableConditionGroup_DTC_0x12dd12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12dd12] */
  /*    36 */  /* [#DemEnableConditionGroup_DTC_0x12de11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12de11] */
  /*    37 */  /* [#DemEnableConditionGroup_DTC_0x12df13, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12df13] */
  /*    38 */  /* [#DemEnableConditionGroup_DTC_0x12e012, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e012] */
  /*    39 */  /* [#DemEnableConditionGroup_DTC_0x12e111, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e111] */
  /*    40 */  /* [#DemEnableConditionGroup_DTC_0x12e213, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e213] */
  /*    41 */  /* [#DemEnableConditionGroup_DTC_0x12e319, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e319] */
  /*    42 */  /* [#DemEnableConditionGroup_DTC_0x12e712, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e712] */
  /*    43 */  /* [#DemEnableConditionGroup_DTC_0x12e811, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e811] */
  /*    44 */  /* [#DemEnableConditionGroup_DTC_0x12e912, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x12e912] */
  /*    45 */  /* [#DemEnableConditionGroup_DTC_0x12ea11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12ea11] */
  /*    46 */  /* [#DemEnableConditionGroup_DTC_0x12f316, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x12f316] */
  /*    47 */  /* [#DemEnableConditionGroup_DTC_0x12f917, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12f917] */
  /*    48 */  /* [#DemEnableConditionGroup_DTC_0x13e919, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x13e919] */
  /*    49 */  /* [#DemEnableConditionGroup_DTC_0x166C64, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x166C64] */
  /* Index        Referable Keys */
  /*    50 */  /* [#DemEnableConditionGroup_DTC_0x179e11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179e11] */
  /*    51 */  /* [#DemEnableConditionGroup_DTC_0x179e12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179e12] */
  /*    52 */  /* [#DemEnableConditionGroup_DTC_0x179f11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179f11] */
  /*    53 */  /* [#DemEnableConditionGroup_DTC_0x179f12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179f12] */
  /*    54 */  /* [#DemEnableConditionGroup_DTC_0x1a0064, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a0064] */
  /*    55 */  /* [#DemEnableConditionGroup_DTC_0x1a7104, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a7104] */
  /*    56 */  /* [#DemEnableConditionGroup_DTC_0x1a714b, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a714b] */
  /*    57 */  /* [#DemEnableConditionGroup_DTC_0x1a7172, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a7172] */
  /*    58 */  /* [#DemEnableConditionGroup_DTC_0xc07988, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xc07988] */
  /*    59 */  /* [#DemEnableConditionGroup_DTC_0xc08913, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xc08913] */
  /*    60 */  /* [#DemEnableConditionGroup_DTC_0xd18787, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd18787] */
  /*    61 */  /* [#DemEnableConditionGroup_DTC_0xd1a087, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd1a087] */
  /*    62 */  /* [#DemEnableConditionGroup_DTC_0xd20781, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd20781] */
  /*    63 */  /* [#DemEnableConditionGroup_DTC_0xd2a081, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd2a081] */
  /*    64 */  /* [#DemEnableConditionGroup_DTC_0xd38782, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd38782] */
  /*    65 */  /* [#DemEnableConditionGroup_DTC_0xd38783, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd38783] */
  /*    66 */  /* [#DemEnableConditionGroup_DTC_0xe00081, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00081] */
  /*    67 */  /* [#DemEnableConditionGroup_DTC_0xe00087, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00087] */
  /*    68 */  /* [#DemEnableConditionGroup_DTC_0xe00214, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00214] */
  /*    69 */  /* [#DemEnableConditionGroup_DTC_0xe00362, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00362] */

#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionGroupState
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionGroupState
  \brief  (DEM_CFG_SUPPORT_ENABLE_CONDITIONS == STD_ON) or there are internal EnableConditions. Table index: Condition group number. Table value: count of conditions in state 'enable'.
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Cfg_EnableConditionGroupStateType, DEM_VAR_NOINIT) Dem_Cfg_EnableConditionGroupState[70];
  /* Index        Referable Keys */
  /*     0 */  /* [##NoEnableConditionGroupConfigured, __Internal_ControlDtcSetting] */
  /*     1 */  /* [#DemEnableConditionGroup_DTC_0x056216, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x056216] */
  /*     2 */  /* [#DemEnableConditionGroup_DTC_0x056317, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x056317] */
  /*     3 */  /* [#DemEnableConditionGroup_DTC_0x0a0804, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0a0804] */
  /*     4 */  /* [#DemEnableConditionGroup_DTC_0x0a084b, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0a084b] */
  /*     5 */  /* [#DemEnableConditionGroup_DTC_0x0a9464, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0a9464] */
  /*     6 */  /* [#DemEnableConditionGroup_DTC_0x0af864, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0af864] */
  /*     7 */  /* [#DemEnableConditionGroup_DTC_0x0cf464, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x0cf464] */
  /*     8 */  /* [#DemEnableConditionGroup_DTC_0x108093, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x108093] */
  /*     9 */  /* [#DemEnableConditionGroup_DTC_0x10C413, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C413] */
  /*    10 */  /* [#DemEnableConditionGroup_DTC_0x10C512, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C512] */
  /*    11 */  /* [#DemEnableConditionGroup_DTC_0x10C613, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C613] */
  /*    12 */  /* [#DemEnableConditionGroup_DTC_0x10C713, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x10C713] */
  /*    13 */  /* [#DemEnableConditionGroup_DTC_0x120a11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120a11] */
  /*    14 */  /* [#DemEnableConditionGroup_DTC_0x120a12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120a12] */
  /*    15 */  /* [#DemEnableConditionGroup_DTC_0x120b11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120b11] */
  /*    16 */  /* [#DemEnableConditionGroup_DTC_0x120b12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120b12] */
  /*    17 */  /* [#DemEnableConditionGroup_DTC_0x120c64, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120c64] */
  /*    18 */  /* [#DemEnableConditionGroup_DTC_0x120c98, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120c98] */
  /*    19 */  /* [#DemEnableConditionGroup_DTC_0x120d64, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120d64] */
  /*    20 */  /* [#DemEnableConditionGroup_DTC_0x120d98, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x120d98] */
  /*    21 */  /* [#DemEnableConditionGroup_DTC_0x12d711, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d711] */
  /*    22 */  /* [#DemEnableConditionGroup_DTC_0x12d712, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d712] */
  /*    23 */  /* [#DemEnableConditionGroup_DTC_0x12d713, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d713] */
  /*    24 */  /* [#DemEnableConditionGroup_DTC_0x12d811, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d811] */
  /*    25 */  /* [#DemEnableConditionGroup_DTC_0x12d812, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d812] */
  /*    26 */  /* [#DemEnableConditionGroup_DTC_0x12d813, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d813] */
  /*    27 */  /* [#DemEnableConditionGroup_DTC_0x12d911, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d911] */
  /*    28 */  /* [#DemEnableConditionGroup_DTC_0x12d912, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d912] */
  /*    29 */  /* [#DemEnableConditionGroup_DTC_0x12d913, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12d913] */
  /*    30 */  /* [#DemEnableConditionGroup_DTC_0x12da11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12da11] */
  /*    31 */  /* [#DemEnableConditionGroup_DTC_0x12da12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12da12] */
  /*    32 */  /* [#DemEnableConditionGroup_DTC_0x12da13, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12da13] */
  /*    33 */  /* [#DemEnableConditionGroup_DTC_0x12db12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12db12] */
  /*    34 */  /* [#DemEnableConditionGroup_DTC_0x12dc11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12dc11] */
  /*    35 */  /* [#DemEnableConditionGroup_DTC_0x12dd12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12dd12] */
  /*    36 */  /* [#DemEnableConditionGroup_DTC_0x12de11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12de11] */
  /*    37 */  /* [#DemEnableConditionGroup_DTC_0x12df13, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12df13] */
  /*    38 */  /* [#DemEnableConditionGroup_DTC_0x12e012, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e012] */
  /*    39 */  /* [#DemEnableConditionGroup_DTC_0x12e111, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e111] */
  /*    40 */  /* [#DemEnableConditionGroup_DTC_0x12e213, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e213] */
  /*    41 */  /* [#DemEnableConditionGroup_DTC_0x12e319, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e319] */
  /*    42 */  /* [#DemEnableConditionGroup_DTC_0x12e712, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e712] */
  /*    43 */  /* [#DemEnableConditionGroup_DTC_0x12e811, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12e811] */
  /*    44 */  /* [#DemEnableConditionGroup_DTC_0x12e912, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x12e912] */
  /*    45 */  /* [#DemEnableConditionGroup_DTC_0x12ea11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12ea11] */
  /*    46 */  /* [#DemEnableConditionGroup_DTC_0x12f316, __Internal_ControlDtcSetting, DemEnableConditionDTC_0x12f316] */
  /*    47 */  /* [#DemEnableConditionGroup_DTC_0x12f917, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x12f917] */
  /*    48 */  /* [#DemEnableConditionGroup_DTC_0x13e919, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x13e919] */
  /*    49 */  /* [#DemEnableConditionGroup_DTC_0x166C64, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x166C64] */
  /* Index        Referable Keys */
  /*    50 */  /* [#DemEnableConditionGroup_DTC_0x179e11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179e11] */
  /*    51 */  /* [#DemEnableConditionGroup_DTC_0x179e12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179e12] */
  /*    52 */  /* [#DemEnableConditionGroup_DTC_0x179f11, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179f11] */
  /*    53 */  /* [#DemEnableConditionGroup_DTC_0x179f12, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x179f12] */
  /*    54 */  /* [#DemEnableConditionGroup_DTC_0x1a0064, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a0064] */
  /*    55 */  /* [#DemEnableConditionGroup_DTC_0x1a7104, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a7104] */
  /*    56 */  /* [#DemEnableConditionGroup_DTC_0x1a714b, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a714b] */
  /*    57 */  /* [#DemEnableConditionGroup_DTC_0x1a7172, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0x1a7172] */
  /*    58 */  /* [#DemEnableConditionGroup_DTC_0xc07988, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xc07988] */
  /*    59 */  /* [#DemEnableConditionGroup_DTC_0xc08913, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xc08913] */
  /*    60 */  /* [#DemEnableConditionGroup_DTC_0xd18787, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd18787] */
  /*    61 */  /* [#DemEnableConditionGroup_DTC_0xd1a087, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd1a087] */
  /*    62 */  /* [#DemEnableConditionGroup_DTC_0xd20781, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd20781] */
  /*    63 */  /* [#DemEnableConditionGroup_DTC_0xd2a081, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd2a081] */
  /*    64 */  /* [#DemEnableConditionGroup_DTC_0xd38782, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd38782] */
  /*    65 */  /* [#DemEnableConditionGroup_DTC_0xd38783, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xd38783] */
  /*    66 */  /* [#DemEnableConditionGroup_DTC_0xe00081, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00081] */
  /*    67 */  /* [#DemEnableConditionGroup_DTC_0xe00087, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00087] */
  /*    68 */  /* [#DemEnableConditionGroup_DTC_0xe00214, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00214] */
  /*    69 */  /* [#DemEnableConditionGroup_DTC_0xe00362, __Internal_ControlDtcSetting, DemEnableCondition_DTC_0xe00362] */

#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionState
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionState
  \brief  (DEM_CFG_SUPPORT_ENABLE_CONDITIONS == STD_ON) or there are internal EnableConditions. Table index: Condition ID. Table value: current condition state '0' disable, '1' enable.
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_EnableConditionStateType, DEM_VAR_NOINIT) Dem_Cfg_EnableConditionState[70];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EventDebounceValue
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EventDebounceValue
  \brief  size = DEM_G_NUMBER_OF_EVENTS
*/ 
#define DEM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Cfg_EventDebounceValueType, DEM_VAR_NOINIT) Dem_Cfg_EventDebounceValue[72];
  /* Index        Referable Keys */
  /*     0 */  /* [#EVENT_INVALID, Satellite#0] */
  /*     1 */  /* [DTC_0x0a084b, Satellite#0] */
  /*     2 */  /* [DTC_0x0a0804, Satellite#0] */
  /*     3 */  /* [DTC_0x0a9464, Satellite#0] */
  /*     4 */  /* [DTC_0x0af864, Satellite#0] */
  /*     5 */  /* [DTC_0x1a714b, Satellite#0] */
  /*     6 */  /* [DTC_0x1a0064, Satellite#0] */
  /*     7 */  /* [DTC_0x1a7104, Satellite#0] */
  /*     8 */  /* [DTC_0x1a7172, Satellite#0] */
  /*     9 */  /* [DTC_0x12d711, Satellite#0] */
  /*    10 */  /* [DTC_0x12d712, Satellite#0] */
  /*    11 */  /* [DTC_0x12d713, Satellite#0] */
  /*    12 */  /* [DTC_0x12d811, Satellite#0] */
  /*    13 */  /* [DTC_0x12d812, Satellite#0] */
  /*    14 */  /* [DTC_0x12d813, Satellite#0] */
  /*    15 */  /* [DTC_0x12d911, Satellite#0] */
  /*    16 */  /* [DTC_0x12d912, Satellite#0] */
  /*    17 */  /* [DTC_0x12d913, Satellite#0] */
  /*    18 */  /* [DTC_0x12da11, Satellite#0] */
  /*    19 */  /* [DTC_0x12da12, Satellite#0] */
  /*    20 */  /* [DTC_0x12da13, Satellite#0] */
  /*    21 */  /* [DTC_0x12db12, Satellite#0] */
  /*    22 */  /* [DTC_0x12dc11, Satellite#0] */
  /*    23 */  /* [DTC_0x12dd12, Satellite#0] */
  /*    24 */  /* [DTC_0x12de11, Satellite#0] */
  /*    25 */  /* [DTC_0x12df13, Satellite#0] */
  /*    26 */  /* [DTC_0x12e012, Satellite#0] */
  /*    27 */  /* [DTC_0x12e111, Satellite#0] */
  /*    28 */  /* [DTC_0x12e213, Satellite#0] */
  /*    29 */  /* [DTC_0x12e319, Satellite#0] */
  /*    30 */  /* [DTC_0x12e712, Satellite#0] */
  /*    31 */  /* [DTC_0x12e811, Satellite#0] */
  /*    32 */  /* [DTC_0x12e912, Satellite#0] */
  /*    33 */  /* [DTC_0x12ea11, Satellite#0] */
  /*    34 */  /* [DTC_0x12f917, Satellite#0] */
  /*    35 */  /* [DTC_0x120a11, Satellite#0] */
  /*    36 */  /* [DTC_0x120a12, Satellite#0] */
  /*    37 */  /* [DTC_0x120b11, Satellite#0] */
  /*    38 */  /* [DTC_0x120b12, Satellite#0] */
  /*    39 */  /* [DTC_0x120c64, Satellite#0] */
  /*    40 */  /* [DTC_0x120c98, Satellite#0] */
  /*    41 */  /* [DTC_0x120d64, Satellite#0] */
  /*    42 */  /* [DTC_0x120d98, Satellite#0] */
  /*    43 */  /* [DTC_0x179e11, Satellite#0] */
  /*    44 */  /* [DTC_0x179e12, Satellite#0] */
  /*    45 */  /* [DTC_0x179f11, Satellite#0] */
  /*    46 */  /* [DTC_0x179f12, Satellite#0] */
  /*    47 */  /* [DTC_0x056216, Satellite#0] */
  /*    48 */  /* [DTC_0x056317, Satellite#0] */
  /*    49 */  /* [DTC_0x108093, Satellite#0] */
  /* Index        Referable Keys */
  /*    50 */  /* [DTC_0xc07988, Satellite#0] */
  /*    51 */  /* [DTC_0xc08913, Satellite#0] */
  /*    52 */  /* [DTC_0xd1a087, Satellite#0] */
  /*    53 */  /* [DTC_0xd2a081, Satellite#0] */
  /*    54 */  /* [DTC_0xd18787, Satellite#0] */
  /*    55 */  /* [DTC_0xd20781, Satellite#0] */
  /*    56 */  /* [DTC_0xd38782, Satellite#0] */
  /*    57 */  /* [DTC_0xd38783, Satellite#0] */
  /*    58 */  /* [DTC_0xe00081, Satellite#0] */
  /*    59 */  /* [DTC_0xe00087, Satellite#0] */
  /*    60 */  /* [DTC_0xe00214, Satellite#0] */
  /*    61 */  /* [DTC_0xe00362, Satellite#0] */
  /*    62 */  /* [DTC_dummy2, Satellite#0] */
  /*    63 */  /* [DTC_dummy1, Satellite#0] */
  /*    64 */  /* [DTC_0x13e919, Satellite#0] */
  /*    65 */  /* [DTC_0x166c64, Satellite#0] */
  /*    66 */  /* [DTC_0x0cf464, Satellite#0] */
  /*    67 */  /* [DTC_0x10c413, Satellite#0] */
  /*    68 */  /* [DTC_0x10c512, Satellite#0] */
  /*    69 */  /* [DTC_0x10c613, Satellite#0] */
  /*    70 */  /* [DTC_0x10c713, Satellite#0] */
  /*    71 */  /* [DTC_0x12f316, Satellite#0] */

#define DEM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EventInternalStatus
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EventInternalStatus
  \brief  size = DEM_G_NUMBER_OF_EVENTS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Event_InternalStatusType, DEM_VAR_NOINIT) Dem_Cfg_EventInternalStatus[72];
  /* Index        Referable Keys */
  /*     0 */  /* [#EVENT_INVALID, Satellite#0] */
  /*     1 */  /* [DTC_0x0a084b, Satellite#0] */
  /*     2 */  /* [DTC_0x0a0804, Satellite#0] */
  /*     3 */  /* [DTC_0x0a9464, Satellite#0] */
  /*     4 */  /* [DTC_0x0af864, Satellite#0] */
  /*     5 */  /* [DTC_0x1a714b, Satellite#0] */
  /*     6 */  /* [DTC_0x1a0064, Satellite#0] */
  /*     7 */  /* [DTC_0x1a7104, Satellite#0] */
  /*     8 */  /* [DTC_0x1a7172, Satellite#0] */
  /*     9 */  /* [DTC_0x12d711, Satellite#0] */
  /*    10 */  /* [DTC_0x12d712, Satellite#0] */
  /*    11 */  /* [DTC_0x12d713, Satellite#0] */
  /*    12 */  /* [DTC_0x12d811, Satellite#0] */
  /*    13 */  /* [DTC_0x12d812, Satellite#0] */
  /*    14 */  /* [DTC_0x12d813, Satellite#0] */
  /*    15 */  /* [DTC_0x12d911, Satellite#0] */
  /*    16 */  /* [DTC_0x12d912, Satellite#0] */
  /*    17 */  /* [DTC_0x12d913, Satellite#0] */
  /*    18 */  /* [DTC_0x12da11, Satellite#0] */
  /*    19 */  /* [DTC_0x12da12, Satellite#0] */
  /*    20 */  /* [DTC_0x12da13, Satellite#0] */
  /*    21 */  /* [DTC_0x12db12, Satellite#0] */
  /*    22 */  /* [DTC_0x12dc11, Satellite#0] */
  /*    23 */  /* [DTC_0x12dd12, Satellite#0] */
  /*    24 */  /* [DTC_0x12de11, Satellite#0] */
  /*    25 */  /* [DTC_0x12df13, Satellite#0] */
  /*    26 */  /* [DTC_0x12e012, Satellite#0] */
  /*    27 */  /* [DTC_0x12e111, Satellite#0] */
  /*    28 */  /* [DTC_0x12e213, Satellite#0] */
  /*    29 */  /* [DTC_0x12e319, Satellite#0] */
  /*    30 */  /* [DTC_0x12e712, Satellite#0] */
  /*    31 */  /* [DTC_0x12e811, Satellite#0] */
  /*    32 */  /* [DTC_0x12e912, Satellite#0] */
  /*    33 */  /* [DTC_0x12ea11, Satellite#0] */
  /*    34 */  /* [DTC_0x12f917, Satellite#0] */
  /*    35 */  /* [DTC_0x120a11, Satellite#0] */
  /*    36 */  /* [DTC_0x120a12, Satellite#0] */
  /*    37 */  /* [DTC_0x120b11, Satellite#0] */
  /*    38 */  /* [DTC_0x120b12, Satellite#0] */
  /*    39 */  /* [DTC_0x120c64, Satellite#0] */
  /*    40 */  /* [DTC_0x120c98, Satellite#0] */
  /*    41 */  /* [DTC_0x120d64, Satellite#0] */
  /*    42 */  /* [DTC_0x120d98, Satellite#0] */
  /*    43 */  /* [DTC_0x179e11, Satellite#0] */
  /*    44 */  /* [DTC_0x179e12, Satellite#0] */
  /*    45 */  /* [DTC_0x179f11, Satellite#0] */
  /*    46 */  /* [DTC_0x179f12, Satellite#0] */
  /*    47 */  /* [DTC_0x056216, Satellite#0] */
  /*    48 */  /* [DTC_0x056317, Satellite#0] */
  /*    49 */  /* [DTC_0x108093, Satellite#0] */
  /* Index        Referable Keys */
  /*    50 */  /* [DTC_0xc07988, Satellite#0] */
  /*    51 */  /* [DTC_0xc08913, Satellite#0] */
  /*    52 */  /* [DTC_0xd1a087, Satellite#0] */
  /*    53 */  /* [DTC_0xd2a081, Satellite#0] */
  /*    54 */  /* [DTC_0xd18787, Satellite#0] */
  /*    55 */  /* [DTC_0xd20781, Satellite#0] */
  /*    56 */  /* [DTC_0xd38782, Satellite#0] */
  /*    57 */  /* [DTC_0xd38783, Satellite#0] */
  /*    58 */  /* [DTC_0xe00081, Satellite#0] */
  /*    59 */  /* [DTC_0xe00087, Satellite#0] */
  /*    60 */  /* [DTC_0xe00214, Satellite#0] */
  /*    61 */  /* [DTC_0xe00362, Satellite#0] */
  /*    62 */  /* [DTC_dummy2, Satellite#0] */
  /*    63 */  /* [DTC_dummy1, Satellite#0] */
  /*    64 */  /* [DTC_0x13e919, Satellite#0] */
  /*    65 */  /* [DTC_0x166c64, Satellite#0] */
  /*    66 */  /* [DTC_0x0cf464, Satellite#0] */
  /*    67 */  /* [DTC_0x10c413, Satellite#0] */
  /*    68 */  /* [DTC_0x10c512, Satellite#0] */
  /*    69 */  /* [DTC_0x10c613, Satellite#0] */
  /*    70 */  /* [DTC_0x10c713, Satellite#0] */
  /*    71 */  /* [DTC_0x12f316, Satellite#0] */

#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FilterInfoTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FilterInfoTable
  \brief  size = DEM_CFG_NUMBER_OF_FILTER
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_FilterData_InfoType, DEM_VAR_NOINIT) Dem_Cfg_FilterInfoTable[2];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FilterReportedEvents
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FilterReportedEvents
  \brief  size = ceiling( DEM_G_NUMBER_OF_EVENTS / 8 )
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_FilterReportedEventsType, DEM_VAR_NOINIT) Dem_Cfg_FilterReportedEvents[9];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FreezeFrameIteratorTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FreezeFrameIteratorTable
  \brief  size = DEM_CFG_NUMBER_OF_FREEZEFRAMEITERATORS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_FreezeFrameIterator_FilterType, DEM_VAR_NOINIT) Dem_Cfg_FreezeFrameIteratorTable[2];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryCommitNumber
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryCommitNumber
  \brief  The array contains these items: Admin, Status, 32 * Primary
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Cfg_MemoryCommitNumberType, DEM_VAR_NOINIT) Dem_Cfg_MemoryCommitNumber[34];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryStatus
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryStatus
  \brief  The array contains these items: Admin, Status, 32 * Primary
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_MemoryStatusType, DEM_VAR_NOINIT) Dem_Cfg_MemoryStatus[34];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryChronology
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_PrimaryChronology
  \brief  size = DEM_CFG_GLOBAL_PRIMARY_SIZE
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_MemoryIndexType, DEM_VAR_NOINIT) Dem_Cfg_PrimaryChronology[32];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_0
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_0;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_1
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_1;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_10
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_10;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_11
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_11;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_12
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_12;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_13
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_13;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_14
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_14;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_15
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_15;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_16
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_16;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_17
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_17;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_18
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_18;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_19
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_19;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_2
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_2;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_20
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_20;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_21
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_21;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_22
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_22;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_23
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_23;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_24
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_24;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_25
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_25;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_26
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_26;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_27
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_27;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_28
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_28;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_29
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_29;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_3
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_3;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_30
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_30;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_31
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_31;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_4
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_4;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_5
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_5;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_6
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_6;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_7
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_7;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_8
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_8;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_9
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_9;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ReadoutBuffer
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_ReadoutBufferEntryType, DEM_VAR_NOINIT) Dem_Cfg_ReadoutBuffer[2];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ReportedEventsOfFilter
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_ReportedEventsOfFilter
  \brief  size = DEM_CFG_NUMBER_OF_FILTER
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_ReportedEventsType, DEM_VAR_NOINIT) Dem_Cfg_ReportedEventsOfFilter[2];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_SatelliteInfo0
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_SatelliteInfo0
  \brief  Buffer for satellite data on OsApplication "0"
*/ 
#define DEM_START_SEC_0_VAR_ZERO_INIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_SatelliteInfoType0, DEM_VAR_ZERO_INIT) Dem_Cfg_SatelliteInfo0 = {0uL};
#define DEM_STOP_SEC_0_VAR_ZERO_INIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_StatusData
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_StatusDataType, DEM_NVM_DATA_NOINIT) Dem_Cfg_StatusData;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL FUNCTIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/



/**********************************************************************************************************************
  END OF FILE: Dem_Lcfg.c     [Vector, VARIANT-PRE-COMPILE, 18.01.00.114202]
**********************************************************************************************************************/

