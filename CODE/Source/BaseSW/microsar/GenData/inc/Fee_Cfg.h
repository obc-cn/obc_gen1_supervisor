/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Fee_Cfg.h $                                                **
**                                                                           **
**  $CC VERSION : \main\26 $                                                 **
**                                                                           **
**  DATE, TIME: 2020-11-23, 11:37:27                                         **
**                                                                           **
**  GENERATOR : Build b141014-0350                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : FEE configuration generated out of ECU configuration      **
**                   file (Fee.bmd)                                          **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/

#ifndef  FEE_CFG_H
#define  FEE_CFG_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

/* Typedefs Imported from Memory Abstract Interface */ 
#include "MemIf_Types.h"

/* Callback functions imported from NvM Module */
#include "NvM_Cbk.h"

/* Functions imported from Fls Module */
#include "Fls_17_Pmu.h"

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/* FEE AS version information */
#define FEE_AS_VERSION (403)
#define FEE_AR_RELEASE_MAJOR_VERSION  (4U)
#define FEE_AR_RELEASE_MINOR_VERSION  (0U)
#define FEE_AR_RELEASE_REVISION_VERSION  (3U)

/* Vendor specific implementation version information */
#define FEE_SW_MAJOR_VERSION  (2U)
#define FEE_SW_MINOR_VERSION  (6U)
#define FEE_SW_PATCH_VERSION  (0U)

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/
                        
/*******************************************************************************
**                    Static configuration parameters                         **
*******************************************************************************/

/* Development error detection enabled/disabled */
#define FEE_DEV_ERROR_DETECT       (STD_OFF)

/* Fee_GetVersionInfo API enabled/disabled */
#define FEE_VERSION_INFO_API       (STD_OFF)

/* Fee_GetCycleCount API enabled/disabled */
#define FEE_GET_CYCLE_COUNT_API    (STD_ON)

/* Fee_SetMode API enabled/disabled */
#define FEE_SET_MODE_SUPPORTED     (STD_OFF)

/* Fee_17_GetPrevData API enabled/disabled */
#define FEE_GET_PREV_DATA_API      (STD_OFF)

#define FEE_MAX_BYTES_PER_CYCLE      (512U)
/* FEE programs state pages upon detection of virgin flash */
#define FEE_VIRGIN_FLASH_ILLEGAL_STATE      (STD_OFF)

/* Enable/Disable Debug support  */
#define FEE_DEBUG_SUPPORT     (STD_OFF)

/* Erase suspend/resume feature supported in FLS */
#define FEE_FLS_SUPPORTS_ERASE_SUSPEND  (STD_OFF)

/* DFlash WordLine size */
#define FEE_DFLASH_WORDLINE_SIZE     (512U)

#define FEE_CONTINUE          (0U)
#define FEE_STOP_AT_GC        (1U)

#define FEE_UNCFG_BLK_OVERFLOW_HANDLE    (FEE_CONTINUE)

/* Virtual page size, i.e., DF_EEPROM page size */
#define FEE_VIRTUAL_PAGE_SIZE      (8U)

/* Logical block's overhead in bytes */
#define FEE_BLOCK_OVERHEAD         (17U)

/* Logical block's data page overhead in bytes */
#define FEE_PAGE_OVERHEAD          (1U)

/* Maximum blocking (delay) time in ms */
#define FEE_MAXIMUM_BLOCKING_TIME  (10U)

/* Maximum number of configured blocks to be handled */
#define FEE_MAX_BLOCK_COUNT        (48U)

/* Symbolic names of logical blocks */
#ifdef FeeConf_FeeBlockConfiguration_FeeBlockConfiguration 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeBlockConfiguration already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeBlockConfiguration ((uint16)2)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeBlockConfiguration */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock0 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock0 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock0 ((uint16)26)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock0 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock1 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock1 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock1 ((uint16)28)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock1 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock2 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock2 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock2 ((uint16)30)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock2 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock3 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock3 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock3 ((uint16)32)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock3 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemStatusDataBlock 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemStatusDataBlock already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemStatusDataBlock ((uint16)12)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemStatusDataBlock */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApPCOM_NvPCOMBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApPCOM_NvPCOMBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApPCOM_NvPCOMBlockNeed ((uint16)20)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApPCOM_NvPCOMBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApWUM_NvWUMBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApWUM_NvWUMBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApWUM_NvWUMBlockNeed ((uint16)18)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApWUM_NvWUMBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeMACAdressDataBlock 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeMACAdressDataBlock already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeMACAdressDataBlock ((uint16)14)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeMACAdressDataBlock */

#ifdef FeeConf_FeeBlockConfiguration_FeeSCCSessionIDBlock 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeSCCSessionIDBlock already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeSCCSessionIDBlock ((uint16)16)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeSCCSessionIDBlock */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemAdminDataBlock0 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemAdminDataBlock0 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemAdminDataBlock0 ((uint16)24)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemAdminDataBlock0 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock10 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock10 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock10 ((uint16)46)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock10 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock11 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock11 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock11 ((uint16)48)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock11 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock12 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock12 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock12 ((uint16)50)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock12 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock13 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock13 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock13 ((uint16)52)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock13 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock14 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock14 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock14 ((uint16)54)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock14 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock15 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock15 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock15 ((uint16)56)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock15 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock16 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock16 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock16 ((uint16)58)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock16 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock17 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock17 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock17 ((uint16)60)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock17 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock18 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock18 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock18 ((uint16)62)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock18 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock19 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock19 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock19 ((uint16)64)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock19 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock20 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock20 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock20 ((uint16)66)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock20 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock21 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock21 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock21 ((uint16)68)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock21 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock22 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock22 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock22 ((uint16)70)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock22 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock23 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock23 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock23 ((uint16)72)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock23 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock24 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock24 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock24 ((uint16)74)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock24 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock25 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock25 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock25 ((uint16)84)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock25 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock26 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock26 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock26 ((uint16)76)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock26 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock27 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock27 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock27 ((uint16)78)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock27 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock28 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock28 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock28 ((uint16)80)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock28 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock29 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock29 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock29 ((uint16)82)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock29 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock4 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock4 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock4 ((uint16)34)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock4 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock5 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock5 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock5 ((uint16)36)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock5 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock6 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock6 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock6 ((uint16)38)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock6 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock7 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock7 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock7 ((uint16)40)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock7 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock8 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock8 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock8 ((uint16)42)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock8 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock9 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock9 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock9 ((uint16)44)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock9 */

#ifdef FeeConf_FeeBlockConfiguration_FeeBlockConfiguration_cons0 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeBlockConfiguration_cons0 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeBlockConfiguration_cons0 ((uint16)3)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeBlockConfiguration_cons0 */

#ifdef FeeConf_FeeBlockConfiguration_FeeCtApJDD_JDDNvBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCtApJDD_JDDNvBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCtApJDD_JDDNvBlockNeed ((uint16)4)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCtApJDD_JDDNvBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApMSC_NvMSCBlockNeedPowerLatchFlag 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApMSC_NvMSCBlockNeedPowerLatchFlag already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApMSC_NvMSCBlockNeedPowerLatchFlag ((uint16)6)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApMSC_NvMSCBlockNeedPowerLatchFlag */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApPSH_NvPSHBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApPSH_NvPSHBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApPSH_NvPSHBlockNeed ((uint16)8)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApPSH_NvPSHBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApPXL_PXLNvBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApPXL_PXLNvBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApPXL_PXLNvBlockNeed ((uint16)10)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApPXL_PXLNvBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApCPT_CPTNvBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApCPT_CPTNvBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApCPT_CPTNvBlockNeed ((uint16)22)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApCPT_CPTNvBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApDGN_DGN_EOL_NVM_Needs 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApDGN_DGN_EOL_NVM_Needs already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApDGN_DGN_EOL_NVM_Needs ((uint16)86)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApDGN_DGN_EOL_NVM_Needs */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpApPLT_PLTNvMNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpApPLT_PLTNvMNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpApPLT_PLTNvMNeed ((uint16)88)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpApPLT_PLTNvMNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeCpHwAbsAIM_AIMVoltRefNvBlockNeed 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeCpHwAbsAIM_AIMVoltRefNvBlockNeed already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeCpHwAbsAIM_AIMVoltRefNvBlockNeed ((uint16)90)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeCpHwAbsAIM_AIMVoltRefNvBlockNeed */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock30 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock30 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock30 ((uint16)92)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock30 */

#ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock31 
/* to prevent double declaration */
#error FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock31 already defined
#else 
#define FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock31 ((uint16)94)
#endif /* #ifdef FeeConf_FeeBlockConfiguration_FeeDemPrimaryDataBlock31 */


#define FEE_DISABLE_DEM_REPORT   (0U)
#define FEE_ENABLE_DEM_REPORT    (1U)

/* DEM Configurations */
#define FEE_GC_INIT_DEM_REPORT       (FEE_DISABLE_DEM_REPORT)
#define FEE_WRITE_DEM_REPORT         (FEE_DISABLE_DEM_REPORT)
#define FEE_READ_DEM_REPORT          (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_WRITE_DEM_REPORT      (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_READ_DEM_REPORT       (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_ERASE_DEM_REPORT      (FEE_DISABLE_DEM_REPORT)
#define FEE_INVALIDATE_DEM_REPORT    (FEE_DISABLE_DEM_REPORT)
#define FEE_WRITE_CYCLES_DEM_REPORT  (FEE_DISABLE_DEM_REPORT)
#define FEE_GC_TRIG_DEM_REPORT       (FEE_DISABLE_DEM_REPORT)
#define FEE_UNCFG_BLK_DEM_REPORT     (FEE_DISABLE_DEM_REPORT)
#define FEE_DEM_ENABLED              (STD_OFF)

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/

#define FEE_CONFIG_PTR      (Fee_CfgPtr)

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/

#endif /* #ifndef FEE_CFG_H */
