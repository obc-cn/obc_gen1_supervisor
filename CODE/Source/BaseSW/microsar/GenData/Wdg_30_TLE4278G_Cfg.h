/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Wdg_30_TLE4278G
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Wdg_30_TLE4278G_Cfg.h
 *   Generation Time: 2020-08-19 13:07:47
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !(defined WDG_30_TLE4278G_CFG_H)
#define WDG_30_TLE4278G_CFG_H


/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"
#include "WdgIf_Types.h"
#include "Dem.h"

#include "Gpt.h"

/**********************************************************************************************************************
  USER CONFIG FILE
**********************************************************************************************************************/
/* User Config File Start */
#define GptConf_GptChannelConfiguration_GptChannelConfiguration_0 GptConf_GptChannel_GptChannelConfiguration_0
/* User Config File End */


/**********************************************************************************************************************
  DIO COMPATIBILITY
**********************************************************************************************************************/
#if !defined (Wdg_30_TLE4278G_DioReadChannel) || !defined (Wdg_30_TLE4278G_DioWriteChannel)
#include "Dio.h"
#endif

#if !defined (Wdg_30_TLE4278G_DioReadChannel)
# define Wdg_30_TLE4278G_DioReadChannel Dio_ReadChannel
#endif

#if !defined (Wdg_30_TLE4278G_DioWriteChannel)
# define Wdg_30_TLE4278G_DioWriteChannel Dio_WriteChannel
#endif



/**********************************************************************************************************************
 *  GENERAL DEFINE BLOCK
 *********************************************************************************************************************/
#ifndef WDG_30_TLE4278G_USE_DUMMY_STATEMENT
#define WDG_30_TLE4278G_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef WDG_30_TLE4278G_DUMMY_STATEMENT
#define WDG_30_TLE4278G_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef WDG_30_TLE4278G_DUMMY_STATEMENT_CONST
#define WDG_30_TLE4278G_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef WDG_30_TLE4278G_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define WDG_30_TLE4278G_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef WDG_30_TLE4278G_ATOMIC_VARIABLE_ACCESS
#define WDG_30_TLE4278G_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef WDG_30_TLE4278G_PROCESSOR_TC234
#define WDG_30_TLE4278G_PROCESSOR_TC234
#endif
#ifndef WDG_30_TLE4278G_COMP_GNU
#define WDG_30_TLE4278G_COMP_GNU
#endif
#ifndef WDG_30_TLE4278G_GEN_GENERATOR_MSR
#define WDG_30_TLE4278G_GEN_GENERATOR_MSR
#endif
#ifndef WDG_30_TLE4278G_CPUTYPE_BITORDER_LSB2MSB
#define WDG_30_TLE4278G_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef WDG_30_TLE4278G_CONFIGURATION_VARIANT_PRECOMPILE
#define WDG_30_TLE4278G_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef WDG_30_TLE4278G_CONFIGURATION_VARIANT_LINKTIME
#define WDG_30_TLE4278G_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef WDG_30_TLE4278G_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define WDG_30_TLE4278G_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef WDG_30_TLE4278G_CONFIGURATION_VARIANT
#define WDG_30_TLE4278G_CONFIGURATION_VARIANT WDG_30_TLE4278G_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef WDG_30_TLE4278G_POSTBUILD_VARIANT_SUPPORT
#define WDG_30_TLE4278G_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
*  INTEGER DEFINES
*********************************************************************************************************************/
/*! \brief Version defines of generator version */
#define WDG_30_TLE4278G_CFG_MAJOR_VERSION        4uL
#define WDG_30_TLE4278G_CFG_MINOR_VERSION        0uL
#define WDG_30_TLE4278G_CFG_PATCH_VERSION        0uL

/*! \brief Version defines that are used to check the compatibility of the generated data */
#define WDG_30_TLE4278G_CFG_COMP_MAJOR_VERSION   2uL
#define WDG_30_TLE4278G_CFG_COMP_MINOR_VERSION   0uL
#define WDG_30_TLE4278G_CFG_COMP_PATCH_VERSION   0uL

/*! \brief Maximum timeout (in ms) to which the watchdog trigger condition can be set */
#define WDG_30_TLE4278G_MAX_TIMEOUT              255uL

/*! \brief Initial timeout (in ms) for the trigger condition */
#define WDG_30_TLE4278G_INITIAL_TIMEOUT          255uL 


/*! \brief Max index for DioModeLevelsFast */
#define WDG_30_TLE4278G_MODE_FAST_IDX_MAX        1uL

/*! \brief Max index for DioModeLevelsSlow */
#define WDG_30_TLE4278G_MODE_SLOW_IDX_MAX        1uL

/*! \brief Max index for DioModeLevelsOff */
#define WDG_30_TLE4278G_MODE_OFF_IDX_MAX         1uL


/**********************************************************************************************************************
*  SWITCH DEFINES
*********************************************************************************************************************/
/*! \brief Enables / disables development error detection */
#ifndef WDG_30_TLE4278G_DEV_ERROR_DETECT
#define WDG_30_TLE4278G_DEV_ERROR_DETECT         STD_OFF
#endif

/*! \brief Enables / disables development error reporting (enabled whenever error detection is configured) */
#ifndef WDG_30_TLE4278G_DEV_ERROR_REPORT
#define WDG_30_TLE4278G_DEV_ERROR_REPORT         STD_OFF
#endif

/*! \brief Allow / forbid disabling the watchdog during runtime */
#define WDG_30_TLE4278G_DISABLE_ALLOWED          STD_ON

/* \brief Enables / disables the version information API (WDG_30_TLE4278G_GetVersionInfo) */
#define WDG_30_TLE4278G_VERSION_INFO_API         STD_OFF

/* \brief Enables / disables ASR3 compatibile trigger API (provide wrapper API WDG_30_TLE4278G_Trigger) */
#define WDG_30_TLE4278G_ASR3X_COMPATIBILITY      STD_OFF

/*! \brief Defines whether made change is perfomed immediately or after next trigger */
#define WDG_30_TLE4278G_MODE_CHANGE_ASYNC        STD_OFF

/*! \brief Modes supported by the watchdog driver (dependent on configuration) */
#define WDG_30_TLE4278G_SUPPORTED_MODES          WDG_30_TLE4278G_MODES_ALL

/*! \brief Indicates wether trigger signal is configured with an asynchronous duty cycle. */
#define WDG_30_TLE4278G_ASYNC_DUTY_CYCLE         STD_ON



/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


typedef struct 
{
  Dio_ChannelType channel;
  Dio_LevelType level;
} Wdg_30_TLE4278G_ModeDioLevelAssignmentType;


/**   \brief  type used in Wdg_30_TLE4278G_Runtime */
typedef struct
{
  Dem_EventIdType EModeFailed /*  DEM event which shall be issued when setting a watchdog mode fails.  */ ;
  Dem_EventIdType EDisableRejected /*  DEM event which shall be issued when mode switch fails because it would disable the watchdog.  */ ;
  WdgIf_ModeType DefaultMode /*  DefaultMode for watchdog driver initialization.  */ ;
  Dio_ChannelType DioPinWdiChannel /*  Dio Channel that is connected to watchdog input pin of device.  */ ;
  Dio_LevelType DioPinWdiInitLevel /*  Physical Level to which DioPinWdiChannel is set during module initialization.   */ ;
  Wdg_30_TLE4278G_ModeDioLevelAssignmentType DioModeLevelsFast[1] /*  Dio Level assignments to set Wdg hardware to WDGIF_FAST_MODE   */ ;
  Wdg_30_TLE4278G_ModeDioLevelAssignmentType DioModeLevelsSlow[1] /*  Dio Level assignments to set Wdg hardware to WDGIF_SLOW_MODE   */ ;
  Wdg_30_TLE4278G_ModeDioLevelAssignmentType DioModeLevelsOff[1] /*  Dio Level assignments to set Wdg hardware to WDGIF_OFF_MODE  */ ;
  Gpt_ChannelType TriggerTimer /*  Gpt Channel used for trigger timing  */ ;
  Gpt_ValueType TriggerTimerCounter[2] /*  Trigger cycle (in Gpt ticks), used for Gpt Channel configuration  */ ;
  Gpt_ValueType RecoverTimerCounter[2] /*  Recover cycle (in Gpt ticks)  */ ;
  uint16 TriggerCycleDuration[2] /*  Trigger cycle (in ms), used for TriggerCondition calculation  */ ;
} Wdg_30_TLE4278G_ConfigType;



/**********************************************************************************************************************
 * GLOBAL CONFIGURATION DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
  Wdg_30_TLE4278G_Runtime
**********************************************************************************************************************/
#define WDG_30_TLE4278G_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** 
  \var    Wdg_30_TLE4278G_Runtime
  \brief  Stores configuration(s) for hardware watchdog.
*/
extern CONST(Wdg_30_TLE4278G_ConfigType, WDG_30_TLE4278G_CONST) Wdg_30_TLE4278G_Runtime[1];

#define WDG_30_TLE4278G_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define WDG_30_TLE4278G_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** 
  \var    Wdg_30_TLE4278G_ConfigPtr
  \brief  Stores a pointer to the components initialized configuration.
*/
extern P2CONST(Wdg_30_TLE4278G_ConfigType, WDG_30_TLE4278G_VAR_NOINIT, WDG_30_TLE4278G_CONST) Wdg_30_TLE4278G_ConfigPtr;

#define WDG_30_TLE4278G_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*! \brief Access macros for the configuration(s) */
#define Wdg_30_TLE4278G_Config_Ptr0       &Wdg_30_TLE4278G_Runtime[0]


/**********************************************************************************************************************
 * GLOBAL MACROS
 *********************************************************************************************************************/
#define WDG_30_TLE4278G_MODE_INDEX(X)        (uint8)(X)-1u /* PRQA S 3453 */ /* MD_MSR_19.6 */

 

#endif /* WDG_30_TLE4278G_CFG_H */

