/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Type.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header file containing user defined AUTOSAR types and RTE structures
 *********************************************************************************************************************/

/* double include prevention */
#ifndef RTE_TYPE_H
# define RTE_TYPE_H

# include "Rte.h"

/* PRQA S 1039 EOF */ /* MD_Rte_1039 */

/**********************************************************************************************************************
 * Data type definitions
 *********************************************************************************************************************/

# define Rte_TypeDef_ABS_VehSpd
typedef uint16 ABS_VehSpd;

# define Rte_TypeDef_ABS_VehSpdValidFlag
typedef boolean ABS_VehSpdValidFlag;

# define Rte_TypeDef_BMS_AuxBattVolt
typedef uint16 BMS_AuxBattVolt;

# define Rte_TypeDef_BMS_DCRelayVoltage
typedef uint16 BMS_DCRelayVoltage;

# define Rte_TypeDef_BMS_Fault
typedef uint16 BMS_Fault;

# define Rte_TypeDef_BMS_HighestChargeCurrentAllow
typedef uint16 BMS_HighestChargeCurrentAllow;

# define Rte_TypeDef_BMS_HighestChargeVoltageAllow
typedef uint16 BMS_HighestChargeVoltageAllow;

# define Rte_TypeDef_BMS_OnBoardChargerEnable
typedef boolean BMS_OnBoardChargerEnable;

# define Rte_TypeDef_BMS_QuickChargeConnectorState
typedef boolean BMS_QuickChargeConnectorState;

# define Rte_TypeDef_BMS_RelayOpenReq
typedef boolean BMS_RelayOpenReq;

# define Rte_TypeDef_BMS_SOC
typedef uint16 BMS_SOC;

# define Rte_TypeDef_BMS_Voltage
typedef uint16 BMS_Voltage;

# define Rte_TypeDef_BSI_PostDriveWakeup
typedef boolean BSI_PostDriveWakeup;

# define Rte_TypeDef_BSI_PreDriveWakeup
typedef boolean BSI_PreDriveWakeup;

# define Rte_TypeDef_BSI_VCUSynchroGPC
typedef boolean BSI_VCUSynchroGPC;

# define Rte_TypeDef_COUPURE_CONSO_CTP
typedef boolean COUPURE_CONSO_CTP;

# define Rte_TypeDef_COUPURE_CONSO_CTPE2
typedef boolean COUPURE_CONSO_CTPE2;

# define Rte_TypeDef_CalVDCLinkRequiredMonophasic
typedef unsigned char CalVDCLinkRequiredMonophasic;

# define Rte_TypeDef_DATA0
typedef uint8 DATA0;

# define Rte_TypeDef_DATA1
typedef uint8 DATA1;

# define Rte_TypeDef_DATA2
typedef uint8 DATA2;

# define Rte_TypeDef_DATA3
typedef uint8 DATA3;

# define Rte_TypeDef_DATA4
typedef uint8 DATA4;

# define Rte_TypeDef_DATA5
typedef uint8 DATA5;

# define Rte_TypeDef_DATA6
typedef uint8 DATA6;

# define Rte_TypeDef_DATA7
typedef uint8 DATA7;

# define Rte_TypeDef_DATA_ACQ_JDD_BSI_2
typedef uint8 DATA_ACQ_JDD_BSI_2;

# define Rte_TypeDef_DCDC_CurrentReference
typedef uint16 DCDC_CurrentReference;

# define Rte_TypeDef_DCDC_HighVoltConnectionAllowed
typedef boolean DCDC_HighVoltConnectionAllowed;

# define Rte_TypeDef_DCDC_InputCurrent
typedef uint8 DCDC_InputCurrent;

# define Rte_TypeDef_DCDC_InputVoltage
typedef uint16 DCDC_InputVoltage;

# define Rte_TypeDef_DCDC_OBCDCDCRTPowerLoad
typedef uint8 DCDC_OBCDCDCRTPowerLoad;

# define Rte_TypeDef_DCDC_OVERTEMP
typedef boolean DCDC_OVERTEMP;

# define Rte_TypeDef_DCDC_OutputCurrent
typedef uint16 DCDC_OutputCurrent;

# define Rte_TypeDef_DCDC_OutputVoltage
typedef uint8 DCDC_OutputVoltage;

# define Rte_TypeDef_DCDC_Temperature
typedef uint8 DCDC_Temperature;

# define Rte_TypeDef_DCDC_VoltageReference
typedef uint16 DCDC_VoltageReference;

# define Rte_TypeDef_DCHV_ADC_IOUT
typedef uint16 DCHV_ADC_IOUT;

# define Rte_TypeDef_DCHV_ADC_NTC_MOD_5
typedef uint8 DCHV_ADC_NTC_MOD_5;

# define Rte_TypeDef_DCHV_ADC_NTC_MOD_6
typedef uint8 DCHV_ADC_NTC_MOD_6;

# define Rte_TypeDef_DCHV_ADC_VOUT
typedef uint16 DCHV_ADC_VOUT;

# define Rte_TypeDef_DCHV_Command_Current_Reference
typedef uint16 DCHV_Command_Current_Reference;

# define Rte_TypeDef_DCHV_IOM_ERR_CAP_FAIL_H
typedef boolean DCHV_IOM_ERR_CAP_FAIL_H;

# define Rte_TypeDef_DCHV_IOM_ERR_CAP_FAIL_L
typedef boolean DCHV_IOM_ERR_CAP_FAIL_L;

# define Rte_TypeDef_DCHV_IOM_ERR_OC_IOUT
typedef boolean DCHV_IOM_ERR_OC_IOUT;

# define Rte_TypeDef_DCHV_IOM_ERR_OC_NEG
typedef boolean DCHV_IOM_ERR_OC_NEG;

# define Rte_TypeDef_DCHV_IOM_ERR_OC_POS
typedef boolean DCHV_IOM_ERR_OC_POS;

# define Rte_TypeDef_DCHV_IOM_ERR_OT
typedef boolean DCHV_IOM_ERR_OT;

# define Rte_TypeDef_DCHV_IOM_ERR_OT_IN
typedef boolean DCHV_IOM_ERR_OT_IN;

# define Rte_TypeDef_DCHV_IOM_ERR_OT_NTC_MOD5
typedef boolean DCHV_IOM_ERR_OT_NTC_MOD5;

# define Rte_TypeDef_DCHV_IOM_ERR_OT_NTC_MOD6
typedef boolean DCHV_IOM_ERR_OT_NTC_MOD6;

# define Rte_TypeDef_DCHV_IOM_ERR_OV_VOUT
typedef boolean DCHV_IOM_ERR_OV_VOUT;

# define Rte_TypeDef_DCHV_IOM_ERR_UV_12V
typedef boolean DCHV_IOM_ERR_UV_12V;

# define Rte_TypeDef_DCLV_Applied_Derating_Factor
typedef uint16 DCLV_Applied_Derating_Factor;

# define Rte_TypeDef_DCLV_Input_Current
typedef uint8 DCLV_Input_Current;

# define Rte_TypeDef_DCLV_Input_Voltage
typedef uint16 DCLV_Input_Voltage;

# define Rte_TypeDef_DCLV_Measured_Current
typedef uint16 DCLV_Measured_Current;

# define Rte_TypeDef_DCLV_Measured_Voltage
typedef uint8 DCLV_Measured_Voltage;

# define Rte_TypeDef_DCLV_OC_A_HV_FAULT
typedef boolean DCLV_OC_A_HV_FAULT;

# define Rte_TypeDef_DCLV_OC_A_LV_FAULT
typedef boolean DCLV_OC_A_LV_FAULT;

# define Rte_TypeDef_DCLV_OC_B_HV_FAULT
typedef boolean DCLV_OC_B_HV_FAULT;

# define Rte_TypeDef_DCLV_OC_B_LV_FAULT
typedef boolean DCLV_OC_B_LV_FAULT;

# define Rte_TypeDef_DCLV_OT_FAULT
typedef boolean DCLV_OT_FAULT;

# define Rte_TypeDef_DCLV_OV_INT_A_FAULT
typedef boolean DCLV_OV_INT_A_FAULT;

# define Rte_TypeDef_DCLV_OV_INT_B_FAULT
typedef boolean DCLV_OV_INT_B_FAULT;

# define Rte_TypeDef_DCLV_OV_UV_HV_FAULT
typedef boolean DCLV_OV_UV_HV_FAULT;

# define Rte_TypeDef_DCLV_PWM_STOP
typedef boolean DCLV_PWM_STOP;

# define Rte_TypeDef_DCLV_Power
typedef uint16 DCLV_Power;

# define Rte_TypeDef_DCLV_T_L_BUCK
typedef uint8 DCLV_T_L_BUCK;

# define Rte_TypeDef_DCLV_T_PP_A
typedef uint8 DCLV_T_PP_A;

# define Rte_TypeDef_DCLV_T_PP_B
typedef uint8 DCLV_T_PP_B;

# define Rte_TypeDef_DCLV_T_SW_BUCK_A
typedef uint8 DCLV_T_SW_BUCK_A;

# define Rte_TypeDef_DCLV_T_SW_BUCK_B
typedef uint8 DCLV_T_SW_BUCK_B;

# define Rte_TypeDef_DCLV_T_TX_PP
typedef uint8 DCLV_T_TX_PP;

# define Rte_TypeDef_DCLV_Temp_Derating_Factor
typedef uint16 DCLV_Temp_Derating_Factor;

# define Rte_TypeDef_DCLV_VoltageReference
typedef uint8 DCLV_VoltageReference;

# define Rte_TypeDef_DIAG_INTEGRA_ELEC
typedef boolean DIAG_INTEGRA_ELEC;

# define Rte_TypeDef_Debug1_1
typedef uint8 Debug1_1;

# define Rte_TypeDef_Debug1_2
typedef uint8 Debug1_2;

# define Rte_TypeDef_Debug1_3
typedef uint8 Debug1_3;

# define Rte_TypeDef_Debug1_4
typedef uint8 Debug1_4;

# define Rte_TypeDef_Debug1_5
typedef uint8 Debug1_5;

# define Rte_TypeDef_Debug1_6
typedef uint8 Debug1_6;

# define Rte_TypeDef_Debug1_7
typedef uint8 Debug1_7;

# define Rte_TypeDef_Debug2_0
typedef uint8 Debug2_0;

# define Rte_TypeDef_Debug2_1
typedef uint8 Debug2_1;

# define Rte_TypeDef_Debug2_2
typedef uint8 Debug2_2;

# define Rte_TypeDef_Debug2_3
typedef uint8 Debug2_3;

# define Rte_TypeDef_Debug2_4
typedef uint8 Debug2_4;

# define Rte_TypeDef_Debug2_5
typedef uint8 Debug2_5;

# define Rte_TypeDef_Debug2_6
typedef uint8 Debug2_6;

# define Rte_TypeDef_Debug2_7
typedef uint8 Debug2_7;

# define Rte_TypeDef_Debug3_0
typedef uint8 Debug3_0;

# define Rte_TypeDef_Debug3_1
typedef uint8 Debug3_1;

# define Rte_TypeDef_Debug3_2
typedef uint8 Debug3_2;

# define Rte_TypeDef_Debug3_3
typedef uint8 Debug3_3;

# define Rte_TypeDef_Debug3_4
typedef uint8 Debug3_4;

# define Rte_TypeDef_Debug3_5
typedef uint8 Debug3_5;

# define Rte_TypeDef_Debug3_6
typedef uint8 Debug3_6;

# define Rte_TypeDef_Debug3_7
typedef uint8 Debug3_7;

# define Rte_TypeDef_Debug4_0
typedef uint8 Debug4_0;

# define Rte_TypeDef_Debug4_1
typedef uint8 Debug4_1;

# define Rte_TypeDef_Debug4_2
typedef uint8 Debug4_2;

# define Rte_TypeDef_Debug4_3
typedef uint8 Debug4_3;

# define Rte_TypeDef_Debug4_4
typedef uint8 Debug4_4;

# define Rte_TypeDef_Debug4_5
typedef uint8 Debug4_5;

# define Rte_TypeDef_Debug4_6
typedef uint8 Debug4_6;

# define Rte_TypeDef_Debug4_7
typedef uint8 Debug4_7;

# define Rte_TypeDef_Degug1_0
typedef uint8 Degug1_0;

# define Rte_TypeDef_EDITION_CALIB
typedef uint8 EDITION_CALIB;

# define Rte_TypeDef_EDITION_SOFT
typedef uint8 EDITION_SOFT;

# define Rte_TypeDef_EFFAC_DEFAUT_DIAG
typedef boolean EFFAC_DEFAUT_DIAG;

# define Rte_TypeDef_EVSE_RTAB_STOP_CHARGE
typedef boolean EVSE_RTAB_STOP_CHARGE;

# define Rte_TypeDef_IdtABSVehSpdThresholdOV
typedef unsigned char IdtABSVehSpdThresholdOV;

# define Rte_TypeDef_IdtACTempTripThreshold
typedef unsigned char IdtACTempTripThreshold;

# define Rte_TypeDef_IdtACTempTripTime
typedef unsigned short IdtACTempTripTime;

# define Rte_TypeDef_IdtACTempWarningThreshold
typedef unsigned char IdtACTempWarningThreshold;

# define Rte_TypeDef_IdtACTempWarningTime
typedef unsigned short IdtACTempWarningTime;

# define Rte_TypeDef_IdtAfts_DelayLed
typedef unsigned short IdtAfts_DelayLed;

# define Rte_TypeDef_IdtAfts_ElockActuatorTime1
typedef unsigned char IdtAfts_ElockActuatorTime1;

# define Rte_TypeDef_IdtAfts_ElockActuatorTime2
typedef unsigned char IdtAfts_ElockActuatorTime2;

# define Rte_TypeDef_IdtAmbientTemperaturePhysicalValue
typedef unsigned char IdtAmbientTemperaturePhysicalValue;

# define Rte_TypeDef_IdtBatteryVolt
typedef unsigned char IdtBatteryVolt;

# define Rte_TypeDef_IdtBatteryVoltMaxTest
typedef unsigned short IdtBatteryVoltMaxTest;

# define Rte_TypeDef_IdtBatteryVoltMinTest
typedef unsigned short IdtBatteryVoltMinTest;

# define Rte_TypeDef_IdtBatteryVoltageSafetyOVLimit1_ConfirmTime
typedef unsigned char IdtBatteryVoltageSafetyOVLimit1_ConfirmTime;

# define Rte_TypeDef_IdtBatteryVoltageSafetyOVLimit1_Threshold
typedef unsigned char IdtBatteryVoltageSafetyOVLimit1_Threshold;

# define Rte_TypeDef_IdtBatteryVoltageSafetyOVLimit2_ConfirmTime
typedef unsigned char IdtBatteryVoltageSafetyOVLimit2_ConfirmTime;

# define Rte_TypeDef_IdtBatteryVoltageSafetyOVLimit2_Threshold
typedef unsigned char IdtBatteryVoltageSafetyOVLimit2_Threshold;

# define Rte_TypeDef_IdtBatteryVoltageThreshold
typedef unsigned char IdtBatteryVoltageThreshold;

# define Rte_TypeDef_IdtBatteryVoltageTime
typedef unsigned char IdtBatteryVoltageTime;

# define Rte_TypeDef_IdtBulkSocConf
typedef unsigned char IdtBulkSocConf;

# define Rte_TypeDef_IdtCOMLatchMaxDuration
typedef unsigned short IdtCOMLatchMaxDuration;

# define Rte_TypeDef_IdtCalThresholdUVTripDischarge
typedef unsigned char IdtCalThresholdUVTripDischarge;

# define Rte_TypeDef_IdtCalTimeUVTripDischarge
typedef unsigned char IdtCalTimeUVTripDischarge;

# define Rte_TypeDef_IdtChLedCtl
typedef unsigned char IdtChLedCtl;

# define Rte_TypeDef_IdtControlPilotDutyPlantMode
typedef unsigned char IdtControlPilotDutyPlantMode;

# define Rte_TypeDef_IdtDCLVFaultTimeToRetryDefault
typedef unsigned short IdtDCLVFaultTimeToRetryDefault;

# define Rte_TypeDef_IdtDCLVFaultTimeToRetryShortCircuit
typedef unsigned short IdtDCLVFaultTimeToRetryShortCircuit;

# define Rte_TypeDef_IdtDCLVTimeConfirmDerating
typedef unsigned char IdtDCLVTimeConfirmDerating;

# define Rte_TypeDef_IdtDCLV_MaxNumberRetries
typedef unsigned char IdtDCLV_MaxNumberRetries;

# define Rte_TypeDef_IdtDCRelayVoltagePlantMode
typedef unsigned char IdtDCRelayVoltagePlantMode;

# define Rte_TypeDef_IdtDCTempTripThreshold
typedef unsigned char IdtDCTempTripThreshold;

# define Rte_TypeDef_IdtDCTempTripTime
typedef unsigned short IdtDCTempTripTime;

# define Rte_TypeDef_IdtDCTempWarningThreshold
typedef unsigned char IdtDCTempWarningThreshold;

# define Rte_TypeDef_IdtDCTempWarningTime
typedef unsigned short IdtDCTempWarningTime;

# define Rte_TypeDef_IdtDebounceADCReference
typedef unsigned char IdtDebounceADCReference;

# define Rte_TypeDef_IdtDebounceControlPilot
typedef unsigned char IdtDebounceControlPilot;

# define Rte_TypeDef_IdtDebounceControlPilotPlantMode
typedef unsigned char IdtDebounceControlPilotPlantMode;

# define Rte_TypeDef_IdtDebounceDCRelayVoltagePlantMode
typedef unsigned char IdtDebounceDCRelayVoltagePlantMode;

# define Rte_TypeDef_IdtDebounceHoldNetworkCom
typedef unsigned char IdtDebounceHoldNetworkCom;

# define Rte_TypeDef_IdtDebounceOvercurrentDCHV
typedef unsigned char IdtDebounceOvercurrentDCHV;

# define Rte_TypeDef_IdtDebouncePhasePlantMode
typedef unsigned char IdtDebouncePhasePlantMode;

# define Rte_TypeDef_IdtDebounceProxDetect
typedef unsigned char IdtDebounceProxDetect;

# define Rte_TypeDef_IdtDebounceProximityPlantMode
typedef unsigned char IdtDebounceProximityPlantMode;

# define Rte_TypeDef_IdtDebounceRCD
typedef unsigned char IdtDebounceRCD;

# define Rte_TypeDef_IdtDebounceStateFailure
typedef unsigned char IdtDebounceStateFailure;

# define Rte_TypeDef_IdtDebounceStateFinished
typedef unsigned char IdtDebounceStateFinished;

# define Rte_TypeDef_IdtDebounceStateInProgress
typedef unsigned char IdtDebounceStateInProgress;

# define Rte_TypeDef_IdtDebounceStateStopped
typedef unsigned char IdtDebounceStateStopped;

# define Rte_TypeDef_IdtDebounceTempoEndCharge
typedef unsigned char IdtDebounceTempoEndCharge;

# define Rte_TypeDef_IdtDegMainWkuExtinctionTime
typedef unsigned short IdtDegMainWkuExtinctionTime;

# define Rte_TypeDef_IdtDemandMaxCurrent
typedef unsigned char IdtDemandMaxCurrent;

# define Rte_TypeDef_IdtDutyCloseRelay
typedef unsigned char IdtDutyCloseRelay;

# define Rte_TypeDef_IdtDutyControlPilot
typedef unsigned char IdtDutyControlPilot;

# define Rte_TypeDef_IdtDutyLedCharge
typedef unsigned char IdtDutyLedCharge;

# define Rte_TypeDef_IdtDutyPlug
typedef unsigned char IdtDutyPlug;

# define Rte_TypeDef_IdtELockDebounce
typedef unsigned char IdtELockDebounce;

# define Rte_TypeDef_IdtELockSensorFaultThreshold
typedef unsigned char IdtELockSensorFaultThreshold;

# define Rte_TypeDef_IdtELockSensorFaultTime
typedef unsigned char IdtELockSensorFaultTime;

# define Rte_TypeDef_IdtELockSensorLimit
typedef unsigned char IdtELockSensorLimit;

# define Rte_TypeDef_IdtEVSEMaximumPowerLimit
typedef unsigned short IdtEVSEMaximumPowerLimit;

# define Rte_TypeDef_IdtElockActuatorTime
typedef unsigned char IdtElockActuatorTime;

# define Rte_TypeDef_IdtElockFeedbackCurrent
typedef unsigned short IdtElockFeedbackCurrent;

# define Rte_TypeDef_IdtElockFeedbackLock
typedef unsigned short IdtElockFeedbackLock;

# define Rte_TypeDef_IdtElockFeedbackUnlock
typedef unsigned short IdtElockFeedbackUnlock;

# define Rte_TypeDef_IdtElockThreshold
typedef unsigned short IdtElockThreshold;

# define Rte_TypeDef_IdtElockTimeError
typedef unsigned char IdtElockTimeError;

# define Rte_TypeDef_IdtEnergyCapacity
typedef unsigned char IdtEnergyCapacity;

# define Rte_TypeDef_IdtEnforcedMainWkuTime
typedef unsigned short IdtEnforcedMainWkuTime;

# define Rte_TypeDef_IdtExtChLedRGBCtl
typedef unsigned char IdtExtChLedRGBCtl;

# define Rte_TypeDef_IdtExtOpPlugLockRaw
typedef unsigned char IdtExtOpPlugLockRaw;

# define Rte_TypeDef_IdtExtPlgLedrCtl
typedef unsigned char IdtExtPlgLedrCtl;

# define Rte_TypeDef_IdtExtinctionTime
typedef unsigned short IdtExtinctionTime;

# define Rte_TypeDef_IdtFrameDiagTime
typedef unsigned char IdtFrameDiagTime;

# define Rte_TypeDef_IdtFrequencyActivateRelays
typedef unsigned char IdtFrequencyActivateRelays;

# define Rte_TypeDef_IdtFrequencyPWM
typedef unsigned char IdtFrequencyPWM;

# define Rte_TypeDef_IdtGlobalTimeoutPlantMode
typedef unsigned short IdtGlobalTimeoutPlantMode;

# define Rte_TypeDef_IdtHVVTripThreshold
typedef unsigned char IdtHVVTripThreshold;

# define Rte_TypeDef_IdtHVVTripTime
typedef unsigned short IdtHVVTripTime;

# define Rte_TypeDef_IdtHVVWarningThreshold
typedef unsigned char IdtHVVWarningThreshold;

# define Rte_TypeDef_IdtHVVWarningTime
typedef unsigned short IdtHVVWarningTime;

# define Rte_TypeDef_IdtInputVoltageThreshold
typedef unsigned char IdtInputVoltageThreshold;

# define Rte_TypeDef_IdtInternalPartialWkuMaxDuration
typedef unsigned short IdtInternalPartialWkuMaxDuration;

# define Rte_TypeDef_IdtLVPTripTime
typedef unsigned short IdtLVPTripTime;

# define Rte_TypeDef_IdtLVVTripThreshold
typedef unsigned char IdtLVVTripThreshold;

# define Rte_TypeDef_IdtLVVTripTime
typedef unsigned short IdtLVVTripTime;

# define Rte_TypeDef_IdtLVVWarningThreshold
typedef unsigned char IdtLVVWarningThreshold;

# define Rte_TypeDef_IdtLVVWarningTime
typedef unsigned short IdtLVVWarningTime;

# define Rte_TypeDef_IdtLedFaultTime
typedef unsigned char IdtLedFaultTime;

# define Rte_TypeDef_IdtLedFeedbackPhysicalValue
typedef unsigned short IdtLedFeedbackPhysicalValue;

# define Rte_TypeDef_IdtLedThresholdSCG
typedef unsigned short IdtLedThresholdSCG;

# define Rte_TypeDef_IdtLedThresholdSCP
typedef unsigned short IdtLedThresholdSCP;

# define Rte_TypeDef_IdtLinTableGlobalTempElement
typedef unsigned char IdtLinTableGlobalTempElement;

# define Rte_TypeDef_IdtLinTableGlobalVoltElement
typedef unsigned short IdtLinTableGlobalVoltElement;

# define Rte_TypeDef_IdtMainWkuDevalidTime
typedef unsigned char IdtMainWkuDevalidTime;

# define Rte_TypeDef_IdtMainWkuValidTime
typedef unsigned char IdtMainWkuValidTime;

# define Rte_TypeDef_IdtMasterPartialWkuY1MaxDuration
typedef unsigned short IdtMasterPartialWkuY1MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY1MinDuration
typedef unsigned short IdtMasterPartialWkuY1MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY2MaxDuration
typedef unsigned short IdtMasterPartialWkuY2MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY2MinDuration
typedef unsigned short IdtMasterPartialWkuY2MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY3MaxDuration
typedef unsigned short IdtMasterPartialWkuY3MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY3MinDuration
typedef unsigned short IdtMasterPartialWkuY3MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY4MaxDuration
typedef unsigned short IdtMasterPartialWkuY4MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY4MinDuration
typedef unsigned short IdtMasterPartialWkuY4MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY5MaxDuration
typedef unsigned short IdtMasterPartialWkuY5MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY5MinDuration
typedef unsigned short IdtMasterPartialWkuY5MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY6MaxDuration
typedef unsigned short IdtMasterPartialWkuY6MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY6MinDuration
typedef unsigned short IdtMasterPartialWkuY6MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY7MaxDuration
typedef unsigned short IdtMasterPartialWkuY7MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY7MinDuration
typedef unsigned short IdtMasterPartialWkuY7MinDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY8MaxDuration
typedef unsigned short IdtMasterPartialWkuY8MaxDuration;

# define Rte_TypeDef_IdtMasterPartialWkuY8MinDuration
typedef unsigned short IdtMasterPartialWkuY8MinDuration;

# define Rte_TypeDef_IdtMaxDemmandCurrent
typedef unsigned char IdtMaxDemmandCurrent;

# define Rte_TypeDef_IdtMaxDemmandVoltage
typedef unsigned short IdtMaxDemmandVoltage;

# define Rte_TypeDef_IdtMaxDiscoveryCurrent
typedef unsigned char IdtMaxDiscoveryCurrent;

# define Rte_TypeDef_IdtMaxDiscoveryVoltage
typedef unsigned short IdtMaxDiscoveryVoltage;

# define Rte_TypeDef_IdtMaxEfficiency
typedef unsigned char IdtMaxEfficiency;

# define Rte_TypeDef_IdtMaxInputACCurrentEVSE
typedef unsigned short IdtMaxInputACCurrentEVSE;

# define Rte_TypeDef_IdtMaxInputVoltage110V
typedef unsigned char IdtMaxInputVoltage110V;

# define Rte_TypeDef_IdtMaxInputVoltage220V
typedef unsigned char IdtMaxInputVoltage220V;

# define Rte_TypeDef_IdtMaxNormalPlugLed
typedef unsigned short IdtMaxNormalPlugLed;

# define Rte_TypeDef_IdtMaxOutputDCHVCurrent
typedef unsigned char IdtMaxOutputDCHVCurrent;

# define Rte_TypeDef_IdtMaxOutputDCHVVoltage
typedef unsigned char IdtMaxOutputDCHVVoltage;

# define Rte_TypeDef_IdtMaxPrechargeCurrent
typedef unsigned char IdtMaxPrechargeCurrent;

# define Rte_TypeDef_IdtMaxPrechargeVDCLinkMono
typedef unsigned char IdtMaxPrechargeVDCLinkMono;

# define Rte_TypeDef_IdtMaxPrechargeVDCLinkTriphasic
typedef unsigned char IdtMaxPrechargeVDCLinkTriphasic;

# define Rte_TypeDef_IdtMaxPrechargeVoltage
typedef unsigned short IdtMaxPrechargeVoltage;

# define Rte_TypeDef_IdtMaxTimeChargeError
typedef unsigned short IdtMaxTimeChargeError;

# define Rte_TypeDef_IdtMaxTimeChargeInProgress
typedef unsigned short IdtMaxTimeChargeInProgress;

# define Rte_TypeDef_IdtMaxTimeDiagTools
typedef unsigned short IdtMaxTimeDiagTools;

# define Rte_TypeDef_IdtMaxTimeEndOfCharge
typedef unsigned short IdtMaxTimeEndOfCharge;

# define Rte_TypeDef_IdtMaxTimeGuideManagement
typedef unsigned short IdtMaxTimeGuideManagement;

# define Rte_TypeDef_IdtMaxTimePlantMode
typedef unsigned char IdtMaxTimePlantMode;

# define Rte_TypeDef_IdtMaxTimeProgrammingCharge
typedef unsigned short IdtMaxTimeProgrammingCharge;

# define Rte_TypeDef_IdtMaxTimeShutdown
typedef unsigned char IdtMaxTimeShutdown;

# define Rte_TypeDef_IdtMinDCHVOutputVoltage
typedef unsigned char IdtMinDCHVOutputVoltage;

# define Rte_TypeDef_IdtMinEfficiency
typedef unsigned char IdtMinEfficiency;

# define Rte_TypeDef_IdtMinInputVoltage110V
typedef unsigned char IdtMinInputVoltage110V;

# define Rte_TypeDef_IdtMinInputVoltage220V
typedef unsigned char IdtMinInputVoltage220V;

# define Rte_TypeDef_IdtMinNormalPlugLed
typedef unsigned short IdtMinNormalPlugLed;

# define Rte_TypeDef_IdtMinPrechargeVDCLinkMono
typedef unsigned char IdtMinPrechargeVDCLinkMono;

# define Rte_TypeDef_IdtMinPrechargeVDCLinkTriphasic
typedef unsigned char IdtMinPrechargeVDCLinkTriphasic;

# define Rte_TypeDef_IdtMsrTempRaw
typedef unsigned short IdtMsrTempRaw;

# define Rte_TypeDef_IdtOBCFaultTimeToRetryDefault
typedef unsigned short IdtOBCFaultTimeToRetryDefault;

# define Rte_TypeDef_IdtOBCFaultTimeToRetryOvertemp
typedef unsigned short IdtOBCFaultTimeToRetryOvertemp;

# define Rte_TypeDef_IdtOBCFaultTimeToRetryVoltageError
typedef unsigned short IdtOBCFaultTimeToRetryVoltageError;

# define Rte_TypeDef_IdtOBCMaxEfficiency
typedef unsigned short IdtOBCMaxEfficiency;

# define Rte_TypeDef_IdtOBCMinEfficiency
typedef unsigned char IdtOBCMinEfficiency;

# define Rte_TypeDef_IdtOBCOffsetAllowed
typedef unsigned short IdtOBCOffsetAllowed;

# define Rte_TypeDef_IdtOBCPTripTime
typedef unsigned short IdtOBCPTripTime;

# define Rte_TypeDef_IdtOBC_DelayEfficiencyDCLV
typedef unsigned char IdtOBC_DelayEfficiencyDCLV;

# define Rte_TypeDef_IdtOBC_DelayEfficiencyHVMono
typedef unsigned char IdtOBC_DelayEfficiencyHVMono;

# define Rte_TypeDef_IdtOBC_DelayEfficiencyHVTri
typedef unsigned char IdtOBC_DelayEfficiencyHVTri;

# define Rte_TypeDef_IdtOBC_MaxNumberRetries
typedef unsigned char IdtOBC_MaxNumberRetries;

# define Rte_TypeDef_IdtOBC_PowerAvailableControlPilot
typedef unsigned short IdtOBC_PowerAvailableControlPilot;

# define Rte_TypeDef_IdtOBC_PowerAvailableHardware
typedef unsigned short IdtOBC_PowerAvailableHardware;

# define Rte_TypeDef_IdtOBC_PowerMaxCalculated
typedef unsigned char IdtOBC_PowerMaxCalculated;

# define Rte_TypeDef_IdtOBC_PowerMaxValue
typedef unsigned char IdtOBC_PowerMaxValue;

# define Rte_TypeDef_IdtOffsetAllowed
typedef unsigned short IdtOffsetAllowed;

# define Rte_TypeDef_IdtOutputTempAftsales
typedef unsigned short IdtOutputTempAftsales;

# define Rte_TypeDef_IdtOutputTempMeas
typedef unsigned short IdtOutputTempMeas;

# define Rte_TypeDef_IdtPDERATING_OBC
typedef unsigned short IdtPDERATING_OBC;

# define Rte_TypeDef_IdtPhasePlantmode
typedef unsigned char IdtPhasePlantmode;

# define Rte_TypeDef_IdtPlgLedrCtrl
typedef unsigned char IdtPlgLedrCtrl;

# define Rte_TypeDef_IdtPlugLedFeedbackPhysicalValue
typedef unsigned short IdtPlugLedFeedbackPhysicalValue;

# define Rte_TypeDef_IdtPlugLedMaxOCDetection
typedef unsigned short IdtPlugLedMaxOCDetection;

# define Rte_TypeDef_IdtPlugLedMinOCDetection
typedef unsigned short IdtPlugLedMinOCDetection;

# define Rte_TypeDef_IdtPlugLedThresholdSCG
typedef unsigned short IdtPlugLedThresholdSCG;

# define Rte_TypeDef_IdtPlugLedThresholdSCP
typedef unsigned short IdtPlugLedThresholdSCP;

# define Rte_TypeDef_IdtProximityDetectPhysicalValue
typedef unsigned short IdtProximityDetectPhysicalValue;

# define Rte_TypeDef_IdtProximityDetectPlantMode
typedef unsigned short IdtProximityDetectPlantMode;

# define Rte_TypeDef_IdtProximityDetectVoltThreshold
typedef unsigned short IdtProximityDetectVoltThreshold;

# define Rte_TypeDef_IdtProximityVoltageStartingPlantMode
typedef unsigned short IdtProximityVoltageStartingPlantMode;

# define Rte_TypeDef_IdtRCDLineGndSCConfirmTime
typedef unsigned short IdtRCDLineGndSCConfirmTime;

# define Rte_TypeDef_IdtRCDLineGndSCRehabilitTime
typedef unsigned short IdtRCDLineGndSCRehabilitTime;

# define Rte_TypeDef_IdtRECHARGE_HMI_STATE
typedef unsigned char IdtRECHARGE_HMI_STATE;

# define Rte_TypeDef_IdtRampGradient
typedef unsigned char IdtRampGradient;

# define Rte_TypeDef_IdtShutdownPreparationMaxTime
typedef unsigned short IdtShutdownPreparationMaxTime;

# define Rte_TypeDef_IdtShutdownPreparationMinTime
typedef unsigned short IdtShutdownPreparationMinTime;

# define Rte_TypeDef_IdtSignalDiagTime
typedef unsigned char IdtSignalDiagTime;

# define Rte_TypeDef_IdtSlavePartialWkuXDevalidTime
typedef unsigned char IdtSlavePartialWkuXDevalidTime;

# define Rte_TypeDef_IdtSlavePartialWkuXLockTime
typedef unsigned short IdtSlavePartialWkuXLockTime;

# define Rte_TypeDef_IdtSlavePartialWkuXValidTime
typedef unsigned char IdtSlavePartialWkuXValidTime;

# define Rte_TypeDef_IdtStopChargeDemmandTimer
typedef unsigned short IdtStopChargeDemmandTimer;

# define Rte_TypeDef_IdtSubstituteValueTemp
typedef unsigned char IdtSubstituteValueTemp;

# define Rte_TypeDef_IdtT_INHIB_DIAG_COM
typedef unsigned char IdtT_INHIB_DIAG_COM;

# define Rte_TypeDef_IdtTargetPrechargeCurrent
typedef unsigned char IdtTargetPrechargeCurrent;

# define Rte_TypeDef_IdtTempDerating
typedef unsigned char IdtTempDerating;

# define Rte_TypeDef_IdtTempSyncRectPhysicalValue
typedef unsigned char IdtTempSyncRectPhysicalValue;

# define Rte_TypeDef_IdtThresholdLVOvercurrentHW
typedef unsigned short IdtThresholdLVOvercurrentHW;

# define Rte_TypeDef_IdtThresholdNoAC
typedef unsigned char IdtThresholdNoAC;

# define Rte_TypeDef_IdtThresholdTempFault
typedef unsigned short IdtThresholdTempFault;

# define Rte_TypeDef_IdtTimeBatteryVoltageSafety
typedef unsigned char IdtTimeBatteryVoltageSafety;

# define Rte_TypeDef_IdtTimeBeforeRunningDiag
typedef unsigned char IdtTimeBeforeRunningDiag;

# define Rte_TypeDef_IdtTimeCloseRelay
typedef unsigned char IdtTimeCloseRelay;

# define Rte_TypeDef_IdtTimeConfirmNoAC
typedef unsigned short IdtTimeConfirmNoAC;

# define Rte_TypeDef_IdtTimeDebChargePush
typedef unsigned char IdtTimeDebChargePush;

# define Rte_TypeDef_IdtTimeDiagTemp
typedef unsigned short IdtTimeDiagTemp;

# define Rte_TypeDef_IdtTimeElockFaultDetected
typedef unsigned char IdtTimeElockFaultDetected;

# define Rte_TypeDef_IdtTimeLedCharge
typedef unsigned char IdtTimeLedCharge;

# define Rte_TypeDef_IdtTimeLockDelay
typedef unsigned char IdtTimeLockDelay;

# define Rte_TypeDef_IdtTimeNomMainWkuDisord
typedef unsigned short IdtTimeNomMainWkuDisord;

# define Rte_TypeDef_IdtTimeNomMainWkuIncst
typedef unsigned short IdtTimeNomMainWkuIncst;

# define Rte_TypeDef_IdtTimeNomMainWkuRehabilit
typedef unsigned short IdtTimeNomMainWkuRehabilit;

# define Rte_TypeDef_IdtTimePlug
typedef unsigned char IdtTimePlug;

# define Rte_TypeDef_IdtTimePushButtonKeepValue
typedef unsigned short IdtTimePushButtonKeepValue;

# define Rte_TypeDef_IdtTimePushInhDelay
typedef unsigned short IdtTimePushInhDelay;

# define Rte_TypeDef_IdtTimeRCDPulse
typedef unsigned short IdtTimeRCDPulse;

# define Rte_TypeDef_IdtTimeRelaysPrechargeClosed
typedef unsigned char IdtTimeRelaysPrechargeClosed;

# define Rte_TypeDef_IdtTimeWakeupCoolingMax
typedef unsigned char IdtTimeWakeupCoolingMax;

# define Rte_TypeDef_IdtTimeWakeupCoolingMin
typedef unsigned char IdtTimeWakeupCoolingMin;

# define Rte_TypeDef_IdtTimeWakeupHoldDiscontactorMax
typedef unsigned short IdtTimeWakeupHoldDiscontactorMax;

# define Rte_TypeDef_IdtTimeWakeupHoldDiscontactorMin
typedef unsigned char IdtTimeWakeupHoldDiscontactorMin;

# define Rte_TypeDef_IdtTimeWakeupPiStateInfo
typedef unsigned char IdtTimeWakeupPiStateInfo;

# define Rte_TypeDef_IdtTimeoutPlantMode
typedef unsigned char IdtTimeoutPlantMode;

# define Rte_TypeDef_IdtTo_DgMux_Prod
typedef unsigned char IdtTo_DgMux_Prod;

# define Rte_TypeDef_IdtVDCLinkRequiredTriphasic
typedef unsigned char IdtVDCLinkRequiredTriphasic;

# define Rte_TypeDef_IdtVehStopMaxTest
typedef unsigned char IdtVehStopMaxTest;

# define Rte_TypeDef_IdtVehicleSpeedDegThreshold
typedef unsigned char IdtVehicleSpeedDegThreshold;

# define Rte_TypeDef_IdtVehicleSpeedThreshold
typedef unsigned char IdtVehicleSpeedThreshold;

# define Rte_TypeDef_IdtVehicleSpeedThresholdDiag
typedef unsigned char IdtVehicleSpeedThresholdDiag;

# define Rte_TypeDef_IdtVoltageCorrectionOffset
typedef signed short IdtVoltageCorrectionOffset;

# define Rte_TypeDef_IdtVoltageExternalADCReference
typedef unsigned short IdtVoltageExternalADCReference;

# define Rte_TypeDef_MODE_DIAG
typedef boolean MODE_DIAG;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_0
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_0;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_1
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_1;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_2
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_2;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_3
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_3;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_4
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_4;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_5
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_5;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_6
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_6;

# define Rte_TypeDef_NEW_JDD_OBC_DCDC_BYTE_7
typedef uint8 NEW_JDD_OBC_DCDC_BYTE_7;

# define Rte_TypeDef_OBC_CommunicationSt
typedef boolean OBC_CommunicationSt;

# define Rte_TypeDef_OBC_CoolingWakeup
typedef boolean OBC_CoolingWakeup;

# define Rte_TypeDef_OBC_DCChargingPlugAConnConf
typedef boolean OBC_DCChargingPlugAConnConf;

# define Rte_TypeDef_OBC_HVBattRechargeWakeup
typedef boolean OBC_HVBattRechargeWakeup;

# define Rte_TypeDef_OBC_HighVoltConnectionAllowed
typedef boolean OBC_HighVoltConnectionAllowed;

# define Rte_TypeDef_OBC_HoldDiscontactorWakeup
typedef boolean OBC_HoldDiscontactorWakeup;

# define Rte_TypeDef_OBC_OBCStartSt
typedef boolean OBC_OBCStartSt;

# define Rte_TypeDef_OBC_OBCTemp
typedef uint8 OBC_OBCTemp;

# define Rte_TypeDef_OBC_OutputCurrent
typedef uint16 OBC_OutputCurrent;

# define Rte_TypeDef_OBC_OutputVoltage
typedef uint16 OBC_OutputVoltage;

# define Rte_TypeDef_OBC_PIStateInfoWakeup
typedef boolean OBC_PIStateInfoWakeup;

# define Rte_TypeDef_OBC_PlugVoltDetection
typedef boolean OBC_PlugVoltDetection;

# define Rte_TypeDef_OBC_PowerMax
typedef uint8 OBC_PowerMax;

# define Rte_TypeDef_OBC_PushChargeType
typedef boolean OBC_PushChargeType;

# define Rte_TypeDef_OBC_SocketTempL
typedef uint8 OBC_SocketTempL;

# define Rte_TypeDef_OBC_SocketTempN
typedef uint8 OBC_SocketTempN;

# define Rte_TypeDef_PFC_IOM_ERR_OC_PH1
typedef boolean PFC_IOM_ERR_OC_PH1;

# define Rte_TypeDef_PFC_IOM_ERR_OC_PH2
typedef boolean PFC_IOM_ERR_OC_PH2;

# define Rte_TypeDef_PFC_IOM_ERR_OC_PH3
typedef boolean PFC_IOM_ERR_OC_PH3;

# define Rte_TypeDef_PFC_IOM_ERR_OC_SHUNT1
typedef boolean PFC_IOM_ERR_OC_SHUNT1;

# define Rte_TypeDef_PFC_IOM_ERR_OC_SHUNT2
typedef boolean PFC_IOM_ERR_OC_SHUNT2;

# define Rte_TypeDef_PFC_IOM_ERR_OC_SHUNT3
typedef boolean PFC_IOM_ERR_OC_SHUNT3;

# define Rte_TypeDef_PFC_IOM_ERR_OT_NTC1_M1
typedef boolean PFC_IOM_ERR_OT_NTC1_M1;

# define Rte_TypeDef_PFC_IOM_ERR_OT_NTC1_M3
typedef boolean PFC_IOM_ERR_OT_NTC1_M3;

# define Rte_TypeDef_PFC_IOM_ERR_OT_NTC1_M4
typedef boolean PFC_IOM_ERR_OT_NTC1_M4;

# define Rte_TypeDef_PFC_IOM_ERR_OV_DCLINK
typedef boolean PFC_IOM_ERR_OV_DCLINK;

# define Rte_TypeDef_PFC_IOM_ERR_OV_VPH12
typedef boolean PFC_IOM_ERR_OV_VPH12;

# define Rte_TypeDef_PFC_IOM_ERR_OV_VPH23
typedef boolean PFC_IOM_ERR_OV_VPH23;

# define Rte_TypeDef_PFC_IOM_ERR_OV_VPH31
typedef boolean PFC_IOM_ERR_OV_VPH31;

# define Rte_TypeDef_PFC_IOM_ERR_UVLO_ISO4
typedef boolean PFC_IOM_ERR_UVLO_ISO4;

# define Rte_TypeDef_PFC_IOM_ERR_UVLO_ISO7
typedef boolean PFC_IOM_ERR_UVLO_ISO7;

# define Rte_TypeDef_PFC_IOM_ERR_UV_12V
typedef boolean PFC_IOM_ERR_UV_12V;

# define Rte_TypeDef_PFC_IPH1_RMS_0A1
typedef uint16 PFC_IPH1_RMS_0A1;

# define Rte_TypeDef_PFC_IPH2_RMS_0A1
typedef uint16 PFC_IPH2_RMS_0A1;

# define Rte_TypeDef_PFC_IPH3_RMS_0A1
typedef uint16 PFC_IPH3_RMS_0A1;

# define Rte_TypeDef_PFC_Temp_M1_C
typedef uint8 PFC_Temp_M1_C;

# define Rte_TypeDef_PFC_Temp_M3_C
typedef uint8 PFC_Temp_M3_C;

# define Rte_TypeDef_PFC_Temp_M4_C
typedef uint8 PFC_Temp_M4_C;

# define Rte_TypeDef_PFC_VPH12_Peak_V
typedef uint16 PFC_VPH12_Peak_V;

# define Rte_TypeDef_PFC_VPH12_RMS_V
typedef uint16 PFC_VPH12_RMS_V;

# define Rte_TypeDef_PFC_VPH1_Freq_Hz
typedef uint16 PFC_VPH1_Freq_Hz;

# define Rte_TypeDef_PFC_VPH23_Peak_V
typedef uint16 PFC_VPH23_Peak_V;

# define Rte_TypeDef_PFC_VPH23_RMS_V
typedef uint16 PFC_VPH23_RMS_V;

# define Rte_TypeDef_PFC_VPH2_Freq_Hz
typedef uint16 PFC_VPH2_Freq_Hz;

# define Rte_TypeDef_PFC_VPH31_Peak_V
typedef uint16 PFC_VPH31_Peak_V;

# define Rte_TypeDef_PFC_VPH31_RMS_V
typedef uint16 PFC_VPH31_RMS_V;

# define Rte_TypeDef_PFC_VPH3_Freq_Hz
typedef uint16 PFC_VPH3_Freq_Hz;

# define Rte_TypeDef_PFC_Vdclink_V
typedef uint16 PFC_Vdclink_V;

# define Rte_TypeDef_Rte_DT_IdtArrayDGN_EOL_NVM_RamMirror_0
typedef unsigned char Rte_DT_IdtArrayDGN_EOL_NVM_RamMirror_0;

# define Rte_TypeDef_Rte_DT_IdtArrayInitAIMVoltRef_0
typedef unsigned char Rte_DT_IdtArrayInitAIMVoltRef_0;

# define Rte_TypeDef_Rte_DT_IdtArrayJDDNvMRamMirror_0
typedef unsigned char Rte_DT_IdtArrayJDDNvMRamMirror_0;

# define Rte_TypeDef_Rte_DT_IdtPCOMNvMArray_0
typedef unsigned char Rte_DT_IdtPCOMNvMArray_0;

# define Rte_TypeDef_Rte_DT_IdtPSHNvMArray_0
typedef unsigned char Rte_DT_IdtPSHNvMArray_0;

# define Rte_TypeDef_Rte_DT_IdtPlantModeTestUTPlugin_0
typedef uint8 Rte_DT_IdtPlantModeTestUTPlugin_0;

# define Rte_TypeDef_SUPV_CoolingWupState
typedef boolean SUPV_CoolingWupState;

# define Rte_TypeDef_SUPV_DTCRegistred
typedef boolean SUPV_DTCRegistred;

# define Rte_TypeDef_SUPV_HVBattChargeWupState
typedef boolean SUPV_HVBattChargeWupState;

# define Rte_TypeDef_SUPV_HoldDiscontactorWupState
typedef boolean SUPV_HoldDiscontactorWupState;

# define Rte_TypeDef_SUPV_PIStateInfoWupState
typedef boolean SUPV_PIStateInfoWupState;

# define Rte_TypeDef_SUPV_PostDriveWupState
typedef boolean SUPV_PostDriveWupState;

# define Rte_TypeDef_SUPV_PreDriveWupState
typedef boolean SUPV_PreDriveWupState;

# define Rte_TypeDef_SUPV_PrecondElecWupState
typedef boolean SUPV_PrecondElecWupState;

# define Rte_TypeDef_SUPV_RCDLineState
typedef boolean SUPV_RCDLineState;

# define Rte_TypeDef_SUPV_StopDelayedHMIWupState
typedef boolean SUPV_StopDelayedHMIWupState;

# define Rte_TypeDef_SUP_CommandVDCLink_V
typedef uint16 SUP_CommandVDCLink_V;

# define Rte_TypeDef_VCU_AccPedalPosition
typedef uint8 VCU_AccPedalPosition;

# define Rte_TypeDef_VCU_ActivedischargeCommand
typedef boolean VCU_ActivedischargeCommand;

# define Rte_TypeDef_VCU_CDEAccJDD
typedef boolean VCU_CDEAccJDD;

# define Rte_TypeDef_VCU_CompteurRazGct
typedef uint8 VCU_CompteurRazGct;

# define Rte_TypeDef_VCU_CptTemporel
typedef uint32 VCU_CptTemporel;

# define Rte_TypeDef_VCU_DCDCVoltageReq
typedef uint8 VCU_DCDCVoltageReq;

# define Rte_TypeDef_VCU_DDEGMVSEEM
typedef uint8 VCU_DDEGMVSEEM;

# define Rte_TypeDef_VCU_DMDActivChiller
typedef boolean VCU_DMDActivChiller;

# define Rte_TypeDef_VCU_DMDMeap2SEEM
typedef uint8 VCU_DMDMeap2SEEM;

# define Rte_TypeDef_VCU_DiagMuxOnPwt
typedef boolean VCU_DiagMuxOnPwt;

# define Rte_TypeDef_VCU_ElecMeterSaturation
typedef boolean VCU_ElecMeterSaturation;

# define Rte_TypeDef_VCU_Kilometrage
typedef uint32 VCU_Kilometrage;

# define Rte_TypeDef_VCU_PrecondElecWakeup
typedef boolean VCU_PrecondElecWakeup;

# define Rte_TypeDef_VCU_StopDelayedHMIWakeup
typedef boolean VCU_StopDelayedHMIWakeup;

# define Rte_TypeDef_VERSION_APPLI
typedef uint8 VERSION_APPLI;

# define Rte_TypeDef_VERSION_SOFT
typedef uint8 VERSION_SOFT;

# define Rte_TypeDef_VERSION_SYSTEME
typedef uint8 VERSION_SYSTEME;

# define Rte_TypeDef_VERS_DATE2_ANNEE
typedef uint8 VERS_DATE2_ANNEE;

# define Rte_TypeDef_VERS_DATE2_JOUR
typedef uint8 VERS_DATE2_JOUR;

# define Rte_TypeDef_VERS_DATE2_MOIS
typedef uint8 VERS_DATE2_MOIS;

# define Rte_TypeDef_dtRef_VOID
typedef void * dtRef_VOID;

# define Rte_TypeDef_dtRef_const_VOID
typedef const void * dtRef_const_VOID;

# define Rte_TypeDef_BMS_CC2_connection_Status
typedef uint8 BMS_CC2_connection_Status;

# define Rte_TypeDef_BMS_FastChargeSt
typedef uint8 BMS_FastChargeSt;

# define Rte_TypeDef_BMS_MainConnectorState
typedef uint8 BMS_MainConnectorState;

# define Rte_TypeDef_BMS_SlowChargeSt
typedef uint8 BMS_SlowChargeSt;

# define Rte_TypeDef_BSI_ChargeState
typedef uint8 BSI_ChargeState;

# define Rte_TypeDef_BSI_ChargeTypeStatus
typedef uint8 BSI_ChargeTypeStatus;

# define Rte_TypeDef_BSI_LockedVehicle
typedef uint8 BSI_LockedVehicle;

# define Rte_TypeDef_BSI_MainWakeup
typedef uint8 BSI_MainWakeup;

# define Rte_TypeDef_BSI_VCUHeatPumpWorkingMode
typedef uint8 BSI_VCUHeatPumpWorkingMode;

# define Rte_TypeDef_DCDC_ActivedischargeSt
typedef uint8 DCDC_ActivedischargeSt;

# define Rte_TypeDef_DCDC_Fault
typedef uint8 DCDC_Fault;

# define Rte_TypeDef_DCDC_FaultLampRequest
typedef uint8 DCDC_FaultLampRequest;

# define Rte_TypeDef_DCDC_OBCMainContactorReq
typedef uint8 DCDC_OBCMainContactorReq;

# define Rte_TypeDef_DCDC_OBCQuickChargeContactorReq
typedef uint8 DCDC_OBCQuickChargeContactorReq;

# define Rte_TypeDef_DCDC_Status
typedef uint8 DCDC_Status;

# define Rte_TypeDef_DCHV_DCHVStatus
typedef uint8 DCHV_DCHVStatus;

# define Rte_TypeDef_DCLV_DCLVStatus
typedef uint8 DCLV_DCLVStatus;

# define Rte_TypeDef_IdtAppRCDECUState
typedef unsigned char IdtAppRCDECUState;

# define Rte_TypeDef_IdtBatteryVoltageState
typedef unsigned char IdtBatteryVoltageState;

# define Rte_TypeDef_IdtDebugPortID
typedef unsigned char IdtDebugPortID;

# define Rte_TypeDef_IdtELockSetPoint
typedef unsigned char IdtELockSetPoint;

# define Rte_TypeDef_IdtFaultLevel
typedef unsigned char IdtFaultLevel;

# define Rte_TypeDef_IdtHWEditionDetected
typedef unsigned char IdtHWEditionDetected;

# define Rte_TypeDef_IdtInputVoltageMode
typedef unsigned char IdtInputVoltageMode;

# define Rte_TypeDef_IdtOutputELockSensor
typedef unsigned char IdtOutputELockSensor;

# define Rte_TypeDef_IdtPOST_Result
typedef unsigned char IdtPOST_Result;

# define Rte_TypeDef_IdtPlantModeDTCNumber
typedef unsigned char IdtPlantModeDTCNumber;

# define Rte_TypeDef_IdtPlantModeTestInfoDID_Result
typedef unsigned char IdtPlantModeTestInfoDID_Result;

# define Rte_TypeDef_IdtPlantModeTestInfoDID_State
typedef unsigned char IdtPlantModeTestInfoDID_State;

# define Rte_TypeDef_IdtPlantModeTestInfoDID_Test
typedef unsigned char IdtPlantModeTestInfoDID_Test;

# define Rte_TypeDef_OBC_ACRange
typedef uint8 OBC_ACRange;

# define Rte_TypeDef_OBC_CP_connection_Status
typedef uint8 OBC_CP_connection_Status;

# define Rte_TypeDef_OBC_ChargingConnectionConfirmati
typedef uint8 OBC_ChargingConnectionConfirmati;

# define Rte_TypeDef_OBC_ChargingMode
typedef uint8 OBC_ChargingMode;

# define Rte_TypeDef_OBC_ElockState
typedef uint8 OBC_ElockState;

# define Rte_TypeDef_OBC_Fault
typedef uint8 OBC_Fault;

# define Rte_TypeDef_OBC_InputVoltageSt
typedef uint8 OBC_InputVoltageSt;

# define Rte_TypeDef_OBC_RechargeHMIState
typedef uint8 OBC_RechargeHMIState;

# define Rte_TypeDef_OBC_Status
typedef uint8 OBC_Status;

# define Rte_TypeDef_PFC_PFCStatus
typedef uint8 PFC_PFCStatus;

# define Rte_TypeDef_SUPV_ECUElecStateRCD
typedef uint8 SUPV_ECUElecStateRCD;

# define Rte_TypeDef_SUP_RequestDCLVStatus
typedef uint8 SUP_RequestDCLVStatus;

# define Rte_TypeDef_SUP_RequestPFCStatus
typedef uint8 SUP_RequestPFCStatus;

# define Rte_TypeDef_SUP_RequestStatusDCHV
typedef uint8 SUP_RequestStatusDCHV;

# define Rte_TypeDef_VCU_CDEApcJDD
typedef uint8 VCU_CDEApcJDD;

# define Rte_TypeDef_VCU_DCDCActivation
typedef uint8 VCU_DCDCActivation;

# define Rte_TypeDef_VCU_EPWT_Status
typedef uint8 VCU_EPWT_Status;

# define Rte_TypeDef_VCU_EtatGmpHyb
typedef uint8 VCU_EtatGmpHyb;

# define Rte_TypeDef_VCU_EtatPrincipSev
typedef uint8 VCU_EtatPrincipSev;

# define Rte_TypeDef_VCU_EtatReseauElec
typedef uint8 VCU_EtatReseauElec;

# define Rte_TypeDef_VCU_Keyposition
typedef uint8 VCU_Keyposition;

# define Rte_TypeDef_VCU_ModeEPSRequest
typedef uint8 VCU_ModeEPSRequest;

# define Rte_TypeDef_VCU_PosShuntJDD
typedef uint8 VCU_PosShuntJDD;

# define Rte_TypeDef_DataArrayType_uint8_1
typedef uint8 DataArrayType_uint8_1[1];

# define Rte_TypeDef_DataArrayType_uint8_2
typedef uint8 DataArrayType_uint8_2[2];

# define Rte_TypeDef_DataArrayType_uint8_4
typedef uint8 DataArrayType_uint8_4[4];

# define Rte_TypeDef_Dcm_Data10ByteType
typedef uint8 Dcm_Data10ByteType[10];

# define Rte_TypeDef_Dcm_Data16ByteType
typedef uint8 Dcm_Data16ByteType[16];

# define Rte_TypeDef_Dcm_Data1ByteType
typedef uint8 Dcm_Data1ByteType[1];

# define Rte_TypeDef_Dcm_Data20ByteType
typedef uint8 Dcm_Data20ByteType[20];

# define Rte_TypeDef_Dcm_Data2ByteType
typedef uint8 Dcm_Data2ByteType[2];

# define Rte_TypeDef_Dcm_Data3ByteType
typedef uint8 Dcm_Data3ByteType[3];

# define Rte_TypeDef_Dcm_Data4095ByteType
typedef uint8 Dcm_Data4095ByteType[4095];

# define Rte_TypeDef_Dcm_Data4ByteType
typedef uint8 Dcm_Data4ByteType[4];

# define Rte_TypeDef_Dcm_Data5ByteType
typedef uint8 Dcm_Data5ByteType[5];

# define Rte_TypeDef_Dcm_Data6ByteType
typedef uint8 Dcm_Data6ByteType[6];

# define Rte_TypeDef_Dcm_Data76ByteType
typedef uint8 Dcm_Data76ByteType[76];

# define Rte_TypeDef_Dem_MaxDataValueType
typedef uint8 Dem_MaxDataValueType[4];

# define Rte_TypeDef_IdtArrayDGN_EOL_NVM_RamMirror
typedef Rte_DT_IdtArrayDGN_EOL_NVM_RamMirror_0 IdtArrayDGN_EOL_NVM_RamMirror[76];

# define Rte_TypeDef_IdtArrayInitAIMVoltRef
typedef Rte_DT_IdtArrayInitAIMVoltRef_0 IdtArrayInitAIMVoltRef[3];

# define Rte_TypeDef_IdtArrayJDDNvMRamMirror
typedef Rte_DT_IdtArrayJDDNvMRamMirror_0 IdtArrayJDDNvMRamMirror[2000];

# define Rte_TypeDef_IdtLinTableGlobalTemp
typedef IdtLinTableGlobalTempElement IdtLinTableGlobalTemp[39];

# define Rte_TypeDef_IdtLinTableGlobalVolt
typedef IdtLinTableGlobalVoltElement IdtLinTableGlobalVolt[39];

# define Rte_TypeDef_IdtPCOMNvMArray
typedef Rte_DT_IdtPCOMNvMArray_0 IdtPCOMNvMArray[16];

# define Rte_TypeDef_IdtPSHNvMArray
typedef Rte_DT_IdtPSHNvMArray_0 IdtPSHNvMArray[4];

# define Rte_TypeDef_IdtPlantModeTestUTPlugin
typedef Rte_DT_IdtPlantModeTestUTPlugin_0 IdtPlantModeTestUTPlugin[2];

# define Rte_TypeDef_SG_DC2
typedef struct
{
  DCDC_ActivedischargeSt DCDC_ActivedischargeSt;
  DCDC_InputCurrent DCDC_InputCurrent;
  DCDC_OBCDCDCRTPowerLoad DCDC_OBCDCDCRTPowerLoad;
  DCDC_OBCMainContactorReq DCDC_OBCMainContactorReq;
  DCDC_OBCQuickChargeContactorReq DCDC_OBCQuickChargeContactorReq;
  DCDC_OutputCurrent DCDC_OutputCurrent;
} SG_DC2;

# define Rte_TypeDef_ComM_InhibitionStatusType
typedef uint8 ComM_InhibitionStatusType;

# define Rte_TypeDef_ComM_UserHandleType
typedef uint16 ComM_UserHandleType;

# define Rte_TypeDef_Dem_DTCGroupType
typedef uint32 Dem_DTCGroupType;

# define Rte_TypeDef_Dem_DTCStatusMaskType
typedef uint8 Dem_DTCStatusMaskType;

# define Rte_TypeDef_Dem_EventIdType
typedef uint16 Dem_EventIdType;

# define Rte_TypeDef_Dem_RatioIdType
typedef uint16 Dem_RatioIdType;

# define Rte_TypeDef_EcuM_TimeType
typedef uint32 EcuM_TimeType;

# define Rte_TypeDef_NvM_BlockIdType
typedef uint16 NvM_BlockIdType;

# define Rte_TypeDef_TimeInMicrosecondsType
typedef uint32 TimeInMicrosecondsType;

# define Rte_TypeDef_WdgM_CheckpointIdType
typedef uint16 WdgM_CheckpointIdType;

# define Rte_TypeDef_WdgM_ModeType
typedef uint8 WdgM_ModeType;

# define Rte_TypeDef_WdgM_SupervisedEntityIdType
typedef uint16 WdgM_SupervisedEntityIdType;

# define Rte_TypeDef_WdgM_ViolationType
typedef uint8 WdgM_ViolationType;

# define Rte_TypeDef_BswM_ESH_Mode
typedef uint8 BswM_ESH_Mode;

# define Rte_TypeDef_BswM_ESH_RunRequest
typedef uint8 BswM_ESH_RunRequest;

# define Rte_TypeDef_ComM_ModeType
typedef uint8 ComM_ModeType;

# define Rte_TypeDef_Dcm_CommunicationModeType
typedef uint8 Dcm_CommunicationModeType;

# define Rte_TypeDef_Dcm_ConfirmationStatusType
typedef uint8 Dcm_ConfirmationStatusType;

# define Rte_TypeDef_Dcm_DiagnosticSessionControlType
typedef uint8 Dcm_DiagnosticSessionControlType;

# define Rte_TypeDef_Dcm_EcuResetType
typedef uint8 Dcm_EcuResetType;

# define Rte_TypeDef_Dcm_NegativeResponseCodeType
typedef uint8 Dcm_NegativeResponseCodeType;

# define Rte_TypeDef_Dcm_OpStatusType
typedef uint8 Dcm_OpStatusType;

# define Rte_TypeDef_Dcm_ProtocolType
typedef uint8 Dcm_ProtocolType;

# define Rte_TypeDef_Dcm_RequestKindType
typedef uint8 Dcm_RequestKindType;

# define Rte_TypeDef_Dcm_SecLevelType
typedef uint8 Dcm_SecLevelType;

# define Rte_TypeDef_Dcm_SesCtrlType
typedef uint8 Dcm_SesCtrlType;

# define Rte_TypeDef_Dem_DTCFormatType
typedef uint8 Dem_DTCFormatType;

# define Rte_TypeDef_Dem_DTCKindType
typedef uint8 Dem_DTCKindType;

# define Rte_TypeDef_Dem_DTCOriginType
typedef uint16 Dem_DTCOriginType;

# define Rte_TypeDef_Dem_DTCSeverityType
typedef uint8 Dem_DTCSeverityType;

# define Rte_TypeDef_Dem_DTRControlType
typedef uint8 Dem_DTRControlType;

# define Rte_TypeDef_Dem_DebounceResetStatusType
typedef uint8 Dem_DebounceResetStatusType;

# define Rte_TypeDef_Dem_DebouncingStateType
typedef uint8 Dem_DebouncingStateType;

# define Rte_TypeDef_Dem_EventStatusType
typedef uint8 Dem_EventStatusType;

# define Rte_TypeDef_Dem_IndicatorStatusType
typedef uint8 Dem_IndicatorStatusType;

# define Rte_TypeDef_Dem_InitMonitorReasonType
typedef uint8 Dem_InitMonitorReasonType;

# define Rte_TypeDef_Dem_IumprDenomCondIdType
typedef uint8 Dem_IumprDenomCondIdType;

# define Rte_TypeDef_Dem_IumprDenomCondStatusType
typedef uint8 Dem_IumprDenomCondStatusType;

# define Rte_TypeDef_Dem_IumprReadinessGroupType
typedef uint8 Dem_IumprReadinessGroupType;

# define Rte_TypeDef_Dem_MonitorStatusType
typedef uint8 Dem_MonitorStatusType;

# define Rte_TypeDef_Dem_OperationCycleStateType
typedef uint8 Dem_OperationCycleStateType;

# define Rte_TypeDef_Dem_UdsStatusByteType
typedef uint8 Dem_UdsStatusByteType;

# define Rte_TypeDef_EcuM_BootTargetType
typedef uint8 EcuM_BootTargetType;

# define Rte_TypeDef_EcuM_ModeType
typedef uint8 EcuM_ModeType;

# define Rte_TypeDef_EcuM_ShutdownCauseType
typedef uint8 EcuM_ShutdownCauseType;

# define Rte_TypeDef_EcuM_StateType
typedef uint8 EcuM_StateType;

# define Rte_TypeDef_EcuM_UserType
typedef uint8 EcuM_UserType;

# define Rte_TypeDef_NvM_RequestResultType
typedef uint8 NvM_RequestResultType;

# define Rte_TypeDef_WdgMMode
typedef uint8 WdgMMode;

# define Rte_TypeDef_WdgM_GlobalStatusType
typedef uint8 WdgM_GlobalStatusType;

# define Rte_TypeDef_WdgM_LocalStatusType
typedef uint8 WdgM_LocalStatusType;


# ifndef RTE_SUPPRESS_UNUSED_DATATYPES
/**********************************************************************************************************************
 * Unused Data type definitions
 *********************************************************************************************************************/

#  define Rte_TypeDef_IdtEVSEMaximumCurrentLimit
typedef unsigned short IdtEVSEMaximumCurrentLimit;

#  define Rte_TypeDef_IdtEVSEMaximumVoltageLimit
typedef unsigned short IdtEVSEMaximumVoltageLimit;

#  define Rte_TypeDef_Dem_OperationCycleIdType
typedef uint8 Dem_OperationCycleIdType;

#  define Rte_TypeDef_NvM_ServiceIdType
typedef uint8 NvM_ServiceIdType;

# endif


/**********************************************************************************************************************
 * Calibration Parameter Types
 *********************************************************************************************************************/

# define Rte_CalprmElementGroup_CtApPLS_DEFAULT_RTE_CDATA_GROUP (0)
typedef struct
{
  IdtACTempTripTime CalACTempTripTime;
  IdtACTempWarningTime CalACTempWarningTime;
  IdtDCTempTripTime CalDCTempTripTime;
  IdtDCTempWarningTime CalDCTempWarningTime;
  IdtHVVTripTime CalHVVTripTime;
  IdtHVVWarningTime CalHVVWarningTime;
  IdtLVPTripTime CalLVPTripTime;
  IdtLVVTripTime CalLVVTripTime;
  IdtLVVWarningTime CalLVVWarningTime;
  IdtOBCMaxEfficiency CalOBCMaxEfficiency;
  IdtOBCOffsetAllowed CalOBCOffsetAllowed;
  IdtOBCPTripTime CalOBCPTripTime;
  IdtOffsetAllowed CalOffsetAllowed;
  IdtACTempTripThreshold CalACTempTripThreshold;
  IdtACTempWarningThreshold CalACTempWarningThreshold;
  IdtDCTempTripThreshold CalDCTempTripThreshold;
  IdtDCTempWarningThreshold CalDCTempWarningThreshold;
  IdtHVVTripThreshold CalHVVTripThreshold;
  IdtHVVWarningThreshold CalHVVWarningThreshold;
  IdtLVVTripThreshold CalLVVTripThreshold;
  IdtLVVWarningThreshold CalLVVWarningThreshold;
  IdtMaxEfficiency CalMaxEfficiency;
  IdtMinEfficiency CalMinEfficiency;
  IdtOBCMinEfficiency CalOBCMinEfficiency;
  IdtOBC_DelayEfficiencyDCLV CalOBC_DelayEfficiencyDCLV;
  IdtOBC_DelayEfficiencyHVMono CalOBC_DelayEfficiencyHVMono;
  IdtOBC_DelayEfficiencyHVTri CalOBC_DelayEfficiencyHVTri;
} Rte_Calprm_CtApPLS_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApFCL_DEFAULT_RTE_CDATA_GROUP (1)
typedef struct
{
  IdtAfts_DelayLed CalAfts_DelayLedBlueOff;
  IdtAfts_DelayLed CalAfts_DelayLedBlueOn;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOff;
  IdtAfts_DelayLed CalAfts_DelayLedGreenOn;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOff;
  IdtAfts_DelayLed CalAfts_DelayLedPlugOn;
  IdtAfts_DelayLed CalAfts_DelayLedRedOff;
  IdtAfts_DelayLed CalAfts_DelayLedRedOn;
  IdtLedThresholdSCG CalLedBlueThresholdSCG;
  IdtLedThresholdSCP CalLedBlueThresholdSCP;
  IdtLedThresholdSCG CalLedGreenThresholdSCG;
  IdtLedThresholdSCP CalLedGreenThresholdSCP;
  IdtLedThresholdSCG CalLedRedThresholdSCG;
  IdtLedThresholdSCP CalLedRedThresholdSCP;
  IdtMaxNormalPlugLed CalMaxNormalPlugLed;
  IdtMinNormalPlugLed CalMinNormalPlugLed;
  IdtPlugLedMaxOCDetection CalPlugLedMaxOCDetection;
  IdtPlugLedMinOCDetection CalPlugLedMinOCDetection;
  IdtPlugLedThresholdSCG CalPlugLedThresholdSCG;
  IdtPlugLedThresholdSCP CalPlugLedThresholdSCP;
  IdtLedFaultTime CalLedBlueOCConfirmTime;
  IdtLedFaultTime CalLedBlueOCHealTime;
  IdtLedFaultTime CalLedBlueSCGConfirmTime;
  IdtLedFaultTime CalLedBlueSCGHealTime;
  IdtLedFaultTime CalLedBlueSCPConfirmTime;
  IdtLedFaultTime CalLedBlueSCPHealTime;
  IdtLedFaultTime CalLedGreenOCConfirmTime;
  IdtLedFaultTime CalLedGreenOCHealTime;
  IdtLedFaultTime CalLedGreenSCGConfirmTime;
  IdtLedFaultTime CalLedGreenSCGHealTime;
  IdtLedFaultTime CalLedGreenSCPConfirmTime;
  IdtLedFaultTime CalLedGreenSCPHealTime;
  IdtLedFaultTime CalLedPlugOCConfirmTime;
  IdtLedFaultTime CalLedPlugOCHealTime;
  IdtLedFaultTime CalLedPlugSCGConfirmTime;
  IdtLedFaultTime CalLedPlugSCGHealTime;
  IdtLedFaultTime CalLedPlugSCPConfirmTime;
  IdtLedFaultTime CalLedPlugSCPHealTime;
  IdtLedFaultTime CalLedRedOCConfirmTime;
  IdtLedFaultTime CalLedRedOCHealTime;
  IdtLedFaultTime CalLedRedSCGConfirmTime;
  IdtLedFaultTime CalLedRedSCGHealTime;
  IdtLedFaultTime CalLedRedSCPConfirmTime;
  IdtLedFaultTime CalLedRedSCPHealTime;
  boolean CalEnableLedBlue;
  boolean CalEnableLedGreen;
  boolean CalEnableLedPlug;
  boolean CalEnableLedRed;
} Rte_Calprm_CtApFCL_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApBAT_DEFAULT_RTE_CDATA_GROUP (2)
typedef struct
{
  IdtBatteryVoltageTime CalBatteryVoltageOVHealTime;
  IdtBatteryVoltageThreshold CalBatteryVoltageThresholdOV;
  IdtBatteryVoltageThreshold CalBatteryVoltageThresholdUV;
  IdtBatteryVoltageTime CalBatteryVoltageUVConfirmTime;
  IdtBatteryVoltageTime CalBatteryVoltageUVHealTime;
  IdtBatteryVoltageTime CarBatteryVoltageOVConfirmTime;
} Rte_Calprm_CtApBAT_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApDER_DEFAULT_RTE_CDATA_GROUP (3)
typedef struct
{
  IdtTempDerating CalDCDC_FinishLinear;
  IdtTempDerating CalDCDC_MaxDeviation;
  IdtTempDerating CalDCDC_MinDeviation;
  IdtTempDerating CalDCDC_StartLinear;
  IdtTempDerating CalDCLVHighTempEndDerating_AmbTemp;
  IdtTempDerating CalDCLVHighTempEndDerating_Buck_A;
  IdtTempDerating CalDCLVHighTempEndDerating_Buck_B;
  IdtTempDerating CalDCLVHighTempEndDerating_PushPull_A;
  IdtTempDerating CalDCLVHighTempEndDerating_PushPull_B;
  IdtTempDerating CalDCLVHighTempEndDerating_SyncRect;
  IdtTempDerating CalDCLVHighTempStartDerating_AmbTemp;
  IdtTempDerating CalDCLVHighTempStartDerating_Buck_A;
  IdtTempDerating CalDCLVHighTempStartDerating_Buck_B;
  IdtTempDerating CalDCLVHighTempStartDerating_PushPull_A;
  IdtTempDerating CalDCLVHighTempStartDerating_PushPull_B;
  IdtTempDerating CalDCLVHighTempStartDerating_SyncRect;
  IdtTempDerating CalDCLVLowTempEndDerating_AmbTemp;
  IdtTempDerating CalDCLVLowTempEndDerating_Buck_A;
  IdtTempDerating CalDCLVLowTempEndDerating_Buck_B;
  IdtTempDerating CalDCLVLowTempEndDerating_PushPull_A;
  IdtTempDerating CalDCLVLowTempEndDerating_PushPull_B;
  IdtTempDerating CalDCLVLowTempEndDerating_SyncRect;
  IdtTempDerating CalDCLVLowTempStartDerating_AmbTemp;
  IdtTempDerating CalDCLVLowTempStartDerating_Buck_A;
  IdtTempDerating CalDCLVLowTempStartDerating_Buck_B;
  IdtTempDerating CalDCLVLowTempStartDerating_PushPull_A;
  IdtTempDerating CalDCLVLowTempStartDerating_PushPull_B;
  IdtTempDerating CalDCLVLowTempStartDerating_SyncRect;
  IdtTempDerating CalOBCHighTempEndDerating_AmbTempOBC;
  IdtTempDerating CalOBCHighTempEndDerating_DCHV_M5;
  IdtTempDerating CalOBCHighTempEndDerating_DCHV_M6;
  IdtTempDerating CalOBCHighTempEndDerating_PFC_M1;
  IdtTempDerating CalOBCHighTempEndDerating_PFC_M4;
  IdtTempDerating CalOBCHighTempStartDerating_AmbTempOBC;
  IdtTempDerating CalOBCHighTempStartDerating_DCHV_M5;
  IdtTempDerating CalOBCHighTempStartDerating_DCHV_M6;
  IdtTempDerating CalOBCHighTempStartDerating_PFC_M1;
  IdtTempDerating CalOBCHighTempStartDerating_PFC_M4;
  IdtTempDerating CalOBCLowTempEndDerating_AmbTempOBC;
  IdtTempDerating CalOBCLowTempEndDerating_DCHV_M5;
  IdtTempDerating CalOBCLowTempEndDerating_DCHV_M6;
  IdtTempDerating CalOBCLowTempEndDerating_PFC_M1;
  IdtTempDerating CalOBCLowTempEndDerating_PFC_M4;
  IdtTempDerating CalOBCLowTempStartDerating_AmbTempOBC;
  IdtTempDerating CalOBCLowTempStartDerating_DCHV_M5;
  IdtTempDerating CalOBCLowTempStartDerating_DCHV_M6;
  IdtTempDerating CalOBCLowTempStartDerating_PFC_M1;
  IdtTempDerating CalOBCLowTempStartDerating_PFC_M4;
  IdtTempDerating CalOBC_FinishLinear;
  IdtTempDerating CalOBC_MaxDeviation;
  IdtTempDerating CalOBC_MinDeviation;
  IdtTempDerating CalOBC_StartLinear;
} Rte_Calprm_CtApDER_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApLVC_DEFAULT_RTE_CDATA_GROUP (4)
typedef struct
{
  uint16 CalDCLVDeratingThreshold;
  IdtDCLVTimeConfirmDerating CalDCLVTimeConfirmDerating;
} Rte_Calprm_CtApLVC_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApCPT_DEFAULT_RTE_CDATA_GROUP (5)
typedef struct
{
  IdtDebounceControlPilot CalDebounceControlPilot;
} Rte_Calprm_CtApCPT_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApRCD_DEFAULT_RTE_CDATA_GROUP (6)
typedef struct
{
  IdtDebounceRCD CalDebounceRCDHigh;
  IdtDebounceRCD CalDebounceRCDLow;
  boolean CalCtrlFlowRCDLine;
} Rte_Calprm_CtApRCD_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApLED_DEFAULT_RTE_CDATA_GROUP (7)
typedef struct
{
  IdtDutyLedCharge CalDutyBlueChargeError;
  IdtDutyLedCharge CalDutyBlueChargeInProgress;
  IdtDutyLedCharge CalDutyBlueEndOfCharge;
  IdtDutyLedCharge CalDutyBlueGuideManagement;
  IdtDutyLedCharge CalDutyBlueProgrammingCharge;
  IdtDutyLedCharge CalDutyGreenChargeError;
  IdtDutyLedCharge CalDutyGreenChargeInProgress;
  IdtDutyLedCharge CalDutyGreenEndOfCharge;
  IdtDutyLedCharge CalDutyGreenGuideManagement;
  IdtDutyLedCharge CalDutyGreenProgrammingCharge;
  IdtDutyPlug CalDutyPlug;
  IdtDutyLedCharge CalDutyRedChargeError;
  IdtDutyLedCharge CalDutyRedChargeInProgress;
  IdtDutyLedCharge CalDutyRedEndOfCharge;
  IdtDutyLedCharge CalDutyRedGuideManagement;
  IdtDutyLedCharge CalDutyRedProgrammingCharge;
  IdtTimeLedCharge CalTimeOffChargeError;
  IdtTimeLedCharge CalTimeOffChargeInProgress;
  IdtTimeLedCharge CalTimeOffEndOfCharge;
  IdtTimeLedCharge CalTimeOffGuideManagement;
  IdtTimePlug CalTimeOffPlugA;
  IdtTimePlug CalTimeOffPlugB;
  IdtTimePlug CalTimeOffPlugC;
  IdtTimeLedCharge CalTimeOffProgrammingCharge;
  IdtTimeLedCharge CalTimeOnChargeError;
  IdtTimeLedCharge CalTimeOnChargeInProgress;
  IdtTimeLedCharge CalTimeOnEndOfCharge;
  IdtTimeLedCharge CalTimeOnGuideManagement;
  IdtTimePlug CalTimeOnPlugA;
  IdtTimePlug CalTimeOnPlugB;
  IdtTimePlug CalTimeOnPlugC;
  IdtTimeLedCharge CalTimeOnProgrammingCharge;
} Rte_Calprm_CtApLED_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtHwAbsPOM_DEFAULT_RTE_CDATA_GROUP (8)
typedef struct
{
  IdtDutyCloseRelay CalDutyKeepOutputPrechargeRelaysClosed;
  IdtDutyCloseRelay CalDutyKeepRelayMonoClosed;
  IdtFrequencyActivateRelays CalFrequencyActivateRelays;
  IdtFrequencyPWM CalFrequencyPWM;
  IdtTimeCloseRelay CalTimeCloseOutputPrechargeRelays;
  IdtTimeCloseRelay CalTimeCloseRelayMono;
} Rte_Calprm_CtHwAbsPOM_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApLSD_DEFAULT_RTE_CDATA_GROUP (9)
typedef struct
{
  IdtELockDebounce CalELockDebounce;
  IdtELockSensorLimit CalELockSensorDriveHigh;
  IdtELockSensorLimit CalELockSensorDriveLow;
  IdtELockSensorLimit CalELockSensorLockHigh;
  IdtELockSensorLimit CalELockSensorLockLow;
  IdtELockSensorLimit CalELockSensorUnlockHigh;
  IdtELockSensorLimit CalELockSensorUnlockLow;
  IdtELockSensorFaultTime CalElockSensorSCGConfirmTime;
  IdtELockSensorFaultTime CalElockSensorSCGHealTime;
  IdtELockSensorFaultTime CalElockSensorSCPConfirmTime;
  IdtELockSensorFaultTime CalElockSensorSCPHealTime;
  IdtELockSensorFaultThreshold CalElockSensorThresholdSCG;
  IdtELockSensorFaultThreshold CalElockSensorThresholdSCP;
  boolean CalCtrlFlowPlugLock;
} Rte_Calprm_CtApLSD_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApLAD_DEFAULT_RTE_CDATA_GROUP (10)
typedef struct
{
  IdtElockThreshold CalElockThresholdNoCurrent;
  IdtElockThreshold CalElockThresholdOvercurrent;
  IdtElockThreshold CalElockThresholdSCG;
  IdtElockThreshold CalElockThresholdSCP;
  IdtAfts_ElockActuatorTime1 CalAfts_ElockActuatorTime1;
  IdtAfts_ElockActuatorTime2 CalAfts_ElockActuatorTime2;
  IdtElockActuatorTime CalOutputElockHOpenCircuitConfirmTime;
  IdtElockActuatorTime CalOutputElockHOpenCircuitHealTime;
  IdtElockActuatorTime CalOutputElockHSCGConfirmTime;
  IdtElockActuatorTime CalOutputElockHSCGHealTime;
  IdtElockActuatorTime CalOutputElockHSCPConfirmTime;
  IdtElockActuatorTime CalOutputElockHSCPHealTime;
  IdtElockActuatorTime CalOutputElockLOpenCircuitConfirmTime;
  IdtElockActuatorTime CalOutputElockLOpenCircuitHealTime;
  IdtElockActuatorTime CalOutputElockLOvercurrentConfirmTime;
  IdtElockActuatorTime CalOutputElockLSCGConfirmTime;
  IdtElockActuatorTime CalOutputElockLSCGHealTime;
  IdtElockActuatorTime CalOutputElockLSCPConfirmTime;
  IdtElockActuatorTime CalOutputElockLSCPHealTime;
  IdtElockActuatorTime CalOutputElockOvercurrentHealTime;
  boolean CalEnableElockActuator;
} Rte_Calprm_CtApLAD_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApJDD_DEFAULT_RTE_CDATA_GROUP (11)
typedef struct
{
  boolean CalEnableJDD;
} Rte_Calprm_CtApJDD_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApPLT_DEFAULT_RTE_CDATA_GROUP (12)
typedef struct
{
  IdtGlobalTimeoutPlantMode CalGlobalTimeoutPlantMode;
  IdtProximityDetectPlantMode CalMaxProximityDetectPlantMode;
  IdtProximityDetectPlantMode CalMinProximityDetectPlantMode;
  IdtProximityVoltageStartingPlantMode CalProximityVoltageStartingPlantMode;
  IdtDebounceControlPilotPlantMode CalDebounceControlPilotPlantMode;
  IdtDebounceDCRelayVoltagePlantMode CalDebounceDCRelayVoltagePlantMode;
  IdtDebouncePhasePlantMode CalDebouncePhasePlantMode;
  IdtDebounceProximityPlantMode CalDebounceProximityPlantMode;
  IdtControlPilotDutyPlantMode CalMaxControlPilotDutyPlantMode;
  IdtDCRelayVoltagePlantMode CalMaxDCRelayVoltagePlantMode;
  IdtPhasePlantmode CalMaxPhasePlantMode;
  IdtMaxTimePlantMode CalMaxTimePlantMode;
  IdtControlPilotDutyPlantMode CalMinControlPilotDutyPlantMode;
  IdtDCRelayVoltagePlantMode CalMinDCRelayVoltagePlantMode;
  IdtPhasePlantmode CalMinPhasePlantMode;
  boolean CalInhibitControlPilotDetection;
  boolean CalInhibitDCRelayVoltage;
  boolean CalInhibitPhaseVoltageDetection;
  boolean CalInhibitProximityDetection;
} Rte_Calprm_CtApPLT_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApCHG_DEFAULT_RTE_CDATA_GROUP (13)
typedef struct
{
  IdtMaxDemmandVoltage CalMaxDemmandVoltage;
  IdtMaxDiscoveryVoltage CalMaxDiscoveryVoltage;
  IdtMaxPrechargeVoltage CalMaxPrechargeVoltage;
  IdtStopChargeDemmandTimer CalStopChargeDemmandTimer;
  IdtTimeConfirmNoAC CalTimeConfirmNoAC;
  IdtBulkSocConf CalBulkSocConf;
  IdtDemandMaxCurrent CalDemandMaxCurrent;
  IdtEnergyCapacity CalEnergyCapacity;
  IdtMaxDemmandCurrent CalMaxDemmandCurrent;
  IdtMaxDiscoveryCurrent CalMaxDiscoveryCurrent;
  IdtMaxInputVoltage110V CalMaxInputVoltage110V;
  IdtMaxInputVoltage220V CalMaxInputVoltage220V;
  IdtMaxPrechargeCurrent CalMaxPrechargeCurrent;
  IdtMinInputVoltage110V CalMinInputVoltage110V;
  IdtMinInputVoltage220V CalMinInputVoltage220V;
  IdtTargetPrechargeCurrent CalTargetPrechargeCurrent;
  IdtThresholdNoAC CalThresholdNoAC;
  IdtTimeElockFaultDetected CalTimeElockFaultDetected;
} Rte_Calprm_CtApCHG_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApOBC_DEFAULT_RTE_CDATA_GROUP (14)
typedef struct
{
  IdtMaxOutputDCHVCurrent CalMaxOutputDCHVCurrent;
  IdtMaxOutputDCHVVoltage CalMaxOutputDCHVVoltage;
  IdtMinDCHVOutputVoltage CalMinDCHVOutputVoltage;
  IdtTimeRelaysPrechargeClosed CalTimeRelaysPrechargeClosed;
  CalVDCLinkRequiredMonophasic CalVDCLinkRequiredMonophasic;
  IdtVDCLinkRequiredTriphasic CalVDCLinkRequiredTriphasic;
} Rte_Calprm_CtApOBC_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApRLY_DEFAULT_RTE_CDATA_GROUP (15)
typedef struct
{
  IdtMaxPrechargeVDCLinkMono CalMaxPrechargeVDCLinkMono;
  IdtMaxPrechargeVDCLinkTriphasic CalMaxPrechargeVDCLinkTriphasic;
  IdtMinPrechargeVDCLinkMono CalMinPrechargeVDCLinkMono;
  IdtMinPrechargeVDCLinkTriphasic CalMinPrechargeVDCLinkTriphasic;
} Rte_Calprm_CtApRLY_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApILT_DEFAULT_RTE_CDATA_GROUP (16)
typedef struct
{
  IdtMaxTimeChargeError CalMaxTimeChargeError;
  IdtMaxTimeChargeInProgress CalMaxTimeChargeInProgress;
  IdtMaxTimeEndOfCharge CalMaxTimeEndOfCharge;
  IdtMaxTimeGuideManagement CalMaxTimeGuideManagement;
  IdtMaxTimeProgrammingCharge CalMaxTimeProgrammingCharge;
  IdtTimePushButtonKeepValue CalTimePushButtonKeepValue;
  IdtDebounceStateFailure CalDebounceStateFailure;
  IdtDebounceStateFinished CalDebounceStateFinished;
  IdtDebounceStateInProgress CalDebounceStateInProgress;
  IdtDebounceStateStopped CalDebounceStateStopped;
  IdtElockTimeError CalElockTimeError;
} Rte_Calprm_CtApILT_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApWUM_DEFAULT_RTE_CDATA_GROUP (17)
typedef struct
{
  IdtMaxTimeDiagTools CalMaxTimeDiagTools;
  IdtTimeWakeupHoldDiscontactorMax CalTimeWakeupHoldDiscontactorMax;
  IdtDebounceHoldNetworkCom CalDebounceHoldNetworkCom;
  IdtDebounceTempoEndCharge CalDebounceTempoEndCharge;
  IdtMaxTimeShutdown CalMaxTimeShutdown;
  IdtTimeWakeupCoolingMax CalTimeWakeupCoolingMax;
  IdtTimeWakeupCoolingMin CalTimeWakeupCoolingMin;
  IdtTimeWakeupHoldDiscontactorMin CalTimeWakeupHoldDiscontactorMin;
  IdtTimeWakeupPiStateInfo CalTimeWakeupPiStateInfo;
} Rte_Calprm_CtApWUM_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApOFM_DEFAULT_RTE_CDATA_GROUP (18)
typedef struct
{
  IdtOBC_MaxNumberRetries CalOBC_MaxNumberRetries;
} Rte_Calprm_CtApOFM_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApMSC_DEFAULT_RTE_CDATA_GROUP (19)
typedef struct
{
  IdtOBC_PowerMaxValue CalOBC_PowerMaxValue;
  IdtTimeLockDelay CalTimeLockDelay;
} Rte_Calprm_CtApMSC_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApPXL_DEFAULT_RTE_CDATA_GROUP (20)
typedef struct
{
  IdtProximityDetectVoltThreshold CalProximityDetectMaxVoltage;
  IdtProximityDetectVoltThreshold CalProximityDetectMinVoltage;
  IdtProximityDetectVoltThreshold CalProximityNotDetectMaxVoltage;
  IdtProximityDetectVoltThreshold CalProximityNotDetectMinVoltage;
  IdtDebounceProxDetect CalDebounceProxDetectConnected;
  IdtDebounceProxDetect CalDebounceProxDetectNotConnected;
  IdtDebounceProxDetect CalDebounceProxInvalid;
} Rte_Calprm_CtApPXL_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApPCOM_DEFAULT_RTE_CDATA_GROUP (21)
typedef struct
{
  uint32 CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_REFERENCE_HORAIRE;
  uint32 CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL;
  uint32 CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE;
  uint32 CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL;
  uint16 CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC;
  uint16 CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage;
  uint16 CalSignal_BMS1_125_Initial_Value_BMS_SOC;
  uint16 CalSignal_BMS1_125_Initial_Value_BMS_Voltage;
  uint16 CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt;
  uint16 CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt;
  uint16 CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow;
  uint16 CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow;
  uint16 CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow;
  uint16 CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow;
  uint16 CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE;
  uint16 CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE;
  uint16 CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage;
  uint16 CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent;
  uint16 CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent;
  uint16 CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage;
  uint16 CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd;
  uint16 CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd;
  IdtFrameDiagTime CalFrame_0F0_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_0F0_LostFrameHeal;
  IdtFrameDiagTime CalFrame_125_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_125_LostFrameHeal;
  IdtFrameDiagTime CalFrame_127_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_127_LostFrameHeal;
  IdtFrameDiagTime CalFrame_129_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_129_LostFrameHeal;
  IdtFrameDiagTime CalFrame_17B_ChkSumConfirm;
  IdtFrameDiagTime CalFrame_17B_ChkSumHeal;
  IdtFrameDiagTime CalFrame_17B_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_17B_LostFrameHeal;
  IdtFrameDiagTime CalFrame_17B_RCConfirm;
  IdtFrameDiagTime CalFrame_17B_RCHeal;
  IdtFrameDiagTime CalFrame_27A_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_27A_LostFrameHeal;
  IdtFrameDiagTime CalFrame_31B_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_31B_LostFrameHeal;
  IdtFrameDiagTime CalFrame_31E_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_31E_LostFrameHeal;
  IdtFrameDiagTime CalFrame_359_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_359_LostFrameHeal;
  IdtFrameDiagTime CalFrame_361_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_361_LostFrameHeal;
  IdtFrameDiagTime CalFrame_372_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_372_LostFrameHeal;
  IdtFrameDiagTime CalFrame_37E_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_37E_LostFrameHeal;
  IdtFrameDiagTime CalFrame_382_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_382_LostFrameHeal;
  IdtFrameDiagTime CalFrame_486_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_486_LostFrameHeal;
  IdtFrameDiagTime CalFrame_552_LostFrameConfirm;
  IdtFrameDiagTime CalFrame_552_LostFrameHeal;
  uint8 CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState;
  uint8 CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState;
  uint8 CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq;
  uint8 CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState;
  uint8 CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState;
  uint8 CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC;
  IdtSignalDiagTime CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage;
  IdtSignalDiagTime CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt;
  IdtSignalDiagTime CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt;
  IdtSignalDiagTime CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt;
  IdtSignalDiagTime CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt;
  uint8 CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault;
  uint8 CalSignal_BMS5_359_Initial_Value_BMS_Fault;
  uint8 CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt;
  uint8 CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable;
  uint8 CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt;
  uint8 CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt;
  uint8 CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable;
  uint8 CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt;
  IdtSignalDiagTime CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow;
  IdtSignalDiagTime CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow;
  IdtSignalDiagTime CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow;
  IdtSignalDiagTime CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow;
  uint8 CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status;
  uint8 CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status;
  IdtSignalDiagTime CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE;
  IdtSignalDiagTime CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE;
  uint8 CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC;
  uint8 CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus;
  uint8 CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle;
  uint8 CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup;
  uint8 CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup;
  uint8 CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup;
  uint8 CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE;
  uint8 CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE;
  uint8 CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC;
  IdtSignalDiagTime CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus;
  IdtSignalDiagTime CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE;
  IdtSignalDiagTime CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus;
  IdtSignalDiagTime CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE;
  uint8 CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand;
  uint8 CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation;
  uint8 CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq;
  uint8 CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand;
  uint8 CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation;
  uint8 CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq;
  IdtSignalDiagTime CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq;
  IdtSignalDiagTime CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation;
  IdtSignalDiagTime CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq;
  IdtSignalDiagTime CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq;
  IdtSignalDiagTime CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation;
  IdtSignalDiagTime CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_Fault;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_Status;
  uint8 CalSignal_DC1_345_Initial_Value_DCDC_Temperature;
  uint8 CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt;
  uint8 CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent;
  uint8 CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD;
  uint8 CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ;
  uint8 CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ;
  uint8 CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC;
  uint8 CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG;
  uint8 CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG;
  uint8 CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_NOMBRE_TRAMES;
  uint8 CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_Fault;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN;
  uint8 CalSignal_OBC1_3A3_Initial_Value_OBC_Status;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION;
  uint8 CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax;
  uint8 CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP;
  uint8 CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup;
  uint8 CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup;
  uint8 CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup;
  uint8 CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE;
  uint8 CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE;
  uint8 CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status;
  uint8 CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status;
  IdtSignalDiagTime CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status;
  IdtSignalDiagTime CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE;
  uint8 CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE;
  uint8 CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition;
  uint8 CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION;
  uint8 CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION;
  uint8 CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT;
  uint8 CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT;
  IdtSignalDiagTime CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT;
  IdtSignalDiagTime CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL;
  IdtSignalDiagTime CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE;
  IdtSignalDiagTime CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT;
  IdtSignalDiagTime CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL;
  IdtSignalDiagTime CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP;
  uint8 CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST;
  IdtSignalDiagTime CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM;
  uint8 CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag;
  uint8 CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP;
  uint8 CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2;
  uint8 CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag;
  uint8 CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP;
  uint8 CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2;
  IdtSignalDiagTime CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd;
  IdtSignalDiagTime CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd;
  uint8 CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition;
  uint8 CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition;
  IdtSignalDiagTime CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition;
  IdtSignalDiagTime CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition;
  IdtSignalDiagTime CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition;
  IdtSignalDiagTime CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition;
  IdtT_INHIB_DIAG_COM CalT_INHIB_DIAG_COM;
  IdtTimeBeforeRunningDiag CalTimeBeforeRunningDiagBSIInfo;
  IdtTimeBeforeRunningDiag CalTimeBeforeRunningDiagEVCU;
  IdtTimeBeforeRunningDiag CalTimeBeforeRunningDiagTBMU;
  IdtTo_DgMux_Prod CalTo_DgMux_Prod;
  boolean CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState;
  boolean CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState;
  boolean CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq;
  boolean CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC;
  boolean CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage;
  boolean CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt;
  boolean CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault;
  boolean CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt;
  boolean CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow;
  boolean CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow;
  boolean CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable;
  boolean CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt;
  boolean CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status;
  boolean CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE;
  boolean CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC;
  boolean CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand;
  boolean CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation;
  boolean CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq;
  boolean CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status;
  boolean CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition;
  boolean CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION;
  boolean CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT;
  boolean CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL;
  boolean CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP;
  boolean CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP;
  boolean CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd;
  boolean CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag;
  boolean CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP;
  boolean CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2;
  boolean CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition;
} Rte_Calprm_CtApPCOM_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApLFM_DEFAULT_RTE_CDATA_GROUP (22)
typedef struct
{
  IdtThresholdLVOvercurrentHW CalThresholdLVOvercurrentHW;
  IdtABSVehSpdThresholdOV CalABSVehSpdThresholdOV;
  IdtBatteryVoltageSafetyOVLimit1_ConfirmTime CalBatteryVoltageSafetyOVLimit1_ConfirmTime;
  IdtBatteryVoltageSafetyOVLimit1_Threshold CalBatteryVoltageSafetyOVLimit1_Threshold;
  IdtBatteryVoltageSafetyOVLimit2_ConfirmTime CalBatteryVoltageSafetyOVLimit2_ConfirmTime;
  IdtBatteryVoltageSafetyOVLimit2_Threshold CalBatteryVoltageSafetyOVLimit2_Threshold;
  IdtDCLV_MaxNumberRetries CalDCLV_MaxNumberRetries;
  IdtTimeBatteryVoltageSafety CalTimeBatteryVoltageSafety;
} Rte_Calprm_CtApLFM_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApFCT_DEFAULT_RTE_CDATA_GROUP (23)
typedef struct
{
  IdtThresholdTempFault CalThresholdSCG_AC1;
  IdtThresholdTempFault CalThresholdSCG_AC2;
  IdtThresholdTempFault CalThresholdSCG_AC3;
  IdtThresholdTempFault CalThresholdSCG_ACN;
  IdtThresholdTempFault CalThresholdSCG_DC1;
  IdtThresholdTempFault CalThresholdSCG_DC2;
  IdtThresholdTempFault CalThresholdSCP_AC1;
  IdtThresholdTempFault CalThresholdSCP_AC2;
  IdtThresholdTempFault CalThresholdSCP_AC3;
  IdtThresholdTempFault CalThresholdSCP_ACN;
  IdtThresholdTempFault CalThresholdSCP_DC1;
  IdtThresholdTempFault CalThresholdSCP_DC2;
  IdtTimeDiagTemp CalTimeConfSCG_AC1;
  IdtTimeDiagTemp CalTimeConfSCG_AC2;
  IdtTimeDiagTemp CalTimeConfSCG_AC3;
  IdtTimeDiagTemp CalTimeConfSCG_ACN;
  IdtTimeDiagTemp CalTimeConfSCG_DC1;
  IdtTimeDiagTemp CalTimeConfSCG_DC2;
  IdtTimeDiagTemp CalTimeConfSCP_AC1;
  IdtTimeDiagTemp CalTimeConfSCP_AC2;
  IdtTimeDiagTemp CalTimeConfSCP_AC3;
  IdtTimeDiagTemp CalTimeConfSCP_ACN;
  IdtTimeDiagTemp CalTimeConfSCP_DC1;
  IdtTimeDiagTemp CalTimeConfSCP_DC2;
  IdtTimeDiagTemp CalTimeHealSCG_AC1;
  IdtTimeDiagTemp CalTimeHealSCG_AC2;
  IdtTimeDiagTemp CalTimeHealSCG_AC3;
  IdtTimeDiagTemp CalTimeHealSCG_ACN;
  IdtTimeDiagTemp CalTimeHealSCG_DC1;
  IdtTimeDiagTemp CalTimeHealSCG_DC2;
  IdtTimeDiagTemp CalTimeHealSCP_AC1;
  IdtTimeDiagTemp CalTimeHealSCP_AC2;
  IdtTimeDiagTemp CalTimeHealSCP_AC3;
  IdtTimeDiagTemp CalTimeHealSCP_ACN;
  IdtTimeDiagTemp CalTimeHealSCP_DC1;
  IdtTimeDiagTemp CalTimeHealSCP_DC2;
  IdtRampGradient CalRampGradient_AC1;
  IdtRampGradient CalRampGradient_AC2;
  IdtRampGradient CalRampGradient_AC3;
  IdtRampGradient CalRampGradient_ACN;
  IdtRampGradient CalRampGradient_DC1;
  IdtRampGradient CalRampGradient_DC2;
  IdtSubstituteValueTemp CalSubstituteValueAC1_C;
  IdtSubstituteValueTemp CalSubstituteValueAC2_C;
  IdtSubstituteValueTemp CalSubstituteValueAC3_C;
  IdtSubstituteValueTemp CalSubstituteValueACN_C;
  IdtSubstituteValueTemp CalSubstituteValueDC1_C;
  IdtSubstituteValueTemp CalSubstituteValueDC2_C;
  boolean CalCtrlFlowAC1Plug;
  boolean CalCtrlFlowAC2Plug;
  boolean CalCtrlFlowAC3Plug;
  boolean CalCtrlFlowACNPlug;
  boolean CalCtrlFlowDC1Plug;
  boolean CalCtrlFlowDC2Plug;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC1;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC2;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_AC3;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_ACN;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_DC1;
  IdtLinTableGlobalTemp CalLinTableGlobalTemp_DC2;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC1;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC2;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_AC3;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_ACN;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_DC1;
  IdtLinTableGlobalVolt CalLinTableGlobalVoltage_DC2;
} Rte_Calprm_CtApFCT_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApDCH_DEFAULT_RTE_CDATA_GROUP (24)
typedef struct
{
  IdtCalThresholdUVTripDischarge CalThresholdUVTripDischarge;
  IdtCalTimeUVTripDischarge CalTimeUVTripDischarge;
} Rte_Calprm_CtApDCH_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApPSH_DEFAULT_RTE_CDATA_GROUP (25)
typedef struct
{
  IdtTimePushInhDelay CalTimePushInhDelay;
  IdtTimeDebChargePush CalTimeDebChargePushHigh;
  IdtTimeDebChargePush CalTimeDebChargePushLow;
  boolean CalCtrlFlowChargePush;
} Rte_Calprm_CtApPSH_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtApDGN_DEFAULT_RTE_CDATA_GROUP (26)
typedef struct
{
  IdtVehicleSpeedThresholdDiag CalVehicleSpeedThresholdDiag;
} Rte_Calprm_CtApDGN_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CtHwAbsAIM_DEFAULT_RTE_CDATA_GROUP (27)
typedef struct
{
  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave1;
  IdtVoltageExternalADCReference CalVoltageExternalADCReferenceWave2;
  IdtDebounceADCReference CalDebounceADCReference;
} Rte_Calprm_CtHwAbsAIM_DEFAULT_RTE_CDATA_GROUP_Type;

# define Rte_CalprmElementGroup_CpCalibration_DEFAULT_RTE_CALPRM_GROUP (28)
typedef struct
{
  IdtBatteryVoltMaxTest PpBatteryVoltMaxTest_DeBatteryVoltMaxTest;
  IdtBatteryVoltMinTest PpBatteryVoltMinTest_DeBatteryVoltMinTest;
  IdtDCLVFaultTimeToRetryDefault PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault;
  IdtDCLVFaultTimeToRetryShortCircuit PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit;
  IdtOBCFaultTimeToRetryDefault PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault;
  IdtOBCFaultTimeToRetryOvertemp PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp;
  IdtOBCFaultTimeToRetryVoltageError PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError;
  IdtDebounceOvercurrentDCHV PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV;
  IdtInputVoltageThreshold PpInputVoltageThreshold_DeInputVoltageThreshold;
  IdtVehStopMaxTest PpVehStopMaxTest_DeVehStopMaxTest;
} Rte_Calprm_CpCalibration_DEFAULT_RTE_CALPRM_GROUP_Type;

# define Rte_CalprmElementGroup_CtApAEM_DEFAULT_RTE_CDATA_GROUP (29)
typedef struct
{
  IdtDegMainWkuExtinctionTime UCE_tiDegMainWkuDeac_C;
  IdtTimeNomMainWkuDisord UCE_tiMainDisrdDet_C;
  IdtTimeNomMainWkuIncst UCE_tiMainIncstDet_C;
  IdtEnforcedMainWkuTime UCE_tiMainTransForc_C;
  IdtTimeNomMainWkuRehabilit UCE_tiMainWkuReh_C;
  IdtCOMLatchMaxDuration UCE_tiMaxTiComLatch_C;
  IdtInternalPartialWkuMaxDuration UCE_tiMaxTiIntPtlWku_C;
  IdtMasterPartialWkuY1MaxDuration UCE_tiMaxTiMstPtlWkuY1_C;
  IdtMasterPartialWkuY2MaxDuration UCE_tiMaxTiMstPtlWkuY2_C;
  IdtMasterPartialWkuY3MaxDuration UCE_tiMaxTiMstPtlWkuY3_C;
  IdtMasterPartialWkuY4MaxDuration UCE_tiMaxTiMstPtlWkuY4_C;
  IdtMasterPartialWkuY5MaxDuration UCE_tiMaxTiMstPtlWkuY5_C;
  IdtMasterPartialWkuY6MaxDuration UCE_tiMaxTiMstPtlWkuY6_C;
  IdtMasterPartialWkuY7MaxDuration UCE_tiMaxTiMstPtlWkuY7_C;
  IdtMasterPartialWkuY8MaxDuration UCE_tiMaxTiMstPtlWkuY8_C;
  IdtShutdownPreparationMaxTime UCE_tiMaxTiShutDownPrep_C;
  IdtMasterPartialWkuY1MinDuration UCE_tiMinTiMstPtlWkuY1_C;
  IdtMasterPartialWkuY2MinDuration UCE_tiMinTiMstPtlWkuY2_C;
  IdtMasterPartialWkuY3MinDuration UCE_tiMinTiMstPtlWkuY3_C;
  IdtMasterPartialWkuY4MinDuration UCE_tiMinTiMstPtlWkuY4_C;
  IdtMasterPartialWkuY5MinDuration UCE_tiMinTiMstPtlWkuY5_C;
  IdtMasterPartialWkuY6MinDuration UCE_tiMinTiMstPtlWkuY6_C;
  IdtMasterPartialWkuY7MinDuration UCE_tiMinTiMstPtlWkuY7_C;
  IdtMasterPartialWkuY8MinDuration UCE_tiMinTiMstPtlWkuY8_C;
  IdtShutdownPreparationMinTime UCE_tiMinTiShutDownPrep_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX10Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX11Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX12Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX13Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX14Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX15Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX16Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX1Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX2Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX3Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX4Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX5Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX6Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX7Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX8Lock_C;
  IdtSlavePartialWkuXLockTime UCE_tiPtlWkuX9Lock_C;
  IdtTimeRCDPulse UCE_tiRCDLineCmdAcv_C;
  IdtRCDLineGndSCConfirmTime UCE_tiRCDLineScgDet_C;
  IdtRCDLineGndSCRehabilitTime UCE_tiRCDLineScgReh_C;
  IdtExtinctionTime UCE_tiTransitoryDeac_C;
  IdtVehicleSpeedThreshold UCE_spdThdDegDeac_C;
  IdtVehicleSpeedDegThreshold UCE_spdThdNomDeac_C;
  IdtMainWkuValidTime UCE_tiMainWkuAcv_C;
  IdtMainWkuDevalidTime UCE_tiNomMainWkuDeac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX10Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX10Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX11Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX11Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX12Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX12Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX13Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX13Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX14Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX14Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX15Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX15Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX16Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX16Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX1Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX1Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX2Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX2Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX3Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX3Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX4Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX4Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX5Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX5Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX6Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX6Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX7Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX7Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX8Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX8Deac_C;
  IdtSlavePartialWkuXValidTime UCE_tiPtlWkuX9Acv_C;
  IdtSlavePartialWkuXDevalidTime UCE_tiPtlWkuX9Deac_C;
  boolean UCE_bInhPtlWkuX10_C;
  boolean UCE_bInhPtlWkuX11_C;
  boolean UCE_bInhPtlWkuX12_C;
  boolean UCE_bInhPtlWkuX13_C;
  boolean UCE_bInhPtlWkuX14_C;
  boolean UCE_bInhPtlWkuX15_C;
  boolean UCE_bInhPtlWkuX16_C;
  boolean UCE_bInhPtlWkuX1_C;
  boolean UCE_bInhPtlWkuX2_C;
  boolean UCE_bInhPtlWkuX3_C;
  boolean UCE_bInhPtlWkuX4_C;
  boolean UCE_bInhPtlWkuX5_C;
  boolean UCE_bInhPtlWkuX6_C;
  boolean UCE_bInhPtlWkuX7_C;
  boolean UCE_bInhPtlWkuX8_C;
  boolean UCE_bInhPtlWkuX9_C;
  boolean UCE_bInhPtlWkuY1_C;
  boolean UCE_bInhPtlWkuY2_C;
  boolean UCE_bInhPtlWkuY3_C;
  boolean UCE_bInhPtlWkuY4_C;
  boolean UCE_bInhPtlWkuY5_C;
  boolean UCE_bInhPtlWkuY6_C;
  boolean UCE_bInhPtlWkuY7_C;
  boolean UCE_bInhPtlWkuY8_C;
  boolean UCE_bSlavePtlWkuX10AcvMod_C;
  boolean UCE_bSlavePtlWkuX11AcvMod_C;
  boolean UCE_bSlavePtlWkuX12AcvMod_C;
  boolean UCE_bSlavePtlWkuX13AcvMod_C;
  boolean UCE_bSlavePtlWkuX14AcvMod_C;
  boolean UCE_bSlavePtlWkuX15AcvMod_C;
  boolean UCE_bSlavePtlWkuX16AcvMod_C;
  boolean UCE_bSlavePtlWkuX1AcvMod_C;
  boolean UCE_bSlavePtlWkuX2AcvMod_C;
  boolean UCE_bSlavePtlWkuX3AcvMod_C;
  boolean UCE_bSlavePtlWkuX4AcvMod_C;
  boolean UCE_bSlavePtlWkuX5AcvMod_C;
  boolean UCE_bSlavePtlWkuX6AcvMod_C;
  boolean UCE_bSlavePtlWkuX7AcvMod_C;
  boolean UCE_bSlavePtlWkuX8AcvMod_C;
  boolean UCE_bSlavePtlWkuX9AcvMod_C;
  boolean UCE_noUCETyp_C;
} Rte_Calprm_CtApAEM_DEFAULT_RTE_CDATA_GROUP_Type;

typedef struct
{
  Rte_Calprm_CpCalibration_DEFAULT_RTE_CALPRM_GROUP_Type Rte_Calprm_CpCalibration;
} Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type;

typedef struct
{
  Rte_Calprm_CtApAEM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApAEM;
  Rte_Calprm_CtApBAT_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApBAT;
  Rte_Calprm_CtApCHG_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApCHG;
  Rte_Calprm_CtApCPT_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApCPT;
  Rte_Calprm_CtApDCH_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApDCH;
  Rte_Calprm_CtApDER_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApDER;
  Rte_Calprm_CtApDGN_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApDGN;
  Rte_Calprm_CtApFCL_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApFCL;
  Rte_Calprm_CtApFCT_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApFCT;
  Rte_Calprm_CtApILT_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApILT;
  Rte_Calprm_CtApJDD_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApJDD;
  Rte_Calprm_CtApLAD_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApLAD;
  Rte_Calprm_CtApLED_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApLED;
  Rte_Calprm_CtApLFM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApLFM;
  Rte_Calprm_CtApLSD_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApLSD;
  Rte_Calprm_CtApLVC_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApLVC;
  Rte_Calprm_CtApMSC_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApMSC;
  Rte_Calprm_CtApOBC_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApOBC;
  Rte_Calprm_CtApOFM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApOFM;
  Rte_Calprm_CtApPCOM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApPCOM;
  Rte_Calprm_CtApPLS_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApPLS;
  Rte_Calprm_CtApPLT_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApPLT;
  Rte_Calprm_CtApPSH_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApPSH;
  Rte_Calprm_CtApPXL_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApPXL;
  Rte_Calprm_CtApRCD_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApRCD;
  Rte_Calprm_CtApRLY_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApRLY;
  Rte_Calprm_CtApWUM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtApWUM;
  Rte_Calprm_CtHwAbsAIM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtHwAbsAIM;
  Rte_Calprm_CtHwAbsPOM_DEFAULT_RTE_CDATA_GROUP_Type Rte_Calprm_CtHwAbsPOM;
} Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type;

# ifdef RTE_CORE

#  define RTE_START_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_VAR_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CALPRM_GROUP;

#  define RTE_STOP_SEC_VAR_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_VAR_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmInitRam_DEFAULT_RTE_CDATA_GROUP;

#  define RTE_STOP_SEC_VAR_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# endif


/**********************************************************************************************************************
 * Constant value definitions
 *********************************************************************************************************************/

# define RTE_START_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern CONST(SG_DC2, RTE_CONST) Rte_C_SG_DC2_0;

# define RTE_STOP_SEC_CONST_UNSPECIFIED
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
# include "Rte_DataHandleType.h"

# ifdef RTE_MICROSAR_PIM_EXPORT


/**********************************************************************************************************************
 * Calibration component and SW-C local calibration parameters
 *********************************************************************************************************************/

#  define RTE_START_SEC_CONST_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern CONST(Rte_Calprm_DEFAULT_RTE_CALPRM_GROUP_Type, RTE_CONST_DEFAULT_RTE_CALPRM_GROUP) Rte_CalprmRom_DEFAULT_RTE_CALPRM_GROUP;

#  define RTE_STOP_SEC_CONST_DEFAULT_RTE_CALPRM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern CONST(Rte_Calprm_DEFAULT_RTE_CDATA_GROUP_Type, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CalprmRom_DEFAULT_RTE_CDATA_GROUP;
extern CONST(uint8, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtApWUM_NvWUMBlockNeed_DefaultValue;
extern CONST(IdtPlantModeTestUTPlugin, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtApPLT_PLTNvMNeed_DefaultValue;
extern CONST(IdtArrayInitAIMVoltRef, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtHwAbsAIM_AIMVoltRefNvBlockNeed_DefaultValue;

#  define RTE_STOP_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS1_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS3_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS5_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS6_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS8_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS9_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSIInfo_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCtrlDCDC_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimParkCommand_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU2_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU3_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_552_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_PCANInfo_LostFrameCounter;
extern VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_TU_LostFrameCounter;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBlueCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterOC;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2CounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2CounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorCounterOverCurrent;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterOpenCircuit;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterOpenCircuit;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLCounterSCP;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockCounterSCG;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockCounterSCP;
extern VAR(IdtOBC_PowerAvailableControlPilot, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerAvailableControlPilot;
extern VAR(IdtOBC_PowerAvailableHardware, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerAvailableHardware;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterHeal;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm;
extern VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal;
extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApCPT_PimCPT_NvMRamMirror;
extern VAR(IdtOutputELockSensor, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSensor_PreviousToLock;
extern VAR(IdtOutputELockSensor, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSensor_PreviousToUnlock;
extern VAR(IdtELockSetPoint, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockSetPoint;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLVHWFault_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_FaultSatellite_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_InternalCom_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputOvercurrent_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShorcircuitHV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShortCircuitLV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShortCircuit_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageHV_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageLimit1_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageLimit2_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_Overvoltage_Error;
extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimMSC_PowerLatchFlag;
extern VAR(IdtOBC_PowerMaxCalculated, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApMSC_PimOBC_PowerMaxCalculated;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC1PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC2PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC3PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ACNPlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ActiveDischarge_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_BatteryHVUndervoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ControlPilot_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DC1PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DC2PlugOvertemp_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVHWFault_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOutputOvercurrent_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOutputShortCircuit_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOvervoltage_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DiagnosticNoACInput_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ElockLocked_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_FaultSatellites_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_OBCInternalCom_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_PFCHWFault_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ProximityLine_Error;
extern VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_VCUModeEPSRequest_Error;
extern VAR(IdtTimeoutPlantMode, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimGlobalTimeoutPlantMode;
extern VAR(IdtTimeoutPlantMode, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimTimeoutPlantMode;
extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPXL_PimPXL_NvMRamMirror;
extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApWUM_NvWUMBlockNeed_MirrorBlock;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedBluePrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedGreenPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedPlugPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultOC;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCL_PimLedRedPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2PrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2PrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultOpenCircuit;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorHPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultOpenCircuit;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorLPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockActuatorPrefaultOverCurrent;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLAD_PimElockNotCurrentMeasured;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockPrefaultSCG;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockPrefaultSCP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLVC_PimDCLVShutdownPathFSP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLVC_PimDCLVShutdownPathGPIO;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOBC_PimOBCShutdownPathFSP;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOBC_PimOBCShutdownPathGPIO;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS1_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS3_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS5_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS6_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS8_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS9_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSIInfo_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCtrlDCDC_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimParkCommand_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU2_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU3_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_552_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_PCANInfo_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_TU_LostFramePrefault;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeControlPilot;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeDCRelayVoltage;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModePhaseVoltage;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeProximity;
extern VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPXL_PimProximityFailure;
extern VAR(IdtArrayDGN_EOL_NVM_RamMirror, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApDGN_PimDGN_EOL_NVM_RamMirror;
extern VAR(IdtArrayJDDNvMRamMirror, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApJDD_PimJDD_NvMRamMirror;
extern VAR(IdtPCOMNvMArray, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_NvPCOMBlockNeed_MirrorBlock;
extern VAR(IdtPlantModeTestUTPlugin, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPLT_PimPlantModeTestUTPlugin;
extern VAR(IdtPSHNvMArray, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPSH_NvPSHBlockNeed_MirrorBlock;
extern VAR(IdtArrayInitAIMVoltRef, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock;

#  define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# endif

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

typedef unsigned int Rte_BitType;
/**********************************************************************************************************************
 * type and extern declarations of RTE internal variables
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Rte Init State Variable
 *********************************************************************************************************************/

# define RTE_STATE_UNINIT    (0U)
# define RTE_STATE_SCHM_INIT (1U)
# define RTE_STATE_INIT      (2U)

# define RTE_START_SEC_VAR_ZERO_INIT_8BIT
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern volatile VAR(uint8, RTE_VAR_ZERO_INIT) Rte_InitState; /* PRQA S 3408 */ /* MD_Rte_3408 */
extern volatile VAR(uint8, RTE_VAR_ZERO_INIT) Rte_StartTiming_InitState; /* PRQA S 0850, 3408 */ /* MD_MSR_MacroArgumentEmpty, MD_Rte_3408 */

# define RTE_STOP_SEC_VAR_ZERO_INIT_8BIT
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef RTE_CORE

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern P2CONST(SchM_ConfigType, AUTOMATIC, RTE_CONST) Rte_VarCfgPtr;

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Calibration Parameters (SW-C local and calibration component calibration parameters)
 *********************************************************************************************************************/

#  define RTE_START_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern CONST(uint8, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtApWUM_NvWUMBlockNeed_DefaultValue; /* PRQA S 3408 */ /* MD_Rte_3408 */
extern CONST(IdtPlantModeTestUTPlugin, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtApPLT_PLTNvMNeed_DefaultValue; /* PRQA S 3408 */ /* MD_Rte_3408 */
extern CONST(IdtArrayInitAIMVoltRef, RTE_CONST_DEFAULT_RTE_CDATA_GROUP) Rte_CtHwAbsAIM_AIMVoltRefNvBlockNeed_DefaultValue; /* PRQA S 3408 */ /* MD_Rte_3408 */

#  define RTE_STOP_SEC_CONST_DEFAULT_RTE_CDATA_GROUP_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Buffers for unqueued S/R
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(boolean, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtEVSEMaximumPowerLimit, RTE_VAR_INIT) Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_OBCMainContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_OBCQuickChargeContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(EVSE_RTAB_STOP_CHARGE, RTE_VAR_INIT) Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_OBCStartSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpStopConditions_DeStopConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_CP_connection_Status, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMaxInputACCurrentEVSE, RTE_VAR_INIT) Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_ActivedischargeSt, RTE_VAR_INIT) Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_Temperature, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Temp_Derating_Factor, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_OBCTemp, RTE_VAR_INIT) Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpOBCDerating_DeOBCDerating; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPDERATING_OBC, RTE_VAR_INIT) Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_FaultLampRequest, RTE_VAR_INIT) Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCLVError_DeDCLVError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_Fault, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_OutputVoltage, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_ElockState, RTE_VAR_INIT) Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_InputCurrent, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_VoltageReference, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUP_RequestDCLVStatus, RTE_VAR_INIT) Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_VoltageReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUP_CommandVDCLink_V, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUP_RequestPFCStatus, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUP_RequestStatusDCHV, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_Fault, RTE_VAR_INIT) Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(ABS_VehSpdValidFlag, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_AuxBattVolt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_CC2_connection_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_FastChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_Fault, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_HighestChargeCurrentAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_HighestChargeVoltageAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_MainConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_OnBoardChargerEnable, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_QuickChargeConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_RelayOpenReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_SOC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_SlowChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BMS_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_ChargeState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_ChargeTypeStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_LockedVehicle, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_MainWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_PostDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_PreDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_VCUHeatPumpWorkingMode, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(BSI_VCUSynchroGPC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(COUPURE_CONSO_CTP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(COUPURE_CONSO_CTPE2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DATA_ACQ_JDD_BSI_2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_ADC_NTC_MOD_5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_ADC_NTC_MOD_6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_Command_Current_Reference, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_CAP_FAIL_H, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_CAP_FAIL_L, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OC_NEG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OC_POS, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OT_IN, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OT_NTC_MOD5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OT_NTC_MOD6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_OV_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCHV_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Applied_Derating_Factor, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Input_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Measured_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Measured_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OC_A_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OC_A_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OC_B_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OC_B_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OT_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OV_INT_A_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OV_INT_B_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_OV_UV_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_PWM_STOP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_Power, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_T_L_BUCK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_T_PP_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_T_PP_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_T_SW_BUCK_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_T_SW_BUCK_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCLV_T_TX_PP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DIAG_INTEGRA_ELEC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(EFFAC_DEFAUT_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(MODE_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OC_PH1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OC_PH2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OC_PH3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OC_SHUNT1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OC_SHUNT2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OC_SHUNT3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OT_NTC1_M1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OT_NTC1_M3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OT_NTC1_M4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OV_DCLINK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OV_VPH12, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OV_VPH23, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_OV_VPH31, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_UVLO_ISO4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_UVLO_ISO7, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IPH1_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IPH2_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_IPH3_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_PFCStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_Temp_M1_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_Temp_M3_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_Temp_M4_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH1_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH2_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH3_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH12_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH23_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH31_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH23_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_VPH31_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(PFC_Vdclink_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_AccPedalPosition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_ActivedischargeCommand, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_CDEAccJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_CDEApcJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_CompteurRazGct, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_CptTemporel, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_DCDCActivation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_DCDCVoltageReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_DDEGMVSEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_DMDActivChiller, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_DMDMeap2SEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_DiagMuxOnPwt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_EPWT_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_ElecMeterSaturation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_EtatPrincipSev, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_EtatReseauElec, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_Keyposition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_Kilometrage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_PosShuntJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_PrecondElecWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VCU_StopDelayedHMIWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_InputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_OutputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtVoltageCorrectionOffset, RTE_VAR_INIT) Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtAmbientTemperaturePhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtElockFeedbackCurrent, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtElockFeedbackLock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtElockFeedbackUnlock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtExtOpPlugLockRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtHWEditionDetected, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlugLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtProximityDetectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtTempSyncRectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtDutyControlPilot, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */

#  define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#  define RTE_START_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(IdtAppRCDECUState, RTE_VAR_INIT) Rte_CpApAEM_PpAppRCDECUState_DeAppRCDECUState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpCANComRequest_DeCANComRequest; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(sint32, RTE_VAR_INIT) Rte_CpApAEM_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpECU_WakeupMain_DeECU_WakeupMain; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpFaultRCDLineSC_DeFaultRCDLineSC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_CoolingWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_HVBattRechargeWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_HoldDiscontactorWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_PIStateInfoWakeup, RTE_VAR_INIT) Rte_CpApAEM_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_ECUElecStateRCD, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_PostDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_PreDriveWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_PrecondElecWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_StopDelayedHMIWupState, RTE_VAR_INIT) Rte_CpApAEM_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApAEM_PpShutdownAuthorization_DeShutdownAuthorization; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(EDITION_CALIB, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_CALIB_EDITION_CALIB; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(EDITION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_EDITION_SOFT_EDITION_SOFT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_CommunicationSt, RTE_VAR_INIT) Rte_CpApDGN_PpInt_OBC_CommunicationSt_OBC_CommunicationSt; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VERSION_APPLI, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_APPLI_VERSION_APPLI; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VERSION_SOFT, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SOFT_VERSION_SOFT; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VERSION_SYSTEME, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERSION_SYSTEME_VERSION_SYSTEME; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VERS_DATE2_ANNEE, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VERS_DATE2_JOUR, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(VERS_DATE2_MOIS, RTE_VAR_INIT) Rte_CpApDGN_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedBrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedGrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtExtChLedRGBCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtChLedRGBCtl_DeExtChLedRrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtExtPlgLedrCtl, RTE_VAR_INIT) Rte_CpApFCL_PpExtPlgLedrCtl_DeExtPlgLedrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultOC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedBlueFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultOC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedGreenFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultOC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedFaults_DeLedRedFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeBlueLedMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeGreenLedMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DePlugLedMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpLedMonitoringConditions_DeRedLedMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultOC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApFCL_PpPlugLedFaults_DeLedPlugFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_PushChargeType, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_PushChargeType_OBC_PushChargeType; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_RechargeHMIState, RTE_VAR_INIT) Rte_CpApILT_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeChargeInProgress; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeEndOfCharge; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeGuideManagement; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLedModes_DeProgrammingCharge; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockLED2BEPR_DeLockLED2BEPR; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApILT_PpLockStateError_DeLockStateError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_0, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_1, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_2, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_3, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_4, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_5, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_6, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(NEW_JDD_OBC_DCDC_BYTE_7, RTE_VAR_INIT) Rte_CpApJDD_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApJDD_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultOpenCircuit; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockHFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultOpenCircuit; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCG; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockLFaultSCP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpELockFaults_DeOutputElockOvercurrent; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApLAD_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedBrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedGrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtChLedCtl, RTE_VAR_INIT) Rte_CpApLED_PpChLedRGBCtl_DeChLedRrCtl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlgLedrCtrl, RTE_VAR_INIT) Rte_CpApLED_PpPlgLedrCtrl_DePlgLedrCtrl; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApMSC_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_OBCDCDCRTPowerLoad, RTE_VAR_INIT) Rte_CpApMSC_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_PowerMax, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_PowerMax_OBC_PowerMax; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_SocketTempL, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempL; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_SocketTempN, RTE_VAR_INIT) Rte_CpApMSC_PpInt_OBC_SocketTemp_OBC_SocketTempN; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtELockSetPoint, RTE_VAR_INIT) Rte_CpApPLT_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeError; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeChargeInProgress; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeEndOfCharge; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeGuideManagement; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLedModes_PlantMode_DeProgrammingCharge; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeState_DePlantModeState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlantModeTestInfoDID_Result, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlantModeTestInfoDID_State, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtPlantModeTestInfoDID_Test, RTE_VAR_INIT) Rte_CpApPLT_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(IdtRECHARGE_HMI_STATE, RTE_VAR_INIT) Rte_CpApPLT_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApPSH_PpOutputChargePushFil_DeOutputChargePushFil; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_RCDLineState, RTE_VAR_INIT) Rte_CpApRCD_PpInt_SUPV_RCDLineState_SUPV_RCDLineState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputPrechargeRelays_DeOutputPrechargeRelays; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApRLY_PpOutputRelayMono_DeOutputRelayMono; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(DCDC_OVERTEMP, RTE_VAR_INIT) Rte_CpApTBD_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(OBC_DCChargingPlugAConnConf, RTE_VAR_INIT) Rte_CpApTBD_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_DTCRegistred, RTE_VAR_INIT) Rte_CpApTBD_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpDiagToolsRequest_DeDiagToolsRequest; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_CoolingWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_HVBattChargeWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_HoldDiscontactorWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(SUPV_PIStateInfoWupState, RTE_VAR_INIT) Rte_CpApWUM_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */
extern VAR(boolean, RTE_VAR_INIT) Rte_CpApWUM_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP; /* PRQA S 3408, 1504 */ /* MD_Rte_3408, MD_MSR_Rule8.7 */

#  define RTE_STOP_SEC_VAR_OsApplication_QM_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

typedef struct
{
  Rte_BitType Rte_ModeSwitchAck_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl_Ack : 1;
  Rte_BitType Rte_ModeSwitchAck_Dcm_DcmEcuReset_DcmEcuReset_Ack : 1;
} Rte_OsApplication_BSW_QM_AckFlagsType;

#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_OsApplication_BSW_QM_AckFlagsType, RTE_VAR_INIT) Rte_OsApplication_BSW_QM_AckFlags;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

typedef struct
{
  Rte_BitType Rte_ModeSwitchAck_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode_Ack : 1;
} Rte_SystemApplication_OsCore0_AckFlagsType;

#  define RTE_START_SEC_VAR_SystemApplication_OsCore0_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Rte_SystemApplication_OsCore0_AckFlagsType, RTE_VAR_INIT) Rte_SystemApplication_OsCore0_AckFlags;

#  define RTE_STOP_SEC_VAR_SystemApplication_OsCore0_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Data structures for mode management
 *********************************************************************************************************************/


#  define RTE_START_SEC_VAR_SystemApplication_OsCore0_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(BswM_ESH_Mode, RTE_VAR_INIT) Rte_ModeMachine_BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode;

#  define RTE_STOP_SEC_VAR_SystemApplication_OsCore0_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Dcm_DiagnosticSessionControlType, RTE_VAR_INIT) Rte_ModeMachine_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#  define RTE_START_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(Dcm_EcuResetType, RTE_VAR_INIT) Rte_ModeMachine_Dcm_DcmEcuReset_DcmEcuReset;

#  define RTE_STOP_SEC_VAR_INIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# endif /* defined(RTE_CORE) */

#endif /* RTE_TYPE_H */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_1039:  MISRA rule: Rule1.2
     Reason:     Same macro and function names are required to meet AUTOSAR spec.
     Risk:       No functional risk. Macro will be undefined before function definition.
     Prevention: Not required.

   MD_Rte_3408:  MISRA rule: Rule8.4
     Reason:     For the purpose of monitoring during calibration or debugging it is necessary to use non-static declarations.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       No functional risk.
     Prevention: Not required.

*/
