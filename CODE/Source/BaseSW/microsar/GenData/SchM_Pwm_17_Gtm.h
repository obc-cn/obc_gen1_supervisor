/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SchM_Pwm_17_Gtm.h
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  Header of BSW Scheduler for BSW Module <Pwm_17_Gtm>
 *********************************************************************************************************************/
#ifndef SCHM_PWM_17_GTM_H
# define SCHM_PWM_17_GTM_H

# ifdef __cplusplus
extern "C" {
# endif  /* __cplusplus */

# include "SchM_Pwm_17_Gtm_Type.h"

# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) SchM_Enter_Pwm_17_Gtm_StartChannel(void);
FUNC(void, RTE_CODE) SchM_Exit_Pwm_17_Gtm_StartChannel(void);
FUNC(void, RTE_CODE) SchM_Enter_Pwm_17_Gtm_SyncDuty(void);
FUNC(void, RTE_CODE) SchM_Exit_Pwm_17_Gtm_SyncDuty(void);

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

# ifdef __cplusplus
} /* extern "C" */
# endif  /* __cplusplus */

#endif /* SCHM_PWM_17_GTM_H */
