/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EthTrcv_Ar7000
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EthTrcv_30_Ar7000_Lcfg.h
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


#if !defined(ETHTRCV_30_AR7000_LCFG_H)
#define ETHTRCV_30_AR7000_LCFG_H

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
#include "EthTrcv_GeneralTypes.h"
#include "EthTrcv_30_Ar7000_Types.h"
#include "EthTrcv_30_Ar7000_HwCfg.h"


/**********************************************************************************************************************
 *  VARIANT DEPENDENT GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  DATA TYPES
 *********************************************************************************************************************/
 

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/
#define ETHTRCV_30_Ar7000_DONT_CARE  (0xFFU)

/**********************************************************************************************************************
 *  RTM MEASUREMENT POINT MACROS
 *********************************************************************************************************************/

#define EthTrcv_30_Ar7000_Rtm_Start(Type)

#define EthTrcv_30_Ar7000_Rtm_Stop(Type)

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/


#define ETHTRCV_30_AR7000_START_SEC_CONST_8BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(uint8,   ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_HwSubIndex[1];
extern CONST(uint8,   ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_HwIndex[1];
extern CONST(uint8,   ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Speed[1];
extern CONST(boolean, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_AutoNegotiation[1];

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_8BIT
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

extern CONST(EthTrcv_30_Ar7000_ConfigType, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_Config;

extern CONST(uint8, ETHTRCV_30_AR7000_CONST) EthTrcv_30_Ar7000_MiiMode[1];

#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

#define ETHTRCV_30_AR7000_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *  FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 *  VARIANT DEPENDENT GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/
 
/**********************************************************************************************************************
 *  ROM VARIABLES
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define ETHTRCV_30_AR7000_STOP_SEC_CONST_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/**********************************************************************************************************************
 *  RAM VARIABLES
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define ETHTRCV_30_AR7000_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#endif /* ETHTRCV_30_AR7000_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_Lcfg.h
 *********************************************************************************************************************/
 
