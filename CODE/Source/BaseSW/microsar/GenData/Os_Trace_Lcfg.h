/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Trace_Lcfg.h
 *   Generation Time: 2020-08-19 13:07:50
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#ifndef OS_TRACE_LCFG_H
# define OS_TRACE_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */
# include "Os_Trace_Types.h"

/* Os kernel module dependencies */

/* Os hal dependencies */

/* User file includes */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Dynamic trace data: OsCore0 */
extern VAR(Os_TraceCoreType, OS_VAR_NOINIT) OsCfg_Trace_OsCore0_Dyn;

# define OS_STOP_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Trace configuration data: ASILB_MAIN_TASK */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASILB_MAIN_TASK;

/*! Trace configuration data: ASILB_init_task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASILB_init_task;

/*! Trace configuration data: Default_BSW_Async_QM_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_BSW_Async_QM_Task;

/*! Trace configuration data: Default_BSW_Async_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_BSW_Async_Task;

/*! Trace configuration data: Default_BSW_Sync_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_BSW_Sync_Task;

/*! Trace configuration data: Default_Init_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_Init_Task;

/*! Trace configuration data: Default_RTE_Mode_switch_Task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Default_RTE_Mode_switch_Task;

/*! Trace configuration data: IdleTask_OsCore0 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_IdleTask_OsCore0;

/*! Trace configuration data: QM_MAIN_TASK */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QM_MAIN_TASK;

/*! Trace configuration data: QM_init_task */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QM_init_task;

/*! Trace configuration data: CanIsr_1 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1;

/*! Trace configuration data: CanIsr_2 */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2;

/*! Trace configuration data: CounterIsr_SystemTimer */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CounterIsr_SystemTimer;

/*! Trace configuration data: DMACH10SR_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DMACH10SR_ISR;

/*! Trace configuration data: DMACH11SR_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DMACH11SR_ISR;

/*! Trace configuration data: GTMTOM1SR0_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_GTMTOM1SR0_ISR;

/*! Trace configuration data: PLC_Interrupt */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_PLC_Interrupt;

/*! Trace configuration data: QSPI1ERR_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QSPI1ERR_ISR;

/*! Trace configuration data: QSPI1PT_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QSPI1PT_ISR;

/*! Trace configuration data: QSPI1UD_ISR */
extern CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_QSPI1UD_ISR;

# define OS_STOP_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_TRACE_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Trace_Lcfg.h
 *********************************************************************************************************************/
