/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_OsApplication_ASILB.c
 *           Config:  OBCP11.dpa
 *      ECU-Project:  OBCP11
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  RTE implementation file
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0857 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define RTE_CORE
#include "Os.h" /* PRQA S 0828, 0883 */ /* MD_MSR_Dir1.1, MD_Rte_Os */
#include "Rte_Type.h"
#include "Rte_Main.h"

#include "Rte_BswM.h"
#include "Rte_ComM.h"
#include "Rte_CtApAEM.h"
#include "Rte_CtApBAT.h"
#include "Rte_CtApCHG.h"
#include "Rte_CtApCPT.h"
#include "Rte_CtApDCH.h"
#include "Rte_CtApDER.h"
#include "Rte_CtApDGN.h"
#include "Rte_CtApFCL.h"
#include "Rte_CtApFCT.h"
#include "Rte_CtApILT.h"
#include "Rte_CtApJDD.h"
#include "Rte_CtApLAD.h"
#include "Rte_CtApLED.h"
#include "Rte_CtApLFM.h"
#include "Rte_CtApLSD.h"
#include "Rte_CtApLVC.h"
#include "Rte_CtApMSC.h"
#include "Rte_CtApOBC.h"
#include "Rte_CtApOFM.h"
#include "Rte_CtApPCOM.h"
#include "Rte_CtApPLS.h"
#include "Rte_CtApPLT.h"
#include "Rte_CtApPSH.h"
#include "Rte_CtApPXL.h"
#include "Rte_CtApRCD.h"
#include "Rte_CtApRLY.h"
#include "Rte_CtApTBD.h"
#include "Rte_CtApWUM.h"
#include "Rte_CtHwAbsAIM.h"
#include "Rte_CtHwAbsIOM.h"
#include "Rte_CtHwAbsPIM.h"
#include "Rte_CtHwAbsPOM.h"
#include "Rte_Dcm.h"
#include "Rte_DemMaster_0.h"
#include "Rte_DemSatellite_0.h"
#include "Rte_Det.h"
#include "Rte_EcuM.h"
#include "Rte_NvM.h"
#include "Rte_Os_OsCore0_swc.h"
#include "Rte_WdgM_OsApplication_ASILB.h"
#include "SchM_Adc.h"
#include "SchM_BswM.h"
#include "SchM_Can.h"
#include "SchM_CanIf.h"
#include "SchM_CanSM.h"
#include "SchM_CanTp.h"
#include "SchM_CanTrcv_30_Tja1145.h"
#include "SchM_Com.h"
#include "SchM_ComM.h"
#include "SchM_Dcm.h"
#include "SchM_Dem.h"
#include "SchM_Det.h"
#include "SchM_Dio.h"
#include "SchM_EcuM.h"
#include "SchM_EthIf.h"
#include "SchM_EthSM.h"
#include "SchM_EthTrcv_30_Ar7000.h"
#include "SchM_Eth_30_Ar7000.h"
#include "SchM_Exi.h"
#include "SchM_Fee.h"
#include "SchM_Fls_17_Pmu.h"
#include "SchM_Gpt.h"
#include "SchM_Icu_17_GtmCcu6.h"
#include "SchM_Irq.h"
#include "SchM_Mcu.h"
#include "SchM_NvM.h"
#include "SchM_PduR.h"
#include "SchM_Port.h"
#include "SchM_Pwm_17_Gtm.h"
#include "SchM_Scc.h"
#include "SchM_Spi.h"
#include "SchM_TcpIp.h"
#include "SchM_WdgM.h"
#include "SchM_Wdg_30_TLE4278G.h"
#include "SchM_Xcp.h"

#include "Rte_Hook.h"

#include "Com.h"
#if defined(IL_ASRCOM_VERSION)
# define RTE_USE_COM_TXSIGNAL_RDACCESS
#endif

#include "Rte_Cbk.h"

/* AUTOSAR 3.x compatibility */
#if !defined (RTE_LOCAL)
# define RTE_LOCAL static
#endif


/**********************************************************************************************************************
 * API for enable / disable interrupts global
 *********************************************************************************************************************/

#if defined(osDisableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableAllInterrupts() osDisableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_DisableAllInterrupts() DisableAllInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableGlobalKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableAllInterrupts() osEnableGlobalKM()   /* MICROSAR OS */
#else
# define Rte_EnableAllInterrupts() EnableAllInterrupts()   /* AUTOSAR OS */
#endif

/**********************************************************************************************************************
 * API for enable / disable interrupts up to the systemLevel
 *********************************************************************************************************************/

#if defined(osDisableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_DisableOSInterrupts() osDisableLevelKM()   /* MICROSAR OS */
#else
# define Rte_DisableOSInterrupts() SuspendOSInterrupts()   /* AUTOSAR OS */
#endif

#if defined(osEnableLevelKM) && !defined(RTE_DISABLE_ENHANCED_INTERRUPT_LOCK_API)
# define Rte_EnableOSInterrupts() osEnableLevelKM()   /* MICROSAR OS */
#else
# define Rte_EnableOSInterrupts() ResumeOSInterrupts()   /* AUTOSAR OS */
#endif


/**********************************************************************************************************************
 * Buffers for unqueued S/R
 *********************************************************************************************************************/

#define RTE_START_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtBatteryVoltageState, RTE_VAR_INIT) Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtEVSEMaximumPowerLimit, RTE_VAR_INIT) Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtInputVoltageMode, RTE_VAR_INIT) Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_OBCMainContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_OBCQuickChargeContactorReq, RTE_VAR_INIT) Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(EVSE_RTAB_STOP_CHARGE, RTE_VAR_INIT) Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_ACRange, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_InputVoltageSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_OBCStartSt, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_OBC_Status = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_Status, RTE_VAR_INIT) Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue = TRUE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApCHG_PpStopConditions_DeStopConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_CP_connection_Status, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status = 1U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_ChargingMode, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode = 1U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_PlugVoltDetection, RTE_VAR_INIT) Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMaxInputACCurrentEVSE, RTE_VAR_INIT) Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_ActivedischargeSt, RTE_VAR_INIT) Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_Temperature, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature = 40U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Temp_Derating_Factor, RTE_VAR_INIT) Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor = 0x400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_OBCTemp, RTE_VAR_INIT) Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp = 100U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApDER_PpOBCDerating_DeOBCDerating = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPDERATING_OBC, RTE_VAR_INIT) Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC = 11000U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_FaultLampRequest, RTE_VAR_INIT) Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempAftsales, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas = 400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas = 400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas = 400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas = 400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas = 400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputTempMeas, RTE_VAR_INIT) Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas = 400U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLFM_PpDCLVError_DeDCLVError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_Fault, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_OutputVoltage, RTE_VAR_INIT) Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_ElockState, RTE_VAR_INIT) Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtOutputELockSensor, RTE_VAR_INIT) Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_InputCurrent, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_Status, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_VoltageReference, RTE_VAR_INIT) Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUP_RequestDCLVStatus, RTE_VAR_INIT) Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_CurrentReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_VoltageReference, RTE_VAR_INIT) Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference = 2100U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_HighVoltConnectionAllowed, RTE_VAR_INIT) Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUP_CommandVDCLink_V, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUP_RequestPFCStatus, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(SUP_RequestStatusDCHV, RTE_VAR_INIT) Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPOST_Result, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue = TRUE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_Fault, RTE_VAR_INIT) Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(ABS_VehSpd, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(ABS_VehSpdValidFlag, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_AuxBattVolt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_CC2_connection_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_DCRelayVoltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_FastChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_Fault, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_HighestChargeCurrentAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_HighestChargeVoltageAllow, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_MainConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_OnBoardChargerEnable, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_QuickChargeConnectorState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_RelayOpenReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_SOC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_SlowChargeSt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BMS_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_ChargeState, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState = 3U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_ChargeTypeStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_LockedVehicle, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_MainWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_PostDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_PreDriveWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_VCUHeatPumpWorkingMode, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(BSI_VCUSynchroGPC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(COUPURE_CONSO_CTP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(COUPURE_CONSO_CTPE2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DATA_ACQ_JDD_BSI_2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_ADC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_ADC_NTC_MOD_5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_ADC_NTC_MOD_6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_ADC_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_Command_Current_Reference, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_DCHVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_CAP_FAIL_H, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_CAP_FAIL_L, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OC_IOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OC_NEG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OC_POS, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OT_IN, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OT_NTC_MOD5, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OT_NTC_MOD6, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_OV_VOUT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCHV_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Applied_Derating_Factor, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_DCLVStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus = 2U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Input_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Input_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Measured_Current, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Measured_Voltage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OC_A_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OC_A_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OC_B_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OC_B_LV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OT_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OV_INT_A_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OV_INT_B_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_OV_UV_HV_FAULT, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_PWM_STOP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_Power, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_T_L_BUCK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_T_PP_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_T_PP_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_T_SW_BUCK_A, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_T_SW_BUCK_B, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCLV_T_TX_PP, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DIAG_INTEGRA_ELEC, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(EFFAC_DEFAUT_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(MODE_DIAG, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OC_PH1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OC_PH2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OC_PH3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OC_SHUNT1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OC_SHUNT2, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OC_SHUNT3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OT_NTC1_M1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OT_NTC1_M3, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OT_NTC1_M4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OV_DCLINK, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OV_VPH12, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OV_VPH23, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_OV_VPH31, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_UVLO_ISO4, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_UVLO_ISO7, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IOM_ERR_UV_12V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IPH1_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IPH2_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_IPH3_RMS_0A1, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_PFCStatus, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus = 8U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_Temp_M1_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C = 75U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_Temp_M3_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_Temp_M4_C, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C = 75U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH1_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH2_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH3_Freq_Hz, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH12_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH23_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH31_Peak_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH12_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH23_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_VPH31_RMS_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(PFC_Vdclink_V, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_AccPedalPosition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_ActivedischargeCommand, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_CDEAccJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_CDEApcJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_CompteurRazGct, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_CptTemporel, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_DCDCActivation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation = 1U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_DCDCVoltageReq, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_DDEGMVSEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_DMDActivChiller, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_DMDMeap2SEEM, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_DiagMuxOnPwt, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_EPWT_Status, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_ElecMeterSaturation, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_EtatGmpHyb, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_EtatPrincipSev, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_EtatReseauElec, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_Keyposition, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_Kilometrage, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_ModeEPSRequest, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_PosShuntJDD, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD = 3U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_PrecondElecWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(VCU_StopDelayedHMIWakeup, RTE_VAR_INIT) Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_InputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(DCDC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_OutputCurrent, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_OutputVoltage, RTE_VAR_INIT) Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtFaultLevel, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtVoltageCorrectionOffset, RTE_VAR_INIT) Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset = 0;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(OBC_ChargingConnectionConfirmati, RTE_VAR_INIT) Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati = 1U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtAmbientTemperaturePhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtBatteryVolt, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 255U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtElockFeedbackCurrent, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtElockFeedbackLock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtElockFeedbackUnlock, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtExtOpPlugLockRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw = 255U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtHWEditionDetected, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtMsrTempRaw, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtPlugLedFeedbackPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtProximityDetectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = 65535U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtTempSyncRectPhysicalValue, RTE_VAR_INIT) Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue = 65U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue = FALSE;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(IdtDutyControlPilot, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 0U;
/* PRQA L:L1 */
/* PRQA S 3408, 1504, 1514 L1 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514 */
VAR(boolean, RTE_VAR_INIT) Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = FALSE;
/* PRQA L:L1 */

#define RTE_STOP_SEC_VAR_OsApplication_ASILB_INIT_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Per-Instance Memory
 *********************************************************************************************************************/

#define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS1_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS3_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS5_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS6_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS8_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS9_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSIInfo_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCtrlDCDC_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimParkCommand_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU2_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU3_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_552_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_PCANInfo_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint32, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_TU_LostFrameCounter; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1CounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1CounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2CounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2CounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3CounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3CounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1CounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1CounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2CounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2CounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockCounterSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockCounterSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint16, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApCPT_PimCPT_NvMRamMirror; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLVHWFault_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_FaultSatellite_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_InternalCom_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputOvercurrent_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShorcircuitHV_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShortCircuitLV_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OutputShortCircuit_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageHV_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageLimit1_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_OvervoltageLimit2_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLFM_PimDCLV_Overvoltage_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC1PlugOvertemp_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC2PlugOvertemp_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_AC3PlugOvertemp_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ACNPlugOvertemp_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ActiveDischarge_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_BatteryHVUndervoltage_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ControlPilot_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DC1PlugOvertemp_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DC2PlugOvertemp_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVHWFault_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOutputOvercurrent_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOutputShortCircuit_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DCHVOvervoltage_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_DiagnosticNoACInput_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ElockLocked_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_FaultSatellites_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_OBCInternalCom_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_PFCHWFault_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_ProximityLine_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtFaultLevel, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOFM_PimOBC_VCUModeEPSRequest_Error; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPXL_PimPXL_NvMRamMirror; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1PrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC1PrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2PrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC2PrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3PrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempAC3PrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempACNPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1PrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC1PrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2PrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApFCT_PimTempDC2PrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockPrefaultSCG; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLSD_PimElockPrefaultSCP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLVC_PimDCLVShutdownPathFSP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApLVC_PimDCLVShutdownPathGPIO; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOBC_PimOBCShutdownPathFSP; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApOBC_PimOBCShutdownPathGPIO; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimABS_VehSpd_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS1_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS3_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS5_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS6_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS8_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS9_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_MainConnectorState_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_SOC_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBMS_Voltage_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSIInfo_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCPT_TEMPOREL_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimCtrlDCDC_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDDE_GMV_SEEM_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_GMP_HYB_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimKILOMETRAGE_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimMODE_EPS_REQUEST_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimParkCommand_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU2_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU3_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_552_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_AccPedalPosition_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_BSI_Wakeup_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCActivation_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_DCDCVoltageReq_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_EPWT_Status_InvalidValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_PCANInfo_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_PimVCU_TU_LostFramePrefault; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(boolean, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPXL_PimProximityFailure; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtPCOMNvMArray, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpApPCOM_NvPCOMBlockNeed_MirrorBlock; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */
VAR(IdtArrayInitAIMVoltRef, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock; /* PRQA S 3408, 1504, 1514, 1533 */ /* MD_Rte_3408, MD_MSR_Rule8.7, MD_Rte_1514, MD_Rte_1533 */

#define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
/**********************************************************************************************************************
 * Defines for Rte_ComSendSignalProxy
 *********************************************************************************************************************/
#define RTE_COM_SENDSIGNALPROXY_NOCHANGE       (0U)
#define RTE_COM_SENDSIGNALPROXY_SEND           (1U)
#define RTE_COM_SENDSIGNALPROXY_INVALIDATE     (2U)


#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

FUNC(void, RTE_CODE) Rte_MemClr(P2VAR(void, AUTOMATIC, RTE_VAR_NOINIT) ptr, uint32_least num);
FUNC(void, RTE_CODE) Rte_MemCpy(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num); /* PRQA S 1505, 3408 */ /* MD_MSR_Rule8.7, MD_Rte_3408 */
FUNC(void, RTE_CODE) Rte_MemCpy32(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num); /* PRQA S 1505, 3408 */ /* MD_MSR_Rule8.7, MD_Rte_3408 */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * Helper functions for mode management
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_BswM_ESH_Mode(BswM_ESH_Mode mode); /* PRQA S 3408 */ /* MD_Rte_3408 */
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_Dcm_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType mode); /* PRQA S 3408 */ /* MD_Rte_3408 */
FUNC(uint8, RTE_CODE) Rte_GetInternalModeIndex_Dcm_DcmEcuReset(Dcm_EcuResetType mode); /* PRQA S 3408 */ /* MD_Rte_3408 */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * Timer handling
 *********************************************************************************************************************/

#if defined OS_US2TICKS_SystemTimer
# define RTE_USEC_SystemTimer OS_US2TICKS_SystemTimer
#else
# define RTE_USEC_SystemTimer(val) ((TickType)RTE_CONST_USEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_MS2TICKS_SystemTimer
# define RTE_MSEC_SystemTimer OS_MS2TICKS_SystemTimer
#else
# define RTE_MSEC_SystemTimer(val) ((TickType)RTE_CONST_MSEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#if defined OS_SEC2TICKS_SystemTimer
# define RTE_SEC_SystemTimer OS_SEC2TICKS_SystemTimer
#else
# define RTE_SEC_SystemTimer(val)  ((TickType)RTE_CONST_SEC_SystemTimer_##val) /* PRQA S 0342 */ /* MD_MSR_Rule20.10_0342 */
#endif

#define RTE_CONST_MSEC_SystemTimer_0 (0UL)
#define RTE_CONST_MSEC_SystemTimer_1 (100000UL)
#define RTE_CONST_MSEC_SystemTimer_10 (1000000UL)
#define RTE_CONST_MSEC_SystemTimer_100 (10000000UL)
#define RTE_CONST_MSEC_SystemTimer_2 (200000UL)
#define RTE_CONST_MSEC_SystemTimer_20 (2000000UL)
#define RTE_CONST_MSEC_SystemTimer_3 (300000UL)
#define RTE_CONST_MSEC_SystemTimer_4 (400000UL)
#define RTE_CONST_MSEC_SystemTimer_5 (500000UL)
#define RTE_CONST_MSEC_SystemTimer_7 (700000UL)
#define RTE_CONST_MSEC_SystemTimer_8 (800000UL)


/**********************************************************************************************************************
 * Internal definitions
 *********************************************************************************************************************/

#define RTE_TASK_TIMEOUT_EVENT_MASK   ((EventMaskType)0x01)
#define RTE_TASK_WAITPOINT_EVENT_MASK ((EventMaskType)0x02)

/**********************************************************************************************************************
 * RTE life cycle API
 *********************************************************************************************************************/

#define RTE_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


FUNC(void, RTE_CODE) Rte_InitMemory_OsApplication_ASILB(void)
{
  /* set default values for internal data */
  Rte_CpApBAT_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions = FALSE;
  Rte_CpApBAT_PpBatteryVoltageState_DeBatteryVoltageState = 0U;
  Rte_CpApCHG_PpControlPilotFreqError_DeControlPilotFreqError = FALSE;
  Rte_CpApCHG_PpDiagnosticNoACInput_DeDiagnosticNoACInput = FALSE;
  Rte_CpApCHG_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit = 0U;
  Rte_CpApCHG_PpElockFaultDetected_DeElockFaultDetected = FALSE;
  Rte_CpApCHG_PpForceElockCloseMode4_DeForceElockCloseMode4 = FALSE;
  Rte_CpApCHG_PpInputVoltageMode_DeInputVoltageMode = 0U;
  Rte_CpApCHG_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed = 0U;
  Rte_CpApCHG_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq = 2U;
  Rte_CpApCHG_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq = 2U;
  Rte_CpApCHG_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE = FALSE;
  Rte_CpApCHG_PpInt_OBC_ACRange_OBC_ACRange = 0U;
  Rte_CpApCHG_PpInt_OBC_ACRange_Delayed_OBC_ACRange = 0U;
  Rte_CpApCHG_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt = 0U;
  Rte_CpApCHG_PpInt_OBC_OBCStartSt_OBC_OBCStartSt = FALSE;
  Rte_CpApCHG_PpInt_OBC_Status_OBC_Status = 0U;
  Rte_CpApCHG_PpInt_OBC_Status_Delayed_OBC_Status = 0U;
  Rte_CpApCHG_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress = FALSE;
  Rte_CpApCHG_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue = FALSE;
  Rte_CpApCHG_PpRequestHWStopOBC_DeRequestHWStopOBC = FALSE;
  Rte_CpApCHG_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue = TRUE;
  Rte_CpApCHG_PpStopConditions_DeStopConditions = FALSE;
  Rte_CpApCPT_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status = 1U;
  Rte_CpApCPT_PpInt_OBC_ChargingMode_OBC_ChargingMode = 1U;
  Rte_CpApCPT_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection = FALSE;
  Rte_CpApCPT_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE = 0U;
  Rte_CpApDCH_PpActiveDischargeRequest_DeActiveDischargeRequest = FALSE;
  Rte_CpApDCH_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt = 0U;
  Rte_CpApDCH_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue = FALSE;
  Rte_CpApDCH_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue = FALSE;
  Rte_CpApDER_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError = FALSE;
  Rte_CpApDER_PpImplausibilityTempBuck_DeImplausibilityTempBuck = FALSE;
  Rte_CpApDER_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull = FALSE;
  Rte_CpApDER_PpInt_DCDC_Temperature_DCDC_Temperature = 40U;
  Rte_CpApDER_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor = 0x400U;
  Rte_CpApDER_PpInt_OBC_OBCTemp_OBC_OBCTemp = 100U;
  Rte_CpApDER_PpOBCDerating_DeOBCDerating = FALSE;
  Rte_CpApDER_PpPDERATING_OBC_DePDERATING_OBC = 11000U;
  Rte_CpApFCT_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest = 0U;
  Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC1Aftsales = 0U;
  Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC2Aftsales = 0U;
  Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempAC3Aftsales = 0U;
  Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempACNAftsales = 0U;
  Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC1Aftsales = 0U;
  Rte_CpApFCT_PpOutputTempAftsales_DeOutputTempDC2Aftsales = 0U;
  Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC1Meas = 400U;
  Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC2Meas = 400U;
  Rte_CpApFCT_PpOutputTempMeas_DeOutputTempAC3Meas = 400U;
  Rte_CpApFCT_PpOutputTempMeas_DeOutputTempACNMeas = 400U;
  Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC1Meas = 400U;
  Rte_CpApFCT_PpOutputTempMeas_DeOutputTempDC2Meas = 400U;
  Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCG = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempAC1FaultSCP = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCG = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempAC2FaultSCP = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCG = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempAC3FaultSCP = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCG = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempACNFaultSCP = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCG = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempDC1FaultSCP = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCG = FALSE;
  Rte_CpApFCT_PpTempFaults_DeTempDC2FaultSCP = FALSE;
  Rte_CpApFCT_PpTempMonitoringConditions_DeAC1TempMonitoringConditions = FALSE;
  Rte_CpApFCT_PpTempMonitoringConditions_DeAC2TempMonitoringConditions = FALSE;
  Rte_CpApFCT_PpTempMonitoringConditions_DeAC3TempMonitoringConditions = FALSE;
  Rte_CpApFCT_PpTempMonitoringConditions_DeACNTempMonitoringConditions = FALSE;
  Rte_CpApFCT_PpTempMonitoringConditions_DeDC1TempMonitoringConditions = FALSE;
  Rte_CpApFCT_PpTempMonitoringConditions_DeDC2TempMonitoringConditions = FALSE;
  Rte_CpApLFM_PpDCDCFaultsList_DeDCDC_OvertemperatureFault = FALSE;
  Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_HWErrors_Fault = FALSE;
  Rte_CpApLFM_PpDCDCFaultsList_DeDCLV_InternalCom_Fault = FALSE;
  Rte_CpApLFM_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError = FALSE;
  Rte_CpApLFM_PpDCLVError_DeDCLVError = FALSE;
  Rte_CpApLFM_PpInt_DCDC_Fault_DCDC_Fault = 0U;
  Rte_CpApLFM_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage = 0U;
  Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCG = FALSE;
  Rte_CpApLSD_PpELockSensorFaults_DeELockSensorFaultSCP = FALSE;
  Rte_CpApLSD_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions = FALSE;
  Rte_CpApLSD_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState = 0U;
  Rte_CpApLSD_PpOutputELockSensor_DeOutputELockSensor = 2U;
  Rte_CpApLVC_PpDCDC_POST_Result_DeDCDC_POST_Result = 0U;
  Rte_CpApLVC_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault = FALSE;
  Rte_CpApLVC_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV = FALSE;
  Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed = FALSE;
  Rte_CpApLVC_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed = FALSE;
  Rte_CpApLVC_PpInt_DCDC_InputCurrent_DCDC_InputCurrent = 0U;
  Rte_CpApLVC_PpInt_DCDC_Status_DCDC_Status = 0U;
  Rte_CpApLVC_PpInt_DCDC_Status_Delayed_DCDC_Status = 0U;
  Rte_CpApLVC_PpInt_DCLV_VoltageReference_DCLV_VoltageReference = 0U;
  Rte_CpApLVC_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus = 0U;
  Rte_CpApLVC_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue = FALSE;
  Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue = FALSE;
  Rte_CpApLVC_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed = FALSE;
  Rte_CpApOBC_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC = FALSE;
  Rte_CpApOBC_PpInt_DCDC_CurrentReference_DCDC_CurrentReference = 0U;
  Rte_CpApOBC_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference = 0U;
  Rte_CpApOBC_PpInt_DCDC_VoltageReference_DCDC_VoltageReference = 2100U;
  Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed = FALSE;
  Rte_CpApOBC_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed = FALSE;
  Rte_CpApOBC_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V = 0U;
  Rte_CpApOBC_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus = 0U;
  Rte_CpApOBC_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV = 0U;
  Rte_CpApOBC_PpOBC_POST_Result_DeOBC_POST_Result = 0U;
  Rte_CpApOBC_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault = FALSE;
  Rte_CpApOBC_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue = FALSE;
  Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue = TRUE;
  Rte_CpApOBC_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue = FALSE;
  Rte_CpApOFM_PpFaultChargeSoftStop_DeFaultChargeSoftStop = FALSE;
  Rte_CpApOFM_PpInt_OBC_Fault_OBC_Fault = 0U;
  Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempACSensorFault = FALSE;
  Rte_CpApOFM_PpOBCFaultsList_DeInlet_OvertempDCSensorFault = FALSE;
  Rte_CpApOFM_PpOBCFaultsList_DeOBC_HWErrors_Fault = FALSE;
  Rte_CpApOFM_PpOBCFaultsList_DeOBC_InternalComFault = FALSE;
  Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvercurrentOutputFault = FALSE;
  Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvertemperatureFault = FALSE;
  Rte_CpApOFM_PpOBCFaultsList_DeOBC_OvervoltageOutputFault = FALSE;
  Rte_CpApPCOM_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault = FALSE;
  Rte_CpApPCOM_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions = FALSE;
  Rte_CpApPCOM_PpBusOFFEnableConditions_DeBusOFFEnableConditions = FALSE;
  Rte_CpApPCOM_PpBusOffCANFault_DeBusOffCANFault = FALSE;
  Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo = FALSE;
  Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU = FALSE;
  Rte_CpApPCOM_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU = FALSE;
  Rte_CpApPCOM_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error = FALSE;
  Rte_CpApPCOM_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552 = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo = FALSE;
  Rte_CpApPCOM_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU = FALSE;
  Rte_CpApPCOM_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault = FALSE;
  Rte_CpApPCOM_PpInt_ABS_VehSpd_ABS_VehSpd = 0U;
  Rte_CpApPCOM_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag = FALSE;
  Rte_CpApPCOM_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt = 0U;
  Rte_CpApPCOM_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status = 0U;
  Rte_CpApPCOM_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage = 0U;
  Rte_CpApPCOM_PpInt_BMS_FastChargeSt_BMS_FastChargeSt = 0U;
  Rte_CpApPCOM_PpInt_BMS_Fault_BMS_Fault = 0U;
  Rte_CpApPCOM_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow = 0U;
  Rte_CpApPCOM_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow = 0U;
  Rte_CpApPCOM_PpInt_BMS_MainConnectorState_BMS_MainConnectorState = 0U;
  Rte_CpApPCOM_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable = FALSE;
  Rte_CpApPCOM_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState = FALSE;
  Rte_CpApPCOM_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq = FALSE;
  Rte_CpApPCOM_PpInt_BMS_SOC_BMS_SOC = 0U;
  Rte_CpApPCOM_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt = 0U;
  Rte_CpApPCOM_PpInt_BMS_Voltage_BMS_Voltage = 0U;
  Rte_CpApPCOM_PpInt_BSI_ChargeState_BSI_ChargeState = 3U;
  Rte_CpApPCOM_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus = 0U;
  Rte_CpApPCOM_PpInt_BSI_LockedVehicle_BSI_LockedVehicle = 0U;
  Rte_CpApPCOM_PpInt_BSI_MainWakeup_BSI_MainWakeup = 0U;
  Rte_CpApPCOM_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup = FALSE;
  Rte_CpApPCOM_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup = FALSE;
  Rte_CpApPCOM_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode = 0U;
  Rte_CpApPCOM_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC = FALSE;
  Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP = FALSE;
  Rte_CpApPCOM_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2 = FALSE;
  Rte_CpApPCOM_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2 = 0U;
  Rte_CpApPCOM_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT = 0U;
  Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5 = 0U;
  Rte_CpApPCOM_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6 = 65U;
  Rte_CpApPCOM_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT = 0U;
  Rte_CpApPCOM_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference = 0U;
  Rte_CpApPCOM_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus = 2U;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5 = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6 = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT = FALSE;
  Rte_CpApPCOM_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor = 0U;
  Rte_CpApPCOM_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus = 2U;
  Rte_CpApPCOM_PpInt_DCLV_Input_Current_DCLV_Input_Current = 0U;
  Rte_CpApPCOM_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage = 0U;
  Rte_CpApPCOM_PpInt_DCLV_Measured_Current_DCLV_Measured_Current = 0U;
  Rte_CpApPCOM_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage = 0U;
  Rte_CpApPCOM_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP = FALSE;
  Rte_CpApPCOM_PpInt_DCLV_Power_DCLV_Power = 0U;
  Rte_CpApPCOM_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK = 0U;
  Rte_CpApPCOM_PpInt_DCLV_T_PP_A_DCLV_T_PP_A = 65U;
  Rte_CpApPCOM_PpInt_DCLV_T_PP_B_DCLV_T_PP_B = 65U;
  Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A = 65U;
  Rte_CpApPCOM_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B = 65U;
  Rte_CpApPCOM_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP = 0U;
  Rte_CpApPCOM_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC = FALSE;
  Rte_CpApPCOM_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG = FALSE;
  Rte_CpApPCOM_PpInt_MODE_DIAG_MODE_DIAG = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7 = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V = FALSE;
  Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1 = 0U;
  Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1 = 0U;
  Rte_CpApPCOM_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1 = 0U;
  Rte_CpApPCOM_PpInt_PFC_PFCStatus_PFC_PFCStatus = 8U;
  Rte_CpApPCOM_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C = 75U;
  Rte_CpApPCOM_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C = 0U;
  Rte_CpApPCOM_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C = 75U;
  Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V = 0U;
  Rte_CpApPCOM_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V = 0U;
  Rte_CpApPCOM_PpInt_PFC_Vdclink_V_PFC_Vdclink_V = 0U;
  Rte_CpApPCOM_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition = 0U;
  Rte_CpApPCOM_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand = FALSE;
  Rte_CpApPCOM_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD = FALSE;
  Rte_CpApPCOM_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD = 0U;
  Rte_CpApPCOM_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct = 0U;
  Rte_CpApPCOM_PpInt_VCU_CptTemporel_VCU_CptTemporel = 0U;
  Rte_CpApPCOM_PpInt_VCU_DCDCActivation_VCU_DCDCActivation = 1U;
  Rte_CpApPCOM_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq = 0U;
  Rte_CpApPCOM_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM = 0U;
  Rte_CpApPCOM_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller = FALSE;
  Rte_CpApPCOM_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM = 0U;
  Rte_CpApPCOM_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt = FALSE;
  Rte_CpApPCOM_PpInt_VCU_EPWT_Status_VCU_EPWT_Status = 0U;
  Rte_CpApPCOM_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation = FALSE;
  Rte_CpApPCOM_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb = 0U;
  Rte_CpApPCOM_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev = 0U;
  Rte_CpApPCOM_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec = 0U;
  Rte_CpApPCOM_PpInt_VCU_Keyposition_VCU_Keyposition = 0U;
  Rte_CpApPCOM_PpInt_VCU_Kilometrage_VCU_Kilometrage = 0U;
  Rte_CpApPCOM_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest = 0U;
  Rte_CpApPCOM_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD = 3U;
  Rte_CpApPCOM_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup = FALSE;
  Rte_CpApPCOM_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup = FALSE;
  Rte_CpApPCOM_PpOBCFramesReception_Error_DeOBCFramesReception_Error = FALSE;
  Rte_CpApPCOM_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled = FALSE;
  Rte_CpApPLS_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError = FALSE;
  Rte_CpApPLS_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError = FALSE;
  Rte_CpApPLS_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError = FALSE;
  Rte_CpApPLS_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error = 0U;
  Rte_CpApPLS_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error = 0U;
  Rte_CpApPLS_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error = 0U;
  Rte_CpApPLS_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault = FALSE;
  Rte_CpApPLS_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault = FALSE;
  Rte_CpApPLS_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault = FALSE;
  Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault = FALSE;
  Rte_CpApPLS_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault = FALSE;
  Rte_CpApPLS_PpInt_DCDC_InputVoltage_DCDC_InputVoltage = 0U;
  Rte_CpApPLS_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent = 0U;
  Rte_CpApPLS_PpInt_OBC_OutputCurrent_OBC_OutputCurrent = 0U;
  Rte_CpApPLS_PpInt_OBC_OutputVoltage_OBC_OutputVoltage = 0U;
  Rte_CpApPLS_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error = 0U;
  Rte_CpApPLS_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error = 0U;
  Rte_CpApPLS_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error = 0U;
  Rte_CpApPLS_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error = 0U;
  Rte_CpApPLS_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error = 0U;
  Rte_CpApPLS_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error = 0U;
  Rte_CpApPLS_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error = 0U;
  Rte_CpApPLS_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError = FALSE;
  Rte_CpApPLS_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error = 0U;
  Rte_CpApPLS_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError = FALSE;
  Rte_CpApPLS_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError = FALSE;
  Rte_CpApPLS_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError = FALSE;
  Rte_CpApPLS_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset = 0;
  Rte_CpApPXL_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati = 1U;
  Rte_CpHwAbsAIM_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue = 65U;
  Rte_CpHwAbsAIM_PpBatteryVolt_DeBatteryVolt = 255U;
  Rte_CpHwAbsAIM_PpElockFeedbackCurrent_DeElockFeedbackCurrent = 0U;
  Rte_CpHwAbsAIM_PpElockFeedbackLock_DeElockFeedbackLock = 0U;
  Rte_CpHwAbsAIM_PpElockFeedbackUnlock_DeElockFeedbackUnlock = 0U;
  Rte_CpHwAbsAIM_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw = 255U;
  Rte_CpHwAbsAIM_PpHWEditionDetected_DeHWEditionDetected = 0U;
  Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue = 0U;
  Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue = 0U;
  Rte_CpHwAbsAIM_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue = 0U;
  Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC1Raw = 0U;
  Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC2Raw = 0U;
  Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempAC3Raw = 0U;
  Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempACNRaw = 0U;
  Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC1Raw = 0U;
  Rte_CpHwAbsAIM_PpMsrTempRaw_DeMsrTempDC2Raw = 0U;
  Rte_CpHwAbsAIM_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue = 0U;
  Rte_CpHwAbsAIM_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue = 65535U;
  Rte_CpHwAbsAIM_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue = 65U;
  Rte_CpHwAbsIOM_PpExtRCDLineRaw_DeExtRCDLineRaw = FALSE;
  Rte_CpHwAbsIOM_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue = FALSE;
  Rte_CpHwAbsIOM_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue = FALSE;
  Rte_CpHwAbsIOM_PpInputChargePushRaw_DeInputChargePushRaw = FALSE;
  Rte_CpHwAbsIOM_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue = FALSE;
  Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue = FALSE;
  Rte_CpHwAbsIOM_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue = FALSE;
  Rte_CpHwAbsPIM_PpDutyControlPilot_DeDutyControlPilot = 0U;
  Rte_CpHwAbsPIM_PpFreqOutRange_DeFreqOutRange = FALSE;

}


/**********************************************************************************************************************
 * Internal/External Tx connections
 *********************************************************************************************************************/

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_1_Debug1_1(Debug1_1 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_1_oDebug1_oInt_CAN_c4f96638_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_2_Debug1_2(Debug1_2 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_2_oDebug1_oInt_CAN_2e7fbb5a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_3_Debug1_3(Debug1_3 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_3_oDebug1_oInt_CAN_c12d0dbb_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_4_Debug1_4(Debug1_4 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_4_oDebug1_oInt_CAN_200307df_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_5_Debug1_5(Debug1_5 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_5_oDebug1_oInt_CAN_cf51b13e_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_6_Debug1_6(Debug1_6 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_6_oDebug1_oInt_CAN_25d76c5c_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug1_7_Debug1_7(Debug1_7 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug1_7_oDebug1_oInt_CAN_ca85dabd_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_0_Debug2_0(Debug2_0 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_0_oDebug2_oInt_CAN_6f352610_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_1_Debug2_1(Debug2_1 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_1_oDebug2_oInt_CAN_806790f1_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_2_Debug2_2(Debug2_2 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_2_oDebug2_oInt_CAN_6ae14d93_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_3_Debug2_3(Debug2_3 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_3_oDebug2_oInt_CAN_85b3fb72_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_4_Debug2_4(Debug2_4 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_4_oDebug2_oInt_CAN_649df116_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_5_Debug2_5(Debug2_5 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_5_oDebug2_oInt_CAN_8bcf47f7_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_6_Debug2_6(Debug2_6 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_6_oDebug2_oInt_CAN_61499a95_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug2_7_Debug2_7(Debug2_7 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug2_7_oDebug2_oInt_CAN_8e1b2c74_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_0_Debug3_0(Debug3_0 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_0_oDebug3_oInt_CAN_e5907668_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_1_Debug3_1(Debug3_1 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_1_oDebug3_oInt_CAN_0ac2c089_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_2_Debug3_2(Debug3_2 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_2_oDebug3_oInt_CAN_e0441deb_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_3_Debug3_3(Debug3_3 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_3_oDebug3_oInt_CAN_0f16ab0a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_4_Debug3_4(Debug3_4 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_4_oDebug3_oInt_CAN_ee38a16e_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_5_Debug3_5(Debug3_5 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_5_oDebug3_oInt_CAN_016a178f_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_6_Debug3_6(Debug3_6 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_6_oDebug3_oInt_CAN_ebeccaed_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug3_7_Debug3_7(Debug3_7 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug3_7_oDebug3_oInt_CAN_04be7c0c_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_0_Debug4_0(Debug4_0 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_0_oDebug4_oInt_CAN_e608cb82_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_1_Debug4_1(Debug4_1 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_1_oDebug4_oInt_CAN_095a7d63_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_2_Debug4_2(Debug4_2 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_2_oDebug4_oInt_CAN_e3dca001_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_3_Debug4_3(Debug4_3 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_3_oDebug4_oInt_CAN_0c8e16e0_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_4_Debug4_4(Debug4_4 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_4_oDebug4_oInt_CAN_eda01c84_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_5_Debug4_5(Debug4_5 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_5_oDebug4_oInt_CAN_02f2aa65_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_6_Debug4_6(Debug4_6 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_6_oDebug4_oInt_CAN_e8747707_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Debug4_7_Debug4_7(Debug4_7 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Debug4_7_oDebug4_oInt_CAN_0726c1e6_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_Degug1_0_Degug1_0(Degug1_0 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_Degug1_0_oDebug1_oInt_CAN_b0849b9e_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_CurrentReference_oSUP_CommandToDCHV_oInt_CAN_e1358897_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_Fault_DCDC_Fault(DCDC_Fault data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_Fault_oDC1_oE_CAN_80e733c8_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_FaultLampRequest_oDC1_oE_CAN_66ccec8a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_HighVoltConnectionAllowed_oDC1_oE_CAN_7634e78c_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_InputVoltage_oDC1_oE_CAN_dc52df4d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_OVERTEMP_oDC1_oE_CAN_9db3c9a4_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_OutputVoltage_oDC1_oE_CAN_b16c6aa0_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_Status_DCDC_Status(DCDC_Status data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_Status_oDC1_oE_CAN_0062049f_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_Temperature_DCDC_Temperature(DCDC_Temperature data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_Temperature_oDC1_oE_CAN_ebbc428a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_DCDC_VoltageReference_oSUP_CommandToDCHV_oInt_CAN_154ec40d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_DCLV_Temp_Derating_Factor_oSUP_CommandToDCLV_oInt_CAN_18f05833_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpDCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_DCLV_VoltageReference_oSUP_CommandToDCLV_oInt_CAN_927eb38f_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpEDITION_CALIB_EDITION_CALIB(EDITION_CALIB data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_EDITION_CALIB_oVERS_OBC_DCDC_oE_CAN_20fa071e_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpEDITION_SOFT_EDITION_SOFT(EDITION_SOFT data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_EDITION_SOFT_oVERS_OBC_DCDC_oE_CAN_bde5b057_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_EVSE_RTAB_STOP_CHARGE_oOBC1_oE_CAN_01a50620_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_0_oNEW_JDD_OBC_DCDC_oE_CAN_bfd944fb_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_1_oNEW_JDD_OBC_DCDC_oE_CAN_58c4e26c_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_2_oNEW_JDD_OBC_DCDC_oE_CAN_aa930f94_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_3_oNEW_JDD_OBC_DCDC_oE_CAN_4d8ea903_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_4_oNEW_JDD_OBC_DCDC_oE_CAN_954dd225_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_5_oNEW_JDD_OBC_DCDC_oE_CAN_725074b2_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_6_oNEW_JDD_OBC_DCDC_oE_CAN_8007994a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_NEW_JDD_OBC_DCDC_BYTE_7_oNEW_JDD_OBC_DCDC_oE_CAN_671a3fdd_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ACRange_OBC_ACRange(OBC_ACRange data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_ACRange_oOBC2_oE_CAN_1149ad47_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_CP_connection_Status_oOBC1_oE_CAN_bef9ca9d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_ChargingConnectionConfirmati_oOBC2_oE_CAN_e178031d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_ChargingMode_oOBC1_oE_CAN_ce17c9da_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_CommunicationSt_oOBC2_oE_CAN_e0b8e211_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_CoolingWakeup_oOBC3_oE_CAN_91759696_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_DCChargingPlugAConnConf_oOBC2_oE_CAN_ef0cc399_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_ElockState_OBC_ElockState(OBC_ElockState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_ElockState_oOBC1_oE_CAN_d48080ac_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_Fault_OBC_Fault(OBC_Fault data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_Fault_oOBC1_oE_CAN_cba9590d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_HVBattRechargeWakeup_oOBC3_oE_CAN_2290bde4_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_HighVoltConnectionAllowed_oOBC1_oE_CAN_b82322d0_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_HoldDiscontactorWakeup_oOBC3_oE_CAN_0a4fcf18_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_InputVoltageSt_oOBC2_oE_CAN_f750717a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_OBCStartSt_oOBC2_oE_CAN_5b2bb089_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_OBCTemp_oOBC2_oE_CAN_0c3bcf20_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_OutputCurrent_oOBC2_oE_CAN_c3e935dd_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_OutputVoltage_oOBC2_oE_CAN_afb611f2_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_PIStateInfoWakeup_oOBC3_oE_CAN_b3c75a3a_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_PlugVoltDetection_oOBC2_oE_CAN_3eae826f_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PowerMax_OBC_PowerMax(OBC_PowerMax data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_PowerMax_oOBC2_oE_CAN_cb72170d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_PushChargeType_oOBC4_oE_CAN_f9102f70_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_RechargeHMIState_oOBC4_oE_CAN_2e6d800d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_SocketTempL_oOBC1_oE_CAN_72ea0018_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_SocketTempN_oOBC1_oE_CAN_9384c4b5_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpOBC_Status_OBC_Status(OBC_Status data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_OBC_Status_oOBC1_oE_CAN_ef77955c_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSG_DC2_SG_DC2(P2CONST(SG_DC2, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComGroupSignal_DCDC_ActivedischargeSt_oDC2_oE_CAN_5a2849b9_Tx, &(*(data)).DCDC_ActivedischargeSt); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  ret |= Com_SendSignal(ComConf_ComGroupSignal_DCDC_InputCurrent_oDC2_oE_CAN_0b6c1afa_Tx, &(*(data)).DCDC_InputCurrent); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  ret |= Com_SendSignal(ComConf_ComGroupSignal_DCDC_OBCDCDCRTPowerLoad_oDC2_oE_CAN_e771c64f_Tx, &(*(data)).DCDC_OBCDCDCRTPowerLoad); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  ret |= Com_SendSignal(ComConf_ComGroupSignal_DCDC_OBCMainContactorReq_oDC2_oE_CAN_40cd3485_Tx, &(*(data)).DCDC_OBCMainContactorReq); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  ret |= Com_SendSignal(ComConf_ComGroupSignal_DCDC_OBCQuickChargeContactorReq_oDC2_oE_CAN_2093b4ef_Tx, &(*(data)).DCDC_OBCQuickChargeContactorReq); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  ret |= Com_SendSignal(ComConf_ComGroupSignal_DCDC_OutputCurrent_oDC2_oE_CAN_6652af17_Tx, &(*(data)).DCDC_OutputCurrent); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  ret |= Com_SendSignalGroup(ComConf_ComSignalGroup_SG_DC2_oDC2_oE_CAN_40b8dcf0_Tx); /* PRQA S 0315, 2986 */ /* MD_Rte_0315, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_CoolingWupState_oSUPV_V2_OBC_DCDC_oE_CAN_addf5c22_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_DTCRegistred_oSUPV_V2_OBC_DCDC_oE_CAN_04412eaa_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_ECUElecStateRCD_oSUPV_V2_OBC_DCDC_oE_CAN_ff7122fa_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_HVBattChargeWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1071a795_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_HoldDiscontactorWupState_oSUPV_V2_OBC_DCDC_oE_CAN_b0368fb5_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_PIStateInfoWupState_oSUPV_V2_OBC_DCDC_oE_CAN_a649d590_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_PostDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_5f77ad32_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_PreDriveWupState_oSUPV_V2_OBC_DCDC_oE_CAN_bfe17473_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_PrecondElecWupState_oSUPV_V2_OBC_DCDC_oE_CAN_4523c42d_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_RCDLineState_oSUPV_V2_OBC_DCDC_oE_CAN_ab22fee8_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUPV_StopDelayedHMIWupState_oSUPV_V2_OBC_DCDC_oE_CAN_1dcc6f45_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUP_CommandVDCLink_V_oSUP_CommandToPFC_oInt_CAN_b6bd9cf9_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUP_RequestDCLVStatus_oSUP_CommandToDCLV_oInt_CAN_08473e1f_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUP_RequestPFCStatus_oSUP_CommandToPFC_oInt_CAN_98b04349_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  Rte_DisableOSInterrupts(); /* PRQA S 1881, 4558 */ /* MD_Rte_Os, MD_Rte_Os */
  ret |= Com_SendSignal(ComConf_ComSignal_SUP_RequestStatusDCHV_oSUP_CommandToDCHV_oInt_CAN_7bfbedf8_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */
  Rte_EnableOSInterrupts(); /* PRQA S 1881, 4558, 2983 */ /* MD_Rte_Os, MD_Rte_Os, MD_Rte_2983 */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERSION_APPLI_VERSION_APPLI(VERSION_APPLI data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_VERSION_APPLI_oVERS_OBC_DCDC_oE_CAN_1774b9bd_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERSION_SOFT_VERSION_SOFT(VERSION_SOFT data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_VERSION_SOFT_oVERS_OBC_DCDC_oE_CAN_dcc20268_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_VERSION_SYSTEME_oVERS_OBC_DCDC_oE_CAN_7264d771_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_VERS_DATE2_ANNEE_oVERS_OBC_DCDC_oE_CAN_258c1038_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_VERS_DATE2_JOUR_oVERS_OBC_DCDC_oE_CAN_526ac526_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

FUNC(Std_ReturnType, RTE_CODE) Rte_Write_CtApPCOM_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS data) /* PRQA S 1505, 2982 */ /* MD_MSR_Rule8.7, MD_Rte_2982 */
{
  Std_ReturnType ret = RTE_E_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */

  ret |= Com_SendSignal(ComConf_ComSignal_VERS_DATE2_MOIS_oVERS_OBC_DCDC_oE_CAN_d5ba8908_Tx, (&data)); /* PRQA S 0315, 1340, 2986 */ /* MD_Rte_0315, MD_Rte_1340, MD_MSR_RetVal */

  return ret;
} /* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */


/**********************************************************************************************************************
 * Task bodies for RTE controlled tasks
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Task:     ASILB_MAIN_TASK
 * Priority: 500
 * Schedule: FULL
 *********************************************************************************************************************/
TASK(ASILB_MAIN_TASK) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{
  EventMaskType ev;

  for(;;)
  {
    (void)WaitEvent(Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_100ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_10ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_2_10ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_3_10ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_4_10ms | Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msRX | Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msTX); /* PRQA S 3417 */ /* MD_Rte_Os */
    (void)GetEvent(ASILB_MAIN_TASK, &ev); /* PRQA S 3417 */ /* MD_Rte_Os */
    (void)ClearEvent(ev & (Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_100ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_10ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_2_10ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_3_10ms | Rte_Ev_Cyclic_ASILB_MAIN_TASK_4_10ms | Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msRX | Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msTX)); /* PRQA S 3417 */ /* MD_Rte_Os */

    if ((ev & Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msRX) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApPCOM_task5msRX(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_10ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtHwAbsIOM_inputTask10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtHwAbsAIM_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtHwAbsPIM_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApBAT_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApOBC_task10msA(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApCHG_task10msA(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApLVC_task10msA(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApLSD_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApPXL_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApDCH_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApCPT_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_ASILB_MAIN_TASK_1_100ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtHwAbsIOM_inputTask100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtHwAbsAIM_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApFCT_task100ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_ASILB_MAIN_TASK_2_10ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApPLS_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApOFM_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApLFM_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApDER_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApCHG_task10msB(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_ASILB_MAIN_TASK_3_10ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApOBC_task10msB(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtApLVC_task10msB(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Cyclic_ASILB_MAIN_TASK_4_10ms) != (EventMaskType)0)
    {
      /* call runnable */
      RCtHwAbsPOM_task10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */

      /* call runnable */
      RCtHwAbsIOM_outputTask10ms(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }

    if ((ev & Rte_Ev_Run_CpApPCOM_RCtApPCOM_task5msTX) != (EventMaskType)0)
    {
      /* call runnable */
      RCtApPCOM_task5msTX(); /* PRQA S 2987 */ /* MD_Rte_2987 */
    }
  }
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

/**********************************************************************************************************************
 * Task:     ASILB_init_task
 * Priority: 999
 * Schedule: FULL
 *********************************************************************************************************************/
TASK(ASILB_init_task) /* PRQA S 3408, 1503 */ /* MD_Rte_3408, MD_MSR_Unreachable */
{

  /* call runnable */
  RCtHwAbsIOM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtHwAbsPOM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtHwAbsAIM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtHwAbsPIM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApPCOM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApBAT_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApPXL_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApCPT_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApPLS_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApOFM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApLFM_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApCHG_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApFCT_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApOBC_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApLVC_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApLSD_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApDCH_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  /* call runnable */
  RCtApDER_init(); /* PRQA S 2987 */ /* MD_Rte_2987 */

  (void)TerminateTask(); /* PRQA S 3417 */ /* MD_Rte_Os */
} /* PRQA S 6010, 6030, 6050, 6080 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL, MD_MSR_STMIF */

#define RTE_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0315:  MISRA rule: Dir1.1
     Reason:     Pointer cast to void because generic access is necessary.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_1340:  MISRA rule: Rule17.8
     Reason:     Passing elements by pointer is a well known concept.
     Risk:       No functional risk. Data flow is handled with care.
     Prevention: Not required.

   MD_Rte_1514:  MISRA rule: Rule8.9
     Reason:     Because of external definition, misra does not see the call.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_1533:  MISRA rule: Rule8.9
     Reason:     Object is referenced by more than one function in different configurations.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_2982:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_2983:  MISRA rule: Rule2.2
     Reason:     For generated code it is difficult to check the usage of each object.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_2987:  MISRA rule: Rule2.2
     Reason:     Used to simplify code generation.
     Risk:       No functional risk. There is no side effect.
     Prevention: Not required.

   MD_Rte_3408:  MISRA rule: Rule8.4
     Reason:     For the purpose of monitoring during calibration or debugging it is necessary to use non-static declarations.
                 This is covered in the MISRA C compliance section of the Rte specification.
     Risk:       No functional risk.
     Prevention: Not required.

   MD_Rte_Os:
     Reason:     This justification is used as summary justification for all deviations caused by the MICROSAR OS
                 which is for testing of the RTE. Those deviations are no issues in the RTE code.
     Risk:       No functional risk.
     Prevention: Not required.

*/
