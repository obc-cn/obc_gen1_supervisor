/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Dcm
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Dcm_Lcfg.c
 *   Generation Time: 2020-08-19 13:07:47
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/



#define DCM_LCFG_SOURCE
/* ----------------------------------------------
 ~&&&   Includes
---------------------------------------------- */
                                                                                                                                                     /* PRQA S 1533 EOF */ /* MD_Dcm_ObjectOnlyAccessedOnce */
#include "Dcm.h"
#include "Rte_Dcm.h"
#include "Dcm_Int.h"
#include "PduR_Dcm.h"
#include "ComM_Dcm.h"
/* ----------------------------------------------
 ~&&&   Defines
---------------------------------------------- */
#if (DCM_DIDMGR_NVM_READ_ENABLED == STD_ON) || \
    (DCM_DIDMGR_NVM_WRITE_ENABLED == STD_ON)
# if defined(NVM_VENDOR_ID)
#  if (NVM_VENDOR_ID == 30u)
/* Only Vector NvM supports this feature up to now */
#   define Dcm_GetDcmNvMBlockId(blockId)                             (uint16)(NvM_GetDcmBlockId(blockId))                                            /* PRQA S 3453 */ /* QAC 7.0:  A function could probably be used instead of this function-like macro */ /* Macro is more efficient! */
#  endif
# endif

/* Default NvM handle offset */
# if !defined(Dcm_GetDcmNvMBlockId)
#  define Dcm_GetDcmNvMBlockId(blockId)                              (uint16)(blockId)                                                               /* PRQA S 3453 */ /* QAC 7.0:  A function could probably be used instead of this function-like macro */ /* Macro is more efficient! */
# endif
#endif
/* ----------------------------------------------
 ~&&&   Call-back function declarations
---------------------------------------------- */
#define DCM_START_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/***********************************************************************************************************************
 *  Dcm_RidMgr_DD43_Start()
***********************************************************************************************************************/
/*! \brief         Wraps RIDs execution interface.
 *  \details       Converts uint8 arrays to other signal types required by the RID execution interface and vice versa.
 *  \param[in]     OpStatus           The operation status
 *  \param[in,out] pMsgContext        Message-related information for one diagnostic protocol identifier
 *  \param[in,out] DataLength         IN: Concrete length of the dynamic request signal
 *                                    OUT: Concrete length of the dynamic response Signal
 *  \param[out]    ErrorCode          Negative response code
 *  \return        E_OK               The operation is finished
 *  \return        DCM_E_PENDING      The operation is not yet finished
 *  \return        DCM_E_FORCE_RCRRP  Forces a RCR-RP response
 *                                    The call out will called again once the response is sent. The OpStatus parameter
 *                                    will contain the transmission result
 *  \return        E_NOT_OK           The operation has failed. A concrete NRC shall be set, otherwise the DCM sends NRC
 *                                    0x22
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_DD43_Start(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_RidMgr_DD43_RequestResults()
***********************************************************************************************************************/
/*! \brief         Wraps RIDs execution interface.
 *  \details       Converts uint8 arrays to other signal types required by the RID execution interface and vice versa.
 *  \param[in]     OpStatus           The operation status
 *  \param[in,out] pMsgContext        Message-related information for one diagnostic protocol identifier
 *  \param[in,out] DataLength         IN: Concrete length of the dynamic request signal
 *                                    OUT: Concrete length of the dynamic response Signal
 *  \param[out]    ErrorCode          Negative response code
 *  \return        E_OK               The operation is finished
 *  \return        DCM_E_PENDING      The operation is not yet finished
 *  \return        DCM_E_FORCE_RCRRP  Forces a RCR-RP response
 *                                    The call out will called again once the response is sent. The OpStatus parameter
 *                                    will contain the transmission result
 *  \return        E_NOT_OK           The operation has failed. A concrete NRC shall be set, otherwise the DCM sends NRC
 *                                    0x22
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_DD43_RequestResults(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_RidMgr_FF00_Start()
***********************************************************************************************************************/
/*! \brief         Wraps RIDs execution interface.
 *  \details       Converts uint8 arrays to other signal types required by the RID execution interface and vice versa.
 *  \param[in]     OpStatus           The operation status
 *  \param[in,out] pMsgContext        Message-related information for one diagnostic protocol identifier
 *  \param[in,out] DataLength         IN: Concrete length of the dynamic request signal
 *                                    OUT: Concrete length of the dynamic response Signal
 *  \param[out]    ErrorCode          Negative response code
 *  \return        E_OK               The operation is finished
 *  \return        DCM_E_PENDING      The operation is not yet finished
 *  \return        DCM_E_FORCE_RCRRP  Forces a RCR-RP response
 *                                    The call out will called again once the response is sent. The OpStatus parameter
 *                                    will contain the transmission result
 *  \return        E_NOT_OK           The operation has failed. A concrete NRC shall be set, otherwise the DCM sends NRC
 *                                    0x22
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_FF00_Start(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DCMCallback_RequestDownload()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented service-processor.
 *  \details       Encapsulates the internally used pContext parameter.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the SID.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DCMCallback_RequestDownload(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DCMCallback_TransferData()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented service-processor.
 *  \details       Encapsulates the internally used pContext parameter.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the SID.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DCMCallback_TransferData(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DCMCallback_RequestTransferExit()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented service-processor.
 *  \details       Encapsulates the internally used pContext parameter.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the SID.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DCMCallback_RequestTransferExit(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_ServiceNoPostProcessor()
***********************************************************************************************************************/
/*! \brief         Dummy post-processor
 *  \details       This post-processor is called for diagnostic services which do not require any post processing.
 *  \param[in]     pContext  Pointer to the context
 *  \param[in]     status    The post-processing status
 *  \context       TASK
 *  \reentrant     FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoPostProcessor(Dcm_ContextPtrType pContext, Dcm_ConfirmationStatusType status);
/***********************************************************************************************************************
 *  Dcm_ServiceNoUpdater()
***********************************************************************************************************************/
/*! \brief         Realizes a dummy paged buffer updater.
 *  \details       This function is never called.
 *  \param[in]     pContext      Pointer to the context
 *  \param[in]     opStatus      The operation status
 *  \param[in,out] pDataContext  Pointer to the data context
 *  \param[out]    ErrorCode     Negative response code
 *  \return        DCM_E_NOT_OK  Operation failed. Take the NRC from ErrorCode. Do not call again
 *  \context       TASK
 *  \reentrant     FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_ServiceNoUpdater(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_DiagDataContextPtrType pDataContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_ServiceNoCancel()
***********************************************************************************************************************/
/*! \brief         Dummy service cancellation.
 *  \details       -
 *  \param[in,out] pDataContext  Pointer to the data context
 *  \context       TASK
 *  \reentrant     FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoCancel(Dcm_ContextPtrType pContext, Dcm_DiagDataContextPtrType pDataContext);
/***********************************************************************************************************************
 *  Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Hard()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented sub-service-processor.
 *  \details       Encapsulates the internally used pContext parameter.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the sub-function.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Hard(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_SubSvcWrapper_DCMCallBack_ECUReset_KeyOffOn()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented sub-service-processor.
 *  \details       Encapsulates the internally used pContext parameter.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the sub-function.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SubSvcWrapper_DCMCallBack_ECUReset_KeyOffOn(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Soft()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented sub-service-processor.
 *  \details       Encapsulates the internally used pContext parameter.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the sub-function.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Soft(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
#define DCM_STOP_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   Uninitialized RAM 8-Bit
---------------------------------------------- */
#define DCM_START_SEC_VAR_NO_INIT_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! DCM protocol descriptor */
DCM_LOCAL VAR(Dcm_MsgItemType, DCM_VAR_NOINIT) Dcm_CfgNetBuffer_000[4095];
#define DCM_STOP_SEC_VAR_NO_INIT_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   ROM 8-Bit
---------------------------------------------- */
#define DCM_START_SEC_CONST_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! TxPduId to DCM connection map */
CONST(Dcm_NetConnRefMemType, DCM_CONST) Dcm_CfgNetTxPduInfo[1]=
{
    0u
};
/*! Map of DCM relevant network handles */
CONST(Dcm_CfgNetNetIdRefMemType, DCM_CONST) Dcm_CfgNetConnComMChannelMap[1]=
{
    0u
};
/*! Look up table of DCM service identifiers */
CONST(uint8, DCM_CONST) Dcm_CfgDiagSvcIdLookUpTable[13]=
{
   12u
  ,0x10u
  ,0x11u
  ,0x14u
  ,0x19u
  ,0x22u
  ,0x27u
  ,0x2Eu
  ,0x31u
  ,0x34u
  ,0x36u
  ,0x37u
  ,0x3Eu
};
/*! Service 0x10 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc10SubFuncLookUpTable[5]=
{
   4u
  ,0x01u
  ,0x02u
  ,0x03u
  ,0x50u
};
/*! Service 0x11 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc11SubFuncLookUpTable[4]=
{
   3u
  ,0x01u
  ,0x02u
  ,0x03u
};
/*! Service 0x19 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc19SubFuncLookUpTable[4]=
{
   3u
  ,0x01u
  ,0x02u
  ,0x04u
};
/*! Service 0x27 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc27SubFuncLookUpTable[3]=
{
   2u
  ,0x01u
  ,0x02u
};
/*! Service 0x3E look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc3ESubFuncLookUpTable[2]=
{
   1u
  ,0x00u
};
#define DCM_STOP_SEC_CONST_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   ROM 16-Bit
---------------------------------------------- */
#define DCM_START_SEC_CONST_16
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! DID look up table  */
CONST(uint16, DCM_CONST) Dcm_CfgDidMgrDidLookUpTable[93]=
{
   92u
  ,0x0100u
  ,0x0101u
  ,0x0102u
  ,0x0103u
  ,0x0104u
  ,0x0105u
  ,0x0D01u
  ,0xC002u
  ,0xD407u
  ,0xD40Cu
  ,0xD49Cu
  ,0xD4B9u
  ,0xD4CAu
  ,0xD4F7u
  ,0xD5BDu
  ,0xD5CFu
  ,0xD5D0u
  ,0xD5D1u
  ,0xD5D2u
  ,0xD805u
  ,0xD806u
  ,0xD807u
  ,0xD808u
  ,0xD809u
  ,0xD80Cu
  ,0xD813u
  ,0xD822u
  ,0xD824u
  ,0xD825u
  ,0xD826u
  ,0xD827u
  ,0xD828u
  ,0xD829u
  ,0xD82Bu
  ,0xD82Cu
  ,0xD82Du
  ,0xD82Eu
  ,0xD82Fu
  ,0xD831u
  ,0xD83Bu
  ,0xD83Cu
  ,0xD83Du
  ,0xD83Eu
  ,0xD83Fu
  ,0xD840u
  ,0xD843u
  ,0xD844u
  ,0xD845u
  ,0xD846u
  ,0xD84Au
  ,0xD84Bu
  ,0xD84Cu
  ,0xD84Du
  ,0xD84Eu
  ,0xD84Fu
  ,0xD850u
  ,0xD851u
  ,0xD852u
  ,0xD853u
  ,0xD854u
  ,0xD855u
  ,0xD866u
  ,0xD8AAu
  ,0xD8E9u
  ,0xD8EBu
  ,0xF080u
  ,0xF08Fu
  ,0xF0FEu
  ,0xF180u
  ,0xF186u
  ,0xF18Bu
  ,0xF18Cu
  ,0xFD00u
  ,0xFD01u
  ,0xFD02u
  ,0xFD03u
  ,0xFD04u
  ,0xFD05u
  ,0xFD06u
  ,0xFD07u
  ,0xFD08u
  ,0xFD09u
  ,0xFD0Au
  ,0xFD0Bu
  ,0xFD0Cu
  ,0xFD0Du
  ,0xFD0Eu
  ,0xFD0Fu
  ,0xFD10u
  ,0xFD11u
  ,0xFD12u
  ,0xFD13u
};
/*! RID look up table  */
CONST(uint16, DCM_CONST) Dcm_CfgRidMgrRidLookUpTable[10]=
{
   9u
  ,0x0400u
  ,0xDD43u
  ,0xDF66u
  ,0xDF68u
  ,0xDF6Cu
  ,0xDF6Du
  ,0xDF6Eu
  ,0xFF00u
  ,0xFF04u
};
#define DCM_STOP_SEC_CONST_16
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   ROM of unspecified size
---------------------------------------------- */
#define DCM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! DCM buffer descriptor */
CONST(Dcm_CfgNetBufferInfoType, DCM_CONST) Dcm_CfgNetBufferInfo[1]=
{
   { Dcm_CfgNetBuffer_000,4095u}
};
/*! RxPduId map */
CONST(Dcm_CfgNetRxPduInfoType, DCM_CONST) Dcm_CfgNetRxPduInfo[2]=
{
   { FALSE, 0u}
  ,{ TRUE, 0u}
};
/*! DCM connection descriptor */
CONST(Dcm_CfgNetConnectionInfoType, DCM_CONST) Dcm_CfgNetConnectionInfo[1]=
{
   { 0x0EF9u,PduRConf_PduRSrcPdu_PduRSrcPdu_f0e620d6, 0u,0u,0x00u, 0u}
};
/*! DCM protocol descriptor */
CONST(Dcm_CfgNetProtocolInfoType, DCM_CONST) Dcm_CfgNetProtocolInfo[1]=
{
   { {        0u,       0u},4095u,248u,TRUE,0u,DemConf_DemClient_DemClient_DCM, 0u}
};
/*! Map of all relevant for DCM network handles */
CONST(NetworkHandleType, DCM_CONST) Dcm_CfgNetAllComMChannelMap[1]=
{
   ComMConf_ComMChannel_CN_E_CAN_7f812c72
};
/*! Look up table of DCM relevant network handles */
CONST(NetworkHandleType, DCM_CONST) Dcm_CfgNetNetworkHandleLookUpTable[2]=
{
   1u
  ,ComMConf_ComMChannel_CN_E_CAN_7f812c72
};
/*! Diagnostic service execution conditions */
CONST(Dcm_CfgStatePreconditionInfoType, DCM_CONST) Dcm_CfgStatePreconditions[8]=
{
   { { 0x0Fu,0x03u}}
  ,{ { 0x07u,0x03u}}
  ,{ { 0x0Du,0x03u}}
  ,{ { 0x04u,0x03u}}
  ,{ { 0x05u,0x03u}}
  ,{ { 0x02u,0x03u}}
  ,{ { 0x06u,0x03u}}
  ,{ { 0x0Cu,0x03u}}
};
/*! Session state properties */
CONST(Dcm_CfgStateSessionInfoType, DCM_CONST) Dcm_CfgStateSessionInfo[4]=
{
   { {       20u,      20u},RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION,0x01u}
  ,{ {       20u,      20u},RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION,0x02u}
  ,{ {       20u,      20u},RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION,0x03u}
  ,{ {       20u,      20u},RTE_MODE_DcmDiagnosticSessionControl_distantVehicleAccessSession_DVAS,0x50u}
};
/*! Security Access state properties */
CONST(Dcm_CfgStateSecurityInfoType, DCM_CONST) Dcm_CfgStateSecurityInfo[1]=
{
   { 0x01u} /* SecLvl: UnlockedL1 */
};
/*! DID properties */
CONST(Dcm_CfgDidMgrDidInfoType, DCM_CONST) Dcm_CfgDidMgrDidInfo[92]=
{
   {    1u,   1u,   1u,   0u,0x01u} /* DID: 0x0100 */
  ,{    1u,   1u,   1u,   1u,0x01u} /* DID: 0x0101 */
  ,{    1u,   1u,   1u,   2u,0x01u} /* DID: 0x0102 */
  ,{    1u,   1u,   1u,   3u,0x01u} /* DID: 0x0103 */
  ,{    1u,   1u,   1u,   4u,0x01u} /* DID: 0x0104 */
  ,{    1u,   1u,   1u,   5u,0x01u} /* DID: 0x0105 */
  ,{    1u,   1u,   1u,   6u,0x01u} /* DID: 0x0D01 */
  ,{    2u,   2u,   2u,   7u,0x01u} /* DID: 0xC002 */
  ,{    1u,   1u,   1u,   8u,0x01u} /* DID: 0xD407 */
  ,{    1u,   1u,   1u,   9u,0x01u} /* DID: 0xD40C */
  ,{    4u,   4u,   4u,  10u,0x01u} /* DID: 0xD49C */
  ,{    1u,   1u,   1u,  11u,0x01u} /* DID: 0xD4B9 */
  ,{    4u,   4u,   4u,  12u,0x01u} /* DID: 0xD4CA */
  ,{    1u,   1u,   1u,  13u,0x01u} /* DID: 0xD4F7 */
  ,{    2u,   2u,   2u,  14u,0x01u} /* DID: 0xD5BD */
  ,{    2u,   2u,   2u,  15u,0x01u} /* DID: 0xD5CF */
  ,{    1u,   1u,   1u,  16u,0x01u} /* DID: 0xD5D0 */
  ,{    2u,   2u,   2u,  17u,0x01u} /* DID: 0xD5D1 */
  ,{    1u,   1u,   1u,  18u,0x01u} /* DID: 0xD5D2 */
  ,{    2u,   2u,   2u,  19u,0x01u} /* DID: 0xD805 */
  ,{    1u,   1u,   1u,  20u,0x01u} /* DID: 0xD806 */
  ,{    2u,   2u,   2u,  21u,0x01u} /* DID: 0xD807 */
  ,{    1u,   1u,   1u,  22u,0x01u} /* DID: 0xD808 */
  ,{    2u,   2u,   2u,  23u,0x01u} /* DID: 0xD809 */
  ,{    2u,   2u,   2u,  24u,0x01u} /* DID: 0xD80C */
  ,{    1u,   1u,   1u,  25u,0x01u} /* DID: 0xD813 */
  ,{    2u,   2u,   2u,  26u,0x01u} /* DID: 0xD822 */
  ,{    2u,   2u,   2u,  27u,0x01u} /* DID: 0xD824 */
  ,{    1u,   1u,   1u,  28u,0x01u} /* DID: 0xD825 */
  ,{    1u,   1u,   1u,  29u,0x01u} /* DID: 0xD826 */
  ,{    1u,   1u,   1u,  30u,0x01u} /* DID: 0xD827 */
  ,{    1u,   1u,   1u,  31u,0x01u} /* DID: 0xD828 */
  ,{    1u,   1u,   1u,  32u,0x01u} /* DID: 0xD829 */
  ,{    2u,   2u,   2u,  33u,0x01u} /* DID: 0xD82B */
  ,{    1u,   1u,   1u,  34u,0x01u} /* DID: 0xD82C */
  ,{    1u,   1u,   1u,  35u,0x01u} /* DID: 0xD82D */
  ,{    1u,   1u,   1u,  36u,0x01u} /* DID: 0xD82E */
  ,{    1u,   1u,   1u,  37u,0x01u} /* DID: 0xD82F */
  ,{    1u,   1u,   1u,  38u,0x01u} /* DID: 0xD831 */
  ,{    1u,   1u,   1u,  39u,0x01u} /* DID: 0xD83B */
  ,{    1u,   1u,   1u,  40u,0x01u} /* DID: 0xD83C */
  ,{    1u,   1u,   1u,  41u,0x01u} /* DID: 0xD83D */
  ,{    1u,   1u,   1u,  42u,0x01u} /* DID: 0xD83E */
  ,{    1u,   1u,   1u,  43u,0x01u} /* DID: 0xD83F */
  ,{    1u,   1u,   1u,  44u,0x01u} /* DID: 0xD840 */
  ,{    1u,   1u,   1u,  45u,0x01u} /* DID: 0xD843 */
  ,{    1u,   1u,   1u,  46u,0x01u} /* DID: 0xD844 */
  ,{    2u,   2u,   2u,  47u,0x01u} /* DID: 0xD845 */
  ,{    1u,   1u,   1u,  48u,0x01u} /* DID: 0xD846 */
  ,{    1u,   1u,   1u,  49u,0x01u} /* DID: 0xD84A */
  ,{    1u,   1u,   1u,  50u,0x01u} /* DID: 0xD84B */
  ,{    1u,   1u,   1u,  51u,0x01u} /* DID: 0xD84C */
  ,{    1u,   1u,   1u,  52u,0x01u} /* DID: 0xD84D */
  ,{    1u,   1u,   1u,  53u,0x01u} /* DID: 0xD84E */
  ,{    1u,   1u,   1u,  54u,0x01u} /* DID: 0xD84F */
  ,{    1u,   1u,   1u,  55u,0x01u} /* DID: 0xD850 */
  ,{    1u,   1u,   1u,  56u,0x01u} /* DID: 0xD851 */
  ,{    1u,   1u,   1u,  57u,0x01u} /* DID: 0xD852 */
  ,{    1u,   1u,   1u,  58u,0x01u} /* DID: 0xD853 */
  ,{    1u,   1u,   1u,  59u,0x01u} /* DID: 0xD854 */
  ,{    1u,   1u,   1u,  60u,0x01u} /* DID: 0xD855 */
  ,{    1u,   1u,   1u,  61u,0x01u} /* DID: 0xD866 */
  ,{    1u,   1u,   1u,  62u,0x01u} /* DID: 0xD8AA */
  ,{    1u,   1u,   1u,  63u,0x01u} /* DID: 0xD8E9 */
  ,{    2u,   2u,   2u,  64u,0x01u} /* DID: 0xD8EB */
  ,{   22u,  22u,  22u,  65u,0x01u} /* DID: 0xF080 */
  ,{   23u,  23u,  23u,  66u,0x01u} /* DID: 0xF08F */
  ,{   24u,  24u,  24u,  67u,0x01u} /* DID: 0xF0FE */
  ,{   13u,  13u,  13u,  68u,0x01u} /* DID: 0xF180 */
  ,{    1u,   1u,   1u,  69u,0x01u} /* DID: 0xF186 */
  ,{    3u,   3u,   3u,  70u,0x01u} /* DID: 0xF18B */
  ,{    0u,  20u,  20u,  71u,0x01u} /* DID: 0xF18C */
  ,{    2u,   2u,   2u,  72u,0x01u} /* DID: 0xFD00 */
  ,{    4u,   4u,   4u,  73u,0x01u} /* DID: 0xFD01 */
  ,{    4u,   4u,   4u,  74u,0x01u} /* DID: 0xFD02 */
  ,{    4u,   4u,   4u,  75u,0x01u} /* DID: 0xFD03 */
  ,{    4u,   4u,   4u,  76u,0x01u} /* DID: 0xFD04 */
  ,{    4u,   4u,   4u,  77u,0x01u} /* DID: 0xFD05 */
  ,{    4u,   4u,   4u,  78u,0x01u} /* DID: 0xFD06 */
  ,{    4u,   4u,   4u,  79u,0x01u} /* DID: 0xFD07 */
  ,{   20u,  20u,  20u,  80u,0x01u} /* DID: 0xFD08 */
  ,{    5u,   5u,   5u,  81u,0x01u} /* DID: 0xFD09 */
  ,{   20u,  20u,  20u,  82u,0x01u} /* DID: 0xFD0A */
  ,{   20u,  20u,  20u,  83u,0x01u} /* DID: 0xFD0B */
  ,{   20u,  20u,  20u,  84u,0x01u} /* DID: 0xFD0C */
  ,{   20u,  20u,  20u,  85u,0x01u} /* DID: 0xFD0D */
  ,{   20u,  20u,  20u,  86u,0x01u} /* DID: 0xFD0E */
  ,{   20u,  20u,  20u,  87u,0x01u} /* DID: 0xFD0F */
  ,{   20u,  20u,  20u,  88u,0x01u} /* DID: 0xFD10 */
  ,{    1u,   1u,   1u,  89u,0x03u} /* DID: 0xFD11 */
  ,{    6u,   6u,   6u,  91u,0x03u} /* DID: 0xFD12 */
  ,{   76u,  76u,  76u,  93u,0x03u} /* DID: 0xFD13 */
};
/*! DID operation properties */
CONST(Dcm_CfgDidMgrDidOpInfoType, DCM_CONST) Dcm_CfgDidMgrDidOpInfo[95]=
{
   {    0u,   0u,0x05u} /* DID: 0x0100 */
  ,{    0u,   2u,0x05u} /* DID: 0x0101 */
  ,{    0u,   4u,0x05u} /* DID: 0x0102 */
  ,{    0u,   6u,0x05u} /* DID: 0x0103 */
  ,{    0u,   8u,0x05u} /* DID: 0x0104 */
  ,{    0u,  10u,0x05u} /* DID: 0x0105 */
  ,{    0u,  12u,0x05u} /* DID: 0x0D01 */
  ,{    0u,  14u,0x05u} /* DID: 0xC002 */
  ,{    2u,  16u,0x05u} /* DID: 0xD407 */
  ,{    2u,  18u,0x05u} /* DID: 0xD40C */
  ,{    2u,  20u,0x05u} /* DID: 0xD49C */
  ,{    2u,  22u,0x05u} /* DID: 0xD4B9 */
  ,{    2u,  24u,0x05u} /* DID: 0xD4CA */
  ,{    2u,  26u,0x05u} /* DID: 0xD4F7 */
  ,{    2u,  28u,0x05u} /* DID: 0xD5BD */
  ,{    2u,  30u,0x05u} /* DID: 0xD5CF */
  ,{    2u,  32u,0x05u} /* DID: 0xD5D0 */
  ,{    2u,  34u,0x05u} /* DID: 0xD5D1 */
  ,{    2u,  36u,0x05u} /* DID: 0xD5D2 */
  ,{    2u,  38u,0x05u} /* DID: 0xD805 */
  ,{    2u,  40u,0x05u} /* DID: 0xD806 */
  ,{    2u,  42u,0x05u} /* DID: 0xD807 */
  ,{    2u,  44u,0x05u} /* DID: 0xD808 */
  ,{    2u,  46u,0x05u} /* DID: 0xD809 */
  ,{    2u,  48u,0x05u} /* DID: 0xD80C */
  ,{    2u,  50u,0x05u} /* DID: 0xD813 */
  ,{    2u,  52u,0x05u} /* DID: 0xD822 */
  ,{    2u,  54u,0x05u} /* DID: 0xD824 */
  ,{    2u,  56u,0x05u} /* DID: 0xD825 */
  ,{    2u,  58u,0x05u} /* DID: 0xD826 */
  ,{    2u,  60u,0x05u} /* DID: 0xD827 */
  ,{    2u,  62u,0x05u} /* DID: 0xD828 */
  ,{    2u,  64u,0x05u} /* DID: 0xD829 */
  ,{    2u,  66u,0x05u} /* DID: 0xD82B */
  ,{    2u,  68u,0x05u} /* DID: 0xD82C */
  ,{    2u,  70u,0x05u} /* DID: 0xD82D */
  ,{    2u,  72u,0x05u} /* DID: 0xD82E */
  ,{    2u,  74u,0x05u} /* DID: 0xD82F */
  ,{    2u,  76u,0x05u} /* DID: 0xD831 */
  ,{    2u,  78u,0x05u} /* DID: 0xD83B */
  ,{    2u,  80u,0x05u} /* DID: 0xD83C */
  ,{    2u,  82u,0x05u} /* DID: 0xD83D */
  ,{    2u,  84u,0x05u} /* DID: 0xD83E */
  ,{    2u,  86u,0x05u} /* DID: 0xD83F */
  ,{    2u,  88u,0x05u} /* DID: 0xD840 */
  ,{    2u,  90u,0x05u} /* DID: 0xD843 */
  ,{    2u,  92u,0x05u} /* DID: 0xD844 */
  ,{    2u,  94u,0x05u} /* DID: 0xD845 */
  ,{    2u,  96u,0x05u} /* DID: 0xD846 */
  ,{    2u,  98u,0x05u} /* DID: 0xD84A */
  ,{    2u, 100u,0x05u} /* DID: 0xD84B */
  ,{    2u, 102u,0x05u} /* DID: 0xD84C */
  ,{    2u, 104u,0x05u} /* DID: 0xD84D */
  ,{    2u, 106u,0x05u} /* DID: 0xD84E */
  ,{    2u, 108u,0x05u} /* DID: 0xD84F */
  ,{    2u, 110u,0x05u} /* DID: 0xD850 */
  ,{    2u, 112u,0x05u} /* DID: 0xD851 */
  ,{    2u, 114u,0x05u} /* DID: 0xD852 */
  ,{    2u, 116u,0x05u} /* DID: 0xD853 */
  ,{    2u, 118u,0x05u} /* DID: 0xD854 */
  ,{    2u, 120u,0x05u} /* DID: 0xD855 */
  ,{    2u, 122u,0x05u} /* DID: 0xD866 */
  ,{    2u, 124u,0x05u} /* DID: 0xD8AA */
  ,{    2u, 126u,0x05u} /* DID: 0xD8E9 */
  ,{    2u, 128u,0x05u} /* DID: 0xD8EB */
  ,{    0u, 130u,0x05u} /* DID: 0xF080 */
  ,{    0u, 132u,0x05u} /* DID: 0xF08F */
  ,{    0u, 134u,0x05u} /* DID: 0xF0FE */
  ,{    0u, 136u,0x05u} /* DID: 0xF180 */
  ,{    0u, 138u,0x05u} /* DID: 0xF186 */
  ,{    0u, 140u,0x05u} /* DID: 0xF18B */
  ,{    0u, 142u,0x07u} /* DID: 0xF18C */
  ,{    2u, 145u,0x05u} /* DID: 0xFD00 */
  ,{    2u, 147u,0x05u} /* DID: 0xFD01 */
  ,{    2u, 149u,0x05u} /* DID: 0xFD02 */
  ,{    2u, 151u,0x05u} /* DID: 0xFD03 */
  ,{    0u, 153u,0x05u} /* DID: 0xFD04 */
  ,{    0u, 155u,0x05u} /* DID: 0xFD05 */
  ,{    0u, 157u,0x05u} /* DID: 0xFD06 */
  ,{    0u, 159u,0x05u} /* DID: 0xFD07 */
  ,{    0u, 161u,0x05u} /* DID: 0xFD08 */
  ,{    0u, 163u,0x05u} /* DID: 0xFD09 */
  ,{    0u, 165u,0x05u} /* DID: 0xFD0A */
  ,{    0u, 167u,0x05u} /* DID: 0xFD0B */
  ,{    0u, 169u,0x05u} /* DID: 0xFD0C */
  ,{    0u, 171u,0x05u} /* DID: 0xFD0D */
  ,{    0u, 173u,0x05u} /* DID: 0xFD0E */
  ,{    0u, 175u,0x05u} /* DID: 0xFD0F */
  ,{    0u, 177u,0x05u} /* DID: 0xFD10 */
  ,{    0u, 179u,0x05u} /* DID: 0xFD11 */
  ,{    3u, 181u,0x01u} /* DID: 0xFD11 */
  ,{    0u, 182u,0x05u} /* DID: 0xFD12 */
  ,{    3u, 184u,0x01u} /* DID: 0xFD12 */
  ,{    0u, 185u,0x05u} /* DID: 0xFD13 */
  ,{    3u, 187u,0x01u} /* DID: 0xFD13 */
};
/*! DID operation classes */
CONST(Dcm_CfgDidMgrDidOpClassInfoType, DCM_CONST) Dcm_CfgDidMgrDidOpClassInfo[189]=
{
   {  0u}
  ,{  1u}
  ,{  2u}
  ,{  3u}
  ,{  4u}
  ,{  5u}
  ,{  6u}
  ,{  7u}
  ,{  8u}
  ,{  9u}
  ,{ 10u}
  ,{ 11u}
  ,{ 12u}
  ,{ 13u}
  ,{ 14u}
  ,{ 15u}
  ,{ 16u}
  ,{ 17u}
  ,{ 18u}
  ,{ 19u}
  ,{ 20u}
  ,{ 21u}
  ,{ 22u}
  ,{ 23u}
  ,{ 24u}
  ,{ 25u}
  ,{ 26u}
  ,{ 27u}
  ,{ 28u}
  ,{ 30u}
  ,{ 32u}
  ,{ 33u}
  ,{ 34u}
  ,{ 35u}
  ,{ 36u}
  ,{ 37u}
  ,{ 38u}
  ,{ 39u}
  ,{ 40u}
  ,{ 41u}
  ,{ 42u}
  ,{ 43u}
  ,{ 44u}
  ,{ 45u}
  ,{ 46u}
  ,{ 47u}
  ,{ 48u}
  ,{ 49u}
  ,{ 50u}
  ,{ 51u}
  ,{ 52u}
  ,{ 53u}
  ,{ 54u}
  ,{ 55u}
  ,{ 56u}
  ,{ 57u}
  ,{ 58u}
  ,{ 59u}
  ,{ 60u}
  ,{ 61u}
  ,{ 62u}
  ,{ 63u}
  ,{ 64u}
  ,{ 65u}
  ,{ 66u}
  ,{ 67u}
  ,{ 68u}
  ,{ 69u}
  ,{ 70u}
  ,{ 71u}
  ,{ 72u}
  ,{ 73u}
  ,{ 74u}
  ,{ 75u}
  ,{ 76u}
  ,{ 77u}
  ,{ 78u}
  ,{ 79u}
  ,{ 80u}
  ,{ 81u}
  ,{ 82u}
  ,{ 83u}
  ,{ 84u}
  ,{ 85u}
  ,{ 86u}
  ,{ 87u}
  ,{ 88u}
  ,{ 89u}
  ,{ 90u}
  ,{ 91u}
  ,{ 92u}
  ,{ 93u}
  ,{ 94u}
  ,{ 95u}
  ,{ 96u}
  ,{ 97u}
  ,{ 98u}
  ,{ 99u}
  ,{ 100u}
  ,{ 101u}
  ,{ 102u}
  ,{ 103u}
  ,{ 104u}
  ,{ 105u}
  ,{ 106u}
  ,{ 107u}
  ,{ 108u}
  ,{ 109u}
  ,{ 110u}
  ,{ 111u}
  ,{ 112u}
  ,{ 113u}
  ,{ 114u}
  ,{ 115u}
  ,{ 116u}
  ,{ 117u}
  ,{ 118u}
  ,{ 119u}
  ,{ 120u}
  ,{ 121u}
  ,{ 122u}
  ,{ 123u}
  ,{ 124u}
  ,{ 125u}
  ,{ 126u}
  ,{ 127u}
  ,{ 128u}
  ,{ 129u}
  ,{ 130u}
  ,{ 131u}
  ,{ 132u}
  ,{ 142u}
  ,{ 152u}
  ,{ 156u}
  ,{ 160u}
  ,{ 177u}
  ,{ 194u}
  ,{ 198u}
  ,{ 202u}
  ,{ 203u}
  ,{ 204u}
  ,{ 207u}
  ,{ 210u}
  ,{ 211u}
  ,{ 212u}
  ,{ 213u}
  ,{ 215u}
  ,{ 217u}
  ,{ 220u}
  ,{ 223u}
  ,{ 224u}
  ,{ 225u}
  ,{ 226u}
  ,{ 227u}
  ,{ 228u}
  ,{ 229u}
  ,{ 230u}
  ,{ 231u}
  ,{ 232u}
  ,{ 233u}
  ,{ 236u}
  ,{ 239u}
  ,{ 244u}
  ,{ 249u}
  ,{ 254u}
  ,{ 259u}
  ,{ 264u}
  ,{ 269u}
  ,{ 274u}
  ,{ 279u}
  ,{ 284u}
  ,{ 289u}
  ,{ 294u}
  ,{ 299u}
  ,{ 304u}
  ,{ 309u}
  ,{ 314u}
  ,{ 319u}
  ,{ 324u}
  ,{ 329u}
  ,{ 330u}
  ,{ 331u}
  ,{ 332u}
  ,{ 333u}
  ,{ 334u}
  ,{ 335u}
  ,{ 336u}
  ,{ 337u}
  ,{ 338u}
};
/*! DID signal operation classes */
CONST(Dcm_CfgDidMgrSignalOpClassInfoType, DCM_CONST) Dcm_CfgDidMgrSignalOpClassInfo[338]=
{
   { ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0100 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0100 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0101 */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0101 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0102 */      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_1_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0102 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0103 */      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_2_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0103 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0104 */      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_3_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0104 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0105 */      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_New_Identification_Parameters_4_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0105 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ReadData)),   1u,   1u,0x0002u} /* DID: 0x0D01 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Size_Of_ECU_Serial_Number_Data_Identifier_SOECUSNDID_SizeOfECUSerialNumberDataIdentifier_SOECUSNDID_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0x0D01 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ReadData)),   2u,   2u,0x0002u} /* DID: 0xC002 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Size_For_Multi_DID_SFMDID_SIZE_multi_DID_SFMDID_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xC002 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD407 */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D407_VCU_AccPedalPosition_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD407 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD40C */           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D40C_ST_ECU_WAKEUP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD40C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ReadData)),   4u,   4u,0x0002u} /* DID: 0xD49C */                 /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D49C_DST_VEH_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD49C */       /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD4B9 */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D4B9_ST_DIAG_MUX_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD4B9 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ReadData)),   4u,   4u,0x0002u} /* DID: 0xD4CA */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D4CA_Ti_BSI_DATE_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD4CA */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD4F7 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_State_of_BEPR_test_mode_for_assembly_plant_ST_MODE_UT_TEST_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD4F7 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD5BD */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD5BD */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT1_BEPR_UT_TEST_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD5BD */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Result_of_the_BEPR_test_for_assembly_plant_RESULT2_BEPR_UT_TEST_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD5BD */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD5CF */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L2_Supply_voltage_side_electric_plug_L2_U_SUPPLY_PLUG_L2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD5CF */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD5D0 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L2_Supply_current_network_side_electric_plug_L2_I_SUPP_63E9E91C_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD5D0 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD5D1 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_d_alimentation_reseau_cote_prise_electrique_L3_Supply_voltage_side_electric_plug_L3_U_SUPPLY_PLUG_L3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD5D1 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD5D2 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Courant_d_alimentation_reseau_cote_prise_electrique_L3_Supply_current_network_side_electric_plug_L3_I_SUPP_1C0B4F7D_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD5D2 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD805 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Vitesse_vehicule_envoyee_via_CAN_par_le_module_ESP_Vehicle_speed_received_from_the_ESP_module_via_CAN_VEL_VEH_ESP_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD805 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD806 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapi_6B35C481_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD806 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD807 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Temperature_mesuree_de_la_broche_1_de_l_embase_de_la_prise_de_charge_rapide_T_DC1_PLUG_RAW_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD807 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD808 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Tension_mesuree_aux_bornes_du_capteur_de_temperature_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapi_85133EA1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD808 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD809 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Temperature_mesuree_de_la_broche_2_de_l_embase_de_la_prise_de_charge_rapide_T_DC2_PLUG_RAW_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD809 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD80C */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D80C_ST_SWITCH_WAKEUP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD80C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD813 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Etat_des_discontacteurs_de_la_batterie_traction_Traction_battery_discontactors_state_ST_DISCONT_BAT_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD813 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD822 */               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D822_U_BAT_ECU_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD822 */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD824 */           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D824_U_DCDC_HT_INP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD824 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD825 */           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D825_I_DCDC_HT_INP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD825 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD826 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Temperature_du_convertisseur_de_courant_DCDC_Temperature_of_the_converter_Direct_Current_to_Direct_Current_484F075F_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD826 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD827 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Etat_du_convertisseur_de_courant_DCDC_Converter_Direct_Current_to_Direct_Current_DCDC_state_DCDC_STATE_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD827 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD828 */           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D828_U_DCDC_BT_INP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD828 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD829 */           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D829_I_DCDC_BT_INP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD829 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD82B */                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82B_U_PLUG_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD82B */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD82C */                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82C_I_PLUG_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD82C */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD82D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Etat_de_la_connexion_de_la_prise_d_alimentation_electrique_base_sur_la_proximity_ST_DECT_CONNECT_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD82D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD82E */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82E_I_PLUG_PILOT_L_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD82E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD82F */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D82F_ST_PUSH_CHG_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD82F */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD831 */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D831_ST_PLUG_REQ_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD831 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD83B */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83B_R_DCDC_REAL_LOAD_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD83B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD83C */       /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83C_ST_DCDC_OVER_TEMP_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD83C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD83D */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83D_ST_DCDC_LV_OVER_CUR_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD83D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD83E */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83E_ST_DCDC_LV_OVER_VOLT_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD83E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD83F */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D83F_ST_DCDC_HV_OVER_CUR_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD83F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD840 */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D840_DCDC_HV_OVERVOLT_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD840 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD843 */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D843_ST_LOCK_CTL_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD843 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD844 */           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D844_ST_UNLOCK_CTL_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD844 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD845 */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D845_U_PLUG_LOCK_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD845 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD846 */        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D846_ST_PLUG_LOCK_MES_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD846 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD84A */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84A_U_AC1_PLUG_MES_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD84A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD84B */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84B_T_AC1_PLUG_RAW_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD84B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD84C */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84C_U_AC2_PLUG_MES_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD84C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD84D */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84D_T_AC2_PLUG_RAW_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD84D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD84E */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84E_R_LEDR_LOAD_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD84E */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD84F */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D84F_R_LEDR_REQ_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD84F */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD850 */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D850_R_LEDG_LOAD_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD850 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD851 */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D851_R_LEDG_REQ_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD851 */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD852 */             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D852_R_LEDB_LOAD_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD852 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD853 */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D853_R_LEDB_REQ_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD853 */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD854 */         /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D854_R_PLUG_LED_LOAD_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD854 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD855 */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D855_R_PLUG_LED_REQ_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD855 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD866 */               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D866_ST_PT_PWT_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD866 */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD8AA */         /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D8AA_ST_RECHARGE_PME_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD8AA */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ReadData)),   1u,   1u,0x0002u} /* DID: 0xD8E9 */                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D8E9_ST_OBC_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD8E9 */      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ReadData)),   2u,   2u,0x0002u} /* DID: 0xD8EB */               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_RDBI_22_NO_SECURED_D8EB_R_HVB_SOC_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xD8EB */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ReadData)),   5u,   5u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ReadData)),   2u,   2u,0x0002u} /* DID: 0xF080 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ReadData)),   5u,   5u,0x0002u} /* DID: 0xF080 */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ReadData)),   4u,   4u,0x0002u} /* DID: 0xF080 */               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Functionnal_Plan_Number_FPN_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_PSA_Supplier_Code_ZA_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Product_Number_FPRN_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_High_order_MSB_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Software_evolution_index_Low_order_LSB_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_High_order_MSB_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_Low_order_LSB_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Diag_Frame_Evolution_Index_DFEI_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_No_of_Parameters_Associated_to_the_Faults_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Authentification_Zone_ZA_Reserved_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF080 */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF08F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ReadData)),   5u,   5u,0x0002u} /* DID: 0xF08F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ReadData)),  16u,  16u,0x0002u} /* DID: 0xF08F */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF08F */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_PSASupplierCode_PSASC_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF08F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_DeviceInitial_DI_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF08F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_FreeField_FF_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF08F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_EMC_Standardization_Label_EMCSL_EMCIndex_EMCI_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF08F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ReadData)),   2u,   2u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ReadData)),   2u,   2u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ReadData)),   2u,   2u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ReadData)),   3u,   3u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ReadData)),   3u,   3u,0x0002u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_0_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Reserved_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_PSA_Supplier_Code_ZI_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Code_system_du_calculateur_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Application_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Version_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Edition_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Day_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Month_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Download_Date_Year_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_site_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Tool_Signature_DTS_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Downloading_Number_DS_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Identification_Zone_for_Downloadable_ECU_ZI_Software_Reference_ulp_or_cal_file_number_in_BCD_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF0FE */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ReadData)),  10u,  10u,0x0002u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_NumberOfModule_NOM_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootSoftwareIdentificationNumber_BSIN_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootVersion_BV_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Boot_Software_Identification_Data_Identifier_BSIDID_BootEdition_BE_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF180 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF186 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Active_Diagnostic_Session_Data_Identifier_ADSDID_diagnosticSessionType_LEV_DS_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF186 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF18B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF18B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ReadData)),   1u,   1u,0x0002u} /* DID: 0xF18B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_day_ECUMDDID_DAY_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF18B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_month_ECUMDDID_MONTH_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF18B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Manufacturing_Date_Data_Identifier_ECUMDDID_ECU_manufacturing_year_ECUMDDID_YEAR_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF18B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadData)),   0u,  20u,0x0002u} /* DID: 0xF18C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ReadDataLength)),   0u,   0u,0x0102u} /* DID: 0xF18C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ECU_Serial_Number_Data_Identifier_ECUSNDID_ECUSerialNumber_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xF18C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_SW_version_Major_version_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD00 */                        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_SW_version_Minor_version_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD00 */                        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_SW_version_Major_version_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD00 */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_SW_version_Minor_version_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD00 */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD01 */                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD01 */               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ReadData)),   2u,   2u,0x0002u} /* DID: 0xFD01 */                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD01 */      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD01 */     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD01 */         /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_OFM_Faults_DataRecord_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD02 */                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_OFM_Faults_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD02 */                 /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_LFM_Faults_DataRecord_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD03 */                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_LFM_Faults_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD03 */                 /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD04 */                    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD04 */          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_OFM_HW_faults_DataRecord_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD05 */                        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD05 */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_LFM_HW_faults_DataRecord_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD06 */                        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD06 */              /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ReadData)),   2u,   2u,0x0002u} /* DID: 0xFD07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_I_reset_counters_Total_Number_of_Resets_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_I_reset_counters_Watchdog_Reset_Counter_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_I_reset_counters_Safety_Reset_Counter_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_II_uC_reset_reasons_snaps_uC_Reset_Reasons_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD08 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_III_safety_reset_error_id_snaps_Safety_Reset_Error_ID_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_0_smu_alarm_group0_snaps_Alarm_Group0_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0A */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_1_smu_alarm_group1_snaps_Alarm_Group1_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_2_smu_alarm_group2_snaps_Alarm_Group2_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_3_smu_alarm_group3_snaps_Alarm_Group3_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_4_smu_alarm_group4_snaps_Alarm_Group4_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_5_smu_alarm_group5_snaps_Alarm_Group5_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ReadData)),   4u,   4u,0x0002u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_1_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_2_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_3_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_ResetReasons_IV_6_smu_alarm_group6_snaps_Alarm_Group6_last_reset_4_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD10 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData)),   1u,   1u,0x0002u} /* DID: 0xFD11 */  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD11 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData)),   1u,   1u,0x1002u} /* DID: 0xFD11 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_PLC_MAC_address_MAC_ReadData)),   6u,   6u,0x0002u} /* DID: 0xFD12 */                             /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_PLC_MAC_address_MAC_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD12 */                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_PLC_MAC_address_MAC_WriteData)),   6u,   6u,0x1002u} /* DID: 0xFD12 */                            /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Mahle_EOL_information_EOLData_ReadData)),  76u,  76u,0x0002u} /* DID: 0xFD13 */                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Mahle_EOL_information_EOLData_ConditionCheckRead)),   0u,   0u,0x0201u} /* DID: 0xFD13 */         /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_Mahle_EOL_information_EOLData_WriteData)),  76u,  76u,0x1002u} /* DID: 0xFD13 */                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! RID properties */
CONST(Dcm_CfgRidMgrRidInfoType, DCM_CONST) Dcm_CfgRidMgrRidInfo[9]=
{
   {    0u,   3u,0x05u, 0u} /* RID: 0x0400 */
  ,{    2u,   3u,0x05u, 0u} /* RID: 0xDD43 */
  ,{    4u,   7u,0x07u, 0u} /* RID: 0xDF66 */
  ,{    7u,   7u,0x07u, 0u} /* RID: 0xDF68 */
  ,{   10u,   7u,0x07u, 0u} /* RID: 0xDF6C */
  ,{   13u,   7u,0x07u, 0u} /* RID: 0xDF6D */
  ,{   16u,   7u,0x07u, 0u} /* RID: 0xDF6E */
  ,{   19u,   5u,0x05u, 0u} /* RID: 0xFF00 */
  ,{   21u,   5u,0x05u, 0u} /* RID: 0xFF04 */
};
/*! RID operation properties */
CONST(Dcm_CfgRidMgrOpInfoType, DCM_CONST) Dcm_CfgRidMgrOpInfo[23]=
{
   { ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_Start)),   0u,   0u,   1u,   1u, 3u} /* RID: 0x0400 */  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults)),   0u,   0u,   1u,   1u, 3u} /* RID: 0x0400 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Dcm_RidMgr_DD43_Start)),   1u,   1u,   2u,   2u, 9u} /* RID: 0xDD43 */                                                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Dcm_RidMgr_DD43_RequestResults)),   0u,   0u,   2u,   2u, 9u} /* RID: 0xDD43 */                                         /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF66 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop)),   0u,   0u,   1u,   1u, 3u} /* RID: 0xDF66 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF66 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Start)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF68 */   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_Stop)),   0u,   0u,   1u,   1u, 3u} /* RID: 0xDF68 */    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF68 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF6C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop)),   0u,   0u,   1u,   1u, 3u} /* RID: 0xDF6C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF6C */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF6D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop)),   0u,   0u,   1u,   1u, 3u} /* RID: 0xDF6D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF6D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF6E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop)),   0u,   0u,   1u,   1u, 3u} /* RID: 0xDF6E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults)),   0u,   0u,   0u,   3u, 6u} /* RID: 0xDF6E */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Dcm_RidMgr_FF00_Start)),   3u,   3u,   1u,   1u, 9u} /* RID: 0xFF00 */                                                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_erase_Memory_EM_RequestResults)),   0u,   0u,   1u,   1u, 3u} /* RID: 0xFF00 */                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_check_Memory_CM_Start)),   0u,   0u,   0u,   2u, 6u} /* RID: 0xFF04 */                         /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_check_Memory_CM_RequestResults)),   0u,   0u,   0u,   2u, 6u} /* RID: 0xFF04 */                /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! DCM service initializers */
CONST(Dcm_DiagSvcInitFuncType, DCM_CONST) Dcm_CfgDiagSvcInitializers[2]=
{
   Dcm_Service27Init
  ,NULL_PTR /* end marker */
};
/*! DCM service properties */
CONST(Dcm_CfgDiagServiceInfoType, DCM_CONST) Dcm_CfgDiagServiceInfo[13]=
{
   { Dcm_Service10Processor,0x01u, 1u,   1u,   2u, 0u, 0u} /* SID: 0x10 */
  ,{ Dcm_Service11Processor,0x01u, 1u,   3u,   4u, 0u, 0u} /* SID: 0x11 */
  ,{ Dcm_Service14Processor,0x00u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x14 */
  ,{ Dcm_Service19Processor,0x01u, 1u,   5u,   0u, 2u, 0u} /* SID: 0x19 */
  ,{ Dcm_Service22Processor,0x00u, 2u,   0u,   0u, 0u, 0u} /* SID: 0x22 */
  ,{ Dcm_Service27Processor,0x03u, 1u,   6u,   0u, 0u, 0u} /* SID: 0x27 */
  ,{ Dcm_Service2EProcessor,0x00u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x2E */
  ,{ Dcm_Service31Processor,0x01u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x31 */
  ,{ Dcm_SvcWrapper_DCMCallback_RequestDownload,0x00u, 0u,   0u,   0u, 0u, 0u} /* SID: 0x34 */
  ,{ Dcm_SvcWrapper_DCMCallback_TransferData,0x00u, 0u,   0u,   0u, 0u, 0u} /* SID: 0x36 */
  ,{ Dcm_SvcWrapper_DCMCallback_RequestTransferExit,0x00u, 0u,   0u,   0u, 0u, 0u} /* SID: 0x37 */
  ,{ Dcm_Service3EProcessor,0x01u, 1u,   0u,   0u, 0u, 0u} /* SID: 0x3E */
  ,{ Dcm_RepeaterDeadEnd,0x00u, 0u,   0u,   0u, 0u, 0u} /* Dcm_RepeaterDeadEnd */
};
/*! Indirection from diag service info to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgDiagSvcIdExecPrecondTable[12]=
{
      0u /* SID: 0x10 */
  ,   3u /* SID: 0x11 */
  ,   4u /* SID: 0x14 */
  ,   0u /* SID: 0x19 */
  ,   0u /* SID: 0x22 */
  ,   5u /* SID: 0x27 */
  ,   3u /* SID: 0x2E */
  ,   6u /* SID: 0x31 */
  ,   5u /* SID: 0x34 */
  ,   5u /* SID: 0x36 */
  ,   5u /* SID: 0x37 */
  ,   0u /* SID: 0x3E */
};
/*! DCM service post processors */
CONST(Dcm_DiagSvcConfirmationFuncType, DCM_CONST) Dcm_CfgDiagSvcPostProcessors[7]=
{
   Dcm_ServiceNoPostProcessor
  ,Dcm_Service10PostProcessor
  ,Dcm_Service10FastPostProcessor
  ,Dcm_Service11PostProcessor
  ,Dcm_Service11FastPostProcessor
  ,Dcm_Service19PostProcessor
  ,Dcm_Service27PostProcessor
};
/*! DCM service paged buffer updater */
CONST(Dcm_DiagSvcUpdateFuncType, DCM_CONST) Dcm_CfgDiagSvcUpdaters[3]=
{
   Dcm_ServiceNoUpdater
  ,Dcm_PagedBufferDataPadding
  ,Dcm_Service19Updater
};
/*! DCM service paged buffer canceller */
CONST(Dcm_DiagSvcCancelFuncType, DCM_CONST) Dcm_CfgDiagSvcCancellers[1]=
{
   Dcm_ServiceNoCancel
};
/*! OEM notification functions */
CONST(Dcm_CfgDiagNotificationInfoType, DCM_CONST) Dcm_CfgDiagOemNotificationInfo[2]=
{
   { Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication,Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation}
  ,{ NULL_PTR,NULL_PTR}
};
/*! Service 0x10 sub-service properties table  */
CONST(Dcm_CfgSvc10SubFuncInfoType, DCM_CONST) Dcm_CfgSvc10SubFuncInfo[4]=
{
   { { 200u,  20u}, 0u} /* Session ID: 0x01 */
  ,{ { 200u,  20u}, 1u} /* Session ID: 0x02 */
  ,{ { 200u,  20u}, 0u} /* Session ID: 0x03 */
  ,{ { 200u,  20u}, 0u} /* Session ID: 0x50 */
};
/*! Indirection from service 0x10 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc10SubFuncExecPrecondTable[4]=
{
      0u /* Session ID: 0x01 */
  ,   1u /* Session ID: 0x02 */
  ,   2u /* Session ID: 0x03 */
  ,   2u /* Session ID: 0x50 */
};
/*! Service 0x11 sub-service properties table  */
CONST(Dcm_CfgSvc11SubFuncInfoType, DCM_CONST) Dcm_CfgSvc11SubFuncInfo[3]=
{
   { Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Hard} /* SF: 0x01 */
  ,{ Dcm_SubSvcWrapper_DCMCallBack_ECUReset_KeyOffOn} /* SF: 0x02 */
  ,{ Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Soft} /* SF: 0x03 */
};
/*! Indirection from service 0x11 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc11SubFuncExecPrecondTable[3]=
{
      3u /* SF: 0x01 */
  ,   3u /* SF: 0x02 */
  ,   3u /* SF: 0x03 */
};
/*! Service 0x19 sub-service properties table  */
CONST(Dcm_CfgSvc19SubFuncInfoType, DCM_CONST) Dcm_CfgSvc19SubFuncInfo[3]=
{
   { Dcm_Service19_01Processor, 2u} /* SF: 0x01 */
  ,{ Dcm_Service19_02Processor, 2u} /* SF: 0x02 */
  ,{ Dcm_Service19_04Processor, 5u} /* SF: 0x04 */
};
/*! Indirection from service 0x19 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc19SubFuncExecPrecondTable[3]=
{
      2u /* SF: 0x01 */
  ,   0u /* SF: 0x02 */
  ,   2u /* SF: 0x04 */
};
/*! Service 0x27 sub-service properties table  */
CONST(Dcm_CfgSvc27SubFuncInfoType, DCM_CONST) Dcm_CfgSvc27SubFuncInfo[2]=
{
   {    1u} /* SF: 0x01 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    5u} /* SF: 0x02 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! Service 0x27 security level properties table  */
CONST(Dcm_CfgSvc27SecLevelInfoType, DCM_CONST) Dcm_CfgSvc27SecLevelInfo[1]=
{
   { ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_UnlockedL1_GetSeed)),Rte_Call_SecurityAccess_UnlockedL1_CompareKey,   4u, 0u} /* SecLvl: UnlockedL1 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! Indirection from service 0x27 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc27SubFuncExecPrecondTable[2]=
{
      5u /* SF: 0x01 */
  ,   5u /* SF: 0x02 */
};
/*! Indirection from service 0x3E sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc3ESubFuncExecPrecondTable[1]=
{
      0u /* SF: 0x00 */
};
#define DCM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   Module call-out implementations
---------------------------------------------- */
#define DCM_START_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/***********************************************************************************************************************
 *  Dcm_RidMgr_DD43_Start()
***********************************************************************************************************************/
/* Implements CDD Dcm_RidMgr<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_DD43_Start(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  uint8 dataInIn_CHOIX_MODE_TEST;

  dataInIn_CHOIX_MODE_TEST = ((uint8)(Dcm_DiagGetReqDataAsU8Rel(pMsgContext, 0u)))                                                                   /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */;

  DCM_IGNORE_UNREF_PARAM(DataLength);                                                                                                                /* PRQA S 3112 */ /* MD_Dcm_3112 */

  return Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(dataInIn_CHOIX_MODE_TEST
                                                                                             , OpStatus
                                                                                             , Dcm_DiagGetResDataRel(pMsgContext, 0u)                /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */ /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */
                                                                                             , Dcm_DiagGetResDataRel(pMsgContext, 1u)                /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
                                                                                             , ErrorCode
                                                                                             );                                                      /* SBSW_DCM_GEN_COMB_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_RidMgr_DD43_RequestResults()
***********************************************************************************************************************/
/* Implements CDD Dcm_RidMgr<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_DD43_RequestResults(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(DataLength);                                                                                                                /* PRQA S 3112 */ /* MD_Dcm_3112 */

  return Rte_Call_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(OpStatus
                                                                                                      , Dcm_DiagGetResDataRel(pMsgContext, 0u)       /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */ /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */
                                                                                                      , Dcm_DiagGetResDataRel(pMsgContext, 1u)       /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
                                                                                                      , ErrorCode
                                                                                                      );                                             /* SBSW_DCM_GEN_COMB_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_RidMgr_FF00_Start()
***********************************************************************************************************************/
/* Implements CDD Dcm_RidMgr<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_FF00_Start(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  uint8 dataInIn_routineControlOptionRecord_FF00;
  uint16 dataInIn_RCEOR_SGN_OTL;

  dataInIn_routineControlOptionRecord_FF00 = ((uint8)(Dcm_DiagGetReqDataAsU8Rel(pMsgContext, 0u)))                                                   /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */;
  dataInIn_RCEOR_SGN_OTL = ((uint16)(Dcm_UtiMake16Bit(Dcm_DiagGetReqDataAsU8Rel(pMsgContext, 1u), Dcm_DiagGetReqDataAsU8Rel(pMsgContext, 2u))));

  DCM_IGNORE_UNREF_PARAM(DataLength);                                                                                                                /* PRQA S 3112 */ /* MD_Dcm_3112 */

  return Rte_Call_RoutineServices_erase_Memory_EM_Start(dataInIn_routineControlOptionRecord_FF00
                                                       , dataInIn_RCEOR_SGN_OTL
                                                       , OpStatus
                                                       , Dcm_DiagGetResDataRel(pMsgContext, 0u)                                                      /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */ /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */
                                                       , ErrorCode
                                                       );                                                                                            /* SBSW_DCM_GEN_COMB_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DCMCallback_RequestDownload()
***********************************************************************************************************************/
/* Implements CDD Dcm_SvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DCMCallback_RequestDownload(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return DCMCallback_RequestDownload(opStatus, pMsgContext, ErrorCode);                                                                              /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DCMCallback_TransferData()
***********************************************************************************************************************/
/* Implements CDD Dcm_SvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DCMCallback_TransferData(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return DCMCallback_TransferData(opStatus, pMsgContext, ErrorCode);                                                                                 /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DCMCallback_RequestTransferExit()
***********************************************************************************************************************/
/* Implements CDD Dcm_SvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DCMCallback_RequestTransferExit(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return DCMCallback_RequestTransferExit(opStatus, pMsgContext, ErrorCode);                                                                          /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_ServiceNoPostProcessor()
***********************************************************************************************************************/
/* Implements CDD Dcm_ServiceNoPostProcessor() */
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoPostProcessor(Dcm_ContextPtrType pContext, Dcm_ConfirmationStatusType status)                    /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(status);                                                                                                                    /* PRQA S 3112 */ /* MD_Dcm_3112 */
}
/***********************************************************************************************************************
 *  Dcm_ServiceNoUpdater()
***********************************************************************************************************************/
/* Implements CDD Dcm_ServiceNoUpdater() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_ServiceNoUpdater(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_DiagDataContextPtrType pDataContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(opStatus);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(pDataContext);                                                                                                              /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(ErrorCode);                                                                                                                 /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return E_NOT_OK;
}
/***********************************************************************************************************************
 *  Dcm_ServiceNoCancel()
***********************************************************************************************************************/
/* Implements CDD Dcm_ServiceNoCancel() */
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoCancel(Dcm_ContextPtrType pContext, Dcm_DiagDataContextPtrType pDataContext)                     /* PRQA S 3673 */ /* MD_Dcm_Design_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(pDataContext);                                                                                                              /* PRQA S 3112 */ /* MD_Dcm_3112 */
  /* nothing to do */
}
/***********************************************************************************************************************
 *  Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Hard()
***********************************************************************************************************************/
/* Implements CDD Dcm_SubSvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Hard(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return DCMCallBack_ECUReset_Hard(opStatus, pMsgContext, ErrorCode);                                                                                /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_SubSvcWrapper_DCMCallBack_ECUReset_KeyOffOn()
***********************************************************************************************************************/
/* Implements CDD Dcm_SubSvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SubSvcWrapper_DCMCallBack_ECUReset_KeyOffOn(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return DCMCallBack_ECUReset_KeyOffOn(opStatus, pMsgContext, ErrorCode);                                                                            /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
}
/***********************************************************************************************************************
 *  Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Soft()
***********************************************************************************************************************/
/* Implements CDD Dcm_SubSvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SubSvcWrapper_DCMCallBack_ECUReset_Soft(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return DCMCallBack_ECUReset_Soft(opStatus, pMsgContext, ErrorCode);                                                                                /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
}
#define DCM_STOP_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ********************************************************************************************************************
 * END OF FILE: Dcm_Lcfg.c
 * ******************************************************************************************************************** */

