/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_MemoryProtection_Lcfg.h
 *   Generation Time: 2020-08-19 13:07:50
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


                                                                                                                        /* PRQA S 0388  EOF */ /* MD_MSR_Dir1.1 */

#ifndef OS_MEMORYPROTECTION_LCFG_H
# define OS_MEMORYPROTECTION_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */
# include "Os_MemoryProtection_Types.h"

/* Os kernel module dependencies */

/* Os hal dependencies */


/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

# define OS_START_SEC_CORE0_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Memory protection configuration data */
extern CONST(Os_MpCoreConfigType, OS_CONST) OsCfg_Mp_OsCore0;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_OsApplication_ASILB;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_OsApplication_BSW_QM;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_OsApplication_QM;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_SystemApplication_OsCore0;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_CanIsr_1;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_CanIsr_2;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_CounterIsr_SystemTimer;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_DMACH10SR_ISR;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_DMACH11SR_ISR;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_GTMTOM1SR0_ISR;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_PLC_Interrupt;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QSPI1ERR_ISR;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QSPI1PT_ISR;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QSPI1UD_ISR;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_ASILB_MAIN_TASK;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_ASILB_init_task;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_BSW_Async_QM_Task;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_BSW_Async_Task;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_BSW_Sync_Task;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_Init_Task;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_Default_RTE_Mode_switch_Task;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_IdleTask_OsCore0;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QM_MAIN_TASK;
extern CONST(Os_MpAccessRightsType, OS_CONST) OsCfg_Mp_QM_init_task;

# define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


# define OS_START_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Memory protection configuration data */
extern CONST(Os_MpSystemConfigType, OS_CONST) OsCfg_Mp_SystemMpu;

# define OS_STOP_SEC_CONST_UNSPECIFIED
# include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_MEMORYPROTECTION_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_MemoryProtection_Lcfg.h
 *********************************************************************************************************************/
