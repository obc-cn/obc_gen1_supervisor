/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: TcpIp
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: TcpIp_Lcfg.c
 *   Generation Time: 2020-11-23 11:39:47
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


/* PRQA S 0828 EOF */ /* MD_MSR_1.1_828 */
/* PRQA S 1257 EOF */ /* MD_CSL_Rule10.3_OldValidLib */

/*lint -e552 */

#define TCPIP_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "TcpIp_Lcfg.h"
#include "TcpIp_Cfg.h"
#include "TcpIp.h"
#include "Appl_Rand.h" 
#include "Scc_Cbk.h" 


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/
#if defined (STATIC)
#else
# define STATIC static
#endif

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/* PRQA S 0612 OBJECT_SIZE */ /* MD_TcpIp_0612_BufferSize */

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  TcpIp_DuplicateAddrDetectionFctPtr
**********************************************************************************************************************/
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_DuplicateAddrDetectionCbkType, TCPIP_CONST) TcpIp_DuplicateAddrDetectionFctPtr = NULL_PTR;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_EthIfCtrl
**********************************************************************************************************************/
/** 
  \var    TcpIp_EthIfCtrl
  \brief  Array mapping from EthIfCtrlIdx to IpCtrlIdx
  \details
  Element        Description
  IpV6CtrlIdx    the index of the 0:1 relation pointing to TcpIp_IpV6Ctrl
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_EthIfCtrlType, TCPIP_CONST) TcpIp_EthIfCtrl[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IpV6CtrlIdx */
  { /*     0 */          0u }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IcmpV6Config
**********************************************************************************************************************/
/** 
  \var    TcpIp_IcmpV6Config
  \details
  Element           Description
  EchoRequestApi    Enable Echo Request API - Value of configuration parameter TcpIpIcmpV6EchoRequestApiEnabled [BOOLEAN]
  HopLimit          Hop Limit - Value of configuration parameter TcpIpIcmpV6HopLimit
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_IcmpV6ConfigType, TCPIP_CONST) TcpIp_IcmpV6Config[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    EchoRequestApi  HopLimit */
  { /*     0 */          FALSE,      64u }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IcmpV6MsgHandlerCbkFctPtr
**********************************************************************************************************************/
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_IcmpMultiPartMsgHandlerCbkType, TCPIP_CONST) TcpIp_IcmpV6MsgHandlerCbkFctPtr = NULL_PTR;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6Ctrl
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6Ctrl
  \details
  Element                               Description
  DefaultLinkMtu                        Default Mtu Size - Value of configuration parameter TcpIpIpV6DefaultMtuSize
  MaskedBits                            contains bitcoded the boolean data of TcpIp_AllowLinkMtuReconfigurationOfIpV6Ctrl, TcpIp_DhcpUserOptionUsedOfIpV6Ctrl, TcpIp_EnableDynHopLimitOfIpV6Ctrl, TcpIp_EnablePathMtuOfIpV6Ctrl, TcpIp_HwChecksumIcmpOfIpV6Ctrl, TcpIp_HwChecksumIpDestinationOptionsOfIpV6Ctrl, TcpIp_HwChecksumIpHopByHopOptionsOfIpV6Ctrl, TcpIp_HwChecksumIpRoutingOfIpV6Ctrl, TcpIp_HwChecksumTcpOfIpV6Ctrl, TcpIp_HwChecksumUdpOfIpV6Ctrl, TcpIp_IpV6MulticastAddrUsedOfIpV6Ctrl, TcpIp_LocalAddrV6BcUsedOfIpV6Ctrl
  PathMtuTimeout                        Path Mtu Timeout - Value of configuration parameter TcpIpIpV6PathMtuTimeout [SECONDS]
  DefaultHopLimit                       Default Hop Limit - Value of configuration parameter TcpIpIpV6DefaultHopLimit
  DefaultTrafficClassFlowLabelNbo       -
  DhcpMode                              -
  DhcpUserOptionEndIdx                  the end index of the 0:n relation pointing to TcpIp_DhcpUserOption
  DhcpUserOptionStartIdx                the start index of the 0:n relation pointing to TcpIp_DhcpUserOption
  EthIfCtrlIdx                          -
  FramePrioDefault                      IP Frame Prio Default - Value of configuration parameter TcpIpIpFramePrioDefault
  InterfaceIdentifierEndIdx             the end index of the 1:n relation pointing to TcpIp_InterfaceIdentifier
  InterfaceIdentifierStartIdx           the start index of the 1:n relation pointing to TcpIp_InterfaceIdentifier
  IpV6DefaultRouterListEntryEndIdx      the end index of the 1:n relation pointing to TcpIp_IpV6DefaultRouterListEntry
  IpV6DefaultRouterListEntryStartIdx    the start index of the 1:n relation pointing to TcpIp_IpV6DefaultRouterListEntry
  IpV6DestinationCacheEntryEndIdx       the end index of the 1:n relation pointing to TcpIp_IpV6DestinationCacheEntry
  IpV6DestinationCacheEntryStartIdx     the start index of the 1:n relation pointing to TcpIp_IpV6DestinationCacheEntry
  IpV6EthBufDataEndIdx                  the end index of the 1:n relation pointing to TcpIp_IpV6EthBufData
  IpV6EthBufDataStartIdx                the start index of the 1:n relation pointing to TcpIp_IpV6EthBufData
  IpV6MulticastAddrEndIdx               the end index of the 0:n relation pointing to TcpIp_IpV6MulticastAddr
  IpV6MulticastAddrStartIdx             the start index of the 0:n relation pointing to TcpIp_IpV6MulticastAddr
  IpV6NeighborCacheEntryEndIdx          the end index of the 1:n relation pointing to TcpIp_IpV6NeighborCacheEntry
  IpV6NeighborCacheEntryStartIdx        the start index of the 1:n relation pointing to TcpIp_IpV6NeighborCacheEntry
  IpV6PrefixListEntryEndIdx             the end index of the 1:n relation pointing to TcpIp_IpV6PrefixListEntry
  IpV6PrefixListEntryStartIdx           the start index of the 1:n relation pointing to TcpIp_IpV6PrefixListEntry
  IpV6SourceAddressEndIdx               the end index of the 1:n relation pointing to TcpIp_IpV6SourceAddress
  IpV6SourceAddressStartIdx             the start index of the 1:n relation pointing to TcpIp_IpV6SourceAddress
  LocalAddrV6BcIdx                      the index of the 0:1 relation pointing to TcpIp_LocalAddrV6
  NdpConfigIdx                          the index of the 1:1 relation pointing to TcpIp_NdpConfig
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_IpV6CtrlType, TCPIP_CONST) TcpIp_IpV6Ctrl[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    DefaultLinkMtu  MaskedBits  PathMtuTimeout  DefaultHopLimit  DefaultTrafficClassFlowLabelNbo  DhcpMode                   DhcpUserOptionEndIdx                     DhcpUserOptionStartIdx                     EthIfCtrlIdx                               FramePrioDefault  InterfaceIdentifierEndIdx  InterfaceIdentifierStartIdx  IpV6DefaultRouterListEntryEndIdx  IpV6DefaultRouterListEntryStartIdx  IpV6DestinationCacheEntryEndIdx  IpV6DestinationCacheEntryStartIdx  IpV6EthBufDataEndIdx  IpV6EthBufDataStartIdx  IpV6MulticastAddrEndIdx                     IpV6MulticastAddrStartIdx                     IpV6NeighborCacheEntryEndIdx  IpV6NeighborCacheEntryStartIdx  IpV6PrefixListEntryEndIdx  IpV6PrefixListEntryStartIdx  IpV6SourceAddressEndIdx  IpV6SourceAddressStartIdx  LocalAddrV6BcIdx  NdpConfigIdx        Referable Keys */
  { /*     0 */          1500u,    0x0201u,           600u,             64u,                           0x00u, IPV6_DHCPV6_MODE_DISABLED, TCPIP_NO_DHCPUSEROPTIONENDIDXOFIPV6CTRL, TCPIP_NO_DHCPUSEROPTIONSTARTIDXOFIPV6CTRL, EthIfConf_EthIfController_EthIfController,               0u,                       64u,                          0u,                               2u,                                 0u,                              5u,                                0u,                   2u,                     0u, TCPIP_NO_IPV6MULTICASTADDRENDIDXOFIPV6CTRL, TCPIP_NO_IPV6MULTICASTADDRSTARTIDXOFIPV6CTRL,                           5u,                             0u,                        5u,                          0u,                      1u,                        0u,               1u,           0u }   /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl] */
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6General
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6General
  \details
  Element                   Description
  IpV6CtrlDefaultIdx        the index of the 0:1 relation pointing to TcpIp_IpV6Ctrl
  IpV6SocketDynIcmpIdx      the index of the 1:1 relation pointing to TcpIp_IpV6SocketDyn
  IpV6SocketDynNdpIdx       the index of the 1:1 relation pointing to TcpIp_IpV6SocketDyn
  IpV6SocketDynTcpRstIdx    the index of the 0:1 relation pointing to TcpIp_IpV6SocketDyn
  MaskedBits                contains bitcoded the boolean data of TcpIp_ExtDestAddrValidationEnabledOfIpV6General, TcpIp_IpV6CtrlDefaultUsedOfIpV6General, TcpIp_IpV6SocketDynTcpRstUsedOfIpV6General
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_IpV6GeneralType, TCPIP_CONST) TcpIp_IpV6General[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IpV6CtrlDefaultIdx                        IpV6SocketDynIcmpIdx  IpV6SocketDynNdpIdx  IpV6SocketDynTcpRstIdx  MaskedBits */
  { /*     0 */ TCPIP_NO_IPV6CTRLDEFAULTIDXOFIPV6GENERAL,                   3u,                  4u,                     2u,      0x01u }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6SourceAddress
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6SourceAddress
  \brief  Structure containing all statically configured IP address information.
  \details
  Element                 Description
  AddressAssignVariant    Ip address configuration source.
  DefaultAddrV6Idx        the index of the 0:1 relation pointing to TcpIp_DefaultAddrV6
  LocalAddrV6Idx          the index of the 1:1 relation pointing to TcpIp_LocalAddrV6
  MaskedBits              contains bitcoded the boolean data of TcpIp_DefaultAddrV6UsedOfIpV6SourceAddress, TcpIp_PrefixIsOnLinkOfIpV6SourceAddress
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_IpV6SourceAddressType, TCPIP_CONST) TcpIp_IpV6SourceAddress[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    AddressAssignVariant               DefaultAddrV6Idx                              LocalAddrV6Idx  MaskedBits        Referable Keys */
  { /*     0 */ TCPIP_IPADDR_ASSIGNMENT_LINKLOCAL, TCPIP_NO_DEFAULTADDRV6IDXOFIPV6SOURCEADDRESS,             0u,      0x00u }   /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpV6LocalAddr_TcpIpCtrl_LinkLocal, /ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_LocalAddrV6
**********************************************************************************************************************/
/** 
  \var    TcpIp_LocalAddrV6
  \brief  -
  \details
  Element                 Description
  IpV6CtrlIdx             the index of the 1:1 relation pointing to TcpIp_IpV6Ctrl
  IpV6MulticastAddrIdx    the index of the 0:1 relation pointing to TcpIp_IpV6MulticastAddr
  IpV6SourceAddressIdx    the index of the 0:1 relation pointing to TcpIp_IpV6SourceAddress
  MaskedBits              contains bitcoded the boolean data of TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6, TcpIp_IpV6SourceAddressUsedOfLocalAddrV6
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_LocalAddrV6Type, TCPIP_CONST) TcpIp_LocalAddrV6[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    IpV6CtrlIdx  IpV6MulticastAddrIdx                        IpV6SourceAddressIdx                        MaskedBits        Referable Keys */
  { /*     0 */          0u, TCPIP_NO_IPV6MULTICASTADDRIDXOFLOCALADDRV6,                                         0u,      0x01u },  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpV6LocalAddr_TcpIpCtrl_LinkLocal, /ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  { /*     1 */          0u, TCPIP_NO_IPV6MULTICASTADDRIDXOFLOCALADDRV6, TCPIP_NO_IPV6SOURCEADDRESSIDXOFLOCALADDRV6,      0x00u }   /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpV6LocalAddr_TcpIpCtrl_AsAn, /ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_NdpConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_NdpConfig
  \details
  Element                    Description
  MaskedBits                 contains bitcoded the boolean data of TcpIp_DynamicReachableTimeOfNdpConfig, TcpIp_DynamicRetransTimerOfNdpConfig, TcpIp_EnableNdpInvNaNcUpdateOfNdpConfig, TcpIp_EnableNdpInvNaOfNdpConfig, TcpIp_EnableNdpInvNsOfNdpConfig, TcpIp_EnableNudOfNdpConfig, TcpIp_EnableOptimisticDadOfNdpConfig, TcpIp_EnableRfc6106DnsslOptOfNdpConfig, TcpIp_EnableRfc6106RdnssOptOfNdpConfig, TcpIp_EnableSlaacDelayOfNdpConfig, TcpIp_RandomReachableTimeOfNdpConfig, TcpIp_RndRtrSolicitationDelayOfNdpConfig
  MaxRtrSolicitationDelay    Max Rtr Solicitation Delay - Value of configuration parameter TcpIpNdpMaxRtrSolicitationDelay [MILLISECONDS]
  MaxSlaacDelay              Max address configuration delay - Value of configuration parameter TcpIpNdpSlaacMaxDelay [MILLISECONDS]
  NudFirstProbeDelay         Delay First Probe Time - Value of configuration parameter TcpIpNdpDelayFirstProbeTime [MILLISECONDS]
  ReachableTime              Default Reachable Time - Value of configuration parameter TcpIpNdpDefaultReachableTime [MILLISECONDS]
  RetransTimer               Default Retrans Timer - Value of configuration parameter TcpIpNdpDefaultRetransTimer [MILLISECONDS]
  RtrSolicitationInterval    Rtr Solicitation Interval - Value of configuration parameter TcpIpNdpRtrSolicitationInterval [MILLISECONDS]
  SlaacMinLifetime           Minimum Address Lifetime - Value of configuration parameter TcpIpNdpSlaacMinLifetime [SECONDS]
  DadTransmits               DAD Number Of Transmissions - Value of configuration parameter TcpIpNdpSlaacDadNumberOfTransmissions
  MaxRandomFactor            Max Random Factor - Value of configuration parameter TcpIpNdpMaxRandomFactor
  MaxRtrSolicitations        Max Rtr Solicitations - Value of configuration parameter TcpIpNdpMaxRtrSolicitations
  MinRandomFactor            Min Random Factor - Value of configuration parameter TcpIpNdpMinRandomFactor
  MulticastSolicits          Num Multicast Solicitations - Value of configuration parameter TcpIpNdpNumMulticastSolicitations
  UnicastSolicits            Num Unicast Solicitations - Value of configuration parameter TcpIpNdpNumUnicastSolicitations
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_NdpConfigType, TCPIP_CONST) TcpIp_NdpConfig[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MaskedBits  MaxRtrSolicitationDelay  MaxSlaacDelay  NudFirstProbeDelay  ReachableTime  RetransTimer  RtrSolicitationInterval  SlaacMinLifetime  DadTransmits  MaxRandomFactor  MaxRtrSolicitations  MinRandomFactor  MulticastSolicits  UnicastSolicits        Referable Keys */
  { /*     0 */    0x0D43u,                   1000u,         1000u,              5000u,        30000u,        1000u,                   4000u,            7200u,           1u,             15u,                  3u,              5u,                3u,              3u }   /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpIpConfig/TcpIpIpV6Config/TcpIpNdpConfig] */
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_RandomNumberFctPtr
**********************************************************************************************************************/
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_GetRandomNumberType, TCPIP_CONST) TcpIp_RandomNumberFctPtr = Appl_Crypto_GetRandNo;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketOwnerConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketOwnerConfig
  \brief  -
  \details
  Element                            Description
  CopyTxDataDynFuncPtr               [User]_CopyTxDataDyn Callback Function. (only required if socket owner uses indirect data provistion in Tcp/UdpTransmit.)
  CopyTxDataFuncPtr                  [User]_CopyTxData Callback Function. (only required if socket owner uses indirect data provistion in Tcp/UdpTransmit.)
  DhcpEventFuncPtr                   Callout triggered on reception and transmission of client DHCP messages.
  LocalIpAddrAssignmentChgFuncPtr    -
  RxIndicationFuncPtr                [User]_RxIndication Callback Function. (required for socket owner that recieves packets.)
  TcpAcceptedFuncPtr                 [User]_TcpAccepted Callback Function. (only required if socker passively accepts TCP connections.)
  TcpConnectedFuncPtr                [User]_TcpConnected Callback Function. (only required if socker owner actively opens TCP connections.)
  TcpIpEventFuncPtr                  [User]_TcpIpEvent Callback Function. (required for every socket owner)
  TlsValidationResultFuncPtr         [User]_TlsValidationResult Callback Function.
  TxConfirmationFuncPtr              [User]_TxConfirmation Callback Function. (optional for every socket owner)
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_SocketOwnerConfigType, TCPIP_CONST) TcpIp_SocketOwnerConfig[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    CopyTxDataDynFuncPtr  CopyTxDataFuncPtr  DhcpEventFuncPtr  LocalIpAddrAssignmentChgFuncPtr     RxIndicationFuncPtr      TcpAcceptedFuncPtr  TcpConnectedFuncPtr      TcpIpEventFuncPtr    TlsValidationResultFuncPtr  TxConfirmationFuncPtr */
  { /*     0 */ Scc_Cbk_CopyTxData  , NULL_PTR         , NULL_PTR        , Scc_Cbk_IP_AddressAssignmentChange, Scc_Cbk_TL_RxIndication, NULL_PTR          , Scc_Cbk_TL_TCPConnected, Scc_Cbk_TL_TCPEvent, Scc_Cbk_TLS_CertChain     , NULL_PTR              }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpConfig
  \details
  Element                    Description
  KeepAliveTime              Keep Alive Time - Value of configuration parameter TcpIpTcpKeepAliveTime [MAIN_FUNCTION_CYCLES]
  FinWait2Timeout            FIN Wait 2 Timeout - Value of configuration parameter TcpIpTcpFinWait2Timeout [MAIN_FUNCTION_CYCLES]
  Msl                        Maximum Segment Lifetime (MSL) - Value of configuration parameter TcpIpTcpMsl [MAIN_FUNCTION_CYCLES]
  RetransTimeoutMax          Retransmission Timeout Max - Value of configuration parameter TcpIpTcpRetransmissionTimeoutMax [MAIN_FUNCTION_CYCLES]
  RxMss                      maximum payload size of received TCP segments.
  TxMss                      maximum payload size of transmitted TCP segments.
  UserTimeoutDefCycles       User Timeout Def - Value of configuration parameter TcpIpTcpUserTimeoutDef [MAIN_FUNCTION_CYCLES]
  KeepAliveInterval          Keep Alive Interval - Value of configuration parameter TcpIpTcpKeepAliveInterval [MAIN_FUNCTION_CYCLES]
  KeepAliveProbesMax         Keep Alive Probes Max - Value of configuration parameter TcpIpTcpKeepAliveProbesMax
  NagleTimeout               Nagle Timeout - Value of configuration parameter TcpIpTcpNagleTimeout [MAIN_FUNCTION_CYCLES]
  RetransTimeout             Retransmission Timeout - Value of configuration parameter TcpIpTcpRetransmissionTimeout [MAIN_FUNCTION_CYCLES]
  TcpOooQSizePerSocketAvg    Avg Num Out Of Order Segments per Socket - Value of configuration parameter TcpIpTcpAvgNumOooSegsPerSocket
  TcpOooQSizePerSocketMax    Max Num Out Of Order Segments per Socket - Value of configuration parameter TcpIpTcpMaxNumOooSegsPerSocket
  TcpRetryQSize              Average Tx Retry Queue Size - Value of configuration parameter TcpIpTcpTxRetryQueueSize
  TimeToLiveDefault          Time-To-Live (TTL) - Value of configuration parameter TcpIpTcpTtl
  UserTimeoutMaxCycles       User Timeout Max - Value of configuration parameter TcpIpTcpUserTimeoutMax [MAIN_FUNCTION_CYCLES]
  UserTimeoutMinCycles       User Timeout Min - Value of configuration parameter TcpIpTcpUserTimeoutMin [MAIN_FUNCTION_CYCLES]
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_TcpConfigType, TCPIP_CONST) TcpIp_TcpConfig[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    KeepAliveTime  FinWait2Timeout  Msl     RetransTimeoutMax  RxMss  TxMss  UserTimeoutDefCycles  KeepAliveInterval  KeepAliveProbesMax  NagleTimeout                      RetransTimeout  TcpOooQSizePerSocketAvg  TcpOooQSizePerSocketMax  TcpRetryQSize  TimeToLiveDefault  UserTimeoutMaxCycles                      UserTimeoutMinCycles                     */
  { /*     0 */      1440000u,          24000u, 24000u,            12000u, 1460u, 1460u,                4000u,                0u,                 0u, TCPIP_NO_NAGLETIMEOUTOFTCPCONFIG,            20u,                      0u,                      0u,           10u,               64u, TCPIP_NO_USERTIMEOUTMAXCYCLESOFTCPCONFIG, TCPIP_NO_USERTIMEOUTMINCYCLESOFTCPCONFIG }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRxBufferDesc
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpRxBufferDesc
  \brief  TCP RX buffer descriptors
  \details
  Element                Description
  TcpRxBufferEndIdx      the end index of the 1:n relation pointing to TcpIp_TcpRxBuffer
  TcpRxBufferLength      the number of relations pointing to TcpIp_TcpRxBuffer
  TcpRxBufferStartIdx    the start index of the 1:n relation pointing to TcpIp_TcpRxBuffer
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_TcpRxBufferDescType, TCPIP_CONST) TcpIp_TcpRxBufferDesc[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TcpRxBufferEndIdx  TcpRxBufferLength  TcpRxBufferStartIdx */
  { /*     0 */             4000u,             4000u,                  0u }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpTxBufferDesc
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpTxBufferDesc
  \brief  TCP TX buffer descriptors
  \details
  Element                Description
  TcpTxBufferEndIdx      the end index of the 1:n relation pointing to TcpIp_TcpTxBuffer
  TcpTxBufferLength      the number of relations pointing to TcpIp_TcpTxBuffer
  TcpTxBufferStartIdx    the start index of the 1:n relation pointing to TcpIp_TcpTxBuffer
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_TcpTxBufferDescType, TCPIP_CONST) TcpIp_TcpTxBufferDesc[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TcpTxBufferEndIdx  TcpTxBufferLength  TcpTxBufferStartIdx */
  { /*     0 */              500u,              500u,                  0u }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TxReqElemList
**********************************************************************************************************************/
/** 
  \var    TcpIp_TxReqElemList
  \brief  UDP TX request list configuration
  \details
  Element              Description
  TxReqElemEndIdx      the end index of the 1:n relation pointing to TcpIp_TxReqElem
  TxReqElemLength      the number of relations pointing to TcpIp_TxReqElem
  TxReqElemStartIdx    the start index of the 1:n relation pointing to TcpIp_TxReqElem
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_TxReqElemListType, TCPIP_CONST) TcpIp_TxReqElemList[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxReqElemEndIdx  TxReqElemLength  TxReqElemStartIdx */
  { /*     0 */              1u,              1u,                0u }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IcmpV6TxMsgBuffer
**********************************************************************************************************************/
/** 
  \var    TcpIp_IcmpV6TxMsgBuffer
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IcmpV6TxMsgBufferType, TCPIP_VAR_NOINIT) TcpIp_IcmpV6TxMsgBuffer[1280];  /* PRQA S 1514, 1533, 0612 */  /* MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_BigStructure */
#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_InterfaceIdentifier
**********************************************************************************************************************/
/** 
  \var    TcpIp_InterfaceIdentifier
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_InterfaceIdentifierUType, TCPIP_VAR_NOINIT) TcpIp_InterfaceIdentifier;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*   ... */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*    63 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6CtrlDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6CtrlDyn
  \brief  -
  \details
  Element                                  Description
  BaseReachableTimeMs                      Time a neighbor is considered to be reachable.
  ReachableTimeMs                          Time a neighbor is considered to be reachable.
  RetransTimerMs                           Interval between retransmissions.
  DefaultLinkMtu                           -
  CtrlPreviousState                        -
  CtrlState                                -
  CurHopLimit                              Hop Limit for outgoing packets, may be changed by received RAs.
  IpV6DefaultRouterListEntryValidEndIdx    the index of the 1:1 relation pointing to TcpIp_IpV6DefaultRouterListEntry
  IpV6DestinationCacheEntryValidEndIdx     the index of the 1:1 relation pointing to TcpIp_IpV6DestinationCacheEntry
  IpV6NeighborCacheEntryValidEndIdx        the index of the 1:1 relation pointing to TcpIp_IpV6NeighborCacheEntry
  IpV6PrefixListEntryValidEndIdx           the index of the 1:1 relation pointing to TcpIp_IpV6PrefixListEntry
  Ndp_RouterSolicitationTxCount            -
  LastBcAddrPtr                            -
  Ndp_PendingDadNa                         -
  Ndp_RouterSolicitationNextTxTime         -
  NextRouterProbeIdx                       -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6CtrlDynUType, TCPIP_VAR_NOINIT) TcpIp_IpV6CtrlDyn;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6DefaultRouterListEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6DefaultRouterListEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6DefaultRouterListEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6DefaultRouterListEntry;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*     1 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6DestinationCacheEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6DestinationCacheEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6DestinationCacheEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6DestinationCacheEntry;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*   ... */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*     4 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6EthBufData
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6EthBufData
  \brief  -
  \details
  Element             Description
  IpV6SocketDynIdx    the index of the 0:1 relation pointing to TcpIp_IpV6SocketDyn
  UlTxReqTabIdx       Store which socket index belongs to the BufIdx of CtrlIdx.
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6EthBufDataUType, TCPIP_VAR_NOINIT) TcpIp_IpV6EthBufData;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Eth/EthConfigSet/EthCtrlConfig] */
  /*     1 */  /* [/ActiveEcuC/Eth/EthConfigSet/EthCtrlConfig] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6NeighborCacheEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6NeighborCacheEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6NeighborCacheEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6NeighborCacheEntry;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*   ... */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*     4 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6PrefixListEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6PrefixListEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6PrefixListEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6PrefixListEntry;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*   ... */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */
  /*     4 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6SocketDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6SocketDyn
  \brief  -
  \details
  Element                             Description
  IpV6HdrVersionTcFlNbo               -
  EthIfFramePrio                      -
  Flags                               -
  HopLimit                            -
  IpV6DestinationCacheEntryHintIdx    the index of the 0:1 relation pointing to TcpIp_IpV6DestinationCacheEntry
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_IpV6SocketDynType, TCPIP_VAR_NOINIT) TcpIp_IpV6SocketDyn[5];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [UDP] */
  /*     1 */  /* [TCP] */
  /*     2 */  /* [TCP-RST] */
  /*     3 */  /* [ICMPv6] */
  /*     4 */  /* [NDP] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6SourceAddressTableEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6SourceAddressTableEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(IpV6_SourceAddressTableEntryType, TCPIP_VAR_NOINIT) TcpIp_IpV6SourceAddressTableEntry[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpV6LocalAddr_TcpIpCtrl_LinkLocal, /ActiveEcuC/TcpIp/TcpIpConfig/TcpIpCtrl/TcpIpIpVXCtrl/TcpIpIpV6Ctrl] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_LocalAddr
**********************************************************************************************************************/
/** 
  \var    TcpIp_LocalAddr
  \details
  Element            Description
  AssignmentState    -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_LocalAddrUType, TCPIP_VAR_NOINIT) TcpIp_LocalAddr;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpV6LocalAddr_TcpIpCtrl_LinkLocal] */
  /*     1 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpV6LocalAddr_TcpIpCtrl_AsAn] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketDyn
  \brief  Generic (TCP/UDP) socket variables
  \details
  Element                 Description
  ListenActiveConnStat    Current state of the socket
  LocalAddrBindIdx        the index of the 0:1 relation pointing to TcpIp_LocalAddr
  SocketOwnerConfigIdx    the index of the 0:1 relation pointing to TcpIp_SocketOwnerConfig
  TxBufRequested          TX buffer request semaphore
  TxIpAddrIdx             Index of local IPxV address that is used for transmission
  LocSock                 IP address and port of the local host
  RemSock                 IP address and port of the remote host
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_SocketDynUType, TCPIP_VAR_NOINIT) TcpIp_SocketDyn;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [UDP] */
  /*     1 */  /* [TCP] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketTcpDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketTcpDyn
  \brief  TCP socket specific variables
  \details
  Element                              Description
  FinWait2Timeout                      -
  IdleTimeoutShort                     -
  Iss                                  -
  MslTimeout                           -
  RcvNxt                               -
  RetransmitTimeout                    -
  RtoReloadValue                       -
  SndNxt                               -
  SndUna                               -
  SndWl1                               -
  SndWl2                               -
  TxNextSendSeqNo                      -
  TxReqSeqNo                           Sequence number of first pending TX request.
  MaxNumListenSockets                  -
  PathMtuNewSize                       -
  RcvWnd                               -
  SndWnd                               -
  TxMaxSegLenByte                      -
  TxMssAgreed                          Maximum segment size agreed during connection setup
  EventIndicationPending               Varibale contains the pending events that have to be forwarded to the socket user
  PathMtuChanged                       -
  RetryQFillNum                        -
  RstReceived                          -
  SockIsServer                         -
  SockState                            -
  SockStateNext                        -
  SocketTcpDynMasterListenSocketIdx    the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn
  TcpRetryQElementFirstIdx             the index of the 0:1 relation pointing to TcpIp_TcpRetryQElement
  TcpRetryQElementLastIdx              the index of the 0:1 relation pointing to TcpIp_TcpRetryQElement
  TcpRxBufferDescIdx                   the index of the 0:1 relation pointing to TcpIp_TcpRxBufferDesc
  TcpTxBufferDescIdx                   the index of the 0:1 relation pointing to TcpIp_TcpTxBufferDesc
  TxFlags                              -
  TxOneTimeOpts                        -
  TxOneTimeOptsLen                     -
  TxOptLen                             -
  TxReqFullyQueued                     all unqueued data from this request is used within the next segment
  BackLogArray                         -
  RequestedRxBufferSize                -
  RequestedTxBufferSize                -
  RxBufferIndPos                       -
  RxBufferRemIndLen                    -
  TxLenByteTx                          -
  TxReqDataBufStartIdx                 byte position inside the TxBuffer
  TxReqDataLenByte                     length of data to be transmitted
  TxReqQueuedLen                       length of data that could already be queued for transmission
  TxTotNotQueuedLen                    -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_SocketTcpDynType, TCPIP_VAR_NOINIT) TcpIp_SocketTcpDyn[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketUdpDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketUdpDyn
  \brief  UDP socket specific variables
  \details
  Element                  Description
  TxReqElemListIdx         the index of the 0:1 relation pointing to TcpIp_TxReqElemList
  TxRetrQueueMaxNum        Maximum number of TX retry queue elements for the socket.
  IpTxRequestDescriptor    TX request descriptor required for the IP layer
  TxReqIpBufPtr            Pointer to the provided TX buffer of the IP layer
  TxRetrQueue              TX retry queue
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_SocketUdpDynType, TCPIP_VAR_NOINIT) TcpIp_SocketUdpDyn[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpResetQElement
**********************************************************************************************************************/
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_Tcp_RstTxQueueType, TCPIP_VAR_NOINIT) TcpIp_TcpResetQElement[8];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRetryQElement
**********************************************************************************************************************/
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_Tcp_TxRetrQueueType, TCPIP_VAR_NOINIT) TcpIp_TcpRetryQElement[10];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRxBuffer
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpRxBuffer
  \brief  TCP TX buffers
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_TcpRxBufferUType, TCPIP_VAR_NOINIT) TcpIp_TcpRxBuffer;  /* PRQA S 0759, 1514, 1533, 0612 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_BigStructure */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpTcpConfig/TcpIpTcpSocketBuffer] */
  /*   ... */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpTcpConfig/TcpIpTcpSocketBuffer] */
  /*  3999 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpTcpConfig/TcpIpTcpSocketBuffer] */

#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRxBufferDescDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpRxBufferDescDyn
  \details
  Element                Description
  TcpRxBufferWriteIdx    the index of the 1:1 relation pointing to TcpIp_TcpRxBuffer
  SocketTcpDynIdx        the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn
  FillLevel          
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_TcpRxBufferDescDynType, TCPIP_VAR_NOINIT) TcpIp_TcpRxBufferDescDyn[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpTxBuffer
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpTxBuffer
  \brief  TCP TX buffers
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_TcpTxBufferUType, TCPIP_VAR_NOINIT) TcpIp_TcpTxBuffer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpTcpConfig/TcpIpTcpSocketBuffer] */
  /*   ... */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpTcpConfig/TcpIpTcpSocketBuffer] */
  /*   499 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpTcpConfig/TcpIpTcpSocketBuffer] */

#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpTxBufferDescDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpTxBufferDescDyn
  \details
  Element                Description
  TcpTxBufferWriteIdx    the index of the 1:1 relation pointing to TcpIp_TcpTxBuffer
  SocketTcpDynIdx        the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn
  FillLevel          
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_TcpTxBufferDescDynType, TCPIP_VAR_NOINIT) TcpIp_TcpTxBufferDescDyn[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TxReqElem
**********************************************************************************************************************/
/** 
  \var    TcpIp_TxReqElem
  \brief  UDP TX request list elements
  \details
  Element                 Description
  TxReqElemDataLenByte    -
  TxReqElemTransmitted    -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_TxReqElemUType, TCPIP_VAR_NOINIT) TcpIp_TxReqElem;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/TcpIp/TcpIpConfig/TcpIpUdpConfig/TcpIpUdpTxReqList_Scc] */

#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TxReqElemListDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_TxReqElemListDyn
  \brief  UDP TX request list variables
  \details
  Element            Description
  FillNum            Number of elements currently stored in TX request ring buffer
  ReadPos            Read position of TX request ring buffer
  SocketUdpDynIdx    the index of the 0:1 relation pointing to TcpIp_SocketUdpDyn
  WritePos           Write position in TX request ring buffer
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_TxReqElemListDynType, TCPIP_VAR_NOINIT) TcpIp_TxReqElemListDyn[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_UdpTxRetryQueueElementChain
**********************************************************************************************************************/
/** 
  \var    TcpIp_UdpTxRetryQueueElementChain
  \brief  UDP TX retry queue element chain
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_DListNodeType, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueueElementChain[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_UdpTxRetryQueueElements
**********************************************************************************************************************/
/** 
  \var    TcpIp_UdpTxRetryQueueElements
  \brief  UDP TX retry queue elements
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_Udp_RetryQueueElementType, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueueElements[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_UdpTxRetryQueuePoolDesc
**********************************************************************************************************************/
/** 
  \var    TcpIp_UdpTxRetryQueuePoolDesc
  \brief  UDP TX retry queue element pool
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(TcpIp_DListDescType, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueuePoolDesc[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_PCConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_PCConfig
  \details
  Element                              Description
  DefaultAddrV6                        the pointer to TcpIp_DefaultAddrV6
  DhcpUserOption                       the pointer to TcpIp_DhcpUserOption
  DhcpUserOptionBuffer                 the pointer to TcpIp_DhcpUserOptionBuffer
  DhcpUserOptionDyn                    the pointer to TcpIp_DhcpUserOptionDyn
  IpV6MulticastAddr                    the pointer to TcpIp_IpV6MulticastAddr
  IpV6MulticastAddrActive              the pointer to TcpIp_IpV6MulticastAddrActive
  PhysAddrConfig                       the pointer to TcpIp_PhysAddrConfig
  TcpOooQElement                       the pointer to TcpIp_TcpOooQElement
  TxReqElem                            the pointer to TcpIp_TxReqElem
  TxReqElemList                        the pointer to TcpIp_TxReqElemList
  TxReqElemListDyn                     the pointer to TcpIp_TxReqElemListDyn
  UdpTxRetryQueueElementChain          the pointer to TcpIp_UdpTxRetryQueueElementChain
  UdpTxRetryQueueElements              the pointer to TcpIp_UdpTxRetryQueueElements
  UdpTxRetryQueuePoolDesc              the pointer to TcpIp_UdpTxRetryQueuePoolDesc
  SizeOfDefaultAddrV6                  the number of accomplishable value elements in TcpIp_DefaultAddrV6
  SizeOfDhcpUserOption                 the number of accomplishable value elements in TcpIp_DhcpUserOption
  SizeOfDhcpUserOptionBuffer           the number of accomplishable value elements in TcpIp_DhcpUserOptionBuffer
  SizeOfIpV6MulticastAddr              the number of accomplishable value elements in TcpIp_IpV6MulticastAddr
  SizeOfIpV6MulticastAddrActive        the number of accomplishable value elements in TcpIp_IpV6MulticastAddrActive
  SizeOfPhysAddrConfig                 the number of accomplishable value elements in TcpIp_PhysAddrConfig
  SizeOfTcpOooQElement                 the number of accomplishable value elements in TcpIp_TcpOooQElement
  SizeOfTxReqElem                      the number of accomplishable value elements in TcpIp_TxReqElem
  SizeOfTxReqElemList                  the number of accomplishable value elements in TcpIp_TxReqElemList
  SizeOfUdpTxRetryQueueElementChain    the number of accomplishable value elements in TcpIp_UdpTxRetryQueueElementChain
  SizeOfUdpTxRetryQueueElements        the number of accomplishable value elements in TcpIp_UdpTxRetryQueueElements
  SizeOfUdpTxRetryQueuePoolDesc        the number of accomplishable value elements in TcpIp_UdpTxRetryQueuePoolDesc
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(TcpIp_PCConfigsType, TCPIP_CONST) TcpIp_PCConfig = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  { /* Index: 0 Keys: [Config] */
      NULL_PTR                           /**< the pointer to TcpIp_DefaultAddrV6 */
    , NULL_PTR                           /**< the pointer to TcpIp_DhcpUserOption */
    , NULL_PTR                           /**< the pointer to TcpIp_DhcpUserOptionBuffer */
    , NULL_PTR                           /**< the pointer to TcpIp_DhcpUserOptionDyn */
    , NULL_PTR                           /**< the pointer to TcpIp_IpV6MulticastAddr */
    , NULL_PTR                           /**< the pointer to TcpIp_IpV6MulticastAddrActive */
    , NULL_PTR                           /**< the pointer to TcpIp_PhysAddrConfig */
    , NULL_PTR                           /**< the pointer to TcpIp_TcpOooQElement */
    , TcpIp_TxReqElem.raw                /**< the pointer to TcpIp_TxReqElem */
    , TcpIp_TxReqElemList                /**< the pointer to TcpIp_TxReqElemList */
    , TcpIp_TxReqElemListDyn             /**< the pointer to TcpIp_TxReqElemListDyn */
    , TcpIp_UdpTxRetryQueueElementChain  /**< the pointer to TcpIp_UdpTxRetryQueueElementChain */
    , TcpIp_UdpTxRetryQueueElements      /**< the pointer to TcpIp_UdpTxRetryQueueElements */
    , TcpIp_UdpTxRetryQueuePoolDesc      /**< the pointer to TcpIp_UdpTxRetryQueuePoolDesc */
    , 0u                                 /**< the number of elements in TcpIp_DefaultAddrV6 */
    , 0u                                 /**< the number of elements in TcpIp_DhcpUserOption */
    , 0u                                 /**< the number of elements in TcpIp_DhcpUserOptionBuffer */
    , 0u                                 /**< the number of elements in TcpIp_IpV6MulticastAddr */
    , 0u                                 /**< the number of elements in TcpIp_IpV6MulticastAddrActive */
    , 0u                                 /**< the number of elements in TcpIp_PhysAddrConfig */
    , 0u                                 /**< the number of elements in TcpIp_TcpOooQElement */
    , 1u                                 /**< the number of elements in TcpIp_TxReqElem */
    , 1u                                 /**< the number of elements in TcpIp_TxReqElemList */
    , 1u                                 /**< the number of elements in TcpIp_UdpTxRetryQueueElementChain */
    , 1u                                 /**< the number of elements in TcpIp_UdpTxRetryQueueElements */
    , 1u                                 /**< the number of elements in TcpIp_UdpTxRetryQueuePoolDesc */
  }
};
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/* PRQA L:OBJECT_SIZE */

#define TCPIP_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/**********************************************************************************************************************
 *  USER FUNCTIONS
 *********************************************************************************************************************/
/* TcpIp_<Up>GetSocket ----------------------------------------------------- */
FUNC(Std_ReturnType, TCPIP_CODE) TcpIp_SccGetSocket(
    TcpIp_DomainType                                      Domain,
    TcpIp_ProtocolType                                    Protocol,
    P2VAR(TcpIp_SocketIdType, AUTOMATIC, TCPIP_APPL_DATA) SocketIdPtr)
{
    return TcpIp_GetSocketForUser(Domain, Protocol, SocketIdPtr, TcpIpConf_TcpIpSocketOwner_Scc);  /* SBSW_TCPIP_NonNullPtrParameter */
}
 

#define TCPIP_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/* PRQA L:MACRO_LIMIT */

/* Justification for module-specific MISRA deviations:

 MD_TEST_Rule5.5_Wrapper: Identifier '%s' is also used as a macro name.
 Reason:     Redefinition allows execution of foreign code.
 Risk:       Code is more difficult to read and to debug.
 Prevention: Code is only present in test environment and not in productive builds.

 MD_CSL_Rule10.3_OldValidLib: The value of an expression shall not be assigned to an object with a narrower essential type or of a different essential type category.
 Reason:     Other Autosar modules generates narrow datatypes for the symbolic name values.
 Risk:       None.
 Prevention: Other modules need to change their SNV generation
*/
/**********************************************************************************************************************
 *  END OF FILE: TcpIp_Lcfg.c
 *********************************************************************************************************************/
