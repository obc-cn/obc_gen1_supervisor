/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Types_Lcfg.h
 *   Generation Time: 2020-08-19 13:07:50
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#ifndef OS_TYPES_LCFG_H
# define OS_TYPES_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
# include "Std_Types.h"

/* Os module declarations */

/* Os kernel module dependencies */

/* Os hal dependencies */

/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
 *********************************************************************************************************************/

/* OS-Application identifiers. */
#define OsApplication_ASILB OsApplication_ASILB
#define OsApplication_BSW_QM OsApplication_BSW_QM
#define OsApplication_QM OsApplication_QM
#define SystemApplication_OsCore0 SystemApplication_OsCore0

/* Trusted function identifiers. */

/* Non-trusted function identifiers. */

/* Fast trusted function identifiers. */

/* Task identifiers. */
#define ASILB_MAIN_TASK ASILB_MAIN_TASK
#define ASILB_init_task ASILB_init_task
#define Default_BSW_Async_QM_Task Default_BSW_Async_QM_Task
#define Default_BSW_Async_Task Default_BSW_Async_Task
#define Default_BSW_Sync_Task Default_BSW_Sync_Task
#define Default_Init_Task Default_Init_Task
#define Default_RTE_Mode_switch_Task Default_RTE_Mode_switch_Task
#define IdleTask_OsCore0 IdleTask_OsCore0
#define QM_MAIN_TASK QM_MAIN_TASK
#define QM_init_task QM_init_task

/* Category 2 ISR identifiers. */
#define CanIsr_1 CanIsr_1
#define CanIsr_2 CanIsr_2
#define CounterIsr_SystemTimer CounterIsr_SystemTimer
#define DMACH10SR_ISR DMACH10SR_ISR
#define DMACH11SR_ISR DMACH11SR_ISR
#define GTMTOM1SR0_ISR GTMTOM1SR0_ISR
#define PLC_Interrupt PLC_Interrupt
#define QSPI1ERR_ISR QSPI1ERR_ISR
#define QSPI1PT_ISR QSPI1PT_ISR
#define QSPI1UD_ISR QSPI1UD_ISR

/* Alarm identifiers. */
#define Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms
#define Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms
#define Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms
#define Rte_Al_TE2_Default_BSW_Async_Task_7_10ms Rte_Al_TE2_Default_BSW_Async_Task_7_10ms
#define Rte_Al_TE_ASILB_MAIN_TASK_1_100ms Rte_Al_TE_ASILB_MAIN_TASK_1_100ms
#define Rte_Al_TE_ASILB_MAIN_TASK_1_10ms Rte_Al_TE_ASILB_MAIN_TASK_1_10ms
#define Rte_Al_TE_ASILB_MAIN_TASK_2_10ms Rte_Al_TE_ASILB_MAIN_TASK_2_10ms
#define Rte_Al_TE_ASILB_MAIN_TASK_3_10ms Rte_Al_TE_ASILB_MAIN_TASK_3_10ms
#define Rte_Al_TE_ASILB_MAIN_TASK_4_10ms Rte_Al_TE_ASILB_MAIN_TASK_4_10ms
#define Rte_Al_TE_CpApAEM_RCtApAEM_task10msA Rte_Al_TE_CpApAEM_RCtApAEM_task10msA
#define Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX
#define Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX
#define Rte_Al_TE_QM_MAIN_TASK_2_100ms Rte_Al_TE_QM_MAIN_TASK_2_100ms
#define Rte_Al_TE_QM_MAIN_TASK_2_10ms Rte_Al_TE_QM_MAIN_TASK_2_10ms
#define Rte_Al_TE_QM_MAIN_TASK_3_100ms Rte_Al_TE_QM_MAIN_TASK_3_100ms
#define Rte_Al_TE_QM_MAIN_TASK_3_10ms Rte_Al_TE_QM_MAIN_TASK_3_10ms

/* Counter identifiers. */
#define SystemTimer SystemTimer

/* ScheduleTable identifiers. */

/* Resource identifiers. */
#define OsResource OsResource

/* Spinlock identifiers. */

/* Peripheral identifiers. */

/* Barrier identifiers. */

/* Trace thread identifiers (Tasks and ISRs inclusive system objects). */
#define Os_TraceId_ASILB_MAIN_TASK Os_TraceId_ASILB_MAIN_TASK
#define Os_TraceId_ASILB_init_task Os_TraceId_ASILB_init_task
#define Os_TraceId_Default_BSW_Async_QM_Task Os_TraceId_Default_BSW_Async_QM_Task
#define Os_TraceId_Default_BSW_Async_Task Os_TraceId_Default_BSW_Async_Task
#define Os_TraceId_Default_BSW_Sync_Task Os_TraceId_Default_BSW_Sync_Task
#define Os_TraceId_Default_Init_Task Os_TraceId_Default_Init_Task
#define Os_TraceId_Default_RTE_Mode_switch_Task Os_TraceId_Default_RTE_Mode_switch_Task
#define Os_TraceId_IdleTask_OsCore0 Os_TraceId_IdleTask_OsCore0
#define Os_TraceId_QM_MAIN_TASK Os_TraceId_QM_MAIN_TASK
#define Os_TraceId_QM_init_task Os_TraceId_QM_init_task
#define Os_TraceId_CanIsr_1 Os_TraceId_CanIsr_1
#define Os_TraceId_CanIsr_2 Os_TraceId_CanIsr_2
#define Os_TraceId_CounterIsr_SystemTimer Os_TraceId_CounterIsr_SystemTimer
#define Os_TraceId_DMACH10SR_ISR Os_TraceId_DMACH10SR_ISR
#define Os_TraceId_DMACH11SR_ISR Os_TraceId_DMACH11SR_ISR
#define Os_TraceId_GTMTOM1SR0_ISR Os_TraceId_GTMTOM1SR0_ISR
#define Os_TraceId_PLC_Interrupt Os_TraceId_PLC_Interrupt
#define Os_TraceId_QSPI1ERR_ISR Os_TraceId_QSPI1ERR_ISR
#define Os_TraceId_QSPI1PT_ISR Os_TraceId_QSPI1PT_ISR
#define Os_TraceId_QSPI1UD_ISR Os_TraceId_QSPI1UD_ISR

/* Trace spinlock identifiers (All spinlocks inclusive system objects). */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/*! OS-Application identifiers. */
typedef enum
{
  OsApplication_ASILB = 0, /* 0x00000001 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OsApplication_BSW_QM = 1, /* 0x00000002 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OsApplication_QM = 2, /* 0x00000004 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  SystemApplication_OsCore0 = 3, /* 0x00000008 */  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_APPID_COUNT = 4,
  INVALID_OSAPPLICATION = OS_APPID_COUNT
} ApplicationType;

/*! Trusted function identifiers. */
typedef enum
{
  OS_TRUSTEDFUNCTIONID_COUNT = 0
} TrustedFunctionIndexType;

/*! Non-trusted function identifiers. */
typedef enum
{
  OS_NONTRUSTEDFUNCTIONID_COUNT = 0
} Os_NonTrustedFunctionIndexType;

/*! Fast trusted function identifiers. */
typedef enum
{
  OS_FASTTRUSTEDFUNCTIONID_COUNT = 0
} Os_FastTrustedFunctionIndexType;

/*! Task identifiers. */
typedef enum
{
  ASILB_MAIN_TASK = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  ASILB_init_task = 1,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Default_BSW_Async_QM_Task = 2,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Default_BSW_Async_Task = 3,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Default_BSW_Sync_Task = 4,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Default_Init_Task = 5,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Default_RTE_Mode_switch_Task = 6,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  IdleTask_OsCore0 = 7,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  QM_MAIN_TASK = 8,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  QM_init_task = 9,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_TASKID_COUNT = 10,
  INVALID_TASK = OS_TASKID_COUNT
} TaskType;

/*! Category 2 ISR identifiers. */
typedef enum
{
  CanIsr_1 = 0,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CanIsr_2 = 1,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  CounterIsr_SystemTimer = 2,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DMACH10SR_ISR = 3,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  DMACH11SR_ISR = 4,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  GTMTOM1SR0_ISR = 5,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  PLC_Interrupt = 6,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  QSPI1ERR_ISR = 7,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  QSPI1PT_ISR = 8,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  QSPI1UD_ISR = 9,   /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_ISRID_COUNT = 10,
  INVALID_ISR = OS_ISRID_COUNT
} ISRType;

/*! Alarm identifiers. */
typedef enum
{
  Rte_Al_TE2_Default_BSW_Async_QM_Task_1_5ms = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE2_Default_BSW_Async_QM_Task_7_10ms = 1,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE2_Default_BSW_Async_QM_Task_8_20ms = 2,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE2_Default_BSW_Async_Task_7_10ms = 3,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASILB_MAIN_TASK_1_100ms = 4,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASILB_MAIN_TASK_1_10ms = 5,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASILB_MAIN_TASK_2_10ms = 6,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASILB_MAIN_TASK_3_10ms = 7,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_ASILB_MAIN_TASK_4_10ms = 8,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_CpApAEM_RCtApAEM_task10msA = 9,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msRX = 10,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_CpApPCOM_RCtApPCOM_task5msTX = 11,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_QM_MAIN_TASK_2_100ms = 12,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_QM_MAIN_TASK_2_10ms = 13,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_QM_MAIN_TASK_3_100ms = 14,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  Rte_Al_TE_QM_MAIN_TASK_3_10ms = 15,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_ALARMID_COUNT = 16
} AlarmType;

/*! Counter identifiers. */
typedef enum
{
  SystemTimer = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_COUNTERID_COUNT = 1
} CounterType;

/*! ScheduleTable identifiers. */
typedef enum
{
  OS_SCHTID_COUNT = 0
} ScheduleTableType;

/*! Resource identifiers. */
typedef enum
{
  OsResource = 0,  /* PRQA S 0784 */ /* MD_Os_Rule5.5_0784 */
  OS_RESOURCEID_COUNT = 1
} ResourceType;

/*! Spinlock identifiers. */
typedef enum
{
  OS_SPINLOCKID_COUNT = 0,
  INVALID_SPINLOCK = OS_SPINLOCKID_COUNT
} SpinlockIdType;

/*! Peripheral identifiers. */
typedef enum
{
  OS_PERIPHERALID_COUNT = 0
} Os_PeripheralIdType;

/*! Barrier identifiers. */
typedef enum
{
  OS_BARRIERID_COUNT = 0
} Os_BarrierIdType;

/*! Trace thread identifiers (Tasks and ISRs inclusive system objects). */
typedef enum
{
  Os_TraceId_ASILB_MAIN_TASK = 0,
  Os_TraceId_ASILB_init_task = 1,
  Os_TraceId_Default_BSW_Async_QM_Task = 2,
  Os_TraceId_Default_BSW_Async_Task = 3,
  Os_TraceId_Default_BSW_Sync_Task = 4,
  Os_TraceId_Default_Init_Task = 5,
  Os_TraceId_Default_RTE_Mode_switch_Task = 6,
  Os_TraceId_IdleTask_OsCore0 = 7,
  Os_TraceId_QM_MAIN_TASK = 8,
  Os_TraceId_QM_init_task = 9,
  Os_TraceId_CanIsr_1 = 10,
  Os_TraceId_CanIsr_2 = 11,
  Os_TraceId_CounterIsr_SystemTimer = 12,
  Os_TraceId_DMACH10SR_ISR = 13,
  Os_TraceId_DMACH11SR_ISR = 14,
  Os_TraceId_GTMTOM1SR0_ISR = 15,
  Os_TraceId_PLC_Interrupt = 16,
  Os_TraceId_QSPI1ERR_ISR = 17,
  Os_TraceId_QSPI1PT_ISR = 18,
  Os_TraceId_QSPI1UD_ISR = 19,
  OS_TRACE_THREADID_COUNT = 20,
  OS_TRACE_INVALID_THREAD = OS_TRACE_THREADID_COUNT + 1
} Os_TraceThreadIdType;

/*! Trace spinlock identifiers (All spinlocks inclusive system objects). */
typedef enum
{
  OS_TRACE_NUMBER_OF_CONFIGURED_SPINLOCKS = OS_SPINLOCKID_COUNT,
  OS_TRACE_NUMBER_OF_ALL_SPINLOCKS = OS_SPINLOCKID_COUNT + 0u,  /* PRQA S 4521 */ /* MD_Os_Rule10.1_4521 */
  OS_TRACE_INVALID_SPINLOCK = OS_TRACE_NUMBER_OF_ALL_SPINLOCKS + 1
} Os_TraceSpinlockIdType;

/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/


#endif /* OS_TYPES_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Types_Lcfg.h
 *********************************************************************************************************************/
