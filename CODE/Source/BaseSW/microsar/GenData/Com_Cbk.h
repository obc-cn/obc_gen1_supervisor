/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Com
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Com_Cbk.h
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


#if !defined (COM_CBK_H)
# define COM_CBK_H

/**********************************************************************************************************************
  MISRA / PClint JUSTIFICATIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#include "Com_Cot.h"

/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/



/**
 * \defgroup ComHandleIdsComRxPdu Handle IDs of handle space ComRxPdu.
 * \brief Rx Pdus
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define ComConf_ComIPdu_BMS1_oE_CAN_1fa2c419_Rx                       0u
#define ComConf_ComIPdu_BMS3_oE_CAN_5d87c364_Rx                       1u
#define ComConf_ComIPdu_BMS5_oE_CAN_9be8cae3_Rx                       2u
#define ComConf_ComIPdu_BMS6_oE_CAN_1567cd00_Rx                       3u
#define ComConf_ComIPdu_BMS8_oE_CAN_00eddf32_Rx                       4u
#define ComConf_ComIPdu_BMS9_oE_CAN_cc47dfac_Rx                       5u
#define ComConf_ComIPdu_BSIInfo_oE_CAN_b362dec3_Rx                    6u
#define ComConf_ComIPdu_CtrlDCDC_oE_CAN_6ce88e20_Rx                   7u
#define ComConf_ComIPdu_DCHV_Fault_oInt_CAN_d7c47a53_Rx               8u
#define ComConf_ComIPdu_DCHV_Status1_oInt_CAN_6c1b5ec3_Rx             9u
#define ComConf_ComIPdu_DCHV_Status2_oInt_CAN_872ce5c0_Rx             10u
#define ComConf_ComIPdu_DCLV_Fault_oInt_CAN_a4cc5d9c_Rx               11u
#define ComConf_ComIPdu_DCLV_Status1_oInt_CAN_85735321_Rx             12u
#define ComConf_ComIPdu_DCLV_Status2_oInt_CAN_6e44e822_Rx             13u
#define ComConf_ComIPdu_DCLV_Status3_oInt_CAN_8186831c_Rx             14u
#define ComConf_ComIPdu_ELECTRON_BSI_oE_CAN_ab0b72ff_Rx               15u
#define ComConf_ComIPdu_NEW_JDD_oE_CAN_60b2710c_Rx                    16u
#define ComConf_ComIPdu_PFC_Fault_oInt_CAN_0a8001a9_Rx                17u
#define ComConf_ComIPdu_PFC_Status1_oInt_CAN_459f2204_Rx              18u
#define ComConf_ComIPdu_PFC_Status2_oInt_CAN_aea89907_Rx              19u
#define ComConf_ComIPdu_PFC_Status3_oInt_CAN_416af239_Rx              20u
#define ComConf_ComIPdu_PFC_Status4_oInt_CAN_a3b6e940_Rx              21u
#define ComConf_ComIPdu_PFC_Status5_oInt_CAN_4c74827e_Rx              22u
#define ComConf_ComIPdu_ParkCommand_oE_CAN_5e7110cb_Rx                23u
#define ComConf_ComIPdu_ProgTool_SupEnterBoot_oInt_CAN_42049dd5_Rx    24u
#define ComConf_ComIPdu_ReqToECANFunction_oE_CAN_01f67ab2_Rx          25u
#define ComConf_ComIPdu_ReqToOBC_oE_CAN_09b8aa3e_Rx                   26u
#define ComConf_ComIPdu_VCU2_oE_CAN_e2d13d8a_Rx                       27u
#define ComConf_ComIPdu_VCU3_oE_CAN_2e7b3d14_Rx                       28u
#define ComConf_ComIPdu_VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx             29u
#define ComConf_ComIPdu_VCU_PCANInfo_oE_CAN_788ea84c_Rx               30u
#define ComConf_ComIPdu_VCU_TU_oE_CAN_e21002f7_Rx                     31u
#define ComConf_ComIPdu_VCU_oE_CAN_8b566170_Rx                        32u
/**\} */

/**
 * \defgroup ComHandleIdsComTxPdu Handle IDs of handle space ComTxPdu.
 * \brief Tx Pdus
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define ComConf_ComIPdu_DC1_oE_CAN_fcac9ee4_Tx                        0u
#define ComConf_ComIPdu_DC2_oE_CAN_72239907_Tx                        1u
#define ComConf_ComIPdu_Debug1_oInt_CAN_177fd136_Tx                   2u
#define ComConf_ComIPdu_Debug2_oInt_CAN_fc486a35_Tx                   3u
#define ComConf_ComIPdu_Debug3_oInt_CAN_138a010b_Tx                   4u
#define ComConf_ComIPdu_Debug4_oInt_CAN_f1561a72_Tx                   5u
#define ComConf_ComIPdu_NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx           6u
#define ComConf_ComIPdu_OBC1_oE_CAN_89fd90e2_Tx                       7u
#define ComConf_ComIPdu_OBC2_oE_CAN_07729701_Tx                       8u
#define ComConf_ComIPdu_OBC3_oE_CAN_cbd8979f_Tx                       9u
#define ComConf_ComIPdu_OBC4_oE_CAN_c11d9e86_Tx                       10u
#define ComConf_ComIPdu_OBCTxDiag_oE_CAN_98fb5e1b_Tx                  11u
#define ComConf_ComIPdu_SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx           12u
#define ComConf_ComIPdu_SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx        13u
#define ComConf_ComIPdu_SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx        14u
#define ComConf_ComIPdu_SUP_CommandToPFC_oInt_CAN_d9411c97_Tx         15u
#define ComConf_ComIPdu_VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx              16u
/**\} */

/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
#define COM_START_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 1 */ /*MD_MSR_MemMap */
/**********************************************************************************************************************
  Com_RxIndication
**********************************************************************************************************************/
/** \brief        This function is called by the lower layer after an I-PDU has been received.
    \param[in]    RxPduId      ID of AUTOSAR COM I-PDU that has been received. Identifies the data that has been received.
                               Range: 0..(maximum number of I-PDU IDs received by AUTOSAR COM) - 1
    \param[in]    PduInfoPtr   Payload information of the received I-PDU (pointer to data and data length).
    \return       none
    \context      The function can be called on interrupt and task level. It is not allowed to use CAT1 interrupts with Rte (BSW00326]). Due to this, the interrupt context shall be configured to a CAT2 interrupt if an Rte is used.
    \synchronous  TRUE
    \reentrant    TRUE, for different Handles
    \trace        SPEC-2737026
    \note         The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(void, COM_CODE) Com_RxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, COM_APPL_DATA) PduInfoPtr);

/**********************************************************************************************************************
  Com_TxConfirmation
**********************************************************************************************************************/
/** \brief        This function is called by the lower layer after the PDU has been transmitted on the network.
                  A confirmation that is received for an I-PDU that does not require a confirmation is silently discarded.
    \param[in]    TxPduId   ID of AUTOSAR COM I-PDU that has been transmitted.
                            Range: 0..(maximum number of I-PDU IDs transmitted by AUTOSAR COM) - 1
    \return       none
    \context      The function can be called on interrupt and task level. It is not allowed to use CAT1 interrupts with Rte (BSW00326]). Due to this, the interrupt context shall be configured to a CAT2 interrupt if an Rte is used.
    \synchronous  TRUE
    \reentrant    TRUE, for different Handles
    \trace        SPEC-2737028
    \note         The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(void, COM_CODE) Com_TxConfirmation(PduIdType TxPduId);



#define COM_STOP_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 1 */ /* MD_MSR_MemMap */

#endif  /* COM_CBK_H */
/**********************************************************************************************************************
  END OF FILE: Com_Cbk.h
**********************************************************************************************************************/

