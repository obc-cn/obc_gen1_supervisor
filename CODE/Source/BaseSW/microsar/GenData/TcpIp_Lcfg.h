/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: TcpIp
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: TcpIp_Lcfg.h
 *   Generation Time: 2020-11-23 11:39:52
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined (TCPIP_LCFG_H)
#define TCPIP_LCFG_H

/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/
/* PRQA S 0784 EOF */ /* MD_TEST_Rule5.5_Wrapper */
/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1 */

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"
#include "IpBase.h"
#include "TcpIp_Cfg.h"
#include "TcpIp_Types.h"
#include "TcpIp_IpV6_Types.h"
#include "Dem.h"



/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
 *********************************************************************************************************************/

/* lower layer APIs used by TcpIp */
#define TCPIP_LL_GetPhysAddr          EthIf_GetPhysAddr
#define TCPIP_LL_UpdatePhysAddrFilter EthIf_UpdatePhysAddrFilter
#define TCPIP_LL_Transmit             EthIf_Transmit
#define TCPIP_LL_ProvideTxBuffer      EthIf_ProvideTxBuffer

/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
#define TCPIP_IPV4_ADDR_ASSIGNMENT_FLAG_LIFETIME_STORE    0x80u
#define TCPIP_IPV4_ADDR_ASSIGNMENT_FLAG_AUTO_TRIGGER      0x40u

#define IPV6_DHCPV6_MODE_DISABLED                0U
#define IPV6_DHCPV6_MODE_MANUAL                  1U
#define IPV6_DHCPV6_MODE_AUTOMATIC               2U

#define TCPIP_DHCPV6_DUID_MAX_LEN             130U /* A DUID can be no more than 128 octets long (not including the type code (2 byte)). [RFC3315 9.1. DUID Contents] */
#define TCPIP_DHCPV6_OPT_STACK_SIZE             2U /* E.g. IA_NA ( IAADDR ( STATUS_CODE ) ) */  /* maximum depth of option nesting. */


#define TCPIP_IPV6_GENERAL_IDX             0
#define TCPIP_IPV4_GENERAL_IDX             0
#define TCPIP_UDP_TX_RETRY_QUEUE_POOL_IDX  0
#define TCPIP_TCP_CONFIG_IDX               0
#define TCPIP_ICMPV4_CONFIG_IDX            0

#define TcpIp_GetRandomNumber() TcpIp_GetRandomNumberFctPtr()()

#define TCPIP_INV_DEM_ID 0xFFu

#define TCPIP_DHCPV4SERVER_ANY_SWITCH_OR_PORT_IDX   0xFFU


/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  TcpIpPCDataSwitches  TcpIp Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define TCPIP_DEFAULTADDRV6                                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTION                                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_CODEOFDHCPUSEROPTION                                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONBUFFERENDIDXOFDHCPUSEROPTION                                            STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONBUFFERLENGTHOFDHCPUSEROPTION                                            STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONBUFFERSTARTIDXOFDHCPUSEROPTION                                          STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DIRECTIONOFDHCPUSEROPTION                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONBUFFER                                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONDYN                                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_LENGTHOFDHCPUSEROPTIONDYN                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPV6CONFIG                                                                          STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_CONFIRMDELAYMAXOFDHCPV6CONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.ConfirmDelayMax' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_CONFIRMDELAYMINOFDHCPV6CONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.ConfirmDelayMin' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_DUIDTIMEOFDHCPV6CONFIG                                                                STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.DuidTime' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_ENABLEDNSSLOPTOFDHCPV6CONFIG                                                          STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.EnableDnsslOpt' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_ENABLERDNSSOPTOFDHCPV6CONFIG                                                          STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.EnableRdnssOpt' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INFOREQUESTDELAYMAXOFDHCPV6CONFIG                                                     STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.InfoRequestDelayMax' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INFOREQUESTDELAYMINOFDHCPV6CONFIG                                                     STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.InfoRequestDelayMin' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_RANDOMIZETIMEOUTSOFDHCPV6CONFIG                                                       STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.RandomizeTimeouts' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_RXREQOPTIONCNTOFDHCPV6CONFIG                                                          STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.RxReqOptionCnt' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_SOLICITDELAYMAXOFDHCPV6CONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.SolicitDelayMax' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_SOLICITDELAYMINOFDHCPV6CONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.SolicitDelayMin' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TXMSGBUFFERSIZEOFDHCPV6CONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.TxMsgBufferSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_USEFIRSTVALIDADVOFDHCPV6CONFIG                                                        STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Config.UseFirstValidAdv' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_DHCPV6DATA                                                                            STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_ADVINFOOFDHCPV6DATA                                                                   STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.AdvInfo' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_CLIENTDUIDOFDHCPV6DATA                                                                STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.ClientDuid' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_CURRENTTRANSIDOFDHCPV6DATA                                                            STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.CurrentTransId' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_ELAPSEDTIMEMSOFDHCPV6DATA                                                             STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.ElapsedTimeMs' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_IANALEASEOFDHCPV6DATA                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.IaNaLease' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_IANAOPTBUFOFDHCPV6DATA                                                                STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.IaNaOptBuf' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_IDLETIMEOFDHCPV6DATA                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.IdleTime' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_LASTSTATEOFDHCPV6DATA                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.LastState' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_MSGOFDHCPV6DATA                                                                       STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.Msg' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_RETRANSPARAMSOFDHCPV6DATA                                                             STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.RetransParams' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_STATEOFDHCPV6DATA                                                                     STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.State' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_TIMEOFDHCPV6DATA                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6Data.Time' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DHCPV6MSGTXBUFFER                                                                     STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6MsgTxBuffer' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_DHCPV6TXOPTION39                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6TxOption39' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_DHCPV6TXOPTION39INFO                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6TxOption39Info' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_MSGFLAGSOFDHCPV6TXOPTION39INFO                                                        STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6TxOption39Info.MsgFlags' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_OPTLENOFDHCPV6TXOPTION39INFO                                                          STD_OFF  /**< Deactivateable: 'TcpIp_DhcpV6TxOption39Info.OptLen' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DUPLICATEADDRDETECTIONFCTPTR                                                          STD_ON
#define TCPIP_ETHIFCTRL                                                                             STD_ON
#define TCPIP_IPV6CTRLIDXOFETHIFCTRL                                                                STD_ON
#define TCPIP_IPV6CTRLUSEDOFETHIFCTRL                                                               STD_ON
#define TCPIP_FINALMAGICNUMBER                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define TCPIP_ICMPV6CONFIG                                                                          STD_ON
#define TCPIP_ECHOREQUESTAPIOFICMPV6CONFIG                                                          STD_ON
#define TCPIP_HOPLIMITOFICMPV6CONFIG                                                                STD_ON
#define TCPIP_ICMPV6ECHODATABUFFER                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_IcmpV6EchoDataBuffer' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_ICMPV6INDADDRLISTRECEIVEDCBK                                                          STD_OFF  /**< Deactivateable: 'TcpIp_IcmpV6IndAddrListReceivedCbk' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_ICMPV6MAXPAYLOADLENCHGCBK                                                             STD_OFF  /**< Deactivateable: 'TcpIp_IcmpV6MaxPayloadLenChgCbk' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_ICMPV6MSGHANDLERCBKFCTPTR                                                             STD_ON
#define TCPIP_ICMPV6TXMSGBUFFER                                                                     STD_ON
#define TCPIP_INITDATAHASHCODE                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define TCPIP_INTEGRITYALGORITHM                                                                    STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityAlgorithm' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_ICVLENGTHOFINTEGRITYALGORITHM                                                         STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityAlgorithm.IcvLength' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYJOBPAIRENDIDXOFINTEGRITYALGORITHM                                            STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityAlgorithm.IntegrityJobPairEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYJOBPAIRSTARTIDXOFINTEGRITYALGORITHM                                          STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityAlgorithm.IntegrityJobPairStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYTRANSFORMIDENTOFINTEGRITYALGORITHM                                           STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityAlgorithm.IntegrityTransformIdent' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYJOBPAIR                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPair' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_GENERATEJOBIDOFINTEGRITYJOBPAIR                                                       STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPair.GenerateJobId' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_GENERATEKEYIDOFINTEGRITYJOBPAIR                                                       STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPair.GenerateKeyId' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYALGORITHMIDXOFINTEGRITYJOBPAIR                                               STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPair.IntegrityAlgorithmIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_VERIFYJOBIDOFINTEGRITYJOBPAIR                                                         STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPair.VerifyJobId' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_VERIFYKEYIDOFINTEGRITYJOBPAIR                                                         STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPair.VerifyKeyId' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYJOBPAIRDYN                                                                   STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPairDyn' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_STATUSOFINTEGRITYJOBPAIRDYN                                                           STD_OFF  /**< Deactivateable: 'TcpIp_IntegrityJobPairDyn.Status' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_INTERFACEIDENTIFIER                                                                   STD_ON
#define TCPIP_IPSECCONFIGSET                                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_AUDITEVENTCALLOUTFUNCTIONOFIPSECCONFIGSET                                             STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet.AuditEventCalloutFunction' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYALGORITHMENDIDXOFIPSECCONFIGSET                                              STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet.IntegrityAlgorithmEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_INTEGRITYALGORITHMSTARTIDXOFIPSECCONFIGSET                                            STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet.IntegrityAlgorithmStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_SPDCALLOUTFUNCTIONOFIPSECCONFIGSET                                                    STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet.SpdCalloutFunction' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_SPDENTRYENDIDXOFIPSECCONFIGSET                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet.SpdEntryEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_SPDENTRYSTARTIDXOFIPSECCONFIGSET                                                      STD_OFF  /**< Deactivateable: 'TcpIp_IpSecConfigSet.SpdEntryStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6CTRL                                                                              STD_ON
#define TCPIP_ALLOWLINKMTURECONFIGURATIONOFIPV6CTRL                                                 STD_ON
#define TCPIP_DEFAULTHOPLIMITOFIPV6CTRL                                                             STD_ON
#define TCPIP_DEFAULTLINKMTUOFIPV6CTRL                                                              STD_ON
#define TCPIP_DEFAULTTRAFFICCLASSFLOWLABELNBOOFIPV6CTRL                                             STD_ON
#define TCPIP_DHCPMODEOFIPV6CTRL                                                                    STD_ON
#define TCPIP_DHCPUSEROPTIONENDIDXOFIPV6CTRL                                                        STD_ON
#define TCPIP_DHCPUSEROPTIONSTARTIDXOFIPV6CTRL                                                      STD_ON
#define TCPIP_DHCPUSEROPTIONUSEDOFIPV6CTRL                                                          STD_ON
#define TCPIP_DHCPV6CONFIGIDXOFIPV6CTRL                                                             STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6ConfigIdx' Reason: 'the optional indirection is deactivated because DhcpV6ConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6CONFIGUSEDOFIPV6CTRL                                                            STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6ConfigUsed' Reason: 'the optional indirection is deactivated because DhcpV6ConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6DATAIDXOFIPV6CTRL                                                               STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6DataIdx' Reason: 'the optional indirection is deactivated because DhcpV6DataUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6DATAUSEDOFIPV6CTRL                                                              STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6DataUsed' Reason: 'the optional indirection is deactivated because DhcpV6DataUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6MSGTXBUFFERENDIDXOFIPV6CTRL                                                     STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6MsgTxBufferEndIdx' Reason: 'the optional indirection is deactivated because DhcpV6MsgTxBufferUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6MSGTXBUFFERSTARTIDXOFIPV6CTRL                                                   STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6MsgTxBufferStartIdx' Reason: 'the optional indirection is deactivated because DhcpV6MsgTxBufferUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6MSGTXBUFFERUSEDOFIPV6CTRL                                                       STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6MsgTxBufferUsed' Reason: 'the optional indirection is deactivated because DhcpV6MsgTxBufferUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6TXOPTION39ENDIDXOFIPV6CTRL                                                      STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6TxOption39EndIdx' Reason: 'the optional indirection is deactivated because DhcpV6TxOption39UsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6TXOPTION39INFOIDXOFIPV6CTRL                                                     STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6TxOption39InfoIdx' Reason: 'the optional indirection is deactivated because DhcpV6TxOption39InfoUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6TXOPTION39INFOUSEDOFIPV6CTRL                                                    STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6TxOption39InfoUsed' Reason: 'the optional indirection is deactivated because DhcpV6TxOption39InfoUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6TXOPTION39STARTIDXOFIPV6CTRL                                                    STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6TxOption39StartIdx' Reason: 'the optional indirection is deactivated because DhcpV6TxOption39UsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_DHCPV6TXOPTION39USEDOFIPV6CTRL                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.DhcpV6TxOption39Used' Reason: 'the optional indirection is deactivated because DhcpV6TxOption39UsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_ENABLEDYNHOPLIMITOFIPV6CTRL                                                           STD_ON
#define TCPIP_ENABLEPATHMTUOFIPV6CTRL                                                               STD_ON
#define TCPIP_ETHIFCTRLIDXOFIPV6CTRL                                                                STD_ON
#define TCPIP_FRAMEPRIODEFAULTOFIPV6CTRL                                                            STD_ON
#define TCPIP_HWCHECKSUMICMPOFIPV6CTRL                                                              STD_ON
#define TCPIP_HWCHECKSUMIPDESTINATIONOPTIONSOFIPV6CTRL                                              STD_ON
#define TCPIP_HWCHECKSUMIPHOPBYHOPOPTIONSOFIPV6CTRL                                                 STD_ON
#define TCPIP_HWCHECKSUMIPROUTINGOFIPV6CTRL                                                         STD_ON
#define TCPIP_HWCHECKSUMTCPOFIPV6CTRL                                                               STD_ON
#define TCPIP_HWCHECKSUMUDPOFIPV6CTRL                                                               STD_ON
#define TCPIP_INTERFACEIDENTIFIERENDIDXOFIPV6CTRL                                                   STD_ON
#define TCPIP_INTERFACEIDENTIFIERSTARTIDXOFIPV6CTRL                                                 STD_ON
#define TCPIP_IPV6DEFAULTROUTERLISTENTRYENDIDXOFIPV6CTRL                                            STD_ON
#define TCPIP_IPV6DEFAULTROUTERLISTENTRYSTARTIDXOFIPV6CTRL                                          STD_ON
#define TCPIP_IPV6DESTINATIONCACHEENTRYENDIDXOFIPV6CTRL                                             STD_ON
#define TCPIP_IPV6DESTINATIONCACHEENTRYSTARTIDXOFIPV6CTRL                                           STD_ON
#define TCPIP_IPV6ETHBUFDATAENDIDXOFIPV6CTRL                                                        STD_ON
#define TCPIP_IPV6ETHBUFDATASTARTIDXOFIPV6CTRL                                                      STD_ON
#define TCPIP_IPV6FRAGMENTATIONCONFIGIDXOFIPV6CTRL                                                  STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.IpV6FragmentationConfigIdx' Reason: 'the optional indirection is deactivated because IpV6FragmentationConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6FRAGMENTATIONCONFIGUSEDOFIPV6CTRL                                                 STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.IpV6FragmentationConfigUsed' Reason: 'the optional indirection is deactivated because IpV6FragmentationConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6MULTICASTADDRENDIDXOFIPV6CTRL                                                     STD_ON
#define TCPIP_IPV6MULTICASTADDRSTARTIDXOFIPV6CTRL                                                   STD_ON
#define TCPIP_IPV6MULTICASTADDRUSEDOFIPV6CTRL                                                       STD_ON
#define TCPIP_IPV6NEIGHBORCACHEENTRYENDIDXOFIPV6CTRL                                                STD_ON
#define TCPIP_IPV6NEIGHBORCACHEENTRYSTARTIDXOFIPV6CTRL                                              STD_ON
#define TCPIP_IPV6PREFIXLISTENTRYENDIDXOFIPV6CTRL                                                   STD_ON
#define TCPIP_IPV6PREFIXLISTENTRYSTARTIDXOFIPV6CTRL                                                 STD_ON
#define TCPIP_IPV6SOURCEADDRESSDHCPIDXOFIPV6CTRL                                                    STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.IpV6SourceAddressDhcpIdx' Reason: 'the optional indirection is deactivated because IpV6SourceAddressDhcpUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6SOURCEADDRESSDHCPUSEDOFIPV6CTRL                                                   STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.IpV6SourceAddressDhcpUsed' Reason: 'the optional indirection is deactivated because IpV6SourceAddressDhcpUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6SOURCEADDRESSENDIDXOFIPV6CTRL                                                     STD_ON
#define TCPIP_IPV6SOURCEADDRESSSTARTIDXOFIPV6CTRL                                                   STD_ON
#define TCPIP_LOCALADDRV6BCIDXOFIPV6CTRL                                                            STD_ON
#define TCPIP_LOCALADDRV6BCUSEDOFIPV6CTRL                                                           STD_ON
#define TCPIP_MASKEDBITSOFIPV6CTRL                                                                  STD_ON
#define TCPIP_MLDCONFIGIDXOFIPV6CTRL                                                                STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.MldConfigIdx' Reason: 'the optional indirection is deactivated because MldConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_MLDCONFIGUSEDOFIPV6CTRL                                                               STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.MldConfigUsed' Reason: 'the optional indirection is deactivated because MldConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_NDPCONFIGIDXOFIPV6CTRL                                                                STD_ON
#define TCPIP_PATHMTUTIMEOUTOFIPV6CTRL                                                              STD_ON
#define TCPIP_PRIVEXTCONFIGIDXOFIPV6CTRL                                                            STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.PrivExtConfigIdx' Reason: 'the optional indirection is deactivated because PrivExtConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_PRIVEXTCONFIGUSEDOFIPV6CTRL                                                           STD_OFF  /**< Deactivateable: 'TcpIp_IpV6Ctrl.PrivExtConfigUsed' Reason: 'the optional indirection is deactivated because PrivExtConfigUsedOfIpV6Ctrl is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6CTRLDYN                                                                           STD_ON
#define TCPIP_BASEREACHABLETIMEMSOFIPV6CTRLDYN                                                      STD_ON
#define TCPIP_CTRLPREVIOUSSTATEOFIPV6CTRLDYN                                                        STD_ON
#define TCPIP_CTRLSTATEOFIPV6CTRLDYN                                                                STD_ON
#define TCPIP_CURHOPLIMITOFIPV6CTRLDYN                                                              STD_ON
#define TCPIP_DEFAULTLINKMTUOFIPV6CTRLDYN                                                           STD_ON
#define TCPIP_IPV6DEFAULTROUTERLISTENTRYVALIDENDIDXOFIPV6CTRLDYN                                    STD_ON
#define TCPIP_IPV6DESTINATIONCACHEENTRYVALIDENDIDXOFIPV6CTRLDYN                                     STD_ON
#define TCPIP_IPV6NEIGHBORCACHEENTRYVALIDENDIDXOFIPV6CTRLDYN                                        STD_ON
#define TCPIP_IPV6PREFIXLISTENTRYVALIDENDIDXOFIPV6CTRLDYN                                           STD_ON
#define TCPIP_LASTBCADDRPTROFIPV6CTRLDYN                                                            STD_ON
#define TCPIP_NDP_PENDINGDADNAOFIPV6CTRLDYN                                                         STD_ON
#define TCPIP_NDP_ROUTERSOLICITATIONNEXTTXTIMEOFIPV6CTRLDYN                                         STD_ON
#define TCPIP_NDP_ROUTERSOLICITATIONTXCOUNTOFIPV6CTRLDYN                                            STD_ON
#define TCPIP_NEXTROUTERPROBEIDXOFIPV6CTRLDYN                                                       STD_ON
#define TCPIP_REACHABLETIMEMSOFIPV6CTRLDYN                                                          STD_ON
#define TCPIP_RETRANSTIMERMSOFIPV6CTRLDYN                                                           STD_ON
#define TCPIP_IPV6DEFAULTROUTERLISTENTRY                                                            STD_ON
#define TCPIP_IPV6DESTINATIONCACHEENTRY                                                             STD_ON
#define TCPIP_IPV6ETHBUFDATA                                                                        STD_ON
#define TCPIP_IPV6SOCKETDYNIDXOFIPV6ETHBUFDATA                                                      STD_ON
#define TCPIP_ULTXREQTABIDXOFIPV6ETHBUFDATA                                                         STD_ON
#define TCPIP_IPV6FRAGMENTTXBUFFERDESCRIPTOR                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentTxBufferDescriptor' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_IPV6FRAGMENTTXDATABUFFERENDIDXOFIPV6FRAGMENTTXBUFFERDESCRIPTOR                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentTxBufferDescriptor.IpV6FragmentTxDataBufferEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTTXDATABUFFERSTARTIDXOFIPV6FRAGMENTTXBUFFERDESCRIPTOR                      STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentTxBufferDescriptor.IpV6FragmentTxDataBufferStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTTXDATABUFFERUSEDOFIPV6FRAGMENTTXBUFFERDESCRIPTOR                          STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentTxBufferDescriptor.IpV6FragmentTxDataBufferUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTTXBUFFERDESCRIPTORDYN                                                     STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentTxBufferDescriptorDyn' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTTXDATABUFFER                                                              STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentTxDataBuffer' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTATIONCONFIG                                                               STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_IPV6FRAGMENTTXBUFFERDESCRIPTORENDIDXOFIPV6FRAGMENTATIONCONFIG                         STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.IpV6FragmentTxBufferDescriptorEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTTXBUFFERDESCRIPTORSTARTIDXOFIPV6FRAGMENTATIONCONFIG                       STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.IpV6FragmentTxBufferDescriptorStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6FRAGMENTTXBUFFERDESCRIPTORUSEDOFIPV6FRAGMENTATIONCONFIG                           STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.IpV6FragmentTxBufferDescriptorUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYBUFFERDESCRIPTORENDIDXOFIPV6FRAGMENTATIONCONFIG                         STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.IpV6ReassemblyBufferDescriptorEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYBUFFERDESCRIPTORSTARTIDXOFIPV6FRAGMENTATIONCONFIG                       STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.IpV6ReassemblyBufferDescriptorStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYBUFFERDESCRIPTORUSEDOFIPV6FRAGMENTATIONCONFIG                           STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.IpV6ReassemblyBufferDescriptorUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REASSEMBLYBUFFERCOUNTOFIPV6FRAGMENTATIONCONFIG                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.ReassemblyBufferCount' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REASSEMBLYBUFFERSIZEOFIPV6FRAGMENTATIONCONFIG                                         STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.ReassemblyBufferSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REASSEMBLYSEGMENTCOUNTOFIPV6FRAGMENTATIONCONFIG                                       STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.ReassemblySegmentCount' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REASSEMBLYTIMEOUTOFIPV6FRAGMENTATIONCONFIG                                            STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.ReassemblyTimeout' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TXFRAGMENTBUFFERCOUNTOFIPV6FRAGMENTATIONCONFIG                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.TxFragmentBufferCount' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TXFRAGMENTBUFFERSIZEOFIPV6FRAGMENTATIONCONFIG                                         STD_OFF  /**< Deactivateable: 'TcpIp_IpV6FragmentationConfig.TxFragmentBufferSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6GENERAL                                                                           STD_ON
#define TCPIP_EXTDESTADDRVALIDATIONENABLEDOFIPV6GENERAL                                             STD_ON
#define TCPIP_IPV6CTRLDEFAULTIDXOFIPV6GENERAL                                                       STD_ON
#define TCPIP_IPV6CTRLDEFAULTUSEDOFIPV6GENERAL                                                      STD_ON
#define TCPIP_IPV6SOCKETDYNDHCPIDXOFIPV6GENERAL                                                     STD_OFF  /**< Deactivateable: 'TcpIp_IpV6General.IpV6SocketDynDhcpIdx' Reason: 'the optional indirection is deactivated because IpV6SocketDynDhcpUsedOfIpV6General is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6SOCKETDYNDHCPUSEDOFIPV6GENERAL                                                    STD_OFF  /**< Deactivateable: 'TcpIp_IpV6General.IpV6SocketDynDhcpUsed' Reason: 'the optional indirection is deactivated because IpV6SocketDynDhcpUsedOfIpV6General is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6SOCKETDYNICMPIDXOFIPV6GENERAL                                                     STD_ON
#define TCPIP_IPV6SOCKETDYNNDPIDXOFIPV6GENERAL                                                      STD_ON
#define TCPIP_IPV6SOCKETDYNTCPRSTIDXOFIPV6GENERAL                                                   STD_ON
#define TCPIP_IPV6SOCKETDYNTCPRSTUSEDOFIPV6GENERAL                                                  STD_ON
#define TCPIP_MASKEDBITSOFIPV6GENERAL                                                               STD_ON
#define TCPIP_IPV6MULTICASTADDR                                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DEFAULTADDRV6IDXOFIPV6MULTICASTADDR                                                   STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DEFAULTADDRV6USEDOFIPV6MULTICASTADDR                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_IPV6MULTICASTADDRACTIVEIDXOFIPV6MULTICASTADDR                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_IPV6MULTICASTADDRACTIVEUSEDOFIPV6MULTICASTADDR                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_LOCALADDRV6IDXOFIPV6MULTICASTADDR                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_MASKEDBITSOFIPV6MULTICASTADDR                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_IPV6MULTICASTADDRACTIVE                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_IPV6NEIGHBORCACHEENTRY                                                                STD_ON
#define TCPIP_IPV6PREFIXLISTENTRY                                                                   STD_ON
#define TCPIP_IPV6REASSEMBLYBUFFERDESCRIPTOR                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_IPV6REASSEMBLYDATABUFFERENDIDXOFIPV6REASSEMBLYBUFFERDESCRIPTOR                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor.IpV6ReassemblyDataBufferEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYDATABUFFERSTARTIDXOFIPV6REASSEMBLYBUFFERDESCRIPTOR                      STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor.IpV6ReassemblyDataBufferStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYDATABUFFERUSEDOFIPV6REASSEMBLYBUFFERDESCRIPTOR                          STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor.IpV6ReassemblyDataBufferUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYSEGMENTENDIDXOFIPV6REASSEMBLYBUFFERDESCRIPTOR                           STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor.IpV6ReassemblySegmentEndIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYSEGMENTSTARTIDXOFIPV6REASSEMBLYBUFFERDESCRIPTOR                         STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor.IpV6ReassemblySegmentStartIdx' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYSEGMENTUSEDOFIPV6REASSEMBLYBUFFERDESCRIPTOR                             STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptor.IpV6ReassemblySegmentUsed' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYBUFFERDESCRIPTORDYN                                                     STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyBufferDescriptorDyn' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYDATABUFFER                                                              STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblyDataBuffer' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6REASSEMBLYSEGMENT                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_IpV6ReassemblySegment' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_IPV6SOCKETDYN                                                                         STD_ON
#define TCPIP_ETHIFFRAMEPRIOOFIPV6SOCKETDYN                                                         STD_ON
#define TCPIP_FLAGSOFIPV6SOCKETDYN                                                                  STD_ON
#define TCPIP_HOPLIMITOFIPV6SOCKETDYN                                                               STD_ON
#define TCPIP_IPV6DESTINATIONCACHEENTRYHINTIDXOFIPV6SOCKETDYN                                       STD_ON
#define TCPIP_IPV6HDRVERSIONTCFLNBOOFIPV6SOCKETDYN                                                  STD_ON
#define TCPIP_IPV6SOURCEADDRESS                                                                     STD_ON
#define TCPIP_ADDRESSASSIGNVARIANTOFIPV6SOURCEADDRESS                                               STD_ON
#define TCPIP_DEFAULTADDRV6IDXOFIPV6SOURCEADDRESS                                                   STD_ON
#define TCPIP_DEFAULTADDRV6USEDOFIPV6SOURCEADDRESS                                                  STD_ON
#define TCPIP_LOCALADDRV6IDXOFIPV6SOURCEADDRESS                                                     STD_ON
#define TCPIP_MASKEDBITSOFIPV6SOURCEADDRESS                                                         STD_ON
#define TCPIP_NVMDATAIDXOFIPV6SOURCEADDRESS                                                         STD_OFF  /**< Deactivateable: 'TcpIp_IpV6SourceAddress.NvmDataIdx' Reason: 'the optional indirection is deactivated because NvmDataUsedOfIpV6SourceAddress is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_NVMDATAUSEDOFIPV6SOURCEADDRESS                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6SourceAddress.NvmDataUsed' Reason: 'the optional indirection is deactivated because NvmDataUsedOfIpV6SourceAddress is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_PREFIXISONLINKOFIPV6SOURCEADDRESS                                                     STD_ON
#define TCPIP_IPV6SOURCEADDRESSREQUESTED                                                            STD_OFF  /**< Deactivateable: 'TcpIp_IpV6SourceAddressRequested' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_IPV6SOURCEADDRESSTABLEENTRY                                                           STD_ON
#define TCPIP_IPV6STATICONLINKPREFIX                                                                STD_OFF  /**< Deactivateable: 'TcpIp_IpV6StaticOnLinkPrefix' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_BITLENGTHOFIPV6STATICONLINKPREFIX                                                     STD_OFF  /**< Deactivateable: 'TcpIp_IpV6StaticOnLinkPrefix.BitLength' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_PREFIXOFIPV6STATICONLINKPREFIX                                                        STD_OFF  /**< Deactivateable: 'TcpIp_IpV6StaticOnLinkPrefix.Prefix' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_LOCALADDR                                                                             STD_ON
#define TCPIP_ASSIGNMENTSTATEOFLOCALADDR                                                            STD_ON
#define TCPIP_LOCALADDRV6                                                                           STD_ON
#define TCPIP_IPV6CTRLIDXOFLOCALADDRV6                                                              STD_ON
#define TCPIP_IPV6MULTICASTADDRIDXOFLOCALADDRV6                                                     STD_ON
#define TCPIP_IPV6MULTICASTADDRUSEDOFLOCALADDRV6                                                    STD_ON
#define TCPIP_IPV6SOURCEADDRESSIDXOFLOCALADDRV6                                                     STD_ON
#define TCPIP_IPV6SOURCEADDRESSUSEDOFLOCALADDRV6                                                    STD_ON
#define TCPIP_MASKEDBITSOFLOCALADDRV6                                                               STD_ON
#define TCPIP_MEASUREMENTDATA                                                                       STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DROPPEDIPV4PACKETSOFMEASUREMENTDATA                                                   STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData.DroppedIpV4Packets' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DROPPEDIPV6PACKETSOFMEASUREMENTDATA                                                   STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData.DroppedIpV6Packets' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DROPPEDTCPPACKETSOFMEASUREMENTDATA                                                    STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData.DroppedTcpPackets' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DROPPEDUDPPACKETSOFMEASUREMENTDATA                                                    STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData.DroppedUdpPackets' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DROPPEDVNDRIPV6PACKETSOFMEASUREMENTDATA                                               STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData.DroppedVndrIpV6Packets' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DROPPEDVNDRLAYER4PACKETSOFMEASUREMENTDATA                                             STD_OFF  /**< Deactivateable: 'TcpIp_MeasurementData.DroppedVndrLayer4Packets' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_MLDCONFIG                                                                             STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_MLDIFSTATEPOOLSIZEOFMLDCONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldIfStatePoolSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDIFSTATESRCADDRPOOLSIZEOFMLDCONFIG                                                  STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldIfStateSrcAddrPoolSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDMAXREPORTMSGSIZEOFMLDCONFIG                                                        STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldMaxReportMsgSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDMAXUNSOLICITEDREPORTINTMSOFMLDCONFIG                                               STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldMaxUnsolicitedReportIntMs' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDMINUNSOLICITEDREPORTINTMSOFMLDCONFIG                                               STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldMinUnsolicitedReportIntMs' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDROBUSTNESSOFMLDCONFIG                                                              STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldRobustness' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDSOCKETMEMBERSHIPPOOLSIZEOFMLDCONFIG                                                STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldSocketMembershipPoolSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDSRCADDRPOOLSIZEOFMLDCONFIG                                                         STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldSrcAddrPoolSize' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MLDV1QUERIERPRESETTIMEOUTMSOFMLDCONFIG                                                STD_OFF  /**< Deactivateable: 'TcpIp_MldConfig.MldV1QuerierPresetTimeoutMs' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_NDPCONFIG                                                                             STD_ON
#define TCPIP_DADTRANSMITSOFNDPCONFIG                                                               STD_ON
#define TCPIP_DYNAMICREACHABLETIMEOFNDPCONFIG                                                       STD_ON
#define TCPIP_DYNAMICRETRANSTIMEROFNDPCONFIG                                                        STD_ON
#define TCPIP_ENABLENDPINVNANCUPDATEOFNDPCONFIG                                                     STD_ON
#define TCPIP_ENABLENDPINVNAOFNDPCONFIG                                                             STD_ON
#define TCPIP_ENABLENDPINVNSOFNDPCONFIG                                                             STD_ON
#define TCPIP_ENABLENUDOFNDPCONFIG                                                                  STD_ON
#define TCPIP_ENABLEOPTIMISTICDADOFNDPCONFIG                                                        STD_ON
#define TCPIP_ENABLERFC6106DNSSLOPTOFNDPCONFIG                                                      STD_ON
#define TCPIP_ENABLERFC6106RDNSSOPTOFNDPCONFIG                                                      STD_ON
#define TCPIP_ENABLESLAACDELAYOFNDPCONFIG                                                           STD_ON
#define TCPIP_IPV6STATICONLINKPREFIXENDIDXOFNDPCONFIG                                               STD_OFF  /**< Deactivateable: 'TcpIp_NdpConfig.IpV6StaticOnLinkPrefixEndIdx' Reason: 'the optional indirection is deactivated because IpV6StaticOnLinkPrefixUsedOfNdpConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6STATICONLINKPREFIXSTARTIDXOFNDPCONFIG                                             STD_OFF  /**< Deactivateable: 'TcpIp_NdpConfig.IpV6StaticOnLinkPrefixStartIdx' Reason: 'the optional indirection is deactivated because IpV6StaticOnLinkPrefixUsedOfNdpConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_IPV6STATICONLINKPREFIXUSEDOFNDPCONFIG                                                 STD_OFF  /**< Deactivateable: 'TcpIp_NdpConfig.IpV6StaticOnLinkPrefixUsed' Reason: 'the optional indirection is deactivated because IpV6StaticOnLinkPrefixUsedOfNdpConfig is always 'FALSE' and the target of the indirection is of the Configuration Class 'PRE_COMPILE'.' */
#define TCPIP_MASKEDBITSOFNDPCONFIG                                                                 STD_ON
#define TCPIP_MAXRANDOMFACTOROFNDPCONFIG                                                            STD_ON
#define TCPIP_MAXRTRSOLICITATIONDELAYOFNDPCONFIG                                                    STD_ON
#define TCPIP_MAXRTRSOLICITATIONSOFNDPCONFIG                                                        STD_ON
#define TCPIP_MAXSLAACDELAYOFNDPCONFIG                                                              STD_ON
#define TCPIP_MINRANDOMFACTOROFNDPCONFIG                                                            STD_ON
#define TCPIP_MULTICASTSOLICITSOFNDPCONFIG                                                          STD_ON
#define TCPIP_NUDFIRSTPROBEDELAYOFNDPCONFIG                                                         STD_ON
#define TCPIP_RANDOMREACHABLETIMEOFNDPCONFIG                                                        STD_ON
#define TCPIP_REACHABLETIMEOFNDPCONFIG                                                              STD_ON
#define TCPIP_RETRANSTIMEROFNDPCONFIG                                                               STD_ON
#define TCPIP_RNDRTRSOLICITATIONDELAYOFNDPCONFIG                                                    STD_ON
#define TCPIP_RTRSOLICITATIONINTERVALOFNDPCONFIG                                                    STD_ON
#define TCPIP_SLAACMINLIFETIMEOFNDPCONFIG                                                           STD_ON
#define TCPIP_UNICASTSOLICITSOFNDPCONFIG                                                            STD_ON
#define TCPIP_NVMDATA                                                                               STD_OFF  /**< Deactivateable: 'TcpIp_NvmData' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_NVMDATAROM                                                                            STD_OFF  /**< Deactivateable: 'TcpIp_NvmDataRom' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_PHYSADDRCONFIG                                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_CHGDISCARDEDFUNCPTROFPHYSADDRCONFIG                                                   STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_CHGFUNCPTROFPHYSADDRCONFIG                                                            STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_PRIVEXTCONFIG                                                                         STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_MAXDESYNCFACTOROFPRIVEXTCONFIG                                                        STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.MaxDesyncFactor' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_MAXTEMPADDRSOFPRIVEXTCONFIG                                                           STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.MaxTempAddrs' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REGENADVANCEOFPRIVEXTCONFIG                                                           STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.RegenAdvance' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TEMPIDGENRETRIESOFPRIVEXTCONFIG                                                       STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.TempIdGenRetries' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TEMPPREFERREDLIFETIMEOFPRIVEXTCONFIG                                                  STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.TempPreferredLifetime' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TEMPVALIDLIFETIMEOFPRIVEXTCONFIG                                                      STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.TempValidLifetime' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_USEONLINKPREFIXESOFPRIVEXTCONFIG                                                      STD_OFF  /**< Deactivateable: 'TcpIp_PrivExtConfig.UseOnLinkPrefixes' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_RANDOMNUMBERFCTPTR                                                                    STD_ON
#define TCPIP_SAENTRY                                                                               STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_DIRECTIONOFSAENTRY                                                                    STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.Direction' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_ESNSUPPORTOFSAENTRY                                                                   STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.EsnSupport' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_INTEGRITYCHKLENOFSAENTRY                                                              STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.IntegrityChkLen' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_INTEGRITYJOBPAIRIDXOFSAENTRY                                                          STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.IntegrityJobPairIdx' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_INTEGRITYTRANSFORMIDENTOFSAENTRY                                                      STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.IntegrityTransformIdent' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_IPSECHDRTYPEOFSAENTRY                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.IpSecHdrType' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_LOCIPV4ADDROFSAENTRY                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.LocIpV4Addr' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_LOCPORTENDOFSAENTRY                                                                   STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.LocPortEnd' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_LOCPORTSTARTOFSAENTRY                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.LocPortStart' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_NONCEOFSAENTRY                                                                        STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.Nonce' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_REMIPV4ADDROFSAENTRY                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.RemIpV4Addr' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_REMPORTENDOFSAENTRY                                                                   STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.RemPortEnd' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_REMPORTSTARTOFSAENTRY                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.RemPortStart' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_SAENTRYPAIRIDXOFSAENTRY                                                               STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.SaEntryPairIdx' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_SEQNUMHOFSAENTRY                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.SeqNumH' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_SEQNUMLOFSAENTRY                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.SeqNumL' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_SPIOFSAENTRY                                                                          STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.Spi' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_UWNDHOFSAENTRY                                                                        STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.UWndH' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_UWNDLOFSAENTRY                                                                        STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.UWndL' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_WNDBITSOFSAENTRY                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.WndBits' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_XPTPROTOFSAENTRY                                                                      STD_OFF  /**< Deactivateable: 'TcpIp_SaEntry.XptProt' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
#define TCPIP_SIZEOFDEFAULTADDRV6                                                                   STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFDHCPUSEROPTION                                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFDHCPUSEROPTIONBUFFER                                                            STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFDHCPUSEROPTIONDYN                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFETHIFCTRL                                                                       STD_ON
#define TCPIP_SIZEOFICMPV6CONFIG                                                                    STD_ON
#define TCPIP_SIZEOFICMPV6TXMSGBUFFER                                                               STD_ON
#define TCPIP_SIZEOFINTERFACEIDENTIFIER                                                             STD_ON
#define TCPIP_SIZEOFIPV6CTRL                                                                        STD_ON
#define TCPIP_SIZEOFIPV6CTRLDYN                                                                     STD_ON
#define TCPIP_SIZEOFIPV6DEFAULTROUTERLISTENTRY                                                      STD_ON
#define TCPIP_SIZEOFIPV6DESTINATIONCACHEENTRY                                                       STD_ON
#define TCPIP_SIZEOFIPV6ETHBUFDATA                                                                  STD_ON
#define TCPIP_SIZEOFIPV6GENERAL                                                                     STD_ON
#define TCPIP_SIZEOFIPV6MULTICASTADDR                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFIPV6MULTICASTADDRACTIVE                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFIPV6NEIGHBORCACHEENTRY                                                          STD_ON
#define TCPIP_SIZEOFIPV6PREFIXLISTENTRY                                                             STD_ON
#define TCPIP_SIZEOFIPV6SOCKETDYN                                                                   STD_ON
#define TCPIP_SIZEOFIPV6SOURCEADDRESS                                                               STD_ON
#define TCPIP_SIZEOFIPV6SOURCEADDRESSTABLEENTRY                                                     STD_ON
#define TCPIP_SIZEOFLOCALADDR                                                                       STD_ON
#define TCPIP_SIZEOFLOCALADDRV6                                                                     STD_ON
#define TCPIP_SIZEOFNDPCONFIG                                                                       STD_ON
#define TCPIP_SIZEOFPHYSADDRCONFIG                                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFSOCKETDYN                                                                       STD_ON
#define TCPIP_SIZEOFSOCKETOWNERCONFIG                                                               STD_ON
#define TCPIP_SIZEOFSOCKETTCPDYN                                                                    STD_ON
#define TCPIP_SIZEOFSOCKETUDPDYN                                                                    STD_ON
#define TCPIP_SIZEOFTCPCONFIG                                                                       STD_ON
#define TCPIP_SIZEOFTCPOOOQELEMENT                                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFTCPRESETQELEMENT                                                                STD_ON
#define TCPIP_SIZEOFTCPRETRYQELEMENT                                                                STD_ON
#define TCPIP_SIZEOFTCPRXBUFFER                                                                     STD_ON
#define TCPIP_SIZEOFTCPRXBUFFERDESC                                                                 STD_ON
#define TCPIP_SIZEOFTCPRXBUFFERDESCDYN                                                              STD_ON
#define TCPIP_SIZEOFTCPTXBUFFER                                                                     STD_ON
#define TCPIP_SIZEOFTCPTXBUFFERDESC                                                                 STD_ON
#define TCPIP_SIZEOFTCPTXBUFFERDESCDYN                                                              STD_ON
#define TCPIP_SIZEOFTXREQELEM                                                                       STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFTXREQELEMLIST                                                                   STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFTXREQELEMLISTDYN                                                                STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFUDPTXRETRYQUEUEELEMENTCHAIN                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFUDPTXRETRYQUEUEELEMENTS                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFUDPTXRETRYQUEUEPOOLDESC                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SOCKETDYN                                                                             STD_ON
#define TCPIP_LISTENACTIVECONNSTATOFSOCKETDYN                                                       STD_ON
#define TCPIP_LOCSOCKOFSOCKETDYN                                                                    STD_ON
#define TCPIP_LOCALADDRBINDIDXOFSOCKETDYN                                                           STD_ON
#define TCPIP_REMSOCKOFSOCKETDYN                                                                    STD_ON
#define TCPIP_SOCKETOWNERCONFIGIDXOFSOCKETDYN                                                       STD_ON
#define TCPIP_TXBUFREQUESTEDOFSOCKETDYN                                                             STD_ON
#define TCPIP_TXIPADDRIDXOFSOCKETDYN                                                                STD_ON
#define TCPIP_SOCKETOWNERCONFIG                                                                     STD_ON
#define TCPIP_COPYTXDATADYNFUNCPTROFSOCKETOWNERCONFIG                                               STD_ON
#define TCPIP_COPYTXDATAFUNCPTROFSOCKETOWNERCONFIG                                                  STD_ON
#define TCPIP_DHCPEVENTFUNCPTROFSOCKETOWNERCONFIG                                                   STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_LOCALIPADDRASSIGNMENTCHGFUNCPTROFSOCKETOWNERCONFIG                                    STD_ON
#define TCPIP_RXINDICATIONFUNCPTROFSOCKETOWNERCONFIG                                                STD_ON
#define TCPIP_TCPACCEPTEDFUNCPTROFSOCKETOWNERCONFIG                                                 STD_ON
#define TCPIP_TCPCONNECTEDFUNCPTROFSOCKETOWNERCONFIG                                                STD_ON
#define TCPIP_TCPIPEVENTFUNCPTROFSOCKETOWNERCONFIG                                                  STD_ON
#define TCPIP_TLSVALIDATIONRESULTFUNCPTROFSOCKETOWNERCONFIG                                         STD_ON
#define TCPIP_TXCONFIRMATIONFUNCPTROFSOCKETOWNERCONFIG                                              STD_ON
#define TCPIP_SOCKETTCPCONGCTRLDYN                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpCongCtrlDyn' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_SOCKETTCPDYN                                                                          STD_ON
#define TCPIP_BACKLOGARRAYOFSOCKETTCPDYN                                                            STD_ON
#define TCPIP_EVENTINDICATIONPENDINGOFSOCKETTCPDYN                                                  STD_ON
#define TCPIP_FINWAIT2TIMEOUTOFSOCKETTCPDYN                                                         STD_ON
#define TCPIP_IDLETIMEOUTSHORTOFSOCKETTCPDYN                                                        STD_ON
#define TCPIP_ISSOFSOCKETTCPDYN                                                                     STD_ON
#define TCPIP_MAXNUMLISTENSOCKETSOFSOCKETTCPDYN                                                     STD_ON
#define TCPIP_MSLTIMEOUTOFSOCKETTCPDYN                                                              STD_ON
#define TCPIP_PATHMTUCHANGEDOFSOCKETTCPDYN                                                          STD_ON
#define TCPIP_PATHMTUNEWSIZEOFSOCKETTCPDYN                                                          STD_ON
#define TCPIP_RCVNXTOFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_RCVWNDOFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_REQUESTEDRXBUFFERSIZEOFSOCKETTCPDYN                                                   STD_ON
#define TCPIP_REQUESTEDTXBUFFERSIZEOFSOCKETTCPDYN                                                   STD_ON
#define TCPIP_RETRANSMITTIMEOUTOFSOCKETTCPDYN                                                       STD_ON
#define TCPIP_RETRYQFILLNUMOFSOCKETTCPDYN                                                           STD_ON
#define TCPIP_RSTRECEIVEDOFSOCKETTCPDYN                                                             STD_ON
#define TCPIP_RTORELOADVALUEOFSOCKETTCPDYN                                                          STD_ON
#define TCPIP_RXBUFFERINDPOSOFSOCKETTCPDYN                                                          STD_ON
#define TCPIP_RXBUFFERREMINDLENOFSOCKETTCPDYN                                                       STD_ON
#define TCPIP_SNDNXTOFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_SNDUNAOFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_SNDWL1OFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_SNDWL2OFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_SNDWNDOFSOCKETTCPDYN                                                                  STD_ON
#define TCPIP_SOCKISSERVEROFSOCKETTCPDYN                                                            STD_ON
#define TCPIP_SOCKSTATENEXTOFSOCKETTCPDYN                                                           STD_ON
#define TCPIP_SOCKSTATEOFSOCKETTCPDYN                                                               STD_ON
#define TCPIP_SOCKETTCPDYNMASTERLISTENSOCKETIDXOFSOCKETTCPDYN                                       STD_ON
#define TCPIP_TCPRETRYQELEMENTFIRSTIDXOFSOCKETTCPDYN                                                STD_ON
#define TCPIP_TCPRETRYQELEMENTLASTIDXOFSOCKETTCPDYN                                                 STD_ON
#define TCPIP_TCPRXBUFFERDESCIDXOFSOCKETTCPDYN                                                      STD_ON
#define TCPIP_TCPTXBUFFERDESCIDXOFSOCKETTCPDYN                                                      STD_ON
#define TCPIP_TXFLAGSOFSOCKETTCPDYN                                                                 STD_ON
#define TCPIP_TXLENBYTETXOFSOCKETTCPDYN                                                             STD_ON
#define TCPIP_TXMAXSEGLENBYTEOFSOCKETTCPDYN                                                         STD_ON
#define TCPIP_TXMSSAGREEDOFSOCKETTCPDYN                                                             STD_ON
#define TCPIP_TXNEXTSENDSEQNOOFSOCKETTCPDYN                                                         STD_ON
#define TCPIP_TXONETIMEOPTSLENOFSOCKETTCPDYN                                                        STD_ON
#define TCPIP_TXONETIMEOPTSOFSOCKETTCPDYN                                                           STD_ON
#define TCPIP_TXOPTLENOFSOCKETTCPDYN                                                                STD_ON
#define TCPIP_TXREQDATABUFSTARTIDXOFSOCKETTCPDYN                                                    STD_ON
#define TCPIP_TXREQDATALENBYTEOFSOCKETTCPDYN                                                        STD_ON
#define TCPIP_TXREQFULLYQUEUEDOFSOCKETTCPDYN                                                        STD_ON
#define TCPIP_TXREQQUEUEDLENOFSOCKETTCPDYN                                                          STD_ON
#define TCPIP_TXREQSEQNOOFSOCKETTCPDYN                                                              STD_ON
#define TCPIP_TXTOTNOTQUEUEDLENOFSOCKETTCPDYN                                                       STD_ON
#define TCPIP_USETLSOFSOCKETTCPDYN                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpDyn.UseTls' Reason: 'TLS not present in configuration.' */
#define TCPIP_SOCKETTCPKEEPALIVEDYN                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpKeepAliveDyn' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_SOCKETTCPNAGLEDYN                                                                     STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpNagleDyn' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_SOCKETTCPOOODYN                                                                       STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpOooDyn' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_SOCKETTCPTSOPTDYN                                                                     STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpTsOptDyn' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_SOCKETTCPUTODYN                                                                       STD_OFF  /**< Deactivateable: 'TcpIp_SocketTcpUtoDyn' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_SOCKETUDPDYN                                                                          STD_ON
#define TCPIP_IPTXREQUESTDESCRIPTOROFSOCKETUDPDYN                                                   STD_ON
#define TCPIP_TXREQELEMLISTIDXOFSOCKETUDPDYN                                                        STD_ON
#define TCPIP_TXREQIPBUFPTROFSOCKETUDPDYN                                                           STD_ON
#define TCPIP_TXRETRQUEUEMAXNUMOFSOCKETUDPDYN                                                       STD_ON
#define TCPIP_TXRETRQUEUEOFSOCKETUDPDYN                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SPDENTRY                                                                              STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define TCPIP_IPSECHDRTYPEOFSPDENTRY                                                                STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.IpSecHdrType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_LOCIPV4ADDRENDOFSPDENTRY                                                              STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.LocIpV4AddrEnd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_LOCIPV4ADDRSTARTOFSPDENTRY                                                            STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.LocIpV4AddrStart' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_LOCPORTENDOFSPDENTRY                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.LocPortEnd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_LOCPORTSTARTOFSPDENTRY                                                                STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.LocPortStart' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_POLICYTYPEOFSPDENTRY                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.PolicyType' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_PRIORITYOFSPDENTRY                                                                    STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.Priority' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REMIPV4ADDRENDOFSPDENTRY                                                              STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.RemIpV4AddrEnd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REMIPV4ADDRSTARTOFSPDENTRY                                                            STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.RemIpV4AddrStart' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REMPORTENDOFSPDENTRY                                                                  STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.RemPortEnd' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_REMPORTSTARTOFSPDENTRY                                                                STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.RemPortStart' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_XPTPROTOCOLOFSPDENTRY                                                                 STD_OFF  /**< Deactivateable: 'TcpIp_SpdEntry.XptProtocol' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define TCPIP_TCPCONFIG                                                                             STD_ON
#define TCPIP_FINWAIT2TIMEOUTOFTCPCONFIG                                                            STD_ON
#define TCPIP_KEEPALIVEINTERVALOFTCPCONFIG                                                          STD_ON
#define TCPIP_KEEPALIVEPROBESMAXOFTCPCONFIG                                                         STD_ON
#define TCPIP_KEEPALIVETIMEOFTCPCONFIG                                                              STD_ON
#define TCPIP_MSLOFTCPCONFIG                                                                        STD_ON
#define TCPIP_NAGLETIMEOUTOFTCPCONFIG                                                               STD_ON
#define TCPIP_RETRANSTIMEOUTMAXOFTCPCONFIG                                                          STD_ON
#define TCPIP_RETRANSTIMEOUTOFTCPCONFIG                                                             STD_ON
#define TCPIP_RXMSSOFTCPCONFIG                                                                      STD_ON
#define TCPIP_TCPOOOQSIZEPERSOCKETAVGOFTCPCONFIG                                                    STD_ON
#define TCPIP_TCPOOOQSIZEPERSOCKETMAXOFTCPCONFIG                                                    STD_ON
#define TCPIP_TCPRETRYQSIZEOFTCPCONFIG                                                              STD_ON
#define TCPIP_TIMETOLIVEDEFAULTOFTCPCONFIG                                                          STD_ON
#define TCPIP_TXMSSOFTCPCONFIG                                                                      STD_ON
#define TCPIP_USERTIMEOUTDEFCYCLESOFTCPCONFIG                                                       STD_ON
#define TCPIP_USERTIMEOUTMAXCYCLESOFTCPCONFIG                                                       STD_ON
#define TCPIP_USERTIMEOUTMINCYCLESOFTCPCONFIG                                                       STD_ON
#define TCPIP_TCPOOOQELEMENT                                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TCPRESETQELEMENT                                                                      STD_ON
#define TCPIP_TCPRETRYQELEMENT                                                                      STD_ON
#define TCPIP_TCPRXBUFFER                                                                           STD_ON
#define TCPIP_TCPRXBUFFERDESC                                                                       STD_ON
#define TCPIP_TCPRXBUFFERENDIDXOFTCPRXBUFFERDESC                                                    STD_ON
#define TCPIP_TCPRXBUFFERLENGTHOFTCPRXBUFFERDESC                                                    STD_ON
#define TCPIP_TCPRXBUFFERSTARTIDXOFTCPRXBUFFERDESC                                                  STD_ON
#define TCPIP_TCPRXBUFFERDESCDYN                                                                    STD_ON
#define TCPIP_FILLLEVELOFTCPRXBUFFERDESCDYN                                                         STD_ON
#define TCPIP_SOCKETTCPDYNIDXOFTCPRXBUFFERDESCDYN                                                   STD_ON
#define TCPIP_TCPRXBUFFERWRITEIDXOFTCPRXBUFFERDESCDYN                                               STD_ON
#define TCPIP_TCPTXBUFFER                                                                           STD_ON
#define TCPIP_TCPTXBUFFERDESC                                                                       STD_ON
#define TCPIP_TCPTXBUFFERENDIDXOFTCPTXBUFFERDESC                                                    STD_ON
#define TCPIP_TCPTXBUFFERLENGTHOFTCPTXBUFFERDESC                                                    STD_ON
#define TCPIP_TCPTXBUFFERSTARTIDXOFTCPTXBUFFERDESC                                                  STD_ON
#define TCPIP_TCPTXBUFFERDESCDYN                                                                    STD_ON
#define TCPIP_FILLLEVELOFTCPTXBUFFERDESCDYN                                                         STD_ON
#define TCPIP_SOCKETTCPDYNIDXOFTCPTXBUFFERDESCDYN                                                   STD_ON
#define TCPIP_TCPTXBUFFERWRITEIDXOFTCPTXBUFFERDESCDYN                                               STD_ON
#define TCPIP_TXREQELEM                                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMDATALENBYTEOFTXREQELEM                                                       STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMTRANSMITTEDOFTXREQELEM                                                       STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMLIST                                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMENDIDXOFTXREQELEMLIST                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMLENGTHOFTXREQELEMLIST                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMSTARTIDXOFTXREQELEMLIST                                                      STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMLISTDYN                                                                      STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_FILLNUMOFTXREQELEMLISTDYN                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_READPOSOFTXREQELEMLISTDYN                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SOCKETUDPDYNIDXOFTXREQELEMLISTDYN                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_WRITEPOSOFTXREQELEMLISTDYN                                                            STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_UDPTXRETRYQUEUEELEMENTCHAIN                                                           STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_UDPTXRETRYQUEUEELEMENTS                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_UDPTXRETRYQUEUEPOOLDESC                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_PCCONFIG                                                                              STD_ON
#define TCPIP_DEFAULTADDRV6OFPCCONFIG                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONBUFFEROFPCCONFIG                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONDYNOFPCCONFIG                                                           STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DHCPUSEROPTIONOFPCCONFIG                                                              STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_DUPLICATEADDRDETECTIONFCTPTROFPCCONFIG                                                STD_ON
#define TCPIP_ETHIFCTRLOFPCCONFIG                                                                   STD_ON
#define TCPIP_FINALMAGICNUMBEROFPCCONFIG                                                            STD_OFF  /**< Deactivateable: 'TcpIp_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define TCPIP_ICMPV6CONFIGOFPCCONFIG                                                                STD_ON
#define TCPIP_ICMPV6MSGHANDLERCBKFCTPTROFPCCONFIG                                                   STD_ON
#define TCPIP_ICMPV6TXMSGBUFFEROFPCCONFIG                                                           STD_ON
#define TCPIP_INITDATAHASHCODEOFPCCONFIG                                                            STD_OFF  /**< Deactivateable: 'TcpIp_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define TCPIP_INTERFACEIDENTIFIEROFPCCONFIG                                                         STD_ON
#define TCPIP_IPV6CTRLDYNOFPCCONFIG                                                                 STD_ON
#define TCPIP_IPV6CTRLOFPCCONFIG                                                                    STD_ON
#define TCPIP_IPV6DEFAULTROUTERLISTENTRYOFPCCONFIG                                                  STD_ON
#define TCPIP_IPV6DESTINATIONCACHEENTRYOFPCCONFIG                                                   STD_ON
#define TCPIP_IPV6ETHBUFDATAOFPCCONFIG                                                              STD_ON
#define TCPIP_IPV6GENERALOFPCCONFIG                                                                 STD_ON
#define TCPIP_IPV6MULTICASTADDRACTIVEOFPCCONFIG                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_IPV6MULTICASTADDROFPCCONFIG                                                           STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_IPV6NEIGHBORCACHEENTRYOFPCCONFIG                                                      STD_ON
#define TCPIP_IPV6PREFIXLISTENTRYOFPCCONFIG                                                         STD_ON
#define TCPIP_IPV6SOCKETDYNOFPCCONFIG                                                               STD_ON
#define TCPIP_IPV6SOURCEADDRESSOFPCCONFIG                                                           STD_ON
#define TCPIP_IPV6SOURCEADDRESSTABLEENTRYOFPCCONFIG                                                 STD_ON
#define TCPIP_LOCALADDROFPCCONFIG                                                                   STD_ON
#define TCPIP_LOCALADDRV6OFPCCONFIG                                                                 STD_ON
#define TCPIP_NDPCONFIGOFPCCONFIG                                                                   STD_ON
#define TCPIP_PHYSADDRCONFIGOFPCCONFIG                                                              STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_RANDOMNUMBERFCTPTROFPCCONFIG                                                          STD_ON
#define TCPIP_SIZEOFDEFAULTADDRV6OFPCCONFIG                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFDHCPUSEROPTIONBUFFEROFPCCONFIG                                                  STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFDHCPUSEROPTIONDYNOFPCCONFIG                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFDHCPUSEROPTIONOFPCCONFIG                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFETHIFCTRLOFPCCONFIG                                                             STD_ON
#define TCPIP_SIZEOFICMPV6CONFIGOFPCCONFIG                                                          STD_ON
#define TCPIP_SIZEOFICMPV6TXMSGBUFFEROFPCCONFIG                                                     STD_ON
#define TCPIP_SIZEOFINTERFACEIDENTIFIEROFPCCONFIG                                                   STD_ON
#define TCPIP_SIZEOFIPV6CTRLDYNOFPCCONFIG                                                           STD_ON
#define TCPIP_SIZEOFIPV6CTRLOFPCCONFIG                                                              STD_ON
#define TCPIP_SIZEOFIPV6DEFAULTROUTERLISTENTRYOFPCCONFIG                                            STD_ON
#define TCPIP_SIZEOFIPV6DESTINATIONCACHEENTRYOFPCCONFIG                                             STD_ON
#define TCPIP_SIZEOFIPV6ETHBUFDATAOFPCCONFIG                                                        STD_ON
#define TCPIP_SIZEOFIPV6GENERALOFPCCONFIG                                                           STD_ON
#define TCPIP_SIZEOFIPV6MULTICASTADDRACTIVEOFPCCONFIG                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFIPV6MULTICASTADDROFPCCONFIG                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFIPV6NEIGHBORCACHEENTRYOFPCCONFIG                                                STD_ON
#define TCPIP_SIZEOFIPV6PREFIXLISTENTRYOFPCCONFIG                                                   STD_ON
#define TCPIP_SIZEOFIPV6SOCKETDYNOFPCCONFIG                                                         STD_ON
#define TCPIP_SIZEOFIPV6SOURCEADDRESSOFPCCONFIG                                                     STD_ON
#define TCPIP_SIZEOFIPV6SOURCEADDRESSTABLEENTRYOFPCCONFIG                                           STD_ON
#define TCPIP_SIZEOFLOCALADDROFPCCONFIG                                                             STD_ON
#define TCPIP_SIZEOFLOCALADDRV6OFPCCONFIG                                                           STD_ON
#define TCPIP_SIZEOFNDPCONFIGOFPCCONFIG                                                             STD_ON
#define TCPIP_SIZEOFPHYSADDRCONFIGOFPCCONFIG                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFSOCKETDYNOFPCCONFIG                                                             STD_ON
#define TCPIP_SIZEOFSOCKETOWNERCONFIGOFPCCONFIG                                                     STD_ON
#define TCPIP_SIZEOFSOCKETTCPDYNOFPCCONFIG                                                          STD_ON
#define TCPIP_SIZEOFSOCKETUDPDYNOFPCCONFIG                                                          STD_ON
#define TCPIP_SIZEOFTCPCONFIGOFPCCONFIG                                                             STD_ON
#define TCPIP_SIZEOFTCPOOOQELEMENTOFPCCONFIG                                                        STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFTCPRESETQELEMENTOFPCCONFIG                                                      STD_ON
#define TCPIP_SIZEOFTCPRETRYQELEMENTOFPCCONFIG                                                      STD_ON
#define TCPIP_SIZEOFTCPRXBUFFERDESCDYNOFPCCONFIG                                                    STD_ON
#define TCPIP_SIZEOFTCPRXBUFFERDESCOFPCCONFIG                                                       STD_ON
#define TCPIP_SIZEOFTCPRXBUFFEROFPCCONFIG                                                           STD_ON
#define TCPIP_SIZEOFTCPTXBUFFERDESCDYNOFPCCONFIG                                                    STD_ON
#define TCPIP_SIZEOFTCPTXBUFFERDESCOFPCCONFIG                                                       STD_ON
#define TCPIP_SIZEOFTCPTXBUFFEROFPCCONFIG                                                           STD_ON
#define TCPIP_SIZEOFTXREQELEMLISTDYNOFPCCONFIG                                                      STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFTXREQELEMLISTOFPCCONFIG                                                         STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFTXREQELEMOFPCCONFIG                                                             STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFUDPTXRETRYQUEUEELEMENTCHAINOFPCCONFIG                                           STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFUDPTXRETRYQUEUEELEMENTSOFPCCONFIG                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SIZEOFUDPTXRETRYQUEUEPOOLDESCOFPCCONFIG                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_SOCKETDYNOFPCCONFIG                                                                   STD_ON
#define TCPIP_SOCKETOWNERCONFIGOFPCCONFIG                                                           STD_ON
#define TCPIP_SOCKETTCPDYNOFPCCONFIG                                                                STD_ON
#define TCPIP_SOCKETUDPDYNOFPCCONFIG                                                                STD_ON
#define TCPIP_TCPCONFIGOFPCCONFIG                                                                   STD_ON
#define TCPIP_TCPOOOQELEMENTOFPCCONFIG                                                              STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TCPRESETQELEMENTOFPCCONFIG                                                            STD_ON
#define TCPIP_TCPRETRYQELEMENTOFPCCONFIG                                                            STD_ON
#define TCPIP_TCPRXBUFFERDESCDYNOFPCCONFIG                                                          STD_ON
#define TCPIP_TCPRXBUFFERDESCOFPCCONFIG                                                             STD_ON
#define TCPIP_TCPRXBUFFEROFPCCONFIG                                                                 STD_ON
#define TCPIP_TCPTXBUFFERDESCDYNOFPCCONFIG                                                          STD_ON
#define TCPIP_TCPTXBUFFERDESCOFPCCONFIG                                                             STD_ON
#define TCPIP_TCPTXBUFFEROFPCCONFIG                                                                 STD_ON
#define TCPIP_TXREQELEMLISTDYNOFPCCONFIG                                                            STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMLISTOFPCCONFIG                                                               STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_TXREQELEMOFPCCONFIG                                                                   STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_UDPTXRETRYQUEUEELEMENTCHAINOFPCCONFIG                                                 STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_UDPTXRETRYQUEUEELEMENTSOFPCCONFIG                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
#define TCPIP_UDPTXRETRYQUEUEPOOLDESCOFPCCONFIG                                                     STD_ON  /**< This preprocessing switch is always STD_ON because the PrecompilePreprocessingStrategy is RUNTIME_CHECKING and feature deactivation conditions are not available. */
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCMinNumericValueDefines  TcpIp Min Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the minimum value in numerical based data.
  \{
*/ 
#define TCPIP_MIN_DHCPUSEROPTIONBUFFER                                                              0u
#define TCPIP_MIN_LENGTHOFDHCPUSEROPTIONDYN                                                         0u
#define TCPIP_MIN_ICMPV6TXMSGBUFFER                                                                 0u
#define TCPIP_MIN_INTERFACEIDENTIFIER                                                               0u
#define TCPIP_MIN_BASEREACHABLETIMEMSOFIPV6CTRLDYN                                                  0u
#define TCPIP_MIN_CTRLPREVIOUSSTATEOFIPV6CTRLDYN                                                    0u
#define TCPIP_MIN_CTRLSTATEOFIPV6CTRLDYN                                                            0u
#define TCPIP_MIN_CURHOPLIMITOFIPV6CTRLDYN                                                          0u
#define TCPIP_MIN_DEFAULTLINKMTUOFIPV6CTRLDYN                                                       0u
#define TCPIP_MIN_IPV6DEFAULTROUTERLISTENTRYVALIDENDIDXOFIPV6CTRLDYN                                0u
#define TCPIP_MIN_IPV6DESTINATIONCACHEENTRYVALIDENDIDXOFIPV6CTRLDYN                                 0u
#define TCPIP_MIN_IPV6NEIGHBORCACHEENTRYVALIDENDIDXOFIPV6CTRLDYN                                    0u
#define TCPIP_MIN_IPV6PREFIXLISTENTRYVALIDENDIDXOFIPV6CTRLDYN                                       0u
#define TCPIP_MIN_NDP_ROUTERSOLICITATIONTXCOUNTOFIPV6CTRLDYN                                        0u
#define TCPIP_MIN_REACHABLETIMEMSOFIPV6CTRLDYN                                                      0u
#define TCPIP_MIN_RETRANSTIMERMSOFIPV6CTRLDYN                                                       0u
#define TCPIP_MIN_IPV6SOCKETDYNIDXOFIPV6ETHBUFDATA                                                  0u
#define TCPIP_MIN_ULTXREQTABIDXOFIPV6ETHBUFDATA                                                     0u
#define TCPIP_MIN_ETHIFFRAMEPRIOOFIPV6SOCKETDYN                                                     0u
#define TCPIP_MIN_FLAGSOFIPV6SOCKETDYN                                                              0u
#define TCPIP_MIN_HOPLIMITOFIPV6SOCKETDYN                                                           0u
#define TCPIP_MIN_IPV6DESTINATIONCACHEENTRYHINTIDXOFIPV6SOCKETDYN                                   0u
#define TCPIP_MIN_IPV6HDRVERSIONTCFLNBOOFIPV6SOCKETDYN                                              0u
#define TCPIP_MIN_ASSIGNMENTSTATEOFLOCALADDR                                                        0u
#define TCPIP_MIN_LISTENACTIVECONNSTATOFSOCKETDYN                                                   0u
#define TCPIP_MIN_LOCALADDRBINDIDXOFSOCKETDYN                                                       0u
#define TCPIP_MIN_SOCKETOWNERCONFIGIDXOFSOCKETDYN                                                   0u
#define TCPIP_MIN_TXIPADDRIDXOFSOCKETDYN                                                            0u
#define TCPIP_MIN_EVENTINDICATIONPENDINGOFSOCKETTCPDYN                                              0u
#define TCPIP_MIN_FINWAIT2TIMEOUTOFSOCKETTCPDYN                                                     0u
#define TCPIP_MIN_IDLETIMEOUTSHORTOFSOCKETTCPDYN                                                    0u
#define TCPIP_MIN_ISSOFSOCKETTCPDYN                                                                 0u
#define TCPIP_MIN_MAXNUMLISTENSOCKETSOFSOCKETTCPDYN                                                 0u
#define TCPIP_MIN_MSLTIMEOUTOFSOCKETTCPDYN                                                          0u
#define TCPIP_MIN_PATHMTUNEWSIZEOFSOCKETTCPDYN                                                      0u
#define TCPIP_MIN_RCVNXTOFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_RCVWNDOFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_RETRANSMITTIMEOUTOFSOCKETTCPDYN                                                   0u
#define TCPIP_MIN_RETRYQFILLNUMOFSOCKETTCPDYN                                                       0u
#define TCPIP_MIN_RTORELOADVALUEOFSOCKETTCPDYN                                                      0u
#define TCPIP_MIN_SNDNXTOFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_SNDUNAOFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_SNDWL1OFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_SNDWL2OFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_SNDWNDOFSOCKETTCPDYN                                                              0u
#define TCPIP_MIN_SOCKSTATENEXTOFSOCKETTCPDYN                                                       0u
#define TCPIP_MIN_SOCKSTATEOFSOCKETTCPDYN                                                           0u
#define TCPIP_MIN_SOCKETTCPDYNMASTERLISTENSOCKETIDXOFSOCKETTCPDYN                                   0u
#define TCPIP_MIN_TCPRETRYQELEMENTFIRSTIDXOFSOCKETTCPDYN                                            0u
#define TCPIP_MIN_TCPRETRYQELEMENTLASTIDXOFSOCKETTCPDYN                                             0u
#define TCPIP_MIN_TCPRXBUFFERDESCIDXOFSOCKETTCPDYN                                                  0u
#define TCPIP_MIN_TCPTXBUFFERDESCIDXOFSOCKETTCPDYN                                                  0u
#define TCPIP_MIN_TXFLAGSOFSOCKETTCPDYN                                                             0u
#define TCPIP_MIN_TXMAXSEGLENBYTEOFSOCKETTCPDYN                                                     0u
#define TCPIP_MIN_TXMSSAGREEDOFSOCKETTCPDYN                                                         0u
#define TCPIP_MIN_TXNEXTSENDSEQNOOFSOCKETTCPDYN                                                     0u
#define TCPIP_MIN_TXONETIMEOPTSLENOFSOCKETTCPDYN                                                    0u
#define TCPIP_MIN_TXONETIMEOPTSOFSOCKETTCPDYN                                                       0u
#define TCPIP_MIN_TXOPTLENOFSOCKETTCPDYN                                                            0u
#define TCPIP_MIN_TXREQSEQNOOFSOCKETTCPDYN                                                          0u
#define TCPIP_MIN_TXREQELEMLISTIDXOFSOCKETUDPDYN                                                    0u
#define TCPIP_MIN_TXRETRQUEUEMAXNUMOFSOCKETUDPDYN                                                   0u
#define TCPIP_MIN_TCPRXBUFFER                                                                       0u
#define TCPIP_MIN_SOCKETTCPDYNIDXOFTCPRXBUFFERDESCDYN                                               0u
#define TCPIP_MIN_TCPRXBUFFERWRITEIDXOFTCPRXBUFFERDESCDYN                                           0u
#define TCPIP_MIN_TCPTXBUFFER                                                                       0u
#define TCPIP_MIN_SOCKETTCPDYNIDXOFTCPTXBUFFERDESCDYN                                               0u
#define TCPIP_MIN_TCPTXBUFFERWRITEIDXOFTCPTXBUFFERDESCDYN                                           0u
#define TCPIP_MIN_TXREQELEMDATALENBYTEOFTXREQELEM                                                   0u
#define TCPIP_MIN_FILLNUMOFTXREQELEMLISTDYN                                                         0u
#define TCPIP_MIN_READPOSOFTXREQELEMLISTDYN                                                         0u
#define TCPIP_MIN_SOCKETUDPDYNIDXOFTXREQELEMLISTDYN                                                 0u
#define TCPIP_MIN_WRITEPOSOFTXREQELEMLISTDYN                                                        0u
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCMaxNumericValueDefines  TcpIp Max Numeric Value Defines (PRE_COMPILE)
  \brief  These defines are used to implement against the maximum value in numerical based data.
  \{
*/ 
#define TCPIP_MAX_DHCPUSEROPTIONBUFFER                                                              255u
#define TCPIP_MAX_LENGTHOFDHCPUSEROPTIONDYN                                                         65535u
#define TCPIP_MAX_ICMPV6TXMSGBUFFER                                                                 255u
#define TCPIP_MAX_INTERFACEIDENTIFIER                                                               255u
#define TCPIP_MAX_BASEREACHABLETIMEMSOFIPV6CTRLDYN                                                  4294967295u
#define TCPIP_MAX_CTRLPREVIOUSSTATEOFIPV6CTRLDYN                                                    255u
#define TCPIP_MAX_CTRLSTATEOFIPV6CTRLDYN                                                            255u
#define TCPIP_MAX_CURHOPLIMITOFIPV6CTRLDYN                                                          255u
#define TCPIP_MAX_DEFAULTLINKMTUOFIPV6CTRLDYN                                                       65535u
#define TCPIP_MAX_IPV6DEFAULTROUTERLISTENTRYVALIDENDIDXOFIPV6CTRLDYN                                255u
#define TCPIP_MAX_IPV6DESTINATIONCACHEENTRYVALIDENDIDXOFIPV6CTRLDYN                                 255u
#define TCPIP_MAX_IPV6NEIGHBORCACHEENTRYVALIDENDIDXOFIPV6CTRLDYN                                    255u
#define TCPIP_MAX_IPV6PREFIXLISTENTRYVALIDENDIDXOFIPV6CTRLDYN                                       255u
#define TCPIP_MAX_NDP_ROUTERSOLICITATIONTXCOUNTOFIPV6CTRLDYN                                        255u
#define TCPIP_MAX_REACHABLETIMEMSOFIPV6CTRLDYN                                                      4294967295u
#define TCPIP_MAX_RETRANSTIMERMSOFIPV6CTRLDYN                                                       4294967295u
#define TCPIP_MAX_IPV6SOCKETDYNIDXOFIPV6ETHBUFDATA                                                  255u
#define TCPIP_MAX_ULTXREQTABIDXOFIPV6ETHBUFDATA                                                     255u
#define TCPIP_MAX_ETHIFFRAMEPRIOOFIPV6SOCKETDYN                                                     255u
#define TCPIP_MAX_FLAGSOFIPV6SOCKETDYN                                                              255u
#define TCPIP_MAX_HOPLIMITOFIPV6SOCKETDYN                                                           255u
#define TCPIP_MAX_IPV6DESTINATIONCACHEENTRYHINTIDXOFIPV6SOCKETDYN                                   255u
#define TCPIP_MAX_IPV6HDRVERSIONTCFLNBOOFIPV6SOCKETDYN                                              4294967295u
#define TCPIP_MAX_ASSIGNMENTSTATEOFLOCALADDR                                                        255u
#define TCPIP_MAX_LISTENACTIVECONNSTATOFSOCKETDYN                                                   255u
#define TCPIP_MAX_LOCALADDRBINDIDXOFSOCKETDYN                                                       255u
#define TCPIP_MAX_SOCKETOWNERCONFIGIDXOFSOCKETDYN                                                   255u
#define TCPIP_MAX_TXIPADDRIDXOFSOCKETDYN                                                            255u
#define TCPIP_MAX_EVENTINDICATIONPENDINGOFSOCKETTCPDYN                                              255u
#define TCPIP_MAX_FINWAIT2TIMEOUTOFSOCKETTCPDYN                                                     4294967295u
#define TCPIP_MAX_IDLETIMEOUTSHORTOFSOCKETTCPDYN                                                    4294967295u
#define TCPIP_MAX_ISSOFSOCKETTCPDYN                                                                 4294967295u
#define TCPIP_MAX_MAXNUMLISTENSOCKETSOFSOCKETTCPDYN                                                 65535u
#define TCPIP_MAX_MSLTIMEOUTOFSOCKETTCPDYN                                                          4294967295u
#define TCPIP_MAX_PATHMTUNEWSIZEOFSOCKETTCPDYN                                                      65535u
#define TCPIP_MAX_RCVNXTOFSOCKETTCPDYN                                                              4294967295u
#define TCPIP_MAX_RCVWNDOFSOCKETTCPDYN                                                              65535u
#define TCPIP_MAX_RETRANSMITTIMEOUTOFSOCKETTCPDYN                                                   4294967295u
#define TCPIP_MAX_RETRYQFILLNUMOFSOCKETTCPDYN                                                       255u
#define TCPIP_MAX_RTORELOADVALUEOFSOCKETTCPDYN                                                      4294967295u
#define TCPIP_MAX_SNDNXTOFSOCKETTCPDYN                                                              4294967295u
#define TCPIP_MAX_SNDUNAOFSOCKETTCPDYN                                                              4294967295u
#define TCPIP_MAX_SNDWL1OFSOCKETTCPDYN                                                              4294967295u
#define TCPIP_MAX_SNDWL2OFSOCKETTCPDYN                                                              4294967295u
#define TCPIP_MAX_SNDWNDOFSOCKETTCPDYN                                                              65535u
#define TCPIP_MAX_SOCKSTATENEXTOFSOCKETTCPDYN                                                       255u
#define TCPIP_MAX_SOCKSTATEOFSOCKETTCPDYN                                                           255u
#define TCPIP_MAX_SOCKETTCPDYNMASTERLISTENSOCKETIDXOFSOCKETTCPDYN                                   255u
#define TCPIP_MAX_TCPRETRYQELEMENTFIRSTIDXOFSOCKETTCPDYN                                            255u
#define TCPIP_MAX_TCPRETRYQELEMENTLASTIDXOFSOCKETTCPDYN                                             255u
#define TCPIP_MAX_TCPRXBUFFERDESCIDXOFSOCKETTCPDYN                                                  255u
#define TCPIP_MAX_TCPTXBUFFERDESCIDXOFSOCKETTCPDYN                                                  255u
#define TCPIP_MAX_TXFLAGSOFSOCKETTCPDYN                                                             255u
#define TCPIP_MAX_TXMAXSEGLENBYTEOFSOCKETTCPDYN                                                     65535u
#define TCPIP_MAX_TXMSSAGREEDOFSOCKETTCPDYN                                                         65535u
#define TCPIP_MAX_TXNEXTSENDSEQNOOFSOCKETTCPDYN                                                     4294967295u
#define TCPIP_MAX_TXONETIMEOPTSLENOFSOCKETTCPDYN                                                    255u
#define TCPIP_MAX_TXONETIMEOPTSOFSOCKETTCPDYN                                                       255u
#define TCPIP_MAX_TXOPTLENOFSOCKETTCPDYN                                                            255u
#define TCPIP_MAX_TXREQSEQNOOFSOCKETTCPDYN                                                          4294967295u
#define TCPIP_MAX_TXREQELEMLISTIDXOFSOCKETUDPDYN                                                    255u
#define TCPIP_MAX_TXRETRQUEUEMAXNUMOFSOCKETUDPDYN                                                   255u
#define TCPIP_MAX_TCPRXBUFFER                                                                       255u
#define TCPIP_MAX_SOCKETTCPDYNIDXOFTCPRXBUFFERDESCDYN                                               255u
#define TCPIP_MAX_TCPRXBUFFERWRITEIDXOFTCPRXBUFFERDESCDYN                                           65535u
#define TCPIP_MAX_TCPTXBUFFER                                                                       255u
#define TCPIP_MAX_SOCKETTCPDYNIDXOFTCPTXBUFFERDESCDYN                                               255u
#define TCPIP_MAX_TCPTXBUFFERWRITEIDXOFTCPTXBUFFERDESCDYN                                           65535u
#define TCPIP_MAX_TXREQELEMDATALENBYTEOFTXREQELEM                                                   65535u
#define TCPIP_MAX_FILLNUMOFTXREQELEMLISTDYN                                                         255u
#define TCPIP_MAX_READPOSOFTXREQELEMLISTDYN                                                         255u
#define TCPIP_MAX_SOCKETUDPDYNIDXOFTXREQELEMLISTDYN                                                 255u
#define TCPIP_MAX_WRITEPOSOFTXREQELEMLISTDYN                                                        255u
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCNoReferenceDefines  TcpIp No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define TCPIP_NO_IPV6CTRLIDXOFETHIFCTRL                                                             255u
#define TCPIP_NO_DHCPUSEROPTIONENDIDXOFIPV6CTRL                                                     255u
#define TCPIP_NO_DHCPUSEROPTIONSTARTIDXOFIPV6CTRL                                                   255u
#define TCPIP_NO_IPV6MULTICASTADDRENDIDXOFIPV6CTRL                                                  255u
#define TCPIP_NO_IPV6MULTICASTADDRSTARTIDXOFIPV6CTRL                                                255u
#define TCPIP_NO_LOCALADDRV6BCIDXOFIPV6CTRL                                                         255u
#define TCPIP_NO_IPV6SOCKETDYNIDXOFIPV6ETHBUFDATA                                                   255u
#define TCPIP_NO_IPV6CTRLDEFAULTIDXOFIPV6GENERAL                                                    255u
#define TCPIP_NO_IPV6SOCKETDYNTCPRSTIDXOFIPV6GENERAL                                                255u
#define TCPIP_NO_DEFAULTADDRV6IDXOFIPV6MULTICASTADDR                                                255u
#define TCPIP_NO_IPV6MULTICASTADDRACTIVEIDXOFIPV6MULTICASTADDR                                      255u
#define TCPIP_NO_IPV6DESTINATIONCACHEENTRYHINTIDXOFIPV6SOCKETDYN                                    255u
#define TCPIP_NO_DEFAULTADDRV6IDXOFIPV6SOURCEADDRESS                                                255u
#define TCPIP_NO_IPV6MULTICASTADDRIDXOFLOCALADDRV6                                                  255u
#define TCPIP_NO_IPV6SOURCEADDRESSIDXOFLOCALADDRV6                                                  255u
#define TCPIP_NO_LOCALADDRBINDIDXOFSOCKETDYN                                                        255u
#define TCPIP_NO_SOCKETOWNERCONFIGIDXOFSOCKETDYN                                                    255u
#define TCPIP_NO_SOCKETTCPDYNMASTERLISTENSOCKETIDXOFSOCKETTCPDYN                                    255u
#define TCPIP_NO_TCPRETRYQELEMENTFIRSTIDXOFSOCKETTCPDYN                                             255u
#define TCPIP_NO_TCPRETRYQELEMENTLASTIDXOFSOCKETTCPDYN                                              255u
#define TCPIP_NO_TCPRXBUFFERDESCIDXOFSOCKETTCPDYN                                                   255u
#define TCPIP_NO_TCPTXBUFFERDESCIDXOFSOCKETTCPDYN                                                   255u
#define TCPIP_NO_TXREQELEMLISTIDXOFSOCKETUDPDYN                                                     255u
#define TCPIP_NO_NAGLETIMEOUTOFTCPCONFIG                                                            255u
#define TCPIP_NO_USERTIMEOUTMAXCYCLESOFTCPCONFIG                                                    255u
#define TCPIP_NO_USERTIMEOUTMINCYCLESOFTCPCONFIG                                                    255u
#define TCPIP_NO_SOCKETTCPDYNIDXOFTCPRXBUFFERDESCDYN                                                255u
#define TCPIP_NO_SOCKETTCPDYNIDXOFTCPTXBUFFERDESCDYN                                                255u
#define TCPIP_NO_SOCKETUDPDYNIDXOFTXREQELEMLISTDYN                                                  255u
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCMaskedBitDefines  TcpIp Masked Bit Defines (PRE_COMPILE)
  \brief  These defines are masks to extract packed boolean data.
  \{
*/ 
#define TCPIP_ALLOWLINKMTURECONFIGURATIONOFIPV6CTRL_MASK                                            0x0800u
#define TCPIP_DHCPUSEROPTIONUSEDOFIPV6CTRL_MASK                                                     0x0400u
#define TCPIP_ENABLEDYNHOPLIMITOFIPV6CTRL_MASK                                                      0x0200u
#define TCPIP_ENABLEPATHMTUOFIPV6CTRL_MASK                                                          0x0100u
#define TCPIP_HWCHECKSUMICMPOFIPV6CTRL_MASK                                                         0x0080u
#define TCPIP_HWCHECKSUMIPDESTINATIONOPTIONSOFIPV6CTRL_MASK                                         0x0040u
#define TCPIP_HWCHECKSUMIPHOPBYHOPOPTIONSOFIPV6CTRL_MASK                                            0x0020u
#define TCPIP_HWCHECKSUMIPROUTINGOFIPV6CTRL_MASK                                                    0x0010u
#define TCPIP_HWCHECKSUMTCPOFIPV6CTRL_MASK                                                          0x0008u
#define TCPIP_HWCHECKSUMUDPOFIPV6CTRL_MASK                                                          0x0004u
#define TCPIP_IPV6MULTICASTADDRUSEDOFIPV6CTRL_MASK                                                  0x0002u
#define TCPIP_LOCALADDRV6BCUSEDOFIPV6CTRL_MASK                                                      0x0001u
#define TCPIP_EXTDESTADDRVALIDATIONENABLEDOFIPV6GENERAL_MASK                                        0x04u
#define TCPIP_IPV6CTRLDEFAULTUSEDOFIPV6GENERAL_MASK                                                 0x02u
#define TCPIP_IPV6SOCKETDYNTCPRSTUSEDOFIPV6GENERAL_MASK                                             0x01u
#define TCPIP_DEFAULTADDRV6USEDOFIPV6MULTICASTADDR_MASK                                             0x02u
#define TCPIP_IPV6MULTICASTADDRACTIVEUSEDOFIPV6MULTICASTADDR_MASK                                   0x01u
#define TCPIP_DEFAULTADDRV6USEDOFIPV6SOURCEADDRESS_MASK                                             0x02u
#define TCPIP_PREFIXISONLINKOFIPV6SOURCEADDRESS_MASK                                                0x01u
#define TCPIP_IPV6MULTICASTADDRUSEDOFLOCALADDRV6_MASK                                               0x02u
#define TCPIP_IPV6SOURCEADDRESSUSEDOFLOCALADDRV6_MASK                                               0x01u
#define TCPIP_DYNAMICREACHABLETIMEOFNDPCONFIG_MASK                                                  0x0800u
#define TCPIP_DYNAMICRETRANSTIMEROFNDPCONFIG_MASK                                                   0x0400u
#define TCPIP_ENABLENDPINVNANCUPDATEOFNDPCONFIG_MASK                                                0x0200u
#define TCPIP_ENABLENDPINVNAOFNDPCONFIG_MASK                                                        0x0100u
#define TCPIP_ENABLENDPINVNSOFNDPCONFIG_MASK                                                        0x0080u
#define TCPIP_ENABLENUDOFNDPCONFIG_MASK                                                             0x0040u
#define TCPIP_ENABLEOPTIMISTICDADOFNDPCONFIG_MASK                                                   0x0020u
#define TCPIP_ENABLERFC6106DNSSLOPTOFNDPCONFIG_MASK                                                 0x0010u
#define TCPIP_ENABLERFC6106RDNSSOPTOFNDPCONFIG_MASK                                                 0x0008u
#define TCPIP_ENABLESLAACDELAYOFNDPCONFIG_MASK                                                      0x0004u
#define TCPIP_RANDOMREACHABLETIMEOFNDPCONFIG_MASK                                                   0x0002u
#define TCPIP_RNDRTRSOLICITATIONDELAYOFNDPCONFIG_MASK                                               0x0001u
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCIsReducedToDefineDefines  TcpIp Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define TCPIP_ISDEF_DEFAULTADDRV6                                                                   STD_OFF
#define TCPIP_ISDEF_CODEOFDHCPUSEROPTION                                                            STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONBUFFERENDIDXOFDHCPUSEROPTION                                      STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONBUFFERLENGTHOFDHCPUSEROPTION                                      STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONBUFFERSTARTIDXOFDHCPUSEROPTION                                    STD_OFF
#define TCPIP_ISDEF_DIRECTIONOFDHCPUSEROPTION                                                       STD_OFF
#define TCPIP_ISDEF_IPV6CTRLIDXOFETHIFCTRL                                                          STD_OFF
#define TCPIP_ISDEF_IPV6CTRLUSEDOFETHIFCTRL                                                         STD_OFF
#define TCPIP_ISDEF_ECHOREQUESTAPIOFICMPV6CONFIG                                                    STD_OFF
#define TCPIP_ISDEF_HOPLIMITOFICMPV6CONFIG                                                          STD_OFF
#define TCPIP_ISDEF_ALLOWLINKMTURECONFIGURATIONOFIPV6CTRL                                           STD_OFF
#define TCPIP_ISDEF_DEFAULTHOPLIMITOFIPV6CTRL                                                       STD_OFF
#define TCPIP_ISDEF_DEFAULTLINKMTUOFIPV6CTRL                                                        STD_OFF
#define TCPIP_ISDEF_DEFAULTTRAFFICCLASSFLOWLABELNBOOFIPV6CTRL                                       STD_OFF
#define TCPIP_ISDEF_DHCPMODEOFIPV6CTRL                                                              STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONENDIDXOFIPV6CTRL                                                  STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONSTARTIDXOFIPV6CTRL                                                STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONUSEDOFIPV6CTRL                                                    STD_OFF
#define TCPIP_ISDEF_ENABLEDYNHOPLIMITOFIPV6CTRL                                                     STD_OFF
#define TCPIP_ISDEF_ENABLEPATHMTUOFIPV6CTRL                                                         STD_OFF
#define TCPIP_ISDEF_ETHIFCTRLIDXOFIPV6CTRL                                                          STD_OFF
#define TCPIP_ISDEF_FRAMEPRIODEFAULTOFIPV6CTRL                                                      STD_OFF
#define TCPIP_ISDEF_HWCHECKSUMICMPOFIPV6CTRL                                                        STD_OFF
#define TCPIP_ISDEF_HWCHECKSUMIPDESTINATIONOPTIONSOFIPV6CTRL                                        STD_OFF
#define TCPIP_ISDEF_HWCHECKSUMIPHOPBYHOPOPTIONSOFIPV6CTRL                                           STD_OFF
#define TCPIP_ISDEF_HWCHECKSUMIPROUTINGOFIPV6CTRL                                                   STD_OFF
#define TCPIP_ISDEF_HWCHECKSUMTCPOFIPV6CTRL                                                         STD_OFF
#define TCPIP_ISDEF_HWCHECKSUMUDPOFIPV6CTRL                                                         STD_OFF
#define TCPIP_ISDEF_INTERFACEIDENTIFIERENDIDXOFIPV6CTRL                                             STD_OFF
#define TCPIP_ISDEF_INTERFACEIDENTIFIERSTARTIDXOFIPV6CTRL                                           STD_OFF
#define TCPIP_ISDEF_IPV6DEFAULTROUTERLISTENTRYENDIDXOFIPV6CTRL                                      STD_OFF
#define TCPIP_ISDEF_IPV6DEFAULTROUTERLISTENTRYSTARTIDXOFIPV6CTRL                                    STD_OFF
#define TCPIP_ISDEF_IPV6DESTINATIONCACHEENTRYENDIDXOFIPV6CTRL                                       STD_OFF
#define TCPIP_ISDEF_IPV6DESTINATIONCACHEENTRYSTARTIDXOFIPV6CTRL                                     STD_OFF
#define TCPIP_ISDEF_IPV6ETHBUFDATAENDIDXOFIPV6CTRL                                                  STD_OFF
#define TCPIP_ISDEF_IPV6ETHBUFDATASTARTIDXOFIPV6CTRL                                                STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRENDIDXOFIPV6CTRL                                               STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRSTARTIDXOFIPV6CTRL                                             STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRUSEDOFIPV6CTRL                                                 STD_OFF
#define TCPIP_ISDEF_IPV6NEIGHBORCACHEENTRYENDIDXOFIPV6CTRL                                          STD_OFF
#define TCPIP_ISDEF_IPV6NEIGHBORCACHEENTRYSTARTIDXOFIPV6CTRL                                        STD_OFF
#define TCPIP_ISDEF_IPV6PREFIXLISTENTRYENDIDXOFIPV6CTRL                                             STD_OFF
#define TCPIP_ISDEF_IPV6PREFIXLISTENTRYSTARTIDXOFIPV6CTRL                                           STD_OFF
#define TCPIP_ISDEF_IPV6SOURCEADDRESSENDIDXOFIPV6CTRL                                               STD_OFF
#define TCPIP_ISDEF_IPV6SOURCEADDRESSSTARTIDXOFIPV6CTRL                                             STD_OFF
#define TCPIP_ISDEF_LOCALADDRV6BCIDXOFIPV6CTRL                                                      STD_OFF
#define TCPIP_ISDEF_LOCALADDRV6BCUSEDOFIPV6CTRL                                                     STD_OFF
#define TCPIP_ISDEF_MASKEDBITSOFIPV6CTRL                                                            STD_OFF
#define TCPIP_ISDEF_NDPCONFIGIDXOFIPV6CTRL                                                          STD_OFF
#define TCPIP_ISDEF_PATHMTUTIMEOUTOFIPV6CTRL                                                        STD_OFF
#define TCPIP_ISDEF_EXTDESTADDRVALIDATIONENABLEDOFIPV6GENERAL                                       STD_OFF
#define TCPIP_ISDEF_IPV6CTRLDEFAULTIDXOFIPV6GENERAL                                                 STD_OFF
#define TCPIP_ISDEF_IPV6CTRLDEFAULTUSEDOFIPV6GENERAL                                                STD_OFF
#define TCPIP_ISDEF_IPV6SOCKETDYNICMPIDXOFIPV6GENERAL                                               STD_OFF
#define TCPIP_ISDEF_IPV6SOCKETDYNNDPIDXOFIPV6GENERAL                                                STD_OFF
#define TCPIP_ISDEF_IPV6SOCKETDYNTCPRSTIDXOFIPV6GENERAL                                             STD_OFF
#define TCPIP_ISDEF_IPV6SOCKETDYNTCPRSTUSEDOFIPV6GENERAL                                            STD_OFF
#define TCPIP_ISDEF_MASKEDBITSOFIPV6GENERAL                                                         STD_OFF
#define TCPIP_ISDEF_DEFAULTADDRV6IDXOFIPV6MULTICASTADDR                                             STD_OFF
#define TCPIP_ISDEF_DEFAULTADDRV6USEDOFIPV6MULTICASTADDR                                            STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRACTIVEIDXOFIPV6MULTICASTADDR                                   STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRACTIVEUSEDOFIPV6MULTICASTADDR                                  STD_OFF
#define TCPIP_ISDEF_LOCALADDRV6IDXOFIPV6MULTICASTADDR                                               STD_OFF
#define TCPIP_ISDEF_MASKEDBITSOFIPV6MULTICASTADDR                                                   STD_OFF
#define TCPIP_ISDEF_ADDRESSASSIGNVARIANTOFIPV6SOURCEADDRESS                                         STD_OFF
#define TCPIP_ISDEF_DEFAULTADDRV6IDXOFIPV6SOURCEADDRESS                                             STD_OFF
#define TCPIP_ISDEF_DEFAULTADDRV6USEDOFIPV6SOURCEADDRESS                                            STD_OFF
#define TCPIP_ISDEF_LOCALADDRV6IDXOFIPV6SOURCEADDRESS                                               STD_OFF
#define TCPIP_ISDEF_MASKEDBITSOFIPV6SOURCEADDRESS                                                   STD_OFF
#define TCPIP_ISDEF_PREFIXISONLINKOFIPV6SOURCEADDRESS                                               STD_OFF
#define TCPIP_ISDEF_IPV6CTRLIDXOFLOCALADDRV6                                                        STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRIDXOFLOCALADDRV6                                               STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDRUSEDOFLOCALADDRV6                                              STD_OFF
#define TCPIP_ISDEF_IPV6SOURCEADDRESSIDXOFLOCALADDRV6                                               STD_OFF
#define TCPIP_ISDEF_IPV6SOURCEADDRESSUSEDOFLOCALADDRV6                                              STD_OFF
#define TCPIP_ISDEF_MASKEDBITSOFLOCALADDRV6                                                         STD_OFF
#define TCPIP_ISDEF_DADTRANSMITSOFNDPCONFIG                                                         STD_OFF
#define TCPIP_ISDEF_DYNAMICREACHABLETIMEOFNDPCONFIG                                                 STD_OFF
#define TCPIP_ISDEF_DYNAMICRETRANSTIMEROFNDPCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_ENABLENDPINVNANCUPDATEOFNDPCONFIG                                               STD_OFF
#define TCPIP_ISDEF_ENABLENDPINVNAOFNDPCONFIG                                                       STD_OFF
#define TCPIP_ISDEF_ENABLENDPINVNSOFNDPCONFIG                                                       STD_OFF
#define TCPIP_ISDEF_ENABLENUDOFNDPCONFIG                                                            STD_OFF
#define TCPIP_ISDEF_ENABLEOPTIMISTICDADOFNDPCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_ENABLERFC6106DNSSLOPTOFNDPCONFIG                                                STD_OFF
#define TCPIP_ISDEF_ENABLERFC6106RDNSSOPTOFNDPCONFIG                                                STD_OFF
#define TCPIP_ISDEF_ENABLESLAACDELAYOFNDPCONFIG                                                     STD_OFF
#define TCPIP_ISDEF_MASKEDBITSOFNDPCONFIG                                                           STD_OFF
#define TCPIP_ISDEF_MAXRANDOMFACTOROFNDPCONFIG                                                      STD_OFF
#define TCPIP_ISDEF_MAXRTRSOLICITATIONDELAYOFNDPCONFIG                                              STD_OFF
#define TCPIP_ISDEF_MAXRTRSOLICITATIONSOFNDPCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_MAXSLAACDELAYOFNDPCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_MINRANDOMFACTOROFNDPCONFIG                                                      STD_OFF
#define TCPIP_ISDEF_MULTICASTSOLICITSOFNDPCONFIG                                                    STD_OFF
#define TCPIP_ISDEF_NUDFIRSTPROBEDELAYOFNDPCONFIG                                                   STD_OFF
#define TCPIP_ISDEF_RANDOMREACHABLETIMEOFNDPCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_REACHABLETIMEOFNDPCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_RETRANSTIMEROFNDPCONFIG                                                         STD_OFF
#define TCPIP_ISDEF_RNDRTRSOLICITATIONDELAYOFNDPCONFIG                                              STD_OFF
#define TCPIP_ISDEF_RTRSOLICITATIONINTERVALOFNDPCONFIG                                              STD_OFF
#define TCPIP_ISDEF_SLAACMINLIFETIMEOFNDPCONFIG                                                     STD_OFF
#define TCPIP_ISDEF_UNICASTSOLICITSOFNDPCONFIG                                                      STD_OFF
#define TCPIP_ISDEF_CHGDISCARDEDFUNCPTROFPHYSADDRCONFIG                                             STD_OFF
#define TCPIP_ISDEF_CHGFUNCPTROFPHYSADDRCONFIG                                                      STD_OFF
#define TCPIP_ISDEF_COPYTXDATADYNFUNCPTROFSOCKETOWNERCONFIG                                         STD_OFF
#define TCPIP_ISDEF_COPYTXDATAFUNCPTROFSOCKETOWNERCONFIG                                            STD_OFF
#define TCPIP_ISDEF_DHCPEVENTFUNCPTROFSOCKETOWNERCONFIG                                             STD_OFF
#define TCPIP_ISDEF_LOCALIPADDRASSIGNMENTCHGFUNCPTROFSOCKETOWNERCONFIG                              STD_OFF
#define TCPIP_ISDEF_RXINDICATIONFUNCPTROFSOCKETOWNERCONFIG                                          STD_OFF
#define TCPIP_ISDEF_TCPACCEPTEDFUNCPTROFSOCKETOWNERCONFIG                                           STD_OFF
#define TCPIP_ISDEF_TCPCONNECTEDFUNCPTROFSOCKETOWNERCONFIG                                          STD_OFF
#define TCPIP_ISDEF_TCPIPEVENTFUNCPTROFSOCKETOWNERCONFIG                                            STD_OFF
#define TCPIP_ISDEF_TLSVALIDATIONRESULTFUNCPTROFSOCKETOWNERCONFIG                                   STD_OFF
#define TCPIP_ISDEF_TXCONFIRMATIONFUNCPTROFSOCKETOWNERCONFIG                                        STD_OFF
#define TCPIP_ISDEF_FINWAIT2TIMEOUTOFTCPCONFIG                                                      STD_OFF
#define TCPIP_ISDEF_KEEPALIVEINTERVALOFTCPCONFIG                                                    STD_OFF
#define TCPIP_ISDEF_KEEPALIVEPROBESMAXOFTCPCONFIG                                                   STD_OFF
#define TCPIP_ISDEF_KEEPALIVETIMEOFTCPCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_MSLOFTCPCONFIG                                                                  STD_OFF
#define TCPIP_ISDEF_NAGLETIMEOUTOFTCPCONFIG                                                         STD_OFF
#define TCPIP_ISDEF_RETRANSTIMEOUTMAXOFTCPCONFIG                                                    STD_OFF
#define TCPIP_ISDEF_RETRANSTIMEOUTOFTCPCONFIG                                                       STD_OFF
#define TCPIP_ISDEF_RXMSSOFTCPCONFIG                                                                STD_OFF
#define TCPIP_ISDEF_TCPOOOQSIZEPERSOCKETAVGOFTCPCONFIG                                              STD_OFF
#define TCPIP_ISDEF_TCPOOOQSIZEPERSOCKETMAXOFTCPCONFIG                                              STD_OFF
#define TCPIP_ISDEF_TCPRETRYQSIZEOFTCPCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_TIMETOLIVEDEFAULTOFTCPCONFIG                                                    STD_OFF
#define TCPIP_ISDEF_TXMSSOFTCPCONFIG                                                                STD_OFF
#define TCPIP_ISDEF_USERTIMEOUTDEFCYCLESOFTCPCONFIG                                                 STD_OFF
#define TCPIP_ISDEF_USERTIMEOUTMAXCYCLESOFTCPCONFIG                                                 STD_OFF
#define TCPIP_ISDEF_USERTIMEOUTMINCYCLESOFTCPCONFIG                                                 STD_OFF
#define TCPIP_ISDEF_TCPRXBUFFERENDIDXOFTCPRXBUFFERDESC                                              STD_OFF
#define TCPIP_ISDEF_TCPRXBUFFERLENGTHOFTCPRXBUFFERDESC                                              STD_OFF
#define TCPIP_ISDEF_TCPRXBUFFERSTARTIDXOFTCPRXBUFFERDESC                                            STD_OFF
#define TCPIP_ISDEF_TCPTXBUFFERENDIDXOFTCPTXBUFFERDESC                                              STD_OFF
#define TCPIP_ISDEF_TCPTXBUFFERLENGTHOFTCPTXBUFFERDESC                                              STD_OFF
#define TCPIP_ISDEF_TCPTXBUFFERSTARTIDXOFTCPTXBUFFERDESC                                            STD_OFF
#define TCPIP_ISDEF_TXREQELEMENDIDXOFTXREQELEMLIST                                                  STD_OFF
#define TCPIP_ISDEF_TXREQELEMLENGTHOFTXREQELEMLIST                                                  STD_OFF
#define TCPIP_ISDEF_TXREQELEMSTARTIDXOFTXREQELEMLIST                                                STD_OFF
#define TCPIP_ISDEF_DEFAULTADDRV6OFPCCONFIG                                                         STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONBUFFEROFPCCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONDYNOFPCCONFIG                                                     STD_OFF
#define TCPIP_ISDEF_DHCPUSEROPTIONOFPCCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_DUPLICATEADDRDETECTIONFCTPTROFPCCONFIG                                          STD_ON
#define TCPIP_ISDEF_ETHIFCTRLOFPCCONFIG                                                             STD_ON
#define TCPIP_ISDEF_ICMPV6CONFIGOFPCCONFIG                                                          STD_ON
#define TCPIP_ISDEF_ICMPV6MSGHANDLERCBKFCTPTROFPCCONFIG                                             STD_ON
#define TCPIP_ISDEF_ICMPV6TXMSGBUFFEROFPCCONFIG                                                     STD_ON
#define TCPIP_ISDEF_INTERFACEIDENTIFIEROFPCCONFIG                                                   STD_ON
#define TCPIP_ISDEF_IPV6CTRLDYNOFPCCONFIG                                                           STD_ON
#define TCPIP_ISDEF_IPV6CTRLOFPCCONFIG                                                              STD_ON
#define TCPIP_ISDEF_IPV6DEFAULTROUTERLISTENTRYOFPCCONFIG                                            STD_ON
#define TCPIP_ISDEF_IPV6DESTINATIONCACHEENTRYOFPCCONFIG                                             STD_ON
#define TCPIP_ISDEF_IPV6ETHBUFDATAOFPCCONFIG                                                        STD_ON
#define TCPIP_ISDEF_IPV6GENERALOFPCCONFIG                                                           STD_ON
#define TCPIP_ISDEF_IPV6MULTICASTADDRACTIVEOFPCCONFIG                                               STD_OFF
#define TCPIP_ISDEF_IPV6MULTICASTADDROFPCCONFIG                                                     STD_OFF
#define TCPIP_ISDEF_IPV6NEIGHBORCACHEENTRYOFPCCONFIG                                                STD_ON
#define TCPIP_ISDEF_IPV6PREFIXLISTENTRYOFPCCONFIG                                                   STD_ON
#define TCPIP_ISDEF_IPV6SOCKETDYNOFPCCONFIG                                                         STD_ON
#define TCPIP_ISDEF_IPV6SOURCEADDRESSOFPCCONFIG                                                     STD_ON
#define TCPIP_ISDEF_IPV6SOURCEADDRESSTABLEENTRYOFPCCONFIG                                           STD_ON
#define TCPIP_ISDEF_LOCALADDROFPCCONFIG                                                             STD_ON
#define TCPIP_ISDEF_LOCALADDRV6OFPCCONFIG                                                           STD_ON
#define TCPIP_ISDEF_NDPCONFIGOFPCCONFIG                                                             STD_ON
#define TCPIP_ISDEF_PHYSADDRCONFIGOFPCCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_RANDOMNUMBERFCTPTROFPCCONFIG                                                    STD_ON
#define TCPIP_ISDEF_SIZEOFDEFAULTADDRV6OFPCCONFIG                                                   STD_OFF
#define TCPIP_ISDEF_SIZEOFDHCPUSEROPTIONBUFFEROFPCCONFIG                                            STD_OFF
#define TCPIP_ISDEF_SIZEOFDHCPUSEROPTIONOFPCCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_SIZEOFIPV6MULTICASTADDRACTIVEOFPCCONFIG                                         STD_OFF
#define TCPIP_ISDEF_SIZEOFIPV6MULTICASTADDROFPCCONFIG                                               STD_OFF
#define TCPIP_ISDEF_SIZEOFPHYSADDRCONFIGOFPCCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_SIZEOFTCPOOOQELEMENTOFPCCONFIG                                                  STD_OFF
#define TCPIP_ISDEF_SIZEOFTXREQELEMLISTOFPCCONFIG                                                   STD_OFF
#define TCPIP_ISDEF_SIZEOFTXREQELEMOFPCCONFIG                                                       STD_OFF
#define TCPIP_ISDEF_SIZEOFUDPTXRETRYQUEUEELEMENTCHAINOFPCCONFIG                                     STD_OFF
#define TCPIP_ISDEF_SIZEOFUDPTXRETRYQUEUEELEMENTSOFPCCONFIG                                         STD_OFF
#define TCPIP_ISDEF_SIZEOFUDPTXRETRYQUEUEPOOLDESCOFPCCONFIG                                         STD_OFF
#define TCPIP_ISDEF_SOCKETDYNOFPCCONFIG                                                             STD_ON
#define TCPIP_ISDEF_SOCKETOWNERCONFIGOFPCCONFIG                                                     STD_ON
#define TCPIP_ISDEF_SOCKETTCPDYNOFPCCONFIG                                                          STD_ON
#define TCPIP_ISDEF_SOCKETUDPDYNOFPCCONFIG                                                          STD_ON
#define TCPIP_ISDEF_TCPCONFIGOFPCCONFIG                                                             STD_ON
#define TCPIP_ISDEF_TCPOOOQELEMENTOFPCCONFIG                                                        STD_OFF
#define TCPIP_ISDEF_TCPRESETQELEMENTOFPCCONFIG                                                      STD_ON
#define TCPIP_ISDEF_TCPRETRYQELEMENTOFPCCONFIG                                                      STD_ON
#define TCPIP_ISDEF_TCPRXBUFFERDESCDYNOFPCCONFIG                                                    STD_ON
#define TCPIP_ISDEF_TCPRXBUFFERDESCOFPCCONFIG                                                       STD_ON
#define TCPIP_ISDEF_TCPRXBUFFEROFPCCONFIG                                                           STD_ON
#define TCPIP_ISDEF_TCPTXBUFFERDESCDYNOFPCCONFIG                                                    STD_ON
#define TCPIP_ISDEF_TCPTXBUFFERDESCOFPCCONFIG                                                       STD_ON
#define TCPIP_ISDEF_TCPTXBUFFEROFPCCONFIG                                                           STD_ON
#define TCPIP_ISDEF_TXREQELEMLISTDYNOFPCCONFIG                                                      STD_OFF
#define TCPIP_ISDEF_TXREQELEMLISTOFPCCONFIG                                                         STD_OFF
#define TCPIP_ISDEF_TXREQELEMOFPCCONFIG                                                             STD_OFF
#define TCPIP_ISDEF_UDPTXRETRYQUEUEELEMENTCHAINOFPCCONFIG                                           STD_OFF
#define TCPIP_ISDEF_UDPTXRETRYQUEUEELEMENTSOFPCCONFIG                                               STD_OFF
#define TCPIP_ISDEF_UDPTXRETRYQUEUEPOOLDESCOFPCCONFIG                                               STD_OFF
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCEqualsAlwaysToDefines  TcpIp Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define TCPIP_EQ2_DEFAULTADDRV6                                                                     
#define TCPIP_EQ2_CODEOFDHCPUSEROPTION                                                              
#define TCPIP_EQ2_DHCPUSEROPTIONBUFFERENDIDXOFDHCPUSEROPTION                                        
#define TCPIP_EQ2_DHCPUSEROPTIONBUFFERLENGTHOFDHCPUSEROPTION                                        
#define TCPIP_EQ2_DHCPUSEROPTIONBUFFERSTARTIDXOFDHCPUSEROPTION                                      
#define TCPIP_EQ2_DIRECTIONOFDHCPUSEROPTION                                                         
#define TCPIP_EQ2_IPV6CTRLIDXOFETHIFCTRL                                                            
#define TCPIP_EQ2_IPV6CTRLUSEDOFETHIFCTRL                                                           
#define TCPIP_EQ2_ECHOREQUESTAPIOFICMPV6CONFIG                                                      
#define TCPIP_EQ2_HOPLIMITOFICMPV6CONFIG                                                            
#define TCPIP_EQ2_ALLOWLINKMTURECONFIGURATIONOFIPV6CTRL                                             
#define TCPIP_EQ2_DEFAULTHOPLIMITOFIPV6CTRL                                                         
#define TCPIP_EQ2_DEFAULTLINKMTUOFIPV6CTRL                                                          
#define TCPIP_EQ2_DEFAULTTRAFFICCLASSFLOWLABELNBOOFIPV6CTRL                                         
#define TCPIP_EQ2_DHCPMODEOFIPV6CTRL                                                                
#define TCPIP_EQ2_DHCPUSEROPTIONENDIDXOFIPV6CTRL                                                    
#define TCPIP_EQ2_DHCPUSEROPTIONSTARTIDXOFIPV6CTRL                                                  
#define TCPIP_EQ2_DHCPUSEROPTIONUSEDOFIPV6CTRL                                                      
#define TCPIP_EQ2_ENABLEDYNHOPLIMITOFIPV6CTRL                                                       
#define TCPIP_EQ2_ENABLEPATHMTUOFIPV6CTRL                                                           
#define TCPIP_EQ2_ETHIFCTRLIDXOFIPV6CTRL                                                            
#define TCPIP_EQ2_FRAMEPRIODEFAULTOFIPV6CTRL                                                        
#define TCPIP_EQ2_HWCHECKSUMICMPOFIPV6CTRL                                                          
#define TCPIP_EQ2_HWCHECKSUMIPDESTINATIONOPTIONSOFIPV6CTRL                                          
#define TCPIP_EQ2_HWCHECKSUMIPHOPBYHOPOPTIONSOFIPV6CTRL                                             
#define TCPIP_EQ2_HWCHECKSUMIPROUTINGOFIPV6CTRL                                                     
#define TCPIP_EQ2_HWCHECKSUMTCPOFIPV6CTRL                                                           
#define TCPIP_EQ2_HWCHECKSUMUDPOFIPV6CTRL                                                           
#define TCPIP_EQ2_INTERFACEIDENTIFIERENDIDXOFIPV6CTRL                                               
#define TCPIP_EQ2_INTERFACEIDENTIFIERSTARTIDXOFIPV6CTRL                                             
#define TCPIP_EQ2_IPV6DEFAULTROUTERLISTENTRYENDIDXOFIPV6CTRL                                        
#define TCPIP_EQ2_IPV6DEFAULTROUTERLISTENTRYSTARTIDXOFIPV6CTRL                                      
#define TCPIP_EQ2_IPV6DESTINATIONCACHEENTRYENDIDXOFIPV6CTRL                                         
#define TCPIP_EQ2_IPV6DESTINATIONCACHEENTRYSTARTIDXOFIPV6CTRL                                       
#define TCPIP_EQ2_IPV6ETHBUFDATAENDIDXOFIPV6CTRL                                                    
#define TCPIP_EQ2_IPV6ETHBUFDATASTARTIDXOFIPV6CTRL                                                  
#define TCPIP_EQ2_IPV6MULTICASTADDRENDIDXOFIPV6CTRL                                                 
#define TCPIP_EQ2_IPV6MULTICASTADDRSTARTIDXOFIPV6CTRL                                               
#define TCPIP_EQ2_IPV6MULTICASTADDRUSEDOFIPV6CTRL                                                   
#define TCPIP_EQ2_IPV6NEIGHBORCACHEENTRYENDIDXOFIPV6CTRL                                            
#define TCPIP_EQ2_IPV6NEIGHBORCACHEENTRYSTARTIDXOFIPV6CTRL                                          
#define TCPIP_EQ2_IPV6PREFIXLISTENTRYENDIDXOFIPV6CTRL                                               
#define TCPIP_EQ2_IPV6PREFIXLISTENTRYSTARTIDXOFIPV6CTRL                                             
#define TCPIP_EQ2_IPV6SOURCEADDRESSENDIDXOFIPV6CTRL                                                 
#define TCPIP_EQ2_IPV6SOURCEADDRESSSTARTIDXOFIPV6CTRL                                               
#define TCPIP_EQ2_LOCALADDRV6BCIDXOFIPV6CTRL                                                        
#define TCPIP_EQ2_LOCALADDRV6BCUSEDOFIPV6CTRL                                                       
#define TCPIP_EQ2_MASKEDBITSOFIPV6CTRL                                                              
#define TCPIP_EQ2_NDPCONFIGIDXOFIPV6CTRL                                                            
#define TCPIP_EQ2_PATHMTUTIMEOUTOFIPV6CTRL                                                          
#define TCPIP_EQ2_EXTDESTADDRVALIDATIONENABLEDOFIPV6GENERAL                                         
#define TCPIP_EQ2_IPV6CTRLDEFAULTIDXOFIPV6GENERAL                                                   
#define TCPIP_EQ2_IPV6CTRLDEFAULTUSEDOFIPV6GENERAL                                                  
#define TCPIP_EQ2_IPV6SOCKETDYNICMPIDXOFIPV6GENERAL                                                 
#define TCPIP_EQ2_IPV6SOCKETDYNNDPIDXOFIPV6GENERAL                                                  
#define TCPIP_EQ2_IPV6SOCKETDYNTCPRSTIDXOFIPV6GENERAL                                               
#define TCPIP_EQ2_IPV6SOCKETDYNTCPRSTUSEDOFIPV6GENERAL                                              
#define TCPIP_EQ2_MASKEDBITSOFIPV6GENERAL                                                           
#define TCPIP_EQ2_DEFAULTADDRV6IDXOFIPV6MULTICASTADDR                                               
#define TCPIP_EQ2_DEFAULTADDRV6USEDOFIPV6MULTICASTADDR                                              
#define TCPIP_EQ2_IPV6MULTICASTADDRACTIVEIDXOFIPV6MULTICASTADDR                                     
#define TCPIP_EQ2_IPV6MULTICASTADDRACTIVEUSEDOFIPV6MULTICASTADDR                                    
#define TCPIP_EQ2_LOCALADDRV6IDXOFIPV6MULTICASTADDR                                                 
#define TCPIP_EQ2_MASKEDBITSOFIPV6MULTICASTADDR                                                     
#define TCPIP_EQ2_ADDRESSASSIGNVARIANTOFIPV6SOURCEADDRESS                                           
#define TCPIP_EQ2_DEFAULTADDRV6IDXOFIPV6SOURCEADDRESS                                               
#define TCPIP_EQ2_DEFAULTADDRV6USEDOFIPV6SOURCEADDRESS                                              
#define TCPIP_EQ2_LOCALADDRV6IDXOFIPV6SOURCEADDRESS                                                 
#define TCPIP_EQ2_MASKEDBITSOFIPV6SOURCEADDRESS                                                     
#define TCPIP_EQ2_PREFIXISONLINKOFIPV6SOURCEADDRESS                                                 
#define TCPIP_EQ2_IPV6CTRLIDXOFLOCALADDRV6                                                          
#define TCPIP_EQ2_IPV6MULTICASTADDRIDXOFLOCALADDRV6                                                 
#define TCPIP_EQ2_IPV6MULTICASTADDRUSEDOFLOCALADDRV6                                                
#define TCPIP_EQ2_IPV6SOURCEADDRESSIDXOFLOCALADDRV6                                                 
#define TCPIP_EQ2_IPV6SOURCEADDRESSUSEDOFLOCALADDRV6                                                
#define TCPIP_EQ2_MASKEDBITSOFLOCALADDRV6                                                           
#define TCPIP_EQ2_DADTRANSMITSOFNDPCONFIG                                                           
#define TCPIP_EQ2_DYNAMICREACHABLETIMEOFNDPCONFIG                                                   
#define TCPIP_EQ2_DYNAMICRETRANSTIMEROFNDPCONFIG                                                    
#define TCPIP_EQ2_ENABLENDPINVNANCUPDATEOFNDPCONFIG                                                 
#define TCPIP_EQ2_ENABLENDPINVNAOFNDPCONFIG                                                         
#define TCPIP_EQ2_ENABLENDPINVNSOFNDPCONFIG                                                         
#define TCPIP_EQ2_ENABLENUDOFNDPCONFIG                                                              
#define TCPIP_EQ2_ENABLEOPTIMISTICDADOFNDPCONFIG                                                    
#define TCPIP_EQ2_ENABLERFC6106DNSSLOPTOFNDPCONFIG                                                  
#define TCPIP_EQ2_ENABLERFC6106RDNSSOPTOFNDPCONFIG                                                  
#define TCPIP_EQ2_ENABLESLAACDELAYOFNDPCONFIG                                                       
#define TCPIP_EQ2_MASKEDBITSOFNDPCONFIG                                                             
#define TCPIP_EQ2_MAXRANDOMFACTOROFNDPCONFIG                                                        
#define TCPIP_EQ2_MAXRTRSOLICITATIONDELAYOFNDPCONFIG                                                
#define TCPIP_EQ2_MAXRTRSOLICITATIONSOFNDPCONFIG                                                    
#define TCPIP_EQ2_MAXSLAACDELAYOFNDPCONFIG                                                          
#define TCPIP_EQ2_MINRANDOMFACTOROFNDPCONFIG                                                        
#define TCPIP_EQ2_MULTICASTSOLICITSOFNDPCONFIG                                                      
#define TCPIP_EQ2_NUDFIRSTPROBEDELAYOFNDPCONFIG                                                     
#define TCPIP_EQ2_RANDOMREACHABLETIMEOFNDPCONFIG                                                    
#define TCPIP_EQ2_REACHABLETIMEOFNDPCONFIG                                                          
#define TCPIP_EQ2_RETRANSTIMEROFNDPCONFIG                                                           
#define TCPIP_EQ2_RNDRTRSOLICITATIONDELAYOFNDPCONFIG                                                
#define TCPIP_EQ2_RTRSOLICITATIONINTERVALOFNDPCONFIG                                                
#define TCPIP_EQ2_SLAACMINLIFETIMEOFNDPCONFIG                                                       
#define TCPIP_EQ2_UNICASTSOLICITSOFNDPCONFIG                                                        
#define TCPIP_EQ2_CHGDISCARDEDFUNCPTROFPHYSADDRCONFIG                                               
#define TCPIP_EQ2_CHGFUNCPTROFPHYSADDRCONFIG                                                        
#define TCPIP_EQ2_COPYTXDATADYNFUNCPTROFSOCKETOWNERCONFIG                                           
#define TCPIP_EQ2_COPYTXDATAFUNCPTROFSOCKETOWNERCONFIG                                              
#define TCPIP_EQ2_DHCPEVENTFUNCPTROFSOCKETOWNERCONFIG                                               
#define TCPIP_EQ2_LOCALIPADDRASSIGNMENTCHGFUNCPTROFSOCKETOWNERCONFIG                                
#define TCPIP_EQ2_RXINDICATIONFUNCPTROFSOCKETOWNERCONFIG                                            
#define TCPIP_EQ2_TCPACCEPTEDFUNCPTROFSOCKETOWNERCONFIG                                             
#define TCPIP_EQ2_TCPCONNECTEDFUNCPTROFSOCKETOWNERCONFIG                                            
#define TCPIP_EQ2_TCPIPEVENTFUNCPTROFSOCKETOWNERCONFIG                                              
#define TCPIP_EQ2_TLSVALIDATIONRESULTFUNCPTROFSOCKETOWNERCONFIG                                     
#define TCPIP_EQ2_TXCONFIRMATIONFUNCPTROFSOCKETOWNERCONFIG                                          
#define TCPIP_EQ2_FINWAIT2TIMEOUTOFTCPCONFIG                                                        
#define TCPIP_EQ2_KEEPALIVEINTERVALOFTCPCONFIG                                                      
#define TCPIP_EQ2_KEEPALIVEPROBESMAXOFTCPCONFIG                                                     
#define TCPIP_EQ2_KEEPALIVETIMEOFTCPCONFIG                                                          
#define TCPIP_EQ2_MSLOFTCPCONFIG                                                                    
#define TCPIP_EQ2_NAGLETIMEOUTOFTCPCONFIG                                                           
#define TCPIP_EQ2_RETRANSTIMEOUTMAXOFTCPCONFIG                                                      
#define TCPIP_EQ2_RETRANSTIMEOUTOFTCPCONFIG                                                         
#define TCPIP_EQ2_RXMSSOFTCPCONFIG                                                                  
#define TCPIP_EQ2_TCPOOOQSIZEPERSOCKETAVGOFTCPCONFIG                                                
#define TCPIP_EQ2_TCPOOOQSIZEPERSOCKETMAXOFTCPCONFIG                                                
#define TCPIP_EQ2_TCPRETRYQSIZEOFTCPCONFIG                                                          
#define TCPIP_EQ2_TIMETOLIVEDEFAULTOFTCPCONFIG                                                      
#define TCPIP_EQ2_TXMSSOFTCPCONFIG                                                                  
#define TCPIP_EQ2_USERTIMEOUTDEFCYCLESOFTCPCONFIG                                                   
#define TCPIP_EQ2_USERTIMEOUTMAXCYCLESOFTCPCONFIG                                                   
#define TCPIP_EQ2_USERTIMEOUTMINCYCLESOFTCPCONFIG                                                   
#define TCPIP_EQ2_TCPRXBUFFERENDIDXOFTCPRXBUFFERDESC                                                
#define TCPIP_EQ2_TCPRXBUFFERLENGTHOFTCPRXBUFFERDESC                                                
#define TCPIP_EQ2_TCPRXBUFFERSTARTIDXOFTCPRXBUFFERDESC                                              
#define TCPIP_EQ2_TCPTXBUFFERENDIDXOFTCPTXBUFFERDESC                                                
#define TCPIP_EQ2_TCPTXBUFFERLENGTHOFTCPTXBUFFERDESC                                                
#define TCPIP_EQ2_TCPTXBUFFERSTARTIDXOFTCPTXBUFFERDESC                                              
#define TCPIP_EQ2_TXREQELEMENDIDXOFTXREQELEMLIST                                                    
#define TCPIP_EQ2_TXREQELEMLENGTHOFTXREQELEMLIST                                                    
#define TCPIP_EQ2_TXREQELEMSTARTIDXOFTXREQELEMLIST                                                  
#define TCPIP_EQ2_DEFAULTADDRV6OFPCCONFIG                                                           
#define TCPIP_EQ2_DHCPUSEROPTIONBUFFEROFPCCONFIG                                                    
#define TCPIP_EQ2_DHCPUSEROPTIONDYNOFPCCONFIG                                                       
#define TCPIP_EQ2_DHCPUSEROPTIONOFPCCONFIG                                                          
#define TCPIP_EQ2_DUPLICATEADDRDETECTIONFCTPTROFPCCONFIG                                            TcpIp_DuplicateAddrDetectionFctPtr
#define TCPIP_EQ2_ETHIFCTRLOFPCCONFIG                                                               TcpIp_EthIfCtrl
#define TCPIP_EQ2_ICMPV6CONFIGOFPCCONFIG                                                            TcpIp_IcmpV6Config
#define TCPIP_EQ2_ICMPV6MSGHANDLERCBKFCTPTROFPCCONFIG                                               TcpIp_IcmpV6MsgHandlerCbkFctPtr
#define TCPIP_EQ2_ICMPV6TXMSGBUFFEROFPCCONFIG                                                       TcpIp_IcmpV6TxMsgBuffer
#define TCPIP_EQ2_INTERFACEIDENTIFIEROFPCCONFIG                                                     TcpIp_InterfaceIdentifier.raw
#define TCPIP_EQ2_IPV6CTRLDYNOFPCCONFIG                                                             TcpIp_IpV6CtrlDyn.raw
#define TCPIP_EQ2_IPV6CTRLOFPCCONFIG                                                                TcpIp_IpV6Ctrl
#define TCPIP_EQ2_IPV6DEFAULTROUTERLISTENTRYOFPCCONFIG                                              TcpIp_IpV6DefaultRouterListEntry.raw
#define TCPIP_EQ2_IPV6DESTINATIONCACHEENTRYOFPCCONFIG                                               TcpIp_IpV6DestinationCacheEntry.raw
#define TCPIP_EQ2_IPV6ETHBUFDATAOFPCCONFIG                                                          TcpIp_IpV6EthBufData.raw
#define TCPIP_EQ2_IPV6GENERALOFPCCONFIG                                                             TcpIp_IpV6General
#define TCPIP_EQ2_IPV6MULTICASTADDRACTIVEOFPCCONFIG                                                 
#define TCPIP_EQ2_IPV6MULTICASTADDROFPCCONFIG                                                       
#define TCPIP_EQ2_IPV6NEIGHBORCACHEENTRYOFPCCONFIG                                                  TcpIp_IpV6NeighborCacheEntry.raw
#define TCPIP_EQ2_IPV6PREFIXLISTENTRYOFPCCONFIG                                                     TcpIp_IpV6PrefixListEntry.raw
#define TCPIP_EQ2_IPV6SOCKETDYNOFPCCONFIG                                                           TcpIp_IpV6SocketDyn
#define TCPIP_EQ2_IPV6SOURCEADDRESSOFPCCONFIG                                                       TcpIp_IpV6SourceAddress
#define TCPIP_EQ2_IPV6SOURCEADDRESSTABLEENTRYOFPCCONFIG                                             TcpIp_IpV6SourceAddressTableEntry
#define TCPIP_EQ2_LOCALADDROFPCCONFIG                                                               TcpIp_LocalAddr.raw
#define TCPIP_EQ2_LOCALADDRV6OFPCCONFIG                                                             TcpIp_LocalAddrV6
#define TCPIP_EQ2_NDPCONFIGOFPCCONFIG                                                               TcpIp_NdpConfig
#define TCPIP_EQ2_PHYSADDRCONFIGOFPCCONFIG                                                          
#define TCPIP_EQ2_RANDOMNUMBERFCTPTROFPCCONFIG                                                      TcpIp_RandomNumberFctPtr
#define TCPIP_EQ2_SIZEOFDEFAULTADDRV6OFPCCONFIG                                                     
#define TCPIP_EQ2_SIZEOFDHCPUSEROPTIONBUFFEROFPCCONFIG                                              
#define TCPIP_EQ2_SIZEOFDHCPUSEROPTIONOFPCCONFIG                                                    
#define TCPIP_EQ2_SIZEOFIPV6MULTICASTADDRACTIVEOFPCCONFIG                                           
#define TCPIP_EQ2_SIZEOFIPV6MULTICASTADDROFPCCONFIG                                                 
#define TCPIP_EQ2_SIZEOFPHYSADDRCONFIGOFPCCONFIG                                                    
#define TCPIP_EQ2_SIZEOFTCPOOOQELEMENTOFPCCONFIG                                                    
#define TCPIP_EQ2_SIZEOFTXREQELEMLISTOFPCCONFIG                                                     
#define TCPIP_EQ2_SIZEOFTXREQELEMOFPCCONFIG                                                         
#define TCPIP_EQ2_SIZEOFUDPTXRETRYQUEUEELEMENTCHAINOFPCCONFIG                                       
#define TCPIP_EQ2_SIZEOFUDPTXRETRYQUEUEELEMENTSOFPCCONFIG                                           
#define TCPIP_EQ2_SIZEOFUDPTXRETRYQUEUEPOOLDESCOFPCCONFIG                                           
#define TCPIP_EQ2_SOCKETDYNOFPCCONFIG                                                               TcpIp_SocketDyn.raw
#define TCPIP_EQ2_SOCKETOWNERCONFIGOFPCCONFIG                                                       TcpIp_SocketOwnerConfig
#define TCPIP_EQ2_SOCKETTCPDYNOFPCCONFIG                                                            TcpIp_SocketTcpDyn
#define TCPIP_EQ2_SOCKETUDPDYNOFPCCONFIG                                                            TcpIp_SocketUdpDyn
#define TCPIP_EQ2_TCPCONFIGOFPCCONFIG                                                               TcpIp_TcpConfig
#define TCPIP_EQ2_TCPOOOQELEMENTOFPCCONFIG                                                          
#define TCPIP_EQ2_TCPRESETQELEMENTOFPCCONFIG                                                        TcpIp_TcpResetQElement
#define TCPIP_EQ2_TCPRETRYQELEMENTOFPCCONFIG                                                        TcpIp_TcpRetryQElement
#define TCPIP_EQ2_TCPRXBUFFERDESCDYNOFPCCONFIG                                                      TcpIp_TcpRxBufferDescDyn
#define TCPIP_EQ2_TCPRXBUFFERDESCOFPCCONFIG                                                         TcpIp_TcpRxBufferDesc
#define TCPIP_EQ2_TCPRXBUFFEROFPCCONFIG                                                             TcpIp_TcpRxBuffer.raw
#define TCPIP_EQ2_TCPTXBUFFERDESCDYNOFPCCONFIG                                                      TcpIp_TcpTxBufferDescDyn
#define TCPIP_EQ2_TCPTXBUFFERDESCOFPCCONFIG                                                         TcpIp_TcpTxBufferDesc
#define TCPIP_EQ2_TCPTXBUFFEROFPCCONFIG                                                             TcpIp_TcpTxBuffer.raw
#define TCPIP_EQ2_TXREQELEMLISTDYNOFPCCONFIG                                                        
#define TCPIP_EQ2_TXREQELEMLISTOFPCCONFIG                                                           
#define TCPIP_EQ2_TXREQELEMOFPCCONFIG                                                               
#define TCPIP_EQ2_UDPTXRETRYQUEUEELEMENTCHAINOFPCCONFIG                                             
#define TCPIP_EQ2_UDPTXRETRYQUEUEELEMENTSOFPCCONFIG                                                 
#define TCPIP_EQ2_UDPTXRETRYQUEUEPOOLDESCOFPCCONFIG                                                 
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCSymbolicInitializationPointers  TcpIp Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define TcpIp_Config_Ptr                                                                            &(TcpIp_PCConfig.Config)  /**< symbolic identifier which shall be used to initialize 'TcpIp' */
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCInitializationSymbols  TcpIp Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define TcpIp_Config                                                                                TcpIp_PCConfig.Config  /**< symbolic identifier which could be used to initialize 'TcpIp */
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCGeneral  TcpIp General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define TCPIP_CHECK_INIT_POINTER                                                                    STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define TCPIP_FINAL_MAGIC_NUMBER                                                                    0xAA1Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of TcpIp */
#define TCPIP_INDIVIDUAL_POSTBUILD                                                                  STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'TcpIp' is not configured to be postbuild capable. */
#define TCPIP_INIT_DATA                                                                             TCPIP_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define TCPIP_INIT_DATA_HASH_CODE                                                                   1138706395  /**< the precompile constant to validate the initialization structure at initialization time of TcpIp with a hashcode. The seed value is '0xAA1Eu' */
#define TCPIP_USE_ECUM_BSW_ERROR_HOOK                                                               STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define TCPIP_USE_INIT_POINTER                                                                      STD_ON  /**< STD_ON if the init pointer TcpIp shall be used. */
/** 
  \}
*/ 





/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  TcpIpPCGetRootDataMacros  TcpIp Get Root Data Macros (PRE_COMPILE)
  \brief  These macros are used to get data pointers of root data.
  \{
*/ 
#define TcpIp_GetDefaultAddrV6OfPCConfig()                                                          TcpIp_ConfigDataPtr->DefaultAddrV6OfPCConfig
#define TcpIp_GetDhcpUserOptionBufferOfPCConfig()                                                   TcpIp_ConfigDataPtr->DhcpUserOptionBufferOfPCConfig
#define TcpIp_GetDhcpUserOptionDynOfPCConfig()                                                      TcpIp_ConfigDataPtr->DhcpUserOptionDynOfPCConfig
#define TcpIp_GetDhcpUserOptionOfPCConfig()                                                         TcpIp_ConfigDataPtr->DhcpUserOptionOfPCConfig
#define TcpIp_GetIpV6MulticastAddrActiveOfPCConfig()                                                TcpIp_ConfigDataPtr->IpV6MulticastAddrActiveOfPCConfig
#define TcpIp_GetIpV6MulticastAddrOfPCConfig()                                                      TcpIp_ConfigDataPtr->IpV6MulticastAddrOfPCConfig
#define TcpIp_GetPhysAddrConfigOfPCConfig()                                                         TcpIp_ConfigDataPtr->PhysAddrConfigOfPCConfig
#define TcpIp_GetSizeOfDefaultAddrV6OfPCConfig()                                                    TcpIp_ConfigDataPtr->SizeOfDefaultAddrV6OfPCConfig
#define TcpIp_GetSizeOfDhcpUserOptionBufferOfPCConfig()                                             TcpIp_ConfigDataPtr->SizeOfDhcpUserOptionBufferOfPCConfig
#define TcpIp_GetSizeOfDhcpUserOptionOfPCConfig()                                                   TcpIp_ConfigDataPtr->SizeOfDhcpUserOptionOfPCConfig
#define TcpIp_GetSizeOfIpV6MulticastAddrActiveOfPCConfig()                                          TcpIp_ConfigDataPtr->SizeOfIpV6MulticastAddrActiveOfPCConfig
#define TcpIp_GetSizeOfIpV6MulticastAddrOfPCConfig()                                                TcpIp_ConfigDataPtr->SizeOfIpV6MulticastAddrOfPCConfig
#define TcpIp_GetSizeOfPhysAddrConfigOfPCConfig()                                                   TcpIp_ConfigDataPtr->SizeOfPhysAddrConfigOfPCConfig
#define TcpIp_GetSizeOfTcpOooQElementOfPCConfig()                                                   TcpIp_ConfigDataPtr->SizeOfTcpOooQElementOfPCConfig
#define TcpIp_GetSizeOfTxReqElemListOfPCConfig()                                                    TcpIp_ConfigDataPtr->SizeOfTxReqElemListOfPCConfig
#define TcpIp_GetSizeOfTxReqElemOfPCConfig()                                                        TcpIp_ConfigDataPtr->SizeOfTxReqElemOfPCConfig
#define TcpIp_GetSizeOfUdpTxRetryQueueElementChainOfPCConfig()                                      TcpIp_ConfigDataPtr->SizeOfUdpTxRetryQueueElementChainOfPCConfig
#define TcpIp_GetSizeOfUdpTxRetryQueueElementsOfPCConfig()                                          TcpIp_ConfigDataPtr->SizeOfUdpTxRetryQueueElementsOfPCConfig
#define TcpIp_GetSizeOfUdpTxRetryQueuePoolDescOfPCConfig()                                          TcpIp_ConfigDataPtr->SizeOfUdpTxRetryQueuePoolDescOfPCConfig
#define TcpIp_GetTcpOooQElementOfPCConfig()                                                         TcpIp_ConfigDataPtr->TcpOooQElementOfPCConfig
#define TcpIp_GetTxReqElemListDynOfPCConfig()                                                       TcpIp_ConfigDataPtr->TxReqElemListDynOfPCConfig
#define TcpIp_GetTxReqElemListOfPCConfig()                                                          TcpIp_ConfigDataPtr->TxReqElemListOfPCConfig
#define TcpIp_GetTxReqElemOfPCConfig()                                                              TcpIp_ConfigDataPtr->TxReqElemOfPCConfig
#define TcpIp_GetUdpTxRetryQueueElementChainOfPCConfig()                                            TcpIp_ConfigDataPtr->UdpTxRetryQueueElementChainOfPCConfig
#define TcpIp_GetUdpTxRetryQueueElementsOfPCConfig()                                                TcpIp_ConfigDataPtr->UdpTxRetryQueueElementsOfPCConfig
#define TcpIp_GetUdpTxRetryQueuePoolDescOfPCConfig()                                                TcpIp_ConfigDataPtr->UdpTxRetryQueuePoolDescOfPCConfig
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCGetConstantDuplicatedRootDataMacros  TcpIp Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define TcpIp_GetDuplicateAddrDetectionFctPtrOfPCConfig()                                           TcpIp_DuplicateAddrDetectionFctPtr  /**< the pointer to TcpIp_DuplicateAddrDetectionFctPtr */
#define TcpIp_GetEthIfCtrlOfPCConfig()                                                              TcpIp_EthIfCtrl  /**< the pointer to TcpIp_EthIfCtrl */
#define TcpIp_GetIcmpV6ConfigOfPCConfig()                                                           TcpIp_IcmpV6Config  /**< the pointer to TcpIp_IcmpV6Config */
#define TcpIp_GetIcmpV6MsgHandlerCbkFctPtrOfPCConfig()                                              TcpIp_IcmpV6MsgHandlerCbkFctPtr  /**< the pointer to TcpIp_IcmpV6MsgHandlerCbkFctPtr */
#define TcpIp_GetIcmpV6TxMsgBufferOfPCConfig()                                                      TcpIp_IcmpV6TxMsgBuffer  /**< the pointer to TcpIp_IcmpV6TxMsgBuffer */
#define TcpIp_GetInterfaceIdentifierOfPCConfig()                                                    TcpIp_InterfaceIdentifier.raw  /**< the pointer to TcpIp_InterfaceIdentifier */
#define TcpIp_GetIpV6CtrlDynOfPCConfig()                                                            TcpIp_IpV6CtrlDyn.raw  /**< the pointer to TcpIp_IpV6CtrlDyn */
#define TcpIp_GetIpV6CtrlOfPCConfig()                                                               TcpIp_IpV6Ctrl  /**< the pointer to TcpIp_IpV6Ctrl */
#define TcpIp_GetIpV6DefaultRouterListEntryOfPCConfig()                                             TcpIp_IpV6DefaultRouterListEntry.raw  /**< the pointer to TcpIp_IpV6DefaultRouterListEntry */
#define TcpIp_GetIpV6DestinationCacheEntryOfPCConfig()                                              TcpIp_IpV6DestinationCacheEntry.raw  /**< the pointer to TcpIp_IpV6DestinationCacheEntry */
#define TcpIp_GetIpV6EthBufDataOfPCConfig()                                                         TcpIp_IpV6EthBufData.raw  /**< the pointer to TcpIp_IpV6EthBufData */
#define TcpIp_GetIpV6GeneralOfPCConfig()                                                            TcpIp_IpV6General  /**< the pointer to TcpIp_IpV6General */
#define TcpIp_GetIpV6NeighborCacheEntryOfPCConfig()                                                 TcpIp_IpV6NeighborCacheEntry.raw  /**< the pointer to TcpIp_IpV6NeighborCacheEntry */
#define TcpIp_GetIpV6PrefixListEntryOfPCConfig()                                                    TcpIp_IpV6PrefixListEntry.raw  /**< the pointer to TcpIp_IpV6PrefixListEntry */
#define TcpIp_GetIpV6SocketDynOfPCConfig()                                                          TcpIp_IpV6SocketDyn  /**< the pointer to TcpIp_IpV6SocketDyn */
#define TcpIp_GetIpV6SourceAddressOfPCConfig()                                                      TcpIp_IpV6SourceAddress  /**< the pointer to TcpIp_IpV6SourceAddress */
#define TcpIp_GetIpV6SourceAddressTableEntryOfPCConfig()                                            TcpIp_IpV6SourceAddressTableEntry  /**< the pointer to TcpIp_IpV6SourceAddressTableEntry */
#define TcpIp_GetLocalAddrOfPCConfig()                                                              TcpIp_LocalAddr.raw  /**< the pointer to TcpIp_LocalAddr */
#define TcpIp_GetLocalAddrV6OfPCConfig()                                                            TcpIp_LocalAddrV6  /**< the pointer to TcpIp_LocalAddrV6 */
#define TcpIp_GetNdpConfigOfPCConfig()                                                              TcpIp_NdpConfig  /**< the pointer to TcpIp_NdpConfig */
#define TcpIp_GetRandomNumberFctPtrOfPCConfig()                                                     TcpIp_RandomNumberFctPtr  /**< the pointer to TcpIp_RandomNumberFctPtr */
#define TcpIp_GetSizeOfEthIfCtrlOfPCConfig()                                                        1u  /**< the number of accomplishable value elements in TcpIp_EthIfCtrl */
#define TcpIp_GetSizeOfIcmpV6ConfigOfPCConfig()                                                     1u  /**< the number of accomplishable value elements in TcpIp_IcmpV6Config */
#define TcpIp_GetSizeOfIcmpV6TxMsgBufferOfPCConfig()                                                1280u  /**< the number of accomplishable value elements in TcpIp_IcmpV6TxMsgBuffer */
#define TcpIp_GetSizeOfInterfaceIdentifierOfPCConfig()                                              64u  /**< the number of accomplishable value elements in TcpIp_InterfaceIdentifier */
#define TcpIp_GetSizeOfIpV6CtrlOfPCConfig()                                                         1u  /**< the number of accomplishable value elements in TcpIp_IpV6Ctrl */
#define TcpIp_GetSizeOfIpV6DefaultRouterListEntryOfPCConfig()                                       2u  /**< the number of accomplishable value elements in TcpIp_IpV6DefaultRouterListEntry */
#define TcpIp_GetSizeOfIpV6DestinationCacheEntryOfPCConfig()                                        5u  /**< the number of accomplishable value elements in TcpIp_IpV6DestinationCacheEntry */
#define TcpIp_GetSizeOfIpV6EthBufDataOfPCConfig()                                                   2u  /**< the number of accomplishable value elements in TcpIp_IpV6EthBufData */
#define TcpIp_GetSizeOfIpV6GeneralOfPCConfig()                                                      1u  /**< the number of accomplishable value elements in TcpIp_IpV6General */
#define TcpIp_GetSizeOfIpV6NeighborCacheEntryOfPCConfig()                                           5u  /**< the number of accomplishable value elements in TcpIp_IpV6NeighborCacheEntry */
#define TcpIp_GetSizeOfIpV6PrefixListEntryOfPCConfig()                                              5u  /**< the number of accomplishable value elements in TcpIp_IpV6PrefixListEntry */
#define TcpIp_GetSizeOfIpV6SocketDynOfPCConfig()                                                    5u  /**< the number of accomplishable value elements in TcpIp_IpV6SocketDyn */
#define TcpIp_GetSizeOfIpV6SourceAddressOfPCConfig()                                                1u  /**< the number of accomplishable value elements in TcpIp_IpV6SourceAddress */
#define TcpIp_GetSizeOfLocalAddrOfPCConfig()                                                        2u  /**< the number of accomplishable value elements in TcpIp_LocalAddr */
#define TcpIp_GetSizeOfLocalAddrV6OfPCConfig()                                                      2u  /**< the number of accomplishable value elements in TcpIp_LocalAddrV6 */
#define TcpIp_GetSizeOfNdpConfigOfPCConfig()                                                        1u  /**< the number of accomplishable value elements in TcpIp_NdpConfig */
#define TcpIp_GetSizeOfSocketDynOfPCConfig()                                                        2u  /**< the number of accomplishable value elements in TcpIp_SocketDyn */
#define TcpIp_GetSizeOfSocketOwnerConfigOfPCConfig()                                                1u  /**< the number of accomplishable value elements in TcpIp_SocketOwnerConfig */
#define TcpIp_GetSizeOfSocketTcpDynOfPCConfig()                                                     1u  /**< the number of accomplishable value elements in TcpIp_SocketTcpDyn */
#define TcpIp_GetSizeOfSocketUdpDynOfPCConfig()                                                     1u  /**< the number of accomplishable value elements in TcpIp_SocketUdpDyn */
#define TcpIp_GetSizeOfTcpConfigOfPCConfig()                                                        1u  /**< the number of accomplishable value elements in TcpIp_TcpConfig */
#define TcpIp_GetSizeOfTcpResetQElementOfPCConfig()                                                 8u  /**< the number of accomplishable value elements in TcpIp_TcpResetQElement */
#define TcpIp_GetSizeOfTcpRetryQElementOfPCConfig()                                                 10u  /**< the number of accomplishable value elements in TcpIp_TcpRetryQElement */
#define TcpIp_GetSizeOfTcpRxBufferDescOfPCConfig()                                                  1u  /**< the number of accomplishable value elements in TcpIp_TcpRxBufferDesc */
#define TcpIp_GetSizeOfTcpRxBufferOfPCConfig()                                                      4000u  /**< the number of accomplishable value elements in TcpIp_TcpRxBuffer */
#define TcpIp_GetSizeOfTcpTxBufferDescOfPCConfig()                                                  1u  /**< the number of accomplishable value elements in TcpIp_TcpTxBufferDesc */
#define TcpIp_GetSizeOfTcpTxBufferOfPCConfig()                                                      500u  /**< the number of accomplishable value elements in TcpIp_TcpTxBuffer */
#define TcpIp_GetSocketDynOfPCConfig()                                                              TcpIp_SocketDyn.raw  /**< the pointer to TcpIp_SocketDyn */
#define TcpIp_GetSocketOwnerConfigOfPCConfig()                                                      TcpIp_SocketOwnerConfig  /**< the pointer to TcpIp_SocketOwnerConfig */
#define TcpIp_GetSocketTcpDynOfPCConfig()                                                           TcpIp_SocketTcpDyn  /**< the pointer to TcpIp_SocketTcpDyn */
#define TcpIp_GetSocketUdpDynOfPCConfig()                                                           TcpIp_SocketUdpDyn  /**< the pointer to TcpIp_SocketUdpDyn */
#define TcpIp_GetTcpConfigOfPCConfig()                                                              TcpIp_TcpConfig  /**< the pointer to TcpIp_TcpConfig */
#define TcpIp_GetTcpResetQElementOfPCConfig()                                                       TcpIp_TcpResetQElement  /**< the pointer to TcpIp_TcpResetQElement */
#define TcpIp_GetTcpRetryQElementOfPCConfig()                                                       TcpIp_TcpRetryQElement  /**< the pointer to TcpIp_TcpRetryQElement */
#define TcpIp_GetTcpRxBufferDescDynOfPCConfig()                                                     TcpIp_TcpRxBufferDescDyn  /**< the pointer to TcpIp_TcpRxBufferDescDyn */
#define TcpIp_GetTcpRxBufferDescOfPCConfig()                                                        TcpIp_TcpRxBufferDesc  /**< the pointer to TcpIp_TcpRxBufferDesc */
#define TcpIp_GetTcpRxBufferOfPCConfig()                                                            TcpIp_TcpRxBuffer.raw  /**< the pointer to TcpIp_TcpRxBuffer */
#define TcpIp_GetTcpTxBufferDescDynOfPCConfig()                                                     TcpIp_TcpTxBufferDescDyn  /**< the pointer to TcpIp_TcpTxBufferDescDyn */
#define TcpIp_GetTcpTxBufferDescOfPCConfig()                                                        TcpIp_TcpTxBufferDesc  /**< the pointer to TcpIp_TcpTxBufferDesc */
#define TcpIp_GetTcpTxBufferOfPCConfig()                                                            TcpIp_TcpTxBuffer.raw  /**< the pointer to TcpIp_TcpTxBuffer */
/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCGetDuplicatedRootDataMacros  TcpIp Get Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated root data elements.
  \{
*/ 
#define TcpIp_GetSizeOfDhcpUserOptionDynOfPCConfig()                                                TcpIp_GetSizeOfDhcpUserOptionOfPCConfig()  /**< the number of accomplishable value elements in TcpIp_DhcpUserOptionDyn */
#define TcpIp_GetSizeOfIpV6CtrlDynOfPCConfig()                                                      TcpIp_GetSizeOfIpV6CtrlOfPCConfig()  /**< the number of accomplishable value elements in TcpIp_IpV6CtrlDyn */
#define TcpIp_GetSizeOfIpV6SourceAddressTableEntryOfPCConfig()                                      TcpIp_GetSizeOfIpV6SourceAddressOfPCConfig()  /**< the number of accomplishable value elements in TcpIp_IpV6SourceAddressTableEntry */
#define TcpIp_GetSizeOfTcpRxBufferDescDynOfPCConfig()                                               TcpIp_GetSizeOfTcpRxBufferDescOfPCConfig()  /**< the number of accomplishable value elements in TcpIp_TcpRxBufferDescDyn */
#define TcpIp_GetSizeOfTcpTxBufferDescDynOfPCConfig()                                               TcpIp_GetSizeOfTcpTxBufferDescOfPCConfig()  /**< the number of accomplishable value elements in TcpIp_TcpTxBufferDescDyn */
#define TcpIp_GetSizeOfTxReqElemListDynOfPCConfig()                                                 TcpIp_GetSizeOfTxReqElemListOfPCConfig()  /**< the number of accomplishable value elements in TcpIp_TxReqElemListDyn */
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  GLOBAL DATA TYPES - SIMPLE
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  TcpIpPCIterableTypes  TcpIp Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate TcpIp_DefaultAddrV6 */
typedef uint8_least TcpIp_DefaultAddrV6IterType;

/**   \brief  type used to iterate TcpIp_DhcpUserOption */
typedef uint8_least TcpIp_DhcpUserOptionIterType;

/**   \brief  type used to iterate TcpIp_DhcpUserOptionBuffer */
typedef uint8_least TcpIp_DhcpUserOptionBufferIterType;

/**   \brief  type used to iterate TcpIp_EthIfCtrl */
typedef uint8_least TcpIp_EthIfCtrlIterType;

/**   \brief  type used to iterate TcpIp_IcmpV6Config */
typedef uint8_least TcpIp_IcmpV6ConfigIterType;

/**   \brief  type used to iterate TcpIp_IcmpV6TxMsgBuffer */
typedef uint16_least TcpIp_IcmpV6TxMsgBufferIterType;

/**   \brief  type used to iterate TcpIp_InterfaceIdentifier */
typedef uint8_least TcpIp_InterfaceIdentifierIterType;

/**   \brief  type used to iterate TcpIp_IpV6Ctrl */
typedef uint8_least TcpIp_IpV6CtrlIterType;

/**   \brief  type used to iterate TcpIp_IpV6DefaultRouterListEntry */
typedef uint8_least TcpIp_IpV6DefaultRouterListEntryIterType;

/**   \brief  type used to iterate TcpIp_IpV6DestinationCacheEntry */
typedef uint8_least TcpIp_IpV6DestinationCacheEntryIterType;

/**   \brief  type used to iterate TcpIp_IpV6EthBufData */
typedef uint8_least TcpIp_IpV6EthBufDataIterType;

/**   \brief  type used to iterate TcpIp_IpV6General */
typedef uint8_least TcpIp_IpV6GeneralIterType;

/**   \brief  type used to iterate TcpIp_IpV6MulticastAddr */
typedef uint8_least TcpIp_IpV6MulticastAddrIterType;

/**   \brief  type used to iterate TcpIp_IpV6MulticastAddrActive */
typedef uint8_least TcpIp_IpV6MulticastAddrActiveIterType;

/**   \brief  type used to iterate TcpIp_IpV6NeighborCacheEntry */
typedef uint8_least TcpIp_IpV6NeighborCacheEntryIterType;

/**   \brief  type used to iterate TcpIp_IpV6PrefixListEntry */
typedef uint8_least TcpIp_IpV6PrefixListEntryIterType;

/**   \brief  type used to iterate TcpIp_IpV6SocketDyn */
typedef uint8_least TcpIp_IpV6SocketDynIterType;

/**   \brief  type used to iterate TcpIp_IpV6SourceAddress */
typedef uint8_least TcpIp_IpV6SourceAddressIterType;

/**   \brief  type used to iterate TcpIp_LocalAddr */
typedef uint8_least TcpIp_LocalAddrIterType;

/**   \brief  type used to iterate TcpIp_LocalAddrV6 */
typedef uint8_least TcpIp_LocalAddrV6IterType;

/**   \brief  type used to iterate TcpIp_NdpConfig */
typedef uint8_least TcpIp_NdpConfigIterType;

/**   \brief  type used to iterate TcpIp_PhysAddrConfig */
typedef uint8_least TcpIp_PhysAddrConfigIterType;

/**   \brief  type used to iterate TcpIp_SocketDyn */
typedef uint8_least TcpIp_SocketDynIterType;

/**   \brief  type used to iterate TcpIp_SocketOwnerConfig */
typedef uint8_least TcpIp_SocketOwnerConfigIterType;

/**   \brief  type used to iterate TcpIp_SocketTcpDyn */
typedef uint8_least TcpIp_SocketTcpDynIterType;

/**   \brief  type used to iterate TcpIp_SocketUdpDyn */
typedef uint8_least TcpIp_SocketUdpDynIterType;

/**   \brief  type used to iterate TcpIp_TcpConfig */
typedef uint8_least TcpIp_TcpConfigIterType;

/**   \brief  type used to iterate TcpIp_TcpOooQElement */
typedef uint8_least TcpIp_TcpOooQElementIterType;

/**   \brief  type used to iterate TcpIp_TcpResetQElement */
typedef uint8_least TcpIp_TcpResetQElementIterType;

/**   \brief  type used to iterate TcpIp_TcpRetryQElement */
typedef uint8_least TcpIp_TcpRetryQElementIterType;

/**   \brief  type used to iterate TcpIp_TcpRxBuffer */
typedef uint16_least TcpIp_TcpRxBufferIterType;

/**   \brief  type used to iterate TcpIp_TcpRxBufferDesc */
typedef uint8_least TcpIp_TcpRxBufferDescIterType;

/**   \brief  type used to iterate TcpIp_TcpTxBuffer */
typedef uint16_least TcpIp_TcpTxBufferIterType;

/**   \brief  type used to iterate TcpIp_TcpTxBufferDesc */
typedef uint8_least TcpIp_TcpTxBufferDescIterType;

/**   \brief  type used to iterate TcpIp_TxReqElem */
typedef uint8_least TcpIp_TxReqElemIterType;

/**   \brief  type used to iterate TcpIp_TxReqElemList */
typedef uint8_least TcpIp_TxReqElemListIterType;

/**   \brief  type used to iterate TcpIp_UdpTxRetryQueueElementChain */
typedef uint8_least TcpIp_UdpTxRetryQueueElementChainIterType;

/**   \brief  type used to iterate TcpIp_UdpTxRetryQueueElements */
typedef uint8_least TcpIp_UdpTxRetryQueueElementsIterType;

/**   \brief  type used to iterate TcpIp_UdpTxRetryQueuePoolDesc */
typedef uint8_least TcpIp_UdpTxRetryQueuePoolDescIterType;

/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCIterableTypesWithSizeRelations  TcpIp Iterable Types With Size Relations (PRE_COMPILE)
  \brief  These type definitions are used to iterate over a VAR based array with the same iterator as the related CONST array.
  \{
*/ 
/**   \brief  type used to iterate TcpIp_DhcpUserOptionDyn */
typedef TcpIp_DhcpUserOptionIterType TcpIp_DhcpUserOptionDynIterType;

/**   \brief  type used to iterate TcpIp_IpV6CtrlDyn */
typedef TcpIp_IpV6CtrlIterType TcpIp_IpV6CtrlDynIterType;

/**   \brief  type used to iterate TcpIp_IpV6SourceAddressTableEntry */
typedef TcpIp_IpV6SourceAddressIterType TcpIp_IpV6SourceAddressTableEntryIterType;

/**   \brief  type used to iterate TcpIp_TcpRxBufferDescDyn */
typedef TcpIp_TcpRxBufferDescIterType TcpIp_TcpRxBufferDescDynIterType;

/**   \brief  type used to iterate TcpIp_TcpTxBufferDescDyn */
typedef TcpIp_TcpTxBufferDescIterType TcpIp_TcpTxBufferDescDynIterType;

/**   \brief  type used to iterate TcpIp_TxReqElemListDyn */
typedef TcpIp_TxReqElemListIterType TcpIp_TxReqElemListDynIterType;

/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCValueTypes  TcpIp Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for TcpIp_CodeOfDhcpUserOption */
typedef uint8 TcpIp_CodeOfDhcpUserOptionType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionBufferEndIdxOfDhcpUserOption */
typedef uint8 TcpIp_DhcpUserOptionBufferEndIdxOfDhcpUserOptionType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionBufferLengthOfDhcpUserOption */
typedef uint8 TcpIp_DhcpUserOptionBufferLengthOfDhcpUserOptionType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionBufferStartIdxOfDhcpUserOption */
typedef uint8 TcpIp_DhcpUserOptionBufferStartIdxOfDhcpUserOptionType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionBuffer */
typedef uint8 TcpIp_DhcpUserOptionBufferType;

/**   \brief  value based type definition for TcpIp_LengthOfDhcpUserOptionDyn */
typedef uint16 TcpIp_LengthOfDhcpUserOptionDynType;

/**   \brief  value based type definition for TcpIp_IpV6CtrlIdxOfEthIfCtrl */
typedef uint8 TcpIp_IpV6CtrlIdxOfEthIfCtrlType;

/**   \brief  value based type definition for TcpIp_IpV6CtrlUsedOfEthIfCtrl */
typedef boolean TcpIp_IpV6CtrlUsedOfEthIfCtrlType;

/**   \brief  value based type definition for TcpIp_EchoRequestApiOfIcmpV6Config */
typedef boolean TcpIp_EchoRequestApiOfIcmpV6ConfigType;

/**   \brief  value based type definition for TcpIp_HopLimitOfIcmpV6Config */
typedef uint8 TcpIp_HopLimitOfIcmpV6ConfigType;

/**   \brief  value based type definition for TcpIp_IcmpV6TxMsgBuffer */
typedef uint8 TcpIp_IcmpV6TxMsgBufferType;

/**   \brief  value based type definition for TcpIp_InterfaceIdentifier */
typedef uint8 TcpIp_InterfaceIdentifierType;

/**   \brief  value based type definition for TcpIp_AllowLinkMtuReconfigurationOfIpV6Ctrl */
typedef boolean TcpIp_AllowLinkMtuReconfigurationOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DefaultHopLimitOfIpV6Ctrl */
typedef uint8 TcpIp_DefaultHopLimitOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DefaultLinkMtuOfIpV6Ctrl */
typedef uint16 TcpIp_DefaultLinkMtuOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DefaultTrafficClassFlowLabelNboOfIpV6Ctrl */
typedef uint8 TcpIp_DefaultTrafficClassFlowLabelNboOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DhcpModeOfIpV6Ctrl */
typedef uint8 TcpIp_DhcpModeOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_DhcpUserOptionEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_DhcpUserOptionStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_DhcpUserOptionUsedOfIpV6Ctrl */
typedef boolean TcpIp_DhcpUserOptionUsedOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_EnableDynHopLimitOfIpV6Ctrl */
typedef boolean TcpIp_EnableDynHopLimitOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_EnablePathMtuOfIpV6Ctrl */
typedef boolean TcpIp_EnablePathMtuOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_EthIfCtrlIdxOfIpV6Ctrl */
typedef uint8 TcpIp_EthIfCtrlIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_FramePrioDefaultOfIpV6Ctrl */
typedef uint8 TcpIp_FramePrioDefaultOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_HwChecksumIcmpOfIpV6Ctrl */
typedef boolean TcpIp_HwChecksumIcmpOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_HwChecksumIpDestinationOptionsOfIpV6Ctrl */
typedef boolean TcpIp_HwChecksumIpDestinationOptionsOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_HwChecksumIpHopByHopOptionsOfIpV6Ctrl */
typedef boolean TcpIp_HwChecksumIpHopByHopOptionsOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_HwChecksumIpRoutingOfIpV6Ctrl */
typedef boolean TcpIp_HwChecksumIpRoutingOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_HwChecksumTcpOfIpV6Ctrl */
typedef boolean TcpIp_HwChecksumTcpOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_HwChecksumUdpOfIpV6Ctrl */
typedef boolean TcpIp_HwChecksumUdpOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_InterfaceIdentifierEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_InterfaceIdentifierEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_InterfaceIdentifierStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_InterfaceIdentifierStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6DefaultRouterListEntryEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6DefaultRouterListEntryStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6DestinationCacheEntryEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6DestinationCacheEntryEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6DestinationCacheEntryStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6DestinationCacheEntryStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6EthBufDataEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6EthBufDataEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6EthBufDataStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6EthBufDataStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6MulticastAddrEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6MulticastAddrStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrUsedOfIpV6Ctrl */
typedef boolean TcpIp_IpV6MulticastAddrUsedOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6NeighborCacheEntryEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6NeighborCacheEntryEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6NeighborCacheEntryStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6NeighborCacheEntryStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6PrefixListEntryEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6PrefixListEntryEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6PrefixListEntryStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6PrefixListEntryStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6SourceAddressEndIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6SourceAddressEndIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_IpV6SourceAddressStartIdxOfIpV6Ctrl */
typedef uint8 TcpIp_IpV6SourceAddressStartIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_LocalAddrV6BcIdxOfIpV6Ctrl */
typedef uint8 TcpIp_LocalAddrV6BcIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_LocalAddrV6BcUsedOfIpV6Ctrl */
typedef boolean TcpIp_LocalAddrV6BcUsedOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_MaskedBitsOfIpV6Ctrl */
typedef uint16 TcpIp_MaskedBitsOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_NdpConfigIdxOfIpV6Ctrl */
typedef uint8 TcpIp_NdpConfigIdxOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_PathMtuTimeoutOfIpV6Ctrl */
typedef uint16 TcpIp_PathMtuTimeoutOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_BaseReachableTimeMsOfIpV6CtrlDyn */
typedef uint32 TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_CtrlPreviousStateOfIpV6CtrlDyn */
typedef uint8 TcpIp_CtrlPreviousStateOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_CtrlStateOfIpV6CtrlDyn */
typedef uint8 TcpIp_CtrlStateOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_CurHopLimitOfIpV6CtrlDyn */
typedef uint8 TcpIp_CurHopLimitOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_DefaultLinkMtuOfIpV6CtrlDyn */
typedef uint16 TcpIp_DefaultLinkMtuOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn */
typedef uint8 TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn */
typedef uint8 TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn */
typedef uint8 TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn */
typedef uint8 TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDyn */
typedef uint8 TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_ReachableTimeMsOfIpV6CtrlDyn */
typedef uint32 TcpIp_ReachableTimeMsOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_RetransTimerMsOfIpV6CtrlDyn */
typedef uint32 TcpIp_RetransTimerMsOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_IpV6SocketDynIdxOfIpV6EthBufData */
typedef uint8 TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType;

/**   \brief  value based type definition for TcpIp_UlTxReqTabIdxOfIpV6EthBufData */
typedef uint8 TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType;

/**   \brief  value based type definition for TcpIp_ExtDestAddrValidationEnabledOfIpV6General */
typedef boolean TcpIp_ExtDestAddrValidationEnabledOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_IpV6CtrlDefaultIdxOfIpV6General */
typedef uint8 TcpIp_IpV6CtrlDefaultIdxOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_IpV6CtrlDefaultUsedOfIpV6General */
typedef boolean TcpIp_IpV6CtrlDefaultUsedOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_IpV6SocketDynIcmpIdxOfIpV6General */
typedef uint8 TcpIp_IpV6SocketDynIcmpIdxOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_IpV6SocketDynNdpIdxOfIpV6General */
typedef uint8 TcpIp_IpV6SocketDynNdpIdxOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_IpV6SocketDynTcpRstIdxOfIpV6General */
typedef uint8 TcpIp_IpV6SocketDynTcpRstIdxOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_IpV6SocketDynTcpRstUsedOfIpV6General */
typedef boolean TcpIp_IpV6SocketDynTcpRstUsedOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_MaskedBitsOfIpV6General */
typedef uint8 TcpIp_MaskedBitsOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_DefaultAddrV6IdxOfIpV6MulticastAddr */
typedef uint8 TcpIp_DefaultAddrV6IdxOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_DefaultAddrV6UsedOfIpV6MulticastAddr */
typedef boolean TcpIp_DefaultAddrV6UsedOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrActiveIdxOfIpV6MulticastAddr */
typedef uint8 TcpIp_IpV6MulticastAddrActiveIdxOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrActiveUsedOfIpV6MulticastAddr */
typedef boolean TcpIp_IpV6MulticastAddrActiveUsedOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_LocalAddrV6IdxOfIpV6MulticastAddr */
typedef uint8 TcpIp_LocalAddrV6IdxOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_MaskedBitsOfIpV6MulticastAddr */
typedef uint8 TcpIp_MaskedBitsOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_EthIfFramePrioOfIpV6SocketDyn */
typedef uint8 TcpIp_EthIfFramePrioOfIpV6SocketDynType;

/**   \brief  value based type definition for TcpIp_FlagsOfIpV6SocketDyn */
typedef uint8 TcpIp_FlagsOfIpV6SocketDynType;

/**   \brief  value based type definition for TcpIp_HopLimitOfIpV6SocketDyn */
typedef uint8 TcpIp_HopLimitOfIpV6SocketDynType;

/**   \brief  value based type definition for TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn */
typedef uint8 TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType;

/**   \brief  value based type definition for TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDyn */
typedef uint32 TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType;

/**   \brief  value based type definition for TcpIp_AddressAssignVariantOfIpV6SourceAddress */
typedef uint8 TcpIp_AddressAssignVariantOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_DefaultAddrV6IdxOfIpV6SourceAddress */
typedef uint8 TcpIp_DefaultAddrV6IdxOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_DefaultAddrV6UsedOfIpV6SourceAddress */
typedef boolean TcpIp_DefaultAddrV6UsedOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_LocalAddrV6IdxOfIpV6SourceAddress */
typedef uint8 TcpIp_LocalAddrV6IdxOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_MaskedBitsOfIpV6SourceAddress */
typedef uint8 TcpIp_MaskedBitsOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_PrefixIsOnLinkOfIpV6SourceAddress */
typedef boolean TcpIp_PrefixIsOnLinkOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_AssignmentStateOfLocalAddr */
typedef uint8 TcpIp_AssignmentStateOfLocalAddrType;

/**   \brief  value based type definition for TcpIp_IpV6CtrlIdxOfLocalAddrV6 */
typedef uint8 TcpIp_IpV6CtrlIdxOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrIdxOfLocalAddrV6 */
typedef uint8 TcpIp_IpV6MulticastAddrIdxOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6 */
typedef boolean TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_IpV6SourceAddressIdxOfLocalAddrV6 */
typedef uint8 TcpIp_IpV6SourceAddressIdxOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_IpV6SourceAddressUsedOfLocalAddrV6 */
typedef boolean TcpIp_IpV6SourceAddressUsedOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_MaskedBitsOfLocalAddrV6 */
typedef uint8 TcpIp_MaskedBitsOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_DadTransmitsOfNdpConfig */
typedef uint8 TcpIp_DadTransmitsOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_DynamicReachableTimeOfNdpConfig */
typedef boolean TcpIp_DynamicReachableTimeOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_DynamicRetransTimerOfNdpConfig */
typedef boolean TcpIp_DynamicRetransTimerOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableNdpInvNaNcUpdateOfNdpConfig */
typedef boolean TcpIp_EnableNdpInvNaNcUpdateOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableNdpInvNaOfNdpConfig */
typedef boolean TcpIp_EnableNdpInvNaOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableNdpInvNsOfNdpConfig */
typedef boolean TcpIp_EnableNdpInvNsOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableNudOfNdpConfig */
typedef boolean TcpIp_EnableNudOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableOptimisticDadOfNdpConfig */
typedef boolean TcpIp_EnableOptimisticDadOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableRfc6106DnsslOptOfNdpConfig */
typedef boolean TcpIp_EnableRfc6106DnsslOptOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableRfc6106RdnssOptOfNdpConfig */
typedef boolean TcpIp_EnableRfc6106RdnssOptOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_EnableSlaacDelayOfNdpConfig */
typedef boolean TcpIp_EnableSlaacDelayOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MaskedBitsOfNdpConfig */
typedef uint16 TcpIp_MaskedBitsOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MaxRandomFactorOfNdpConfig */
typedef uint8 TcpIp_MaxRandomFactorOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MaxRtrSolicitationDelayOfNdpConfig */
typedef uint16 TcpIp_MaxRtrSolicitationDelayOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MaxRtrSolicitationsOfNdpConfig */
typedef uint8 TcpIp_MaxRtrSolicitationsOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MaxSlaacDelayOfNdpConfig */
typedef uint16 TcpIp_MaxSlaacDelayOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MinRandomFactorOfNdpConfig */
typedef uint8 TcpIp_MinRandomFactorOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_MulticastSolicitsOfNdpConfig */
typedef uint8 TcpIp_MulticastSolicitsOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_NudFirstProbeDelayOfNdpConfig */
typedef uint16 TcpIp_NudFirstProbeDelayOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_RandomReachableTimeOfNdpConfig */
typedef boolean TcpIp_RandomReachableTimeOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_ReachableTimeOfNdpConfig */
typedef uint16 TcpIp_ReachableTimeOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_RetransTimerOfNdpConfig */
typedef uint16 TcpIp_RetransTimerOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_RndRtrSolicitationDelayOfNdpConfig */
typedef boolean TcpIp_RndRtrSolicitationDelayOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_RtrSolicitationIntervalOfNdpConfig */
typedef uint16 TcpIp_RtrSolicitationIntervalOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_SlaacMinLifetimeOfNdpConfig */
typedef uint16 TcpIp_SlaacMinLifetimeOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_UnicastSolicitsOfNdpConfig */
typedef uint8 TcpIp_UnicastSolicitsOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_SizeOfDefaultAddrV6 */
typedef uint8 TcpIp_SizeOfDefaultAddrV6Type;

/**   \brief  value based type definition for TcpIp_SizeOfDhcpUserOption */
typedef uint8 TcpIp_SizeOfDhcpUserOptionType;

/**   \brief  value based type definition for TcpIp_SizeOfDhcpUserOptionBuffer */
typedef uint8 TcpIp_SizeOfDhcpUserOptionBufferType;

/**   \brief  value based type definition for TcpIp_SizeOfDhcpUserOptionDyn */
typedef uint8 TcpIp_SizeOfDhcpUserOptionDynType;

/**   \brief  value based type definition for TcpIp_SizeOfEthIfCtrl */
typedef uint8 TcpIp_SizeOfEthIfCtrlType;

/**   \brief  value based type definition for TcpIp_SizeOfIcmpV6Config */
typedef uint8 TcpIp_SizeOfIcmpV6ConfigType;

/**   \brief  value based type definition for TcpIp_SizeOfIcmpV6TxMsgBuffer */
typedef uint16 TcpIp_SizeOfIcmpV6TxMsgBufferType;

/**   \brief  value based type definition for TcpIp_SizeOfInterfaceIdentifier */
typedef uint8 TcpIp_SizeOfInterfaceIdentifierType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6Ctrl */
typedef uint8 TcpIp_SizeOfIpV6CtrlType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6CtrlDyn */
typedef uint8 TcpIp_SizeOfIpV6CtrlDynType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6DefaultRouterListEntry */
typedef uint8 TcpIp_SizeOfIpV6DefaultRouterListEntryType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6DestinationCacheEntry */
typedef uint8 TcpIp_SizeOfIpV6DestinationCacheEntryType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6EthBufData */
typedef uint8 TcpIp_SizeOfIpV6EthBufDataType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6General */
typedef uint8 TcpIp_SizeOfIpV6GeneralType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6MulticastAddr */
typedef uint8 TcpIp_SizeOfIpV6MulticastAddrType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6MulticastAddrActive */
typedef uint8 TcpIp_SizeOfIpV6MulticastAddrActiveType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6NeighborCacheEntry */
typedef uint8 TcpIp_SizeOfIpV6NeighborCacheEntryType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6PrefixListEntry */
typedef uint8 TcpIp_SizeOfIpV6PrefixListEntryType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6SocketDyn */
typedef uint8 TcpIp_SizeOfIpV6SocketDynType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6SourceAddress */
typedef uint8 TcpIp_SizeOfIpV6SourceAddressType;

/**   \brief  value based type definition for TcpIp_SizeOfIpV6SourceAddressTableEntry */
typedef uint8 TcpIp_SizeOfIpV6SourceAddressTableEntryType;

/**   \brief  value based type definition for TcpIp_SizeOfLocalAddr */
typedef uint8 TcpIp_SizeOfLocalAddrType;

/**   \brief  value based type definition for TcpIp_SizeOfLocalAddrV6 */
typedef uint8 TcpIp_SizeOfLocalAddrV6Type;

/**   \brief  value based type definition for TcpIp_SizeOfNdpConfig */
typedef uint8 TcpIp_SizeOfNdpConfigType;

/**   \brief  value based type definition for TcpIp_SizeOfPhysAddrConfig */
typedef uint8 TcpIp_SizeOfPhysAddrConfigType;

/**   \brief  value based type definition for TcpIp_SizeOfSocketDyn */
typedef uint8 TcpIp_SizeOfSocketDynType;

/**   \brief  value based type definition for TcpIp_SizeOfSocketOwnerConfig */
typedef uint8 TcpIp_SizeOfSocketOwnerConfigType;

/**   \brief  value based type definition for TcpIp_SizeOfSocketTcpDyn */
typedef uint8 TcpIp_SizeOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SizeOfSocketUdpDyn */
typedef uint8 TcpIp_SizeOfSocketUdpDynType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpConfig */
typedef uint8 TcpIp_SizeOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpOooQElement */
typedef uint8 TcpIp_SizeOfTcpOooQElementType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpResetQElement */
typedef uint8 TcpIp_SizeOfTcpResetQElementType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpRetryQElement */
typedef uint8 TcpIp_SizeOfTcpRetryQElementType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpRxBuffer */
typedef uint16 TcpIp_SizeOfTcpRxBufferType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpRxBufferDesc */
typedef uint8 TcpIp_SizeOfTcpRxBufferDescType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpRxBufferDescDyn */
typedef uint8 TcpIp_SizeOfTcpRxBufferDescDynType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpTxBuffer */
typedef uint16 TcpIp_SizeOfTcpTxBufferType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpTxBufferDesc */
typedef uint8 TcpIp_SizeOfTcpTxBufferDescType;

/**   \brief  value based type definition for TcpIp_SizeOfTcpTxBufferDescDyn */
typedef uint8 TcpIp_SizeOfTcpTxBufferDescDynType;

/**   \brief  value based type definition for TcpIp_SizeOfTxReqElem */
typedef uint8 TcpIp_SizeOfTxReqElemType;

/**   \brief  value based type definition for TcpIp_SizeOfTxReqElemList */
typedef uint8 TcpIp_SizeOfTxReqElemListType;

/**   \brief  value based type definition for TcpIp_SizeOfTxReqElemListDyn */
typedef uint8 TcpIp_SizeOfTxReqElemListDynType;

/**   \brief  value based type definition for TcpIp_SizeOfUdpTxRetryQueueElementChain */
typedef uint8 TcpIp_SizeOfUdpTxRetryQueueElementChainType;

/**   \brief  value based type definition for TcpIp_SizeOfUdpTxRetryQueueElements */
typedef uint8 TcpIp_SizeOfUdpTxRetryQueueElementsType;

/**   \brief  value based type definition for TcpIp_SizeOfUdpTxRetryQueuePoolDesc */
typedef uint8 TcpIp_SizeOfUdpTxRetryQueuePoolDescType;

/**   \brief  value based type definition for TcpIp_ListenActiveConnStatOfSocketDyn */
typedef uint8 TcpIp_ListenActiveConnStatOfSocketDynType;

/**   \brief  value based type definition for TcpIp_LocalAddrBindIdxOfSocketDyn */
typedef uint8 TcpIp_LocalAddrBindIdxOfSocketDynType;

/**   \brief  value based type definition for TcpIp_SocketOwnerConfigIdxOfSocketDyn */
typedef uint8 TcpIp_SocketOwnerConfigIdxOfSocketDynType;

/**   \brief  value based type definition for TcpIp_TxBufRequestedOfSocketDyn */
typedef boolean TcpIp_TxBufRequestedOfSocketDynType;

/**   \brief  value based type definition for TcpIp_TxIpAddrIdxOfSocketDyn */
typedef uint8 TcpIp_TxIpAddrIdxOfSocketDynType;

/**   \brief  value based type definition for TcpIp_EventIndicationPendingOfSocketTcpDyn */
typedef uint8 TcpIp_EventIndicationPendingOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_FinWait2TimeoutOfSocketTcpDyn */
typedef uint32 TcpIp_FinWait2TimeoutOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_IdleTimeoutShortOfSocketTcpDyn */
typedef uint32 TcpIp_IdleTimeoutShortOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_IssOfSocketTcpDyn */
typedef uint32 TcpIp_IssOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_MaxNumListenSocketsOfSocketTcpDyn */
typedef uint16 TcpIp_MaxNumListenSocketsOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_MslTimeoutOfSocketTcpDyn */
typedef uint32 TcpIp_MslTimeoutOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_PathMtuChangedOfSocketTcpDyn */
typedef boolean TcpIp_PathMtuChangedOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_PathMtuNewSizeOfSocketTcpDyn */
typedef uint16 TcpIp_PathMtuNewSizeOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_RcvNxtOfSocketTcpDyn */
typedef uint32 TcpIp_RcvNxtOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_RcvWndOfSocketTcpDyn */
typedef uint16 TcpIp_RcvWndOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_RetransmitTimeoutOfSocketTcpDyn */
typedef uint32 TcpIp_RetransmitTimeoutOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_RetryQFillNumOfSocketTcpDyn */
typedef uint8 TcpIp_RetryQFillNumOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_RstReceivedOfSocketTcpDyn */
typedef boolean TcpIp_RstReceivedOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_RtoReloadValueOfSocketTcpDyn */
typedef uint32 TcpIp_RtoReloadValueOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SndNxtOfSocketTcpDyn */
typedef uint32 TcpIp_SndNxtOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SndUnaOfSocketTcpDyn */
typedef uint32 TcpIp_SndUnaOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SndWl1OfSocketTcpDyn */
typedef uint32 TcpIp_SndWl1OfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SndWl2OfSocketTcpDyn */
typedef uint32 TcpIp_SndWl2OfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SndWndOfSocketTcpDyn */
typedef uint16 TcpIp_SndWndOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SockIsServerOfSocketTcpDyn */
typedef boolean TcpIp_SockIsServerOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SockStateNextOfSocketTcpDyn */
typedef uint8 TcpIp_SockStateNextOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SockStateOfSocketTcpDyn */
typedef uint8 TcpIp_SockStateOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDyn */
typedef uint8 TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDyn */
typedef uint8 TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TcpRetryQElementLastIdxOfSocketTcpDyn */
typedef uint8 TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TcpRxBufferDescIdxOfSocketTcpDyn */
typedef uint8 TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TcpTxBufferDescIdxOfSocketTcpDyn */
typedef uint8 TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxFlagsOfSocketTcpDyn */
typedef uint8 TcpIp_TxFlagsOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxMaxSegLenByteOfSocketTcpDyn */
typedef uint16 TcpIp_TxMaxSegLenByteOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxMssAgreedOfSocketTcpDyn */
typedef uint16 TcpIp_TxMssAgreedOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxNextSendSeqNoOfSocketTcpDyn */
typedef uint32 TcpIp_TxNextSendSeqNoOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxOneTimeOptsLenOfSocketTcpDyn */
typedef uint8 TcpIp_TxOneTimeOptsLenOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxOneTimeOptsOfSocketTcpDyn */
typedef uint8 TcpIp_TxOneTimeOptsOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxOptLenOfSocketTcpDyn */
typedef uint8 TcpIp_TxOptLenOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxReqFullyQueuedOfSocketTcpDyn */
typedef boolean TcpIp_TxReqFullyQueuedOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxReqSeqNoOfSocketTcpDyn */
typedef uint32 TcpIp_TxReqSeqNoOfSocketTcpDynType;

/**   \brief  value based type definition for TcpIp_TxReqElemListIdxOfSocketUdpDyn */
typedef uint8 TcpIp_TxReqElemListIdxOfSocketUdpDynType;

/**   \brief  value based type definition for TcpIp_TxRetrQueueMaxNumOfSocketUdpDyn */
typedef uint8 TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType;

/**   \brief  value based type definition for TcpIp_FinWait2TimeoutOfTcpConfig */
typedef uint16 TcpIp_FinWait2TimeoutOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_KeepAliveIntervalOfTcpConfig */
typedef uint8 TcpIp_KeepAliveIntervalOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_KeepAliveProbesMaxOfTcpConfig */
typedef uint8 TcpIp_KeepAliveProbesMaxOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_KeepAliveTimeOfTcpConfig */
typedef uint32 TcpIp_KeepAliveTimeOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_MslOfTcpConfig */
typedef uint16 TcpIp_MslOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_NagleTimeoutOfTcpConfig */
typedef uint8 TcpIp_NagleTimeoutOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_RetransTimeoutMaxOfTcpConfig */
typedef uint16 TcpIp_RetransTimeoutMaxOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_RetransTimeoutOfTcpConfig */
typedef uint8 TcpIp_RetransTimeoutOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_RxMssOfTcpConfig */
typedef uint16 TcpIp_RxMssOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_TcpOooQSizePerSocketAvgOfTcpConfig */
typedef uint8 TcpIp_TcpOooQSizePerSocketAvgOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_TcpOooQSizePerSocketMaxOfTcpConfig */
typedef uint8 TcpIp_TcpOooQSizePerSocketMaxOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_TcpRetryQSizeOfTcpConfig */
typedef uint8 TcpIp_TcpRetryQSizeOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_TimeToLiveDefaultOfTcpConfig */
typedef uint8 TcpIp_TimeToLiveDefaultOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_TxMssOfTcpConfig */
typedef uint16 TcpIp_TxMssOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_UserTimeoutDefCyclesOfTcpConfig */
typedef uint16 TcpIp_UserTimeoutDefCyclesOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_UserTimeoutMaxCyclesOfTcpConfig */
typedef uint8 TcpIp_UserTimeoutMaxCyclesOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_UserTimeoutMinCyclesOfTcpConfig */
typedef uint8 TcpIp_UserTimeoutMinCyclesOfTcpConfigType;

/**   \brief  value based type definition for TcpIp_TcpRxBuffer */
typedef uint8 TcpIp_TcpRxBufferType;

/**   \brief  value based type definition for TcpIp_TcpRxBufferEndIdxOfTcpRxBufferDesc */
typedef uint16 TcpIp_TcpRxBufferEndIdxOfTcpRxBufferDescType;

/**   \brief  value based type definition for TcpIp_TcpRxBufferLengthOfTcpRxBufferDesc */
typedef uint16 TcpIp_TcpRxBufferLengthOfTcpRxBufferDescType;

/**   \brief  value based type definition for TcpIp_TcpRxBufferStartIdxOfTcpRxBufferDesc */
typedef uint16 TcpIp_TcpRxBufferStartIdxOfTcpRxBufferDescType;

/**   \brief  value based type definition for TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDyn */
typedef uint8 TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType;

/**   \brief  value based type definition for TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDyn */
typedef uint16 TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType;

/**   \brief  value based type definition for TcpIp_TcpTxBuffer */
typedef uint8 TcpIp_TcpTxBufferType;

/**   \brief  value based type definition for TcpIp_TcpTxBufferEndIdxOfTcpTxBufferDesc */
typedef uint16 TcpIp_TcpTxBufferEndIdxOfTcpTxBufferDescType;

/**   \brief  value based type definition for TcpIp_TcpTxBufferLengthOfTcpTxBufferDesc */
typedef uint16 TcpIp_TcpTxBufferLengthOfTcpTxBufferDescType;

/**   \brief  value based type definition for TcpIp_TcpTxBufferStartIdxOfTcpTxBufferDesc */
typedef uint16 TcpIp_TcpTxBufferStartIdxOfTcpTxBufferDescType;

/**   \brief  value based type definition for TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDyn */
typedef uint8 TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType;

/**   \brief  value based type definition for TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDyn */
typedef uint16 TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType;

/**   \brief  value based type definition for TcpIp_TxReqElemDataLenByteOfTxReqElem */
typedef uint16 TcpIp_TxReqElemDataLenByteOfTxReqElemType;

/**   \brief  value based type definition for TcpIp_TxReqElemTransmittedOfTxReqElem */
typedef boolean TcpIp_TxReqElemTransmittedOfTxReqElemType;

/**   \brief  value based type definition for TcpIp_TxReqElemEndIdxOfTxReqElemList */
typedef uint8 TcpIp_TxReqElemEndIdxOfTxReqElemListType;

/**   \brief  value based type definition for TcpIp_TxReqElemLengthOfTxReqElemList */
typedef uint8 TcpIp_TxReqElemLengthOfTxReqElemListType;

/**   \brief  value based type definition for TcpIp_TxReqElemStartIdxOfTxReqElemList */
typedef uint8 TcpIp_TxReqElemStartIdxOfTxReqElemListType;

/**   \brief  value based type definition for TcpIp_FillNumOfTxReqElemListDyn */
typedef uint8 TcpIp_FillNumOfTxReqElemListDynType;

/**   \brief  value based type definition for TcpIp_ReadPosOfTxReqElemListDyn */
typedef uint8 TcpIp_ReadPosOfTxReqElemListDynType;

/**   \brief  value based type definition for TcpIp_SocketUdpDynIdxOfTxReqElemListDyn */
typedef uint8 TcpIp_SocketUdpDynIdxOfTxReqElemListDynType;

/**   \brief  value based type definition for TcpIp_WritePosOfTxReqElemListDyn */
typedef uint8 TcpIp_WritePosOfTxReqElemListDynType;

/** 
  \}
*/ 




/**********************************************************************************************************************
 *  GLOBAL DATA TYPES AND STRUCTURES (TCPIP)
 *********************************************************************************************************************/


#if !defined (TCPIP_LOCAL_INLINE)
# define TCPIP_LOCAL_INLINE LOCAL_INLINE
#endif

#define TcpIp_ConfigDataPtr (TcpIp_Config_Ptr)

typedef union 
{ /* PRQA S 750 */ /* MD_MSR_Union */
  uint8  data[4];
  uint32 data32;
} TcpIp_NvmDataBlockType;

typedef union 
{ /* PRQA S 750 */ /* MD_MSR_Union */
  uint8 data[6];
  uint16 data16[3];
} TcpIp_PhysAddrType;


typedef void (*TcpIp_IcmpV6IndAddrListReceivedCbkType)(uint8 CtrlIdx, TCPIP_P2C(uint8) RemoteLLAddrPtr, TCPIP_P2C(IpV6_AddrType) AddrListPtr, uint8 AddrCount);
typedef void (*TcpIp_IcmpV6MsgHandlerCbkType)(uint8 Type, uint8 Code, IPV6_P2C(uint8) OrigPktPtr, uint16 OrigPktLen, IPV6_P2C(uint8) MultiPartExtPtr, uint16 MultiPartExtLen);
typedef void (*TcpIp_IcmpV6MaxPayloadLenChgCbkType)(uint8 CtrlIdx, TCPIP_P2C(TcpIp_SockAddrType) SockAddrPtr, uint16 MaxPayloadLen);

typedef void(*TcpIp_PhysAddrTableChgDiscardedCbkType)(uint8 CtrlIdx, TCPIP_P2C(TcpIp_SockAddrType) IpAddrPtr);

typedef struct
{
  IpBase_AddrInType MatchIpSrcAddr;       /* IP source address of fragments (used to find reassembly buffer) */
  IpBase_AddrInType MatchIpDstAddr;       /* IP destination address of fragments (used to find reassembly buffer) */
  uint32            TimeMs;               /* Value of IpV4_Ip_TimeMs when the reassembly of this packet was started. */
  uint16            MatchIdentification;  /* Value of IP header identification field of fragments (used to find reassembly buffer) */
  uint16            NextGapOfs;           /* Offset (bytes) of the next gap in the reassembly buffer */
  uint16            FragmentLen;          /* length of the reassembly packet in bytes (will be 0 until last fragment has been received) */
  uint8             MatchIpProtocol;      /* Value of IP header protocal field of fragments (used to find reassembly buffer) */
  uint8             Status;               /* Status of this fragment buffer (UNUSED, REASSEMBLY_IN_PROGRESS, REASSEMBLY_COMPLETE, REASSEMBLY_ABORTED) */
  uint8             ReassemblyBufDescIdx; /* Index of the reassembly buffer descriptor itself (Accessed only in case the descriptor is given by a pointer) */
  uint8             Flags;                /* FIRST_FRAG_RECEIVED, LAST_FRAG_RECEIVED */
} IpV4_Ip_ReassemblyBufferDescType;

typedef struct
{
  uint8             Flags;
  uint8             TimeToLive;
  uint8             TypeOfService;
  uint8             EthIfFramePrio;
  uint8             PhysDestAddr[IPBASE_ETH_PHYS_ADDR_LEN_BYTE];
} IpV4_Ip_TransferBlockType;

typedef struct
{
  uint32                    DestAddrV4;       /*<! IPv4 destination address */
  IpBase_FamilyType         AddressFamily;    /*<! IP Family (INET or INET6) */
  uint16                    EthPayloadLen;    /*<! Length of ethernet payload (buffer) in bytes */
  uint16                    IpPayloadOfs;     /*<! Offset of IP payload (length of IP header) inside ethernet payload in bytes */
# if (TCPIP_SUPPORT_IPSEC == STD_ON)                                                                                    /* COV_MSR_UNSUPPORTED */
  uint16                    ExtHdrLenBytes;   /*<! Length of the IpSec header in bytes */
  TcpIp_IpSecPolicyType     IpSecPolicy;      /*<! Indicates the Ip security Policy of the packet */
  TcpIp_SizeOfSaEntryType   SaEntryIdx;       /*<! Security Association to be used for authentication of this packet */
  TcpIp_IpProtocolType      XptProtocol;      /*<! Next layer protocol type */
  uint8                     MessageType;      /*<! In case of XptProtocol == ICMP, the MessageType specifies the ICMP type. */
# endif
  boolean                   IsEthBufV6;       /*<! Indicates whether BufIdx refers to an EthIf buffer or an internal IPv6 fragment buffer */
  uint8                    *EthPayloadPtr;    /*<! Pointer to ethernet payload */
  uint8                     BufIdx;           /*<! Index of the first ethernet buffer for the IP packet. */
  uint8                     SrcAddrIdx;       /*<! IP address identifier of the address that will be used as source address */
  uint8                     CtrlIdx;          /*<! Index of the EthIf controller */
  uint8                     CurBufIdxV4;      /*<! Index of the currently active ethernet buffer. (Only used for IPv4 fragmentation.) */
  TcpIp_SizeOfSocketDynType SockIdx;          /*<! Index of the TcpIp socket this request belongs to. */
} TcpIp_IpTxRequestDescriptorType;

typedef struct
{
  TcpIp_IpTxRequestDescriptorType IpTxRequestDesc;  /* IP TX request descriptor */
  TcpIp_SockAddrBaseType          Destination;      /* IP destination address of the pending packet */
  uint16                          UdpPayloadLen;    /* length of the UDP payload in bytes */
  uint8                           UdpTxReqTabIdx;   /* UDP tx request table index (used for TxConfirmation) */
} TcpIp_Udp_RetryQueueElementType;  /* Used for UDP tx chache in case of PhysAddrMiss */


typedef struct
{
  boolean SynAvailable;
  uint32  RxSeqNo;
  uint16  RemPort;
  uint16  MaxTxSegSize;
  uint16  Window;
} TcpIp_Tcp_BackLogEleType;

typedef struct
{
  uint32 SeqNo;
  uint32 AckNo;
  uint8                  IpAddrIdx;
  TcpIp_SockAddrBaseType LocSock;
  TcpIp_SockAddrBaseType RemSock;
  uint8 Flags;
} TcpIp_Tcp_RstTxQueueType;

typedef struct
{
  /* TxRetrQueue element points to the start of the data inside the TxBuffer (circular buffer!). If the segments is
  splitted in two parts (wraparound), the second part will start at byte 0 of the TxBuffer. */
  TcpIp_SizeOfTcpTxBufferType        SizeTotByte;       /* size of the whole data block in byte */
  TcpIp_SizeOfTcpTxBufferType        DataBufStartIdx;   /* index of start of the first data block for this queue element (wraparound -> two blocks) */
  uint32                             SeqNo;             /* start sequence number for this message */
  uint32                             TimeStamp;         /* ECU TCP timestamp at queueing time */
  uint32                             UserTimeoutTs;     /* total timeout timestamp for acknowlaging this segment */
  TcpIp_SizeOfTcpRetryQElementType   EleNext;           /* index of next element */
  TcpIp_SizeOfTcpRetryQElementType   ElePrev;           /* index of previous element */
  TcpIp_SizeOfSocketTcpDynType       SocketTcpIdx;      /* index of related TCP connection */
  uint8                              TransmitCounter;   /* count the (re-)transmissions of this segment */
  uint8                              Flags;             /* TCP flags */
  uint8                              OneTimeOptions;    /* options that shall be sent once */ /* currently only used by UserTimeoutOption */
  uint8                              OneTimeOptionsLen; /* options len that shall be sent once */
  uint8                              SackFlag;          /* TCP SACK dirty flag */
} TcpIp_Tcp_TxRetrQueueType;  /* RetransmitQueueType */

typedef struct
{
  uint32                         SeqNo;          /* (TCP) sequence number */
  uint16                         LenByte;        /* length of received data */
  TcpIp_SizeOfTcpOooQElementType NextEleIdx;     /* index of the next element */
} TcpIp_Tcp_RxPreBufEleType;  /* 'out of order' rx elements */


typedef struct
{
  uint32 Xid;                     /* Transaction ID */
  uint16 Secs;
  uint16 Flags;
  TcpIp_NetAddrType Ciaddr;       /* Client IP address */
  TcpIp_NetAddrType NetMask;      /* Network Mask */
  TcpIp_NetAddrType DhcpSiaddr;   /* DHCP-server IP address */
  TcpIp_NetAddrType DefGWiaddr;   /* Default-GW IP address */
  TcpIp_NetAddrType DnsServAddr;  /* DNS-server IP address */
  uint32 T1;
  uint32 T2;
  uint32 LeaseTime;
} TcpIp_DhcpV4_DataStructType;


typedef struct
{
  uint32 S;  /*!< seconds  */
  uint32 Ms; /*!< milliseconds (< 1000) */
} TcpIp_DhcpV6_TimeType;

typedef struct
{
  TcpIp_DhcpV6_TimeType AbsRetransTimeout;  /* [msec] Maximum Retransmission Duration */
  uint32                InitRetransTimeout; /* [msec] Initial Retransmission Timeout */
  uint32                MaxRetransTime;     /* [msec] Maximum Retransmission Time */
  uint32                Timeout;            /* [msec] Retrasmission Timeout */
  uint8                 MaxRetransCount;    /* [#] Maximum Retransmission Count */
  uint8                 TxCount;            /* [#] Retransmission Count */
} TcpIp_DhcpV6_RetransParamType;

typedef struct
{
#ifdef C_COMMENT_VECTOR  /* ESCAN00089902 - Removed MsgPtr->CtrlIdx. */
#endif
  P2VAR(uint8, AUTOMATIC, TCPIP_APPL_VAR) MsgBufPtr;
  uint16                  MsgBufLen;
  uint16                  MsgBufPos;
  uint16                  OptStartPos[TCPIP_DHCPV6_OPT_STACK_SIZE]; /* used to store the offset of the current option field */
  uint8                   OptStackPos;                              /* current usage of the option stack */
  boolean                 TxPending;
} TcpIp_DhcpV6_MsgType;


typedef struct
{
  IpBase_AddrIn6Type Addr;
  uint32             PreferredUntil;
  uint32             ValidUntil;
} TcpIp_DhcpV6_IaAddrLeaseType;

typedef struct
{
  boolean                                        Valid;
  uint32                                         IaId;
  uint32                                         T1Timeout;
  uint32                                         T2Timeout;
  TcpIp_DhcpV6_IaAddrLeaseType                   AddrLease;
} TcpIp_DhcpV6_IaNaLeaseType;

typedef struct
{
  boolean Valid;
  uint8 Preference;
  uint8 ServerId[TCPIP_DHCPV6_DUID_MAX_LEN];
  uint8 ServerIdLen;
} TcpIp_DhcpV6_AdvInfoType;

typedef struct
{
  uint16           TypeNbo;
  uint16           HwTypeNbo;
  uint32           TimeNbo;
  Eth_PhysAddrType PhysAddr;
} TcpIp_DhcpV6_DuIdType1;

typedef struct
{
  uint16                  StatusCode;
  uint16                  StatusTextLen;
  P2CONST(uint8, AUTOMATIC, TCPIP_APPL_VAR) StatusTextPtr;
} TcpIp_DhcpV6_OptStatusCodeType;

typedef struct
{
  boolean                        Valid;
  IpBase_AddrIn6Type             Addr;
  uint32                         PreferredLifetime;
  uint32                         ValidLifetime;
  TcpIp_DhcpV6_OptStatusCodeType StatusCode;
} TcpIp_DhcpV6_OptIaAddrType;

typedef struct
{
  boolean                                      Valid;
  uint32                                       IaId;
  uint32                                       T1;
  uint32                                       T2;
  TcpIp_DhcpV6_OptStatusCodeType               StatusCode;
  TcpIp_DhcpV6_OptIaAddrType                   IaAddr;
} TcpIp_DhcpV6_OptIaNaType;



/**********************************************************************************************************************
  GLOBAL DATA TYPES - COMPLEX
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  TcpIpPCStructTypes  TcpIp Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in TcpIp_DhcpUserOption */
typedef struct sTcpIp_DhcpUserOptionType
{
  TcpIp_CodeOfDhcpUserOptionType CodeOfDhcpUserOption;  /**< Code of the user option. */
  TcpIp_DhcpUserOptionBufferEndIdxOfDhcpUserOptionType DhcpUserOptionBufferEndIdxOfDhcpUserOption;  /**< the end index of the 1:n relation pointing to TcpIp_DhcpUserOptionBuffer */
  TcpIp_DhcpUserOptionBufferLengthOfDhcpUserOptionType DhcpUserOptionBufferLengthOfDhcpUserOption;  /**< the number of relations pointing to TcpIp_DhcpUserOptionBuffer */
  TcpIp_DhcpUserOptionBufferStartIdxOfDhcpUserOptionType DhcpUserOptionBufferStartIdxOfDhcpUserOption;  /**< the start index of the 1:n relation pointing to TcpIp_DhcpUserOptionBuffer */
  TcpIp_DhcpUserOptionDirectionType DirectionOfDhcpUserOption;  /**< Direction of the user option. */
} TcpIp_DhcpUserOptionType;

/**   \brief  type used in TcpIp_DhcpUserOptionDyn */
typedef struct sTcpIp_DhcpUserOptionDynType
{
  TcpIp_LengthOfDhcpUserOptionDynType LengthOfDhcpUserOptionDyn;
} TcpIp_DhcpUserOptionDynType;

/**   \brief  type used in TcpIp_EthIfCtrl */
typedef struct sTcpIp_EthIfCtrlType
{
  TcpIp_IpV6CtrlIdxOfEthIfCtrlType IpV6CtrlIdxOfEthIfCtrl;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6Ctrl */
} TcpIp_EthIfCtrlType;

/**   \brief  type used in TcpIp_IcmpV6Config */
typedef struct sTcpIp_IcmpV6ConfigType
{
  TcpIp_EchoRequestApiOfIcmpV6ConfigType EchoRequestApiOfIcmpV6Config;  /**< Enable Echo Request API - Value of configuration parameter TcpIpIcmpV6EchoRequestApiEnabled [BOOLEAN] */
  TcpIp_HopLimitOfIcmpV6ConfigType HopLimitOfIcmpV6Config;  /**< Hop Limit - Value of configuration parameter TcpIpIcmpV6HopLimit */
} TcpIp_IcmpV6ConfigType;

/**   \brief  type used in TcpIp_IpV6Ctrl */
typedef struct sTcpIp_IpV6CtrlType
{
  TcpIp_DefaultLinkMtuOfIpV6CtrlType DefaultLinkMtuOfIpV6Ctrl;  /**< Default Mtu Size - Value of configuration parameter TcpIpIpV6DefaultMtuSize */
  TcpIp_MaskedBitsOfIpV6CtrlType MaskedBitsOfIpV6Ctrl;  /**< contains bitcoded the boolean data of TcpIp_AllowLinkMtuReconfigurationOfIpV6Ctrl, TcpIp_DhcpUserOptionUsedOfIpV6Ctrl, TcpIp_EnableDynHopLimitOfIpV6Ctrl, TcpIp_EnablePathMtuOfIpV6Ctrl, TcpIp_HwChecksumIcmpOfIpV6Ctrl, TcpIp_HwChecksumIpDestinationOptionsOfIpV6Ctrl, TcpIp_HwChecksumIpHopByHopOptionsOfIpV6Ctrl, TcpIp_HwChecksumIpRoutingOfIpV6Ctrl, TcpIp_HwChecksumTcpOfIpV6Ctrl, TcpIp_HwChecksumUdpOfIpV6Ctrl, TcpIp_IpV6MulticastAddrUsedOfIpV6Ctrl, TcpIp_LocalAddrV6BcUsedOfIpV6Ctrl */
  TcpIp_PathMtuTimeoutOfIpV6CtrlType PathMtuTimeoutOfIpV6Ctrl;  /**< Path Mtu Timeout - Value of configuration parameter TcpIpIpV6PathMtuTimeout [SECONDS] */
  TcpIp_DefaultHopLimitOfIpV6CtrlType DefaultHopLimitOfIpV6Ctrl;  /**< Default Hop Limit - Value of configuration parameter TcpIpIpV6DefaultHopLimit */
  TcpIp_DefaultTrafficClassFlowLabelNboOfIpV6CtrlType DefaultTrafficClassFlowLabelNboOfIpV6Ctrl;  /**< - */
  TcpIp_DhcpModeOfIpV6CtrlType DhcpModeOfIpV6Ctrl;  /**< - */
  TcpIp_DhcpUserOptionEndIdxOfIpV6CtrlType DhcpUserOptionEndIdxOfIpV6Ctrl;  /**< the end index of the 0:n relation pointing to TcpIp_DhcpUserOption */
  TcpIp_DhcpUserOptionStartIdxOfIpV6CtrlType DhcpUserOptionStartIdxOfIpV6Ctrl;  /**< the start index of the 0:n relation pointing to TcpIp_DhcpUserOption */
  TcpIp_EthIfCtrlIdxOfIpV6CtrlType EthIfCtrlIdxOfIpV6Ctrl;  /**< - */
  TcpIp_FramePrioDefaultOfIpV6CtrlType FramePrioDefaultOfIpV6Ctrl;  /**< IP Frame Prio Default - Value of configuration parameter TcpIpIpFramePrioDefault */
  TcpIp_InterfaceIdentifierEndIdxOfIpV6CtrlType InterfaceIdentifierEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_InterfaceIdentifier */
  TcpIp_InterfaceIdentifierStartIdxOfIpV6CtrlType InterfaceIdentifierStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_InterfaceIdentifier */
  TcpIp_IpV6DefaultRouterListEntryEndIdxOfIpV6CtrlType IpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_IpV6DefaultRouterListEntry */
  TcpIp_IpV6DefaultRouterListEntryStartIdxOfIpV6CtrlType IpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_IpV6DefaultRouterListEntry */
  TcpIp_IpV6DestinationCacheEntryEndIdxOfIpV6CtrlType IpV6DestinationCacheEntryEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_IpV6DestinationCacheEntry */
  TcpIp_IpV6DestinationCacheEntryStartIdxOfIpV6CtrlType IpV6DestinationCacheEntryStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_IpV6DestinationCacheEntry */
  TcpIp_IpV6EthBufDataEndIdxOfIpV6CtrlType IpV6EthBufDataEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_IpV6EthBufData */
  TcpIp_IpV6EthBufDataStartIdxOfIpV6CtrlType IpV6EthBufDataStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_IpV6EthBufData */
  TcpIp_IpV6MulticastAddrEndIdxOfIpV6CtrlType IpV6MulticastAddrEndIdxOfIpV6Ctrl;  /**< the end index of the 0:n relation pointing to TcpIp_IpV6MulticastAddr */
  TcpIp_IpV6MulticastAddrStartIdxOfIpV6CtrlType IpV6MulticastAddrStartIdxOfIpV6Ctrl;  /**< the start index of the 0:n relation pointing to TcpIp_IpV6MulticastAddr */
  TcpIp_IpV6NeighborCacheEntryEndIdxOfIpV6CtrlType IpV6NeighborCacheEntryEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_IpV6NeighborCacheEntry */
  TcpIp_IpV6NeighborCacheEntryStartIdxOfIpV6CtrlType IpV6NeighborCacheEntryStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_IpV6NeighborCacheEntry */
  TcpIp_IpV6PrefixListEntryEndIdxOfIpV6CtrlType IpV6PrefixListEntryEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_IpV6PrefixListEntry */
  TcpIp_IpV6PrefixListEntryStartIdxOfIpV6CtrlType IpV6PrefixListEntryStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_IpV6PrefixListEntry */
  TcpIp_IpV6SourceAddressEndIdxOfIpV6CtrlType IpV6SourceAddressEndIdxOfIpV6Ctrl;  /**< the end index of the 1:n relation pointing to TcpIp_IpV6SourceAddress */
  TcpIp_IpV6SourceAddressStartIdxOfIpV6CtrlType IpV6SourceAddressStartIdxOfIpV6Ctrl;  /**< the start index of the 1:n relation pointing to TcpIp_IpV6SourceAddress */
  TcpIp_LocalAddrV6BcIdxOfIpV6CtrlType LocalAddrV6BcIdxOfIpV6Ctrl;  /**< the index of the 0:1 relation pointing to TcpIp_LocalAddrV6 */
  TcpIp_NdpConfigIdxOfIpV6CtrlType NdpConfigIdxOfIpV6Ctrl;  /**< the index of the 1:1 relation pointing to TcpIp_NdpConfig */
} TcpIp_IpV6CtrlType;

/**   \brief  type used in TcpIp_IpV6CtrlDyn */
typedef struct sTcpIp_IpV6CtrlDynType
{
  TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType BaseReachableTimeMsOfIpV6CtrlDyn;  /**< Time a neighbor is considered to be reachable. */
  TcpIp_ReachableTimeMsOfIpV6CtrlDynType ReachableTimeMsOfIpV6CtrlDyn;  /**< Time a neighbor is considered to be reachable. */
  TcpIp_RetransTimerMsOfIpV6CtrlDynType RetransTimerMsOfIpV6CtrlDyn;  /**< Interval between retransmissions. */
  TcpIp_DefaultLinkMtuOfIpV6CtrlDynType DefaultLinkMtuOfIpV6CtrlDyn;  /**< - */
  TcpIp_CtrlPreviousStateOfIpV6CtrlDynType CtrlPreviousStateOfIpV6CtrlDyn;  /**< - */
  TcpIp_CtrlStateOfIpV6CtrlDynType CtrlStateOfIpV6CtrlDyn;  /**< - */
  TcpIp_CurHopLimitOfIpV6CtrlDynType CurHopLimitOfIpV6CtrlDyn;  /**< Hop Limit for outgoing packets, may be changed by received RAs. */
  TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6DefaultRouterListEntry */
  TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6DestinationCacheEntry */
  TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6NeighborCacheEntry */
  TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6PrefixListEntry */
  TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType Ndp_RouterSolicitationTxCountOfIpV6CtrlDyn;  /**< - */
  P2CONST(IpV6_AddrType, AUTOMATIC, IPV4_APPL_VAR) LastBcAddrPtrOfIpV6CtrlDyn;  /**< - */
  IpV6_Ndp_PendingDadNaType Ndp_PendingDadNaOfIpV6CtrlDyn;  /**< - */
  IpV6_TimeType Ndp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn;  /**< - */
  IpV6_ListIdxType NextRouterProbeIdxOfIpV6CtrlDyn;  /**< - */
} TcpIp_IpV6CtrlDynType;

/**   \brief  type used in TcpIp_IpV6EthBufData */
typedef struct sTcpIp_IpV6EthBufDataType
{
  TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType IpV6SocketDynIdxOfIpV6EthBufData;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6SocketDyn */
  TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType UlTxReqTabIdxOfIpV6EthBufData;  /**< Store which socket index belongs to the BufIdx of CtrlIdx. */
} TcpIp_IpV6EthBufDataType;

/**   \brief  type used in TcpIp_IpV6General */
typedef struct sTcpIp_IpV6GeneralType
{
  TcpIp_IpV6CtrlDefaultIdxOfIpV6GeneralType IpV6CtrlDefaultIdxOfIpV6General;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6Ctrl */
  TcpIp_IpV6SocketDynIcmpIdxOfIpV6GeneralType IpV6SocketDynIcmpIdxOfIpV6General;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6SocketDyn */
  TcpIp_IpV6SocketDynNdpIdxOfIpV6GeneralType IpV6SocketDynNdpIdxOfIpV6General;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6SocketDyn */
  TcpIp_IpV6SocketDynTcpRstIdxOfIpV6GeneralType IpV6SocketDynTcpRstIdxOfIpV6General;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6SocketDyn */
  TcpIp_MaskedBitsOfIpV6GeneralType MaskedBitsOfIpV6General;  /**< contains bitcoded the boolean data of TcpIp_ExtDestAddrValidationEnabledOfIpV6General, TcpIp_IpV6CtrlDefaultUsedOfIpV6General, TcpIp_IpV6SocketDynTcpRstUsedOfIpV6General */
} TcpIp_IpV6GeneralType;

/**   \brief  type used in TcpIp_IpV6MulticastAddr */
typedef struct sTcpIp_IpV6MulticastAddrType
{
  TcpIp_DefaultAddrV6IdxOfIpV6MulticastAddrType DefaultAddrV6IdxOfIpV6MulticastAddr;  /**< the index of the 0:1 relation pointing to TcpIp_DefaultAddrV6 */
  TcpIp_IpV6MulticastAddrActiveIdxOfIpV6MulticastAddrType IpV6MulticastAddrActiveIdxOfIpV6MulticastAddr;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6MulticastAddrActive */
  TcpIp_LocalAddrV6IdxOfIpV6MulticastAddrType LocalAddrV6IdxOfIpV6MulticastAddr;  /**< the index of the 1:1 relation pointing to TcpIp_LocalAddrV6 */
  TcpIp_MaskedBitsOfIpV6MulticastAddrType MaskedBitsOfIpV6MulticastAddr;  /**< contains bitcoded the boolean data of TcpIp_DefaultAddrV6UsedOfIpV6MulticastAddr, TcpIp_IpV6MulticastAddrActiveUsedOfIpV6MulticastAddr */
} TcpIp_IpV6MulticastAddrType;

/**   \brief  type used in TcpIp_IpV6SocketDyn */
typedef struct sTcpIp_IpV6SocketDynType
{
  TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType IpV6HdrVersionTcFlNboOfIpV6SocketDyn;  /**< - */
  TcpIp_EthIfFramePrioOfIpV6SocketDynType EthIfFramePrioOfIpV6SocketDyn;  /**< - */
  TcpIp_FlagsOfIpV6SocketDynType FlagsOfIpV6SocketDyn;  /**< - */
  TcpIp_HopLimitOfIpV6SocketDynType HopLimitOfIpV6SocketDyn;  /**< - */
  TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType IpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6DestinationCacheEntry */
} TcpIp_IpV6SocketDynType;

/**   \brief  type used in TcpIp_IpV6SourceAddress */
typedef struct sTcpIp_IpV6SourceAddressType
{
  TcpIp_AddressAssignVariantOfIpV6SourceAddressType AddressAssignVariantOfIpV6SourceAddress;  /**< Ip address configuration source. */
  TcpIp_DefaultAddrV6IdxOfIpV6SourceAddressType DefaultAddrV6IdxOfIpV6SourceAddress;  /**< the index of the 0:1 relation pointing to TcpIp_DefaultAddrV6 */
  TcpIp_LocalAddrV6IdxOfIpV6SourceAddressType LocalAddrV6IdxOfIpV6SourceAddress;  /**< the index of the 1:1 relation pointing to TcpIp_LocalAddrV6 */
  TcpIp_MaskedBitsOfIpV6SourceAddressType MaskedBitsOfIpV6SourceAddress;  /**< contains bitcoded the boolean data of TcpIp_DefaultAddrV6UsedOfIpV6SourceAddress, TcpIp_PrefixIsOnLinkOfIpV6SourceAddress */
} TcpIp_IpV6SourceAddressType;

/**   \brief  type used in TcpIp_LocalAddr */
typedef struct sTcpIp_LocalAddrType
{
  TcpIp_AssignmentStateOfLocalAddrType AssignmentStateOfLocalAddr;  /**< - */
} TcpIp_LocalAddrType;

/**   \brief  type used in TcpIp_LocalAddrV6 */
typedef struct sTcpIp_LocalAddrV6Type
{
  TcpIp_IpV6CtrlIdxOfLocalAddrV6Type IpV6CtrlIdxOfLocalAddrV6;  /**< the index of the 1:1 relation pointing to TcpIp_IpV6Ctrl */
  TcpIp_IpV6MulticastAddrIdxOfLocalAddrV6Type IpV6MulticastAddrIdxOfLocalAddrV6;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6MulticastAddr */
  TcpIp_IpV6SourceAddressIdxOfLocalAddrV6Type IpV6SourceAddressIdxOfLocalAddrV6;  /**< the index of the 0:1 relation pointing to TcpIp_IpV6SourceAddress */
  TcpIp_MaskedBitsOfLocalAddrV6Type MaskedBitsOfLocalAddrV6;  /**< contains bitcoded the boolean data of TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6, TcpIp_IpV6SourceAddressUsedOfLocalAddrV6 */
} TcpIp_LocalAddrV6Type;

/**   \brief  type used in TcpIp_NdpConfig */
typedef struct sTcpIp_NdpConfigType
{
  TcpIp_MaskedBitsOfNdpConfigType MaskedBitsOfNdpConfig;  /**< contains bitcoded the boolean data of TcpIp_DynamicReachableTimeOfNdpConfig, TcpIp_DynamicRetransTimerOfNdpConfig, TcpIp_EnableNdpInvNaNcUpdateOfNdpConfig, TcpIp_EnableNdpInvNaOfNdpConfig, TcpIp_EnableNdpInvNsOfNdpConfig, TcpIp_EnableNudOfNdpConfig, TcpIp_EnableOptimisticDadOfNdpConfig, TcpIp_EnableRfc6106DnsslOptOfNdpConfig, TcpIp_EnableRfc6106RdnssOptOfNdpConfig, TcpIp_EnableSlaacDelayOfNdpConfig, TcpIp_RandomReachableTimeOfNdpConfig, TcpIp_RndRtrSolicitationDelayOfNdpConfig */
  TcpIp_MaxRtrSolicitationDelayOfNdpConfigType MaxRtrSolicitationDelayOfNdpConfig;  /**< Max Rtr Solicitation Delay - Value of configuration parameter TcpIpNdpMaxRtrSolicitationDelay [MILLISECONDS] */
  TcpIp_MaxSlaacDelayOfNdpConfigType MaxSlaacDelayOfNdpConfig;  /**< Max address configuration delay - Value of configuration parameter TcpIpNdpSlaacMaxDelay [MILLISECONDS] */
  TcpIp_NudFirstProbeDelayOfNdpConfigType NudFirstProbeDelayOfNdpConfig;  /**< Delay First Probe Time - Value of configuration parameter TcpIpNdpDelayFirstProbeTime [MILLISECONDS] */
  TcpIp_ReachableTimeOfNdpConfigType ReachableTimeOfNdpConfig;  /**< Default Reachable Time - Value of configuration parameter TcpIpNdpDefaultReachableTime [MILLISECONDS] */
  TcpIp_RetransTimerOfNdpConfigType RetransTimerOfNdpConfig;  /**< Default Retrans Timer - Value of configuration parameter TcpIpNdpDefaultRetransTimer [MILLISECONDS] */
  TcpIp_RtrSolicitationIntervalOfNdpConfigType RtrSolicitationIntervalOfNdpConfig;  /**< Rtr Solicitation Interval - Value of configuration parameter TcpIpNdpRtrSolicitationInterval [MILLISECONDS] */
  TcpIp_SlaacMinLifetimeOfNdpConfigType SlaacMinLifetimeOfNdpConfig;  /**< Minimum Address Lifetime - Value of configuration parameter TcpIpNdpSlaacMinLifetime [SECONDS] */
  TcpIp_DadTransmitsOfNdpConfigType DadTransmitsOfNdpConfig;  /**< DAD Number Of Transmissions - Value of configuration parameter TcpIpNdpSlaacDadNumberOfTransmissions */
  TcpIp_MaxRandomFactorOfNdpConfigType MaxRandomFactorOfNdpConfig;  /**< Max Random Factor - Value of configuration parameter TcpIpNdpMaxRandomFactor */
  TcpIp_MaxRtrSolicitationsOfNdpConfigType MaxRtrSolicitationsOfNdpConfig;  /**< Max Rtr Solicitations - Value of configuration parameter TcpIpNdpMaxRtrSolicitations */
  TcpIp_MinRandomFactorOfNdpConfigType MinRandomFactorOfNdpConfig;  /**< Min Random Factor - Value of configuration parameter TcpIpNdpMinRandomFactor */
  TcpIp_MulticastSolicitsOfNdpConfigType MulticastSolicitsOfNdpConfig;  /**< Num Multicast Solicitations - Value of configuration parameter TcpIpNdpNumMulticastSolicitations */
  TcpIp_UnicastSolicitsOfNdpConfigType UnicastSolicitsOfNdpConfig;  /**< Num Unicast Solicitations - Value of configuration parameter TcpIpNdpNumUnicastSolicitations */
} TcpIp_NdpConfigType;

/**   \brief  type used in TcpIp_PhysAddrConfig */
typedef struct sTcpIp_PhysAddrConfigType
{
  TcpIp_PhysAddrTableChgCbkType ChgFuncPtrOfPhysAddrConfig;  /**< - */
  TcpIp_PhysAddrTableChgDiscardedCbkType ChgDiscardedFuncPtrOfPhysAddrConfig;  /**< - */
} TcpIp_PhysAddrConfigType;

/**   \brief  type used in TcpIp_SocketDyn */
typedef struct sTcpIp_SocketDynType
{
  TcpIp_ListenActiveConnStatOfSocketDynType ListenActiveConnStatOfSocketDyn;  /**< Current state of the socket */
  TcpIp_LocalAddrBindIdxOfSocketDynType LocalAddrBindIdxOfSocketDyn;  /**< the index of the 0:1 relation pointing to TcpIp_LocalAddr */
  TcpIp_SocketOwnerConfigIdxOfSocketDynType SocketOwnerConfigIdxOfSocketDyn;  /**< the index of the 0:1 relation pointing to TcpIp_SocketOwnerConfig */
  TcpIp_TxBufRequestedOfSocketDynType TxBufRequestedOfSocketDyn;  /**< TX buffer request semaphore */
  TcpIp_TxIpAddrIdxOfSocketDynType TxIpAddrIdxOfSocketDyn;  /**< Index of local IPxV address that is used for transmission */
  TcpIp_SockAddrBaseType LocSockOfSocketDyn;  /**< IP address and port of the local host */
  TcpIp_SockAddrBaseType RemSockOfSocketDyn;  /**< IP address and port of the remote host */
} TcpIp_SocketDynType;

/**   \brief  type used in TcpIp_SocketOwnerConfig */
typedef struct sTcpIp_SocketOwnerConfigType
{
  TcpIp_SocketOwnerCopyTxDataDynType CopyTxDataDynFuncPtrOfSocketOwnerConfig;  /**< [User]_CopyTxDataDyn Callback Function. (only required if socket owner uses indirect data provistion in Tcp/UdpTransmit.) */
  TcpIp_SocketOwnerCopyTxDataType CopyTxDataFuncPtrOfSocketOwnerConfig;  /**< [User]_CopyTxData Callback Function. (only required if socket owner uses indirect data provistion in Tcp/UdpTransmit.) */
  TcpIp_SocketOwnerDhcpEventType DhcpEventFuncPtrOfSocketOwnerConfig;  /**< Callout triggered on reception and transmission of client DHCP messages. */
  TcpIp_SocketOwnerLocalIpAddrAssignmentChgType LocalIpAddrAssignmentChgFuncPtrOfSocketOwnerConfig;  /**< - */
  TcpIp_SocketOwnerRxIndicationType RxIndicationFuncPtrOfSocketOwnerConfig;  /**< [User]_RxIndication Callback Function. (required for socket owner that recieves packets.) */
  TcpIp_SocketOwnerTcpAcceptedType TcpAcceptedFuncPtrOfSocketOwnerConfig;  /**< [User]_TcpAccepted Callback Function. (only required if socker passively accepts TCP connections.) */
  TcpIp_SocketOwnerTcpConnectedType TcpConnectedFuncPtrOfSocketOwnerConfig;  /**< [User]_TcpConnected Callback Function. (only required if socker owner actively opens TCP connections.) */
  TcpIp_SocketOwnerTcpIpEventType TcpIpEventFuncPtrOfSocketOwnerConfig;  /**< [User]_TcpIpEvent Callback Function. (required for every socket owner) */
  TcpIp_SocketOwnerTlsValidationResultType TlsValidationResultFuncPtrOfSocketOwnerConfig;  /**< [User]_TlsValidationResult Callback Function. */
  TcpIp_SocketOwnerTxConfirmationType TxConfirmationFuncPtrOfSocketOwnerConfig;  /**< [User]_TxConfirmation Callback Function. (optional for every socket owner) */
} TcpIp_SocketOwnerConfigType;

/**   \brief  type used in TcpIp_SocketTcpDyn */
typedef struct sTcpIp_SocketTcpDynType
{
  TcpIp_FinWait2TimeoutOfSocketTcpDynType FinWait2TimeoutOfSocketTcpDyn;  /**< - */
  TcpIp_IdleTimeoutShortOfSocketTcpDynType IdleTimeoutShortOfSocketTcpDyn;  /**< - */
  TcpIp_IssOfSocketTcpDynType IssOfSocketTcpDyn;  /**< - */
  TcpIp_MslTimeoutOfSocketTcpDynType MslTimeoutOfSocketTcpDyn;  /**< - */
  TcpIp_RcvNxtOfSocketTcpDynType RcvNxtOfSocketTcpDyn;  /**< - */
  TcpIp_RetransmitTimeoutOfSocketTcpDynType RetransmitTimeoutOfSocketTcpDyn;  /**< - */
  TcpIp_RtoReloadValueOfSocketTcpDynType RtoReloadValueOfSocketTcpDyn;  /**< - */
  TcpIp_SndNxtOfSocketTcpDynType SndNxtOfSocketTcpDyn;  /**< - */
  TcpIp_SndUnaOfSocketTcpDynType SndUnaOfSocketTcpDyn;  /**< - */
  TcpIp_SndWl1OfSocketTcpDynType SndWl1OfSocketTcpDyn;  /**< - */
  TcpIp_SndWl2OfSocketTcpDynType SndWl2OfSocketTcpDyn;  /**< - */
  TcpIp_TxNextSendSeqNoOfSocketTcpDynType TxNextSendSeqNoOfSocketTcpDyn;  /**< - */
  TcpIp_TxReqSeqNoOfSocketTcpDynType TxReqSeqNoOfSocketTcpDyn;  /**< Sequence number of first pending TX request. */
  TcpIp_MaxNumListenSocketsOfSocketTcpDynType MaxNumListenSocketsOfSocketTcpDyn;  /**< - */
  TcpIp_PathMtuNewSizeOfSocketTcpDynType PathMtuNewSizeOfSocketTcpDyn;  /**< - */
  TcpIp_RcvWndOfSocketTcpDynType RcvWndOfSocketTcpDyn;  /**< - */
  TcpIp_SndWndOfSocketTcpDynType SndWndOfSocketTcpDyn;  /**< - */
  TcpIp_TxMaxSegLenByteOfSocketTcpDynType TxMaxSegLenByteOfSocketTcpDyn;  /**< - */
  TcpIp_TxMssAgreedOfSocketTcpDynType TxMssAgreedOfSocketTcpDyn;  /**< Maximum segment size agreed during connection setup */
  TcpIp_EventIndicationPendingOfSocketTcpDynType EventIndicationPendingOfSocketTcpDyn;  /**< Varibale contains the pending events that have to be forwarded to the socket user */
  TcpIp_PathMtuChangedOfSocketTcpDynType PathMtuChangedOfSocketTcpDyn;  /**< - */
  TcpIp_RetryQFillNumOfSocketTcpDynType RetryQFillNumOfSocketTcpDyn;  /**< - */
  TcpIp_RstReceivedOfSocketTcpDynType RstReceivedOfSocketTcpDyn;  /**< - */
  TcpIp_SockIsServerOfSocketTcpDynType SockIsServerOfSocketTcpDyn;  /**< - */
  TcpIp_SockStateOfSocketTcpDynType SockStateOfSocketTcpDyn;  /**< - */
  TcpIp_SockStateNextOfSocketTcpDynType SockStateNextOfSocketTcpDyn;  /**< - */
  TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType SocketTcpDynMasterListenSocketIdxOfSocketTcpDyn;  /**< the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn */
  TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType TcpRetryQElementFirstIdxOfSocketTcpDyn;  /**< the index of the 0:1 relation pointing to TcpIp_TcpRetryQElement */
  TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType TcpRetryQElementLastIdxOfSocketTcpDyn;  /**< the index of the 0:1 relation pointing to TcpIp_TcpRetryQElement */
  TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType TcpRxBufferDescIdxOfSocketTcpDyn;  /**< the index of the 0:1 relation pointing to TcpIp_TcpRxBufferDesc */
  TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType TcpTxBufferDescIdxOfSocketTcpDyn;  /**< the index of the 0:1 relation pointing to TcpIp_TcpTxBufferDesc */
  TcpIp_TxFlagsOfSocketTcpDynType TxFlagsOfSocketTcpDyn;  /**< - */
  TcpIp_TxOneTimeOptsOfSocketTcpDynType TxOneTimeOptsOfSocketTcpDyn;  /**< - */
  TcpIp_TxOneTimeOptsLenOfSocketTcpDynType TxOneTimeOptsLenOfSocketTcpDyn;  /**< - */
  TcpIp_TxOptLenOfSocketTcpDynType TxOptLenOfSocketTcpDyn;  /**< - */
  TcpIp_TxReqFullyQueuedOfSocketTcpDynType TxReqFullyQueuedOfSocketTcpDyn;  /**< all unqueued data from this request is used within the next segment */
  TcpIp_Tcp_BackLogEleType BackLogArrayOfSocketTcpDyn;  /**< - */
  TcpIp_SizeOfTcpRxBufferType RequestedRxBufferSizeOfSocketTcpDyn;  /**< - */
  TcpIp_SizeOfTcpTxBufferType RequestedTxBufferSizeOfSocketTcpDyn;  /**< - */
  TcpIp_SizeOfTcpRxBufferType RxBufferIndPosOfSocketTcpDyn;  /**< - */
  TcpIp_SizeOfTcpRxBufferType RxBufferRemIndLenOfSocketTcpDyn;  /**< - */
  TcpIp_SizeOfTcpTxBufferType TxLenByteTxOfSocketTcpDyn;  /**< - */
  TcpIp_SizeOfTcpTxBufferType TxReqDataBufStartIdxOfSocketTcpDyn;  /**< byte position inside the TxBuffer */
  TcpIp_SizeOfTcpTxBufferType TxReqDataLenByteOfSocketTcpDyn;  /**< length of data to be transmitted */
  TcpIp_SizeOfTcpTxBufferType TxReqQueuedLenOfSocketTcpDyn;  /**< length of data that could already be queued for transmission */
  TcpIp_SizeOfTcpTxBufferType TxTotNotQueuedLenOfSocketTcpDyn;  /**< - */
} TcpIp_SocketTcpDynType;

/**   \brief  type used in TcpIp_SocketUdpDyn */
typedef struct sTcpIp_SocketUdpDynType
{
  TcpIp_TxReqElemListIdxOfSocketUdpDynType TxReqElemListIdxOfSocketUdpDyn;  /**< the index of the 0:1 relation pointing to TcpIp_TxReqElemList */
  TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType TxRetrQueueMaxNumOfSocketUdpDyn;  /**< Maximum number of TX retry queue elements for the socket. */
  TcpIp_IpTxRequestDescriptorType IpTxRequestDescriptorOfSocketUdpDyn;  /**< TX request descriptor required for the IP layer */
  P2VAR(uint8, AUTOMATIC, IPV4_APPL_VAR) TxReqIpBufPtrOfSocketUdpDyn;  /**< Pointer to the provided TX buffer of the IP layer */
  TcpIp_DListType TxRetrQueueOfSocketUdpDyn;  /**< TX retry queue */
} TcpIp_SocketUdpDynType;

/**   \brief  type used in TcpIp_TcpConfig */
typedef struct sTcpIp_TcpConfigType
{
  TcpIp_KeepAliveTimeOfTcpConfigType KeepAliveTimeOfTcpConfig;  /**< Keep Alive Time - Value of configuration parameter TcpIpTcpKeepAliveTime [MAIN_FUNCTION_CYCLES] */
  TcpIp_FinWait2TimeoutOfTcpConfigType FinWait2TimeoutOfTcpConfig;  /**< FIN Wait 2 Timeout - Value of configuration parameter TcpIpTcpFinWait2Timeout [MAIN_FUNCTION_CYCLES] */
  TcpIp_MslOfTcpConfigType MslOfTcpConfig;  /**< Maximum Segment Lifetime (MSL) - Value of configuration parameter TcpIpTcpMsl [MAIN_FUNCTION_CYCLES] */
  TcpIp_RetransTimeoutMaxOfTcpConfigType RetransTimeoutMaxOfTcpConfig;  /**< Retransmission Timeout Max - Value of configuration parameter TcpIpTcpRetransmissionTimeoutMax [MAIN_FUNCTION_CYCLES] */
  TcpIp_RxMssOfTcpConfigType RxMssOfTcpConfig;  /**< maximum payload size of received TCP segments. */
  TcpIp_TxMssOfTcpConfigType TxMssOfTcpConfig;  /**< maximum payload size of transmitted TCP segments. */
  TcpIp_UserTimeoutDefCyclesOfTcpConfigType UserTimeoutDefCyclesOfTcpConfig;  /**< User Timeout Def - Value of configuration parameter TcpIpTcpUserTimeoutDef [MAIN_FUNCTION_CYCLES] */
  TcpIp_KeepAliveIntervalOfTcpConfigType KeepAliveIntervalOfTcpConfig;  /**< Keep Alive Interval - Value of configuration parameter TcpIpTcpKeepAliveInterval [MAIN_FUNCTION_CYCLES] */
  TcpIp_KeepAliveProbesMaxOfTcpConfigType KeepAliveProbesMaxOfTcpConfig;  /**< Keep Alive Probes Max - Value of configuration parameter TcpIpTcpKeepAliveProbesMax */
  TcpIp_NagleTimeoutOfTcpConfigType NagleTimeoutOfTcpConfig;  /**< Nagle Timeout - Value of configuration parameter TcpIpTcpNagleTimeout [MAIN_FUNCTION_CYCLES] */
  TcpIp_RetransTimeoutOfTcpConfigType RetransTimeoutOfTcpConfig;  /**< Retransmission Timeout - Value of configuration parameter TcpIpTcpRetransmissionTimeout [MAIN_FUNCTION_CYCLES] */
  TcpIp_TcpOooQSizePerSocketAvgOfTcpConfigType TcpOooQSizePerSocketAvgOfTcpConfig;  /**< Avg Num Out Of Order Segments per Socket - Value of configuration parameter TcpIpTcpAvgNumOooSegsPerSocket */
  TcpIp_TcpOooQSizePerSocketMaxOfTcpConfigType TcpOooQSizePerSocketMaxOfTcpConfig;  /**< Max Num Out Of Order Segments per Socket - Value of configuration parameter TcpIpTcpMaxNumOooSegsPerSocket */
  TcpIp_TcpRetryQSizeOfTcpConfigType TcpRetryQSizeOfTcpConfig;  /**< Average Tx Retry Queue Size - Value of configuration parameter TcpIpTcpTxRetryQueueSize */
  TcpIp_TimeToLiveDefaultOfTcpConfigType TimeToLiveDefaultOfTcpConfig;  /**< Time-To-Live (TTL) - Value of configuration parameter TcpIpTcpTtl */
  TcpIp_UserTimeoutMaxCyclesOfTcpConfigType UserTimeoutMaxCyclesOfTcpConfig;  /**< User Timeout Max - Value of configuration parameter TcpIpTcpUserTimeoutMax [MAIN_FUNCTION_CYCLES] */
  TcpIp_UserTimeoutMinCyclesOfTcpConfigType UserTimeoutMinCyclesOfTcpConfig;  /**< User Timeout Min - Value of configuration parameter TcpIpTcpUserTimeoutMin [MAIN_FUNCTION_CYCLES] */
} TcpIp_TcpConfigType;

/**   \brief  type used in TcpIp_TcpRxBufferDesc */
typedef struct sTcpIp_TcpRxBufferDescType
{
  TcpIp_TcpRxBufferEndIdxOfTcpRxBufferDescType TcpRxBufferEndIdxOfTcpRxBufferDesc;  /**< the end index of the 1:n relation pointing to TcpIp_TcpRxBuffer */
  TcpIp_TcpRxBufferLengthOfTcpRxBufferDescType TcpRxBufferLengthOfTcpRxBufferDesc;  /**< the number of relations pointing to TcpIp_TcpRxBuffer */
  TcpIp_TcpRxBufferStartIdxOfTcpRxBufferDescType TcpRxBufferStartIdxOfTcpRxBufferDesc;  /**< the start index of the 1:n relation pointing to TcpIp_TcpRxBuffer */
} TcpIp_TcpRxBufferDescType;

/**   \brief  type used in TcpIp_TcpRxBufferDescDyn */
typedef struct sTcpIp_TcpRxBufferDescDynType
{
  TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType TcpRxBufferWriteIdxOfTcpRxBufferDescDyn;  /**< the index of the 1:1 relation pointing to TcpIp_TcpRxBuffer */
  TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType SocketTcpDynIdxOfTcpRxBufferDescDyn;  /**< the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn */
  TcpIp_SizeOfTcpRxBufferType FillLevelOfTcpRxBufferDescDyn;
} TcpIp_TcpRxBufferDescDynType;

/**   \brief  type used in TcpIp_TcpTxBufferDesc */
typedef struct sTcpIp_TcpTxBufferDescType
{
  TcpIp_TcpTxBufferEndIdxOfTcpTxBufferDescType TcpTxBufferEndIdxOfTcpTxBufferDesc;  /**< the end index of the 1:n relation pointing to TcpIp_TcpTxBuffer */
  TcpIp_TcpTxBufferLengthOfTcpTxBufferDescType TcpTxBufferLengthOfTcpTxBufferDesc;  /**< the number of relations pointing to TcpIp_TcpTxBuffer */
  TcpIp_TcpTxBufferStartIdxOfTcpTxBufferDescType TcpTxBufferStartIdxOfTcpTxBufferDesc;  /**< the start index of the 1:n relation pointing to TcpIp_TcpTxBuffer */
} TcpIp_TcpTxBufferDescType;

/**   \brief  type used in TcpIp_TcpTxBufferDescDyn */
typedef struct sTcpIp_TcpTxBufferDescDynType
{
  TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType TcpTxBufferWriteIdxOfTcpTxBufferDescDyn;  /**< the index of the 1:1 relation pointing to TcpIp_TcpTxBuffer */
  TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType SocketTcpDynIdxOfTcpTxBufferDescDyn;  /**< the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn */
  TcpIp_SizeOfTcpTxBufferType FillLevelOfTcpTxBufferDescDyn;
} TcpIp_TcpTxBufferDescDynType;

/**   \brief  type used in TcpIp_TxReqElem */
typedef struct sTcpIp_TxReqElemType
{
  TcpIp_TxReqElemDataLenByteOfTxReqElemType TxReqElemDataLenByteOfTxReqElem;  /**< - */
  TcpIp_TxReqElemTransmittedOfTxReqElemType TxReqElemTransmittedOfTxReqElem;  /**< - */
} TcpIp_TxReqElemType;

/**   \brief  type used in TcpIp_TxReqElemList */
typedef struct sTcpIp_TxReqElemListType
{
  TcpIp_TxReqElemEndIdxOfTxReqElemListType TxReqElemEndIdxOfTxReqElemList;  /**< the end index of the 1:n relation pointing to TcpIp_TxReqElem */
  TcpIp_TxReqElemLengthOfTxReqElemListType TxReqElemLengthOfTxReqElemList;  /**< the number of relations pointing to TcpIp_TxReqElem */
  TcpIp_TxReqElemStartIdxOfTxReqElemListType TxReqElemStartIdxOfTxReqElemList;  /**< the start index of the 1:n relation pointing to TcpIp_TxReqElem */
} TcpIp_TxReqElemListType;

/**   \brief  type used in TcpIp_TxReqElemListDyn */
typedef struct sTcpIp_TxReqElemListDynType
{
  TcpIp_FillNumOfTxReqElemListDynType FillNumOfTxReqElemListDyn;  /**< Number of elements currently stored in TX request ring buffer */
  TcpIp_ReadPosOfTxReqElemListDynType ReadPosOfTxReqElemListDyn;  /**< Read position of TX request ring buffer */
  TcpIp_SocketUdpDynIdxOfTxReqElemListDynType SocketUdpDynIdxOfTxReqElemListDyn;  /**< the index of the 0:1 relation pointing to TcpIp_SocketUdpDyn */
  TcpIp_WritePosOfTxReqElemListDynType WritePosOfTxReqElemListDyn;  /**< Write position in TX request ring buffer */
} TcpIp_TxReqElemListDynType;

/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCSymbolicStructTypes  TcpIp Symbolic Struct Types (PRE_COMPILE)
  \brief  These structs are used in unions to have a symbol based data representation style.
  \{
*/ 
/**   \brief  type to be used as symbolic data element access to TcpIp_InterfaceIdentifier */
typedef struct TcpIp_InterfaceIdentifierStructSTag
{
  TcpIp_InterfaceIdentifierType TcpIpIpV6Ctrl[64];
} TcpIp_InterfaceIdentifierStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_IpV6CtrlDyn */
typedef struct TcpIp_IpV6CtrlDynStructSTag
{
  TcpIp_IpV6CtrlDynType TcpIpCtrl;
} TcpIp_IpV6CtrlDynStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_IpV6DefaultRouterListEntry */
typedef struct TcpIp_IpV6DefaultRouterListEntryStructSTag
{
  IpV6_DefaultRouterListEntryType TcpIpIpV6Ctrl[2];
} TcpIp_IpV6DefaultRouterListEntryStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_IpV6DestinationCacheEntry */
typedef struct TcpIp_IpV6DestinationCacheEntryStructSTag
{
  IpV6_DestinationCacheEntryType TcpIpIpV6Ctrl[5];
} TcpIp_IpV6DestinationCacheEntryStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_IpV6EthBufData */
typedef struct TcpIp_IpV6EthBufDataStructSTag
{
  TcpIp_IpV6EthBufDataType EthCtrlConfig[2];
} TcpIp_IpV6EthBufDataStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_IpV6NeighborCacheEntry */
typedef struct TcpIp_IpV6NeighborCacheEntryStructSTag
{
  IpV6_NeighborCacheEntryType TcpIpIpV6Ctrl[5];
} TcpIp_IpV6NeighborCacheEntryStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_IpV6PrefixListEntry */
typedef struct TcpIp_IpV6PrefixListEntryStructSTag
{
  IpV6_PrefixListEntryType TcpIpIpV6Ctrl[5];
} TcpIp_IpV6PrefixListEntryStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_LocalAddr */
typedef struct TcpIp_LocalAddrStructSTag
{
  TcpIp_LocalAddrType TcpIpV6LocalAddr_TcpIpCtrl_LinkLocal;
  TcpIp_LocalAddrType TcpIpV6LocalAddr_TcpIpCtrl_AsAn;
} TcpIp_LocalAddrStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_SocketDyn */
typedef struct TcpIp_SocketDynStructSTag
{
  TcpIp_SocketDynType UDP;
  TcpIp_SocketDynType TCP;
} TcpIp_SocketDynStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_TcpRxBuffer */
typedef struct TcpIp_TcpRxBufferStructSTag
{
  TcpIp_TcpRxBufferType TcpIpTcpSocketBuffer[4000];
} TcpIp_TcpRxBufferStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_TcpTxBuffer */
typedef struct TcpIp_TcpTxBufferStructSTag
{
  TcpIp_TcpTxBufferType TcpIpTcpSocketBuffer[500];
} TcpIp_TcpTxBufferStructSType;

/**   \brief  type to be used as symbolic data element access to TcpIp_TxReqElem */
typedef struct TcpIp_TxReqElemStructSTag
{
  TcpIp_TxReqElemType TcpIpUdpTxReqList_Scc;
} TcpIp_TxReqElemStructSType;

/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCUnionIndexAndSymbolTypes  TcpIp Union Index And Symbol Types (PRE_COMPILE)
  \brief  These unions are used to access arrays in an index and symbol based style.
  \{
*/ 
/**   \brief  type to access TcpIp_InterfaceIdentifier in an index and symbol based style. */
typedef union TcpIp_InterfaceIdentifierUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_InterfaceIdentifierType raw[64];
  TcpIp_InterfaceIdentifierStructSType str;
} TcpIp_InterfaceIdentifierUType;

/**   \brief  type to access TcpIp_IpV6CtrlDyn in an index and symbol based style. */
typedef union TcpIp_IpV6CtrlDynUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_IpV6CtrlDynType raw[1];
  TcpIp_IpV6CtrlDynStructSType str;
} TcpIp_IpV6CtrlDynUType;

/**   \brief  type to access TcpIp_IpV6DefaultRouterListEntry in an index and symbol based style. */
typedef union TcpIp_IpV6DefaultRouterListEntryUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  IpV6_DefaultRouterListEntryType raw[2];
  TcpIp_IpV6DefaultRouterListEntryStructSType str;
} TcpIp_IpV6DefaultRouterListEntryUType;

/**   \brief  type to access TcpIp_IpV6DestinationCacheEntry in an index and symbol based style. */
typedef union TcpIp_IpV6DestinationCacheEntryUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  IpV6_DestinationCacheEntryType raw[5];
  TcpIp_IpV6DestinationCacheEntryStructSType str;
} TcpIp_IpV6DestinationCacheEntryUType;

/**   \brief  type to access TcpIp_IpV6EthBufData in an index and symbol based style. */
typedef union TcpIp_IpV6EthBufDataUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_IpV6EthBufDataType raw[2];
  TcpIp_IpV6EthBufDataStructSType str;
} TcpIp_IpV6EthBufDataUType;

/**   \brief  type to access TcpIp_IpV6NeighborCacheEntry in an index and symbol based style. */
typedef union TcpIp_IpV6NeighborCacheEntryUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  IpV6_NeighborCacheEntryType raw[5];
  TcpIp_IpV6NeighborCacheEntryStructSType str;
} TcpIp_IpV6NeighborCacheEntryUType;

/**   \brief  type to access TcpIp_IpV6PrefixListEntry in an index and symbol based style. */
typedef union TcpIp_IpV6PrefixListEntryUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  IpV6_PrefixListEntryType raw[5];
  TcpIp_IpV6PrefixListEntryStructSType str;
} TcpIp_IpV6PrefixListEntryUType;

/**   \brief  type to access TcpIp_LocalAddr in an index and symbol based style. */
typedef union TcpIp_LocalAddrUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_LocalAddrType raw[2];
  TcpIp_LocalAddrStructSType str;
} TcpIp_LocalAddrUType;

/**   \brief  type to access TcpIp_SocketDyn in an index and symbol based style. */
typedef union TcpIp_SocketDynUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_SocketDynType raw[2];
  TcpIp_SocketDynStructSType str;
} TcpIp_SocketDynUType;

/**   \brief  type to access TcpIp_TcpRxBuffer in an index and symbol based style. */
typedef union TcpIp_TcpRxBufferUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_TcpRxBufferType raw[4000];
  TcpIp_TcpRxBufferStructSType str;
} TcpIp_TcpRxBufferUType;

/**   \brief  type to access TcpIp_TcpTxBuffer in an index and symbol based style. */
typedef union TcpIp_TcpTxBufferUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_TcpTxBufferType raw[500];
  TcpIp_TcpTxBufferStructSType str;
} TcpIp_TcpTxBufferUType;

/**   \brief  type to access TcpIp_TxReqElem in an index and symbol based style. */
typedef union TcpIp_TxReqElemUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  TcpIp_TxReqElemType raw[1];
  TcpIp_TxReqElemStructSType str;
} TcpIp_TxReqElemUType;

/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCRootPointerTypes  TcpIp Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to TcpIp_DefaultAddrV6 */
typedef P2CONST(IpV6_AddrType, TYPEDEF, TCPIP_CONST) TcpIp_DefaultAddrV6PtrType;

/**   \brief  type used to point to TcpIp_DhcpUserOption */
typedef P2CONST(TcpIp_DhcpUserOptionType, TYPEDEF, TCPIP_CONST) TcpIp_DhcpUserOptionPtrType;

/**   \brief  type used to point to TcpIp_DhcpUserOptionBuffer */
typedef P2VAR(TcpIp_DhcpUserOptionBufferType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_DhcpUserOptionBufferPtrType;

/**   \brief  type used to point to TcpIp_DhcpUserOptionDyn */
typedef P2VAR(TcpIp_DhcpUserOptionDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_DhcpUserOptionDynPtrType;

/**   \brief  type used to point to TcpIp_EthIfCtrl */
typedef P2CONST(TcpIp_EthIfCtrlType, TYPEDEF, TCPIP_CONST) TcpIp_EthIfCtrlPtrType;

/**   \brief  type used to point to TcpIp_IcmpV6Config */
typedef P2CONST(TcpIp_IcmpV6ConfigType, TYPEDEF, TCPIP_CONST) TcpIp_IcmpV6ConfigPtrType;

/**   \brief  type used to point to TcpIp_IcmpV6TxMsgBuffer */
typedef P2VAR(TcpIp_IcmpV6TxMsgBufferType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IcmpV6TxMsgBufferPtrType;

/**   \brief  type used to point to TcpIp_InterfaceIdentifier */
typedef P2VAR(TcpIp_InterfaceIdentifierType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_InterfaceIdentifierPtrType;

/**   \brief  type used to point to TcpIp_IpV6Ctrl */
typedef P2CONST(TcpIp_IpV6CtrlType, TYPEDEF, TCPIP_CONST) TcpIp_IpV6CtrlPtrType;

/**   \brief  type used to point to TcpIp_IpV6CtrlDyn */
typedef P2VAR(TcpIp_IpV6CtrlDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6CtrlDynPtrType;

/**   \brief  type used to point to TcpIp_Ndp_PendingDadNaOfIpV6CtrlDyn */
typedef P2VAR(IpV6_Ndp_PendingDadNaType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_Ndp_PendingDadNaOfIpV6CtrlDynPtrType;

/**   \brief  type used to point to TcpIp_IpV6DefaultRouterListEntry */
typedef P2VAR(IpV6_DefaultRouterListEntryType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6DefaultRouterListEntryPtrType;

/**   \brief  type used to point to TcpIp_IpV6DestinationCacheEntry */
typedef P2VAR(IpV6_DestinationCacheEntryType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6DestinationCacheEntryPtrType;

/**   \brief  type used to point to TcpIp_IpV6EthBufData */
typedef P2VAR(TcpIp_IpV6EthBufDataType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6EthBufDataPtrType;

/**   \brief  type used to point to TcpIp_IpV6General */
typedef P2CONST(TcpIp_IpV6GeneralType, TYPEDEF, TCPIP_CONST) TcpIp_IpV6GeneralPtrType;

/**   \brief  type used to point to TcpIp_IpV6MulticastAddr */
typedef P2CONST(TcpIp_IpV6MulticastAddrType, TYPEDEF, TCPIP_CONST) TcpIp_IpV6MulticastAddrPtrType;

/**   \brief  type used to point to TcpIp_IpV6MulticastAddrActive */
typedef P2VAR(IpV6_AddrType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6MulticastAddrActivePtrType;

/**   \brief  type used to point to TcpIp_IpV6NeighborCacheEntry */
typedef P2VAR(IpV6_NeighborCacheEntryType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6NeighborCacheEntryPtrType;

/**   \brief  type used to point to TcpIp_IpV6PrefixListEntry */
typedef P2VAR(IpV6_PrefixListEntryType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6PrefixListEntryPtrType;

/**   \brief  type used to point to TcpIp_IpV6SocketDyn */
typedef P2VAR(TcpIp_IpV6SocketDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6SocketDynPtrType;

/**   \brief  type used to point to TcpIp_IpV6SourceAddress */
typedef P2CONST(TcpIp_IpV6SourceAddressType, TYPEDEF, TCPIP_CONST) TcpIp_IpV6SourceAddressPtrType;

/**   \brief  type used to point to TcpIp_IpV6SourceAddressTableEntry */
typedef P2VAR(IpV6_SourceAddressTableEntryType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpV6SourceAddressTableEntryPtrType;

/**   \brief  type used to point to TcpIp_LocalAddr */
typedef P2VAR(TcpIp_LocalAddrType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_LocalAddrPtrType;

/**   \brief  type used to point to TcpIp_LocalAddrV6 */
typedef P2CONST(TcpIp_LocalAddrV6Type, TYPEDEF, TCPIP_CONST) TcpIp_LocalAddrV6PtrType;

/**   \brief  type used to point to TcpIp_NdpConfig */
typedef P2CONST(TcpIp_NdpConfigType, TYPEDEF, TCPIP_CONST) TcpIp_NdpConfigPtrType;

/**   \brief  type used to point to TcpIp_PhysAddrConfig */
typedef P2CONST(TcpIp_PhysAddrConfigType, TYPEDEF, TCPIP_CONST) TcpIp_PhysAddrConfigPtrType;

/**   \brief  type used to point to TcpIp_SocketDyn */
typedef P2VAR(TcpIp_SocketDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_SocketDynPtrType;

/**   \brief  type used to point to TcpIp_LocSockOfSocketDyn */
typedef P2VAR(TcpIp_SockAddrBaseType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_LocSockOfSocketDynPtrType;

/**   \brief  type used to point to TcpIp_RemSockOfSocketDyn */
typedef P2VAR(TcpIp_SockAddrBaseType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_RemSockOfSocketDynPtrType;

/**   \brief  type used to point to TcpIp_SocketOwnerConfig */
typedef P2CONST(TcpIp_SocketOwnerConfigType, TYPEDEF, TCPIP_CONST) TcpIp_SocketOwnerConfigPtrType;

/**   \brief  type used to point to TcpIp_SocketTcpDyn */
typedef P2VAR(TcpIp_SocketTcpDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_SocketTcpDynPtrType;

/**   \brief  type used to point to TcpIp_SocketUdpDyn */
typedef P2VAR(TcpIp_SocketUdpDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_SocketUdpDynPtrType;

/**   \brief  type used to point to TcpIp_IpTxRequestDescriptorOfSocketUdpDyn */
typedef P2VAR(TcpIp_IpTxRequestDescriptorType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_IpTxRequestDescriptorOfSocketUdpDynPtrType;

/**   \brief  type used to point to TcpIp_TxRetrQueueOfSocketUdpDyn */
typedef P2VAR(TcpIp_DListType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TxRetrQueueOfSocketUdpDynPtrType;

/**   \brief  type used to point to TcpIp_TcpConfig */
typedef P2CONST(TcpIp_TcpConfigType, TYPEDEF, TCPIP_CONST) TcpIp_TcpConfigPtrType;

/**   \brief  type used to point to TcpIp_TcpOooQElement */
typedef P2VAR(TcpIp_Tcp_RxPreBufEleType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpOooQElementPtrType;

/**   \brief  type used to point to TcpIp_TcpResetQElement */
typedef P2VAR(TcpIp_Tcp_RstTxQueueType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpResetQElementPtrType;

/**   \brief  type used to point to TcpIp_TcpRetryQElement */
typedef P2VAR(TcpIp_Tcp_TxRetrQueueType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpRetryQElementPtrType;

/**   \brief  type used to point to TcpIp_TcpRxBuffer */
typedef P2VAR(TcpIp_TcpRxBufferType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpRxBufferPtrType;

/**   \brief  type used to point to TcpIp_TcpRxBufferDesc */
typedef P2CONST(TcpIp_TcpRxBufferDescType, TYPEDEF, TCPIP_CONST) TcpIp_TcpRxBufferDescPtrType;

/**   \brief  type used to point to TcpIp_TcpRxBufferDescDyn */
typedef P2VAR(TcpIp_TcpRxBufferDescDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpRxBufferDescDynPtrType;

/**   \brief  type used to point to TcpIp_TcpTxBuffer */
typedef P2VAR(TcpIp_TcpTxBufferType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpTxBufferPtrType;

/**   \brief  type used to point to TcpIp_TcpTxBufferDesc */
typedef P2CONST(TcpIp_TcpTxBufferDescType, TYPEDEF, TCPIP_CONST) TcpIp_TcpTxBufferDescPtrType;

/**   \brief  type used to point to TcpIp_TcpTxBufferDescDyn */
typedef P2VAR(TcpIp_TcpTxBufferDescDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TcpTxBufferDescDynPtrType;

/**   \brief  type used to point to TcpIp_TxReqElem */
typedef P2VAR(TcpIp_TxReqElemType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TxReqElemPtrType;

/**   \brief  type used to point to TcpIp_TxReqElemList */
typedef P2CONST(TcpIp_TxReqElemListType, TYPEDEF, TCPIP_CONST) TcpIp_TxReqElemListPtrType;

/**   \brief  type used to point to TcpIp_TxReqElemListDyn */
typedef P2VAR(TcpIp_TxReqElemListDynType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_TxReqElemListDynPtrType;

/**   \brief  type used to point to TcpIp_UdpTxRetryQueueElementChain */
typedef P2VAR(TcpIp_DListNodeType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueueElementChainPtrType;

/**   \brief  type used to point to TcpIp_UdpTxRetryQueueElements */
typedef P2VAR(TcpIp_Udp_RetryQueueElementType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueueElementsPtrType;

/**   \brief  type used to point to TcpIp_UdpTxRetryQueuePoolDesc */
typedef P2VAR(TcpIp_DListDescType, TYPEDEF, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueuePoolDescPtrType;

/** 
  \}
*/ 

/** 
  \defgroup  TcpIpPCRootValueTypes  TcpIp Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in TcpIp_PCConfig */
typedef struct sTcpIp_PCConfigType
{
  TcpIp_DefaultAddrV6PtrType DefaultAddrV6OfPCConfig;  /**< the pointer to TcpIp_DefaultAddrV6 */
  TcpIp_DhcpUserOptionPtrType DhcpUserOptionOfPCConfig;  /**< the pointer to TcpIp_DhcpUserOption */
  TcpIp_DhcpUserOptionBufferPtrType DhcpUserOptionBufferOfPCConfig;  /**< the pointer to TcpIp_DhcpUserOptionBuffer */
  TcpIp_DhcpUserOptionDynPtrType DhcpUserOptionDynOfPCConfig;  /**< the pointer to TcpIp_DhcpUserOptionDyn */
  TcpIp_IpV6MulticastAddrPtrType IpV6MulticastAddrOfPCConfig;  /**< the pointer to TcpIp_IpV6MulticastAddr */
  TcpIp_IpV6MulticastAddrActivePtrType IpV6MulticastAddrActiveOfPCConfig;  /**< the pointer to TcpIp_IpV6MulticastAddrActive */
  TcpIp_PhysAddrConfigPtrType PhysAddrConfigOfPCConfig;  /**< the pointer to TcpIp_PhysAddrConfig */
  TcpIp_TcpOooQElementPtrType TcpOooQElementOfPCConfig;  /**< the pointer to TcpIp_TcpOooQElement */
  TcpIp_TxReqElemPtrType TxReqElemOfPCConfig;  /**< the pointer to TcpIp_TxReqElem */
  TcpIp_TxReqElemListPtrType TxReqElemListOfPCConfig;  /**< the pointer to TcpIp_TxReqElemList */
  TcpIp_TxReqElemListDynPtrType TxReqElemListDynOfPCConfig;  /**< the pointer to TcpIp_TxReqElemListDyn */
  TcpIp_UdpTxRetryQueueElementChainPtrType UdpTxRetryQueueElementChainOfPCConfig;  /**< the pointer to TcpIp_UdpTxRetryQueueElementChain */
  TcpIp_UdpTxRetryQueueElementsPtrType UdpTxRetryQueueElementsOfPCConfig;  /**< the pointer to TcpIp_UdpTxRetryQueueElements */
  TcpIp_UdpTxRetryQueuePoolDescPtrType UdpTxRetryQueuePoolDescOfPCConfig;  /**< the pointer to TcpIp_UdpTxRetryQueuePoolDesc */
  TcpIp_SizeOfDefaultAddrV6Type SizeOfDefaultAddrV6OfPCConfig;  /**< the number of accomplishable value elements in TcpIp_DefaultAddrV6 */
  TcpIp_SizeOfDhcpUserOptionType SizeOfDhcpUserOptionOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_DhcpUserOption */
  TcpIp_SizeOfDhcpUserOptionBufferType SizeOfDhcpUserOptionBufferOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_DhcpUserOptionBuffer */
  TcpIp_SizeOfIpV6MulticastAddrType SizeOfIpV6MulticastAddrOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_IpV6MulticastAddr */
  TcpIp_SizeOfIpV6MulticastAddrActiveType SizeOfIpV6MulticastAddrActiveOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_IpV6MulticastAddrActive */
  TcpIp_SizeOfPhysAddrConfigType SizeOfPhysAddrConfigOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_PhysAddrConfig */
  TcpIp_SizeOfTcpOooQElementType SizeOfTcpOooQElementOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_TcpOooQElement */
  TcpIp_SizeOfTxReqElemType SizeOfTxReqElemOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_TxReqElem */
  TcpIp_SizeOfTxReqElemListType SizeOfTxReqElemListOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_TxReqElemList */
  TcpIp_SizeOfUdpTxRetryQueueElementChainType SizeOfUdpTxRetryQueueElementChainOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_UdpTxRetryQueueElementChain */
  TcpIp_SizeOfUdpTxRetryQueueElementsType SizeOfUdpTxRetryQueueElementsOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_UdpTxRetryQueueElements */
  TcpIp_SizeOfUdpTxRetryQueuePoolDescType SizeOfUdpTxRetryQueuePoolDescOfPCConfig;  /**< the number of accomplishable value elements in TcpIp_UdpTxRetryQueuePoolDesc */
} TcpIp_PCConfigType;

typedef TcpIp_PCConfigType TcpIp_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/**   \brief  type to access TcpIp_PCConfig in a symbol based style. */
typedef struct sTcpIp_PCConfigsType
{
  TcpIp_PCConfigType Config;  /**< [Config] */
} TcpIp_PCConfigsType;

/** 
  \}
*/ 




/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  TcpIp_DuplicateAddrDetectionFctPtr
**********************************************************************************************************************/
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_DuplicateAddrDetectionCbkType, TCPIP_CONST) TcpIp_DuplicateAddrDetectionFctPtr;  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_EthIfCtrl
**********************************************************************************************************************/
/** 
  \var    TcpIp_EthIfCtrl
  \brief  Array mapping from EthIfCtrlIdx to IpCtrlIdx
  \details
  Element        Description
  IpV6CtrlIdx    the index of the 0:1 relation pointing to TcpIp_IpV6Ctrl
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_EthIfCtrlType, TCPIP_CONST) TcpIp_EthIfCtrl[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IcmpV6Config
**********************************************************************************************************************/
/** 
  \var    TcpIp_IcmpV6Config
  \details
  Element           Description
  EchoRequestApi    Enable Echo Request API - Value of configuration parameter TcpIpIcmpV6EchoRequestApiEnabled [BOOLEAN]
  HopLimit          Hop Limit - Value of configuration parameter TcpIpIcmpV6HopLimit
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_IcmpV6ConfigType, TCPIP_CONST) TcpIp_IcmpV6Config[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IcmpV6MsgHandlerCbkFctPtr
**********************************************************************************************************************/
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_IcmpMultiPartMsgHandlerCbkType, TCPIP_CONST) TcpIp_IcmpV6MsgHandlerCbkFctPtr;
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6Ctrl
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6Ctrl
  \details
  Element                               Description
  DefaultLinkMtu                        Default Mtu Size - Value of configuration parameter TcpIpIpV6DefaultMtuSize
  MaskedBits                            contains bitcoded the boolean data of TcpIp_AllowLinkMtuReconfigurationOfIpV6Ctrl, TcpIp_DhcpUserOptionUsedOfIpV6Ctrl, TcpIp_EnableDynHopLimitOfIpV6Ctrl, TcpIp_EnablePathMtuOfIpV6Ctrl, TcpIp_HwChecksumIcmpOfIpV6Ctrl, TcpIp_HwChecksumIpDestinationOptionsOfIpV6Ctrl, TcpIp_HwChecksumIpHopByHopOptionsOfIpV6Ctrl, TcpIp_HwChecksumIpRoutingOfIpV6Ctrl, TcpIp_HwChecksumTcpOfIpV6Ctrl, TcpIp_HwChecksumUdpOfIpV6Ctrl, TcpIp_IpV6MulticastAddrUsedOfIpV6Ctrl, TcpIp_LocalAddrV6BcUsedOfIpV6Ctrl
  PathMtuTimeout                        Path Mtu Timeout - Value of configuration parameter TcpIpIpV6PathMtuTimeout [SECONDS]
  DefaultHopLimit                       Default Hop Limit - Value of configuration parameter TcpIpIpV6DefaultHopLimit
  DefaultTrafficClassFlowLabelNbo       -
  DhcpMode                              -
  DhcpUserOptionEndIdx                  the end index of the 0:n relation pointing to TcpIp_DhcpUserOption
  DhcpUserOptionStartIdx                the start index of the 0:n relation pointing to TcpIp_DhcpUserOption
  EthIfCtrlIdx                          -
  FramePrioDefault                      IP Frame Prio Default - Value of configuration parameter TcpIpIpFramePrioDefault
  InterfaceIdentifierEndIdx             the end index of the 1:n relation pointing to TcpIp_InterfaceIdentifier
  InterfaceIdentifierStartIdx           the start index of the 1:n relation pointing to TcpIp_InterfaceIdentifier
  IpV6DefaultRouterListEntryEndIdx      the end index of the 1:n relation pointing to TcpIp_IpV6DefaultRouterListEntry
  IpV6DefaultRouterListEntryStartIdx    the start index of the 1:n relation pointing to TcpIp_IpV6DefaultRouterListEntry
  IpV6DestinationCacheEntryEndIdx       the end index of the 1:n relation pointing to TcpIp_IpV6DestinationCacheEntry
  IpV6DestinationCacheEntryStartIdx     the start index of the 1:n relation pointing to TcpIp_IpV6DestinationCacheEntry
  IpV6EthBufDataEndIdx                  the end index of the 1:n relation pointing to TcpIp_IpV6EthBufData
  IpV6EthBufDataStartIdx                the start index of the 1:n relation pointing to TcpIp_IpV6EthBufData
  IpV6MulticastAddrEndIdx               the end index of the 0:n relation pointing to TcpIp_IpV6MulticastAddr
  IpV6MulticastAddrStartIdx             the start index of the 0:n relation pointing to TcpIp_IpV6MulticastAddr
  IpV6NeighborCacheEntryEndIdx          the end index of the 1:n relation pointing to TcpIp_IpV6NeighborCacheEntry
  IpV6NeighborCacheEntryStartIdx        the start index of the 1:n relation pointing to TcpIp_IpV6NeighborCacheEntry
  IpV6PrefixListEntryEndIdx             the end index of the 1:n relation pointing to TcpIp_IpV6PrefixListEntry
  IpV6PrefixListEntryStartIdx           the start index of the 1:n relation pointing to TcpIp_IpV6PrefixListEntry
  IpV6SourceAddressEndIdx               the end index of the 1:n relation pointing to TcpIp_IpV6SourceAddress
  IpV6SourceAddressStartIdx             the start index of the 1:n relation pointing to TcpIp_IpV6SourceAddress
  LocalAddrV6BcIdx                      the index of the 0:1 relation pointing to TcpIp_LocalAddrV6
  NdpConfigIdx                          the index of the 1:1 relation pointing to TcpIp_NdpConfig
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_IpV6CtrlType, TCPIP_CONST) TcpIp_IpV6Ctrl[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6General
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6General
  \details
  Element                   Description
  IpV6CtrlDefaultIdx        the index of the 0:1 relation pointing to TcpIp_IpV6Ctrl
  IpV6SocketDynIcmpIdx      the index of the 1:1 relation pointing to TcpIp_IpV6SocketDyn
  IpV6SocketDynNdpIdx       the index of the 1:1 relation pointing to TcpIp_IpV6SocketDyn
  IpV6SocketDynTcpRstIdx    the index of the 0:1 relation pointing to TcpIp_IpV6SocketDyn
  MaskedBits                contains bitcoded the boolean data of TcpIp_ExtDestAddrValidationEnabledOfIpV6General, TcpIp_IpV6CtrlDefaultUsedOfIpV6General, TcpIp_IpV6SocketDynTcpRstUsedOfIpV6General
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_IpV6GeneralType, TCPIP_CONST) TcpIp_IpV6General[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6SourceAddress
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6SourceAddress
  \brief  Structure containing all statically configured IP address information.
  \details
  Element                 Description
  AddressAssignVariant    Ip address configuration source.
  DefaultAddrV6Idx        the index of the 0:1 relation pointing to TcpIp_DefaultAddrV6
  LocalAddrV6Idx          the index of the 1:1 relation pointing to TcpIp_LocalAddrV6
  MaskedBits              contains bitcoded the boolean data of TcpIp_DefaultAddrV6UsedOfIpV6SourceAddress, TcpIp_PrefixIsOnLinkOfIpV6SourceAddress
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_IpV6SourceAddressType, TCPIP_CONST) TcpIp_IpV6SourceAddress[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_LocalAddrV6
**********************************************************************************************************************/
/** 
  \var    TcpIp_LocalAddrV6
  \brief  -
  \details
  Element                 Description
  IpV6CtrlIdx             the index of the 1:1 relation pointing to TcpIp_IpV6Ctrl
  IpV6MulticastAddrIdx    the index of the 0:1 relation pointing to TcpIp_IpV6MulticastAddr
  IpV6SourceAddressIdx    the index of the 0:1 relation pointing to TcpIp_IpV6SourceAddress
  MaskedBits              contains bitcoded the boolean data of TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6, TcpIp_IpV6SourceAddressUsedOfLocalAddrV6
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_LocalAddrV6Type, TCPIP_CONST) TcpIp_LocalAddrV6[2];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_NdpConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_NdpConfig
  \details
  Element                    Description
  MaskedBits                 contains bitcoded the boolean data of TcpIp_DynamicReachableTimeOfNdpConfig, TcpIp_DynamicRetransTimerOfNdpConfig, TcpIp_EnableNdpInvNaNcUpdateOfNdpConfig, TcpIp_EnableNdpInvNaOfNdpConfig, TcpIp_EnableNdpInvNsOfNdpConfig, TcpIp_EnableNudOfNdpConfig, TcpIp_EnableOptimisticDadOfNdpConfig, TcpIp_EnableRfc6106DnsslOptOfNdpConfig, TcpIp_EnableRfc6106RdnssOptOfNdpConfig, TcpIp_EnableSlaacDelayOfNdpConfig, TcpIp_RandomReachableTimeOfNdpConfig, TcpIp_RndRtrSolicitationDelayOfNdpConfig
  MaxRtrSolicitationDelay    Max Rtr Solicitation Delay - Value of configuration parameter TcpIpNdpMaxRtrSolicitationDelay [MILLISECONDS]
  MaxSlaacDelay              Max address configuration delay - Value of configuration parameter TcpIpNdpSlaacMaxDelay [MILLISECONDS]
  NudFirstProbeDelay         Delay First Probe Time - Value of configuration parameter TcpIpNdpDelayFirstProbeTime [MILLISECONDS]
  ReachableTime              Default Reachable Time - Value of configuration parameter TcpIpNdpDefaultReachableTime [MILLISECONDS]
  RetransTimer               Default Retrans Timer - Value of configuration parameter TcpIpNdpDefaultRetransTimer [MILLISECONDS]
  RtrSolicitationInterval    Rtr Solicitation Interval - Value of configuration parameter TcpIpNdpRtrSolicitationInterval [MILLISECONDS]
  SlaacMinLifetime           Minimum Address Lifetime - Value of configuration parameter TcpIpNdpSlaacMinLifetime [SECONDS]
  DadTransmits               DAD Number Of Transmissions - Value of configuration parameter TcpIpNdpSlaacDadNumberOfTransmissions
  MaxRandomFactor            Max Random Factor - Value of configuration parameter TcpIpNdpMaxRandomFactor
  MaxRtrSolicitations        Max Rtr Solicitations - Value of configuration parameter TcpIpNdpMaxRtrSolicitations
  MinRandomFactor            Min Random Factor - Value of configuration parameter TcpIpNdpMinRandomFactor
  MulticastSolicits          Num Multicast Solicitations - Value of configuration parameter TcpIpNdpNumMulticastSolicitations
  UnicastSolicits            Num Unicast Solicitations - Value of configuration parameter TcpIpNdpNumUnicastSolicitations
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_NdpConfigType, TCPIP_CONST) TcpIp_NdpConfig[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_RandomNumberFctPtr
**********************************************************************************************************************/
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_GetRandomNumberType, TCPIP_CONST) TcpIp_RandomNumberFctPtr;
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketOwnerConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketOwnerConfig
  \brief  -
  \details
  Element                            Description
  CopyTxDataDynFuncPtr               [User]_CopyTxDataDyn Callback Function. (only required if socket owner uses indirect data provistion in Tcp/UdpTransmit.)
  CopyTxDataFuncPtr                  [User]_CopyTxData Callback Function. (only required if socket owner uses indirect data provistion in Tcp/UdpTransmit.)
  DhcpEventFuncPtr                   Callout triggered on reception and transmission of client DHCP messages.
  LocalIpAddrAssignmentChgFuncPtr    -
  RxIndicationFuncPtr                [User]_RxIndication Callback Function. (required for socket owner that recieves packets.)
  TcpAcceptedFuncPtr                 [User]_TcpAccepted Callback Function. (only required if socker passively accepts TCP connections.)
  TcpConnectedFuncPtr                [User]_TcpConnected Callback Function. (only required if socker owner actively opens TCP connections.)
  TcpIpEventFuncPtr                  [User]_TcpIpEvent Callback Function. (required for every socket owner)
  TlsValidationResultFuncPtr         [User]_TlsValidationResult Callback Function.
  TxConfirmationFuncPtr              [User]_TxConfirmation Callback Function. (optional for every socket owner)
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_SocketOwnerConfigType, TCPIP_CONST) TcpIp_SocketOwnerConfig[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpConfig
  \details
  Element                    Description
  KeepAliveTime              Keep Alive Time - Value of configuration parameter TcpIpTcpKeepAliveTime [MAIN_FUNCTION_CYCLES]
  FinWait2Timeout            FIN Wait 2 Timeout - Value of configuration parameter TcpIpTcpFinWait2Timeout [MAIN_FUNCTION_CYCLES]
  Msl                        Maximum Segment Lifetime (MSL) - Value of configuration parameter TcpIpTcpMsl [MAIN_FUNCTION_CYCLES]
  RetransTimeoutMax          Retransmission Timeout Max - Value of configuration parameter TcpIpTcpRetransmissionTimeoutMax [MAIN_FUNCTION_CYCLES]
  RxMss                      maximum payload size of received TCP segments.
  TxMss                      maximum payload size of transmitted TCP segments.
  UserTimeoutDefCycles       User Timeout Def - Value of configuration parameter TcpIpTcpUserTimeoutDef [MAIN_FUNCTION_CYCLES]
  KeepAliveInterval          Keep Alive Interval - Value of configuration parameter TcpIpTcpKeepAliveInterval [MAIN_FUNCTION_CYCLES]
  KeepAliveProbesMax         Keep Alive Probes Max - Value of configuration parameter TcpIpTcpKeepAliveProbesMax
  NagleTimeout               Nagle Timeout - Value of configuration parameter TcpIpTcpNagleTimeout [MAIN_FUNCTION_CYCLES]
  RetransTimeout             Retransmission Timeout - Value of configuration parameter TcpIpTcpRetransmissionTimeout [MAIN_FUNCTION_CYCLES]
  TcpOooQSizePerSocketAvg    Avg Num Out Of Order Segments per Socket - Value of configuration parameter TcpIpTcpAvgNumOooSegsPerSocket
  TcpOooQSizePerSocketMax    Max Num Out Of Order Segments per Socket - Value of configuration parameter TcpIpTcpMaxNumOooSegsPerSocket
  TcpRetryQSize              Average Tx Retry Queue Size - Value of configuration parameter TcpIpTcpTxRetryQueueSize
  TimeToLiveDefault          Time-To-Live (TTL) - Value of configuration parameter TcpIpTcpTtl
  UserTimeoutMaxCycles       User Timeout Max - Value of configuration parameter TcpIpTcpUserTimeoutMax [MAIN_FUNCTION_CYCLES]
  UserTimeoutMinCycles       User Timeout Min - Value of configuration parameter TcpIpTcpUserTimeoutMin [MAIN_FUNCTION_CYCLES]
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_TcpConfigType, TCPIP_CONST) TcpIp_TcpConfig[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRxBufferDesc
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpRxBufferDesc
  \brief  TCP RX buffer descriptors
  \details
  Element                Description
  TcpRxBufferEndIdx      the end index of the 1:n relation pointing to TcpIp_TcpRxBuffer
  TcpRxBufferLength      the number of relations pointing to TcpIp_TcpRxBuffer
  TcpRxBufferStartIdx    the start index of the 1:n relation pointing to TcpIp_TcpRxBuffer
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_TcpRxBufferDescType, TCPIP_CONST) TcpIp_TcpRxBufferDesc[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpTxBufferDesc
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpTxBufferDesc
  \brief  TCP TX buffer descriptors
  \details
  Element                Description
  TcpTxBufferEndIdx      the end index of the 1:n relation pointing to TcpIp_TcpTxBuffer
  TcpTxBufferLength      the number of relations pointing to TcpIp_TcpTxBuffer
  TcpTxBufferStartIdx    the start index of the 1:n relation pointing to TcpIp_TcpTxBuffer
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_TcpTxBufferDescType, TCPIP_CONST) TcpIp_TcpTxBufferDesc[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TxReqElemList
**********************************************************************************************************************/
/** 
  \var    TcpIp_TxReqElemList
  \brief  UDP TX request list configuration
  \details
  Element              Description
  TxReqElemEndIdx      the end index of the 1:n relation pointing to TcpIp_TxReqElem
  TxReqElemLength      the number of relations pointing to TcpIp_TxReqElem
  TxReqElemStartIdx    the start index of the 1:n relation pointing to TcpIp_TxReqElem
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_TxReqElemListType, TCPIP_CONST) TcpIp_TxReqElemList[1];
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IcmpV6TxMsgBuffer
**********************************************************************************************************************/
/** 
  \var    TcpIp_IcmpV6TxMsgBuffer
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IcmpV6TxMsgBufferType, TCPIP_VAR_NOINIT) TcpIp_IcmpV6TxMsgBuffer[1280];
#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_InterfaceIdentifier
**********************************************************************************************************************/
/** 
  \var    TcpIp_InterfaceIdentifier
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_InterfaceIdentifierUType, TCPIP_VAR_NOINIT) TcpIp_InterfaceIdentifier;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6CtrlDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6CtrlDyn
  \brief  -
  \details
  Element                                  Description
  BaseReachableTimeMs                      Time a neighbor is considered to be reachable.
  ReachableTimeMs                          Time a neighbor is considered to be reachable.
  RetransTimerMs                           Interval between retransmissions.
  DefaultLinkMtu                           -
  CtrlPreviousState                        -
  CtrlState                                -
  CurHopLimit                              Hop Limit for outgoing packets, may be changed by received RAs.
  IpV6DefaultRouterListEntryValidEndIdx    the index of the 1:1 relation pointing to TcpIp_IpV6DefaultRouterListEntry
  IpV6DestinationCacheEntryValidEndIdx     the index of the 1:1 relation pointing to TcpIp_IpV6DestinationCacheEntry
  IpV6NeighborCacheEntryValidEndIdx        the index of the 1:1 relation pointing to TcpIp_IpV6NeighborCacheEntry
  IpV6PrefixListEntryValidEndIdx           the index of the 1:1 relation pointing to TcpIp_IpV6PrefixListEntry
  Ndp_RouterSolicitationTxCount            -
  LastBcAddrPtr                            -
  Ndp_PendingDadNa                         -
  Ndp_RouterSolicitationNextTxTime         -
  NextRouterProbeIdx                       -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6CtrlDynUType, TCPIP_VAR_NOINIT) TcpIp_IpV6CtrlDyn;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6DefaultRouterListEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6DefaultRouterListEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6DefaultRouterListEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6DefaultRouterListEntry;  /* PRQA S 0777, 0759 */  /* MD_MSR_Rule5.1, MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6DestinationCacheEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6DestinationCacheEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6DestinationCacheEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6DestinationCacheEntry;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6EthBufData
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6EthBufData
  \brief  -
  \details
  Element             Description
  IpV6SocketDynIdx    the index of the 0:1 relation pointing to TcpIp_IpV6SocketDyn
  UlTxReqTabIdx       Store which socket index belongs to the BufIdx of CtrlIdx.
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6EthBufDataUType, TCPIP_VAR_NOINIT) TcpIp_IpV6EthBufData;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6NeighborCacheEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6NeighborCacheEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6NeighborCacheEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6NeighborCacheEntry;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6PrefixListEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6PrefixListEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6PrefixListEntryUType, TCPIP_VAR_NOINIT) TcpIp_IpV6PrefixListEntry;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6SocketDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6SocketDyn
  \brief  -
  \details
  Element                             Description
  IpV6HdrVersionTcFlNbo               -
  EthIfFramePrio                      -
  Flags                               -
  HopLimit                            -
  IpV6DestinationCacheEntryHintIdx    the index of the 0:1 relation pointing to TcpIp_IpV6DestinationCacheEntry
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_IpV6SocketDynType, TCPIP_VAR_NOINIT) TcpIp_IpV6SocketDyn[5];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_IpV6SourceAddressTableEntry
**********************************************************************************************************************/
/** 
  \var    TcpIp_IpV6SourceAddressTableEntry
  \brief  -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(IpV6_SourceAddressTableEntryType, TCPIP_VAR_NOINIT) TcpIp_IpV6SourceAddressTableEntry[1];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_LocalAddr
**********************************************************************************************************************/
/** 
  \var    TcpIp_LocalAddr
  \details
  Element            Description
  AssignmentState    -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_LocalAddrUType, TCPIP_VAR_NOINIT) TcpIp_LocalAddr;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketDyn
  \brief  Generic (TCP/UDP) socket variables
  \details
  Element                 Description
  ListenActiveConnStat    Current state of the socket
  LocalAddrBindIdx        the index of the 0:1 relation pointing to TcpIp_LocalAddr
  SocketOwnerConfigIdx    the index of the 0:1 relation pointing to TcpIp_SocketOwnerConfig
  TxBufRequested          TX buffer request semaphore
  TxIpAddrIdx             Index of local IPxV address that is used for transmission
  LocSock                 IP address and port of the local host
  RemSock                 IP address and port of the remote host
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_SocketDynUType, TCPIP_VAR_NOINIT) TcpIp_SocketDyn;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketTcpDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketTcpDyn
  \brief  TCP socket specific variables
  \details
  Element                              Description
  FinWait2Timeout                      -
  IdleTimeoutShort                     -
  Iss                                  -
  MslTimeout                           -
  RcvNxt                               -
  RetransmitTimeout                    -
  RtoReloadValue                       -
  SndNxt                               -
  SndUna                               -
  SndWl1                               -
  SndWl2                               -
  TxNextSendSeqNo                      -
  TxReqSeqNo                           Sequence number of first pending TX request.
  MaxNumListenSockets                  -
  PathMtuNewSize                       -
  RcvWnd                               -
  SndWnd                               -
  TxMaxSegLenByte                      -
  TxMssAgreed                          Maximum segment size agreed during connection setup
  EventIndicationPending               Varibale contains the pending events that have to be forwarded to the socket user
  PathMtuChanged                       -
  RetryQFillNum                        -
  RstReceived                          -
  SockIsServer                         -
  SockState                            -
  SockStateNext                        -
  SocketTcpDynMasterListenSocketIdx    the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn
  TcpRetryQElementFirstIdx             the index of the 0:1 relation pointing to TcpIp_TcpRetryQElement
  TcpRetryQElementLastIdx              the index of the 0:1 relation pointing to TcpIp_TcpRetryQElement
  TcpRxBufferDescIdx                   the index of the 0:1 relation pointing to TcpIp_TcpRxBufferDesc
  TcpTxBufferDescIdx                   the index of the 0:1 relation pointing to TcpIp_TcpTxBufferDesc
  TxFlags                              -
  TxOneTimeOpts                        -
  TxOneTimeOptsLen                     -
  TxOptLen                             -
  TxReqFullyQueued                     all unqueued data from this request is used within the next segment
  BackLogArray                         -
  RequestedRxBufferSize                -
  RequestedTxBufferSize                -
  RxBufferIndPos                       -
  RxBufferRemIndLen                    -
  TxLenByteTx                          -
  TxReqDataBufStartIdx                 byte position inside the TxBuffer
  TxReqDataLenByte                     length of data to be transmitted
  TxReqQueuedLen                       length of data that could already be queued for transmission
  TxTotNotQueuedLen                    -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_SocketTcpDynType, TCPIP_VAR_NOINIT) TcpIp_SocketTcpDyn[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_SocketUdpDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_SocketUdpDyn
  \brief  UDP socket specific variables
  \details
  Element                  Description
  TxReqElemListIdx         the index of the 0:1 relation pointing to TcpIp_TxReqElemList
  TxRetrQueueMaxNum        Maximum number of TX retry queue elements for the socket.
  IpTxRequestDescriptor    TX request descriptor required for the IP layer
  TxReqIpBufPtr            Pointer to the provided TX buffer of the IP layer
  TxRetrQueue              TX retry queue
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_SocketUdpDynType, TCPIP_VAR_NOINIT) TcpIp_SocketUdpDyn[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpResetQElement
**********************************************************************************************************************/
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_Tcp_RstTxQueueType, TCPIP_VAR_NOINIT) TcpIp_TcpResetQElement[8];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRetryQElement
**********************************************************************************************************************/
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_Tcp_TxRetrQueueType, TCPIP_VAR_NOINIT) TcpIp_TcpRetryQElement[10];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRxBuffer
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpRxBuffer
  \brief  TCP TX buffers
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_TcpRxBufferUType, TCPIP_VAR_NOINIT) TcpIp_TcpRxBuffer;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpRxBufferDescDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpRxBufferDescDyn
  \details
  Element                Description
  TcpRxBufferWriteIdx    the index of the 1:1 relation pointing to TcpIp_TcpRxBuffer
  SocketTcpDynIdx        the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn
  FillLevel          
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_TcpRxBufferDescDynType, TCPIP_VAR_NOINIT) TcpIp_TcpRxBufferDescDyn[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpTxBuffer
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpTxBuffer
  \brief  TCP TX buffers
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_TcpTxBufferUType, TCPIP_VAR_NOINIT) TcpIp_TcpTxBuffer;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TcpTxBufferDescDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_TcpTxBufferDescDyn
  \details
  Element                Description
  TcpTxBufferWriteIdx    the index of the 1:1 relation pointing to TcpIp_TcpTxBuffer
  SocketTcpDynIdx        the index of the 0:1 relation pointing to TcpIp_SocketTcpDyn
  FillLevel          
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_TcpTxBufferDescDynType, TCPIP_VAR_NOINIT) TcpIp_TcpTxBufferDescDyn[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TxReqElem
**********************************************************************************************************************/
/** 
  \var    TcpIp_TxReqElem
  \brief  UDP TX request list elements
  \details
  Element                 Description
  TxReqElemDataLenByte    -
  TxReqElemTransmitted    -
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_TxReqElemUType, TCPIP_VAR_NOINIT) TcpIp_TxReqElem;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_TxReqElemListDyn
**********************************************************************************************************************/
/** 
  \var    TcpIp_TxReqElemListDyn
  \brief  UDP TX request list variables
  \details
  Element            Description
  FillNum            Number of elements currently stored in TX request ring buffer
  ReadPos            Read position of TX request ring buffer
  SocketUdpDynIdx    the index of the 0:1 relation pointing to TcpIp_SocketUdpDyn
  WritePos           Write position in TX request ring buffer
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_TxReqElemListDynType, TCPIP_VAR_NOINIT) TcpIp_TxReqElemListDyn[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_UdpTxRetryQueueElementChain
**********************************************************************************************************************/
/** 
  \var    TcpIp_UdpTxRetryQueueElementChain
  \brief  UDP TX retry queue element chain
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_DListNodeType, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueueElementChain[1];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_UdpTxRetryQueueElements
**********************************************************************************************************************/
/** 
  \var    TcpIp_UdpTxRetryQueueElements
  \brief  UDP TX retry queue elements
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_Udp_RetryQueueElementType, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueueElements[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_UdpTxRetryQueuePoolDesc
**********************************************************************************************************************/
/** 
  \var    TcpIp_UdpTxRetryQueuePoolDesc
  \brief  UDP TX retry queue element pool
*/ 
#define TCPIP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(TcpIp_DListDescType, TCPIP_VAR_NOINIT) TcpIp_UdpTxRetryQueuePoolDesc[1];
#define TCPIP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  TcpIp_PCConfig
**********************************************************************************************************************/
/** 
  \var    TcpIp_PCConfig
  \details
  Element                              Description
  DefaultAddrV6                        the pointer to TcpIp_DefaultAddrV6
  DhcpUserOption                       the pointer to TcpIp_DhcpUserOption
  DhcpUserOptionBuffer                 the pointer to TcpIp_DhcpUserOptionBuffer
  DhcpUserOptionDyn                    the pointer to TcpIp_DhcpUserOptionDyn
  IpV6MulticastAddr                    the pointer to TcpIp_IpV6MulticastAddr
  IpV6MulticastAddrActive              the pointer to TcpIp_IpV6MulticastAddrActive
  PhysAddrConfig                       the pointer to TcpIp_PhysAddrConfig
  TcpOooQElement                       the pointer to TcpIp_TcpOooQElement
  TxReqElem                            the pointer to TcpIp_TxReqElem
  TxReqElemList                        the pointer to TcpIp_TxReqElemList
  TxReqElemListDyn                     the pointer to TcpIp_TxReqElemListDyn
  UdpTxRetryQueueElementChain          the pointer to TcpIp_UdpTxRetryQueueElementChain
  UdpTxRetryQueueElements              the pointer to TcpIp_UdpTxRetryQueueElements
  UdpTxRetryQueuePoolDesc              the pointer to TcpIp_UdpTxRetryQueuePoolDesc
  SizeOfDefaultAddrV6                  the number of accomplishable value elements in TcpIp_DefaultAddrV6
  SizeOfDhcpUserOption                 the number of accomplishable value elements in TcpIp_DhcpUserOption
  SizeOfDhcpUserOptionBuffer           the number of accomplishable value elements in TcpIp_DhcpUserOptionBuffer
  SizeOfIpV6MulticastAddr              the number of accomplishable value elements in TcpIp_IpV6MulticastAddr
  SizeOfIpV6MulticastAddrActive        the number of accomplishable value elements in TcpIp_IpV6MulticastAddrActive
  SizeOfPhysAddrConfig                 the number of accomplishable value elements in TcpIp_PhysAddrConfig
  SizeOfTcpOooQElement                 the number of accomplishable value elements in TcpIp_TcpOooQElement
  SizeOfTxReqElem                      the number of accomplishable value elements in TcpIp_TxReqElem
  SizeOfTxReqElemList                  the number of accomplishable value elements in TcpIp_TxReqElemList
  SizeOfUdpTxRetryQueueElementChain    the number of accomplishable value elements in TcpIp_UdpTxRetryQueueElementChain
  SizeOfUdpTxRetryQueueElements        the number of accomplishable value elements in TcpIp_UdpTxRetryQueueElements
  SizeOfUdpTxRetryQueuePoolDesc        the number of accomplishable value elements in TcpIp_UdpTxRetryQueuePoolDesc
*/ 
#define TCPIP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(TcpIp_PCConfigsType, TCPIP_CONST) TcpIp_PCConfig;
#define TCPIP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL INLINE FUNCTION PROTOTYPES
**********************************************************************************************************************/
#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetDataInlineFunctionPrototypes  Get Data Inline Function Prototypes
  \brief  These inline functions can be used to read CONST and VAR data.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(IpV6_AddrType, TCPIP_CODE) TcpIp_GetDefaultAddrV6(TcpIp_DefaultAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_CodeOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetCodeOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferEndIdxOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBufferEndIdxOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferLengthOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBufferLengthOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferStartIdxOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBufferStartIdxOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionDirectionType, TCPIP_CODE) TcpIp_GetDirectionOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LengthOfDhcpUserOptionDynType, TCPIP_CODE) TcpIp_GetLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DuplicateAddrDetectionCbkType, TCPIP_CODE) TcpIp_GetDuplicateAddrDetectionFctPtr(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlIdxOfEthIfCtrlType, TCPIP_CODE) TcpIp_GetIpV6CtrlIdxOfEthIfCtrl(TcpIp_EthIfCtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EchoRequestApiOfIcmpV6ConfigType, TCPIP_CODE) TcpIp_IsEchoRequestApiOfIcmpV6Config(TcpIp_IcmpV6ConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HopLimitOfIcmpV6ConfigType, TCPIP_CODE) TcpIp_GetHopLimitOfIcmpV6Config(TcpIp_IcmpV6ConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IcmpMultiPartMsgHandlerCbkType, TCPIP_CODE) TcpIp_GetIcmpV6MsgHandlerCbkFctPtr(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IcmpV6TxMsgBufferType, TCPIP_CODE) TcpIp_GetIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierType, TCPIP_CODE) TcpIp_GetInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultHopLimitOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDefaultHopLimitOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultLinkMtuOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDefaultLinkMtuOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultTrafficClassFlowLabelNboOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDefaultTrafficClassFlowLabelNboOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpModeOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDhcpModeOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDhcpUserOptionEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDhcpUserOptionStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EthIfCtrlIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetEthIfCtrlIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_FramePrioDefaultOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetFramePrioDefaultOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetInterfaceIdentifierEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetInterfaceIdentifierStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6EthBufDataEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6EthBufDataEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6EthBufDataStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6EthBufDataStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6SourceAddressEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6SourceAddressStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6BcIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetLocalAddrV6BcIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_NdpConfigIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetNdpConfigIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_PathMtuTimeoutOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetPathMtuTimeoutOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_CtrlPreviousStateOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_CtrlStateOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_CurHopLimitOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultLinkMtuOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(P2CONST(IpV6_AddrType, AUTOMATIC, IPV4_APPL_VAR), TCPIP_CODE) TcpIp_GetLastBcAddrPtrOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_Ndp_PendingDadNaType, TCPIP_CODE) TcpIp_GetNdp_PendingDadNaOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_TimeType, TCPIP_CODE) TcpIp_GetNdp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_ListIdxType, TCPIP_CODE) TcpIp_GetNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_ReachableTimeMsOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimerMsOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_DefaultRouterListEntryType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntry(TcpIp_IpV6DefaultRouterListEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_DestinationCacheEntryType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntry(TcpIp_IpV6DestinationCacheEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType, TCPIP_CODE) TcpIp_GetIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType, TCPIP_CODE) TcpIp_GetUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlDefaultIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6CtrlDefaultIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynIcmpIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6SocketDynIcmpIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynNdpIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6SocketDynNdpIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynTcpRstIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6SocketDynTcpRstIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6IdxOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetDefaultAddrV6IdxOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrActiveIdxOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrActiveIdxOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6IdxOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetLocalAddrV6IdxOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_AddrType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrActive(TcpIp_IpV6MulticastAddrActiveIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_NeighborCacheEntryType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntry(TcpIp_IpV6NeighborCacheEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_PrefixListEntryType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntry(TcpIp_IpV6PrefixListEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EthIfFramePrioOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_FlagsOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HopLimitOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_AddressAssignVariantOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetAddressAssignVariantOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6IdxOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetDefaultAddrV6IdxOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6IdxOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetLocalAddrV6IdxOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index);
TCPIP_LOCAL_INLINE FUNC(IpV6_SourceAddressTableEntryType, TCPIP_CODE) TcpIp_GetIpV6SourceAddressTableEntry(TcpIp_IpV6SourceAddressTableEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_AssignmentStateOfLocalAddrType, TCPIP_CODE) TcpIp_GetAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlIdxOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetIpV6CtrlIdxOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrIdxOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrIdxOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressIdxOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetIpV6SourceAddressIdxOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetMaskedBitsOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DadTransmitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetDadTransmitsOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaskedBitsOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxRandomFactorOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxRandomFactorOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxRtrSolicitationDelayOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxRtrSolicitationDelayOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxRtrSolicitationsOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxRtrSolicitationsOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxSlaacDelayOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxSlaacDelayOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MinRandomFactorOfNdpConfigType, TCPIP_CODE) TcpIp_GetMinRandomFactorOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MulticastSolicitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetMulticastSolicitsOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_NudFirstProbeDelayOfNdpConfigType, TCPIP_CODE) TcpIp_GetNudFirstProbeDelayOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_ReachableTimeOfNdpConfigType, TCPIP_CODE) TcpIp_GetReachableTimeOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimerOfNdpConfigType, TCPIP_CODE) TcpIp_GetRetransTimerOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RtrSolicitationIntervalOfNdpConfigType, TCPIP_CODE) TcpIp_GetRtrSolicitationIntervalOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SlaacMinLifetimeOfNdpConfigType, TCPIP_CODE) TcpIp_GetSlaacMinLifetimeOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UnicastSolicitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetUnicastSolicitsOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_PhysAddrTableChgDiscardedCbkType, TCPIP_CODE) TcpIp_GetChgDiscardedFuncPtrOfPhysAddrConfig(TcpIp_PhysAddrConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_PhysAddrTableChgCbkType, TCPIP_CODE) TcpIp_GetChgFuncPtrOfPhysAddrConfig(TcpIp_PhysAddrConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_GetRandomNumberType, TCPIP_CODE) TcpIp_GetRandomNumberFctPtr(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_ListenActiveConnStatOfSocketDynType, TCPIP_CODE) TcpIp_GetListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockAddrBaseType, TCPIP_CODE) TcpIp_GetLocSockOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrBindIdxOfSocketDynType, TCPIP_CODE) TcpIp_GetLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockAddrBaseType, TCPIP_CODE) TcpIp_GetRemSockOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerConfigIdxOfSocketDynType, TCPIP_CODE) TcpIp_GetSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxBufRequestedOfSocketDynType, TCPIP_CODE) TcpIp_IsTxBufRequestedOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxIpAddrIdxOfSocketDynType, TCPIP_CODE) TcpIp_GetTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerCopyTxDataDynType, TCPIP_CODE) TcpIp_GetCopyTxDataDynFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerCopyTxDataType, TCPIP_CODE) TcpIp_GetCopyTxDataFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerDhcpEventType, TCPIP_CODE) TcpIp_GetDhcpEventFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerLocalIpAddrAssignmentChgType, TCPIP_CODE) TcpIp_GetLocalIpAddrAssignmentChgFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerRxIndicationType, TCPIP_CODE) TcpIp_GetRxIndicationFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTcpAcceptedType, TCPIP_CODE) TcpIp_GetTcpAcceptedFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTcpConnectedType, TCPIP_CODE) TcpIp_GetTcpConnectedFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTcpIpEventType, TCPIP_CODE) TcpIp_GetTcpIpEventFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTlsValidationResultType, TCPIP_CODE) TcpIp_GetTlsValidationResultFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTxConfirmationType, TCPIP_CODE) TcpIp_GetTxConfirmationFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_BackLogEleType, TCPIP_CODE) TcpIp_GetBackLogArrayOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EventIndicationPendingOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_FinWait2TimeoutOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IdleTimeoutShortOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IssOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxNumListenSocketsOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MslTimeoutOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_PathMtuChangedOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsPathMtuChangedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_PathMtuNewSizeOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RcvNxtOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RcvWndOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransmitTimeoutOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetryQFillNumOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RstReceivedOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsRstReceivedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RtoReloadValueOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndNxtOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndUnaOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndWl1OfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndWl2OfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndWndOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockIsServerOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsSockIsServerOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockStateNextOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockStateOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxFlagsOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxMaxSegLenByteOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxMssAgreedOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxNextSendSeqNoOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxOneTimeOptsLenOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxOneTimeOptsOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxOptLenOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqFullyQueuedOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsTxReqFullyQueuedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqSeqNoOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpTxRequestDescriptorType, TCPIP_CODE) TcpIp_GetIpTxRequestDescriptorOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemListIdxOfSocketUdpDynType, TCPIP_CODE) TcpIp_GetTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(P2VAR(uint8, AUTOMATIC, IPV4_APPL_VAR), TCPIP_CODE) TcpIp_GetTxReqIpBufPtrOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType, TCPIP_CODE) TcpIp_GetTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DListType, TCPIP_CODE) TcpIp_GetTxRetrQueueOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_FinWait2TimeoutOfTcpConfigType, TCPIP_CODE) TcpIp_GetFinWait2TimeoutOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_KeepAliveIntervalOfTcpConfigType, TCPIP_CODE) TcpIp_GetKeepAliveIntervalOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_KeepAliveProbesMaxOfTcpConfigType, TCPIP_CODE) TcpIp_GetKeepAliveProbesMaxOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_KeepAliveTimeOfTcpConfigType, TCPIP_CODE) TcpIp_GetKeepAliveTimeOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_MslOfTcpConfigType, TCPIP_CODE) TcpIp_GetMslOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_NagleTimeoutOfTcpConfigType, TCPIP_CODE) TcpIp_GetNagleTimeoutOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimeoutMaxOfTcpConfigType, TCPIP_CODE) TcpIp_GetRetransTimeoutMaxOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimeoutOfTcpConfigType, TCPIP_CODE) TcpIp_GetRetransTimeoutOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RxMssOfTcpConfigType, TCPIP_CODE) TcpIp_GetRxMssOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpOooQSizePerSocketAvgOfTcpConfigType, TCPIP_CODE) TcpIp_GetTcpOooQSizePerSocketAvgOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpOooQSizePerSocketMaxOfTcpConfigType, TCPIP_CODE) TcpIp_GetTcpOooQSizePerSocketMaxOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQSizeOfTcpConfigType, TCPIP_CODE) TcpIp_GetTcpRetryQSizeOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TimeToLiveDefaultOfTcpConfigType, TCPIP_CODE) TcpIp_GetTimeToLiveDefaultOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxMssOfTcpConfigType, TCPIP_CODE) TcpIp_GetTxMssOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UserTimeoutDefCyclesOfTcpConfigType, TCPIP_CODE) TcpIp_GetUserTimeoutDefCyclesOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UserTimeoutMaxCyclesOfTcpConfigType, TCPIP_CODE) TcpIp_GetUserTimeoutMaxCyclesOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UserTimeoutMinCyclesOfTcpConfigType, TCPIP_CODE) TcpIp_GetUserTimeoutMinCyclesOfTcpConfig(TcpIp_TcpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_RxPreBufEleType, TCPIP_CODE) TcpIp_GetTcpOooQElement(TcpIp_TcpOooQElementIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_RstTxQueueType, TCPIP_CODE) TcpIp_GetTcpResetQElement(TcpIp_TcpResetQElementIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_TxRetrQueueType, TCPIP_CODE) TcpIp_GetTcpRetryQElement(TcpIp_TcpRetryQElementIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferType, TCPIP_CODE) TcpIp_GetTcpRxBuffer(TcpIp_TcpRxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferEndIdxOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetTcpRxBufferEndIdxOfTcpRxBufferDesc(TcpIp_TcpRxBufferDescIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferLengthOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetTcpRxBufferLengthOfTcpRxBufferDesc(TcpIp_TcpRxBufferDescIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferStartIdxOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetTcpRxBufferStartIdxOfTcpRxBufferDesc(TcpIp_TcpRxBufferDescIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType, TCPIP_CODE) TcpIp_GetSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType, TCPIP_CODE) TcpIp_GetTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferType, TCPIP_CODE) TcpIp_GetTcpTxBuffer(TcpIp_TcpTxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferEndIdxOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetTcpTxBufferEndIdxOfTcpTxBufferDesc(TcpIp_TcpTxBufferDescIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferLengthOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetTcpTxBufferLengthOfTcpTxBufferDesc(TcpIp_TcpTxBufferDescIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferStartIdxOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetTcpTxBufferStartIdxOfTcpTxBufferDesc(TcpIp_TcpTxBufferDescIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType, TCPIP_CODE) TcpIp_GetSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType, TCPIP_CODE) TcpIp_GetTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemDataLenByteOfTxReqElemType, TCPIP_CODE) TcpIp_GetTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemTransmittedOfTxReqElemType, TCPIP_CODE) TcpIp_IsTxReqElemTransmittedOfTxReqElem(TcpIp_TxReqElemIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemEndIdxOfTxReqElemListType, TCPIP_CODE) TcpIp_GetTxReqElemEndIdxOfTxReqElemList(TcpIp_TxReqElemListIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemLengthOfTxReqElemListType, TCPIP_CODE) TcpIp_GetTxReqElemLengthOfTxReqElemList(TcpIp_TxReqElemListIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemStartIdxOfTxReqElemListType, TCPIP_CODE) TcpIp_GetTxReqElemStartIdxOfTxReqElemList(TcpIp_TxReqElemListIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_FillNumOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_ReadPosOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketUdpDynIdxOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_WritePosOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DListNodeType, TCPIP_CODE) TcpIp_GetUdpTxRetryQueueElementChain(TcpIp_UdpTxRetryQueueElementChainIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Udp_RetryQueueElementType, TCPIP_CODE) TcpIp_GetUdpTxRetryQueueElements(TcpIp_UdpTxRetryQueueElementsIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DListDescType, TCPIP_CODE) TcpIp_GetUdpTxRetryQueuePoolDesc(TcpIp_UdpTxRetryQueuePoolDescIterType Index);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetBitDataInlineFunctionPrototypes  Get Bit Data Inline Function Prototypes
  \brief  These inline functions can be used to read bitcoded data elements.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsIpV6SocketDynTcpRstUsedOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsIpV6MulticastAddrActiveUsedOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsPrefixIsOnLinkOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsEnableNdpInvNaOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsEnableNudOfNdpConfig(TcpIp_NdpConfigIterType Index);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetDeduplicatedInlineFunctionPrototypes  Get Deduplicated Inline Function Prototypes
  \brief  These inline functions  can be used to read deduplicated data elements.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlUsedOfEthIfCtrlType, TCPIP_CODE) TcpIp_IsIpV6CtrlUsedOfEthIfCtrl(TcpIp_EthIfCtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_AllowLinkMtuReconfigurationOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsAllowLinkMtuReconfigurationOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionUsedOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsDhcpUserOptionUsedOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnablePathMtuOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsEnablePathMtuOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIcmpOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIcmpOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIpDestinationOptionsOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIpDestinationOptionsOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIpHopByHopOptionsOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIpHopByHopOptionsOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIpRoutingOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIpRoutingOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumTcpOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumTcpOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumUdpOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumUdpOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrUsedOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsIpV6MulticastAddrUsedOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6BcUsedOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsLocalAddrV6BcUsedOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_ExtDestAddrValidationEnabledOfIpV6GeneralType, TCPIP_CODE) TcpIp_IsExtDestAddrValidationEnabledOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlDefaultUsedOfIpV6GeneralType, TCPIP_CODE) TcpIp_IsIpV6CtrlDefaultUsedOfIpV6General(TcpIp_IpV6GeneralIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6UsedOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_IsDefaultAddrV6UsedOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6UsedOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_IsDefaultAddrV6UsedOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6Type, TCPIP_CODE) TcpIp_IsIpV6MulticastAddrUsedOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressUsedOfLocalAddrV6Type, TCPIP_CODE) TcpIp_IsIpV6SourceAddressUsedOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DynamicReachableTimeOfNdpConfigType, TCPIP_CODE) TcpIp_IsDynamicReachableTimeOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DynamicRetransTimerOfNdpConfigType, TCPIP_CODE) TcpIp_IsDynamicRetransTimerOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableNdpInvNaNcUpdateOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableNdpInvNaNcUpdateOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableNdpInvNsOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableNdpInvNsOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableOptimisticDadOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableOptimisticDadOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableRfc6106DnsslOptOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableRfc6106DnsslOptOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableRfc6106RdnssOptOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableRfc6106RdnssOptOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableSlaacDelayOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableSlaacDelayOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RandomReachableTimeOfNdpConfigType, TCPIP_CODE) TcpIp_IsRandomReachableTimeOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RndRtrSolicitationDelayOfNdpConfigType, TCPIP_CODE) TcpIp_IsRndRtrSolicitationDelayOfNdpConfig(TcpIp_NdpConfigIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDefaultAddrV6Type, TCPIP_CODE) TcpIp_GetSizeOfDefaultAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetSizeOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDhcpUserOptionBufferType, TCPIP_CODE) TcpIp_GetSizeOfDhcpUserOptionBuffer(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDhcpUserOptionDynType, TCPIP_CODE) TcpIp_GetSizeOfDhcpUserOptionDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfEthIfCtrlType, TCPIP_CODE) TcpIp_GetSizeOfEthIfCtrl(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIcmpV6ConfigType, TCPIP_CODE) TcpIp_GetSizeOfIcmpV6Config(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIcmpV6TxMsgBufferType, TCPIP_CODE) TcpIp_GetSizeOfIcmpV6TxMsgBuffer(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfInterfaceIdentifierType, TCPIP_CODE) TcpIp_GetSizeOfInterfaceIdentifier(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetSizeOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetSizeOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6DefaultRouterListEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6DefaultRouterListEntry(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6DestinationCacheEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6DestinationCacheEntry(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6EthBufDataType, TCPIP_CODE) TcpIp_GetSizeOfIpV6EthBufData(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetSizeOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetSizeOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6MulticastAddrActiveType, TCPIP_CODE) TcpIp_GetSizeOfIpV6MulticastAddrActive(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6NeighborCacheEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6NeighborCacheEntry(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6PrefixListEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6PrefixListEntry(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetSizeOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetSizeOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6SourceAddressTableEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6SourceAddressTableEntry(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfLocalAddrType, TCPIP_CODE) TcpIp_GetSizeOfLocalAddr(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetSizeOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfNdpConfigType, TCPIP_CODE) TcpIp_GetSizeOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfPhysAddrConfigType, TCPIP_CODE) TcpIp_GetSizeOfPhysAddrConfig(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketDynType, TCPIP_CODE) TcpIp_GetSizeOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketOwnerConfigType, TCPIP_CODE) TcpIp_GetSizeOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSizeOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketUdpDynType, TCPIP_CODE) TcpIp_GetSizeOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpConfigType, TCPIP_CODE) TcpIp_GetSizeOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpOooQElementType, TCPIP_CODE) TcpIp_GetSizeOfTcpOooQElement(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpResetQElementType, TCPIP_CODE) TcpIp_GetSizeOfTcpResetQElement(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRetryQElementType, TCPIP_CODE) TcpIp_GetSizeOfTcpRetryQElement(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetSizeOfTcpRxBuffer(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetSizeOfTcpRxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferDescDynType, TCPIP_CODE) TcpIp_GetSizeOfTcpRxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetSizeOfTcpTxBuffer(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetSizeOfTcpTxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferDescDynType, TCPIP_CODE) TcpIp_GetSizeOfTcpTxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTxReqElemType, TCPIP_CODE) TcpIp_GetSizeOfTxReqElem(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTxReqElemListType, TCPIP_CODE) TcpIp_GetSizeOfTxReqElemList(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetSizeOfTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfUdpTxRetryQueueElementChainType, TCPIP_CODE) TcpIp_GetSizeOfUdpTxRetryQueueElementChain(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfUdpTxRetryQueueElementsType, TCPIP_CODE) TcpIp_GetSizeOfUdpTxRetryQueueElements(void);
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfUdpTxRetryQueuePoolDescType, TCPIP_CODE) TcpIp_GetSizeOfUdpTxRetryQueuePoolDesc(void);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCSetDataInlineFunctionPrototypes  Set Data Inline Function Prototypes
  \brief  These inline functions can be used to write data.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index, TcpIp_DhcpUserOptionBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index, TcpIp_LengthOfDhcpUserOptionDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index, TcpIp_IcmpV6TxMsgBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index, TcpIp_InterfaceIdentifierType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_CtrlPreviousStateOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_CtrlStateOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_CurHopLimitOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_DefaultLinkMtuOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLastBcAddrPtrOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, P2CONST(IpV6_AddrType, AUTOMATIC, IPV4_APPL_VAR) Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNdp_PendingDadNaOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, IpV6_Ndp_PendingDadNaType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNdp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, IpV6_TimeType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, IpV6_ListIdxType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_ReachableTimeMsOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_RetransTimerMsOfIpV6CtrlDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DefaultRouterListEntry(TcpIp_IpV6DefaultRouterListEntryIterType Index, IpV6_DefaultRouterListEntryType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DestinationCacheEntry(TcpIp_IpV6DestinationCacheEntryIterType Index, IpV6_DestinationCacheEntryType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index, TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index, TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6MulticastAddrActive(TcpIp_IpV6MulticastAddrActiveIterType Index, IpV6_AddrType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6NeighborCacheEntry(TcpIp_IpV6NeighborCacheEntryIterType Index, IpV6_NeighborCacheEntryType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6PrefixListEntry(TcpIp_IpV6PrefixListEntryIterType Index, IpV6_PrefixListEntryType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_EthIfFramePrioOfIpV6SocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_FlagsOfIpV6SocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_HopLimitOfIpV6SocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6SourceAddressTableEntry(TcpIp_IpV6SourceAddressTableEntryIterType Index, IpV6_SourceAddressTableEntryType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index, TcpIp_AssignmentStateOfLocalAddrType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_ListenActiveConnStatOfSocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLocSockOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_SockAddrBaseType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_LocalAddrBindIdxOfSocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRemSockOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_SockAddrBaseType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_SocketOwnerConfigIdxOfSocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxBufRequestedOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_TxBufRequestedOfSocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_TxIpAddrIdxOfSocketDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetBackLogArrayOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_Tcp_BackLogEleType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_EventIndicationPendingOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_FinWait2TimeoutOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_IdleTimeoutShortOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_IssOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_MaxNumListenSocketsOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_MslTimeoutOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetPathMtuChangedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_PathMtuChangedOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_PathMtuNewSizeOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RcvNxtOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RcvWndOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RetransmitTimeoutOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RetryQFillNumOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRstReceivedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RstReceivedOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RtoReloadValueOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndNxtOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndUnaOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndWl1OfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndWl2OfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndWndOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSockIsServerOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SockIsServerOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SockStateNextOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SockStateOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxFlagsOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxMaxSegLenByteOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxMssAgreedOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxNextSendSeqNoOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxOneTimeOptsLenOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxOneTimeOptsOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxOptLenOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqFullyQueuedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxReqFullyQueuedOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxReqSeqNoOfSocketTcpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpTxRequestDescriptorOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_IpTxRequestDescriptorType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_TxReqElemListIdxOfSocketUdpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqIpBufPtrOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, P2VAR(uint8, AUTOMATIC, IPV4_APPL_VAR) Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxRetrQueueOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_DListType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpOooQElement(TcpIp_TcpOooQElementIterType Index, TcpIp_Tcp_RxPreBufEleType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpResetQElement(TcpIp_TcpResetQElementIterType Index, TcpIp_Tcp_RstTxQueueType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRetryQElement(TcpIp_TcpRetryQElementIterType Index, TcpIp_Tcp_TxRetrQueueType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRxBuffer(TcpIp_TcpRxBufferIterType Index, TcpIp_TcpRxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index, TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index, TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpTxBuffer(TcpIp_TcpTxBufferIterType Index, TcpIp_TcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index, TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index, TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index, TcpIp_TxReqElemDataLenByteOfTxReqElemType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqElemTransmittedOfTxReqElem(TcpIp_TxReqElemIterType Index, TcpIp_TxReqElemTransmittedOfTxReqElemType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_FillNumOfTxReqElemListDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_ReadPosOfTxReqElemListDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_SocketUdpDynIdxOfTxReqElemListDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_WritePosOfTxReqElemListDynType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUdpTxRetryQueueElementChain(TcpIp_UdpTxRetryQueueElementChainIterType Index, TcpIp_DListNodeType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUdpTxRetryQueueElements(TcpIp_UdpTxRetryQueueElementsIterType Index, TcpIp_Udp_RetryQueueElementType Value);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUdpTxRetryQueuePoolDesc(TcpIp_UdpTxRetryQueuePoolDescIterType Index, TcpIp_DListDescType Value);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetAddressOfDataInlineFunctionPrototypes  Get Address Of Data Inline Function Prototypes
  \brief  These inline functions can be used to get the data by the address operator.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6PtrType, TCPIP_CODE) TcpIp_GetAddrDefaultAddrV6(TcpIp_DefaultAddrV6IterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferPtrType, TCPIP_CODE) TcpIp_GetAddrDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IcmpV6TxMsgBufferPtrType, TCPIP_CODE) TcpIp_GetAddrIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierPtrType, TCPIP_CODE) TcpIp_GetAddrInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_Ndp_PendingDadNaOfIpV6CtrlDynPtrType, TCPIP_CODE) TcpIp_GetAddrNdp_PendingDadNaOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6DefaultRouterListEntry(TcpIp_IpV6DefaultRouterListEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6DestinationCacheEntry(TcpIp_IpV6DestinationCacheEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrActivePtrType, TCPIP_CODE) TcpIp_GetAddrIpV6MulticastAddrActive(TcpIp_IpV6MulticastAddrActiveIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6NeighborCacheEntry(TcpIp_IpV6NeighborCacheEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6PrefixListEntry(TcpIp_IpV6PrefixListEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressTableEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6SourceAddressTableEntry(TcpIp_IpV6SourceAddressTableEntryIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocSockOfSocketDynPtrType, TCPIP_CODE) TcpIp_GetAddrLocSockOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_RemSockOfSocketDynPtrType, TCPIP_CODE) TcpIp_GetAddrRemSockOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpTxRequestDescriptorOfSocketUdpDynPtrType, TCPIP_CODE) TcpIp_GetAddrIpTxRequestDescriptorOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxRetrQueueOfSocketUdpDynPtrType, TCPIP_CODE) TcpIp_GetAddrTxRetrQueueOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpOooQElementPtrType, TCPIP_CODE) TcpIp_GetAddrTcpOooQElement(TcpIp_TcpOooQElementIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpResetQElementPtrType, TCPIP_CODE) TcpIp_GetAddrTcpResetQElement(TcpIp_TcpResetQElementIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQElementPtrType, TCPIP_CODE) TcpIp_GetAddrTcpRetryQElement(TcpIp_TcpRetryQElementIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferPtrType, TCPIP_CODE) TcpIp_GetAddrTcpRxBuffer(TcpIp_TcpRxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferPtrType, TCPIP_CODE) TcpIp_GetAddrTcpTxBuffer(TcpIp_TcpTxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UdpTxRetryQueueElementChainPtrType, TCPIP_CODE) TcpIp_GetAddrUdpTxRetryQueueElementChain(TcpIp_UdpTxRetryQueueElementChainIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UdpTxRetryQueueElementsPtrType, TCPIP_CODE) TcpIp_GetAddrUdpTxRetryQueueElements(TcpIp_UdpTxRetryQueueElementsIterType Index);
TCPIP_LOCAL_INLINE FUNC(TcpIp_UdpTxRetryQueuePoolDescPtrType, TCPIP_CODE) TcpIp_GetAddrUdpTxRetryQueuePoolDesc(TcpIp_UdpTxRetryQueuePoolDescIterType Index);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCHasInlineFunctionPrototypes  Has Inline Function Prototypes
  \brief  These inline functions can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCodeOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferEndIdxOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferLengthOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferStartIdxOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDirectionOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLengthOfDhcpUserOptionDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDuplicateAddrDetectionFctPtr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfCtrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlIdxOfEthIfCtrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlUsedOfEthIfCtrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6Config(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEchoRequestApiOfIcmpV6Config(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHopLimitOfIcmpV6Config(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6MsgHandlerCbkFctPtr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6TxMsgBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifier(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasAllowLinkMtuReconfigurationOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultHopLimitOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultLinkMtuOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultTrafficClassFlowLabelNboOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpModeOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionUsedOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableDynHopLimitOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnablePathMtuOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfCtrlIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFramePrioDefaultOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIcmpOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIpDestinationOptionsOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIpHopByHopOptionsOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIpRoutingOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumTcpOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumUdpOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifierEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifierStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufDataEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufDataStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrUsedOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressEndIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressStartIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6BcIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6BcUsedOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdpConfigIdxOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPathMtuTimeoutOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasBaseReachableTimeMsOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCtrlPreviousStateOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCtrlStateOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCurHopLimitOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultLinkMtuOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLastBcAddrPtrOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdp_PendingDadNaOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNextRouterProbeIdxOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasReachableTimeMsOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimerMsOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufData(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynIdxOfIpV6EthBufData(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUlTxReqTabIdxOfIpV6EthBufData(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasExtDestAddrValidationEnabledOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDefaultIdxOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDefaultUsedOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynIcmpIdxOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynNdpIdxOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynTcpRstIdxOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynTcpRstUsedOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6IdxOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6UsedOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActiveIdxOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActiveUsedOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6IdxOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActive(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfFramePrioOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFlagsOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHopLimitOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6HdrVersionTcFlNboOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasAddressAssignVariantOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6IdxOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6UsedOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6IdxOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPrefixIsOnLinkOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressTableEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasAssignmentStateOfLocalAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlIdxOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrIdxOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrUsedOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressIdxOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressUsedOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDadTransmitsOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDynamicReachableTimeOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDynamicRetransTimerOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNdpInvNaNcUpdateOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNdpInvNaOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNdpInvNsOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNudOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableOptimisticDadOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableRfc6106DnsslOptOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableRfc6106RdnssOptOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableSlaacDelayOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxRandomFactorOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxRtrSolicitationDelayOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxRtrSolicitationsOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxSlaacDelayOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMinRandomFactorOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMulticastSolicitsOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNudFirstProbeDelayOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRandomReachableTimeOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasReachableTimeOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimerOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRndRtrSolicitationDelayOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRtrSolicitationIntervalOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSlaacMinLifetimeOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUnicastSolicitsOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPhysAddrConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasChgDiscardedFuncPtrOfPhysAddrConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasChgFuncPtrOfPhysAddrConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRandomNumberFctPtr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDefaultAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOption(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfEthIfCtrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6Config(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6TxMsgBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfInterfaceIdentifier(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6Ctrl(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6CtrlDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DefaultRouterListEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DestinationCacheEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6EthBufData(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6General(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddrActive(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6NeighborCacheEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6PrefixListEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddress(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddressTableEntry(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddr(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddrV6(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfNdpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfPhysAddrConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpOooQElement(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpResetQElement(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRetryQElement(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElem(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemList(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElementChain(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElements(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueuePoolDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasListenActiveConnStatOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocSockOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrBindIdxOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRemSockOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketOwnerConfigIdxOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxBufRequestedOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxIpAddrIdxOfSocketDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCopyTxDataDynFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCopyTxDataFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpEventFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalIpAddrAssignmentChgFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxIndicationFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpAcceptedFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpConnectedFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpIpEventFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTlsValidationResultFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxConfirmationFuncPtrOfSocketOwnerConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasBackLogArrayOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEventIndicationPendingOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFinWait2TimeoutOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIdleTimeoutShortOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIssOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxNumListenSocketsOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMslTimeoutOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPathMtuChangedOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPathMtuNewSizeOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRcvNxtOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRcvWndOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRequestedRxBufferSizeOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRequestedTxBufferSizeOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransmitTimeoutOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetryQFillNumOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRstReceivedOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRtoReloadValueOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxBufferIndPosOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxBufferRemIndLenOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndNxtOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndUnaOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndWl1OfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndWl2OfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndWndOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSockIsServerOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSockStateNextOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSockStateOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElementFirstIdxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElementLastIdxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescIdxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescIdxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxFlagsOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxLenByteTxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxMaxSegLenByteOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxMssAgreedOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxNextSendSeqNoOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxOneTimeOptsLenOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxOneTimeOptsOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxOptLenOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqDataBufStartIdxOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqDataLenByteOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqFullyQueuedOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqQueuedLenOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqSeqNoOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxTotNotQueuedLenOfSocketTcpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpTxRequestDescriptorOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListIdxOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqIpBufPtrOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxRetrQueueMaxNumOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxRetrQueueOfSocketUdpDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFinWait2TimeoutOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasKeepAliveIntervalOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasKeepAliveProbesMaxOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasKeepAliveTimeOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMslOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNagleTimeoutOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimeoutMaxOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimeoutOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxMssOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQSizePerSocketAvgOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQSizePerSocketMaxOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQSizeOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTimeToLiveDefaultOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxMssOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUserTimeoutDefCyclesOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUserTimeoutMaxCyclesOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUserTimeoutMinCyclesOfTcpConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQElement(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpResetQElement(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElement(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferEndIdxOfTcpRxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferLengthOfTcpRxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferStartIdxOfTcpRxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFillLevelOfTcpRxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynIdxOfTcpRxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBuffer(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferEndIdxOfTcpTxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferLengthOfTcpTxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferStartIdxOfTcpTxBufferDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFillLevelOfTcpTxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynIdxOfTcpTxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElem(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemDataLenByteOfTxReqElem(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemTransmittedOfTxReqElem(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemList(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemEndIdxOfTxReqElemList(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemLengthOfTxReqElemList(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemStartIdxOfTxReqElemList(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFillNumOfTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasReadPosOfTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketUdpDynIdxOfTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasWritePosOfTxReqElemListDyn(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElementChain(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElements(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueuePoolDesc(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6OfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDuplicateAddrDetectionFctPtrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfCtrlOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6ConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6MsgHandlerCbkFctPtrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6TxMsgBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifierOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufDataOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6GeneralOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActiveOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressTableEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6OfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdpConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPhysAddrConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRandomNumberFctPtrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDefaultAddrV6OfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfEthIfCtrlOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6ConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6TxMsgBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfInterfaceIdentifierOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6CtrlDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6CtrlOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DefaultRouterListEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DestinationCacheEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6EthBufDataOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6GeneralOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddrActiveOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6NeighborCacheEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6PrefixListEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SocketDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddressOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddressTableEntryOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddrOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddrV6OfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfNdpConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfPhysAddrConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketOwnerConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketTcpDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketUdpDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpOooQElementOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpResetQElementOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRetryQElementOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDescDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDescOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDescDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDescOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemListDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemListOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElementChainOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElementsOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueuePoolDescOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketOwnerConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketUdpDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpConfigOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQElementOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpResetQElementOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElementOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListDynOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElementChainOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElementsOfPCConfig(void);
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueuePoolDescOfPCConfig(void);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCIncrementInlineFunctionPrototypes  Increment Inline Function Prototypes
  \brief  These inline functions can be used to increment VAR data with numerical nature.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRxBuffer(TcpIp_TcpRxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpTxBuffer(TcpIp_TcpTxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCDecrementInlineFunctionPrototypes  Decrement Inline Function Prototypes
  \brief  These inline functions can be used to decrement VAR data with numerical nature.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRxBuffer(TcpIp_TcpRxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpTxBuffer(TcpIp_TcpTxBufferIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index);
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



#if !defined(NO_INLINE_FUNCTION_DEFINITIONS)
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL INLINE FUNCTIONS
**********************************************************************************************************************/
#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetDataInlineFunctions  TcpIp Get Data Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to read CONST and VAR data.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(IpV6_AddrType, TCPIP_CODE) TcpIp_GetDefaultAddrV6(TcpIp_DefaultAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetDefaultAddrV6OfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_CodeOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetCodeOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_CodeOfDhcpUserOptionType) TcpIp_GetDhcpUserOptionOfPCConfig()[(Index)].CodeOfDhcpUserOption;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferEndIdxOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBufferEndIdxOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionBufferEndIdxOfDhcpUserOptionType) TcpIp_GetDhcpUserOptionOfPCConfig()[(Index)].DhcpUserOptionBufferEndIdxOfDhcpUserOption;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferLengthOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBufferLengthOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionBufferLengthOfDhcpUserOptionType) TcpIp_GetDhcpUserOptionOfPCConfig()[(Index)].DhcpUserOptionBufferLengthOfDhcpUserOption;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferStartIdxOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBufferStartIdxOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionBufferStartIdxOfDhcpUserOptionType) TcpIp_GetDhcpUserOptionOfPCConfig()[(Index)].DhcpUserOptionBufferStartIdxOfDhcpUserOption;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionDirectionType, TCPIP_CODE) TcpIp_GetDirectionOfDhcpUserOption(TcpIp_DhcpUserOptionIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionDirectionType) TcpIp_GetDhcpUserOptionOfPCConfig()[(Index)].DirectionOfDhcpUserOption;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferType, TCPIP_CODE) TcpIp_GetDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionBufferType) TcpIp_GetDhcpUserOptionBufferOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LengthOfDhcpUserOptionDynType, TCPIP_CODE) TcpIp_GetLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_LengthOfDhcpUserOptionDynType) TcpIp_GetDhcpUserOptionDynOfPCConfig()[(Index)].LengthOfDhcpUserOptionDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DuplicateAddrDetectionCbkType, TCPIP_CODE) TcpIp_GetDuplicateAddrDetectionFctPtr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DuplicateAddrDetectionCbkType) TcpIp_GetDuplicateAddrDetectionFctPtrOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlIdxOfEthIfCtrlType, TCPIP_CODE) TcpIp_GetIpV6CtrlIdxOfEthIfCtrl(TcpIp_EthIfCtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6CtrlIdxOfEthIfCtrlType) TcpIp_GetEthIfCtrlOfPCConfig()[(Index)].IpV6CtrlIdxOfEthIfCtrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EchoRequestApiOfIcmpV6ConfigType, TCPIP_CODE) TcpIp_IsEchoRequestApiOfIcmpV6Config(TcpIp_IcmpV6ConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EchoRequestApiOfIcmpV6ConfigType) ((TcpIp_GetIcmpV6ConfigOfPCConfig()[(Index)].EchoRequestApiOfIcmpV6Config) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HopLimitOfIcmpV6ConfigType, TCPIP_CODE) TcpIp_GetHopLimitOfIcmpV6Config(TcpIp_IcmpV6ConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HopLimitOfIcmpV6ConfigType) TcpIp_GetIcmpV6ConfigOfPCConfig()[(Index)].HopLimitOfIcmpV6Config;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IcmpMultiPartMsgHandlerCbkType, TCPIP_CODE) TcpIp_GetIcmpV6MsgHandlerCbkFctPtr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IcmpMultiPartMsgHandlerCbkType) TcpIp_GetIcmpV6MsgHandlerCbkFctPtrOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IcmpV6TxMsgBufferType, TCPIP_CODE) TcpIp_GetIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IcmpV6TxMsgBufferType) TcpIp_GetIcmpV6TxMsgBufferOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierType, TCPIP_CODE) TcpIp_GetInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_InterfaceIdentifierType) TcpIp_GetInterfaceIdentifierOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultHopLimitOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDefaultHopLimitOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultHopLimitOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].DefaultHopLimitOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultLinkMtuOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDefaultLinkMtuOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultLinkMtuOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].DefaultLinkMtuOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultTrafficClassFlowLabelNboOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDefaultTrafficClassFlowLabelNboOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultTrafficClassFlowLabelNboOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].DefaultTrafficClassFlowLabelNboOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpModeOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDhcpModeOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpModeOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].DhcpModeOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDhcpUserOptionEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].DhcpUserOptionEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetDhcpUserOptionStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].DhcpUserOptionStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EthIfCtrlIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetEthIfCtrlIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EthIfCtrlIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].EthIfCtrlIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_FramePrioDefaultOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetFramePrioDefaultOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_FramePrioDefaultOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].FramePrioDefaultOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetInterfaceIdentifierEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_InterfaceIdentifierEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].InterfaceIdentifierEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetInterfaceIdentifierStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_InterfaceIdentifierStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].InterfaceIdentifierStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DefaultRouterListEntryEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DefaultRouterListEntryStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DestinationCacheEntryEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6DestinationCacheEntryEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DestinationCacheEntryStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6DestinationCacheEntryStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6EthBufDataEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6EthBufDataEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6EthBufDataEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6EthBufDataEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6EthBufDataStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6EthBufDataStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6EthBufDataStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6EthBufDataStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6MulticastAddrEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6MulticastAddrEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6MulticastAddrStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6MulticastAddrStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6NeighborCacheEntryEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6NeighborCacheEntryEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6NeighborCacheEntryStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6NeighborCacheEntryStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntryEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6PrefixListEntryEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6PrefixListEntryEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntryStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6PrefixListEntryStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6PrefixListEntryStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressEndIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6SourceAddressEndIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SourceAddressEndIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6SourceAddressEndIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressStartIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetIpV6SourceAddressStartIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SourceAddressStartIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].IpV6SourceAddressStartIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6BcIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetLocalAddrV6BcIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_LocalAddrV6BcIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].LocalAddrV6BcIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaskedBitsOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].MaskedBitsOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_NdpConfigIdxOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetNdpConfigIdxOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_NdpConfigIdxOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].NdpConfigIdxOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_PathMtuTimeoutOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetPathMtuTimeoutOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_PathMtuTimeoutOfIpV6CtrlType) TcpIp_GetIpV6CtrlOfPCConfig()[(Index)].PathMtuTimeoutOfIpV6Ctrl;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].BaseReachableTimeMsOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_CtrlPreviousStateOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_CtrlPreviousStateOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlPreviousStateOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_CtrlStateOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_CtrlStateOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlStateOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_CurHopLimitOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_CurHopLimitOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CurHopLimitOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultLinkMtuOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultLinkMtuOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].DefaultLinkMtuOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(P2CONST(IpV6_AddrType, AUTOMATIC, IPV4_APPL_VAR), TCPIP_CODE) TcpIp_GetLastBcAddrPtrOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].LastBcAddrPtrOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(IpV6_Ndp_PendingDadNaType, TCPIP_CODE) TcpIp_GetNdp_PendingDadNaOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_PendingDadNaOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(IpV6_TimeType, TCPIP_CODE) TcpIp_GetNdp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_RouterSolicitationTxCountOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(IpV6_ListIdxType, TCPIP_CODE) TcpIp_GetNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (IpV6_ListIdxType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].NextRouterProbeIdxOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_ReachableTimeMsOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_ReachableTimeMsOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].ReachableTimeMsOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimerMsOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RetransTimerMsOfIpV6CtrlDynType) TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].RetransTimerMsOfIpV6CtrlDyn;
}
TCPIP_LOCAL_INLINE FUNC(IpV6_DefaultRouterListEntryType, TCPIP_CODE) TcpIp_GetIpV6DefaultRouterListEntry(TcpIp_IpV6DefaultRouterListEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6DefaultRouterListEntryOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(IpV6_DestinationCacheEntryType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntry(TcpIp_IpV6DestinationCacheEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6DestinationCacheEntryOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType, TCPIP_CODE) TcpIp_GetIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType) TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].IpV6SocketDynIdxOfIpV6EthBufData;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType, TCPIP_CODE) TcpIp_GetUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType) TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].UlTxReqTabIdxOfIpV6EthBufData;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlDefaultIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6CtrlDefaultIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6CtrlDefaultIdxOfIpV6GeneralType) TcpIp_GetIpV6GeneralOfPCConfig()[(Index)].IpV6CtrlDefaultIdxOfIpV6General;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynIcmpIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6SocketDynIcmpIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SocketDynIcmpIdxOfIpV6GeneralType) TcpIp_GetIpV6GeneralOfPCConfig()[(Index)].IpV6SocketDynIcmpIdxOfIpV6General;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynNdpIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6SocketDynNdpIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SocketDynNdpIdxOfIpV6GeneralType) TcpIp_GetIpV6GeneralOfPCConfig()[(Index)].IpV6SocketDynNdpIdxOfIpV6General;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SocketDynTcpRstIdxOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetIpV6SocketDynTcpRstIdxOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SocketDynTcpRstIdxOfIpV6GeneralType) TcpIp_GetIpV6GeneralOfPCConfig()[(Index)].IpV6SocketDynTcpRstIdxOfIpV6General;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaskedBitsOfIpV6GeneralType) TcpIp_GetIpV6GeneralOfPCConfig()[(Index)].MaskedBitsOfIpV6General;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6IdxOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetDefaultAddrV6IdxOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultAddrV6IdxOfIpV6MulticastAddrType) TcpIp_GetIpV6MulticastAddrOfPCConfig()[(Index)].DefaultAddrV6IdxOfIpV6MulticastAddr;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrActiveIdxOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrActiveIdxOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6MulticastAddrActiveIdxOfIpV6MulticastAddrType) TcpIp_GetIpV6MulticastAddrOfPCConfig()[(Index)].IpV6MulticastAddrActiveIdxOfIpV6MulticastAddr;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6IdxOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetLocalAddrV6IdxOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_LocalAddrV6IdxOfIpV6MulticastAddrType) TcpIp_GetIpV6MulticastAddrOfPCConfig()[(Index)].LocalAddrV6IdxOfIpV6MulticastAddr;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaskedBitsOfIpV6MulticastAddrType) TcpIp_GetIpV6MulticastAddrOfPCConfig()[(Index)].MaskedBitsOfIpV6MulticastAddr;
}
TCPIP_LOCAL_INLINE FUNC(IpV6_AddrType, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrActive(TcpIp_IpV6MulticastAddrActiveIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6MulticastAddrActiveOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(IpV6_NeighborCacheEntryType, TCPIP_CODE) TcpIp_GetIpV6NeighborCacheEntry(TcpIp_IpV6NeighborCacheEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6NeighborCacheEntryOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(IpV6_PrefixListEntryType, TCPIP_CODE) TcpIp_GetIpV6PrefixListEntry(TcpIp_IpV6PrefixListEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6PrefixListEntryOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EthIfFramePrioOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EthIfFramePrioOfIpV6SocketDynType) TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].EthIfFramePrioOfIpV6SocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_FlagsOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_FlagsOfIpV6SocketDynType) TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].FlagsOfIpV6SocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HopLimitOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HopLimitOfIpV6SocketDynType) TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].HopLimitOfIpV6SocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType) TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType) TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6HdrVersionTcFlNboOfIpV6SocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_AddressAssignVariantOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetAddressAssignVariantOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_AddressAssignVariantOfIpV6SourceAddressType) TcpIp_GetIpV6SourceAddressOfPCConfig()[(Index)].AddressAssignVariantOfIpV6SourceAddress;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6IdxOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetDefaultAddrV6IdxOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultAddrV6IdxOfIpV6SourceAddressType) TcpIp_GetIpV6SourceAddressOfPCConfig()[(Index)].DefaultAddrV6IdxOfIpV6SourceAddress;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6IdxOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetLocalAddrV6IdxOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_LocalAddrV6IdxOfIpV6SourceAddressType) TcpIp_GetIpV6SourceAddressOfPCConfig()[(Index)].LocalAddrV6IdxOfIpV6SourceAddress;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetMaskedBitsOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaskedBitsOfIpV6SourceAddressType) TcpIp_GetIpV6SourceAddressOfPCConfig()[(Index)].MaskedBitsOfIpV6SourceAddress;
}
TCPIP_LOCAL_INLINE FUNC(IpV6_SourceAddressTableEntryType, TCPIP_CODE) TcpIp_GetIpV6SourceAddressTableEntry(TcpIp_IpV6SourceAddressTableEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetIpV6SourceAddressTableEntryOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_AssignmentStateOfLocalAddrType, TCPIP_CODE) TcpIp_GetAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_AssignmentStateOfLocalAddrType) TcpIp_GetLocalAddrOfPCConfig()[(Index)].AssignmentStateOfLocalAddr;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlIdxOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetIpV6CtrlIdxOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6CtrlIdxOfLocalAddrV6Type) TcpIp_GetLocalAddrV6OfPCConfig()[(Index)].IpV6CtrlIdxOfLocalAddrV6;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrIdxOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetIpV6MulticastAddrIdxOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6MulticastAddrIdxOfLocalAddrV6Type) TcpIp_GetLocalAddrV6OfPCConfig()[(Index)].IpV6MulticastAddrIdxOfLocalAddrV6;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressIdxOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetIpV6SourceAddressIdxOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SourceAddressIdxOfLocalAddrV6Type) TcpIp_GetLocalAddrV6OfPCConfig()[(Index)].IpV6SourceAddressIdxOfLocalAddrV6;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetMaskedBitsOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaskedBitsOfLocalAddrV6Type) TcpIp_GetLocalAddrV6OfPCConfig()[(Index)].MaskedBitsOfLocalAddrV6;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DadTransmitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetDadTransmitsOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DadTransmitsOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].DadTransmitsOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaskedBitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaskedBitsOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaskedBitsOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MaskedBitsOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxRandomFactorOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxRandomFactorOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaxRandomFactorOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MaxRandomFactorOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxRtrSolicitationDelayOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxRtrSolicitationDelayOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaxRtrSolicitationDelayOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MaxRtrSolicitationDelayOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxRtrSolicitationsOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxRtrSolicitationsOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaxRtrSolicitationsOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MaxRtrSolicitationsOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxSlaacDelayOfNdpConfigType, TCPIP_CODE) TcpIp_GetMaxSlaacDelayOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaxSlaacDelayOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MaxSlaacDelayOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MinRandomFactorOfNdpConfigType, TCPIP_CODE) TcpIp_GetMinRandomFactorOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MinRandomFactorOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MinRandomFactorOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MulticastSolicitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetMulticastSolicitsOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MulticastSolicitsOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].MulticastSolicitsOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_NudFirstProbeDelayOfNdpConfigType, TCPIP_CODE) TcpIp_GetNudFirstProbeDelayOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_NudFirstProbeDelayOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].NudFirstProbeDelayOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_ReachableTimeOfNdpConfigType, TCPIP_CODE) TcpIp_GetReachableTimeOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_ReachableTimeOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].ReachableTimeOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimerOfNdpConfigType, TCPIP_CODE) TcpIp_GetRetransTimerOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RetransTimerOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].RetransTimerOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RtrSolicitationIntervalOfNdpConfigType, TCPIP_CODE) TcpIp_GetRtrSolicitationIntervalOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RtrSolicitationIntervalOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].RtrSolicitationIntervalOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SlaacMinLifetimeOfNdpConfigType, TCPIP_CODE) TcpIp_GetSlaacMinLifetimeOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SlaacMinLifetimeOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].SlaacMinLifetimeOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UnicastSolicitsOfNdpConfigType, TCPIP_CODE) TcpIp_GetUnicastSolicitsOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_UnicastSolicitsOfNdpConfigType) TcpIp_GetNdpConfigOfPCConfig()[(Index)].UnicastSolicitsOfNdpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_PhysAddrTableChgDiscardedCbkType, TCPIP_CODE) TcpIp_GetChgDiscardedFuncPtrOfPhysAddrConfig(TcpIp_PhysAddrConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_PhysAddrTableChgDiscardedCbkType) TcpIp_GetPhysAddrConfigOfPCConfig()[(Index)].ChgDiscardedFuncPtrOfPhysAddrConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_PhysAddrTableChgCbkType, TCPIP_CODE) TcpIp_GetChgFuncPtrOfPhysAddrConfig(TcpIp_PhysAddrConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_PhysAddrTableChgCbkType) TcpIp_GetPhysAddrConfigOfPCConfig()[(Index)].ChgFuncPtrOfPhysAddrConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_GetRandomNumberType, TCPIP_CODE) TcpIp_GetRandomNumberFctPtr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_GetRandomNumberType) TcpIp_GetRandomNumberFctPtrOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_ListenActiveConnStatOfSocketDynType, TCPIP_CODE) TcpIp_GetListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_ListenActiveConnStatOfSocketDynType) TcpIp_GetSocketDynOfPCConfig()[(Index)].ListenActiveConnStatOfSocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockAddrBaseType, TCPIP_CODE) TcpIp_GetLocSockOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetSocketDynOfPCConfig()[(Index)].LocSockOfSocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrBindIdxOfSocketDynType, TCPIP_CODE) TcpIp_GetLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_LocalAddrBindIdxOfSocketDynType) TcpIp_GetSocketDynOfPCConfig()[(Index)].LocalAddrBindIdxOfSocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockAddrBaseType, TCPIP_CODE) TcpIp_GetRemSockOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetSocketDynOfPCConfig()[(Index)].RemSockOfSocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerConfigIdxOfSocketDynType, TCPIP_CODE) TcpIp_GetSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerConfigIdxOfSocketDynType) TcpIp_GetSocketDynOfPCConfig()[(Index)].SocketOwnerConfigIdxOfSocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxBufRequestedOfSocketDynType, TCPIP_CODE) TcpIp_IsTxBufRequestedOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxBufRequestedOfSocketDynType) ((TcpIp_GetSocketDynOfPCConfig()[(Index)].TxBufRequestedOfSocketDyn) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxIpAddrIdxOfSocketDynType, TCPIP_CODE) TcpIp_GetTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxIpAddrIdxOfSocketDynType) TcpIp_GetSocketDynOfPCConfig()[(Index)].TxIpAddrIdxOfSocketDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerCopyTxDataDynType, TCPIP_CODE) TcpIp_GetCopyTxDataDynFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerCopyTxDataDynType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].CopyTxDataDynFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerCopyTxDataType, TCPIP_CODE) TcpIp_GetCopyTxDataFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerCopyTxDataType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].CopyTxDataFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerDhcpEventType, TCPIP_CODE) TcpIp_GetDhcpEventFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerDhcpEventType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].DhcpEventFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerLocalIpAddrAssignmentChgType, TCPIP_CODE) TcpIp_GetLocalIpAddrAssignmentChgFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerLocalIpAddrAssignmentChgType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].LocalIpAddrAssignmentChgFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerRxIndicationType, TCPIP_CODE) TcpIp_GetRxIndicationFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerRxIndicationType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].RxIndicationFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTcpAcceptedType, TCPIP_CODE) TcpIp_GetTcpAcceptedFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerTcpAcceptedType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].TcpAcceptedFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTcpConnectedType, TCPIP_CODE) TcpIp_GetTcpConnectedFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerTcpConnectedType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].TcpConnectedFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTcpIpEventType, TCPIP_CODE) TcpIp_GetTcpIpEventFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerTcpIpEventType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].TcpIpEventFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTlsValidationResultType, TCPIP_CODE) TcpIp_GetTlsValidationResultFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerTlsValidationResultType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].TlsValidationResultFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketOwnerTxConfirmationType, TCPIP_CODE) TcpIp_GetTxConfirmationFuncPtrOfSocketOwnerConfig(TcpIp_SocketOwnerConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketOwnerTxConfirmationType) TcpIp_GetSocketOwnerConfigOfPCConfig()[(Index)].TxConfirmationFuncPtrOfSocketOwnerConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_BackLogEleType, TCPIP_CODE) TcpIp_GetBackLogArrayOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].BackLogArrayOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EventIndicationPendingOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EventIndicationPendingOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].EventIndicationPendingOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_FinWait2TimeoutOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_FinWait2TimeoutOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].FinWait2TimeoutOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IdleTimeoutShortOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IdleTimeoutShortOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IdleTimeoutShortOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IssOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IssOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IssOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MaxNumListenSocketsOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MaxNumListenSocketsOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MaxNumListenSocketsOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MslTimeoutOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MslTimeoutOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MslTimeoutOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_PathMtuChangedOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsPathMtuChangedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_PathMtuChangedOfSocketTcpDynType) ((TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].PathMtuChangedOfSocketTcpDyn) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_PathMtuNewSizeOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_PathMtuNewSizeOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].PathMtuNewSizeOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RcvNxtOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RcvNxtOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvNxtOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RcvWndOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RcvWndOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvWndOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedRxBufferSizeOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedTxBufferSizeOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransmitTimeoutOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RetransmitTimeoutOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetransmitTimeoutOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetryQFillNumOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RetryQFillNumOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetryQFillNumOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RstReceivedOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsRstReceivedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RstReceivedOfSocketTcpDynType) ((TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RstReceivedOfSocketTcpDyn) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RtoReloadValueOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RtoReloadValueOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RtoReloadValueOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferIndPosOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferRemIndLenOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndNxtOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SndNxtOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndNxtOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndUnaOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SndUnaOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndUnaOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndWl1OfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SndWl1OfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl1OfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndWl2OfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SndWl2OfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl2OfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SndWndOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SndWndOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWndOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockIsServerOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsSockIsServerOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SockIsServerOfSocketTcpDynType) ((TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockIsServerOfSocketTcpDyn) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockStateNextOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SockStateNextOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateNextOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SockStateOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SockStateOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SocketTcpDynMasterListenSocketIdxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementFirstIdxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementLastIdxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRxBufferDescIdxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpTxBufferDescIdxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxFlagsOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxFlagsOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxFlagsOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxLenByteTxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxMaxSegLenByteOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxMaxSegLenByteOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMaxSegLenByteOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxMssAgreedOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxMssAgreedOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMssAgreedOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxNextSendSeqNoOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxNextSendSeqNoOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxNextSendSeqNoOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxOneTimeOptsLenOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxOneTimeOptsLenOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsLenOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxOneTimeOptsOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxOneTimeOptsOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxOptLenOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxOptLenOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOptLenOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataBufStartIdxOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataLenByteOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqFullyQueuedOfSocketTcpDynType, TCPIP_CODE) TcpIp_IsTxReqFullyQueuedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqFullyQueuedOfSocketTcpDynType) ((TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqFullyQueuedOfSocketTcpDyn) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqQueuedLenOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqSeqNoOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqSeqNoOfSocketTcpDynType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqSeqNoOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxTotNotQueuedLenOfSocketTcpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpTxRequestDescriptorType, TCPIP_CODE) TcpIp_GetIpTxRequestDescriptorOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].IpTxRequestDescriptorOfSocketUdpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemListIdxOfSocketUdpDynType, TCPIP_CODE) TcpIp_GetTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqElemListIdxOfSocketUdpDynType) TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxReqElemListIdxOfSocketUdpDyn;
}
TCPIP_LOCAL_INLINE FUNC(P2VAR(uint8, AUTOMATIC, IPV4_APPL_VAR), TCPIP_CODE) TcpIp_GetTxReqIpBufPtrOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxReqIpBufPtrOfSocketUdpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType, TCPIP_CODE) TcpIp_GetTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType) TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueMaxNumOfSocketUdpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DListType, TCPIP_CODE) TcpIp_GetTxRetrQueueOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueOfSocketUdpDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_FinWait2TimeoutOfTcpConfigType, TCPIP_CODE) TcpIp_GetFinWait2TimeoutOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_FinWait2TimeoutOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].FinWait2TimeoutOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_KeepAliveIntervalOfTcpConfigType, TCPIP_CODE) TcpIp_GetKeepAliveIntervalOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_KeepAliveIntervalOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].KeepAliveIntervalOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_KeepAliveProbesMaxOfTcpConfigType, TCPIP_CODE) TcpIp_GetKeepAliveProbesMaxOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_KeepAliveProbesMaxOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].KeepAliveProbesMaxOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_KeepAliveTimeOfTcpConfigType, TCPIP_CODE) TcpIp_GetKeepAliveTimeOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_KeepAliveTimeOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].KeepAliveTimeOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_MslOfTcpConfigType, TCPIP_CODE) TcpIp_GetMslOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_MslOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].MslOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_NagleTimeoutOfTcpConfigType, TCPIP_CODE) TcpIp_GetNagleTimeoutOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_NagleTimeoutOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].NagleTimeoutOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimeoutMaxOfTcpConfigType, TCPIP_CODE) TcpIp_GetRetransTimeoutMaxOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RetransTimeoutMaxOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].RetransTimeoutMaxOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RetransTimeoutOfTcpConfigType, TCPIP_CODE) TcpIp_GetRetransTimeoutOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RetransTimeoutOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].RetransTimeoutOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RxMssOfTcpConfigType, TCPIP_CODE) TcpIp_GetRxMssOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RxMssOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].RxMssOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpOooQSizePerSocketAvgOfTcpConfigType, TCPIP_CODE) TcpIp_GetTcpOooQSizePerSocketAvgOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpOooQSizePerSocketAvgOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].TcpOooQSizePerSocketAvgOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpOooQSizePerSocketMaxOfTcpConfigType, TCPIP_CODE) TcpIp_GetTcpOooQSizePerSocketMaxOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpOooQSizePerSocketMaxOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].TcpOooQSizePerSocketMaxOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQSizeOfTcpConfigType, TCPIP_CODE) TcpIp_GetTcpRetryQSizeOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRetryQSizeOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].TcpRetryQSizeOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TimeToLiveDefaultOfTcpConfigType, TCPIP_CODE) TcpIp_GetTimeToLiveDefaultOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TimeToLiveDefaultOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].TimeToLiveDefaultOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxMssOfTcpConfigType, TCPIP_CODE) TcpIp_GetTxMssOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxMssOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].TxMssOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UserTimeoutDefCyclesOfTcpConfigType, TCPIP_CODE) TcpIp_GetUserTimeoutDefCyclesOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_UserTimeoutDefCyclesOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].UserTimeoutDefCyclesOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UserTimeoutMaxCyclesOfTcpConfigType, TCPIP_CODE) TcpIp_GetUserTimeoutMaxCyclesOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_UserTimeoutMaxCyclesOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].UserTimeoutMaxCyclesOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UserTimeoutMinCyclesOfTcpConfigType, TCPIP_CODE) TcpIp_GetUserTimeoutMinCyclesOfTcpConfig(TcpIp_TcpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_UserTimeoutMinCyclesOfTcpConfigType) TcpIp_GetTcpConfigOfPCConfig()[(Index)].UserTimeoutMinCyclesOfTcpConfig;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_RxPreBufEleType, TCPIP_CODE) TcpIp_GetTcpOooQElement(TcpIp_TcpOooQElementIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetTcpOooQElementOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_RstTxQueueType, TCPIP_CODE) TcpIp_GetTcpResetQElement(TcpIp_TcpResetQElementIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetTcpResetQElementOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Tcp_TxRetrQueueType, TCPIP_CODE) TcpIp_GetTcpRetryQElement(TcpIp_TcpRetryQElementIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetTcpRetryQElementOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferType, TCPIP_CODE) TcpIp_GetTcpRxBuffer(TcpIp_TcpRxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferType) TcpIp_GetTcpRxBufferOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferEndIdxOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetTcpRxBufferEndIdxOfTcpRxBufferDesc(TcpIp_TcpRxBufferDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferEndIdxOfTcpRxBufferDescType) TcpIp_GetTcpRxBufferDescOfPCConfig()[(Index)].TcpRxBufferEndIdxOfTcpRxBufferDesc;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferLengthOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetTcpRxBufferLengthOfTcpRxBufferDesc(TcpIp_TcpRxBufferDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferLengthOfTcpRxBufferDescType) TcpIp_GetTcpRxBufferDescOfPCConfig()[(Index)].TcpRxBufferLengthOfTcpRxBufferDesc;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferStartIdxOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetTcpRxBufferStartIdxOfTcpRxBufferDesc(TcpIp_TcpRxBufferDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferStartIdxOfTcpRxBufferDescType) TcpIp_GetTcpRxBufferDescOfPCConfig()[(Index)].TcpRxBufferStartIdxOfTcpRxBufferDesc;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferType) TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpRxBufferDescDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType, TCPIP_CODE) TcpIp_GetSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType) TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpRxBufferDescDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType, TCPIP_CODE) TcpIp_GetTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType) TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].TcpRxBufferWriteIdxOfTcpRxBufferDescDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferType, TCPIP_CODE) TcpIp_GetTcpTxBuffer(TcpIp_TcpTxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferType) TcpIp_GetTcpTxBufferOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferEndIdxOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetTcpTxBufferEndIdxOfTcpTxBufferDesc(TcpIp_TcpTxBufferDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferEndIdxOfTcpTxBufferDescType) TcpIp_GetTcpTxBufferDescOfPCConfig()[(Index)].TcpTxBufferEndIdxOfTcpTxBufferDesc;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferLengthOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetTcpTxBufferLengthOfTcpTxBufferDesc(TcpIp_TcpTxBufferDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferLengthOfTcpTxBufferDescType) TcpIp_GetTcpTxBufferDescOfPCConfig()[(Index)].TcpTxBufferLengthOfTcpTxBufferDesc;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferStartIdxOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetTcpTxBufferStartIdxOfTcpTxBufferDesc(TcpIp_TcpTxBufferDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferStartIdxOfTcpTxBufferDescType) TcpIp_GetTcpTxBufferDescOfPCConfig()[(Index)].TcpTxBufferStartIdxOfTcpTxBufferDesc;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpTxBufferDescDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType, TCPIP_CODE) TcpIp_GetSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType) TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpTxBufferDescDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType, TCPIP_CODE) TcpIp_GetTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType) TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].TcpTxBufferWriteIdxOfTcpTxBufferDescDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemDataLenByteOfTxReqElemType, TCPIP_CODE) TcpIp_GetTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqElemDataLenByteOfTxReqElemType) TcpIp_GetTxReqElemOfPCConfig()[(Index)].TxReqElemDataLenByteOfTxReqElem;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemTransmittedOfTxReqElemType, TCPIP_CODE) TcpIp_IsTxReqElemTransmittedOfTxReqElem(TcpIp_TxReqElemIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqElemTransmittedOfTxReqElemType) ((TcpIp_GetTxReqElemOfPCConfig()[(Index)].TxReqElemTransmittedOfTxReqElem) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemEndIdxOfTxReqElemListType, TCPIP_CODE) TcpIp_GetTxReqElemEndIdxOfTxReqElemList(TcpIp_TxReqElemListIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqElemEndIdxOfTxReqElemListType) TcpIp_GetTxReqElemListOfPCConfig()[(Index)].TxReqElemEndIdxOfTxReqElemList;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemLengthOfTxReqElemListType, TCPIP_CODE) TcpIp_GetTxReqElemLengthOfTxReqElemList(TcpIp_TxReqElemListIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqElemLengthOfTxReqElemListType) TcpIp_GetTxReqElemListOfPCConfig()[(Index)].TxReqElemLengthOfTxReqElemList;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxReqElemStartIdxOfTxReqElemListType, TCPIP_CODE) TcpIp_GetTxReqElemStartIdxOfTxReqElemList(TcpIp_TxReqElemListIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TxReqElemStartIdxOfTxReqElemListType) TcpIp_GetTxReqElemListOfPCConfig()[(Index)].TxReqElemStartIdxOfTxReqElemList;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_FillNumOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_FillNumOfTxReqElemListDynType) TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].FillNumOfTxReqElemListDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_ReadPosOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_ReadPosOfTxReqElemListDynType) TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].ReadPosOfTxReqElemListDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SocketUdpDynIdxOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SocketUdpDynIdxOfTxReqElemListDynType) TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].SocketUdpDynIdxOfTxReqElemListDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_WritePosOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_WritePosOfTxReqElemListDynType) TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].WritePosOfTxReqElemListDyn;
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DListNodeType, TCPIP_CODE) TcpIp_GetUdpTxRetryQueueElementChain(TcpIp_UdpTxRetryQueueElementChainIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetUdpTxRetryQueueElementChainOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Udp_RetryQueueElementType, TCPIP_CODE) TcpIp_GetUdpTxRetryQueueElements(TcpIp_UdpTxRetryQueueElementsIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetUdpTxRetryQueueElementsOfPCConfig()[(Index)];
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DListDescType, TCPIP_CODE) TcpIp_GetUdpTxRetryQueuePoolDesc(TcpIp_UdpTxRetryQueuePoolDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return TcpIp_GetUdpTxRetryQueuePoolDescOfPCConfig()[(Index)];
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetBitDataInlineFunctions  TcpIp Get Bit Data Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to read bitcoded data elements.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TCPIP_ENABLEDYNHOPLIMITOFIPV6CTRL_MASK == (TcpIp_GetMaskedBitsOfIpV6Ctrl(Index) & TCPIP_ENABLEDYNHOPLIMITOFIPV6CTRL_MASK));   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsIpV6SocketDynTcpRstUsedOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TCPIP_IPV6SOCKETDYNTCPRSTUSEDOFIPV6GENERAL_MASK == (TcpIp_GetMaskedBitsOfIpV6General(Index) & TCPIP_IPV6SOCKETDYNTCPRSTUSEDOFIPV6GENERAL_MASK));   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsIpV6MulticastAddrActiveUsedOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TCPIP_IPV6MULTICASTADDRACTIVEUSEDOFIPV6MULTICASTADDR_MASK == (TcpIp_GetMaskedBitsOfIpV6MulticastAddr(Index) & TCPIP_IPV6MULTICASTADDRACTIVEUSEDOFIPV6MULTICASTADDR_MASK));   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsPrefixIsOnLinkOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TCPIP_PREFIXISONLINKOFIPV6SOURCEADDRESS_MASK == (TcpIp_GetMaskedBitsOfIpV6SourceAddress(Index) & TCPIP_PREFIXISONLINKOFIPV6SOURCEADDRESS_MASK));   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsEnableNdpInvNaOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TCPIP_ENABLENDPINVNAOFNDPCONFIG_MASK == (TcpIp_GetMaskedBitsOfNdpConfig(Index) & TCPIP_ENABLENDPINVNAOFNDPCONFIG_MASK));   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_IsEnableNudOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TCPIP_ENABLENUDOFNDPCONFIG_MASK == (TcpIp_GetMaskedBitsOfNdpConfig(Index) & TCPIP_ENABLENUDOFNDPCONFIG_MASK));   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetDeduplicatedInlineFunctions  TcpIp Get Deduplicated Inline Functions (PRE_COMPILE)
  \brief  These inline functions  can be used to read deduplicated data elements.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlUsedOfEthIfCtrlType, TCPIP_CODE) TcpIp_IsIpV6CtrlUsedOfEthIfCtrl(TcpIp_EthIfCtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6CtrlUsedOfEthIfCtrlType) (((boolean)(TcpIp_GetIpV6CtrlIdxOfEthIfCtrl(Index) != TCPIP_NO_IPV6CTRLIDXOFETHIFCTRL)) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_AllowLinkMtuReconfigurationOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsAllowLinkMtuReconfigurationOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_AllowLinkMtuReconfigurationOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionUsedOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsDhcpUserOptionUsedOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionUsedOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnablePathMtuOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsEnablePathMtuOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnablePathMtuOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIcmpOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIcmpOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HwChecksumIcmpOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIpDestinationOptionsOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIpDestinationOptionsOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HwChecksumIpDestinationOptionsOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIpHopByHopOptionsOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIpHopByHopOptionsOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HwChecksumIpHopByHopOptionsOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumIpRoutingOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumIpRoutingOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HwChecksumIpRoutingOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumTcpOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumTcpOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HwChecksumTcpOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_HwChecksumUdpOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsHwChecksumUdpOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_HwChecksumUdpOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrUsedOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsIpV6MulticastAddrUsedOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6MulticastAddrUsedOfIpV6CtrlType) (((!(TcpIp_IsEnableDynHopLimitOfIpV6Ctrl(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocalAddrV6BcUsedOfIpV6CtrlType, TCPIP_CODE) TcpIp_IsLocalAddrV6BcUsedOfIpV6Ctrl(TcpIp_IpV6CtrlIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_LocalAddrV6BcUsedOfIpV6CtrlType) (((boolean)(TcpIp_GetLocalAddrV6BcIdxOfIpV6Ctrl(Index) != TCPIP_NO_LOCALADDRV6BCIDXOFIPV6CTRL)) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_ExtDestAddrValidationEnabledOfIpV6GeneralType, TCPIP_CODE) TcpIp_IsExtDestAddrValidationEnabledOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_ExtDestAddrValidationEnabledOfIpV6GeneralType) (((!(TcpIp_IsIpV6SocketDynTcpRstUsedOfIpV6General(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6CtrlDefaultUsedOfIpV6GeneralType, TCPIP_CODE) TcpIp_IsIpV6CtrlDefaultUsedOfIpV6General(TcpIp_IpV6GeneralIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6CtrlDefaultUsedOfIpV6GeneralType) (((!(TcpIp_IsIpV6SocketDynTcpRstUsedOfIpV6General(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6UsedOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_IsDefaultAddrV6UsedOfIpV6MulticastAddr(TcpIp_IpV6MulticastAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultAddrV6UsedOfIpV6MulticastAddrType) (((!(TcpIp_IsIpV6MulticastAddrActiveUsedOfIpV6MulticastAddr(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6UsedOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_IsDefaultAddrV6UsedOfIpV6SourceAddress(TcpIp_IpV6SourceAddressIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DefaultAddrV6UsedOfIpV6SourceAddressType) (((boolean)(TcpIp_GetDefaultAddrV6IdxOfIpV6SourceAddress(Index) != TCPIP_NO_DEFAULTADDRV6IDXOFIPV6SOURCEADDRESS)) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6Type, TCPIP_CODE) TcpIp_IsIpV6MulticastAddrUsedOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6MulticastAddrUsedOfLocalAddrV6Type) (((boolean)(TcpIp_GetIpV6MulticastAddrIdxOfLocalAddrV6(Index) != TCPIP_NO_IPV6MULTICASTADDRIDXOFLOCALADDRV6)) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressUsedOfLocalAddrV6Type, TCPIP_CODE) TcpIp_IsIpV6SourceAddressUsedOfLocalAddrV6(TcpIp_LocalAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IpV6SourceAddressUsedOfLocalAddrV6Type) (((boolean)(TcpIp_GetIpV6SourceAddressIdxOfLocalAddrV6(Index) != TCPIP_NO_IPV6SOURCEADDRESSIDXOFLOCALADDRV6)) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DynamicReachableTimeOfNdpConfigType, TCPIP_CODE) TcpIp_IsDynamicReachableTimeOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DynamicReachableTimeOfNdpConfigType) (((!(TcpIp_IsEnableNdpInvNaOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DynamicRetransTimerOfNdpConfigType, TCPIP_CODE) TcpIp_IsDynamicRetransTimerOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DynamicRetransTimerOfNdpConfigType) (((!(TcpIp_IsEnableNdpInvNaOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableNdpInvNaNcUpdateOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableNdpInvNaNcUpdateOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnableNdpInvNaNcUpdateOfNdpConfigType) (((!(TcpIp_IsEnableNdpInvNaOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableNdpInvNsOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableNdpInvNsOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnableNdpInvNsOfNdpConfigType) (((!(TcpIp_IsEnableNudOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableOptimisticDadOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableOptimisticDadOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnableOptimisticDadOfNdpConfigType) (((!(TcpIp_IsEnableNudOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableRfc6106DnsslOptOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableRfc6106DnsslOptOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnableRfc6106DnsslOptOfNdpConfigType) (((!(TcpIp_IsEnableNudOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableRfc6106RdnssOptOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableRfc6106RdnssOptOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnableRfc6106RdnssOptOfNdpConfigType) (((!(TcpIp_IsEnableNudOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_EnableSlaacDelayOfNdpConfigType, TCPIP_CODE) TcpIp_IsEnableSlaacDelayOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_EnableSlaacDelayOfNdpConfigType) (((!(TcpIp_IsEnableNudOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RandomReachableTimeOfNdpConfigType, TCPIP_CODE) TcpIp_IsRandomReachableTimeOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RandomReachableTimeOfNdpConfigType) (((!(TcpIp_IsEnableNdpInvNaOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RndRtrSolicitationDelayOfNdpConfigType, TCPIP_CODE) TcpIp_IsRndRtrSolicitationDelayOfNdpConfig(TcpIp_NdpConfigIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_RndRtrSolicitationDelayOfNdpConfigType) (((!(TcpIp_IsEnableNdpInvNaOfNdpConfig(Index)))) != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDefaultAddrV6Type, TCPIP_CODE) TcpIp_GetSizeOfDefaultAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfDefaultAddrV6Type) TcpIp_GetSizeOfDefaultAddrV6OfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDhcpUserOptionType, TCPIP_CODE) TcpIp_GetSizeOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfDhcpUserOptionType) TcpIp_GetSizeOfDhcpUserOptionOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDhcpUserOptionBufferType, TCPIP_CODE) TcpIp_GetSizeOfDhcpUserOptionBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfDhcpUserOptionBufferType) TcpIp_GetSizeOfDhcpUserOptionBufferOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfDhcpUserOptionDynType, TCPIP_CODE) TcpIp_GetSizeOfDhcpUserOptionDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfDhcpUserOptionDynType) TcpIp_GetSizeOfDhcpUserOptionDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfEthIfCtrlType, TCPIP_CODE) TcpIp_GetSizeOfEthIfCtrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfEthIfCtrlType) TcpIp_GetSizeOfEthIfCtrlOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIcmpV6ConfigType, TCPIP_CODE) TcpIp_GetSizeOfIcmpV6Config(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIcmpV6ConfigType) TcpIp_GetSizeOfIcmpV6ConfigOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIcmpV6TxMsgBufferType, TCPIP_CODE) TcpIp_GetSizeOfIcmpV6TxMsgBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIcmpV6TxMsgBufferType) TcpIp_GetSizeOfIcmpV6TxMsgBufferOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfInterfaceIdentifierType, TCPIP_CODE) TcpIp_GetSizeOfInterfaceIdentifier(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfInterfaceIdentifierType) TcpIp_GetSizeOfInterfaceIdentifierOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6CtrlType, TCPIP_CODE) TcpIp_GetSizeOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6CtrlType) TcpIp_GetSizeOfIpV6CtrlOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6CtrlDynType, TCPIP_CODE) TcpIp_GetSizeOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6CtrlDynType) TcpIp_GetSizeOfIpV6CtrlDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6DefaultRouterListEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6DefaultRouterListEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6DefaultRouterListEntryType) TcpIp_GetSizeOfIpV6DefaultRouterListEntryOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6DestinationCacheEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6DestinationCacheEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6DestinationCacheEntryType) TcpIp_GetSizeOfIpV6DestinationCacheEntryOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6EthBufDataType, TCPIP_CODE) TcpIp_GetSizeOfIpV6EthBufData(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6EthBufDataType) TcpIp_GetSizeOfIpV6EthBufDataOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6GeneralType, TCPIP_CODE) TcpIp_GetSizeOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6GeneralType) TcpIp_GetSizeOfIpV6GeneralOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6MulticastAddrType, TCPIP_CODE) TcpIp_GetSizeOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6MulticastAddrType) TcpIp_GetSizeOfIpV6MulticastAddrOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6MulticastAddrActiveType, TCPIP_CODE) TcpIp_GetSizeOfIpV6MulticastAddrActive(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6MulticastAddrActiveType) TcpIp_GetSizeOfIpV6MulticastAddrActiveOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6NeighborCacheEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6NeighborCacheEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6NeighborCacheEntryType) TcpIp_GetSizeOfIpV6NeighborCacheEntryOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6PrefixListEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6PrefixListEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6PrefixListEntryType) TcpIp_GetSizeOfIpV6PrefixListEntryOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6SocketDynType, TCPIP_CODE) TcpIp_GetSizeOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6SocketDynType) TcpIp_GetSizeOfIpV6SocketDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6SourceAddressType, TCPIP_CODE) TcpIp_GetSizeOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6SourceAddressType) TcpIp_GetSizeOfIpV6SourceAddressOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfIpV6SourceAddressTableEntryType, TCPIP_CODE) TcpIp_GetSizeOfIpV6SourceAddressTableEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfIpV6SourceAddressTableEntryType) TcpIp_GetSizeOfIpV6SourceAddressTableEntryOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfLocalAddrType, TCPIP_CODE) TcpIp_GetSizeOfLocalAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfLocalAddrType) TcpIp_GetSizeOfLocalAddrOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfLocalAddrV6Type, TCPIP_CODE) TcpIp_GetSizeOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfLocalAddrV6Type) TcpIp_GetSizeOfLocalAddrV6OfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfNdpConfigType, TCPIP_CODE) TcpIp_GetSizeOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfNdpConfigType) TcpIp_GetSizeOfNdpConfigOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfPhysAddrConfigType, TCPIP_CODE) TcpIp_GetSizeOfPhysAddrConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfPhysAddrConfigType) TcpIp_GetSizeOfPhysAddrConfigOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketDynType, TCPIP_CODE) TcpIp_GetSizeOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfSocketDynType) TcpIp_GetSizeOfSocketDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketOwnerConfigType, TCPIP_CODE) TcpIp_GetSizeOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfSocketOwnerConfigType) TcpIp_GetSizeOfSocketOwnerConfigOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketTcpDynType, TCPIP_CODE) TcpIp_GetSizeOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfSocketTcpDynType) TcpIp_GetSizeOfSocketTcpDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfSocketUdpDynType, TCPIP_CODE) TcpIp_GetSizeOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfSocketUdpDynType) TcpIp_GetSizeOfSocketUdpDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpConfigType, TCPIP_CODE) TcpIp_GetSizeOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpConfigType) TcpIp_GetSizeOfTcpConfigOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpOooQElementType, TCPIP_CODE) TcpIp_GetSizeOfTcpOooQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpOooQElementType) TcpIp_GetSizeOfTcpOooQElementOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpResetQElementType, TCPIP_CODE) TcpIp_GetSizeOfTcpResetQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpResetQElementType) TcpIp_GetSizeOfTcpResetQElementOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRetryQElementType, TCPIP_CODE) TcpIp_GetSizeOfTcpRetryQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRetryQElementType) TcpIp_GetSizeOfTcpRetryQElementOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferType, TCPIP_CODE) TcpIp_GetSizeOfTcpRxBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferType) TcpIp_GetSizeOfTcpRxBufferOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferDescType, TCPIP_CODE) TcpIp_GetSizeOfTcpRxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferDescType) TcpIp_GetSizeOfTcpRxBufferDescOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpRxBufferDescDynType, TCPIP_CODE) TcpIp_GetSizeOfTcpRxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpRxBufferDescDynType) TcpIp_GetSizeOfTcpRxBufferDescDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferType, TCPIP_CODE) TcpIp_GetSizeOfTcpTxBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferType) TcpIp_GetSizeOfTcpTxBufferOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferDescType, TCPIP_CODE) TcpIp_GetSizeOfTcpTxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferDescType) TcpIp_GetSizeOfTcpTxBufferDescOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTcpTxBufferDescDynType, TCPIP_CODE) TcpIp_GetSizeOfTcpTxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTcpTxBufferDescDynType) TcpIp_GetSizeOfTcpTxBufferDescDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTxReqElemType, TCPIP_CODE) TcpIp_GetSizeOfTxReqElem(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTxReqElemType) TcpIp_GetSizeOfTxReqElemOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTxReqElemListType, TCPIP_CODE) TcpIp_GetSizeOfTxReqElemList(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTxReqElemListType) TcpIp_GetSizeOfTxReqElemListOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfTxReqElemListDynType, TCPIP_CODE) TcpIp_GetSizeOfTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfTxReqElemListDynType) TcpIp_GetSizeOfTxReqElemListDynOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfUdpTxRetryQueueElementChainType, TCPIP_CODE) TcpIp_GetSizeOfUdpTxRetryQueueElementChain(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfUdpTxRetryQueueElementChainType) TcpIp_GetSizeOfUdpTxRetryQueueElementChainOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfUdpTxRetryQueueElementsType, TCPIP_CODE) TcpIp_GetSizeOfUdpTxRetryQueueElements(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfUdpTxRetryQueueElementsType) TcpIp_GetSizeOfUdpTxRetryQueueElementsOfPCConfig();
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_SizeOfUdpTxRetryQueuePoolDescType, TCPIP_CODE) TcpIp_GetSizeOfUdpTxRetryQueuePoolDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_SizeOfUdpTxRetryQueuePoolDescType) TcpIp_GetSizeOfUdpTxRetryQueuePoolDescOfPCConfig();
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCSetDataInlineFunctions  TcpIp Set Data Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to write data.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index, TcpIp_DhcpUserOptionBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetDhcpUserOptionBufferOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index, TcpIp_LengthOfDhcpUserOptionDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetDhcpUserOptionDynOfPCConfig()[(Index)].LengthOfDhcpUserOptionDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index, TcpIp_IcmpV6TxMsgBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIcmpV6TxMsgBufferOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index, TcpIp_InterfaceIdentifierType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetInterfaceIdentifierOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_BaseReachableTimeMsOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].BaseReachableTimeMsOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_CtrlPreviousStateOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlPreviousStateOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_CtrlStateOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlStateOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_CurHopLimitOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CurHopLimitOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_DefaultLinkMtuOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].DefaultLinkMtuOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLastBcAddrPtrOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, P2CONST(IpV6_AddrType, AUTOMATIC, IPV4_APPL_VAR) Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].LastBcAddrPtrOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNdp_PendingDadNaOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, IpV6_Ndp_PendingDadNaType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_PendingDadNaOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNdp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, IpV6_TimeType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_Ndp_RouterSolicitationTxCountOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_RouterSolicitationTxCountOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, IpV6_ListIdxType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].NextRouterProbeIdxOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_ReachableTimeMsOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].ReachableTimeMsOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index, TcpIp_RetransTimerMsOfIpV6CtrlDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].RetransTimerMsOfIpV6CtrlDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DefaultRouterListEntry(TcpIp_IpV6DefaultRouterListEntryIterType Index, IpV6_DefaultRouterListEntryType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6DefaultRouterListEntryOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DestinationCacheEntry(TcpIp_IpV6DestinationCacheEntryIterType Index, IpV6_DestinationCacheEntryType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6DestinationCacheEntryOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index, TcpIp_IpV6SocketDynIdxOfIpV6EthBufDataType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].IpV6SocketDynIdxOfIpV6EthBufData = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index, TcpIp_UlTxReqTabIdxOfIpV6EthBufDataType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].UlTxReqTabIdxOfIpV6EthBufData = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6MulticastAddrActive(TcpIp_IpV6MulticastAddrActiveIterType Index, IpV6_AddrType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6MulticastAddrActiveOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6NeighborCacheEntry(TcpIp_IpV6NeighborCacheEntryIterType Index, IpV6_NeighborCacheEntryType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6NeighborCacheEntryOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6PrefixListEntry(TcpIp_IpV6PrefixListEntryIterType Index, IpV6_PrefixListEntryType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6PrefixListEntryOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_EthIfFramePrioOfIpV6SocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].EthIfFramePrioOfIpV6SocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_FlagsOfIpV6SocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].FlagsOfIpV6SocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_HopLimitOfIpV6SocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].HopLimitOfIpV6SocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_IpV6DestinationCacheEntryHintIdxOfIpV6SocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index, TcpIp_IpV6HdrVersionTcFlNboOfIpV6SocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6HdrVersionTcFlNboOfIpV6SocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpV6SourceAddressTableEntry(TcpIp_IpV6SourceAddressTableEntryIterType Index, IpV6_SourceAddressTableEntryType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SourceAddressTableEntryOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index, TcpIp_AssignmentStateOfLocalAddrType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetLocalAddrOfPCConfig()[(Index)].AssignmentStateOfLocalAddr = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_ListenActiveConnStatOfSocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].ListenActiveConnStatOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLocSockOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_SockAddrBaseType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].LocSockOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_LocalAddrBindIdxOfSocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].LocalAddrBindIdxOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRemSockOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_SockAddrBaseType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].RemSockOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_SocketOwnerConfigIdxOfSocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].SocketOwnerConfigIdxOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxBufRequestedOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_TxBufRequestedOfSocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].TxBufRequestedOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index, TcpIp_TxIpAddrIdxOfSocketDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].TxIpAddrIdxOfSocketDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetBackLogArrayOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_Tcp_BackLogEleType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].BackLogArrayOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_EventIndicationPendingOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].EventIndicationPendingOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_FinWait2TimeoutOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].FinWait2TimeoutOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_IdleTimeoutShortOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IdleTimeoutShortOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_IssOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IssOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_MaxNumListenSocketsOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MaxNumListenSocketsOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_MslTimeoutOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MslTimeoutOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetPathMtuChangedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_PathMtuChangedOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].PathMtuChangedOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_PathMtuNewSizeOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].PathMtuNewSizeOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RcvNxtOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvNxtOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RcvWndOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvWndOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedRxBufferSizeOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedTxBufferSizeOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RetransmitTimeoutOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetransmitTimeoutOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RetryQFillNumOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetryQFillNumOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRstReceivedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RstReceivedOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RstReceivedOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_RtoReloadValueOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RtoReloadValueOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferIndPosOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferRemIndLenOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndNxtOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndNxtOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndUnaOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndUnaOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndWl1OfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl1OfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndWl2OfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl2OfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SndWndOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWndOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSockIsServerOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SockIsServerOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockIsServerOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SockStateNextOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateNextOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SockStateOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SocketTcpDynMasterListenSocketIdxOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SocketTcpDynMasterListenSocketIdxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpRetryQElementFirstIdxOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementFirstIdxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpRetryQElementLastIdxOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementLastIdxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpRxBufferDescIdxOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRxBufferDescIdxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TcpTxBufferDescIdxOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpTxBufferDescIdxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxFlagsOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxFlagsOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxLenByteTxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxMaxSegLenByteOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMaxSegLenByteOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxMssAgreedOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMssAgreedOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxNextSendSeqNoOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxNextSendSeqNoOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxOneTimeOptsLenOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsLenOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxOneTimeOptsOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxOptLenOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOptLenOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataBufStartIdxOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataLenByteOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqFullyQueuedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxReqFullyQueuedOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqFullyQueuedOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqQueuedLenOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_TxReqSeqNoOfSocketTcpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqSeqNoOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxTotNotQueuedLenOfSocketTcpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetIpTxRequestDescriptorOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_IpTxRequestDescriptorType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].IpTxRequestDescriptorOfSocketUdpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_TxReqElemListIdxOfSocketUdpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxReqElemListIdxOfSocketUdpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqIpBufPtrOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, P2VAR(uint8, AUTOMATIC, IPV4_APPL_VAR) Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxReqIpBufPtrOfSocketUdpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_TxRetrQueueMaxNumOfSocketUdpDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueMaxNumOfSocketUdpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxRetrQueueOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index, TcpIp_DListType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueOfSocketUdpDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpOooQElement(TcpIp_TcpOooQElementIterType Index, TcpIp_Tcp_RxPreBufEleType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpOooQElementOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpResetQElement(TcpIp_TcpResetQElementIterType Index, TcpIp_Tcp_RstTxQueueType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpResetQElementOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRetryQElement(TcpIp_TcpRetryQElementIterType Index, TcpIp_Tcp_TxRetrQueueType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRetryQElementOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRxBuffer(TcpIp_TcpRxBufferIterType Index, TcpIp_TcpRxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index, TcpIp_SizeOfTcpRxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpRxBufferDescDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index, TcpIp_SocketTcpDynIdxOfTcpRxBufferDescDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpRxBufferDescDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index, TcpIp_TcpRxBufferWriteIdxOfTcpRxBufferDescDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].TcpRxBufferWriteIdxOfTcpRxBufferDescDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpTxBuffer(TcpIp_TcpTxBufferIterType Index, TcpIp_TcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index, TcpIp_SizeOfTcpTxBufferType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpTxBufferDescDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index, TcpIp_SocketTcpDynIdxOfTcpTxBufferDescDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpTxBufferDescDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index, TcpIp_TcpTxBufferWriteIdxOfTcpTxBufferDescDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].TcpTxBufferWriteIdxOfTcpTxBufferDescDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index, TcpIp_TxReqElemDataLenByteOfTxReqElemType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemOfPCConfig()[(Index)].TxReqElemDataLenByteOfTxReqElem = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetTxReqElemTransmittedOfTxReqElem(TcpIp_TxReqElemIterType Index, TcpIp_TxReqElemTransmittedOfTxReqElemType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemOfPCConfig()[(Index)].TxReqElemTransmittedOfTxReqElem = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_FillNumOfTxReqElemListDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].FillNumOfTxReqElemListDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_ReadPosOfTxReqElemListDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].ReadPosOfTxReqElemListDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_SocketUdpDynIdxOfTxReqElemListDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].SocketUdpDynIdxOfTxReqElemListDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index, TcpIp_WritePosOfTxReqElemListDynType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].WritePosOfTxReqElemListDyn = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUdpTxRetryQueueElementChain(TcpIp_UdpTxRetryQueueElementChainIterType Index, TcpIp_DListNodeType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetUdpTxRetryQueueElementChainOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUdpTxRetryQueueElements(TcpIp_UdpTxRetryQueueElementsIterType Index, TcpIp_Udp_RetryQueueElementType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetUdpTxRetryQueueElementsOfPCConfig()[(Index)] = (Value);
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_SetUdpTxRetryQueuePoolDesc(TcpIp_UdpTxRetryQueuePoolDescIterType Index, TcpIp_DListDescType Value)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetUdpTxRetryQueuePoolDescOfPCConfig()[(Index)] = (Value);
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCGetAddressOfDataInlineFunctions  TcpIp Get Address Of Data Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to get the data by the address operator.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(TcpIp_DefaultAddrV6PtrType, TCPIP_CODE) TcpIp_GetAddrDefaultAddrV6(TcpIp_DefaultAddrV6IterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetDefaultAddrV6OfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_DhcpUserOptionBufferPtrType, TCPIP_CODE) TcpIp_GetAddrDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_DhcpUserOptionBufferPtrType) (&(TcpIp_GetDhcpUserOptionBufferOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IcmpV6TxMsgBufferPtrType, TCPIP_CODE) TcpIp_GetAddrIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_IcmpV6TxMsgBufferPtrType) (&(TcpIp_GetIcmpV6TxMsgBufferOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_InterfaceIdentifierPtrType, TCPIP_CODE) TcpIp_GetAddrInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_InterfaceIdentifierPtrType) (&(TcpIp_GetInterfaceIdentifierOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_Ndp_PendingDadNaOfIpV6CtrlDynPtrType, TCPIP_CODE) TcpIp_GetAddrNdp_PendingDadNaOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_PendingDadNaOfIpV6CtrlDyn));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DefaultRouterListEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6DefaultRouterListEntry(TcpIp_IpV6DefaultRouterListEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6DefaultRouterListEntryOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6DestinationCacheEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6DestinationCacheEntry(TcpIp_IpV6DestinationCacheEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6DestinationCacheEntryOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6MulticastAddrActivePtrType, TCPIP_CODE) TcpIp_GetAddrIpV6MulticastAddrActive(TcpIp_IpV6MulticastAddrActiveIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6MulticastAddrActiveOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6NeighborCacheEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6NeighborCacheEntry(TcpIp_IpV6NeighborCacheEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6NeighborCacheEntryOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6PrefixListEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6PrefixListEntry(TcpIp_IpV6PrefixListEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6PrefixListEntryOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpV6SourceAddressTableEntryPtrType, TCPIP_CODE) TcpIp_GetAddrIpV6SourceAddressTableEntry(TcpIp_IpV6SourceAddressTableEntryIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetIpV6SourceAddressTableEntryOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_LocSockOfSocketDynPtrType, TCPIP_CODE) TcpIp_GetAddrLocSockOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetSocketDynOfPCConfig()[(Index)].LocSockOfSocketDyn));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_RemSockOfSocketDynPtrType, TCPIP_CODE) TcpIp_GetAddrRemSockOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetSocketDynOfPCConfig()[(Index)].RemSockOfSocketDyn));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_IpTxRequestDescriptorOfSocketUdpDynPtrType, TCPIP_CODE) TcpIp_GetAddrIpTxRequestDescriptorOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].IpTxRequestDescriptorOfSocketUdpDyn));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TxRetrQueueOfSocketUdpDynPtrType, TCPIP_CODE) TcpIp_GetAddrTxRetrQueueOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueOfSocketUdpDyn));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpOooQElementPtrType, TCPIP_CODE) TcpIp_GetAddrTcpOooQElement(TcpIp_TcpOooQElementIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetTcpOooQElementOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpResetQElementPtrType, TCPIP_CODE) TcpIp_GetAddrTcpResetQElement(TcpIp_TcpResetQElementIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetTcpResetQElementOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRetryQElementPtrType, TCPIP_CODE) TcpIp_GetAddrTcpRetryQElement(TcpIp_TcpRetryQElementIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetTcpRetryQElementOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpRxBufferPtrType, TCPIP_CODE) TcpIp_GetAddrTcpRxBuffer(TcpIp_TcpRxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpRxBufferPtrType) (&(TcpIp_GetTcpRxBufferOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_TcpTxBufferPtrType, TCPIP_CODE) TcpIp_GetAddrTcpTxBuffer(TcpIp_TcpTxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (TcpIp_TcpTxBufferPtrType) (&(TcpIp_GetTcpTxBufferOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UdpTxRetryQueueElementChainPtrType, TCPIP_CODE) TcpIp_GetAddrUdpTxRetryQueueElementChain(TcpIp_UdpTxRetryQueueElementChainIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetUdpTxRetryQueueElementChainOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UdpTxRetryQueueElementsPtrType, TCPIP_CODE) TcpIp_GetAddrUdpTxRetryQueueElements(TcpIp_UdpTxRetryQueueElementsIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetUdpTxRetryQueueElementsOfPCConfig()[(Index)]));
}
TCPIP_LOCAL_INLINE FUNC(TcpIp_UdpTxRetryQueuePoolDescPtrType, TCPIP_CODE) TcpIp_GetAddrUdpTxRetryQueuePoolDesc(TcpIp_UdpTxRetryQueuePoolDescIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (&(TcpIp_GetUdpTxRetryQueuePoolDescOfPCConfig()[(Index)]));
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCHasInlineFunctions  TcpIp Has Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetDefaultAddrV6OfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetDhcpUserOptionOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCodeOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferEndIdxOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferLengthOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferStartIdxOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDirectionOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetDhcpUserOptionBufferOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetDhcpUserOptionDynOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLengthOfDhcpUserOptionDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDuplicateAddrDetectionFctPtr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfCtrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlIdxOfEthIfCtrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlUsedOfEthIfCtrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6Config(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEchoRequestApiOfIcmpV6Config(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHopLimitOfIcmpV6Config(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6MsgHandlerCbkFctPtr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6TxMsgBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifier(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasAllowLinkMtuReconfigurationOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultHopLimitOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultLinkMtuOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultTrafficClassFlowLabelNboOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpModeOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionUsedOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableDynHopLimitOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnablePathMtuOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfCtrlIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFramePrioDefaultOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIcmpOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIpDestinationOptionsOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIpHopByHopOptionsOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumIpRoutingOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumTcpOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHwChecksumUdpOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifierEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifierStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufDataEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufDataStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrUsedOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressEndIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressStartIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6BcIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6BcUsedOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdpConfigIdxOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPathMtuTimeoutOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasBaseReachableTimeMsOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCtrlPreviousStateOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCtrlStateOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCurHopLimitOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultLinkMtuOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLastBcAddrPtrOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdp_PendingDadNaOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdp_RouterSolicitationNextTxTimeOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNextRouterProbeIdxOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasReachableTimeMsOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimerMsOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufData(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynIdxOfIpV6EthBufData(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUlTxReqTabIdxOfIpV6EthBufData(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasExtDestAddrValidationEnabledOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDefaultIdxOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDefaultUsedOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynIcmpIdxOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynNdpIdxOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynTcpRstIdxOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynTcpRstUsedOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetIpV6MulticastAddrOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6IdxOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6UsedOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActiveIdxOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActiveUsedOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6IdxOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActive(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetIpV6MulticastAddrActiveOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfFramePrioOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFlagsOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasHopLimitOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6HdrVersionTcFlNboOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasAddressAssignVariantOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6IdxOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6UsedOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6IdxOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPrefixIsOnLinkOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressTableEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasAssignmentStateOfLocalAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlIdxOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrIdxOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrUsedOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressIdxOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressUsedOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDadTransmitsOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDynamicReachableTimeOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDynamicRetransTimerOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNdpInvNaNcUpdateOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNdpInvNaOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNdpInvNsOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableNudOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableOptimisticDadOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableRfc6106DnsslOptOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableRfc6106RdnssOptOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEnableSlaacDelayOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaskedBitsOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxRandomFactorOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxRtrSolicitationDelayOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxRtrSolicitationsOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxSlaacDelayOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMinRandomFactorOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMulticastSolicitsOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNudFirstProbeDelayOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRandomReachableTimeOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasReachableTimeOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimerOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRndRtrSolicitationDelayOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRtrSolicitationIntervalOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSlaacMinLifetimeOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUnicastSolicitsOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPhysAddrConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetPhysAddrConfigOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasChgDiscardedFuncPtrOfPhysAddrConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasChgFuncPtrOfPhysAddrConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRandomNumberFctPtr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDefaultAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOption(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfEthIfCtrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6Config(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6TxMsgBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfInterfaceIdentifier(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6Ctrl(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6CtrlDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DefaultRouterListEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DestinationCacheEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6EthBufData(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6General(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddrActive(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6NeighborCacheEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6PrefixListEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddress(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddressTableEntry(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddr(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddrV6(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfNdpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfPhysAddrConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpOooQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpResetQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRetryQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElem(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemList(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElementChain(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElements(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueuePoolDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasListenActiveConnStatOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocSockOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrBindIdxOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRemSockOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketOwnerConfigIdxOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxBufRequestedOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxIpAddrIdxOfSocketDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCopyTxDataDynFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasCopyTxDataFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpEventFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalIpAddrAssignmentChgFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxIndicationFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpAcceptedFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpConnectedFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpIpEventFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTlsValidationResultFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxConfirmationFuncPtrOfSocketOwnerConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasBackLogArrayOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEventIndicationPendingOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFinWait2TimeoutOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIdleTimeoutShortOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIssOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMaxNumListenSocketsOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMslTimeoutOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPathMtuChangedOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPathMtuNewSizeOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRcvNxtOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRcvWndOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRequestedRxBufferSizeOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRequestedTxBufferSizeOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransmitTimeoutOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetryQFillNumOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRstReceivedOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRtoReloadValueOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxBufferIndPosOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxBufferRemIndLenOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndNxtOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndUnaOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndWl1OfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndWl2OfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSndWndOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSockIsServerOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSockStateNextOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSockStateOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElementFirstIdxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElementLastIdxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescIdxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescIdxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxFlagsOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxLenByteTxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxMaxSegLenByteOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxMssAgreedOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxNextSendSeqNoOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxOneTimeOptsLenOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxOneTimeOptsOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxOptLenOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqDataBufStartIdxOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqDataLenByteOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqFullyQueuedOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqQueuedLenOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqSeqNoOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxTotNotQueuedLenOfSocketTcpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpTxRequestDescriptorOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListIdxOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqIpBufPtrOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxRetrQueueMaxNumOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxRetrQueueOfSocketUdpDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFinWait2TimeoutOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasKeepAliveIntervalOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasKeepAliveProbesMaxOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasKeepAliveTimeOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasMslOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNagleTimeoutOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimeoutMaxOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRetransTimeoutOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRxMssOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQSizePerSocketAvgOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQSizePerSocketMaxOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQSizeOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTimeToLiveDefaultOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxMssOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUserTimeoutDefCyclesOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUserTimeoutMaxCyclesOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUserTimeoutMinCyclesOfTcpConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetTcpOooQElementOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpResetQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElement(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferEndIdxOfTcpRxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferLengthOfTcpRxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferStartIdxOfTcpRxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFillLevelOfTcpRxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynIdxOfTcpRxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBuffer(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferEndIdxOfTcpTxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferLengthOfTcpTxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferStartIdxOfTcpTxBufferDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFillLevelOfTcpTxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynIdxOfTcpTxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElem(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetTxReqElemOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemDataLenByteOfTxReqElem(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemTransmittedOfTxReqElem(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemList(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetTxReqElemListOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemEndIdxOfTxReqElemList(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemLengthOfTxReqElemList(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemStartIdxOfTxReqElemList(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetTxReqElemListDynOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasFillNumOfTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasReadPosOfTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketUdpDynIdxOfTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasWritePosOfTxReqElemListDyn(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElementChain(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetUdpTxRetryQueueElementChainOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElements(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetUdpTxRetryQueueElementsOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueuePoolDesc(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TcpIp_GetUdpTxRetryQueuePoolDescOfPCConfig() != NULL_PTR);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDefaultAddrV6OfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDhcpUserOptionOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasDuplicateAddrDetectionFctPtrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasEthIfCtrlOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6ConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6MsgHandlerCbkFctPtrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIcmpV6TxMsgBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasInterfaceIdentifierOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6CtrlOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DefaultRouterListEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6DestinationCacheEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6EthBufDataOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6GeneralOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrActiveOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6MulticastAddrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6NeighborCacheEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6PrefixListEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SocketDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasIpV6SourceAddressTableEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasLocalAddrV6OfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasNdpConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasPhysAddrConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasRandomNumberFctPtrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDefaultAddrV6OfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfDhcpUserOptionOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfEthIfCtrlOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6ConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIcmpV6TxMsgBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfInterfaceIdentifierOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6CtrlDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6CtrlOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DefaultRouterListEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6DestinationCacheEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6EthBufDataOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6GeneralOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddrActiveOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6MulticastAddrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6NeighborCacheEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6PrefixListEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SocketDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddressOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfIpV6SourceAddressTableEntryOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddrOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfLocalAddrV6OfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfNdpConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfPhysAddrConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketOwnerConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketTcpDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfSocketUdpDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpOooQElementOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpResetQElementOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRetryQElementOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDescDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferDescOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpRxBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDescDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferDescOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTcpTxBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemListDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemListOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfTxReqElemOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElementChainOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueueElementsOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSizeOfUdpTxRetryQueuePoolDescOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketOwnerConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketTcpDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasSocketUdpDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpConfigOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpOooQElementOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpResetQElementOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRetryQElementOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferDescOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpRxBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferDescOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTcpTxBufferOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListDynOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemListOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasTxReqElemOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElementChainOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueueElementsOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
TCPIP_LOCAL_INLINE FUNC(boolean, TCPIP_CODE) TcpIp_HasUdpTxRetryQueuePoolDescOfPCConfig(void)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  return (boolean) (TRUE != FALSE);   /* PRQA S 4304 */  /* MD_MSR_AutosarBoolean */
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCIncrementInlineFunctions  TcpIp Increment Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to increment VAR data with numerical nature.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetDhcpUserOptionBufferOfPCConfig()[(Index)]++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetDhcpUserOptionDynOfPCConfig()[(Index)].LengthOfDhcpUserOptionDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIcmpV6TxMsgBufferOfPCConfig()[(Index)]++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetInterfaceIdentifierOfPCConfig()[(Index)]++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].BaseReachableTimeMsOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlPreviousStateOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlStateOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CurHopLimitOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].DefaultLinkMtuOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_RouterSolicitationTxCountOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].NextRouterProbeIdxOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].ReachableTimeMsOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].RetransTimerMsOfIpV6CtrlDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].IpV6SocketDynIdxOfIpV6EthBufData++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].UlTxReqTabIdxOfIpV6EthBufData++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].EthIfFramePrioOfIpV6SocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].FlagsOfIpV6SocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].HopLimitOfIpV6SocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6HdrVersionTcFlNboOfIpV6SocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetLocalAddrOfPCConfig()[(Index)].AssignmentStateOfLocalAddr++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].ListenActiveConnStatOfSocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].LocalAddrBindIdxOfSocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].SocketOwnerConfigIdxOfSocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].TxIpAddrIdxOfSocketDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].EventIndicationPendingOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].FinWait2TimeoutOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IdleTimeoutShortOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IssOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MaxNumListenSocketsOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MslTimeoutOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].PathMtuNewSizeOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvNxtOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvWndOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedRxBufferSizeOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedTxBufferSizeOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetransmitTimeoutOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetryQFillNumOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RtoReloadValueOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferIndPosOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferRemIndLenOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndNxtOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndUnaOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl1OfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl2OfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWndOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateNextOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SocketTcpDynMasterListenSocketIdxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementFirstIdxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementLastIdxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRxBufferDescIdxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpTxBufferDescIdxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxFlagsOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxLenByteTxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMaxSegLenByteOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMssAgreedOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxNextSendSeqNoOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsLenOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOptLenOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataBufStartIdxOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataLenByteOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqQueuedLenOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqSeqNoOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxTotNotQueuedLenOfSocketTcpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxReqElemListIdxOfSocketUdpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueMaxNumOfSocketUdpDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRxBuffer(TcpIp_TcpRxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferOfPCConfig()[(Index)]++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpRxBufferDescDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpRxBufferDescDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].TcpRxBufferWriteIdxOfTcpRxBufferDescDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpTxBuffer(TcpIp_TcpTxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferOfPCConfig()[(Index)]++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpTxBufferDescDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpTxBufferDescDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].TcpTxBufferWriteIdxOfTcpTxBufferDescDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemOfPCConfig()[(Index)].TxReqElemDataLenByteOfTxReqElem++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].FillNumOfTxReqElemListDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].ReadPosOfTxReqElemListDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].SocketUdpDynIdxOfTxReqElemListDyn++;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_IncWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].WritePosOfTxReqElemListDyn++;
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

#define TCPIP_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/** 
  \defgroup  TcpIpPCDecrementInlineFunctions  TcpIp Decrement Inline Functions (PRE_COMPILE)
  \brief  These inline functions can be used to decrement VAR data with numerical nature.
  \{
*/ 
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecDhcpUserOptionBuffer(TcpIp_DhcpUserOptionBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetDhcpUserOptionBufferOfPCConfig()[(Index)]--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecLengthOfDhcpUserOptionDyn(TcpIp_DhcpUserOptionDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetDhcpUserOptionDynOfPCConfig()[(Index)].LengthOfDhcpUserOptionDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIcmpV6TxMsgBuffer(TcpIp_IcmpV6TxMsgBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIcmpV6TxMsgBufferOfPCConfig()[(Index)]--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecInterfaceIdentifier(TcpIp_InterfaceIdentifierIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetInterfaceIdentifierOfPCConfig()[(Index)]--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecBaseReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].BaseReachableTimeMsOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecCtrlPreviousStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlPreviousStateOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecCtrlStateOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CtrlStateOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecCurHopLimitOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].CurHopLimitOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecDefaultLinkMtuOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].DefaultLinkMtuOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DefaultRouterListEntryValidEndIdxOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryValidEndIdxOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6NeighborCacheEntryValidEndIdxOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].IpV6PrefixListEntryValidEndIdxOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecNdp_RouterSolicitationTxCountOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].Ndp_RouterSolicitationTxCountOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecNextRouterProbeIdxOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].NextRouterProbeIdxOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecReachableTimeMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].ReachableTimeMsOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRetransTimerMsOfIpV6CtrlDyn(TcpIp_IpV6CtrlDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6CtrlDynOfPCConfig()[(Index)].RetransTimerMsOfIpV6CtrlDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6SocketDynIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].IpV6SocketDynIdxOfIpV6EthBufData--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecUlTxReqTabIdxOfIpV6EthBufData(TcpIp_IpV6EthBufDataIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6EthBufDataOfPCConfig()[(Index)].UlTxReqTabIdxOfIpV6EthBufData--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecEthIfFramePrioOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].EthIfFramePrioOfIpV6SocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFlagsOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].FlagsOfIpV6SocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecHopLimitOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].HopLimitOfIpV6SocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6DestinationCacheEntryHintIdxOfIpV6SocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIpV6HdrVersionTcFlNboOfIpV6SocketDyn(TcpIp_IpV6SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetIpV6SocketDynOfPCConfig()[(Index)].IpV6HdrVersionTcFlNboOfIpV6SocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecAssignmentStateOfLocalAddr(TcpIp_LocalAddrIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetLocalAddrOfPCConfig()[(Index)].AssignmentStateOfLocalAddr--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecListenActiveConnStatOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].ListenActiveConnStatOfSocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecLocalAddrBindIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].LocalAddrBindIdxOfSocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketOwnerConfigIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].SocketOwnerConfigIdxOfSocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxIpAddrIdxOfSocketDyn(TcpIp_SocketDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketDynOfPCConfig()[(Index)].TxIpAddrIdxOfSocketDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecEventIndicationPendingOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].EventIndicationPendingOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFinWait2TimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].FinWait2TimeoutOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIdleTimeoutShortOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IdleTimeoutShortOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecIssOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].IssOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecMaxNumListenSocketsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MaxNumListenSocketsOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecMslTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].MslTimeoutOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecPathMtuNewSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].PathMtuNewSizeOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRcvNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvNxtOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRcvWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RcvWndOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRequestedRxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedRxBufferSizeOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRequestedTxBufferSizeOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RequestedTxBufferSizeOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRetransmitTimeoutOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetransmitTimeoutOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRetryQFillNumOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RetryQFillNumOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRtoReloadValueOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RtoReloadValueOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRxBufferIndPosOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferIndPosOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecRxBufferRemIndLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].RxBufferRemIndLenOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndNxtOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndNxtOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndUnaOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndUnaOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndWl1OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl1OfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndWl2OfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWl2OfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSndWndOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SndWndOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSockStateNextOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateNextOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSockStateOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SockStateOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketTcpDynMasterListenSocketIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].SocketTcpDynMasterListenSocketIdxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRetryQElementFirstIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementFirstIdxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRetryQElementLastIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRetryQElementLastIdxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpRxBufferDescIdxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpTxBufferDescIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TcpTxBufferDescIdxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxFlagsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxFlagsOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxLenByteTxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxLenByteTxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxMaxSegLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMaxSegLenByteOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxMssAgreedOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxMssAgreedOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxNextSendSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxNextSendSeqNoOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxOneTimeOptsLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsLenOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxOneTimeOptsOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOneTimeOptsOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxOptLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxOptLenOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqDataBufStartIdxOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataBufStartIdxOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqDataLenByteOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqDataLenByteOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqQueuedLenOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqSeqNoOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxReqSeqNoOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxTotNotQueuedLenOfSocketTcpDyn(TcpIp_SocketTcpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketTcpDynOfPCConfig()[(Index)].TxTotNotQueuedLenOfSocketTcpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqElemListIdxOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxReqElemListIdxOfSocketUdpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxRetrQueueMaxNumOfSocketUdpDyn(TcpIp_SocketUdpDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetSocketUdpDynOfPCConfig()[(Index)].TxRetrQueueMaxNumOfSocketUdpDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRxBuffer(TcpIp_TcpRxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferOfPCConfig()[(Index)]--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFillLevelOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpRxBufferDescDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketTcpDynIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpRxBufferDescDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpRxBufferWriteIdxOfTcpRxBufferDescDyn(TcpIp_TcpRxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpRxBufferDescDynOfPCConfig()[(Index)].TcpRxBufferWriteIdxOfTcpRxBufferDescDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpTxBuffer(TcpIp_TcpTxBufferIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferOfPCConfig()[(Index)]--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFillLevelOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].FillLevelOfTcpTxBufferDescDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketTcpDynIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].SocketTcpDynIdxOfTcpTxBufferDescDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTcpTxBufferWriteIdxOfTcpTxBufferDescDyn(TcpIp_TcpTxBufferDescDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTcpTxBufferDescDynOfPCConfig()[(Index)].TcpTxBufferWriteIdxOfTcpTxBufferDescDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecTxReqElemDataLenByteOfTxReqElem(TcpIp_TxReqElemIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemOfPCConfig()[(Index)].TxReqElemDataLenByteOfTxReqElem--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecFillNumOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].FillNumOfTxReqElemListDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecReadPosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].ReadPosOfTxReqElemListDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecSocketUdpDynIdxOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].SocketUdpDynIdxOfTxReqElemListDyn--;
}
TCPIP_LOCAL_INLINE FUNC(void, TCPIP_CODE) TcpIp_DecWritePosOfTxReqElemListDyn(TcpIp_TxReqElemListDynIterType Index)  /* PRQA S 3219 */  /* MD_MSR_Unreachable */
{
  TcpIp_GetTxReqElemListDynOfPCConfig()[(Index)].WritePosOfTxReqElemListDyn--;
}
/** 
  \}
*/ 

#define TCPIP_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


#endif

/**********************************************************************************************************************
 *  USER FUNCTIONS
 *********************************************************************************************************************/
 #define TCPIP_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/* TcpIp_<Up>GetSocket ----------------------------------------------------- */
extern FUNC(Std_ReturnType, TCPIP_CODE) TcpIp_SccGetSocket(
    TcpIp_DomainType                                      Domain,
    TcpIp_ProtocolType                                    Protocol,
    P2VAR(TcpIp_SocketIdType, AUTOMATIC, TCPIP_APPL_DATA) SocketIdPtr);
 

#define TCPIP_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           <USERBLOCK TCPIP>
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           </USERBLOCK>
 *********************************************************************************************************************/


#endif  /* TCPIP_LCFG_H */

/**********************************************************************************************************************
 *  END OF FILE: TcpIp_Lcfg.h
 *********************************************************************************************************************/
