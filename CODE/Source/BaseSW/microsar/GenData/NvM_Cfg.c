/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: NvM
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: NvM_Cfg.c
 *   Generation Time: 2020-11-23 11:39:42
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

	
/* PRQA S 5087 MemMap */ /* MD_MSR_MemMap */
    
/**********************************************************************************************************************
 *  MODULE SWITCH
 *********************************************************************************************************************/
/* this switch enables the header file(s) to hide some parts. */
#define NVM_CFG_SOURCE

/* multiple inclusion protection */
#define NVM_H_

/* Required for RTE ROM block definitions */
#define RTE_MICROSAR_PIM_EXPORT

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"

/* This tag will only be replaced, if one or more callbacks via Service Ports had been configured */


/**********************************************************************************************************************
 *  MODULE HEADER INCLUDES
 *********************************************************************************************************************/
/* only includes the public part of config */
#include "NvM_Cfg.h"
#include "NvM_PrivateCfg.h"

#include "BswM_NvM.h"

/**********************************************************************************************************************
 *  VERSION CHECKS
 *********************************************************************************************************************/
#if ((NVM_CFG_MAJOR_VERSION != (5u)) \
        || (NVM_CFG_MINOR_VERSION != (13u)))
# error "Version numbers of NvM_Cfg.c and NvM_Cfg.h are inconsistent!"
#endif

/* include list of the callback definitions */
#include "Dem_Cbk.h" 
#include "Scc.h" 
#include "Eth_30_Ar7000.h" 


/* include configured file declaring or defining resource (un)locking service(s) */
#include "SchM_NvM.h"

/**********************************************************************************************************************
 *  PUBLIC CONSTANTS
 *********************************************************************************************************************/
#define NVM_START_SEC_CONST_16
#include "MemMap.h"

/* maximum number of bytes to be processed in one crc calculation step */
CONST(uint16, NVM_CONFIG_CONST) NvM_NoOfCrcBytes_u16 = 64u;

/* constant holding Crc queue size value */
CONST(uint16, NVM_PRIVATE_CONST) NvM_CrcQueueSize_u16 = NVM_TOTAL_NUM_OF_NVRAM_BLOCKS;

#define NVM_STOP_SEC_CONST_16
#include "MemMap.h"

/* 8Bit Data section containing the CRC buffers, as well as the internal buffer */
#define NVM_START_SEC_VAR_NOINIT_8
#include "MemMap.h"

static VAR(uint8, NVM_PRIVATE_DATA) NvMConfigBlock_RamBlock_au8[4u];

#if ((NVM_CRC_INT_BUFFER == STD_ON) || (NVM_REPAIR_REDUNDANT_BLOCKS_API == STD_ON))
static VAR(uint8, NVM_PRIVATE_DATA) EthMACAdressDataBlock_Crc[2uL]; 
static VAR(uint8, NVM_PRIVATE_DATA) SCCSessionIDNvMBlockDescriptor_Crc[2uL]; 
static VAR(uint8, NVM_PRIVATE_DATA) CpHwAbsAIM_AIMVoltRefNvBlockNeed_Crc[2uL]; 


/* create the internal buffer of size NVM_INTERNAL_BUFFER_LENGTH */
VAR(uint8, NVM_PRIVATE_DATA) NvM_InternalBuffer_au8[NVM_INTERNAL_BUFFER_LENGTH]; /* PRQA S 1533 */ /* MD_NvM_Cfg_8.9_InternalBuffer */
#endif




#define NVM_STOP_SEC_VAR_NOINIT_8
#include "MemMap.h"

#define NVM_START_SEC_CONST_DESCRIPTOR_TABLE
#include "MemMap.h"

CONST(NvM_BlockIdType, NVM_PUBLIC_CONST) NvM_NoOfBlockIds_t = NVM_TOTAL_NUM_OF_NVRAM_BLOCKS;

CONST(NvM_CompiledConfigIdType, NVM_PUBLIC_CONST) NvM_CompiledConfigId_t = {(uint16)NVM_COMPILED_CONFIG_ID}; /* PRQA S 0759 */ /* MD_MSR_Union */
 
 
/* block descriptor table that holds the static configuration parameters of the RAM, ROM and NVBlocks.
* This table has to be adjusted according to the configuration of the NVManager.
*/

CONST(NvM_BlockDescriptorType, NVM_CONFIG_CONST) NvM_BlockDescriptorTable_at[NVM_TOTAL_NUM_OF_NVRAM_BLOCKS] =
    {
      { /*  MultiBlockRequest  */ 
        NULL_PTR /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0001u /*  NV block Base number (defined by FEE/EA)  */ , 
        0U /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        0U /*  NvMNvBlockNVRAMDataLength  */ , 
        0u /*  NvMNvBlockNum  */ , 
        255u /*  NvMBlockJobPriority  */ , 
        0u /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        0u /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  NvMConfigBlock  */ 
        (NvM_RamAddressType)NvMConfigBlock_RamBlock_au8 /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0002u /*  NV block Base number (defined by FEE/EA)  */ , 
        2u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        2u /*  NvMNvBlockNVRAMDataLength  */ , 
        2u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_REDUNDANT /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_CRC_16_ON /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_OFF |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemStatusDataBlock  */ 
        (NvM_RamAddressType)&Dem_Cfg_StatusData /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x000Cu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_StatusData) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_StatusData) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock3  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_3 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0020u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_3) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_3) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock2  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_2 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x001Eu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_2) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_2) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock1  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_1 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x001Cu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_1) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_1) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock0  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_0 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x001Au /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_0) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_0) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemAdminDataBlock  */ 
        (NvM_RamAddressType)&Dem_Cfg_AdminData /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0018u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_AdminData) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_AdminData) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock8  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_8 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x002Au /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_8) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_8) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApPCOM_NvPCOMBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpApPCOM_NvPCOMBlockNeed_MirrorBlock /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0014u /*  NV block Base number (defined by FEE/EA)  */ , 
        16u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        16u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock9  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_9 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x002Cu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_9) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_9) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApWUM_NvWUMBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpApWUM_NvWUMBlockNeed_MirrorBlock /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Rte_CtApWUM_NvWUMBlockNeed_DefaultValue /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0012u /*  NV block Base number (defined by FEE/EA)  */ , 
        1u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        1u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_OFF |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock10  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_10 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x002Eu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_10) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_10) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  EthMACAdressDataBlock  */ 
        (NvM_RamAddressType)&Eth_30_Ar7000_VPhysSrcAddr_0 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Eth_30_Ar7000_VPhysSrcAddrRomDefault_0 /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        EthMACAdressDataBlock_Crc /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x000Eu /*  NV block Base number (defined by FEE/EA)  */ , 
        6u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        6u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_CRC_16_ON /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  SCCSessionIDNvMBlockDescriptor  */ 
        (NvM_RamAddressType)&Scc_SessionIDNvm /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Scc_SessionIDNvmRomDefault /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        SCCSessionIDNvMBlockDescriptor_Crc /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0010u /*  NV block Base number (defined by FEE/EA)  */ , 
        9u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        9u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_CRC_16_ON /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock11  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_11 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0030u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_11) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_11) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock12  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_12 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0032u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_12) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_12) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock13  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_13 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0034u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_13) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_13) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock14  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_14 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0036u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_14) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_14) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock15  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_15 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0038u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_15) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_15) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock16  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_16 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x003Au /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_16) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_16) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock17  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_17 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x003Cu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_17) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_17) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock18  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_18 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x003Eu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_18) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_18) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock19  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_19 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0040u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_19) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_19) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock4  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_4 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0022u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_4) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_4) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock20  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_20 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0042u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_20) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_20) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock21  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_21 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0044u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_21) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_21) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock22  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_22 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0046u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_22) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_22) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock5  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_5 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0024u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_5) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_5) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock23  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_23 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0048u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_23) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_23) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock24  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_24 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x004Au /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_24) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_24) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock25  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_25 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0054u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_25) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_25) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock6  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_6 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0026u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_6) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_6) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock26  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_26 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x004Cu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_26) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_26) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock27  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_27 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x004Eu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_27) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_27) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock28  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_28 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0050u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_28) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_28) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock7  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_7 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0028u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_7) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_7) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock29  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_29 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0052u /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_29) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_29) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock30  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_30 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x005Cu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_30) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_30) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  DemPrimaryDataBlock31  */ 
        (NvM_RamAddressType)&Dem_Cfg_PrimaryEntry_31 /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Dem_Cfg_MemoryEntryInit /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        Dem_NvM_JobFinished /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x005Eu /*  NV block Base number (defined by FEE/EA)  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_31) /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        (uint16)sizeof(Dem_Cfg_PrimaryEntry_31) /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        0u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_ON
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApJDD_JDDNvBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpApJDD_PimJDD_NvMRamMirror /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0004u /*  NV block Base number (defined by FEE/EA)  */ , 
        2000u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        2000u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApMSC_NvMSCBlockNeedPowerLatchFlag  */ 
        (NvM_RamAddressType)&Rte_CpApMSC_PimMSC_PowerLatchFlag /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0006u /*  NV block Base number (defined by FEE/EA)  */ , 
        1u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        1u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_OFF |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApPSH_NvPSHBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpApPSH_NvPSHBlockNeed_MirrorBlock /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0008u /*  NV block Base number (defined by FEE/EA)  */ , 
        4u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        4u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApPXL_PXLNvBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpApPXL_PimPXL_NvMRamMirror /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x000Au /*  NV block Base number (defined by FEE/EA)  */ , 
        1u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        1u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApCPT_CPTNvBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpApCPT_PimCPT_NvMRamMirror /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0016u /*  NV block Base number (defined by FEE/EA)  */ , 
        1u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        1u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApDGN_DGN_EOL_NVM_Needs  */ 
        (NvM_RamAddressType)&Rte_CpApDGN_PimDGN_EOL_NVM_RamMirror /*  NvMRamBlockDataAddress  */ , 
        NULL_PTR /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0056u /*  NV block Base number (defined by FEE/EA)  */ , 
        76u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        76u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_OFF |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpApPLT_PLTNvMNeed  */ 
        (NvM_RamAddressType)&Rte_CpApPLT_PimPlantModeTestUTPlugin /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Rte_CtApPLT_PLTNvMNeed_DefaultValue /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        NULL_PTR /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x0058u /*  NV block Base number (defined by FEE/EA)  */ , 
        2u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        2u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_USE_CRC_OFF /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_ON |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }, 
      { /*  CpHwAbsAIM_AIMVoltRefNvBlockNeed  */ 
        (NvM_RamAddressType)&Rte_CpHwAbsAIM_AIMVoltRefNvBlockNeed_MirrorBlock /*  NvMRamBlockDataAddress  */ , 
        (NvM_RomAddressType)&Rte_CtHwAbsAIM_AIMVoltRefNvBlockNeed_DefaultValue /*  NvMRomBlockDataAddress  */ , 
        NULL_PTR /*  NvMInitBlockCallback  */ , 
        NULL_PTR /*  NvMInitBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMSingleBlockCallback  */ , 
        NULL_PTR /*  NvMSingleBlockCallback (extended)  */ , 
        NULL_PTR /*  NvMReadRamBlockFromNvCallback  */ , 
        NULL_PTR /*  NvMWriteRamBlockToNvCallback  */ , 
        NULL_PTR /*  NvMBlockPreWriteTransformCallback  */ , 
        NULL_PTR /*  NvMBlockPostReadTransformCallback  */ , 
        CpHwAbsAIM_AIMVoltRefNvBlockNeed_Crc /*  RamBlockCRC data buffer (defined by NvM)  */ , 
        NULL_PTR /*  CRCCompMechanism CRC data (defined by NvM)  */ , 
        0x005Au /*  NV block Base number (defined by FEE/EA)  */ , 
        3u /*  NvMNvBlockLength  */ , 
        0u /*  CsmJobArrayIndex  */ , 
        3u /*  NvMNvBlockNVRAMDataLength  */ , 
        1u /*  NvMNvBlockNum  */ , 
        127u /*  NvMBlockJobPriority  */ , 
        MEMIF_Fee /*  Device Id (defined by MemIf)  */ , 
        NVM_BLOCK_NATIVE /*  NvMBlockManagementType  */ , 
        NVM_BLOCK_CRC_16_ON /*  NvMBlockCrcType  */ , 
        (
NVM_CALC_RAM_BLOCK_CRC_OFF |
NVM_BLOCK_WRITE_PROT_OFF |
NVM_BLOCK_WRITE_BLOCK_ONCE_OFF |
NVM_RESISTANT_TO_CHANGED_SW_ON |
NVM_SELECT_BLOCK_FOR_READALL_ON |
NVM_SELECT_BLOCK_FOR_WRITEALL_OFF |
NVM_CBK_DURING_READALL_OFF
) /*  Flags  */ , 
        STD_OFF /*  NvMBswMBlockStatusInformation  */ 
      }
    };

/* Permanent RAM and ROM block length checks - compile time (only available for blocks with enabled length check */

/* PRQA S 3494, 3213, 1755 BlockLengthChecks */ /* MD_NvM_Cfg_14.3, MD_NvM_Cfg_2.4 */

/* How does it work:
 * data length = sizeof(ramBlock - CrcLength 
 *     - CRC internal buffer enabled: CRC length == 0, RAM blocks store only data, CRC is handles internally
 *     - CRC internal buffer disabled: CRC length is the number of CRC bytes, for blocks without CRC the length == 0
 *     - for ROM blocks the CRC does not matter
 * Data length has to be > or < or == to configured NvM block length, depending on configuration (see above). 
 * In case the lengths do not match a bitfield with length -1 will be created and shall cause a compiler error.
 * The compiler error shall mark the line with invalid bitfield (bitfield length == -1) - the line includes all required information:
 *     - Block_ + NvM block name
 *     - length error description
 *     - RAM block name, CRC length and configured NvM block length
 */

typedef unsigned int NvM_LengthCheck;
 
/* Block Length Check and Automatic Block Length enabled: error if sizeof RAM block is greater than the configured block length */
#define SizeOfRamBlockGreaterThanConfiguredLength(ramBlock, crcLength, blockLength) (((sizeof(ramBlock) - (crcLength)) > (blockLength)) ? -1 : 1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/* Block Length Check and Strict Length Check enabled: error if the sizeof RAM block does not match the configured block length */
#define SizeOfRamBlockDoesNotMatchConfiguredLength(ramBlock, crcLength, blockLength) (((sizeof(ramBlock) - (crcLength)) != (blockLength)) ? -1 : 1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/* Block Length Check enabled and Strict Length Check disabled: error if the sizeof RAM block is less than the configured block length */
#define SizeOfRamBlockLessThanConfiguredLength(ramBlock, crcLength, blockLength) (((sizeof(ramBlock) - (crcLength)) < (blockLength)) ? -1 : 1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/* RAM block length checks */
struct PermanentRamBlockLengthChecks {
  NvM_LengthCheck Block_DemStatusDataBlock : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_StatusData, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock3 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_3, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock2 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_2, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock1 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_1, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock0 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_0, 0u, 128u);
  NvM_LengthCheck Block_DemAdminDataBlock : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_AdminData, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock8 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_8, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock9 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_9, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock10 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_10, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock11 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_11, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock12 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_12, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock13 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_13, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock14 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_14, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock15 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_15, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock16 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_16, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock17 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_17, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock18 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_18, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock19 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_19, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock4 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_4, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock20 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_20, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock21 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_21, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock22 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_22, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock5 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_5, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock23 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_23, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock24 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_24, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock25 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_25, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock6 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_6, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock26 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_26, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock27 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_27, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock28 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_28, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock7 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_7, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock29 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_29, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock30 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_30, 0u, 128u);
  NvM_LengthCheck Block_DemPrimaryDataBlock31 : SizeOfRamBlockGreaterThanConfiguredLength(Dem_Cfg_PrimaryEntry_31, 0u, 128u);
};

/* Block Length Check and Automatic Block Length enabled: error if sizeof ROM block is less than sizeof RAM block */
#define SizeOfRomBlockLessThanSizeOfRamBlock(romBlock, ramBlock) ((sizeof(romBlock) < sizeof(ramBlock)) ? -1 : 1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/* Block Length Check and Strict Length Check enabled: error if the sizeof ROM block does not match the configured block length */
#define SizeOfRomBlockDoesNotMatchConfiguredLength(romBlock, blockLength) ((sizeof(romBlock) != (blockLength)) ? -1 : 1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
/* Block Length Check enabled, Strict Length disabled: error if the sizeof ROM block is less than the configured block length */
#define SizeOfRomBlockLessThanConfiguredLength(romBlock, blockLength) ((sizeof(romBlock) < (blockLength)) ? -1 : 1) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/* ROM block length checks */
struct PermRomBlockLengthCheck {
  NvM_LengthCheck Block_DemPrimaryDataBlock3 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_3);
  NvM_LengthCheck Block_DemPrimaryDataBlock2 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_2);
  NvM_LengthCheck Block_DemPrimaryDataBlock1 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_1);
  NvM_LengthCheck Block_DemPrimaryDataBlock0 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_0);
  NvM_LengthCheck Block_DemPrimaryDataBlock8 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_8);
  NvM_LengthCheck Block_DemPrimaryDataBlock9 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_9);
  NvM_LengthCheck Block_DemPrimaryDataBlock10 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_10);
  NvM_LengthCheck Block_DemPrimaryDataBlock11 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_11);
  NvM_LengthCheck Block_DemPrimaryDataBlock12 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_12);
  NvM_LengthCheck Block_DemPrimaryDataBlock13 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_13);
  NvM_LengthCheck Block_DemPrimaryDataBlock14 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_14);
  NvM_LengthCheck Block_DemPrimaryDataBlock15 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_15);
  NvM_LengthCheck Block_DemPrimaryDataBlock16 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_16);
  NvM_LengthCheck Block_DemPrimaryDataBlock17 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_17);
  NvM_LengthCheck Block_DemPrimaryDataBlock18 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_18);
  NvM_LengthCheck Block_DemPrimaryDataBlock19 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_19);
  NvM_LengthCheck Block_DemPrimaryDataBlock4 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_4);
  NvM_LengthCheck Block_DemPrimaryDataBlock20 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_20);
  NvM_LengthCheck Block_DemPrimaryDataBlock21 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_21);
  NvM_LengthCheck Block_DemPrimaryDataBlock22 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_22);
  NvM_LengthCheck Block_DemPrimaryDataBlock5 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_5);
  NvM_LengthCheck Block_DemPrimaryDataBlock23 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_23);
  NvM_LengthCheck Block_DemPrimaryDataBlock24 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_24);
  NvM_LengthCheck Block_DemPrimaryDataBlock25 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_25);
  NvM_LengthCheck Block_DemPrimaryDataBlock6 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_6);
  NvM_LengthCheck Block_DemPrimaryDataBlock26 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_26);
  NvM_LengthCheck Block_DemPrimaryDataBlock27 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_27);
  NvM_LengthCheck Block_DemPrimaryDataBlock28 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_28);
  NvM_LengthCheck Block_DemPrimaryDataBlock7 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_7);
  NvM_LengthCheck Block_DemPrimaryDataBlock29 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_29);
  NvM_LengthCheck Block_DemPrimaryDataBlock30 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_30);
  NvM_LengthCheck Block_DemPrimaryDataBlock31 : SizeOfRomBlockLessThanSizeOfRamBlock(Dem_Cfg_MemoryEntryInit, Dem_Cfg_PrimaryEntry_31);
};

/* PRQA L:BlockLengthChecks */

/* Permanent RAM and ROM block length checks - END */

#define NVM_STOP_SEC_CONST_DESCRIPTOR_TABLE
#include "MemMap.h"

#define NVM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"

#if(NVM_API_CONFIG_CLASS != NVM_API_CONFIG_CLASS_1)
/* Job Queue used for normal and high prio jobs */
VAR(NvM_QueueEntryType, NVM_PRIVATE_DATA) NvM_JobQueue_at[NVM_SIZE_STANDARD_JOB_QUEUE + NVM_SIZE_IMMEDIATE_JOB_QUEUE];
#endif

#define NVM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "MemMap.h"

#define NVM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "MemMap.h"

/* array of RAM Block Management areas, defined to be usable in block descriptor table */
VAR(NvM_RamMngmtAreaType, NVM_CONFIG_DATA) NvM_BlockMngmtArea_at[NVM_TOTAL_NUM_OF_NVRAM_BLOCKS];

/* management area for DCM blocks */
VAR(NvM_RamMngmtAreaType, NVM_CONFIG_DATA) NvM_DcmBlockMngmt_t;

#define NVM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "MemMap.h"

#define NVM_START_SEC_CODE
#include "MemMap.h"

/**********************************************************************************************************************
*  NvM_EnterCriticalSection
**********************************************************************************************************************/
/*!
 * \internal
 *  - #10 enter SchM exclusive area for NvM 
 * \endinternal
 */
FUNC(void, NVM_PRIVATE_CODE) NvM_EnterCriticalSection(void)
{
  /* do what ever was defined to do for locking the resources */
  SchM_Enter_NvM_NVM_EXCLUSIVE_AREA_0();
}

/**********************************************************************************************************************
*  NvM_ExitCriticalSection
**********************************************************************************************************************/
/*!
 * \internal
 *  - #10 exit SchM exclusive area for NvM 
 * \endinternal
 */
FUNC(void, NVM_PRIVATE_CODE) NvM_ExitCriticalSection(void)
{
  /* do what ever was defined to do for unlocking the resources */
  SchM_Exit_NvM_NVM_EXCLUSIVE_AREA_0();
}

/* PRQA S 3453 1 */ /* MD_MSR_FctLikeMacro */
#define NvM_invokeMultiBlockMode(serv, res) BswM_NvM_CurrentJobMode((serv),(res)) /*  if NvMBswMMultiBlockJobStatusInformation is TRUE  */
/* PRQA S 3453 1 */ /* MD_MSR_FctLikeMacro */
#define NvM_invokeMultiCbk(serv, res)   /*  if Multi Block Callback is configured  */

/**********************************************************************************************************************
*  NvM_MultiBlockCbk
**********************************************************************************************************************/
/*!
 * \internal
 *  - #10 invoke the BSWM notification if any is configured
 *  - #20 in case the given job result isn't set to pending, invoke the multi block job end notification
 * \endinternal
 */
/* PRQA S 3206 1 */ /* MD_NvM_Cfg_2.7 */
FUNC(void, NVM_PRIVATE_CODE) NvM_MultiBlockCbk(NvM_ServiceIdType ServiceId, NvM_RequestResultType JobResult)
{
  /* The complete function body is designed to be optimized away by the compiler, if it is not needed    *
   * If the used macro is empty, the compiler may decide to remove code because it would contain         *
   * empty execution blocks (it does not matter whether conditions were TRUE or FALSE                    */
  NvM_invokeMultiBlockMode(ServiceId, JobResult);

  if(JobResult != NVM_REQ_PENDING)
  {
    NvM_invokeMultiCbk(ServiceId, JobResult);
  }   
}

#define NVM_STOP_SEC_CODE
#include "MemMap.h"

/* PRQA L:MemMap */

/* Justification for module specific MISRA deviations:

MD_NvM_Cfg_2.4
Reason: NvM provides compile time block length checks via structures with bitfields with positive or negative length -
        the negative length lead to compiler errors. It is possible to use == or even >= check, if only one is used,
        the other one will never be used. The macros are always available. The created structures will never be used by NvM.
Risk: No risk.
Prevention: No prevention.

MD_NvM_Cfg_2.7:
Reason: The function NvM_MultiBlockCbk gets all needed parameters to invoke the BSWM and multi block job end callback.
        If both are disabled, the function is empty and does nothing - the passed parameters remain unused.
Risk: No risk.
Prevention: No prevention.

MD_NvM_Cfg_8.9_InternalBuffer:
Reason: NvM uses an internal buffer for explicit synchronization, in internal CRC buffer use case and for repair redundant blocks.
        Depending on configuration all, one or even none of the uses is enabled - therefore sometimes the internal buffer is
        used only once.
Risk: No risk.
Prevention: No prevention.

MD_NvM_Cfg_8.11:
Reason: Array of unknown size is used in order to reduce dependencies.
Risk: In case the array size shall be determined it would be incorrect.
Prevention: No prevention.

MD_NvM_Cfg_14.3:
Reason: NvM provides compile time block length checks via bitfields with positive or negative length - the negative length
        lead to compiler errors. With valid configuration (all block length are configured correctly), all checks are false.
Risk: No risk.
Prevention: No prevention. If needed the compile time checks can be disabled via configuration.

 */

/**********************************************************************************************************************
 *  END OF FILE: NvM_Cfg.c
 *********************************************************************************************************************/


