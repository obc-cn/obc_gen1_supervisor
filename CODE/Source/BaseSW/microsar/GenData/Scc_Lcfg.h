/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Scc
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Scc_Lcfg.h
 *   Generation Time: 2020-11-23 11:39:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined(SCC_LCFG_H)
#define SCC_LCFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Scc_Types.h"
#include "Scc_Cfg.h"

 
/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
 *********************************************************************************************************************/



/* 16-bit variables */ 
#define SCC_START_SEC_VAR_NOINIT_16BIT_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define SCC_STOP_SEC_VAR_NOINIT_16BIT_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"



/* unspecified variables */
#define SCC_START_SEC_VAR_NOINIT_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define SCC_STOP_SEC_VAR_NOINIT_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"



/* 16-bit constants */ 
#define SCC_START_SEC_CONST_16BIT_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define SCC_STOP_SEC_CONST_16BIT_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"




/* unspecified constants */
#define SCC_START_SEC_CONST_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

extern CONST(Scc_ConfigType, SCC_CONST) Scc_Config;
 
#define SCC_STOP_SEC_CONST_UNSPECIFIED_NVM
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/
#define SCC_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"


#define SCC_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_19.1 */
#include "MemMap.h"

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/
/* PRQA L:VAR_LEN */

#endif /* SCC_LCFG_H */
/**********************************************************************************************************************
 *  END OF FILE: Scc_Lcfg.h
 *********************************************************************************************************************/
