
/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  :  SfrTst_LCfg.c                                                **
**                                                                            **
**  DATE, TIME: 2020-11-23, 11:38:28                                          **
**                                                                            **
**  GENERATOR : Build b141014-0350                                            **
**                                                                            **
**  BSW MODULE DECRIPTION : SfrTst.bmd/xdm                                    **
**                                                                            **
**  VARIANT   : VariantLinkTime                                               **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / HighTec /WindRiver                                  **
**                                                                            **
**  AUTHOR    : DL-BLR-ATV-STC                                                **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : This file contains configuration settings for SFR test     **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "SfrTst_Cfg.h"
#include "Ifx_reg.h"

#define IFX_SFRTST_START_SEC_LINKTIMECFG_ASIL_B
#include "Ifx_MemMap.h"
/*******************************************************************************
**                      Private Macro definition                              **
*******************************************************************************/

/*******************************************************************************
**                      Configuration Options                                 **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/


/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Funtion Declarations                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/


/* SfrTest configuration  */
const SfrTst_ConfigType RegisterConfig1[1] =
{
  /* Register Address,  Value,  Mask , BitWidth        */
  {&FLASH0_FCON.U, 0xa12b6aU,  0x80000U, STL_BIT_32}, 
};
/* SfrTest configuration  */
const SfrTst_ConfigType RegisterConfig2[35] =
{
  /* Register Address,  Value,  Mask , BitWidth        */
  {&SMU_FSP.U, 0x3fff18U,  0xffc000ffU, STL_BIT_32}, 
  {&SMU_AGC.U, 0x20000021U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG5CF0.U, 0x21U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG6CF0.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG0FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG1FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG2FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG3FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG4FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG5FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG6FSP.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_RMCTL.U, 0x0U,  0x1U, STL_BIT_32}, 
  {&SMU_RTC.U, 0x3fff01U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_PCTL.U, 0x83U,  0x83U, STL_BIT_32}, 
  {&SMU_RTAC0.U, 0x938b0800U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_RTAC1.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG0CF0.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG1CF0.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG2CF0.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG3CF0.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG4CF0.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG0CF1.U, 0xc3bbbU,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG0CF2.U, 0xc3bbbU,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG1CF1.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG1CF2.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG2CF1.U, 0xe7e001f0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG2CF2.U, 0xe7e001f0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG3CF1.U, 0xee67fbffU,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG3CF2.U, 0xee67fbffU,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG4CF1.U, 0xeaeeeU,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG4CF2.U, 0xeaeeeU,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG5CF1.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG5CF2.U, 0x21U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG6CF1.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
  {&SMU_AG6CF2.U, 0x0U,  0xffffffffU, STL_BIT_32}, 
};

/* The index given in the API function is used to choose which */
/* configuration is used from AllAvailableSets                 */
const SfrTst_ParamSetType AllAvailableSets[SFR_TST_CFG_PARAM_COUNT] =
{

  {1U,  0xffffffffU, RegisterConfig1},
 
  {35U,  0xbc04db33U, RegisterConfig2}
};
#define IFX_SFRTST_STOP_SEC_LINKTIMECFG_ASIL_B
#include "Ifx_MemMap.h"    
