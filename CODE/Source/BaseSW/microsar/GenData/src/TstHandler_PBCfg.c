/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  :  TstHandler_PBCfg.c                                           **
**                                                                            **
**  DATE, TIME: 2020-11-23, 11:37:18                                          **
**                                                                            **
**  GENERATOR : Build b141014-0350                                            **
**                                                                            **
**  BSW MODULE DECRIPTION : TstHandler.bmd                                    **
**                                                                            **
**  VARIANT   : VariantPB                                                     **
**                                                                            **
**  PLATFORM  : Infineon Aurix                                                **
**                                                                            **
**  COMPILER  : Tasking / GNU / Diab                                          **
**                                                                            **
**  AUTHOR    : DL-BLR-ATV-STC                                                **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : Test Handler configuration generated out of ECU            **
**                 configuration file                                         **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "TstHandler_Cfg.h"

#define IFX_TSTHANDLER_START_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"

/*******************************************************************************
**                      Private Macro definition                              **
*******************************************************************************/

/*******************************************************************************
**                      Configuration Options                                 **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/


/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Funtion Declarations                           **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
#include "TstHandler.h"


static const Th_TestLibFuncPtrType Th_MTLFuncList_ConfigSet0_Core0[TH_TOTAL_TESTS] =
{
  /* 0 */ &ClkmTst_ClkmTst,
  /* 1 */ &VltmTst_VltmTst,
  /* 2 */ &TrapTst_TrapTst,
  /* 3 */ &SriTst_SriTst,
  /* 4 */ &PmuEccEdcTst_EccEdcTst,
  /* 5 */ &SpbTst_TimeoutTst,
  /* 6 */ &SffTst_SffTst,
  /* 7 */ &LockStepTst_LockStepTst,
  /* 8 */ &SramEccTst_SramEccTst,
  /* 9 */ &PflashMonTst_PflashMonTst,
  /* 10 */ &CpuBusMpuLfmTst_LfmTest,
  /* 11 */ &CpuMpuTst_CpuMpuTst,
  /* 12 */ &IomTst_IomTst,
  /* 13 */ &IRTst_IRTst,
  /* 14 */ &PhlSramTst_PhlSramTst,
  /* 15 */ &SmuTst_NmiTst,
  /* 16 */ &SmuTst_IrqTst,
  /* 17 */ &SmuTst_RtTst,
  /* 18 */ &RegAccProtTst_RegAccProtTst,
  /* 19 */ &SfrTst_SfrCmpTst,
  /* 20 */ &SfrTst_SfrCrcTst,
};


static const Th_TestlibTestSetType Th_PreRunTests_ConfigSet0_Core0[3][SL_MAX_TESTS_PRE_RUN_GRP]=
{
  {
    {Core0_CpuMpuTst_ConfigSet0, 0U, 0U},
    {Core0_PflsahMonTst_ConfigSet0, 0U, 0U},
    {Core0_PmuEccEdcTst_ConfigSet0, 0U, 0U},
    {Core0_SramEccTst_ConfigSet0, 0U, 0U},
    {Core0_SriTst_ConfigSet0, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
  },
  {
    {Core0_IomTst_ConfigSet0, 0U, 0U},
    {Core0_PhlSramTst_ConfigSet0, 0U, 0U},
    {Core0_SmuTst_IrqTst_ConfigSet0, 0U, 0U},
    {Core0_SmuTst_NmiTst_ConfigSet0, 0U, 0U},
    {Core0_SmuTst_RtTst_ConfigSet0, 0U, 0U},
    {Core0_SpbTstTimeout_ConfigSet0, 0U, 0U},
    {Core0_RegAccProTst_ConfigSet0, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
  },
  {
    {Core0_ClkmTst_ConfigSet0, 0U, 0U},
    {Core0_CpuBusMpuLfmTst_ConfigSet0, 0U, 0U},
    {Core0_IRTst_ConfigSet0, 0U, 0U},
    {Core0_LockStepTst_ConfigSet0, 0U, 0U},
    {Core0_SffTst_ConfigSet0, 0U, 0U},
    {Core0_TrapTst_ConfigSet0, 0U, 0U},
    {Core0_VltmTst_ConfigSet0, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
  },
};

static const Th_TestlibTestSetType Th_RuntimeTests_ConfigSet0_Core0[1][SL_MAX_TESTS_RUN_TIME_GRP]=
{
  {
    {Core0_SfrCmpTst_ConfigSet0, 1U, 0U},
    {Core0_SfrCrcTst_ConfigSet0, 1U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
    {TH_INVALID_TEST_ID, 0U, 0U},
  },
};

/* Test Handler Module Configuration */
const Sl_ConfigType Sl_ConfigRoot[1] = 
{
  /* ConfigSet for core 0*/
  {
    /* Pointer to Early Pre-run Test configuration */
    NULL_PTR,
    /* Pointer to Pre-run Test configuration */
    *Th_PreRunTests_ConfigSet0_Core0,
    /* Pointer to Run time Test configuration */
    *Th_RuntimeTests_ConfigSet0_Core0,
    /* Pointer to Post-run Test configuration */
    NULL_PTR,
    /* Start of array of TestLib function pointers  */
    &Th_MTLFuncList_ConfigSet0_Core0[0],
    /* Address of SMU configuration for pre-run phase */
    &Smu_ConfigRoot[SmuConfigSet_0],
    /* Address of SMU configuration for runtime phase*/
    &Smu_ConfigRoot[SmuConfigSet_1],
    /* Number of early pre-run test groups  */
    0U,
    /* Number of pre-run test groups  */
    3U,
    /* Number of run-time test groups  */
    1U,
    /* Number of post-run test groups  */
    0U,
    /* Core Id to which this config set corresponds to */
    0U
  },
};
#define IFX_TSTHANDLER_STOP_SEC_POSTBUILDCFG_ASIL_B
#include "Ifx_MemMap.h"
