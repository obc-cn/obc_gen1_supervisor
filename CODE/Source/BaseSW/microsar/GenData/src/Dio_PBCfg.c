/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2014)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**   $FILENAME   : Dio_PBCfg.c $                                              **
**                                                                            **
**   $CC VERSION : \main\30 $                                                 **
**                                                                            **
**   DATE, TIME  : 2020-11-23, 11:39:23                                       **
**                                                                            **
**   GENERATOR   : Build b141014-0350                                         **
**                                                                            **
**   AUTHOR      : DL-AUTOSAR-Engineering                                     **
**                                                                            **
**   VENDOR      : Infineon Technologies                                      **
**                                                                            **
**   DESCRIPTION : DIO configuration generated out of ECU configuration       **
**                  file                                                      **
**                                                                            **
**   MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                             Includes                                       **
*******************************************************************************/

/* Include Port Module File */
#include "Dio.h"

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/*
  Information on MISRA-2004 violations:
  - MISRA Rule 19.1: Only preprocessor statements and comments before 
    '#include'Inclusion of the file MemMap.h is done for memory mapping of 
    the code and data. This is violation of MISRA Rule 19.1, but cannot 
    be avoided.
*/

/* Memory mapping of the DIO configuration */
#define DIO_START_SEC_POSTBUILDCFG
#include "MemMap.h"



/*
      Configuration of DIO Channel groups 
      DioConfig 1
*/
static const Dio_ChannelGroupType Dio_kChannelGroupConfig_0[DIO_CHANNELGROUPCOUNT_0]=
{
  {
    /* DioPort_0, DioChGrpId_P33_1 */
    (Dio_PortLevelType)0x20, /* Mask    */
    (uint8)5, 	            /* Offset  */
    (Dio_PortType)0	        /* Port Id */
  }
};

static const Dio_PortChannelIdType Dio_kPortChannelConfig_0[] =
{
  { /* Port0 */
     DIO_PORT_CONFIGURED,
     (0x0060U)
  },
  { /* Port2 */
     DIO_PORT_CONFIGURED,
     (0x00ccU)
  },
  { /* Port10 */
     DIO_PORT_NOT_CONFIGURED,
     (0x0000U)
  },
  { /* Port11 */
     DIO_PORT_CONFIGURED,
     (0x1404U)
  },
  { /* Port13 */
     DIO_PORT_CONFIGURED,
     (0x0003U)
  },
  { /* Port14 */
     DIO_PORT_CONFIGURED,
     (0x0100U)
  },
  { /* Port15 */
     DIO_PORT_CONFIGURED,
     (0x01fbU)
  },
  { /* Port20 */
     DIO_PORT_CONFIGURED,
     (0x00c0U)
  },
  { /* Port21 */
     DIO_PORT_CONFIGURED,
     (0x0008U)
  },
  { /* Port22 */
     DIO_PORT_CONFIGURED,
     (0x001bU)
  },
  { /* Port23 */
     DIO_PORT_NOT_CONFIGURED,
     (0x0000U)
  },
  { /* Port33 */
     DIO_PORT_CONFIGURED,
     (0x1b90U)
  },
  { /* Port34 */
     DIO_PORT_NOT_CONFIGURED,
     (0x0000U)
  },
  { /* Port40 */
     DIO_PORT_NOT_CONFIGURED,
     (0x0000U)
  },
  { /* Port41 */
     DIO_PORT_NOT_CONFIGURED,
     (0x0000U)
  }
};


const Dio_ConfigType Dio_ConfigRoot[1] =
{
  {
    /* Marker check value */
    (uint32)(((uint32)DIO_MODULE_ID<<DIO_MODULEID_SHIFT_VAL) | (DIO_INSTANCE_ID)),

    /* Dio Port and Channelconfiguration set 0 */
    &Dio_kPortChannelConfig_0[0],
    /* Dio Channelgroup configuration set 0 */
    &Dio_kChannelGroupConfig_0[0],
    /* Configured number of Dio Channelgroups for configuration set 0 */
    DIO_CHANNELGROUPCOUNT_0,
  }
};

#define DIO_STOP_SEC_POSTBUILDCFG
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives
  is allowed only for MemMap.h*/
#include "MemMap.h"

/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
