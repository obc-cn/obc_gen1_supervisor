/*******************************************************************************
**                                                                            **
** Copyright (C) Infineon Technologies (2018)                                 **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to Infineon       **
** Technologies. Passing on and copying of this document, and communication   **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME  : Spi_PBCfg.c                                                   **
**                                                                            **
**  $CC VERSION : \main\94 $                                                 **
**                                                                            **
**  DATE, TIME: 2020-11-23, 11:38:15                                         **
**                                                                            **
**  GENERATOR : Build b141014-0350                                            **
**                                                                            **
**  AUTHOR    : DL-AUTOSAR-Engineering                                        **
**                                                                            **
**  VENDOR    : Infineon Technologies                                         **
**                                                                            **
**  DESCRIPTION  : SPI configuration generated out of ECU configuration       **
**                 file                                                       **
**                                                                            **
**  MAY BE CHANGED BY USER [yes/no]: no                                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Include SPI Module File */
/* [cover parentID=DS_NAS_SPI_PR699,DS_NAS_SPI_PR709] */

#include "Spi.h"
/* Inclusion of Mcal Specific Global Header File */
#include "Mcal.h"

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/
/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/* MISRA RULE 87 VIOLATION: Inclusion of MemMap.h in between the code can't
   be avoided as it is required for mapping global variables, constants
   and code
*/
/* Violates MISRA Required Rule 16.9,
            function identifier used without '&' or parenthisized parameter list
           when using function pointer in configurations
*/

/*
                     Container: SpiChannelConfiguration
*/
#define SPI_START_SEC_POSTBUILDCFG
/*
 * To be used for global or static constants (unspecified size)
*/
#include "MemMap.h"
/*
Configuration : Channel Configuration Constant Structure.
The IB Channels are configured first followed by EB.
*/
static const Spi_ChannelConfigType Spi_kChannelConfig0[] =
{
/* EB Channel: CH_RW_REG_0 Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000ffU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)4U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: CH_RW_REG_1 Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000ffU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)4U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: CH_RW_REG_2 Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000ffU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)4U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: CH_RW_REG_3 Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000ffU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)4U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: Spi_ChlCmd Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000daU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)2U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: Spi_ChlData Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000daU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)1528U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
/* EB Channel: Spi_ChlReg Configuration */
  {
    /* Default Data, SPI_DEFAULT_DATA */
    (uint32)0x000000daU,
    /* Data Configuration */
    Spi_DataConfig(8U, /* Data Width */
                   SPI_DATA_MSB_FIRST), /* Transfer Start */
    /* EB Channels : SPI112: Max EB Buffers  */
    (Spi_NumberOfDataType)2U,
    /* Buffer Type, SPI_CHANNEL_TYPE */
    (uint8)SPI_EB_BUFFER,
  },
};

/*
                    Container: SpiJobConfiguration
*/
/* Notification Function of JOB_RW_REG_0 is NULL_PTR */
/* Notification Function of JOB_RW_REG_1 is NULL_PTR */
/* Notification Function of JOB_RW_REG_2 is NULL_PTR */
/* Notification Function of JOB_RW_REG_3 is NULL_PTR */
/* Notification Function of Spi_QCA7005_JobData is NULL_PTR */
/* Notification Function of Spi_QCA7005_JobReg is NULL_PTR */


/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: JOB_RW_REG_0*/
static const Spi_ChannelType JOB_RW_REG_0_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_CH_RW_REG_0,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: JOB_RW_REG_1*/
static const Spi_ChannelType JOB_RW_REG_1_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_CH_RW_REG_1,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: JOB_RW_REG_2*/
static const Spi_ChannelType JOB_RW_REG_2_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_CH_RW_REG_2,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: JOB_RW_REG_3*/
static const Spi_ChannelType JOB_RW_REG_3_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_CH_RW_REG_3,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: Spi_QCA7005_JobData*/
static const Spi_ChannelType Spi_QCA7005_JobData_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_Spi_ChlCmd,
  SpiConf_SpiChannel_Spi_ChlData,
  SPI_CHANNEL_LINK_DELIMITER
};
/*
Configuration: Channel Assignment
*/
/*Channel Assignment of Job: Spi_QCA7005_JobReg*/
static const Spi_ChannelType Spi_QCA7005_JobReg_ChannelLinkPtr[] =
{
  SpiConf_SpiChannel_Spi_ChlCmd,
  SpiConf_SpiChannel_Spi_ChlReg,
  SPI_CHANNEL_LINK_DELIMITER
};

/*
Configuration: Job Configuration Constant Structure.
*/
static const Spi_JobConfigType Spi_kJobConfig0[] =
{

/* Job: JOB_RW_REG_0 Configuration */
  {
   /* Job End Notification: Spi_JobEndNotification, SPI118 */
    NULL_PTR,
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    JOB_RW_REG_0_ChannelLinkPtr,
    /* Baud Rate (1000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x9U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x02U,/*IDLE*/ 0x04U,
        /*LPRE*/0x03U, /*LEAD*/0x04U,
        /*TPRE*/0x03U, /*TRAIL*/0x04U),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_LOW,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority : SPI_JOB_PRIORITY, SPI002,SPI093 */
    SPI_JOB_PRIORITY_0,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */
      (uint8)((uint8)SPI_QSPI_CHANNEL2 << 4U) | (SPI_QSPI0_INDEX),

    /* Channel Based Chip Select */
    (uint8)0U,
    
    /* Spi Parity Selection */
    (uint8)SPI_QSPI_PARITY_UNUSED,
    
  },
/* Job: JOB_RW_REG_1 Configuration */
  {
   /* Job End Notification: Spi_JobEndNotification, SPI118 */
    NULL_PTR,
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    JOB_RW_REG_1_ChannelLinkPtr,
    /* Baud Rate (1000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x9U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x02U,/*IDLE*/ 0x04U,
        /*LPRE*/0x03U, /*LEAD*/0x04U,
        /*TPRE*/0x03U, /*TRAIL*/0x04U),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_LOW,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority : SPI_JOB_PRIORITY, SPI002,SPI093 */
    SPI_JOB_PRIORITY_0,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */
      (uint8)((uint8)SPI_QSPI_CHANNEL2 << 4U) | (SPI_QSPI0_INDEX),

    /* Channel Based Chip Select */
    (uint8)0U,
    
    /* Spi Parity Selection */
    (uint8)SPI_QSPI_PARITY_UNUSED,
    
  },
/* Job: JOB_RW_REG_2 Configuration */
  {
   /* Job End Notification: Spi_JobEndNotification, SPI118 */
    NULL_PTR,
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    JOB_RW_REG_2_ChannelLinkPtr,
    /* Baud Rate (1000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x9U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x02U,/*IDLE*/ 0x04U,
        /*LPRE*/0x03U, /*LEAD*/0x04U,
        /*TPRE*/0x03U, /*TRAIL*/0x04U),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_LOW,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority : SPI_JOB_PRIORITY, SPI002,SPI093 */
    SPI_JOB_PRIORITY_0,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */
      (uint8)((uint8)SPI_QSPI_CHANNEL2 << 4U) | (SPI_QSPI0_INDEX),

    /* Channel Based Chip Select */
    (uint8)0U,
    
    /* Spi Parity Selection */
    (uint8)SPI_QSPI_PARITY_UNUSED,
    
  },
/* Job: JOB_RW_REG_3 Configuration */
  {
   /* Job End Notification: Spi_JobEndNotification, SPI118 */
    NULL_PTR,
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    JOB_RW_REG_3_ChannelLinkPtr,
    /* Baud Rate (1000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x9U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x02U,/*IDLE*/ 0x04U,
        /*LPRE*/0x03U, /*LEAD*/0x04U,
        /*TPRE*/0x03U, /*TRAIL*/0x04U),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_LOW,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority : SPI_JOB_PRIORITY, SPI002,SPI093 */
    SPI_JOB_PRIORITY_0,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */
      (uint8)((uint8)SPI_QSPI_CHANNEL2 << 4U) | (SPI_QSPI0_INDEX),

    /* Channel Based Chip Select */
    (uint8)0U,
    
    /* Spi Parity Selection */
    (uint8)SPI_QSPI_PARITY_UNUSED,
    
  },
/* Job: Spi_QCA7005_JobData Configuration */
  {
   /* Job End Notification: Spi_JobEndNotification, SPI118 */
    NULL_PTR,
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    Spi_QCA7005_JobData_ChannelLinkPtr,
    /* Baud Rate (2000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x4U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x02U,/*IDLE*/ 0x04U,
        /*LPRE*/0x03U, /*LEAD*/0x04U,
        /*TPRE*/0x03U, /*TRAIL*/0x04U),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */
      (uint8)((uint8)SPI_QSPI_CHANNEL9 << 4U) | (SPI_QSPI1_INDEX),

    /* Channel Based Chip Select */
    (uint8)0U,
    
    /* Spi Parity Selection */
    (uint8)SPI_QSPI_PARITY_UNUSED,
    
  },
/* Job: Spi_QCA7005_JobReg Configuration */
  {
   /* Job End Notification: Spi_JobEndNotification, SPI118 */
    NULL_PTR,
    /* User given name outside the naming convention */
    /* Spi_ChannelLinkPtr */
    Spi_QCA7005_JobReg_ChannelLinkPtr,
    /* Baud Rate (2000000 Hz) Hw configuration Parameters */
    Spi_BaudRateParams(/*TQ*/(0x0U), /*Q*/(0x4U),
              /*A*/(0x3U), /*B*/(0x3U), /*C*/(0x3U)),

    /* Time Delay Configuration */
      (uint32)Spi_DelayParams(/*IPRE*/0x02U,/*IDLE*/ 0x04U,
        /*LPRE*/0x03U, /*LEAD*/0x04U,
        /*TPRE*/0x03U, /*TRAIL*/0x04U),

    SPI_CS_HW, /* Hw CS Pin is Selected */

    /*CS Active level Polarity*/
    SPI_CS_POLARITY_LOW,

 /* Shift Clock Configuration : Clock Idle Polarity: SPI_SHIFT_CLOCK_IDLE_LEVEL,
                                  Clock Phase: SPI_DATA_SHIFT_EDGE */
    Spi_ShiftClkConfig(SPI_CLK_IDLE_HIGH,
                      SPI_DATA_SHIFT_LEAD),
   /* Job Priority escalated to Maximum as it is mapped
      to one or more non-interruptible sequence */
    SPI_JOB_PRIORITY_3,

    /* Spi HW Unit. bit[7:4]: Channel no, bit[3:0]: hw module no */
      (uint8)((uint8)SPI_QSPI_CHANNEL9 << 4U) | (SPI_QSPI1_INDEX),

    /* Channel Based Chip Select */
    (uint8)0U,
    
    /* Spi Parity Selection */
    (uint8)SPI_QSPI_PARITY_UNUSED,
    
  }
};

/*
                     Container: Spi_SequenceConfiguration
*/
/* Notification Function of Sequence: SEQ_RW_REG_N is NULL_PTR */
/* Notification Function of Sequence: SEQ_RW_REG_M is NULL_PTR */
/* Notification Function of Sequence: SEQ_RW_REG_L is NULL_PTR */
/* Notification Function of Sequence: Spi_SeqData */
extern void Eth_30_Ar7000_SpiSeqEndCbk_0(void);
/* Notification Function of Sequence: Spi_SeqReg */
extern void Eth_30_Ar7000_SpiSeqEndCbk_1(void);

/* Job Assignment of Sequence: SEQ_RW_REG_N */
static const Spi_JobType SEQ_RW_REG_N_JobLinkPtr[] =
{
  SpiConf_SpiJob_JOB_RW_REG_0,
  SPI_JOB_LINK_DELIMITER
};
/* Job Assignment of Sequence: SEQ_RW_REG_M */
static const Spi_JobType SEQ_RW_REG_M_JobLinkPtr[] =
{
  SpiConf_SpiJob_JOB_RW_REG_0,
  SpiConf_SpiJob_JOB_RW_REG_1,
  SpiConf_SpiJob_JOB_RW_REG_2,
  SPI_JOB_LINK_DELIMITER
};
/* Job Assignment of Sequence: SEQ_RW_REG_L */
static const Spi_JobType SEQ_RW_REG_L_JobLinkPtr[] =
{
  SpiConf_SpiJob_JOB_RW_REG_0,
  SpiConf_SpiJob_JOB_RW_REG_1,
  SpiConf_SpiJob_JOB_RW_REG_2,
  SpiConf_SpiJob_JOB_RW_REG_3,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: Spi_SeqData */
static const Spi_JobType Spi_SeqData_JobLinkPtr[] =
{
  SpiConf_SpiJob_Spi_QCA7005_JobData,
  SPI_JOB_LINK_DELIMITER
};
/*
Configuration: Job Assignment
*/
/* Job Assignment of Sequence: Spi_SeqReg */
static const Spi_JobType Spi_SeqReg_JobLinkPtr[] =
{
  SpiConf_SpiJob_Spi_QCA7005_JobReg,
  SPI_JOB_LINK_DELIMITER
};


/* Printing of shared sequence */
static const Spi_SequenceType SEQ_RW_REG_N_SharedSeqsPtr[] = {
  SpiConf_SpiSequence_SEQ_RW_REG_M,
  SpiConf_SpiSequence_SEQ_RW_REG_L,
  SPI_SHARED_JOBS_DELIMITER
};


/* Printing of shared sequence */
static const Spi_SequenceType SEQ_RW_REG_M_SharedSeqsPtr[] = {
  SpiConf_SpiSequence_SEQ_RW_REG_N,
  SpiConf_SpiSequence_SEQ_RW_REG_L,
  SPI_SHARED_JOBS_DELIMITER
};


/* Printing of shared sequence */
static const Spi_SequenceType SEQ_RW_REG_L_SharedSeqsPtr[] = {
  SpiConf_SpiSequence_SEQ_RW_REG_N,
  SpiConf_SpiSequence_SEQ_RW_REG_M,
  SPI_SHARED_JOBS_DELIMITER
};


/*
Configuration: Sequence Configuration Constant Structure.
*/
static const Spi_SequenceConfigType Spi_kSequenceConfig0[] =
{   /* Sequence: SEQ_RW_REG_N Configuration */
  {
    /* Spi_SeqEndNotification */
    NULL_PTR,

    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SEQ_RW_REG_N_JobLinkPtr,

    /* User given name outside the naming convention */
    /* Sequences that share jobs with this sequence */
    SEQ_RW_REG_N_SharedSeqsPtr,
    /* This holds the total number of jobs linked to this sequence */
    1U,
    /* Sequence Interruptible or Not (SPI125, SPI126) */
    SPI_SEQ_INT_TRUE,   },
  /* Sequence: SEQ_RW_REG_M Configuration */
  {
    /* Spi_SeqEndNotification */
    NULL_PTR,

    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SEQ_RW_REG_M_JobLinkPtr,

    /* User given name outside the naming convention */
    /* Sequences that share jobs with this sequence */
    SEQ_RW_REG_M_SharedSeqsPtr,
    /* This holds the total number of jobs linked to this sequence */
    3U,
    /* Sequence Interruptible or Not (SPI125, SPI126) */
    SPI_SEQ_INT_TRUE,   },
  /* Sequence: SEQ_RW_REG_L Configuration */
  {
    /* Spi_SeqEndNotification */
    NULL_PTR,

    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    SEQ_RW_REG_L_JobLinkPtr,

    /* User given name outside the naming convention */
    /* Sequences that share jobs with this sequence */
    SEQ_RW_REG_L_SharedSeqsPtr,
    /* This holds the total number of jobs linked to this sequence */
    4U,
    /* Sequence Interruptible or Not (SPI125, SPI126) */
    SPI_SEQ_INT_TRUE,   },
    
  /* Sequence: Spi_SeqData Configuration */
  {
    /* Spi_SeqEndNotification */
    &Eth_30_Ar7000_SpiSeqEndCbk_0,

    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    Spi_SeqData_JobLinkPtr,

    /* User given name outside the naming convention */
    /* Sequences that share jobs with this sequence */
    NULL_PTR,
     /* This holds the total number of jobs linked to this sequence */
    1U,
    /* Sequence Interruptible or Not (SPI125, SPI126) */
    SPI_SEQ_INT_FALSE,   },
  /* Sequence: Spi_SeqReg Configuration */
  {
    /* Spi_SeqEndNotification */
    &Eth_30_Ar7000_SpiSeqEndCbk_1,

    /* User given name outside the naming convention */    /* Spi_JobLinkPtr */
    Spi_SeqReg_JobLinkPtr,

    /* User given name outside the naming convention */
    /* Sequences that share jobs with this sequence */
    NULL_PTR,
     /* This holds the total number of jobs linked to this sequence */
    1U,
    /* Sequence Interruptible or Not (SPI125, SPI126) */
    SPI_SEQ_INT_FALSE,   }
};

static const Spi_DmaConfigType Spi_kDmaConfig0[]=
{

  { /* QSPI0 Module Hw Dma Config */
    DMA_CHANNEL2,  /* Tx */    DMA_CHANNEL1   /* Rx */  },

  { /* QSPI1 Module Hw Dma Config */
    DMA_CHANNEL11,  /* Tx */    DMA_CHANNEL10   /* Rx */  },

  { /* QSPI2 Module Hw Dma Config */
    DMA_CHANNEL13,  /* Tx */    DMA_CHANNEL12   /* Rx */  },

  { /* QSPI3 Module Hw Dma Config */
    DMA_CHANNEL15,  /* Tx */    DMA_CHANNEL14   /* Rx */  },

};

static const Spi_HWModuleConfigType Spi_kModuleConfig0[]=
{
  /* QSPI0 Module */
  {
      /*Clock Settings:Sleep Control Disabled*/
      SPI_CLK_SLEEP_DISABLE,
	  /*SSOC register value for QSPI0*/
	  (uint32)0x0U,
      SPI_QSPI0_MRIS_SEL,
      &Spi_kDmaConfig0[0U],
  },
  /* QSPI1 Module */
  {
      /*Clock Settings:Sleep Control Disabled*/
      SPI_CLK_SLEEP_DISABLE,
	  /*SSOC register value for QSPI1*/
	  (uint32)0x0U,
      SPI_QSPI1_MRIS_SEL,
      &Spi_kDmaConfig0[1U],
  },
  /* QSPI2 Module */
  {
    SPI_2_NOT_CONFIGURED,
    (uint32)0x0U,
    SPI_2_NOT_CONFIGURED,
    NULL_PTR,
  },
  /* QSPI3 Module */
  {
    SPI_3_NOT_CONFIGURED,
    (uint32)0x0U,
    SPI_3_NOT_CONFIGURED,
    NULL_PTR,
  },
};



static const Spi_BaudrateEconType Spi_kBaudrateEcon0[]=
{
  {
    Spi_BaudRateECON(0x9U, 0x3U, 0x3U, 0x3U,
    SPI_DATA_SHIFT_LEAD,
    SPI_CLK_IDLE_LOW,
    SPI_QSPI_PARITY_DISABLE),
    (uint8)((uint8)SPI_QSPI_CHANNEL2 << 4U) | (SPI_QSPI0_INDEX)
  },
  {
    Spi_BaudRateECON(0x4U, 0x3U, 0x3U, 0x3U,
    SPI_DATA_SHIFT_LEAD,
    SPI_CLK_IDLE_HIGH,
    SPI_QSPI_PARITY_DISABLE),
    (uint8)((uint8)SPI_QSPI_CHANNEL9 << 4U) | (SPI_QSPI1_INDEX)
  }
};

const Spi_ConfigType Spi_ConfigRoot[1U] =
{
  {
    Spi_kChannelConfig0,
    Spi_kJobConfig0,
    Spi_kSequenceConfig0,
    {
      &Spi_kModuleConfig0[0U],
      &Spi_kModuleConfig0[1U],
      &Spi_kModuleConfig0[2U],
      &Spi_kModuleConfig0[3U],
    },
    Spi_kBaudrateEcon0,
    (Spi_JobType)(sizeof(Spi_kJobConfig0) / sizeof(Spi_JobConfigType)),
    (Spi_ChannelType)(sizeof(Spi_kChannelConfig0) / \
                      sizeof(Spi_ChannelConfigType)),
    (Spi_SequenceType)(sizeof(Spi_kSequenceConfig0) / \
                                        sizeof(Spi_SequenceConfigType)),
    (uint8)(sizeof(Spi_kBaudrateEcon0) / sizeof(Spi_BaudrateEconType)),
    3U
  }
};


#define SPI_STOP_SEC_POSTBUILDCFG
/* Allows to map variables, constants and code of modules to individual
  memory sections.*/
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
allowed only for MemMap.h*/  
#include "MemMap.h"

/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/
/* General Notes */
/*
SPI095: The code file structure shall not be defined within this specification
completely. At this point it shall be pointed out that the code-file structure
shall include the following file named:
- Spi_Lcfg.c � for link time and for post-build configurable parameters and
- Spi_PBcfg.c � for post build time configurable parameters.
These files shall contain all link time and post-build time configurable
parameters.
This file shall contain all link time and post-build time configurable
parameters.
For the implementation of VariantPC, the implementation stores the
pre compile time parameters that have to be defined as constants in this file.

SPI123: In this configuration, all Sequences declared are considered as Non
Interruptible Sequences. That means, their dedicated parameter
SPI_INTERRUPTIBLE_SEQUENCE (see SPI064 & SPI106) could be omitted or the
FALSE value should be used as default.

*/


