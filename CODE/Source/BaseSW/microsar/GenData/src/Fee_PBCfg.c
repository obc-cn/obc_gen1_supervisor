/******************************************************************************
**                                                                           **
** Copyright (C) Infineon Technologies (2013)                                **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to Infineon      **
** Technologies. Passing on and copying of this document, and communication  **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : Fee_PBCfg.c $                                              **
**                                                                           **
**  $CC VERSION : \main\18 $                                                 **
**                                                                           **
**  DATE, TIME: 2020-11-23, 11:37:27                                         **
**                                                                           **
**  GENERATOR : Build b141014-0350                                           **
**                                                                           **
**  AUTHOR    : DL-AUTOSAR-Engineering                                       **
**                                                                           **
**  VENDOR    : Infineon Technologies                                        **
**                                                                           **
**  DESCRIPTION  : FEE configuration generated out of ECU configuration      **
**                   file (Fee.bmd)                                          **
**                                                                           **
**  MAY BE CHANGED BY USER [yes/no]: No                                      **
**                                                                           **
******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/

/* Include Fee Module Header File */
#include "Fee.h"

/*******************************************************************************
**                      Imported Compiler Switch Checks                       **
*******************************************************************************/

/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

/*******************************************************************************
**                      Global Function Declarations                          **
*******************************************************************************/

/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/

/* Allocate space for state data variables in RAM */
#define FEE_START_SEC_VAR_FAST_UNSPECIFIED
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
 allowed only for MemMap.h*/
#include "MemMap.h"

/* Fee State Variable structure */
static Fee_StateDataType Fee_StateVar;

#define FEE_STOP_SEC_VAR_FAST_UNSPECIFIED
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
 allowed only for MemMap.h*/
#include "MemMap.h"


/* User configured logical block initialization structure */
#define FEE_START_SEC_CONST_UNSPECIFIED
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
 allowed only for MemMap.h*/
#include "MemMap.h"

static const Fee_BlockType Fee_BlockConfig[] =
{
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    2U, /* Block number */
    4U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    26U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    28U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    30U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    32U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    12U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    20U, /* Block number */
    16U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    18U, /* Block number */
    1U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    14U, /* Block number */
    8U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    16U, /* Block number */
    11U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    24U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    46U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    48U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    50U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    52U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    54U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    56U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    58U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    60U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    62U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    64U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    66U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    68U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    70U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    72U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    74U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    84U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    76U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    78U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    80U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    82U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    34U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    36U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    38U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    40U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    42U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    44U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    3U, /* Block number */
    4U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    4U, /* Block number */
    2000U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    6U, /* Block number */
    1U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    8U, /* Block number */
    4U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    10U, /* Block number */
    1U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    22U, /* Block number */
    1U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    86U, /* Block number */
    76U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    88U, /* Block number */
    2U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_NORMAL_DATA,     /* Block type is Normal */
    90U, /* Block number */
    5U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    92U, /* Block number */
    128U /* Fee Block Size */
  },
  {
    0U, /* Block Cycle Count */
    (uint8)FEE_IMMEDIATE_DATA,  /* Block type is Immediate */
    94U, /* Block number */
    128U /* Fee Block Size */
  }
};

/* Fee Global initialization structure. */
const Fee_ConfigType Fee_ConfigRoot[] =
{
  {
    /* Fee State Data Structure */
    &Fee_StateVar,
    /* Pointer to logical block configurations */
    &Fee_BlockConfig[0],
    /* Fee Job end notification API */
    (Fee_NotifFunctionPtrType)NULL_PTR,
    /* Fee Job error notification API */
    (Fee_NotifFunctionPtrType)NULL_PTR,
    /* Fee threshold value */
    200U,
    /* Number of blocks configured */
    48U,

    {
      /* Ignore the unconfigured blocks */
      FEE_UNCONFIG_BLOCK_IGNORE,
      /* Restart Garbage Collection during initialization */
      FEE_GC_RESTART_INIT,
      /* Erase Suspend feature is disabled */
      FEE_ERASE_SUSPEND_DISABLED,
      /* Reserved */
      0U
    },

    /* Fee Illegal State notification */
    (Fee_NotifFunctionPtrType)NULL_PTR,
    /* Erase All feature is enabled */
    (boolean)TRUE
  }
};

#define FEE_STOP_SEC_CONST_UNSPECIFIED
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
 allowed only for MemMap.h*/
#include "MemMap.h"

#define FEE_START_SEC_SPL_VAR_32BIT
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
 allowed only for MemMap.h*/
#include "MemMap.h"
const Fee_ConfigType * Fee_CfgPtr = &Fee_ConfigRoot[0];
#define FEE_STOP_SEC_SPL_VAR_32BIT
/*IFX_MISRA_RULE_19_01_STATUS=File inclusion after pre-processor directives is
 allowed only for MemMap.h*/
#include "MemMap.h"

