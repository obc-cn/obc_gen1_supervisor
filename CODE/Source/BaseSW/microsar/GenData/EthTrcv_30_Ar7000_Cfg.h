/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: EthTrcv_Ar7000
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: EthTrcv_30_Ar7000_Cfg.h
 *   Generation Time: 2020-08-19 13:07:44
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/


#if !defined(ETHTRCV_30_AR7000_CFG_H)
#define ETHTRCV_30_AR7000_CFG_H

/* START of Checksum exclude for - EthTrcv_PrecompileCRC */

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"

/* PRQA S 857 EOF */ /* MD_MSR_1.1_857 */

/**********************************************************************************************************************
 *  SYMBOLIC NAME VALUE DEFINES: TRCV
 *********************************************************************************************************************/
 #define EthTrcvConf_EthTrcvConfig_EthTrcvConfig (0uL) 


/**********************************************************************************************************************
 *  LINKTIME / PRECOMPILE CRC
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_CRC_CHECK                    STD_OFF
#define ETHTRCV_30_AR7000_PRECOMPILE_CRC               (uint32) 0xa6844d7cU


/**********************************************************************************************************************
 *  GENERAL DEFINES
 *********************************************************************************************************************/
#ifndef ETHTRCV_30_AR7000_USE_DUMMY_STATEMENT
#define ETHTRCV_30_AR7000_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef ETHTRCV_30_AR7000_DUMMY_STATEMENT
#define ETHTRCV_30_AR7000_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef ETHTRCV_30_AR7000_DUMMY_STATEMENT_CONST
#define ETHTRCV_30_AR7000_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef ETHTRCV_30_AR7000_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define ETHTRCV_30_AR7000_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef ETHTRCV_30_AR7000_ATOMIC_VARIABLE_ACCESS
#define ETHTRCV_30_AR7000_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef ETHTRCV_30_AR7000_PROCESSOR_TC234
#define ETHTRCV_30_AR7000_PROCESSOR_TC234
#endif
#ifndef ETHTRCV_30_AR7000_COMP_GNU
#define ETHTRCV_30_AR7000_COMP_GNU
#endif
#ifndef ETHTRCV_30_AR7000_GEN_GENERATOR_MSR
#define ETHTRCV_30_AR7000_GEN_GENERATOR_MSR
#endif
#ifndef ETHTRCV_30_AR7000_CPUTYPE_BITORDER_LSB2MSB
#define ETHTRCV_30_AR7000_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_PRECOMPILE
#define ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_LINKTIME
#define ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef ETHTRCV_30_AR7000_CONFIGURATION_VARIANT
#define ETHTRCV_30_AR7000_CONFIGURATION_VARIANT ETHTRCV_30_AR7000_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef ETHTRCV_30_AR7000_POSTBUILD_VARIANT_SUPPORT
#define ETHTRCV_30_AR7000_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
 *  INTEGER DEFINES
 *********************************************************************************************************************/
/* START of Checksum include for - EthTrcv_PrecompileCRC */

#define ETHTRCV_30_AR7000_CONFIG_VARIANT            1U
#define ETHTRCV_30_AR7000_MAX_TRCVS_TOTAL           1u
#define ETHTRCV_30_AR7000_WAKEUP_TYPE               ETHTRCV_WAKEUP_NOT_SUPPORTED
#define ETHTRCV_30_AR7000_MAX_WAKEUP_MAP_NUM        0u

#define ETHTRCV_30_AR7000_HW_IDX              EthConf_EthCtrlConfig_EthCtrlConfig
#define ETHTRCV_30_AR7000_ETHIF_CTRL          EthIfConf_EthIfController_EthIfController
#define ETHTRCV_30_AR7000_MAINFUNC_CYCLE_TIME 20u

/* END of Checksum include for - EthTrcv_PrecompileCRC */


/**********************************************************************************************************************
 *  SWITCH DEFINES
 *********************************************************************************************************************/
/* START of Checksum include for - EthTrcv_PrecompileCRC */

#define ETHTRCV_30_AR7000_DEV_ERROR_DETECT          STD_OFF
#define ETHTRCV_30_AR7000_DEM_ERROR_DETECT          STD_OFF
#define ETHTRCV_30_AR7000_VERSION_INFO_API          STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_GET_TRCV_MODE      STD_ON
#define ETHTRCV_30_AR7000_ENABLE_SET_TRCV_MODE      STD_ON
#define ETHTRCV_30_AR7000_ENABLE_START_AUTO_NEG     STD_ON
#define ETHTRCV_30_AR7000_ENABLE_GET_LINK_STATE     STD_ON
#define ETHTRCV_30_AR7000_ENABLE_GET_BAUD_RATE      STD_ON
#define ETHTRCV_30_AR7000_ENABLE_GET_DUPLEX_MODE    STD_ON


/**********************************************************************************************************************
 *  AR7000 Firmware Download
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_FIRMWARE_DOWNLOAD                STD_ON
#define ETHTRCV_30_AR7000_ENABLE_TURBO_FW_DWLD                    STD_OFF
#define ETHTRCV_30_AR7000_FW_DWLD_ATTEMPTS                        3u
#define ETHTRCV_30_AR7000_ETHBLOCKSIZE                            1400u
#define ETHTRCV_30_AR7000_DEFAULTBLOCKSIZE                        64u
#define ETHTRCV_30_AR7000_MAX_API_CALL_RET_CNT                    3u
#define ETHTRCV_30_AR7000_HST_ACTION_QUEUE_SIZE                   5u
#define ETHTRCV_30_AR7000_ENABLE_DET_ON_DISABLED_HST_ACT_EVENTS   STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_HIF_COM_EVENT   STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_DEVICE_READY_FOR_SLAC_COM_EVENT  STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_MEMBER_OF_AVLN_WITH_PEERS_EVENT  STD_OFF


/**********************************************************************************************************************
 *  AR7000 NMK 
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_SET_NMK                          STD_ON
#define ETHTRCV_30_AR7000_ENABLE_READ_NMK                         STD_OFF
#define ETHTRCV_30_AR7000_CALC_NMK_LOCALLY                        STD_OFF
#define ETHTRCV_30_AR7000_NMK_SIZE_BYTE                           16u
#define ETHTRCV_30_AR7000_NID_SIZE_BYTE                           7u

#define ETHTRCV_30_AR7000_ENABLE_RESET_AND_LEAVE_API              STD_OFF
#define ETHTRCV_30_AR7000_SET_KEY_MAX_RETRIES                     3u

/**********************************************************************************************************************
 *  AR7000 Simple Connect
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_SIMPLE_CONNECT_API               STD_ON
 
/**********************************************************************************************************************
 *  AR7000 PIB Change
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_PIB_BACKUP                       STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_PIB_CHANGE_CBK                   STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_SET_PIB_DATA_API                 STD_OFF

/**********************************************************************************************************************
 *  AR7000 Physical Address
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_WRITE_PHYS_ADDRESS               STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_PHYS_ADDR_CBK                    STD_OFF
/**********************************************************************************************************************
 *  AR7000 Reset Device
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_RESET_DEVICE                     STD_ON

/**********************************************************************************************************************
 *  AR7000 Firmware Version
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_GET_SW_VERSION                   STD_ON
 
/**********************************************************************************************************************
 *  AR7000 DAK
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_WRITE_DAK                        STD_OFF

/**********************************************************************************************************************
 *  AR7000 Error Handling
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_DET_ON_FW_OR_COM_ERROR           STD_ON
 
/**********************************************************************************************************************
 *  AR7000 Further QCA Specifics
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_SM_HOOK_ROUTINE                  STD_OFF
#define ETHTRCV_30_AR7000_ENABLE_MODOP_API                        STD_ON
#define ETHTRCV_30_AR7000_ENABLE_SET_PROPERTY_API                 STD_ON
#define ETHTRCV_30_AR7000_MTYPE                                   0x88E1u
#define ETHTRCV_30_AR7000_LINKSTATEPOLLINGFREQ                    50u
#define ETHTRCV_30_AR7000_CONFTIMEOUT                             30u
#define ETHTRCV_30_AR7000_LINKSTATE_LOCK_CYCLES                   200u
#define ETHTRCV_30_AR7000_WRITE_APPLET_LOCK_CYCLES                20u
#define ETHTRCV_30_AR7000_MODOP_MAX_RETRANS                       3u
#define ETHTRCV_30_AR7000_HEARTBEAT_TIMEOUT                       100u

/**********************************************************************************************************************
 *  SLAC
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_ENABLE_SLAC                             STD_ON

/**********************************************************************************************************************
 *  SLAC - TIMINGS, LENGTHS, CONSTANTS
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_SLAC_MAX_ATTEN_CHAR_PROFILES            10u
#define ETHTRCV_30_AR7000_SLAC_T_TOGGLE                           6u
#define ETHTRCV_30_AR7000_SLAC_T_INIT                             6u
#define ETHTRCV_30_AR7000_SLAC_T_VALIDATE_REQ_RETRY               100u
#define ETHTRCV_30_AR7000_SLAC_NUM_PROFILE_GROUPS                 58u
#define ETHTRCV_30_AR7000_SLAC_RUNID_LEN_BYTE                     8u
#define ETHTRCV_30_AR7000_SLAC_SOUND_RND_LEN_BYTE                 16u
#define ETHTRCV_30_AR7000_SLAC_RESP_TYPE                          1u
#define ETHTRCV_30_AR7000_SLAC_MATCH_MVF_LEN                      86u
#define ETHTRCV_30_AR7000_SLAC_MIN_DETECTED_NUM_SOUNDS            6u

/**********************************************************************************************************************
 *  SLAC - Macros
 *********************************************************************************************************************/
/* PRQA S 3453 1 */ /* MD_MSR_19.7 */
#define ETHTRCV_30_AR7000_SLAC_CALC_TOGGLE_TIMEOUT(NumToggle)     ((ETHTRCV_30_AR7000_SLAC_T_TOGGLE * (NumToggle) * 2) + ETHTRCV_30_AR7000_SLAC_T_INIT)

/**********************************************************************************************************************
 *  SLAC - PROCESS SETTINGS
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_SLAC_START_ATTEN_REPETITIONS            3u
#define ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CCO_STATE               STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_SET_AMP_MAP_STATE           STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_SET_CAP3_PRIO               STD_OFF
#define ETHTRCV_30_AR7000_SLAC_WAIT_ON_ATTEN_RESULTS              STD_OFF
#define ETHTRCV_30_AR7000_SLAC_USE_LOCAL_AMP_MAP                  STD_OFF
/* Sum of these two parameters must not be more than ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE */
#define ETHTRCV_30_AR7000_SLAC_AMP_MAP_EXCH_DELAY                 100U
#define ETHTRCV_30_AR7000_SLAC_FAST_LINK_POLL_PERIOD              60U
#define ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATE_CBK             STD_ON
#define ETHTRCV_30_AR7000_SLAC_ENABLE_ASSOC_STATUS_ERROR_FLAG     STD_ON

/**********************************************************************************************************************
 *  SLAC - CALIBRATION AND DIAGNOSSTIC SERVICE
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_SLAC_ENABLE_CALIBRATION                 STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_GET_ATABLE_API              STD_OFF

/**********************************************************************************************************************
 *  SLAC - ISO PARAMETERS
 *********************************************************************************************************************/
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_RESPONSE                  200u
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_MNBC_TIMEOUT_MIN          6u
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_MNBC_TIMEOUT_MAX          6u
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_JOIN                      12000u
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_SESSION                   0u
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_SEQUENCE                  200u
 #define ETHTRCV_30_AR7000_SLAC_T_BATCH_INTERVAL                  2u
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_ATTEN_RESULTS             1200u
 /* ETHTRCV_30_AR7000_SLAC_T_ASSOC_ATTEN_RESULTS - n*BATCH_INTERVALL*MAINFUNCTION_PERIOD (Timeout starts with last START_ATTEN_CHAR.IND) */
 #define ETHTRCV_30_AR7000_SLAC_T_ASSOC_SOUNDING                  1120u
 #define ETHTRCV_30_AR7000_SLAC_T_AMP_MAP_EXCHANGE                200u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_REREQU                    2u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_MATCH_REREQU              2u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_ATTEN_CHAR_RSP_REREQU     1u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_MNBC_MIN                  10u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_MNBC_MAX                  10u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_MIN            0u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_DIRECT         50u
 #define ETHTRCV_30_AR7000_SLAC_C_ASSOC_SIGNALATTN_INDIRECT       70u

/**********************************************************************************************************************
 *  SLAC - FIELD CHECKS
 *********************************************************************************************************************/
#define ETHTRCV_30_AR7000_SLAC_ENABLE_RESP_TYPE_CHECK             STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_APPL_TYPE_CHECK             STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_SIGNAL_TYPE_CHECK           STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_SECURITY_TYPE_CHECK         STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_UNICAST_CHECKS          STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_MAC_FIELD_CHECKS            STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_MVF_CHECK                   STD_OFF
#define ETHTRCV_30_AR7000_SLAC_ENABLE_NUM_SOUND_CHECK             STD_OFF


/* END of Checksum include for - EthTrcv_PrecompileCRC */


/**********************************************************************************************************************
 *  ETHTRCV ECUC GLOBAL CONFIGURATION CONTAINER NAME
 *********************************************************************************************************************/
/* START of Checksum include for - EthTrcv_PrecompileCRC */


#define EthTrcv_30_Ar7000_ConfigSet                     EthTrcv_30_Ar7000_Config

/* END of Checksum include for - EthTrcv_PrecompileCRC */




/* END of Checksum exclude for - EthTrcv_PrecompileCRC */

#endif /* ETHTRCV_30_AR7000_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: EthTrcv_30_Ar7000_Cfg.h
 *********************************************************************************************************************/
 

