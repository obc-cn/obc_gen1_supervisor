/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: TcpIp
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: TcpIp_Cfg.h
 *   Generation Time: 2020-08-19 13:07:49
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined(TCPIP_CFG_H)
#define TCPIP_CFG_H

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Std_Types.h"

/**********************************************************************************************************************
 *  GENERAL DEFINES
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  TCPIP SWITCH DEFINES
 *********************************************************************************************************************/
#define TCPIP_VERSION_INFO_API                     STD_OFF
#define TCPIP_DEV_ERROR_DETECT                     STD_OFF
#define TCPIP_DEV_ERROR_REPORT                     STD_OFF

#define TCPIP_SUPPORT_DHCPV4                       STD_OFF

#define TCPIP_SUPPORT_DHCPV6                       STD_OFF
#define TCPIP_SUPPORT_DHCPV6_OPTIONS               STD_OFF

#define TCPIP_SUPPORT_TCP                          STD_ON
#define TCPIP_SUPPORT_DNS                          STD_OFF
#define TCPIP_SUPPORT_DEM                          STD_OFF
#define TCPIP_SUPPORT_IPV4                         STD_OFF
#define TCPIP_SUPPORT_IPV6                         STD_ON
#define TCPIP_SUPPORT_SOAD                         STD_OFF
#define TCPIP_SUPPORT_TLS                          STD_OFF
#define TCPIP_SUPPORT_ICMPV4                       STD_OFF
#define TCPIP_SUPPORT_DHCPV4SERVER                 STD_OFF
#define TCPIP_SUPPORT_DIAG_ACCESS_EXTENSIONS       STD_OFF
#define TCPIP_SUPPORT_MEASUREMENT_DATA             STD_OFF
#define TCPIP_SUPPORT_IPSEC                        STD_OFF

#define TCPIP_SINGLE_MAIN_FUNCTION_ENABLED         STD_ON


#define TCPIP_SUPPORT_TCP_USER_TIMEOUT_OPTION      STD_OFF
#define TCPIP_SUPPORT_TCP_CONGESTION_CONTROL       STD_OFF
#define TCPIP_SUPPORT_TCP_TIMESTAMP_OPTION         STD_OFF
#define TCPIP_SUPPORT_TCP_SELECTIVE_ACK            STD_OFF
#define TCPIP_SUPPORT_TCP_KEEP_ALIVE               STD_OFF
#define TCPIP_SUPPORT_TCP_RX_OOO                   STD_OFF
#define TCPIP_SUPPORT_TCP_NAGLE_TIMEOUT            STD_OFF

#define TCPIP_TCP_DIAG_READ_ACK_SEQ_NUM_ENABLED    STD_OFF

#define TCPIP_SUPPORT_IPV6_PRIVACY_EXTENSIONS      STD_OFF /* Not supported. */
#define TCPIP_SUPPORT_MLD                          STD_OFF /* Not supported. */
#define TCPIP_SUPPORT_IPV6_RX_FRAGMENTATION        STD_OFF
#define TCPIP_SUPPORT_IPV6_TX_FRAGMENTATION        STD_OFF
#define TCPIP_SUPPORT_NDP_DAD                      STD_ON
#define TCPIP_SUPPORT_NDP_DAD_OPTIMISTIC           STD_OFF
#define TCPIP_SUPPORT_NDP_INV_NS                   STD_OFF
#define TCPIP_SUPPORT_NDP_INV_NA                   STD_OFF
#define TCPIP_SUPPORT_NDP_INV_NA_NC_UPDATE         STD_ON
#define TCPIP_SUPPORT_NDP_NUD                      STD_ON
#define TCPIP_SUPPORT_IPV6_PATH_MTU                STD_OFF
#define TCPIP_SUPPORT_IPV6_DLISTS                  STD_OFF
#define TCPIP_SUPPORT_IPV6_MULTICAST_LOOPBACK      STD_ON
#define TCPIP_SUPPORT_ICMPV6_MSG_PARAM_PROBLEM     STD_ON
#define TCPIP_SUPPORT_ICMPV6_ECHO_REPLY            STD_OFF
#define TCPIP_SUPPORT_ICMPV6_ECHO_REQUEST          STD_OFF
#define TCPIP_SUPPORT_ICMPV6_ERROR_MESSAGES        STD_ON
#define TCPIP_SUPPORT_ICMPV6_MSG_DST_UNREACHABLE   STD_OFF
#define TCPIP_SUPPORT_NDP_OPT_RFC6106_RDNSS        STD_OFF
#define TCPIP_SUPPORT_NDP_OPT_RFC6106_DNSSL        STD_OFF
#define TCPIP_SUPPORT_NDP_ROUTER_SOLICITATIONS     STD_ON
#define TCPIP_SUPPORT_IPV6_ETHIF_ADDR_FILTER_API   STD_OFF
#define TCPIP_SUPPORT_IPV6_NVM_ADDR_STORAGE        STD_OFF
#define TCPIP_SUPPORT_IPV6_DYN_REACHABLE_TIME      STD_ON
#define TCPIP_SUPPORT_IPV6_RND_REACHABLE_TIME      STD_ON
#define TCPIP_SUPPORT_IPV6_DYN_RETRANS_TIMER       STD_ON
#define TCPIP_SUPPORT_IPV6_DYN_HOP_LIMIT           STD_ON

#define TCPIP_MAIN_FCT_PERIOD_MSEC                 5u



/**********************************************************************************************************************
 *  SYMBOLIC NAME DEFINES: LOCAL ADRESSES
 *********************************************************************************************************************/
#define TcpIpConf_TcpIpLocalAddr_TcpIpV6LocalAddr_TcpIpCtrl_AsAn (1uL)
#define TcpIpConf_TcpIpLocalAddr_TcpIpV6LocalAddr_TcpIpCtrl_LinkLocal (0uL)

/**********************************************************************************************************************
 *  SYMBOLIC NAME DEFINES: SOCKET USERS
 *********************************************************************************************************************/
#define TcpIpConf_TcpIpSocketOwner_Scc (0uL)

/**********************************************************************************************************************
 *  MODULE SPECIFIC DEFINES
 *********************************************************************************************************************/
#ifndef TCPIP_USE_DUMMY_STATEMENT
#define TCPIP_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef TCPIP_DUMMY_STATEMENT
#define TCPIP_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef TCPIP_DUMMY_STATEMENT_CONST
#define TCPIP_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef TCPIP_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define TCPIP_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef TCPIP_ATOMIC_VARIABLE_ACCESS
#define TCPIP_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef TCPIP_PROCESSOR_TC234
#define TCPIP_PROCESSOR_TC234
#endif
#ifndef TCPIP_COMP_GNU
#define TCPIP_COMP_GNU
#endif
#ifndef TCPIP_GEN_GENERATOR_MSR
#define TCPIP_GEN_GENERATOR_MSR
#endif
#ifndef TCPIP_CPUTYPE_BITORDER_LSB2MSB
#define TCPIP_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef TCPIP_CONFIGURATION_VARIANT_PRECOMPILE
#define TCPIP_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef TCPIP_CONFIGURATION_VARIANT_LINKTIME
#define TCPIP_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef TCPIP_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define TCPIP_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef TCPIP_CONFIGURATION_VARIANT
#define TCPIP_CONFIGURATION_VARIANT TCPIP_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef TCPIP_POSTBUILD_VARIANT_SUPPORT
#define TCPIP_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif

/* User Config File Start */

/* User Config File End */


#endif /* TCPIP_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: TcpIp_Cfg.h
 *********************************************************************************************************************/
