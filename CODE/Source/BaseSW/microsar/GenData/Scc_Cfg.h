/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Scc
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Scc_Cfg.h
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined(SCC_CFG_H)
#define SCC_CFG_H

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
 
#include "Std_Types.h"

#include "EthIf_Cfg.h"
#include "EthTrcv_30_Ar7000_Cfg.h"
#include "TcpIp_Cfg.h"

/**********************************************************************************************************************
 *  SCC ECUC GLOBAL CONFIGURATION CONTAINER NAME
 *********************************************************************************************************************/
#define SccConfigSet                         Scc_Config

/**********************************************************************************************************************
 *  GENERAL DEFINES
 *********************************************************************************************************************/
#ifndef SCC_USE_DUMMY_STATEMENT
#define SCC_USE_DUMMY_STATEMENT STD_ON /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef SCC_DUMMY_STATEMENT
#define SCC_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef SCC_DUMMY_STATEMENT_CONST
#define SCC_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef SCC_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define SCC_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef SCC_ATOMIC_VARIABLE_ACCESS
#define SCC_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef SCC_PROCESSOR_TC234
#define SCC_PROCESSOR_TC234
#endif
#ifndef SCC_COMP_GNU
#define SCC_COMP_GNU
#endif
#ifndef SCC_GEN_GENERATOR_MSR
#define SCC_GEN_GENERATOR_MSR
#endif
#ifndef SCC_CPUTYPE_BITORDER_LSB2MSB
#define SCC_CPUTYPE_BITORDER_LSB2MSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef SCC_CONFIGURATION_VARIANT_PRECOMPILE
#define SCC_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef SCC_CONFIGURATION_VARIANT_LINKTIME
#define SCC_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef SCC_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define SCC_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef SCC_CONFIGURATION_VARIANT
#define SCC_CONFIGURATION_VARIANT SCC_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef SCC_POSTBUILD_VARIANT_SUPPORT
#define SCC_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
 *  CONFIGURATION VARIANT
 *********************************************************************************************************************/
#define SCC_CONFIG_VARIANT                   SCC_CONFIGURATION_VARIANT


/**********************************************************************************************************************
 *  DEFINES
 *********************************************************************************************************************/

/* General Defines */
#define SCC_VERSION_INFO_API                          STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccVersionInfoApi */
#define SCC_DEV_ERROR_DETECT                          STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccDevErrorDetect */
#define SCC_DEV_ERROR_REPORT                          STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccDevErrorDetect */
                                                      
/* V2G Defines */                                     
#define SCC_V2G_PROT_VER                              1u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccV2GProtocolVersion */

/* EVSE Compatibility Defines */                      
#define SCC_IGNORE_EVSE_MAXIMUM_POWER_LIMIT           STD_ON /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableEvseCompatibility */
                                                      
/* Connection Defines */                              
#define SCC_V2GTP_MAX_RX_LEN                          4000u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccV2GSocketRxBufLen */
#define SCC_V2GTP_MAX_TX_LEN                          500u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccV2GSocketTxBufLen */
#define SCC_TCP_RX_BUF_LEN                            4000u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccV2GSocketRxBufLen */
#define SCC_TCP_TX_BUF_LEN                            500u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccV2GSocketTxBufLen */
#define SCC_EXI_RX_STREAM_PBUF_ELEMENTS               2u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccV2GTPMaxTcpRxSegments */
                                                      
#define SCC_SCHEMA_DIN                                1u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccSchemaPriorityDin */
#define SCC_SCHEMA_ISO                                2u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccSchemaPriorityIsoFdis */
#define SCC_SCHEMA_ISO_ED2                            0u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccSchemaPriorityIsoEd2 */
                                                      
#define SCC_CHARGING_AC                               STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableAcCharging */
#define SCC_CHARGING_DC                               STD_ON /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableDcCharging */
#define SCC_CHARGING_WPT                              STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableWptCharging */
#define SCC_CHARGING_AC_BPT                           STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableBptCharging && DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableAcCharging */
#define SCC_CHARGING_DC_BPT                           STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableBptCharging && DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableDcCharging */
                                                      
#define SCC_ENABLE_TLS                                STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccEnableTls */
#define SCC_ENABLE_PNC_CHARGING                       STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnablePncCharging */
#define SCC_ENABLE_CERTIFICATE_UPDATE                 STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccPncConfig/SccEnableCertificateUpdate */
#define SCC_ENABLE_CERTIFICATE_INSTALLATION           STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccPncConfig/SccEnableCertificateInstallation */
#define SCC_ENABLE_ONGOING_CALLBACKS                  STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableOngoingCallbacks */
#define SCC_ENABLE_CPS_CHECK                          STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccPncConfig/SccCertCpsCheckEnabled */
#define SCC_ENABLE_EMAID_VALIDATION                   STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccPncConfig/SccEnableEMAIDValidation */
#define SCC_ENABLE_STATE_MACHINE                      STD_ON /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableStateMachine */
#define SCC_ENABLE_STATEM_APPL_CONTROLLED_FLOW        STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccApplicationControlledFlow */
#define SCC_ENABLE_SLAC_HANDLING                      STD_ON /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccEnableSlacHandling */

#define SCC_DOMAIN_COMPONENT_TLS_LEAF_LENGTH          0u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccPncConfig/SccDomainComponentTlsLeafCert */

#define SCC_EXI_STRUCT_BUF_LEN                        10000u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccBufferConfig/SccExiStructBufLen */
#define SCC_EXI_TEMP_BUF_LEN                          500u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccBufferConfig/SccExiTempBufLen */
#define SCC_ENABLE_INTERNAL_CHARGING_PROFILE_BUFFER   STD_OFF /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccBufferConfig/SccEnableInternalChargingProfileBuffer */

#define SCC_IPV6_ADDRESS                              TcpIpConf_TcpIpLocalAddr_TcpIpV6LocalAddr_TcpIpCtrl_AsAn /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccIPv6AddressRef */
#define SCC_CTRL_IDX                                  EthIfConf_EthIfController_EthIfController /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccConnectionConfig/SccEthIfCtrlRef */
#define SCC_TRANSCEIVER_INDEX                         EthTrcvConf_EthTrcvConfig_EthTrcvConfig /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccSlacConfig/SccTrcvRef */

/**********************************************************************************************************************
 *  PRECOMPILE CONFIG
 *********************************************************************************************************************/

#define SCC_MAIN_FUNCTION_PERIOD_MS             5u

#define SCC_NUM_OF_DYN_CONFIG_PARAMS            0u

/**********************************************************************************************************************
 *  NVM BLOCK DEFINES
 *********************************************************************************************************************/
#define SCC_SESSION_ID_NVM_BLOCK_LEN            9uL /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccNvmConfig/SccSessionIdNvmBlock */
#define SCC_SESSION_ID_NVM_BLOCK                NvMConf_NvMBlockDescriptor_SCCSessionIDNvMBlockDescriptor /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccNvmConfig/SccSessionIdNvmBlock */


#define SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_REQ      36864u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccV2GTpHdrPayloadTypeSdpReq */
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_SDP_RES      36865u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccV2GTpHdrPayloadTypeSdpRes */
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_SAP          32769u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccV2GTpHdrPayloadTypeSap */
#define SCC_V2GTP_HDR_PAYLOAD_TYPE_EXI          32769u /* DefinitionRef: /MICROSAR/Scc/SccGeneral/SccV2GConfig/SccV2GTpHdrPayloadTypeExi */

#endif /* SCC_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Scc_Cfg.h
 *********************************************************************************************************************/

