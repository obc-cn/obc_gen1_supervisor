/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Scc
 *           Program: MSR_Vector_SLP4
 *          Customer: MAHLE Electronics S.A.
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: SAK-TC234L AC
 *    License Scope : The usage is restricted to CBD1900270_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Scc_Interface_Cfg.h
 *   Generation Time: 2020-08-19 13:07:45
 *           Project: OBCP11 - Version 1.0
 *          Delivery: CBD1900270_D04
 *      Tool Version: DaVinci Configurator  5.20.55 SP4
 *
 *
 *********************************************************************************************************************/

#if !defined(SCC_INTERFACE_CFG_H)
#define SCC_INTERFACE_CFG_H

/**********************************************************************************************************************
 *  INCLUDE
 *********************************************************************************************************************/
#include "Scc_StateM_Vector.h"

#include "Dem.h"



/**********************************************************************************************************************
 *  MISRA & PC-lint
 *********************************************************************************************************************/
/* PRQA S 3453,3458 FUNCTION_LIKE_MACROS */ /* MD_MSR_19.4,MD_MSR_19.7 */

/**********************************************************************************************************************
 * RX INTERFACE
 *********************************************************************************************************************/
 /* Core */
#define Scc_Get_Core_CyclicMsgTrigRx(DataPtr)                                  Scc_StateM_Get_Core_CyclicMsgTrigRx(DataPtr) 
#define Scc_Get_Core_MsgTrig(DataPtr)                                          Scc_StateM_Get_Core_MsgTrig(DataPtr) 
#define Scc_Get_Core_SDPSecurityRx(DataPtr)                                    Scc_StateM_Get_Core_SDPSecurityRx(DataPtr) 
#define Scc_Get_Core_ForceSAPSchema(DataPtr)                                   Appl_SccCbk_Get_Core_ForceSAPSchema(DataPtr) 


 /* SLAC */
#define Scc_Get_SLAC_StartMode(DataPtr)                                        Scc_StateM_Get_SLAC_StartMode(DataPtr) 
#define Scc_Get_SLAC_QCAIdleTimer(DataPtr)                                     Scc_StateM_Get_SLAC_QCAIdleTimer(DataPtr) 


 /* StateM */
#define Scc_Get_StateM_ChargingControl(DataPtr)                                Appl_SccCbk_Get_StateM_ChargingControl(DataPtr) 
#define Scc_Get_StateM_ControlPilotState(DataPtr)                              Appl_SccCbk_Get_StateM_ControlPilotState(DataPtr) 
#define Scc_Get_StateM_EnergyTransferMode(DataPtr)                             Appl_SccCbk_Get_StateM_EnergyTransferMode(DataPtr) 
#define Scc_Get_StateM_EVTimeStamp(DataPtr)                                    Appl_SccCbk_Get_StateM_EVTimeStamp(DataPtr) 
#define Scc_Get_StateM_FlowControl(DataPtr)                                    Appl_SccCbk_Get_StateM_FlowControl(DataPtr) 
#define Scc_Get_StateM_FunctionControl(DataPtr)                                Appl_SccCbk_Get_StateM_FunctionControl(DataPtr) 
#define Scc_Get_StateM_PWMState(DataPtr)                                       Appl_SccCbk_Get_StateM_PWMState(DataPtr) 


 /* ISO common */
#define Scc_Get_ISO_ChargeProgress(DataPtr)                                    Scc_StateM_Get_ISO_ChargeProgress(DataPtr) 
#define Scc_Get_ISO_ChargingProfilePtr(DataPtr, Flag)                          Appl_SccCbk_Get_ISO_ChargingProfilePtr(DataPtr, Flag) 
#define Scc_Get_ISO_ChargingSession(DataPtr)                                   Appl_SccCbk_Get_ISO_ChargingSession(DataPtr) 
#define Scc_Get_ISO_DepartureTime(DataPtr, Flag)                               Appl_SccCbk_Get_ISO_DepartureTime(DataPtr, Flag) 
#define Scc_Get_ISO_MaxEntriesSAScheduleTuple(DataPtr, Flag)                   Appl_SccCbk_Get_ISO_MaxEntriesSAScheduleTuple(DataPtr, Flag) 
#define Scc_Get_ISO_RequestedEnergyTransferMode(DataPtr)                       Scc_StateM_Get_ISO_RequestedEnergyTransferMode(DataPtr) 
#define Scc_Get_ISO_SAScheduleTupleID(DataPtr)                                 Appl_SccCbk_Get_ISO_SAScheduleTupleID(DataPtr) 
#define Scc_Get_ISO_SkipSAScheduleListCheck(DataPtr)                           Appl_SccCbk_Get_ISO_SkipSAScheduleListCheck(DataPtr) 
#define Scc_Get_ISO_PnC_SkipSalesTariffSignatureCheck(DataPtr)                 Appl_SccCbk_Get_ISO_PnC_SkipSalesTariffSignatureCheck(DataPtr) 
#define Scc_Get_ISO_SelectedPaymentOption(DataPtr)                             Scc_StateM_Get_ISO_SelectedPaymentOption(DataPtr) 
#define Scc_Get_ISO_SelectedServiceListPtr(DataPtr)                            Scc_StateM_Get_ISO_SelectedServiceListPtr(DataPtr) 
#define Scc_Get_ISO_ServiceCategory(DataPtr, Flag)                             Appl_SccCbk_Get_ISO_ServiceCategory(DataPtr, Flag) 
#define Scc_Get_ISO_ServiceIDRx(DataPtr)                                       Scc_StateM_Get_ISO_ServiceIDRx(DataPtr) 
#define Scc_Get_ISO_ServiceScope(DataPtr, Flag)                                Appl_SccCbk_Get_ISO_ServiceScope(DataPtr, Flag) 
#define Scc_Get_ISO_PnC_CurrentTime(DataPtr)                                   Appl_SccCbk_Get_ISO_PnC_CurrentTime(DataPtr) 



 /* ISO DC */
#define Scc_Get_ISO_DC_BulkChargingComplete(DataPtr, Flag)                     Appl_SccCbk_Get_IsoDin_DC_BulkChargingComplete(DataPtr, Flag) 
#define Scc_Get_ISO_DC_BulkSOC(DataPtr, Flag)                                  Appl_SccCbk_Get_IsoDin_DC_BulkSOC(DataPtr, Flag) 
#define Scc_Get_ISO_DC_ChargingComplete(DataPtr)                               Appl_SccCbk_Get_IsoDin_DC_ChargingComplete(DataPtr) 
#define Scc_Get_ISO_DC_EVEnergyCapacity(DataPtr, Flag)                         Appl_SccCbk_Get_IsoDin_DC_EVEnergyCapacity(DataPtr, Flag) 
#define Scc_Get_ISO_DC_EVEnergyRequest(DataPtr, Flag)                          Appl_SccCbk_Get_IsoDin_DC_EVEnergyRequest(DataPtr, Flag) 
#define Scc_Get_ISO_DC_EVErrorCode(DataPtr)                                    Appl_SccCbk_Get_ISO_DC_EVErrorCode(DataPtr) 
#define Scc_Get_ISO_DC_EVMaximumCurrentLimit(DataPtr, Flag)                    Appl_SccCbk_Get_IsoDin_DC_EVMaximumCurrentLimit(DataPtr, Flag) 
#define Scc_Get_ISO_DC_EVMaximumPowerLimit(DataPtr, Flag)                      Appl_SccCbk_Get_IsoDin_DC_EVMaximumPowerLimit(DataPtr, Flag) 
#define Scc_Get_ISO_DC_EVMaximumVoltageLimit(DataPtr, Flag)                    Appl_SccCbk_Get_IsoDin_DC_EVMaximumVoltageLimit(DataPtr, Flag) 
#define Scc_Get_ISO_DC_EVPowerDeliveryParameterFlag(DataPtr)                   Appl_SccCbk_Get_IsoDin_DC_EVPowerDeliveryParameterFlag(DataPtr) 
#define Scc_Get_ISO_DC_EVReady(DataPtr)                                        Appl_SccCbk_Get_IsoDin_DC_EVReady(DataPtr) 
#define Scc_Get_ISO_DC_EVRESSSOC(DataPtr)                                      Appl_SccCbk_Get_IsoDin_DC_EVRESSSOC(DataPtr) 
#define Scc_Get_ISO_DC_EVTargetCurrent(DataPtr)                                Appl_SccCbk_Get_IsoDin_DC_EVTargetCurrent(DataPtr) 
#define Scc_Get_ISO_DC_EVTargetVoltage(DataPtr)                                Appl_SccCbk_Get_IsoDin_DC_EVTargetVoltage(DataPtr) 
#define Scc_Get_ISO_DC_FullSOC(DataPtr, Flag)                                  Appl_SccCbk_Get_IsoDin_DC_FullSOC(DataPtr, Flag) 
#define Scc_Get_ISO_DC_RemainingTimeToBulkSoC(DataPtr, Flag)                   Appl_SccCbk_Get_IsoDin_DC_RemainingTimeToBulkSoC(DataPtr, Flag) 
#define Scc_Get_ISO_DC_RemainingTimeToFullSoC(DataPtr, Flag)                   Appl_SccCbk_Get_IsoDin_DC_RemainingTimeToFullSoC(DataPtr, Flag) 


 /* DIN */
#define Scc_Get_DIN_BulkChargingComplete(DataPtr, Flag)                        Appl_SccCbk_Get_IsoDin_DC_BulkChargingComplete(DataPtr, Flag) 
#define Scc_Get_DIN_BulkSOC(DataPtr, Flag)                                     Appl_SccCbk_Get_IsoDin_DC_BulkSOC(DataPtr, Flag) 
#define Scc_Get_DIN_ChargingComplete(DataPtr)                                  Appl_SccCbk_Get_IsoDin_DC_ChargingComplete(DataPtr) 
#define Scc_Get_DIN_ChargingProfilePtr(DataPtr, Flag)                          Appl_SccCbk_Get_DIN_ChargingProfilePtr(DataPtr, Flag) 
#define Scc_Get_DIN_EVCabinConditioning(DataPtr, Flag)                         Appl_SccCbk_Get_DIN_EVCabinConditioning(DataPtr, Flag) 
#define Scc_Get_DIN_EVEnergyCapacity(DataPtr, Flag)                            Appl_SccCbk_Get_IsoDin_DC_EVEnergyCapacity(DataPtr, Flag) 
#define Scc_Get_DIN_EVEnergyRequest(DataPtr, Flag)                             Appl_SccCbk_Get_IsoDin_DC_EVEnergyRequest(DataPtr, Flag) 
#define Scc_Get_DIN_EVErrorCode(DataPtr)                                       Appl_SccCbk_Get_DIN_EVErrorCode(DataPtr) 
#define Scc_Get_DIN_EVMaximumCurrentLimit(DataPtr, Flag)                       Appl_SccCbk_Get_IsoDin_DC_EVMaximumCurrentLimit(DataPtr, Flag) 
#define Scc_Get_DIN_EVMaximumPowerLimit(DataPtr, Flag)                         Appl_SccCbk_Get_IsoDin_DC_EVMaximumPowerLimit(DataPtr, Flag) 
#define Scc_Get_DIN_EVMaximumVoltageLimit(DataPtr, Flag)                       Appl_SccCbk_Get_IsoDin_DC_EVMaximumVoltageLimit(DataPtr, Flag) 
#define Scc_Get_DIN_EVPowerDeliveryParameterFlag(DataPtr)                      Appl_SccCbk_Get_IsoDin_DC_EVPowerDeliveryParameterFlag(DataPtr) 
#define Scc_Get_DIN_EVReady(DataPtr)                                           Appl_SccCbk_Get_IsoDin_DC_EVReady(DataPtr) 
#define Scc_Get_DIN_EVRESSConditioning(DataPtr, Flag)                          Appl_SccCbk_Get_DIN_EVRESSConditioning(DataPtr, Flag) 
#define Scc_Get_DIN_EVRESSSOC(DataPtr)                                         Appl_SccCbk_Get_IsoDin_DC_EVRESSSOC(DataPtr) 
#define Scc_Get_DIN_EVTargetCurrent(DataPtr)                                   Appl_SccCbk_Get_IsoDin_DC_EVTargetCurrent(DataPtr) 
#define Scc_Get_DIN_EVTargetVoltage(DataPtr)                                   Appl_SccCbk_Get_IsoDin_DC_EVTargetVoltage(DataPtr) 
#define Scc_Get_DIN_FullSOC(DataPtr, Flag)                                     Appl_SccCbk_Get_IsoDin_DC_FullSOC(DataPtr, Flag) 
#define Scc_Get_DIN_ReadyToChargeState(DataPtr)                                Scc_StateM_Get_DIN_ReadyToChargeState(DataPtr) 
#define Scc_Get_DIN_RemainingTimeToBulkSoC(DataPtr, Flag)                      Appl_SccCbk_Get_IsoDin_DC_RemainingTimeToBulkSoC(DataPtr, Flag) 
#define Scc_Get_DIN_RemainingTimeToFullSoC(DataPtr, Flag)                      Appl_SccCbk_Get_IsoDin_DC_RemainingTimeToFullSoC(DataPtr, Flag) 
#define Scc_Get_DIN_RequestedEnergyTransferMode(DataPtr)                       Scc_StateM_Get_DIN_RequestedEnergyTransferMode(DataPtr) 
#define Scc_Get_DIN_ServiceCategory(DataPtr, Flag)                             Scc_StateM_Get_DIN_ServiceCategory(DataPtr, Flag) 


 /* Random function for SLAC */
#define Scc_Get_RandomData(DataPtr, DataLen)                                   ApplRand_GetRandArray(DataPtr, DataLen)

/**********************************************************************************************************************
 * TX INTERFACE
 *********************************************************************************************************************/
 /* Core */
#define Scc_Set_Core_CyclicMsgRcvd(Data)                                       { Scc_StateM_Set_Core_CyclicMsgRcvd(Data); Appl_SccCbk_Set_Core_CyclicMsgRcvd(Data); } 
#define Scc_Set_Core_CyclicMsgTrigTx(Data)                                     { Scc_StateM_Set_Core_CyclicMsgTrigTx(Data); Appl_SccCbk_Set_Core_CyclicMsgTrigTx(Data); } 
#define Scc_Set_Core_IPAssigned(Data)                                          { Scc_StateM_Set_Core_IPAssigned(Data); Appl_SccCbk_Set_Core_IPAssigned(Data); } 
#define Scc_Set_Core_MsgState(Data)                                            Appl_SccCbk_Set_Core_MsgState(Data) 
#define Scc_Set_Core_MsgStatus(Data)                                           { Scc_StateM_Set_Core_MsgStatus(Data); Appl_SccCbk_Set_Core_MsgStatus(Data); } 
#define Scc_Set_Core_SAPResponseCode(Data)                                     Appl_SccCbk_Set_Core_SAPResponseCode(Data) 
#define Scc_Set_Core_SAPSchemaID(Data)                                         { Scc_StateM_Set_Core_SAPSchemaID(Data); Appl_SccCbk_Set_Core_SAPSchemaID(Data); } 
#define Scc_Set_Core_SDPTransportProtocol(Data)                                Appl_SccCbk_Set_Core_SDPTransportProtocol(Data) 
#define Scc_Set_Core_SDPSecurityTx(Data)                                       { Scc_StateM_Set_Core_SDPSecurityTx(Data); Appl_SccCbk_Set_Core_SDPSecurityTx(Data); } 
#define Scc_Set_Core_SECCIPAddress(DataPtr)                                    Appl_SccCbk_Set_Core_SECCIPAddress(DataPtr) 
#define Scc_Set_Core_SECCPort(Data)                                            Appl_SccCbk_Set_Core_SECCPort(Data) 
#define Scc_Set_Core_StackError(Data)                                          { Scc_StateM_Set_Core_StackError(Data); Appl_SccCbk_Set_Core_StackError(Data); } 
#define Scc_Set_Core_TCPSocketState(Data)                                      Appl_SccCbk_Set_Core_TCPSocketState(Data) 
#define Scc_Set_Core_TrcvLinkState(Data)                                       { Scc_StateM_Set_Core_TrcvLinkState(Data); Appl_SccCbk_Set_Core_TrcvLinkState(Data); } 
#define Scc_Set_Core_TLS_CertChain(DataPtr, Flag)                              Appl_SccCbk_Set_Core_TLS_CertChain(DataPtr, Flag) 
#define Scc_Set_Core_TLS_UnknownCALeafCert(DataPtr)                            Appl_SccCbk_Set_Core_TLS_UnknownCALeafCert(DataPtr) 
#define Scc_Set_Core_V2GResponse(DataPtr)                                      Appl_SccCbk_Set_Core_V2GResponse(DataPtr) 
#define Scc_Set_Core_V2GRequest(DataPtr)                                       Appl_SccCbk_Set_Core_V2GRequest(DataPtr) 


 /* SLAC */
#define Scc_Set_SLAC_AssociationStatus(Data)                                   Appl_SccCbk_Set_SLAC_AssociationStatus(Data) 
#define Scc_Set_SLAC_NMK(DataPtr)                                               
#define Scc_Set_SLAC_NID(DataPtr)                                               
#define Scc_Set_SLAC_ToggleRequest(Data)                                        


 /* StateM */
#define Scc_Set_StateM_EnergyTransferModeFlags(Data)                           Appl_SccCbk_Set_StateM_EnergyTransferModeFlags(Data) 
#define Scc_Set_StateM_InternetAvailable(Data)                                 Appl_SccCbk_Set_StateM_InternetAvailable(Data) 
#define Scc_Set_StateM_StateMachineError(Data)                                 Appl_SccCbk_Set_StateM_StateMachineError(Data) 
#define Scc_Set_StateM_StateMachineMessageState(Data)                          Appl_SccCbk_Set_StateM_StateMachineMessageState(Data) 
#define Scc_Set_StateM_StateMachineStatus(Data)                                Appl_SccCbk_Set_StateM_StateMachineStatus(Data) 


 /* ISO common */
#define Scc_Set_ISO_ChargeService(DataPtr)                                     { Scc_StateM_Set_ISO_ChargeService(DataPtr); Appl_SccCbk_Set_ISO_ChargeService(DataPtr); } 
#define Scc_Set_ISO_EVSEID(DataPtr)                                            Appl_SccCbk_Set_ISO_EVSEID(DataPtr) 
#define Scc_Set_ISO_EVSENotification(Data)                                     Appl_SccCbk_Set_ISO_EVSENotification(Data) 
#define Scc_Set_ISO_EVSEProcessing(Data)                                       { Scc_StateM_Set_ISO_EVSEProcessing(Data); Appl_SccCbk_Set_ISO_EVSEProcessing(Data); } 
#define Scc_Set_ISO_EVSETimeStamp(Data, Flag)                                  Appl_SccCbk_Set_ISO_EVSETimeStamp(Data, Flag) 
#define Scc_Set_ISO_MeterInfo(DataPtr, Flag)                                   Appl_SccCbk_Set_ISO_MeterInfo(DataPtr, Flag) 
#define Scc_Set_ISO_Notification(DataPtr, Flag)                                Appl_SccCbk_Set_ISO_Notification(DataPtr, Flag) 
#define Scc_Set_ISO_NotificationMaxDelay(Data)                                 Appl_SccCbk_Set_ISO_NotificationMaxDelay(Data) 
#define Scc_Set_ISO_PaymentOptionList(DataPtr)                                 { Scc_StateM_Set_ISO_PaymentOptionList(DataPtr); Appl_SccCbk_Set_ISO_PaymentOptionList(DataPtr); } 
#define Scc_Set_ISO_ResponseCode(Data)                                         Appl_SccCbk_Set_ISO_ResponseCode(Data) 
#define Scc_Set_ISO_SAScheduleList(DataPtr)                                    Appl_SccCbk_Set_ISO_SAScheduleList(DataPtr) 
#define Scc_Set_ISO_ServiceIDTx(Data)                                          { Scc_StateM_Set_ISO_ServiceIDTx(Data); Appl_SccCbk_Set_ISO_ServiceIDTx(Data); } 
#define Scc_Set_ISO_ServiceList(DataPtr, Flag)                                 { Scc_StateM_Set_ISO_ServiceList(DataPtr, Flag); Appl_SccCbk_Set_ISO_ServiceList(DataPtr, Flag); } 
#define Scc_Set_ISO_ServiceParameterList(DataPtr, Flag)                        { Scc_StateM_Set_ISO_ServiceParameterList(DataPtr, Flag); Appl_SccCbk_Set_ISO_ServiceParameterList(DataPtr, Flag); } 
#define Scc_Set_ISO_PnC_ReceiptRequired(Data, Flag)                            Scc_StateM_Set_ISO_PnC_ReceiptRequired(Data, Flag) 
#define Scc_Set_ISO_PnC_RetryCounter(Data, Flag)                                
#define Scc_Set_ISO_SessionID(DataPtr)                                         Appl_SccCbk_Set_ISO_SessionID(DataPtr) 



 /* ISO DC */
#define Scc_Set_ISO_DC_EVSECurrentLimitAchieved(Data)                          Appl_SccCbk_Set_IsoDin_DC_EVSECurrentLimitAchieved(Data) 
#define Scc_Set_ISO_DC_EVSECurrentRegulationTolerance(DataPtr, Flag)           Appl_SccCbk_Set_IsoDin_DC_EVSECurrentRegulationTolerance(DataPtr, Flag) 
#define Scc_Set_ISO_DC_EVSEEnergyToBeDelivered(DataPtr, Flag)                  Appl_SccCbk_Set_IsoDin_DC_EVSEEnergyToBeDelivered(DataPtr, Flag) 
#define Scc_Set_ISO_DC_EVSEIsolationStatus(Data, Flag)                         Appl_SccCbk_Set_ISO_DC_EVSEIsolationStatus(Data, Flag) 
#define Scc_Set_ISO_DC_EVSEMaximumCurrentLimit(DataPtr, Flag)                  Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumCurrentLimit(DataPtr, Flag) 
#define Scc_Set_ISO_DC_EVSEMaximumPowerLimit(DataPtr, Flag)                    Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumPowerLimit(DataPtr, Flag) 
#define Scc_Set_ISO_DC_EVSEMaximumVoltageLimit(DataPtr, Flag)                  Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumVoltageLimit(DataPtr, Flag) 
#define Scc_Set_ISO_DC_EVSEMinimumCurrentLimit(DataPtr)                        Appl_SccCbk_Set_IsoDin_DC_EVSEMinimumCurrentLimit(DataPtr) 
#define Scc_Set_ISO_DC_EVSEMinimumVoltageLimit(DataPtr)                        Appl_SccCbk_Set_IsoDin_DC_EVSEMinimumVoltageLimit(DataPtr) 
#define Scc_Set_ISO_DC_EVSEPeakCurrentRipple(DataPtr)                          Appl_SccCbk_Set_IsoDin_DC_EVSEPeakCurrentRipple(DataPtr) 
#define Scc_Set_ISO_DC_EVSEPowerLimitAchieved(Data)                            Appl_SccCbk_Set_IsoDin_DC_EVSEPowerLimitAchieved(Data) 
#define Scc_Set_ISO_DC_EVSEPresentCurrent(DataPtr)                             Appl_SccCbk_Set_IsoDin_DC_EVSEPresentCurrent(DataPtr) 
#define Scc_Set_ISO_DC_EVSEPresentVoltage(DataPtr)                             Appl_SccCbk_Set_IsoDin_DC_EVSEPresentVoltage(DataPtr) 
#define Scc_Set_ISO_DC_EVSEStatusCode(Data)                                    Appl_SccCbk_Set_ISO_DC_EVSEStatusCode(Data) 
#define Scc_Set_ISO_DC_EVSEVoltageLimitAchieved(Data)                          Appl_SccCbk_Set_IsoDin_DC_EVSEVoltageLimitAchieved(Data) 


 /* DIN */
#define Scc_Set_DIN_ChargeService(DataPtr)                                     { Scc_StateM_Set_DIN_ChargeService(DataPtr); Appl_SccCbk_Set_DIN_ChargeService(DataPtr); } 
#define Scc_Set_DIN_DateTimeNow(Data, Flag)                                    Appl_SccCbk_Set_DIN_DateTimeNow(Data, Flag) 
#define Scc_Set_DIN_EVSECurrentLimitAchieved(Data)                             Appl_SccCbk_Set_IsoDin_DC_EVSECurrentLimitAchieved(Data) 
#define Scc_Set_DIN_EVSECurrentRegulationTolerance(DataPtr, Flag)              Appl_SccCbk_Set_IsoDin_DC_EVSECurrentRegulationTolerance(DataPtr, Flag) 
#define Scc_Set_DIN_EVSEEnergyToBeDelivered(DataPtr, Flag)                     Appl_SccCbk_Set_IsoDin_DC_EVSEEnergyToBeDelivered(DataPtr, Flag) 
#define Scc_Set_DIN_EVSEID(DataPtr)                                            Appl_SccCbk_Set_DIN_EVSEID(DataPtr) 
#define Scc_Set_DIN_EVSEIsolationStatus(Data, Flag)                            Appl_SccCbk_Set_DIN_EVSEIsolationStatus(Data, Flag) 
#define Scc_Set_DIN_EVSEMaximumCurrentLimit(DataPtr, Flag)                     Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumCurrentLimit(DataPtr, Flag) 
#define Scc_Set_DIN_EVSEMaximumPowerLimit(DataPtr, Flag)                       Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumPowerLimit(DataPtr, Flag) 
#define Scc_Set_DIN_EVSEMaximumVoltageLimit(DataPtr, Flag)                     Appl_SccCbk_Set_IsoDin_DC_EVSEMaximumVoltageLimit(DataPtr, Flag) 
#define Scc_Set_DIN_EVSEMinimumCurrentLimit(DataPtr)                           Appl_SccCbk_Set_IsoDin_DC_EVSEMinimumCurrentLimit(DataPtr) 
#define Scc_Set_DIN_EVSEMinimumVoltageLimit(DataPtr)                           Appl_SccCbk_Set_IsoDin_DC_EVSEMinimumVoltageLimit(DataPtr) 
#define Scc_Set_DIN_EVSENotification(Data)                                     Appl_SccCbk_Set_DIN_EVSENotification(Data) 
#define Scc_Set_DIN_EVSEPeakCurrentRipple(DataPtr)                             Appl_SccCbk_Set_IsoDin_DC_EVSEPeakCurrentRipple(DataPtr) 
#define Scc_Set_DIN_EVSEPowerLimitAchieved(Data)                               Appl_SccCbk_Set_IsoDin_DC_EVSEPowerLimitAchieved(Data) 
#define Scc_Set_DIN_EVSEPresentCurrent(DataPtr)                                Appl_SccCbk_Set_IsoDin_DC_EVSEPresentCurrent(DataPtr) 
#define Scc_Set_DIN_EVSEPresentVoltage(DataPtr)                                Appl_SccCbk_Set_IsoDin_DC_EVSEPresentVoltage(DataPtr) 
#define Scc_Set_DIN_EVSEProcessing(Data)                                       { Scc_StateM_Set_DIN_EVSEProcessing(Data); Appl_SccCbk_Set_DIN_EVSEProcessing(Data); } 
#define Scc_Set_DIN_EVSEStatusCode(Data)                                       Appl_SccCbk_Set_DIN_EVSEStatusCode(Data) 
#define Scc_Set_DIN_EVSEVoltageLimitAchieved(Data)                             Appl_SccCbk_Set_IsoDin_DC_EVSEVoltageLimitAchieved(Data) 
#define Scc_Set_DIN_NotificationMaxDelay(Data)                                 Appl_SccCbk_Set_DIN_NotificationMaxDelay(Data) 
#define Scc_Set_DIN_ResponseCode(Data)                                         Appl_SccCbk_Set_DIN_ResponseCode(Data) 
#define Scc_Set_DIN_SAScheduleList(DataPtr)                                    Appl_SccCbk_Set_DIN_SAScheduleList(DataPtr) 
#define Scc_Set_DIN_SessionID(DataPtr)                                         Appl_SccCbk_Set_DIN_SessionID(DataPtr) 



/**********************************************************************************************************************
 *  DEM EVENTS
 *********************************************************************************************************************/
#define Scc_DemReportErrorStatusFailed(ErrId)        Dem_ReportErrorStatus((ErrId), DEM_EVENT_STATUS_FAILED)
#define Scc_DemReportErrorStatusPassed(ErrId)        Dem_ReportErrorStatus((ErrId), DEM_EVENT_STATUS_PASSED)

/**********************************************************************************************************************
 *  MISRA & PC-lint
 *********************************************************************************************************************/
/* PRQA L:FUNCTION_LIKE_MACROS */

#endif /* SCC_INTERFACE_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Scc_Interface_Cfg.h
 *********************************************************************************************************************/

