/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLFM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApLFM
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLFM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup LFM
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief DCLV Fault Management
 * \details This module manages the fault detection for the DCLV. And generates
 * the DCLVerror signal that stops the DCLV peripheral.
 *
 * \{
 * \file  LFM.c
 * \brief  Generic code of the LFM module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApLFM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

#define LFM_CLEAN_FAULT_TIME 1000U
#define LFM_505V		5050U
#define LFM_GAIN_10 10U
#define LFM_GAIN_100 100U
#define LFM_FAULT_NUMBER 14U

/** Forbidden range for DCDC_OutputVoltage*/
#define LFM_FORBIDDEN_RANGE_DCDC_OutputVoltage	0xFFU
/** Battery offset in dV*/
#define LFM_LVV_OFFSET				40U
/*DCLV measure current invalid*/
#define LFM_DCLV_OUT_CURRENT_INVALID	0xFFFUL

/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
typedef struct
{
		boolean State;
		uint16 Counter;
}LFM_debounce_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Faults recorded during the last charge cycle.*/
static IdtFaultLevel LFM_SnapShot[LFM_FAULT_NUMBER];
/** Current Faults state record.*/
static IdtFaultLevel LFM_FaultVar[LFM_FAULT_NUMBER];
/** Restart counters Variables*/
static uint32 LFM_RestartCounter[LFM_FAULT_NUMBER];
/** Retry counter*/
static uint8 LFM_RetryCounter;

/*Internal Calibratables.*/
static uint32 LFM_DCLVFaultTimeToRetryDefault;
static uint32 LFM_DCLVFaultTimeToRetryShortCircuit;

static uint16 LFM_DebounceOvercurrentDCHV;
static uint16 LFM_TimeBatteryVoltageSafety;
static uint16 LFM_BatteryVoltageSafetyOVLimit2_ConfirmTime;
static uint16 LFM_BatteryVoltageSafetyOVLimit1_ConfirmTime;

static IdtThresholdLVOvercurrentHW LFM_ThresholdLVOvercurrentHW;
static IdtBatteryVoltageSafetyOVLimit1_Threshold LFM_BatteryVoltageSafetyOVLimit1_Threshold;
static IdtBatteryVoltageSafetyOVLimit2_Threshold LFM_BatteryVoltageSafetyOVLimit2_Threshold;
static IdtABSVehSpdThresholdOV LFM_ABSVehSpdThresholdOV;

static boolean LFM_HWErrors_Fault = FALSE;
static boolean LFM_InternalCom = FALSE;

#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static uint8 LFM_DebugData[4] = {0,0,0,0};
static uint8 LFM_UDS_HWFaultData[4] = {0,0,0,0};
static boolean LFM_CleanFaults_Condition=FALSE;

#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/** Individual fault analysis.*/
static IdtFaultLevel LFM_IndividualCheck(void);
/** Update the fault snapshot.*/
static void LFM_CleanCounters(void);
/** Check Second level promotion.*/
static boolean LFM_CheckPromotion(void);
/**
 * \brief Get fault level
 * \details This function resturs the fault level depending on the individual faults detected.
 * \param[in] firstLevel Firt level fault
 * \param[in] secondLevel Second level fault
 * \param[in] thirdlevel Third level fault
 * \return Fault level
 */
static IdtFaultLevel LFM_GetFaultLevel(boolean firstLevel, boolean secondLevel, boolean thirdlevel);
/**
 * \brief Debounce
 * \details Debounce function.
 * \param[in] input Input
 * \param[in] ConfirmTime Confirm Time
 * \param[in] HealTime Heal Time
 * \param[in|out] Debounce Debounce Object
 */
static void LFM_debounce(boolean Input, uint16 ConfirmTime, uint16 HealTime, LFM_debounce_t *Debounce);
/**
 * \brief Individual fault report
 * \details this function is used to force the Second level fault while the time
 * to restart and then force it to not fault.
 */
static IdtFaultLevel LFM_OutputReport(IdtFaultLevel ErrorLevel,uint32 RestartTime,uint32 *RestartCounter);
/** Report Fault state.*/
static DCDC_Fault LFM_ReportDCLVfault(IdtFaultLevel FaultLevel);

/** Individual fault function for: OvervoltageHV. */
static void LFM_Function_OvervoltageHV(uint8 FaultIndex);
/** Individual fault function for: OvervoltageLimit1. */
static void LFM_Function_OvervoltageLimit1(uint8 FaultIndex);
/** Individual fault function for: OvervoltageLimit2. */
static void LFM_Function_OvervoltageLimit2(uint8 FaultIndex);
/** Individual fault function for: OutputOvercurrent. */
static void LFM_Function_OutputOvercurrent(uint8 FaultIndex);
/** void fault function for: Overvoltage. */
static void LFM_Function_Overvoltage(uint8 FaultIndex, IdtFaultLevel OVlimit1, IdtFaultLevel OVlimit2);
/** Individual fault function for: OutputShortCircuitLV. */
static void LFM_Function_OutputShortCircuitLV(uint8 FaultIndex);
/** Individual fault function for: EfficiencyDCLV. */
static void LFM_Function_EfficiencyDCLV(uint8 FaultIndex);
/** Individual fault function for: OutputShortCircuit. */
static void LFM_Function_OutputShortCircuit(uint8 FaultIndex);
/** Individual fault function for: InternalCom. */
static void LFM_Function_InternalCom(uint8 FaultIndex);
/** Individual fault function for: InputVoltage. */
static void LFM_Function_InputVoltage(uint8 FaultIndex);
/** Individual fault function for: BatteryVoltage. */
static void LFM_Function_BatteryVoltage(uint8 FaultIndex);
/** Individual fault function for: OutShortCircuitHV. */
static void LFM_Function_OutShortCircuitHV(uint8 FaultIndex);
/** Individual fault function for: HWfault. */
static void LFM_Function_DCLVHWfault(uint8 FaultIndex);
/** Individual fault function for: SWfault. */
static void LFM_Function_DCLVSWfault(uint8 FaultIndex);

/** Update calibratables*/
static void LFM_UpdateCalibratables(void);

/**
 * Publish DTC triggers
 */
static void LFM_DTCTriggerReport(void);

/**
 * Get TRUE if any error fault is present
 */
/* Function not used */
/*
static boolean LFM_GetFaultPresent(uint8 FaultIndex);
*/

/**Clean snapshot*/
static void LFM_CleanSanpShot(void);

/** Set HW fault for debuging*/
static void LFM_Set_HW_fault(uint8 position, boolean Value);

/*Check condition for cleaning all the faults*/
static boolean LFM_Cleanfault(void);

static DCDC_Fault LFM_DecodeReportStateNoFaultOrLevel3(IdtFaultLevel FaultLevel);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCDC_HighVoltConnectionAllowed: Boolean
 * DCDC_OutputVoltage: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_OC_A_HV_FAULT: Boolean
 * DCLV_OC_A_LV_FAULT: Boolean
 * DCLV_OC_B_HV_FAULT: Boolean
 * DCLV_OC_B_LV_FAULT: Boolean
 * DCLV_OT_FAULT: Boolean
 * DCLV_OV_INT_A_FAULT: Boolean
 * DCLV_OV_INT_B_FAULT: Boolean
 * DCLV_OV_UV_HV_FAULT: Boolean
 * DCLV_PWM_STOP: Boolean
 * IdtABSVehSpdThresholdOV: Integer in interval [0...100]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit1_ConfirmTime: Integer in interval [0...100]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit1_Threshold: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit2_ConfirmTime: Integer in interval [0...100]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtBatteryVoltageSafetyOVLimit2_Threshold: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryShortCircuit: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLV_MaxNumberRetries: Integer in interval [0...60]
 * IdtDebounceOvercurrentDCHV: Integer in interval [0...10]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtThresholdLVOvercurrentHW: Integer in interval [0...400]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtTimeBatteryVoltageSafety: Integer in interval [0...255]
 *   Unit: [s], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_RCD (0U)
 *   Cx1_No_main_wake_up_request (1U)
 *   Cx2_Main_wake_up_request (2U)
 *   Cx3_Not_valid (3U)
 * DCDC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   DCDC_FAULT_NO_FAULT (0U)
 *   DCDC_FAULT_LEVEL_1 (64U)
 *   DCDC_FAULT_LEVEL_2 (128U)
 *   DCDC_FAULT_LEVEL_3 (192U)
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 *   FAULT_LEVEL_NO_FAULT (0U)
 *   FAULT_LEVEL_1 (1U)
 *   FAULT_LEVEL_2 (2U)
 *   FAULT_LEVEL_3 (3U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 *
 * Array Types:
 * ============
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtFaultLevel *Rte_Pim_PimDCLVHWFault_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_FaultSatellite_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_InternalCom_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputOvercurrent_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputShorcircuitHV_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputShortCircuitLV_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OutputShortCircuit_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OvervoltageHV_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OvervoltageLimit1_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_OvervoltageLimit2_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimDCLV_Overvoltage_Error(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtThresholdLVOvercurrentHW Rte_CData_CalThresholdLVOvercurrentHW(void)
 *   IdtABSVehSpdThresholdOV Rte_CData_CalABSVehSpdThresholdOV(void)
 *   IdtBatteryVoltageSafetyOVLimit1_ConfirmTime Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime(void)
 *   IdtBatteryVoltageSafetyOVLimit1_Threshold Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold(void)
 *   IdtBatteryVoltageSafetyOVLimit2_ConfirmTime Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime(void)
 *   IdtBatteryVoltageSafetyOVLimit2_Threshold Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold(void)
 *   IdtDCLV_MaxNumberRetries Rte_CData_CalDCLV_MaxNumberRetries(void)
 *   IdtTimeBatteryVoltageSafety Rte_CData_CalTimeBatteryVoltageSafety(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtDCLVFaultTimeToRetryDefault Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void)
 *   IdtDCLVFaultTimeToRetryShortCircuit Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void)
 *   IdtDebounceOvercurrentDCHV Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void)
 *
 *********************************************************************************************************************/


#define CtApLFM_START_SEC_CODE
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_LFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus; /* Remove compilation warning */

	 *ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_LFM_HW_faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_LFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) DataServices_LFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_LFM_HW_faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus; /* Remove compilation warning */

	Data[0] = LFM_UDS_HWFaultData[0];
	Data[1] = LFM_UDS_HWFaultData[1];
	Data[2] = LFM_UDS_HWFaultData[2];
	Data[3] = LFM_UDS_HWFaultData[3];


  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLFMGetDCLVHWFaults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpLFMGetDCLVHWFaults> of PortPrototype <PpLFMGetDCLVHWFaults>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RCtApLFMGetDCLVHWFaults(uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFMGetDCLVHWFaults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLFM_CODE) RCtApLFMGetDCLVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFMGetDCLVHWFaults
 *********************************************************************************************************************/
	data[0] = LFM_UDS_HWFaultData[0];
	data[1] = LFM_UDS_HWFaultData[1];
	data[2] = LFM_UDS_HWFaultData[2];
	data[3] = LFM_UDS_HWFaultData[3];

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLFM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLFM_CODE) RCtApLFM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	 uint8 index;


	 LFM_DCLVFaultTimeToRetryDefault = (uint32)LFM_GAIN_10 * (uint32)Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault();
	 LFM_DCLVFaultTimeToRetryShortCircuit = (uint32)LFM_GAIN_10 * (uint32)Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit();

	 LFM_DebounceOvercurrentDCHV = (uint16)LFM_GAIN_100 * (uint16)Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV();
	 LFM_TimeBatteryVoltageSafety = (uint16)LFM_GAIN_100 * (uint16)Rte_CData_CalTimeBatteryVoltageSafety();
	 LFM_BatteryVoltageSafetyOVLimit2_ConfirmTime = (uint16)LFM_GAIN_100 * (uint16)Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime();
	 LFM_BatteryVoltageSafetyOVLimit1_ConfirmTime = (uint16)LFM_GAIN_100 * (uint16)Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime();

	 LFM_ThresholdLVOvercurrentHW = Rte_CData_CalThresholdLVOvercurrentHW();
	 LFM_BatteryVoltageSafetyOVLimit1_Threshold = Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold();
	 LFM_BatteryVoltageSafetyOVLimit2_Threshold = Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold();
	 LFM_ABSVehSpdThresholdOV = Rte_CData_CalABSVehSpdThresholdOV();

	 LFM_RetryCounter = 0U;

	for(index=0U;index<(uint8)LFM_FAULT_NUMBER;index++)
  {
		LFM_SnapShot[index] = FAULT_LEVEL_NO_FAULT;
		LFM_RestartCounter[index] = 0U;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLFM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCLVError_DeDCLVError(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(DCDC_Fault data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLFM_CODE) RCtApLFM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLFM_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean LFM_FirstLevel = FALSE;

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static IdtFaultLevel GeneralLevel_previous = FAULT_LEVEL_NO_FAULT;

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel GeneralLevel;
	boolean LFM_DCLVerror = FALSE;
	DCDC_Fault LFM_FaultState;

	uint8 index;
	uint8 byte_index;
	uint8 bit_index;


	LFM_UpdateCalibratables();


	LFM_CleanFaults_Condition=LFM_Cleanfault();

	if(TRUE==LFM_CleanFaults_Condition)
	{
		LFM_FirstLevel = FALSE;
	}


	GeneralLevel = LFM_IndividualCheck();

	if((FAULT_LEVEL_1==GeneralLevel)||(TRUE==LFM_FirstLevel))
	{
		LFM_FirstLevel = TRUE;
		LFM_DCLVerror = TRUE;
		GeneralLevel = FAULT_LEVEL_1;
	}
	else if(FAULT_LEVEL_2==GeneralLevel)
	{
		LFM_DCLVerror = TRUE;

		if(FAULT_LEVEL_2!=GeneralLevel_previous)
		{
			if(TRUE==LFM_CheckPromotion())
			{
				LFM_FirstLevel = TRUE;
				GeneralLevel = FAULT_LEVEL_1;
			}
		}
	}
	else if(FAULT_LEVEL_2==GeneralLevel_previous)
	{
		LFM_CleanCounters();
	}
	else
	{
		/*MISRA*/
	}

	GeneralLevel_previous = GeneralLevel;


	LFM_FaultState = LFM_ReportDCLVfault(GeneralLevel);

	(void)Rte_Write_PpDCLVError_DeDCLVError(LFM_DCLVerror);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_Fault_DCDC_Fault(LFM_FaultState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	LFM_DTCTriggerReport();

	for (index = 0; index < LFM_FAULT_NUMBER; index++)
	{
		byte_index = index >> 3;
		bit_index = index % 8U;

		if (LFM_FaultVar[index] != FAULT_LEVEL_NO_FAULT)
		{
			LFM_DebugData[byte_index] |= (1U << bit_index);
		}

	}

	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,0U,LFM_DebugData[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,1U,LFM_DebugData[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,2U,LFM_DebugData[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,3U,LFM_DebugData[3]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,4U,LFM_UDS_HWFaultData[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,5U,LFM_UDS_HWFaultData[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,6U,LFM_UDS_HWFaultData[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(2U,7U,LFM_UDS_HWFaultData[3]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_LFM_Faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_LFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus; /* Remove compilation warning */

	*ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_LFM_Faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_LFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_LFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_LFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLFM_CODE) RDataServices_LFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_LFM_Faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus; /* Remove compilation warning */

	Data[0] = LFM_DebugData[0];
	Data[1] = LFM_DebugData[1];
	Data[2] = LFM_DebugData[2];
	Data[3] = LFM_DebugData[3];

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLFM_STOP_SEC_CODE
#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static IdtFaultLevel LFM_IndividualCheck(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;
	boolean FirstLevel = FALSE;
	boolean SecondLevel = FALSE;
	boolean ThirdLevel = FALSE;

	LFM_Function_OvervoltageHV(0U);
	LFM_Function_OvervoltageLimit1(1U);
	LFM_Function_OvervoltageLimit2(2U);
	LFM_Function_OutputOvercurrent(3U);
	LFM_Function_Overvoltage(4U,LFM_FaultVar[1],LFM_FaultVar[2]);
	LFM_Function_OutputShortCircuitLV(5U);
	LFM_Function_OutputShortCircuit(6U);
	LFM_Function_InternalCom(7U);
	LFM_Function_EfficiencyDCLV(8U);
	LFM_Function_InputVoltage(9U);
	LFM_Function_BatteryVoltage(10U);
	LFM_Function_OutShortCircuitHV(11U);
	LFM_Function_DCLVHWfault(12U);
	LFM_Function_DCLVSWfault(13U);

	for(index=0U;index<(uint8)LFM_FAULT_NUMBER;index++)
	{
		if(FAULT_LEVEL_1==LFM_FaultVar[index])
		{
			FirstLevel = TRUE;
		}
		else if(FAULT_LEVEL_2==LFM_FaultVar[index])
		{
			SecondLevel = TRUE;
		}
		else if(FAULT_LEVEL_3==LFM_FaultVar[index])
		{
			ThirdLevel = TRUE;
		}
		else
		{
			/*MISRA*/
		}
	}

	return LFM_GetFaultLevel(FirstLevel,SecondLevel,ThirdLevel);

}

static void LFM_Function_OvervoltageHV(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	DCHV_ADC_VOUT DCHV_ADC_VOUT_CANSignal;
	DCLV_Input_Voltage DCLV_Input_Voltage_CANSignal;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_ADC_VOUT_CANSignal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&DCLV_Input_Voltage_CANSignal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(((LFM_505V/LFM_GAIN_10)<DCLV_Input_Voltage_CANSignal)||(LFM_505V<DCHV_ADC_VOUT_CANSignal))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	ErrorLevel=LFM_GetFaultLevel(FALSE,FALSE,Input);


	/*Write into RTE*/
	*(Rte_Pim_PimDCLV_OvervoltageHV_Error()) = ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}

}
static void LFM_Function_OvervoltageLimit1(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static LFM_debounce_t ThirdLevel = {FALSE,0U};

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	IdtBatteryVolt BatteryVoltagePhysicalValue;
	ABS_VehSpd ABS_VehSpd_CANSignal;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&BatteryVoltagePhysicalValue);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&ABS_VehSpd_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((BatteryVoltagePhysicalValue>LFM_BatteryVoltageSafetyOVLimit1_Threshold)
			&&((uint16)ABS_VehSpd_CANSignal>((uint16)LFM_ABSVehSpdThresholdOV*(uint16)LFM_GAIN_100)))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	LFM_debounce(Input,LFM_BatteryVoltageSafetyOVLimit1_ConfirmTime,0U,&ThirdLevel);
	ErrorLevel=LFM_GetFaultLevel(FALSE,FALSE,ThirdLevel.State);

	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_OvervoltageLimit1_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_OvervoltageLimit2(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static LFM_debounce_t ThirdLevel = {FALSE,0U};

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	IdtBatteryVolt BatteryVoltagePhysicalValue;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&BatteryVoltagePhysicalValue);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(BatteryVoltagePhysicalValue>LFM_BatteryVoltageSafetyOVLimit2_Threshold )
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	LFM_debounce(Input,LFM_BatteryVoltageSafetyOVLimit2_ConfirmTime,0U,&ThirdLevel);
	ErrorLevel=LFM_GetFaultLevel(FALSE,FALSE,ThirdLevel.State);

	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_OvervoltageLimit2_Error())=ErrorLevel;


	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void  LFM_Function_OutputOvercurrent(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static LFM_debounce_t SeconLevel = {FALSE,0U};

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	boolean Input;
	OBC_Status OBC_Status_CANSignal;
	DCHV_ADC_IOUT DCHV_ADC_IOUT_CANSignal;
	DCHV_Command_Current_Reference Command_Current;
	DCHV_DCHVStatus DCHVstatus;
	uint32 Imax;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&DCHVstatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&DCHV_ADC_IOUT_CANSignal ); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(&Command_Current); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	Imax = (uint32)Command_Current + ((uint32)Command_Current>>1);

	if(((Cx3_conversion_working_==OBC_Status_CANSignal)
				||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&((uint32)DCHV_ADC_IOUT_CANSignal>Imax)
				&&(Cx1_DCHV_STATUS_RUN==DCHVstatus))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	LFM_debounce(Input,LFM_DebounceOvercurrentDCHV,0U,&SeconLevel);
	ErrorLevel=LFM_GetFaultLevel(FALSE,SeconLevel.State,FALSE);
	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);

	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_OutputOvercurrent_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_Overvoltage(uint8 FaultIndex, IdtFaultLevel OVlimit1, IdtFaultLevel OVlimit2)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static LFM_debounce_t SeconLevel = {FALSE,0U};

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	IdtFaultLevel ErrorLevel;
	uint8 BatteryVoltage_dv;
	IdtFaultLevel BatteryPlausibility;
	boolean DCDC_OutputVoltage_ProducerError = FALSE;

	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&BatteryVoltage_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(&BatteryPlausibility);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*offset compensation*/
	if(LFM_LVV_OFFSET>=BatteryVoltage_dv)
	{
		BatteryVoltage_dv = 0U;
	}
	else
	{
		BatteryVoltage_dv -= LFM_LVV_OFFSET;
	}


	if((FAULT_LEVEL_NO_FAULT!=OVlimit1) || (FAULT_LEVEL_NO_FAULT!= OVlimit2))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}
	LFM_debounce(Input,LFM_TimeBatteryVoltageSafety,0U,&SeconLevel);
	ErrorLevel=LFM_GetFaultLevel(FALSE,SeconLevel.State,FALSE);

	if((FAULT_LEVEL_NO_FAULT!=ErrorLevel)||(FALSE!=Input)||(FAULT_LEVEL_2==BatteryPlausibility))
	{
		BatteryVoltage_dv = LFM_FORBIDDEN_RANGE_DCDC_OutputVoltage;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		DCDC_OutputVoltage_ProducerError = TRUE;
	}

	/* Write producer error */
	(void)Rte_Write_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(DCDC_OutputVoltage_ProducerError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);

	(void)Rte_Write_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(BatteryVoltage_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_Overvoltage_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_OutputShortCircuitLV(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	DCLV_OC_A_LV_FAULT DCLV_OC_A_LV_FAULT_CANSignal;
	DCLV_OC_B_LV_FAULT DCLV_OC_B_LV_FAULT_CANSignal;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(&DCLV_OC_A_LV_FAULT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(&DCLV_OC_B_LV_FAULT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FALSE!=DCLV_OC_A_LV_FAULT_CANSignal)||(FALSE!=DCLV_OC_B_LV_FAULT_CANSignal))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	ErrorLevel=LFM_GetFaultLevel(FALSE,Input,FALSE);
	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryShortCircuit,&LFM_RestartCounter[FaultIndex]);

	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_OutputShortCircuitLV_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_EfficiencyDCLV(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	IdtFaultLevel DCLV_EfficiencyDCLV_Error;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(&DCLV_EfficiencyDCLV_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	ErrorLevel=LFM_OutputReport(DCLV_EfficiencyDCLV_Error,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_OutputShortCircuit(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	IdtFaultLevel DCLV_EfficiencyDCLV_Error;
	DCLV_Measured_Current DCLV_Measured_Current_CANSignal;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(&DCLV_EfficiencyDCLV_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(&DCLV_Measured_Current_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FAULT_LEVEL_NO_FAULT==DCLV_EfficiencyDCLV_Error)
			&&(LFM_DCLV_OUT_CURRENT_INVALID>DCLV_Measured_Current_CANSignal)
			&&((uint16)DCLV_Measured_Current_CANSignal>((uint16)LFM_ThresholdLVOvercurrentHW*(uint16)LFM_GAIN_10)))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	ErrorLevel=LFM_GetFaultLevel(FALSE,Input,FALSE);
	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);

	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_OutputShortCircuit_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_InternalCom(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 delay=0U;

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	boolean DCLVFramesReception_Error;
	IdtFaultLevel ErrorLevel;
	boolean DCLV_postOK;


	/*Read RTE*/
	(void)Rte_Read_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(&DCLVFramesReception_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(&DCLV_postOK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((delay<1500U)&&(FALSE==DCLV_postOK))
	{
		/*wait until the POST is over or 5seg*/
		delay++;
		Input = FALSE;
	}
	else if(FALSE!=DCLVFramesReception_Error)
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	/* DTC Fault*/
	LFM_InternalCom = Input;

	ErrorLevel=LFM_GetFaultLevel(FALSE,Input,FALSE);
	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);

	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_InternalCom_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_InputVoltage(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	IdtFaultLevel DCLV_InputVoltage_Error;
	IdtFaultLevel ErrorLevel;
	


	/*Read RTE*/
	(void)Rte_Read_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(&DCLV_InputVoltage_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	ErrorLevel=LFM_OutputReport(DCLV_InputVoltage_Error,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);



	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_BatteryVoltage(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel DCLV_BatteryVoltage_Error;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(&DCLV_BatteryVoltage_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	ErrorLevel=LFM_OutputReport(DCLV_BatteryVoltage_Error,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_OutShortCircuitHV(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	DCDC_Status DCDC_Status_CANSignal;
	DCLV_OC_A_HV_FAULT DCLV_OC_A_HV_FAULT_CANSignal;
	DCLV_OC_A_HV_FAULT DCLV_OC_B_HV_FAULT_CANSignal;
	IdtFaultLevel ErrorLevel;


	/*Read RTE*/
	(void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&DCDC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(&DCLV_OC_A_HV_FAULT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(&DCLV_OC_B_HV_FAULT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(((Cx3_conversion_working_==DCDC_Status_CANSignal)||(Cx5_degradation_mode==DCDC_Status_CANSignal))
			&&((FALSE!=DCLV_OC_A_HV_FAULT_CANSignal)||(FALSE!=DCLV_OC_B_HV_FAULT_CANSignal)))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	ErrorLevel=LFM_GetFaultLevel(Input,FALSE,FALSE);


	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_OutputShorcircuitHV_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
}
static void LFM_Function_DCLVHWfault(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel ErrorLevel;
	boolean Fault_IN;
	boolean Fault_OUT;
	DCDC_Status DCDC_Status_CANSignal;


	(void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&DCDC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx3_conversion_working_==DCDC_Status_CANSignal)||(Cx5_degradation_mode==DCDC_Status_CANSignal))
	{
		(void)Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT = Fault_IN;
		LFM_Set_HW_fault(0U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(1U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(2U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(3U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(4U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(5U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(6U,Fault_IN);
		(void)Rte_Read_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		Fault_OUT |= Fault_IN;
		LFM_Set_HW_fault(7U,Fault_IN);
	}
	else
	{
		Fault_OUT = FALSE;
	}

	ErrorLevel=LFM_GetFaultLevel(FALSE,Fault_OUT,FALSE);
	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);


	/*Write into RTE*/
	(*Rte_Pim_PimDCLVHWFault_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}
	/* To report to the DTC */
	LFM_HWErrors_Fault = Fault_OUT;

}

static void LFM_Function_DCLVSWfault(uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static LFM_debounce_t SeconLevel = {FALSE,0U};

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel ErrorLevel;
	boolean Input;
	DCDC_Status DCDC_Status_CANSignal;
	DCLV_DCLVStatus DCLV_state;


	(void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&DCDC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&DCLV_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(((Cx3_conversion_working_==DCDC_Status_CANSignal)||(Cx5_degradation_mode==DCDC_Status_CANSignal))
			&&((Cx2_ERROR==DCLV_state)||(Cx4_SAFE==DCLV_state)))
	{
		Input = TRUE;
	}
	else
	{
		Input = FALSE;
	}

	LFM_debounce(Input,3U,0U,&SeconLevel);
	ErrorLevel=LFM_GetFaultLevel(FALSE,SeconLevel.State,FALSE);
	ErrorLevel=LFM_OutputReport(ErrorLevel,LFM_DCLVFaultTimeToRetryDefault,&LFM_RestartCounter[FaultIndex]);


	/*Write into RTE*/
	(*Rte_Pim_PimDCLV_FaultSatellite_Error())=ErrorLevel;

	if(LFM_FAULT_NUMBER>FaultIndex)
	{
		LFM_FaultVar[FaultIndex] = ErrorLevel;
	}

}

static IdtFaultLevel LFM_GetFaultLevel(boolean firstLevel, boolean secondLevel, boolean thirdlevel)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel retVal;


	if(TRUE==firstLevel)
	{
		retVal = FAULT_LEVEL_1 ;
	}
	else if(TRUE==secondLevel)
	{
		retVal = FAULT_LEVEL_2 ;
	}
	else if(TRUE==thirdlevel)
	{
		retVal = FAULT_LEVEL_3 ;
	}
	else
	{
		retVal = FAULT_LEVEL_NO_FAULT ;
	}

	return retVal;
}
static void LFM_debounce(boolean Input, uint16 ConfirmTime, uint16 HealTime, LFM_debounce_t *Debounce)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 CounterMax;


	if(NULL_PTR != Debounce)
	{

		if(TRUE==Input)
		{
			CounterMax = ConfirmTime;
		}
		else
		{
			CounterMax = HealTime;
		}

		if(Input==Debounce->State)
		{
			Debounce->Counter = 0U;
		}
		else if(Debounce->Counter>=CounterMax)
		{
			Debounce->State = Input;
			Debounce->Counter = 0U;
		}
		else
		{
			Debounce->Counter ++;
		}
	}
}
static IdtFaultLevel  LFM_OutputReport(IdtFaultLevel ErrorLevel,uint32 RestartTime,uint32 *RestartCounter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel RetVal;


	if(NULL_PTR==RestartCounter)
	{
		RetVal = ErrorLevel;
	}
	else if(TRUE==LFM_CleanFaults_Condition)
	{
		(*RestartCounter)=0U;
		RetVal = ErrorLevel;
	}
	else if(0U==*RestartCounter)
	{
		if(FAULT_LEVEL_2==ErrorLevel)
		{
			/*Initialize time to restart*/
			RetVal = FAULT_LEVEL_2;
			(*RestartCounter)++;
		}
		else
		{
			/*Waiting for second level error...*/
			RetVal = ErrorLevel;
		}
	}
	else if(*RestartCounter>=RestartTime)
	{
		/*Force No fault*/
		RetVal = FAULT_LEVEL_NO_FAULT;
	}
	else
	{
		RetVal = FAULT_LEVEL_2;
		(*RestartCounter)++;
	}


	return RetVal;
}

static void LFM_CleanCounters(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;


	for(index=0U;index<(uint8)LFM_FAULT_NUMBER;index++)
	{
		LFM_RestartCounter[index] = 0U;
	}
}
static boolean LFM_CheckPromotion(void)
{
	uint8 index;
	boolean RetVal = FALSE;

	for(index=0U;index<(uint8)LFM_FAULT_NUMBER;index++)
	{
		if((FAULT_LEVEL_2==LFM_SnapShot[index])
				&&(FAULT_LEVEL_2==LFM_FaultVar[index]))
		{
			RetVal=TRUE;
		}
		LFM_SnapShot[index] = LFM_FaultVar[index];
	}


	if(FALSE==RetVal)
	{
		/*No promoted, clean retry counter.*/
		LFM_RetryCounter=0U;
	}
	else
	{
		/*Same fault than before, increase retry counter.*/
		LFM_RetryCounter++;
	}


	if(Rte_CData_CalDCLV_MaxNumberRetries()<=LFM_RetryCounter)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}


	return RetVal;
}
static DCDC_Fault LFM_ReportDCLVfault(IdtFaultLevel FaultLevel)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 DebounceCounter = 0U;

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static DCDC_Fault ReportState = DCDC_FAULT_NO_FAULT;

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	DCDC_Status DCLVState;


	(void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&DCLVState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(TRUE==LFM_CleanFaults_Condition)
	{
		/*Clean general fault level*/
		ReportState = DCDC_FAULT_NO_FAULT;
		DebounceCounter = 0U;
		LFM_CleanSanpShot();
	}
	else if(FAULT_LEVEL_1==FaultLevel)
	{
		ReportState = DCDC_FAULT_LEVEL_1;
		DebounceCounter = 0U;
	}
	else if(FAULT_LEVEL_2==FaultLevel)
	{
		ReportState = DCDC_FAULT_LEVEL_2;
		DebounceCounter = 0U;
	}
	else if((ReportState == DCDC_FAULT_NO_FAULT)||(ReportState == DCDC_FAULT_LEVEL_3))
	{
		DebounceCounter = 0U;
		/* Function created to reduce CC */
		ReportState = LFM_DecodeReportStateNoFaultOrLevel3(FaultLevel);
	}
	else if((Cx3_conversion_working_!=DCLVState)&&(Cx5_degradation_mode!=DCLVState))
	{
		DebounceCounter = 0U;
	}
	else if(LFM_CLEAN_FAULT_TIME>DebounceCounter)
	{
		DebounceCounter ++;
	}
	else
	{
		ReportState = LFM_DecodeReportStateNoFaultOrLevel3(FaultLevel);
		LFM_CleanSanpShot();
	}


	return ReportState;
}

static DCDC_Fault LFM_DecodeReportStateNoFaultOrLevel3(IdtFaultLevel FaultLevel)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	DCDC_Fault ReportState;

	if(FAULT_LEVEL_3==FaultLevel)
	{
		ReportState = DCDC_FAULT_LEVEL_3;
	}
	else
	{
		ReportState = DCDC_FAULT_NO_FAULT;
	}

	return ReportState;
}

/** Update calibratables*/
static void LFM_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	 LFM_DCLVFaultTimeToRetryDefault = (uint32)LFM_GAIN_10 * (uint32)Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault();
	 LFM_DCLVFaultTimeToRetryShortCircuit = (uint32)LFM_GAIN_10 * (uint32)Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit();

	 LFM_DebounceOvercurrentDCHV = (uint16)LFM_GAIN_100 * (uint16)Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV();
	 LFM_TimeBatteryVoltageSafety = (uint16)LFM_GAIN_100 * (uint16)Rte_CData_CalTimeBatteryVoltageSafety();
	 LFM_BatteryVoltageSafetyOVLimit2_ConfirmTime = (uint16)LFM_GAIN_100 * (uint16)Rte_CData_CalBatteryVoltageSafetyOVLimit2_ConfirmTime();
	 LFM_BatteryVoltageSafetyOVLimit1_ConfirmTime = (uint16)LFM_GAIN_100 * (uint16)Rte_CData_CalBatteryVoltageSafetyOVLimit1_ConfirmTime();

	 LFM_ThresholdLVOvercurrentHW = Rte_CData_CalThresholdLVOvercurrentHW();
	 LFM_BatteryVoltageSafetyOVLimit1_Threshold = Rte_CData_CalBatteryVoltageSafetyOVLimit1_Threshold();
	 LFM_BatteryVoltageSafetyOVLimit2_Threshold = Rte_CData_CalBatteryVoltageSafetyOVLimit2_Threshold();
	 LFM_ABSVehSpdThresholdOV = Rte_CData_CalABSVehSpdThresholdOV();

}

static void LFM_DTCTriggerReport(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	boolean faultData;

	(void)Rte_Read_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(&faultData);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/* We can send it directly because the values match with the faults: */
	/* FAULT = TRUE = DTC ACTIVE, NO FAULT = FALSE = DTC INACTIVE */
	(void)Rte_Write_PpDCDCFaultsList_DeDCDC_OvertemperatureFault(faultData);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	
	/* HWErrors_Fault is evaluated on HW Errors function, now it is only checked */
	(void)Rte_Write_PpDCDCFaultsList_DeDCLV_HWErrors_Fault(LFM_HWErrors_Fault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpDCDCFaultsList_DeDCLV_InternalCom_Fault(LFM_InternalCom);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

#if 0
/* Function not used */
static boolean LFM_GetFaultPresent(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ret_value;


	if (LFM_FaultVar[ArrayPosition] != FALSE)
	{
		ret_value = TRUE;
	}
	else
	{
		ret_value = FALSE;
	}

	return ret_value;
}
#endif

static void LFM_CleanSanpShot(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;

	LFM_RetryCounter=0U;

	for(index=0U;index<(uint8)LFM_FAULT_NUMBER;index++)
	{
		LFM_SnapShot[index] = FAULT_LEVEL_NO_FAULT;
	}
}

static void LFM_Set_HW_fault(uint8 position, boolean Value)
{

	DCDC_Status DCLVState;

	(void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&DCLVState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx1_Init_mode!=DCLVState)
		&&(32U>position))
	{
		LFM_UDS_HWFaultData[position>>3U] |= Value<<(position%8U);
	}
	else if(TRUE==LFM_CleanFaults_Condition)
	{
		LFM_UDS_HWFaultData[position>>3U] = 0U;/* PRQA S 2842 #RTE this out of bounds is done to truncate*/
	}
	else
	{
		/*Misra*/
	}
}
static boolean LFM_Cleanfault(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static BSI_MainWakeup MainWakeUp_previous = Cx0_Invalid_RCD;
	static uint8 ConnectionDebounce=0U;

	#define CtApLFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	 BSI_MainWakeup MainWakeUp;
	 boolean CleanCondition = FALSE;
	 OBC_ChargingConnectionConfirmati Connection;

	 (void)Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&MainWakeUp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	 (void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Connection);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	 if(((Cx1_No_main_wake_up_request==MainWakeUp)||(Cx2_Main_wake_up_request==MainWakeUp))
			 &&(MainWakeUp_previous!=MainWakeUp))
	 {
		 /*Transition to No_main_wake_up or Main_wake_up*/
		 CleanCondition = TRUE;
	 }

	 MainWakeUp_previous = MainWakeUp;

	 if(Cx1_not_connected!=Connection)
	 {
		 ConnectionDebounce = 0U;
	 }
	 else if(100U>ConnectionDebounce)
	 {
		 ConnectionDebounce++;
	 }
	 else if(100U==ConnectionDebounce)
	 {
		 /*Just clean the faults one time*/
		 CleanCondition = TRUE;
		 ConnectionDebounce++;
	 }
	 else
	 {
		 /*Don't clean agin the fault for this reason*/
	 }


	 return CleanCondition;
}



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
