/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApFCL.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApFCL
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApFCL>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup FCL
 * \ingroup AppSw
 * \author Pablo Bolas.
 * \brief Flow control LED.
 * \details This module manages the LED flow control. In case that a error is
 * detected inside any LED this module will modify the LED state.
 *
 * \{
 * \file  fCL.c
 * \brief  Generic code of the FCL module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApFCL.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
#define FCL_THRESHOLD_GAIN				10U
#define FCL_THRESHOLD_OFFSET_SCP	7000U
#define FCL_THRESHOLD_OFFSET_OC		5000U
#define FCL_DIAGNOSIS_SCG_DC			50U

#define FCL_PLUGLED_DIAGNOSIS_TIME	10U/*100ms*/

#define FCL_GAIN_100 100U
#define FCL_GAIN_10 	10U

#define FCL_THRES_OFFSET 500U


/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some 
 * of them are not used*/

/*! Individual RGB LEDs*/
typedef enum{
	/*! RED*/
	FCL_LED_RED = 0U,
	/*! GREEN*/
	FCL_LED_GREEN,
	/*! BLUE*/
	FCL_LED_BLUE,
	/*! NUMBER OF RGB LEDs*/
	FCL_LED_NUMBER
}FCL_rgbLED_t;

typedef enum{
	FCL_DIAG_NO_FAULT,
	FCL_DIAG_SCP_INIT,
	FCL_DIAG_SCP_DETECTION,
	FCL_DIAG_SCG_INIT,
	FCL_DIAG_SCG_DETECTION,
	FCL_DIAG_OC_INIT,
	FCL_DIAG_OC_DETECTION
}FCL_diagnosisState_t;

typedef enum{
	FCL_FAULT_SCP,
	FCL_FAULT_SCG,
	FCL_FAULT_OC,
	FCL_FAULT_NUMBER
}FCL_fault_t;

typedef enum{
	FCL_PLUG_DIAG_NO_FAULT,
	FCL_PLUG_DIAG_FAULT
}FCL_plugLEDdiagnosisState_t;

typedef enum{
	FCL_TEST_DISABLE = 0,
	FCL_TEST_ONGOING = 1
}FCL_ActuatorTestState_t;

typedef enum{
	FCL_TEST_FAIL_ETAT_GMP = 0,
	FCL_TEST_FAIL_VEHSPD = 1,
	FCL_TEST_FAIL_ECU_STATE = 2,
	FCL_TEST_FAIL_BAT_LEVEL = 3,
	FCL_TEST_FAIL_SCP = 4,
	FCL_TEST_FAIL_SCG = 5,
	FCL_TEST_FAIL_OC = 6,
	FCL_TEST_FAIL_STOP = 7
}FCL_TestResultMask_t;

typedef enum{
	FCL_RESP_ONGOING = 0x01,
	FCL_RESP_END_OK = 0x02,
	FCL_RESP_END_NOK = 0x03,
	FCL_RESP_STOP = 0x04

}FCL_testState_t;

typedef struct
{
		boolean State;
		uint8 DebounceCounter;
}FCL_Debounce_t;

typedef struct
{
		uint16 ConfirmTime;
		uint16 HealTime;
}FCL_DebounceCal_t;

typedef struct
{
		boolean Enable;
		uint16 SCP_Threshold;
		uint16 SCG_Threshold;
		FCL_DebounceCal_t DebounceCal[FCL_FAULT_NUMBER];
}FCL_LEDCal_t;

typedef struct
{
		FCL_ActuatorTestState_t State;
		boolean Stop;
		uint32 Counter;
		uint8 Result;
}FCL_ActuatorTestVar_t;

typedef struct
{
		uint16 timeOn;
		uint16 timeOff;
}FCL_ActuatorTestCal_t;

/*PRQA S 3205 --*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static FCL_Debounce_t FCL_RGB_Diagnosis[FCL_LED_NUMBER][FCL_FAULT_NUMBER];
static FCL_diagnosisState_t FCL_DiagnosisState;
static uint8 FCL_DiagnosisSCG_LEDstate;
static FCL_ActuatorTestVar_t FCL_RGB_ActuratorTest[FCL_LED_NUMBER];

static FCL_plugLEDdiagnosisState_t FCL_plugLED_state;
static FCL_Debounce_t FCL_Plug_Diagnosis[FCL_FAULT_NUMBER];
static FCL_ActuatorTestVar_t FCL_Plug_ActuratorTest;

static IdtAppRCDECUState FCL_ECU_state;
static ABS_VehSpd FCL_VehSpd;
static VCU_EtatGmpHyb FCL_VCU_EtatGmpHyb_VCU_EtatGmpHyb;
static uint16 FCL_BatteryLevel;

static IdtVehStopMaxTest FCL_VehStopMaxTest;
static IdtBatteryVoltMinTest FCL_BatteryVoltMinTest;
static IdtBatteryVoltMaxTest FCL_BatteryVoltMaxTest;

static uint16 FCL_HWEdition;

#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
static void FCL_UpdateCalibratables(FCL_LEDCal_t LedCalData[FCL_LED_NUMBER], FCL_ActuatorTestCal_t ActuatorTestData[FCL_LED_NUMBER]);

static void FCL_Diagnosis_Task(uint8 *DC_out,const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);

static void FCL_DiagnosisStateMachine(uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_DiagnosisState_NoFault(boolean FaultDetected, const uint16 LEDfeedBack[FCL_LED_NUMBER],uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_DiagnosisState_SCPdetection(const uint16 LEDfeedBack[FCL_LED_NUMBER], uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_DiagnosisState_SCGdetection(boolean FaultDetected, const uint16 LEDfeedBack[FCL_LED_NUMBER],uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_DiagnosisState_OCdetection(boolean FaultDetected, const uint16 LEDfeedBack[FCL_LED_NUMBER],uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_DCvalueForSCP(uint8 *DC_out);
static void FCL_DCvalueForSCG(uint8 led, uint8 *DC_out);
static void FCL_setPrefault(uint8 led, uint8 fault, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER]);
static void FCL_resetPrefault(uint8 led, uint8 fault, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER]);
static void FCL_Debouce_Task(boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_MonitoringConditions(boolean MonitoringConditions[FCL_LED_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);
static void FCL_Debounce(FCL_Debounce_t *DebounceObject, boolean Input, FCL_DebounceCal_t CalData);
static void FCL_Variant_Task(uint8 DC_out[FCL_LED_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER]);

static void FCL_PlugLED_DiagnosisTask(boolean DiagnosisReady, uint8 *DCout, uint8 last_DCout);
static boolean FCL_PlugLed_diagnosisReady(uint8 Last_DCout);

static void FCL_PlugState_NOfault(boolean DiagnosisReady,uint16 FeedBack, uint8 last_DCout);
static void FCL_PlugState_fault(boolean DiagnosisReady, uint16 FeedBack, boolean Prefault[FCL_FAULT_NUMBER], uint8 last_DCout);
static boolean FCL_MeasureInRange(uint32 Measure, uint32 Low_Threshold, uint32 High_Threshold);

static void FCL_RGB_AcutatorTest(uint8 DCout[FCL_LED_NUMBER], const FCL_ActuatorTestCal_t ActuatorTestCal[FCL_LED_NUMBER]);
static void FCL_Plug_ActuatorTest(uint8 *DCout, const FCL_ActuatorTestCal_t ActuatorTestCal);
static boolean FCL_ActuatorTest_DC(uint8 *DCout, FCL_ActuatorTestVar_t *TestVar, FCL_ActuatorTestCal_t TestCal);
static void FCL_TestFaultsCheck(FCL_ActuatorTestVar_t *TestVar, const FCL_Debounce_t LedState[FCL_FAULT_NUMBER]);
static Dcm_NegativeResponseCodeType FCL_ConditionsCheck(uint8 *Result);
static void FCL_UpdateConditions(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * IdtAfts_DelayLed: Integer in interval [0...500]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtChLedCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtChLedRGBCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtPlgLedrCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtLedFaultTime: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtLedThresholdSCG: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtLedThresholdSCP: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 7000
 * IdtMaxNormalPlugLed: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtMinNormalPlugLed: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlgLedrCtrl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtPlugLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtPlugLedMaxOCDetection: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 5000
 * IdtPlugLedMinOCDetection: Integer in interval [0...1000]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlugLedThresholdSCG: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtPlugLedThresholdSCP: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 7000
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 *   HW_EDITION_UNKNOWN (0U)
 *   HW_EDITION_D21 (1U)
 *   HW_EDITION_D31 (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimLedBlueCounterOC(void)
 *   uint16 *Rte_Pim_PimLedBlueCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedBlueCounterSCP(void)
 *   uint16 *Rte_Pim_PimLedGreenCounterOC(void)
 *   uint16 *Rte_Pim_PimLedGreenCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedGreenCounterSCP(void)
 *   uint16 *Rte_Pim_PimLedPlugCounterOC(void)
 *   uint16 *Rte_Pim_PimLedPlugCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedPlugCounterSCP(void)
 *   uint16 *Rte_Pim_PimLedRedCounterOC(void)
 *   uint16 *Rte_Pim_PimLedRedCounterSCG(void)
 *   uint16 *Rte_Pim_PimLedRedCounterSCP(void)
 *   boolean *Rte_Pim_PimLedBluePrefaultOC(void)
 *   boolean *Rte_Pim_PimLedBluePrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedBluePrefaultSCP(void)
 *   boolean *Rte_Pim_PimLedGreenPrefaultOC(void)
 *   boolean *Rte_Pim_PimLedGreenPrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedGreenPrefaultSCP(void)
 *   boolean *Rte_Pim_PimLedPlugPrefaultOC(void)
 *   boolean *Rte_Pim_PimLedPlugPrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedPlugPrefaultSCP(void)
 *   boolean *Rte_Pim_PimLedRedPrefaultOC(void)
 *   boolean *Rte_Pim_PimLedRedPrefaultSCG(void)
 *   boolean *Rte_Pim_PimLedRedPrefaultSCP(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedBlueOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedBlueOn(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedGreenOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedGreenOn(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedPlugOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedPlugOn(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedRedOff(void)
 *   IdtAfts_DelayLed Rte_CData_CalAfts_DelayLedRedOn(void)
 *   IdtLedThresholdSCG Rte_CData_CalLedBlueThresholdSCG(void)
 *   IdtLedThresholdSCP Rte_CData_CalLedBlueThresholdSCP(void)
 *   IdtLedThresholdSCG Rte_CData_CalLedGreenThresholdSCG(void)
 *   IdtLedThresholdSCP Rte_CData_CalLedGreenThresholdSCP(void)
 *   IdtLedThresholdSCG Rte_CData_CalLedRedThresholdSCG(void)
 *   IdtLedThresholdSCP Rte_CData_CalLedRedThresholdSCP(void)
 *   IdtMaxNormalPlugLed Rte_CData_CalMaxNormalPlugLed(void)
 *   IdtMinNormalPlugLed Rte_CData_CalMinNormalPlugLed(void)
 *   IdtPlugLedMaxOCDetection Rte_CData_CalPlugLedMaxOCDetection(void)
 *   IdtPlugLedMinOCDetection Rte_CData_CalPlugLedMinOCDetection(void)
 *   IdtPlugLedThresholdSCG Rte_CData_CalPlugLedThresholdSCG(void)
 *   IdtPlugLedThresholdSCP Rte_CData_CalPlugLedThresholdSCP(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedBlueSCPHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedGreenSCPHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedPlugSCPHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedOCConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedOCHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCGConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCGHealTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCPConfirmTime(void)
 *   IdtLedFaultTime Rte_CData_CalLedRedSCPHealTime(void)
 *   boolean Rte_CData_CalEnableLedBlue(void)
 *   boolean Rte_CData_CalEnableLedGreen(void)
 *   boolean Rte_CData_CalEnableLedPlug(void)
 *   boolean Rte_CData_CalEnableLedRed(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtBatteryVoltMaxTest Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
 *   IdtBatteryVoltMinTest Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
 *   IdtVehStopMaxTest Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
 *
 *********************************************************************************************************************/


#define CtApFCL_START_SEC_CODE
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCL_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_init_doc
 *********************************************************************************************************************/

/*!
 * \brief FCL initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCL_CODE) RCtApFCL_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 Led_index;
	uint8 Fault_index;


	/*RGB LED*/
	FCL_DiagnosisState = FCL_DIAG_NO_FAULT;

	for(Led_index=0U;(uint8)FCL_LED_NUMBER>Led_index;Led_index++)
	{
		FCL_RGB_ActuratorTest[Led_index].Counter = 0U;
		FCL_RGB_ActuratorTest[Led_index].Result = 0U;
		FCL_RGB_ActuratorTest[Led_index].State = FCL_TEST_DISABLE;
		FCL_RGB_ActuratorTest[Led_index].Stop = FALSE;

		for(Fault_index=0U;(uint8)FCL_FAULT_NUMBER>Fault_index;Fault_index++)
		{
			FCL_RGB_Diagnosis[Led_index][Fault_index].DebounceCounter = 0U;
			FCL_RGB_Diagnosis[Led_index][Fault_index].State = (boolean)FCL_TEST_DISABLE;
		}
	}

	/*Plug LED*/
	FCL_plugLED_state = FCL_PLUG_DIAG_NO_FAULT;

	FCL_Plug_ActuratorTest.Counter = 0U;
	FCL_Plug_ActuratorTest.Result = 0U;
	FCL_Plug_ActuratorTest.State = FCL_TEST_DISABLE;
	FCL_Plug_ActuratorTest.Stop = FALSE;

	for(Fault_index=0U;(uint8)FCL_FAULT_NUMBER>Fault_index;Fault_index++)
	{
		FCL_Plug_Diagnosis[Fault_index].DebounceCounter = 0U;
		FCL_Plug_Diagnosis[Fault_index].State = FALSE;
	}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCL_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl *data)
 *   Std_ReturnType Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl *data)
 *   Std_ReturnType Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl data)
 *   Std_ReturnType Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl data)
 *   Std_ReturnType Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedBlueFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedBlueFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedBlueFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedGreenFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedGreenFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedGreenFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedRedFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedRedFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpLedFaults_DeLedRedFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task100ms_doc
 *********************************************************************************************************************/

/*!
 * \brief FCL task.
 *
 * This function is called periodically by the scheduler. This function should
 * control the RGB led in case of fault.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCL_CODE) RCtApFCL_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtAppRCDECUState ECU_state;
	uint8 DC_out[FCL_LED_NUMBER];
	boolean EnableLed;
	boolean MonitoringConditions[FCL_LED_NUMBER];
	FCL_LEDCal_t LedCalData[FCL_LED_NUMBER];
	FCL_ActuatorTestCal_t ActuatorTestCal[FCL_LED_NUMBER];
	IdtPOST_Result OBCPOST_Result100;
	IdtPOST_Result DCDCPOST_Result100;


	/*Inputs*/
	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&ECU_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpChLedRGBCtl_DeChLedRrCtl(&(DC_out[FCL_LED_RED]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpChLedRGBCtl_DeChLedGrCtl(&(DC_out[FCL_LED_GREEN]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpChLedRGBCtl_DeChLedBrCtl(&(DC_out[FCL_LED_BLUE]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Update calibratables*/
	FCL_UpdateCalibratables(LedCalData,ActuatorTestCal);

	(void)Rte_Read_PpHWEditionDetected_DeHWEditionDetected(&FCL_HWEdition);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&OBCPOST_Result100);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&DCDCPOST_Result100);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	FCL_RGB_AcutatorTest(DC_out,ActuatorTestCal);

	/*ECU state*/
	if(((APP_STATE_1==ECU_state)||(APP_STATE_4==ECU_state)||(APP_STATE_5==ECU_state))&&(POST_OK == OBCPOST_Result100)&&(POST_OK == DCDCPOST_Result100))
	{
		/*Diagnosis task*/
		FCL_Diagnosis_Task(DC_out,LedCalData);
	}
	else
	{
		/*Force output to 0 but keep fault state*/
		DC_out[FCL_LED_RED] = 0U;
		DC_out[FCL_LED_GREEN] = 0U;
		DC_out[FCL_LED_BLUE] = 0U;
	}

	/*Monitoring Conditions*/
	FCL_MonitoringConditions(MonitoringConditions,LedCalData);

	/*Ignore fault if the led is not active*/
	FCL_Variant_Task(DC_out,LedCalData);

	if((HW_EDITION_D31 == FCL_HWEdition) && (FCL_DIAG_OC_DETECTION == FCL_DiagnosisState))
	{
		/*Perform a reset during 10ms*/
		EnableLed = FALSE;
	}
	else if ((HW_EDITION_D31 != FCL_HWEdition) && (FCL_DIAG_SCG_DETECTION == FCL_DiagnosisState))
	{
		/*Perform a reset during 10ms*/
		EnableLed = FALSE;
	}
	else
	{
		EnableLed = TRUE;
	}

	(void)Rte_Write_PpExtChLedRGBCtl_DeExtChLedRrCtl((IdtExtChLedRGBCtl)DC_out[FCL_LED_RED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpExtChLedRGBCtl_DeExtChLedGrCtl((IdtExtChLedRGBCtl)DC_out[FCL_LED_GREEN]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpExtChLedRGBCtl_DeExtChLedBrCtl((IdtExtChLedRGBCtl)DC_out[FCL_LED_BLUE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(EnableLed);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpLedFaults_DeLedRedFaultOC(FCL_RGB_Diagnosis[FCL_LED_RED][FCL_FAULT_OC].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedFaults_DeLedRedFaultSCG(FCL_RGB_Diagnosis[FCL_LED_RED][FCL_FAULT_SCG].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedFaults_DeLedRedFaultSCP(FCL_RGB_Diagnosis[FCL_LED_RED][FCL_FAULT_SCP].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpLedFaults_DeLedGreenFaultOC(FCL_RGB_Diagnosis[FCL_LED_GREEN][FCL_FAULT_OC].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedFaults_DeLedGreenFaultSCG(FCL_RGB_Diagnosis[FCL_LED_GREEN][FCL_FAULT_SCG].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedFaults_DeLedGreenFaultSCP(FCL_RGB_Diagnosis[FCL_LED_GREEN][FCL_FAULT_SCP].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	(void)Rte_Write_PpLedFaults_DeLedBlueFaultOC(FCL_RGB_Diagnosis[FCL_LED_BLUE][FCL_FAULT_OC].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedFaults_DeLedBlueFaultSCG(FCL_RGB_Diagnosis[FCL_LED_BLUE][FCL_FAULT_SCG].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedFaults_DeLedBlueFaultSCP(FCL_RGB_Diagnosis[FCL_LED_BLUE][FCL_FAULT_SCP].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpLedMonitoringConditions_DeBlueLedMonitoringConditions(MonitoringConditions[FCL_LED_BLUE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedMonitoringConditions_DeGreenLedMonitoringConditions(MonitoringConditions[FCL_LED_GREEN]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedMonitoringConditions_DeRedLedMonitoringConditions(MonitoringConditions[FCL_LED_RED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/




/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCL_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl *data)
 *   Std_ReturnType Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl data)
 *   Std_ReturnType Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(boolean data)
 *   Std_ReturnType Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief FCL task.
 *
 * This function is called periodically by the scheduler. This function should
 * manage the plug led in case of fault.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCL_CODE) RCtApFCL_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCL_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 Last_DCout = 0U;

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 DC_out;
	boolean DiagnosisReady;
	IdtAppRCDECUState ECU_state;
	IdtBatteryVoltageState BatteryState;
	uint8 FaultIndex;
	boolean MonotiringConditions;
	FCL_ActuatorTestCal_t ActuatorCal;
	IdtPOST_Result OBCPOST_Result10;
	IdtPOST_Result DCDCPOST_Result10;

	FCL_UpdateConditions();

	(void)Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&OBCPOST_Result10);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&DCDCPOST_Result10);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&ECU_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&BatteryState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpPlgLedrCtrl_DePlgLedrCtrl(&DC_out);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Actuator Test Calibratables: Max value is 500*/
	ActuatorCal.timeOn = (uint16)Rte_CData_CalAfts_DelayLedPlugOn()* (uint16)FCL_GAIN_10;
	ActuatorCal.timeOff =  (uint16)Rte_CData_CalAfts_DelayLedRedOff()* (uint16)FCL_GAIN_10;

	FCL_Plug_ActuatorTest(&DC_out,ActuatorCal);

	DiagnosisReady = FCL_PlugLed_diagnosisReady(Last_DCout);

	if(TRUE==Rte_CData_CalEnableLedPlug())
	{
		/*ECU state*/
		if(((APP_STATE_1==ECU_state)||(APP_STATE_4==ECU_state)||(APP_STATE_5==ECU_state))&&(POST_OK == OBCPOST_Result10)&&(POST_OK == DCDCPOST_Result10))
		{
			/*Diagnosis task*/
			FCL_PlugLED_DiagnosisTask(DiagnosisReady,&DC_out,Last_DCout);

		}
		else
		{
			DC_out = 0U;
		}
	}
	else
	{
		/*Led not enable*/
		DC_out = 0U;
		FCL_plugLED_state = FCL_PLUG_DIAG_NO_FAULT;
		for(FaultIndex=0U;(uint8)FCL_FAULT_NUMBER>FaultIndex;FaultIndex++)
		{
			FCL_Plug_Diagnosis[FaultIndex].State=FALSE;
			FCL_Plug_Diagnosis[FaultIndex].DebounceCounter = 0U;
		}
	}

	/*MonitoringConditions*/
	if((TRUE==Rte_CData_CalEnableLedPlug())&&(BAT_VALID_RANGE==BatteryState))
	{
		MonotiringConditions = TRUE;
	}
	else
	{
		MonotiringConditions = FALSE;
	}


	Last_DCout = DC_out;
	(void)Rte_Write_PpExtPlgLedrCtl_DeExtPlgLedrCtl(DC_out);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedMonitoringConditions_DePlugLedMonitoringConditions(MonotiringConditions);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpPlugLedFaults_DeLedPlugFaultOC(FCL_Plug_Diagnosis[FCL_FAULT_OC].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCG(FCL_Plug_Diagnosis[FCL_FAULT_SCG].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpPlugLedFaults_DeLedPlugFaultSCP(FCL_Plug_Diagnosis[FCL_FAULT_SCP].State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_RequestResults (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if(FCL_TEST_ONGOING == FCL_Plug_ActuratorTest.State)
		{
			Out_ComplexStructure_0[0] =(uint8) FCL_RESP_ONGOING;
			*DataLength = 1U;
		}
		else if(0U!=FCL_Plug_ActuratorTest.Result)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_Plug_ActuratorTest.Result;
		}
		else
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_OK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_Plug_ActuratorTest.Result;
		}
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;
	}
  return ret;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Start (returns application error)
 *********************************************************************************************************************/

	uint8 Result = 0U;
	Std_ReturnType ret;
	Dcm_NegativeResponseCodeType Error;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	Error = FCL_ConditionsCheck(&Result);

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if (0U != Result)
		{
			FCL_Plug_ActuratorTest.State = FCL_TEST_DISABLE;
			FCL_Plug_ActuratorTest.Result=Result;
			FCL_Plug_ActuratorTest.Stop = TRUE;

			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 1U;
			*ErrorCode = Error;
			ret = RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;
		}
		else
		{
			FCL_Plug_ActuratorTest.State = FCL_TEST_ONGOING;
			FCL_Plug_ActuratorTest.Result=0U;
			FCL_Plug_ActuratorTest.Stop = FALSE;

			Out_ComplexStructure_0[0]=(uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_Stop (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	FCL_Plug_ActuratorTest.Stop = TRUE;

	if((NULL_PTR!=Out_RSR_04)&&(NULL_PTR!=ErrorCode))
	{
		*Out_RSR_04 = (uint8)FCL_RESP_STOP;
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_LED_for_header_plug_in_box_lock_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_RequestResults (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if(FCL_TEST_ONGOING == FCL_RGB_ActuratorTest[FCL_LED_BLUE].State)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
		}
		else if(0U!=FCL_RGB_ActuratorTest[FCL_LED_BLUE].Result)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_RGB_ActuratorTest[FCL_LED_BLUE].Result;
		}
		else
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_OK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_RGB_ActuratorTest[FCL_LED_BLUE].Result;
		}
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Start (returns application error)
 *********************************************************************************************************************/

	uint8 Result = 0U;
	Std_ReturnType ret;
	Dcm_NegativeResponseCodeType Error;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	Error = FCL_ConditionsCheck(&Result);

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if (0U != Result)
		{
			FCL_RGB_ActuratorTest[FCL_LED_BLUE].State = FCL_TEST_DISABLE;
			FCL_RGB_ActuratorTest[FCL_LED_BLUE].Result=Result;
			FCL_RGB_ActuratorTest[FCL_LED_BLUE].Stop = TRUE;

			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 1U;
			*ErrorCode = Error;
			ret = RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;
		}
		else
		{
			FCL_RGB_ActuratorTest[FCL_LED_BLUE].State = FCL_TEST_ONGOING;
			FCL_RGB_ActuratorTest[FCL_LED_BLUE].Result=0U;
			FCL_RGB_ActuratorTest[FCL_LED_BLUE].Stop = FALSE;

			Out_ComplexStructure_0[0]=(uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_Stop (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	FCL_RGB_ActuratorTest[FCL_LED_BLUE].Stop = TRUE;

	if((NULL_PTR!=Out_RSR_04)&&(NULL_PTR!=ErrorCode))
	{
		*Out_RSR_04 = (uint8)FCL_RESP_STOP;
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_blue_LED_for_header_plug_in_box_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_RequestResults (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if(FCL_TEST_ONGOING == FCL_RGB_ActuratorTest[FCL_LED_GREEN].State)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
		}
		else if(0U!=FCL_RGB_ActuratorTest[FCL_LED_GREEN].Result)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_RGB_ActuratorTest[FCL_LED_GREEN].Result;
		}
		else
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_OK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_RGB_ActuratorTest[FCL_LED_GREEN].Result;
		}
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Start (returns application error)
 *********************************************************************************************************************/

	uint8 Result = 0U;
	Std_ReturnType ret;
	Dcm_NegativeResponseCodeType Error;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	Error = FCL_ConditionsCheck(&Result);

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if (0U != Result)
		{
			FCL_RGB_ActuratorTest[FCL_LED_GREEN].State = FCL_TEST_DISABLE;
			FCL_RGB_ActuratorTest[FCL_LED_GREEN].Result=Result;
			FCL_RGB_ActuratorTest[FCL_LED_GREEN].Stop = TRUE;

			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 1U;
			*ErrorCode = Error;
			ret = RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;
		}
		else
		{
			FCL_RGB_ActuratorTest[FCL_LED_GREEN].State = FCL_TEST_ONGOING;
			FCL_RGB_ActuratorTest[FCL_LED_GREEN].Result=0U;
			FCL_RGB_ActuratorTest[FCL_LED_GREEN].Stop = FALSE;

			Out_ComplexStructure_0[0]=(uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;
	}


  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_Stop (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	FCL_RGB_ActuratorTest[FCL_LED_GREEN].Stop = TRUE;

	if((NULL_PTR!=Out_RSR_04)&&(NULL_PTR!=ErrorCode))
	{
		*Out_RSR_04 = (uint8)FCL_RESP_STOP;
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_green_LED_for_header_plug_in_box_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_RequestResults (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if(FCL_TEST_ONGOING == FCL_RGB_ActuratorTest[FCL_LED_RED].State)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
		}
		else if(0U!=FCL_RGB_ActuratorTest[FCL_LED_RED].Result)
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_RGB_ActuratorTest[FCL_LED_RED].Result;
		}
		else
		{
			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_OK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1]=0U;
			Out_ComplexStructure_0[2] = FCL_RGB_ActuratorTest[FCL_LED_RED].Result;
		}
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Start (returns application error)
 *********************************************************************************************************************/

	uint8 Result = 0U;
	Std_ReturnType ret;
	Dcm_NegativeResponseCodeType Error;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	Error = FCL_ConditionsCheck(&Result);

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if (0U != Result)
		{
			FCL_RGB_ActuratorTest[FCL_LED_RED].State = FCL_TEST_DISABLE;
			FCL_RGB_ActuratorTest[FCL_LED_RED].Result=Result;
			FCL_RGB_ActuratorTest[FCL_LED_RED].Stop = TRUE;

			Out_ComplexStructure_0[0] = (uint8)FCL_RESP_END_NOK;
			*DataLength = 1U;
			*ErrorCode = Error;
			ret = RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;
		}
		else
		{

			FCL_RGB_ActuratorTest[FCL_LED_RED].State = FCL_TEST_ONGOING;
			FCL_RGB_ActuratorTest[FCL_LED_RED].Result=0U;
			FCL_RGB_ActuratorTest[FCL_LED_RED].Stop = FALSE;

			Out_ComplexStructure_0[0]=(uint8)FCL_RESP_ONGOING;
			*DataLength = 1U;
			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApFCL_CODE) RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPFCL_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RRoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_Stop (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	FCL_RGB_ActuratorTest[FCL_LED_RED].Stop = TRUE;

	if((NULL_PTR!=Out_RSR_04)&&(NULL_PTR!=ErrorCode))
	{
		*Out_RSR_04 = (uint8)FCL_RESP_STOP;
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_red_LED_for_header_plug_in_box_E_NOT_OK;
	}
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApFCL_STOP_SEC_CODE
#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static void FCL_UpdateCalibratables(FCL_LEDCal_t LedCalData[FCL_LED_NUMBER], FCL_ActuatorTestCal_t ActuatorTestData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if((NULL_PTR!=LedCalData)&&(NULL_PTR!=ActuatorTestData))
	{
		/*RED*/
		(LedCalData)[FCL_LED_RED].Enable = Rte_CData_CalEnableLedRed();
		(LedCalData)[FCL_LED_RED].SCP_Threshold = Rte_CData_CalLedRedThresholdSCP();
		(LedCalData)[FCL_LED_RED].SCG_Threshold = Rte_CData_CalLedRedThresholdSCG();
		(LedCalData)[FCL_LED_RED].DebounceCal[FCL_FAULT_SCP].ConfirmTime = Rte_CData_CalLedRedSCPConfirmTime();
		(LedCalData)[FCL_LED_RED].DebounceCal[FCL_FAULT_SCP].HealTime = Rte_CData_CalLedRedSCPHealTime();
		(LedCalData)[FCL_LED_RED].DebounceCal[FCL_FAULT_SCG].ConfirmTime = Rte_CData_CalLedRedSCGConfirmTime();
		(LedCalData)[FCL_LED_RED].DebounceCal[FCL_FAULT_SCG].HealTime = Rte_CData_CalLedRedSCGHealTime();
		(LedCalData)[FCL_LED_RED].DebounceCal[FCL_FAULT_OC].ConfirmTime = Rte_CData_CalLedRedOCConfirmTime();
		(LedCalData)[FCL_LED_RED].DebounceCal[FCL_FAULT_OC].HealTime = Rte_CData_CalLedRedOCHealTime();

		(ActuatorTestData)[FCL_LED_RED].timeOn = Rte_CData_CalAfts_DelayLedRedOn();
		(ActuatorTestData)[FCL_LED_RED].timeOff = Rte_CData_CalAfts_DelayLedRedOff();

		/*GREEN*/
		(LedCalData)[FCL_LED_GREEN].Enable = Rte_CData_CalEnableLedGreen();
		(LedCalData)[FCL_LED_GREEN].SCP_Threshold = Rte_CData_CalLedGreenThresholdSCP();
		(LedCalData)[FCL_LED_GREEN].SCG_Threshold = Rte_CData_CalLedGreenThresholdSCG();
		(LedCalData)[FCL_LED_GREEN].DebounceCal[FCL_FAULT_SCP].ConfirmTime = Rte_CData_CalLedGreenSCPConfirmTime();
		(LedCalData)[FCL_LED_GREEN].DebounceCal[FCL_FAULT_SCP].HealTime = Rte_CData_CalLedGreenSCPHealTime();
		(LedCalData)[FCL_LED_GREEN].DebounceCal[FCL_FAULT_SCG].ConfirmTime = Rte_CData_CalLedGreenSCGConfirmTime();
		(LedCalData)[FCL_LED_GREEN].DebounceCal[FCL_FAULT_SCG].HealTime = Rte_CData_CalLedGreenSCGHealTime();
		(LedCalData)[FCL_LED_GREEN].DebounceCal[FCL_FAULT_OC].ConfirmTime = Rte_CData_CalLedGreenOCConfirmTime();
		(LedCalData)[FCL_LED_GREEN].DebounceCal[FCL_FAULT_OC].HealTime = Rte_CData_CalLedGreenOCHealTime();

		(ActuatorTestData)[FCL_LED_GREEN].timeOn = Rte_CData_CalAfts_DelayLedGreenOn();
		(ActuatorTestData)[FCL_LED_GREEN].timeOff = Rte_CData_CalAfts_DelayLedGreenOff();

		/*BLUE*/
		(LedCalData)[FCL_LED_BLUE].Enable = Rte_CData_CalEnableLedBlue();
		(LedCalData)[FCL_LED_BLUE].SCP_Threshold = Rte_CData_CalLedBlueThresholdSCP();
		(LedCalData)[FCL_LED_BLUE].SCG_Threshold = Rte_CData_CalLedBlueThresholdSCG();
		(LedCalData)[FCL_LED_BLUE].DebounceCal[FCL_FAULT_SCP].ConfirmTime = Rte_CData_CalLedBlueSCPConfirmTime();
		(LedCalData)[FCL_LED_BLUE].DebounceCal[FCL_FAULT_SCP].HealTime = Rte_CData_CalLedBlueSCPHealTime();
		(LedCalData)[FCL_LED_BLUE].DebounceCal[FCL_FAULT_SCG].ConfirmTime = Rte_CData_CalLedBlueSCGConfirmTime();
		(LedCalData)[FCL_LED_BLUE].DebounceCal[FCL_FAULT_SCG].HealTime = Rte_CData_CalLedBlueSCGHealTime();
		(LedCalData)[FCL_LED_BLUE].DebounceCal[FCL_FAULT_OC].ConfirmTime = Rte_CData_CalLedBlueOCConfirmTime();
		(LedCalData)[FCL_LED_BLUE].DebounceCal[FCL_FAULT_OC].HealTime = Rte_CData_CalLedBlueOCHealTime();

		(ActuatorTestData)[FCL_LED_BLUE].timeOn = Rte_CData_CalAfts_DelayLedBlueOn();
		(ActuatorTestData)[FCL_LED_BLUE].timeOff = Rte_CData_CalAfts_DelayLedBlueOff();
}

}
static void FCL_Diagnosis_Task(uint8 *DC_out,const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */
	static boolean FCL_PreFault[FCL_LED_NUMBER][FCL_FAULT_NUMBER];

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean firstime = FALSE;

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean DiagnosisEnable;
	uint8 Led_index,Fault_index;
	//boolean PreFault[FCL_LED_NUMBER][FCL_FAULT_NUMBER];

	if (NULL_PTR != LedCalData)
	{

		if (FALSE == firstime)
		{
			/*Initialize data to default value*/
			for(Led_index=0U;(uint8)FCL_LED_NUMBER>Led_index;Led_index++)
			{
				for(Fault_index=0U;(uint8)FCL_FAULT_NUMBER>Fault_index;Fault_index++)
				{
					FCL_PreFault[Led_index][Fault_index]=FALSE;
				}
			}
			firstime = TRUE;
		}

		/* PRQA S 4558, 4404, 4558 ++ #We do not use a native bool type, GNU-C supports this implementation*/
		DiagnosisEnable = 	LedCalData[FCL_LED_RED].Enable
												&& LedCalData[FCL_LED_GREEN].Enable
												&& LedCalData[FCL_LED_BLUE].Enable;
		/* PRQA S 4558, 4404, 4558 -- #We do not use a native bool type, GNU-C supports this implementation*/
		if(TRUE==DiagnosisEnable)
		{
			FCL_DiagnosisStateMachine(DC_out,FCL_PreFault,LedCalData); /* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
		}
		else
		{
			/*Diagnosis not enable.*/
			FCL_DiagnosisState = FCL_DIAG_NO_FAULT;
			/*Do not modify DCout*/
		}

		/*Debounce task*/
		FCL_Debouce_Task(FCL_PreFault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */



		/*Outs*/
		*Rte_Pim_PimLedBluePrefaultOC() = FCL_PreFault[FCL_LED_BLUE][FCL_FAULT_OC];
		*Rte_Pim_PimLedBluePrefaultSCG() = FCL_PreFault[FCL_LED_BLUE][FCL_FAULT_SCG];
		*Rte_Pim_PimLedBluePrefaultSCP() = FCL_PreFault[FCL_LED_BLUE][FCL_FAULT_SCP];
		*Rte_Pim_PimLedGreenPrefaultOC() = FCL_PreFault[FCL_LED_GREEN][FCL_FAULT_OC];
		*Rte_Pim_PimLedGreenPrefaultSCG() = FCL_PreFault[FCL_LED_GREEN][FCL_FAULT_SCG];
		*Rte_Pim_PimLedGreenPrefaultSCP() = FCL_PreFault[FCL_LED_GREEN][FCL_FAULT_SCP];
		*Rte_Pim_PimLedRedPrefaultOC() = FCL_PreFault[FCL_LED_RED][FCL_FAULT_OC];
		*Rte_Pim_PimLedRedPrefaultSCG() = FCL_PreFault[FCL_LED_RED][FCL_FAULT_SCG];
		*Rte_Pim_PimLedRedPrefaultSCP() = FCL_PreFault[FCL_LED_RED][FCL_FAULT_SCP];

		*Rte_Pim_PimLedBlueCounterOC() = (uint16)FCL_RGB_Diagnosis[FCL_LED_BLUE][FCL_FAULT_OC].DebounceCounter;
		*Rte_Pim_PimLedBlueCounterSCG() = (uint16)FCL_RGB_Diagnosis[FCL_LED_BLUE][FCL_FAULT_SCG].DebounceCounter;
		*Rte_Pim_PimLedBlueCounterSCP() = (uint16)FCL_RGB_Diagnosis[FCL_LED_BLUE][FCL_FAULT_SCP].DebounceCounter;
		*Rte_Pim_PimLedGreenCounterOC() = (uint16)FCL_RGB_Diagnosis[FCL_LED_GREEN][FCL_FAULT_OC].DebounceCounter;
		*Rte_Pim_PimLedGreenCounterSCG() = (uint16)FCL_RGB_Diagnosis[FCL_LED_GREEN][FCL_FAULT_SCG].DebounceCounter;
		*Rte_Pim_PimLedGreenCounterSCP() = (uint16)FCL_RGB_Diagnosis[FCL_LED_GREEN][FCL_FAULT_SCP].DebounceCounter;
		*Rte_Pim_PimLedRedCounterOC() = (uint16)FCL_RGB_Diagnosis[FCL_LED_RED][FCL_FAULT_OC].DebounceCounter;
		*Rte_Pim_PimLedRedCounterSCG() = (uint16)FCL_RGB_Diagnosis[FCL_LED_RED][FCL_FAULT_SCG].DebounceCounter;
		*Rte_Pim_PimLedRedCounterSCP() = (uint16)FCL_RGB_Diagnosis[FCL_LED_RED][FCL_FAULT_SCP].DebounceCounter;
	}
}
static void FCL_DiagnosisStateMachine(uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean FaultDetected;
	uint16 LEDfeedBack[FCL_LED_NUMBER];


	/*Input*/
	(void)Rte_Read_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(&FaultDetected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(&(LEDfeedBack[FCL_LED_RED]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(&(LEDfeedBack[FCL_LED_BLUE]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(&(LEDfeedBack[FCL_LED_GREEN]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	switch(FCL_DiagnosisState)
	{

		case FCL_DIAG_NO_FAULT:
			FCL_DiagnosisState_NoFault(FaultDetected,LEDfeedBack,DC_out,Prefault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
			break;

		case FCL_DIAG_SCP_DETECTION:
			FCL_DiagnosisState_SCPdetection(LEDfeedBack,DC_out,Prefault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
			break;

		case FCL_DIAG_SCG_DETECTION:
			FCL_DiagnosisState_SCGdetection(FaultDetected,LEDfeedBack,DC_out,Prefault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
			break;

		case FCL_DIAG_OC_DETECTION:
			FCL_DiagnosisState_OCdetection(FaultDetected,LEDfeedBack, DC_out,Prefault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
			break;

		default:
			/*Not expected*/
			FCL_DiagnosisState_NoFault(FaultDetected,LEDfeedBack,DC_out,Prefault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
			break;
	}

}
static void FCL_DiagnosisState_NoFault(boolean FaultDetected, const uint16 LEDfeedBack[FCL_LED_NUMBER],uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	uint8 Led_index,Fault_index;

	if(FALSE!=FaultDetected)
	{
		/*Fault detected, start diagnosis*/
		FCL_DiagnosisState = FCL_DIAG_SCP_DETECTION;
		/*Values for SCP detection*/
		FCL_DCvalueForSCP(DC_out);
	}
	else
	{
		/*No fault detected*/
		/*Do not modify DCout*/

		/*Initialize data to default value*/
		for(Led_index=0U;(uint8)FCL_LED_NUMBER>Led_index;Led_index++)
		{
			for(Fault_index=0U;(uint8)FCL_FAULT_NUMBER>Fault_index;Fault_index++)
			{
				/* SCG detection clears the fault */
				if ((uint8)FCL_FAULT_SCG != Fault_index)
				{
					Prefault[Led_index][Fault_index]=FALSE;
				}
			}
		}

		if (HW_EDITION_D31 == FCL_HWEdition)
		{
			FCL_DiagnosisSCG_LEDstate = 0U;
			/*Check if SCG is present*/
			FCL_DiagnosisState_SCGdetection(FaultDetected,LEDfeedBack,DC_out,Prefault,LedCalData);/* PRQA S 2784 # QAC is not able to detect the correct dimension of the arrays, but they actually fulfill the required size */
		}
	}
}
static void FCL_DiagnosisState_SCPdetection(const uint16 LEDfeedBack[FCL_LED_NUMBER], uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean FaultLocalized = FALSE;
	uint8 Led_index;

	if ((NULL_PTR != LEDfeedBack) && (NULL_PTR != LedCalData))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
		/*Diagnosis*/
		for(Led_index=0U;(uint8)FCL_LED_NUMBER>Led_index;Led_index++)
		{
			uint32 SCP_Threshold = ((uint32)LedCalData[Led_index].SCP_Threshold * (uint32)FCL_THRESHOLD_GAIN)+(uint32)FCL_THRESHOLD_OFFSET_SCP;
			if((uint32)LEDfeedBack[Led_index]>SCP_Threshold)
			{
				FCL_setPrefault(Led_index,(uint8)FCL_FAULT_SCP,Prefault);
				FaultLocalized = TRUE;
			}
		}

		if(FALSE==FaultLocalized)
		{
			/*prepare SCG detection*/
			FCL_DiagnosisState = FCL_DIAG_OC_DETECTION;
			/*Restart SCG led diagnosis*/
			FCL_DiagnosisSCG_LEDstate = 0U;
			FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);
		}
		else
		{
			/*prepare SCP detection again*/
			FCL_DiagnosisState = FCL_DIAG_SCP_DETECTION;
			/*Values for SCP detection*/
			FCL_DCvalueForSCP(DC_out);
		}
	}
}
static void FCL_DiagnosisState_SCGdetection(boolean FaultDetected, const uint16 LEDfeedBack[FCL_LED_NUMBER],uint8 *DC_out, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */
	static uint32 SCG_Threshold;

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	//uint32 SCG_Threshold;
	uint8 Led_index;
	boolean FaultLocalized = FALSE;

	if ((NULL_PTR != LEDfeedBack) && (NULL_PTR != LedCalData))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
		if (HW_EDITION_D31 == FCL_HWEdition)
		{
			if (FALSE == FaultDetected)
			{

					/*Diagnosis*/
					for(Led_index=0U;(uint8)FCL_LED_NUMBER>Led_index;Led_index++)
					{
						if ((0U < DC_out[Led_index]) && ((uint32)LEDfeedBack[Led_index]>20U))
						{
							SCG_Threshold = (uint32)LedCalData[Led_index].SCG_Threshold * (uint32)FCL_THRESHOLD_GAIN;
							if ((50U > DC_out[Led_index]))
							{
								SCG_Threshold = SCG_Threshold - FCL_THRES_OFFSET;
							}
							if((uint32)LEDfeedBack[Led_index]<SCG_Threshold)
							{
								/*Pre-fault detected, don't leave this state*/
								FCL_setPrefault(Led_index,(uint8)FCL_FAULT_SCG,Prefault);
								//FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);
								FCL_DiagnosisState = FCL_DIAG_SCG_DETECTION;
								FaultLocalized = TRUE;
							}
							else
							{
								FCL_resetPrefault(Led_index,(uint8)FCL_FAULT_SCG,Prefault);
							}
						}
					}

					if(FALSE!=FaultLocalized)
					{
						/*Fault detected*/
					}
			        else
			        {
			            /*No fault detected*/
			            FCL_DiagnosisState = FCL_DIAG_NO_FAULT;
			            /*Do not modify DCout*/
			        }


			}
			else
			{
				FCL_DiagnosisState = FCL_DIAG_NO_FAULT;
				/*Do not modify DCout*/
			}
		}
		else
		{
			SCG_Threshold = (uint32)LedCalData[FCL_DiagnosisSCG_LEDstate].SCG_Threshold * (uint32)FCL_THRESHOLD_GAIN;

			if(((uint32)LEDfeedBack[FCL_DiagnosisSCG_LEDstate] < SCG_Threshold))
			{
				/*Pre-fault detected, don't leave this state*/
				FCL_setPrefault(FCL_DiagnosisSCG_LEDstate,(uint8)FCL_FAULT_SCG,Prefault);
				FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);

			}
			else if(((uint8)FCL_LED_NUMBER-1U)>FCL_DiagnosisSCG_LEDstate)
			{
				/* Clear the previous error if it was set*/
				FCL_resetPrefault(FCL_DiagnosisSCG_LEDstate,(uint8)FCL_FAULT_SCG,Prefault);
				/*Prepare new led*/
				FCL_DiagnosisSCG_LEDstate++;
				FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);

			}
			else
			{

				/*No fault detected*/
				FCL_DiagnosisSCG_LEDstate = 0U;
				FCL_DiagnosisState = FCL_DIAG_NO_FAULT;
				/*Do not modify DCout*/
			}
		}

	}
}

static void FCL_DiagnosisState_OCdetection(boolean FaultDetected, const uint16 LEDfeedBack[FCL_LED_NUMBER],uint8 *DC_out , boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 SCG4OC_Threshold;

	if ((NULL_PTR != LEDfeedBack) && (NULL_PTR != LedCalData))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
        SCG4OC_Threshold = (uint32)LedCalData[FCL_DiagnosisSCG_LEDstate].SCG_Threshold * (uint32)FCL_THRESHOLD_GAIN;

		if((TRUE==FaultDetected) && ((uint32)LEDfeedBack[FCL_DiagnosisSCG_LEDstate]>SCG4OC_Threshold))
		{
			/*Pre-fault detected, don't leave this state*/
			FCL_setPrefault(FCL_DiagnosisSCG_LEDstate,(uint8)FCL_FAULT_OC,Prefault);
			FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);
		}
		else if(((uint8)FCL_LED_NUMBER-1U)>FCL_DiagnosisSCG_LEDstate)
		{
			/*Prepare new led*/
			FCL_DiagnosisSCG_LEDstate++;
			FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);
		}
		else
		{
			if (FALSE!=FaultDetected)
			{
				/*Still fault detected, check SCG*/
				FCL_DiagnosisSCG_LEDstate = 0U;
				FCL_DiagnosisState = FCL_DIAG_SCG_DETECTION;
				if (HW_EDITION_D31 != FCL_HWEdition)
				{
					FCL_DCvalueForSCG(FCL_DiagnosisSCG_LEDstate,DC_out);
				}
			}
			else
			{

				/*No fault detected*/
				FCL_DiagnosisSCG_LEDstate = 0U;
				FCL_DiagnosisState = FCL_DIAG_NO_FAULT;
				/*Do not modify DCout*/
			}
		}
	}

}

static void FCL_DCvalueForSCP(uint8 *DC_out)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(NULL_PTR!=DC_out)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
	{
		(DC_out)[FCL_LED_RED] = 0U;
		(DC_out)[FCL_LED_GREEN] = 0U;
		(DC_out)[FCL_LED_BLUE] = 0U;
	}
}
static void FCL_DCvalueForSCG(uint8 led, uint8 DC_out[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if((NULL_PTR!=DC_out)&&((uint8)FCL_LED_NUMBER>led))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
		(DC_out)[FCL_LED_RED] = 0U;
		(DC_out)[FCL_LED_GREEN] = 0U;
		(DC_out)[FCL_LED_BLUE] = 0U;

		(DC_out)[led] = FCL_DIAGNOSIS_SCG_DC;
	}
}
static void FCL_setPrefault(uint8 led, uint8 fault, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if((NULL_PTR!=Prefault)&&((uint8)FCL_LED_NUMBER>led)&&((uint8)FCL_FAULT_NUMBER>fault))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
		(Prefault)[led][fault]=TRUE;
	}
}

static void FCL_resetPrefault(uint8 led, uint8 fault, boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if((NULL_PTR!=Prefault)&&((uint8)FCL_LED_NUMBER>led)&&((uint8)FCL_FAULT_NUMBER>fault))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
		(Prefault)[led][fault]=FALSE;
	}
}

static void FCL_Debouce_Task(boolean Prefault[FCL_LED_NUMBER][FCL_FAULT_NUMBER], const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])/* PRQA S 3673 # Input parameter kept as non const even if it is not modified. Rationale: Avoid complex data castings in the function call */
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 led_index;
	uint8 fault_index;
	boolean fault_input;
	IdtBatteryVoltageState BatteryState;
	boolean BateryFault;


	if (NULL_PTR != Prefault)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
	{
		(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&BatteryState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


		if(BatteryState!=BAT_VALID_RANGE)
		{
			BateryFault = TRUE;
		}
		else
		{
			BateryFault = FALSE;
		}


		for(led_index=0U;(uint8)FCL_LED_NUMBER>led_index;led_index++)
		{
			for(fault_index=0U;(uint8)FCL_FAULT_NUMBER>fault_index;fault_index++)
			{
				if((TRUE==Prefault[led_index][fault_index])&&(FALSE==BateryFault))
				{
					fault_input = TRUE;
				}
				else
				{
					fault_input = FALSE;
				}
				/*Debounce*/
				FCL_Debounce(&(FCL_RGB_Diagnosis[led_index][fault_index]),fault_input,LedCalData[led_index].DebounceCal[fault_index]);
			}
		}
	}
}
static void FCL_Variant_Task(uint8 DC_out[FCL_LED_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 led_index;
	uint8 fault_index;

	if (NULL_PTR!=LedCalData)
	{
		for(led_index=0U;(uint8)FCL_LED_NUMBER>led_index;led_index++)
		{
			if((TRUE!=LedCalData[led_index].Enable)&&(NULL_PTR!=DC_out))/* PRQA S 2995 #Null pointer checking for robustness*/
			{
				/*Disable Fault and Duty cycle*/
				(DC_out)[led_index] = 0U;
				for(fault_index=0U;(uint8)FCL_FAULT_NUMBER>fault_index;fault_index++)
				{
					FCL_RGB_Diagnosis[led_index][fault_index].State = FALSE;
					FCL_RGB_Diagnosis[led_index][fault_index].DebounceCounter = 0U;
				}
			}

		}
	}
}
static void FCL_MonitoringConditions(boolean MonitoringConditions[FCL_LED_NUMBER],const FCL_LEDCal_t LedCalData[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtBatteryVoltageState BatteryState;
	uint8 led_index;


	(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&BatteryState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(NULL_PTR!=MonitoringConditions)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
	{
		for(led_index=0U;(uint8)FCL_LED_NUMBER>led_index;led_index++)
		{
			if((TRUE==LedCalData[led_index].Enable)&&(BAT_VALID_RANGE==BatteryState))
			{
				(MonitoringConditions)[led_index]=TRUE;
			}
			else
			{
				(MonitoringConditions)[led_index]=FALSE;
			}
		}
	}
}


static void FCL_Debounce(FCL_Debounce_t *DebounceObject, boolean Input, FCL_DebounceCal_t CalData)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 MaxTime;


	if(NULL_PTR!=DebounceObject)
	{
		if(FALSE==Input)
		{
			MaxTime = CalData.HealTime;
		}
		else
		{
			MaxTime = CalData.ConfirmTime;
		}


		if(Input==(DebounceObject->State))
		{
			DebounceObject->DebounceCounter=0U;
		}
		else if((DebounceObject->DebounceCounter)>=MaxTime)
		{
			DebounceObject->State = Input;
			DebounceObject->DebounceCounter=0U;
		}
		else
		{
			DebounceObject->DebounceCounter++;
		}
	}
}


static void FCL_PlugLED_DiagnosisTask(boolean DiagnosisReady, uint8 *DCout, uint8 last_DCout)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FaultIndex;
	uint16 FeedBack;
	boolean Prefault_type[FCL_FAULT_NUMBER];
	IdtBatteryVoltageState BatteryState;
	FCL_DebounceCal_t FCL_Plug_DiagnosisCal[FCL_FAULT_NUMBER];


	(void)Rte_Read_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&BatteryState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Update Calibratables.*/
	FCL_Plug_DiagnosisCal[FCL_FAULT_SCP].ConfirmTime = (uint16)Rte_CData_CalLedPlugSCPConfirmTime() * (uint16)FCL_GAIN_10;
	FCL_Plug_DiagnosisCal[FCL_FAULT_SCP].HealTime = (uint16)Rte_CData_CalLedPlugSCPHealTime() * (uint16)FCL_GAIN_10;
	FCL_Plug_DiagnosisCal[FCL_FAULT_SCG].ConfirmTime = (uint16)Rte_CData_CalLedPlugSCGConfirmTime() * (uint16)FCL_GAIN_10;
	FCL_Plug_DiagnosisCal[FCL_FAULT_SCG].HealTime = (uint16)Rte_CData_CalLedPlugSCGHealTime() * (uint16)FCL_GAIN_10;
	FCL_Plug_DiagnosisCal[FCL_FAULT_OC].ConfirmTime = (uint16)Rte_CData_CalLedPlugOCConfirmTime() * (uint16)FCL_GAIN_10;
	FCL_Plug_DiagnosisCal[FCL_FAULT_OC].HealTime = (uint16)Rte_CData_CalLedPlugOCHealTime() * (uint16)FCL_GAIN_10;

	/*Initialize pre-fault*/
	Prefault_type[FCL_FAULT_SCP] = FALSE;
	Prefault_type[FCL_FAULT_SCG] = FALSE;
	Prefault_type[FCL_FAULT_OC] = FALSE;


	/*State machine*/
	switch(FCL_plugLED_state)
	{
		case FCL_PLUG_DIAG_NO_FAULT:
			FCL_PlugState_NOfault(DiagnosisReady,FeedBack,last_DCout);
			break;

		case FCL_PLUG_DIAG_FAULT:
			FCL_PlugState_fault(DiagnosisReady,FeedBack,Prefault_type,last_DCout);
			break;

		default:
			/*Not expected*/
			FCL_PlugState_NOfault(DiagnosisReady,FeedBack,last_DCout);
			break;
	}

	/*Debounce Output*/
	for(FaultIndex=0U;(uint8)FCL_FAULT_NUMBER>FaultIndex;FaultIndex++)
	{
		boolean Input;
		if((TRUE==Prefault_type[FaultIndex])&&(BAT_VALID_RANGE==BatteryState))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		FCL_Debounce(&(FCL_Plug_Diagnosis[FaultIndex]),Input,FCL_Plug_DiagnosisCal[FaultIndex]);
	}

	/*Outputs*/
	if(NULL_PTR==DCout)
	{
		/*Not expected*/
	}
	else if(FCL_PLUG_DIAG_FAULT==FCL_plugLED_state)
	{
		*DCout = 50U;
	}
	else
	{
		/*Do not modify DCout*/
	}

	*Rte_Pim_PimLedPlugPrefaultOC() = Prefault_type[FCL_FAULT_OC];
	*Rte_Pim_PimLedPlugPrefaultSCG() = Prefault_type[FCL_FAULT_SCG];
	*Rte_Pim_PimLedPlugPrefaultSCP() = Prefault_type[FCL_FAULT_SCP];

	*Rte_Pim_PimLedPlugCounterOC() = (uint16)FCL_Plug_Diagnosis[FCL_FAULT_OC].DebounceCounter;
	*Rte_Pim_PimLedPlugCounterSCG() = (uint16)FCL_Plug_Diagnosis[FCL_FAULT_SCG].DebounceCounter;
	*Rte_Pim_PimLedPlugCounterSCP() = (uint16)FCL_Plug_Diagnosis[FCL_FAULT_SCP].DebounceCounter;

}
static void FCL_PlugState_NOfault(boolean DiagnosisReady,uint16 FeedBack, uint8 last_DCout)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 Threshold_high;
	uint32 Threshold_low;


	if(TRUE==DiagnosisReady)
	{
		if(0U==last_DCout)
		{
			Threshold_high = (uint32)Rte_CData_CalMaxNormalPlugLed()*(uint32)FCL_THRESHOLD_GAIN;
			Threshold_low = (uint32)0U;
		}
		else
		{
			Threshold_high = (uint32)Rte_CData_CalMaxNormalPlugLed()*(uint32)FCL_THRESHOLD_GAIN;
			Threshold_low = (uint32)Rte_CData_CalMinNormalPlugLed()* (uint32)FCL_THRESHOLD_GAIN;
		}

		if(FALSE==FCL_MeasureInRange((uint32)FeedBack, Threshold_low,Threshold_high))
		{
			FCL_plugLED_state = FCL_PLUG_DIAG_FAULT;
		}
	}
}
static void FCL_PlugState_fault(boolean DiagnosisReady, uint16 FeedBack, boolean Prefault[FCL_FAULT_NUMBER], uint8 last_DCout)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if((TRUE==DiagnosisReady) && (NULL_PTR!=Prefault))/* PRQA S 2995 #Null pointer checking for robustness*/
	{
		boolean Faultdetected = FALSE;
		uint32 Threshold_SCG = (uint32)Rte_CData_CalPlugLedThresholdSCG()*(uint32)FCL_THRESHOLD_GAIN;
		uint32 Threshold_SCP = ((uint32)Rte_CData_CalPlugLedThresholdSCP()*(uint32)FCL_THRESHOLD_GAIN) + (uint32)FCL_THRESHOLD_OFFSET_SCP;
		uint32 Threshold_OClow = ((uint32)Rte_CData_CalPlugLedMinOCDetection()*(uint32)FCL_THRESHOLD_GAIN);
		uint32 Threshold_OChigh =  ((uint32)Rte_CData_CalPlugLedMaxOCDetection()*(uint32)FCL_THRESHOLD_GAIN) + (uint32)FCL_THRESHOLD_OFFSET_OC;

		if ((60U>last_DCout))
		{
			Threshold_SCG = Threshold_SCG - (uint32)FCL_THRES_OFFSET;
		}

		if(TRUE==FCL_MeasureInRange(FeedBack,0U,Threshold_SCG))
		{
			(Prefault)[FCL_FAULT_SCG] = TRUE;
			Faultdetected = TRUE;
		}

		if(FALSE==FCL_MeasureInRange(FeedBack,0U,Threshold_SCP))
		{
			(Prefault)[FCL_FAULT_SCP] = TRUE;
			Faultdetected = TRUE;
		}

		if(TRUE==FCL_MeasureInRange(FeedBack,Threshold_OClow,Threshold_OChigh))
		{
			(Prefault)[FCL_FAULT_OC] = TRUE;
			Faultdetected = TRUE;
		}

		if(FALSE==Faultdetected)
		{
			FCL_plugLED_state = FCL_PLUG_DIAG_NO_FAULT;
		}
	}
}
static boolean FCL_MeasureInRange(uint32 Measure, uint32 Low_Threshold, uint32 High_Threshold)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	if((Measure>High_Threshold)||(Measure<Low_Threshold))
	{
		RetVal = FALSE;
	}
	else
	{
		RetVal = TRUE;
	}

	return RetVal;
}
static boolean FCL_PlugLed_diagnosisReady(uint8 Last_DCout)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 DiagnosisCounter;

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 Last_DCout_previous = 0U;

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean DiagnosisReady;


	if(Last_DCout_previous!=Last_DCout)
	{
		DiagnosisCounter = 1U;
	}


	if(FCL_PLUGLED_DIAGNOSIS_TIME>DiagnosisCounter)
	{
		DiagnosisCounter++;
		DiagnosisReady = FALSE;
	}
	else
	{
		DiagnosisReady = TRUE;
	}

	Last_DCout_previous = Last_DCout;

	return DiagnosisReady;

}
static void FCL_RGB_AcutatorTest(uint8 DCout[FCL_LED_NUMBER], const FCL_ActuatorTestCal_t ActuatorTestCal[FCL_LED_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 LedIndex;


	if(NULL_PTR!=DCout)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
	{
		for(LedIndex=0U;(uint8)FCL_LED_NUMBER>LedIndex;LedIndex++)
		{
			if(FCL_TEST_ONGOING==FCL_RGB_ActuratorTest[LedIndex].State)
			{
				boolean StopCondition;
				/*Read Faults*/
				FCL_TestFaultsCheck(&(FCL_RGB_ActuratorTest[LedIndex]),FCL_RGB_Diagnosis[LedIndex]);

				/*End Condition (Error detected or Timeout), Overwrite DCout*/
				StopCondition = FCL_ActuatorTest_DC(&((DCout)[LedIndex]),&(FCL_RGB_ActuratorTest[LedIndex]),ActuatorTestCal[LedIndex]);

				/* In case of OBCP-27193, OBCP-27206 and OBCP-27221 */
				/*if((0U!=(FCL_RGB_ActuratorTest[LedIndex].Result))||(TRUE==StopCondition))*/

				/*Only the stop condition to avoid glitches on status DTCs*/
				if (TRUE==StopCondition)
				{
					FCL_RGB_ActuratorTest[LedIndex].State = FCL_TEST_DISABLE;
				}
			}
			else
			{
				FCL_RGB_ActuratorTest[LedIndex].Counter=0U;
			}
		}
	}
}
static void FCL_Plug_ActuatorTest(uint8 *DCout, const FCL_ActuatorTestCal_t ActuatorTestCal)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(FCL_TEST_ONGOING==FCL_Plug_ActuratorTest.State)
	{
		boolean StopCondition;

		/*Read Faults*/
		FCL_TestFaultsCheck(&FCL_Plug_ActuratorTest,FCL_Plug_Diagnosis);

		/*End Condition (Error detected or Timeout), Overwrite DCout*/
		StopCondition = FCL_ActuatorTest_DC(DCout,&FCL_Plug_ActuratorTest,ActuatorTestCal);

		/* In case of OBCP-27235 */
		/*if((0U!=(FCL_Plug_ActuratorTest.Result))||(TRUE==StopCondition))*/

		/*Only the stop condition to avoid glitches on status DTCs*/
		if (TRUE==StopCondition)
		{
			FCL_Plug_ActuratorTest.State = FCL_TEST_DISABLE;
		}
	}
	else
	{
		FCL_Plug_ActuratorTest.Counter=0U;
	}
}
static boolean FCL_ActuatorTest_DC(uint8 *DCout, FCL_ActuatorTestVar_t *TestVar, FCL_ActuatorTestCal_t TestCal)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean TimeOut = FALSE;


	if((NULL_PTR!=TestVar)&&(NULL_PTR!=DCout))
	{
		if((uint32)(TestVar->Counter)<(uint32)(TestCal.timeOn))
		{
			*DCout = 100U;
			TestVar->Counter++;
		}
		else if((uint32)(TestVar->Counter)<((uint32)(TestCal.timeOn)+(uint32)(TestCal.timeOff)))
		{
			*DCout = 0U;
			TestVar->Counter++;
		}
		else
		{
			TimeOut = TRUE;
		}
	}
	else
	{
		TimeOut = TRUE; /* Avoid infinite diagnostic */
	}
	return TimeOut;
}

static void FCL_TestFaultsCheck(FCL_ActuatorTestVar_t *TestVar, const FCL_Debounce_t LedState[FCL_FAULT_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Fault;

	if((NULL_PTR!=TestVar)&&(NULL_PTR!=LedState))
	{
		TestVar->Result = LedState[FCL_FAULT_SCP].State<<(uint8)FCL_TEST_FAIL_SCP;
		TestVar->Result |= LedState[FCL_FAULT_SCG].State<<(uint8)FCL_TEST_FAIL_SCG;
		TestVar->Result |= LedState[FCL_FAULT_OC].State<<(uint8)FCL_TEST_FAIL_OC;
		TestVar->Result |= (TestVar->Stop)<<(uint8)FCL_TEST_FAIL_STOP;

		if((APP_STATE_4!=FCL_ECU_state)&&(APP_STATE_5!=FCL_ECU_state))
		{
			Fault = TRUE;
		}
		else
		{
			Fault = FALSE;
		}
		TestVar->Result |= Fault<<(uint8)FCL_TEST_FAIL_ECU_STATE;

		(void)FCL_ConditionsCheck(&(TestVar->Result));

	}
}

static Dcm_NegativeResponseCodeType FCL_ConditionsCheck(uint8 *Result)
{
	boolean Fault;
	uint32 threshold;
	Dcm_NegativeResponseCodeType Errortype = DCM_E_CONDITIONSNOTCORRECT;

	if (NULL_PTR != Result)
	{
		/* The priority is set as: the lowest code will be the one with the highest priority,
		 * so for that reason is the last to be evaluated
		 */
		if (FCL_BatteryVoltMinTest > FCL_BatteryLevel)
		{
			Fault = TRUE;
			Errortype = DCM_E_VOLTAGETOOLOW;
		}
		else
		{
			Fault = FALSE;
		}
		*Result |= Fault<<(uint8)FCL_TEST_FAIL_BAT_LEVEL;

		if (FCL_BatteryVoltMaxTest < FCL_BatteryLevel)
		{
			Fault = TRUE;
			Errortype = DCM_E_VOLTAGETOOHIGH;
		}
		else
		{
			Fault = FALSE;
		}
		*Result |= Fault<<(uint8)FCL_TEST_FAIL_BAT_LEVEL;

		threshold = (uint32)FCL_VehStopMaxTest*(uint32)FCL_GAIN_100;
		if(FALSE!= FCL_MeasureInRange(FCL_VehSpd,0U,threshold))
		{
			Fault = FALSE;
		}
		else
		{
			Fault = TRUE;
			Errortype = DCM_E_VEHICLESPEEDTOOHIGH;
		}
		*Result |= Fault<<(uint8)FCL_TEST_FAIL_VEHSPD;

		if(Cx0_PWT_inactive != FCL_VCU_EtatGmpHyb_VCU_EtatGmpHyb)
		{
			Fault = TRUE;
			Errortype = DCM_E_ENGINEISRUNNING;
		}
		else
		{
			Fault = FALSE;
		}
		*Result |= Fault<<(uint8)FCL_TEST_FAIL_ETAT_GMP; /* PRQA S 2985 # The value of the macro FCL_TEST_FAIL_ETAT_GMP is 0. Therefore it is no needed. It has been kept for coherency purposes */
	}

	return Errortype;
}

static void FCL_UpdateConditions(void)
{
	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&FCL_ECU_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&FCL_VehSpd);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&FCL_VCU_EtatGmpHyb_VCU_EtatGmpHyb);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&FCL_BatteryLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Thresholds */
	FCL_VehStopMaxTest = Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();
	FCL_BatteryVoltMinTest = Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
	FCL_BatteryVoltMaxTest = Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
