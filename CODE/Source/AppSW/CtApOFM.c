/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApOFM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApOFM
 *  Generation Time:  2020-11-23 11:42:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApOFM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup OFM
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief OBC Fault Management
 * \details This module manages the fault detection for the OBC. And generates
 * the OBC_SoftStop signal that stops the OBC peripheral.
 *
 * \{
 * \file  OFM.c
 * \brief  Generic code of the OFM module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApOFM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Number of faults.*/
#define OFM_FAULT_NUMBER					27U
/** Conversion gain 10.*/
#define OFM_GAIN_10								10U
/** Tempresture threshold 90C.*/
#define OFM_TEMP_THRESHOLD_90			1300U
/** Voltage threshold 505V.*/
#define OFM_505V									5050U
/** Voltage threshold 210V.*/
#define OFM_210V									2100U
/** Delay to clean second level fault.*/
#define OFM_CLEAN_FAULT_TIME 				2500U
 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

/** Fault configuration.*/
typedef struct{
		/** Fault severity.*/
		IdtFaultLevel Level;
		/** Restart time.*/
		uint32 ReStartTime;
		/** Confirm time for debounce*/
		uint16 ConfirmTime;
}OFM_FaultConf_t;

/** Fault variables.*/
typedef struct{
		/** Debounce counter.*/
		uint16 Counter;
		/** Fault state */
		boolean State;
}OFM_debounce_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Faults recorded during the last charge cycle.*/
static IdtFaultLevel OFM_SnapShot[OFM_FAULT_NUMBER];
/** Fault Variables*/
static IdtFaultLevel OFM_FaultVar[OFM_FAULT_NUMBER];
/** Fault Variables Individual*/
static IdtFaultLevel OFM_FaultVar_individual[OFM_FAULT_NUMBER];
/** Restart Counters.*/
static uint32 OFM_RestartCounter[OFM_FAULT_NUMBER];


/** Time to re-start: Over-temperature.*/
static uint32 OFM_OBCFaultTimeToRetryOvertemp;
/** Time to re-start: Default.*/
static uint32 OFM_OBCFaultTimeToRetryDefault;
/** Time to re-start: over-Voltage.*/
static uint32 OFM_OBCFaultTimeToRetryVoltageError;
/** Over-current debounce time.*/
static uint16 OFM_DebounceOvercurrentDCHV;
/** Number of retrys*/
static uint8 OFM_RetryCounter;

#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static uint8 OFM_DebugData[4] = {0,0,0,0};
static uint8 OFM_UDS_HWFaultData[4] = {0,0,0,0};

static uint8 OFM_PFC_HWFault[4] = {0,0,0,0};
static uint8 OFM_DCHV_HWFault[4] = {0,0,0,0};

static boolean OFM_InternalComFault = FALSE;

#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/

/** Individual fault analysis.*/
static IdtFaultLevel OFM_IndividualCheck(void);
/** Report Fault state.*/
static OBC_Fault OFM_ReportOBCfault(IdtFaultLevel GeneralState);
/** Update Fault state.*/
static void OFM_UpdateFaultState(OFM_FaultConf_t Configuration, boolean Input, OFM_debounce_t *FaultVar,uint8 FaultIndex);

/**
 * \brief Debounce
 * \details Debounce function.
 * \param[in] input Input
 * \param[in] ConfirmTime Confirm Time
 * \param[in] HealTime Heal Time
 * \param[in|out] Debounce Debounce Object
 */
static void OFM_debounce(boolean Input, uint16 ConfirmTime, uint16 HealTime, OFM_debounce_t *Debounce);
/**
 * \brief Get fault level
 * \details This function resturs the fault level depending on the individual faults detected.
 * \param[in] firstLevel Firt level fault
 * \param[in] secondLevel Second level fault
 * \param[in] thirdlevel Third level fault
 * \return Fault level
 */
static IdtFaultLevel OFM_GetFaultLevel(boolean firstLevel, boolean secondLevel, boolean thirdlevel);
/**
 * \brief Individual fault report
 * \details this function is used to force the Second level fault while the time
 * to restart and then force it to not fault.
 */
static IdtFaultLevel OFM_OutputReport(IdtFaultLevel ErrorLevel,uint32 RestartTime,uint32 *RestartCounter);
/** Check fault promotion.*/
static boolean OFM_CheckPromotion(void);
/** Clean Counters to detect new errors.*/
static void OFM_CleanCounters(void);


/** Individual fault function for: DC1PlugOvertemp.*/
static void OFM_Function_DC1PlugOvertemp(uint8 ArrayPosition);
/** Individual fault function for: DC2PlugOvertemp.*/
static void OFM_Function_DC2PlugOvertemp(uint8 ArrayPosition);
/** Individual fault function for: AC1PlugOvertemp.*/
static void OFM_Function_AC1PlugOvertemp(uint8 ArrayPosition);
/** Individual fault function for: AC2PlugOvertemp.*/
static void OFM_Function_AC2PlugOvertemp(uint8 ArrayPosition);
/** Individual fault function for: AC3PlugOvertemp.*/
static void OFM_Function_AC3PlugOvertemp(uint8 ArrayPosition);
/** Individual fault function for: ACNPlugOvertemp.*/
static void OFM_Function_ACNPlugOvertemp(uint8 ArrayPosition);
/** Individual fault function for: DCHVOutputShortCircuit.*/
static void OFM_Function_DCHVOutputShortCircuit(uint8 ArrayPosition);
/** Individual fault function for: DCHVUnderVoltage.*/
static void OFM_Function_DCHVUnderVoltage(uint8 ArrayPosition);
/** Individual fault function for: DCHVOvervoltage.*/
static void OFM_Function_DCHVOvervoltage(uint8 ArrayPosition);
/** Individual fault function for: ElockLocked.*/
static void OFM_Function_ElockLocked(uint8 ArrayPosition);
/** Individual fault function for: ProximityLine.*/
static void OFM_Function_ProximityLine(uint8 ArrayPosition);
/** Individual fault function for: DCHVOutputOvercurrent.*/
static void OFM_Function_DCHVOutputOvercurrent(uint8 ArrayPosition);
/** Individual fault function for: OBCInternalCom.*/
static void OFM_Function_OBCInternalCom(uint8 ArrayPosition);
/** Individual fault function for: PFCHWFault.*/
static void OFM_Function_PFCHWFault(uint8 ArrayPosition);
/** Individual fault function for: DCHVHWFault.*/
static void OFM_Function_DCHVHWFault(uint8 ArrayPosition);
/** Individual fault function for: OutputVoltageDCHV.*/
static void OFM_Function_OutputVoltageDCHV(uint8 ArrayPosition);
/** Individual fault function for: BatteryVoltage.*/
static void OFM_Function_BatteryVoltage(uint8 ArrayPosition);
/** Individual fault function for: EfficiencyDCLV.*/
static void OFM_Function_EfficiencyDCLV(uint8 ArrayPosition);
/** Individual fault function for: EfficiencyMonophasic.*/
static void OFM_Function_EfficiencyMonophasic(uint8 ArrayPosition);
/** Individual fault function for: EfficiencyTriphasic.*/
static void OFM_Function_EfficiencyTriphasic(uint8 ArrayPosition);
/** Individual fault function for: DCTemperature.*/
static void OFM_Function_DCTemperature(uint8 ArrayPosition);
/** Individual fault function for: ACTemperatureMono.*/
static void OFM_Function_ACTemperatureMono(uint8 ArrayPosition);
/** Individual fault function for: ACTemperatureTri.*/
static void OFM_Function_ACTemperatureTri(uint8 ArrayPosition);
/** Individual fault function for: SW fault.*/
static void OFM_Function_SWFault(uint8 ArrayPosition);
/** Individual fault function for: No AC detection.*/
static void OFM_Function_ACnoDetected(uint8 ArrayPosition);

/** Update calibratables*/
static void OFM_UpdateCalibratables(void);

/**
 * Publish DTC triggers
 */
static void OFM_DTCTriggerReport(void);

/**
 * Get TRUE if any error fault is present
 */
static boolean OFM_GetFaultPresent(uint8 ArrayPosition);
/**Clean snapshot*/
static void OFM_CleanSanpShot(void);
/** Set HW fault for debuging*/
static void OFM_Set_HW_fault(uint8 position, boolean Value);

static boolean OFM_GetOvertempFault(boolean NTC1_M1_fault, boolean NTC1_M3_fault, boolean NTC1_M4_fault, boolean NTC_MOD5_fault, boolean NTC_MOD6_fault);

static OBC_Fault OFM_DecodeReportStateNoFaultOrLevel3(IdtFaultLevel GeneralState);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * DCHV_IOM_ERR_CAP_FAIL_H: Boolean
 * DCHV_IOM_ERR_CAP_FAIL_L: Boolean
 * DCHV_IOM_ERR_OC_IOUT: Boolean
 * DCHV_IOM_ERR_OC_NEG: Boolean
 * DCHV_IOM_ERR_OC_POS: Boolean
 * DCHV_IOM_ERR_OT: Boolean
 * DCHV_IOM_ERR_OT_IN: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD5: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD6: Boolean
 * DCHV_IOM_ERR_OV_VOUT: Boolean
 * DCHV_IOM_ERR_UV_12V: Boolean
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtDebounceOvercurrentDCHV: Integer in interval [0...10]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtOBCFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryOvertemp: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryVoltageError: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_MaxNumberRetries: Integer in interval [0...60]
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_PlugVoltDetection: Boolean
 * PFC_IOM_ERR_OC_PH1: Boolean
 * PFC_IOM_ERR_OC_PH2: Boolean
 * PFC_IOM_ERR_OC_PH3: Boolean
 * PFC_IOM_ERR_OC_SHUNT1: Boolean
 * PFC_IOM_ERR_OC_SHUNT2: Boolean
 * PFC_IOM_ERR_OC_SHUNT3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M1: Boolean
 * PFC_IOM_ERR_OT_NTC1_M3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M4: Boolean
 * PFC_IOM_ERR_OV_DCLINK: Boolean
 * PFC_IOM_ERR_OV_VPH12: Boolean
 * PFC_IOM_ERR_OV_VPH23: Boolean
 * PFC_IOM_ERR_OV_VPH31: Boolean
 * PFC_IOM_ERR_UVLO_ISO4: Boolean
 * PFC_IOM_ERR_UVLO_ISO7: Boolean
 * PFC_IOM_ERR_UV_12V: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 *   FAULT_LEVEL_NO_FAULT (0U)
 *   FAULT_LEVEL_1 (1U)
 *   FAULT_LEVEL_2 (2U)
 *   FAULT_LEVEL_3 (3U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 * Array Types:
 * ============
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtFaultLevel *Rte_Pim_PimOBC_AC1PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_AC2PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_AC3PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ACNPlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ActiveDischarge_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_BatteryHVUndervoltage_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ControlPilot_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DC1PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DC2PlugOvertemp_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVHWFault_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DCHVOvervoltage_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_DiagnosticNoACInput_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ElockLocked_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_FaultSatellites_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_OBCInternalCom_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_PFCHWFault_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_ProximityLine_Error(void)
 *   IdtFaultLevel *Rte_Pim_PimOBC_VCUModeEPSRequest_Error(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtOBC_MaxNumberRetries Rte_CData_CalOBC_MaxNumberRetries(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtOBCFaultTimeToRetryDefault Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(void)
 *   IdtOBCFaultTimeToRetryOvertemp Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(void)
 *   IdtOBCFaultTimeToRetryVoltageError Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(void)
 *   IdtDebounceOvercurrentDCHV Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV(void)
 *
 *********************************************************************************************************************/


#define CtApOFM_START_SEC_CODE
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_OFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;		/* Remove compilator warnings */

	 *ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_OFM_HW_faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_OFM_HW_faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_HW_faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) DataServices_OFM_HW_faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_OFM_HW_faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;		/* Remove compilator warnings */

	Data[0] = OFM_UDS_HWFaultData[0];
	Data[1] = OFM_UDS_HWFaultData[1];
	Data[2] = OFM_UDS_HWFaultData[2];
	Data[3] = OFM_UDS_HWFaultData[3];

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOFM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RCtApOFM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	 uint8 index;


	 OFM_DebounceOvercurrentDCHV = (uint16)Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV();
	 OFM_OBCFaultTimeToRetryOvertemp = (uint32)OFM_GAIN_10 * (uint32)Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp();
	 OFM_OBCFaultTimeToRetryDefault = (uint32)OFM_GAIN_10 * (uint32)Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault();
	 OFM_OBCFaultTimeToRetryVoltageError = (uint32)OFM_GAIN_10 * (uint32)Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError();
	 OFM_RetryCounter = 0U;


	 for(index=0U;index<(uint8)OFM_FAULT_NUMBER;index++)
	 {
		OFM_SnapShot[index] = FAULT_LEVEL_NO_FAULT;
		OFM_RestartCounter[index] = 0U;
		OFM_FaultVar[index]= FAULT_LEVEL_NO_FAULT;
	 }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOFM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean *data)
 *   Std_ReturnType Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(boolean *data)
 *   Std_ReturnType Rte_Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(IdtFaultLevel *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_Fault_OBC_Fault(OBC_Fault data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeInlet_OvertempACSensorFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_HWErrors_Fault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_InternalComFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_OvertemperatureFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RCtApOFM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOFM_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean OFM_FirstLevel = FALSE;

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static IdtFaultLevel GeneralLevel_previous = FAULT_LEVEL_NO_FAULT;

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel GeneralLevel;
	OBC_Fault OFM_FaultState;
	boolean OFM_OBCerror = FALSE;
	uint8 index;
	uint8 byte_index;
	uint8 bit_index;
	OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation;

	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&CHG_OBC_ConnectionConfirmation);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	OFM_UpdateCalibratables();

	if(Cx1_not_connected==CHG_OBC_ConnectionConfirmation)
	{
		/*Clean Permanent first level fault*/
		OFM_FirstLevel = FALSE;
	}

	GeneralLevel = OFM_IndividualCheck();

	if((FAULT_LEVEL_1==GeneralLevel)||(TRUE==OFM_FirstLevel))
	{
		OFM_FirstLevel = TRUE;
		OFM_OBCerror = TRUE;
		GeneralLevel = FAULT_LEVEL_1;
	}
	else if(FAULT_LEVEL_2==GeneralLevel)
	{
		OFM_OBCerror = TRUE;

		if(FAULT_LEVEL_2!=GeneralLevel_previous)
		{
			if(TRUE==OFM_CheckPromotion())
			{
				OFM_FirstLevel = TRUE;
				GeneralLevel = FAULT_LEVEL_1;
			}
		}
	}
	else if(FAULT_LEVEL_2==GeneralLevel_previous)
	{
		OFM_CleanCounters();
	}
	else
	{
		/*MISRA*/
	}

	GeneralLevel_previous = GeneralLevel;

	OFM_FaultState = OFM_ReportOBCfault(GeneralLevel);

	(void)Rte_Write_PpFaultChargeSoftStop_DeFaultChargeSoftStop(OFM_OBCerror);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_Fault_OBC_Fault(OFM_FaultState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	OFM_DTCTriggerReport();


	/* Info for diagnostics */
	for (index = 0; index < OFM_FAULT_NUMBER; index++)
	{

		byte_index = index >>3;
		bit_index = index % 8U;

		if (OFM_FaultVar[index] != FAULT_LEVEL_NO_FAULT)
		{
			OFM_DebugData[byte_index] |= (1U << bit_index);
		}
	}

	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,0U,OFM_DebugData[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,1U,OFM_DebugData[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,2U,OFM_DebugData[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,3U,OFM_DebugData[3]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,4U,OFM_UDS_HWFaultData[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,5U,OFM_UDS_HWFaultData[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,6U,OFM_UDS_HWFaultData[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(1U,7U,OFM_UDS_HWFaultData[3]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_OFM_Faults_DataRecord_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_OFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;		/* Remove compilator warnings */

	*ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_OFM_Faults_DataRecord_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_OFM_Faults_DataRecord>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_OFM_Faults_DataRecord_DCM_E_PENDING
 *   RTE_E_DataServices_OFM_Faults_DataRecord_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApOFM_CODE) RDataServices_OFM_Faults_DataRecord_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_OFM_Faults_DataRecord_ReadData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;		/* Remove compilator warnings */

	Data[0] = OFM_DebugData[0];
	Data[1] = OFM_DebugData[1];
	Data[2] = OFM_DebugData[2];
	Data[3] = OFM_DebugData[3];


  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpOFMGetDCHVHWFaults> of PortPrototype <PpOFMGetDCHVHWFaults>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetDCHVHWFaults_OpOFMGetDCHVHWFaults
 *********************************************************************************************************************/
	data[0] = OFM_DCHV_HWFault[0];
	data[1] = OFM_DCHV_HWFault[1];
	data[2] = OFM_DCHV_HWFault[2];
	data[3] = OFM_DCHV_HWFault[3];

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpOFMGetPFCHWFaults> of PortPrototype <PpOFMGetPFCHWFaults>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOFM_CODE) RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults(P2VAR(uint8, AUTOMATIC, RTE_CTAPOFM_APPL_VAR) data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpOFMGetPFCHWFaults_OpOFMGetPFCHWFaults
 *********************************************************************************************************************/

	data[0] = OFM_PFC_HWFault[0];
	data[1] = OFM_PFC_HWFault[1];
	data[2] = OFM_PFC_HWFault[2];
	data[3] = OFM_PFC_HWFault[3];

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApOFM_STOP_SEC_CODE
#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static IdtFaultLevel OFM_IndividualCheck(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean FirstLevel = FALSE;
	boolean SecondLevel = FALSE;
	boolean ThirdLevel = FALSE;
	uint8 index;


	/*the argument is the array position.*/
	OFM_Function_DC1PlugOvertemp(0U);
	OFM_Function_DC2PlugOvertemp(1U);
	OFM_Function_AC1PlugOvertemp(2U);
	OFM_Function_AC2PlugOvertemp(3U);
	OFM_Function_AC3PlugOvertemp(4U);
	OFM_Function_ACNPlugOvertemp(5U);
	/*CP fault has been removed*/
	OFM_FaultVar[6] = FAULT_LEVEL_NO_FAULT;
	OFM_Function_DCHVOutputShortCircuit(7U);
	OFM_Function_DCHVUnderVoltage(8U);
	OFM_Function_DCHVOvervoltage(9U);
	OFM_Function_ElockLocked(10U);
	OFM_Function_ProximityLine(11U);
	/*Active Discharge remove*/
	OFM_FaultVar[12] = FAULT_LEVEL_NO_FAULT;
	OFM_Function_DCHVOutputOvercurrent(13U);
	OFM_Function_OBCInternalCom(14U);
	OFM_Function_PFCHWFault(15U);
	OFM_Function_DCHVHWFault(16U);
	OFM_Function_OutputVoltageDCHV(17U);
	OFM_Function_BatteryVoltage(18U);
	OFM_Function_EfficiencyDCLV(19U);
	OFM_Function_EfficiencyMonophasic(20U);
	OFM_Function_EfficiencyTriphasic(21U);
	OFM_Function_DCTemperature(22U);
	OFM_Function_ACTemperatureMono(23U);
	OFM_Function_ACTemperatureTri(24U);
	OFM_Function_SWFault(25U);
	OFM_Function_ACnoDetected(26U);

	for(index=0U;index<(uint8)OFM_FAULT_NUMBER;index++)
	{
		if(FAULT_LEVEL_1==OFM_FaultVar[index])
		{
			FirstLevel = TRUE;
		}
		else if(FAULT_LEVEL_2==OFM_FaultVar[index])
		{
			SecondLevel = TRUE;
		}
		else if(FAULT_LEVEL_3==OFM_FaultVar[index])
		{
			ThirdLevel = TRUE;
		}
		else
		{
			/*MISRA*/
		}
	}

	return OFM_GetFaultLevel(FirstLevel,SecondLevel,ThirdLevel);


}

static OBC_Fault OFM_ReportOBCfault(IdtFaultLevel GeneralState)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 DebounceCounter = 0U;

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OBC_Fault ReportState = OBC_FAULT_NO_FAULT;

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_Status OBC_State;
	OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation;

	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_State);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&CHG_OBC_ConnectionConfirmation);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx1_not_connected==CHG_OBC_ConnectionConfirmation)
	{
		/*Clean general fault level*/
		ReportState = OBC_FAULT_NO_FAULT;
		DebounceCounter = 0U;
		OFM_RetryCounter = 0U;
		OFM_CleanSanpShot();
	}
	else if(FAULT_LEVEL_1==GeneralState)
	{
		ReportState = OBC_FAULT_LEVEL_1;
		DebounceCounter = 0U;
	}
	else if(FAULT_LEVEL_2==GeneralState)
	{
		ReportState = OBC_FAULT_LEVEL_2;
		DebounceCounter = 0U;
	}
	else if((ReportState == OBC_FAULT_NO_FAULT)||(ReportState == OBC_FAULT_LEVEL_3))
	{
		DebounceCounter = 0U;

		/* This function has ben created to reduce CC in UT */
		ReportState = OFM_DecodeReportStateNoFaultOrLevel3(GeneralState);

	}
	else if((Cx3_conversion_working_ != OBC_State)&&(Cx5_degradation_mode != OBC_State))
	{
		DebounceCounter = 0U;
	}
	else if(OFM_CLEAN_FAULT_TIME>DebounceCounter)
	{
		DebounceCounter ++;
	}
	else
	{
		/* This function has ben created to reduce CC in UT */
		ReportState = OFM_DecodeReportStateNoFaultOrLevel3(GeneralState);
		OFM_CleanSanpShot();
	}

	return ReportState;
}


static OBC_Fault OFM_DecodeReportStateNoFaultOrLevel3(IdtFaultLevel GeneralState)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_Fault ReportState;


	if(FAULT_LEVEL_3==GeneralState)
	{
		ReportState = OBC_FAULT_LEVEL_3;
	}
	else
	{
		ReportState = OBC_FAULT_NO_FAULT;
	}

	return ReportState;

}

static void OFM_UpdateFaultState(OFM_FaultConf_t Configuration, boolean Input,OFM_debounce_t *FaultVar, uint8 FaultIndex)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel ErrorLevel;

	OFM_debounce(Input,Configuration.ConfirmTime,0U,FaultVar);

	if(FaultVar==NULL_PTR)
	{
		if(TRUE==Input)
		{
			ErrorLevel = Configuration.Level;
		}
		else
		{
			ErrorLevel = FAULT_LEVEL_NO_FAULT;
		}
	}
	else if(TRUE==FaultVar->State)
	{
		ErrorLevel = Configuration.Level;
	}
	else
	{
		ErrorLevel = FAULT_LEVEL_NO_FAULT;
	}

	OFM_FaultVar_individual[FaultIndex] = ErrorLevel;

	ErrorLevel = OFM_OutputReport(ErrorLevel,Configuration.ReStartTime,&OFM_RestartCounter[FaultIndex]);

	OFM_FaultVar[FaultIndex] = ErrorLevel;

}
static IdtFaultLevel OFM_GetFaultLevel(boolean firstLevel, boolean secondLevel, boolean thirdlevel)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel retVal;


	if(TRUE==firstLevel)
	{
		retVal = FAULT_LEVEL_1 ;
	}
	else if(TRUE==secondLevel)
	{
		retVal = FAULT_LEVEL_2 ;
	}
	else if(TRUE==thirdlevel)
	{
		retVal = FAULT_LEVEL_3 ;
	}
	else
	{
		retVal = FAULT_LEVEL_NO_FAULT ;
	}

	return retVal;
}
static void OFM_debounce(boolean Input, uint16 ConfirmTime, uint16 HealTime, OFM_debounce_t *Debounce)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 CounterMax;


	if(NULL_PTR != Debounce)
	{

		if(TRUE==Input)
		{
			CounterMax = ConfirmTime;
		}
		else
		{
			CounterMax = HealTime;
		}

		if(Input==Debounce->State)
		{
			Debounce->Counter = 0U;
		}
		else if(Debounce->Counter>=CounterMax)
		{
			Debounce->State = Input;
			Debounce->Counter = 0U;
		}
		else
		{
			Debounce->Counter ++;
		}
	}
}
static IdtFaultLevel  OFM_OutputReport(IdtFaultLevel ErrorLevel,uint32 RestartTime,uint32 *RestartCounter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel RetVal;
	OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation;

	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&CHG_OBC_ConnectionConfirmation);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(NULL_PTR==RestartCounter)
	{
		RetVal = ErrorLevel;
	}
	else if(Cx1_not_connected==CHG_OBC_ConnectionConfirmation)
	{
		/*Clean restart counter*/
		(*RestartCounter)=0U;
		RetVal = ErrorLevel;
	}
	else if(0U==*RestartCounter)
	{
		if(FAULT_LEVEL_2==ErrorLevel)
		{
			/*Initialize time to restart*/
			RetVal = FAULT_LEVEL_2;
			(*RestartCounter)++;
		}
		else
		{
			/*Waiting for second level error...*/
			RetVal = ErrorLevel;
		}
	}
	else if(*RestartCounter>=RestartTime)
	{
		/*Force No fault*/
		RetVal = FAULT_LEVEL_NO_FAULT;
	}
	else
	{
		RetVal = FAULT_LEVEL_2;
		(*RestartCounter)++;
	}


	return RetVal;
}
static void OFM_CleanCounters(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;

	for(index=0U;index<(uint8)OFM_FAULT_NUMBER;index++)
	{
		OFM_RestartCounter[index] = 0U;
	}
}
static boolean OFM_CheckPromotion(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;
	boolean RetVal = FALSE;


	for(index=0U;index<(uint8)OFM_FAULT_NUMBER;index++)
	{
		if((FAULT_LEVEL_2==OFM_SnapShot[index])
				&&(FAULT_LEVEL_2==OFM_FaultVar[index]))
		{
			RetVal=TRUE;
		}
		OFM_SnapShot[index] = OFM_FaultVar[index];
	}

	if(FALSE==RetVal)
	{
		/*No promoted, clean retry counter.*/
		OFM_RetryCounter=0U;
	}
	else
	{
		/*Same fault than before, increase retry counter.*/
		OFM_RetryCounter++;
	}


	if(Rte_CData_CalOBC_MaxNumberRetries()<=OFM_RetryCounter)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}


static void OFM_Function_DC1PlugOvertemp(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	IdtOutputTempMeas OutputTempDC1Meas;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryOvertemp;

	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(&OutputTempDC1Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_TEMP_THRESHOLD_90<OutputTempDC1Meas)&&(Cx3_Euro_fast_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DC1PlugOvertemp_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_DC2PlugOvertemp(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	boolean Input;
	OFM_FaultConf_t Configuration;
	IdtOutputTempMeas OutputTempDC2Meas;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryOvertemp;


	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(&OutputTempDC2Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_TEMP_THRESHOLD_90<OutputTempDC2Meas)&&(Cx3_Euro_fast_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DC2PlugOvertemp_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_AC1PlugOvertemp(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	IdtOutputTempMeas OutputTempAC1Meas;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryOvertemp;


	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&OutputTempAC1Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_TEMP_THRESHOLD_90<OutputTempAC1Meas)&&(Cx1_slow_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_AC1PlugOvertemp_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_AC2PlugOvertemp(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	IdtOutputTempMeas OutputTempAC2Meas;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryOvertemp;


	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(&OutputTempAC2Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_TEMP_THRESHOLD_90<OutputTempAC2Meas)&&(Cx1_slow_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_AC2PlugOvertemp_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_AC3PlugOvertemp(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	IdtOutputTempMeas OutputTempAC3Meas;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryOvertemp;


	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(&OutputTempAC3Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_TEMP_THRESHOLD_90<OutputTempAC3Meas)&&(Cx1_slow_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_AC3PlugOvertemp_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_ACNPlugOvertemp(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	IdtOutputTempMeas OutputTempACNMeas;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryOvertemp;

	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(&OutputTempACNMeas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_TEMP_THRESHOLD_90<OutputTempACNMeas)&&(Cx1_slow_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_ACNPlugOvertemp_Error())=OFM_FaultVar[ArrayPosition];
	}
}

static void OFM_Function_DCHVOutputShortCircuit(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	OBC_Status OBC_Status_CANSignal;
	DCHV_IOM_ERR_OC_IOUT DCHV_IOM_ERR_OC_IOUT_CANSignal;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;

	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(&DCHV_IOM_ERR_OC_IOUT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(((Cx3_conversion_working_==OBC_Status_CANSignal)
				||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&(FALSE!=DCHV_IOM_ERR_OC_IOUT_CANSignal))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DCHVOutputShortCircuit_Error())=OFM_FaultVar[ArrayPosition];
	}
}

static void OFM_Function_DCHVOvervoltage(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	DCHV_ADC_VOUT DCHV_ADC_VOUT_CANSignal;
	DCLV_Input_Voltage DCLV_Input_Voltage_CANSignal;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryVoltageError;

	/*Read RTE*/
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_ADC_VOUT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&DCLV_Input_Voltage_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(((OFM_505V/OFM_GAIN_10)<DCLV_Input_Voltage_CANSignal)||(OFM_505V<DCHV_ADC_VOUT_CANSignal))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DCHVOvervoltage_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_ElockLocked(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	OBC_Status OBC_Status_CANSignal;
	IdtOutputELockSensor OutputElockSensor;


	Configuration.Level = FAULT_LEVEL_1;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = 0U;

	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&OutputElockSensor);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(((Cx3_conversion_working_==OBC_Status_CANSignal)
						||(Cx5_degradation_mode==OBC_Status_CANSignal))
						&&(ELOCK_LOCKED!=OutputElockSensor))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_ElockLocked_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_ProximityLine(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	OBC_Status OBC_Status_CANSignal;
	OBC_ChargingConnectionConfirmati OBC_ChargingConnection;


	Configuration.Level = FAULT_LEVEL_1;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = 0U;

	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&OBC_ChargingConnection);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(((			Cx3_conversion_working_==OBC_Status_CANSignal)
						||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&(Cx2_full_connected!=OBC_ChargingConnection))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_ProximityLine_Error())=OFM_FaultVar[ArrayPosition];
	}
}

static void OFM_Function_DCHVOutputOvercurrent(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	OBC_Status OBC_Status_CANSignal;
	DCHV_ADC_IOUT DCHV_ADC_IOUT_CANSignal;
	DCHV_Command_Current_Reference Command_Current;
	DCHV_DCHVStatus DCHVstatus;
	uint32 Imax;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = OFM_DebounceOvercurrentDCHV;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;

	/*Read RTE*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&DCHVstatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&DCHV_ADC_IOUT_CANSignal ); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(&Command_Current); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	Imax = (uint32)Command_Current + ((uint32)Command_Current>>1);

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(((Cx3_conversion_working_==OBC_Status_CANSignal)
				||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&((uint32)DCHV_ADC_IOUT_CANSignal>Imax)
				&&(Cx1_DCHV_STATUS_RUN==DCHVstatus))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DCHVOutputOvercurrent_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_OBCInternalCom(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};
	static uint16 OFM_initialDelay=0U;

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	boolean OBCFramesReception_Error;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;
	boolean OBC_postOK;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;

	/*Read RTE*/
	(void)Rte_Read_PpOBCFramesReception_Error_DeOBCFramesReception_Error(&OBCFramesReception_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(&OBC_postOK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((OFM_initialDelay<1500U)&&(FALSE==OBC_postOK))
		{
			/*wait until the POST is over or 5seg*/
			OFM_initialDelay++;
			Input = FALSE;
		}
		else if((FALSE!=OBCFramesReception_Error)&&(Cx1_slow_charging==OBC_ChargingMode_CANSigna))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_InternalComFault = Input;

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_OBCInternalCom_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_PFCHWFault(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	boolean Fault_IN;
	boolean Fault_OUT;

	OBC_ChargingMode OBC_ChargingMode_CANSigna;
	OBC_Status OBC_Status_CANSignal;

	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 150U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;

	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*Read RTE*/
	(void)Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT = Fault_IN;
	OFM_Set_HW_fault(11U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(12U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(13U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(14U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(15U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(16U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(17U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(18U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(19U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(20U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(21U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(22U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(23U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(24U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(25U,Fault_IN);
	(void)Rte_Read_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(26U,Fault_IN);

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((Cx1_slow_charging==OBC_ChargingMode_CANSigna)
				&&((Cx3_conversion_working_==OBC_Status_CANSignal)||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&(FALSE!=Fault_OUT))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_PFCHWFault_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_DCHVHWFault(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	boolean Fault_IN;
	boolean Fault_OUT;

	OBC_ChargingMode OBC_ChargingMode_CANSigna;
	OBC_Status OBC_Status_CANSignal;

	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 0U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;

	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Read RTE*/
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT = Fault_IN;
	OFM_Set_HW_fault(0U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(1U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(2U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(3U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(4U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(5U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(6U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(7U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(8U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(9U,Fault_IN);
	(void)Rte_Read_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(&Fault_IN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Fault_OUT |= Fault_IN;
	OFM_Set_HW_fault(10U,Fault_IN);



	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if((Cx1_slow_charging==OBC_ChargingMode_CANSigna)
				&&((Cx3_conversion_working_==OBC_Status_CANSignal)||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&(FALSE!=Fault_OUT))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DCHVHWFault_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_OutputVoltageDCHV(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_OutputVoltageDCHV_Error;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(&OBC_OutputVoltageDCHV_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_OutputVoltageDCHV_Error = OFM_OutputReport(OBC_OutputVoltageDCHV_Error,OFM_OBCFaultTimeToRetryVoltageError,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_OutputVoltageDCHV_Error;

	}
}
static void OFM_Function_BatteryVoltage(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_BatteryVoltage_Erro;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(&OBC_BatteryVoltage_Erro);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_BatteryVoltage_Erro = OFM_OutputReport(OBC_BatteryVoltage_Erro,OFM_OBCFaultTimeToRetryVoltageError,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_BatteryVoltage_Erro;
	}
}
static void OFM_Function_EfficiencyDCLV(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_EfficiencyDCLV_Error;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(&OBC_EfficiencyDCLV_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_EfficiencyDCLV_Error = OFM_OutputReport(OBC_EfficiencyDCLV_Error,OFM_OBCFaultTimeToRetryVoltageError,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_EfficiencyDCLV_Error;
	}
}
static void OFM_Function_EfficiencyMonophasic(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_EfficiencyMonophasic_Error;
	/*Read RTE*/
	(void)Rte_Read_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(&OBC_EfficiencyMonophasic_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_EfficiencyMonophasic_Error = OFM_OutputReport(OBC_EfficiencyMonophasic_Error,OFM_OBCFaultTimeToRetryVoltageError,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_EfficiencyMonophasic_Error;
	}
}
static void OFM_Function_EfficiencyTriphasic(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_EfficiencyTriphasic_Error;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(&OBC_EfficiencyTriphasic_Error); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_EfficiencyTriphasic_Error = OFM_OutputReport(OBC_EfficiencyTriphasic_Error,OFM_OBCFaultTimeToRetryVoltageError,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_EfficiencyTriphasic_Error;
	}
}
static void OFM_Function_DCTemperature(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_DCTemperature_Error;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(&OBC_DCTemperature_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_DCTemperature_Error = OFM_OutputReport(OBC_DCTemperature_Error,OFM_OBCFaultTimeToRetryOvertemp,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_DCTemperature_Error;
	}
}
static void OFM_Function_ACTemperatureMono(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_ACTemperatureMono_Error;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(&OBC_ACTemperatureMono_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/



	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_ACTemperatureMono_Error = OFM_OutputReport(OBC_ACTemperatureMono_Error,OFM_OBCFaultTimeToRetryOvertemp,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_ACTemperatureMono_Error;
	}
}
static void OFM_Function_ACTemperatureTri(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel OBC_ACTemperatureTri_Error;


	/*Read RTE*/
	(void)Rte_Read_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(&OBC_ACTemperatureTri_Error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		OBC_ACTemperatureTri_Error = OFM_OutputReport(OBC_ACTemperatureTri_Error,OFM_OBCFaultTimeToRetryOvertemp,&OFM_RestartCounter[ArrayPosition]);
		OFM_FaultVar[ArrayPosition] = OBC_ACTemperatureTri_Error;
	}
}
static void OFM_Function_DCHVUnderVoltage(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	DCHV_ADC_VOUT DCHV_ADC_VOUT_CANSignal;
	OBC_Status OBC_Status_CANSignal;


#if(APP_USE_RESISTANCES==APP_NO_RESISTANCES)
	Configuration.Level = FAULT_LEVEL_2;
#elif(APP_USE_RESISTANCES==APP_ALLOW_RESISTANCES)
	Configuration.Level = FAULT_LEVEL_3;
#else
	#error "Configured resistance version not valid"
#endif

	Configuration.ConfirmTime = 10U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;


	/*Read RTE*/
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_ADC_VOUT_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(((Cx3_conversion_working_==OBC_Status_CANSignal)||(Cx5_degradation_mode==OBC_Status_CANSignal))
				&&(OFM_210V>DCHV_ADC_VOUT_CANSignal))
		{
			Input = TRUE;
		}
		else
		{
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_BatteryHVUndervoltage_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_SWFault(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OFM_debounce_t Debounce_var = {FALSE,0U};

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;
	OFM_FaultConf_t Configuration;
	boolean OBCFault_Feedback;
	boolean OBCFault;
	OBC_ChargingMode OBC_ChargingMode_CANSigna;
	OBC_Status OBC_Status_CANSignal;


	Configuration.Level = FAULT_LEVEL_2;
	Configuration.ConfirmTime = 100U;
	Configuration.ReStartTime = OFM_OBCFaultTimeToRetryDefault;



	/*Read RTE*/
	(void)Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(&OBCFault_Feedback);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(&OBCFault);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSigna);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(OFM_FAULT_NUMBER>ArrayPosition)
	{

		if(Cx1_slow_charging!=OBC_ChargingMode_CANSigna)
		{
			/*Don't report the fault*/
			Input = FALSE;
		}
		else if((Cx3_conversion_working_!=OBC_Status_CANSignal)&&(Cx5_degradation_mode!=OBC_Status_CANSignal))
		{
			/*Don't report the fault*/
			Input = FALSE;
		}
		else if((OBCFault_Feedback==TRUE)&&(FALSE==OBCFault))
		{
			/*Fault detected*/
			Input = TRUE;
		}
		else
		{
			/*No fault detected*/
			Input = FALSE;
		}

		OFM_UpdateFaultState(Configuration,Input,&Debounce_var,ArrayPosition);

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_FaultSatellites_Error())=OFM_FaultVar[ArrayPosition];
	}
}
static void OFM_Function_ACnoDetected(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean FirstTime = TRUE;

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Input;

	(void)Rte_Read_PpDiagnosticNoACInput_DeDiagnosticNoACInput(&Input);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(OFM_FAULT_NUMBER>ArrayPosition)
	{
		if(FALSE!=FirstTime)
		{
			/*Ignore first value*/
			OFM_FaultVar[ArrayPosition] = FAULT_LEVEL_NO_FAULT;
			FirstTime = FALSE;
		}
		else if(FALSE==Input)
		{
			OFM_FaultVar[ArrayPosition] = FAULT_LEVEL_NO_FAULT;
		}
		else
		{
			OFM_FaultVar[ArrayPosition] = FAULT_LEVEL_1;
		}

		/*Write into RTE*/
		(*Rte_Pim_PimOBC_DiagnosticNoACInput_Error())=OFM_FaultVar[ArrayPosition];
	}

}
static void OFM_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	 OFM_DebounceOvercurrentDCHV = (uint16)Rte_Prm_PpDebounceOvercurrentDCHV_DeDebounceOvercurrentDCHV();
	 OFM_OBCFaultTimeToRetryOvertemp = (uint32)OFM_GAIN_10 * (uint32)Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp();
	 OFM_OBCFaultTimeToRetryDefault = (uint32)OFM_GAIN_10 * (uint32)Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault();
	 OFM_OBCFaultTimeToRetryVoltageError = (uint32)OFM_GAIN_10 * (uint32)Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError();

}

static void OFM_DTCTriggerReport(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	boolean faultData;

    boolean NTC1_M1_fault;
    boolean NTC1_M3_fault;
    boolean NTC1_M4_fault;
    boolean NTC_MOD5_fault;
    boolean NTC_MOD6_fault;

	/*PRQA S 4558, 4404, 3415 ++ # Native bool data type not available in this platform. Logical operation use base type (uint8) which is compatible with the definition of the operation by GNU-C */

    faultData = OFM_GetFaultPresent(2U);
    faultData = faultData || OFM_GetFaultPresent(3U);
    faultData = faultData || OFM_GetFaultPresent(4U);
    faultData = faultData || OFM_GetFaultPresent(5U);

	(void)Rte_Write_PpOBCFaultsList_DeInlet_OvertempACSensorFault(faultData);	/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	faultData = OFM_GetFaultPresent(0U);
	faultData = faultData || OFM_GetFaultPresent(1U);

	(void)Rte_Write_PpOBCFaultsList_DeInlet_OvertempDCSensorFault(faultData);	/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	faultData = OFM_GetFaultPresent(13U);

	(void)Rte_Write_PpOBCFaultsList_DeOBC_OvercurrentOutputFault(faultData);	/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	faultData = OFM_GetFaultPresent(9U);

    (void)Rte_Write_PpOBCFaultsList_DeOBC_OvervoltageOutputFault(faultData);		/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


    faultData = OFM_GetFaultPresent(15U);
    faultData = faultData || OFM_GetFaultPresent(16U);

	/*PRQA S 4558, 4404, 3415 --*/

    (void)Rte_Write_PpOBCFaultsList_DeOBC_HWErrors_Fault(faultData);			/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


    (void)Rte_Write_PpOBCFaultsList_DeOBC_InternalComFault(OFM_InternalComFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/



    (void)Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(&NTC1_M1_fault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(&NTC1_M3_fault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(&NTC1_M4_fault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(&NTC_MOD5_fault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(&NTC_MOD6_fault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

    /* Function to reduce CC in UT */
    faultData = OFM_GetOvertempFault(NTC1_M1_fault, NTC1_M3_fault, NTC1_M4_fault, NTC_MOD5_fault, NTC_MOD6_fault);

    (void)Rte_Write_PpOBCFaultsList_DeOBC_OvertemperatureFault(faultData);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static boolean OFM_GetOvertempFault(boolean NTC1_M1_fault, boolean NTC1_M3_fault, boolean NTC1_M4_fault, boolean NTC_MOD5_fault, boolean NTC_MOD6_fault)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean faultData;

    if ((FALSE != NTC1_M1_fault) || (FALSE != NTC1_M3_fault) || (FALSE != NTC1_M4_fault)
    		|| (FALSE != NTC_MOD5_fault) || (FALSE != NTC_MOD6_fault))
    {
    	faultData = TRUE;
    }
    else
    {
    	faultData = FALSE;
    }

    return faultData;
}

static boolean OFM_GetFaultPresent(uint8 ArrayPosition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ret_value;


	if (OFM_FaultVar_individual[ArrayPosition] != FALSE)
	{
		ret_value = TRUE;
	}
	else
	{
		ret_value = FALSE;
	}

	return ret_value;
}

static void OFM_CleanSanpShot(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApOFM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApOFM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApOFM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApOFM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApOFM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;

	OFM_RetryCounter = 0U;

	for(index=0U;index<(uint8)OFM_FAULT_NUMBER;index++)
	{
		OFM_SnapShot[index] = FAULT_LEVEL_NO_FAULT;
	}
}

static void OFM_Set_HW_fault(uint8 position, boolean Value)
{
	OBC_Status Status;
	uint8 position_temp;
	uint8 position_disp_3;

	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&Status);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	position_disp_3 = position >> 3U;

	if((Cx0_off_mode!=Status)
			&& (32U>position))
	{
		OFM_UDS_HWFaultData[position_disp_3] |= (Value<<(position%8U));
	}
	else
	{
		OFM_UDS_HWFaultData[position_disp_3] = 0U;
	}

	if (0U == (position/11U))
	{
		OFM_DCHV_HWFault[position_disp_3] |= Value<<(position%8U);
	}
	else
	{
		position_temp = position - 11U;
		position_disp_3 = position_temp>>3U;
		OFM_PFC_HWFault[position_disp_3] |= Value<<(position_temp%8U);
	}
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
