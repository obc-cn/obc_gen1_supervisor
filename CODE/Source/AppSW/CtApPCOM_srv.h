/*! 
 * \addtogroup PCOM
 * \{
 * \file  CtApPCOM.h
 * \brief  Public interface of PCOM services.
 */

#ifndef PCOM_SRV_H
#define PCOM_SRV_H

/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "Platform_Types.h"
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* PUBLIC TYPES                                                              */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* GLOBAL CONSTANTS                                                          */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* GLOBAL VARIABLES                                                          */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* GLOBAL FUNCTIONS                                                          */
/*---------------------------------------------------------------------------*/

/**
 * \brief Frame_TX_Update_Chk_RC service
 * \param CAN_channel Can channel. 0:Internal, 1:External.
 * \param frameID Can frame ID 
 * \param  *frame_data Fill in checksum and RC fields
 * \retval TRUE The frame has been processed properly.
 *
 */
boolean PCOM_Frame_TX_Update_Chk_RCService(PduIdType frameID,uint8  *frame_data);

/**
 * \brief Frame_RX_Indication service
 * \param CAN_channel Can channel. 0:Internal, 1:External.
 * \param frameID Can frame ID 
 * \param  *frame_data Whole frame payload
 *
 */
void PCOM_Frame_RX_IndicationService(volatile PduIdType frameID, const volatile uint8  *frame_data);


void PCOM_DC2_TimeoutNotification(void);

void PCOM_DC2_SendNotification(void);

void PCOM_BusOffBeginNotification(uint8 NetworkHandle, uint8 * OnlineDelayCyclesPtr);
void PCOM_BusOffEndNotification(uint8 NetworkHandle);


#endif /* PCOM_SRV_H */

/*!
 * \}
 */
