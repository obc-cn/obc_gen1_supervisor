/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApJDD.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApJDD
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApJDD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_UdsStatusByteType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApJDD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0857, 0380 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "CtApJDD_cfg.h"
#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
#define JDD_CPTTEMPOREL_INVALID_1 		((VCU_CptTemporel) 0xFFFFFFFEU) 		/*!< CPTTemporel invalid value 1 */
#define JDD_CPTTEMPOREL_INVALID_2 		((VCU_CptTemporel) 0xFFFFFFFFU) 		/*!< CPTTemporel invalid value 2 */

#define JDD_COMPUTERRAZGCT_INVALID_1 		((VCU_CompteurRazGct) 0xFF) 		/*!< COMPUTERRAZGCT invalid value 1 */
#define JDD_COMPUTERRAZGCT_INVALID_2 		((VCU_CompteurRazGct) 0xFE) 		/*!< COMPUTERRAZGCT invalid value 2 */

#define JDD_COMPUTERRAZGCT_DATEVALID_OFFSET	(24U) 					/*!< COMPUTERRAZGCT MSB offset */
#define JDD_COMPUTERRAZGCT_DATEVALID_MASK	(0xFF000000U) 				/*!< COMPUTERRAZGCT MSB offset */

#define JDD_LOCALCOUNTER_INCREMENTVALUE		(1U) 					/*!< Local counter increment value */
#define JDD_LOCALCOUNTER_TICK_MS		(10U) 					/*!< Increment value each 10 task tiks */
#define JDD_LOCALCOUNTER_PARTIAL_MAX		(100U) 					/*!< JDD_LOCALCOUNTER_INCREMENTVALUE period */

#define JDD_LOCALCOUNTER_MAX			(65535U) 				/*!< MAX JDD_LOCALCOUNTER */

#define JDD_LOCAL_COUNTER_MAX_NOT_RAISED	(JDD_LocalCounter < JDD_LOCALCOUNTER_MAX) /*!< Reset local Counter count */

#define JDD_TIMINGREFERENCE_ERROR		(0xFFFFFFFFU) 				/*! TimingReference error value */


#define JDD_LOCAL_COUNTER_RESET()		(JDD_LocalCounter = 0U)			/*!< Reset local Counter count */
#define JDD_LOCAL_COUNTER_PARTIAL_RESET()	(JDD_LocalCounterPartial = 0U) 		/*!< Reset local Counter count */

#define JDD_SEND_ACK_TIMEOUT_MS			(100U) 					/*!< ACK Timeout of 100 ms */
#define JDD_SEND_WAIT_MS			(100U) 					/*!< ACK Timeout of 100 ms */
#define JDD_SEND_ACK_RETRIES			(3U) 					/*!< ACK Timeout of 100 ms */
#define JDD_SEND_EXCLUSSION_TIME_MS		(1000U) 				/*!< ACK Exclussion Timeout of 1s */
#define JDD_SEND_EXCLUSSION_RETRIES		(3U) 					/*!< ACK Exclussion Retries*/

#define JDD_COMM_NOMBRE_TRAMES			((uint16) 0x02U) 		/* According requirement OBCP-27263 */
#define JDD_COMM_ETAT_FAILED			((uint8) 0x01U) 			/* FAILING According requirement OBCP-27263 */
#define JDD_COMM_ETAT_PASSED			((uint8) 0x00U) 			/* FAILING According requirement OBCP-27263 */
#define JDD_COMM_TYPE_MESSAGE			((uint16) 0x01U) 			/* FAILING According requirement OBCP-27263 */
#define JDD_COMM_FRAME_1_ID			((uint16) 0x01U) 			/* FRAME 1 ID*/
#define JDD_COMM_FRAME_2_ID			((uint16) 0x02U) 			/* FRAME 2 ID */

#define JDD_COMM_KILOMETRAGE_HIGHER_BIT		((VCU_Kilometrage) 0x800000) 		/* FRAME 2 ID */
#define JDD_COMM_KILOMETRAGE_MASK		((VCU_Kilometrage) 0xFFFFFF) 		/* FRAME 2 ID */

#define JDD_ACK_RESET_TIMEOUT()			(JDD_comm_status.timeout =0U)		/* Resets timeout counter */
#define JDD_ACK_TIMEOUT_TICK()			(JDD_comm_status.timeout +=JDD_LOCALCOUNTER_TICK_MS)		/* Resets timeout counter */
#define JDD_ACK_ECUNUMBER			((DATA_ACQ_JDD_BSI_2) 0xA6U) 		/*!< OBC ECU number */

#define JDD_QUEUE_EMPTY				(0xFFU) 				/*!< Value indicating EMPTY queue */

#define JDD_CANFRAME_SIZE			(8U)					/*!< JDD CAN Frame size */

#define JDD_EXTRACT_BYTE(src, byteNum)		((uint8)( (src) >> ( (byteNum) * 8U)))		/*!<  Extract desired byte */ /* PRQA S 3453 #Did it as a macro for better understanding*/

/**
 * \defgroup JDD_nvm
 * \{
 */

#define JDD_NVM_EVENTS_VERSION			(1U)	/*!< NVM Version */
#define JDD_NVM_EVENTS_VERSION_SIZE		(1U)	/*!< NVM Version Size */
#define JDD_NVM_EVENTS_VIRGINFLAG		(0x55U)	/*!< NVM Version */
#define JDD_NVM_EVENTS_VIRGINFLAG_SIZE		(1U)	/*!< NVM Version */
#define JDD_NVM_MEM_BYTELEN			(2000U) /* sizeof(IdtArrayJDDNvMRamMirror) */

#define JDD_NVM_SIGNAL_SIZE			(22U)

#if((JDD_NUMBER_OF_DTCS*JDD_NVM_SIGNAL_SIZE)>JDD_NVM_MEM_BYTELEN - JDD_NVM_EVENTS_VIRGINFLAG_SIZE)
#error "JDD: Not enougth memory available to store all DTCs"
#endif

/* PRQA S 0857, 0380 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/
/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
typedef enum
{
  JDD_NVM_STATE_FAIL,
  JDD_NVM_STATE_PASS,
  JDD_NVM_STATE_ALL
}jdd_nvm_state_t;

/** \} end JDD_nvm */

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApJDD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static uint8 jdd_dtc_queue[JDD_NUMBER_OF_DTCS];
static jdd_dtc_id JDD_queue_endCursor;
static jdd_dtc_id JDD_queue_startCursor;

static JDD_comm_status_t JDD_comm_status;

#define CtApJDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApJDD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static boolean JDD_VCU552_pending = FALSE;
static boolean JDD_VCU552_received = FALSE;
static boolean JDD_ACK_Received = FALSE;
static uint16 JDD_LocalCounter = 0U;
static VCU_CptTemporel JDD_DateValid = (VCU_CptTemporel)0;
static jdd_timing_reference_t JDD_TimingReference = 0U;
static uint8  JDD_LocalCounterPartial = 0U;

#define CtApJDD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApJDD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
static boolean JDD_pendingToSend(const JDD_dtc_config_t ** dataPending,jdd_dtc_state_t *failureSatus);
static void JDD_clearFirstQueue(boolean clearRelatedDTC);
static void JDD_clearEvent(const JDD_dtc_config_t 	*dtcConfig);
static void JDD_clearEventState(const JDD_dtc_config_t *dtcConfig,jdd_dtc_state_t failureStatus);
static void JDD_communicate_ComposeFrame1(const JDD_dtc_config_t *dtcConfig,jdd_dtc_state_t failureStatus);
static void JDD_communicate_ComposeFrame2(const JDD_dtc_config_t *dtcConfig,jdd_dtc_state_t failureStatus);
static void JDD_communicate_SetFrame(const uint8 src[JDD_CANFRAME_SIZE]);
static void JDD_communicate(void);
static void JDD_updateEvents(void);
static uint32 JDD_getSituation(void);
static void JDD_updateEvents_confirmation(const JDD_dtc_config_t * dtcConfig);
static void JDD_updateEvents_healing(const JDD_dtc_config_t * dtcConfig);
static void JDD_updateEvents_enqueue(uint8 dtcConfigIndex);
static void JDD_manageTiming(void);
static void JDD_updateDateValid(void);
static void JDD_updateFirstFrameEventsTimes(boolean localCounterError, uint16 localCounterValue);
static void JDD_frameCheck(boolean *validFrameReceived);
static void JDD_localCounterTick(boolean *temporelLocal_overflow);

static void JDD_Initialize_NVM(void);
static boolean JDD_DeSerialize_NVM(const JDD_dtc_config_t * dtcConfig);
static void JDD_Serialize_NVM_Event(const JDD_dtc_config_t * dtcConfig,jdd_nvm_state_t stateSerialize);

/* Implements OBCP-27248 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DATA_ACQ_JDD_BSI_2: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_0: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_1: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_2: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_3: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_4: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_5: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_6: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_7: Integer in interval [0...255]
 * Rte_DT_IdtArrayJDDNvMRamMirror_0: Integer in interval [0...255]
 * VCU_CDEAccJDD: Boolean
 * VCU_CompteurRazGct: Integer in interval [0...253]
 * VCU_CptTemporel: Integer in interval [0...4294967295]
 * VCU_Kilometrage: Integer in interval [0...16777214]
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_UDS_STATUS_TF (1U)
 *   DEM_UDS_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_UDS_STATUS_TF_BflPn 0
 *   DEM_UDS_STATUS_TF_BflLn 1
 *   DEM_UDS_STATUS_TFTOC (2U)
 *   DEM_UDS_STATUS_TFTOC_BflMask 2U (0b00000010)
 *   DEM_UDS_STATUS_TFTOC_BflPn 1
 *   DEM_UDS_STATUS_TFTOC_BflLn 1
 *   DEM_UDS_STATUS_PDTC (4U)
 *   DEM_UDS_STATUS_PDTC_BflMask 4U (0b00000100)
 *   DEM_UDS_STATUS_PDTC_BflPn 2
 *   DEM_UDS_STATUS_PDTC_BflLn 1
 *   DEM_UDS_STATUS_CDTC (8U)
 *   DEM_UDS_STATUS_CDTC_BflMask 8U (0b00001000)
 *   DEM_UDS_STATUS_CDTC_BflPn 3
 *   DEM_UDS_STATUS_CDTC_BflLn 1
 *   DEM_UDS_STATUS_TNCSLC (16U)
 *   DEM_UDS_STATUS_TNCSLC_BflMask 16U (0b00010000)
 *   DEM_UDS_STATUS_TNCSLC_BflPn 4
 *   DEM_UDS_STATUS_TNCSLC_BflLn 1
 *   DEM_UDS_STATUS_TFSLC (32U)
 *   DEM_UDS_STATUS_TFSLC_BflMask 32U (0b00100000)
 *   DEM_UDS_STATUS_TFSLC_BflPn 5
 *   DEM_UDS_STATUS_TFSLC_BflLn 1
 *   DEM_UDS_STATUS_TNCTOC (64U)
 *   DEM_UDS_STATUS_TNCTOC_BflMask 64U (0b01000000)
 *   DEM_UDS_STATUS_TNCTOC_BflPn 6
 *   DEM_UDS_STATUS_TNCTOC_BflLn 1
 *   DEM_UDS_STATUS_WIR (128U)
 *   DEM_UDS_STATUS_WIR_BflMask 128U (0b10000000)
 *   DEM_UDS_STATUS_WIR_BflPn 7
 *   DEM_UDS_STATUS_WIR_BflLn 1
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * VCU_CDEApcJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_APC_unchanged (0U)
 *   Cx1_APC_unchanged (1U)
 *   Cx2_APC_OFF (2U)
 *   Cx3_APC_ON_ (3U)
 * VCU_EtatPrincipSev: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Stop (0U)
 *   Cx1_Contact (1U)
 *   Cx2_Start (2U)
 *   Cx3_Reserved (3U)
 * VCU_EtatReseauElec: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_TI_NOMINAL (0U)
 *   Cx1_TI_DEGRADE (1U)
 *   Cx2_TI_DISPO_DEM (2U)
 *   Cx3_DEMARRAGE (3U)
 *   Cx4_REDEMARRAGE (4U)
 *   Cx5_TA_NOMINAL (5U)
 *   Cx6_TA_DEGRADE (6U)
 *   Cx7_TA_SECU (7U)
 *   Cx8_Reserved (8U)
 *   Cx9_Reserved (9U)
 *   CxA_Reserved (10U)
 *   CxB_Reserved (11U)
 *   CxC_Reserved (12U)
 *   CxD_Reserved (13U)
 *   CxE_Reserved (14U)
 *   CxF_Reserved (15U)
 * VCU_PosShuntJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Park (0U)
 *   Cx1_Client (1U)
 *   Cx2_Absent (2U)
 *   Cx3_Undetermined (3U)
 *
 * Array Types:
 * ============
 * IdtArrayJDDNvMRamMirror: Array with 2000 element(s) of type Rte_DT_IdtArrayJDDNvMRamMirror_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   Rte_DT_IdtArrayJDDNvMRamMirror_0 *Rte_Pim_PimJDD_NvMRamMirror(void)
 *     Returnvalue: Rte_DT_IdtArrayJDDNvMRamMirror_0* is of type IdtArrayJDDNvMRamMirror
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   boolean Rte_CData_CalEnableJDD(void)
 *
 *********************************************************************************************************************/


#define CtApJDD_START_SEC_CODE
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpJDD_FrameNewJDD55FRcvd> of PortPrototype <PpJDD_FrameNewJDD55FRcvd>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd
 *********************************************************************************************************************/
  DATA_ACQ_JDD_BSI_2 recVal;
  (void) Rte_Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(&recVal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  if(recVal == JDD_ACK_ECUNUMBER)
    {
      JDD_ACK_Received = TRUE;
    }

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpJDD_FrameVCU552Rcvd> of PortPrototype <PpJDD_FrameVCU552Rcvd>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd
 *********************************************************************************************************************/
  JDD_VCU552_pending = TRUE;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApJDD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) RCtApJDD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_init
 *********************************************************************************************************************/

  /* - Static non-init variables declaration ---------- */
#define CtApJDD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static non-init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Static zero-init variables declaration --------- */
#define CtApJDD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static zero-init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Static init variables declaration -------------- */
#define CtApJDD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Automatic variables declaration ---------------- */
  jdd_dtc_id indexDTC;

  for(indexDTC = 0U; indexDTC< JDD_NUMBER_OF_DTCS; indexDTC++)
    {
      jdd_dtc_queue[indexDTC] = JDD_QUEUE_EMPTY;
      jdd_dtc_configs[indexDTC].data->enqued = FALSE;
      jdd_dtc_configs[indexDTC].data->confirmation.state = JDD_STATE_NO_RAISED;
      jdd_dtc_configs[indexDTC].data->healing.state = JDD_STATE_NO_RAISED;
      jdd_dtc_configs[indexDTC].data->globalState = JDD_DTC_STATE_IDLE;
    }
  JDD_queue_endCursor = 0U;
  JDD_queue_startCursor = 0U;
  /* Implements OBCP-273669 */
  JDD_Initialize_NVM();

  JDD_comm_status.state = JDD_COMM_IDLE;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApJDD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(boolean *data)
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBusOffCANFault_DeBusOffCANFault(boolean *data)
 *   Std_ReturnType Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(boolean *data)
 *   Std_ReturnType Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *   Std_ReturnType Rte_Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data)
 *   Std_ReturnType Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data)
 *   Std_ReturnType Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data)
 *   Std_ReturnType Rte_Write_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056216_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056216_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056317_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x056317_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a0804_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a0804_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a084b_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a084b_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a9464_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0a9464_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0af864_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0af864_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0cf464_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x0cf464_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x108093_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x108093_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c413_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c413_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c512_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c512_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c613_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c613_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c713_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x10c713_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120a12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120b12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c64_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c98_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120c98_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d64_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d98_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x120d98_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d711_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d711_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d712_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d712_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d713_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d713_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d811_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d811_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d812_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d812_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d813_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d813_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d911_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d911_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d912_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d912_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d913_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12d913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da13_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12da13_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12db12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12db12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dc11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dc11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dd12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12dd12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12de11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12de11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12df13_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12df13_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e012_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e012_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e111_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e111_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e213_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e213_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e319_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e319_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e712_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e712_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e811_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e811_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e912_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12e912_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12ea11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12ea11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f316_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f316_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f917_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x12f917_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x13e919_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x13e919_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x166c64_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x166c64_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179e12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f11_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f11_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f12_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x179f12_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a0064_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a0064_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7104_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7104_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a714b_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a714b_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7172_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0x1a7172_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd18787_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd18787_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd1a087_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd1a087_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd20781_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd20781_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd2a081_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd2a081_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38782_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38782_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38783_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xd38783_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00081_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00081_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00087_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00087_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00214_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00214_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00362_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xe00362_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApJDD_CODE) RCtApJDD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApJDD_task10ms
 *********************************************************************************************************************/

  /* - Static non-init variables declaration ---------- */
#define CtApJDD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static non-init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Static zero-init variables declaration --------- */
#define CtApJDD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static zero-init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Static init variables declaration -------------- */
#define CtApJDD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Automatic variables declaration ---------------- */
  boolean JDD_enabled;
  IdtAppRCDECUState ecuSTATE;
  boolean BusOFF;
  boolean BusMUTE;

  (void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&ecuSTATE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Read_PpBusOffCANFault_DeBusOffCANFault(&BusOFF);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Read_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(&BusMUTE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/



  JDD_enabled = Rte_CData_CalEnableJDD();

  if(FALSE != JDD_enabled)
    {
      /* Implements OBCP-27352 */
      JDD_manageTiming();
      JDD_updateEvents();

      if(((ecuSTATE > 0U) && (ecuSTATE <6U)) && (FALSE != JDD_VCU552_received) && (FALSE == BusOFF) && (FALSE == BusMUTE))
	{
	  /* Implements OBCP-27260 */
	  JDD_communicate();
	}
    }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApJDD_STOP_SEC_CODE
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**
 * Gets oldest event in queue and returns it to \p dataPending and its status to \failureStatus
 * \param[out] dataPending Pointer to structure with pending event
 * \param[out] failureSatus Failure status of event \p dataPending
 * \return If there is data to send
 */
static boolean JDD_pendingToSend(const JDD_dtc_config_t **  dataPending,jdd_dtc_state_t *failureSatus)
{
  boolean retVal = FALSE;
  const JDD_dtc_config_t *configData;

  if ((NULL_PTR != dataPending) && (NULL_PTR != failureSatus))
  {
	  if((jdd_dtc_queue[JDD_queue_startCursor] != JDD_QUEUE_EMPTY) && (JDD_queue_endCursor != JDD_queue_startCursor))
		{
		  configData = &jdd_dtc_configs[jdd_dtc_queue[JDD_queue_startCursor]];
		  if((configData->data->confirmation.state == JDD_STATE_RAISED) && (configData->data->globalState == JDD_DTC_STATE_FAILING))
		{
		  *dataPending = configData;
		  *failureSatus = JDD_DTC_STATE_FAILING;
		  retVal = TRUE;
		}
		  else if((configData->data->healing.state == JDD_STATE_RAISED) && (configData->data->globalState == JDD_DTC_STATE_PASSING))
		{
		  *dataPending = configData;
		  *failureSatus = JDD_DTC_STATE_PASSING;
		  retVal = TRUE;
		}
		  else
		{
		  /* Wrong state */
		  JDD_clearFirstQueue(TRUE);
		  *dataPending = NULL_PTR;
		}
		}
  }
  return retVal;
}
/**
 * Removes an event from queue and clear content
 * \param[in] clearRelatedDTC clear related DTC
 */
static void JDD_clearFirstQueue(boolean clearRelatedDTC)
{
  if(jdd_dtc_queue[JDD_queue_startCursor] != JDD_QUEUE_EMPTY)
    {
      if(FALSE != clearRelatedDTC)
	{
	  JDD_clearEvent(&jdd_dtc_configs[jdd_dtc_queue[JDD_queue_startCursor]]);
	}
      jdd_dtc_configs[jdd_dtc_queue[JDD_queue_startCursor]].data->enqued = FALSE;
      jdd_dtc_queue[JDD_queue_startCursor] = JDD_QUEUE_EMPTY;
    }
  JDD_queue_startCursor +=1U;
  if(JDD_queue_startCursor == JDD_NUMBER_OF_DTCS)
    {
      JDD_queue_startCursor = 0U;
    }
}

/**
 * Clears all event information
 * \param jdd_dtc_queue
 */
static void JDD_clearEvent(const JDD_dtc_config_t 	*dtcConfig)
{
  dtcConfig->data->confirmation.state = JDD_STATE_NO_RAISED;
  dtcConfig->data->healing.state = JDD_STATE_NO_RAISED;
  JDD_Serialize_NVM_Event(dtcConfig, JDD_NVM_STATE_ALL);
}

static void JDD_clearEventState(const JDD_dtc_config_t *dtcConfig,jdd_dtc_state_t failureStatus)
{

  if(failureStatus == JDD_DTC_STATE_FAILING)
    {
      dtcConfig->data->confirmation.state = JDD_STATE_NO_RAISED;
      JDD_Serialize_NVM_Event(dtcConfig,JDD_NVM_STATE_FAIL);
    }
  else
    {
      dtcConfig->data->healing.state = JDD_STATE_NO_RAISED;
      JDD_Serialize_NVM_Event(dtcConfig,JDD_NVM_STATE_PASS);
    }

}


static void JDD_communicate(void)
{
  boolean dataPending;
  boolean IntegrationModeRequested;
  (void) Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(&IntegrationModeRequested);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  /* TODO: Importante aclar requisito */
/* #warning "Clarify requirement"*//* Still here only as a reminder */
  switch(JDD_comm_status.state)
  {
    case JDD_COMM_IDLE:
      {
	/* Implements OBCP-27339 */
	if (FALSE == IntegrationModeRequested)
	  {
	    dataPending = JDD_pendingToSend(&(JDD_comm_status.sending),&(JDD_comm_status.failureSatus));
	    if(FALSE != dataPending)
	      {
		JDD_comm_status.state = JDD_COMM_SEND_FRAME1;
		JDD_comm_status.retries = 0U;
		JDD_comm_status.exclusionRetries = 0U;
	      }
	  }
	break;
      }
    case JDD_COMM_SEND_FRAME1:
      {
	if(JDD_comm_status.sending != NULL_PTR)
	  {
	    JDD_communicate_ComposeFrame1(JDD_comm_status.sending,JDD_comm_status.failureSatus);
	    JDD_ACK_RESET_TIMEOUT();
	    /* This call is needed to compensate the lost cycle in the state change */
	    /* If not, the total timeout would be 110 ms instead of 100 ms */
	    JDD_ACK_TIMEOUT_TICK();
	    JDD_comm_status.state = JDD_COMM_WAIT;
	    (void) Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	  }else{
	      JDD_comm_status.state = JDD_COM_SEND_FAILED;
	  }
	break;
      }
    case JDD_COMM_WAIT:
      {
	JDD_ACK_TIMEOUT_TICK();
	if(JDD_comm_status.timeout == JDD_SEND_WAIT_MS)
	  {
	    /* Wait 100ms*/
	    JDD_comm_status.state = JDD_COMM_SEND_FRAME2;
	  }
	break;
      }
    case JDD_COMM_SEND_FRAME2:
      {
	if(JDD_comm_status.sending != NULL_PTR)
	  {
	    JDD_communicate_ComposeFrame2(JDD_comm_status.sending,JDD_comm_status.failureSatus);
	    JDD_ACK_RESET_TIMEOUT();
	    JDD_ACK_TIMEOUT_TICK();
	    JDD_comm_status.state = JDD_COMM_WAIT_ACK;
	    JDD_ACK_Received  = FALSE;
	    (void) Rte_Call_PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	  }else{
	      JDD_comm_status.state = JDD_COM_SEND_FAILED;
	  }
	break;
      }
    case JDD_COMM_WAIT_ACK:
      {
	if(JDD_ACK_Received == FALSE)
	  {
	    JDD_ACK_TIMEOUT_TICK();
	    /* Implements OBCP-27257 */
	    if(JDD_comm_status.timeout >= JDD_SEND_ACK_TIMEOUT_MS)
	      {
		/* Timeout Raised */
		JDD_comm_status.retries += 1U;
		if(JDD_comm_status.retries < JDD_SEND_ACK_RETRIES)
		  {
		    JDD_comm_status.state = JDD_COMM_SEND_FRAME1;

		  }
		else
		  {
		    JDD_ACK_RESET_TIMEOUT();
		    JDD_ACK_TIMEOUT_TICK();
		    JDD_comm_status.state = JDD_COMM_WAIT_EXCLUSION_TIME;
		  }
	      }
	  }
	else
	  {
	    /* Implements OBCP-27258 */
	    JDD_comm_status.state = JDD_COM_SEND_COMPLETED;
	    JDD_ACK_RESET_TIMEOUT();
	    JDD_ACK_TIMEOUT_TICK();
	    JDD_ACK_Received = FALSE;
	  }
	break;
      }
    case JDD_COMM_WAIT_EXCLUSION_TIME:
      {
	/* Implements OBCP-27258 */
	if(JDD_comm_status.timeout >= JDD_SEND_EXCLUSSION_TIME_MS)
	  {
	    /* Timeout Raised */
	    JDD_comm_status.exclusionRetries += 1U;
	    if(JDD_comm_status.exclusionRetries < JDD_SEND_EXCLUSSION_RETRIES)
	      {
		JDD_comm_status.state = JDD_COMM_SEND_FRAME1;
		JDD_comm_status.retries = 0U;

	      }
	    else
	      {
		/* If maximum exclusion retries, discard DTC and continue */
		JDD_comm_status.state = JDD_COM_DISCARD_FROMQUEUE;
	      }
	  }
	else
	  {
	    JDD_comm_status.timeout += JDD_LOCALCOUNTER_TICK_MS;
	  }
	break;
      }
    case JDD_COM_SEND_COMPLETED:
      {
	JDD_ACK_TIMEOUT_TICK();
	if(JDD_comm_status.timeout >= JDD_SEND_WAIT_MS)
	  {
	    /* Wait 100ms*/
	    JDD_clearEventState(JDD_comm_status.sending,JDD_comm_status.failureSatus);
	    JDD_comm_status.state = JDD_COM_SEND_END;
	  }
	break;
      }
    case JDD_COM_SEND_FAILED:
      {
	JDD_comm_status.state = JDD_COM_SLEEP;
	break;
      }
    case JDD_COM_DISCARD_FROMQUEUE:
      {
	/* Clear from the queue but not the DTC */
	JDD_clearFirstQueue(FALSE);
	JDD_clearEventState(JDD_comm_status.sending,JDD_comm_status.failureSatus);
	JDD_comm_status.sending = NULL_PTR;
	JDD_ACK_RESET_TIMEOUT();
	JDD_ACK_TIMEOUT_TICK();
	JDD_comm_status.state = JDD_COM_FINAL_WAIT;
	break;
      }
    case JDD_COM_SEND_END:
      {
	if((JDD_comm_status.sending->data->confirmation.state == JDD_STATE_NO_RAISED) && (JDD_comm_status.sending->data->healing.state == JDD_STATE_NO_RAISED))
	  {
	    JDD_clearFirstQueue(TRUE);
	    JDD_comm_status.sending = NULL_PTR;
	  }
	JDD_ACK_RESET_TIMEOUT();
	JDD_ACK_TIMEOUT_TICK();
	JDD_comm_status.state = JDD_COM_FINAL_WAIT;
	break;
      }

    case JDD_COM_FINAL_WAIT:
      {
	JDD_ACK_TIMEOUT_TICK();
	/* Implements OBCP-27257 */
	if(JDD_comm_status.timeout >= JDD_SEND_ACK_TIMEOUT_MS)
	  {
	    JDD_comm_status.state = JDD_COMM_IDLE;
	  }
	break;
      }
    case JDD_COM_SLEEP:
    default:
      {
	/* DO NOTHING - SLEEP FOR EVER */
	break;
      }
  }

}

/**
 * Bulks JDD FRame from buffer to RTE data
 * \param[in] src Buffer with data to be transfered
 */
static void JDD_communicate_SetFrame(const uint8 src[JDD_CANFRAME_SIZE])
{

  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(src[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(src[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(src[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(src[3]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(src[4]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(src[5]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(src[6]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void) Rte_Write_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(src[7]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/* PRQA S 2985 ++ #operation redundance avoidance*/
/**
 * Fills signals for frame 1
 * \param dtcConfig[in]  DTC to be sent
 * \param failureStatus[in] status to be sent
 */
static void JDD_communicate_ComposeFrame1(const JDD_dtc_config_t *dtcConfig,jdd_dtc_state_t failureStatus)
{
  /* Implements OBCP-27264 */
  uint8 buffer[JDD_CANFRAME_SIZE];

  CODES_DEFAUT temp_CODES_DEFAULT;
  jdd_timing_reference_t temp_REFERENCE_HORARIE;
  uint8 temp_HEADER = 0U;
  uint8 temp_ETAT_DTC;

  /* Implements OBCP-27264 */
  temp_CODES_DEFAULT = dtcConfig->code;

  if(failureStatus == JDD_DTC_STATE_FAILING)
    {
      temp_REFERENCE_HORARIE = dtcConfig->data->confirmation.referenceHorarie;
      temp_ETAT_DTC = JDD_COMM_ETAT_FAILED;
    }
  else
    {
      temp_REFERENCE_HORARIE = dtcConfig->data->healing.referenceHorarie;
      temp_ETAT_DTC = JDD_COMM_ETAT_PASSED;
    }

  /* HEADER */
  temp_HEADER |= (uint8) ((JDD_COMM_NOMBRE_TRAMES << 6U) & 0xC0U);
  temp_HEADER |= (uint8) ((JDD_COMM_FRAME_1_ID << 4U) & 0x30U);
  temp_HEADER |= (uint8) ((temp_ETAT_DTC << 3U) & 0x08U);
  /* RESERVED */
  temp_HEADER |= (uint8) ((JDD_COMM_TYPE_MESSAGE) & 0x03U);
  buffer[0U] = temp_HEADER;
  /* END OF HEADER */

  buffer[1U] = JDD_EXTRACT_BYTE(temp_CODES_DEFAULT,2U);
  buffer[2U] = JDD_EXTRACT_BYTE(temp_CODES_DEFAULT,1U);
  buffer[3U] = JDD_EXTRACT_BYTE(temp_CODES_DEFAULT,0U);

  buffer[4U] = JDD_EXTRACT_BYTE(temp_REFERENCE_HORARIE,3U);
  buffer[5U] = JDD_EXTRACT_BYTE(temp_REFERENCE_HORARIE,2U);
  buffer[6U] = JDD_EXTRACT_BYTE(temp_REFERENCE_HORARIE,1U);
  buffer[7U] = JDD_EXTRACT_BYTE(temp_REFERENCE_HORARIE,0U);

  JDD_communicate_SetFrame(buffer);
}
/* PRQA S 2985 -- #operation redundance avoidance*/
/* PRQA S 2985 ++ #operation redundance avoidance*/
/**
 * Fills signals for frame 2
 * \param dtcConfig[in]  DTC to be sent
 * \param failureStatus[in] status to be sent
 */

static void JDD_communicate_ComposeFrame2(const JDD_dtc_config_t *dtcConfig,jdd_dtc_state_t failureStatus)
{
  uint8 buffer[JDD_CANFRAME_SIZE];
  VCU_Kilometrage temp_kilometrageJDD = 0U;
  uint8 temp_HEADER = 0U;
  uint8 temp_ETAT_DTC;
  uint32 temp_SITUATION_VIE;
  /* Implements OBCP-27264 */
  if(failureStatus == JDD_DTC_STATE_FAILING)
    {
      if(dtcConfig->data->confirmation.kilometrage != 0U)
	{
	  /* Implements OBCP-27342 */
	  temp_kilometrageJDD = ((dtcConfig->data->confirmation.kilometrage | JDD_COMM_KILOMETRAGE_HIGHER_BIT) & JDD_COMM_KILOMETRAGE_MASK) ;
	}

      temp_ETAT_DTC = JDD_COMM_ETAT_FAILED;
      temp_SITUATION_VIE = dtcConfig->data->confirmation.situation;
    }
  else
    {
      if(dtcConfig->data->healing.kilometrage != 0U)
	{
	  /* Implements OBCP-27342 */
	  temp_kilometrageJDD = ((dtcConfig->data->healing.kilometrage | JDD_COMM_KILOMETRAGE_HIGHER_BIT) & JDD_COMM_KILOMETRAGE_MASK);
	}
      temp_ETAT_DTC = JDD_COMM_ETAT_PASSED;
      temp_SITUATION_VIE = dtcConfig->data->healing.situation;
    }

  /* HEADER */
  temp_HEADER |= (uint8) ((JDD_COMM_NOMBRE_TRAMES << 6U) & 0xC0U);
  temp_HEADER |= (uint8) ((JDD_COMM_FRAME_2_ID << 4U) & 0x30U);
  temp_HEADER |= (uint8) ((temp_ETAT_DTC << 3U) & 0x08U);
  /* RESERVED */
  temp_HEADER |= (uint8) ((JDD_COMM_TYPE_MESSAGE) & 0x03U);
  buffer[0U] = temp_HEADER;
  /* END OF HEADER */

  buffer[1U] = JDD_EXTRACT_BYTE(temp_kilometrageJDD,2U);
  buffer[2U] = JDD_EXTRACT_BYTE(temp_kilometrageJDD,1U);
  buffer[3U] = JDD_EXTRACT_BYTE(temp_kilometrageJDD,0U);

  buffer[4U] = JDD_EXTRACT_BYTE(temp_SITUATION_VIE,2U);
  buffer[5U] = JDD_EXTRACT_BYTE(temp_SITUATION_VIE,1U);
  buffer[6U] = JDD_EXTRACT_BYTE(temp_SITUATION_VIE,0U);

  buffer[7U] = 0U;

  JDD_communicate_SetFrame(buffer);
}
/* PRQA S 2985 -- #operation redundance avoidance*/

/**
 * Update events state
 */
static void JDD_updateEvents(void)
{
  jdd_dtc_id indexDTC;
  boolean isFailing;
  /* For allowing faut injection and simulate data pointer NULL */
  JDD_dtc_data_t *data;

  for(indexDTC = 0U; indexDTC < JDD_NUMBER_OF_DTCS; indexDTC++)
    {
      /* For allowing faut injection and simulate data pointer NULL */
       data = jdd_dtc_configs[indexDTC].data;

		if(data != NULL_PTR)
		{
			if (0U == data->enqued)
			{
			  isFailing = jdd_dtc_configs[indexDTC].isFailing();

			  if((FALSE != isFailing) && (data->globalState != JDD_DTC_STATE_FAILING))
				{

				  /* If not data saved previously and not sent */
				  JDD_updateEvents_confirmation(&jdd_dtc_configs[indexDTC]);
				  JDD_updateEvents_enqueue(indexDTC);
				  data->globalState = JDD_DTC_STATE_FAILING;

				}
			  else if((FALSE == isFailing) && (data->globalState == JDD_DTC_STATE_FAILING))
				{

				  JDD_updateEvents_healing(&jdd_dtc_configs[indexDTC]);
				  JDD_updateEvents_enqueue(indexDTC);
				  data->globalState = JDD_DTC_STATE_PASSING;
				}
			  else
				{
				  /* DO NOTHING - MISRA COMPLIANCE */
				}
			}
		}
    }
}

/**
 * Composes  SITUATION_VIE_JDDD bitfield
 * \return  SITUATION_VIE_JDDD
 */
static uint32 JDD_getSituation(void)
{
  uint32 situation = 0U;
  VCU_EtatReseauElec electricalNetworkState;

  VCU_EtatPrincipSev mainStatus;
  VCU_PosShuntJDD shuntParkPosition;
  VCU_CDEAccJDD accJDD;
  VCU_CDEApcJDD apcJDD;
  IdtAppRCDECUState rcdPLC;
  boolean bsiWakeup;

  /* Implements OBCP-27352 */
  (void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&rcdPLC);/* PRQA S 3110, 3417, 2982, 3426 #RTE read/write confirmation not used*/
  situation |= ((uint32) (1U)) << 22U;

  /* Implements OBCP-27345 */
  (void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&rcdPLC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  situation |= (((uint32)rcdPLC & 0x07U)) << 19U;

  (void)Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(&bsiWakeup);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  if(FALSE != bsiWakeup)
    {
      /* Implements OBCP-27351 */
      situation |= ((uint32) (1U)) << 23U;
    }
  else
    {
      /* Implements OBCP-27346 */
      (void)Rte_Read_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(&apcJDD);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
      situation |= (((uint32)apcJDD & 0x03U)) << 17U;
      (void)Rte_Read_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(&accJDD);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
      situation |= (((uint32) accJDD & 0x01U)) << 16U;

      /* Implements OBCP-27346 */
      (void) Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(&shuntParkPosition);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
      situation |= (((uint32)shuntParkPosition & 0x03U)) << 14U;

      /* Implements OBCP-27348 */
      (void)Rte_Read_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(&mainStatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
      situation |= (((uint32)mainStatus & 0x03U)) << 12U;

      /* Implements OBCP-27349 */
      /* Set ICE = Zero by initialization of frame */

      /* Implements OBCP-27350 */
      (void)Rte_Read_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(&electricalNetworkState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
      situation |= (((uint32) electricalNetworkState & 0x0FU)) << 4U;
    }

  return situation;
}

/**
 * Update event \p dtcConfig when failure is found
 * \param[out] dtcConfig Event to be updated
 */
static void JDD_updateEvents_confirmation(const JDD_dtc_config_t * dtcConfig)
{

  dtcConfig->data->confirmation.state = JDD_STATE_RAISED;
  dtcConfig->data->confirmation.referenceHorarie = JDD_TimingReference;
  (void)Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(&dtcConfig->data->confirmation.kilometrage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  dtcConfig->data->confirmation.situation = JDD_getSituation();
  JDD_Serialize_NVM_Event(dtcConfig,JDD_NVM_STATE_FAIL);
}

/**
 * Update event \p dtcConfig when hailing is found
 * \param[out] dtcConfig Event to be updated
 */
static void JDD_updateEvents_healing(const JDD_dtc_config_t * dtcConfig)
{
  dtcConfig->data->healing.state = JDD_STATE_RAISED;
  dtcConfig->data->healing.referenceHorarie = JDD_TimingReference;
  (void) Rte_Read_PpInt_VCU_Kilometrage_VCU_Kilometrage(&dtcConfig->data->healing.kilometrage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  dtcConfig->data->healing.situation = JDD_getSituation();
  JDD_Serialize_NVM_Event(dtcConfig,JDD_NVM_STATE_PASS);
}

/**
 * Enqueue event if not yet done
 * \param[in,out] dtcConfig Event to be enqueued
 * \param[in] state new event state
 */

static void JDD_updateEvents_enqueue(uint8 dtcConfigIndex)
{
  const JDD_dtc_config_t *dtcConfig = &jdd_dtc_configs[dtcConfigIndex];
  if (dtcConfigIndex < JDD_NUMBER_OF_DTCS)
  {
	  if(dtcConfig->data->enqued == FALSE)
	  {
		  jdd_dtc_queue[JDD_queue_endCursor] = dtcConfigIndex;
		  JDD_queue_endCursor +=1U;
		  if(JDD_queue_endCursor == JDD_NUMBER_OF_DTCS)
		  {
			  JDD_queue_endCursor = 0U;
		  }
		  dtcConfig->data->enqued  = TRUE;
	  }
  }
}


/**
 * Manages and updates timestamps
 * \param[in] localCounterError Returns TRUE if JDD_LocalCounter > JDD_DateValid during first frame
 *
 */
static void JDD_manageTiming(void)
{
  boolean validFrameReceived;
  boolean localCounter_Overflow = FALSE;
  boolean referenceHorarieError = FALSE;
  uint16 prevJDD_LocalCounter;
  boolean isFirstFrame = FALSE;
  prevJDD_LocalCounter = JDD_LocalCounter;

  JDD_frameCheck(&validFrameReceived);
  if(FALSE != validFrameReceived)
    {

      /* Implements OBCP-27244 */
      JDD_LOCAL_COUNTER_RESET();
      JDD_LOCAL_COUNTER_PARTIAL_RESET();

      JDD_updateDateValid();

      if(FALSE == JDD_VCU552_received)
	{
	  isFirstFrame = TRUE;
	  JDD_VCU552_received = TRUE;
	}
    }
  else
    {
      /* If not received increase LocalCounter */
      JDD_localCounterTick(&localCounter_Overflow);
    }

  if(FALSE != localCounter_Overflow)
    {
      /* Implements OBCP-27250 */
      referenceHorarieError = TRUE;
      JDD_TimingReference = JDD_TIMINGREFERENCE_ERROR;
    }
  else
    {

      if(FALSE != JDD_VCU552_received)
	{
	  if(FALSE != isFirstFrame)
	    {
	      if(prevJDD_LocalCounter > JDD_DateValid)
		{
		  /* Implements OBCP-27250 */
		  referenceHorarieError = TRUE;
		}

	      JDD_updateFirstFrameEventsTimes(referenceHorarieError, prevJDD_LocalCounter);
	    }

	  /* Implements OBCP-27252 */
	  JDD_TimingReference = JDD_DateValid + JDD_LocalCounter;
	}
      else
	{
	  JDD_TimingReference = JDD_LocalCounter;
	}
    }

  (void) Rte_Write_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(referenceHorarieError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/**
 * Updates JDD_DateValid if valid data is received
 */
static void JDD_updateDateValid(void)
{
  VCU_CompteurRazGct VCU_CompteurRazGct_signal;
  VCU_CptTemporel VCU_CptTemporel_signal;

  (void)Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel(&VCU_CptTemporel_signal);/* PRQA S 3110, 3417, 0315, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Read_Pp_VCU_CompteurRazGct_VCU_CompteurRazGct(&VCU_CompteurRazGct_signal);/* PRQA S 3110, 3417, 0315, 3426 #RTE read/write confirmation not used*/

  JDD_DateValid = VCU_CptTemporel_signal;
  if((JDD_COMPUTERRAZGCT_INVALID_1 != VCU_CompteurRazGct_signal) && (JDD_COMPUTERRAZGCT_INVALID_2 != VCU_CompteurRazGct_signal))
    {
      /* Implements OBCP-27389 */
      JDD_DateValid |= ((((VCU_CptTemporel) VCU_CompteurRazGct_signal) << JDD_COMPUTERRAZGCT_DATEVALID_OFFSET) & JDD_COMPUTERRAZGCT_DATEVALID_MASK);/* PRQA S 2985 # Operation redundancy avoidance*/
    }
}

/**
 * Updates the triggered events timings just after the first frame is received.
 * \brief Uses as time reference JDD_TIMINGREFERENCE_ERROR if \p localCounterError is TRUE. If not, uses calculated JDD_TimingReference
 * \param[in] localCounterError If frame is in OBCP-27255 case 1
 * \param[in] localCounterValue Stored local counter value
 */
static void JDD_updateFirstFrameEventsTimes(boolean localCounterError, uint16 localCounterValue)
{
  jdd_dtc_id indexDTC;
  jdd_dtc_id cursorDTC = JDD_queue_startCursor;
  const JDD_dtc_config_t *configData;

  for(indexDTC = 0U; indexDTC< JDD_NUMBER_OF_DTCS; indexDTC++)
    {
      if((jdd_dtc_queue[cursorDTC] != JDD_QUEUE_EMPTY) && (cursorDTC != JDD_queue_endCursor))
		{
		  configData = &jdd_dtc_configs[jdd_dtc_queue[cursorDTC]];

		  if(configData->data->confirmation.state == JDD_STATE_RAISED)
			{
			  /* Implements OBCP-27255 */
			  if(FALSE != localCounterError)
			{
			  configData->data->confirmation.referenceHorarie = JDD_TIMINGREFERENCE_ERROR;
			}
			  else
			{
			  configData->data->confirmation.referenceHorarie = JDD_DateValid - localCounterValue + configData->data->confirmation.referenceHorarie;
			}
			  JDD_Serialize_NVM_Event(configData,JDD_NVM_STATE_FAIL);
			}

		  if(configData->data->healing.state == JDD_STATE_RAISED)
			{
			  /* Implements OBCP-27255 */
			  if(FALSE != localCounterError)
				{
				  configData->data->healing.referenceHorarie = JDD_TIMINGREFERENCE_ERROR;
				}
				  else
				{
				  configData->data->healing.referenceHorarie = JDD_DateValid - localCounterValue + configData->data->healing.referenceHorarie;
				}
			  JDD_Serialize_NVM_Event(configData,JDD_NVM_STATE_PASS);
			}

		  cursorDTC +=1U;
		  if(cursorDTC == JDD_NUMBER_OF_DTCS)
			{
			  cursorDTC = 0;
			}
		}
		  else
		{
		  /* End of queued events */
		  break;
		}
    }
}

/**
 * Verify JDD RX frame check
 * \param[out] validFrameReceived Returns true is a valid frame is found
 * \return Local counter raised its maximum value
 */
static void JDD_frameCheck(boolean *validFrameReceived)
{

  VCU_CptTemporel VCU_CptTemporel_signal;

  *validFrameReceived = FALSE;

  if(FALSE != JDD_VCU552_pending)
    {
      (void) Rte_Read_Pp_VCU_CptTemporel_VCU_CptTemporel(&VCU_CptTemporel_signal);/* PRQA S 3110, 3417, 0315, 3426 #RTE read/write confirmation not used*/
      if((JDD_CPTTEMPOREL_INVALID_1 != VCU_CptTemporel_signal) && (JDD_CPTTEMPOREL_INVALID_2 != VCU_CptTemporel_signal))
	{
	  *validFrameReceived = TRUE;
	}
      else
      {
    	  /* JACE: Workaround */
    	  /* If an invalid signal value is received after being synchronized,
    	   *  then we must consider that we are no longer synchornized.
    	   *  To be tested if it works.
    	   */
    	  JDD_VCU552_received = FALSE;
      }

      /* Reset the flag */
      JDD_VCU552_pending = FALSE;
    }
}

/**
 * Manages Local counter ticks
 * \param[out]  temporelLocal_overflow Returns TRUE if local counter raised Limit
 */
static void JDD_localCounterTick(boolean *temporelLocal_overflow)
{
  *temporelLocal_overflow = FALSE;
  /* Implements OBCP-27245 */
  if(JDD_LocalCounterPartial < JDD_LOCALCOUNTER_PARTIAL_MAX)
    {
      JDD_LocalCounterPartial += JDD_LOCALCOUNTER_TICK_MS;
    }
  else
    {
      if(JDD_LOCAL_COUNTER_MAX_NOT_RAISED)
	{
	  JDD_LocalCounter += JDD_LOCALCOUNTER_INCREMENTVALUE;
	  JDD_LOCAL_COUNTER_PARTIAL_RESET();
	}
      else
	{
	  *temporelLocal_overflow = TRUE;
	}
    }
}

/**
 * Initializes NVM memory or format it if version is not the same
 */
static void JDD_Initialize_NVM(void)
{
  boolean isShutingDown;
  uint8 resetRequest;
  Rte_DT_IdtArrayJDDNvMRamMirror_0 * pData = Rte_Pim_PimJDD_NvMRamMirror();
  
  jdd_dtc_id indexDTC;
  boolean eventRaised;
  uint16 pos;
WUM_GetResetRequest(&resetRequest);
(void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&isShutingDown);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

  /* Verify that version is OK and Flag of virgin memory is also the expected. If not reset memory */
  if((pData[0U] != JDD_NVM_EVENTS_VERSION) || (pData[JDD_NVM_MEM_BYTELEN-JDD_NVM_EVENTS_VIRGINFLAG_SIZE] != JDD_NVM_EVENTS_VIRGINFLAG))
    {
      pData[0U] = JDD_NVM_EVENTS_VERSION;
      pData[JDD_NVM_MEM_BYTELEN-JDD_NVM_EVENTS_VIRGINFLAG_SIZE] = JDD_NVM_EVENTS_VIRGINFLAG;
      for(pos = JDD_NVM_EVENTS_VERSION_SIZE; pos  < (JDD_NVM_MEM_BYTELEN-JDD_NVM_EVENTS_VIRGINFLAG_SIZE); pos++)
	{
	  pData[pos] = (uint8) 0U;
	}

      if((FALSE == isShutingDown) && (0x02U != resetRequest))
	{
	  (void)Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(TRUE);
	}
    }
  else
    {
      for(indexDTC = 0U; indexDTC < JDD_NUMBER_OF_DTCS; indexDTC++)
	{
	  eventRaised = JDD_DeSerialize_NVM(&jdd_dtc_configs[indexDTC]);
	  if(FALSE != eventRaised)
	    {
	      JDD_updateEvents_enqueue(indexDTC);
	    }
	}

    }
}

/**
 * Updates event \p dtcConfig
 * \param[in,out] dtcConfig event to be stored
 * \param[in] src origin of data
 * \return Indicates if the event is raised and needs to be enqueued
 */
/* PRQA S 2985 ++ #operation redundance avoidance*/
static boolean  JDD_DeSerialize_NVM(const JDD_dtc_config_t * dtcConfig)
{
  const Rte_DT_IdtArrayJDDNvMRamMirror_0 * src = Rte_Pim_PimJDD_NvMRamMirror();
  uint16 pos = ((uint16) dtcConfig->nvmIndex * JDD_NVM_SIGNAL_SIZE) + JDD_NVM_EVENTS_VERSION_SIZE;

  boolean retVal = FALSE;
  dtcConfig->data->confirmation.state = src[pos+ 0U];/* PRQA S 4442 #Cast permitted due to src is readed directly from NVM*/
  dtcConfig->data->confirmation.kilometrage = ( ((VCU_Kilometrage) src[pos+ 1U] ) << 16U) & 0x00FF0000U;
  dtcConfig->data->confirmation.kilometrage |= ( ((VCU_Kilometrage) src[pos+ 2U] ) << 8U) & 0x0000FF00U;
  dtcConfig->data->confirmation.kilometrage |= ( ((VCU_Kilometrage) src[pos+ 3U] ) )	    & 0x000000FFU;

  dtcConfig->data->confirmation.referenceHorarie = ( ((VCU_Kilometrage)  src[pos+ 4U] ) <<  24U) & 0xFF000000U;
  dtcConfig->data->confirmation.referenceHorarie |= ( ((VCU_Kilometrage) src[pos+ 5U] ) << 16U)  & 0x00FF0000U;
  dtcConfig->data->confirmation.referenceHorarie |= ( ((VCU_Kilometrage) src[pos+ 6U] ) << 8U)   & 0x0000FF00U;
  dtcConfig->data->confirmation.referenceHorarie |= ( ((VCU_Kilometrage) src[pos+ 7U] ) )	   & 0x000000FFU;

  dtcConfig->data->confirmation.situation = ( ((VCU_Kilometrage) src[pos+ 8U] ) << 16U) & 0x00FF0000U;
  dtcConfig->data->confirmation.situation |= ( ((VCU_Kilometrage) src[pos+ 9U] ) << 8U) & 0x0000FF00U;
  dtcConfig->data->confirmation.situation |= ( ((VCU_Kilometrage) src[pos+ 10U] ) )	   & 0x000000FFU;

  dtcConfig->data->healing.state = src[pos+ 11U];/* PRQA S 4442 #Cast permitted due to src is readed directly from NVM*/
  dtcConfig->data->healing.kilometrage = ( ((VCU_Kilometrage) src[pos+ 12U] ) << 16U) & 0x00FF0000U;
  dtcConfig->data->healing.kilometrage |= ( ((VCU_Kilometrage) src[pos+ 13U] ) << 8U) & 0x0000FF00U;
  dtcConfig->data->healing.kilometrage |= ( ((VCU_Kilometrage) src[pos+ 14U] ) )	& 0x000000FFU;

  dtcConfig->data->healing.referenceHorarie = ( ((VCU_Kilometrage)  src[pos+ 15U] ) <<  24U) & 0xFF000000U;
  dtcConfig->data->healing.referenceHorarie |= ( ((VCU_Kilometrage) src[pos+ 16U] ) << 16U)  & 0x00FF0000U;
  dtcConfig->data->healing.referenceHorarie |= ( ((VCU_Kilometrage) src[pos+ 17U] ) << 8U)   & 0x0000FF00U;
  dtcConfig->data->healing.referenceHorarie |= ( ((VCU_Kilometrage) src[pos+ 18U] ) )	   & 0x000000FFU;

  dtcConfig->data->healing.situation = ( ((VCU_Kilometrage) src[pos+ 19U] ) << 16U) & 0x00FF0000U;
  dtcConfig->data->healing.situation |= ( ((VCU_Kilometrage) src[pos+ 20U] ) << 8U) & 0x0000FF00U;
  dtcConfig->data->healing.situation |= ( ((VCU_Kilometrage) src[pos+ 21U] ) )	   & 0x000000FFU;

  if( (JDD_STATE_RAISED == dtcConfig->data->confirmation.state) || (JDD_STATE_RAISED == dtcConfig->data->healing.state))
    {
      retVal = TRUE;
    }
  return retVal;

}


/**
 * Stores in NVM ram \p dtcConfig variable data
 * \param[in] dtcConfig event to be stored
 * \param[in] stateSerialize Updates just pass with JDD_NVM_STATE_PASS, just fail with JDD_NVM_STATE_FAIL or both with JDD_NVM_STATE_ALL
 */

static void JDD_Serialize_NVM_Event(const JDD_dtc_config_t * dtcConfig,jdd_nvm_state_t stateSerialize)
{
  boolean isShutingDown;
  uint8 resetRequest;
  Rte_DT_IdtArrayJDDNvMRamMirror_0 * dest = Rte_Pim_PimJDD_NvMRamMirror();
  uint16 pos;
  (void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&isShutingDown);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  WUM_GetResetRequest(&resetRequest);

  if((FALSE == isShutingDown) && (0x02U != resetRequest) && (NULL_PTR != dtcConfig) && (JDD_NUMBER_OF_DTCS > dtcConfig->nvmIndex))
    {
	  pos = ((uint16) dtcConfig->nvmIndex * JDD_NVM_SIGNAL_SIZE) + JDD_NVM_EVENTS_VERSION_SIZE;
      if((JDD_NVM_STATE_FAIL == stateSerialize)||(JDD_NVM_STATE_ALL == stateSerialize))
	{
	  dest[pos+0U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.state,0U);/* PRQA S 4393, 4523 #Type used permitted*/

	  dest[pos+1U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.kilometrage,2U);
	  dest[pos+2U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.kilometrage,1U);
	  dest[pos+3U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.kilometrage,0U);
	  dest[pos+4U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.referenceHorarie,3U);
	  dest[pos+5U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.referenceHorarie,2U);
	  dest[pos+6U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.referenceHorarie,1U);
	  dest[pos+7U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.referenceHorarie,0U);
	  dest[pos+8U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.situation,2U);
	  dest[pos+9U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.situation,1U);
	  dest[pos+10U] = JDD_EXTRACT_BYTE(dtcConfig->data->confirmation.situation,0U);
	}

      if((JDD_NVM_STATE_PASS == stateSerialize)||(JDD_NVM_STATE_ALL == stateSerialize))
	{
	  dest[pos+11U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.state,0U);/* PRQA S 4393, 4523 #Type used permitted*/

	  dest[pos+12U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.kilometrage,2U);
	  dest[pos+13U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.kilometrage,1U);
	  dest[pos+14U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.kilometrage,0U);
	  dest[pos+15U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.referenceHorarie,3U);
	  dest[pos+16U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.referenceHorarie,2U);
	  dest[pos+17U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.referenceHorarie,1U);
	  dest[pos+18U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.referenceHorarie,0U);
	  dest[pos+19U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.situation,2U);
	  dest[pos+20U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.situation,1U);
	  dest[pos+21U] = JDD_EXTRACT_BYTE(dtcConfig->data->healing.situation,0U);
	}
      (void)Rte_Call_NvMService_AC3_SRBS_JDDNvBlockNeed_SetRamBlockStatus(TRUE);
    }
}
/* PRQA S 2985 -- #operation redundance avoidance*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
