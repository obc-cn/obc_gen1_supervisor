/**************************************************************************//**
 * \file	ctApPCOM_RX_cfg.c
 * \author	M0069810 Juan Carlos Romanillos
 * \date		20 nov. 2019
 * \copyright	MAHLE ELECTRONICS - 2019
 * \brief
 * \ingroup ctApPCOM_cfg
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
#include "CtApPCOM_RX_cfg.h"
 /* PRQA S 0857,0380 -- */
/*****************************************************************************
 * CONSTANT DEFINITIONS
 *****************************************************************************/

/*****************************************************************************
 * TYPEDEFS
 *****************************************************************************/

/*****************************************************************************
 * PUBLIC_VARIABLE_DEFINITIONS
 *****************************************************************************/

/*****************************************************************************
 * MODULE_VARIABLES
 *****************************************************************************/

/*****************************************************************************
 * MODULE_FUNCTION_PROTOTYPES
 *****************************************************************************/

/*****************************************************************************
 * PUBLIC_FUNCTION_DEFINITIONS
 *****************************************************************************/

/*****************************************************************************
 * MODULE_FUNCTION_DEFINITIONS
 *****************************************************************************/

/* PRQA S 0315 EOF # This supression applies to all read calls to rte. These functions of the rte are called according to Autosar standard format */

/* PRQA S 3417, 3226  EOF # This violation applies to all write calls to rte. These function is called according to Autosar format */

Std_ReturnType PCOM_RX__BMS_SOC_GetInitial(uint16 * value)
{
	  *value = PCOM_BMS_SOC_Initial;
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_SOC_GetPhysical(BMS_SOC * value)
{
  return  Rte_Read_PpBMS_SOC_BMS_SOC(value);
}
Std_ReturnType PCOM_RX__BMS_SOC_GetDefaultSubstValue(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC();
  return E_OK;
}
boolean PCOM_RX__BMS_SOC_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC();
}
boolean PCOM_RX__BMS_SOC_StubForbidden(pcom_gen_t * value) /* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

  if((*value >= (0x3E9UL)) && (*value <= (0x3FDUL))){
	  result=TRUE;
  }

  return result;
}
Std_ReturnType PCOM_RX__BMS_SOC_Set(const BMS_SOC * value)
{
	PCOM_BMS_SOC_Initial = (uint16)(*value);
  return  Rte_Write_PpInt_BMS_SOC_BMS_SOC(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_SOC_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC();
}
IdtSignalDiagTime PCOM_RX__BMS_SOC_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC();
}
IdtSignalDiagTime PCOM_RX__BMS_SOC_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC();
}
IdtSignalDiagTime PCOM_RX__BMS_SOC_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC();
}

Std_ReturnType PCOM_RX__BMS_Voltage_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_Voltage();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_Voltage_GetPhysical(BMS_Voltage * value)
{
  return  Rte_Read_PpBMS_Voltage_BMS_Voltage(value);
}
Std_ReturnType PCOM_RX__BMS_Voltage_GetDefaultSubstValue(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage();
  return E_OK;
}
boolean PCOM_RX__BMS_Voltage_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage();
}
boolean PCOM_RX__BMS_Voltage_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if((*value >= (0x1F5UL)) && (*value <= (0x1FDUL))){
		  result=TRUE;
	  }

	  return result;
}
Std_ReturnType PCOM_RX__BMS_Voltage_Set(const BMS_Voltage * value)
{
  return  Rte_Write_PpInt_BMS_Voltage_BMS_Voltage(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_Voltage_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage();
}
IdtSignalDiagTime PCOM_RX__BMS_Voltage_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage();
}
IdtSignalDiagTime PCOM_RX__BMS_Voltage_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage();
}
IdtSignalDiagTime PCOM_RX__BMS_Voltage_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage();
}

Std_ReturnType PCOM_RX__BMS_RelayOpenReq_GetInitial(BMS_RelayOpenReq * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_GetPhysical(BMS_RelayOpenReq * value)
{
  return  Rte_Read_PpBMS_RelayOpenReq_BMS_RelayOpenReq(value);
}
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_GetDefaultSubstValue(BMS_RelayOpenReq * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq();
  return E_OK;
}
boolean PCOM_RX__BMS_RelayOpenReq_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq();
}
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_Set(const BMS_RelayOpenReq * value)
{
  return  Rte_Write_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(*value);
}

Std_ReturnType PCOM_RX__BMS_MainConnectorState_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_MainConnectorState_GetPhysical(BMS_MainConnectorState * value)
{
  return  Rte_Read_PpBMS_MainConnectorState_BMS_MainConnectorState(value);
}
Std_ReturnType PCOM_RX__BMS_MainConnectorState_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState();
  return E_OK;
}
boolean PCOM_RX__BMS_MainConnectorState_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState();
}
Std_ReturnType PCOM_RX__BMS_MainConnectorState_Set(const BMS_MainConnectorState * value)
{
  return  Rte_Write_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_MainConnectorState_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState();
}
IdtSignalDiagTime PCOM_RX__BMS_MainConnectorState_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState();
}

Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_GetInitial(BMS_QuickChargeConnectorState * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_GetPhysical(BMS_QuickChargeConnectorState * value)
{
  return  Rte_Read_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(value);
}
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_GetDefaultSubstValue(BMS_QuickChargeConnectorState * value)
{
  *value =  Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState();
  return E_OK;
}
boolean PCOM_RX__BMS_QuickChargeConnectorState_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState();
}
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_Set(const BMS_QuickChargeConnectorState * value)
{
  return  Rte_Write_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(*value);
}

Std_ReturnType PCOM_RX__BMS_AuxBattVolt_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_AuxBattVolt_GetPhysical(BMS_AuxBattVolt * value)
{
  return  Rte_Read_PpBMS_AuxBattVolt_BMS_AuxBattVolt(value);
}
Std_ReturnType PCOM_RX__BMS_AuxBattVolt_GetDefaultSubstValue(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt();
  return E_OK;
}
boolean PCOM_RX__BMS_AuxBattVolt_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt();
}
boolean PCOM_RX__BMS_AuxBattVolt_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if((*value >= (0x961UL)) && (*value <= (0xFFEUL))){
		  result=TRUE;
	  }

	  return result;
}
Std_ReturnType PCOM_RX__BMS_AuxBattVolt_Set(const BMS_AuxBattVolt * value)
{
  return  Rte_Write_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt();
}
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt();
}
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt();
}
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt();
}

Std_ReturnType PCOM_RX__BMS_Fault_GetInitial(BMS_Fault * value)
{
  *value =  Rte_CData_CalSignal_BMS5_359_Initial_Value_BMS_Fault();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_Fault_GetPhysical(BMS_Fault * value)
{
  return  Rte_Read_PpBMS_Fault_BMS_Fault(value);
}
Std_ReturnType PCOM_RX__BMS_Fault_GetDefaultSubstValue(BMS_Fault * value)
{
  *value =  Rte_CData_CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault();
  return E_OK;
}
boolean PCOM_RX__BMS_Fault_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault();
}
Std_ReturnType PCOM_RX__BMS_Fault_Set(const BMS_Fault * value)
{
  return  Rte_Write_PpInt_BMS_Fault_BMS_Fault(*value);
}

Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_GetPhysical(BMS_HighestChargeVoltageAllow * value)
{
  return  Rte_Read_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(value);
}
Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_GetDefaultSubstValue(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow();
  return E_OK;
}
boolean PCOM_RX__BMS_HighestChargeVoltageAllow_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow();
}
boolean PCOM_RX__BMS_HighestChargeVoltageAllow_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if((*value >= (0x1771UL)) && (*value <= (0xFFFFUL))){
		  result=TRUE;
	  }

	  return result;
}
Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_Set(const BMS_HighestChargeVoltageAllow * value)
{
  return  Rte_Write_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeVoltageAllow_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow();
}
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeVoltageAllow_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow();
}

Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_GetPhysical(BMS_HighestChargeCurrentAllow * value)
{
  return  Rte_Read_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(value);
}
Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_GetDefaultSubstValue(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow();
  return E_OK;
}
boolean PCOM_RX__BMS_HighestChargeCurrentAllow_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow();
}
boolean PCOM_RX__BMS_HighestChargeCurrentAllow_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if((*value >= (0x7D1UL)) && (*value <= (0x7FFUL))){	/* Bug found in PRQA, this checking was configured with wrong values and were always been evaluated as FALSE */
		  result=TRUE;
	  }

	  return result;
}
Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_Set(const BMS_HighestChargeCurrentAllow * value)
{
  return  Rte_Write_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeCurrentAllow_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow();
}
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeCurrentAllow_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow();
}

Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_GetInitial(BMS_OnBoardChargerEnable * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_GetPhysical(BMS_OnBoardChargerEnable * value)
{
  return  Rte_Read_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(value);
}
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_GetDefaultSubstValue(BMS_OnBoardChargerEnable * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable();
  return E_OK;
}
boolean PCOM_RX__BMS_OnBoardChargerEnable_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable();
}
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_Set(const BMS_OnBoardChargerEnable * value)
{
  return  Rte_Write_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(*value);
}

Std_ReturnType PCOM_RX__BMS_SlowChargeSt_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_GetPhysical(BMS_SlowChargeSt * value)
{
  return  Rte_Read_PpBMS_SlowChargeSt_BMS_SlowChargeSt(value);
}
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt();
  return E_OK;
}
boolean PCOM_RX__BMS_SlowChargeSt_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt();
}
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_Set(const BMS_SlowChargeSt * value)
{
  return  Rte_Write_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt(*value);
}

Std_ReturnType PCOM_RX__BMS_FastChargeSt_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_FastChargeSt_GetPhysical(BMS_FastChargeSt * value)
{
  return  Rte_Read_PpBMS_FastChargeSt_BMS_FastChargeSt(value);
}
Std_ReturnType PCOM_RX__BMS_FastChargeSt_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt();
  return E_OK;
}
boolean PCOM_RX__BMS_FastChargeSt_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt();
}
Std_ReturnType PCOM_RX__BMS_FastChargeSt_Set(const BMS_FastChargeSt * value)
{
  return  Rte_Write_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(*value);
}

Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_GetPhysical(BMS_CC2_connection_Status * value)
{
  return  Rte_Read_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status(value);
}
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status();
  return E_OK;
}
boolean PCOM_RX__BMS_CC2_connection_Status_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status();
}
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_Set(const BMS_CC2_connection_Status * value)
{
  return  Rte_Write_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(*value);
}

Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE();
  return E_OK;
}
Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetPhysical(BMS_DCRelayVoltage * value)
{
  return Rte_Read_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage(value);
}
Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetDefaultSubstValue(uint16 * value)
{
  *value =  Rte_CData_CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE();
  return E_OK;
}
boolean PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE();
}
boolean PCOM_RX__BMS_DC_RELAY_VOLTAGE_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if((*value >= (0x1F5UL)) && (*value <= ((0x1FFUL)))){
		  result=TRUE;
	  }

	  return result;
}
Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_Set(const BMS_DCRelayVoltage * value)
{
  return Rte_Write_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(*value);
}
IdtSignalDiagTime PCOM_RX__BMS_DC_RELAY_VOLTAGE_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE();
}
IdtSignalDiagTime PCOM_RX__BMS_DC_RELAY_VOLTAGE_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE();
}

Std_ReturnType PCOM_RX__BSI_MainWakeup_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup();
  return E_OK;
}
Std_ReturnType PCOM_RX__BSI_MainWakeup_GetPhysical(BSI_MainWakeup * value)
{
  return  Rte_Read_PpBSI_MainWakeup_BSI_MainWakeup(value);
}
Std_ReturnType PCOM_RX__BSI_MainWakeup_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup();
  return E_OK;
}
boolean PCOM_RX__BSI_MainWakeup_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup();
}
Std_ReturnType PCOM_RX__BSI_MainWakeup_Set(const BSI_MainWakeup * value)
{
  return  Rte_Write_PpInt_BSI_MainWakeup_BSI_MainWakeup(*value);
}

Std_ReturnType PCOM_RX__BSI_LockedVehicle_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle();
  return E_OK;
}
Std_ReturnType PCOM_RX__BSI_LockedVehicle_GetPhysical(BSI_LockedVehicle * value)
{
  return  Rte_Read_PpBSI_LockedVehicle_BSI_LockedVehicle(value);
}
Std_ReturnType PCOM_RX__BSI_LockedVehicle_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle();
  return E_OK;
}
boolean PCOM_RX__BSI_LockedVehicle_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle();
}
Std_ReturnType PCOM_RX__BSI_LockedVehicle_Set(const BSI_LockedVehicle * value)
{
  return  Rte_Write_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(*value);
}

Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus();
  return E_OK;
}
Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_GetPhysical(BSI_ChargeTypeStatus * value)
{
  return  Rte_Read_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus(value);
}
Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus();
  return E_OK;
}
boolean PCOM_RX__BSI_ChargeTypeStatus_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus();
}
boolean PCOM_RX__BSI_ChargeTypeStatus_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if(*value == (0x3UL)){
		  result=TRUE;
	  }

	  return result;
}
Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_Set(const BSI_ChargeTypeStatus * value)
{
  return  Rte_Write_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(*value);
}
IdtSignalDiagTime PCOM_RX__BSI_ChargeTypeStatus_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus();
}
IdtSignalDiagTime PCOM_RX__BSI_ChargeTypeStatus_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus();
}

Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_GetInitial(BSI_PreDriveWakeup * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup();
  return E_OK;
}
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_GetPhysical(BSI_PreDriveWakeup * value)
{
  return  Rte_Read_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup(value);
}
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_GetDefaultSubstValue(BSI_PreDriveWakeup * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup();
  return E_OK;
}
boolean PCOM_RX__BSI_PreDriveWakeup_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup();
}
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_Set(const BSI_PreDriveWakeup * value)
{
  return  Rte_Write_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(*value);
}

Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_GetInitial(BSI_PostDriveWakeup * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup();
  return E_OK;
}
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_GetPhysical(BSI_PostDriveWakeup * value)
{
  return  Rte_Read_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup(value);
}
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_GetDefaultSubstValue(BSI_PostDriveWakeup * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup();
  return E_OK;
}
boolean PCOM_RX__BSI_PostDriveWakeup_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup();
}
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_Set(const BSI_PostDriveWakeup * value)
{
  return  Rte_Write_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(*value);
}

Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_GetInitial(BSI_VCUSynchroGPC * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_GetPhysical(BSI_VCUSynchroGPC * value)
{
  return  Rte_Read_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC(value);
}
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_GetDefaultSubstValue(BSI_VCUSynchroGPC * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC();
  return E_OK;
}
boolean PCOM_RX__VCU_SYNCHRO_GPC_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC();
}
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_Set(const BSI_VCUSynchroGPC * value)
{
  return  Rte_Write_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(*value);
}

Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetPhysical(BSI_VCUHeatPumpWorkingMode * value)
{
  return  Rte_Read_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(value);
}
Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE();
  return E_OK;
}
boolean PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE();
}
boolean PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

  if((*value==(0x12UL))||(((*value>=(0x14UL))&&(*value<=(0x1FUL))))){
	  result=TRUE;
  }

  return result;

}
Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_Set(const BSI_VCUHeatPumpWorkingMode * value)
{
  return  Rte_Write_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(*value);
}
IdtSignalDiagTime PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE();
}
IdtSignalDiagTime PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE();
}

Std_ReturnType PCOM_RX__CHARGE_STATE_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE();
  return E_OK;
}
Std_ReturnType PCOM_RX__CHARGE_STATE_GetPhysical(BSI_ChargeState * value)
{
  return  Rte_Read_PpBSI_ChargeState_BSI_ChargeState(value);
}
Std_ReturnType PCOM_RX__CHARGE_STATE_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE();
  return E_OK;
}
boolean PCOM_RX__CHARGE_STATE_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE();
}
Std_ReturnType PCOM_RX__CHARGE_STATE_Set(const BSI_ChargeState * value)
{
  return  Rte_Write_PpInt_BSI_ChargeState_BSI_ChargeState(*value);
}

Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_GetPhysical(VCU_DCDCVoltageReq * value)
{
  return  Rte_Read_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq(value);
}
Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq();
  return E_OK;
}
boolean PCOM_RX__VCU_DCDCVoltageReq_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq();
}
boolean PCOM_RX__VCU_DCDCVoltageReq_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	  if((*value>=(0x6DUL))&&(*value<=(0x7EUL))){
		  result=TRUE;
	  }

	  return result;

}
Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_Set(const VCU_DCDCVoltageReq * value)
{
  return  Rte_Write_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(*value);
}
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq();
}
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq();
}

Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_GetInitial(VCU_ActivedischargeCommand * value)
{
  *value =  Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_GetPhysical(VCU_ActivedischargeCommand * value)
{
  return  Rte_Read_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand(value);
}
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_GetDefaultSubstValue(VCU_ActivedischargeCommand * value)
{
  *value =  Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand();
  return E_OK;
}
boolean PCOM_RX__VCU_ActivedischargeCommand_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand();
}
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_Set(const VCU_ActivedischargeCommand * value)
{
  return  Rte_Write_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(*value);
}

Std_ReturnType PCOM_RX__VCU_DCDCActivation_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_DCDCActivation_GetPhysical(VCU_DCDCActivation * value)
{
  return  Rte_Read_PpVCU_DCDCActivation_VCU_DCDCActivation(value);
}
Std_ReturnType PCOM_RX__VCU_DCDCActivation_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation();
  return E_OK;
}
boolean PCOM_RX__VCU_DCDCActivation_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation();
}
Std_ReturnType PCOM_RX__VCU_DCDCActivation_Set(const VCU_DCDCActivation * value)
{
  return  Rte_Write_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(*value);
}
IdtSignalDiagTime PCOM_RX__VCU_DCDCActivation_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation();
}
IdtSignalDiagTime PCOM_RX__VCU_DCDCActivation_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation();
}

Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_GetInitial(DIAG_INTEGRA_ELEC * value)
{
  *value =  Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC();
  return E_OK;
}
Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_GetPhysical(DIAG_INTEGRA_ELEC * value)
{
  return  Rte_Read_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(value);
}

Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_Set(const DIAG_INTEGRA_ELEC * value)
{
  return  Rte_Write_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(*value);
}

Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_GetInitial(EFFAC_DEFAUT_DIAG * value)
{
  *value =  Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG();
  return E_OK;
}
Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_GetPhysical(EFFAC_DEFAUT_DIAG * value)
{
  return  Rte_Read_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(value);
}

Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_Set(const EFFAC_DEFAUT_DIAG * value)
{
  return  Rte_Write_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(*value);
}

Std_ReturnType PCOM_RX__MODE_DIAG_GetInitial(MODE_DIAG * value)
{
  *value =  Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG();
  return E_OK;
}
Std_ReturnType PCOM_RX__MODE_DIAG_GetPhysical(MODE_DIAG * value)
{
  return  Rte_Read_PpMODE_DIAG_MODE_DIAG(value);
}

Std_ReturnType PCOM_RX__MODE_DIAG_Set(const MODE_DIAG * value)
{
  return  Rte_Write_PpInt_MODE_DIAG_MODE_DIAG(*value);
}


Std_ReturnType PCOM_RX__DATA_ACQ_JDD_BSI_2_GetInitial(DATA_ACQ_JDD_BSI_2 * value)
{
  *value = 0U;
  return E_OK;
}
Std_ReturnType PCOM_RX__DATA_ACQ_JDD_BSI_2_GetPhysical(DATA_ACQ_JDD_BSI_2 * value)
{
  return Rte_Read_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(value);
}
Std_ReturnType PCOM_RX__DATA_ACQ_JDD_BSI_2_Set(const DATA_ACQ_JDD_BSI_2 * value)
{
  return Rte_Write_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(*value);
}


Std_ReturnType PCOM_RX__VCU_EPWT_Status_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_EPWT_Status_GetPhysical(VCU_EPWT_Status * value)
{
  return  Rte_Read_PpVCU_EPWT_Status_VCU_EPWT_Status(value);
}
Std_ReturnType PCOM_RX__VCU_EPWT_Status_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status();
  return E_OK;
}
boolean PCOM_RX__VCU_EPWT_Status_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status();
}
Std_ReturnType PCOM_RX__VCU_EPWT_Status_Set(const VCU_EPWT_Status * value)
{
  return  Rte_Write_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(*value);
}
IdtSignalDiagTime PCOM_RX__VCU_EPWT_Status_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status();
}
IdtSignalDiagTime PCOM_RX__VCU_EPWT_Status_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status();
}

Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT();
  return E_OK;
}
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_GetPhysical(VCU_CompteurRazGct * value)
{
  return  Rte_Read_PpVCU_CompteurRazGct_VCU_CompteurRazGct(value);
}
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT();
  return E_OK;
}
boolean PCOM_RX__COMPTEUR_RAZ_GCT_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT();
}
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_Set(const VCU_CompteurRazGct * value)
{
  return  Rte_Write_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(*value);
}
IdtSignalDiagTime PCOM_RX__COMPTEUR_RAZ_GCT_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT();
}
IdtSignalDiagTime PCOM_RX__COMPTEUR_RAZ_GCT_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT();
}

Std_ReturnType PCOM_RX__CPT_TEMPOREL_GetInitial(uint32 * value)
{
  *value =  Rte_CData_CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL();
  return E_OK;
}
Std_ReturnType PCOM_RX__CPT_TEMPOREL_GetPhysical(VCU_CptTemporel * value)
{
  return  Rte_Read_PpVCU_CptTemporel_VCU_CptTemporel(value);
}
Std_ReturnType PCOM_RX__CPT_TEMPOREL_GetDefaultSubstValue(uint32 * value)
{
  *value =  Rte_CData_CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL();
  return E_OK;
}
boolean PCOM_RX__CPT_TEMPOREL_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL();
}
Std_ReturnType PCOM_RX__CPT_TEMPOREL_Set(const VCU_CptTemporel * value)
{
  return  Rte_Write_PpInt_VCU_CptTemporel_VCU_CptTemporel(*value);
}
IdtSignalDiagTime PCOM_RX__CPT_TEMPOREL_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL();
}
IdtSignalDiagTime PCOM_RX__CPT_TEMPOREL_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL();
}


Std_ReturnType PCOM_RX__KILOMETRAGE_GetInitial(uint32 * value)
{
  *value = PCOM_KILOMETRAGE_Initial;
  return E_OK;
}
Std_ReturnType PCOM_RX__KILOMETRAGE_GetPhysical(VCU_Kilometrage * value)
{
  return  Rte_Read_PpVCU_Kilometrage_VCU_Kilometrage(value);
}
Std_ReturnType PCOM_RX__KILOMETRAGE_GetDefaultSubstValue(uint32 * value)
{
  *value =  Rte_CData_CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE();
  return E_OK;
}
boolean PCOM_RX__KILOMETRAGE_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE();
}
Std_ReturnType PCOM_RX__KILOMETRAGE_Set(const VCU_Kilometrage * value)
{
	PCOM_KILOMETRAGE_Initial = (uint32)(*value);
  return  Rte_Write_PpInt_VCU_Kilometrage_VCU_Kilometrage(*value);
}
IdtSignalDiagTime PCOM_RX__KILOMETRAGE_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE();
}
IdtSignalDiagTime PCOM_RX__KILOMETRAGE_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE();
}

Std_ReturnType PCOM_RX__POS_SHUNT_JDD_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD();
  return E_OK;
}
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_GetPhysical(VCU_PosShuntJDD * value)
{
  return  Rte_Read_PpVCU_PosShuntJDD_VCU_PosShuntJDD(value);
}
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD();
  return E_OK;
}
boolean PCOM_RX__POS_SHUNT_JDD_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD();
}
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_Set(const VCU_PosShuntJDD * value)
{
  return  Rte_Write_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(*value);
}

Std_ReturnType PCOM_RX__CDE_APC_JDD_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD();
  return E_OK;
}
Std_ReturnType PCOM_RX__CDE_APC_JDD_GetPhysical(VCU_CDEApcJDD * value)
{
  return  Rte_Read_PpVCU_CDEApcJDD_VCU_CDEApcJDD(value);
}
Std_ReturnType PCOM_RX__CDE_APC_JDD_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD();
  return E_OK;
}
boolean PCOM_RX__CDE_APC_JDD_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD();
}
Std_ReturnType PCOM_RX__CDE_APC_JDD_Set(const VCU_CDEApcJDD * value)
{
  return  Rte_Write_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(*value);
}

Std_ReturnType PCOM_RX__CDE_ACC_JDD_GetInitial(VCU_CDEAccJDD * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD();
  return E_OK;
}
Std_ReturnType PCOM_RX__CDE_ACC_JDD_GetPhysical(VCU_CDEAccJDD * value)
{
  return  Rte_Read_PpVCU_CDEAccJDD_VCU_CDEAccJDD(value);
}
Std_ReturnType PCOM_RX__CDE_ACC_JDD_GetDefaultSubstValue(VCU_CDEAccJDD * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD();
  return E_OK;
}
boolean PCOM_RX__CDE_ACC_JDD_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD();
}
Std_ReturnType PCOM_RX__CDE_ACC_JDD_Set(const VCU_CDEAccJDD * value)
{
  return  Rte_Write_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(*value);
}

Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV();
  return E_OK;
}
Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_GetPhysical(VCU_EtatPrincipSev * value)
{
  return  Rte_Read_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev(value);
}
Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV();
  return E_OK;
}
boolean PCOM_RX__ETAT_PRINCIP_SEV_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV();
}
boolean PCOM_RX__ETAT_PRINCIP_SEV_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	  if(*value==(0x3UL)){
		  result=TRUE;
	  }

	  return result;

}
Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_Set(const VCU_EtatPrincipSev * value)
{
  return  Rte_Write_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(*value);
}
IdtSignalDiagTime PCOM_RX__ETAT_PRINCIP_SEV_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV();
}
IdtSignalDiagTime PCOM_RX__ETAT_PRINCIP_SEV_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV();
}

Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC();
  return E_OK;
}
Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_GetPhysical(VCU_EtatReseauElec * value)
{
  return  Rte_Read_PpVCU_EtatReseauElec_VCU_EtatReseauElec(value);
}
Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC();
  return E_OK;
}
boolean PCOM_RX__ETAT_RESEAU_ELEC_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC();
}
boolean PCOM_RX__ETAT_RESEAU_ELEC_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	  boolean result=FALSE;

	  if((*value>=(0x8UL))&&(*value<=(0xFUL))){
		  result=TRUE;
	  }

	  return result;

}
Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_Set(const VCU_EtatReseauElec * value)
{
  return  Rte_Write_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(*value);
}
IdtSignalDiagTime PCOM_RX__ETAT_RESEAU_ELEC_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC();
}
IdtSignalDiagTime PCOM_RX__ETAT_RESEAU_ELEC_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC();
}

Std_ReturnType PCOM_RX__ETAT_GMP_HYB_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB();
  return E_OK;
}
Std_ReturnType PCOM_RX__ETAT_GMP_HYB_GetPhysical(VCU_EtatGmpHyb * value)
{
  return  Rte_Read_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb(value);
}
Std_ReturnType PCOM_RX__ETAT_GMP_HYB_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB();
  return E_OK;
}
boolean PCOM_RX__ETAT_GMP_HYB_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB();
}
boolean PCOM_RX__ETAT_GMP_HYB_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result;

	switch(*value){
		case(0x1UL):
		case(0x2UL):
		case(0x3UL):
		case(0x5UL):
		case(0x6UL):
		case(0x7UL):
		case(0x9UL):
		case(0xBUL):
		case(0xDUL):
		case(0xFUL):
			result=TRUE;
			break;
		default:
			result=FALSE;
			break;
	}

	return result;

}
Std_ReturnType PCOM_RX__ETAT_GMP_HYB_Set(const VCU_EtatGmpHyb * value)
{
  return  Rte_Write_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(*value);
}
IdtSignalDiagTime PCOM_RX__ETAT_GMP_HYB_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB();
}
IdtSignalDiagTime PCOM_RX__ETAT_GMP_HYB_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB();
}

Std_ReturnType PCOM_RX__DDE_GMV_SEEM_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM();
  return E_OK;
}
Std_ReturnType PCOM_RX__DDE_GMV_SEEM_GetPhysical(VCU_DDEGMVSEEM * value)
{
  return  Rte_Read_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM(value);
}
Std_ReturnType PCOM_RX__DDE_GMV_SEEM_GetDefaultSubstValue(VCU_DDEGMVSEEM * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM();
  return E_OK;
}
boolean PCOM_RX__DDE_GMV_SEEM_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM();
}
boolean PCOM_RX__DDE_GMV_SEEM_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;
	  if((*value>=(0x6FUL))&&(*value<=(0xFDUL))){
		  result=TRUE;
	  }

	  return result;

}
Std_ReturnType PCOM_RX__DDE_GMV_SEEM_Set(const VCU_DDEGMVSEEM * value)
{
  return  Rte_Write_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(*value);
}
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM();
}
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM();
}
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM();
}
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM();
}

Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM();
  return E_OK;
}
Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_GetPhysical(VCU_DMDMeap2SEEM * value)
{
  return  Rte_Read_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(value);
}
Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM();
  return E_OK;
}
boolean PCOM_RX__DMD_MEAP_2_SEEM_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM();
}
boolean PCOM_RX__DMD_MEAP_2_SEEM_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
		boolean result=FALSE;

	  if((*value>=(0x65UL))&&(*value<=(0x7FUL))){
		  result=TRUE;
	  }

	  return result;

}
Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_Set(const VCU_DMDMeap2SEEM * value)
{
  return  Rte_Write_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(*value);
}
IdtSignalDiagTime PCOM_RX__DMD_MEAP_2_SEEM_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM();
}
IdtSignalDiagTime PCOM_RX__DMD_MEAP_2_SEEM_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM();
}

Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetInitial(VCU_StopDelayedHMIWakeup * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP();
  return E_OK;
}
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetPhysical(VCU_StopDelayedHMIWakeup * value)
{
  return  Rte_Read_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(value);
}
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetDefaultSubstValue(VCU_StopDelayedHMIWakeup * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP();
  return E_OK;
}
boolean PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP();
}
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_Set(const VCU_StopDelayedHMIWakeup * value)
{
  return  Rte_Write_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(*value);
}

Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST();
  return E_OK;
}
Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_GetPhysical(VCU_ModeEPSRequest * value)
{
  return  Rte_Read_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest(value);
}
Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST();
  return E_OK;
}
boolean PCOM_RX__MODE_EPS_REQUEST_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST();
}
boolean PCOM_RX__MODE_EPS_REQUEST_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	boolean result=FALSE;

	if((*value>=(0x6UL))&&(*value<=(0x7UL))){
	  result=TRUE;
	}

	return result;

}
Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_Set(const VCU_ModeEPSRequest * value)
{
  return  Rte_Write_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(*value);
}
IdtSignalDiagTime PCOM_RX__MODE_EPS_REQUEST_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST();
}
IdtSignalDiagTime PCOM_RX__MODE_EPS_REQUEST_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST();
}

Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_GetInitial(VCU_PrecondElecWakeup * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP();
  return E_OK;
}
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_GetPhysical(VCU_PrecondElecWakeup * value)
{
  return  Rte_Read_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup(value);
}
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_GetDefaultSubstValue(VCU_PrecondElecWakeup * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP();
  return E_OK;
}
boolean PCOM_RX__PRECOND_ELEC_WAKEUP_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP();
}
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_Set(const VCU_PrecondElecWakeup * value)
{
  return  Rte_Write_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(*value);
}

Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_GetInitial(VCU_DMDActivChiller * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER();
  return E_OK;
}
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_GetPhysical(VCU_DMDActivChiller * value)
{
  return  Rte_Read_PpVCU_DMDActivChiller_VCU_DMDActivChiller(value);
}
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_GetDefaultSubstValue(VCU_DMDActivChiller * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER();
  return E_OK;
}
boolean PCOM_RX__DMD_ACTIV_CHILLER_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER();
}
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_Set(const VCU_DMDActivChiller * value)
{
  return  Rte_Write_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(*value);
}

Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_GetInitial(VCU_DiagMuxOnPwt * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT();
  return E_OK;
}
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_GetPhysical(VCU_DiagMuxOnPwt * value)
{
  return  Rte_Read_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(value);
}
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_GetDefaultSubstValue(VCU_DiagMuxOnPwt * value)
{
  *value =  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT();
  return E_OK;
}
boolean PCOM_RX__DIAG_MUX_ON_PWT_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT();
}
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_Set(const VCU_DiagMuxOnPwt * value)
{
  return  Rte_Write_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(*value);
}

Std_ReturnType PCOM_RX__ABS_VehSpd_GetInitial(ABS_VehSpd * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd();
  return E_OK;
}
Std_ReturnType PCOM_RX__ABS_VehSpd_GetPhysical(ABS_VehSpd * value)
{
  return  Rte_Read_PpABS_VehSpd_ABS_VehSpd(value);
}
Std_ReturnType PCOM_RX__ABS_VehSpd_GetDefaultSubstValue(ABS_VehSpd * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd();
  return E_OK;
}
boolean PCOM_RX__ABS_VehSpd_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd();
}
Std_ReturnType PCOM_RX__ABS_VehSpd_Set(const ABS_VehSpd * value)
{
  return  Rte_Write_PpInt_ABS_VehSpd_ABS_VehSpd(*value);
}
IdtSignalDiagTime PCOM_RX__ABS_VehSpd_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd();
}
IdtSignalDiagTime PCOM_RX__ABS_VehSpd_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd();
}

Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_GetInitial(ABS_VehSpdValidFlag * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag();
  return E_OK;
}
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_GetPhysical(ABS_VehSpdValidFlag * value)
{
  return  Rte_Read_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag(value);
}
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_GetDefaultSubstValue(ABS_VehSpdValidFlag * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag();
  return E_OK;
}
boolean PCOM_RX__ABS_VehSpdValidFlag_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag();
}
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_Set(const ABS_VehSpdValidFlag * value)
{
  return  Rte_Write_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(*value);
}

Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_GetInitial(COUPURE_CONSO_CTPE2 * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2();
  return E_OK;
}
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_GetPhysical(COUPURE_CONSO_CTPE2 * value)
{
  return  Rte_Read_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(value);
}
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_GetDefaultSubstValue(COUPURE_CONSO_CTPE2 * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2();
  return E_OK;
}
boolean PCOM_RX__COUPURE_CONSO_CTPE2_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2();
}
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_Set(const COUPURE_CONSO_CTPE2 * value)
{
  return  Rte_Write_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(*value);
}

Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_GetInitial(COUPURE_CONSO_CTP * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP();
  return E_OK;
}
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_GetPhysical(COUPURE_CONSO_CTP * value)
{
  return  Rte_Read_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP(value);
}
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_GetDefaultSubstValue(COUPURE_CONSO_CTP * value)
{
  *value =  Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP();
  return E_OK;
}
boolean PCOM_RX__COUPURE_CONSO_CTP_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP();
}
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_Set(const COUPURE_CONSO_CTP * value)
{
  return  Rte_Write_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(*value);
}

Std_ReturnType PCOM_RX__VCU_AccPedalPosition_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition();
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_AccPedalPosition_GetPhysical(VCU_AccPedalPosition * value)
{
  return  Rte_Read_PpVCU_AccPedalPosition_VCU_AccPedalPosition(value);
}
Std_ReturnType PCOM_RX__VCU_AccPedalPosition_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition();
  return E_OK;
}
boolean PCOM_RX__VCU_AccPedalPosition_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition();
}
boolean PCOM_RX__VCU_AccPedalPosition_StubForbidden(pcom_gen_t * value)/* PRQA S 3673 # Parameter not set as const for coherency with the other functions */
{
	  boolean result=FALSE;

	  if((*value>=(0x65UL))&&(*value<=(0xFEUL))){
		  result=TRUE;
	  }

	  return result;

}
Std_ReturnType PCOM_RX__VCU_AccPedalPosition_Set(const VCU_AccPedalPosition * value)
{
  return  Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(*value);
}
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_invalid_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition();
}
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_invalid_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition();
}
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_forbidden_timeConfirm(void)
{
  return  Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition();
}
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_forbidden_timeHealed(void)
{
  return  Rte_CData_CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition();
}


Std_ReturnType PCOM_RX__VCU_Keyposition_GetInitial(uint8 * value)
{
  *value = PCOM_VCU_Keyposition_Initial;
  return E_OK;
}
Std_ReturnType PCOM_RX__VCU_Keyposition_GetPhysical(VCU_Keyposition * value)
{
  return  Rte_Read_PpVCU_Keyposition_VCU_Keyposition(value);
}
Std_ReturnType PCOM_RX__VCU_Keyposition_GetDefaultSubstValue(uint8 * value)
{
  *value =  Rte_CData_CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition();
  return E_OK;
}
boolean PCOM_RX__VCU_Keyposition_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition();
}
Std_ReturnType PCOM_RX__VCU_Keyposition_Set(const VCU_Keyposition * value)
{
	PCOM_VCU_Keyposition_Initial = (uint8)(*value);
  return  Rte_Write_PpInt_VCU_Keyposition_VCU_Keyposition(*value);
}

Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_GetInitial(VCU_ElecMeterSaturation * value)
{
  *value =  Rte_CData_CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION();
  return E_OK;
}
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_GetPhysical(VCU_ElecMeterSaturation * value)
{
  return  Rte_Read_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation(value);
}
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_GetDefaultSubstValue(VCU_ElecMeterSaturation * value)
{
  *value =  Rte_CData_CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION();
  return E_OK;
}
boolean PCOM_RX__ELEC_METER_SATURATION_GetOutputSelector(void)
{
  return  Rte_CData_CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION();
}
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_Set(const VCU_ElecMeterSaturation * value)
{
  return  Rte_Write_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(*value);
}

Std_ReturnType PCOM_RX__DCHV_DCHVStatus_Set(const DCHV_DCHVStatus * value){
	return Rte_Write_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(*value);
}
Std_ReturnType PCOM_RX__DCHV_DCHVStatus_GetPhysical(DCHV_DCHVStatus * value){
	(void)Rte_Read_PpDCHV_DCHVStatus_DCHV_DCHVStatus(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCHV_ADC_VOUT_Set(const DCHV_ADC_VOUT * value){
	return Rte_Write_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(*value);
}
Std_ReturnType PCOM_RX__DCHV_ADC_VOUT_GetPhysical(DCHV_ADC_VOUT * value){
	 (void)Rte_Read_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCHV_ADC_IOUT_Set(const DCHV_ADC_IOUT * value){
	return Rte_Write_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(*value);
}
Std_ReturnType PCOM_RX__DCHV_ADC_IOUT_GetPhysical(DCHV_ADC_IOUT * value){
  (void)Rte_Read_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCHV_Command_Current_Reference_Set(const DCHV_Command_Current_Reference * value){
	return Rte_Write_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(*value);
}
Std_ReturnType PCOM_RX__DCHV_Command_Current_Reference_GetPhysical(DCHV_Command_Current_Reference * value){
  (void)Rte_Read_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference(value);
	return E_OK;
}


Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_6_Set(const DCHV_ADC_NTC_MOD_6 * value){
	return Rte_Write_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(*value);
}
Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_6_GetPhysical(DCHV_ADC_NTC_MOD_6 * value){
	 (void)Rte_Read_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_5_Set(const DCHV_ADC_NTC_MOD_5 * value){
	return Rte_Write_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(*value);
}
Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_5_GetPhysical(DCHV_ADC_NTC_MOD_5 * value){
	 (void)Rte_Read_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(value);
	return E_OK;
}


Std_ReturnType PCOM_RX__DCLV_DCLVStatus_Set(const DCLV_DCLVStatus * value){
	return Rte_Write_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(*value);
}
Std_ReturnType PCOM_RX__DCLV_DCLVStatus_GetPhysical(DCLV_DCLVStatus * value){
	 (void)Rte_Read_PpDCLV_DCLVStatus_DCLV_DCLVStatus(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_Measured_Voltage_Set(const DCLV_Measured_Voltage * value){
	return Rte_Write_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(*value);
}
Std_ReturnType PCOM_RX__DCLV_Measured_Voltage_GetPhysical(DCLV_Measured_Voltage * value){
	 (void)Rte_Read_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_Measured_Current_Set(const DCLV_Measured_Current * value){
	return Rte_Write_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(*value);
}
Std_ReturnType PCOM_RX__DCLV_Measured_Current_GetPhysical(DCLV_Measured_Current * value){
	 (void)Rte_Read_PpDCLV_Measured_Current_DCLV_Measured_Current(value);
	return E_OK;
}


Std_ReturnType PCOM_RX__DCLV_Power_Set(const DCLV_Power * value){
	return Rte_Write_PpInt_DCLV_Power_DCLV_Power(*value);
}
Std_ReturnType PCOM_RX__DCLV_Power_GetPhysical(DCLV_Power * value){
	 (void)Rte_Read_PpDCLV_Power_DCLV_Power(value);
	return E_OK;
}



Std_ReturnType PCOM_RX__DCLV_Input_Current_Set(const DCLV_Input_Current * value){
	return Rte_Write_PpInt_DCLV_Input_Current_DCLV_Input_Current(*value);
}
Std_ReturnType PCOM_RX__DCLV_Input_Current_GetPhysical(DCLV_Input_Current * value){
	 (void)Rte_Read_PpDCLV_Input_Current_DCLV_Input_Current(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_Input_Voltage_Set(const DCLV_Input_Voltage * value){
	return Rte_Write_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(*value);
}
Std_ReturnType PCOM_RX__DCLV_Input_Voltage_GetPhysical(DCLV_Input_Voltage * value){
	 (void)Rte_Read_PpDCLV_Input_Voltage_DCLV_Input_Voltage(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_Applied_Derating_Factor_Set(const DCLV_Applied_Derating_Factor * value){
	return Rte_Write_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(*value);
}
Std_ReturnType PCOM_RX__DCLV_Applied_Derating_Factor_GetPhysical(DCLV_Applied_Derating_Factor * value){
	 (void)Rte_Read_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(value);
	return E_OK;
}




Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_A_Set(const DCLV_T_SW_BUCK_A * value){
	return Rte_Write_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(*value);
}
Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_A_GetPhysical(DCLV_T_SW_BUCK_A * value){
	  (void)Rte_Read_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_B_Set(const DCLV_T_SW_BUCK_B * value){
	return Rte_Write_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(*value);
}
Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_B_GetPhysical(DCLV_T_SW_BUCK_B * value){
	  (void)Rte_Read_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_T_PP_A_Set(const DCLV_T_PP_A * value){
	return Rte_Write_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(*value);
}
Std_ReturnType PCOM_RX__DCLV_T_PP_A_GetPhysical(DCLV_T_PP_A * value){
	 (void)Rte_Read_PpDCLV_T_PP_A_DCLV_T_PP_A(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_T_PP_B_Set(const DCLV_T_PP_B * value){
	return Rte_Write_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(*value);
}
Std_ReturnType PCOM_RX__DCLV_T_PP_B_GetPhysical(DCLV_T_PP_B * value){
	 (void)Rte_Read_PpDCLV_T_PP_B_DCLV_T_PP_B(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_T_L_BUCK_Set(const DCLV_T_L_BUCK * value){
	return Rte_Write_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(*value);
}
Std_ReturnType PCOM_RX__DCLV_T_L_BUCK_GetPhysical(DCLV_T_L_BUCK * value){
	 (void)Rte_Read_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_T_TX_PP_Set(const DCLV_T_TX_PP * value){
	return Rte_Write_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(*value);
}
Std_ReturnType PCOM_RX__DCLV_T_TX_PP_GetPhysical(DCLV_T_TX_PP * value){
	 (void)Rte_Read_PpDCLV_T_TX_PP_DCLV_T_TX_PP(value);
	return E_OK;
}



Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_IOUT_Set(const DCHV_IOM_ERR_OC_IOUT * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_IOUT_GetPhysical(DCHV_IOM_ERR_OC_IOUT * value){
	  (void)Rte_Read_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OV_VOUT_Set(const DCHV_IOM_ERR_OV_VOUT * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OV_VOUT_GetPhysical(DCHV_IOM_ERR_OV_VOUT * value){
	 (void)Rte_Read_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_Set(const DCHV_IOM_ERR_OT * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_GetPhysical(DCHV_IOM_ERR_OT * value){
	  (void)Rte_Read_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD5_Set(const DCHV_IOM_ERR_OT_NTC_MOD5 * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD5_GetPhysical(DCHV_IOM_ERR_OT_NTC_MOD5 * value){
	 (void)Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD6_Set(const DCHV_IOM_ERR_OT_NTC_MOD6 * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD6_GetPhysical(DCHV_IOM_ERR_OT_NTC_MOD6 * value){
	 (void)Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_UV_12V_Set(const DCHV_IOM_ERR_UV_12V * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_UV_12V_GetPhysical(DCHV_IOM_ERR_UV_12V * value){
	  (void)Rte_Read_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_POS_Set(const DCHV_IOM_ERR_OC_POS * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_POS_GetPhysical(DCHV_IOM_ERR_OC_POS * value){
	 (void)Rte_Read_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_NEG_Set(const DCHV_IOM_ERR_OC_NEG * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_NEG_GetPhysical(DCHV_IOM_ERR_OC_NEG * value){
	  (void)Rte_Read_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_H_Set(const DCHV_IOM_ERR_CAP_FAIL_H * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_H_GetPhysical(DCHV_IOM_ERR_CAP_FAIL_H * value){
	  (void)Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_L_Set(const DCHV_IOM_ERR_CAP_FAIL_L * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_L_GetPhysical(DCHV_IOM_ERR_CAP_FAIL_L * value){
	 (void)Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_IN_Set(const DCHV_IOM_ERR_OT_IN * value){
	return Rte_Write_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(*value);
}
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_IN_GetPhysical(DCHV_IOM_ERR_OT_IN * value){
	  (void)Rte_Read_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_PFCStatus_Set(const PFC_PFCStatus * value){
	return Rte_Write_PpInt_PFC_PFCStatus_PFC_PFCStatus(*value);
}
Std_ReturnType PCOM_RX__PFC_PFCStatus_GetPhysical(PFC_PFCStatus * value){
	 (void)Rte_Read_PpPFC_PFCStatus_PFC_PFCStatus(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_Vdclink_V_Set(const PFC_Vdclink_V * value){
	return Rte_Write_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(*value);
}
Std_ReturnType PCOM_RX__PFC_Vdclink_V_GetPhysical(PFC_Vdclink_V * value){
	 (void)Rte_Read_PpPFC_Vdclink_V_PFC_Vdclink_V(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_Temp_M1_C_Set(const PFC_Temp_M1_C * value){
	return Rte_Write_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(*value);
}
Std_ReturnType PCOM_RX__PFC_Temp_M1_C_GetPhysical(PFC_Temp_M1_C * value){
	 (void)Rte_Read_PpPFC_Temp_M1_C_PFC_Temp_M1_C(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_Temp_M3_C_Set(const PFC_Temp_M3_C * value){
	return Rte_Write_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(*value);
}
Std_ReturnType PCOM_RX__PFC_Temp_M3_C_GetPhysical(PFC_Temp_M3_C * value){
	 (void)Rte_Read_PpPFC_Temp_M3_C_PFC_Temp_M3_C(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_Temp_M4_C_Set(const PFC_Temp_M4_C * value){
	return Rte_Write_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(*value);
}
Std_ReturnType PCOM_RX__PFC_Temp_M4_C_GetPhysical(PFC_Temp_M4_C * value){
	 (void)Rte_Read_PpPFC_Temp_M4_C_PFC_Temp_M4_C(value);
	return E_OK;
}


Std_ReturnType PCOM_RX__PFC_VPH12_Peak_V_Set(const PFC_VPH12_Peak_V * value){
	return Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH12_Peak_V_GetPhysical(PFC_VPH12_Peak_V * value){
	  (void)Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_VPH23_Peak_V_Set(const PFC_VPH23_Peak_V * value){
	return Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH23_Peak_V_GetPhysical(PFC_VPH23_Peak_V * value){
	 (void)Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_VPH31_Peak_V_Set(const PFC_VPH31_Peak_V * value){
	return Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH31_Peak_V_GetPhysical(PFC_VPH31_Peak_V * value){
	  (void)Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_VPH12_RMS_V_Set(const PFC_VPH12_RMS_V * value){
	return Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH12_RMS_V_GetPhysical(PFC_VPH12_RMS_V * value){
	 (void)Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_VPH23_RMS_V_Set(const PFC_VPH23_RMS_V * value){
	return Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH23_RMS_V_GetPhysical(PFC_VPH23_RMS_V * value){
	  (void)Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_VPH31_RMS_V_Set(const PFC_VPH31_RMS_V * value){
	return Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH31_RMS_V_GetPhysical(PFC_VPH31_RMS_V * value){
 (void)Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IPH1_RMS_0A1_Set(const PFC_IPH1_RMS_0A1 * value){
	return Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(*value);
}
Std_ReturnType PCOM_RX__PFC_IPH1_RMS_0A1_GetPhysical(PFC_IPH1_RMS_0A1 * value){
	  (void)Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IPH2_RMS_0A1_Set(const PFC_IPH2_RMS_0A1 * value){
	return Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(*value);
}
Std_ReturnType PCOM_RX__PFC_IPH2_RMS_0A1_GetPhysical(PFC_IPH2_RMS_0A1 * value){
 (void)Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IPH3_RMS_0A1_Set(const PFC_IPH3_RMS_0A1 * value){
	return Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(*value);
}
Std_ReturnType PCOM_RX__PFC_IPH3_RMS_0A1_GetPhysical(PFC_IPH3_RMS_0A1 * value){
	  (void)Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_VPH1_Freq_Hz_Set(const PFC_VPH1_Freq_Hz * value){
	return Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH1_Freq_Hz_GetPhysical(PFC_VPH1_Freq_Hz * value){
	 (void)Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_VPH2_Freq_Hz_Set(const PFC_VPH2_Freq_Hz * value){
	return Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH2_Freq_Hz_GetPhysical(PFC_VPH2_Freq_Hz * value){
	 (void)Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(value);
	return E_OK;
}
Std_ReturnType PCOM_RX__PFC_VPH3_Freq_Hz_Set(const PFC_VPH3_Freq_Hz * value){
	return Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(*value);
}
Std_ReturnType PCOM_RX__PFC_VPH3_Freq_Hz_GetPhysical(PFC_VPH3_Freq_Hz * value){
	  (void)Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(value);
	return E_OK;
}

Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO7_Set(const PFC_IOM_ERR_UVLO_ISO7 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO7_GetPhysical(PFC_IOM_ERR_UVLO_ISO7 * value){
  (void)Rte_Read_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UV_12V_Set(const PFC_IOM_ERR_UV_12V * value){
return Rte_Write_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UV_12V_GetPhysical(PFC_IOM_ERR_UV_12V * value){
 (void)Rte_Read_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO4_Set(const PFC_IOM_ERR_UVLO_ISO4 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO4_GetPhysical(PFC_IOM_ERR_UVLO_ISO4 * value){
  (void)Rte_Read_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M4_Set(const PFC_IOM_ERR_OT_NTC1_M4 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M4_GetPhysical(PFC_IOM_ERR_OT_NTC1_M4 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M3_Set(const PFC_IOM_ERR_OT_NTC1_M3 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M3_GetPhysical(PFC_IOM_ERR_OT_NTC1_M3 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M1_Set(const PFC_IOM_ERR_OT_NTC1_M1 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M1_GetPhysical(PFC_IOM_ERR_OT_NTC1_M1 * value){
  (void)Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_DCLINK_Set(const PFC_IOM_ERR_OV_DCLINK * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_DCLINK_GetPhysical(PFC_IOM_ERR_OV_DCLINK * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH12_Set(const PFC_IOM_ERR_OV_VPH12 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH12_GetPhysical(PFC_IOM_ERR_OV_VPH12 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH31_Set(const PFC_IOM_ERR_OV_VPH31 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH31_GetPhysical(PFC_IOM_ERR_OV_VPH31 * value){
  (void)Rte_Read_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH23_Set(const PFC_IOM_ERR_OV_VPH23 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH23_GetPhysical(PFC_IOM_ERR_OV_VPH23 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH3_Set(const PFC_IOM_ERR_OC_PH3 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH3_GetPhysical(PFC_IOM_ERR_OC_PH3 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH2_Set(const PFC_IOM_ERR_OC_PH2 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH2_GetPhysical(PFC_IOM_ERR_OC_PH2 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH1_Set(const PFC_IOM_ERR_OC_PH1 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH1_GetPhysical(PFC_IOM_ERR_OC_PH1 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT1_Set(const PFC_IOM_ERR_OC_SHUNT1 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT1_GetPhysical(PFC_IOM_ERR_OC_SHUNT1 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT2_Set(const PFC_IOM_ERR_OC_SHUNT2 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT2_GetPhysical(PFC_IOM_ERR_OC_SHUNT2 * value){
 (void)Rte_Read_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(value);
return E_OK;
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT3_Set(const PFC_IOM_ERR_OC_SHUNT3 * value){
return Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(*value);
}
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT3_GetPhysical(PFC_IOM_ERR_OC_SHUNT3 * value){
  (void)Rte_Read_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(value);
return E_OK;
}


Std_ReturnType PCOM_RX__DCLV_PWM_STOP_Set(const DCLV_PWM_STOP * value){
return Rte_Write_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(*value);
}
Std_ReturnType PCOM_RX__DCLV_PWM_STOP_GetPhysical(DCLV_PWM_STOP * value){
  (void)Rte_Read_PpDCLV_PWM_STOP_DCLV_PWM_STOP(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OT_FAULT_Set(const DCLV_OT_FAULT * value){
return Rte_Write_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OT_FAULT_GetPhysical(DCLV_OT_FAULT * value){
  (void)Rte_Read_PpDCLV_OT_FAULT_DCLV_OT_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OC_A_HV_FAULT_Set(const DCLV_OC_A_HV_FAULT * value){
return Rte_Write_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OC_A_HV_FAULT_GetPhysical(DCLV_OC_A_HV_FAULT * value){
  (void)Rte_Read_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OC_B_HV_FAULT_Set(const DCLV_OC_B_HV_FAULT * value){
return Rte_Write_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OC_B_HV_FAULT_GetPhysical(DCLV_OC_B_HV_FAULT * value){
  (void)Rte_Read_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OV_INT_A_FAULT_Set(const DCLV_OV_INT_A_FAULT * value){
return Rte_Write_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OV_INT_A_FAULT_GetPhysical(DCLV_OV_INT_A_FAULT * value){
  (void)Rte_Read_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OV_INT_B_FAULT_Set(const DCLV_OV_INT_B_FAULT * value){
return Rte_Write_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OV_INT_B_FAULT_GetPhysical(DCLV_OV_INT_B_FAULT * value){
  (void)Rte_Read_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OV_UV_HV_FAULT_Set(const DCLV_OV_UV_HV_FAULT * value){
return Rte_Write_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OV_UV_HV_FAULT_GetPhysical(DCLV_OV_UV_HV_FAULT * value){
  (void)Rte_Read_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OC_A_LV_FAULT_Set(const DCLV_OC_A_LV_FAULT * value){
return Rte_Write_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OC_A_LV_FAULT_GetPhysical(DCLV_OC_A_LV_FAULT * value){
  (void)Rte_Read_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(value);
return E_OK;
}

Std_ReturnType PCOM_RX__DCLV_OC_B_LV_FAULT_Set(const DCLV_OC_B_LV_FAULT * value){
return Rte_Write_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(*value);
}
Std_ReturnType PCOM_RX__DCLV_OC_B_LV_FAULT_GetPhysical(DCLV_OC_B_LV_FAULT * value){
  (void)Rte_Read_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(value);
return E_OK;
}


/* EOF */
