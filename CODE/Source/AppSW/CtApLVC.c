/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLVC.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApLVC
 *  Generation Time:  2020-11-23 11:42:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLVC>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup LVC
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Low Voltage converter control.
 * \details This module implements a ...
 *
 * \{
 * \file  LVC.c
 * \brief  Generic code of the LVC module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApLVC.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "AppExternals.h"
#include "WdgM.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/**20ms delay*/
#define LVC_DELAY_20MS			2U

/** Minimal time in Post state*/
#define LVC_POST_MIN_TIME	10U

#define LVC_SEG_TO_TASKTICK(IN)		((uint16)(IN)*(uint16)100U) /* PRQA S 3453 #Used in that way for better understanding*/

/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/


/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
typedef enum{
	LVC_STATE_POST = 0U,
	LVC_STATE_STOP,
	LVC_STATE_CONVERSION,
	LVC_STATE_DEGRADATION,
	LVC_STATE_INIT,
	LVC_STATE_STANDBY,
	LVC_STATE_POWER_OFF,
	LVC_STATE_FAIL
}LVC_state_t;


/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** LCV state.*/
static LVC_state_t LVC_state;
/** DCDC reported state.*/
static DCDC_Status LVC_reportState;

/** OutputSupFaultToDCLV */
static boolean LVC_OutputSupFaultToDCLV_Value;

/** Current POST polarity*/
static boolean LVC_PostPolarity;

/** POST has been done OK*/
static DCDC_HighVoltConnectionAllowed LVC_Post_OK;

#define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/**
 * \brief Signal measurement report.
 * \details This function reports the DCLV measurements.
 */
static void LVC_SignalReport(void);
/**
 * \brief DCLV state Machine.
 * \details This function represents the state machine that controles the DCLV.
 */
static void LVC_StateMachine(void);

/** Report Post state to RTE.*/
static void LVC_ReportPostState(void);


/** DCLV state Machine: Power off state.*/
static void LVC_State_PowerOFF(void);
/** DCLV state Machine: Post state.*/
static void LVC_State_Post(void);
/** DCLV state Machine: Stop state.*/
static void LVC_State_Stop(void);
/** DCLV state Machine: Conversion state.*/
static void LVC_State_Conversion(VCU_DCDCActivation DCDCactive, boolean DCLVerror);
/** DCLV state Machine: Conversion state.*/
static void LVC_State_Degradation(VCU_DCDCActivation DCDCactive, boolean DCLVerror);
/** DCLV state Machine: Standby state.*/
static void LVC_State_Standby(VCU_DCDCActivation DCDCactive, boolean DCLVerror);
/** DCLV state Machine: Initialization state.*/
static void LVC_State_Init(VCU_DCDCActivation DCDCactive, boolean DCLVerror);
/** DCLV state Machine: Write outputs.*/
static void LVC_StateMachine_Out(boolean DCLVerror);
/** Reported state in standby*/
static DCDC_Status LCV_StandbyStateReport(boolean DCLVerror);
/** Write Outputs sub-function to reduce halstead*/
static void LVC_SetOutputs(boolean ForceFault, SUP_RequestDCLVStatus DCLVstate,
													 DCLV_VoltageReference Vref, DCDC_HighVoltConnectionAllowed ConnectionAllowed,
														DCDC_Status DCLVreportState, boolean VCC_DCLV);
/** Publish DTC triggers */
static void LVC_DTCTriggerReport(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCDC_HighVoltConnectionAllowed: Boolean
 * DCDC_InputCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Applied_Derating_Factor: Integer in interval [0...2047]
 * DCLV_Input_Current: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Measured_Voltage: Integer in interval [0...175]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCLV_VoltageReference: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * IdtDCLVTimeConfirmDerating: Integer in interval [0...60]
 *   Unit: [s], Factor: 1, Offset: 0
 * VCU_DCDCVoltageReq: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * SUP_RequestDCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCLV_STATE_STANDBY (0U)
 *   Cx1_DCLV_STATE_RUN (1U)
 *   Cx2_DCLV_STATE_RESET_ERROR (2U)
 *   Cx3_DCLV_STATE_DISCHARGE (3U)
 *   Cx4_DCLV_STATE_SHUTDOWN (4U)
 * VCU_DCDCActivation: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Not_used (0U)
 *   Cx1_DCDC_Off (1U)
 *   Cx2_DCDC_On (2U)
 *   Cx3_Unavailable_Value (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   boolean *Rte_Pim_PimDCLVShutdownPathFSP(void)
 *   boolean *Rte_Pim_PimDCLVShutdownPathGPIO(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   uint16 Rte_CData_CalDCLVDeratingThreshold(void)
 *   IdtDCLVTimeConfirmDerating Rte_CData_CalDCLVTimeConfirmDerating(void)
 *
 *********************************************************************************************************************/


#define CtApLVC_START_SEC_CODE
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLVC_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_init_doc
 *********************************************************************************************************************/

/*!
 * \brief LVC initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLVC_CODE) RCtApLVC_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_init
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	 LVC_state = LVC_STATE_POST;
	 LVC_reportState = Cx0_off_mode;
	 LVC_PostPolarity = TRUE;
	 LVC_Post_OK = FALSE;
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLVC_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_Delayed_DeOutputSupFaultToDCLVCPhysicalValue_Delayed(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLVC_CODE) RCtApLVC_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msA
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	/*This value has been calculated during task B, that is executed after the task A. Thus, this value is 10ms delayed.*/
	(void)Rte_Write_PpInt_DCDC_Status_Delayed_DCDC_Status(LVC_reportState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_Delayed_DCDC_HighVoltConnectionAllowed(LVC_Post_OK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLVC_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCLVError_DeDCLVError(boolean *data)
 *   Std_ReturnType Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean *data)
 *   Std_ReturnType Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result data)
 *   Std_ReturnType Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(boolean data)
 *   Std_ReturnType Rte_Write_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(DCDC_InputCurrent data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Status_DCDC_Status(DCDC_Status data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data)
 *   Std_ReturnType Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msB_doc
 *********************************************************************************************************************/

/*!
 * \brief LVC task.
 *
 * This function is called periodically by the scheduler. This function should
 * Manage the Low voltage converter state.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLVC_CODE) RCtApLVC_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLVC_task10msB
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */

	LVC_DTCTriggerReport();

	LVC_SignalReport();
	LVC_StateMachine();

	LVC_ReportPostState();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLVC_STOP_SEC_CODE
#include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/

static void LVC_ReportPostState(void)
{
	IdtPOST_Result PostResult;

	if(LVC_STATE_FAIL == LVC_state)
	{
		PostResult = POST_NOK;
	}
	else if((LVC_STATE_POST == LVC_state)
			||(LVC_STATE_POWER_OFF == LVC_state))
	{
		PostResult = POST_ONGOING;
	}
	else
	{
		PostResult = POST_OK;
	}

	(void)Rte_Write_PpDCDC_POST_Result_DeDCDC_POST_Result(PostResult);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void LVC_SignalReport(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCLV_Input_Current Input_Current;

	(void)Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(&Input_Current);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_InputCurrent_DCDC_InputCurrent (Input_Current);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void LVC_StateMachine(void)
{
    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	VCU_DCDCActivation DCDCactive;
	boolean DCLVerror;

	(void)Rte_Read_PpDCLVError_DeDCLVError(&DCLVerror);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(&DCDCactive);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	switch(LVC_state){

		case LVC_STATE_POWER_OFF:
			LVC_State_PowerOFF();
			break;

		case LVC_STATE_POST:
			LVC_State_Post();
			break;

		case LVC_STATE_STANDBY:
			LVC_State_Standby(DCDCactive, DCLVerror);
			break;

		case LVC_STATE_INIT:
			LVC_State_Init(DCDCactive, DCLVerror);
			break;

		case LVC_STATE_CONVERSION:
			LVC_State_Conversion(DCDCactive,DCLVerror);
			break;

		case LVC_STATE_DEGRADATION:
			LVC_State_Degradation(DCDCactive,DCLVerror);
			break;

		case LVC_STATE_STOP:
			LVC_State_Stop();
			break;

		default:
		case LVC_STATE_FAIL:
			/*Don't leave fail state*/
			LVC_state = LVC_STATE_FAIL;
			break;
	}

	LVC_StateMachine_Out(DCLVerror);

}
static void LVC_State_PowerOFF(void)
{

  /* - Static non-init variables declaration ---------- */
  #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
  #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static non-init variables HERE... */

  #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
  #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Static zero-init variables declaration --------- */
  #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
  #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 MinTime = 0U;
	static uint8 RetryNum = 0U;

  #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
  #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Static init variables declaration -------------- */
  #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
  #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

  /* Static init variables HERE... */

  #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
  #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


  /* - Automatic variables declaration ---------------- */
	uint16 OffTime;


	OffTime = 70U + (20U*(uint16)RetryNum);

	if(5U<=RetryNum)
	{
		/*Max number of retry reached, don't try again*/
		LVC_state = LVC_STATE_FAIL;
	}
	else if(OffTime<=MinTime)
	{
		/*Min time reached, try post*/
		RetryNum++;
		MinTime = 0U;
		LVC_state = LVC_STATE_POST;
	}
	else
	{
		MinTime++;
	}
}
static void LVC_State_Post(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 Debounce = 0U;
	static uint8 TimeOut = 0U;

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCLV_DCLVStatus DCLVstate;
	boolean Fault_feedback;

	(void)Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&DCLVstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(&Fault_feedback);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	TimeOut++;

	if(100U<TimeOut)
	{
		/*TimeOut reached, restart the sequence*/
		TimeOut = 0U;
		Debounce = 0U;
		LVC_state = LVC_STATE_POWER_OFF;
		LVC_PostPolarity = TRUE;
	}
	else if(((Cx0_STANDBY!=DCLVstate)&&(Cx3_ALARM!=DCLVstate))
					||(LVC_PostPolarity!=Fault_feedback))
	{
		Debounce = 0U;
	}
	else if(LVC_POST_MIN_TIME>Debounce)
	{
		Debounce++;
	}
	else if(FALSE!=LVC_PostPolarity)
	{
		/*First polarity OK*/
		Debounce=0U;
		LVC_PostPolarity = FALSE;
	}
	else
	{
		/*Full Post OK*/
		LVC_state = LVC_STATE_STANDBY;
		Debounce = 0U;
	}


}
static void LVC_State_Standby(VCU_DCDCActivation DCDCactive, boolean DCLVerror)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	if((Cx2_DCDC_On==DCDCactive)&&(FALSE==DCLVerror))
	{
		LVC_state = LVC_STATE_INIT;
	}
}

static void LVC_State_Init(VCU_DCDCActivation DCDCactive, boolean DCLVerror)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCLV_DCLVStatus DCLV_state;


	(void)Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&DCLV_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx2_DCDC_On!=DCDCactive)||(FALSE!=DCLVerror))
	{
		LVC_state = LVC_STATE_STOP;
	}
	else if(Cx0_STANDBY==DCLV_state)
	{
		LVC_state = LVC_STATE_CONVERSION;
	}
	else
	{
		/*Misra*/
	}

}
static void LVC_State_Conversion(VCU_DCDCActivation DCDCactive, boolean DCLVerror)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 DebounceCounter = 0U;

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCLV_Applied_Derating_Factor Derating;
	boolean ImplausibilityTempPushPull;
	boolean ImplausibilityTempBuck;
	uint16 DebounceTime = LVC_SEG_TO_TASKTICK(Rte_CData_CalDCLVTimeConfirmDerating());

	(void)Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(&Derating);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(&ImplausibilityTempPushPull);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(&ImplausibilityTempBuck);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx2_DCDC_On!=DCDCactive)||(FALSE!=DCLVerror))
	{
		LVC_state = LVC_STATE_STOP;
		DebounceCounter = 0U;
	}
	else if((FALSE!=ImplausibilityTempPushPull)||(FALSE!=ImplausibilityTempBuck))
	{
		LVC_state = LVC_STATE_DEGRADATION;
		DebounceCounter = 0U;
	}
	else if(Rte_CData_CalDCLVDeratingThreshold()<=Derating)
	{
		DebounceCounter = 0U;
	}
	else if(DebounceTime>DebounceCounter)
	{
		DebounceCounter++;
	}
	else
	{
		LVC_state = LVC_STATE_DEGRADATION;
		DebounceCounter = 0U;
	}
}
static void LVC_State_Degradation(VCU_DCDCActivation DCDCactive, boolean DCLVerror)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 DebounceCounter = 0U;

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCLV_Applied_Derating_Factor Derating;
	boolean ImplausibilityTempPushPull;
	boolean ImplausibilityTempBuck;
	uint16 DebounceTime = LVC_SEG_TO_TASKTICK(Rte_CData_CalDCLVTimeConfirmDerating());


	(void)Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(&Derating);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(&ImplausibilityTempPushPull);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpImplausibilityTempBuck_DeImplausibilityTempBuck(&ImplausibilityTempBuck);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((Cx2_DCDC_On!=DCDCactive)||(FALSE!=DCLVerror))
	{
		LVC_state = LVC_STATE_STOP;
		DebounceCounter = 0U;
	}
	else if(Rte_CData_CalDCLVDeratingThreshold()>Derating)
	{
		DebounceCounter = 0U;
	}
	else if(DebounceTime>DebounceCounter)
	{
		DebounceCounter++;
	}
	else if((FALSE==ImplausibilityTempPushPull)&&(FALSE==ImplausibilityTempBuck))
	{
		LVC_state = LVC_STATE_CONVERSION;
		DebounceCounter = 0U;
	}
	else
	{
		/*Temperature Implausibility, stay in degradation*/
	}
}
static void LVC_State_Stop(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/*The standby state output is executed 10ms before this code. */
	static uint8 delay = 1U;

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	if(LVC_DELAY_20MS>delay)
	{
		delay++;
	}
	else
	{
		delay = 1U;
		LVC_state = LVC_STATE_STANDBY;
	}
}

static void LVC_StateMachine_Out(boolean DCLVerror)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */

	VCU_DCDCVoltageReq Vrequest;


	(void)Rte_Read_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(&Vrequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Use a function to set values for reduce the halstead*/
	switch(LVC_state){

		case LVC_STATE_POWER_OFF:
			LVC_SetOutputs(TRUE,Cx0_DCLV_STATE_STANDBY,0U,FALSE,Cx1_Init_mode,FALSE);
			break;

		case LVC_STATE_STANDBY:
			LVC_SetOutputs(TRUE,Cx2_DCLV_STATE_RESET_ERROR,0U,TRUE,LCV_StandbyStateReport(DCLVerror),TRUE);
			break;

		case LVC_STATE_INIT:
			LVC_SetOutputs(FALSE,Cx0_DCLV_STATE_STANDBY,Vrequest,TRUE,Cx3_conversion_working_,TRUE);
			break;

		case LVC_STATE_CONVERSION:
			LVC_SetOutputs(FALSE,Cx1_DCLV_STATE_RUN,Vrequest,TRUE,Cx3_conversion_working_,TRUE);
			break;

		case LVC_STATE_DEGRADATION:
			LVC_SetOutputs(FALSE,Cx1_DCLV_STATE_RUN,Vrequest,TRUE,Cx5_degradation_mode,TRUE);
			break;

		case LVC_STATE_STOP:
			/*Don't update the DCLV state*/
			LVC_SetOutputs(FALSE,Cx0_DCLV_STATE_STANDBY,0U,TRUE,LVC_reportState,TRUE);
			break;

		case LVC_STATE_POST:
			LVC_SetOutputs(LVC_PostPolarity,Cx2_DCLV_STATE_RESET_ERROR,0U,FALSE,Cx1_Init_mode,TRUE);
			break;

		default:
		case LVC_STATE_FAIL:
			LVC_SetOutputs(TRUE,Cx0_DCLV_STATE_STANDBY,0U,FALSE,Cx4_error_mode,TRUE);
			break;


	}
}
static DCDC_Status LCV_StandbyStateReport(boolean DCLVerror)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCDC_Status DCLVreportState;

	if(FALSE==DCLVerror)
	{
		DCLVreportState = Cx2_standby_mode;
	}
	else
	{
		DCLVreportState = Cx4_error_mode;
	}

	return DCLVreportState;
}
static void LVC_SetOutputs(boolean ForceFault, SUP_RequestDCLVStatus DCLVstate,
													 DCLV_VoltageReference Vref, DCDC_HighVoltConnectionAllowed ConnectionAllowed,
													DCDC_Status DCLVreportState, boolean VCC_DCLV)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApLVC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApLVC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApLVC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApLVC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApLVC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	LVC_reportState=DCLVreportState;
	LVC_Post_OK = ConnectionAllowed;

	(void)Rte_Write_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(ConnectionAllowed);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_Status_DCDC_Status(DCLVreportState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(Vref);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(DCLVstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(ForceFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(VCC_DCLV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	LVC_OutputSupFaultToDCLV_Value = ForceFault;
}

static void LVC_DTCTriggerReport(void)
{
	boolean faultData;
	boolean Fault_feedback;

	(void)Rte_Read_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(&Fault_feedback);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	/* VCC DCLV is a constant value */

	if ((FALSE == Fault_feedback) && (FALSE != LVC_OutputSupFaultToDCLV_Value))
	{
		faultData = TRUE;
	}
	else
	{
		faultData = FALSE;
	}
	(void)Rte_Write_PpDCLV_ShutdownPathFault_DeDCLV_ShutdownPathFault(faultData);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
