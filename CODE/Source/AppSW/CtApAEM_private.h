/*
 * File: CtApAEM_private.h
 *
 * Code generated for Simulink model 'CtApAEM'.
 *
 * Model version                  : 1.15
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Mon Nov 11 10:01:38 2019
 *
 * Target selection: autosar.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_CtApAEM_private_h_
#define RTW_HEADER_CtApAEM_private_h_
#include "rtwtypes.h"
#include "CtApAEM.h"
#ifndef UCHAR_MAX
#include <limits.h>
#endif

#if ( UCHAR_MAX != (0xFFU) ) || ( SCHAR_MAX != (0x7F) )
#error Code was generated for compiler with different sized uchar/char. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( USHRT_MAX != (0xFFFFU) ) || ( SHRT_MAX != (0x7FFF) )
#error Code was generated for compiler with different sized ushort/short. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( UINT_MAX != (0xFFFFFFFFU) ) || ( INT_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized uint/int. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

#if ( ULONG_MAX != (0xFFFFFFFFU) ) || ( LONG_MAX != (0x7FFFFFFF) )
#error Code was generated for compiler with different sized ulong/long. \
Consider adjusting Test hardware word size settings on the \
Hardware Implementation pane to match your compiler word sizes as \
defined in limits.h of the compiler. Alternatively, you can \
select the Test hardware is the same as production hardware option and \
select the Enable portable word sizes option on the Code Generation > \
Verification pane for ERT based targets, which will disable the \
preprocessor word size checks.
#endif

extern void CtApAEM_rising_edge_Reset(DW_rising_edge_CtApAEM_T *localDW);
extern void CtApAEM_rising_edge(boolean_T rtu_Signal, B_rising_edge_CtApAEM_T
  *localB, DW_rising_edge_CtApAEM_T *localDW);
extern void CtApAEM_TurnOnDelay1_Init(DW_TurnOnDelay1_CtApAEM_T *localDW);
extern void CtApAEM_TurnOnDelay1_Reset(DW_TurnOnDelay1_CtApAEM_T *localDW);
extern void CtApAEM_TurnOnDelay1(boolean_T rtu_In, real_T rtu_Tdly, real_T
  rtu_Te, B_TurnOnDelay1_CtApAEM_T *localB, DW_TurnOnDelay1_CtApAEM_T *localDW);
extern void CtApAEM_DetectSat(real_T rtu_SatMax, real_T rtu_In, real_T
  rtu_SatMin, B_DetectSat_CtApAEM_T *localB);
extern void CtApAEM_falling_edge1_Reset(DW_falling_edge1_CtApAEM_T *localDW);
extern void CtApAEM_falling_edge1(boolean_T rtu_Signal,
  B_falling_edge1_CtApAEM_T *localB, DW_falling_edge1_CtApAEM_T *localDW);
extern void CtApAEM_uBitDecoder(int32_T rtu_U8in, B_uBitDecoder_CtApAEM_T
  *localB);
extern void CtApAEM_uBitEncoder(boolean_T rtu_Bit7, boolean_T rtu_Bit6,
  boolean_T rtu_Bit5, boolean_T rtu_Bit4, boolean_T rtu_Bit3, boolean_T rtu_Bit2,
  boolean_T rtu_Bit1, boolean_T rtu_Bit0, B_uBitEncoder_CtApAEM_T *localB);
extern void Gerer_etat_Reveil_partiel_maitr(boolean_T rtu_UCE_bMstPtlWkuYjNd,
  boolean_T rtu_UCE_bMstPtlWkuYjHldReq, boolean_T rtu_UCE_bNomMainWkuAcv,
  int32_T rtu_UCE_stRCDSt, real_T rtu_UCE_tiMinTiMstPtlWkuYj, real_T
  rtu_UCE_tiMaxTiMstPtlWkuYj, B_Gerer_etat_Reveil_partiel_m_T *localB,
  DW_Gerer_etat_Reveil_partiel__T *localDW);
extern void CtApAEM_u6BitDecoder(int32_T rtu_U16in, B_u6BitDecoder_CtApAEM_T
  *localB);
extern void CtApAEM_u6BitEncoder(boolean_T rtu_Bit15, boolean_T rtu_Bit14,
  boolean_T rtu_Bit13, boolean_T rtu_Bit12, boolean_T rtu_Bit11, boolean_T
  rtu_Bit10, boolean_T rtu_Bit9, boolean_T rtu_Bit8, boolean_T rtu_Bit7,
  boolean_T rtu_Bit6, boolean_T rtu_Bit5, boolean_T rtu_Bit4, boolean_T rtu_Bit3,
  boolean_T rtu_Bit2, boolean_T rtu_Bit1, boolean_T rtu_Bit0,
  B_u6BitEncoder_CtApAEM_T *localB);
extern void Gerer_etat_Reveil_partie_b_Init(DW_Gerer_etat_Reveil_partie_i_T
  *localDW);
extern void Gerer_etat_Reveil_partiel_escla(boolean_T rtu_UCE_bSlavePtlWkuXiReq,
  boolean_T rtu_Ext_bRCDLine, int32_T rtu_UCE_stRCDSt, boolean_T
  rtu_UCE_bNomMainWkuAcv, boolean_T rtu_UCE_bSlavePtlWkuXiAcvMod, real_T
  rtu_UCE_tiPtlWkuXiAcv, real_T rtu_UCE_tiPtlWkuXiDeac, real_T
  rtu_UCE_tiPtlWkuXiLock, B_Gerer_etat_Reveil_partiel_e_T *localB,
  DW_Gerer_etat_Reveil_partie_i_T *localDW);

#endif                                 /* RTW_HEADER_CtApAEM_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
