/*
 * PCOM_cfg.h
 *
 *  Created on: 22 may. 2019
 *      Author: M0058239
 */

#ifndef PCOM_CFG_H
#define PCOM_CFG_H


#include "Platform_types.h"
#include "Compiler.h"
#include "Rte_CtApPCOM.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */


#define		PCOM_NUM_TX_FRAMES		12U
#define		PCOM_NUM_RX_FRAMES		30U

#define		PCOM_NUM_TX_SIGNALS		87U
#define		PCOM_NUM_RX_SIGNALS		59U

#define PCOM_TX_RESERVED_NA			(0U)
#define PCOM_SIGNAL_LIMITS_NA			((uint32)0)
#define PCOM_TX_INVALID_NA			(0U)

/**
 *  Sending frames timeouts
 */
#define PCOM_TX_SEND_FRAME_LEVEL		(0U)

#define PCOM_TX_FRAMES_DC1_TIMEOUT		(15U) 	/* 75 ms */
#define PCOM_TX_FRAMES_DC2_TIMEOUT		(0U)	/* Always */
#define PCOM_TX_FRAMES_OBC1_TIMEOUT		(15U) 	/* 75 ms */
#define PCOM_TX_FRAMES_OBC2_TIMEOUT 		(15U) 	/* 75 ms */
#define PCOM_TX_FRAMES_OBC3_TIMEOUT		(6U) 	/* 30 ms */
#define PCOM_TX_FRAMES_OBC4_TIMEOUT 		(194U) 	/* 970 ms */
#define PCOM_TX_FRAMES_SUPV_V2_OBC_TIMEOUT	(194U)  /* 970 ms */
#define PCOM_TX_FRAMES_NA_TIMEOUT		(0U)	/* NOT APPLY */


#define PCOM_TX_FRAMES_DC1_345_SIGNALS_COUNT 			(8U)
#define PCOM_TX_FRAMES_DC2_0C5_SIGNALS_COUNT 			(6U)
#define PCOM_TX_FRAMES_VERS_OBC_DCDC_0CE_COUNT 			(8U)
#define PCOM_TX_FRAMES_NEW_JDD_OBC_DCDC_5B1_SIGNALS_COUNT	(8U)
#define PCOM_TX_FRAMES_OBC1_3A3_SIGNALS_COUNT 			(9U)
#define PCOM_TX_FRAMES_OBC2_3A2_SIGNALS_COUNT 			(11U)
#define PCOM_TX_FRAMES_OBC3_230_SIGNALS_COUNT 			(4U)
#define PCOM_TX_FRAMES_OBC4_439_SIGNALS_COUNT 			(2U)
#define PCOM_TX_FRAMES_SUPV_V2_OBC_DCDC_591_SIGNALS_COUNT 	(11U)
#define PCOM_TX_FRAMES_FrameDCHV_SIGNALS_COUNT 			(3U)
#define PCOM_TX_FRAMES_FrameDCLV_SIGNALS_COUNT 			(3U)
#define PCOM_TX_FRAMES_FramePFC_SIGNALS_COUNT 			(2U)

#define PCOM_RX_FRAMES_BMS1_125_SIGNALS_COUNT 				(5U)
#define PCOM_RX_FRAMES_BMS3_127_SIGNALS_COUNT 				(1U)
#define PCOM_RX_FRAMES_BMS5_359_SIGNALS_COUNT 				(1U)
#define PCOM_RX_FRAMES_BMS6_361_SIGNALS_COUNT 				(5U)
#define PCOM_RX_FRAMES_BMS8_31B_SIGNALS_COUNT 				(1U)
#define PCOM_RX_FRAMES_BMS9_129_SIGNALS_COUNT 				(1U)
#define PCOM_RX_FRAMES_BSIInfo_382_SIGNALS_COUNT 			(8U)
#define PCOM_RX_FRAMES_CtrlDCDC_372_SIGNALS_COUNT 			(3U)
#define PCOM_RX_FRAMES_ELECTRON_BSI_092_SIGNALS_COUNT 			(3U)
#define PCOM_RX_FRAMES_NEW_JDD_55F_SIGNALS_COUNT 			(1U)
#define PCOM_RX_FRAMES_ParkCommand_31E_SIGNALS_COUNT 			(1U)
#define PCOM_RX_FRAMES_VCU_552_SIGNALS_COUNT 				(3U)
#define PCOM_RX_FRAMES_VCU_BSI_Wakeup_27A_SIGNALS_COUNT 		(13U)
#define PCOM_RX_FRAMES_VCU_PCANInfo_17B_SIGNALS_COUNT 			(4U)
#define PCOM_RX_FRAMES_VCU_TU_37E_SIGNALS_COUNT 			(1U)
#define PCOM_RX_FRAMES_VCU2_0F0_SIGNALS_COUNT 				(1U)
#define PCOM_RX_FRAMES_VCU3_486_SIGNALS_COUNT 				(1U)
#define PCOM_RX_FRAMES_DCHVSTATUS1_SIGNALS_COUNT       			(4U)
#define PCOM_RX_FRAMES_DCHVSTATUS2_SIGNALS_COUNT     			(2U)
#define PCOM_RX_FRAMES_DCHVFAULTS_SIGNALS_COUNT					(11U)
#define PCOM_RX_FRAMES_DCLVSTATUS1_SIGNALS_COUNT         		(4U)
#define PCOM_RX_FRAMES_DCLVSTATUS3_SIGNALS_COUNT        		(3U)
#define PCOM_RX_FRAMES_DCLVFAULTS_SIGNALS_COUNT         		(9U)
#define PCOM_RX_FRAMES_DCLVSTATUS2_SIGNALS_COUNT				(6U)
#define PCOM_RX_FRAMES_PFCSTATUS1_SIGNALS_COUNT					(5U)
#define PCOM_RX_FRAMES_PFCSTATUS2_SIGNALS_COUNT					(3U)
#define PCOM_RX_FRAMES_PFCSTATUS3_SIGNALS_COUNT					(6U)
#define PCOM_RX_FRAMES_PFCSTATUS4_SIGNALS_COUNT					(0U)
#define PCOM_RX_FRAMES_PFCSTATUS5_SIGNALS_COUNT					(3U)
#define PCOM_RX_FRAMES_PFCFAULTS_SIGNALS_COUNT					(16U)
#define PCOM_RX_TOTAL_SIGNALS								(128U)


#define PCOM_FEATURE_NA				(NULL_PTR)

typedef		uint32 pcom_limits_type_higher_t;
typedef		sint32 pcom_limits_type_lower_t;

typedef void (*PCOM_RX_Grid_ErrorIndication_t)(boolean ErrorState);



typedef enum PCOM_Frame_Mode_e
{
  PCOM_PERIODIC = 0,
  PCOM_EVENT = 1,
  PCOM_MIXED = 2
}PCOM_Frame_Mode_t;

typedef enum
{
  PCOM_CAN_EXT = 0,
  PCOM_CAN_INT_OBC = 1,
  PCOM_CAN_INT_LVC = 2,
  PCOM_CAN_NUMBER = 3
}PCOM_can_grid_t;

typedef enum
{
  PCOM_SIGNAL_INACTIVE,
  PCOM_SIGNAL_ACTIVE
}PCOM_Signals_enable_t;

typedef enum
{
  PCOM_SIGNAL_NORMAL,
  PCOM_SIGNAL_ROLLINCOUNTER,
  PCOM_SIGNAL_CHECKSUM
}PCOM_Signals_porpouse_t;

typedef enum
{
  PCOM_SIGNAL_AVAILABLE,
  PCOM_SIGNAL_NOTAVAILABLE
}PCOM_Signals_available_t;

typedef enum
{
  PCOM_SIGNAL_APPLICABLE,
  PCOM_SIGNAL_NOTAPPLICABLE
}PCOM_Signals_applicable_t;

typedef enum
{
  PCOM_TX_DIAGNOSTIC_FORBIDDEN,
  PCOM_TX_DIAGNOSTIC_RUNNING
}PCOM_Signals_diagnostic_t;

typedef enum
{
  PCOM_TX_DIAGFAULT_NO_DETECTED = 0,
  PCOM_TX_DIAGFAULT_DETECTED = 1
}PCOM_Signals_diagFaults_t;

typedef enum
{
  PCOM_TX_DEBOUNCE_UNINIT,
  PCOM_TX_DEBOUNCE_PASSING,
  PCOM_TX_DEBOUNCE_FAILING
}PCOM_Signals_debouncing_t;

typedef enum
{
  PCOM_SIGNAL_TYPE_BOOL =0,
  PCOM_SIGNAL_TYPE_U32 =1,
  PCOM_SIGNAL_TYPE_U16 =2,
  PCOM_SIGNAL_TYPE_U8 =3,
  PCOM_SIGNAL_TYPE_S8 =4,
  PCOM_SIGNAL_TYPE_S16 =5,
  PCOM_SIGNAL_TYPE_S32 =6
}PCOM_Signals_types_t;

typedef enum
{
  PCOM_CANSIGNAL_TYPE_UNM,
  PCOM_CANSIGNAL_TYPE_SNM,
  PCOM_CANSIGNAL_TYPE_BMP,
  PCOM_CANSIGNAL_TYPE_NA,
  PCOM_CANSIGNAL_TYPE_OTHER
}PCOM_CanSignal_type_t;

typedef enum
{
  PCOM_CANSIGNAL_LIMITS_NO,
  PCOM_CANSIGNAL_LIMITS_UPPER,
  PCOM_CANSIGNAL_LIMITS_LOWER,
  PCOM_CANSIGNAL_LIMITS_BOTH
}PCOM_CanSignal_limits_t;

typedef enum
{
  PCOM_TX_ORIG_INITIAL,
  PCOM_TX_ORIG_NOTAVAIL,
  PCOM_TX_ORIG_MEMORIZED,
  PCOM_TX_ORIG_RAW,
  PCOM_TX_ORIG_SUBSTITUTION,
  PCOM_TX_ORIG_PHYSICAL,
  PCOM_TX_DEFAULT_INITIAL
}PCOM_TX_orig_ptr_t;


typedef uint32 pcom_gen_t;

/* PRQA S 1336 EOF # Function prototype typedefs does not include parameter name. Needed to support different function names for each different signal */

typedef Std_ReturnType (*callableU8)(uint8 *);
typedef Std_ReturnType (*callableU16)(uint16 *);
typedef Std_ReturnType (*callableU32)(uint32 *);

typedef Std_ReturnType (*callableS8)(sint8 *);
typedef Std_ReturnType (*callableS16)(sint16 *);
typedef Std_ReturnType (*callableS32)(sint32 *);
typedef Std_ReturnType (*callableBool)(boolean *);

typedef Std_ReturnType (*callableU8Const)(const uint8 *);
typedef Std_ReturnType (*callableU16Const)(const uint16 *);
typedef Std_ReturnType (*callableU32Const)(const uint32 *);

typedef Std_ReturnType (*callableS8Const)(const sint8 *);
typedef Std_ReturnType (*callableS16Const)(const sint16 *);
typedef Std_ReturnType (*callableS32Const)(const sint32 *);
typedef Std_ReturnType (*callableBoolConst)(const boolean *);

typedef void (*callableGeneric)(pcom_gen_t *);
typedef Std_ReturnType (*getValFuncPtr)(void *);


typedef 	boolean(*PCOM_EventTriggerFunction_t)(void);
typedef		boolean(*PCOM_SignalUpdatedFunction_t)(void);
typedef		PCOM_Signals_diagnostic_t(*PCOM_DiagEnableFunc_t)(void);


typedef struct PCOM_TX_Signals_Config_s
{
  PCOM_Signals_enable_t		(*enable)(void);
  PCOM_Signals_types_t		dataType;
  PCOM_Signals_applicable_t	applicableReserved;
  PCOM_CanSignal_type_t		canSignalType;
  struct {
    getValFuncPtr		calculated;
    getValFuncPtr		initial;
    boolean(*producerError)	(void);
  } get;
  struct
  {
    uint32			reserved;
    uint32			invalid;
  } values;
  struct {
    PCOM_CanSignal_limits_t 	range;
    pcom_limits_type_higher_t 	upper;
    pcom_limits_type_lower_t 	lower;
  }limits;
  getValFuncPtr		canSignal;
} PCOM_TX_Signals_Config_t;

typedef struct
{
  const PCOM_TX_Signals_Config_t	*signalsData;
  uint16 				signalsCount;
}PCOM_TX_Frame_Signals_Config_t;

typedef struct PCOM_TX_Frame_Config_s
{
  boolean						enable;
  PduIdType						CAN_Id;
  PCOM_Frame_Mode_t					mode;
  boolean						HasChkSum;
  boolean						HasRC;
  uint8							NibbleChkSum;
  uint8							NibbleRC;
  uint8							ChkSumConst;
  uint16						ComposeOffset; /* Offset to be composed after being sent */
  PCOM_EventTriggerFunction_t				EventTriggerFunction;
  PCOM_SignalUpdatedFunction_t				SignalUpdatedFunction;
  PCOM_TX_Frame_Signals_Config_t			signals;
} PCOM_TX_Frame_Config_t;

typedef struct PCOM_TX_Frame_Data_s
{
  uint8							CurrentRC;
  uint16						ComposeTimeout;
}PCOM_TX_Frame_Data_t;

typedef uint32 pcom_notavaialble_t;

typedef struct{
  pcom_gen_t			memorizedValue;
  pcom_gen_t 			rawValue;
  pcom_gen_t 			substutionValue;
  PCOM_Signals_available_t	availability;
  PCOM_Signals_diagFaults_t	diagInvalidFault;
  PCOM_Signals_diagFaults_t	diagForbidenFault;
  struct
  {
    struct{
      PCOM_Signals_debouncing_t	prefault;
      uint16			counterFail;
      uint16			counterHealed;
    }diagInvalid;
    struct{
      PCOM_Signals_debouncing_t	prefault;
      uint16			counterFail;
      uint16			counterHealed;
    }diagForbidden;
  }debounce;

}PCOM_RX_Signals_Data_t;

typedef struct PCOM_RX_Signals_Config_s
{
  PCOM_Signals_enable_t		enable;
  PCOM_Signals_porpouse_t	porpouse;
  PCOM_Signals_types_t		dataType;
  boolean					diagnosticable;
  PCOM_Signals_applicable_t	LIDapplicable;
  struct {
    getValFuncPtr		outputValue;
  }set;
  struct {
    getValFuncPtr		initial;
    getValFuncPtr		physical;
    boolean			(*outputSelectorFault)(void);
    getValFuncPtr		defaultSubstutionValue;
    boolean			(*forbidden)(pcom_gen_t*);
    struct{
      boolean 			isAvailable;
      pcom_gen_t		value;
    }invalid;
    struct{
      boolean 			isAvailable;
      pcom_notavaialble_t	value;
    }notAvailableCAN;
  }get;
  struct
  {
    PCOM_CanSignal_limits_t 	range;
    pcom_gen_t 			upper;
    pcom_gen_t 			lower;
  }limits;
  struct{
    struct{
      PCOM_Signals_diagnostic_t	* enable;
      IdtSignalDiagTime		(*timeConfirm)(void);
      IdtSignalDiagTime		(*timeHealed)(void);
    }invalid;
    struct{
      PCOM_Signals_diagnostic_t	* enable;
      IdtSignalDiagTime		(*timeConfirm)(void);
      IdtSignalDiagTime		(*timeHealed)(void);
    }forbidden;
  }diag;
  PCOM_RX_Signals_Data_t	*data;
} PCOM_RX_Signals_Config_t;

typedef struct
{
  const PCOM_RX_Signals_Config_t			*signalsData;
  uint16 						signalsCount;
}PCOM_RX_Frame_Signals_Config_t;

typedef volatile struct PCOM_RX_Frame_Config_s
{
  PCOM_can_grid_t 			CAN_grid;
  boolean				enable;
  PduIdType				CAN_Id;
  PCOM_Frame_Mode_t			mode;
  uint16				period;
  uint8					targetDelay;
  uint16				LostFrameDetection;
  boolean				HasChkSum;
  boolean				HasRC;
  uint8					NibbleChkSum;
  uint8					NibbleRC;
  uint8					ChkSumConst;
  PCOM_DiagEnableFunc_t			DiagLostFrameEnableFunc;
  PCOM_DiagEnableFunc_t			ChkSumEnableFunc;
  PCOM_DiagEnableFunc_t			RCEnableFunc;
  boolean					hasLID;
  PCOM_RX_Frame_Signals_Config_t		signals;
} PCOM_RX_Frame_Config_t;

typedef volatile struct PCOM_RX_Frame_Data_s
{

  boolean				LostFramePreFault;
  boolean				LostFrameFault;
  boolean				ChkSumPreFault;
  boolean				ChkSumFault;
  boolean				RCPreFault;
  boolean				RCFault;
  boolean				LostFrame_receiveFlag;
  uint16				LastFrameTime;
  boolean 			RC_receiveFlag;
  uint8					NewRCValue;
  uint8					LastRCValue;
  uint8					LostFrameConfirm;	/* Units of period */
  uint8					LostFrameHeal;		/* Units of period */
  uint8					ChkSumConfirm;		/* Units of period */
  uint8					ChkSumHeal;			/* Units of period */
  uint8					RCConfirm;			/* Units of period */
  uint8					RCHeal;				/* Units of period */
  uint8					currentDelay;
  boolean				receiveFlag;
  uint32				DebugRCbuffer;
  struct
  {
    PCOM_Signals_debouncing_t		forbiddenPrefault;
    PCOM_Signals_diagFaults_t		forbidenFault;
  }LID;
}PCOM_RX_Frame_Data_t;



#define CtApTBD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern boolean PCOM_IntCANDiagnosticsEnable;

#define CtApTBD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */



/* - Static non-init variables declaration ---------- */
#define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern PCOM_RX_Signals_Data_t PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[PCOM_RX_TOTAL_SIGNALS];

extern uint16 PCOM_BMS_SOC_Initial;
extern uint32 PCOM_KILOMETRAGE_Initial;
extern uint8 PCOM_VCU_Keyposition_Initial;

#define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern PCOM_Signals_diagnostic_t PCOM_DiagEnable_E_VCU;
extern PCOM_Signals_diagnostic_t PCOM_DiagEnable_TBMU;
extern PCOM_Signals_diagnostic_t PCOM_DiagEnable_BSI;

#define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


extern boolean PCOM_Frame_5b1_SendEvent;
extern boolean PCOM_Frame_0ce_SendEvent;


static PCOM_Signals_diagnostic_t PCOM_AlwaysON_function(void);

/*General CAN error function.*/
static void PCOM_InternalCAN_OBC_ErrorIndication(boolean ErrorState);
static void PCOM_InternalCAN_LVC_ErrorIndication(boolean ErrorState);

/*TX frames, signals are updated.*/
static boolean PCOM_FrameNum345_SignalUpdatedFunction(void);
static boolean PCOM_FrameNum3A3_SignalUpdatedFunction(void);
static boolean PCOM_FrameNum3A2_SignalUpdatedFunction(void);
static boolean PCOM_FrameDCHV_SignalUpdatedFunction(void);
static boolean PCOM_FrameDCLV_SignalUpdatedFunction(void);
static boolean PCOM_FramePFC_SignalUpdatedFunction(void);

/*TX frames, Event trigger.*/
static boolean PCOM_FrameNum0CE_EventTriggerFunction(void);
static boolean PCOM_FrameNum5B1_EventTriggerFunction(void);
static boolean PCOM_FrameNum439_EventTriggerFunction(void);
static boolean PCOM_FrameNum591_EventTriggerFunction(void);

/*RX frame, Enable timeout.*/
static PCOM_Signals_diagnostic_t PCOM_FrameNum0F0_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum125_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum127_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum129_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum17B_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum27A_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum359_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum361_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum372_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum37E_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum382_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum31B_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum31E_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum486_LostFrameEnableFunction(void);
static PCOM_Signals_diagnostic_t PCOM_FrameNum552_LostFrameEnableFunction(void);


/*TX frame, Update signals.*/
void PCOM_FrameNum7fd_UpdateFrameSignals_TX(void);
void PCOM_FrameNum5b1_UpdateFrameSignals_TX(void);
void PCOM_FrameNum591_UpdateFrameSignals_TX(void);
void PCOM_FrameNum58f_UpdateFrameSignals_TX(void);
void PCOM_FrameNum439_UpdateFrameSignals_TX(void);
void PCOM_FrameNum3a3_UpdateFrameSignals_TX(void);
void PCOM_FrameNum3a2_UpdateFrameSignals_TX(void);
void PCOM_FrameNum345_UpdateFrameSignals_TX(void);
void PCOM_FrameNum230_UpdateFrameSignals_TX(void);
void PCOM_FrameNum0ce_UpdateFrameSignals_TX(void);
void PCOM_FrameNum0c5_UpdateFrameSignals_TX(void);
void PCOM_FrameDCLV_UpdateFrameSignals_TX(void);
void PCOM_FrameDCHV_UpdateFrameSignals_TX(void);
void PCOM_FramePFC_UpdateFrameSignals_TX(void);


/*RX frame, Update signals*/
void PCOM_FrameNum0f0_UpdateFrameSignals_RX(void);
void PCOM_FrameNum125_UpdateFrameSignals_RX(void);
void PCOM_FrameNum127_UpdateFrameSignals_RX(void);
void PCOM_FrameNum129_UpdateFrameSignals_RX(void);
void PCOM_FrameNum1b7_UpdateFrameSignals_RX(void);
void PCOM_FrameNum27a_UpdateFrameSignals_RX(void);
void PCOM_FrameNum359_UpdateFrameSignals_RX(void);
void PCOM_FrameNum361_UpdateFrameSignals_RX(void);
void PCOM_FrameNum372_UpdateFrameSignals_RX(void);
void PCOM_FrameNum37e_UpdateFrameSignals_RX(void);
void PCOM_FrameNum382_UpdateFrameSignals_RX(void);
void PCOM_FrameNum31b_UpdateFrameSignals_RX(void);
void PCOM_FrameNum31e_UpdateFrameSignals_RX(void);
void PCOM_FrameNum486_UpdateFrameSignals_RX(void);
void PCOM_FrameNum552_UpdateFrameSignals_RX(void);
void PCOM_FrameNum092_UpdateFrameSignals_RX(void);
void PCOM_FrameNum55f_UpdateFrameSignals_RX(void);
void PCOM_FrameDCLVstatus1_UpdateFrameSignals_RX(void);
void PCOM_FrameDCLVstatus2_UpdateFrameSignals_RX(void);
void PCOM_FrameDCLVstatus3_UpdateFrameSignals_RX(void);
void PCOM_FrameDCLVfaults_UpdateFrameSignals_RX(void);
void PCOM_FrameDCHVstatus1_UpdateFrameSignals_RX(void);
void PCOM_FrameDCHVstatus2_UpdateFrameSignals_RX(void);
void PCOM_FrameDCHVfaults_UpdateFrameSignals_RX(void);
void PCOM_FramePFCstatus1_UpdateFrameSignals_RX(void);
void PCOM_FramePFCstatus2_UpdateFrameSignals_RX(void);
void PCOM_FramePFCstatus3_UpdateFrameSignals_RX(void);
void PCOM_FramePFCstatus4_UpdateFrameSignals_RX(void);
void PCOM_FramePFCstatus5_UpdateFrameSignals_RX(void);
void PCOM_FramePFCfaults_UpdateFrameSignals_RX(void);



/*Configuration Array.*/
extern const PCOM_TX_Frame_Config_t PCOM_TX_Frame_Config[PCOM_NUM_TX_FRAMES];
extern const PCOM_RX_Frame_Config_t PCOM_RX_Frame_Config[PCOM_NUM_RX_FRAMES];
extern const PCOM_RX_Grid_ErrorIndication_t PCOM_RX_Grid_ErrorIndication [PCOM_CAN_NUMBER];


#endif /* PCOM_CFG_H */
