/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApBAT.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApBAT
 *  Generation Time:  2020-11-23 11:42:16
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApBAT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup BAT
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Battery Voltage diagnosis.
 * \details This module shall report the battery state depending on the battery
 *  voltage. All parameters are stored in \ref NVM.
 *
 * \{
 * \file  BAT.c
 * \brief  Generic code of the BAT module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApBAT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */

/* PRQA S 0857 ++ #Following macros are autogenrated.*/
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/*! Signal not available value for U8*/
#define BAT_SNA_U8		0xFF


/* PRQA S 0857 -- #Following macros are autogenrated.*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/
/*! Battery faults*/
typedef enum {
	/*! Overvoltage*/
	BAT_FAULT_OVERVOLT = 0U,
	/*! Undervoltage*/
	BAT_FAULT_UNDERVOLT,
	/*! Number of faults*/
	BAT_FAULT_NUMBER
}BAT_fault_enum_t;

/*! Actions that required a debounce.*/
typedef enum{
	/*! Fault deactivation.*/
	BAT_FAULT_DEACTIVATION = 0U,
	/*! Fault activation.*/
	BAT_FAULT_ACTIVATION = 1U,
	/*! Number of actions that require a debounce*/
	BAT_FAUTL_TRANSITION_NUMBER = 2U
}BAT_fault_transition_t;
/*PRQA S 3205 --*/

/*!
 * \brief Fault structure
 * \details This structure contains all the elements needed to manage a fault.
 */
typedef struct{
	/*! Prefault state.*/
	boolean pre_fault;
	/*! Threshold to activate the fault.*/
	IdtBatteryVoltageThreshold threshold;
	/*! Output state.*/
	boolean out_state;
	/*! Debounce counter.*/
	IdtBatteryVoltageTime counter;
	/*! Debounce time for each transition.*/
	IdtBatteryVoltageTime counter_max[BAT_FAUTL_TRANSITION_NUMBER];
}BAT_fault_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApBAT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*! Battery faults declaration.*/
static BAT_fault_t BAT_faults[BAT_FAULT_NUMBER];

#define CtApBAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApBAT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApBAT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApBAT_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApBAT_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/*!
 * \brief Fault debounce.
 * \details This function implements a debounce for the fault generation.
 * \param[in] BAT_fault_index Fault index.
 */
static void BAT_fault_debounce(BAT_fault_enum_t BAT_fault_index);

/** Update calibratables.*/
static void BAT_UpdateCalibratables(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltageThreshold: Integer in interval [0...200]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltageTime: Integer in interval [0...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtBatteryVoltageTime Rte_CData_CalBatteryVoltageOVHealTime(void)
 *   IdtBatteryVoltageThreshold Rte_CData_CalBatteryVoltageThresholdOV(void)
 *   IdtBatteryVoltageThreshold Rte_CData_CalBatteryVoltageThresholdUV(void)
 *   IdtBatteryVoltageTime Rte_CData_CalBatteryVoltageUVConfirmTime(void)
 *   IdtBatteryVoltageTime Rte_CData_CalBatteryVoltageUVHealTime(void)
 *   IdtBatteryVoltageTime Rte_CData_CarBatteryVoltageOVConfirmTime(void)
 *
 *********************************************************************************************************************/


#define CtApBAT_START_SEC_CODE
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApBAT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApBAT_init_doc
 *********************************************************************************************************************/

/*!
 * \brief BAT initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApBAT_CODE) RCtApBAT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApBAT_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApBAT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApBAT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApBAT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*GET NVM parameters.*/
	BAT_faults[BAT_FAULT_OVERVOLT].threshold = Rte_CData_CalBatteryVoltageThresholdOV();
	BAT_faults[BAT_FAULT_OVERVOLT].counter_max[BAT_FAULT_ACTIVATION] = Rte_CData_CarBatteryVoltageOVConfirmTime();
	BAT_faults[BAT_FAULT_OVERVOLT].counter_max[BAT_FAULT_DEACTIVATION] =	Rte_CData_CalBatteryVoltageOVHealTime();
	BAT_faults[BAT_FAULT_OVERVOLT].counter = 0U;
	BAT_faults[BAT_FAULT_OVERVOLT].out_state = FALSE;

	BAT_faults[BAT_FAULT_UNDERVOLT].threshold = Rte_CData_CalBatteryVoltageThresholdUV();
	BAT_faults[BAT_FAULT_UNDERVOLT].counter_max[BAT_FAULT_ACTIVATION] = Rte_CData_CalBatteryVoltageUVConfirmTime();
	BAT_faults[BAT_FAULT_UNDERVOLT].counter_max[BAT_FAULT_DEACTIVATION] = Rte_CData_CalBatteryVoltageUVHealTime();
	BAT_faults[BAT_FAULT_UNDERVOLT].counter = 0U;
	BAT_faults[BAT_FAULT_UNDERVOLT].out_state = FALSE;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApBAT_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApBAT_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief BAT task.
 *
 * This function is called periodically by the scheduler. This function should
 * generate the battery state depending on the battery voltage.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApBAT_CODE) RCtApBAT_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApBAT_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApBAT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApBAT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApBAT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtBatteryVolt BAT_battery_volt;
	IdtBatteryVoltageState BAT_battery_state;


	BAT_UpdateCalibratables();

	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&BAT_battery_volt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((IdtBatteryVolt)BAT_SNA_U8 != BAT_battery_volt)
	{
		/*Prefault detection.*/
		if(BAT_battery_volt > BAT_faults[BAT_FAULT_OVERVOLT].threshold)
		{
			/*Overvoltage*/
			BAT_faults[BAT_FAULT_OVERVOLT].pre_fault = TRUE;
		}
		else
		{
			BAT_faults[BAT_FAULT_OVERVOLT].pre_fault = FALSE;
		}

		if(BAT_battery_volt < BAT_faults[BAT_FAULT_UNDERVOLT].threshold)
		{
			/*Undervoltage*/
			BAT_faults[BAT_FAULT_UNDERVOLT].pre_fault = TRUE;
		}
		else
		{
			BAT_faults[BAT_FAULT_UNDERVOLT].pre_fault = FALSE;
		}

		/*Debounce*/
		BAT_fault_debounce(BAT_FAULT_OVERVOLT);
		BAT_fault_debounce(BAT_FAULT_UNDERVOLT);

		/*State report*/
		if(TRUE == BAT_faults[BAT_FAULT_OVERVOLT].out_state)
		{
			BAT_battery_state = BAT_OVERVOLTAGE;
		}
		else if(TRUE == BAT_faults[BAT_FAULT_UNDERVOLT].out_state)
		{
			BAT_battery_state = BAT_UNDERVOLTAGE;
		}
		else
		{
			BAT_battery_state = BAT_VALID_RANGE;
		}

		(void)Rte_Write_PpBatteryVoltageState_DeBatteryVoltageState(BAT_battery_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		/* Monitoring conditions are always TRUE for battery voltage diagnostics */
		(void)Rte_Write_PpBatteryVoltMonitoringConditions_DeBatteryVoltMonitoringConditions(TRUE); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApBAT_STOP_SEC_CODE
#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static void BAT_fault_debounce(BAT_fault_enum_t BAT_fault_index)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApBAT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApBAT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApBAT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* The input state is where the functions wants to go. Therefore, depending on that state
	 * the function should use a specific debounce time.*/
	IdtBatteryVoltageTime BAT_counterTime_max;


	if(TRUE == BAT_faults[BAT_fault_index].pre_fault)
	{
		BAT_counterTime_max = BAT_faults[BAT_fault_index].counter_max[BAT_FAULT_ACTIVATION];
	}
	else
	{
		BAT_counterTime_max = BAT_faults[BAT_fault_index].counter_max[BAT_FAULT_DEACTIVATION];
	}


	if(BAT_faults[BAT_fault_index].pre_fault == BAT_faults[BAT_fault_index].out_state)
	{
		/*Edge detected, clean counter*/
		BAT_faults[BAT_fault_index].counter=0U;
	}
	else if(BAT_faults[BAT_fault_index].counter < BAT_counterTime_max)
	{
		/*update counter*/
		BAT_faults[BAT_fault_index].counter++;
	}
	else
	{
		/*Counter time reached, update output*/
		BAT_faults[BAT_fault_index].out_state = BAT_faults[BAT_fault_index].pre_fault;
		BAT_faults[BAT_fault_index].counter=0U;
	}
}

static void BAT_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApBAT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApBAT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApBAT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApBAT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApBAT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


  /*GET NVM parameters.*/
	BAT_faults[BAT_FAULT_OVERVOLT].threshold = Rte_CData_CalBatteryVoltageThresholdOV();
	BAT_faults[BAT_FAULT_OVERVOLT].counter_max[BAT_FAULT_ACTIVATION] = Rte_CData_CarBatteryVoltageOVConfirmTime();
	BAT_faults[BAT_FAULT_OVERVOLT].counter_max[BAT_FAULT_DEACTIVATION] =	Rte_CData_CalBatteryVoltageOVHealTime();

	BAT_faults[BAT_FAULT_UNDERVOLT].threshold = Rte_CData_CalBatteryVoltageThresholdUV();
	BAT_faults[BAT_FAULT_UNDERVOLT].counter_max[BAT_FAULT_ACTIVATION] = Rte_CData_CalBatteryVoltageUVConfirmTime();
	BAT_faults[BAT_FAULT_UNDERVOLT].counter_max[BAT_FAULT_DEACTIVATION] = Rte_CData_CalBatteryVoltageUVHealTime();
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/

/*Doxygen End*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
