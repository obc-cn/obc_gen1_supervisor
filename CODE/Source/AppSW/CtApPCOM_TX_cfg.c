/**************************************************************************//**
 * \file	ctApPCOM_TX_cfg.c
 * \author	M0069810 Juan Carlos Romanillos <juancarlos.romanillos at es.mahle.com>
 * \date		19 nov. 2019
 * \copyright	MAHLE ELECTRONICS - 2019
 * \brief
 * \ingroup ctApPCOM_cfg
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
#include "CtApPCOM_TX_cfg.h"

/*****************************************************************************
 * CONSTANT DEFINITIONS
 *****************************************************************************/

/*****************************************************************************
 * TYPEDEFS
 *****************************************************************************/

/*****************************************************************************
 * PUBLIC_VARIABLE_DEFINITIONS
 *****************************************************************************/

/*****************************************************************************
 * MODULE_VARIABLES
 *****************************************************************************/
/* PRQA S 3214 ++ # Macros needed for memory mapping */

/* - Static non-init variables declaration ---------- */
#define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* Static non-init variables HERE... */

#define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static SG_DC2 SG_DC2_SG_DC2 = {0,0,0,2,2,0};

#define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* PRQA S 3214 -- */

 /* PRQA S 0857,0380 --*/

/*****************************************************************************
 * MODULE_FUNCTION_PROTOTYPES
 *****************************************************************************/

/*****************************************************************************
 * PUBLIC_FUNCTION_DEFINITIONS
 *****************************************************************************/

/*****************************************************************************
 * MODULE_FUNCTION_DEFINITIONS
 *****************************************************************************/

Std_ReturnType PCOM_TX__DCDC_Temperature_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Temperature();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_Temperature_GetCalculated(DCDC_Temperature * value)
{
  return  Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_Temperature_Set(const DCDC_Temperature * value)
{
  return  Rte_Write_PpDCDC_Temperature_DCDC_Temperature(*value);
}
boolean PCOM_TX_ProducerError_DCDC_Temperature_DCDC_Temperature(void)
{
	  boolean retVal;
	  (void) Rte_Read_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
	  return retVal;
}


Std_ReturnType PCOM_TX__DCDC_Status_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Status();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_Status_GetCalculated(DCDC_Status * value)
{
  return  Rte_Read_PpInt_DCDC_Status_DCDC_Status(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_Status_Set(const DCDC_Status * value)
{
  return  Rte_Write_PpDCDC_Status_DCDC_Status(*value);
}
boolean PCOM_TX_ProducerError_DCDC_Status_DCDC_Status(void)
{
  /* TODO NO PRODUCER ERROR*/
  return FALSE;
}

Std_ReturnType PCOM_TX__DCDC_FaultLampRequest_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_FaultLampRequest_GetCalculated(DCDC_FaultLampRequest * value)
{
  return  Rte_Read_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_FaultLampRequest_Set(const DCDC_FaultLampRequest * value)
{
  return  Rte_Write_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(*value);
}



Std_ReturnType PCOM_TX__DCDC_HighVoltConnectionAllowed_GetInitial(boolean * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_HighVoltConnectionAllowed_GetCalculated(DCDC_HighVoltConnectionAllowed * value)
{
  return  Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_HighVoltConnectionAllowed_Set(const DCDC_HighVoltConnectionAllowed * value)
{
  return  Rte_Write_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(*value);
}


Std_ReturnType PCOM_TX__DCDC_Fault_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Fault();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_Fault_GetCalculated(DCDC_Fault * value)
{
  return  Rte_Read_PpInt_DCDC_Fault_DCDC_Fault(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_Fault_Set(const DCDC_Fault * value)
{
  return  Rte_Write_PpDCDC_Fault_DCDC_Fault(*value);
}


Std_ReturnType PCOM_TX__DCDC_InputVoltage_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_InputVoltage_GetCalculated(DCDC_InputVoltage * value)
{
  return  Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_InputVoltage_Set(const DCDC_InputVoltage * value)
{
  return  Rte_Write_PpDCDC_InputVoltage_DCDC_InputVoltage(*value);
}
boolean PCOM_TX_ProducerError_DCDC_InputVoltage_DCDC_InputVoltage(void)
{
  boolean retVal;
  (void) Rte_Read_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}

Std_ReturnType PCOM_TX__DCDC_OVERTEMP_GetInitial(boolean * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_OVERTEMP_GetCalculated(DCDC_OVERTEMP * value)
{
  return  Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_OVERTEMP_Set(const DCDC_OVERTEMP * value)
{
  return  Rte_Write_PpDCDC_OVERTEMP_DCDC_OVERTEMP(*value);
}


Std_ReturnType PCOM_TX__DCDC_OutputVoltage_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_OutputVoltage_GetCalculated(DCDC_OutputVoltage * value)
{
  return  Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_OutputVoltage_Set(const DCDC_OutputVoltage * value)
{
  return  Rte_Write_PpDCDC_OutputVoltage_DCDC_OutputVoltage(*value);
}

boolean PCOM_TX_ProducerError_DCDC_OutputVoltage_DCDC_OutputVoltage(void)
{
  boolean retVal;
  (void) Rte_Read_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}

Std_ReturnType PCOM_TX__DCDC_ActivedischargeSt_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_ActivedischargeSt_GetCalculated(DCDC_ActivedischargeSt * value)
{
  return Rte_Read_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}

Std_ReturnType PCOM_TX__DCDC_ActivedischargeSt_Set(const DCDC_ActivedischargeSt * value)
{
  SG_DC2_SG_DC2.DCDC_ActivedischargeSt = *value;
  return Rte_Write_PpSG_DC2_SG_DC2(&SG_DC2_SG_DC2);
}

Std_ReturnType PCOM_TX__DCDC_OutputCurrent_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_OutputCurrent_GetCalculated(DCDC_OutputCurrent * value)
{
  return  Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_OutputCurrent_Set(const DCDC_OutputCurrent * value)
{
  SG_DC2_SG_DC2.DCDC_OutputCurrent = *value;
  return Rte_Write_PpSG_DC2_SG_DC2(&SG_DC2_SG_DC2);
}

boolean PCOM_TX_ProducerError_DCDC_OutputCurrent_DCDC_OutputCurrent(void)
{
  boolean retVal;
  (void) Rte_Read_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}


Std_ReturnType PCOM_TX__DCDC_InputCurrent_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent();
  return E_OK;
}
Std_ReturnType PCOM_TX__DCDC_InputCurrent_GetCalculated(DCDC_InputCurrent * value)
{
  return  Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DCDC_InputCurrent_Set(const DCDC_InputCurrent * value)
{
  SG_DC2_SG_DC2.DCDC_InputCurrent = *value;
  return Rte_Write_PpSG_DC2_SG_DC2(&SG_DC2_SG_DC2);
}
boolean PCOM_TX_ProducerError_DCDC_InputCurrent_DCDC_InputCurrent(void)
{
  boolean retVal;
  (void) Rte_Read_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}

Std_ReturnType PCOM_TX__OBC_DCDC_RT_POWER_LOAD_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_DCDC_RT_POWER_LOAD_GetCalculated(DCDC_OBCDCDCRTPowerLoad * value)
{
  return  Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_DCDC_RT_POWER_LOAD_Set(const DCDC_OBCDCDCRTPowerLoad * value)
{
  SG_DC2_SG_DC2.DCDC_OBCDCDCRTPowerLoad = *value;
  return Rte_Write_PpSG_DC2_SG_DC2(&SG_DC2_SG_DC2);
}

Std_ReturnType PCOM_TX__OBC_MAIN_CONTACTOR_REQ_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_MAIN_CONTACTOR_REQ_GetCalculated(DCDC_OBCMainContactorReq * value)
{
  return  Rte_Read_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_MAIN_CONTACTOR_REQ_Set(const DCDC_OBCMainContactorReq * value)
{
  SG_DC2_SG_DC2.DCDC_OBCMainContactorReq = *value;
  return Rte_Write_PpSG_DC2_SG_DC2(&SG_DC2_SG_DC2);
}

Std_ReturnType PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ();
  return E_OK;
}

Std_ReturnType PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_GetCalculated(DCDC_OBCQuickChargeContactorReq * value)
{
  return Rte_Read_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}

Std_ReturnType PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_Set(const DCDC_OBCQuickChargeContactorReq * value)
{
  SG_DC2_SG_DC2.DCDC_OBCQuickChargeContactorReq = *value;
  return Rte_Write_PpSG_DC2_SG_DC2(&SG_DC2_SG_DC2);
}



Std_ReturnType PCOM_TX__JDD_BYTE0_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE0_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE0_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE1_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE1_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE1_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE2_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE2_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE2_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE3_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE3_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE3_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE4_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE4_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE4_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE5_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE5_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE5_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE6_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE6_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE6_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(*value);
}
Std_ReturnType PCOM_TX__JDD_BYTE7_GetInitial(uint8 * value)
{
  *value = 0x00U;
  return E_OK;
}
Std_ReturnType PCOM_TX__JDD_BYTE7_GetCalculated(uint8 * value)
{
  return  Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_JDD_BYTE7_Set(const uint8 * value)
{
  return  Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(*value);
}



Std_ReturnType PCOM_TX__OBC_SocketTempL_GetInitial(OBC_SocketTempL * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL();
  return E_OK;
}

Std_ReturnType PCOM_TX__OBC_SocketTempL_GetCalculated(OBC_SocketTempL * value)
{
  return  Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_SocketTempL_Set(const OBC_SocketTempL * value)
{
  return  Rte_Write_PpOBC_SocketTemp_OBC_SocketTempL(*value);
}
boolean PCOM_TX_ProducerError_OBC_SocketTempL_OBC_SocketTempL(void)
{
  boolean retVal;
  (void) Rte_Read_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}


Std_ReturnType PCOM_TX__OBC_SocketTempN_GetInitial(OBC_SocketTempN * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_SocketTempN_GetCalculated(OBC_SocketTempN * value)
{
  return  Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_SocketTempN_Set(const OBC_SocketTempN * value)
{
  return Rte_Write_PpOBC_SocketTemp_OBC_SocketTempN(*value);
}
boolean PCOM_TX_ProducerError_OBC_SocketTempN_OBC_SocketTempN(void)
{
  boolean retVal;
  (void) Rte_Read_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}

Std_ReturnType PCOM_TX__OBC_HighVoltConnectionAllowed_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_HighVoltConnectionAllowed_GetCalculated(OBC_HighVoltConnectionAllowed * value)
{
  return  Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_HighVoltConnectionAllowed_Set(const OBC_HighVoltConnectionAllowed * value)
{
  return  Rte_Write_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(*value);
}


Std_ReturnType PCOM_TX__OBC_ElockState_GetInitial(OBC_ElockState * value)
{
  *value = Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_ElockState_GetCalculated(OBC_ElockState * value)
{
  return  Rte_Read_PpInt_OBC_ElockState_OBC_ElockState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_ElockState_Set(const OBC_ElockState * value)
{
  return  Rte_Write_PpOBC_ElockState_OBC_ElockState(*value);
}


Std_ReturnType PCOM_TX__OBC_ChargingMode_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_ChargingMode_GetCalculated(OBC_ChargingMode * value)
{
  return  Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_ChargingMode_Set(const OBC_ChargingMode * value)
{
  return  Rte_Write_PpOBC_ChargingMode_OBC_ChargingMode(*value);
}


Std_ReturnType PCOM_TX__OBC_Fault_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Fault();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_Fault_GetCalculated(OBC_Fault * value)
{
  return  Rte_Read_PpInt_OBC_Fault_OBC_Fault(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_Fault_Set(const OBC_Fault * value)
{
  return  Rte_Write_PpOBC_Fault_OBC_Fault(*value);
}


Std_ReturnType PCOM_TX__OBC_Status_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Status();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_Status_GetCalculated(OBC_Status * value)
{
  return  Rte_Read_PpInt_OBC_Status_OBC_Status(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_Status_Set(const OBC_Status * value)
{
  return  Rte_Write_PpOBC_Status_OBC_Status(*value);
}
boolean PCOM_TX_ProducerError_OBC_Status_OBC_Status(void)
{
  /* TODO NO PRODUCER ERROR*/
  return FALSE;
}

Std_ReturnType PCOM_TX__EVSE_RTAB_STOP_CHARGE_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE();
  return E_OK;
}
Std_ReturnType PCOM_TX__EVSE_RTAB_STOP_CHARGE_GetCalculated(EVSE_RTAB_STOP_CHARGE * value)
{
  return  Rte_Read_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__EVSE_RTAB_STOP_CHARGE_Set(const EVSE_RTAB_STOP_CHARGE * value)
{
  return  Rte_Write_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(*value);
}

Std_ReturnType PCOM_TX__OBC_CP_connection_Status_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_CP_connection_Status_GetCalculated(OBC_CP_connection_Status * value)
{
  return  Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_CP_connection_Status_Set(const OBC_CP_connection_Status * value)
{
  return  Rte_Write_PpOBC_CP_connection_Status_OBC_CP_connection_Status(*value);
}
boolean PCOM_TX_ProducerError_OBC_CP_connection_Status_OBC_CP_connection_Status(void)
{
  /* TODO NO PRODUCER ERROR*/
  return FALSE;
}

Std_ReturnType PCOM_TX__OBC_OutputVoltage_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_OutputVoltage_GetCalculated(OBC_OutputVoltage * value)
{
  return  Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_OutputVoltage_Set(const OBC_OutputVoltage * value)
{
  return  Rte_Write_PpOBC_OutputVoltage_OBC_OutputVoltage(*value);
}
boolean PCOM_TX_ProducerError_OBC_OutputVoltage_OBC_OutputVoltage(void)
{
  boolean retVal;
  (void) Rte_Read_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}

Std_ReturnType PCOM_TX__OBC_OutputCurrent_GetInitial(uint16 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_OutputCurrent_GetCalculated(OBC_OutputCurrent * value)
{
  return Rte_Read_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_OutputCurrent_Set(const OBC_OutputCurrent * value)
{
  return  Rte_Write_PpOBC_OutputCurrent_OBC_OutputCurrent(*value);
}
boolean PCOM_TX_ProducerError_OBC_OutputCurrent_OBC_OutputCurrent(void)
{
  boolean retVal;
  (void) Rte_Read_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(&retVal);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
  return retVal;
}

Std_ReturnType PCOM_TX__OBC_InputVoltageSt_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_InputVoltageSt_GetCalculated(OBC_InputVoltageSt * value)
{
  return  Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_InputVoltageSt_Set(const OBC_InputVoltageSt * value)
{
  return  Rte_Write_PpOBC_InputVoltageSt_OBC_InputVoltageSt(*value);
}
boolean PCOM_TX_ProducerError_OBC_InputVoltageSt_OBC_InputVoltageSt(void)
{
  /* TODO NO PRODUCER ERROR*/
  return FALSE;
}

Std_ReturnType PCOM_TX__OBC_CommunicationSt_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_CommunicationSt_GetCalculated(OBC_CommunicationSt * value)
{
  return  Rte_Read_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_CommunicationSt_Set(const OBC_CommunicationSt * value)
{
  return  Rte_Write_PpOBC_CommunicationSt_OBC_CommunicationSt(*value);
}

Std_ReturnType PCOM_TX__OBC_ACRange_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_ACRange_GetCalculated(OBC_ACRange * value)
{
  return  Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_ACRange_Set(const OBC_ACRange * value)
{
  return  Rte_Write_PpOBC_ACRange_OBC_ACRange(*value);
}


Std_ReturnType PCOM_TX__OBC_OBCTemp_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_OBCTemp_GetCalculated(OBC_OBCTemp * value)
{
  return  Rte_Read_PpInt_OBC_OBCTemp_OBC_OBCTemp(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_OBCTemp_Set(const OBC_OBCTemp * value)
{
  return  Rte_Write_PpOBC_OBCTemp_OBC_OBCTemp(*value);
}


Std_ReturnType PCOM_TX__OBC_PowerMax_GetInitial(uint8 * value)
{
  *value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax();
  return E_OK;
}
Std_ReturnType PCOM_TX__OBC_PowerMax_GetCalculated(OBC_PowerMax * value)
{
  return  Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_PowerMax_Set(const OBC_PowerMax * value)
{
  return  Rte_Write_PpOBC_PowerMax_OBC_PowerMax(*value);
}
boolean PCOM_TX_ProducerError_OBC_PowerMax_OBC_PowerMax(void)
{
  /* TODO NO PRODUCER ERROR*/
  return FALSE;
}


/* COMMANDS */

Std_ReturnType PCOM_TX_SUP_RequestPFCStatus_GetCalculated(SUP_RequestPFCStatus * value)
{
  return  Rte_Read_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}

Std_ReturnType PCOM_TX_SUP_RequestPFCStatus_Set(const SUP_RequestPFCStatus * value)
{
  return  Rte_Write_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(*value);
}

Std_ReturnType PCOM_TX_SUP_CommandVDCLink_V_GetCalculated(SUP_CommandVDCLink_V * value)
{
  return  Rte_Read_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_SUP_CommandVDCLink_V_Set(const SUP_CommandVDCLink_V * value)
{
  return  Rte_Write_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V((SUP_CommandVDCLink_V) *value);
}


Std_ReturnType PCOM_TX_SUP_RequestStatusDCHV_GetCalculated(SUP_RequestStatusDCHV * value)
{
  return  Rte_Read_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_SUP_RequestStatusDCHV_Set(const SUP_RequestStatusDCHV * value)
{
  return  Rte_Write_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(*value);
}
Std_ReturnType PCOM_TX_DCDC_VoltageReference_GetCalculated(DCDC_VoltageReference * value)
{
  return  Rte_Read_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_DCDC_VoltageReference_Set(const DCDC_VoltageReference * value)
{
  return  Rte_Write_PpDCDC_VoltageReference_DCDC_VoltageReference(*value);
}
Std_ReturnType PCOM_TX_DCDC_CurrentReference_GetCalculated(DCDC_CurrentReference * value)
{
  return  Rte_Read_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_DCDC_CurrentReference_Set(const DCDC_CurrentReference * value)
{
  return  Rte_Write_PpDCDC_CurrentReference_DCDC_CurrentReference(*value);
}



Std_ReturnType PCOM_TX_SUP_RequestDCLVStatus_GetCalculated(SUP_RequestDCLVStatus * value)
{
  return  Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_SUP_RequestDCLVStatus_Set(const SUP_RequestDCLVStatus * value)
{
  return  Rte_Write_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(*value);
}

Std_ReturnType PCOM_TX_DCLV_VoltageReference_GetCalculated(DCLV_VoltageReference * value)
{
  return  Rte_Read_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_DCLV_VoltageReference_Set(const DCLV_VoltageReference * value)
{
  return  Rte_Write_PpDCLV_VoltageReference_DCLV_VoltageReference(*value);
}

Std_ReturnType PCOM_TX_DCLV_Temp_Derating_Factor_GetCalculated(DCLV_Temp_Derating_Factor * value)
{
  return  Rte_Read_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX_DCLV_Temp_Derating_Factor_Set(const DCLV_Temp_Derating_Factor * value)
{
  return  Rte_Write_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(*value);
}


Std_ReturnType PCOM_TX__OBC_OBCStartSt_GetInitial(OBC_OBCStartSt * value)
{
*value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_OBCStartSt_GetCalculated(OBC_OBCStartSt * value)
{
return  Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_OBCStartSt_Set(const OBC_OBCStartSt * value)
{
return  Rte_Write_PpOBC_OBCStartSt_OBC_OBCStartSt(*value);
}

Std_ReturnType PCOM_TX__OBC_PLUG_VOLT_DETECTION_GetInitial(OBC_PlugVoltDetection * value)
{
*value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_PLUG_VOLT_DETECTION_GetCalculated(OBC_PlugVoltDetection * value)
{
return Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_PLUG_VOLT_DETECTION_Set(const OBC_PlugVoltDetection * value)
{
return Rte_Write_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(*value);
}

Std_ReturnType PCOM_TX__OBC_ChargingConnectionConfirmation_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_ChargingConnectionConfirmation_GetCalculated(OBC_ChargingConnectionConfirmati * value)
{
return Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_ChargingConnectionConfirmation_Set(const OBC_ChargingConnectionConfirmati * value)
{
return Rte_Write_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(*value);
}
boolean PCOM_TX_ProducerError_OBC_ChargingConnectionConfirmation_OBC_ChargingConnectionConfirmation(void)
{
  /* TODO NO PRODUCER ERROR*/
return FALSE;
}

Std_ReturnType PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_GetCalculated(OBC_DCChargingPlugAConnConf * value)
{
return Rte_Read_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_Set(const OBC_DCChargingPlugAConnConf * value)
{
return Rte_Write_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(*value);
}

Std_ReturnType PCOM_TX__OBC_HVBattRechargeWakeup_GetInitial(OBC_HVBattRechargeWakeup * value)
{
*value =  Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_HVBattRechargeWakeup_GetCalculated(OBC_HVBattRechargeWakeup * value)
{
return  Rte_Read_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_HVBattRechargeWakeup_Set(const OBC_HVBattRechargeWakeup * value)
{
return  Rte_Write_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(*value);
}

Std_ReturnType PCOM_TX__OBC_PIStateInfoWakeup_GetInitial(OBC_PIStateInfoWakeup * value)
{
*value =  Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_PIStateInfoWakeup_GetCalculated(OBC_PIStateInfoWakeup * value)
{
return  Rte_Read_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_PIStateInfoWakeup_Set(const OBC_PIStateInfoWakeup * value)
{
return  Rte_Write_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(*value);
}

Std_ReturnType PCOM_TX__OBC_HoldDiscontactorWakeup_GetInitial(OBC_HoldDiscontactorWakeup * value)
{
*value =  Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_HoldDiscontactorWakeup_GetCalculated(OBC_HoldDiscontactorWakeup * value)
{
return  Rte_Read_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_HoldDiscontactorWakeup_Set(const OBC_HoldDiscontactorWakeup * value)
{
return  Rte_Write_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(*value);
}

Std_ReturnType PCOM_TX__OBC_COOLING_WAKEUP_GetInitial(OBC_CoolingWakeup * value)
{
*value =  Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_COOLING_WAKEUP_GetCalculated(OBC_CoolingWakeup * value)
{
return Rte_Read_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_COOLING_WAKEUP_Set(const OBC_CoolingWakeup * value)
{
return Rte_Write_PpOBC_CoolingWakeup_OBC_CoolingWakeup(*value);
}

Std_ReturnType PCOM_TX__RECHARGE_HMI_STATE_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__RECHARGE_HMI_STATE_GetCalculated(OBC_RechargeHMIState * value)
{
return Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__RECHARGE_HMI_STATE_Set(const OBC_RechargeHMIState * value)
{
return Rte_Write_PpOBC_RechargeHMIState_OBC_RechargeHMIState(*value);
}

Std_ReturnType PCOM_TX__OBC_PUSH_CHARGE_TYPE_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE();
return E_OK;
}
Std_ReturnType PCOM_TX__OBC_PUSH_CHARGE_TYPE_GetCalculated(OBC_PushChargeType * value)
{
return Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__OBC_PUSH_CHARGE_TYPE_Set(const OBC_PushChargeType * value)
{
return Rte_Write_PpOBC_PushChargeType_OBC_PushChargeType(*value);
}

Std_ReturnType PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_GetInitial(SUPV_StopDelayedHMIWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_GetCalculated(SUPV_StopDelayedHMIWupState * value)
{
return Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_Set(const SUPV_StopDelayedHMIWupState * value)
{
return Rte_Write_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(*value);
}

Std_ReturnType PCOM_TX__HV_BATT_CHARGE_WUP_STATE_GetInitial(SUPV_HVBattChargeWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__HV_BATT_CHARGE_WUP_STATE_GetCalculated(SUPV_HVBattChargeWupState * value)
{
return Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__HV_BATT_CHARGE_WUP_STATE_Set(const SUPV_HVBattChargeWupState * value)
{
return Rte_Write_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(*value);
}

Std_ReturnType PCOM_TX__PI_STATE_INFO_WUP_STATE_GetInitial(SUPV_PIStateInfoWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__PI_STATE_INFO_WUP_STATE_GetCalculated(SUPV_PIStateInfoWupState * value)
{
return Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__PI_STATE_INFO_WUP_STATE_Set(const SUPV_PIStateInfoWupState * value)
{
return Rte_Write_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(*value);
}

Std_ReturnType PCOM_TX__PRE_DRIVE_WUP_STATE_GetInitial(SUPV_PreDriveWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__PRE_DRIVE_WUP_STATE_GetCalculated(SUPV_PreDriveWupState * value)
{
return Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__PRE_DRIVE_WUP_STATE_Set(const SUPV_PreDriveWupState * value)
{
return Rte_Write_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(*value);
}

Std_ReturnType PCOM_TX__POST_DRIVE_WUP_STATE_GetInitial(SUPV_PostDriveWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__POST_DRIVE_WUP_STATE_GetCalculated(SUPV_PostDriveWupState * value)
{
return Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__POST_DRIVE_WUP_STATE_Set(const SUPV_PostDriveWupState * value)
{
return Rte_Write_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(*value);
}

Std_ReturnType PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_GetInitial(SUPV_HoldDiscontactorWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_GetCalculated(SUPV_HoldDiscontactorWupState * value)
{
return Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_Set(const SUPV_HoldDiscontactorWupState * value)
{
return Rte_Write_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(*value);
}

Std_ReturnType PCOM_TX__PRECOND_ELEC_WUP_STATE_GetInitial(SUPV_PrecondElecWupState * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__PRECOND_ELEC_WUP_STATE_GetCalculated(SUPV_PrecondElecWupState * value)
{
return Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__PRECOND_ELEC_WUP_STATE_Set(const SUPV_PrecondElecWupState * value)
{
return Rte_Write_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(*value);
}

Std_ReturnType PCOM_TX__DTC_REGISTRED_GetInitial(SUPV_DTCRegistred * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED();
return E_OK;
}
Std_ReturnType PCOM_TX__DTC_REGISTRED_GetCalculated(SUPV_DTCRegistred * value)
{
return Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__DTC_REGISTRED_Set(const SUPV_DTCRegistred * value)
{
return Rte_Write_PpSUPV_DTCRegistred_SUPV_DTCRegistred(*value);
}

Std_ReturnType PCOM_TX__ECU_ELEC_STATE_RCD_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD();
return E_OK;
}
Std_ReturnType PCOM_TX__ECU_ELEC_STATE_RCD_GetCalculated(SUPV_ECUElecStateRCD * value)
{
return Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__ECU_ELEC_STATE_RCD_Set(const SUPV_ECUElecStateRCD * value)
{
return Rte_Write_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(*value);
}

Std_ReturnType PCOM_TX__RCD_LINE_STATE_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__RCD_LINE_STATE_GetCalculated(SUPV_RCDLineState * value)
{
return Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__RCD_LINE_STATE_Set(const SUPV_RCDLineState * value)
{
return Rte_Write_PpSUPV_RCDLineState_SUPV_RCDLineState(*value);
}

Std_ReturnType PCOM_TX__COOLING_WUP_STATE_GetInitial(uint8 * value)
{
*value =  Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE();
return E_OK;
}
Std_ReturnType PCOM_TX__COOLING_WUP_STATE_GetCalculated(SUPV_CoolingWupState * value)
{
return Rte_Read_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__COOLING_WUP_STATE_Set(const SUPV_CoolingWupState * value)
{
return Rte_Write_PpSUPV_CoolingWupState_SUPV_CoolingWupState(*value);
}


Std_ReturnType PCOM_TX__VERSION_SYSTEME_GetCalculated(VERSION_SYSTEME * value)
{
	return Rte_Read_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__VERSION_SYSTEME_Set(const VERSION_SYSTEME * value)
{
	return Rte_Write_PpVERSION_SYSTEME_VERSION_SYSTEME(*value);
}

Std_ReturnType PCOM_TX__VERS_DATE2_JOUR_GetCalculated(VERS_DATE2_JOUR * value)
{
	return Rte_Read_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__VERS_DATE2_JOUR_Set(const VERS_DATE2_JOUR * value)
{
	return Rte_Write_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR(*value);
}

Std_ReturnType PCOM_TX__VERS_DATE2_MOIS_GetCalculated(VERS_DATE2_MOIS * value)
{
	return Rte_Read_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__VERS_DATE2_MOIS_Set(const VERS_DATE2_MOIS * value)
{
	return Rte_Write_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS(*value);
}

Std_ReturnType PCOM_TX__VERS_DATE2_ANNEE_GetCalculated(VERS_DATE2_ANNEE * value)
{
	return Rte_Read_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__VERS_DATE2_ANNEE_Set(const VERS_DATE2_ANNEE * value)
{
	return Rte_Write_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE(*value);
}

Std_ReturnType PCOM_TX__VERSION_APPLI_GetCalculated(VERSION_APPLI * value)
{
	return Rte_Read_PpInt_VERSION_APPLI_VERSION_APPLI(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__VERSION_APPLI_Set(const VERSION_APPLI * value)
{
	return Rte_Write_PpVERSION_APPLI_VERSION_APPLI(*value);
}

Std_ReturnType PCOM_TX__VERSION_SOFT_GetCalculated(VERSION_SOFT * value)
{
	return Rte_Read_PpInt_VERSION_SOFT_VERSION_SOFT(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__VERSION_SOFT_Set(const VERSION_SOFT * value)
{
	return Rte_Write_PpVERSION_SOFT_VERSION_SOFT(*value);
}

Std_ReturnType PCOM_TX__EDITION_SOFT_GetCalculated(EDITION_SOFT * value)
{
	return Rte_Read_PpInt_EDITION_SOFT_EDITION_SOFT(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__EDITION_SOFT_Set(const EDITION_SOFT * value)
{
	return Rte_Write_PpEDITION_SOFT_EDITION_SOFT(*value);
}

Std_ReturnType PCOM_TX__EDITION_CALIB_GetCalculated(EDITION_CALIB * value)
{
	return Rte_Read_PpInt_EDITION_CALIB_EDITION_CALIB(value);/* PRQA S 3226, 3110, 3417, 3426 # Remove violations related to access to Vector RTE*/
}
Std_ReturnType PCOM_TX__EDITION_CALIB_Set(const EDITION_CALIB * value)
{
	return Rte_Write_PpEDITION_CALIB_EDITION_CALIB(*value);
}


/* EOF */
