/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtHwAbsAIM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtHwAbsAIM
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtHwAbsAIM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup AIM
 * \ingroup BaseSw
 * \author Pablo Bolas
 * \brief This module is the interface between the ADC and the RTE.
 * \details This module reads raw value form the ADC and publish them into the
 * DFA in physical units.
 *
 * \{
 * \file  aim.c
 * \brief  Generic code of the aim module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtHwAbsAIM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
#include "Adc.h"
#include "AppExternals.h"	/* PRQA S 0857 # Macro definitions rule. */

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/*! SNA value definition for U16.*/
#define AIM_SNA_U16 				0xFFFF
/*! SNA value definition for U8.*/
#define AIM_SNA_U8 					0xFF

/*! Temperature line gain conversion*/
#define AIM_TEMP_GAIN				3300U
/*! Temperature line Q format*/
#define AIM_TEMP_Q_FORMAT			12U

/*! E-Lock conversion gain.*/
/* Value for HW V3 */
#define AIM_ELOCK_GAIN				42409U
/*! E-Lock Q format*/
#define AIM_ELOCK_Q_FORMAT			20U

/**Nominal*/
#define AIM_NOMINAL_BATTERY_VOLT 120U
/** Filter accuracy.*/
#define AIM_BAT_FILTER_Q			8U
/** Filter gain*/
#define AIM_BAT_FILTER_GAIN		200U /*time constant (tau) = 40ms*/



/*! Battery conversion gain.*/
#define AIM_BATTERY_GAIN			183U
/*! Battery Q format*/
#define AIM_BATTERY_Q_FORMAT		12U

/*! Proximity conversion gain.*/
/*Calculation process:
 *
 * 1- Get gain (sensorVoltage/uCvoltage) for all "ranges"(resistors).
 * 2- Calculate the average between 100ohm and 1500ohm (valid range).
 * 3- Apply Q factor and conversion units.
 *
 * Result:
 * Gain average : 1,314852773 , Max error: 1.9418 %.
 * uC units : 1,314852773 * (3300 / 2^12) * 2^Q = 433,901415
 *
 */
#define AIM_PROXIMITY_GAIN			434U
/*! Proximity Q format*/
#define AIM_PROXIMITY_Q_FORMAT		12U
/*! LED feedback conversion gain.*/
#define AIM_LED_GAIN			10560U
/*! LED feedback format*/
#define AIM_LED_Q_FORMAT		12U
/*! Elock feedback conversion gain.*/
#define AIM_ELOCKFEEDBACK_GAIN			330U
/*! Elock feedback Q format*/
#define AIM_ELOCKFEEDBACK_Q_FORMAT		12U

/** ADC group 0 size*/
#define AIM_GROUP_0_SIZE			11U
/** ADC group 1 size*/
#define AIM_GROUP_1_SIZE      10U

/** Size of the LUT for the temperature linearization */
#define AIM_TEMP_LUT_SIZE		1024U


#define AIM_VOLT_REF_GAIN_MV		10U		/* 10 mV gain */
#define AIM_TIME_REF_TASK_MULT		1U		/* 10 ms gain */


#define AIM_95_PERCENT_MULT			973U
#define AIM_95_PERCENT_Q			10U

#define AIM_105_PERCENT_MULT		1075U
#define AIM_105_PERCENT_Q			10U

#define AIM_OFFSET_REF				3300U
#define AIM_ADC_DISPLACEMENT		12U

#define AIM_NUM_REF_VOLTAGES 		2U

#define AIM_REFERENCE_DETECTION_TIME_TICKS 25U /* OBCP-30253 250ms */
#define AIM_REFERENCE_DETECTION_TIME_OUT_TICKS 100U     /* OBCP-20252 1000ms */
#define ADC_REFERENCE_SIGNATURE 0xA5U

#define AIM_REF_MODE_WAVE1 0U
#define AIM_REF_MODE_WAVE2 1U
#define AIM_REF_MODE_MAX   2U

/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** ADC group0 buffer.*/
static Adc_ValueGroupType AIM_raw_value_group0[AIM_GROUP_0_SIZE];
 /** ADC group1 buffer.*/
static Adc_ValueGroupType AIM_raw_value_group1[AIM_GROUP_1_SIZE];

#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* PRQA S 3218 ++ # This variable is used only in one function. But it is defined outside the function due to the size of the variable. This makes the code easier to read */
static const uint8 AIM_LUTTemp[AIM_TEMP_LUT_SIZE]={   
/* PRQA S 3218 -- */
0U,/*Outofrange,roundedas-40C*/
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
3U,
4U,
5U,
6U,
6U,
7U,
8U,
9U,
10U,
10U,
11U,
12U,
12U,
13U,
14U,
14U,
15U,
15U,
16U,
16U,
17U,
17U,
18U,
18U,
19U,
19U,
20U,
20U,
20U,
21U,
21U,
22U,
22U,
22U,
23U,
23U,
23U,
24U,
24U,
24U,
25U,
25U,
25U,
26U,
26U,
26U,
27U,
27U,
27U,
28U,
28U,
28U,
28U,
29U,
29U,
29U,
30U,
30U,
30U,
30U,
31U,
31U,
31U,
31U,
32U,
32U,
32U,
32U,
33U,
33U,
33U,
33U,
34U,
34U,
34U,
34U,
34U,
35U,
35U,
35U,
35U,
35U,
36U,
36U,
36U,
36U,
37U,
37U,
37U,
37U,
37U,
37U,
38U,
38U,
38U,
38U,
38U,
39U,
39U,
39U,
39U,
39U,
40U,
40U,
40U,
40U,
40U,
40U,
41U,
41U,
41U,
41U,
41U,
41U,
42U,
42U,
42U,
42U,
42U,
42U,
43U,
43U,
43U,
43U,
43U,
43U,
44U,
44U,
44U,
44U,
44U,
44U,
45U,
45U,
45U,
45U,
45U,
45U,
45U,
46U,
46U,
46U,
46U,
46U,
46U,
46U,
47U,
47U,
47U,
47U,
47U,
47U,
47U,
48U,
48U,
48U,
48U,
48U,
48U,
48U,
49U,
49U,
49U,
49U,
49U,
49U,
49U,
49U,
50U,
50U,
50U,
50U,
50U,
50U,
50U,
50U,
51U,
51U,
51U,
51U,
51U,
51U,
51U,
52U,
52U,
52U,
52U,
52U,
52U,
52U,
52U,
52U,
53U,
53U,
53U,
53U,
53U,
53U,
53U,
53U,
54U,
54U,
54U,
54U,
54U,
54U,
54U,
54U,
55U,
55U,
55U,
55U,
55U,
55U,
55U,
55U,
55U,
56U,
56U,
56U,
56U,
56U,
56U,
56U,
56U,
56U,
57U,
57U,
57U,
57U,
57U,
57U,
57U,
57U,
57U,
58U,
58U,
58U,
58U,
58U,
58U,
58U,
58U,
58U,
59U,
59U,
59U,
59U,
59U,
59U,
59U,
59U,
59U,
60U,
60U,
60U,
60U,
60U,
60U,
60U,
60U,
60U,
60U,
61U,
61U,
61U,
61U,
61U,
61U,
61U,
61U,
61U,
61U,
62U,
62U,
62U,
62U,
62U,
62U,
62U,
62U,
62U,
63U,
63U,
63U,
63U,
63U,
63U,
63U,
63U,
63U,
63U,
64U,
64U,
64U,
64U,
64U,
64U,
64U,
64U,
64U,
64U,
64U,
65U,
65U,
65U,
65U,
65U,
65U,
65U,
65U,
65U,
65U,
66U,
66U,
66U,
66U,
66U,
66U,
66U,
66U,
66U,
66U,
67U,
67U,
67U,
67U,
67U,
67U,
67U,
67U,
67U,
67U,
67U,
68U,
68U,
68U,
68U,
68U,
68U,
68U,
68U,
68U,
68U,
69U,
69U,
69U,
69U,
69U,
69U,
69U,
69U,
69U,
69U,
69U,
70U,
70U,
70U,
70U,
70U,
70U,
70U,
70U,
70U,
70U,
71U,
71U,
71U,
71U,
71U,
71U,
71U,
71U,
71U,
71U,
71U,
72U,
72U,
72U,
72U,
72U,
72U,
72U,
72U,
72U,
72U,
72U,
73U,
73U,
73U,
73U,
73U,
73U,
73U,
73U,
73U,
73U,
73U,
74U,
74U,
74U,
74U,
74U,
74U,
74U,
74U,
74U,
74U,
74U,
75U,
75U,
75U,
75U,
75U,
75U,
75U,
75U,
75U,
75U,
76U,
76U,
76U,
76U,
76U,
76U,
76U,
76U,
76U,
76U,
76U,
77U,
77U,
77U,
77U,
77U,
77U,
77U,
77U,
77U,
77U,
77U,
78U,
78U,
78U,
78U,
78U,
78U,
78U,
78U,
78U,
78U,
78U,
79U,
79U,
79U,
79U,
79U,
79U,
79U,
79U,
79U,
79U,
79U,
80U,
80U,
80U,
80U,
80U,
80U,
80U,
80U,
80U,
80U,
80U,
81U,
81U,
81U,
81U,
81U,
81U,
81U,
81U,
81U,
81U,
82U,
82U,
82U,
82U,
82U,
82U,
82U,
82U,
82U,
82U,
82U,
83U,
83U,
83U,
83U,
83U,
83U,
83U,
83U,
83U,
83U,
83U,
84U,
84U,
84U,
84U,
84U,
84U,
84U,
84U,
84U,
84U,
85U,
85U,
85U,
85U,
85U,
85U,
85U,
85U,
85U,
85U,
85U,
86U,
86U,
86U,
86U,
86U,
86U,
86U,
86U,
86U,
86U,
87U,
87U,
87U,
87U,
87U,
87U,
87U,
87U,
87U,
87U,
87U,
88U,
88U,
88U,
88U,
88U,
88U,
88U,
88U,
88U,
88U,
89U,
89U,
89U,
89U,
89U,
89U,
89U,
89U,
89U,
89U,
90U,
90U,
90U,
90U,
90U,
90U,
90U,
90U,
90U,
90U,
91U,
91U,
91U,
91U,
91U,
91U,
91U,
91U,
91U,
91U,
92U,
92U,
92U,
92U,
92U,
92U,
92U,
92U,
92U,
93U,
93U,
93U,
93U,
93U,
93U,
93U,
93U,
93U,
93U,
94U,
94U,
94U,
94U,
94U,
94U,
94U,
94U,
94U,
95U,
95U,
95U,
95U,
95U,
95U,
95U,
95U,
95U,
95U,
96U,
96U,
96U,
96U,
96U,
96U,
96U,
96U,
96U,
97U,
97U,
97U,
97U,
97U,
97U,
97U,
97U,
97U,
98U,
98U,
98U,
98U,
98U,
98U,
98U,
98U,
98U,
99U,
99U,
99U,
99U,
99U,
99U,
99U,
99U,
99U,
100U,
100U,
100U,
100U,
100U,
100U,
100U,
100U,
101U,
101U,
101U,
101U,
101U,
101U,
101U,
101U,
102U,
102U,
102U,
102U,
102U,
102U,
102U,
102U,
102U,
103U,
103U,
103U,
103U,
103U,
103U,
103U,
103U,
104U,
104U,
104U,
104U,
104U,
104U,
104U,
104U,
105U,
105U,
105U,
105U,
105U,
105U,
105U,
106U,
106U,
106U,
106U,
106U,
106U,
106U,
106U,
107U,
107U,
107U,
107U,
107U,
107U,
107U,
108U,
108U,
108U,
108U,
108U,
108U,
108U,
108U,
109U,
109U,
109U,
109U,
109U,
109U,
109U,
110U,
110U,
110U,
110U,
110U,
110U,
110U,
111U,
111U,
111U,
111U,
111U,
111U,
112U,
112U,
112U,
112U,
112U,
112U,
112U,
113U,
113U,
113U,
113U,
113U,
113U,
114U,
114U,
114U,
114U,
114U,
114U,
114U,
115U,
115U,
115U,
115U,
115U,
115U,
116U,
116U,
116U,
116U,
116U,
116U,
117U,
117U,
117U,
117U,
117U,
117U,
118U,
118U,
118U,
118U,
118U,
119U,
119U,
119U,
119U,
119U,
119U,
120U,
120U,
120U,
120U,
120U,
121U,
121U,
121U,
121U,
121U,
122U,
122U,
122U,
122U,
122U,
123U,
123U,
123U,
123U,
123U,
124U,
124U,
124U,
124U,
124U,
125U,
125U,
125U,
125U,
125U,
126U,
126U,
126U,
126U,
127U,
127U,
127U,
127U,
127U,
128U,
128U,
128U,
128U,
129U,
129U,
129U,
129U,
130U,
130U,
130U,
130U,
131U,
131U,
131U,
131U,
132U,
132U,
132U,
132U,
133U,
133U,
133U,
133U,
134U,
134U,
134U,
135U,
135U,
135U,
135U,
136U,
136U,
136U,
137U,
137U,
137U,
137U,
138U,
138U,
138U,
139U,
139U,
139U,
140U,
140U,
140U,
141U,
141U,
141U,
142U,
142U,
142U,
143U,
143U,
143U,
144U,
144U,
145U,
145U,
145U,
146U,
146U,
147U,
147U,
147U,
148U,
148U,
149U,
149U,
149U,
150U,
150U,
151U,
151U,
152U,
152U,
153U,
153U,
154U,
154U,
155U,
155U,
156U,
156U,
157U,
157U,
158U,
159U,
159U,
160U,
160U,
161U,
162U,
162U,
163U,
163U,
164U,
165U,
166U,
166U,
167U,
168U,
168U,
169U,
170U,
171U,
172U,
173U,
174U,
174U,
175U,
176U,
177U,
178U,
179U,
180U,
182U,
183U,
190U,
190U,
190U,
190U,
190U,
190U,/*Outofrange,roundedas150C*/
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U,
190U
};

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/*! \brief Linear conversion.
 *  \details This function converts the raw value read from the ADC.
 *  If the measure is not available the return value shall be SNA.
 *  \param[in] ADC_raw_value Available flag and raw value.
 *  \return Conversion result.
 */
static uint16 AIM_conversion_temp(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value);

/*! \brief Linear conversion.
 *  \details This function converts the raw value read from the ADC.
 *  If the measure is not available the return value shall be SNA.
 *  \param[in] ADC_raw_value Available flag and raw value.
 *  \return Conversion result.
 */
static uint16 AIM_conversion_prox(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value);


/*! \brief Linear conversion.
 *  \details This function converts the raw value read from the ADC.
 *  If the measure is not available the return value shall be SNA.
 *  \param[in] ADC_raw_value Available flag and raw value.
 *  \return Conversion result.
 */
 static uint8 AIM_conversion_batt(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value);

/*! \brief Linear conversion.
 *  \details This function converts the raw value read from the ADC.
 *  If the measure is not available the return value shall be SNA.
 *  \param[in] ADC_raw_value Available flag and raw value.
 *  \return Conversion result.
 */
static uint8 AIM_conversion_ELock(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value);

/*! \brief Linear conversion.
 *  \details This function converts the raw value read from the ADC.
 *  If the measure is not available the return value shall be SNA.
 *  \param[in] ADC_raw_value Available flag and raw value.
 *  \return Conversion result.
 */
static uint16 AIM_conversion_ElockFeedback(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value);

/*! \brief Linear conversion.
 *  \details This function converts the raw value read from the ADC.
 *  If the measure is not available the return value shall be SNA.
 *  \param[in] ADC_raw_value Available flag and raw value.
 *  \return Conversion result.
 */
static uint16 AIM_conversion_led(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value);
/** Raw value filter*/
static uint8 AIM_Elock_filter(uint8 input);
static uint8 AIM_Battery_filter(uint8 input);

/* Process safety voltage references */
static void AIM_Safety_ref_process(const uint16 p_in_voltage_ref[AIM_NUM_REF_VOLTAGES]);
static void AIM_Safety_ref_get_debounce_time(uint16 * safetyRefDebounceTimeParam);
static boolean AIM_Safety_ref_check_prefaults(uint16 safetyRefVoltageParam, uint16 safetyVoltageRef);
static boolean AIM_Safety_ref_mode_detect(const uint16 p_in_voltage_ref[AIM_NUM_REF_VOLTAGES], uint16* p_out_cal_ref, boolean* p_detected);
static uint16 AIM_Safety_read_cal_ADC_reference(uint8 mode);
static Std_ReturnType AIM_Vref_NVM_Write(uint8 signature, uint16 vref);
static uint8 AIM_Vref_NVM_Read_Signature(void);
static uint16 AIM_Vref_NVM_Read_Vref(void);
static void AIM_GenerateHWEditionDetected(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtAmbientTemperaturePhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtDebounceADCReference: Integer in interval [1...100]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtElockFeedbackCurrent: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackLock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackUnlock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtExtOpPlugLockRaw: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtMsrTempRaw: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtPlugLedFeedbackPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtProximityDetectPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtTempSyncRectPhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtVoltageExternalADCReference: Integer in interval [1...350]
 *   Unit: [mV], Factor: 10, Offset: 0
 * Rte_DT_IdtArrayInitAIMVoltRef_0: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 *   HW_EDITION_UNKNOWN (0U)
 *   HW_EDITION_D21 (1U)
 *   HW_EDITION_D31 (2U)
 *
 * Array Types:
 * ============
 * IdtArrayInitAIMVoltRef: Array with 3 element(s) of type Rte_DT_IdtArrayInitAIMVoltRef_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   Rte_DT_IdtArrayInitAIMVoltRef_0 *Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock(void)
 *     Returnvalue: Rte_DT_IdtArrayInitAIMVoltRef_0* is of type IdtArrayInitAIMVoltRef
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtVoltageExternalADCReference Rte_CData_CalVoltageExternalADCReferenceWave1(void)
 *   IdtVoltageExternalADCReference Rte_CData_CalVoltageExternalADCReferenceWave2(void)
 *   IdtDebounceADCReference Rte_CData_CalDebounceADCReference(void)
 *   Rte_DT_IdtArrayInitAIMVoltRef_0 *Rte_CData_AIMVoltRefNvBlockNeed_DefaultValue(void)
 *     Returnvalue: Rte_DT_IdtArrayInitAIMVoltRef_0* is of type IdtArrayInitAIMVoltRef
 *
 *********************************************************************************************************************/


#define CtHwAbsAIM_START_SEC_CODE
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsAIM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_init_doc
 *********************************************************************************************************************/

/*!
 * \brief AIM initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


    /* [Enrique.Bueno] Set Adc buffer is not required for 'AdcNonAutosarResultPolling' param enabled */
#if 0
	/*Set buffer*/
	Adc_SetupResultBuffer(AdcConf_AdcGroup_AdcSWGroup_0,AIM_raw_value_group0);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	Adc_SetupResultBuffer(AdcConf_AdcGroup_AdcGroup_1,AIM_raw_value_group1);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
#endif

	/*Start Conversion*/
	Adc_StartGroupConversion(AdcConf_AdcGroup_AdcSWGroup_0);
	Adc_StartGroupConversion(AdcConf_AdcGroup_AdcGroup_1);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsAIM_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue data)
 *   Std_ReturnType Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue(IdtLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw data)
 *   Std_ReturnType Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task100ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief AIM task.
  *
  * This function is called periodically by the scheduler. This function should
  * publish in the DFA the measures performed by the ADC.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 AIM_temp_conversion;
	volatile Std_ReturnType fresh_value_group0;
	volatile Std_ReturnType fresh_value_group1;


	/*Get ADC raw values*/
	/*Safety ASILB -> BufferMarker should be set to 0x00C8*/
	AIM_raw_value_group0[0] = 0x00C8;
	fresh_value_group0 = Adc_17_GetGroupResult(AdcConf_AdcGroup_AdcSWGroup_0,AIM_raw_value_group0);
	AIM_raw_value_group1[0] = 0x00C8;
	fresh_value_group1 = Adc_17_GetGroupResult(AdcConf_AdcGroup_AdcGroup_1,AIM_raw_value_group1);

	/*Lines temperatures*/
	AIM_temp_conversion = AIM_conversion_temp(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_6],fresh_value_group0);
	(void)Rte_Write_PpMsrTempRaw_DeMsrTempAC1Raw((IdtMsrTempRaw)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_temp(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_4],fresh_value_group0);
	(void)Rte_Write_PpMsrTempRaw_DeMsrTempAC2Raw((IdtMsrTempRaw)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_temp(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_5],fresh_value_group0);
	(void)Rte_Write_PpMsrTempRaw_DeMsrTempAC3Raw((IdtMsrTempRaw)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_temp(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_3],fresh_value_group0);
	(void)Rte_Write_PpMsrTempRaw_DeMsrTempACNRaw((IdtMsrTempRaw)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_temp(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_1],fresh_value_group0);
	(void)Rte_Write_PpMsrTempRaw_DeMsrTempDC1Raw((IdtMsrTempRaw)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_temp(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_2],fresh_value_group0);
	(void)Rte_Write_PpMsrTempRaw_DeMsrTempDC2Raw((IdtMsrTempRaw)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*RGB LEDs feedback*/
	AIM_temp_conversion = AIM_conversion_led(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_24],fresh_value_group1);
	(void)Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedRedFeedbackPhysicalValue((IdtLedFeedbackPhysicalValue)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_led(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_23],fresh_value_group1);
	(void)Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedGreenFeedbackPhysicalValue((IdtLedFeedbackPhysicalValue)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	AIM_temp_conversion = AIM_conversion_led(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_22],fresh_value_group1);
	(void)Rte_Write_PpLedRGBFeedbackPhysicalValue_DeLedBlueFeedbackPhysicalValue((IdtLedFeedbackPhysicalValue)AIM_temp_conversion);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* On board temperature sensors */
	/* Ambient temperature */
	AIM_temp_conversion = AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_9];
	/* Use only 10 bits to access the LUT */
	AIM_temp_conversion = AIM_temp_conversion >> 2;
	if (AIM_temp_conversion < AIM_TEMP_LUT_SIZE)
	{
		(void)Rte_Write_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(AIM_LUTTemp[AIM_temp_conversion]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(3U,7U,AIM_LUTTemp[AIM_temp_conversion]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	/* SyncRect temperature sensor */
	AIM_temp_conversion = AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_17];
	/* Use only 10 bits to access the LUT */
	AIM_temp_conversion = AIM_temp_conversion >> 2;
	if (AIM_temp_conversion < AIM_TEMP_LUT_SIZE)
	{
		(void)Rte_Write_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(AIM_LUTTemp[AIM_temp_conversion]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsAIM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt data)
 *   Std_ReturnType Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent data)
 *   Std_ReturnType Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock data)
 *   Std_ReturnType Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock data)
 *   Std_ReturnType Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw data)
 *   Std_ReturnType Rte_Write_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected data)
 *   Std_ReturnType Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue(IdtPlugLedFeedbackPhysicalValue data)
 *   Std_ReturnType Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK
 *   Std_ReturnType Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief AIM task.
 *
 * This function is called periodically by the scheduler. This function should
 * publish in the DFA the measures performed by the ADC.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsAIM_CODE) RCtHwAbsAIM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsAIM_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 AIM_temp_conversion_U8;
	uint16 AIM_temp_conversion_U16;
	volatile Std_ReturnType fresh_value_group0;
	volatile Std_ReturnType fresh_value_group1;
	uint16 safetyVoltageRefs[AIM_NUM_REF_VOLTAGES];


	/*Get ADC raw values*/
	/*Safety ASILB -> BufferMarker should be set to 0x00C8*/
	AIM_raw_value_group0[0] = 0x00C8;
	fresh_value_group0 = Adc_17_GetGroupResult(AdcConf_AdcGroup_AdcSWGroup_0,AIM_raw_value_group0);
	AIM_raw_value_group1[0] = 0x00C8;
	fresh_value_group1 = Adc_17_GetGroupResult(AdcConf_AdcGroup_AdcGroup_1,AIM_raw_value_group1);

	/*Battery*/
	AIM_temp_conversion_U8 = AIM_conversion_batt(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_0],fresh_value_group0);
	AIM_temp_conversion_U8 = AIM_Battery_filter(AIM_temp_conversion_U8);
	(void)Rte_Write_PpBatteryVolt_DeBatteryVolt((IdtBatteryVolt)AIM_temp_conversion_U8);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*E-Lock*/
	AIM_temp_conversion_U8 = AIM_conversion_ELock(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_19],fresh_value_group1);
	AIM_temp_conversion_U8 = AIM_Elock_filter(AIM_temp_conversion_U8);
	(void)Rte_Write_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw((IdtExtOpPlugLockRaw)AIM_temp_conversion_U8);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(3U,5U,AIM_temp_conversion_U8);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Proximity*/
	AIM_temp_conversion_U16 = AIM_conversion_prox(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_18],fresh_value_group1);
	(void)Rte_Write_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue((IdtProximityDetectPhysicalValue)AIM_temp_conversion_U16);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(3U,6U,(uint8)((AIM_temp_conversion_U16>>1) & (uint16)AIM_SNA_U8));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*E-Lock Feedback*/
	AIM_temp_conversion_U16 = AIM_conversion_ElockFeedback(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_21],fresh_value_group1);
	(void)Rte_Write_PpElockFeedbackCurrent_DeElockFeedbackCurrent((IdtElockFeedbackCurrent)AIM_temp_conversion_U16);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	AIM_temp_conversion_U16 = AIM_conversion_ElockFeedback(AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_8],fresh_value_group0);
	(void)Rte_Write_PpElockFeedbackLock_DeElockFeedbackLock((IdtElockFeedbackLock)AIM_temp_conversion_U16);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	AIM_temp_conversion_U16 = AIM_conversion_ElockFeedback(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_20],fresh_value_group1);
	(void)Rte_Write_PpElockFeedbackUnlock_DeElockFeedbackUnlock((IdtElockFeedbackUnlock)AIM_temp_conversion_U16);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Plug LED feedback*/
	AIM_temp_conversion_U16 = AIM_conversion_led(AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_25],fresh_value_group1);
	(void)Rte_Write_PpPlugLedFeedbackPhysicalValue_DePlugLedFeedbackPhysicalValue((IdtLedFeedbackPhysicalValue)AIM_temp_conversion_U16);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/* Safety references */

	safetyVoltageRefs[0] = AIM_raw_value_group0[AdcConf_AdcChannel_AdcChannel_10];
	safetyVoltageRefs[1] = AIM_raw_value_group1[AdcConf_AdcChannel_AdcChannel_16];

	AIM_Safety_ref_process(safetyVoltageRefs);
	AIM_GenerateHWEditionDetected();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtHwAbsAIM_STOP_SEC_CODE
#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/



/*! \brief Helper for OBC-30253; Write NVM signature and Vref.
 * \param[in]: signature: signature indicating if vref has been detected to store in NVM
 * \param[in]: vref: vref detected.
 * \return Std_ReturnType.
 */
static Std_ReturnType AIM_Vref_NVM_Write(uint8 signature, uint16 vref)
{
	Rte_DT_IdtArrayInitAIMVoltRef_0* dest = Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock();

	dest[0U] = signature;
	dest[1U] = (uint8) ((vref >> 8) & 0xFFU); /* PRQA S 2985 #Not removed for better understanding*/

	dest[2U] = (uint8) (vref & 0xFFU);

	return Rte_Call_PS_CpHwAbsAIM_AIMVoltRefNvBlockNeed_WriteBlock((dtRef_const_VOID) dest);/* PRQA S 0314 # Conversion needed to serialize/deserialize data into NvM */
}

/*! \brief Helper for OBC-30253; Read NVM signature.
 *  \return Vref.
 */
static uint8 AIM_Vref_NVM_Read_Signature(void)
{
	Rte_DT_IdtArrayInitAIMVoltRef_0* dest = Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock();

	return dest[0U];
}

/*! \brief Helper for OBC-30253; Read NVM stored Vref.
 *  \return Vref.
 */
static uint16 AIM_Vref_NVM_Read_Vref(void)
{
	Rte_DT_IdtArrayInitAIMVoltRef_0* dest = Rte_Pim_AIMVoltRefNvBlockNeed_MirrorBlock();

	return (((uint16) dest[1U] << 8) | dest[2U]);

}



static uint16 AIM_conversion_temp(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 AIM_result;


	if(E_OK!=fresh_value)
	{
		/*measure not available.*/
		AIM_result = AIM_SNA_U16;
	}else
	{
		/*linear conversion.*/
		AIM_result =(uint16)(((uint32)ADC_raw_value
				* (uint32)AIM_TEMP_GAIN) >> AIM_TEMP_Q_FORMAT);
	}
	return AIM_result;
}

static uint16 AIM_conversion_prox(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 AIM_result;


	if(E_OK!=fresh_value)
	{
		/*measure not available.*/
		AIM_result = AIM_SNA_U16;
	}else
	{
		/*linear conversion.*/
		AIM_result =(uint16)((
				((uint32)ADC_raw_value
				* (uint32)AIM_PROXIMITY_GAIN)
				+/*+0.5*/
				((uint32)(1U)<<(AIM_PROXIMITY_Q_FORMAT-1U))
				)
				>> AIM_PROXIMITY_Q_FORMAT);
	}
	return AIM_result;
}

static uint8 AIM_conversion_batt(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 AIM_result;


	if(E_OK!=fresh_value)
	{
		/*measure not available.*/
		AIM_result = AIM_SNA_U8;
	}else
	{
		uint16 AIM_result_temp;
		/*As a high accuracy is required the result is rounded.*/
		AIM_result_temp = (uint16)(
				(
					(
						(uint32)ADC_raw_value
						* (uint32)AIM_BATTERY_GAIN
					)
					+ /*+0.5*/
					((uint32)(1U)<<(AIM_BATTERY_Q_FORMAT-1U))
				)
				>> AIM_BATTERY_Q_FORMAT);

		/*Overflow*/
		if((uint16)AIM_SNA_U8 <= AIM_result_temp)
		{
			AIM_result = AIM_SNA_U8;
		}
		else
		{
			AIM_result = (uint8) AIM_result_temp;
		}

	}
	return AIM_result;

}

static uint8 AIM_conversion_ELock(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 AIM_result;


	if(E_OK!=fresh_value)
	{
		/*measure not available.*/
		AIM_result = AIM_SNA_U8;
	}else
	{
		uint16 AIM_result_temp;
		/*As a high accuracy is required the result is rounded.*/
		AIM_result_temp = (uint16)(
				(
					(
						(uint32)ADC_raw_value
						* (uint32)AIM_ELOCK_GAIN
					)
					+ /*+0.5*/
					((uint32)(1U)<<(AIM_ELOCK_Q_FORMAT-1U))
				)
				>> AIM_ELOCK_Q_FORMAT);

		/*Overflow*/
		if((uint16)AIM_SNA_U8 <= AIM_result_temp)
		{
			AIM_result = AIM_SNA_U8;
		}
		else
		{
			AIM_result = (uint8) AIM_result_temp;
		}

	}
	return AIM_result;
}

static uint16 AIM_conversion_ElockFeedback(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 AIM_result;


	if(E_OK!=fresh_value)
	{
		/*measure not available.*/
		AIM_result = AIM_SNA_U16;
	}else
	{
		/*linear conversion.*/
		AIM_result =(uint16)((
				((uint32)ADC_raw_value
				* (uint32)AIM_ELOCKFEEDBACK_GAIN)
				+/*+0.5*/
				((uint32)(1U)<<(AIM_ELOCKFEEDBACK_Q_FORMAT-1U))
				)
				>> AIM_ELOCKFEEDBACK_Q_FORMAT);
	}
	return AIM_result;
}

static uint16 AIM_conversion_led(Adc_ValueGroupType ADC_raw_value, Std_ReturnType fresh_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 AIM_result;


	if(E_OK!=fresh_value)
	{
		/*measure not available.*/
		AIM_result = AIM_SNA_U16;
	}else
	{
		/*As a high accuracy is required the result is rounded.*/
		AIM_result = (uint16)(
				(
					(
						(uint32)ADC_raw_value
						* (uint32)AIM_LED_GAIN
					)
					+ /*+0.5*/
					((uint32)(1U)<<(AIM_LED_Q_FORMAT-1U))
				)
				>> AIM_LED_Q_FORMAT);
	}
	return AIM_result;
}
static uint8 AIM_Elock_filter(uint8 input)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/*Initial value: half nominal battery level*/
	static uint16 FilteredValue = (uint16)AIM_NOMINAL_BATTERY_VOLT<<(AIM_BAT_FILTER_Q-1U);

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 output;


	FilteredValue = (uint16)((((uint32)FilteredValue*(uint32)AIM_BAT_FILTER_GAIN) + ((((uint32)1U<<AIM_BAT_FILTER_Q)-(uint32)AIM_BAT_FILTER_GAIN)*((uint32)input<<AIM_BAT_FILTER_Q)))>>AIM_BAT_FILTER_Q);
	output = (uint8)((FilteredValue+((uint16)1U<<(AIM_BAT_FILTER_Q-1U)))>>AIM_BAT_FILTER_Q);
	return output;
}
static uint8 AIM_Battery_filter(uint8 input)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/*Initial value: Nominal battery level*/
	static uint16 FilteredValue = (uint16)AIM_NOMINAL_BATTERY_VOLT<<(AIM_BAT_FILTER_Q);

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 output;


	FilteredValue = (uint16)((((uint32)FilteredValue*(uint32)AIM_BAT_FILTER_GAIN) + ((((uint32)1U<<AIM_BAT_FILTER_Q)-(uint32)AIM_BAT_FILTER_GAIN)*((uint32)input<<AIM_BAT_FILTER_Q)))>>AIM_BAT_FILTER_Q);
	output = (uint8)((FilteredValue+((uint16)1U<<(AIM_BAT_FILTER_Q-1U)))>>AIM_BAT_FILTER_Q);
	return output;
}


/*! \brief Helper for OBC-30253; Read reference depending on mode (WAVE1 or WAVE2).
 *  \param[in] mode: WAVE1 or WAVE2.
 *  \return Calibrate value scaled.
 */
static uint16 AIM_Safety_read_cal_ADC_reference(uint8 mode)
{
	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 voltage_param;

	switch (mode)
	{
		case AIM_REF_MODE_WAVE1:
			voltage_param = (uint16)((uint32)Rte_CData_CalVoltageExternalADCReferenceWave1()
					        * (uint32)AIM_VOLT_REF_GAIN_MV);
			break;
		case AIM_REF_MODE_WAVE2:
			voltage_param = (uint16)((uint32)Rte_CData_CalVoltageExternalADCReferenceWave2()
					        * (uint32)AIM_VOLT_REF_GAIN_MV);
			break;
		default:
			voltage_param = 0;
			break;
	}

	return voltage_param;
}

/*! \brief Helper for OBC-30253; Check mode for all references.
 *  \param[in] p_in_voltage_ref: Array of reference values to check.
 *  \param[out] p_out_mode: Array with mode detected for each input.
 *  \return last successfully detected reference.
 */
static uint16 AIM_safety_ref_mode(const uint16 p_in_voltage_ref[AIM_NUM_REF_VOLTAGES], uint8 p_detected_mode_array[AIM_NUM_REF_VOLTAGES])
{
	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Automatic variables declaration ---------------- */
	uint8 mode_idx;
	uint8 ref_idx;
	uint16 expected_ref;
	uint16 output_reference = 0;

	if (NULL_PTR != p_detected_mode_array)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
	{
		/* Check all reference voltages against calibration values for every mode */
		for (ref_idx=0; ref_idx < AIM_NUM_REF_VOLTAGES; ref_idx++)
		{
			p_detected_mode_array[ref_idx] = AIM_REF_MODE_MAX;  /* default mode not found */

			if (NULL_PTR != p_in_voltage_ref)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
			{
				for (mode_idx=AIM_REF_MODE_WAVE1; mode_idx != AIM_REF_MODE_MAX; mode_idx++)
				{
					expected_ref = AIM_Safety_read_cal_ADC_reference(mode_idx);

					if (FALSE == AIM_Safety_ref_check_prefaults(expected_ref, p_in_voltage_ref[ref_idx]))
					{
						p_detected_mode_array[ref_idx] = mode_idx;
						output_reference = expected_ref;
						break;
					}
				}
			}
		}
	}

	return output_reference;
}


/*! \brief Helper for OBC-30253; Check mode for all references.
 *  \details Check that all references are in range with the same mode reference.
 *  \param[in] p_in_voltage_ref: Array of reference values to check.
 *  \param[out] p_out_cal_voltage_ref: Output with calibration value for the mode detected.
 *  \return TRUE if error.
 */
static boolean AIM_safety_ref_mode_check(const uint16 p_in_voltage_ref[AIM_NUM_REF_VOLTAGES], uint16* p_out_cal_voltage_ref)
{
	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Automatic variables declaration ---------------- */
	uint8 ref_idx;
	uint8 p_detected_mode_array[AIM_NUM_REF_VOLTAGES];
	uint8 detected_mode = AIM_REF_MODE_MAX; /* default error */ /* PRQA S 2981 #Variable Initalised for better understanding*/
	boolean error = TRUE;

	if ((NULL_PTR != p_in_voltage_ref)/* PRQA S 2995 #Null pointer checking for robustness*/
		&& (NULL_PTR != p_out_cal_voltage_ref)
		)
	{
		*p_out_cal_voltage_ref = AIM_safety_ref_mode(p_in_voltage_ref, p_detected_mode_array);

		detected_mode = p_detected_mode_array[0];
		if (detected_mode != AIM_REF_MODE_MAX)
		{
			for (ref_idx=1; ref_idx < AIM_NUM_REF_VOLTAGES; ref_idx++)/* PRQA S 2877 #loop with constant value for versatility and portability*/
			{
				if (detected_mode != p_detected_mode_array[ref_idx])
				{
					detected_mode = AIM_REF_MODE_MAX;
					break;
				}
			}
		}
		if (detected_mode < AIM_REF_MODE_MAX)
		{
			error = FALSE;
		}
	}

	return error;
}

/*! \brief Helper for OBC-30253; Detect reference mode.
 *  \details If voltage reference not detected yet, detect reference mode; If detected return reference voltage..
 *  \param[in] p_in_voltage_ref: Array with Adc read references.
 *  \param[out] p_out_cal_ref: Return calibration reference value.
 *  \param[out] pDetected: Return TRUE if reference mode detection has finished.
 *  \return TRUE if error.
 */
static boolean AIM_Safety_ref_mode_detect(const uint16 p_in_voltage_ref[AIM_NUM_REF_VOLTAGES], uint16* p_out_cal_ref, boolean* p_detected)
{
	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static uint16 detectedTimeOut = 0;
	static uint16 detectedTime = 0;
	static uint16 lastSafetyCalVoltageRef = 0;

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Automatic variables declaration ---------------- */
	boolean error = TRUE;
	uint8 adc_ref_signature;

	if (NULL_PTR != p_detected)
	{
		*p_detected = FALSE; /* default not detected */

		if ((NULL_PTR != p_out_cal_ref)
			&& 	(NULL_PTR != p_in_voltage_ref))/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
		{
			error = FALSE; /* Default */

			/* Read signature */
			adc_ref_signature = AIM_Vref_NVM_Read_Signature();

			if (ADC_REFERENCE_SIGNATURE == adc_ref_signature)
			{
				/* Signature OK, return saved reference */
				*p_out_cal_ref = AIM_Vref_NVM_Read_Vref();
				*p_detected = TRUE;
			}
			else
			{
				/* Signature not OK => not detected yet */
				if (AIM_REFERENCE_DETECTION_TIME_OUT_TICKS < detectedTimeOut)
				{
					/* Detection time out (1s) */
					error = TRUE;
				}
				else
				{
					detectedTimeOut++;

					/* Check if reference detected for 250ms consecutive */
					if (FALSE == AIM_safety_ref_mode_check(p_in_voltage_ref, p_out_cal_ref))
					{
						/* References OK; *pSafetyCalVoltageRef with detected reference */
						if (*p_out_cal_ref != lastSafetyCalVoltageRef)
						{
							/* if reference has changed reset detection counter */
							detectedTime = 0;
						}

						lastSafetyCalVoltageRef = *p_out_cal_ref;

						if (AIM_REFERENCE_DETECTION_TIME_TICKS < detectedTime)
						{
							/* Save signature and detected reference */
							(void) AIM_Vref_NVM_Write(ADC_REFERENCE_SIGNATURE, *p_out_cal_ref);
							*p_detected = TRUE;
						}
						else
						{
							detectedTime++;
						}
					}
					else
					{
						/* If reference error reset detection time */
						detectedTime = 0;
					}
				}
			}
		}
	}


	return error;
}


static void AIM_Safety_ref_process(const uint16 p_in_voltage_ref[AIM_NUM_REF_VOLTAGES])
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static uint16 safety_ref_counters[2] = {0U, 0U};

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Automatic variables declaration ---------------- */
	uint16 reference_cal_param = 0;
	uint16 debounce_time = 0;
	boolean reference_prefault;
	uint8 index;
	boolean detected;
	boolean error = TRUE; /* PRQA S 2981 #Variable Initalised for better understanding*/

	if (NULL_PTR != p_in_voltage_ref)/* PRQA S 2995, 2991 #Null pointer checking for robustness*/
	{
		/* Check if reference mode has already been detected (OBCP-30253), detect it if not */
		error = AIM_Safety_ref_mode_detect(p_in_voltage_ref, &reference_cal_param, &detected);

		if ((FALSE != detected) && (FALSE == error))
		{
			/* Convert time parameter to task counts */
			AIM_Safety_ref_get_debounce_time(&debounce_time);

			for (index = 0U; index < AIM_NUM_REF_VOLTAGES; index++)
			{
				/* Check prefaults */
				reference_prefault = AIM_Safety_ref_check_prefaults(reference_cal_param, p_in_voltage_ref[index]);

				/* Debounce prefaults */
				if (FALSE == reference_prefault)
				{
					/* No fault, clean counter */
					safety_ref_counters[index] = 0U;
				}
				else
				{
					/* Prefault present, increase counter */
					safety_ref_counters[index]++;
					/* Check debounce condition */
					if (safety_ref_counters[index] > debounce_time)
					{
						/* Set Fault to TRUE */
						error = TRUE;
					}
				}
			}
		}
	}
	else
	{
		/* In case of pointer error, reset (default) */

	}

	if (FALSE != error)
	{
		/* Error detected. Perform RESET */
		WUM_ResetRequest(0x01);

	}

}

static void AIM_Safety_ref_get_debounce_time(uint16 * safetyRefDebounceTimeParam)
{
	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Automatic variables declaration ---------------- */

	if (NULL_PTR != safetyRefDebounceTimeParam)
	{

/* PRQA S 2985 ++ # The actual value of the macro AIM_TIME_REF_TASK_MULT  is 1. This is the reason of the rule violation. It shall be kept in this form due to possible future changes of the value of the macro */
		*safetyRefDebounceTimeParam =
				(uint16)((uint32)Rte_CData_CalDebounceADCReference()
				*
				((uint32)AIM_TIME_REF_TASK_MULT));
/* PRQA S 2985 -- */
	}
}

static boolean AIM_Safety_ref_check_prefaults(uint16 safetyRefVoltageParam, uint16 safetyVoltageRef)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsAIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsAIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsAIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsAIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsAIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 lowThreshold;
	uint16 highThreshold;
	boolean retValue;
	uint16 safetyVoltageRefInt;

	lowThreshold = (uint16)(((uint32)safetyRefVoltageParam * (uint32)AIM_95_PERCENT_MULT) >> (AIM_95_PERCENT_Q));
	highThreshold = (uint16)(((uint32)safetyRefVoltageParam * (uint32)AIM_105_PERCENT_MULT) >> (AIM_105_PERCENT_Q));

	safetyVoltageRefInt = (uint16)(((uint32)safetyVoltageRef *(uint32)AIM_OFFSET_REF) >> (AIM_ADC_DISPLACEMENT));

	if ((safetyVoltageRefInt >= lowThreshold) && (safetyVoltageRefInt <= highThreshold))
	{
		/* No fault */
		retValue = FALSE;
	}
	else
	{
		/* Fault */
		retValue = TRUE;
	}

	return retValue;

}

static void AIM_GenerateHWEditionDetected(void)
{
	uint8 VrefSignature;
	uint16 VrefValue;
	uint16 VrefWave1;
	uint16 VrefWave2;
	IdtHWEditionDetected AIM_HEditionDetected;

	/* The saved voltage reference expected values in NvM is converted into physical units */
	/* It must be converted again to compare */
	VrefWave1 = (uint16)((uint32)Rte_CData_CalVoltageExternalADCReferenceWave1()
			        * (uint32)AIM_VOLT_REF_GAIN_MV);

	VrefWave2 = (uint16)((uint32)Rte_CData_CalVoltageExternalADCReferenceWave2()
			        * (uint32)AIM_VOLT_REF_GAIN_MV);

	VrefSignature = AIM_Vref_NVM_Read_Signature();
	VrefValue = AIM_Vref_NVM_Read_Vref();

	if (ADC_REFERENCE_SIGNATURE != VrefSignature)
	{
		/* Voltage reference still not found */

		AIM_HEditionDetected = HW_EDITION_UNKNOWN;
	}
	else
	{
		if (VrefValue == VrefWave1)
		{
			AIM_HEditionDetected = HW_EDITION_D21;
		}
		else if (VrefValue == VrefWave2)
		{
			AIM_HEditionDetected = HW_EDITION_D31;
		}
		else
		{
			AIM_HEditionDetected = HW_EDITION_UNKNOWN;
		}
	}

	(void)Rte_Write_PpHWEditionDetected_DeHWEditionDetected(AIM_HEditionDetected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
