/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApRLY.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApRLY
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApRLY>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup RLY
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Relay management.
 * \details This module controls the relay states.
 *
 * \{
 * \file  RLY.c
 * \brief  Generic code of the RLY module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApRLY.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Value for 2.5s delay.*/
#define RLY_DELAY_2500ms				250U
/** Value for 500ms delay.*/
#define RLY_DELAY_500ms					50U

/** DC-link charged percentage to close relays*/
#define RLY_DCLINK_CHARGED_PERCENTAGE	85U

 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/

/** Relays controlled by RLY module.*/
typedef enum{
	/** Monophasic relay.*/
	RLY_RELAY_MONO=0U,
	/** Precharge relay.*/
	RLY_RELAY_PREC,
	/** Number of relays*/
	RLY_RELAY_NUM
}RLY_relay_t;

/*PRQA S 3205 --*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Relay states.*/
static boolean RLY_relayState[RLY_RELAY_NUM];

static uint8 RLY_CloseMonoCounter;
static uint8 RLY_ClosePreCounter;

#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/**
 * \brief Force open task.
 * \details This function checks if the relays must be opened.
 * \param[in] RLY_OBC_staus OBC state.
 * \param[in] RLY_InVoltMode Input Voltage Mode.
 */
static void RLY_ForceOpenTask(OBC_Status RLY_OBC_staus,IdtInputVoltageMode RLY_InVoltMode);

/**
 * \details Close Task.
 * \brief his function Checks if the relays should be closed.
 * \param[in] RLY_OBC_staus OBC state.
 * \param[in] RLY_InVoltMode Input voltage mode.
 *
 */
static void RLY_CloseTask(OBC_Status RLY_OBC_staus,IdtInputVoltageMode RLY_InVoltMode);

/**
 * \brief Relay Close condition.
 * \details This function checks if the set conditions are met to close each
 * relay.
 * \param[in] RLY_InVoltMode Input voltage mode.
 */
static void RLY_CloseCondition(IdtInputVoltageMode RLY_InVoltMode);

/** Close mono sequence.*/
static void RLY_CloseMono(void);
/** Close precharge relay sequence.*/
static void RLY_ClosePrecharge(void);




/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtMaxPrechargeVDCLinkMono: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 300
 * IdtMaxPrechargeVDCLinkTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 500
 * IdtMinPrechargeVDCLinkMono: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinPrechargeVDCLinkTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 0
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxPrechargeVDCLinkMono Rte_CData_CalMaxPrechargeVDCLinkMono(void)
 *   IdtMaxPrechargeVDCLinkTriphasic Rte_CData_CalMaxPrechargeVDCLinkTriphasic(void)
 *   IdtMinPrechargeVDCLinkMono Rte_CData_CalMinPrechargeVDCLinkMono(void)
 *   IdtMinPrechargeVDCLinkTriphasic Rte_CData_CalMinPrechargeVDCLinkTriphasic(void)
 *
 *********************************************************************************************************************/


#define CtApRLY_START_SEC_CODE
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApRLY_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_init_doc
 *********************************************************************************************************************/


/*!
 * \brief RLY initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApRLY_CODE) RCtApRLY_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	RLY_relayState[RLY_RELAY_MONO] = FALSE;
	RLY_relayState[RLY_RELAY_PREC] = FALSE;

	RLY_CloseMonoCounter = 0U;
	RLY_ClosePreCounter = 0U;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApRLY_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpStopConditions_DeStopConditions(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputRelayMono_DeOutputRelayMono(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_task10ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief RLY task.
  *
  * This function is called periodically by the scheduler. This function should
  * update the relay states.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApRLY_CODE) RCtApRLY_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRLY_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_ChargingMode RLY_ChargingMode;
	OBC_Status RLY_OBC_staus;
	IdtInputVoltageMode RLY_InVoltMode;



	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&RLY_ChargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_OBC_Status(&RLY_OBC_staus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&RLY_InVoltMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx2_standby_mode==RLY_OBC_staus)
	{

		RLY_relayState[RLY_RELAY_MONO] = FALSE;
		RLY_relayState[RLY_RELAY_PREC] = FALSE;

		RLY_CloseMonoCounter = 0U;
		RLY_ClosePreCounter = 0U;
	}
	else if(Cx1_slow_charging==RLY_ChargingMode)
	{
		RLY_CloseTask(RLY_OBC_staus,RLY_InVoltMode);
		RLY_ForceOpenTask(RLY_OBC_staus,RLY_InVoltMode);
	}
	else
	{
		RLY_CloseMonoCounter = 0U;
		RLY_ClosePreCounter = 0U;
	}

	(void)Rte_Write_PpOutputRelayMono_DeOutputRelayMono(RLY_relayState[RLY_RELAY_MONO]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOutputPrechargeRelays_DeOutputPrechargeRelays(RLY_relayState[RLY_RELAY_PREC]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(3U,3U,RLY_relayState[RLY_RELAY_PREC]|(RLY_relayState[RLY_RELAY_MONO]<<1));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApRLY_STOP_SEC_CODE
#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static void RLY_ForceOpenTask(OBC_Status RLY_OBC_staus,IdtInputVoltageMode RLY_InVoltMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */



	if(((Cx3_conversion_working_==RLY_OBC_staus)||(Cx5_degradation_mode==RLY_OBC_staus))
					&&(IVM_MONOPHASIC_INPUT!=RLY_InVoltMode)
					&&(IVM_TRIPHASIC_INPUT!=RLY_InVoltMode))
	{
			RLY_relayState[RLY_RELAY_MONO] = FALSE;
			RLY_relayState[RLY_RELAY_PREC] = FALSE;
	}

}

static void RLY_CloseTask(OBC_Status RLY_OBC_staus,IdtInputVoltageMode RLY_InVoltMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RLY_StopCondition;
	boolean RLY_FaultCharge;


	(void)Rte_Read_PpStopConditions_DeStopConditions(&RLY_StopCondition);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&RLY_FaultCharge);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(((Cx3_conversion_working_==RLY_OBC_staus)||(Cx5_degradation_mode==RLY_OBC_staus))
				 && (FALSE==RLY_StopCondition)
				 && (FALSE==RLY_FaultCharge))
	{
		RLY_CloseCondition(RLY_InVoltMode);
	}
	else
	{
		RLY_CloseMonoCounter = 0U;
		RLY_ClosePreCounter = 0U;
	}
}
static void RLY_CloseCondition(IdtInputVoltageMode RLY_InVoltMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(RLY_InVoltMode==IVM_MONOPHASIC_INPUT)
	{
		/*Monophasic*/
		RLY_CloseMono();
		/*Close precharge relay condition*/
		RLY_ClosePrecharge();
	}
	else if(RLY_InVoltMode==IVM_TRIPHASIC_INPUT)
	{
		/*Triphasic*/
		/*Close precharge relay condition*/
		RLY_ClosePrecharge();
		RLY_CloseMonoCounter = 0U;

	}
	else
	{
		/*NONE mode*/
		RLY_CloseMonoCounter = 0U;
		RLY_ClosePreCounter = 0U;
	}


}
static void RLY_CloseMono(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	/*After 500ms close Mono relay*/
	if(RLY_DELAY_500ms<=RLY_CloseMonoCounter)
	{
		RLY_relayState[RLY_RELAY_MONO] = TRUE;
	}
	else
	{
		RLY_CloseMonoCounter ++;
	}
}
static void RLY_ClosePrecharge(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApRLY_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRLY_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRLY_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRLY_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRLY_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Inputs*/
	OBC_InputVoltageSt InVoltState;
	uint16 DClinkV;
	uint16 PeakV;


	(void)Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&InVoltState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(&DClinkV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(&PeakV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Calculate 85% of V peak*/
	PeakV = (uint16)(((uint32)PeakV*(uint32)RLY_DCLINK_CHARGED_PERCENTAGE)/(uint32)100U);

	if((Cx1_220V_AC_connected==InVoltState)
		&&(DClinkV>PeakV))
	{
		/*Precharge relay debounce*/
		if(RLY_ClosePreCounter >= RLY_DELAY_2500ms){
			/*Close precharge relay*/
			RLY_relayState[RLY_RELAY_PREC] = TRUE;
			RLY_ClosePreCounter = 0U;
		}
		else
		{
			RLY_ClosePreCounter++;
		}
	}
	else
	{
		RLY_ClosePreCounter = 0U;
	}

}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
