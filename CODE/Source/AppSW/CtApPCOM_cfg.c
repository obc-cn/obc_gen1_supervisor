/*
 * PCOM_TX_cfg.c
 *
 *  Created on: 19 Nov 19
 *      Author: M006981
 */

/* PRQA S 0313 EOF # Casting to different function pointer type needed to create a configuration structure */
/* PRQA S 4447 EOF # Casting in configuration structure needed */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */

 /* PRQA S 0857,0380 ++ # Max amount of macros check */
#include "CtApPCOM_cfg.h"
#include "COM_Cbk.h"
#include "CtApPCOM_TX_cfg.h"
#include "CtApPCOM_RX_cfg.h"

/*Rule 8.7: The function is only referenced in one translation unit - but not 
 * the one in which it is defined*/
/*PRQA S 1531 ++ #This is done to keep the isolation between modules.*/

const PCOM_RX_Grid_ErrorIndication_t PCOM_RX_Grid_ErrorIndication [PCOM_CAN_NUMBER] =    /* PRQA S 1533 */
    {
	NULL_PTR,
	PCOM_InternalCAN_OBC_ErrorIndication,
	PCOM_InternalCAN_LVC_ErrorIndication
    };


#define PCOM_FRAME_5MSTASK_ITERATION_20MS 	 (1U)
#define PCOM_FRAME_5MSTASK_ITERATION_10MS 	 (0U)
#define PCOM_FRAME_5MSTASK_ITERATION_50MS 	 (4U)
#define PCOM_FRAME_5MSTASK_ITERATION_100MS 	 (9U)			/* PRQA S 0791 # Macro name similar to other macro names. Kept for clarity */
#define PCOM_FRAME_5MSTASK_ITERATION_1000MS 	 (99U)		/* PRQA S 0791 # Macro name similar to other macro names. Kept for clarity */
#define PCOM_FRAME_5MSTASK_ITERATION_UNIQUE	 (0U)

/*Framelost confimation time for internal CAN*/
#define PCOM_INTERNAL_FRAMELOST							50U



/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/


/* - Static non-init variables declaration ---------- */
#define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* NOTE: 'static' storage-class specifier is not required here due to 
       this variable is defined as external at 'ctApPCOM_cfg.h' header file */
PCOM_RX_Signals_Data_t PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[PCOM_RX_TOTAL_SIGNALS];

/* NOTE: 'static' storage-class specifier is not required here due to 
       this variable is defined as external at 'ctApPCOM_cfg.h' header file */
uint16 PCOM_BMS_SOC_Initial;
uint32 PCOM_KILOMETRAGE_Initial;
uint8 PCOM_VCU_Keyposition_Initial;



#define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* NOTE: 'static' storage-class specifier is not required here due to 
       this variable is defined as external at 'ctApPCOM_cfg.h' header file */
PCOM_Signals_diagnostic_t PCOM_DiagEnable_E_VCU = PCOM_TX_DIAGNOSTIC_FORBIDDEN;
PCOM_Signals_diagnostic_t PCOM_DiagEnable_TBMU = PCOM_TX_DIAGNOSTIC_FORBIDDEN;
PCOM_Signals_diagnostic_t PCOM_DiagEnable_BSI = PCOM_TX_DIAGNOSTIC_FORBIDDEN;

/* static PCOM_Signals_diagnostic_t PCOM_DiagEnable_Permited = PCOM_TX_DIAGNOSTIC_RUNNING; */
static PCOM_Signals_diagnostic_t PCOM_DiagEnable_Forbidden = PCOM_TX_DIAGNOSTIC_FORBIDDEN;

#define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


 /* PRQA S 0857,0380 -- */

boolean PCOM_Frame_5b1_SendEvent = FALSE;
boolean PCOM_Frame_0ce_SendEvent = FALSE;


static PCOM_Signals_enable_t  PCOM_Signals_enable_default(void);


/**
 * \defGroup PCOM_TX_SIG TX signals functions
 * \{
 */

static const PCOM_TX_Signals_Config_t PCOM_Signals_DC1_345[PCOM_TX_FRAMES_DC1_345_SIGNALS_COUNT] =
    {
	/* SIGNAL 'DCDC_Temperature' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,
	    {
		(getValFuncPtr) PCOM_TX__DCDC_Temperature_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_Temperature_GetInitial,
		PCOM_TX_ProducerError_DCDC_Temperature_DCDC_Temperature
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFF
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		254,
		0
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_Temperature_Set
	},
	/* SIGNAL 'DCDC_Status' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_Status_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_Status_GetInitial,
		PCOM_TX_ProducerError_DCDC_Status_DCDC_Status
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x7
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_Status_Set
	},
	/* SIGNAL 'DCDC_FaultLampRequest' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_FaultLampRequest_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_FaultLampRequest_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_FaultLampRequest_Set
	},
	/* SIGNAL 'DCDC_HighVoltConnectionAllowed' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_HighVoltConnectionAllowed_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_HighVoltConnectionAllowed_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_HighVoltConnectionAllowed_Set
	},
	/* SIGNAL 'DCDC_Fault' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,
	    {
		(getValFuncPtr) PCOM_TX__DCDC_Fault_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_Fault_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		255,
		0
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_Fault_Set
	},
	/* SIGNAL 'DCDC_InputVoltage' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_InputVoltage_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_InputVoltage_GetInitial,
		PCOM_TX_ProducerError_DCDC_InputVoltage_DCDC_InputVoltage
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x1FF,
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		500,
		0
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_InputVoltage_Set
	},
	/* SIGNAL 'DCDC_OVERTEMP' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_OVERTEMP_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_OVERTEMP_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_OVERTEMP_Set
	},
	/* SIGNAL 'DCDC_OutputVoltage' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_OutputVoltage_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_OutputVoltage_GetInitial,
		PCOM_TX_ProducerError_DCDC_OutputVoltage_DCDC_OutputVoltage
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFFU,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		175,
		0
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_OutputVoltage_Set
	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_DC2_0C5[PCOM_TX_FRAMES_DC2_0C5_SIGNALS_COUNT] =
    {
	/* SIGNAL 'DCDC_ActivedischargeSt' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_ActivedischargeSt_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_ActivedischargeSt_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_ActivedischargeSt_Set
	},
	/* SIGNAL 'DCDC_OutputCurrent' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_OutputCurrent_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_OutputCurrent_GetInitial,
		PCOM_TX_ProducerError_DCDC_OutputCurrent_DCDC_OutputCurrent
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFFFU,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		2540,
		0
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_OutputCurrent_Set
	},
	/* SIGNAL 'DCDC_InputCurrent' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__DCDC_InputCurrent_GetCalculated,
		(getValFuncPtr) PCOM_TX__DCDC_InputCurrent_GetInitial,
		PCOM_TX_ProducerError_DCDC_InputCurrent_DCDC_InputCurrent
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFF,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		254,
		0
	    },
	    (getValFuncPtr) PCOM_TX__DCDC_InputCurrent_Set
	},
	/* SIGNAL 'OBC_DCDC_RT_POWER_LOAD' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_DCDC_RT_POWER_LOAD_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_DCDC_RT_POWER_LOAD_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		100,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_DCDC_RT_POWER_LOAD_Set
	},
	/* SIGNAL 'OBC_MAIN_CONTACTOR_REQ' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_MAIN_CONTACTOR_REQ_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_MAIN_CONTACTOR_REQ_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_MAIN_CONTACTOR_REQ_Set
	},
	/* SIGNAL 'OBC_QUICK_CHARGE_CONTACTOR_REQ' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_Set
	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_NEW_JDD_OBC_DCDC_5B1[PCOM_TX_FRAMES_NEW_JDD_OBC_DCDC_5B1_SIGNALS_COUNT] =
    {
	/* SIGNAL 'JDD_ BYTE 0' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE0_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE0_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE0_Set
	},
	{ /* SIGNAL 'JDD_ BYTE 1' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE1_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE1_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE1_Set
	},
	{/* SIGNAL 'JDD_ BYTE 2' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE2_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE2_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE2_Set
	},
	{/* SIGNAL 'JDD_ BYTE 3' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE3_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE3_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE3_Set
	},
	{/* SIGNAL 'JDD_ BYTE 4' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE4_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE4_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE4_Set
	},
	{/* SIGNAL 'JDD_ BYTE 5' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE5_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE5_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE5_Set
	},
	{/* SIGNAL 'JDD_ BYTE 6' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE6_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE6_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE6_Set
	},
	{/* SIGNAL 'JDD_ BYTE 7' */
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__JDD_BYTE7_GetCalculated,
		(getValFuncPtr) PCOM_TX__JDD_BYTE7_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_JDD_BYTE7_Set
	}
    };


static const PCOM_TX_Signals_Config_t PCOM_Signals_OBC1_3A3[PCOM_TX_FRAMES_OBC1_3A3_SIGNALS_COUNT] =
    {
	/* SIGNAL 'OBC_SocketTempL' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_SocketTempL_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_SocketTempL_GetInitial,
		PCOM_TX_ProducerError_OBC_SocketTempL_OBC_SocketTempL
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFF,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		190,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_SocketTempL_Set
	},
	/* SIGNAL 'OBC_SocketTempN' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_SocketTempN_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_SocketTempN_GetInitial,
		PCOM_TX_ProducerError_OBC_SocketTempN_OBC_SocketTempN
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFF,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		190,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_SocketTempN_Set
	},
	/* SIGNAL 'OBC_HighVoltConnectionAllowed' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_HighVoltConnectionAllowed_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_HighVoltConnectionAllowed_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_HighVoltConnectionAllowed_Set
	},
	/* SIGNAL 'OBC_ElockState' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_ElockState_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_ElockState_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_ElockState_Set
	},
	/* SIGNAL 'OBC_ChargingMode' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_ChargingMode_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_ChargingMode_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_ChargingMode_Set
	},
	/* SIGNAL 'OBC_Fault' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_Fault_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_Fault_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		255,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_Fault_Set
	},
	/* SIGNAL 'OBC_Status' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_Status_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_Status_GetInitial,
		PCOM_TX_ProducerError_OBC_Status_OBC_Status
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x111,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_Status_Set
	},
	/* SIGNAL 'EVSE_RTAB_STOP_CHARGE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__EVSE_RTAB_STOP_CHARGE_GetCalculated,
		(getValFuncPtr) PCOM_TX__EVSE_RTAB_STOP_CHARGE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__EVSE_RTAB_STOP_CHARGE_Set
	},
	/* SIGNAL 'OBC_CP_connection_Status' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_CP_connection_Status_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_CP_connection_Status_GetInitial,
		PCOM_TX_ProducerError_OBC_CP_connection_Status_OBC_CP_connection_Status
	},
	    {
		PCOM_TX_RESERVED_NA,
		0x00,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_CP_connection_Status_Set
	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_OBC2_3A2[PCOM_TX_FRAMES_OBC2_3A2_SIGNALS_COUNT] =
    {
	/* SIGNAL 'OBC_OutputVoltage' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_OutputVoltage_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_OutputVoltage_GetInitial,
		PCOM_TX_ProducerError_OBC_OutputVoltage_OBC_OutputVoltage
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x1FFFU,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		6000,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_OutputVoltage_Set
	},
	/* SIGNAL 'OBC_OutputCurrent' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_OutputCurrent_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_OutputCurrent_GetInitial,
		PCOM_TX_ProducerError_OBC_OutputCurrent_OBC_OutputCurrent
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x1FFU,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		350,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_OutputCurrent_Set
	},
	/* SIGNAL 'OBC_InputVoltageSt' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__OBC_InputVoltageSt_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_InputVoltageSt_GetInitial,
		PCOM_TX_ProducerError_OBC_InputVoltageSt_OBC_InputVoltageSt
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x3
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_InputVoltageSt_Set
	},
	/* SIGNAL 'OBC_CommunicationSt' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_CommunicationSt_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_CommunicationSt_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_CommunicationSt_Set
	},
	/* SIGNAL 'OBC_ACRange' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_ACRange_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_ACRange_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_ACRange_Set
	},
	/* SIGNAL 'OBC_OBCTemp' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_OBCTemp_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_OBCTemp_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		255,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_OBCTemp_Set
	},
	/* SIGNAL 'OBC_PowerMax' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_UNM,

	    {
		(getValFuncPtr) PCOM_TX__OBC_PowerMax_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_PowerMax_GetInitial,
		PCOM_TX_ProducerError_OBC_PowerMax_OBC_PowerMax
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0xFF,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		250,
		0
	    },
	    (getValFuncPtr) PCOM_TX__OBC_PowerMax_Set
	},
	/* SIGNAL 'OBC_OBCStartSt' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_OBCStartSt_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_OBCStartSt_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_OBCStartSt_Set
	},
	/* SIGNAL 'OBC_PLUG_VOLT_DETECTION' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_PLUG_VOLT_DETECTION_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_PLUG_VOLT_DETECTION_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_PLUG_VOLT_DETECTION_Set
	},
	/* SIGNAL 'OBC_ChargingConnectionConfirmation' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_ChargingConnectionConfirmation_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_ChargingConnectionConfirmation_GetInitial,
		PCOM_TX_ProducerError_OBC_ChargingConnectionConfirmation_OBC_ChargingConnectionConfirmation
	    },
	    {
		PCOM_TX_RESERVED_NA,
		0x00,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_ChargingConnectionConfirmation_Set
	},
	/* SIGNAL 'OBC_DCChargingPlugAConnectionConfirmation' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_Set
	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_OBC3_230[PCOM_TX_FRAMES_OBC3_230_SIGNALS_COUNT] =
    {
	/* SIGNAL 'OBC_HVBattRechargeWakeup' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_HVBattRechargeWakeup_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_HVBattRechargeWakeup_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_HVBattRechargeWakeup_Set
	},
	/* SIGNAL 'OBC_PIStateInfoWakeup' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__OBC_PIStateInfoWakeup_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_PIStateInfoWakeup_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_PIStateInfoWakeup_Set
	},
	/* SIGNAL 'OBC_HoldDiscontactorWakeup' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_HoldDiscontactorWakeup_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_HoldDiscontactorWakeup_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_HoldDiscontactorWakeup_Set
	},
	/* SIGNAL 'OBC_COOLING_WAKEUP' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__OBC_COOLING_WAKEUP_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_COOLING_WAKEUP_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_COOLING_WAKEUP_Set

	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_VERS_OBC_DCDC_0CE[PCOM_TX_FRAMES_VERS_OBC_DCDC_0CE_COUNT] =
{
		/* SIGNAL 'VERSION_SYSTEME' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__VERSION_SYSTEME_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__VERSION_SYSTEME_Set
		},
		/* SIGNAL 'VERS_DATE2_JOUR' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__VERS_DATE2_JOUR_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__VERS_DATE2_JOUR_Set
		},
		/* SIGNAL 'VERS_DATE2_MOIS' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__VERS_DATE2_MOIS_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__VERS_DATE2_MOIS_Set
		},
		/* SIGNAL 'VERS_DATE2_ANNEE' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__VERS_DATE2_ANNEE_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__VERS_DATE2_ANNEE_Set
		},
		/* SIGNAL 'VERSION_APPLI' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__VERSION_APPLI_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__VERSION_APPLI_Set
		},
		/* SIGNAL 'VERSION_SOFT' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__VERSION_SOFT_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__VERSION_SOFT_Set
		},
		/* SIGNAL 'EDITION_SOFT' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__EDITION_SOFT_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__EDITION_SOFT_Set
		},
		/* SIGNAL 'EDITION_CALIB' */
		{
		    PCOM_Signals_enable_default,
		    PCOM_SIGNAL_TYPE_U8,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    PCOM_CANSIGNAL_TYPE_BMP,

		    {
			(getValFuncPtr) PCOM_TX__EDITION_CALIB_GetCalculated,
			NULL_PTR,
			PCOM_FEATURE_NA
		    },
		    {
			PCOM_TX_RESERVED_NA,
			PCOM_TX_INVALID_NA,

		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO,
			PCOM_SIGNAL_LIMITS_NA,
			PCOM_SIGNAL_LIMITS_NA
		    },
		    (getValFuncPtr) PCOM_TX__EDITION_CALIB_Set
		}
};

static const PCOM_TX_Signals_Config_t PCOM_Signals_OBC4_439[PCOM_TX_FRAMES_OBC4_439_SIGNALS_COUNT] =
    {
	/* SIGNAL 'RECHARGE_HMI_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__RECHARGE_HMI_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__RECHARGE_HMI_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__RECHARGE_HMI_STATE_Set
	},
	/* SIGNAL 'OBC_PUSH_CHARGE_TYPE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__OBC_PUSH_CHARGE_TYPE_GetCalculated,
		(getValFuncPtr) PCOM_TX__OBC_PUSH_CHARGE_TYPE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__OBC_PUSH_CHARGE_TYPE_Set
	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_FRAMEPFC[PCOM_TX_FRAMES_FramePFC_SIGNALS_COUNT] =
    {
	/* SIGNAL 'SUP_RequestPFCStatus' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_SUP_RequestPFCStatus_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_SUP_RequestPFCStatus_Set
	},
	/* SIGNAL 'SUP_CommandVDCLink_V' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_SUP_CommandVDCLink_V_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_SUP_CommandVDCLink_V_Set
	}
    };


static const PCOM_TX_Signals_Config_t PCOM_Signals_FrameDCLV[PCOM_TX_FRAMES_FrameDCLV_SIGNALS_COUNT] =
    {
	/* SIGNAL 'RequestDCLVStatus' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_SUP_RequestDCLVStatus_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_SUP_RequestDCLVStatus_Set
	},
	/* SIGNAL 'DCLV_VoltageReference' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_DCLV_VoltageReference_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_DCLV_VoltageReference_Set
	},
	/* SIGNAL 'DCLV_Temp_Derating_Factor' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_DCLV_Temp_Derating_Factor_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_DCLV_Temp_Derating_Factor_Set
	}
    };


static const PCOM_TX_Signals_Config_t PCOM_Signals_FrameDCHV[PCOM_TX_FRAMES_FrameDCHV_SIGNALS_COUNT] =
    {
	/* SIGNAL 'SUP_RequestStatusDCHV' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_SUP_RequestStatusDCHV_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_SUP_RequestStatusDCHV_Set
	},
	/* SIGNAL 'DCDC_VoltageReference' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_DCDC_VoltageReference_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_DCDC_VoltageReference_Set
	},
	/* SIGNAL 'DCDC_CurrentReference' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U16,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_NA,
	    {
		(getValFuncPtr) PCOM_TX_DCDC_CurrentReference_GetCalculated,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX_DCDC_CurrentReference_Set
	}
    };

static const PCOM_TX_Signals_Config_t PCOM_Signals_SUPV_V2_OBC_DCDC_591[PCOM_TX_FRAMES_SUPV_V2_OBC_DCDC_591_SIGNALS_COUNT] =
    {
	/* SIGNAL 'STOP_DELAYED_HMI_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_Set
	},
	/* SIGNAL 'HV_BATT_CHARGE_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__HV_BATT_CHARGE_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__HV_BATT_CHARGE_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__HV_BATT_CHARGE_WUP_STATE_Set
	},
	/* SIGNAL 'PI_STATE_INFO_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__PI_STATE_INFO_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__PI_STATE_INFO_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__PI_STATE_INFO_WUP_STATE_Set
	},
	/* SIGNAL 'PRE_DRIVE_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__PRE_DRIVE_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__PRE_DRIVE_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__PRE_DRIVE_WUP_STATE_Set
	},
	/* SIGNAL 'POST_DRIVE_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__POST_DRIVE_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__POST_DRIVE_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__POST_DRIVE_WUP_STATE_Set
	},
	/* SIGNAL 'HOLD_DISCONTACTOR_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_Set
	},
	/* SIGNAL 'PRECOND_ELEC_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__PRECOND_ELEC_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__PRECOND_ELEC_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__PRECOND_ELEC_WUP_STATE_Set
	},
	/* SIGNAL 'DTC_REGISTRED' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__DTC_REGISTRED_GetCalculated,
		(getValFuncPtr) PCOM_TX__DTC_REGISTRED_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__DTC_REGISTRED_Set
	},
	/* SIGNAL 'ECU_ELEC_STATE_RCD' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_U8,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__ECU_ELEC_STATE_RCD_GetCalculated,
		(getValFuncPtr) PCOM_TX__ECU_ELEC_STATE_RCD_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__ECU_ELEC_STATE_RCD_Set
	},
	/* SIGNAL 'RCD_LINE_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,

	    {
		(getValFuncPtr) PCOM_TX__RCD_LINE_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__RCD_LINE_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__RCD_LINE_STATE_Set
	},
	/* SIGNAL 'COOLING_WUP_STATE' */
	{
	    PCOM_Signals_enable_default,
	    PCOM_SIGNAL_TYPE_BOOL,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    PCOM_CANSIGNAL_TYPE_BMP,
	    {
		(getValFuncPtr) PCOM_TX__COOLING_WUP_STATE_GetCalculated,
		(getValFuncPtr) PCOM_TX__COOLING_WUP_STATE_GetInitial,
		PCOM_FEATURE_NA
	    },
	    {
		PCOM_TX_RESERVED_NA,
		PCOM_TX_INVALID_NA,

	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO,
		PCOM_SIGNAL_LIMITS_NA,
		PCOM_SIGNAL_LIMITS_NA
	    },
	    (getValFuncPtr) PCOM_TX__COOLING_WUP_STATE_Set
	}
    };


const PCOM_TX_Frame_Config_t PCOM_TX_Frame_Config[PCOM_NUM_TX_FRAMES] =
    {
	{
	    TRUE,
	    ComConf_ComIPdu_DC1_oE_CAN_fcac9ee4_Tx,
	    PCOM_PERIODIC,
	    TRUE,
	    TRUE,
	    7U,
	    14U,
	    0x0C,
	    PCOM_TX_FRAMES_DC1_TIMEOUT,
	    NULL_PTR,
	    PCOM_FrameNum345_SignalUpdatedFunction,
	    {
		PCOM_Signals_DC1_345,
		PCOM_TX_FRAMES_DC1_345_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_DC2_oE_CAN_72239907_Tx,
	    PCOM_PERIODIC,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0x00,
	    PCOM_TX_FRAMES_DC2_TIMEOUT,
	    NULL_PTR,
	    NULL_PTR,
	    {
		PCOM_Signals_DC2_0C5,
		PCOM_TX_FRAMES_DC2_0C5_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_OBC1_oE_CAN_89fd90e2_Tx,
	    PCOM_PERIODIC,
	    TRUE,
	    TRUE,
	    14U,
	    15U,
	    0x00,
	    PCOM_TX_FRAMES_OBC1_TIMEOUT,
	    NULL_PTR,
	    PCOM_FrameNum3A3_SignalUpdatedFunction,
	    {
		PCOM_Signals_OBC1_3A3,
		PCOM_TX_FRAMES_OBC1_3A3_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_OBC2_oE_CAN_07729701_Tx,
	    PCOM_PERIODIC,
	    TRUE,
	    TRUE,
	    12U,
	    13U,
	    0x0F,
	    PCOM_TX_FRAMES_OBC2_TIMEOUT,
	    NULL_PTR,
	    PCOM_FrameNum3A2_SignalUpdatedFunction,
	    {
		PCOM_Signals_OBC2_3A2,
		PCOM_TX_FRAMES_OBC2_3A2_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_OBC3_oE_CAN_cbd8979f_Tx,
	    PCOM_PERIODIC,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0x00,
	    PCOM_TX_FRAMES_OBC3_TIMEOUT,
	    NULL_PTR,
	    NULL_PTR,
	    {
		PCOM_Signals_OBC3_230,
		PCOM_TX_FRAMES_OBC3_230_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx,
	    PCOM_EVENT,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0x00,
	    PCOM_TX_FRAMES_NA_TIMEOUT,
	    PCOM_FrameNum0CE_EventTriggerFunction,
	    NULL_PTR,
	    {
	    PCOM_Signals_VERS_OBC_DCDC_0CE,
		PCOM_TX_FRAMES_VERS_OBC_DCDC_0CE_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx,
	    PCOM_EVENT,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0x00,
	    PCOM_TX_FRAMES_NA_TIMEOUT,
	    PCOM_FrameNum5B1_EventTriggerFunction,
	    NULL_PTR,
	    {
		PCOM_Signals_NEW_JDD_OBC_DCDC_5B1,
		PCOM_TX_FRAMES_NEW_JDD_OBC_DCDC_5B1_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_OBC4_oE_CAN_c11d9e86_Tx,
	    PCOM_MIXED,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0x00,
	    PCOM_TX_FRAMES_OBC4_TIMEOUT,
	    PCOM_FrameNum439_EventTriggerFunction,
	    NULL_PTR,
	    {
		PCOM_Signals_OBC4_439,
		PCOM_TX_FRAMES_OBC4_439_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_SUPV_V2_OBC_DCDC_oE_CAN_f7542654_Tx,
	    PCOM_MIXED,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0x00,
	    PCOM_TX_FRAMES_SUPV_V2_OBC_TIMEOUT,
	    PCOM_FrameNum591_EventTriggerFunction,
	    NULL_PTR,
	    {
		PCOM_Signals_SUPV_V2_OBC_DCDC_591,
		PCOM_TX_FRAMES_SUPV_V2_OBC_DCDC_591_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_SUP_CommandToDCHV_oInt_CAN_8f40604c_Tx,
	    PCOM_PERIODIC,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_TX_FRAMES_NA_TIMEOUT,
	    NULL_PTR,
	    PCOM_FrameDCHV_SignalUpdatedFunction,
	    {
		PCOM_Signals_FrameDCHV,
		PCOM_TX_FRAMES_FrameDCHV_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_SUP_CommandToPFC_oInt_CAN_d9411c97_Tx,
	    PCOM_PERIODIC,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_TX_FRAMES_NA_TIMEOUT,
	    NULL_PTR,
	    PCOM_FramePFC_SignalUpdatedFunction,
	    {
		PCOM_Signals_FRAMEPFC,
		PCOM_TX_FRAMES_FramePFC_SIGNALS_COUNT
	    }
	},
	{
	    TRUE,
	    ComConf_ComIPdu_SUP_CommandToDCLV_oInt_CAN_3f9913ce_Tx,
	    PCOM_PERIODIC,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_TX_FRAMES_NA_TIMEOUT,
	    NULL_PTR,
	    PCOM_FrameDCLV_SignalUpdatedFunction,
	    {
		PCOM_Signals_FrameDCLV,
		PCOM_TX_FRAMES_FrameDCLV_SIGNALS_COUNT
	    }
	}
    };

/**
 * \defGroup PCOM_RX_SIG RX signals functions
 * \{
 */

static const PCOM_RX_Signals_Config_t PCOM_Signals_BMS1_125[PCOM_RX_FRAMES_BMS1_125_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BMS_SOC' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_SOC_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_SOC_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_SOC_GetPhysical,
		PCOM_RX__BMS_SOC_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_SOC_GetDefaultSubstValue,
		PCOM_RX__BMS_SOC_StubForbidden,
		{
		    TRUE,
		    0x3FF
		},
		{
		    TRUE,
		    0x3FE
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		1000,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_RX__BMS_SOC_invalid_timeConfirm,
		    PCOM_RX__BMS_SOC_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_RX__BMS_SOC_forbidden_timeConfirm,
		    PCOM_RX__BMS_SOC_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0]
	},
	{
	    /* SIGNAL 'BMS_Voltage' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_Voltage_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_Voltage_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_Voltage_GetPhysical,
		PCOM_RX__BMS_Voltage_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_Voltage_GetDefaultSubstValue,
		PCOM_RX__BMS_Voltage_StubForbidden,
		{
		    TRUE,
		    0x1FF
		},
		{
		    TRUE,
		    0x1FE
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		500,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_RX__BMS_Voltage_invalid_timeConfirm,
		    PCOM_RX__BMS_Voltage_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_RX__BMS_Voltage_forbidden_timeConfirm,
		    PCOM_RX__BMS_Voltage_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1]
	},
	{
	    /* SIGNAL 'BMS_RelayOpenReq' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_RelayOpenReq_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_RelayOpenReq_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_RelayOpenReq_GetPhysical,
		PCOM_RX__BMS_RelayOpenReq_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_RelayOpenReq_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[2]
	},
	{
	    /* SIGNAL 'BMS_MainConnectorState' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_MainConnectorState_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_MainConnectorState_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_MainConnectorState_GetPhysical,
		PCOM_RX__BMS_MainConnectorState_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_MainConnectorState_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0x3
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_RX__BMS_MainConnectorState_invalid_timeConfirm,
		    PCOM_RX__BMS_MainConnectorState_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[3]
	},
	{
	    /* SIGNAL 'BMS_QuickChargeConnectorState' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_QuickChargeConnectorState_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_QuickChargeConnectorState_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_QuickChargeConnectorState_GetPhysical,
		PCOM_RX__BMS_QuickChargeConnectorState_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_QuickChargeConnectorState_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[4]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_BMS3_127[PCOM_RX_FRAMES_BMS3_127_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BMS_AuxBattVolt' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_AuxBattVolt_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_AuxBattVolt_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_AuxBattVolt_GetPhysical,
		PCOM_RX__BMS_AuxBattVolt_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_AuxBattVolt_GetDefaultSubstValue,
		PCOM_RX__BMS_AuxBattVolt_StubForbidden,
		{
		    TRUE,
		    0xFFF
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		2400,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_RX__BMS_AuxBattVolt_invalid_timeConfirm,
		    PCOM_RX__BMS_AuxBattVolt_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_TBMU,
			PCOM_RX__BMS_AuxBattVolt_forbidden_timeConfirm,
			PCOM_RX__BMS_AuxBattVolt_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[5]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_BMS5_359[PCOM_RX_FRAMES_BMS5_359_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BMS_Fault' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_Fault_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_Fault_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_Fault_GetPhysical,
		PCOM_RX__BMS_Fault_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_Fault_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		4095,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[6]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_BMS6_361[PCOM_RX_FRAMES_BMS6_361_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BMS_HighestChargeVoltageAllow' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeVoltageAllow_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeVoltageAllow_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeVoltageAllow_GetPhysical,
		PCOM_RX__BMS_HighestChargeVoltageAllow_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeVoltageAllow_GetDefaultSubstValue,
		PCOM_RX__BMS_HighestChargeVoltageAllow_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		6000,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
			PCOM_RX__BMS_HighestChargeVoltageAllow_forbidden_timeConfirm,
			PCOM_RX__BMS_HighestChargeVoltageAllow_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[7]
	},
	{
	    /* SIGNAL 'BMS_HighestChargeCurrentAllow' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeCurrentAllow_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeCurrentAllow_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeCurrentAllow_GetPhysical,
		PCOM_RX__BMS_HighestChargeCurrentAllow_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_HighestChargeCurrentAllow_GetDefaultSubstValue,
		PCOM_RX__BMS_HighestChargeCurrentAllow_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		4000,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
			PCOM_RX__BMS_HighestChargeCurrentAllow_forbidden_timeConfirm,
			PCOM_RX__BMS_HighestChargeCurrentAllow_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[8]
	},
	{
	    /* SIGNAL 'BMS_OnBoardChargerEnable' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_OnBoardChargerEnable_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_OnBoardChargerEnable_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_OnBoardChargerEnable_GetPhysical,
		PCOM_RX__BMS_OnBoardChargerEnable_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_OnBoardChargerEnable_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[9]
	},
	{
	    /* SIGNAL 'BMS_SlowChargeSt' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_SlowChargeSt_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_SlowChargeSt_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_SlowChargeSt_GetPhysical,
		PCOM_RX__BMS_SlowChargeSt_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_SlowChargeSt_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[10]
	},
	{
	    /* SIGNAL 'BMS_FastChargeSt' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_FastChargeSt_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_FastChargeSt_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_FastChargeSt_GetPhysical,
		PCOM_RX__BMS_FastChargeSt_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_FastChargeSt_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[11]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_BMS8_31B[PCOM_RX_FRAMES_BMS8_31B_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BMS_CC2_connection_Status' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_CC2_connection_Status_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_CC2_connection_Status_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_CC2_connection_Status_GetPhysical,
		PCOM_RX__BMS_CC2_connection_Status_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_CC2_connection_Status_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[12]
	}
#if 0
	,
	{
	    /* SIGNAL 'BMS_InterlockLineState' */
	    /* TODO review signal */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_NOT_SPECIFIED_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_InterlockLineState_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_InterlockLineState_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_InterlockLineState_GetPhysical,
		PCOM_RX__BMS_InterlockLineState_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_InterlockLineState_GetDefaultSubstValue,
		PCOM_RX__BMS_InterlockLineState_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    PCOM_RX__BMS_InterlockLineState_forbidden_timeConfirm,
		    PCOM_RX__BMS_InterlockLineState_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[13]
	},
	{
	    /* SIGNAL 'BMS_DisconnectCause' */
	    /* TODO review signal */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_NOT_SPECIFIED_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_DisconnectCause_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_DisconnectCause_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_DisconnectCause_GetPhysical,
		PCOM_RX__BMS_DisconnectCause_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_DisconnectCause_GetDefaultSubstValue,
		PCOM_RX__BMS_DisconnectCause_StubForbidden,
		{
		    TRUE,
		    0x7
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    PCOM_RX__BMS_DisconnectCause_invalid_timeConfirm,
		    PCOM_RX__BMS_DisconnectCause_invalid_timeHealed
		},
		{
		    PCOM_RX__BMS_DisconnectCause_forbidden_timeConfirm,
		    PCOM_RX__BMS_DisconnectCause_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[14]
	},
	{
	    /* SIGNAL 'BMS_InsulationFault' */
	    /* TODO review signal, nothing found in CtApPCOM.c */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_NOT_SPECIFIED_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_InsulationFault_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_InsulationFault_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_InsulationFault_GetPhysical,
		PCOM_RX__BMS_InsulationFault_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_InsulationFault_GetDefaultSubstValue,
		PCOM_RX__BMS_InsulationFault_StubForbidden,
		{
		    TRUE,
		    0x4
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    PCOM_RX__BMS_InsulationFault_invalid_timeConfirm,
		    PCOM_RX__BMS_InsulationFault_invalid_timeHealed
		},
		{
		    PCOM_RX__BMS_InsulationFault_forbidden_timeConfirm,
		    PCOM_RX__BMS_InsulationFault_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[15]
	}
#endif
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_BMS9_129[PCOM_RX_FRAMES_BMS9_129_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BMS_DC_RELAY_VOLTAGE' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BMS_DC_RELAY_VOLTAGE_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetInitial,
		(getValFuncPtr) PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetPhysical,
		PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetDefaultSubstValue,
		PCOM_RX__BMS_DC_RELAY_VOLTAGE_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		500,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_TBMU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_TBMU,
			PCOM_RX__BMS_DC_RELAY_VOLTAGE_forbidden_timeConfirm,
			PCOM_RX__BMS_DC_RELAY_VOLTAGE_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[16]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_BSIInfo_382[PCOM_RX_FRAMES_BSIInfo_382_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'BSI_MainWakeup' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BSI_MainWakeup_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BSI_MainWakeup_GetInitial,
		(getValFuncPtr) PCOM_RX__BSI_MainWakeup_GetPhysical,
		PCOM_RX__BSI_MainWakeup_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BSI_MainWakeup_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    TRUE,
		    0x3
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[17]
	},
	{
	    /* SIGNAL 'BSI_LockedVehicle' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BSI_LockedVehicle_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BSI_LockedVehicle_GetInitial,
		(getValFuncPtr) PCOM_RX__BSI_LockedVehicle_GetPhysical,
		PCOM_RX__BSI_LockedVehicle_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BSI_LockedVehicle_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[18]
	},
	{
	    /* SIGNAL 'BSI_ChargeTypeStatus' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BSI_ChargeTypeStatus_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BSI_ChargeTypeStatus_GetInitial,
		(getValFuncPtr) PCOM_RX__BSI_ChargeTypeStatus_GetPhysical,
		PCOM_RX__BSI_ChargeTypeStatus_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BSI_ChargeTypeStatus_GetDefaultSubstValue,
		PCOM_RX__BSI_ChargeTypeStatus_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_RX__BSI_ChargeTypeStatus_forbidden_timeConfirm,
		    PCOM_RX__BSI_ChargeTypeStatus_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[19]
	},
	{
	    /* SIGNAL 'BSI_PreDriveWakeup' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BSI_PreDriveWakeup_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BSI_PreDriveWakeup_GetInitial,
		(getValFuncPtr) PCOM_RX__BSI_PreDriveWakeup_GetPhysical,
		PCOM_RX__BSI_PreDriveWakeup_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BSI_PreDriveWakeup_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[20]
	},
	{
	    /* SIGNAL 'BSI_PostDriveWakeup' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__BSI_PostDriveWakeup_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__BSI_PostDriveWakeup_GetInitial,
		(getValFuncPtr) PCOM_RX__BSI_PostDriveWakeup_GetPhysical,
		PCOM_RX__BSI_PostDriveWakeup_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__BSI_PostDriveWakeup_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[21]
	},
	{
	    /* SIGNAL 'VCU_SYNCHRO_GPC' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_SYNCHRO_GPC_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_SYNCHRO_GPC_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_SYNCHRO_GPC_GetPhysical,
		PCOM_RX__VCU_SYNCHRO_GPC_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_SYNCHRO_GPC_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[22]
	},
	{
	    /* SIGNAL 'VCU_HEAT_PUMP_WORKING_MODE' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetPhysical,
		PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetDefaultSubstValue,
		PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_forbidden_timeConfirm,
		    PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[23]
	},
	{
	    /* SIGNAL 'CHARGE_STATE' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__CHARGE_STATE_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__CHARGE_STATE_GetInitial,
		(getValFuncPtr) PCOM_RX__CHARGE_STATE_GetPhysical,
		PCOM_RX__CHARGE_STATE_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__CHARGE_STATE_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    TRUE,
		    0x3
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[24]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_CtrlDCDC_372[PCOM_RX_FRAMES_CtrlDCDC_372_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'VCU_DCDCVoltageReq' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_DCDCVoltageReq_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_DCDCVoltageReq_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_DCDCVoltageReq_GetPhysical,
		PCOM_RX__VCU_DCDCVoltageReq_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_DCDCVoltageReq_GetDefaultSubstValue,
		PCOM_RX__VCU_DCDCVoltageReq_StubForbidden,
		{
		    TRUE,
		    0x7F
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		108,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__VCU_DCDCVoltageReq_invalid_timeConfirm,
		    PCOM_RX__VCU_DCDCVoltageReq_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
			PCOM_RX__VCU_DCDCVoltageReq_forbidden_timeConfirm,
			PCOM_RX__VCU_DCDCVoltageReq_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25]
	},
	{
	    /* SIGNAL 'VCU_ActivedischargeCommand' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_ActivedischargeCommand_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_ActivedischargeCommand_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_ActivedischargeCommand_GetPhysical,
		PCOM_RX__VCU_ActivedischargeCommand_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_ActivedischargeCommand_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[26]
	},
	{
	    /* SIGNAL 'VCU_DCDCActivation' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_DCDCActivation_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_DCDCActivation_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_DCDCActivation_GetPhysical,
		PCOM_RX__VCU_DCDCActivation_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_DCDCActivation_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0x3
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_BSI,
			PCOM_RX__VCU_DCDCActivation_invalid_timeConfirm,
			PCOM_RX__VCU_DCDCActivation_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_BSI,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[27]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_ELECTRON_BSI_092[PCOM_RX_FRAMES_ELECTRON_BSI_092_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DIAG_INTEGRA_ELEC' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DIAG_INTEGRA_ELEC_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__DIAG_INTEGRA_ELEC_GetInitial,
		(getValFuncPtr) PCOM_RX__DIAG_INTEGRA_ELEC_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[28]
	},
	{
	    /* SIGNAL 'EFFAC_DEFAUT_DIAG' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__EFFAC_DEFAUT_DIAG_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__EFFAC_DEFAUT_DIAG_GetInitial,
		(getValFuncPtr) PCOM_RX__EFFAC_DEFAUT_DIAG_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[29]
	},
	{
	    /* SIGNAL 'MODE_DIAG' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__MODE_DIAG_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__MODE_DIAG_GetInitial,
		(getValFuncPtr) PCOM_RX__MODE_DIAG_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[30]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_NEW_JDD_55F[PCOM_RX_FRAMES_NEW_JDD_55F_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DATA_ACQ_JDD_BSI_2' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
		PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DATA_ACQ_JDD_BSI_2_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__DATA_ACQ_JDD_BSI_2_GetInitial,
		(getValFuncPtr) PCOM_RX__DATA_ACQ_JDD_BSI_2_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
		},
		{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[31]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_ParkCommand_31E[PCOM_RX_FRAMES_ParkCommand_31E_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'VCU_EPWT_Status' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_EPWT_Status_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_EPWT_Status_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_EPWT_Status_GetPhysical,
		PCOM_RX__VCU_EPWT_Status_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_EPWT_Status_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0x0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__VCU_EPWT_Status_invalid_timeConfirm,
		    PCOM_RX__VCU_EPWT_Status_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[32]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_VCU_552[PCOM_RX_FRAMES_VCU_552_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'COMPTEUR_RAZ_GCT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__COMPTEUR_RAZ_GCT_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__COMPTEUR_RAZ_GCT_GetInitial,
		(getValFuncPtr) PCOM_RX__COMPTEUR_RAZ_GCT_GetPhysical,
		PCOM_RX__COMPTEUR_RAZ_GCT_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__COMPTEUR_RAZ_GCT_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0xFF
		},
		{
		    TRUE,
		    0xFE
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		253,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__COMPTEUR_RAZ_GCT_invalid_timeConfirm,
		    PCOM_RX__COMPTEUR_RAZ_GCT_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[33]
	},
	{
	    /* SIGNAL 'CPT_TEMPOREL' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U32,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__CPT_TEMPOREL_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__CPT_TEMPOREL_GetInitial,
		(getValFuncPtr) PCOM_RX__CPT_TEMPOREL_GetPhysical,
		PCOM_RX__CPT_TEMPOREL_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__CPT_TEMPOREL_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0xFFFFFFFFU
		},
		{
		    TRUE,
		    0xFFFFFFFEU
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		4294967293,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__CPT_TEMPOREL_invalid_timeConfirm,
		    PCOM_RX__CPT_TEMPOREL_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[34]
	},
	{
	    /* SIGNAL 'KILOMETRAGE' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U32,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__KILOMETRAGE_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__KILOMETRAGE_GetInitial,
		(getValFuncPtr) PCOM_RX__KILOMETRAGE_GetPhysical,
		PCOM_RX__KILOMETRAGE_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__KILOMETRAGE_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0xFFFFFF
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		16777214,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__KILOMETRAGE_invalid_timeConfirm,
		    PCOM_RX__KILOMETRAGE_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[35]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_VCU_BSI_Wakeup_27A[PCOM_RX_FRAMES_VCU_BSI_Wakeup_27A_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'POS_SHUNT_JDD' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__POS_SHUNT_JDD_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__POS_SHUNT_JDD_GetInitial,
		(getValFuncPtr) PCOM_RX__POS_SHUNT_JDD_GetPhysical,
		PCOM_RX__POS_SHUNT_JDD_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__POS_SHUNT_JDD_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[36]
	},
	{
	    /* SIGNAL 'CDE_APC_JDD' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__CDE_APC_JDD_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__CDE_APC_JDD_GetInitial,
		(getValFuncPtr) PCOM_RX__CDE_APC_JDD_GetPhysical,
		PCOM_RX__CDE_APC_JDD_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__CDE_APC_JDD_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[37]
	},
	{
	    /* SIGNAL 'CDE_ACC_JDD' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__CDE_ACC_JDD_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__CDE_ACC_JDD_GetInitial,
		(getValFuncPtr) PCOM_RX__CDE_ACC_JDD_GetPhysical,
		PCOM_RX__CDE_ACC_JDD_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__CDE_ACC_JDD_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[38]
	},
	{
	    /* SIGNAL 'ETAT_PRINCIP_SEV' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__ETAT_PRINCIP_SEV_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__ETAT_PRINCIP_SEV_GetInitial,
		(getValFuncPtr) PCOM_RX__ETAT_PRINCIP_SEV_GetPhysical,
		PCOM_RX__ETAT_PRINCIP_SEV_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__ETAT_PRINCIP_SEV_GetDefaultSubstValue,
		PCOM_RX__ETAT_PRINCIP_SEV_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__ETAT_PRINCIP_SEV_forbidden_timeConfirm,
		    PCOM_RX__ETAT_PRINCIP_SEV_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[39]
	},
	{
	    /* SIGNAL 'ETAT_RESEAU_ELEC' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__ETAT_RESEAU_ELEC_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__ETAT_RESEAU_ELEC_GetInitial,
		(getValFuncPtr) PCOM_RX__ETAT_RESEAU_ELEC_GetPhysical,
		PCOM_RX__ETAT_RESEAU_ELEC_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__ETAT_RESEAU_ELEC_GetDefaultSubstValue,
		PCOM_RX__ETAT_RESEAU_ELEC_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__ETAT_RESEAU_ELEC_forbidden_timeConfirm,
		    PCOM_RX__ETAT_RESEAU_ELEC_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[40]
	},
	{
	    /* SIGNAL 'ETAT_GMP_HYB' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__ETAT_GMP_HYB_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__ETAT_GMP_HYB_GetInitial,
		(getValFuncPtr) PCOM_RX__ETAT_GMP_HYB_GetPhysical,
		PCOM_RX__ETAT_GMP_HYB_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__ETAT_GMP_HYB_GetDefaultSubstValue,
		PCOM_RX__ETAT_GMP_HYB_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__ETAT_GMP_HYB_forbidden_timeConfirm,
		    PCOM_RX__ETAT_GMP_HYB_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[41]
	},
	{
	    /* SIGNAL 'DDE_GMV_SEEM' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DDE_GMV_SEEM_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__DDE_GMV_SEEM_GetInitial,
		(getValFuncPtr) PCOM_RX__DDE_GMV_SEEM_GetPhysical,
		PCOM_RX__DDE_GMV_SEEM_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__DDE_GMV_SEEM_GetDefaultSubstValue,
		PCOM_RX__DDE_GMV_SEEM_StubForbidden,
		{
		    TRUE,
		    0xFF
		},
		{
		    TRUE,
		    0xFE
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		110,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__DDE_GMV_SEEM_invalid_timeConfirm,
		    PCOM_RX__DDE_GMV_SEEM_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__DDE_GMV_SEEM_forbidden_timeConfirm,
		    PCOM_RX__DDE_GMV_SEEM_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42]
	},
	{
	    /* SIGNAL 'DMD_MEAP_2_SEEM' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DMD_MEAP_2_SEEM_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__DMD_MEAP_2_SEEM_GetInitial,
		(getValFuncPtr) PCOM_RX__DMD_MEAP_2_SEEM_GetPhysical,
		PCOM_RX__DMD_MEAP_2_SEEM_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__DMD_MEAP_2_SEEM_GetDefaultSubstValue,
		PCOM_RX__DMD_MEAP_2_SEEM_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		100,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__DMD_MEAP_2_SEEM_forbidden_timeConfirm,
		    PCOM_RX__DMD_MEAP_2_SEEM_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[43]
	},
	{
	    /* SIGNAL 'STOP_DELAYED_HMI_WAKEUP' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__STOP_DELAYED_HMI_WAKEUP_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetInitial,
		(getValFuncPtr) PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetPhysical,
		PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[44]
	},
	{
	    /* SIGNAL 'MODE_EPS_REQUEST' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__MODE_EPS_REQUEST_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__MODE_EPS_REQUEST_GetInitial,
		(getValFuncPtr) PCOM_RX__MODE_EPS_REQUEST_GetPhysical,
		PCOM_RX__MODE_EPS_REQUEST_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__MODE_EPS_REQUEST_GetDefaultSubstValue,
		PCOM_RX__MODE_EPS_REQUEST_StubForbidden,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__MODE_EPS_REQUEST_forbidden_timeConfirm,
		    PCOM_RX__MODE_EPS_REQUEST_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[45]
	},
	{
	    /* SIGNAL 'PRECOND_ELEC_WAKEUP' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PRECOND_ELEC_WAKEUP_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__PRECOND_ELEC_WAKEUP_GetInitial,
		(getValFuncPtr) PCOM_RX__PRECOND_ELEC_WAKEUP_GetPhysical,
		PCOM_RX__PRECOND_ELEC_WAKEUP_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__PRECOND_ELEC_WAKEUP_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[46]
	},
	{
	    /* SIGNAL 'DMD_ACTIV_CHILLER' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DMD_ACTIV_CHILLER_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__DMD_ACTIV_CHILLER_GetInitial,
		(getValFuncPtr) PCOM_RX__DMD_ACTIV_CHILLER_GetPhysical,
		PCOM_RX__DMD_ACTIV_CHILLER_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__DMD_ACTIV_CHILLER_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[47]
	},
	{
	    /* SIGNAL 'DIAG_MUX_ON_PWT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DIAG_MUX_ON_PWT_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__DIAG_MUX_ON_PWT_GetInitial,
		(getValFuncPtr) PCOM_RX__DIAG_MUX_ON_PWT_GetPhysical,
		PCOM_RX__DIAG_MUX_ON_PWT_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__DIAG_MUX_ON_PWT_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[48]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_VCU_PCANInfo_17B[PCOM_RX_FRAMES_VCU_PCANInfo_17B_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'ABS_VehSpd' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__ABS_VehSpd_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__ABS_VehSpd_GetInitial,
		(getValFuncPtr) PCOM_RX__ABS_VehSpd_GetPhysical,
		PCOM_RX__ABS_VehSpd_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__ABS_VehSpd_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    TRUE,
		    0xFFFF
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		65534,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__ABS_VehSpd_invalid_timeConfirm,
		    PCOM_RX__ABS_VehSpd_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[49]
	},
	{
	    /* SIGNAL 'ABS_VehSpdValidFlag' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__ABS_VehSpdValidFlag_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__ABS_VehSpdValidFlag_GetInitial,
		(getValFuncPtr) PCOM_RX__ABS_VehSpdValidFlag_GetPhysical,
		PCOM_RX__ABS_VehSpdValidFlag_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__ABS_VehSpdValidFlag_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[50]
	},
	{
	    /* SIGNAL 'COUPURE_CONSO_CTPE2' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTPE2_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTPE2_GetInitial,
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTPE2_GetPhysical,
		PCOM_RX__COUPURE_CONSO_CTPE2_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTPE2_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[51]
	},
	{
	    /* SIGNAL 'COUPURE_CONSO_CTP' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTP_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTP_GetInitial,
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTP_GetPhysical,
		PCOM_RX__COUPURE_CONSO_CTP_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__COUPURE_CONSO_CTP_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[52]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_VCU_TU_37E[PCOM_RX_FRAMES_VCU_TU_37E_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'VCU_AccPedalPosition' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_AccPedalPosition_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_AccPedalPosition_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_AccPedalPosition_GetPhysical,
		PCOM_RX__VCU_AccPedalPosition_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_AccPedalPosition_GetDefaultSubstValue,
		PCOM_RX__VCU_AccPedalPosition_StubForbidden,
		{
		    TRUE,
		    0xFF
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_BOTH,
		100,
		0
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__VCU_AccPedalPosition_invalid_timeConfirm,
		    PCOM_RX__VCU_AccPedalPosition_invalid_timeHealed
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_RX__VCU_AccPedalPosition_forbidden_timeConfirm,
		    PCOM_RX__VCU_AccPedalPosition_forbidden_timeHealed
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_VCU2_0F0[PCOM_RX_FRAMES_VCU2_0F0_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'VCU_Keyposition' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__VCU_Keyposition_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__VCU_Keyposition_GetInitial,
		(getValFuncPtr) PCOM_RX__VCU_Keyposition_GetPhysical,
		PCOM_RX__VCU_Keyposition_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__VCU_Keyposition_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[54]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_VCU3_486[PCOM_RX_FRAMES_VCU3_486_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'ELEC_METER_SATURATION' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    TRUE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__ELEC_METER_SATURATION_Set
	    },
	    {
		(getValFuncPtr) PCOM_RX__ELEC_METER_SATURATION_GetInitial,
		(getValFuncPtr) PCOM_RX__ELEC_METER_SATURATION_GetPhysical,
		PCOM_RX__ELEC_METER_SATURATION_GetOutputSelector,
		(getValFuncPtr) PCOM_RX__ELEC_METER_SATURATION_GetDefaultSubstValue,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_E_VCU,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[55]
	}
    };

/* HAY QUE GENERAR ESTAS DE AQUI ABAJO VOY PO RAQUI */

static const PCOM_RX_Signals_Config_t PCOM_Signals_DCHVstatus1[PCOM_RX_FRAMES_DCHVSTATUS1_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DCHV_DCHVStatus' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_DCHVStatus_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_DCHVStatus_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[56]
	},
	{
	    /* SIGNAL 'DCHV_ADC_VOUT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_ADC_VOUT_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_ADC_VOUT_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[57]
	},
	{
	    /* SIGNAL 'DCHV_ADC_IOUT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_ADC_IOUT_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_ADC_IOUT_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[58]
	},
	{
	    /* SIGNAL 'DCHV_Command_Current_Reference' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_Command_Current_Reference_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_Command_Current_Reference_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[125]
	}
    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_DCHVstatus2[PCOM_RX_FRAMES_DCHVSTATUS2_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DCHV_ADC_NTC_MOD_6' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_ADC_NTC_MOD_6_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_ADC_NTC_MOD_6_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[107]
	},
	{
	    /* SIGNAL 'DCHV_ADC_NTC_MOD_5' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_ADC_NTC_MOD_5_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_ADC_NTC_MOD_5_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[122]
	}
    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_DCLVstatus1[PCOM_RX_FRAMES_DCLVSTATUS1_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DCLVStatus' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_DCLVStatus_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_DCLVStatus_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[59]
	},
	{
	    /* SIGNAL 'DCLV_Measured_Voltage' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_Measured_Voltage_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_Measured_Voltage_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[60]
	},
	{
	    /* SIGNAL 'DCLV_Measured_Current' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_Measured_Current_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_Measured_Current_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[61]
	},
	{
	    /* SIGNAL 'DCLV_Power' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_Power_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_Power_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[127]
	}

    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_DCLVstatus2[PCOM_RX_FRAMES_DCLVSTATUS2_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DCLV_T_SW_BUCK_A' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_T_SW_BUCK_A_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_T_SW_BUCK_A_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[108]
	},
	{
	    /* SIGNAL 'DCLV_T_SW_BUCK_B' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_T_SW_BUCK_B_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_T_SW_BUCK_B_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[109]
	},
	{
	    /* SIGNAL 'DCLV_T_PP_A' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_T_PP_A_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_T_PP_A_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[110]
	},
	{
	    /* SIGNAL 'DCLV_T_PP_B' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_T_PP_B_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_T_PP_B_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[111]
	},
	{
	    /* SIGNAL 'DCLV_T_L_BUCK' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_T_L_BUCK_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_T_L_BUCK_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[123]
	},
	{
	    /* SIGNAL 'DCLV_T_TX_PP' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_T_TX_PP_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_T_TX_PP_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[124]
	}
    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_DCLVstatus3[PCOM_RX_FRAMES_DCLVSTATUS3_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DCLV_Input_Current' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_Input_Current_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_Input_Current_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[62]
	},
	{
	    /* SIGNAL 'DCLV_Input_Voltage' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_Input_Voltage_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_Input_Voltage_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[63]
	},
	{
	    /* SIGNAL 'DCLV_Applied_Derating_Factor' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCLV_Applied_Derating_Factor_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCLV_Applied_Derating_Factor_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[126]
	}
    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_DCLVFaults[PCOM_RX_FRAMES_DCLVFAULTS_SIGNALS_COUNT] =
{
		{
		    /* SIGNAL 'DCLV_PWM_STOP' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_PWM_STOP_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_PWM_STOP_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[112]
		},
		{
		    /* SIGNAL 'DCLV_OT_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OT_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OT_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[113]
		},
		{
		    /* SIGNAL 'DCLV_OC_A_HV_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OC_A_HV_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OC_A_HV_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[114]
		},
		{
		    /* SIGNAL 'DCLV_OC_B_HV_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OC_B_HV_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OC_B_HV_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[115]
		},
		{
		    /* SIGNAL 'DCLV_OV_INT_A_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OV_INT_A_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OV_INT_A_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[116]
		},
		{
		    /* SIGNAL 'DCLV_OV_INT_B_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OV_INT_B_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OV_INT_B_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[117]
		},
		{
		    /* SIGNAL 'DCLV_OV_UV_HV_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OV_UV_HV_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OV_UV_HV_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[118]
		},
		{
		    /* SIGNAL 'DCLV_OC_A_LV_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OC_A_LV_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OC_A_LV_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[119]
		},
		{
		    /* SIGNAL 'DCLV_OC_B_LV_FAULT' */
		    PCOM_SIGNAL_ACTIVE,
		    PCOM_SIGNAL_NORMAL,
			PCOM_SIGNAL_TYPE_BOOL,
		    FALSE,
		    PCOM_SIGNAL_NOTAPPLICABLE,
		    {
			(getValFuncPtr) PCOM_RX__DCLV_OC_B_LV_FAULT_Set
		    },
		    {
			(getValFuncPtr) PCOM_FEATURE_NA,
			(getValFuncPtr) PCOM_RX__DCLV_OC_B_LV_FAULT_GetPhysical,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			PCOM_FEATURE_NA,
			{
			    FALSE,
			    0
			},
			{
			    FALSE,
			    0
			}
		    },
		    {
			PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
		    },
		    {
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			},
			{
			    &PCOM_DiagEnable_Forbidden,
			    PCOM_FEATURE_NA,
			    PCOM_FEATURE_NA
			}
		    },
		    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[120]
		}
};


static const PCOM_RX_Signals_Config_t PCOM_Signals_DCHVfaults[PCOM_RX_FRAMES_DCHVFAULTS_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OC_IOUT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OC_IOUT_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OC_IOUT_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[64]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OV_VOUT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OV_VOUT_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OV_VOUT_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[65]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OT' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[66]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OT_NTC_MOD5' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD5_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD5_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[67]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OT_NTC_MOD6' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD6_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD6_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[68]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_UV_12V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_UV_12V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_UV_12V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[69]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OC_POS' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OC_POS_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OC_POS_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[70]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OC_NEG' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OC_NEG_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OC_NEG_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[71]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_CAP_FAIL_H' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_H_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_H_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[72]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_CAP_FAIL_L' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_L_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_L_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[73]
	},
	{
	    /* SIGNAL 'DCHV_IOM_ERR_OT_IN' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_IN_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__DCHV_IOM_ERR_OT_IN_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[74]
	}

    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_PFCstatus1[PCOM_RX_FRAMES_PFCSTATUS1_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'PFC_PFCStatus' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_PFCStatus_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_PFCStatus_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[75]
	},
	{
	    /* SIGNAL 'PFC_Vdclink_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_Vdclink_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_Vdclink_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[76]
	},
	{
	    /* SIGNAL 'PFC_Temp_M1_C' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_Temp_M1_C_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_Temp_M1_C_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[105]
	},
	{
	    /* SIGNAL 'PFC_Temp_M3_C' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_Temp_M3_C_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_Temp_M3_C_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[121]
	},
	{
	    /* SIGNAL 'PFC_Temp_M4_C' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U8,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_Temp_M4_C_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_Temp_M4_C_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[106]
	}
    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_PFCstatus2[PCOM_RX_FRAMES_PFCSTATUS2_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'PFC_VPH12_Peak_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH12_Peak_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH12_Peak_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[77]
	},
	{
	    /* SIGNAL 'PFC_VPH23_Peak_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH23_Peak_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH23_Peak_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[78]
	},
	{
	    /* SIGNAL 'PFC_VPH31_Peak_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH31_Peak_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH31_Peak_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[79]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_PFCstatus3[PCOM_RX_FRAMES_PFCSTATUS3_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'PFC_VPH12_RMS_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH12_RMS_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH12_RMS_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[80]
	},
	{
	    /* SIGNAL 'PFC_VPH23_RMS_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH23_RMS_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH23_RMS_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[81]
	},
	{
	    /* SIGNAL 'PFC_VPH31_RMS_V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH31_RMS_V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH31_RMS_V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[82]
	},
	{
	    /* SIGNAL 'PFC_IPH1_RMS_0A1' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IPH1_RMS_0A1_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IPH1_RMS_0A1_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[83]
	},
	{
	    /* SIGNAL 'PFC_IPH2_RMS_0A1' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IPH2_RMS_0A1_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IPH2_RMS_0A1_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[84]
	},
	{
	    /* SIGNAL 'PFC_IPH3_RMS_0A1' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IPH3_RMS_0A1_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IPH3_RMS_0A1_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[85]
	}
    };


static const PCOM_RX_Signals_Config_t PCOM_Signals_PFCstatus5[PCOM_RX_FRAMES_PFCSTATUS5_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'PFC_VPH1_Freq_Hz' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH1_Freq_Hz_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH1_Freq_Hz_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[86]
	},
	{
	    /* SIGNAL 'PFC_VPH2_Freq_Hz' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH2_Freq_Hz_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH2_Freq_Hz_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[87]
	},
	{
	    /* SIGNAL 'PFC_VPH3_Freq_Hz' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_U16,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_VPH3_Freq_Hz_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_VPH3_Freq_Hz_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[88]
	}
    };

static const PCOM_RX_Signals_Config_t PCOM_Signals_PFCfaults[PCOM_RX_FRAMES_PFCFAULTS_SIGNALS_COUNT] =
    {
	{
	    /* SIGNAL 'PFC_IOM_ERR_UVLO_ISO7' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_UVLO_ISO7_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_UVLO_ISO7_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[89]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_UV_12V' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_UV_12V_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_UV_12V_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[90]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_UVLO_ISO4' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_UVLO_ISO4_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_UVLO_ISO4_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[91]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OT_NTC1_M4' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OT_NTC1_M4_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OT_NTC1_M4_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[92]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OT_NTC1_M3' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OT_NTC1_M3_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OT_NTC1_M3_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[93]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OT_NTC1_M1' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OT_NTC1_M1_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OT_NTC1_M1_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[94]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OV_DCLINK' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_DCLINK_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_DCLINK_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[95]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OV_VPH12' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_VPH12_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_VPH12_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[96]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OV_VPH31' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_VPH31_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_VPH31_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[97]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OV_VPH23' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_VPH23_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OV_VPH23_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[98]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OC_PH3' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_PH3_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_PH3_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[99]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OC_PH2' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_PH2_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_PH2_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[100]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OC_PH1' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_PH1_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_PH1_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[101]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OC_SHUNT1' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_SHUNT1_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_SHUNT1_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[102]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OC_SHUNT2' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_SHUNT2_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_SHUNT2_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[103]
	},
	{
	    /* SIGNAL 'PFC_IOM_ERR_OC_SHUNT3' */
	    PCOM_SIGNAL_ACTIVE,
	    PCOM_SIGNAL_NORMAL,
	    PCOM_SIGNAL_TYPE_BOOL,
	    FALSE,
	    PCOM_SIGNAL_NOTAPPLICABLE,
	    {
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_SHUNT3_Set
	    },
	    {
		(getValFuncPtr) PCOM_FEATURE_NA,
		(getValFuncPtr) PCOM_RX__PFC_IOM_ERR_OC_SHUNT3_GetPhysical,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		PCOM_FEATURE_NA,
		{
		    FALSE,
		    0
		},
		{
		    FALSE,
		    0
		}
	    },
	    {
		PCOM_CANSIGNAL_LIMITS_NO, PCOM_SIGNAL_LIMITS_NA,PCOM_SIGNAL_LIMITS_NA
	    },
	    {
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		},
		{
		    &PCOM_DiagEnable_Forbidden,
		    PCOM_FEATURE_NA,
		    PCOM_FEATURE_NA
		}
	    },
	    &PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[104]
	}

    };


const PCOM_RX_Frame_Config_t PCOM_RX_Frame_Config[PCOM_NUM_RX_FRAMES] =
    {
	{ /* 0 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_VCU2_oE_CAN_e2d13d8a_Rx,	/* VCU2 */
	    PCOM_PERIODIC,
	    20U,
	    PCOM_FRAME_5MSTASK_ITERATION_20MS,
	    40U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum0F0_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_VCU2_0F0,
		PCOM_RX_FRAMES_VCU2_0F0_SIGNALS_COUNT,
	    }
	},
	{ /*  1 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BMS1_oE_CAN_1fa2c419_Rx,	/* BMS1 */
	    PCOM_PERIODIC,
	    20U,
	    PCOM_FRAME_5MSTASK_ITERATION_20MS,
	    40U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum125_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BMS1_125,
		PCOM_RX_FRAMES_BMS1_125_SIGNALS_COUNT
	    }
	},
	{/* 2 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BMS3_oE_CAN_5d87c364_Rx,	/* BMS3 */
	    PCOM_PERIODIC,
	    20U,
	    PCOM_FRAME_5MSTASK_ITERATION_20MS,
	    40U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum127_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BMS3_127,
		PCOM_RX_FRAMES_BMS3_127_SIGNALS_COUNT
	    }
	},
	{/* 3 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BMS9_oE_CAN_cc47dfac_Rx,	/* BMS9 */
	    PCOM_PERIODIC,
	    20U,
	    PCOM_FRAME_5MSTASK_ITERATION_20MS,
	    40U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum129_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BMS9_129,
		PCOM_RX_FRAMES_BMS9_129_SIGNALS_COUNT
	    }
	},
	{/*  4 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_VCU_PCANInfo_oE_CAN_788ea84c_Rx,	/* VCU_PCANInfo */
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
	    30U,
	    TRUE,
	    TRUE,
	    14U,
	    15U,
	    0x03U,
	    PCOM_FrameNum17B_LostFrameEnableFunction,
	    PCOM_FrameNum17B_LostFrameEnableFunction,
	    PCOM_FrameNum17B_LostFrameEnableFunction,
	    FALSE,
	    {
		PCOM_Signals_VCU_PCANInfo_17B,
		PCOM_RX_FRAMES_VCU_PCANInfo_17B_SIGNALS_COUNT
	    }
	},
	{/* 5 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_VCU_BSI_Wakeup_oE_CAN_0e6d34d5_Rx,	/* VCU_BSI_Wakeup */
	    PCOM_PERIODIC,
	    50U,
	    PCOM_FRAME_5MSTASK_ITERATION_50MS,
	    60U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum27A_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_VCU_BSI_Wakeup_27A,
		PCOM_RX_FRAMES_VCU_BSI_Wakeup_27A_SIGNALS_COUNT
	    }
	},
	{/* 6*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BMS5_oE_CAN_9be8cae3_Rx,	/* BMS5 */
	    PCOM_PERIODIC,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum359_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BMS5_359,
		PCOM_RX_FRAMES_BMS5_359_SIGNALS_COUNT
	    }
	},
	{/*7 */
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BMS6_oE_CAN_1567cd00_Rx,	/* BMS6 */
	    PCOM_PERIODIC,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum361_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BMS6_361,
		PCOM_RX_FRAMES_BMS6_361_SIGNALS_COUNT
	    }
	},
	{/* 8*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_CtrlDCDC_oE_CAN_6ce88e20_Rx,	/* CtrlDCDC */
	    PCOM_PERIODIC,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum372_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_CtrlDCDC_372,
		PCOM_RX_FRAMES_CtrlDCDC_372_SIGNALS_COUNT
	    }
	},
	{/*9*/
	    PCOM_CAN_EXT,
	    FALSE,
	    ComConf_ComIPdu_VCU_TU_oE_CAN_e21002f7_Rx,	/* VCU_TU */
	    PCOM_PERIODIC,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum37E_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_VCU_TU_37E,
		PCOM_RX_FRAMES_VCU_TU_37E_SIGNALS_COUNT
	    }
	},
	{/* 10*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BSIInfo_oE_CAN_b362dec3_Rx,	/* BSIInfo */
	    PCOM_PERIODIC,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum382_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BSIInfo_382,
		PCOM_RX_FRAMES_BSIInfo_382_SIGNALS_COUNT
	    }
	},
	{/* 11*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_BMS8_oE_CAN_00eddf32_Rx,	/* BMS8 */
	    PCOM_PERIODIC,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum31B_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_BMS8_31B,
		PCOM_RX_FRAMES_BMS8_31B_SIGNALS_COUNT
	    }
	},
	{/* 12*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_ParkCommand_oE_CAN_5e7110cb_Rx,	/* ParkCommand */
	    PCOM_MIXED,
	    100U,
	    PCOM_FRAME_5MSTASK_ITERATION_100MS,
	    110U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum31E_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_ParkCommand_31E,
		PCOM_RX_FRAMES_ParkCommand_31E_SIGNALS_COUNT
	    }
	},
	{/* 13*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_VCU3_oE_CAN_2e7b3d14_Rx,	/* VCU3 */
	    PCOM_MIXED,
	    1000U,
	    PCOM_FRAME_5MSTASK_ITERATION_1000MS,
	    1100U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum486_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_VCU3_486,
		PCOM_RX_FRAMES_VCU3_486_SIGNALS_COUNT
	    }
	},
	{/* 14*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_VCU_oE_CAN_8b566170_Rx,	/* VCU */
	    PCOM_MIXED,
	    1000U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
	    1100U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    PCOM_FrameNum552_LostFrameEnableFunction,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_VCU_552,
		PCOM_RX_FRAMES_VCU_552_SIGNALS_COUNT
	    }
	},
	{/* 15*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_ELECTRON_BSI_oE_CAN_ab0b72ff_Rx,	/* Electron_BSI */
	    PCOM_EVENT,
	    0U,
	    PCOM_FRAME_5MSTASK_ITERATION_UNIQUE,
	    0U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    NULL_PTR,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_ELECTRON_BSI_092,
		PCOM_RX_FRAMES_ELECTRON_BSI_092_SIGNALS_COUNT
	    }
	},
	{/* 16*/
	    PCOM_CAN_EXT,
	    TRUE,
	    ComConf_ComIPdu_NEW_JDD_oE_CAN_60b2710c_Rx,	/* NEW_JDD */
	    PCOM_EVENT,
	    0U,
	    PCOM_FRAME_5MSTASK_ITERATION_UNIQUE,
	    0U,
	    FALSE,
	    FALSE,
	    0U,
	    0U,
	    0U,
	    NULL_PTR,
	    NULL_PTR,
	    NULL_PTR,
	    FALSE,
	    {
		PCOM_Signals_NEW_JDD_55F,
		PCOM_RX_FRAMES_NEW_JDD_55F_SIGNALS_COUNT
	    }
	},
	{/*17 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_PFC_Status1_oInt_CAN_459f2204_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_PFCstatus1,
		PCOM_RX_FRAMES_PFCSTATUS1_SIGNALS_COUNT
	    }
	},
	{/*18 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_PFC_Status2_oInt_CAN_aea89907_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_PFCstatus2,
		PCOM_RX_FRAMES_PFCSTATUS2_SIGNALS_COUNT
	    }
	},/* 19*/
	{
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_PFC_Status3_oInt_CAN_416af239_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_PFCstatus3,
		PCOM_RX_FRAMES_PFCSTATUS3_SIGNALS_COUNT
	    }
	},
	{/*20 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_PFC_Status4_oInt_CAN_a3b6e940_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		NULL_PTR, /*TODO vacia, borrar? */
		PCOM_RX_FRAMES_PFCSTATUS4_SIGNALS_COUNT
	    }
	},
	{/*21 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_PFC_Status5_oInt_CAN_4c74827e_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_PFCstatus5,
		PCOM_RX_FRAMES_PFCSTATUS5_SIGNALS_COUNT
	    }
	},
	{/*22 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_PFC_Fault_oInt_CAN_0a8001a9_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_PFCfaults,
		PCOM_RX_FRAMES_PFCFAULTS_SIGNALS_COUNT
	    }
	},
	{/*23 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_DCHV_Status1_oInt_CAN_6c1b5ec3_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_DCHVstatus1,
		PCOM_RX_FRAMES_DCHVSTATUS1_SIGNALS_COUNT
	    }
	},
	{/*24 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_DCHV_Status2_oInt_CAN_872ce5c0_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
	    PCOM_Signals_DCHVstatus2,
		PCOM_RX_FRAMES_DCHVSTATUS2_SIGNALS_COUNT
	    }
	},
	{/*25 */
	    PCOM_CAN_INT_OBC,
	    TRUE,
	    ComConf_ComIPdu_DCHV_Fault_oInt_CAN_d7c47a53_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_DCHVfaults,
		PCOM_RX_FRAMES_DCHVFAULTS_SIGNALS_COUNT
	    }
	},
	{/*26 */
	    PCOM_CAN_INT_LVC,
	    TRUE,
	    ComConf_ComIPdu_DCLV_Status1_oInt_CAN_85735321_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_DCLVstatus1,
		PCOM_RX_FRAMES_DCLVSTATUS1_SIGNALS_COUNT
	    }
	},
	{/* 27 */
	    PCOM_CAN_INT_LVC,
	    TRUE,
	    ComConf_ComIPdu_DCLV_Status2_oInt_CAN_6e44e822_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
	    PCOM_Signals_DCLVstatus2,
		PCOM_RX_FRAMES_DCLVSTATUS2_SIGNALS_COUNT
	    }
	},
	{/* 28*/
	    PCOM_CAN_INT_LVC,
	    TRUE,
	    ComConf_ComIPdu_DCLV_Status3_oInt_CAN_8186831c_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
		PCOM_Signals_DCLVstatus3,
		PCOM_RX_FRAMES_DCLVSTATUS3_SIGNALS_COUNT
	    }
	},
	{/* 29*/
	    PCOM_CAN_INT_LVC,
	    TRUE,
	    ComConf_ComIPdu_DCLV_Fault_oInt_CAN_a4cc5d9c_Rx,
	    PCOM_PERIODIC,
	    10U,
	    PCOM_FRAME_5MSTASK_ITERATION_10MS,
			PCOM_INTERNAL_FRAMELOST,
	    FALSE,
	    TRUE,
	    0U,
	    15U,
	    0U,
	    PCOM_AlwaysON_function,
	    NULL_PTR,
	    PCOM_AlwaysON_function,
	    FALSE,
	    {
	    PCOM_Signals_DCLVFaults,
		PCOM_RX_FRAMES_DCLVFAULTS_SIGNALS_COUNT
	    }
	}

    };

/** \} */

static PCOM_Signals_diagnostic_t PCOM_AlwaysON_function(void)
{
	PCOM_Signals_diagnostic_t ret_value;

	if (FALSE == PCOM_IntCANDiagnosticsEnable)
	{
		ret_value = PCOM_TX_DIAGNOSTIC_FORBIDDEN;
	}
	else
	{
		ret_value = PCOM_TX_DIAGNOSTIC_RUNNING;
	}

  return ret_value;
}

static boolean PCOM_FrameNum345_SignalUpdatedFunction(void)
{
  return TRUE;
}

static boolean PCOM_FrameNum3A3_SignalUpdatedFunction(void)
{
  return TRUE;
}

static boolean PCOM_FrameNum3A2_SignalUpdatedFunction(void)
{
  return TRUE;
}
static boolean PCOM_FrameDCHV_SignalUpdatedFunction(void)
{
  return TRUE;
}
static boolean PCOM_FramePFC_SignalUpdatedFunction(void)
{
  return TRUE;
}
static boolean PCOM_FrameDCLV_SignalUpdatedFunction(void)
{
  return TRUE;
}


static boolean PCOM_FrameNum0CE_EventTriggerFunction(void)
{
	boolean RetValue;

	RetValue = PCOM_Frame_0ce_SendEvent;

  return RetValue;
}

static boolean PCOM_FrameNum5B1_EventTriggerFunction(void)
{
	boolean RetValue;

	RetValue = PCOM_Frame_5b1_SendEvent;

  return RetValue;
}

static boolean PCOM_FrameNum439_EventTriggerFunction(void)
{
  return FALSE;
}

static boolean PCOM_FrameNum591_EventTriggerFunction(void)
{

/* PRQA S 3214 ++ # Macros needed for memory mapping feature */
    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */



    static SUPV_DTCRegistred PCOM_SUPV_DTCRegistred_prev = 0;
    static SUPV_ECUElecStateRCD PCOM_SUPV_ECUElecStateRCD_prev = 0;
    static SUPV_RCDLineState PCOM_SUPV_RCDLineState_prev = 0;
    static SUPV_StopDelayedHMIWupState PCOM_SUPV_StopDelayedHMIWupState_prev = 0;
    static SUPV_HVBattChargeWupState PCOM_SUPV_HVBattChargeWupState_prev = 0;
    static SUPV_PIStateInfoWupState PCOM_SUPV_PIStateInfoWupState_prev = 0;
    static SUPV_PreDriveWupState PCOM_SUPV_PreDriveWupState_prev = 0;
    static SUPV_PostDriveWupState PCOM_SUPV_PostDriveWupState_prev = 0;
    static SUPV_HoldDiscontactorWupState PCOM_SUPV_HoldDiscontactorWupState_prev = 0;
    static SUPV_PrecondElecWupState PCOM_SUPV_PrecondElecWupState_prev = 0;

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    static boolean PCOM_Frame591_FirstTime = TRUE;

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* PRQA S 3214 --*/

    /* - Automatic variables declaration ---------------- */
	SUPV_DTCRegistred PCOM_SUPV_DTCRegistred_var ;
	SUPV_ECUElecStateRCD PCOM_SUPV_ECUElecStateRCD_var;
	SUPV_RCDLineState PCOM_SUPV_RCDLineState_var;
	SUPV_StopDelayedHMIWupState PCOM_SUPV_StopDelayedHMIWupState_var; /* PRQA S 0779 # Variable name  similar to another variable. Needed for clarification */
	SUPV_HVBattChargeWupState PCOM_SUPV_HVBattChargeWupState_var;/* PRQA S 0779 # Variable name  similar to another variable. Needed for clarification */
	SUPV_PIStateInfoWupState PCOM_SUPV_PIStateInfoWupState_var;
	SUPV_PreDriveWupState PCOM_SUPV_PreDriveWupState_var;
	SUPV_PostDriveWupState PCOM_SUPV_PostDriveWupState_var;
	SUPV_HoldDiscontactorWupState PCOM_SUPV_HoldDiscontactorWupState_var;/* PRQA S 0779 # Variable name  similar to another variable. Needed for clarification */
	SUPV_PrecondElecWupState PCOM_SUPV_PrecondElecWupState_var;
	boolean ret_value;


	if (FALSE != PCOM_Frame591_FirstTime)
	{
		/* First time. Just save initial values for signals */
		(void)Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(&PCOM_SUPV_DTCRegistred_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(&PCOM_SUPV_ECUElecStateRCD_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&PCOM_SUPV_RCDLineState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(&PCOM_SUPV_StopDelayedHMIWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(&PCOM_SUPV_HVBattChargeWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(&PCOM_SUPV_PIStateInfoWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(&PCOM_SUPV_PreDriveWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(&PCOM_SUPV_PostDriveWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(&PCOM_SUPV_HoldDiscontactorWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(&PCOM_SUPV_PrecondElecWupState_prev);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		ret_value = FALSE;
		PCOM_Frame591_FirstTime = FALSE;
	}
	else
	{
		/* Get actual signal values */
		(void)Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(&PCOM_SUPV_DTCRegistred_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(&PCOM_SUPV_ECUElecStateRCD_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&PCOM_SUPV_RCDLineState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(&PCOM_SUPV_StopDelayedHMIWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(&PCOM_SUPV_HVBattChargeWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(&PCOM_SUPV_PIStateInfoWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(&PCOM_SUPV_PreDriveWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(&PCOM_SUPV_PostDriveWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(&PCOM_SUPV_HoldDiscontactorWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(&PCOM_SUPV_PrecondElecWupState_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		/* Not first time. Compare if any monitorized signal has changed */
		if (
				(PCOM_SUPV_DTCRegistred_prev != PCOM_SUPV_DTCRegistred_var) ||
				(PCOM_SUPV_ECUElecStateRCD_prev != PCOM_SUPV_ECUElecStateRCD_var) ||
				(PCOM_SUPV_RCDLineState_prev != PCOM_SUPV_RCDLineState_var) ||
				(PCOM_SUPV_StopDelayedHMIWupState_prev != PCOM_SUPV_StopDelayedHMIWupState_var) ||
				(PCOM_SUPV_HVBattChargeWupState_prev != PCOM_SUPV_HVBattChargeWupState_var) ||
				(PCOM_SUPV_PIStateInfoWupState_prev != PCOM_SUPV_PIStateInfoWupState_var) ||
				(PCOM_SUPV_PreDriveWupState_prev != PCOM_SUPV_PreDriveWupState_var) ||
				(PCOM_SUPV_PostDriveWupState_prev != PCOM_SUPV_PostDriveWupState_var) ||
				(PCOM_SUPV_HoldDiscontactorWupState_prev != PCOM_SUPV_HoldDiscontactorWupState_var) ||
				(PCOM_SUPV_PrecondElecWupState_prev != PCOM_SUPV_PrecondElecWupState_var)
				)
		{
			/* Save new signal values */
			PCOM_SUPV_DTCRegistred_prev = PCOM_SUPV_DTCRegistred_var;
			PCOM_SUPV_ECUElecStateRCD_prev = PCOM_SUPV_ECUElecStateRCD_var;
			PCOM_SUPV_RCDLineState_prev = PCOM_SUPV_RCDLineState_var;
			PCOM_SUPV_StopDelayedHMIWupState_prev = PCOM_SUPV_StopDelayedHMIWupState_var;
			PCOM_SUPV_HVBattChargeWupState_prev = PCOM_SUPV_HVBattChargeWupState_var;
			PCOM_SUPV_PIStateInfoWupState_prev = PCOM_SUPV_PIStateInfoWupState_var;
			PCOM_SUPV_PreDriveWupState_prev = PCOM_SUPV_PreDriveWupState_var;
			PCOM_SUPV_PostDriveWupState_prev = PCOM_SUPV_PostDriveWupState_var;
			PCOM_SUPV_HoldDiscontactorWupState_prev = PCOM_SUPV_HoldDiscontactorWupState_var;
			PCOM_SUPV_PrecondElecWupState_prev = PCOM_SUPV_PrecondElecWupState_var;

			/* Set frame to be sent */
			ret_value = TRUE;
		}
		else
		{
			/* No frame shall be sent */
			ret_value = FALSE;
		}
	}

  return ret_value;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum0F0_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum125_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_TBMU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum127_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_TBMU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum129_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_TBMU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum17B_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum27A_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum359_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_TBMU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum361_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_TBMU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum372_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum37E_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum382_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_BSI;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum31B_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_TBMU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum31E_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum486_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}

static PCOM_Signals_diagnostic_t PCOM_FrameNum552_LostFrameEnableFunction(void)
{
  return PCOM_DiagEnable_E_VCU;
}


static PCOM_Signals_enable_t  PCOM_Signals_enable_default(void)
{
  return PCOM_SIGNAL_ACTIVE;
}

/*Grid error indication functions.*/
static void PCOM_InternalCAN_OBC_ErrorIndication(boolean ErrorState)
{
  (void)Rte_Write_PpOBCFramesReception_Error_DeOBCFramesReception_Error(ErrorState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}
static void PCOM_InternalCAN_LVC_ErrorIndication(boolean ErrorState)
{
  (void)Rte_Write_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(ErrorState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/* PRQA S 3110, 3200, 3417, 3426 -- */

/*PRQA S 1531 --*/
