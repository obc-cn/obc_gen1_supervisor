/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPLS.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApPLS
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPLS>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup PLS
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Plausibility
 * \details This module checks the plausibility between signals and report
 * the corresponding error.
 * \{
 * \file  PLS.c
 * \brief  Generic code of the PLS module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * WdgM_CheckpointIdType
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *
 * Operation Prototypes:
 * =====================
 * CheckpointReached of Port Interface WdgM_AliveSupervision
 *   Indicates to the Watchdog Manager that a Checkpoint within a Supervised Entity has been reached.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApPLS.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "WdgM.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/** Thisphasic Phases.*/
#define PLS_TRIPHASINUM				3U
/** Gain 10 multiplier used for conversions.*/
#define PLS_GAIN_10						10U
/** Gain 100 multiplier used for conversions.*/
#define PLS_GAIN_100					100U
/** Nominal HV battery voltage.*/
#define PLS_HV_NOMINAL_DECI_V	4500U
/** Nominal LV battery voltage.*/
#define PLS_LV_NOMINAL_DECI_V	120U
/** Max value for U16.*/
#define PLS_SNA_U16						0xFFFFU
/** Square root of 3 gain.*/
#define PLS_SQRT3_Q15					56756U
/** Square root of 3 accuracy.*/
#define PLS_Q15								15
/** Max efficiency allowed.*/
#define PLS_EFFICIENCY_MAX		100U
/** Battery offset in dV*/
#define PLS_LVV_OFFSET				40U
/** Voltage error filter window: +- dV*/
#define PLS_VOLATGE_ERROR_WINDOW		100
/**Voltage error filter resolution*/
#define PLS_VOLTAGE_ERROR_Q					8
/**Voltage error filter gain*/
#define PLS_VOLTAGE_ERROR_FILTER		(uint32)50U
/**Maximum Voltage Reported dV*/
#define PLS_MAX_VOLATGE							8000
/*DCLV measure current invalid*/
#define PLS_DCLV_OUT_CURRENT_INVALID	0xFFFU
 /* PRQA S 0857,0380 --*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/** Debounce varibale*/
typedef struct
{
		/** Debounce state.*/
		boolean State;
		/** Debounce Counter.*/
		uint16 Counter;
}PLS_debounce_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Calibratable: HVVWarningThreshold .*/
 static IdtHVVWarningThreshold PLS_HVVWarningThreshold;
/** Calibratable: HVVWarningTime .*/
 static IdtHVVWarningTime PLS_HVVWarningTime;
/** Calibratable: HVVTripThreshold .*/
 static IdtHVVTripThreshold PLS_HVVTripThreshold;
/** Calibratable: HVVTripTime .*/
 static IdtHVVTripTime PLS_HVVTripTime;
/** Calibratable: LVVWarningThreshold .*/
 static IdtLVVWarningThreshold PLS_LVVWarningThreshold;
/** Calibratable: LVVWarningTime .*/
 static IdtLVVWarningTime PLS_LVVWarningTime;
/** Calibratable: LVVTripThreshold .*/
 static IdtLVVTripThreshold PLS_LVVTripThreshold;
/** Calibratable: LVVTripTime .*/
 static IdtLVVTripTime PLS_LVVTripTime;
/** Calibratable: MinEfficiency .*/
 static IdtMinEfficiency PLS_MinEfficiency;
/** Calibratable: MaxEfficiency .*/
 static IdtMaxEfficiency PLS_MaxEfficiency;
/** Calibratable: OffsetAllowed .*/
 static IdtOffsetAllowed PLS_OffsetAllowed;
/** Calibratable: LVPTripTime .*/
 static IdtLVPTripTime PLS_LVPTripTime;
/** Calibratable: OBCMinEfficiency .*/
 static IdtOBCMinEfficiency PLS_OBCMinEfficiency;
/** Calibratable: OBCMaxEfficiency .*/
 static IdtOBCMaxEfficiency PLS_OBCMaxEfficiency;
/** Calibratable: OBCOffsetAllowed .*/
 static IdtOBCOffsetAllowed PLS_OBCOffsetAllowed;
/** Calibratable: OBCPTripTime .*/
 static IdtOBCPTripTime PLS_OBCPTripTime;
/** Calibratable: DCTempWarningThreshold .*/
 static IdtDCTempWarningThreshold PLS_DCTempWarningThreshold;
/** Calibratable: DCTempTripThreshold .*/
 static IdtDCTempTripThreshold PLS_DCTempTripThreshold;
/** Calibratable: DCTempTripTime .*/
 static IdtDCTempTripTime PLS_DCTempTripTime;
/** Calibratable: DCTempWarningTime .*/
 static IdtDCTempWarningTime PLS_DCTempWarningTime;
/** Calibratable: ACTempTripThreshold .*/
 static IdtACTempTripThreshold PLS_ACTempTripThreshold;
/** Calibratable: ACTempWarning .*/
 static IdtACTempWarningThreshold PLS_ACTempWarningThreshold;
/** Calibratable: ACTempTripTime .*/
 static IdtACTempTripTime PLS_ACTempTripTime;
/** Calibratable: ACTempWarningTime .*/
 static IdtACTempWarningTime PLS_ACTempWarningTime;

#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static boolean PLS_DTCTriggerLV = FALSE;
static boolean PLS_DTCTriggerHV = FALSE;


static boolean PLS_DTCTriggerEff_DCLV = FALSE;
static boolean PLS_DTCTriggerTempAC_mono = FALSE;
static boolean PLS_DTCTriggerTempAC_tri = FALSE;
static boolean PLS_DTCTriggerTempDC = FALSE;

#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/** Plausibility sub-function: HV voltage.*/
static void PLS_volt_hv(uint16 VolategReported);
/** Plausibility sub-function: LV voltage.*/
static void PLS_volt_bat(void);
/** Plausibility sub-function: DCLV efficiency.*/
static void PLS_pwr_dclv(void);
/** Plausibility sub-function: OBC monophasic efficiency.*/
static boolean PLS_pwr_mono(void);
/** Plausibility sub-function: OBC Triphasic efficiency.*/
static boolean PLS_pwr_tri(void);
/** Plausibility sub-function: DC temperature.*/
static boolean PLS_temp_dc(void);
/** Plausibility sub-function: Monophasic temperature.*/
static boolean PLS_temp_mono(void);
/** Plausibility sub-function: Triphasic temperature.*/
static boolean PLS_temp_tri(void);

/**
 * \brief Value is not in range.
 * \details This function checks if the input value is not in range.
 * \param[in] input Input
 * \param[in] LowThrehold Low Threshold
 * \param[in] HighThreshold High Threshold
 * \retval TRUE value not in trange
 */
static boolean PLS_IsNotInRange(sint32 input, sint32 LowThrehold, sint32 HighThreshold);
/**
 * \brief Debounce
 * \details Debounce function.
 * \param[in] input Input
 * \param[in] ConfirmTime Confirm Time
 * \param[in] HealTime Heal Time
 * \param[in|out] Debounce Debounce Object
 */
static void PLS_debounce(boolean Input, uint16 ConfirmTime, uint16 HealTime, PLS_debounce_t *Debounce);
/**
 * \brief Get fault level
 * \details This function resturs the fault level depending on the individual faults detected.
 * \param[in] firstLevel Firt level fault
 * \param[in] secondLevel Second level fault
 * \param[in] thirdlevel Third level fault
 * \return Fault level
 */
static IdtFaultLevel PLS_GetFaultLevel(boolean firstLevel, boolean secondLevel, boolean thirdlevel);
/**
 * \brief Max difference.
 * \details this function returns the maximum difference among three inputs
 * \param[in] input input values.
 * \return Max differnce.
 */
static uint16 PLS_GetMaxDiff(const uint16 input[PLS_TRIPHASINUM]);
/**
 * \brief Get power.
 * \details This function returns the power calculated with the voltage and current.
 * \param[in] V_dv Voltage (dv)
 * \param[in] I_da Current (da)
 * \param[in] offset Power offset (W)
 * \param[in] Efficiency Efficiency (%)
 * \param[in] Triphasic Thiphasic mode.
 * \return Power.
 */
static uint16 PLS_GetPower(uint16 V_dv, uint16 I_da, uint8 Efficiency, boolean Triphasic, sint32 offset);

/** Volatge filter error*/
static uint16 PLS_VoltageError(void);

/** Update calibratables*/
static void PLS_UpdateCalibratables(void);

static IdtFaultLevel PLS_pwr_dclv_ErrorLevel_for_OBC(OBC_Status OBC_Status_OBC_Status, DCHV_DCHVStatus DCHVstatus, IdtFaultLevel ErrorLevel);

static void PLS_Publish_DCDC_OutputCurrent(IdtFaultLevel ErrorLevel, uint16 DCLV_Measured_Current_da);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_Voltage: Integer in interval [0...500]
 * DCDC_InputVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCDC_OutputCurrent: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCLV_Input_Current: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Measured_Voltage: Integer in interval [0...175]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * IdtACTempTripThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtACTempTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtACTempWarningThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtACTempWarningTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtDCLVFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCLVFaultTimeToRetryShortCircuit: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDCTempTripThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtDCTempTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtDCTempWarningThreshold: Integer in interval [0...250]
 *   Unit: [deg C], Factor: 0.1, Offset: 0
 * IdtDCTempWarningTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtHVVTripThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtHVVTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtHVVWarningThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtHVVWarningTime: Integer in interval [1...2000]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVPTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVVTripThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtLVVTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtLVVWarningThreshold: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtLVVWarningTime: Integer in interval [1...2000]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtMaxEfficiency: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtMinEfficiency: Integer in interval [0...100]
 *   Factor: 0.01, Offset: 0
 * IdtOBCFaultTimeToRetryDefault: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryOvertemp: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCFaultTimeToRetryVoltageError: Integer in interval [0...10000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBCMaxEfficiency: Integer in interval [0...150]
 *   Factor: 0.01, Offset: 0
 * IdtOBCMinEfficiency: Integer in interval [0...150]
 *   Factor: 0.01, Offset: 0
 * IdtOBCOffsetAllowed: Integer in interval [0...10000]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBCPTripTime: Integer in interval [1...500]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtOBC_DelayEfficiencyDCLV: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_DelayEfficiencyHVMono: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOBC_DelayEfficiencyHVTri: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtOffsetAllowed: Integer in interval [0...1000]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtVoltageCorrectionOffset: Integer in interval [-32768...32767]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * OBC_OutputCurrent: Integer in interval [0...511]
 * OBC_OutputVoltage: Integer in interval [0...8191]
 * PFC_IPH1_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH2_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH3_RMS_0A1: Integer in interval [0...1023]
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * IdtFaultLevel: Enumeration of integer in interval [0...3] with enumerators
 *   FAULT_LEVEL_NO_FAULT (0U)
 *   FAULT_LEVEL_1 (1U)
 *   FAULT_LEVEL_2 (2U)
 *   FAULT_LEVEL_3 (3U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtACTempTripTime Rte_CData_CalACTempTripTime(void)
 *   IdtACTempWarningTime Rte_CData_CalACTempWarningTime(void)
 *   IdtDCTempTripTime Rte_CData_CalDCTempTripTime(void)
 *   IdtDCTempWarningTime Rte_CData_CalDCTempWarningTime(void)
 *   IdtHVVTripTime Rte_CData_CalHVVTripTime(void)
 *   IdtHVVWarningTime Rte_CData_CalHVVWarningTime(void)
 *   IdtLVPTripTime Rte_CData_CalLVPTripTime(void)
 *   IdtLVVTripTime Rte_CData_CalLVVTripTime(void)
 *   IdtLVVWarningTime Rte_CData_CalLVVWarningTime(void)
 *   IdtOBCMaxEfficiency Rte_CData_CalOBCMaxEfficiency(void)
 *   IdtOBCOffsetAllowed Rte_CData_CalOBCOffsetAllowed(void)
 *   IdtOBCPTripTime Rte_CData_CalOBCPTripTime(void)
 *   IdtOffsetAllowed Rte_CData_CalOffsetAllowed(void)
 *   IdtACTempTripThreshold Rte_CData_CalACTempTripThreshold(void)
 *   IdtACTempWarningThreshold Rte_CData_CalACTempWarningThreshold(void)
 *   IdtDCTempTripThreshold Rte_CData_CalDCTempTripThreshold(void)
 *   IdtDCTempWarningThreshold Rte_CData_CalDCTempWarningThreshold(void)
 *   IdtHVVTripThreshold Rte_CData_CalHVVTripThreshold(void)
 *   IdtHVVWarningThreshold Rte_CData_CalHVVWarningThreshold(void)
 *   IdtLVVTripThreshold Rte_CData_CalLVVTripThreshold(void)
 *   IdtLVVWarningThreshold Rte_CData_CalLVVWarningThreshold(void)
 *   IdtMaxEfficiency Rte_CData_CalMaxEfficiency(void)
 *   IdtMinEfficiency Rte_CData_CalMinEfficiency(void)
 *   IdtOBCMinEfficiency Rte_CData_CalOBCMinEfficiency(void)
 *   IdtOBC_DelayEfficiencyDCLV Rte_CData_CalOBC_DelayEfficiencyDCLV(void)
 *   IdtOBC_DelayEfficiencyHVMono Rte_CData_CalOBC_DelayEfficiencyHVMono(void)
 *   IdtOBC_DelayEfficiencyHVTri Rte_CData_CalOBC_DelayEfficiencyHVTri(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtDCLVFaultTimeToRetryDefault Rte_Prm_PpDCLVFaultTimeToRetryDefault_DeDCLVFaultTimeToRetryDefault(void)
 *   IdtDCLVFaultTimeToRetryShortCircuit Rte_Prm_PpDCLVFaultTimeToRetryShortCircuit_DeDCLVFaultTimeToRetryShortCircuit(void)
 *   IdtOBCFaultTimeToRetryDefault Rte_Prm_PpOBCFaultTimeToRetryDefault_DeOBCFaultTimeToRetryDefault(void)
 *   IdtOBCFaultTimeToRetryOvertemp Rte_Prm_PpOBCFaultTimeToRetryOvertemp_DeOBCFaultTimeToRetryOvertemp(void)
 *   IdtOBCFaultTimeToRetryVoltageError Rte_Prm_PpOBCFaultTimeToRetryVoltageError_DeOBCFaultTimeToRetryVoltageError(void)
 *
 *********************************************************************************************************************/


#define CtApPLS_START_SEC_CODE
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLS_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLS_CODE) RCtApPLS_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	PLS_HVVWarningThreshold = Rte_CData_CalHVVWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_HVVWarningTime = Rte_CData_CalHVVWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_HVVTripThreshold = Rte_CData_CalHVVTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_HVVTripTime = Rte_CData_CalHVVTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVWarningThreshold = Rte_CData_CalLVVWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVWarningTime = Rte_CData_CalLVVWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVTripThreshold = Rte_CData_CalLVVTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVTripTime = Rte_CData_CalLVVTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_MinEfficiency = Rte_CData_CalMinEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_MaxEfficiency = Rte_CData_CalMaxEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OffsetAllowed = Rte_CData_CalOffsetAllowed();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVPTripTime = Rte_CData_CalLVPTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCMinEfficiency = Rte_CData_CalOBCMinEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCMaxEfficiency = Rte_CData_CalOBCMaxEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCOffsetAllowed = Rte_CData_CalOBCOffsetAllowed();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCPTripTime = Rte_CData_CalOBCPTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempWarningThreshold = Rte_CData_CalDCTempWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempTripThreshold = Rte_CData_CalDCTempTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempTripTime = Rte_CData_CalDCTempTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempWarningTime = Rte_CData_CalDCTempWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempTripThreshold = Rte_CData_CalACTempTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempWarningThreshold = Rte_CData_CalACTempWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempTripTime = Rte_CData_CalACTempTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempWarningTime = Rte_CData_CalACTempWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLS_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCDC_OutputCurrent data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data)
 *   Std_ReturnType Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(IdtFaultLevel data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(IdtVoltageCorrectionOffset data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLS_CODE) RCtApPLS_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLS_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean TemperatureError;
	boolean OutCurrentError;
	uint16 VolategReported;
	boolean DTCTriggerTempAC;
	uint16 DCHV_ADC_IOUT_CANSignal_da;
	boolean PLS_ProgramFlowSoftModeEnabled;

	(void)Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&PLS_ProgramFlowSoftModeEnabled);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (FALSE == PLS_ProgramFlowSoftModeEnabled)
	{
		(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP3);
	}

	PLS_UpdateCalibratables();

  VolategReported = PLS_VoltageError();

	PLS_volt_hv(VolategReported);
	PLS_volt_bat();
	PLS_pwr_dclv();
	OutCurrentError = PLS_pwr_mono();
	OutCurrentError |= PLS_pwr_tri();
	TemperatureError = PLS_temp_dc();
	TemperatureError |= PLS_temp_mono();
	TemperatureError |= PLS_temp_tri();


	DTCTriggerTempAC = PLS_DTCTriggerTempAC_mono;
	DTCTriggerTempAC |= PLS_DTCTriggerTempAC_tri;

	(void)Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&DCHV_ADC_IOUT_CANSignal_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(DCHV_ADC_IOUT_CANSignal_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(OutCurrentError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpDiagPlausFaultsList_DeACPlugTemp_PlausibilityFault(DTCTriggerTempAC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDiagPlausFaultsList_DeDCLV_EfficiencyFault(PLS_DTCTriggerEff_DCLV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDiagPlausFaultsList_DeDCPlugTemp_PlausibilityFault(PLS_DTCTriggerTempDC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDiagPlausFaultsList_DeVoltageHVBattery_PlausibilityFault(PLS_DTCTriggerHV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDiagPlausFaultsList_DeVoltageLVBattery_PlausibilityFault(PLS_DTCTriggerLV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	(void)Rte_Write_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(TemperatureError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(TemperatureError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPLS_STOP_SEC_CODE
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void PLS_volt_hv(uint16 VolategReported)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static PLS_debounce_t Warning = {FALSE,0U};

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	uint16 DCHV_ADC_VOUT_dv;
	uint16 DCLV_Input_Voltage_v;
	IdtPOST_Result PostOBC;
	IdtPOST_Result PostDCDC;

	/*Output*/
	IdtFaultLevel ErrorLevel;
	boolean CAN_DCDC_InputVoltage_ProducerError;

	boolean TripPreFlag;
	boolean WarningPreFlag;
	sint32 Difference;
	sint32 VoltHVtrip = (sint32)(uint32)(((uint32)PLS_HV_NOMINAL_DECI_V*(uint32)PLS_HVVTripThreshold)/PLS_GAIN_100);
	sint32 VoltHVWarning = (sint32)(uint32)(((uint32)PLS_HV_NOMINAL_DECI_V*(uint32)PLS_HVVWarningThreshold)/PLS_GAIN_100);


	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_ADC_VOUT_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&DCLV_Input_Voltage_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&PostOBC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&PostDCDC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	Difference = (sint32)(uint32)DCHV_ADC_VOUT_dv - (sint32)(uint32)((uint32)PLS_GAIN_10*(uint32)DCLV_Input_Voltage_v);

	TripPreFlag = PLS_IsNotInRange(Difference,-VoltHVtrip,VoltHVtrip);
	WarningPreFlag = PLS_IsNotInRange(Difference,-VoltHVWarning,VoltHVWarning);

	PLS_debounce(TripPreFlag,PLS_HVVTripTime, 0U, &Trip);
	PLS_debounce(WarningPreFlag,PLS_HVVWarningTime, 0U, &Warning);

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,Warning.State);


	if((POST_NOK==PostOBC)||(POST_NOK==PostDCDC))
	{
		/*Post fail, don't overwrite the value, */
		(void)Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(VolategReported);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCLV_Input_Voltage_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	}
	else if((POST_OK==PostOBC)&&(POST_OK==PostDCDC))
	{
		/*Post OK, report the correct value for each one.*/
		(void)Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(VolategReported);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCLV_Input_Voltage_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else if(POST_OK==PostOBC)
	{
		/*Just the OBC post has finished*/

		/*Disable the plausibility check*/
		ErrorLevel = FAULT_LEVEL_NO_FAULT;

		/*Clean debounce variable*/
		Trip.State = FALSE;
		Trip.Counter = 0U;
		Warning.State = FALSE;
		Warning.Counter = 0U;

		/*Use DCHV measure for both*/
		DCLV_Input_Voltage_v = DCHV_ADC_VOUT_dv/PLS_GAIN_10;

		(void)Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(DCHV_ADC_VOUT_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCLV_Input_Voltage_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	}
	else if(POST_OK==PostDCDC)
	{

		/*Just the DCDC post has finished*/

		/*Disable the plausibility check*/
		ErrorLevel = FAULT_LEVEL_NO_FAULT;

		/*Clean debounce variable*/
		Trip.State = FALSE;
		Trip.Counter = 0U;
		Warning.State = FALSE;
		Warning.Counter = 0U;

		/*Use DCLV measure for both*/
		DCHV_ADC_VOUT_dv = DCLV_Input_Voltage_v*PLS_GAIN_10;

		(void)Rte_Write_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(DCHV_ADC_VOUT_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCLV_Input_Voltage_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	}
	else
	{
		/*Both POST are ongoing*/

		/*Disable the plausibility check*/
		ErrorLevel = FAULT_LEVEL_NO_FAULT;

		/*Clean debounce variable*/
		Trip.State = FALSE;
		Trip.Counter = 0U;
		Warning.State = FALSE;
		Warning.Counter = 0U;

	}



	if((FAULT_LEVEL_NO_FAULT==ErrorLevel)||(FAULT_LEVEL_3==ErrorLevel))
	{
		CAN_DCDC_InputVoltage_ProducerError = FALSE;
	}
	else
	{
		CAN_DCDC_InputVoltage_ProducerError = TRUE;
	}

	/* DTC raised only with trip deviation */
	PLS_DTCTriggerHV = Trip.State;
	(void)Rte_Write_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(CAN_DCDC_InputVoltage_ProducerError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(CAN_DCDC_InputVoltage_ProducerError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOBC_OutputVoltageDCHV_Error_DeOBC_OutputVoltageDCHV_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDCLV_InputVoltage_Error_DeDCLV_InputVoltage_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void PLS_volt_bat(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static PLS_debounce_t Warning = {FALSE,0U};

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	uint8 DCLV_Measured_Voltage_dv;
	uint8 BatteryVoltage_dv;
	IdtPOST_Result DCDCPostState;

	/*Output*/
	IdtFaultLevel ErrorLevel;

	boolean TripPreFlag;
	boolean WarningPreFlag;
	sint32 Difference;
	sint32 VoltLVtrip = (sint32)(uint32)(((uint32)PLS_LV_NOMINAL_DECI_V*(uint32)PLS_LVVTripThreshold)/PLS_GAIN_100);
	sint32 VoltLVWarning = (sint32)(uint32)(((uint32)PLS_LV_NOMINAL_DECI_V*(uint32)PLS_LVVWarningThreshold)/PLS_GAIN_100);


	(void)Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(&DCLV_Measured_Voltage_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&BatteryVoltage_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpDCDC_POST_Result_DeDCDC_POST_Result(&DCDCPostState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*offset compensation*/
	if(PLS_LVV_OFFSET>=BatteryVoltage_dv)
	{
		BatteryVoltage_dv = 0U;
	}
	else
	{
		BatteryVoltage_dv -= PLS_LVV_OFFSET;
	}


	Difference = (sint32)DCLV_Measured_Voltage_dv - (sint32)BatteryVoltage_dv;

	TripPreFlag = PLS_IsNotInRange(Difference,-VoltLVtrip,VoltLVtrip);
	WarningPreFlag = PLS_IsNotInRange(Difference,-VoltLVWarning,VoltLVWarning);

	PLS_debounce(TripPreFlag,PLS_LVVTripTime, 0U, &Trip);
	PLS_debounce(WarningPreFlag,PLS_LVVWarningTime, 0U, &Warning);

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,Warning.State);


	if(POST_ONGOING==DCDCPostState)
	{
		/*Don't report any error while the DCDC POST is ongoing*/
		ErrorLevel = FAULT_LEVEL_NO_FAULT;
		/*Clean debounce variable*/
		Trip.State = FALSE;
		Trip.Counter = 0U;
		Warning.State = FALSE;
		Warning.Counter = 0U;
	}
	else if(POST_NOK == DCDCPostState)
	{
		/*POST NOK -> report implausibility error*/
		ErrorLevel = FAULT_LEVEL_2;

		Trip.State = TRUE;
		Trip.Counter = 0U;
		Warning.State = TRUE;
		Warning.Counter = 0U;
	}
	else
	{
		/*POST OK*/
		/*Misra, don't overwrite plausibility result*/
	}


	/* DTC raised only with trip deviation */
	PLS_DTCTriggerLV = Trip.State;

	(void)Rte_Write_PpOBC_BatteryVoltage_Error_DeOBC_BatteryVoltage_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDCLV_BatteryVoltage_Error_DeDCLV_BatteryVoltage_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void PLS_pwr_dclv(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static uint16 StartDelayDCLV = 0U;

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Inputs*/
	OBC_Status OBC_Status_OBC_Status;
	DCDC_Status	DCDC_Status_DCDC_Status;
	uint16 DCLV_Measured_Voltage_dv;
	uint16 DCLV_Measured_Current_da;
	uint16 DCLV_Input_Voltage_v;
	uint8 DCLV_Input_Current_da;
	DCHV_DCHVStatus DCHVstatus;
	DCLV_DCLVStatus DCLVStatus;
	/*Output*/
	IdtFaultLevel ErrorLevel;
	IdtFaultLevel ErrorLevel_for_OBC;

	boolean TripFlagPre;
	uint16 OutputPowerDCLV;
	uint16 MinTheoricalOutputPower;
	uint16 MaxTheoricalOutputPower;
	uint32 tempU32;


	(void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&DCDC_Status_DCDC_Status);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&OBC_Status_OBC_Status);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(&DCLV_Measured_Voltage_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(&DCLV_Measured_Current_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&DCLV_Input_Voltage_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Input_Current_DCLV_Input_Current(&DCLV_Input_Current_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&DCHVstatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&DCLVStatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Convert to deci volts*/
	tempU32=(uint32)DCLV_Input_Voltage_v * (uint32)PLS_GAIN_10;
	if((uint32)PLS_SNA_U16<tempU32)
	{
		DCLV_Input_Voltage_v = (uint16)PLS_SNA_U16;
	}
	else
	{
		DCLV_Input_Voltage_v = (uint16)tempU32;
	}

	/*No overflow problem*/
	DCLV_Measured_Voltage_dv += PLS_LVV_OFFSET;

	if(PLS_DCLV_OUT_CURRENT_INVALID<=DCLV_Measured_Current_da)
	{
		/*Ivalid vaue, don't evaluate the efficiency fault*/
		Trip.Counter = 0U;
		Trip.State = FALSE;
		StartDelayDCLV = 0U;
	}
	else if(((Cx3_conversion_working_==DCDC_Status_DCDC_Status)||(Cx5_degradation_mode==DCDC_Status_DCDC_Status))
			&&((Cx1_RUN==DCLVStatus)||(Cx5_DERATED==DCLVStatus)))
	{
		if(StartDelayDCLV>=((uint16)Rte_CData_CalOBC_DelayEfficiencyDCLV()*(uint16)PLS_GAIN_10))
		{
			OutputPowerDCLV=PLS_GetPower(DCLV_Measured_Voltage_dv,DCLV_Measured_Current_da,PLS_EFFICIENCY_MAX,FALSE,0);
			MinTheoricalOutputPower =PLS_GetPower(DCLV_Input_Voltage_v,DCLV_Input_Current_da,PLS_MinEfficiency,FALSE,-(sint32)(uint32)PLS_OffsetAllowed);
			MaxTheoricalOutputPower =PLS_GetPower(DCLV_Input_Voltage_v,DCLV_Input_Current_da,PLS_MaxEfficiency,FALSE,(sint32)(uint32)PLS_OffsetAllowed);

			TripFlagPre = PLS_IsNotInRange((sint32)(uint32)OutputPowerDCLV,(sint32)(uint32)MinTheoricalOutputPower,(sint32)(uint32)MaxTheoricalOutputPower);
			PLS_debounce(TripFlagPre,PLS_LVPTripTime, PLS_LVPTripTime, &Trip);
		}
		else
		{
			StartDelayDCLV++;
		}
	}
	else
	{
		Trip.Counter = 0U;
		Trip.State = FALSE;
		StartDelayDCLV = 0U;
	}

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,FALSE);

	/* Added function to reduce CC */
	PLS_Publish_DCDC_OutputCurrent(ErrorLevel, DCLV_Measured_Current_da);


	if (FAULT_LEVEL_NO_FAULT!=ErrorLevel)
	{
		PLS_DTCTriggerEff_DCLV |= TRUE;
	}
	else
	{
		PLS_DTCTriggerEff_DCLV = FALSE;
	}

	/* Added function to reduce CC */
	ErrorLevel_for_OBC = PLS_pwr_dclv_ErrorLevel_for_OBC(OBC_Status_OBC_Status, DCHVstatus, ErrorLevel);


	(void)Rte_Write_PpOBC_EfficiencyDCLV_Error_DeOBC_EfficiencyDCLV_Error(ErrorLevel_for_OBC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDCLV_EfficiencyDCLV_Error_DeDCLV_EfficiencyDCLV_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static void PLS_Publish_DCDC_OutputCurrent(IdtFaultLevel ErrorLevel, uint16 DCLV_Measured_Current_da)
{
#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static non-init variables HERE... */

#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */



#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Automatic variables declaration ---------------- */
	boolean Notvalid_InCurrent;
	boolean Notvalid_OutCurrent;

	if((FAULT_LEVEL_NO_FAULT!=ErrorLevel)&&(FAULT_LEVEL_3!=ErrorLevel))

	{
		/*Plausibility fail*/
		Notvalid_InCurrent = TRUE;
		Notvalid_OutCurrent = TRUE;
	}
	else if(PLS_DCLV_OUT_CURRENT_INVALID<=DCLV_Measured_Current_da)
	{
		/*Invalid output current value*/
		Notvalid_InCurrent = FALSE;
		Notvalid_OutCurrent = TRUE;
	}
	else
	{
		Notvalid_InCurrent = FALSE;
		Notvalid_OutCurrent = FALSE;
	}

	(void)Rte_Write_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCLV_Measured_Current_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(Notvalid_InCurrent);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(Notvalid_OutCurrent);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static IdtFaultLevel PLS_pwr_dclv_ErrorLevel_for_OBC(OBC_Status OBC_Status_OBC_Status, DCHV_DCHVStatus DCHVstatus, IdtFaultLevel ErrorLevel)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 StartDelayDCHV = 0U;

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel ErrorLevel_for_OBC;


	if(((Cx3_conversion_working_==OBC_Status_OBC_Status)||(Cx5_degradation_mode==OBC_Status_OBC_Status))
			&&(Cx1_DCHV_STATUS_RUN==DCHVstatus))
	{
		if(StartDelayDCHV>=((uint16)Rte_CData_CalOBC_DelayEfficiencyDCLV()*(uint16)PLS_GAIN_10))
		{
			ErrorLevel_for_OBC = ErrorLevel;
		}
		else
		{
			ErrorLevel_for_OBC = FAULT_LEVEL_NO_FAULT;
			StartDelayDCHV++;
		}
	}
	else
	{
		ErrorLevel_for_OBC = FAULT_LEVEL_NO_FAULT;
		StartDelayDCHV = 0U;
	}

	return ErrorLevel_for_OBC;
}

static boolean PLS_pwr_mono(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static uint16 StartDelayDCHV = 0U;
	static boolean PLS_DTCTriggerEff_OBC_mono = FALSE;

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	OBC_ChargingMode OBC_ChargingMode_CANSignal;
	IdtInputVoltageMode InputVoltageMode;
	uint16 DCHV_ADC_VOUT_CANSignal_dv;
	uint16 DCHV_ADC_IOUT_CANSignal_da;
	uint16 PFC_VPH12_RMS_V_CANSignal_v;
	uint16 PFC_IPH1_RMS_0A1_CANSignal_da;
	DCHV_DCHVStatus DCHVstatus;
	/*Output*/
	IdtFaultLevel ErrorLevel;
	boolean ForbiddenRange;

	boolean TripFlagPre;
	uint16 OutputPowerOBC;
	uint16 MinTheoricalOutputPower;
	uint16 MaxTheoricalOutputPower;
	uint32 tempU32;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(&InputVoltageMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_ADC_VOUT_CANSignal_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&DCHV_ADC_IOUT_CANSignal_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&PFC_VPH12_RMS_V_CANSignal_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(&PFC_IPH1_RMS_0A1_CANSignal_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	(void)Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&DCHVstatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*Convert to deci volts*/
	tempU32=(uint32)PFC_VPH12_RMS_V_CANSignal_v * (uint32)PLS_GAIN_10;
	if((uint32)PLS_SNA_U16<tempU32)
	{
		PFC_VPH12_RMS_V_CANSignal_v = (uint16)PLS_SNA_U16;
	}
	else
	{
		PFC_VPH12_RMS_V_CANSignal_v = (uint16)tempU32;
	}

	if((Cx1_slow_charging==OBC_ChargingMode_CANSignal)
			&&(IVM_MONOPHASIC_INPUT==InputVoltageMode)
			&&(Cx1_DCHV_STATUS_RUN==DCHVstatus))
	{
		if(StartDelayDCHV>=((uint16)Rte_CData_CalOBC_DelayEfficiencyHVMono()*(uint16)PLS_GAIN_10))
		{
			OutputPowerOBC = PLS_GetPower(DCHV_ADC_VOUT_CANSignal_dv,DCHV_ADC_IOUT_CANSignal_da,PLS_EFFICIENCY_MAX,FALSE,0);
			MinTheoricalOutputPower = PLS_GetPower(PFC_VPH12_RMS_V_CANSignal_v,PFC_IPH1_RMS_0A1_CANSignal_da,PLS_OBCMinEfficiency, FALSE , -(sint32)(uint32)PLS_OBCOffsetAllowed);
			MaxTheoricalOutputPower = PLS_GetPower(PFC_VPH12_RMS_V_CANSignal_v,PFC_IPH1_RMS_0A1_CANSignal_da,(uint8)PLS_OBCMaxEfficiency, FALSE , (sint32)(uint32)PLS_OBCOffsetAllowed);
			TripFlagPre = PLS_IsNotInRange((sint32)(uint32)OutputPowerOBC,(sint32)(uint32)MinTheoricalOutputPower,(sint32)(uint32)MaxTheoricalOutputPower);
			PLS_debounce(TripFlagPre,PLS_OBCPTripTime , PLS_OBCPTripTime , &Trip);
		}
		else
		{
			StartDelayDCHV++;
		}
	}
	else
	{
		Trip.Counter = 0U;
		Trip.State = FALSE;
		StartDelayDCHV = 0U;
	}

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,FALSE);

	if (FAULT_LEVEL_NO_FAULT!=ErrorLevel)
	{
		PLS_DTCTriggerEff_OBC_mono |= TRUE;
		ForbiddenRange = TRUE;
	}
	else
	{
		PLS_DTCTriggerEff_OBC_mono = FALSE;
		ForbiddenRange = FALSE;
	}

	(void)Rte_Write_PpOBC_EfficiencyMonophasic_Error_DeOBC_EfficiencyMonophasic_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return ForbiddenRange;
}
static boolean PLS_pwr_tri(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static uint16 StartDelayDCHV = 0U;
	static boolean PLS_DTCTriggerEff_OBC_tri = FALSE;

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	OBC_ChargingMode OBC_ChargingMode_CANSignal;
	IdtInputVoltageMode InputVoltageMode;
	uint16 DCHV_ADC_VOUT_CANSignal_dv;
	uint16 DCHV_ADC_IOUT_CANSignal_da;
	uint16 PFC_VPH12_RMS_V_CANSignal_v;
	uint16 PFC_IPH1_RMS_0A1_CANSignal_da;
	DCHV_DCHVStatus DCHVstatus;
	/*Output*/
	IdtFaultLevel ErrorLevel;
	boolean ForbiddenRange;

	boolean TripFlagPre;
	uint16 OutputPowerOBC;
	uint16 MinTheoricalOutputPower;
	uint16 MaxTheoricalOutputPower;
	uint32 tempU32;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(&InputVoltageMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_ADC_VOUT_CANSignal_dv);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(&DCHV_ADC_IOUT_CANSignal_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&PFC_VPH12_RMS_V_CANSignal_v);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(&PFC_IPH1_RMS_0A1_CANSignal_da);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&DCHVstatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Convert to deci volts*/
	tempU32=(uint32)PFC_VPH12_RMS_V_CANSignal_v * (uint32)PLS_GAIN_10;
	if((uint32)PLS_SNA_U16<tempU32)
	{
		PFC_VPH12_RMS_V_CANSignal_v = (uint16)PLS_SNA_U16;
	}
	else
	{
		PFC_VPH12_RMS_V_CANSignal_v = (uint16)tempU32;
	}

	if((Cx1_slow_charging==OBC_ChargingMode_CANSignal)
			&&(IVM_TRIPHASIC_INPUT==InputVoltageMode)
			&&((Cx1_DCHV_STATUS_RUN==DCHVstatus)))
	{
		if(StartDelayDCHV>=((uint16)Rte_CData_CalOBC_DelayEfficiencyHVTri()*(uint16)PLS_GAIN_10))
		{
			OutputPowerOBC = PLS_GetPower(DCHV_ADC_VOUT_CANSignal_dv,DCHV_ADC_IOUT_CANSignal_da,PLS_EFFICIENCY_MAX,FALSE,0);
			MinTheoricalOutputPower = PLS_GetPower(PFC_VPH12_RMS_V_CANSignal_v,PFC_IPH1_RMS_0A1_CANSignal_da,(uint8)PLS_OBCMinEfficiency, TRUE , -(sint32)(uint32)PLS_OBCOffsetAllowed);
			MaxTheoricalOutputPower = PLS_GetPower(PFC_VPH12_RMS_V_CANSignal_v,PFC_IPH1_RMS_0A1_CANSignal_da,(uint8)PLS_OBCMaxEfficiency, TRUE , (sint32)(uint32)PLS_OBCOffsetAllowed);
			TripFlagPre = PLS_IsNotInRange((sint32)(uint32)OutputPowerOBC,(sint32)(uint32)MinTheoricalOutputPower,(sint32)(uint32)MaxTheoricalOutputPower);
			PLS_debounce(TripFlagPre,PLS_OBCPTripTime , PLS_OBCPTripTime , &Trip);
		}
		else
		{
			StartDelayDCHV++;
		}
	}
	else
	{
		Trip.Counter = 0U;
		Trip.State = FALSE;
		StartDelayDCHV = 0U;
	}

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,FALSE);

	if (FAULT_LEVEL_NO_FAULT!=ErrorLevel)
	{
		PLS_DTCTriggerEff_OBC_tri |= TRUE;
		ForbiddenRange = TRUE;
	}
	else
	{
		PLS_DTCTriggerEff_OBC_tri = FALSE;
		ForbiddenRange = FALSE;
	}

	(void)Rte_Write_PpOBC_EfficiencyTriphasic_Error_DeOBC_EfficiencyTriphasic_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return ForbiddenRange;
}
static boolean PLS_temp_dc(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static PLS_debounce_t Warning = {FALSE,0U};

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	OBC_ChargingMode OBC_ChargingMode_CANSignal;
	uint16 OutputTempDC1Meas;
	uint16 OutputTempDC2Meas;
	/*Output*/
	boolean RetVal;
	IdtFaultLevel ErrorLevel;

	boolean TripPreFlag;
	boolean WarningPreFlag;
	sint32 Difference;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(&OutputTempDC1Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(&OutputTempDC2Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx3_Euro_fast_charging==OBC_ChargingMode_CANSignal)
	{
			Difference = (sint32)OutputTempDC1Meas -(sint32)OutputTempDC2Meas;
			TripPreFlag = PLS_IsNotInRange(Difference,-(sint32)(uint32)PLS_DCTempTripThreshold,(sint32)(uint32)PLS_DCTempTripThreshold);
			WarningPreFlag= PLS_IsNotInRange(Difference,-(sint32)(uint32)PLS_DCTempWarningThreshold,(sint32)(uint32)PLS_DCTempWarningThreshold);
			PLS_debounce(TripPreFlag,PLS_DCTempTripTime, 0U,  &Trip);
			PLS_debounce(WarningPreFlag,PLS_DCTempWarningTime, 0U,  &Warning);
	}
	else
	{
		Trip.Counter = 0U;
		Warning.Counter = 0U;
		Trip.State = FALSE;
		Warning.State = FALSE;
	}

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,Warning.State);
	if((FAULT_LEVEL_NO_FAULT==ErrorLevel)||(FAULT_LEVEL_3==ErrorLevel))
	{
		RetVal = FALSE;
	}
	else
	{
		RetVal = TRUE;
	}

	if (FAULT_LEVEL_NO_FAULT!=ErrorLevel)
	{
		PLS_DTCTriggerTempDC |= TRUE;
	}
	else
	{
		PLS_DTCTriggerTempDC = FALSE;
	}

	(void)Rte_Write_PpOBC_DCTemperature_Error_DeOBC_DCTemperature_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return RetVal;

}
static boolean PLS_temp_mono(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static PLS_debounce_t Warning = {FALSE,0U};

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	OBC_ChargingMode OBC_ChargingMode_CANSignal;
	IdtInputVoltageMode InputVoltageMode;
	uint16 OutputTempAC1Meas;
	uint16 OutputTempACNMeas;
	/*Output*/
	boolean RetVal;
	IdtFaultLevel ErrorLevel;

	boolean TripPreFlag;
	boolean WarningPreFlag;
	sint32 Difference;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(&InputVoltageMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&OutputTempAC1Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(&OutputTempACNMeas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx1_slow_charging==OBC_ChargingMode_CANSignal)&&(IVM_MONOPHASIC_INPUT==InputVoltageMode))
	{
		Difference = (sint32)OutputTempAC1Meas -(sint32)OutputTempACNMeas;
		TripPreFlag = PLS_IsNotInRange(Difference,-(sint32)(uint32)PLS_ACTempTripThreshold ,(sint32)(uint32)PLS_ACTempTripThreshold );
		WarningPreFlag= PLS_IsNotInRange(Difference,-(sint32)(uint32)PLS_ACTempWarningThreshold,(sint32)(uint32)PLS_ACTempWarningThreshold);
		PLS_debounce(TripPreFlag, PLS_ACTempTripTime, 0U,  &Trip);
		PLS_debounce(WarningPreFlag,PLS_ACTempWarningTime , 0U,  &Warning);
	}
	else
	{
		Trip.Counter = 0U;
		Warning.Counter = 0U;
		Trip.State = FALSE;
		Warning.State = FALSE;
	}

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,Warning.State);
	if((FAULT_LEVEL_NO_FAULT==ErrorLevel)||(FAULT_LEVEL_3==ErrorLevel))
	{
		RetVal = FALSE;
	}
	else
	{
		RetVal = TRUE;
	}

	if (FAULT_LEVEL_NO_FAULT!=ErrorLevel)
	{
		PLS_DTCTriggerTempAC_mono |= TRUE;
	}
	else
	{
		PLS_DTCTriggerTempAC_mono = FALSE;
	}

	(void)Rte_Write_PpOBC_ACTemperatureMono_Error_DeOBC_ACTemperatureMono_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return RetVal;

}
static boolean PLS_temp_tri(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static PLS_debounce_t Trip = {FALSE,0U};
	static PLS_debounce_t Warning = {FALSE,0U};

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	OBC_ChargingMode OBC_ChargingMode_CANSignal;
	IdtInputVoltageMode InputVoltageMode;
	uint16 OutputTempACMeas[PLS_TRIPHASINUM];
	/*Output*/
	boolean RetVal;
	IdtFaultLevel ErrorLevel;

	boolean TripPreFlag;
	boolean WarningPreFlag;
	sint32 Difference;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&OBC_ChargingMode_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(&InputVoltageMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&OutputTempACMeas[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(&OutputTempACMeas[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(&OutputTempACMeas[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx1_slow_charging==OBC_ChargingMode_CANSignal)&&(IVM_TRIPHASIC_INPUT==InputVoltageMode))
	{
		Difference = (sint32)PLS_GetMaxDiff(OutputTempACMeas);
		TripPreFlag = PLS_IsNotInRange(Difference,-(sint32)(uint32)PLS_ACTempTripThreshold ,(sint32)(uint32)PLS_ACTempTripThreshold );
		WarningPreFlag= PLS_IsNotInRange(Difference,-(sint32)(uint32)PLS_ACTempWarningThreshold,(sint32)(uint32)PLS_ACTempWarningThreshold);
		PLS_debounce(TripPreFlag, PLS_ACTempTripTime, 0U,  &Trip);
		PLS_debounce(WarningPreFlag,PLS_ACTempWarningTime , 0U,  &Warning);
	}
	else
	{
		Trip.Counter = 0U;
		Warning.Counter = 0U;
		Trip.State = FALSE;
		Warning.State = FALSE;
	}

	ErrorLevel = PLS_GetFaultLevel(FALSE,Trip.State,Warning.State);
	if((FAULT_LEVEL_NO_FAULT==ErrorLevel)||(FAULT_LEVEL_3==ErrorLevel))
	{
		RetVal = FALSE;
	}
	else
	{
		RetVal = TRUE;
	}

	if (FAULT_LEVEL_NO_FAULT!=ErrorLevel)
	{
		PLS_DTCTriggerTempAC_tri |= TRUE;
	}
	else
	{
		PLS_DTCTriggerTempAC_tri = FALSE;
	}

	(void)Rte_Write_PpOBC_ACTemperatureTri_Error_DeOBC_ACTemperatureTri_Error(ErrorLevel);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return RetVal;
}
static boolean PLS_IsNotInRange(sint32 input, sint32 LowThrehold, sint32 HighThreshold)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean retVal;


	if((input>=LowThrehold)&&(input<=HighThreshold))
	{
		retVal = FALSE;
	}
	else
	{
		retVal = TRUE;
	}

	return retVal;
}
static void PLS_debounce(boolean Input, uint16 ConfirmTime, uint16 HealTime, PLS_debounce_t *Debounce)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 CounterMax;


	if(NULL_PTR != Debounce)
	{

		if(TRUE==Input)
		{
			CounterMax = ConfirmTime;
		}
		else
		{
			CounterMax = HealTime;
		}

		if(Input==Debounce->State)
		{
			Debounce->Counter = 0U;
		}
		else if(Debounce->Counter>=CounterMax)
		{
			Debounce->State = Input;
			Debounce->Counter = 0U;
		}
		else
		{
			Debounce->Counter ++;
		}
	}
}
static IdtFaultLevel PLS_GetFaultLevel(boolean firstLevel, boolean secondLevel, boolean thirdlevel)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtFaultLevel retVal;


	if(TRUE==firstLevel)
	{
		retVal = FAULT_LEVEL_1 ;
	}
	else if(TRUE==secondLevel)
	{
		retVal = FAULT_LEVEL_2 ;
	}
	else if(TRUE==thirdlevel)
	{
		retVal = FAULT_LEVEL_3 ;
	}
	else
	{
		retVal = FAULT_LEVEL_NO_FAULT ;
	}

	return retVal;
}
static uint16 PLS_GetMaxDiff(const uint16 input[PLS_TRIPHASINUM])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 MaxVal ;
	uint16 MinVal;
	uint8 index;


	if(NULL_PTR!=input) /* PRQA S 2995,2991 # This statement is allways TRUE, but checking against null pointer is recommended */
	{
		MaxVal = input[0];
		MinVal = input[0];

		for(index=0;index<PLS_TRIPHASINUM;index++)
		{
			if(input[index]>MaxVal)
			{
				MaxVal = input[index];
			}
			if(input[index]<MinVal)
			{
				MinVal = input[index];
			}
		}
	}

	return (MaxVal - MinVal);
}
static uint16 PLS_GetPower(uint16 V_dv, uint16 I_da, uint8 Efficiency, boolean Triphasic, sint32 offset)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Under flow controlled, max result: U27*/
	uint32 power = ((((uint32)V_dv * (uint32)I_da)/PLS_GAIN_100) * (uint32)Efficiency)/PLS_GAIN_100;


	/*Triphasic option*/
	if(TRUE==Triphasic)
	{
		/*Under flow controlled, max result: U28*/
		power = (uint32)(((uint64)power*(uint64)PLS_SQRT3_Q15)>>PLS_Q15);
	}

	/*Add offset*/
	if((sint32)power<(-offset))
	{
		power = 0U;
	}
	else
	{
		/*max result: S31 + U28 = U32*/
		power = (uint32)(uint64)(sint64)((sint64)power+(sint64)offset);
	}

	/*Limit power to 65kw*/
	if((uint32)PLS_SNA_U16<power)
	{
		power = (uint32)PLS_SNA_U16;
	}

	return (uint16)power;
}


static uint16 PLS_VoltageError(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
    static uint32 Error_internal = ((uint32)PLS_VOLATGE_ERROR_WINDOW)<<PLS_VOLTAGE_ERROR_Q;

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	
	sint32 Error_input;
	sint16 Output;
	sint16 VolategReported;

	DCHV_ADC_VOUT DCHV_volatge;
	BMS_Voltage BMS_voltage;

	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_volatge);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(&BMS_voltage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	Error_input =  (sint32)((uint32)BMS_voltage*(uint32)PLS_GAIN_10)-(sint32)DCHV_volatge; /* PRQA S 4394 # Casting needed to not loose data with negative numbers in the substraction */


	/*Use an offset to work always with positive values*/
	if((PLS_VOLATGE_ERROR_WINDOW<Error_input)||(-PLS_VOLATGE_ERROR_WINDOW>Error_input))
	{
		/*Invalid value, Don't report any error.*/
		/*Apply the offset*/
		Error_internal = ((uint32)PLS_VOLATGE_ERROR_WINDOW)<<PLS_VOLTAGE_ERROR_Q;
	}
	else
	{
		/*The error is between [-window,+window]*/
		uint32 Error_U16 = (uint32)(Error_input+PLS_VOLATGE_ERROR_WINDOW);/* PRQA S 4393 # Casting needed due to the data type of the destination variable */
		/*Integral using + window offset*/
		Error_internal = (((Error_U16 * PLS_VOLTAGE_ERROR_FILTER)<<PLS_VOLTAGE_ERROR_Q) + ((((uint32)1U<<PLS_VOLTAGE_ERROR_Q)-PLS_VOLTAGE_ERROR_FILTER)*Error_internal))>>PLS_VOLTAGE_ERROR_Q;
	}
	/*without offset*/
	Output = (sint16)(Error_internal>>PLS_VOLTAGE_ERROR_Q)-(sint16)PLS_VOLATGE_ERROR_WINDOW; /* PRQA S 4394 # Casting needed to not loose data with negative numbers in the substraction */


	VolategReported = (sint16)DCHV_volatge + Output;

	if(0>VolategReported)
	{
		/*Underflow*/
		VolategReported = 0;
	}
	else if((sint32)PLS_MAX_VOLATGE<VolategReported)
	{
		/*Overflow*/
		VolategReported = PLS_MAX_VOLATGE;
	}
	else
	{
		/*Misra*/
	}

	(void)Rte_Write_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(Output);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return (uint16)VolategReported;

}

static void PLS_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLS_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLS_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLS_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLS_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLS_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	PLS_HVVWarningThreshold = Rte_CData_CalHVVWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_HVVWarningTime = Rte_CData_CalHVVWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_HVVTripThreshold = Rte_CData_CalHVVTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_HVVTripTime = Rte_CData_CalHVVTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVWarningThreshold = Rte_CData_CalLVVWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVWarningTime = Rte_CData_CalLVVWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVTripThreshold = Rte_CData_CalLVVTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVVTripTime = Rte_CData_CalLVVTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_MinEfficiency = Rte_CData_CalMinEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_MaxEfficiency = Rte_CData_CalMaxEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OffsetAllowed = Rte_CData_CalOffsetAllowed();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_LVPTripTime = Rte_CData_CalLVPTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCMinEfficiency = Rte_CData_CalOBCMinEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCMaxEfficiency = Rte_CData_CalOBCMaxEfficiency();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCOffsetAllowed = Rte_CData_CalOBCOffsetAllowed();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_OBCPTripTime = Rte_CData_CalOBCPTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempWarningThreshold = Rte_CData_CalDCTempWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempTripThreshold = Rte_CData_CalDCTempTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempTripTime = Rte_CData_CalDCTempTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_DCTempWarningTime = Rte_CData_CalDCTempWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempTripThreshold = Rte_CData_CalACTempTripThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempWarningThreshold = Rte_CData_CalACTempWarningThreshold();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempTripTime = Rte_CData_CalACTempTripTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	PLS_ACTempWarningTime = Rte_CData_CalACTempWarningTime()/PLS_GAIN_10;/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
