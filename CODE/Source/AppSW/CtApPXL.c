/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPXL.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApPXL
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPXL>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup PXL
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Proximity line detection.
 * \details This module manages the proximity line detection.
 *
 * \{
 * \file  PXL.c
 * \brief  Generic code of the PXL module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApPXL.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/** Signal not available value for U16.*/
#define PXL_SNA_U16		(uint16)0xFFFF
/** Vehicle speed failure value: 5 km/h*/
#define PXL_VEHICLE_SPEED_FAIL	500U
 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Upper edge for connected range.*/
static uint16 PXL_raw_max_connecte;
/** Lower edge for connected range.*/
static uint16 PXL_raw_min_connected;
/** Upper edge for disconnected range.*/
static uint16 PXL_raw_max_disconnecte;
/** Lower edge for disconnected range.*/
static uint16 PXL_raw_min_disconnected;
/** Debounce time to report Connected state.*/
static uint8 PXL_DebounceConnected;
/** Debounce time to report Disconnected state.*/
static uint8 PXL_DebounceDisconnected;
/** Debounce time to report Invalid state.*/
static uint8 PXL_DebounceInvalid;

static OBC_ChargingConnectionConfirmati PXL_last_State;
static OBC_ChargingConnectionConfirmati PXL_DebounceState;


#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTION                                                            */
/*---------------------------------------------------------------------------*/
/**
 * \brief Fail detection
 * \details This function detects if the is a fail in the proximity line detection.
 * \param[in] DebounceState Proximity line state after debounce.
 * \return Proximity line state taking into account the fail possibility.
 */
static OBC_ChargingConnectionConfirmati PXL_FailDetection(OBC_ChargingConnectionConfirmati DebounceState);
/** Update calibratables*/
static void PXL_UpdateCalibratables(void);

static void PXL_NVM_Get_Initial_State(const uint8 * ramMirror);

static void PXL_NVM_Write(uint8 * ramMirror, OBC_ChargingConnectionConfirmati PXL_OutState);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * IdtDebounceProxDetect: Integer in interval [0...255]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtProximityDetectPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityDetectVoltThreshold: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint8 *Rte_Pim_PimPXL_NvMRamMirror(void)
 *   boolean *Rte_Pim_PimProximityFailure(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtProximityDetectVoltThreshold Rte_CData_CalProximityDetectMaxVoltage(void)
 *   IdtProximityDetectVoltThreshold Rte_CData_CalProximityDetectMinVoltage(void)
 *   IdtProximityDetectVoltThreshold Rte_CData_CalProximityNotDetectMaxVoltage(void)
 *   IdtProximityDetectVoltThreshold Rte_CData_CalProximityNotDetectMinVoltage(void)
 *   IdtDebounceProxDetect Rte_CData_CalDebounceProxDetectConnected(void)
 *   IdtDebounceProxDetect Rte_CData_CalDebounceProxDetectNotConnected(void)
 *   IdtDebounceProxDetect Rte_CData_CalDebounceProxInvalid(void)
 *
 *********************************************************************************************************************/


#define CtApPXL_START_SEC_CODE
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPXL_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPXL_init_doc
 *********************************************************************************************************************/

/*!
 * \brief PXL initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPXL_CODE) RCtApPXL_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPXL_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	PXL_DebounceConnected=(uint8)Rte_CData_CalDebounceProxDetectConnected();
	PXL_DebounceDisconnected=(uint8)Rte_CData_CalDebounceProxDetectNotConnected();
	PXL_DebounceInvalid=(uint8)Rte_CData_CalDebounceProxInvalid();
	PXL_raw_min_connected=(uint16)Rte_CData_CalProximityDetectMinVoltage();
	PXL_raw_max_connecte=(uint16)Rte_CData_CalProximityDetectMaxVoltage();
	PXL_raw_min_disconnected=(uint16)Rte_CData_CalProximityNotDetectMinVoltage();
	PXL_raw_max_disconnecte=(uint16)Rte_CData_CalProximityNotDetectMaxVoltage();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPXL_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPXL_task10ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief PXL task.
  *
  * This function is called periodically by the scheduler. This function reads
  * from the \ref AIM the Proximity sensor value and determines the proximity line
  * state.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPXL_CODE) RCtApPXL_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPXL_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 PXL_debounceCounter = 0U;

	#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_ChargingConnectionConfirmati PXL_OutState;
	uint16 PXL_rawValue;
	uint8 * ramMirror;


	ramMirror = Rte_Pim_PimPXL_NvMRamMirror();

	PXL_UpdateCalibratables();

	PXL_NVM_Get_Initial_State(ramMirror);

	(void)Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(&PXL_rawValue);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(PXL_SNA_U16!=PXL_rawValue)
	{
		/*Measure available.*/
		uint8 PXL_counterMax;
		OBC_ChargingConnectionConfirmati PXL_preState;

		if((PXL_rawValue>=PXL_raw_min_connected)
				&&(PXL_rawValue<=PXL_raw_max_connecte))
		{
			/*Line connected.*/
			PXL_preState = Cx2_full_connected;
			PXL_counterMax = PXL_DebounceConnected;
		}
		else if((PXL_rawValue>=PXL_raw_min_disconnected)
				&&(PXL_rawValue<=PXL_raw_max_disconnecte))
		{
			/*Line disconnected*/
			PXL_preState = Cx1_not_connected;
			PXL_counterMax = PXL_DebounceDisconnected;
		}
		else
		{
			/*Invalid value*/
			PXL_preState = Cx0_invalid_value;
			PXL_counterMax = PXL_DebounceInvalid;
		}


		if(PXL_last_State!=PXL_preState)
		{
			/*Reset counter*/
			PXL_debounceCounter = 0U;
		}

		if(PXL_debounceCounter<PXL_counterMax)
		{
			/*Debounce in progress, increase counter*/
			PXL_debounceCounter++;
		}
		else
		{
			/*Debounce done, update state*/
			PXL_DebounceState=PXL_preState;
			PXL_debounceCounter = 0U;
		}

		PXL_last_State = PXL_preState;
		PXL_OutState = PXL_FailDetection(PXL_DebounceState);

		(void)Rte_Write_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(PXL_OutState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		PXL_NVM_Write(ramMirror, PXL_OutState);

	}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPXL_STOP_SEC_CODE
#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static OBC_ChargingConnectionConfirmati PXL_FailDetection (OBC_ChargingConnectionConfirmati DebounceState)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean FailDetected = FALSE;
	static uint8 FailCounter = 0U;

	#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	ABS_VehSpd PXL_vehicleSpeed;
	OBC_ChargingConnectionConfirmati OutState;


	(void)Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd( &PXL_vehicleSpeed);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*Fail management*/
	if(Cx2_full_connected!=DebounceState)
	{
		FailDetected = FALSE;
		FailCounter = 0U;
	}
	else if (PXL_VEHICLE_SPEED_FAIL>=PXL_vehicleSpeed)
	{
		FailCounter = 0U;
	}
	else if(FailCounter<PXL_DebounceInvalid)
	{
		FailCounter++;
	}
	else
	{
		FailDetected = TRUE;
		FailCounter = 0U;
	}

	if(FALSE!=FailDetected)
	{
		OutState = Cx0_invalid_value;
	}
	else
	{
		OutState = DebounceState;
	}

	return OutState;
}
static void PXL_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	PXL_DebounceConnected=(uint8)Rte_CData_CalDebounceProxDetectConnected();
	PXL_DebounceDisconnected=(uint8)Rte_CData_CalDebounceProxDetectNotConnected();
	PXL_DebounceInvalid=(uint8)Rte_CData_CalDebounceProxInvalid();
	PXL_raw_min_connected=(uint16)Rte_CData_CalProximityDetectMinVoltage();
	PXL_raw_max_connecte=(uint16)Rte_CData_CalProximityDetectMaxVoltage();
	PXL_raw_min_disconnected=(uint16)Rte_CData_CalProximityNotDetectMinVoltage();
	PXL_raw_max_disconnecte=(uint16)Rte_CData_CalProximityNotDetectMaxVoltage();
}

static void PXL_NVM_Get_Initial_State(const uint8 * ramMirror)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static boolean PXL_FirstTime = TRUE;

	#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	if (PXL_FirstTime != FALSE)
	{
		/* Take the value of this signal when the OBC went to sleep mode in the previous cycle */
		PXL_last_State = (OBC_ChargingConnectionConfirmati)(*ramMirror);
		PXL_DebounceState = (OBC_ChargingConnectionConfirmati)(*ramMirror);

		PXL_FirstTime = FALSE;
	}


}

static void PXL_NVM_Write(uint8 * ramMirror, OBC_ChargingConnectionConfirmati PXL_OutState)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPXL_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPXL_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPXL_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPXL_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPXL_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	uint8 reset_type;
	boolean shutdown_auth;


	/* Mark NvM to be saved */
	WUM_GetResetRequest(&reset_type);
	(void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&shutdown_auth);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FALSE == shutdown_auth) && (0x02U != reset_type))
	{
		*ramMirror = (uint8)PXL_OutState;
		(void)Rte_Call_NvMService_AC3_SRBS_PXLNvBlockNeed_SetRamBlockStatus(TRUE);
	}


}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
