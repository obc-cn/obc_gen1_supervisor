/**************************************************************************//**
 * \file	ctApJDD_cfg.h
 * \author	Juan Carlos Romanillos <juancarlos.romanillos(at)es.mahle.com
 * \date	11 dic. 2019
 * \copyright	MAHLE ELECTRONICS - 2019
 * \brief	Configuration file for JDD comunications
 *****************************************************************************/

/*****************************************************************************
 * INCLUDES                                                                  
 *****************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
#include "Rte_CtApJDD.h"
#include "CtApJDD_cfg.h"
/*****************************************************************************
 * CONSTANT DEFINITIONS                                                      
 *****************************************************************************/

/*****************************************************************************
 * TYPEDEFS                                                                  
 *****************************************************************************/

#define JDD_DATA_0x056216 (0U)
#define JDD_DATA_0x056317 (1U)
#define JDD_DATA_0x0a0804 (2U)
#define JDD_DATA_0x0a084b (3U)
#define JDD_DATA_0x0a9464 (4U)
#define JDD_DATA_0x0af864 (5U)
#define JDD_DATA_0x108093 (6U)
#define JDD_DATA_0x13e919 (7U)
#define JDD_DATA_0x120a11 (8U)
#define JDD_DATA_0x120a12 (9U)
#define JDD_DATA_0x120b11 (10U)
#define JDD_DATA_0x120b12 (11U)
#define JDD_DATA_0x120c64 (12U)
#define JDD_DATA_0x120c98 (13U)
#define JDD_DATA_0x120d64 (14U)
#define JDD_DATA_0x120d98 (15U)
#define JDD_DATA_0x12d711 (16U)
#define JDD_DATA_0x12d712 (17U)
#define JDD_DATA_0x12d713 (18U)
#define JDD_DATA_0x12d811 (19U)
#define JDD_DATA_0x12d812 (20U)
#define JDD_DATA_0x12d813 (21U)
#define JDD_DATA_0x12d911 (22U)
#define JDD_DATA_0x12d912 (23U)
#define JDD_DATA_0x12d913 (24U)
#define JDD_DATA_0x12da11 (25U)
#define JDD_DATA_0x12da12 (26U)
#define JDD_DATA_0x12da13 (27U)
#define JDD_DATA_0x12db12 (28U)
#define JDD_DATA_0x12dc11 (29U)
#define JDD_DATA_0x12dd12 (30U)
#define JDD_DATA_0x12de11 (31U)
#define JDD_DATA_0x12df13 (32U)
#define JDD_DATA_0x12e012 (33U)
#define JDD_DATA_0x12e111 (34U)
#define JDD_DATA_0x12e213 (35U)
#define JDD_DATA_0x12e319 (36U)
#define JDD_DATA_0x12e712 (37U)
#define JDD_DATA_0x12e811 (38U)
#define JDD_DATA_0x12e912 (39U)
#define JDD_DATA_0x12ea11 (40U)
#define JDD_DATA_0x12f917 (41U)
#define JDD_DATA_0x179e11 (42U)
#define JDD_DATA_0x179e12 (43U)
#define JDD_DATA_0x179f11 (44U)
#define JDD_DATA_0x179f12 (45U)
#define JDD_DATA_0x1a0064 (46U)
#define JDD_DATA_0x1a7104 (47U)
#define JDD_DATA_0x1a714b (48U)
#define JDD_DATA_0x1a7172 (49U)
#define JDD_DATA_0xc07988 (50U)
#define JDD_DATA_0xc08913 (51U)
#define JDD_DATA_0xd18787 (52U)
#define JDD_DATA_0xd1a087 (53U)
#define JDD_DATA_0xd20781 (54U)
#define JDD_DATA_0xd2a081 (55U)
#define JDD_DATA_0xd38782 (56U)
#define JDD_DATA_0xd38783 (57U)
#define JDD_DATA_0xe00081 (58U)
#define JDD_DATA_0xe00087 (59U)
#define JDD_DATA_0xe00214 (60U)
#define JDD_DATA_0xe00362 (61U)
#define JDD_DATA_0x166c64 (62U)
#define JDD_DATA_0x0cf464 (63U)
#define JDD_DATA_0x10c413 (64U)
#define JDD_DATA_0x10c512 (65U)
#define JDD_DATA_0x10c613 (66U)
#define JDD_DATA_0x10c713 (67U)
#define JDD_DATA_0x12f316 (68U)

 /* PRQA S 0857,0380 -- */
/*****************************************************************************
 * PUBLIC_VARIABLE_DEFINITIONS                                               
 *****************************************************************************/

/*****************************************************************************
 * MODULE_FUNCTION_PROTOTYPES
 *****************************************************************************/
static boolean JDD_0x056216_isFailed(void);
static boolean JDD_0x056317_isFailed(void);
static boolean JDD_0x0a0804_isFailed(void);
static boolean JDD_0x0a084b_isFailed(void);
static boolean JDD_0x0a9464_isFailed(void);
static boolean JDD_0x0af864_isFailed(void);
static boolean JDD_0x108093_isFailed(void);
static boolean JDD_0x13e919_isFailed(void);
static boolean JDD_0x120a11_isFailed(void);
static boolean JDD_0x120a12_isFailed(void);
static boolean JDD_0x120b11_isFailed(void);
static boolean JDD_0x120b12_isFailed(void);
static boolean JDD_0x120c64_isFailed(void);
static boolean JDD_0x120c98_isFailed(void);
static boolean JDD_0x120d64_isFailed(void);
static boolean JDD_0x120d98_isFailed(void);
static boolean JDD_0x12d711_isFailed(void);
static boolean JDD_0x12d712_isFailed(void);
static boolean JDD_0x12d713_isFailed(void);
static boolean JDD_0x12d811_isFailed(void);
static boolean JDD_0x12d812_isFailed(void);
static boolean JDD_0x12d813_isFailed(void);
static boolean JDD_0x12d911_isFailed(void);
static boolean JDD_0x12d912_isFailed(void);
static boolean JDD_0x12d913_isFailed(void);
static boolean JDD_0x12da11_isFailed(void);
static boolean JDD_0x12da12_isFailed(void);
static boolean JDD_0x12da13_isFailed(void);
static boolean JDD_0x12db12_isFailed(void);
static boolean JDD_0x12dc11_isFailed(void);
static boolean JDD_0x12dd12_isFailed(void);
static boolean JDD_0x12de11_isFailed(void);
static boolean JDD_0x12df13_isFailed(void);
static boolean JDD_0x12e012_isFailed(void);
static boolean JDD_0x12e111_isFailed(void);
static boolean JDD_0x12e213_isFailed(void);
static boolean JDD_0x12e319_isFailed(void);
static boolean JDD_0x12e712_isFailed(void);
static boolean JDD_0x12e811_isFailed(void);
static boolean JDD_0x12e912_isFailed(void);
static boolean JDD_0x12ea11_isFailed(void);
static boolean JDD_0x12f917_isFailed(void);
static boolean JDD_0x179e11_isFailed(void);
static boolean JDD_0x179e12_isFailed(void);
static boolean JDD_0x179f11_isFailed(void);
static boolean JDD_0x179f12_isFailed(void);
static boolean JDD_0x1a0064_isFailed(void);
static boolean JDD_0x1a7104_isFailed(void);
static boolean JDD_0x1a714b_isFailed(void);
static boolean JDD_0x1a7172_isFailed(void);
static boolean JDD_0xc07988_isFailed(void);
static boolean JDD_0xc08913_isFailed(void);
static boolean JDD_0xd18787_isFailed(void);
static boolean JDD_0xd1a087_isFailed(void);
static boolean JDD_0xd20781_isFailed(void);
static boolean JDD_0xd2a081_isFailed(void);
static boolean JDD_0xd38782_isFailed(void);
static boolean JDD_0xd38783_isFailed(void);
static boolean JDD_0xe00081_isFailed(void);
static boolean JDD_0xe00087_isFailed(void);
static boolean JDD_0xe00214_isFailed(void);
static boolean JDD_0xe00362_isFailed(void);
static boolean JDD_0x166c64_isFailed(void);
static boolean JDD_0x0cf464_isFailed(void);
static boolean JDD_0x10c413_isFailed(void);
static boolean JDD_0x10c512_isFailed(void);
static boolean JDD_0x10c613_isFailed(void);
static boolean JDD_0x10c713_isFailed(void);
static boolean JDD_0x12f316_isFailed(void);

/* PRQA S 3214 EOF #Following macros are autogenrated.*/

/*****************************************************************************
 * MODULE_VARIABLES                                                          
 *****************************************************************************/
/* - Static non-init variables declaration ---------- */
#define CtApJDD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* NOTE: 'static' storage-class specifier is not required here due to 
       this variable is defined as external at 'ctApJDD_cfg.h' header file */
JDD_dtc_data_t jdd_dtc_data[JDD_NUMBER_OF_DTCS]; /* PRQA S 1504 */

#define CtApJDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApJDD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApJDD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


const JDD_dtc_config_t 	jdd_dtc_configs [JDD_NUMBER_OF_DTCS] = { /* PRQA S 1531 */
    {
	/* Code 0x056216 */
	JDD_DATA_0x056216,
	0x056216,
	JDD_0x056216_isFailed,
	&jdd_dtc_data[JDD_DATA_0x056216]
    },
    {
	/* Code 0x056317 */
	JDD_DATA_0x056317,
	0x056317,
	JDD_0x056317_isFailed,
	&jdd_dtc_data[JDD_DATA_0x056317]
    },
    {
	/* Code 0x0a0804 */
	JDD_DATA_0x0a0804,
	0x0a0804,
	JDD_0x0a0804_isFailed,
	&jdd_dtc_data[JDD_DATA_0x0a0804]
    },
    {
	/* Code 0x0a084b */
	JDD_DATA_0x0a084b,
	0x0a084b,
	JDD_0x0a084b_isFailed,
	&jdd_dtc_data[JDD_DATA_0x0a084b]
    },
    {
	/* Code 0x0a9464 */
	JDD_DATA_0x0a9464,
	0x0a9464,
	JDD_0x0a9464_isFailed,
	&jdd_dtc_data[JDD_DATA_0x0a9464]
    },
    {
	/* Code 0x0af864 */
	JDD_DATA_0x0af864,
	0x0af864,
	JDD_0x0af864_isFailed,
	&jdd_dtc_data[JDD_DATA_0x0af864]
    },
    {
	/* Code 0x108093 */
	JDD_DATA_0x108093,
	0x108093,
	JDD_0x108093_isFailed,
	&jdd_dtc_data[JDD_DATA_0x108093]
    },
    {
	/* Code 0x13e919 */
	JDD_DATA_0x13e919,
	0x13e919,
	JDD_0x13e919_isFailed,
	&jdd_dtc_data[JDD_DATA_0x13e919]
    },
    {
	/* Code 0x120a11 */
	JDD_DATA_0x120a11,
	0x120a11,
	JDD_0x120a11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120a11]
    },
    {
	/* Code 0x120a12 */
	JDD_DATA_0x120a12,
	0x120a12,
	JDD_0x120a12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120a12]
    },
    {
	/* Code 0x120b11 */
	JDD_DATA_0x120b11,
	0x120b11,
	JDD_0x120b11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120b11]
    },
    {
	/* Code 0x120b12 */
	JDD_DATA_0x120b12,
	0x120b12,
	JDD_0x120b12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120b12]
    },
    {
	/* Code 0x120c64 */
	JDD_DATA_0x120c64,
	0x120c64,
	JDD_0x120c64_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120c64]
    },
    {
	/* Code 0x120c98 */
	JDD_DATA_0x120c98,
	0x120c98,
	JDD_0x120c98_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120c98]
    },
    {
	/* Code 0x120d64 */
	JDD_DATA_0x120d64,
	0x120d64,
	JDD_0x120d64_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120d64]
    },
    {
	/* Code 0x120d98 */
	JDD_DATA_0x120d98,
	0x120d98,
	JDD_0x120d98_isFailed,
	&jdd_dtc_data[JDD_DATA_0x120d98]
    },
    {
	/* Code 0x12d711 */
	JDD_DATA_0x12d711,
	0x12d711,
	JDD_0x12d711_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d711]
    },
    {
	/* Code 0x12d712 */
	JDD_DATA_0x12d712,
	0x12d712,
	JDD_0x12d712_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d712]
    },
    {
	/* Code 0x12d713 */
	JDD_DATA_0x12d713,
	0x12d713,
	JDD_0x12d713_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d713]
    },
    {
	/* Code 0x12d811 */
	JDD_DATA_0x12d811,
	0x12d811,
	JDD_0x12d811_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d811]
    },
    {
	/* Code 0x12d812 */
	JDD_DATA_0x12d812,
	0x12d812,
	JDD_0x12d812_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d812]
    },
    {
	/* Code 0x12d813 */
	JDD_DATA_0x12d813,
	0x12d813,
	JDD_0x12d813_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d813]
    },
    {
	/* Code 0x12d911 */
	JDD_DATA_0x12d911,
	0x12d911,
	JDD_0x12d911_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d911]
    },
    {
	/* Code 0x12d912 */
	JDD_DATA_0x12d912,
	0x12d912,
	JDD_0x12d912_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d912]
    },
    {
	/* Code 0x12d913 */
	JDD_DATA_0x12d913,
	0x12d913,
	JDD_0x12d913_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12d913]
    },
    {
	/* Code 0x12da11 */
	JDD_DATA_0x12da11,
	0x12da11,
	JDD_0x12da11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12da11]
    },
    {
	/* Code 0x12da12 */
	JDD_DATA_0x12da12,
	0x12da12,
	JDD_0x12da12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12da12]
    },
    {
	/* Code 0x12da13 */
	JDD_DATA_0x12da13,
	0x12da13,
	JDD_0x12da13_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12da13]
    },
    {
	/* Code 0x12db12 */
	JDD_DATA_0x12db12,
	0x12db12,
	JDD_0x12db12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12db12]
    },
    {
	/* Code 0x12dc11 */
	JDD_DATA_0x12dc11,
	0x12dc11,
	JDD_0x12dc11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12dc11]
    },
    {
	/* Code 0x12dd12 */
	JDD_DATA_0x12dd12,
	0x12dd12,
	JDD_0x12dd12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12dd12]
    },
    {
	/* Code 0x12de11 */
	JDD_DATA_0x12de11,
	0x12de11,
	JDD_0x12de11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12de11]
    },
    {
	/* Code 0x12df13 */
	JDD_DATA_0x12df13,
	0x12df13,
	JDD_0x12df13_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12df13]
    },
    {
	/* Code 0x12e012 */
	JDD_DATA_0x12e012,
	0x12e012,
	JDD_0x12e012_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e012]
    },
    {
	/* Code 0x12e111 */
	JDD_DATA_0x12e111,
	0x12e111,
	JDD_0x12e111_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e111]
    },
    {
	/* Code 0x12e213 */
	JDD_DATA_0x12e213,
	0x12e213,
	JDD_0x12e213_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e213]
    },
    {
	/* Code 0x12e319 */
	JDD_DATA_0x12e319,
	0x12e319,
	JDD_0x12e319_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e319]
    },
    {
	/* Code 0x12e712 */
	JDD_DATA_0x12e712,
	0x12e712,
	JDD_0x12e712_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e712]
    },
    {
	/* Code 0x12e811 */
	JDD_DATA_0x12e811,
	0x12e811,
	JDD_0x12e811_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e811]
    },
    {
	/* Code 0x12e912 */
	JDD_DATA_0x12e912,
	0x12e912,
	JDD_0x12e912_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12e912]
    },
    {
	/* Code 0x12ea11 */
	JDD_DATA_0x12ea11,
	0x12ea11,
	JDD_0x12ea11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12ea11]
    },
    {
	/* Code 0x12f917 */
	JDD_DATA_0x12f917,
	0x12f917,
	JDD_0x12f917_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12f917]
    },
    {
	/* Code 0x179e11 */
	JDD_DATA_0x179e11,
	0x179e11,
	JDD_0x179e11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x179e11]
    },
    {
	/* Code 0x179e12 */
	JDD_DATA_0x179e12,
	0x179e12,
	JDD_0x179e12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x179e12]
    },
    {
	/* Code 0x179f11 */
	JDD_DATA_0x179f11,
	0x179f11,
	JDD_0x179f11_isFailed,
	&jdd_dtc_data[JDD_DATA_0x179f11]
    },
    {
	/* Code 0x179f12 */
	JDD_DATA_0x179f12,
	0x179f12,
	JDD_0x179f12_isFailed,
	&jdd_dtc_data[JDD_DATA_0x179f12]
    },
    {
	/* Code 0x1a0064 */
	JDD_DATA_0x1a0064,
	0x1a0064,
	JDD_0x1a0064_isFailed,
	&jdd_dtc_data[JDD_DATA_0x1a0064]
    },
    {
	/* Code 0x1a7104 */
	JDD_DATA_0x1a7104,
	0x1a7104,
	JDD_0x1a7104_isFailed,
	&jdd_dtc_data[JDD_DATA_0x1a7104]
    },
    {
	/* Code 0x1a714b */
	JDD_DATA_0x1a714b,
	0x1a714b,
	JDD_0x1a714b_isFailed,
	&jdd_dtc_data[JDD_DATA_0x1a714b]
    },
    {
	/* Code 0x1a7172 */
	JDD_DATA_0x1a7172,
	0x1a7172,
	JDD_0x1a7172_isFailed,
	&jdd_dtc_data[JDD_DATA_0x1a7172]
    },
    {
	/* Code 0xc07988 */
	JDD_DATA_0xc07988,
	0xc07988,
	JDD_0xc07988_isFailed,
	&jdd_dtc_data[JDD_DATA_0xc07988]
    },
    {
	/* Code 0xc08913 */
	JDD_DATA_0xc08913,
	0xc08913,
	JDD_0xc08913_isFailed,
	&jdd_dtc_data[JDD_DATA_0xc08913]
    },
    {
	/* Code 0xd18787 */
	JDD_DATA_0xd18787,
	0xd18787,
	JDD_0xd18787_isFailed,
	&jdd_dtc_data[JDD_DATA_0xd18787]
    },
    {
	/* Code 0xd1a087 */
	JDD_DATA_0xd1a087,
	0xd1a087,
	JDD_0xd1a087_isFailed,
	&jdd_dtc_data[JDD_DATA_0xd1a087]
    },
    {
	/* Code 0xd20781 */
	JDD_DATA_0xd20781,
	0xd20781,
	JDD_0xd20781_isFailed,
	&jdd_dtc_data[JDD_DATA_0xd20781]
    },
    {
	/* Code 0xd2a081 */
	JDD_DATA_0xd2a081,
	0xd2a081,
	JDD_0xd2a081_isFailed,
	&jdd_dtc_data[JDD_DATA_0xd2a081]
    },
    {
	/* Code 0xd38782 */
	JDD_DATA_0xd38782,
	0xd38782,
	JDD_0xd38782_isFailed,
	&jdd_dtc_data[JDD_DATA_0xd38782]
    },
    {
	/* Code 0xd38783 */
	JDD_DATA_0xd38783,
	0xd38783,
	JDD_0xd38783_isFailed,
	&jdd_dtc_data[JDD_DATA_0xd38783]
    },
    {
	/* Code 0xe00081 */
	JDD_DATA_0xe00081,
	0xe00081,
	JDD_0xe00081_isFailed,
	&jdd_dtc_data[JDD_DATA_0xe00081]
    },
    {
	/* Code 0xe00087 */
	JDD_DATA_0xe00087,
	0xe00087,
	JDD_0xe00087_isFailed,
	&jdd_dtc_data[JDD_DATA_0xe00087]
    },
    {
	/* Code 0xe00214 */
	JDD_DATA_0xe00214,
	0xe00214,
	JDD_0xe00214_isFailed,
	&jdd_dtc_data[JDD_DATA_0xe00214]
    },
    {
	/* Code 0xe00362 */
	JDD_DATA_0xe00362,
	0xe00362,
	JDD_0xe00362_isFailed,
	&jdd_dtc_data[JDD_DATA_0xe00362]
    },
    {
	/* Code 0x166c64 */
	JDD_DATA_0x166c64,
	0x166c64,
	JDD_0x166c64_isFailed,
	&jdd_dtc_data[JDD_DATA_0x166c64]
    },
    {
	/* Code 0x0cf464 */
	JDD_DATA_0x0cf464,
	0x0cf464,
	JDD_0x0cf464_isFailed,
	&jdd_dtc_data[JDD_DATA_0x0cf464]
    },
	{
	/* Code 0x10c413 */
	JDD_DATA_0x10c413,
	0x10c413,
	JDD_0x10c413_isFailed,
	&jdd_dtc_data[JDD_DATA_0x10c413]
    },
	{
	/* Code 0x10c512 */
	JDD_DATA_0x10c512,
	0x10c512,
	JDD_0x10c512_isFailed,
	&jdd_dtc_data[JDD_DATA_0x10c512]
    },
	{
	/* Code 0x10c613 */
	JDD_DATA_0x10c613,
	0x10c613,
	JDD_0x10c613_isFailed,
	&jdd_dtc_data[JDD_DATA_0x10c613]
    },
	{
	/* Code 0x10c713 */
	JDD_DATA_0x10c713,
	0x10c713,
	JDD_0x10c713_isFailed,
	&jdd_dtc_data[JDD_DATA_0x10c713]
    },
	{
	/* Code 0x12f316 */
	JDD_DATA_0x12f316,
	0x12f316,
	JDD_0x12f316_isFailed,
	&jdd_dtc_data[JDD_DATA_0x12f316]
    }
};


/*****************************************************************************
 * PUBLIC_FUNCTION_DEFINITIONS                                               
 *****************************************************************************/

/*****************************************************************************
 * MODULE_FUNCTION_DEFINITIONS                                               
 *****************************************************************************/

static boolean JDD_0x056216_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x056216_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x056317_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x056317_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x0a0804_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x0a0804_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x0a084b_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x0a084b_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x0a9464_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x0a9464_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x0af864_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x0af864_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x108093_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x108093_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x13e919_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x13e919_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120a11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120a11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120a12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120a12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120b11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120b11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120b12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120b12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120c64_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120c64_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120c98_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120c98_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120d64_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120d64_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x120d98_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x120d98_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d711_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d711_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d712_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d712_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d713_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d713_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d811_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d811_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d812_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d812_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d813_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d813_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d911_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d911_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d912_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d912_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12d913_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12d913_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12da11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12da11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12da12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12da12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12da13_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12da13_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12db12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12db12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12dc11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12dc11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12dd12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12dd12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12de11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12de11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12df13_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12df13_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e012_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e012_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e111_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e111_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e213_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e213_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e319_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e319_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e712_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e712_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e811_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e811_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12e912_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12e912_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12ea11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12ea11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12f917_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12f917_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x179e11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x179e11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x179e12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x179e12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x179f11_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x179f11_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x179f12_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x179f12_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x1a0064_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x1a0064_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x1a7104_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x1a7104_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x1a714b_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x1a714b_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x1a7172_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x1a7172_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xc07988_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xc08913_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xd18787_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xd18787_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xd1a087_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xd1a087_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xd20781_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xd20781_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xd2a081_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xd2a081_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xd38782_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xd38782_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xd38783_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xd38783_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xe00081_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xe00081_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xe00087_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xe00087_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xe00214_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xe00214_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0xe00362_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0xe00362_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x166c64_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x166c64_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x0cf464_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x0cf464_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x10c413_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x10c413_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x10c512_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x10c512_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x10c613_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x10c613_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x10c713_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x10c713_GetEventFailed(&EventFailed);
  return EventFailed;
}
static boolean JDD_0x12f316_isFailed(void){
  boolean EventFailed;
  (void)Rte_Call_EvtInfo_DTC_0x12f316_GetEventFailed(&EventFailed);
  return EventFailed;
}
/* EOF */
