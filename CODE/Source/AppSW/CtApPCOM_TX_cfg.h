/**************************************************************************//**
 * \file	ctApPCOM_TX_cfg.h
 * \author	M0069810 Juan Carlos Romanillos <juancarlos.romanillos at es.mahle.com>
 * \date		19 nov. 2019
 * \copyright	MAHLE ELECTRONICS - 2019
 * \brief
 * \ingroup ctApPCOM_cfg
 *****************************************************************************/

#ifndef APPSW_CTAPPCOM_TX_CFG_H_
#define APPSW_CTAPPCOM_TX_CFG_H_
/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "CtApPCOM_cfg.h"
#include "COM_Cbk.h"

/******************************************************************************
 * PUBLIC CONSTANT DEFINITIONS
 *****************************************************************************/

/******************************************************************************
 * PUBLIC TYPEDEFS
 *****************************************************************************/

/******************************************************************************
 * PUBLIC VARIABLE DECLARATIONS
 *****************************************************************************/

/******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES
 *****************************************************************************/

/* PRQA S 0777,0779 EOF # The naming of the functions must match the names of the CAN signals */

Std_ReturnType PCOM_TX__DCDC_Temperature_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_Temperature_GetCalculated(DCDC_Temperature * value);
Std_ReturnType PCOM_TX__DCDC_Temperature_Set(const DCDC_Temperature * value);
boolean PCOM_TX_ProducerError_DCDC_Temperature_DCDC_Temperature(void);

Std_ReturnType PCOM_TX__DCDC_Status_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_Status_GetCalculated(DCDC_Status * value);
Std_ReturnType PCOM_TX__DCDC_Status_Set(const DCDC_Status * value);
boolean PCOM_TX_ProducerError_DCDC_Status_DCDC_Status(void);

Std_ReturnType PCOM_TX__DCDC_FaultLampRequest_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_FaultLampRequest_GetCalculated(DCDC_FaultLampRequest * value);
Std_ReturnType PCOM_TX__DCDC_FaultLampRequest_Set(const DCDC_FaultLampRequest * value);

Std_ReturnType PCOM_TX__DCDC_HighVoltConnectionAllowed_GetInitial(boolean * value);
Std_ReturnType PCOM_TX__DCDC_HighVoltConnectionAllowed_GetCalculated(DCDC_HighVoltConnectionAllowed * value);
Std_ReturnType PCOM_TX__DCDC_HighVoltConnectionAllowed_Set(const DCDC_HighVoltConnectionAllowed * value);

Std_ReturnType PCOM_TX__DCDC_Fault_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_Fault_GetCalculated(DCDC_Fault * value);
Std_ReturnType PCOM_TX__DCDC_Fault_Set(const DCDC_Fault * value);

Std_ReturnType PCOM_TX__DCDC_InputVoltage_GetInitial(uint16 * value);
Std_ReturnType PCOM_TX__DCDC_InputVoltage_GetCalculated(DCDC_InputVoltage * value);
Std_ReturnType PCOM_TX__DCDC_InputVoltage_Set(const DCDC_InputVoltage * value);
boolean PCOM_TX_ProducerError_DCDC_InputVoltage_DCDC_InputVoltage(void);

Std_ReturnType PCOM_TX__DCDC_OVERTEMP_GetInitial(boolean * value);
Std_ReturnType PCOM_TX__DCDC_OVERTEMP_GetCalculated(DCDC_OVERTEMP * value);
Std_ReturnType PCOM_TX__DCDC_OVERTEMP_Set(const DCDC_OVERTEMP * value);

Std_ReturnType PCOM_TX__DCDC_OutputVoltage_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_OutputVoltage_GetCalculated(DCDC_OutputVoltage * value);
Std_ReturnType PCOM_TX__DCDC_OutputVoltage_Set(const DCDC_OutputVoltage * value);
boolean PCOM_TX_ProducerError_DCDC_OutputVoltage_DCDC_OutputVoltage(void);

Std_ReturnType PCOM_TX__DCDC_ActivedischargeSt_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_ActivedischargeSt_GetCalculated(DCDC_ActivedischargeSt * value);
Std_ReturnType PCOM_TX__DCDC_ActivedischargeSt_Set(const DCDC_ActivedischargeSt * value);

Std_ReturnType PCOM_TX__DCDC_OutputCurrent_GetInitial(uint16 * value);
Std_ReturnType PCOM_TX__DCDC_OutputCurrent_GetCalculated(DCDC_OutputCurrent * value);
Std_ReturnType PCOM_TX__DCDC_OutputCurrent_Set(const DCDC_OutputCurrent * value);
boolean PCOM_TX_ProducerError_DCDC_OutputCurrent_DCDC_OutputCurrent(void);

Std_ReturnType PCOM_TX__DCDC_InputCurrent_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__DCDC_InputCurrent_GetCalculated(DCDC_InputCurrent * value);
Std_ReturnType PCOM_TX__DCDC_InputCurrent_Set(const DCDC_InputCurrent * value);
boolean PCOM_TX_ProducerError_DCDC_InputCurrent_DCDC_InputCurrent(void);

Std_ReturnType PCOM_TX__OBC_DCDC_RT_POWER_LOAD_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_DCDC_RT_POWER_LOAD_GetCalculated(DCDC_OBCDCDCRTPowerLoad * value);
Std_ReturnType PCOM_TX__OBC_DCDC_RT_POWER_LOAD_Set(const DCDC_OBCDCDCRTPowerLoad * value);

Std_ReturnType PCOM_TX__OBC_MAIN_CONTACTOR_REQ_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_MAIN_CONTACTOR_REQ_GetCalculated(DCDC_OBCMainContactorReq * value);
Std_ReturnType PCOM_TX__OBC_MAIN_CONTACTOR_REQ_Set(const DCDC_OBCMainContactorReq * value);

Std_ReturnType PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_GetCalculated(DCDC_OBCQuickChargeContactorReq * value);
Std_ReturnType PCOM_TX__OBC_QUICK_CHARGE_CONTACTOR_REQ_Set(const DCDC_OBCQuickChargeContactorReq * value);

Std_ReturnType PCOM_TX__JDD_BYTE0_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE0_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE0_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE1_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE1_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE1_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE2_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE2_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE2_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE3_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE3_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE3_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE4_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE4_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE4_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE5_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE5_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE5_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE6_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE6_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE6_Set(const uint8 * value);

Std_ReturnType PCOM_TX__JDD_BYTE7_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__JDD_BYTE7_GetCalculated(uint8 * value);
Std_ReturnType PCOM_TX_JDD_BYTE7_Set(const uint8 * value);

/*
Std_ReturnType PCOM_TX__NUMERO_TRAME_GetCalculated(NUMERO_TRAME * value);
Std_ReturnType PCOM_TX__NUMERO_TRAME_Set(const NUMERO_TRAME * value);

Std_ReturnType PCOM_TX__ETAT_DTC_GetCalculated(ETAT_DTC * value);
Std_ReturnType PCOM_TX__ETAT_DTC_Set(const ETAT_DTC * value);

Std_ReturnType PCOM_TX__TYPE_MESSAGE_GetCalculated(TYPE_MESSAGE * value);
Std_ReturnType PCOM_TX__TYPE_MESSAGE_Set(const TYPE_MESSAGE * value);

Std_ReturnType PCOM_TX__KILOMETRAGE_JDD_GetCalculated(KILOMETRAGE_JDD * value);
Std_ReturnType PCOM_TX__KILOMETRAGE_JDD_Set(const KILOMETRAGE_JDD * value);
boolean PCOM_TX_ProducerError_KILOMETRAGE_JDD_KILOMETRAGE_JDD(void);

Std_ReturnType PCOM_TX__CODES_DEFAUT_GetCalculated(CODES_DEFAUT * value);
Std_ReturnType PCOM_TX__CODES_DEFAUT_Set(const CODES_DEFAUT * value);

Std_ReturnType PCOM_TX__SITUATION_VIE_JDD_GetCalculated(SITUATION_VIE_JDD * value);
Std_ReturnType PCOM_TX__SITUATION_VIE_JDD_Set(const SITUATION_VIE_JDD * value);

Std_ReturnType PCOM_TX__REFERENCE_HORAIRE_GetInitial(uint32 * value);
Std_ReturnType PCOM_TX__REFERENCE_HORAIRE_GetCalculated(REFERENCE_HORAIRE * value);
Std_ReturnType PCOM_TX__REFERENCE_HORAIRE_Set(const REFERENCE_HORAIRE * value);
boolean PCOM_TX_ProducerError_REFERENCE_HORAIRE_REFERENCE_HORAIRE(void);
*/
Std_ReturnType PCOM_TX__OBC_SocketTempL_GetInitial(OBC_SocketTempL * value);
Std_ReturnType PCOM_TX__OBC_SocketTempL_GetCalculated(OBC_SocketTempL * value);
Std_ReturnType PCOM_TX__OBC_SocketTempL_Set(const OBC_SocketTempL * value);
boolean PCOM_TX_ProducerError_OBC_SocketTempL_OBC_SocketTempL(void);

Std_ReturnType PCOM_TX__OBC_SocketTempN_GetInitial(OBC_SocketTempN * value);
Std_ReturnType PCOM_TX__OBC_SocketTempN_GetCalculated(OBC_SocketTempN * value);
Std_ReturnType PCOM_TX__OBC_SocketTempN_Set(const OBC_SocketTempN * value);
boolean PCOM_TX_ProducerError_OBC_SocketTempN_OBC_SocketTempN(void);

Std_ReturnType PCOM_TX__OBC_HighVoltConnectionAllowed_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_HighVoltConnectionAllowed_GetCalculated(OBC_HighVoltConnectionAllowed * value);
Std_ReturnType PCOM_TX__OBC_HighVoltConnectionAllowed_Set(const OBC_HighVoltConnectionAllowed * value);

Std_ReturnType PCOM_TX__OBC_ElockState_GetInitial(OBC_ElockState * value);
Std_ReturnType PCOM_TX__OBC_ElockState_GetCalculated(OBC_ElockState * value);
Std_ReturnType PCOM_TX__OBC_ElockState_Set(const OBC_ElockState * value);

Std_ReturnType PCOM_TX__OBC_ChargingMode_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_ChargingMode_GetCalculated(OBC_ChargingMode * value);
Std_ReturnType PCOM_TX__OBC_ChargingMode_Set(const OBC_ChargingMode * value);

Std_ReturnType PCOM_TX__OBC_Fault_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_Fault_GetCalculated(OBC_Fault * value);
Std_ReturnType PCOM_TX__OBC_Fault_Set(const OBC_Fault * value);

Std_ReturnType PCOM_TX__OBC_Status_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_Status_GetCalculated(OBC_Status * value);
Std_ReturnType PCOM_TX__OBC_Status_Set(const OBC_Status * value);
boolean PCOM_TX_ProducerError_OBC_Status_OBC_Status(void);

Std_ReturnType PCOM_TX__EVSE_RTAB_STOP_CHARGE_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__EVSE_RTAB_STOP_CHARGE_GetCalculated(EVSE_RTAB_STOP_CHARGE * value);
Std_ReturnType PCOM_TX__EVSE_RTAB_STOP_CHARGE_Set(const EVSE_RTAB_STOP_CHARGE * value);


Std_ReturnType PCOM_TX__OBC_CP_connection_Status_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_CP_connection_Status_GetCalculated(OBC_CP_connection_Status * value);
Std_ReturnType PCOM_TX__OBC_CP_connection_Status_Set(const OBC_CP_connection_Status * value);
boolean PCOM_TX_ProducerError_OBC_CP_connection_Status_OBC_CP_connection_Status(void);

Std_ReturnType PCOM_TX__OBC_OutputVoltage_GetInitial(uint16 * value);
Std_ReturnType PCOM_TX__OBC_OutputVoltage_GetCalculated(OBC_OutputVoltage * value);
Std_ReturnType PCOM_TX__OBC_OutputVoltage_Set(const OBC_OutputVoltage * value);
boolean PCOM_TX_ProducerError_OBC_OutputVoltage_OBC_OutputVoltage(void);

Std_ReturnType PCOM_TX__OBC_OutputCurrent_GetInitial(uint16 * value);
Std_ReturnType PCOM_TX__OBC_OutputCurrent_GetCalculated(OBC_OutputCurrent * value);
Std_ReturnType PCOM_TX__OBC_OutputCurrent_Set(const OBC_OutputCurrent * value);
boolean PCOM_TX_ProducerError_OBC_OutputCurrent_OBC_OutputCurrent(void);

Std_ReturnType PCOM_TX__OBC_InputVoltageSt_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_InputVoltageSt_GetCalculated(OBC_InputVoltageSt * value);
Std_ReturnType PCOM_TX__OBC_InputVoltageSt_Set(const OBC_InputVoltageSt * value);
boolean PCOM_TX_ProducerError_OBC_InputVoltageSt_OBC_InputVoltageSt(void);

Std_ReturnType PCOM_TX__OBC_CommunicationSt_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_CommunicationSt_GetCalculated(OBC_CommunicationSt * value);
Std_ReturnType PCOM_TX__OBC_CommunicationSt_Set(const OBC_CommunicationSt * value);

Std_ReturnType PCOM_TX__OBC_ACRange_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_ACRange_GetCalculated(OBC_ACRange * value);
Std_ReturnType PCOM_TX__OBC_ACRange_Set(const OBC_ACRange * value);

Std_ReturnType PCOM_TX__OBC_OBCTemp_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_OBCTemp_GetCalculated(OBC_OBCTemp * value);
Std_ReturnType PCOM_TX__OBC_OBCTemp_Set(const OBC_OBCTemp * value);

Std_ReturnType PCOM_TX__OBC_PowerMax_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_PowerMax_GetCalculated(OBC_PowerMax * value);
Std_ReturnType PCOM_TX__OBC_PowerMax_Set(const OBC_PowerMax * value);
boolean PCOM_TX_ProducerError_OBC_PowerMax_OBC_PowerMax(void);

Std_ReturnType PCOM_TX_SUP_RequestPFCStatus_GetCalculated(SUP_RequestPFCStatus * value);
Std_ReturnType PCOM_TX_SUP_RequestPFCStatus_Set(const SUP_RequestPFCStatus * value);

Std_ReturnType PCOM_TX_SUP_CommandVDCLink_V_GetCalculated(SUP_CommandVDCLink_V * value);
Std_ReturnType PCOM_TX_SUP_CommandVDCLink_V_Set(const SUP_CommandVDCLink_V * value);

Std_ReturnType PCOM_TX_SUP_RequestStatusDCHV_GetCalculated(SUP_RequestStatusDCHV * value);
Std_ReturnType PCOM_TX_SUP_RequestStatusDCHV_Set(const SUP_RequestStatusDCHV * value);

Std_ReturnType PCOM_TX_DCDC_VoltageReference_GetCalculated(DCDC_VoltageReference * value);
Std_ReturnType PCOM_TX_DCDC_VoltageReference_Set(const DCDC_VoltageReference * value);

Std_ReturnType PCOM_TX_DCDC_CurrentReference_GetCalculated(DCDC_CurrentReference * value);
Std_ReturnType PCOM_TX_DCDC_CurrentReference_Set(const DCDC_CurrentReference * value);

Std_ReturnType PCOM_TX_SUP_RequestDCLVStatus_GetCalculated(SUP_RequestDCLVStatus * value);
Std_ReturnType PCOM_TX_SUP_RequestDCLVStatus_Set(const SUP_RequestDCLVStatus * value);

Std_ReturnType PCOM_TX_DCLV_VoltageReference_GetCalculated(DCLV_VoltageReference * value);
Std_ReturnType PCOM_TX_DCLV_VoltageReference_Set(const DCLV_VoltageReference * value);

Std_ReturnType PCOM_TX_DCLV_Temp_Derating_Factor_GetCalculated(DCLV_Temp_Derating_Factor * value);
Std_ReturnType PCOM_TX_DCLV_Temp_Derating_Factor_Set(const DCLV_Temp_Derating_Factor * value);

Std_ReturnType PCOM_TX__OBC_OBCStartSt_GetInitial(OBC_OBCStartSt * value);
Std_ReturnType PCOM_TX__OBC_OBCStartSt_GetCalculated(OBC_OBCStartSt * value);
Std_ReturnType PCOM_TX__OBC_OBCStartSt_Set(const OBC_OBCStartSt * value);
boolean PCOM_TX_ProducerError_OBC_OBCStartSt_OBC_OBCStartSt(void);

Std_ReturnType PCOM_TX__OBC_PLUG_VOLT_DETECTION_GetInitial(OBC_PlugVoltDetection * value);
Std_ReturnType PCOM_TX__OBC_PLUG_VOLT_DETECTION_GetCalculated(OBC_PlugVoltDetection * value);
Std_ReturnType PCOM_TX__OBC_PLUG_VOLT_DETECTION_Set(const OBC_PlugVoltDetection * value);

Std_ReturnType PCOM_TX__OBC_ChargingConnectionConfirmation_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_ChargingConnectionConfirmation_GetCalculated(OBC_ChargingConnectionConfirmati * value);
Std_ReturnType PCOM_TX__OBC_ChargingConnectionConfirmation_Set(const OBC_ChargingConnectionConfirmati * value);
boolean PCOM_TX_ProducerError_OBC_ChargingConnectionConfirmation_OBC_ChargingConnectionConfirmation(void);

Std_ReturnType PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_GetCalculated(OBC_DCChargingPlugAConnConf * value);
Std_ReturnType PCOM_TX__OBC_DCChargingPlugAConnectionConfirmation_Set(const OBC_DCChargingPlugAConnConf * value);

Std_ReturnType PCOM_TX__OBC_HVBattRechargeWakeup_GetInitial(OBC_HVBattRechargeWakeup * value);
Std_ReturnType PCOM_TX__OBC_HVBattRechargeWakeup_GetCalculated(OBC_HVBattRechargeWakeup * value);
Std_ReturnType PCOM_TX__OBC_HVBattRechargeWakeup_Set(const OBC_HVBattRechargeWakeup * value);

Std_ReturnType PCOM_TX__OBC_PIStateInfoWakeup_GetInitial(OBC_PIStateInfoWakeup * value);
Std_ReturnType PCOM_TX__OBC_PIStateInfoWakeup_GetCalculated(OBC_PIStateInfoWakeup * value);
Std_ReturnType PCOM_TX__OBC_PIStateInfoWakeup_Set(const OBC_PIStateInfoWakeup * value);

Std_ReturnType PCOM_TX__OBC_HoldDiscontactorWakeup_GetInitial(OBC_HoldDiscontactorWakeup * value);
Std_ReturnType PCOM_TX__OBC_HoldDiscontactorWakeup_GetCalculated(OBC_HoldDiscontactorWakeup * value);
Std_ReturnType PCOM_TX__OBC_HoldDiscontactorWakeup_Set(const OBC_HoldDiscontactorWakeup * value);

Std_ReturnType PCOM_TX__OBC_COOLING_WAKEUP_GetInitial(OBC_CoolingWakeup * value);
Std_ReturnType PCOM_TX__OBC_COOLING_WAKEUP_GetCalculated(OBC_CoolingWakeup * value);
Std_ReturnType PCOM_TX__OBC_COOLING_WAKEUP_Set(const OBC_CoolingWakeup * value);


Std_ReturnType PCOM_TX__RECHARGE_HMI_STATE_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__RECHARGE_HMI_STATE_GetCalculated(OBC_RechargeHMIState * value);
Std_ReturnType PCOM_TX__RECHARGE_HMI_STATE_Set(const OBC_RechargeHMIState * value);

Std_ReturnType PCOM_TX__OBC_PUSH_CHARGE_TYPE_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__OBC_PUSH_CHARGE_TYPE_GetCalculated(OBC_PushChargeType * value);
Std_ReturnType PCOM_TX__OBC_PUSH_CHARGE_TYPE_Set(const OBC_PushChargeType * value);

Std_ReturnType PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_GetInitial(SUPV_StopDelayedHMIWupState * value);
Std_ReturnType PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_GetCalculated(SUPV_StopDelayedHMIWupState * value);
Std_ReturnType PCOM_TX__STOP_DELAYED_HMI_WUP_STATE_Set(const SUPV_StopDelayedHMIWupState * value);

Std_ReturnType PCOM_TX__HV_BATT_CHARGE_WUP_STATE_GetInitial(SUPV_HVBattChargeWupState * value);
Std_ReturnType PCOM_TX__HV_BATT_CHARGE_WUP_STATE_GetCalculated(SUPV_HVBattChargeWupState * value);
Std_ReturnType PCOM_TX__HV_BATT_CHARGE_WUP_STATE_Set(const SUPV_HVBattChargeWupState * value);

Std_ReturnType PCOM_TX__PI_STATE_INFO_WUP_STATE_GetInitial(SUPV_PIStateInfoWupState * value);
Std_ReturnType PCOM_TX__PI_STATE_INFO_WUP_STATE_GetCalculated(SUPV_PIStateInfoWupState * value);
Std_ReturnType PCOM_TX__PI_STATE_INFO_WUP_STATE_Set(const SUPV_PIStateInfoWupState * value);

Std_ReturnType PCOM_TX__PRE_DRIVE_WUP_STATE_GetInitial(SUPV_PreDriveWupState * value);
Std_ReturnType PCOM_TX__PRE_DRIVE_WUP_STATE_GetCalculated(SUPV_PreDriveWupState * value);
Std_ReturnType PCOM_TX__PRE_DRIVE_WUP_STATE_Set(const SUPV_PreDriveWupState * value);

Std_ReturnType PCOM_TX__POST_DRIVE_WUP_STATE_GetInitial(SUPV_PostDriveWupState * value);
Std_ReturnType PCOM_TX__POST_DRIVE_WUP_STATE_GetCalculated(SUPV_PostDriveWupState * value);
Std_ReturnType PCOM_TX__POST_DRIVE_WUP_STATE_Set(const SUPV_PostDriveWupState * value);

Std_ReturnType PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_GetInitial(SUPV_HoldDiscontactorWupState * value);
Std_ReturnType PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_GetCalculated(SUPV_HoldDiscontactorWupState * value);
Std_ReturnType PCOM_TX__HOLD_DISCONTACTOR_WUP_STATE_Set(const SUPV_HoldDiscontactorWupState * value);

Std_ReturnType PCOM_TX__PRECOND_ELEC_WUP_STATE_GetInitial(SUPV_PrecondElecWupState * value);
Std_ReturnType PCOM_TX__PRECOND_ELEC_WUP_STATE_GetCalculated(SUPV_PrecondElecWupState * value);
Std_ReturnType PCOM_TX__PRECOND_ELEC_WUP_STATE_Set(const SUPV_PrecondElecWupState * value);

Std_ReturnType PCOM_TX__DTC_REGISTRED_GetInitial(SUPV_DTCRegistred * value);
Std_ReturnType PCOM_TX__DTC_REGISTRED_GetCalculated(SUPV_DTCRegistred * value);
Std_ReturnType PCOM_TX__DTC_REGISTRED_Set(const SUPV_DTCRegistred * value);

Std_ReturnType PCOM_TX__ECU_ELEC_STATE_RCD_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__ECU_ELEC_STATE_RCD_GetCalculated(SUPV_ECUElecStateRCD * value);
Std_ReturnType PCOM_TX__ECU_ELEC_STATE_RCD_Set(const SUPV_ECUElecStateRCD * value);

Std_ReturnType PCOM_TX__RCD_LINE_STATE_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__RCD_LINE_STATE_GetCalculated(SUPV_RCDLineState * value);
Std_ReturnType PCOM_TX__RCD_LINE_STATE_Set(const SUPV_RCDLineState * value);

Std_ReturnType PCOM_TX__COOLING_WUP_STATE_GetInitial(uint8 * value);
Std_ReturnType PCOM_TX__COOLING_WUP_STATE_GetCalculated(SUPV_CoolingWupState * value);
Std_ReturnType PCOM_TX__COOLING_WUP_STATE_Set(const SUPV_CoolingWupState * value);


Std_ReturnType PCOM_TX__VERSION_SYSTEME_GetCalculated(VERSION_SYSTEME * value);
Std_ReturnType PCOM_TX__VERSION_SYSTEME_Set(const VERSION_SYSTEME * value);

Std_ReturnType PCOM_TX__VERS_DATE2_JOUR_GetCalculated(VERS_DATE2_JOUR * value);
Std_ReturnType PCOM_TX__VERS_DATE2_JOUR_Set(const VERS_DATE2_JOUR * value);

Std_ReturnType PCOM_TX__VERS_DATE2_MOIS_GetCalculated(VERS_DATE2_MOIS * value);
Std_ReturnType PCOM_TX__VERS_DATE2_MOIS_Set(const VERS_DATE2_MOIS * value);

Std_ReturnType PCOM_TX__VERS_DATE2_ANNEE_GetCalculated(VERS_DATE2_ANNEE * value);
Std_ReturnType PCOM_TX__VERS_DATE2_ANNEE_Set(const VERS_DATE2_ANNEE * value);

Std_ReturnType PCOM_TX__VERSION_APPLI_GetCalculated(VERSION_APPLI * value);
Std_ReturnType PCOM_TX__VERSION_APPLI_Set(const VERSION_APPLI * value);

Std_ReturnType PCOM_TX__VERSION_SOFT_GetCalculated(VERSION_SOFT * value);
Std_ReturnType PCOM_TX__VERSION_SOFT_Set(const VERSION_SOFT * value);

Std_ReturnType PCOM_TX__EDITION_SOFT_GetCalculated(EDITION_SOFT * value);
Std_ReturnType PCOM_TX__EDITION_SOFT_Set(const EDITION_SOFT * value);

Std_ReturnType PCOM_TX__EDITION_CALIB_GetCalculated(EDITION_CALIB * value);
Std_ReturnType PCOM_TX__EDITION_CALIB_Set(const EDITION_CALIB * value);


#endif


/* EOF */
