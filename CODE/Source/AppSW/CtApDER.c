/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApDER.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApDER
 *  Generation Time:  2020-11-23 11:42:16
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApDER>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApDER.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static non-init variables HERE... */

#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define DER_TEMPOFFSET			40U
#define DER_POWER_MAX 			1024U
#define DER_U8_MAX					0xFFU
#define DER_MAS_DIFF_TEMPERATURE	10
#define DER_OBC_TEMP_OFFSET	60U

/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/
typedef enum
{
	DER_PFC_M1,
	DER_PFC_M4,
	DER_DCHV_MOD_6,
	DER_DCHV_MOD_5,
	DER_OBC_AMBIENT,
	DER_OBC_NUM_CHANNELS
}DER_OBC_channels_t; /* PRQA S 3205 #Elemwnts of the enum are already used*/


typedef enum
{
	DER_DCLV_BUCK_A,
	DER_DCLV_BUCK_B,
	DER_DCLV_PP_A,
	DER_DCLV_PP_B,
	DER_DCLV_SYNC_RECT,
	DER_DCDC_NUM_CHANNELS
}DER_DCDC_channels_t;/* PRQA S 3205 #Elemwnts of the enum are already used*/



static uint16 DER_OBCtemp_offset40;



static void DER_process_DCLV_Derating(void);

static void DER_process_OBC_Derating(void);

static boolean DER_value_is_within_range(uint8 DER_ReferenceValue, IdtTempDerating DER_RangeLowValue,
																					IdtTempDerating DER_RangeHighValue);

static boolean DER_check_consistency_calibratable(IdtTempDerating PointA, IdtTempDerating PointB,
																		IdtTempDerating PointC, IdtTempDerating PointD);

static boolean DER_derating_calculate(boolean DeratingHighTemp, boolean DeratingLowTemp, boolean TempOutOfRange);

static boolean DER_check_temp_of_out_range(uint8 DCLVTemperature, IdtTempDerating HighTempEndDerating,
															IdtTempDerating LowTempEndDerating, boolean DataConsistency);

static uint16 DER_formula_derating_calculate(uint8 DCLVTemperature,
								   IdtTempDerating TempStartDerating, IdtTempDerating TempEndDerating, uint16 parameter);

static DCLV_Temp_Derating_Factor DER_dcdc_derating_task(uint8 DCLVTemperature,
		IdtTempDerating DCLVLowTempEndDerating, IdtTempDerating DCLVLowTempStartDerating, IdtTempDerating DCLVHighTempStartDerating, IdtTempDerating DCLVHighTempEndDerating);

static DCLV_Temp_Derating_Factor DER_GetMinimumDeratingDCLV(DCLV_Temp_Derating_Factor * input_DCLV_Temp_Derating_Factor, uint8 num_channels);

static void DER_dclv_ImplausibilityCheck(DCLV_Temp_Derating_Factor *IndivFactorDeratingDCLV);
static boolean DER_ImplausibilityCheck(uint8 Temp1, uint8 Temp2, uint8 Diff_max);

static IdtPDERATING_OBC DER_obc_derating_task(uint8 OBCTemperature,
				IdtTempDerating OBCLowTempEndDerating, IdtTempDerating OBCLowTempStartDerating, IdtTempDerating OBCHighTempStartDerating, IdtTempDerating OBCHighTempEndDerating);

static void DER_Temp_interpolation_DCLV(uint8 * DCLV_TempValue_data);
static void DER_Temp_interpolation_OBC(uint8 * OBC_TempValue_data);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCDC_Temperature: Integer in interval [0...255]
 * DCHV_ADC_NTC_MOD_5: Integer in interval [0...255]
 * DCHV_ADC_NTC_MOD_6: Integer in interval [0...255]
 * DCLV_T_PP_A: Integer in interval [0...255]
 * DCLV_T_PP_B: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_A: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_B: Integer in interval [0...255]
 * DCLV_Temp_Derating_Factor: Integer in interval [0...2047]
 * IdtAmbientTemperaturePhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtPDERATING_OBC: Integer in interval [0...1024]
 * IdtTempDerating: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtTempSyncRectPhysicalValue: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * OBC_OBCTemp: Integer in interval [0...255]
 * PFC_Temp_M1_C: Integer in interval [0...255]
 * PFC_Temp_M4_C: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtTempDerating Rte_CData_CalDCDC_FinishLinear(void)
 *   IdtTempDerating Rte_CData_CalDCDC_MaxDeviation(void)
 *   IdtTempDerating Rte_CData_CalDCDC_MinDeviation(void)
 *   IdtTempDerating Rte_CData_CalDCDC_StartLinear(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempEndDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVHighTempStartDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempEndDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_AmbTemp(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_Buck_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_Buck_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_PushPull_A(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_PushPull_B(void)
 *   IdtTempDerating Rte_CData_CalDCLVLowTempStartDerating_SyncRect(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempEndDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCHighTempStartDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempEndDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_DCHV_M5(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_DCHV_M6(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_PFC_M1(void)
 *   IdtTempDerating Rte_CData_CalOBCLowTempStartDerating_PFC_M4(void)
 *   IdtTempDerating Rte_CData_CalOBC_FinishLinear(void)
 *   IdtTempDerating Rte_CData_CalOBC_MaxDeviation(void)
 *   IdtTempDerating Rte_CData_CalOBC_MinDeviation(void)
 *   IdtTempDerating Rte_CData_CalOBC_StartLinear(void)
 *
 *********************************************************************************************************************/


#define CtApDER_START_SEC_CODE
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDER_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDER_CODE) RCtApDER_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	 DER_OBCtemp_offset40 = 0U;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDER_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(IdtAmbientTemperaturePhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data)
 *   Std_ReturnType Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(IdtTempSyncRectPhysicalValue *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(boolean data)
 *   Std_ReturnType Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(boolean data)
 *   Std_ReturnType Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data)
 *   Std_ReturnType Rte_Write_PpOBCDerating_DeOBCDerating(boolean data)
 *   Std_ReturnType Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDER_CODE) RCtApDER_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDER_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	DER_process_OBC_Derating();

	DER_process_DCLV_Derating();



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApDER_STOP_SEC_CODE
#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void DER_process_OBC_Derating(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;
	PFC_Temp_M1_C PFC_Temp_M1_C_temperature;
	PFC_Temp_M4_C PFC_Temp_M4_C_temperature;
	DCHV_ADC_NTC_MOD_6 DCHV_ADC_NTC_MOD_6_temperature;
	DCHV_ADC_NTC_MOD_5 DCHV_ADC_NTC_MOD_5_temperature;
	IdtAmbientTemperaturePhysicalValue AmbientTemperaturePhysicalValue;


	uint16 GlobalTemperature;
	IdtTempDerating temp_thresholds[4];
	IdtPDERATING_OBC PDERATING_OBC_var[DER_OBC_NUM_CHANNELS];
	OBC_OBCTemp TemperatureCorrected[DER_OBC_NUM_CHANNELS];
	boolean DER_OBCGlobalDeratingStatus;
	IdtPDERATING_OBC GlobalPDERATING_OBC_var;


	/* Get temperature */
	(void)Rte_Read_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(&PFC_Temp_M1_C_temperature);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	/* Adjust special case of PFC temperatures reporting */
	/* PFC temperatures have an offset of -50 but the other temperatures have an offset of -40 */
	if (PFC_Temp_M1_C_temperature <=10U)
	{
		PFC_Temp_M1_C_temperature = 0U;
	}
	else
	{
		PFC_Temp_M1_C_temperature -= 10U;
	}

	/* Adjust Value */
	DER_Temp_interpolation_OBC(&PFC_Temp_M1_C_temperature);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalOBCLowTempEndDerating_PFC_M1();
	temp_thresholds[1] = Rte_CData_CalOBCLowTempStartDerating_PFC_M1();
	temp_thresholds[2] = Rte_CData_CalOBCHighTempStartDerating_PFC_M1();
	temp_thresholds[3] = Rte_CData_CalOBCHighTempEndDerating_PFC_M1();

	/* Call function */
	PDERATING_OBC_var[DER_PFC_M1] = DER_obc_derating_task(PFC_Temp_M1_C_temperature,
			temp_thresholds[0], temp_thresholds[1], temp_thresholds[2], temp_thresholds[3]);
	TemperatureCorrected[DER_PFC_M1] = PFC_Temp_M1_C_temperature;

	/* Get temperature */
	(void)Rte_Read_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(&PFC_Temp_M4_C_temperature);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	/* Adjust special case of PFC temperatures reporting */
	/* PFC temperatures have an offset of -50 but the other temperatures have an offset of -40 */
	if (PFC_Temp_M4_C_temperature <= 10U)
	{
		PFC_Temp_M4_C_temperature = 0U;
	}
	else
	{
		PFC_Temp_M4_C_temperature -= 10U;
	}

	/* Adjust Value */
	DER_Temp_interpolation_OBC(&PFC_Temp_M4_C_temperature);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalOBCLowTempEndDerating_PFC_M4();
	temp_thresholds[1] = Rte_CData_CalOBCLowTempStartDerating_PFC_M4();
	temp_thresholds[2] = Rte_CData_CalOBCHighTempStartDerating_PFC_M4();
	temp_thresholds[3] = Rte_CData_CalOBCHighTempEndDerating_PFC_M4();

	/* Call function */
	PDERATING_OBC_var[DER_PFC_M4] = DER_obc_derating_task(PFC_Temp_M4_C_temperature,
			temp_thresholds[0], temp_thresholds[1], temp_thresholds[2], temp_thresholds[3]);
	TemperatureCorrected[DER_PFC_M4] = PFC_Temp_M4_C_temperature;

	/* Get temperature */
	(void)Rte_Read_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(&DCHV_ADC_NTC_MOD_6_temperature);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Adjust Value */
	DER_Temp_interpolation_OBC(&DCHV_ADC_NTC_MOD_6_temperature);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalOBCLowTempEndDerating_DCHV_M6();
	temp_thresholds[1] = Rte_CData_CalOBCLowTempStartDerating_DCHV_M6();
	temp_thresholds[2] = Rte_CData_CalOBCHighTempStartDerating_DCHV_M6();
	temp_thresholds[3] = Rte_CData_CalOBCHighTempEndDerating_DCHV_M6();

	/* Call function */
	PDERATING_OBC_var[DER_DCHV_MOD_6] = DER_obc_derating_task(DCHV_ADC_NTC_MOD_6_temperature,
			temp_thresholds[0], temp_thresholds[1], temp_thresholds[2], temp_thresholds[3]);
	TemperatureCorrected[DER_DCHV_MOD_6] = DCHV_ADC_NTC_MOD_6_temperature;


	/* Get temperature */
	(void)Rte_Read_PpAmbientTemperaturePhysicalValue_DeAmbientTemperaturePhysicalValue(&AmbientTemperaturePhysicalValue);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalOBCLowTempEndDerating_AmbTempOBC();
	temp_thresholds[1] = Rte_CData_CalOBCLowTempStartDerating_AmbTempOBC();
	temp_thresholds[2] = Rte_CData_CalOBCHighTempStartDerating_AmbTempOBC();
	temp_thresholds[3] = Rte_CData_CalOBCHighTempEndDerating_AmbTempOBC();

	/* Call function */
	PDERATING_OBC_var[DER_OBC_AMBIENT] = DER_obc_derating_task(AmbientTemperaturePhysicalValue,
			temp_thresholds[0], temp_thresholds[1], temp_thresholds[2], temp_thresholds[3]);
	TemperatureCorrected[DER_OBC_AMBIENT] = AmbientTemperaturePhysicalValue;



	/* Get temperature */
	(void)Rte_Read_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(&DCHV_ADC_NTC_MOD_5_temperature);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalOBCLowTempEndDerating_DCHV_M5();
	temp_thresholds[1] = Rte_CData_CalOBCLowTempStartDerating_DCHV_M5();
	temp_thresholds[2] = Rte_CData_CalOBCHighTempStartDerating_DCHV_M5();
	temp_thresholds[3] = Rte_CData_CalOBCHighTempEndDerating_DCHV_M5();

	/* Call function */

	if(0U!=DCHV_ADC_NTC_MOD_5_temperature)
	{
		PDERATING_OBC_var[DER_DCHV_MOD_5] = DER_obc_derating_task(DCHV_ADC_NTC_MOD_5_temperature,
				temp_thresholds[0], temp_thresholds[1], temp_thresholds[2], temp_thresholds[3]);
		TemperatureCorrected[DER_DCHV_MOD_5] = DCHV_ADC_NTC_MOD_5_temperature;
	}
	else
	{
		/*Don't use this temperature for derating*/
		PDERATING_OBC_var[DER_DCHV_MOD_5] = DER_POWER_MAX;
		TemperatureCorrected[DER_DCHV_MOD_5] = 0U;
	}




	/* Calculate most restricting output power */
	GlobalPDERATING_OBC_var = DER_POWER_MAX;
	DER_OBCGlobalDeratingStatus = FALSE;
	GlobalTemperature = 0U;
	for(index = 0U; index < (uint8)DER_OBC_NUM_CHANNELS; index++)
	{
		if (PDERATING_OBC_var[index] < GlobalPDERATING_OBC_var)
		{
			GlobalPDERATING_OBC_var = PDERATING_OBC_var[index];
			DER_OBCGlobalDeratingStatus = TRUE;
		}

		if(TemperatureCorrected[index]>GlobalTemperature)
		{
			GlobalTemperature = TemperatureCorrected[index];
		}

	}

	DER_OBCtemp_offset40 = GlobalTemperature;
	GlobalTemperature += DER_OBC_TEMP_OFFSET;
	if((uint16)DER_U8_MAX<GlobalTemperature)
	{
		GlobalTemperature = (uint16)DER_U8_MAX;
	}

	(void)Rte_Write_PpOBCDerating_DeOBCDerating(DER_OBCGlobalDeratingStatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpPDERATING_OBC_DePDERATING_OBC(GlobalPDERATING_OBC_var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_OBCTemp_OBC_OBCTemp((uint8)GlobalTemperature);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static void DER_process_DCLV_Derating(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	DCLV_Temp_Derating_Factor IndivFactorDeratingDCLV[DER_DCDC_NUM_CHANNELS];
	DCLV_Temp_Derating_Factor FactorDeratingDCLV;
	uint8 DCLV_output_temperature;
	IdtTempDerating temp_thresholds[4];
	DCLV_T_SW_BUCK_A DCLV_T_SW_BUCK_A_temp;
	DCLV_T_SW_BUCK_B DCLV_T_SW_BUCK_B_temp;
	DCLV_T_PP_A DCLV_T_PP_A_temp;
	DCLV_T_PP_B DCLV_T_PP_B_temp;
	/* Unused variable */
	/*
	IdtAmbientTemperaturePhysicalValue AmbientTemperaturePhysicalValueTemp;
	*/
	IdtTempSyncRectPhysicalValue TempSyncRectPhysicalValueTemp;


	/* Get temperature */
	(void)Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(&DCLV_T_SW_BUCK_A_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Adjust Value */
	DER_Temp_interpolation_DCLV(&DCLV_T_SW_BUCK_A_temp);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalDCLVLowTempEndDerating_Buck_A();
	temp_thresholds[1] = Rte_CData_CalDCLVLowTempStartDerating_Buck_A();
	temp_thresholds[2] = Rte_CData_CalDCLVHighTempStartDerating_Buck_A();
	temp_thresholds[3] = Rte_CData_CalDCLVHighTempEndDerating_Buck_A();

	/* Call function */
	IndivFactorDeratingDCLV[DER_DCLV_BUCK_A] = DER_dcdc_derating_task(
			DCLV_T_SW_BUCK_A_temp,
			temp_thresholds[0],
			temp_thresholds[1],
			temp_thresholds[2],
			temp_thresholds[3]);


	/* Get temperature */
	(void)Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(&DCLV_T_SW_BUCK_B_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Adjust Value */
	DER_Temp_interpolation_DCLV(&DCLV_T_SW_BUCK_B_temp);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalDCLVLowTempEndDerating_Buck_B();
	temp_thresholds[1] = Rte_CData_CalDCLVLowTempStartDerating_Buck_B();
	temp_thresholds[2] = Rte_CData_CalDCLVHighTempStartDerating_Buck_B();
	temp_thresholds[3] = Rte_CData_CalDCLVHighTempEndDerating_Buck_B();

	/* Call function */
	IndivFactorDeratingDCLV[DER_DCLV_BUCK_B] = DER_dcdc_derating_task(
			DCLV_T_SW_BUCK_B_temp,
			temp_thresholds[0],
			temp_thresholds[1],
			temp_thresholds[2],
			temp_thresholds[3]);


	/* Get temperature */
	(void)Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(&DCLV_T_PP_A_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Adjust Value */
	DER_Temp_interpolation_DCLV(&DCLV_T_PP_A_temp);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalDCLVLowTempEndDerating_PushPull_A();
	temp_thresholds[1] = Rte_CData_CalDCLVLowTempStartDerating_PushPull_A();
	temp_thresholds[2] = Rte_CData_CalDCLVHighTempStartDerating_PushPull_A();
	temp_thresholds[3] = Rte_CData_CalDCLVHighTempEndDerating_PushPull_A();

	/* Call function */
	IndivFactorDeratingDCLV[DER_DCLV_PP_A] = DER_dcdc_derating_task(
			DCLV_T_PP_A_temp,
			temp_thresholds[0],
			temp_thresholds[1],
			temp_thresholds[2],
			temp_thresholds[3]);


	/* Get temperature */
	(void)Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(&DCLV_T_PP_B_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Adjust Value */
	DER_Temp_interpolation_DCLV(&DCLV_T_PP_B_temp);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalDCLVLowTempEndDerating_PushPull_B();
	temp_thresholds[1] = Rte_CData_CalDCLVLowTempStartDerating_PushPull_B();
	temp_thresholds[2] = Rte_CData_CalDCLVHighTempStartDerating_PushPull_B();
	temp_thresholds[3] = Rte_CData_CalDCLVHighTempEndDerating_PushPull_B();

	/* Call function */
	IndivFactorDeratingDCLV[DER_DCLV_PP_B] = DER_dcdc_derating_task(
			DCLV_T_PP_B_temp,
			temp_thresholds[0],
			temp_thresholds[1],
			temp_thresholds[2],
			temp_thresholds[3]);


	/* Get temperature */
	(void)Rte_Read_PpTempSyncRectPhysicalValue_DeTempSyncRectPhysicalValue(&TempSyncRectPhysicalValueTemp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Adjust Value */
	DER_Temp_interpolation_DCLV(&TempSyncRectPhysicalValueTemp);

	/* Get calibratables */
	temp_thresholds[0] = Rte_CData_CalDCLVLowTempEndDerating_SyncRect();
	temp_thresholds[1] = Rte_CData_CalDCLVLowTempStartDerating_SyncRect();
	temp_thresholds[2] = Rte_CData_CalDCLVHighTempStartDerating_SyncRect();
	temp_thresholds[3] = Rte_CData_CalDCLVHighTempEndDerating_SyncRect();

	/* Call function */
	IndivFactorDeratingDCLV[DER_DCLV_SYNC_RECT] = DER_dcdc_derating_task(
			TempSyncRectPhysicalValueTemp,
			temp_thresholds[0],
			temp_thresholds[1],
			temp_thresholds[2],
			temp_thresholds[3]);

	DER_dclv_ImplausibilityCheck(IndivFactorDeratingDCLV);

	/* Calculate most restricting derating factor */
	FactorDeratingDCLV = DER_GetMinimumDeratingDCLV(IndivFactorDeratingDCLV, (uint8)DER_DCDC_NUM_CHANNELS);
	(void)Rte_Write_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(FactorDeratingDCLV);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Calculate the maximum temperature value */
	/* Change to reach 100 % coverage in UT */
	DCLV_output_temperature = DCLV_T_SW_BUCK_A_temp;
	if (DCLV_T_SW_BUCK_B_temp > DCLV_output_temperature)
	{
		DCLV_output_temperature = DCLV_T_SW_BUCK_B_temp;
	}
	if (DCLV_T_PP_A_temp > DCLV_output_temperature)
	{
		DCLV_output_temperature = DCLV_T_PP_A_temp;
	}
	if (DCLV_T_PP_B_temp > DCLV_output_temperature)
	{
		DCLV_output_temperature = DCLV_T_PP_B_temp;
	}
	if (TempSyncRectPhysicalValueTemp > DCLV_output_temperature)
	{
		DCLV_output_temperature = TempSyncRectPhysicalValueTemp;
	}
	if(DER_OBCtemp_offset40 > DCLV_output_temperature)
	{
		DCLV_output_temperature = (uint8)DER_OBCtemp_offset40;
	}

	(void)Rte_Write_PpInt_DCDC_Temperature_DCDC_Temperature(DCLV_output_temperature);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


}

static boolean DER_value_is_within_range(uint8 DER_ReferenceValue, IdtTempDerating DER_RangeLowValue,
																					IdtTempDerating DER_RangeHighValue)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean value_is_within_range;
	value_is_within_range = FALSE;


	if(DER_ReferenceValue > DER_RangeLowValue){
		if(DER_ReferenceValue < DER_RangeHighValue)
		{
			value_is_within_range = TRUE;
		}
	}
	return value_is_within_range;
}

static boolean DER_check_consistency_calibratable(IdtTempDerating PointA, IdtTempDerating PointB,
		IdtTempDerating PointC, IdtTempDerating PointD){

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean data_consistency;


	data_consistency = FALSE; /*Initialize as data unconsistent*/

	if((PointA < PointB)&& (PointB < PointC) && (PointC < PointD))
	{
		data_consistency = TRUE;
	}
	return data_consistency;
}

static boolean DER_derating_calculate(boolean DeratingHighTemp, boolean DeratingLowTemp, boolean TempOutOfRange)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean value_Derating;


	value_Derating = FALSE;
	if((DeratingHighTemp == TRUE) || (DeratingLowTemp == TRUE) || (TempOutOfRange == TRUE))
	{
		value_Derating = TRUE;
	}
	return value_Derating;
}

static boolean DER_check_temp_of_out_range(uint8 DCLVTemperature, IdtTempDerating HighTempEndDerating,
		IdtTempDerating LowTempEndDerating, boolean DataConsistency)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean TempOutOfRange;


	if((DCLVTemperature >= HighTempEndDerating)
											|| (DCLVTemperature <= LowTempEndDerating) || (FALSE == DataConsistency))
	{
		TempOutOfRange = TRUE;

	}
	else
	{
		TempOutOfRange = FALSE;
	}

	return TempOutOfRange;
}

static uint16 DER_formula_derating_calculate(uint8 DCLVTemperature,
IdtTempDerating TempStartDerating, IdtTempDerating TempEndDerating, uint16 parameter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 result;
	uint32 quotient;
	uint32 divisor;

	if (TempEndDerating != TempStartDerating)
	{
		if(TempEndDerating > TempStartDerating)
		{
			divisor = ((uint32)TempEndDerating - (uint32)TempStartDerating);
		}
		else
		{
			divisor = ((uint32)TempStartDerating - (uint32)TempEndDerating);
		}

		if(TempEndDerating > DCLVTemperature)
		{
			quotient = ((uint32)TempEndDerating - (uint32)DCLVTemperature);
		}
		else
		{
			quotient = ((uint32)DCLVTemperature - (uint32)TempEndDerating);
		}

		result = (uint16)(((uint32)((uint32)quotient*(uint32)parameter)
															/(uint32)divisor));
	}
	else
	{
		result = 0U;
	}

	return result;

}

static DCLV_Temp_Derating_Factor DER_dcdc_derating_task(uint8 DCLVTemperature,
		IdtTempDerating DCLVLowTempEndDerating, IdtTempDerating DCLVLowTempStartDerating, IdtTempDerating DCLVHighTempStartDerating, IdtTempDerating DCLVHighTempEndDerating)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	DCLV_Temp_Derating_Factor Factorderatingdclv;
	boolean DCLVDeratingHighTemp;
	boolean DCLVDeratingLowTemp;
	boolean DataConsistency;
	boolean TempOutOfRange;
	boolean DCLVDerating;


	DCLVDeratingLowTemp = DER_value_is_within_range(DCLVTemperature, DCLVLowTempEndDerating, DCLVLowTempStartDerating);
	DCLVDeratingHighTemp = DER_value_is_within_range(DCLVTemperature, DCLVHighTempStartDerating, DCLVHighTempEndDerating);
	DataConsistency = DER_check_consistency_calibratable(DCLVLowTempEndDerating, DCLVLowTempStartDerating,
																	DCLVHighTempStartDerating, DCLVHighTempEndDerating);
	TempOutOfRange = DER_check_temp_of_out_range( DCLVTemperature, DCLVHighTempEndDerating, DCLVLowTempEndDerating, DataConsistency);
	DCLVDerating = DER_derating_calculate(DCLVDeratingHighTemp, DCLVDeratingLowTemp, TempOutOfRange);

	if(FALSE == DCLVDerating)
	{
		Factorderatingdclv = DER_POWER_MAX;
	}
	else if(TRUE == TempOutOfRange)
	{
		Factorderatingdclv = 0;
	}
	else if(TRUE == DCLVDeratingHighTemp)/* && (FALSE == DCLVDeratingLowTemp)*/
	{
		/* Removed condition due to UT coverage. */
		Factorderatingdclv = (DCLV_Temp_Derating_Factor)DER_formula_derating_calculate(DCLVTemperature, DCLVHighTempStartDerating, DCLVHighTempEndDerating, DER_POWER_MAX);
	}
	else /* if((TRUE == DCLVDeratingLowTemp) && (FALSE == DCLVDeratingHighTemp)) */
	{
		/* The IF condition has been removed as it is redundant. Detected in UT */
		Factorderatingdclv = (DCLV_Temp_Derating_Factor)DER_formula_derating_calculate(DCLVTemperature, DCLVLowTempStartDerating, DCLVLowTempEndDerating, DER_POWER_MAX);
	}

	/* The following code has been removed as it has been detected as unreachable in the UT */
	/*
	else
	{
		Factorderatingdclv = 0;
	}
	*/

	return Factorderatingdclv;


}

static IdtPDERATING_OBC DER_obc_derating_task(uint8 OBCTemperature,
				IdtTempDerating OBCLowTempEndDerating, IdtTempDerating OBCLowTempStartDerating, IdtTempDerating OBCHighTempStartDerating, IdtTempDerating OBCHighTempEndDerating)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtPDERATING_OBC Pderating_OBC;
	boolean OBCDeratingHighTemp;
	boolean OBCDeratingLowTemp;
	boolean DataConsistency;
	boolean TempOutOfRange;
	boolean OBCDeratingTempValue;

	OBCDeratingLowTemp = DER_value_is_within_range(OBCTemperature, OBCLowTempEndDerating, OBCLowTempStartDerating);
	OBCDeratingHighTemp = DER_value_is_within_range(OBCTemperature, OBCHighTempStartDerating, OBCHighTempEndDerating);

	DataConsistency = DER_check_consistency_calibratable(OBCLowTempEndDerating, OBCLowTempStartDerating,
																	OBCHighTempStartDerating, OBCHighTempEndDerating);
	TempOutOfRange = DER_check_temp_of_out_range( OBCTemperature, OBCHighTempEndDerating, OBCLowTempEndDerating, DataConsistency);

	OBCDeratingTempValue = DER_derating_calculate(OBCDeratingHighTemp, OBCDeratingLowTemp, TempOutOfRange);


	if(FALSE==OBCDeratingTempValue)
	{
		Pderating_OBC = DER_POWER_MAX;
	}
	else if(FALSE != TempOutOfRange)
	{
		Pderating_OBC = 0U;
	}
	else if ((FALSE != OBCDeratingHighTemp) /*&& (FALSE == OBCDeratingLowTemp )*/)
	{
		/* Removed condition due to UT coverage. */
		Pderating_OBC = (IdtPDERATING_OBC)DER_formula_derating_calculate(OBCTemperature, OBCHighTempStartDerating, OBCHighTempEndDerating, DER_POWER_MAX);
	}
	else /*if ((FALSE == OBCDeratingHighTemp) && (FALSE != OBCDeratingLowTemp ))*/
	{
		/* The IF condition has been removed as it is redundant. Detected in UT */
		Pderating_OBC = (IdtPDERATING_OBC)DER_formula_derating_calculate(OBCTemperature, OBCLowTempStartDerating, OBCLowTempEndDerating, DER_POWER_MAX);
	}
	/*
	 * This ELSE statement has been removed because it is unreachable code and unnecessary. Detected by UT
	 */
	/*
	else
	{

		Pderating_OBC = 0U;
	}
	*/

	/* The following code has been removed as it has been detected as unreachable in the UT */
	/*
	if((FALSE == OBCDeratingHighTemp) && (FALSE == OBCDeratingLowTemp) && (FALSE == TempOutOfRange))
	{
		*DeratingStatus = FALSE;
		Pderating_OBC = 11000U;
	}
	*/

	return Pderating_OBC;

}


static DCLV_Temp_Derating_Factor DER_GetMinimumDeratingDCLV(DCLV_Temp_Derating_Factor * input_DCLV_Temp_Derating_Factor, uint8 num_channels)/* PRQA S 3673 #It is used to know if it is initialised*/
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	DCLV_Temp_Derating_Factor ret_value = 0x400;
	uint8 index;

	if ((void*)NULL_PTR != input_DCLV_Temp_Derating_Factor)
	{
		for (index = 0; index < num_channels; index++)
		{
			if (input_DCLV_Temp_Derating_Factor[index] < ret_value)
			{
				ret_value = input_DCLV_Temp_Derating_Factor[index];
			}
		}
	}

	return ret_value;
}
static void DER_dclv_ImplausibilityCheck(DCLV_Temp_Derating_Factor *IndivFactorDeratingDCLV)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	DCLV_T_SW_BUCK_A DCLV_T_SW_BUCK_A_temp;
	DCLV_T_SW_BUCK_B DCLV_T_SW_BUCK_B_temp;
	DCLV_T_PP_A DCLV_T_PP_A_temp;
	DCLV_T_PP_B DCLV_T_PP_B_temp;


	/*Output*/
	boolean PushPull_Implausibilitity;
	boolean Buck_Implausibilitity;
	boolean ProducerError;


	/* Get temperature */
	(void)Rte_Read_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(&DCLV_T_PP_B_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(&DCLV_T_PP_A_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(&DCLV_T_SW_BUCK_B_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(&DCLV_T_SW_BUCK_A_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	PushPull_Implausibilitity = DER_ImplausibilityCheck(DCLV_T_PP_A_temp,DCLV_T_PP_B_temp,DER_MAS_DIFF_TEMPERATURE);
	Buck_Implausibilitity = DER_ImplausibilityCheck(DCLV_T_SW_BUCK_A_temp,DCLV_T_SW_BUCK_B_temp,DER_MAS_DIFF_TEMPERATURE);

	ProducerError = PushPull_Implausibilitity || Buck_Implausibilitity;/* PRQA S 4558, 4404, 4558 #We do not use a native bool type, GNU-C supports this implementation*/

	if(NULL_PTR!=IndivFactorDeratingDCLV)
	{
		if(FALSE!=PushPull_Implausibilitity)
		{
			/*Dont use this temperature for derating*/
			IndivFactorDeratingDCLV[DER_DCLV_PP_A] = DER_POWER_MAX;
			IndivFactorDeratingDCLV[DER_DCLV_PP_B] = DER_POWER_MAX;
		}

		if(FALSE!=Buck_Implausibilitity)
		{
			IndivFactorDeratingDCLV[DER_DCLV_BUCK_A] = DER_POWER_MAX;
			IndivFactorDeratingDCLV[DER_DCLV_BUCK_B] = DER_POWER_MAX;

		}
	}

	(void)Rte_Write_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(ProducerError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpImplausibilityTempPushPull_DeImplausibilityTempPushPull(PushPull_Implausibilitity);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpImplausibilityTempBuck_DeImplausibilityTempBuck(Buck_Implausibilitity);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static boolean DER_ImplausibilityCheck(uint8 Temp1, uint8 Temp2, uint8 Diff_max)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	sint16 Diff;
	boolean retVal;

	Diff = (sint16)(uint16)Temp1-(sint16)(uint16)Temp2;

	if(((sint16)(uint16)Diff_max<Diff)||(-(sint16)(uint16)Diff_max>Diff))
	{
		retVal = TRUE;
	}
	else
	{
		retVal = FALSE;
	}

	return retVal;
}

static void DER_Temp_interpolation_DCLV(uint8 *DCLV_TempValue_data)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	sint16 FinishLinear_data;
	sint16 MaxDeviation_data;
	sint16 MinDeviation_data;
	sint16 StartLinear_data;
	sint16 Offset_data;
	sint16 sTempValue;

	if(NULL_PTR!=DCLV_TempValue_data)
	{
		/* Cast value to work with signed values */
		sTempValue = (sint16)(uint16)(*DCLV_TempValue_data);

		/* Get Calibratable values */
		FinishLinear_data = (sint16)(Rte_CData_CalDCDC_FinishLinear());
		MaxDeviation_data = ((sint16)Rte_CData_CalDCDC_MaxDeviation() - (sint16)DER_TEMPOFFSET);
		MinDeviation_data = ((sint16)Rte_CData_CalDCDC_MinDeviation() -(sint16) DER_TEMPOFFSET);
		StartLinear_data = (sint16)(Rte_CData_CalDCDC_StartLinear());

		/* Get offset value by a linear interpolation */
		if (StartLinear_data >= sTempValue)
		{
			Offset_data = MinDeviation_data;
		}
		else if (FinishLinear_data <= sTempValue)
		{
			Offset_data = MaxDeviation_data;
		}
		else
		{
			/*Max result is a S16*/
			Offset_data = MinDeviation_data + (sint16)((((sint32)MaxDeviation_data -(sint32) MinDeviation_data) * ((sint32)sTempValue - (sint32)StartLinear_data)) /
					((sint32)FinishLinear_data - (sint32)StartLinear_data)) ;
		}

		/* Add offset to the signed signal */
		sTempValue = sTempValue - (Offset_data);

		/* Update signal */
		if((sint16)0>sTempValue)
		{
			/*Underflow*/
			(*DCLV_TempValue_data) = 0U;
		}
		else if((sint16)(uint16)DER_U8_MAX<sTempValue)
		{
			/*Overflow*/
			(*DCLV_TempValue_data) = DER_U8_MAX;
		}
		else
		{
			(*DCLV_TempValue_data) = (uint8)(uint16)(sTempValue);
		}
	}
}

static void DER_Temp_interpolation_OBC(uint8 *OBC_TempValue_data)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDER_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDER_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDER_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDER_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDER_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	sint16 FinishLinear_data;
	sint16 MaxDeviation_data;
	sint16 MinDeviation_data;
	sint16 StartLinear_data;
	sint16 Offset_data;
	sint16 sTempValue;

	if(NULL_PTR!=OBC_TempValue_data)
	{
		/* Cast value to work with signed values */
		sTempValue = (sint16)(uint16)(*OBC_TempValue_data);

		/* Get Calibratable values */
		FinishLinear_data = (sint16)(Rte_CData_CalOBC_FinishLinear());
		MaxDeviation_data = ((sint16)Rte_CData_CalOBC_MaxDeviation() - (sint16) DER_TEMPOFFSET);
		MinDeviation_data = ((sint16)Rte_CData_CalOBC_MinDeviation() - (sint16) DER_TEMPOFFSET);
		StartLinear_data = (sint16)(Rte_CData_CalOBC_StartLinear());

		/* Get offset value by a linear interpolation */
		if (StartLinear_data >= sTempValue)
		{
			Offset_data = MinDeviation_data;
		}
		else if (FinishLinear_data <= sTempValue)
		{
			Offset_data = MaxDeviation_data;
		}
		else
		{
			/*Max result is a S16*/
			Offset_data = MinDeviation_data + (sint16)((((sint32)MaxDeviation_data - (sint32)MinDeviation_data) * ((sint32)sTempValue - (sint32)StartLinear_data)) /
					((sint32)FinishLinear_data - (sint32)StartLinear_data)) ;
		}

		/* Add offset to the signed signal */
		sTempValue = sTempValue - (Offset_data);

		/* Update signal */
		if((sint16)0>sTempValue)
		{
			/*Underflow*/
			(*OBC_TempValue_data) = 0U;
		}
		else if((sint16)(uint16)DER_U8_MAX<sTempValue)
		{
			/*Overflow*/
			(*OBC_TempValue_data) = DER_U8_MAX;
		}
		else
		{
			(*OBC_TempValue_data) = (uint8)(uint16)(sTempValue);
		}
	}
}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
