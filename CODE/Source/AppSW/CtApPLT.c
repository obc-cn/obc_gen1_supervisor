/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPLT.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApPLT
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPLT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup PLT
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Plant Mode
 * \details This module generates the HMI that controles the LEDs in plant
 * mode.
 *
 * \{
 * \file  PLT.c
 * \brief  Generic code of the PLT module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApPLT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */

#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Square root of 3 gain.*/
#define PLT_SQRT3_GAIN		37837U
/** Square root of 3 Q factor.*/
#define PLT_SQRT3_Q			16U

/** Timer inactive value.*/
#define PLT_TIME_OUT_INACTIVE	0xFFU
 /* PRQA S 0857,0380 --*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

#define PLT_VEHSPD_FACTOR 100U /* ABS_VehSpd Factor 0.01; IdtVehStopMaxTest Factor 1 */

/** HMI states*/
#define	PLT_HMISTATE_DISCONNECTED 0x00U /*! Hose disconnected.*/
#define	PLT_HMISTATE_INPROGRESS 0x01U /*! Charge in progress*/
#define	PLT_HMISTATE_FAILURE 0x02U /*! Charge failed*/
#define	PLT_HMISTATE_STOPPED 0x03 /*! Charge stopped*/
#define	PLT_HMISTATE_FINISHED 0x04 /*! Charge finished*/
#define	PLT_HMISTATE_NUMBER 5

#define PLT_TEST_UT_PLUGIN_NOT_INIT 0xFFU

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

/** Calibratables */
typedef struct {
	IdtProximityDetectPlantMode max_proximity_detect_plant_mode;
	IdtProximityDetectPlantMode min_proximity_detect_plant_mode;
	IdtDebounceControlPilotPlantMode debounce_control_pilot_plant_mode;
	IdtDebounceDCRelayVoltagePlantMode debounce_DC_relay_voltage_plant_mode;
	IdtDebouncePhasePlantMode debounce_phase_plant_mode;
	IdtDebounceProximityPlantMode debounce_proximity_plant_mode;
	IdtControlPilotDutyPlantMode max_control_pilot_duty_plant_mode;
	IdtDCRelayVoltagePlantMode max_DC_relay_voltage_plant_mode;
	IdtPhasePlantmode max_phase_plant_mode;
	IdtMaxTimePlantMode max_time_plant_mode;
	IdtControlPilotDutyPlantMode min_control_pilot_duty_plant_mode;
	IdtDCRelayVoltagePlantMode min_DC_relay_voltage_plant_mode;
	IdtPhasePlantmode min_phase_plant_mode;
	uint8 nv_m_need_default_value;
	boolean inhibit_control_pilot_detection;
	boolean inhibit_DC_relay_voltage;
	boolean inhibit_phase_voltage_detection;
	boolean inhibit_proximity_detection;
	IdtGlobalTimeoutPlantMode global_timeout_plant_mode;
	IdtProximityVoltageStartingPlantMode proximity_voltage_starting_plant_mode;
} plt_calibratables_t;

typedef struct {
	IdtDutyControlPilot duty_control_pilot;
	BMS_DCRelayVoltage dc_relay_voltage;
	OBC_ChargingConnectionConfirmati obc_charging_connection_confirmati;
	PFC_VPH12_Peak_V vph12_peak_v;
	PFC_VPH23_Peak_V vph23_peak_v;
	PFC_VPH31_Peak_V vph31_peak_v;
	VCU_ModeEPSRequest vcu_mode_eps_request;
	VCU_PosShuntJDD vcu_pos_shunt_jdd;
	IdtProximityDetectPhysicalValue proximity_detect_physical_value;
	boolean shutdown_authorization;
} plt_variables_t;

typedef struct {
	IdtBatteryVolt battery_volt;
	ABS_VehSpd abs_veh_spd;
	VCU_EtatGmpHyb etat_gmp_hyb;
	uint32 battery_volt_max_test;
	uint32 battery_volt_min_test;
	uint32 veh_stop_max_test;
} condition_variables_t;

typedef struct
{
  boolean ac_phase_out_of_range;
  boolean proxi_voltage_out_of_range;
  boolean pilot_duty_out_of_range;
  boolean dc_lines_out_of_range;
} plt_errors_t;


typedef enum  {
	PLT_INTERNAL_STATE_INACTIVE, 		   /*  PlantModeState == FALSE */
	PLT_INTERNAL_STATE_WAITING_CONNECTION, /*  PlantModeState == TRUE and TimeoutPlantMode == Inactive */
	PLT_INTERNAL_STATE_IN_PROGRESS,        /*  PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has NOT reached the end */
	PLT_INTERNAL_STATE_COMPLETED,          /*  PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end */
	PLT_INTERNAL_STATE_FINISHED           /*  PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end
	                                        *  and OBC_ChargingConnectionConfirmation_CANSignal switches from 'Full connected' (0x02) to 'Not connected' */
} internal_state_t;





/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

/* OBCP-30071 */
static boolean PLT_Afts_bAcvBEPRTestMode = FALSE;

#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/

static void PLT_Process_HMI_State(IdtRECHARGE_HMI_STATE hmi_state, uint8 eps_mode);
static void PLT_Read_Calibratables(plt_calibratables_t* p_calibratables);
static void PLT_Read_Variables(plt_variables_t* p_variables);
static Dcm_NegativeResponseCodeType PLT_Routine_Control_Conditions_Check(void);
static boolean PLT_Check_Range(uint16 input, uint16 upper_trheshold, uint16 lower_treshold);
static void PLT_Set_DTCS(const plt_errors_t* p_errors);
static void PLT_Heal_DTCS(void);

static internal_state_t PLT_Update_State(const plt_variables_t* p_variables,
										 const plt_calibratables_t* p_calibratables,
										 boolean test_mode,
										 uint8 test_ut_plugin,
										 boolean* p_state_change
								        );
static boolean PLT_Check_Errors(const plt_variables_t* p_variables,
						        const plt_calibratables_t* p_calibratables,
  						        plt_errors_t* p_errors,
								boolean reset
						       );

static boolean PLT_Check_AC_Phase_Out_Of_Range(const plt_calibratables_t* p_calibratables,
											   PFC_VPH12_Peak_V vph12_peak_v,
										       PFC_VPH23_Peak_V vph23_peak_v,
										       PFC_VPH31_Peak_V vph31_peak_v,
											   boolean reset
	                                         );

static boolean PLT_Check_Proxi_Voltage_Out_Of_Range(const plt_calibratables_t* p_calibratables,
											        IdtProximityDetectPhysicalValue proximity_detect_physical_value,
													boolean reset
	   	   	   	   	   	   	   	   	   	   	       );

static boolean PLT_Check_Pilot_Duty_Out_Of_Range(const plt_calibratables_t* p_calibratables,
										         IdtDutyControlPilot duty_control_pilot,
												 boolean reset
	   	   	   	   	   	   	   	   	   	        );

static boolean PLT_Check_DC_Lines_Out_Of_Range(const plt_calibratables_t* p_calibratables,
										       BMS_DCRelayVoltage dc_relay_voltage,
											   boolean reset
	   	   	   	   	   	   	   	   	   	      );
static void PLT_Update_DID_Variables(boolean test_mode,uint8 test_utp_plugin, const plt_errors_t* errors);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtControlPilotDutyPlantMode: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDCRelayVoltagePlantMode: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtDebounceControlPilotPlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceDCRelayVoltagePlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebouncePhasePlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceProximityPlantMode: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtGlobalTimeoutPlantMode: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtMaxTimePlantMode: Integer in interval [0...250]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtPhasePlantmode: Integer in interval [0...220]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtProximityDetectPhysicalValue: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityDetectPlantMode: Integer in interval [0...1200]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtProximityVoltageStartingPlantMode: Integer in interval [0...1200]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtRECHARGE_HMI_STATE: Integer in interval [0...255]
 * IdtTimeoutPlantMode: Integer in interval [0...255]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * Rte_DT_IdtPlantModeTestUTPlugin_0: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtPlantModeDTCNumber: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_DTC_10C413 (0U)
 *   PLT_DTC_10C512 (1U)
 *   PLT_DTC_10C613 (2U)
 *   PLT_DTC_10C713 (3U)
 * IdtPlantModeTestInfoDID_Result: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_TEST_NOT_DONE (0U)
 *   PLT_TEST_DONE_WITH_ERROR (1U)
 *   PLT_TEST_DONE_WITHOUT_ERROR (2U)
 * IdtPlantModeTestInfoDID_State: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_CHARGE_MODE (0U)
 *   PLT_TEST_MODE (1U)
 * IdtPlantModeTestInfoDID_Test: Enumeration of integer in interval [0...255] with enumerators
 *   PLT_TEST_NOK (0U)
 *   PLT_TEST_OK (1U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_PosShuntJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Park (0U)
 *   Cx1_Client (1U)
 *   Cx2_Absent (2U)
 *   Cx3_Undetermined (3U)
 *
 * Array Types:
 * ============
 * IdtPlantModeTestUTPlugin: Array with 2 element(s) of type Rte_DT_IdtPlantModeTestUTPlugin_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtTimeoutPlantMode *Rte_Pim_PimGlobalTimeoutPlantMode(void)
 *   IdtTimeoutPlantMode *Rte_Pim_PimTimeoutPlantMode(void)
 *   boolean *Rte_Pim_PimPlantModeControlPilot(void)
 *   boolean *Rte_Pim_PimPlantModeDCRelayVoltage(void)
 *   boolean *Rte_Pim_PimPlantModePhaseVoltage(void)
 *   boolean *Rte_Pim_PimPlantModeProximity(void)
 *   Rte_DT_IdtPlantModeTestUTPlugin_0 *Rte_Pim_PimPlantModeTestUTPlugin(void)
 *     Returnvalue: Rte_DT_IdtPlantModeTestUTPlugin_0* is of type IdtPlantModeTestUTPlugin
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtGlobalTimeoutPlantMode Rte_CData_CalGlobalTimeoutPlantMode(void)
 *   IdtProximityDetectPlantMode Rte_CData_CalMaxProximityDetectPlantMode(void)
 *   IdtProximityDetectPlantMode Rte_CData_CalMinProximityDetectPlantMode(void)
 *   IdtProximityVoltageStartingPlantMode Rte_CData_CalProximityVoltageStartingPlantMode(void)
 *   IdtDebounceControlPilotPlantMode Rte_CData_CalDebounceControlPilotPlantMode(void)
 *   IdtDebounceDCRelayVoltagePlantMode Rte_CData_CalDebounceDCRelayVoltagePlantMode(void)
 *   IdtDebouncePhasePlantMode Rte_CData_CalDebouncePhasePlantMode(void)
 *   IdtDebounceProximityPlantMode Rte_CData_CalDebounceProximityPlantMode(void)
 *   IdtControlPilotDutyPlantMode Rte_CData_CalMaxControlPilotDutyPlantMode(void)
 *   IdtDCRelayVoltagePlantMode Rte_CData_CalMaxDCRelayVoltagePlantMode(void)
 *   IdtPhasePlantmode Rte_CData_CalMaxPhasePlantMode(void)
 *   IdtMaxTimePlantMode Rte_CData_CalMaxTimePlantMode(void)
 *   IdtControlPilotDutyPlantMode Rte_CData_CalMinControlPilotDutyPlantMode(void)
 *   IdtDCRelayVoltagePlantMode Rte_CData_CalMinDCRelayVoltagePlantMode(void)
 *   IdtPhasePlantmode Rte_CData_CalMinPhasePlantMode(void)
 *   boolean Rte_CData_CalInhibitControlPilotDetection(void)
 *   boolean Rte_CData_CalInhibitDCRelayVoltage(void)
 *   boolean Rte_CData_CalInhibitPhaseVoltageDetection(void)
 *   boolean Rte_CData_CalInhibitProximityDetection(void)
 *   Rte_DT_IdtPlantModeTestUTPlugin_0 *Rte_CData_PLTNvMNeed_DefaultValue(void)
 *     Returnvalue: Rte_DT_IdtPlantModeTestUTPlugin_0* is of type IdtPlantModeTestUTPlugin
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtBatteryVoltMaxTest Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
 *   IdtBatteryVoltMinTest Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
 *   IdtVehStopMaxTest Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
 *
 *********************************************************************************************************************/


#define CtApPLT_START_SEC_CODE
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_init_doc
 *********************************************************************************************************************/

/*!
 * \brief PLT initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLT_CODE) RCtApPLT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPLT_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
 *   Std_ReturnType Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(IdtProximityDetectPhysicalValue *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeChargeError(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeGuideManagement(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(boolean data)
 *   Std_ReturnType Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean data)
 *   Std_ReturnType Rte_Write_PpPlantModeState_DePlantModeState(boolean data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(IdtPlantModeTestInfoDID_Result data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(IdtPlantModeTestInfoDID_State data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(IdtPlantModeTestInfoDID_Test data)
 *   Std_ReturnType Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(IdtPlantModeDTCNumber DTC_Id)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(IdtPlantModeDTCNumber DTC_Id)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_Defs_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_task100ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief PLT task.
  *
  * This function is called periodically by the scheduler. This function should
  * update the HMI state depending on the plant mode state.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPLT_CODE) RCtApPLT_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPLT_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static  plt_errors_t st_errors;

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 st_test_utp_plugin = PLT_TEST_UT_PLUGIN_NOT_INIT;
	static boolean st_test_mode = FALSE;
	static boolean st_any_error = FALSE;
	static boolean st_dtc_errors = FALSE;


	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	plt_calibratables_t calibratables;
	plt_variables_t variables;
	internal_state_t internal_state;
	boolean internal_state_change;
	IdtELockSetPoint elock_setpoint_plant_mode;
	IdtRECHARGE_HMI_STATE recharge_hmi_state_plant_mode;
	boolean lock_led2bepr_plant_mode;
	uint8 resetRequest;

	PLT_Read_Calibratables(/* Out */ &calibratables);

	PLT_Read_Variables(/* Out */ &variables);

	/* OBCP-30094 */
	/* During initialization phase, the variable TestUTPlugin shall take the value saved in NVM */
	if (PLT_TEST_UT_PLUGIN_NOT_INIT == st_test_utp_plugin)
	{
		st_test_utp_plugin = (uint8)(Rte_Pim_PimPlantModeTestUTPlugin()[0]);
		PLT_Afts_bAcvBEPRTestMode = (uint8)(Rte_Pim_PimPlantModeTestUTPlugin()[1]);
	}

	/* OBCP-30092 */
	/* When bAcvBEPRTestMode switches from FALSE to TRUE shall set TestUTPlugin = 'Not Done' (0x00). */
	if ((FALSE == st_test_mode) && (FALSE != PLT_Afts_bAcvBEPRTestMode))
	{
		st_test_utp_plugin = PLT_TEST_NOT_DONE;
	}
	st_test_mode = PLT_Afts_bAcvBEPRTestMode;

	/* OBCP-24716, OBCP-24717 */
	/* internal_state:
	 * PLT_INTERNAL_STATE_INACTIVE           => PlantModeState == FALSE
	 * PLT_INTERNAL_STATE_WAITING_CONNECTION => PlantModeState == TRUE and TimeoutPlantMode == Inactive
	 * PLT_INTERNAL_STATE_IN_PROGRESS        => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has NOT reached the end
	 * PLT_INTERNAL_STATE_COMPLETED          => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end
	 * PLT_INTERNAL_STATE_FINISHED           => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end
     *                                           and OBC_ChargingConnectionConfirmation_CANSignal switches from 'Full connected' (0x02) to 'Not connected' */
	internal_state =  PLT_Update_State(/* In */ &variables,
									   /* In */ &calibratables,
									  st_test_mode,
									  st_test_utp_plugin,
									  /* Out */ &internal_state_change
									  );


	switch (internal_state)
	{

		case PLT_INTERNAL_STATE_IN_PROGRESS:
					  /* OBCP-24722 */
					 elock_setpoint_plant_mode = ELOCK_SETPOINT_LOCKED;
					 lock_led2bepr_plant_mode = TRUE;
					 recharge_hmi_state_plant_mode = PLT_HMISTATE_INPROGRESS;

					 /* OBCP-24740, OBCP-24739, OBCP-24751, OBCP-24752 */
					 st_any_error =  PLT_Check_Errors( /* in */ &variables,
												       /* in */ &calibratables,
												       /* out */ &st_errors,
													   internal_state_change  /* Reset when first time */
					 						         );

					 break;
		case PLT_INTERNAL_STATE_COMPLETED:
				  /* OBCP-30087 */
				 elock_setpoint_plant_mode = ELOCK_SETPOINT_UNLOCKED;
				 lock_led2bepr_plant_mode = FALSE;
				 if (FALSE != st_any_error)
				 {
					 /* OBCP-24753 */
					 recharge_hmi_state_plant_mode = PLT_HMISTATE_FAILURE;

					 /* OBCP-24754, OBCP-24756, OBCP-24761, OBCP-24760 */
					 if (FALSE != internal_state_change)
					 {
						 PLT_Set_DTCS(/* in */ &st_errors);
						 st_dtc_errors = TRUE;
					 }
				 }
				 else
				 {
					 /* OBCP-24755 */
					 recharge_hmi_state_plant_mode = PLT_HMISTATE_FINISHED;
				 }
				 break;

		case PLT_INTERNAL_STATE_FINISHED:
				elock_setpoint_plant_mode = ELOCK_SETPOINT_UNLOCKED;
				lock_led2bepr_plant_mode = FALSE;
				/* OBCP-30090 */
			 	recharge_hmi_state_plant_mode = PLT_HMISTATE_DISCONNECTED;

				if (FALSE != st_any_error)
				{
					/* OBCP-30089 */
					st_test_utp_plugin = PLT_TEST_DONE_WITH_ERROR;
				}
				else
				{
					/* OBCP-30088 */
					st_test_utp_plugin = PLT_TEST_DONE_WITHOUT_ERROR;
				}
				/* Finish test and store result on NVM */
				PLT_Afts_bAcvBEPRTestMode = FALSE;
			 	break;

		case PLT_INTERNAL_STATE_WAITING_CONNECTION:
			 /* OBCP-24718 */
			 elock_setpoint_plant_mode = ELOCK_SETPOINT_UNLOCKED;
			 lock_led2bepr_plant_mode = FALSE;
			 recharge_hmi_state_plant_mode = PLT_HMISTATE_DISCONNECTED;
			 break;

		case PLT_INTERNAL_STATE_INACTIVE:
		default:
			 elock_setpoint_plant_mode = ELOCK_SETPOINT_UNLOCKED;
			 lock_led2bepr_plant_mode = FALSE;
			 recharge_hmi_state_plant_mode = PLT_HMISTATE_DISCONNECTED;
			 break;
	}


	/* OBCP-30086, OBCP-30085, OBCP-24759, OBCP-24758, OBCP-24757 */
	PLT_Process_HMI_State(recharge_hmi_state_plant_mode, variables.vcu_mode_eps_request);

	PLT_Update_DID_Variables(st_test_mode,st_test_utp_plugin, /* in */ &st_errors);

	/* Update state variables */
	(void) Rte_Write_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(elock_setpoint_plant_mode);  /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Write_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(lock_led2bepr_plant_mode); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Write_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(recharge_hmi_state_plant_mode); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* OBCP-24760 */
	/* The DTC shall be healed if VCU_PosShuntJDD_CANSignal == 'Client' (0x01) */
	if ((Cx1_Client == variables.vcu_pos_shunt_jdd)
		&& (FALSE != st_dtc_errors))
	{
		PLT_Heal_DTCS();
		st_dtc_errors = FALSE;
	}

	/* OBCP-30093 */
	/* The value of TestUTPlugin shall be save in NVM during shudown phase. */
	Rte_Pim_PimPlantModeTestUTPlugin()[0] = (Rte_DT_IdtPlantModeTestUTPlugin_0)st_test_utp_plugin;
	Rte_Pim_PimPlantModeTestUTPlugin()[1] = (Rte_DT_IdtPlantModeTestUTPlugin_0)PLT_Afts_bAcvBEPRTestMode;
	WUM_GetResetRequest(&resetRequest);
	if ((FALSE == variables.shutdown_authorization) && (0x02U != resetRequest))
	{
		(void) Rte_Call_PS_CpApPLT_PLTNvMNeed_SetRamBlockStatus(TRUE);
	}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_02, uint8 *Out_MODE_TEST, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPLT_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_RequestResults (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret = RTE_E_OK;

	if((NULL_PTR!=Out_RSR_02) && (NULL_PTR!=Out_MODE_TEST) && (NULL_PTR!=ErrorCode))
	{

		  *Out_RSR_02 = 0x02U;

		  if (FALSE != PLT_Afts_bAcvBEPRTestMode)
		  {
			  *Out_MODE_TEST = 0x01U;
		  }
		  else
		  {
			  *Out_MODE_TEST = 0x00U;
		  }

		  *ErrorCode = DCM_E_POSITIVERESPONSE;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}

		ret = RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK;
	}

    return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, uint8 *Out_RSR_02, uint8 *Out_MODE_TEST, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_DCM_E_PENDING
 *   RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPLT_CODE) RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start(uint8 In_CHOIX_MODE_TEST, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_RSR_02, P2VAR(uint8, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) Out_MODE_TEST, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPLT_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_Start (returns application error)
 *********************************************************************************************************************/
	Dcm_NegativeResponseCodeType conditions_error;
	Std_ReturnType ret = RTE_E_OK;

	/* OBCP-30070,  */
	if((NULL_PTR!=Out_RSR_02) && (NULL_PTR!=Out_MODE_TEST) && (NULL_PTR!=ErrorCode))
	{
		  conditions_error = PLT_Routine_Control_Conditions_Check();
		  if (DCM_E_POSITIVERESPONSE == conditions_error)
		  {
			  /* Start: When a RoutineControl - Start routine (ID: 0xDD43) is received with byte 5 == '0x01' */
			  if (0x01U == In_CHOIX_MODE_TEST)
			  {
				  PLT_Afts_bAcvBEPRTestMode = TRUE;
			  }
			  /* Stop:  When a RoutineControl - Start routine (ID: 0xDD43) is received with byte 5 == '0x00' */
			  if (0x00U == In_CHOIX_MODE_TEST)
			  {
				  PLT_Afts_bAcvBEPRTestMode = FALSE;
			  }
		  }
		  else
		  {
			  ret = RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK;
		  }

		  *Out_RSR_02 = 0x02U;

		  if (FALSE != PLT_Afts_bAcvBEPRTestMode)
		  {
			  *Out_MODE_TEST = 0x01U;
		  }
		  else
		  {
			  *Out_MODE_TEST = 0x00U;
		  }

		  *ErrorCode = conditions_error;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}

		ret = RTE_E_RoutineServices_Operation_of_BEPR_test_mode_change_for_assembly_plant_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPLT_STOP_SEC_CODE
#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/

/*! \brief Check if input is whithin range.
 *  \param[in] input: Input to check.
 *  \param[in] upper_trheshold: Maximun value allowed.
 *  \param[in] lower_treshold: Minimun value allowed.
 *  \return TRUE if value in range, FALSE otherwise.
 */
static boolean PLT_Check_Range(uint16 input, uint16 upper_trheshold, uint16 lower_treshold)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean retVal;


	if ((upper_trheshold < input) || (lower_treshold > input))
	{
		retVal=FALSE;
	}
	else
	{
		retVal=TRUE;
	}

	return retVal;
}


/*! \brief OBCP-24740: Check Phase Voltages within limits
 *  \param[in] plt_calibratables_t: Calibratables with limits and inhibit.
 *  \param[in] vph12_peak_v: phase 12 input.
 *  \param[in] vph23_peak_v: phase 23 input.
 *  \param[in] vph31_peak_v: phase 31 input.
 *  \param[in] reset: reset detection process (debounce counter).
 *  \return TRUE if error (value not in range), FALSE if value in range or inhibited.
 */
static boolean PLT_Check_AC_Phase_Out_Of_Range(const plt_calibratables_t* p_calibratables,
										PFC_VPH12_Peak_V vph12_peak_v,
										PFC_VPH23_Peak_V vph23_peak_v,
										PFC_VPH31_Peak_V vph31_peak_v,
										boolean reset
	   )
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static uint16 st_debounce_counter = 0;
	static boolean error = TRUE;

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	boolean condition_ok;
	uint16 v_phase[3];
	uint8 index;

	if (FALSE != p_calibratables->inhibit_phase_voltage_detection)
	{
		/* Inhibited */
		error = FALSE;
		st_debounce_counter = 0;
	}
	else
	{
		/* No inhibited */

		if (FALSE != reset)
		{
			/* Reset (done at plant mode in progress start) */
			error = TRUE;
			st_debounce_counter = 0;
		}
		else
		{
			/* if error check condition for healing */
			if (FALSE != error)
			{
				v_phase[0] = vph12_peak_v;
				v_phase[1] = vph23_peak_v;
				v_phase[2] = vph31_peak_v;

				condition_ok = TRUE;
				for(index=0U; index < 3U; index++)
				{
					/*Convert phase to line voltage.*/
					v_phase[index] = (uint16)(((uint32)v_phase[index]*(uint32)PLT_SQRT3_GAIN) >>PLT_SQRT3_Q);
					if (FALSE == PLT_Check_Range(v_phase[index],
		                                         p_calibratables->max_phase_plant_mode,
							                     p_calibratables->min_phase_plant_mode)
							                    )
					{
						condition_ok = FALSE;
					}
				}


				if (FALSE != condition_ok)
				{
					if (st_debounce_counter < p_calibratables->debounce_phase_plant_mode)
					{
						st_debounce_counter++;
					}
					else
					{
						error = FALSE;
					}
				}
				else
				{
					st_debounce_counter = 0;
				}
			}
		}
	}

	return error;

}

/*! \brief OBCP-24739: Check Proximity Voltages within limits
 *  \param[in] plt_calibratables_t: Calibratables with limits and inhibit.
 *  \param[in] proximity_detect_physical_value: proximity input value
 *  \param[in] reset: reset detection process (debounce counter).
 *  \return TRUE if error (value not in range), FALSE if value in range or inhibited.
 */
static boolean PLT_Check_Proxi_Voltage_Out_Of_Range(const plt_calibratables_t* p_calibratables,
											 IdtProximityDetectPhysicalValue proximity_detect_physical_value,
											 boolean reset
	   	   	   	   	   	   	   	   	   	   	)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static uint16 st_debounce_counter = 0;
	static boolean error = TRUE;

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	boolean condition_ok;

	if (FALSE != p_calibratables->inhibit_proximity_detection)
	{
		/* Inhibited */
		error = FALSE;
		st_debounce_counter = 0;
	}
	else
	{
		/* No inhibited */

		if (FALSE != reset)
		{
			/* Reset (done at plant mode in progress start) */
			error = TRUE;
			st_debounce_counter = 0;
		}
		else
		{
			/* if error check if condition for healing */
			if (FALSE != error)
			{
				condition_ok = PLT_Check_Range(proximity_detect_physical_value,
	   	   	   	   	                           p_calibratables->max_proximity_detect_plant_mode,
						                       p_calibratables->min_proximity_detect_plant_mode);

				if (FALSE != condition_ok)
				{
					if (st_debounce_counter < p_calibratables->debounce_proximity_plant_mode)
					{
						st_debounce_counter++;
					}
					else
					{
						error = FALSE;
					}
				}
				else
				{
					st_debounce_counter = 0;
				}
			}
		}
	}

	return error;

}

/*! \brief OBCP-24751: Check Control pilot duty within limits during debounce time
 *  \param[in] plt_calibratables_t: Calibratables with limits and inhibit.
 *  \param[in] duty_control_pilot: input value
 *  \param[in] reset: reset detection process (debounce counter).
 *  \return TRUE if error (value not in range), FALSE if value in range or inhibited.
 */
static boolean PLT_Check_Pilot_Duty_Out_Of_Range(const plt_calibratables_t* p_calibratables,
										  IdtDutyControlPilot duty_control_pilot,
 										  boolean reset
	   	   	   	   	   	   	   	   	   	  )
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static uint16 st_debounce_counter = 0;
	static boolean error = TRUE;

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	boolean condition_ok;

	if (FALSE != p_calibratables->inhibit_control_pilot_detection)
	{
		/* Inhibited */
		error = FALSE;
		st_debounce_counter = 0;
	}
	else
	{
		/* No inhibited */

		if (FALSE != reset)
		{
			/* Reset (done at plant mode in progress start) */
			error = TRUE;
			st_debounce_counter = 0;
		}
		else
		{
			/* if error check if condition for healing */
			if (FALSE != error)
			{
				condition_ok = PLT_Check_Range(duty_control_pilot,
	   	   	   	   	                           p_calibratables->max_control_pilot_duty_plant_mode,
						                       p_calibratables->min_control_pilot_duty_plant_mode);

				if (FALSE != condition_ok)
				{
					if (st_debounce_counter < p_calibratables->debounce_control_pilot_plant_mode)
					{
						st_debounce_counter++;
					}
					else
					{
						error = FALSE;
					}
				}
				else
				{
					st_debounce_counter = 0;
				}
			}
		}
	}

	return error;
}

/*! \brief OBCP-24752: Check DC relay voltage within limits
 *  \param[in] plt_calibratables_t: Calibratables with limits and inhibit.
 *  \param[in] dc_relay_voltage: input value
 *  \param[in] reset: reset detection process (debounce counter).
 *  \return TRUE if error (value not in range), FALSE if value in range or inhibited.
 */
static boolean PLT_Check_DC_Lines_Out_Of_Range(const plt_calibratables_t* p_calibratables,
										BMS_DCRelayVoltage dc_relay_voltage,
  									    boolean reset
	   	   	   	   	   	   	   	   	   	)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static uint16 st_debounce_counter = 0;
	static boolean error = TRUE;

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	boolean condition_ok;

	if (FALSE != p_calibratables->inhibit_DC_relay_voltage)
	{
		/* Inhibited */
		error = FALSE;
		st_debounce_counter = 0;
	}
	else
	{
		/* No inhibited */

		if (FALSE != reset)
		{
			/* Reset (done at plant mode in progress start) */
			error = TRUE;
			st_debounce_counter = 0;
		}
		else
		{
			/* if error check if condition for healing */
			if (FALSE != error)
			{
				condition_ok = PLT_Check_Range(dc_relay_voltage,
	   	   	   	   	                           p_calibratables->max_DC_relay_voltage_plant_mode,
						                       p_calibratables->min_DC_relay_voltage_plant_mode);

				if (FALSE != condition_ok)
				{
					if (st_debounce_counter < p_calibratables->debounce_DC_relay_voltage_plant_mode)
					{
						st_debounce_counter++;
					}
					else
					{
						error = FALSE;
					}
				}
				else
				{
					st_debounce_counter = 0;
				}
			}
		}
	}

	return error;

}



/*! \brief OBCP-24740, OBCP-24739, OBCP-24751, OBCP-24752: Check all signal within limits
 *  \param[in] p_variables: Struct with all inputs.
 *  \param[in] p_calibratables: Struct calibratables with limits and inhibit.
 *  \param[out] p_errors: Struct for returning the errors
 *  \param[in] reset: reset all errors and reinit debounce conters.
 *  \return TRUE if any error, FALSE if no errors.
 */
static boolean PLT_Check_Errors(const plt_variables_t* p_variables,
						        const plt_calibratables_t* p_calibratables,
  						        plt_errors_t* p_errors,
 					            boolean reset
						       )
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	boolean error = FALSE;

	/* OBCP-24740 */
	p_errors->ac_phase_out_of_range  = PLT_Check_AC_Phase_Out_Of_Range( /* in */ p_calibratables,
																	    p_variables->vph12_peak_v,
																	    p_variables->vph23_peak_v,
																	    p_variables->vph31_peak_v,
																		reset
																	   );
	/* OBCP-24739 */
	p_errors->proxi_voltage_out_of_range = PLT_Check_Proxi_Voltage_Out_Of_Range( /* in */ p_calibratables,
																				 p_variables->proximity_detect_physical_value,
																				 reset
											   									);

    /* OBCP-24751 */
	p_errors->pilot_duty_out_of_range = PLT_Check_Pilot_Duty_Out_Of_Range( /* in */ p_calibratables,
																		   p_variables->duty_control_pilot,
																		   reset
											   							 );
	/* OBCP-24752 */
	p_errors->dc_lines_out_of_range = PLT_Check_DC_Lines_Out_Of_Range( /* in */ p_calibratables,
																       p_variables->dc_relay_voltage,
																	   reset
																     );

	/* For testing */
	if (FALSE != p_errors->pilot_duty_out_of_range)
	{
		*(Rte_Pim_PimPlantModeControlPilot()) = PLT_TEST_NOK;
		error = TRUE;
	}
	else
	{
		*(Rte_Pim_PimPlantModeControlPilot()) = PLT_TEST_OK;
	}

	if (FALSE != p_errors->dc_lines_out_of_range)
	{
		*(Rte_Pim_PimPlantModeDCRelayVoltage()) = PLT_TEST_NOK;
		error = TRUE;
	}
	else
	{
		*(Rte_Pim_PimPlantModeDCRelayVoltage()) = PLT_TEST_OK;
	}

	if (FALSE != p_errors->ac_phase_out_of_range)
	{
		*(Rte_Pim_PimPlantModePhaseVoltage()) = PLT_TEST_NOK;
		error = TRUE;
	}
	else
	{
		*(Rte_Pim_PimPlantModePhaseVoltage()) = PLT_TEST_OK;
	}

	if (FALSE != p_errors->proxi_voltage_out_of_range)
	{
		*(Rte_Pim_PimPlantModeProximity()) = PLT_TEST_NOK;
		error = TRUE;
	}
	else
	{
		*(Rte_Pim_PimPlantModeProximity()) = PLT_TEST_OK;
	}


	return error;
}

/*! \brief OBCP-30086, OBCP-30085, OBCP-24759, OBCP-24758, OBCP-24757: Set HMI state variables
 *  \param[in] hmi_state: hmi state.
 *  \param[in] eps_mode: VCU_MODE_EPS_REQUEST_CANSignal.
 */
static void PLT_Process_HMI_State(IdtRECHARGE_HMI_STATE hmi_state, uint8 eps_mode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean hmi_output[PLT_HMISTATE_NUMBER]={FALSE};

	/* OBCP-30086, OBCP-30085, OBCP-24759, OBCP-24758 */
	if((PLT_HMISTATE_DISCONNECTED != hmi_state)
			||((uint8)Cx1_Active_Drive != eps_mode))
	{
		hmi_output[hmi_state] = TRUE;
	}

	(void)Rte_Write_PpLedModes_PlantMode_DeEndOfCharge(hmi_output[PLT_HMISTATE_FINISHED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_PlantMode_DeProgrammingCharge(hmi_output[PLT_HMISTATE_STOPPED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_PlantMode_DeChargeError(hmi_output[PLT_HMISTATE_FAILURE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_PlantMode_DeChargeInProgress(hmi_output[PLT_HMISTATE_INPROGRESS]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_PlantMode_DeGuideManagement(hmi_output[PLT_HMISTATE_DISCONNECTED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

/*! \brief Read all calibratable variables
 *  \param[out] p_calibratables: struct for returning read values.
 */
static void PLT_Read_Calibratables(plt_calibratables_t* p_calibratables)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	p_calibratables->max_proximity_detect_plant_mode = Rte_CData_CalMaxProximityDetectPlantMode();
	p_calibratables->min_proximity_detect_plant_mode = Rte_CData_CalMinProximityDetectPlantMode();
	p_calibratables->debounce_control_pilot_plant_mode = Rte_CData_CalDebounceControlPilotPlantMode();
	p_calibratables->debounce_DC_relay_voltage_plant_mode = Rte_CData_CalDebounceDCRelayVoltagePlantMode();
	p_calibratables->debounce_phase_plant_mode = Rte_CData_CalDebouncePhasePlantMode();
	p_calibratables->debounce_proximity_plant_mode = Rte_CData_CalDebounceProximityPlantMode();
	p_calibratables->max_control_pilot_duty_plant_mode = Rte_CData_CalMaxControlPilotDutyPlantMode();
	p_calibratables->max_DC_relay_voltage_plant_mode = Rte_CData_CalMaxDCRelayVoltagePlantMode();
	p_calibratables->max_phase_plant_mode = Rte_CData_CalMaxPhasePlantMode();
	p_calibratables->max_time_plant_mode = Rte_CData_CalMaxTimePlantMode();
	p_calibratables->min_control_pilot_duty_plant_mode = Rte_CData_CalMinControlPilotDutyPlantMode();
	p_calibratables->min_DC_relay_voltage_plant_mode = Rte_CData_CalMinDCRelayVoltagePlantMode();
	p_calibratables->min_phase_plant_mode = Rte_CData_CalMinPhasePlantMode();
	p_calibratables->nv_m_need_default_value = Rte_CData_PLTNvMNeed_DefaultValue();
	p_calibratables->inhibit_control_pilot_detection = Rte_CData_CalInhibitControlPilotDetection();
	p_calibratables->inhibit_DC_relay_voltage = Rte_CData_CalInhibitDCRelayVoltage();
	p_calibratables->inhibit_phase_voltage_detection = Rte_CData_CalInhibitPhaseVoltageDetection();
	p_calibratables->inhibit_proximity_detection = Rte_CData_CalInhibitProximityDetection();
	p_calibratables->proximity_voltage_starting_plant_mode = Rte_CData_CalProximityVoltageStartingPlantMode();
	p_calibratables->global_timeout_plant_mode = Rte_CData_CalGlobalTimeoutPlantMode();
}

/*! \brief Read all but condition RTE variables used by the module
 *  \param[out] p_variables: struct for returning read values.
 */
static void PLT_Read_Variables(plt_variables_t* p_variables)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	(void) Rte_Read_PpDutyControlPilot_DeDutyControlPilot(&p_variables->duty_control_pilot);  /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(&p_variables->dc_relay_voltage); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&p_variables->obc_charging_connection_confirmati);  /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(&p_variables->vph12_peak_v); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(&p_variables->vph23_peak_v); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(&p_variables->vph31_peak_v); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&p_variables->vcu_mode_eps_request); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(&p_variables->vcu_pos_shunt_jdd); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpProximityDetectPhysicalValue_DeProximityDetectPhysicalValue(&p_variables->proximity_detect_physical_value); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&p_variables->shutdown_authorization); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}


/*! \brief Read Routine_Control condition RTE variables used by the module
 *  \param[out] p_condition_variables: struct for returning read values.
 */
static void PLT_Read_Condition_Variables(condition_variables_t* p_condition_variables)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	(void) Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&p_condition_variables->etat_gmp_hyb); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpBatteryVolt_DeBatteryVolt(&p_condition_variables->battery_volt); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&p_condition_variables->abs_veh_spd); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	p_condition_variables->battery_volt_max_test = Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
	p_condition_variables->battery_volt_min_test = Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
	p_condition_variables->veh_stop_max_test = Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();
}

/*! \brief Check Routine_Control conditions
 *  \return DCM_E_POSITIVERESPONSE if conditions met
 *  or any error DCM_E_VOLTAGETOOLOW, DCM_E_VOLTAGETOOHIGH, DCM_E_VEHICLESPEEDTOOHIGH, DCM_E_ENGINEISRUNNING
 */
static Dcm_NegativeResponseCodeType PLT_Routine_Control_Conditions_Check(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	Dcm_NegativeResponseCodeType error = DCM_E_POSITIVERESPONSE;

	/* The priority is set as: the lowest code will be the one with the highest priority,
	 * so for that reason is the last to be evaluated
	 */
	condition_variables_t condition_variables;

	PLT_Read_Condition_Variables(/* Out */ &condition_variables);

	if (condition_variables.battery_volt_min_test > condition_variables.battery_volt)
	{
		error = DCM_E_VOLTAGETOOLOW;
	}
	if (condition_variables.battery_volt_max_test < condition_variables.battery_volt)
	{
		error = DCM_E_VOLTAGETOOHIGH;
	}
	if (condition_variables.abs_veh_spd > ((uint32)condition_variables.veh_stop_max_test * (uint32)PLT_VEHSPD_FACTOR))
	{
		error = DCM_E_VEHICLESPEEDTOOHIGH;
	}
	if (Cx0_PWT_inactive != condition_variables.etat_gmp_hyb)
	{
		error = DCM_E_ENGINEISRUNNING;
	}

	return error;
}


/*! \brief Update internal state, keep track of time_out
 *  \param[in] p_variables: Struct with all inputs.
 *  \param[in] p_calibratables: Struct calibratables with limits and inhibit.
 *  \param[in] test_mode: TRUE if plant mode is active
 *  \param[in] test_ut_plugin: test result
 *  \param[out] p_state_change: return TRUE if state has changed
 *
 *  \return internal_state:
 * PLT_INTERNAL_STATE_INACTIVE =>  PlantModeState == FALSE
 * PLT_INTERNAL_STATE_WAITING_CONNECTION => (PlantModeState == TRUE and TimeoutPlantMode == Inactive)
 * PLT_INTERNAL_STATE_IN_PROGRESS => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has NOT reached the end
 * PLT_INTERNAL_STATE_COMPLETED => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end
 * PLT_INTERNAL_STATE_FINISHED =>  PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end
 *  and OBC_ChargingConnectionConfirmation_CANSignal switches from 'Full connected' (0x02) to 'Not connected'
 */
static internal_state_t PLT_Update_State(const plt_variables_t* p_variables,
								         const plt_calibratables_t* p_calibratables,
								         boolean test_mode,
								         uint8 test_ut_plugin,
								         boolean* p_state_change
								        )
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static IdtMaxTimePlantMode time_out_plant_mode = PLT_TIME_OUT_INACTIVE;
	static IdtGlobalTimeoutPlantMode time_out_exit_plant_mode = PLT_TIME_OUT_INACTIVE;
	static OBC_ChargingConnectionConfirmati previous_obc_charging_connection_confirmation = Cx0_invalid_value;
	static boolean disconnected = FALSE;
	static internal_state_t previous_internal_state = PLT_INTERNAL_STATE_INACTIVE;
	static boolean plant_mode = FALSE;

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	internal_state_t internal_state = PLT_INTERNAL_STATE_INACTIVE;/* PRQA S 2981 # Variable Initialized for better understanding*/

	/* OBCP-24716, OBCP-24717 */
	if ((Cx0_Park == p_variables->vcu_pos_shunt_jdd)
		&& (FALSE != test_mode)
		&& (PLT_TEST_DONE_WITHOUT_ERROR != test_ut_plugin)
		)
	{
		if (FALSE == plant_mode)
		{
			/* OBCP-24717
			 * If PlantModeState = FALSE and VCU_PosShuntJDD_CANSignal == 'Park' (0x00) and Afts_bAcvBEPRTestMode == TRUE
			 * and TestUTPlugin != 'Done without error' (0x02) shall set PlantModeState = TRUE and TimeoutPlantMode = Inactive
			 */
			plant_mode = TRUE;
			time_out_plant_mode = PLT_TIME_OUT_INACTIVE;
			time_out_exit_plant_mode = PLT_TIME_OUT_INACTIVE;
		}
	}
	else
	{
		/* OBCP-24716
		 * If VCU_PosShuntJDD_CANSignal != 'Park' (0x00) or Afts_bAcvBEPRTestMode == FALSE
		 * or TestUTPlugin == 'Done without error' (0x02) shall set PlantModeState = FALSE
		 */
		plant_mode = FALSE;
	}

	/* Set internal_state */
	/* PLT_INTERNAL_STATE_INACTIVE =>  PlantModeState == FALSE */
	/* PLT_INTERNAL_STATE_WAITING_CONNECTION => (PlantModeState == TRUE and TimeoutPlantMode == Inactive) */
	/* PLT_INTERNAL_STATE_IN_PROGRESS => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has NOT reached the end */
	/* PLT_INTERNAL_STATE_COMPLETED => PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end */
	/* PLT_INTERNAL_STATE_FINISHED =>  PlantModeState == TRUE and TimeoutPlantMode != Inactive amd countdown has reached the end
	 *  and OBC_ChargingConnectionConfirmation_CANSignal switches from 'Full connected' (0x02) to 'Not connected' or global timeout*/
	if (FALSE != plant_mode)
	{
		if (PLT_TIME_OUT_INACTIVE == time_out_plant_mode)
		{

			/* Part of  OBCP-24722 */
			/* ProximityDetectPhysicalValue is below ProximityVoltageStartingPlantMode */
			if (p_variables->proximity_detect_physical_value < p_calibratables->proximity_voltage_starting_plant_mode)
			{
				time_out_plant_mode = p_calibratables->max_time_plant_mode + 1U;
				time_out_exit_plant_mode = p_calibratables->global_timeout_plant_mode + 1U;

				internal_state = PLT_INTERNAL_STATE_IN_PROGRESS;
			}
			else
			{
				internal_state = PLT_INTERNAL_STATE_WAITING_CONNECTION;
			}
		}
		else if (0U != time_out_plant_mode)
		{
			internal_state = PLT_INTERNAL_STATE_IN_PROGRESS;
			disconnected = FALSE;
		}
		else
		{
			if (FALSE != disconnected)
			{
				internal_state = PLT_INTERNAL_STATE_FINISHED;
			}
			else
			{
				if ( ( (Cx2_full_connected == previous_obc_charging_connection_confirmation)
					   && (Cx1_not_connected == p_variables->obc_charging_connection_confirmati)
					 )
					|| (0U == time_out_exit_plant_mode)

					)
				{
					disconnected = TRUE;
					internal_state = PLT_INTERNAL_STATE_FINISHED;
				}
				else
				{
					internal_state = PLT_INTERNAL_STATE_COMPLETED;
				}
			}
		}
	}
	else
	{
		internal_state = PLT_INTERNAL_STATE_INACTIVE;
	}

	/* plant mode completed cuntdown */
	if (PLT_INTERNAL_STATE_IN_PROGRESS == internal_state)
	{
		time_out_plant_mode--;
	}

	/* plant mode finished Countdown */
	if ((PLT_INTERNAL_STATE_IN_PROGRESS == internal_state)
		|| 	(PLT_INTERNAL_STATE_COMPLETED == internal_state))
	{
		time_out_exit_plant_mode--;
	}

	previous_obc_charging_connection_confirmation = p_variables->obc_charging_connection_confirmati;

	/* Notify internal_state changes */
	if (previous_internal_state != internal_state)
	{
		*p_state_change = TRUE;
		previous_internal_state = internal_state;
	}
	else
	{
		*p_state_change = FALSE;
	}

	/* For testing */
	*(Rte_Pim_PimTimeoutPlantMode()) = time_out_plant_mode;
	(void) Rte_Write_PpPlantModeState_DePlantModeState(plant_mode); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return internal_state;
}

/*! \brief Set dtcs
 *  \param[in] p_errors: struct with detected errors
 */
static void PLT_Set_DTCS(const plt_errors_t* p_errors)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	if (FALSE != p_errors->ac_phase_out_of_range)
	{
		/* set the DTC P10C4-13 */
		(void)Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(PLT_DTC_10C413); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	if (FALSE != p_errors->proxi_voltage_out_of_range)
	{
		/* set the DTC P10C5-12 */
		(void)Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(PLT_DTC_10C512); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	}

	if (FALSE != p_errors->pilot_duty_out_of_range)
	{
		/* set the DTC P10C6-13. */
		(void)Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(PLT_DTC_10C613); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	if (FALSE != p_errors->dc_lines_out_of_range)
	{
		/* set the DTC P10C7-13 */
		(void)Rte_Call_PpPlantModeDTC_OpPlantModeDTCSet(PLT_DTC_10C713); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}

/*! \brief Heal dtcs
 */
static void PLT_Heal_DTCS(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPLT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPLT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPLT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPLT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPLT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	(void) Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(PLT_DTC_10C413); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(PLT_DTC_10C512); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(PLT_DTC_10C613); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void) Rte_Call_PpPlantModeDTC_OpPlantModeDTCReset(PLT_DTC_10C713); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/*! \brief Set dtcs
 *  \param[in] test_mode: is TRUE if in plant mode
 *  \param[in] test_utp_plugin: test status
 *  \param[in] errors: struct with detected errors
 */
static void PLT_Update_DID_Variables(boolean test_mode, uint8 test_utp_plugin, const plt_errors_t* errors)
{

	if (FALSE != test_mode)
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(PLT_TEST_MODE); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_State(PLT_CHARGE_MODE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Result(test_utp_plugin);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (FALSE != errors->ac_phase_out_of_range)
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(PLT_TEST_NOK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ACPhase(PLT_TEST_OK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	if (FALSE != errors->pilot_duty_out_of_range)
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(PLT_TEST_NOK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_PilotDuty(PLT_TEST_OK);	/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	if (FALSE != errors->proxi_voltage_out_of_range)
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(PLT_TEST_NOK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_ProxiVoltage(PLT_TEST_OK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	if (FALSE != errors->dc_lines_out_of_range)
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(PLT_TEST_NOK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		(void) Rte_Write_PpPlantModeTestInfoDID_DePlantModeTestInfoDID_Test_DCLines(PLT_TEST_OK);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
