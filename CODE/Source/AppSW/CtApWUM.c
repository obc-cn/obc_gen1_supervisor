/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApWUM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApWUM
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApWUM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup WUM
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Wakeup Module
 * \details This module manages the sleep request coming from the BSI.
 *
 * \{
 * \file  WUM.c
 * \brief  Generic code of the WUM module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * ComM_ModeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Operation Prototypes:
 * =====================
 * ReleaseRUN of Port Interface EcuM_StateRequest
 *   Release of the RUN state.
 *
 * RequestRUN of Port Interface EcuM_StateRequest
 *   Request for the RUN state.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApWUM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "AppExternals.h"
#include "WdgM.h"
#include "Mcu.h"
#include "IfxPort_reg.h"


/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Signal not available value for U8.*/
#define WUM_SNA_U8							0xFFU
/** GMV edge (21%).*/
#define WUM_PERCENTAGE_21				21U
/** MEAP edge (25%).*/
#define WUM_PERCENTAGE_25				25U
/** Time Conversion to 5s.*/
#define WUM_TIME_FACTOR_5S			500U
/** Time Conversion to 1s.*/
#define WUM_TIME_FACTOR_1S			100U
/** Time Conversion to 1min.*/
#define WUM_TIME_FACTOR_1MIN		6000U

#define	WUM_ELECTRON_BYTE_MASK		0xC0U

#define	WUM_EFFAC_DEFAUT_DIAG_BYTE_MASK		0x01U

 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are autogenerated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
enum{
	 WUM_CONDITION_VOLT_DETECTION = 0,
	 WUM_CONDITION_VOLT_DETECTION_FALL,
	 WUM_CONDITION_HMI_WAKEUP,
	 WUM_CONDITION_ESP_CHARGE,
	 WUM_CONDITION_ESP_NDISCHARGE,
	 WUM_CONDITION_ESP_ACTIVE,
	 WUM_CONDITION_CONNECTION,
	 WUM_CONDITION_CONNECTION_FALL,
	 WUM_CONDITION_BUTTON,
	 WUM_CONDITION_BUTTON_2,
	 WUM_CONDITION_HMI_CHANGE,
	 WUM_CONDITION_CHILLER,
	 WUM_CONDITION_GMV,
	 WUM_CONDITION_MEAP,
	 WUM_CONDITION_PUMP,
	 WUM_CONDITION_CONSO_CPT,
	 WUM_CONDITION_CONSO_CPTE2,
	 WUM_CONDITION_CP,
	 WUM_CONDITION_NUMBER
}; /* PRQA S 3205 # Data type not used as is, but its elements are used as constants */

typedef struct
{
	uint32 free_run_counter;
	uint32 timestamp[3];
	uint8 value[3];
	uint8 index;
	boolean ElectronicIntegrationRequest;
}WUM_ElecIntegrMode_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/

/* - Static non-init variables declaration ---------- */
#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static boolean  WUM_Discontactors;
static boolean	WUM_HVbatt;
static boolean	WUM_PIstate;
static boolean	WUM_Cooling;
static WUM_ElecIntegrMode_t WUM_ElecIntegrMode;

#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static boolean WUM_DiagToolsRequest = FALSE;
static uint16 WUM_DiagToolsRequestTimeoutCounter = 0;
static uint8 WUM_resetType = 0;

#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/** Main Mode state.*/
static boolean WUM_GetMainMode(void);
/**
 * \brief Update individual conditions.
 * \details This function updates the individaul conditions and checks if any rising edge is detected.
 * \param[out] ConditionState Current condition state.
 * \param[out] RisingEdge Rising edge detection.
 * */
static void WUM_UpdateWakeUpConditions(boolean ConditionState[WUM_CONDITION_NUMBER],boolean RisingEdge[WUM_CONDITION_NUMBER]);
/** Check individual conditions.*/
static void WUM_IndividualConditions(boolean ConditionState[WUM_CONDITION_NUMBER]);
/** Cooling set condition depending on the input.*/
static boolean WUM_Cooling_SetCondition(const boolean Condition[WUM_CONDITION_NUMBER]);
/** \brief HV battery output state.
 *  \details This function updates the HV battery wake up depending on the set condition
 *  and the clean conditions.
 * */
static void WUM_HVbatt_OutState(boolean HVbatt_SetCondition, boolean ESP_active);
/** \brief PI state output state.
 *  \details This function updates the PI state wake up depending on the set condition
 *  and the clean conditions.
 * */
static void  WUM_PIstate_OutState(boolean PIstate_SetCondition);
/** \brief Discontactors output state.
 *  \details This function updates the Discontactors wake up depending on the set condition
 *  and the clean conditions.
 * */
static void WUM_discontartors_OutState(boolean Discontactors_SetCondition, boolean NdischargeChange);
/** \brief Cooling output state.
 *  \details This function updates the Cooling wake up depending on the set condition
 *  and the clean conditions.
 * */
static void WUM_cooling_OutState(boolean Cooling_SetCondition, boolean Cooling_ConditionState, boolean ESP_active_state);
/** \brief Need OBC output state.
 *  \details This function updates the Need OBC wake up depending on the set condition
 *  and the clean conditions.
 * */
static void WUM_needOBC_OutState(boolean MainMode, boolean HMI_wakeup);

/** Individual function for: VOLT_DETECTION.*/
static boolean WUM_Condition_VOLT_DETECTION(void);
/** Individual function for: VOLT_DETECTION_FALL.*/
static boolean WUM_Condition_VOLT_DETECTION_FALL(void);
/** Individual function for: HMI_WAKEUP.*/
static boolean WUM_Condition_HMI_WAKEUP(void);
/** Individual function for: ESP_CHARGE.*/
static boolean WUM_Condition_ESP_CHARGE(void);
/** Individual function for: ESP_NDISCHARGE.*/
static boolean WUM_Condition_ESP_NDISCHARGE(void);
/** Individual function for: ESP_ACTIVE.*/
static boolean WUM_Condition_ESP_ACTIVE(void);
/** Individual function for: CONNECTION.*/
static boolean WUM_Condition_CONNECTION(void);
/** Individual function for: CONNECTION_FALL.*/
static boolean WUM_Condition_CONNECTION_FALL(void);
/** Individual function for: BUTTON.*/
static boolean WUM_Condition_BUTTON(void);
/** Individual function for: BUTTON_2.*/
static boolean WUM_Condition_BUTTON_2(void);
/** Individual function for: HMI_CHANGE.*/
static boolean WUM_Condition_HMI_CHANGE(void);
/** Individual function for: CHILLER.*/
static boolean WUM_Condition_CHILLER(void);
/** Individual function for: GMV.*/
static boolean WUM_Condition_GMV(void);
/** Individual function for: MEAP.*/
static boolean WUM_Condition_MEAP(void);
/** Individual function for: PUMP.*/
static boolean WUM_Condition_PUMP(void);
/** Individual function for: CONSO_CPT.*/
static boolean WUM_Condition_CONSO_CPT(void);
/** Individual function for: CONSO_CPTE2.*/
static boolean WUM_Condition_CONSO_CPTE2(void);
/** Individual function for: ControlPilot.*/
static boolean WUM_Condition_CP(void);


static void WUM_ElectronicIntegration(void);
static boolean WUM_CheckConditionsEnterIntegrationMode(void);
static boolean WUM_CheckConditionsExitIntegrationMode(void);
static boolean WUM_received3FramesInOneSecondEIM(uint8 * value);
static uint32 WUM_calculateDistance(uint32 data1, uint32 data2);

static void WUM_DiagnosticToolsRequestOperation(void);

static void WUM_GlobalClearFaults(void);
static boolean WUM_received3FramesInOneSecondGCF(uint8 * value);

#define CAL_START_SEC_CONST_DEFAULT_CALIBRATION_ID
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Calibration File Identifier (Default Value) */
const uint16 CAL_CalibrationFileID = 0xAA55; /* PRQA S 3408 # Not used in this file. Just a signature for the calibration mapping version */

#define CAL_STOP_SEC_CONST_DEFAULT_CALIBRATION_ID
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BSI_PostDriveWakeup: Boolean
 * BSI_PreDriveWakeup: Boolean
 * COUPURE_CONSO_CTP: Boolean
 * COUPURE_CONSO_CTPE2: Boolean
 * IdtDebounceHoldNetworkCom: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtDebounceTempoEndCharge: Integer in interval [0...120]
 *   Unit: [s], Factor: 5, Offset: 0
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtMaxTimeDiagTools: Integer in interval [0...1024]
 *   Unit: [ms], Factor: 1, Offset: 0
 * IdtMaxTimeShutdown: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupCoolingMax: Integer in interval [0...240]
 *   Unit: [minutes], Factor: 1, Offset: 0
 * IdtTimeWakeupCoolingMin: Integer in interval [0...63]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupHoldDiscontactorMax: Integer in interval [0...600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupHoldDiscontactorMin: Integer in interval [0...120]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeWakeupPiStateInfo: Integer in interval [0...120]
 *   Unit: [s], Factor: 5, Offset: 0
 * OBC_PlugVoltDetection: Boolean
 * OBC_PushChargeType: Boolean
 * SUPV_CoolingWupState: Boolean
 * SUPV_HVBattChargeWupState: Boolean
 * SUPV_HoldDiscontactorWupState: Boolean
 * SUPV_PIStateInfoWupState: Boolean
 * SUPV_RCDLineState: Boolean
 * VCU_DDEGMVSEEM: Integer in interval [0...100]
 * VCU_DMDActivChiller: Boolean
 * VCU_DMDMeap2SEEM: Integer in interval [0...100]
 * VCU_DiagMuxOnPwt: Boolean
 * VCU_PrecondElecWakeup: Boolean
 * VCU_StopDelayedHMIWakeup: Boolean
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * sint32: Integer in interval [-2147483648...2147483647] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_VCUHeatPumpWorkingMode: Enumeration of integer in interval [0...31] with enumerators
 *   Cx00_Stop_mode (0U)
 *   Cx01_Failure_mode_1 (1U)
 *   Cx02_Failure_mode_2 (2U)
 *   Cx03_Failure_mode_3 (3U)
 *   Cx04_Failure_mode_4 (4U)
 *   Cx05_Failure_mode_5 (5U)
 *   Cx06_Failure_mode_6 (6U)
 *   Cx07_Failure_mode_7 (7U)
 *   Cx08_AC_mode (8U)
 *   Cx09_HP_mode (9U)
 *   Cx0A_Deshum_total_heating_mode (10U)
 *   Cx0B_Deshum_simple_heating_mode (11U)
 *   Cx0C_De_icing_mode_ (12U)
 *   Cx0D_transition_mode (13U)
 *   Cx0E_Failure_mode (14U)
 *   Cx0F_Chiller_alone_mode (15U)
 *   Cx10_AC_chiffler_mode (16U)
 *   Cx11_Deshum_Chiller_mode (17U)
 *   Cx12_Reserved (18U)
 *   Cx13_Water_Heather_mode (19U)
 *   Cx14_Reserved (20U)
 *   Cx15_Reserved (21U)
 *   Cx16_Reserved (22U)
 *   Cx17_Reserved (23U)
 *   Cx18_Reserved (24U)
 *   Cx19_Reserved (25U)
 *   Cx1A_Reserved (26U)
 *   Cx1B_Reserved (27U)
 *   Cx1C_Reserved (28U)
 *   Cx1D_Reserved (29U)
 *   Cx1E_Reserved (30U)
 *   Cx1F_Reserved (31U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint8 *Rte_Pim_NvWUMBlockNeed_MirrorBlock(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxTimeDiagTools Rte_CData_CalMaxTimeDiagTools(void)
 *   IdtTimeWakeupHoldDiscontactorMax Rte_CData_CalTimeWakeupHoldDiscontactorMax(void)
 *   IdtDebounceHoldNetworkCom Rte_CData_CalDebounceHoldNetworkCom(void)
 *   IdtDebounceTempoEndCharge Rte_CData_CalDebounceTempoEndCharge(void)
 *   IdtMaxTimeShutdown Rte_CData_CalMaxTimeShutdown(void)
 *   IdtTimeWakeupCoolingMax Rte_CData_CalTimeWakeupCoolingMax(void)
 *   IdtTimeWakeupCoolingMin Rte_CData_CalTimeWakeupCoolingMin(void)
 *   IdtTimeWakeupHoldDiscontactorMin Rte_CData_CalTimeWakeupHoldDiscontactorMin(void)
 *   IdtTimeWakeupPiStateInfo Rte_CData_CalTimeWakeupPiStateInfo(void)
 *   uint8 Rte_CData_NvWUMBlockNeed_DefaultValue(void)
 *
 *********************************************************************************************************************/


#define CtApWUM_START_SEC_CODE
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpWUM_DiagToolRequestRcvd> of PortPrototype <PpWUM_DiagToolRequestRcvd>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpWUM_DiagToolRequestRcvd_OpWUM_DiagToolRequestRcvd
 *********************************************************************************************************************/

	WUM_DiagToolsRequest = TRUE;
	WUM_DiagToolsRequestTimeoutCounter = 0U;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApWUM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_init_doc
 *********************************************************************************************************************/

/*!
 * \brief WUM initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtApWUM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/* Initialize Master Partial-WakeUp request signals */
	WUM_Discontactors = FALSE;
	WUM_HVbatt = FALSE;
	WUM_PIstate = FALSE;
	WUM_Cooling = FALSE;


	/* Initialize Comms to No_COM just after a Reset */
	(void)Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(COMM_NO_COMMUNICATION);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(COMM_NO_COMMUNICATION);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it to PCOM SWC */
	/* Initialize Ecu State to RequestRUN just after a Reset */
	//Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN();
/* [Enrique.Bueno] Workaround STOP */

	WUM_ElecIntegrMode.free_run_counter = 0U;
	WUM_ElecIntegrMode.index = 0U;
	WUM_ElecIntegrMode.timestamp[0] = 0xFFFFFFFFU;
	WUM_ElecIntegrMode.timestamp[1] = 0xFFFFFFFFU;
	WUM_ElecIntegrMode.timestamp[2] = 0xFFFFFFFFU;
	WUM_ElecIntegrMode.value[0] = 0;
	WUM_ElecIntegrMode.value[1] = 0;
	WUM_ElecIntegrMode.value[2] = 0;

	/* PRQA S 0314 ++ # Conversion needed to serialize/deserialize data into NvM */
	(void)Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock((void*)(&WUM_ElecIntegrMode.ElectronicIntegrationRequest));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	/* PRQA S 0314 -- */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApWUM_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP *data)
 *   Std_ReturnType Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(boolean data)
 *   Std_ReturnType Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest(boolean data)
 *   Std_ReturnType Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(boolean data)
 *   Std_ReturnType Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(boolean data)
 *   Std_ReturnType Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data)
 *   Std_ReturnType Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data)
 *   Std_ReturnType Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtApWUM_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msA
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean MainMode_Previous = FALSE;
	static boolean WUM_firstTime = FALSE;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean MainMode;


	boolean Discontactors_SetCondition  = FALSE;
	boolean	HVbatt_SetCondition = FALSE;
	boolean	PIstate_SetCondition = FALSE;
	boolean	Cooling_SetCondition = FALSE;

	boolean ConditionState[WUM_CONDITION_NUMBER];
	boolean RisingEdge[WUM_CONDITION_NUMBER];

	boolean Cooling_ConditionState = FALSE;
	boolean Cooling_RisingEdge;


	/*Individual conditions.*/
	WUM_UpdateWakeUpConditions(ConditionState,RisingEdge);

	MainMode = WUM_GetMainMode();

	if(FALSE==WUM_firstTime)
	{
		WUM_Discontactors= FALSE;
		WUM_HVbatt= FALSE;
		WUM_PIstate= FALSE;
		WUM_Cooling = FALSE;
		WUM_firstTime = TRUE;

	}
	else if(FALSE!=MainMode)
	{
		WUM_Discontactors= FALSE;
		WUM_HVbatt= FALSE;
		WUM_PIstate= FALSE;
		WUM_Cooling = FALSE;
	}
	else
	{

		Cooling_ConditionState = WUM_Cooling_SetCondition(ConditionState);
		Cooling_RisingEdge = WUM_Cooling_SetCondition(RisingEdge);

		if(FALSE!=MainMode_Previous)
		{
			Discontactors_SetCondition = TRUE;
			Cooling_SetCondition = Cooling_ConditionState;
		}
		else
		{
			Cooling_SetCondition = Cooling_RisingEdge;
		}

		/* PRQA S 4558,4404 ++ # This platform does not support the native boolean data type and a uint8 type is used instead. GNU C specifies the behaviour of logical operator with non boolean data types */
		PIstate_SetCondition = RisingEdge[WUM_CONDITION_CONNECTION] ||
													RisingEdge[WUM_CONDITION_CONNECTION_FALL] ||
													 RisingEdge[WUM_CONDITION_BUTTON] ||
													 RisingEdge[WUM_CONDITION_BUTTON_2] ||
													 ConditionState[WUM_CONDITION_HMI_CHANGE] ||
													 RisingEdge[WUM_CONDITION_VOLT_DETECTION] ||
													 RisingEdge[WUM_CONDITION_VOLT_DETECTION_FALL];

		HVbatt_SetCondition = RisingEdge[WUM_CONDITION_CP] ||
													RisingEdge[WUM_CONDITION_VOLT_DETECTION] ||
													(ConditionState[WUM_CONDITION_HMI_WAKEUP] && (ConditionState[WUM_CONDITION_CP] || ConditionState[WUM_CONDITION_VOLT_DETECTION])) ||
													ConditionState[WUM_CONDITION_ESP_CHARGE];
		/* PRQA S 4558,4404 -- */
	}

	MainMode_Previous = MainMode;

	WUM_HVbatt_OutState(HVbatt_SetCondition,ConditionState[WUM_CONDITION_ESP_ACTIVE]);
	WUM_PIstate_OutState(PIstate_SetCondition);
	WUM_discontartors_OutState(Discontactors_SetCondition,RisingEdge[WUM_CONDITION_ESP_NDISCHARGE]);
	WUM_cooling_OutState(Cooling_SetCondition, Cooling_ConditionState, ConditionState[WUM_CONDITION_ESP_ACTIVE]);

	WUM_needOBC_OutState(MainMode, ConditionState[WUM_CONDITION_HMI_WAKEUP]);

	WUM_ElectronicIntegration();
	WUM_GlobalClearFaults();

	WUM_DiagnosticToolsRequestOperation();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApWUM_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpCANComRequest_DeCANComRequest(sint32 *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msB_doc
 *********************************************************************************************************************/

 /*
 * Rte signals meaning:
 * ===================
 * Rte_Read_PpCANComRequest_DeCANComRequest:
 *   No_COM (0)
 *   FULL_COM (1)
 *   Electronic_Integration_mode (2)
 *
 * Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization:
 *   Inactive (FALSE)
 *   Active (TRUE)
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtApWUM_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApWUM_task10msB
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static sint32 WUM_can_com_request_prev = 0;
/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it to PCOM SWC */
	//static boolean WUM_shutdown_authorization_prev = FALSE;
/* [Enrique.Bueno] Workaround STOP */
	//static boolean WUM_reset_req_ongoing = FALSE;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean WUM_first_loop = TRUE;

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	sint32 WUM_can_com_request;
/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it to PCOM SWC */
	//boolean WUM_shutdown_authorization;
/* [Enrique.Bueno] Workaround STOP */

/* [Enrique.Bueno] Workaround START: Key on-of reset actions disabled in order to avoid SW stacks after
                                     sending twice consecutive requests */
	//if((WUM_resetType != 0x00) && (WUM_reset_req_ongoing == FALSE))
/* [Enrique.Bueno] Workaround STOP */




	/* Manage Communications requests */
	(void)Rte_Read_PpCANComRequest_DeCANComRequest(&WUM_can_com_request);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
/* [Enrique.Bueno] Workaround START: WUM_can_com_request forced to FULL_COM always */
		//WUM_can_com_request = 1;
/* [Enrique.Bueno] Workaround STOP */

		if (FALSE != WUM_first_loop)
		{
			/* Activate internal CAN communications in the first operating cycle. */
			(void)Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(COMM_FULL_COMMUNICATION);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			WUM_first_loop = FALSE;
		}

/* [Enrique.Bueno] Workaround START: Key on-off reset actions disabled in order to avoid SW stacks after
                                     sending twice consecutive requests */
		//if((WUM_can_com_request != WUM_can_com_request_prev) && (WUM_reset_req_ongoing == FALSE))
/* [Enrique.Bueno] Workaround STOP */
		if(WUM_can_com_request != WUM_can_com_request_prev)
		{
			if(WUM_can_com_request != 0)
			{
				(void)Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(COMM_FULL_COMMUNICATION);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			}
			else
			{
				(void)Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(COMM_NO_COMMUNICATION);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			}
		}
	WUM_can_com_request_prev = WUM_can_com_request;


/* [Enrique.Bueno] Workaround STOP */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpWUM_ElectronBSI_Frame_Received> of PortPrototype <PpWUM_ElectronBSI_Frame_Received>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApWUM_CODE) RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received
 *********************************************************************************************************************/

	WUM_ElecIntegrMode.timestamp[WUM_ElecIntegrMode.index] = WUM_ElecIntegrMode.free_run_counter;
	WUM_ElecIntegrMode.value[WUM_ElecIntegrMode.index] = frameData;

	WUM_ElecIntegrMode.index++;
	if (WUM_ElecIntegrMode.index >= 3U)
	{
		WUM_ElecIntegrMode.index = 0;
	}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApWUM_STOP_SEC_CODE
#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static boolean WUM_GetMainMode(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean MainMode;
	IdtAppRCDECUState ECU_state;


	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&ECU_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((APP_STATE_5==ECU_state)||(APP_STATE_4==ECU_state))
	{
		MainMode = TRUE;
	}
	else
	{
		MainMode = FALSE;
	}
	return MainMode;
}


static void WUM_UpdateWakeUpConditions(boolean ConditionState[WUM_CONDITION_NUMBER],boolean RisingEdge[WUM_CONDITION_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean ConditionState_Previous[WUM_CONDITION_NUMBER] = {WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8,WUM_SNA_U8};

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 index;


	if((NULL_PTR!=ConditionState)&&(NULL_PTR!=RisingEdge))/*PRQA S 2991, 2995 #Tessy allows null pointers.*/
	{

		WUM_IndividualConditions(ConditionState);

		for(index=0U;index<(uint8)WUM_CONDITION_NUMBER;index++)
		{

			if((WUM_SNA_U8==ConditionState[index])||(WUM_SNA_U8==ConditionState_Previous[index]))
			{
				RisingEdge[index] = FALSE;
			}
			else if((FALSE==ConditionState_Previous[index])&&(FALSE!=ConditionState[index]))
			{
				RisingEdge[index] = TRUE;
			}
			else
			{
				RisingEdge[index] = FALSE;
			}
			ConditionState_Previous[index]=ConditionState[index];
		}
	}
}
static void WUM_IndividualConditions(boolean ConditionState[WUM_CONDITION_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	ConditionState[WUM_CONDITION_VOLT_DETECTION] = WUM_Condition_VOLT_DETECTION();
	ConditionState[WUM_CONDITION_VOLT_DETECTION_FALL] = WUM_Condition_VOLT_DETECTION_FALL();
	ConditionState[WUM_CONDITION_HMI_WAKEUP] = WUM_Condition_HMI_WAKEUP();
	ConditionState[WUM_CONDITION_ESP_CHARGE] = WUM_Condition_ESP_CHARGE();
	ConditionState[WUM_CONDITION_ESP_NDISCHARGE] = WUM_Condition_ESP_NDISCHARGE();
	ConditionState[WUM_CONDITION_ESP_ACTIVE] = WUM_Condition_ESP_ACTIVE();
	ConditionState[WUM_CONDITION_CONNECTION] = WUM_Condition_CONNECTION();
	ConditionState[WUM_CONDITION_CONNECTION_FALL] = WUM_Condition_CONNECTION_FALL();
	ConditionState[WUM_CONDITION_BUTTON] = WUM_Condition_BUTTON();
	ConditionState[WUM_CONDITION_BUTTON_2] = WUM_Condition_BUTTON_2();
	ConditionState[WUM_CONDITION_HMI_CHANGE] = WUM_Condition_HMI_CHANGE();
	ConditionState[WUM_CONDITION_CHILLER] = WUM_Condition_CHILLER();
	ConditionState[WUM_CONDITION_GMV] = WUM_Condition_GMV();
	ConditionState[WUM_CONDITION_MEAP] = WUM_Condition_MEAP();
	ConditionState[WUM_CONDITION_PUMP] = WUM_Condition_PUMP();
	ConditionState[WUM_CONDITION_CONSO_CPT] = WUM_Condition_CONSO_CPT();
	ConditionState[WUM_CONDITION_CONSO_CPTE2] = WUM_Condition_CONSO_CPTE2();
	ConditionState[WUM_CONDITION_CP] = WUM_Condition_CP();
}
static boolean WUM_Cooling_SetCondition(const boolean Condition[WUM_CONDITION_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	if(NULL_PTR != Condition)/*PRQA S 2991, 2995 #Tessy allows null pointers.*/
	{
		/* PRQA S 4558,4404 ++ # This platform does not support the native boolean data type and a uint8 type is used instead. GNU C specifies the behaviour of logical operator with non boolean data types */
		RetVal = Condition[WUM_CONDITION_CHILLER] ||
						 Condition[WUM_CONDITION_GMV] ||
				     Condition[WUM_CONDITION_MEAP] ||
						 Condition[WUM_CONDITION_PUMP] ||
						 Condition[WUM_CONDITION_CONSO_CPT] ||
						 Condition[WUM_CONDITION_CONSO_CPTE2];
		/* PRQA S 4558,4404 -- */
	}
	else
	{
		RetVal = FALSE;/*PRQA S 2880 #Tessy allows null pointers.*/
	}

	return RetVal;
}
static void WUM_HVbatt_OutState(boolean HVbatt_SetCondition, boolean ESP_active)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 DebounceCounter = 0U;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	/* PRQA S 4558,4404 ++ # This platform does not support the native boolean data type and a uint8 type is used instead. GNU C specifies the behaviour of logical operator with non boolean data types */
	WUM_HVbatt = WUM_HVbatt || HVbatt_SetCondition;
	/* PRQA S 4558,4404 -- */

	if(FALSE!=ESP_active)
	{
		DebounceCounter = 0U;
	}
	else if (DebounceCounter<((uint32)Rte_CData_CalDebounceTempoEndCharge()*(uint32)WUM_TIME_FACTOR_5S))
	{
		DebounceCounter++;
	}
	else if(FALSE==HVbatt_SetCondition)
	{
		WUM_HVbatt = FALSE;
	}
	else
	{
	    /*Misra*/
	}

	(void)Rte_Write_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP(WUM_HVbatt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(WUM_HVbatt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void  WUM_PIstate_OutState(boolean PIstate_SetCondition)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 TimeOut = 0U;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	/* PRQA S 4558,4404 ++ # This platform does not support the native boolean data type and a uint8 type is used instead. GNU C specifies the behaviour of logical operator with non boolean data types */
	WUM_PIstate = WUM_PIstate || PIstate_SetCondition; 
	/* PRQA S 4558,4404 --*/

	if(FALSE!=PIstate_SetCondition)
	{
		TimeOut = (uint32)Rte_CData_CalTimeWakeupPiStateInfo()*(uint32)WUM_TIME_FACTOR_5S;
	}

	if(0U==TimeOut)
	{
		WUM_PIstate = FALSE;
	}
	else
	{
		TimeOut--;
	}

	(void)Rte_Write_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP(WUM_PIstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(WUM_PIstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void WUM_discontartors_OutState(boolean Discontactors_SetCondition, boolean NdischargeChange)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 TimeMax=0U, TimeMin=0U;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/* PRQA S 4558,4404 ++ # This platform does not support the native boolean data type and a uint8 type is used instead. GNU C specifies the behaviour of logical operator with non boolean data types */
	WUM_Discontactors = WUM_Discontactors || Discontactors_SetCondition;
	/* PRQA S 4558,4404 --*/

	/*Maxtime*/
	if(FALSE==WUM_Discontactors)
	{
		TimeMax = 0U;
	}
	else if(((uint32)Rte_CData_CalTimeWakeupHoldDiscontactorMax()*(uint32)WUM_TIME_FACTOR_1S)>TimeMax)
	{
		TimeMax ++;
	}
	else
	{
		WUM_Discontactors = FALSE;
		TimeMax = 0U;
	}

	/*Min time*/
	if(FALSE==WUM_Discontactors)
	{
		TimeMin = 0U;
	}
	else if(((uint32)Rte_CData_CalTimeWakeupHoldDiscontactorMin()*(uint32)WUM_TIME_FACTOR_1S)>TimeMin)
	{
		TimeMin ++;
	}
	else if(FALSE!=NdischargeChange)
	{
		WUM_Discontactors = FALSE;
		TimeMin = 0U;
	}
	else
	{
		/*Misra*/
	}

	(void)Rte_Write_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(WUM_Discontactors);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(WUM_Discontactors);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void WUM_cooling_OutState(boolean Cooling_SetCondition, boolean Cooling_ConditionState, boolean ESP_active_state)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 TimeMax=0U, TimeMin=0U;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	WUM_Cooling |= Cooling_SetCondition;


	if(FALSE==WUM_Cooling)
	{
		TimeMax = 0U;
	}
	else if(FALSE!=ESP_active_state)
	{
		TimeMax = 0U;
	}
	else if(((uint32)Rte_CData_CalTimeWakeupCoolingMax()*(uint32)WUM_TIME_FACTOR_1MIN)>TimeMax)
	{
		TimeMax ++;
	}
	else
	{
		WUM_Cooling = FALSE;
		TimeMax = 0U;
	}


	if((FALSE!=Cooling_ConditionState)||(FALSE==WUM_Cooling))
	{
		TimeMin = 0U;
	}
	else if(((uint32)Rte_CData_CalTimeWakeupCoolingMin()*(uint32)WUM_TIME_FACTOR_1S)>TimeMin)
	{
		TimeMin ++;
	}
	else
	{
		WUM_Cooling = FALSE;
		TimeMin = 0U;
	}

	(void)Rte_Write_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(WUM_Cooling);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(WUM_Cooling);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void WUM_needOBC_OutState(boolean MainMode, boolean HMI_wakeup)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 DebouncCounter = 0U;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean needOBC ;
	boolean SetCondition;
	boolean RCD_line;
	boolean BSI_PreDriveWakeup_var;
	boolean BSI_PostDriveWakeup_var;
	boolean PrecondElecWakeup;


	(void)Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&RCD_line); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(&BSI_PreDriveWakeup_var); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(&BSI_PostDriveWakeup_var); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(&PrecondElecWakeup); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	SetCondition = MainMode | HMI_wakeup | WUM_HVbatt | WUM_PIstate | WUM_Discontactors | WUM_Cooling |
			     RCD_line | BSI_PreDriveWakeup_var | BSI_PostDriveWakeup_var | PrecondElecWakeup;

	if(FALSE!=SetCondition)
	{
		needOBC = TRUE;
		DebouncCounter = (uint32)Rte_CData_CalDebounceHoldNetworkCom() * (uint32)WUM_TIME_FACTOR_1S;
	}
	else if(DebouncCounter>0U)
	{
		needOBC = TRUE;
		DebouncCounter --;
	}
	else
	{
		needOBC = FALSE;
	}

	(void)Rte_Write_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(needOBC);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}
static boolean WUM_Condition_VOLT_DETECTION(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	(void)Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(&RetVal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(FALSE!=RetVal)
	{
		RetVal = TRUE;
	}
	return RetVal;
}
static boolean WUM_Condition_VOLT_DETECTION_FALL(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	(void)Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(&RetVal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Invert logic to detect a falling edge */
	/* PRQA S 4558,4404 ++ # This platform does not support the native boolean data type and a uint8 type is used instead. GNU C specifies the behaviour of logical operator with non boolean data types */
	RetVal = !RetVal;
	/* PRQA S 4558,4404 --*/

	return RetVal;
}
static boolean WUM_Condition_HMI_WAKEUP(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	(void)Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(&RetVal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(TRUE!=RetVal)
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_ESP_CHARGE(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	VCU_ModeEPSRequest EPSrequest;


	(void)Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&EPSrequest); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx3_PI_Charge==EPSrequest)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_ESP_NDISCHARGE(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	VCU_ModeEPSRequest EPSrequest;


	(void)Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&EPSrequest); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx2_Discharge !=EPSrequest)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_ESP_ACTIVE(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	VCU_ModeEPSRequest EPSrequest;


	(void)Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&EPSrequest); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx3_PI_Charge == EPSrequest) || (Cx4_PI_Balance == EPSrequest) || (Cx5_PI_Discharge == EPSrequest) )
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_CONNECTION(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	OBC_ChargingConnectionConfirmati Connection;


	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Connection); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx2_full_connected == Connection)
	{
		RetVal = TRUE;
	}
	else if(Cx1_not_connected == Connection)
	{
		RetVal = FALSE;
	}
	else
	{
		/*This condition is only used to check the transition between not connected
		 and connected. (Rising edge)*/
		RetVal = WUM_SNA_U8;
	}

	return RetVal;
}
static boolean WUM_Condition_CONNECTION_FALL(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	OBC_ChargingConnectionConfirmati Connection;


	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&Connection); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx1_not_connected == Connection)
	{
		RetVal = TRUE;
	}
	else if(Cx2_full_connected == Connection)
	{
		RetVal = FALSE;
	}
	else
	{
		/*This condition is only used to check the transition between not connected
		 and connected. (Rising edge)*/
		RetVal = WUM_SNA_U8;
	}

	return RetVal;
}
static boolean WUM_Condition_BUTTON(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	(void)Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(&RetVal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Invert Push button state*/
	if(FALSE==RetVal)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_BUTTON_2(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	(void)Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(&RetVal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Do not invert Push button state*/
	return RetVal;
}
static boolean WUM_Condition_HMI_CHANGE(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OBC_RechargeHMIState HMI_state_previous = Cx0_Disconnected;

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	OBC_RechargeHMIState HMI_state;


	(void)Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(&HMI_state); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(HMI_state != HMI_state_previous)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	HMI_state_previous = HMI_state;
	return RetVal;
}
static boolean WUM_Condition_CHILLER(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;


	(void)Rte_Read_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(&RetVal); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(TRUE!=RetVal)
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_GMV(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	VCU_DDEGMVSEEM gmv;


	(void)Rte_Read_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(&gmv); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(WUM_PERCENTAGE_21<=gmv)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_MEAP(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	VCU_DMDMeap2SEEM meap;


	(void)Rte_Read_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(&meap); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(WUM_PERCENTAGE_25<=meap)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}
	return RetVal;
}
static boolean WUM_Condition_PUMP(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	BSI_VCUHeatPumpWorkingMode WorkingMode;


	(void)Rte_Read_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(&WorkingMode); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(Cx0C_De_icing_mode_==WorkingMode)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_CONSO_CPT(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	COUPURE_CONSO_CTP Consumption;


	(void)Rte_Read_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(&Consumption); /* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	if(FALSE==Consumption)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_CONSO_CPTE2(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RetVal;
	COUPURE_CONSO_CTPE2 Consumption;


	(void)Rte_Read_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(&Consumption);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(FALSE==Consumption)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static boolean WUM_Condition_CP(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 CP_value;
	boolean RetVal;


	(void)Rte_Read_PpDutyControlPilot_DeDutyControlPilot(&CP_value);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(100U==CP_value)
	{
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}

static void WUM_ElectronicIntegration(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean	enterModeRequest;
	boolean exitModeRequest;


	WUM_ElecIntegrMode.free_run_counter++;


	/* Check enter integration request */
	enterModeRequest = WUM_CheckConditionsEnterIntegrationMode();

	/* Check exit integration request */
	exitModeRequest = WUM_CheckConditionsExitIntegrationMode();

	/* Check change */
	/* Exit integration mode request has higher priority than enter integration mode */
	if ((FALSE != exitModeRequest) && (FALSE != WUM_ElecIntegrMode.ElectronicIntegrationRequest))
	{
		/* Perform a transition to exit electronic integration mode */
		WUM_ElecIntegrMode.ElectronicIntegrationRequest = FALSE;
		/* PRQA S 0314 ++ # Conversion needed to serialize/deserialize data into NvM */
		(void)Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock((void*)(&WUM_ElecIntegrMode.ElectronicIntegrationRequest));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		/* PRQA S 0314 -- */
	}
	else if ((FALSE != enterModeRequest) && (FALSE == WUM_ElecIntegrMode.ElectronicIntegrationRequest))
	{
		/* Perform a transition to enter electronic integration mode */
		WUM_ElecIntegrMode.ElectronicIntegrationRequest = TRUE;
		/* PRQA S 0314 ++ # Conversion needed to serialize/deserialize data into NvM */
		(void)Rte_Call_NvMService_AC3_SRBS_NvWUMBlockNeed_WriteBlock((void*)(&WUM_ElecIntegrMode.ElectronicIntegrationRequest));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		/* PRQA S 0314 -- */
	}
	else
	{
		/* No modification in the state */
	}

	/* Publish the output */
	(void)Rte_Write_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest(WUM_ElecIntegrMode.ElectronicIntegrationRequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static boolean WUM_CheckConditionsEnterIntegrationMode(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 value = 0x00;
	boolean result;
	boolean ret_value = FALSE;
	VCU_DiagMuxOnPwt VCU_DiagMuxOnPwtSignal;
	SUPV_RCDLineState SUPV_RCDLineStateSignal;

	result = WUM_received3FramesInOneSecondEIM(&value);

	(void)Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&VCU_DiagMuxOnPwtSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&SUPV_RCDLineStateSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((result != FALSE)
			&& (WUM_ELECTRON_BYTE_MASK == value)
			&& (0x01U != VCU_DiagMuxOnPwtSignal)
			/* Deactivated the newly conditions added */
			&& (FALSE != SUPV_RCDLineStateSignal)
	)
	{
		/* This is the condition to enter electronic integration mode */
		ret_value = TRUE;
	}

	return ret_value;
}


static boolean WUM_CheckConditionsExitIntegrationMode(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 value = 0x00;
	boolean result;
	boolean ret_value = FALSE;
	VCU_DiagMuxOnPwt VCU_DiagMuxOnPwtSignal;
	SUPV_RCDLineState SUPV_RCDLineStateSignal;

	result = WUM_received3FramesInOneSecondEIM(&value);

	(void)Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&VCU_DiagMuxOnPwtSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&SUPV_RCDLineStateSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (0x01U == VCU_DiagMuxOnPwtSignal)
	{
		/* One condition to exti integration mode */
		ret_value = TRUE;
	}

	if ((result != FALSE) && (0x00U == value))
	{
		/* Another condition to exit integration mode */
		ret_value = TRUE;
	}

	if (FALSE == SUPV_RCDLineStateSignal)
	{
		/* Another condition to exit integration mode */
		ret_value = TRUE;
	}

	return ret_value;
}


static boolean WUM_received3FramesInOneSecondEIM(uint8 * value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ret_value;
	uint8 masked_values[3];
	uint32 distances[3];



	if ((void*)NULL_PTR != value)
	{
		if((WUM_ElecIntegrMode.timestamp[0] == 0xFFFFFFFFU) ||
				(WUM_ElecIntegrMode.timestamp[1] == 0xFFFFFFFFU) ||
				(WUM_ElecIntegrMode.timestamp[2] == 0xFFFFFFFFU)
				)
		{
			/* Not enough frames received */
			ret_value = FALSE;
		}
		else
		{
			masked_values[0] = WUM_ElecIntegrMode.value[0] & WUM_ELECTRON_BYTE_MASK;
			masked_values[1] = WUM_ElecIntegrMode.value[1] & WUM_ELECTRON_BYTE_MASK;
			masked_values[2] = WUM_ElecIntegrMode.value[2] & WUM_ELECTRON_BYTE_MASK;

			if ((masked_values[0] != masked_values[1]) || (masked_values[1] != masked_values[2]))
			{
				/* Consecutive frames are different */
				ret_value = FALSE;
			}
			else
			{
				distances[0] = WUM_calculateDistance(WUM_ElecIntegrMode.timestamp[0],WUM_ElecIntegrMode.free_run_counter);
				distances[1] = WUM_calculateDistance(WUM_ElecIntegrMode.timestamp[1],WUM_ElecIntegrMode.free_run_counter);
				distances[2] = WUM_calculateDistance(WUM_ElecIntegrMode.timestamp[2],WUM_ElecIntegrMode.free_run_counter);

				if (
						(distances[0] > (uint32)WUM_TIME_FACTOR_1S) ||
						(distances[1] > (uint32)WUM_TIME_FACTOR_1S) ||
						(distances[2] > (uint32)WUM_TIME_FACTOR_1S)
						)
				{
					/* Too much separated frames */
					ret_value = FALSE;
				}
				else
				{
					/* Match!!! */
					ret_value = TRUE;
					*value = masked_values[0];
				}

			}

		}
	}
	else
	{
		/* input NULL pointer */
		ret_value = FALSE;
	}

	return ret_value;
}


static uint32 WUM_calculateDistance(uint32 data1, uint32 data2)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 ret_value;


	if (data1 > data2)
	{
		ret_value = data1 - data2;
	}
	else
	{
		ret_value = data2 - data1;
	}

	return ret_value;
}

static void WUM_DiagnosticToolsRequestOperation(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 maxTimeDiagToolsPeriods;
	IdtAppRCDECUState AppRCDECUState;
	SUPV_RCDLineState RCDLineState;


	/* Get calibratable and adapt to function period counts */
	maxTimeDiagToolsPeriods = ((uint16)Rte_CData_CalMaxTimeDiagTools())/10U;


	if (FALSE != WUM_DiagToolsRequest)
	{
		/* Check timeout */
		if (WUM_DiagToolsRequestTimeoutCounter > maxTimeDiagToolsPeriods)
		{
			/* Timeout */
			WUM_DiagToolsRequest = FALSE;
			WUM_DiagToolsRequestTimeoutCounter = 0U;
		}
		else
		{
			WUM_DiagToolsRequestTimeoutCounter++;
		}
	}

	/* Check the other conditions to set DiagToolsRequest to FALSE */
	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&AppRCDECUState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&RCDLineState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((0x00U == RCDLineState) || (0x04U == AppRCDECUState))
	{
		WUM_DiagToolsRequest = FALSE;
		WUM_DiagToolsRequestTimeoutCounter = 0U;
	}

	/* Set the final signal output value */
	(void)Rte_Write_PpDiagToolsRequest_DeDiagToolsRequest(WUM_DiagToolsRequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

void WUM_ResetRequest(uint8 resetType)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	WUM_resetType = resetType;
}

void WUM_GetResetRequest(uint8 * resetType)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	if (NULL_PTR != resetType)
	{
		*resetType = WUM_resetType;
	}
}


static void WUM_GlobalClearFaults(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean WUM_GCF_Conditions_Prev = FALSE;

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 value;
	boolean result;
	VCU_DiagMuxOnPwt VCU_DiagMuxOnPwtSignal;
	boolean WUM_GCF_Conditions = FALSE;

	result = WUM_received3FramesInOneSecondGCF(&value);

	(void)Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&VCU_DiagMuxOnPwtSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if (
			(FALSE != result) &&									/* Three frames in one second received */
			(value == WUM_EFFAC_DEFAUT_DIAG_BYTE_MASK) &&		/* Received request for clearing */
			(VCU_DiagMuxOnPwtSignal != 0x01U)
	)
	{
		/* Request valid */
		WUM_GCF_Conditions = TRUE;
	}

	/* Only call the service to clean DTCs first time conditions are met */
	if ((WUM_GCF_Conditions == TRUE) && (WUM_GCF_Conditions_Prev == FALSE))
	{
		(void)Rte_Call_PpDGN_AppCleanDTCs_OpAppCleanDTCs();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	WUM_GCF_Conditions_Prev = WUM_GCF_Conditions;

}


static boolean WUM_received3FramesInOneSecondGCF(uint8 * value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApWUM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApWUM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApWUM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApWUM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApWUM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ret_value;
	uint8 masked_values[3];
	uint32 distances[3];



	if ((void*)NULL_PTR != value)
	{
		if((WUM_ElecIntegrMode.timestamp[0] == 0xFFFFFFFFU) ||
				(WUM_ElecIntegrMode.timestamp[1] == 0xFFFFFFFFU) ||
				(WUM_ElecIntegrMode.timestamp[2] == 0xFFFFFFFFU)
				)
		{
			/* Not enough frames received */
			ret_value = FALSE;
		}
		else
		{
			masked_values[0] = WUM_ElecIntegrMode.value[0] & WUM_EFFAC_DEFAUT_DIAG_BYTE_MASK;
			masked_values[1] = WUM_ElecIntegrMode.value[1] & WUM_EFFAC_DEFAUT_DIAG_BYTE_MASK;
			masked_values[2] = WUM_ElecIntegrMode.value[2] & WUM_EFFAC_DEFAUT_DIAG_BYTE_MASK;

			if ((masked_values[0] != masked_values[1]) || (masked_values[1] != masked_values[2]))
			{
				/* Consecutive frames are different */
				ret_value = FALSE;
			}
			else
			{
				distances[0] = WUM_calculateDistance(WUM_ElecIntegrMode.timestamp[0],WUM_ElecIntegrMode.free_run_counter);
				distances[1] = WUM_calculateDistance(WUM_ElecIntegrMode.timestamp[1],WUM_ElecIntegrMode.free_run_counter);
				distances[2] = WUM_calculateDistance(WUM_ElecIntegrMode.timestamp[2],WUM_ElecIntegrMode.free_run_counter);

				if (
						(distances[0] > (uint32)WUM_TIME_FACTOR_1S) ||
						(distances[1] > (uint32)WUM_TIME_FACTOR_1S) ||
						(distances[2] > (uint32)WUM_TIME_FACTOR_1S)
						)
				{
					/* Too much separated frames */
					ret_value = FALSE;
				}
				else
				{
					/* Match!!! */
					ret_value = TRUE;
					*value = masked_values[0];
				}

			}

		}
	}
	else
	{
		/* input NULL pointer */
		ret_value = FALSE;
	}

	return ret_value;
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
