/*
 * AppExternals.h
 *
 *  Created on: 4 nov. 2019
 *      Author: M0058239
 */

#ifndef APPSW_APPEXTERNALS_H_
#define APPSW_APPEXTERNALS_H_

#include "Eth_30_Ar7000_Types.h"


#define		APP_SAMPLES_D1_2						4
#define		APP_SAMPLES_D2_1						5
#define		APP_SAMPLES_D2_1_NO_XCP					6

#define 	APP_NO_RESISTANCES					0
#define		APP_ALLOW_RESISTANCES				1

#define		APP_DEBUG_DID_DISABLED				0
#define		APP_DEBUG_DID_ENABLED				1

#define		APP_XCP_DEACTIVATED					0
#define		APP_XCP_ACTIVATED					1

/* Configuration options */
#define		APP_HW_VERSION							APP_SAMPLES_D2_1_NO_XCP
#define		APP_USE_RESISTANCES					APP_NO_RESISTANCES
/* End configuration options */


#if (APP_HW_VERSION == APP_SAMPLES_D2_1_NO_XCP)
#define		APP_XCP_MODE						APP_XCP_DEACTIVATED
#define 	APP_DEBUG_DID						APP_DEBUG_DID_DISABLED
#else
#define		APP_XCP_MODE						APP_XCP_ACTIVATED
#define 	APP_DEBUG_DID						APP_DEBUG_DID_ENABLED
#endif


#define		APP_SW_MAJOR_VERSION_GENERAL					21U
#define		APP_SW_MINOR_VERSION_GENERAL					2U
#define		APP_SW_MAJOR_VERSION							((APP_USE_RESISTANCES*100)+APP_SW_MAJOR_VERSION_GENERAL)
//#define		APP_SW_MINOR_VERSION							((APP_SW_MINOR_VERSION_GENERAL*10U)+APP_HW_VERSION)
#define		APP_SW_MINOR_VERSION							28U

/* Legacy ZA field values for FBL older than 1.2 */
#define		APP_ZA_PRODUCT_NUMBER_BYTE_0		152U;
#define		APP_ZA_PRODUCT_NUMBER_BYTE_1		65U;
#define		APP_ZA_PRODUCT_NUMBER_BYTE_2		131U;
#define		APP_ZA_PRODUCT_NUMBER_BYTE_3		133U;
#define		APP_ZA_PRODUCT_NUMBER_BYTE_4		128U;

#define		APP_ZA_PLAN_NUMBER_BYTE_0			152U;
#define		APP_ZA_PLAN_NUMBER_BYTE_1			65U;
#define		APP_ZA_PLAN_NUMBER_BYTE_2			131U;
#define		APP_ZA_PLAN_NUMBER_BYTE_3			133U;
#define		APP_ZA_PLAN_NUMBER_BYTE_4			128U;



extern void EthClient_Cbk_PlcResetOnOsd(uint8 CtrlIdx, Eth_30_Ar7000_OsdResetType Mode);
extern void CHG_Eth_No_Com(void);
extern void CHG_Eth_Full_Com(void);
extern void WUM_ResetRequest(uint8 resetType);
extern void WUM_GetResetRequest(uint8 * resetType);
extern void MSC_SaveDonePowerLatch(void);
extern void DGN_GetNumOfLogRecords(void);

#endif /* APPSW_APPEXTERNALS_H_ */
