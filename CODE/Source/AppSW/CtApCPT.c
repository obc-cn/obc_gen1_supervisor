/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApCPT.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApCPT
 *  Generation Time:  2020-11-23 11:42:16
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApCPT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup CPT
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Control Pilot interface.
 * \details This module manages the information coming form the control pilot.
 * Its main function is to report the control pilot state and determine the max
 * input current allowed by the EVSE.
 *
 * \{
 * \file  CPT.c
 * \brief  Generic code of the CPT module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApCPT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Autogenrated file.*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */

/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/

#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/**None mode threshold.*/
#define Cx0_no_charging_THRESHOLD 3U
/**Fast mode threshold.*/
#define Cx3_Euro_fast_charging_THRESHOLD 7U
/**Slow mode threshold.*/
#define Cx1_slow_charging_THRESHOLD 97U

/** Max duty cycle for 0A section.*/
#define CPT_DUTY_0A_THRESHOLD	8U
/** 0A value.*/
#define CPT_CURRENT_0A			0U
/** Max duty cycle for 6A section.*/
#define CPT_DUTY_6A_THRESHOLD	10U
/** 6A value.*/
#define CPT_CURRENT_6A			60U
/** Max duty cycle for formula 1 section.*/
#define CPT_DUTY_F1				85U
/** Formula 1 gain.*/
#define CPT_CURRENT_F1_GAIN		6U
/** Max duty cycle for formula 2 section.*/
#define CPT_DUTY_F2				96U
/** Formula 2 offset.*/
#define CPT_CURRENT_F2_OFFSET	64U
/** Formula 2 gain.*/
#define CPT_CURRENT_F2_GAIN		25U
/** Max duty cycle for max current.*/
#define CPT_DUTY_MAX			97U
/** Max current value.*/
#define CPT_CURRENT_MAX			800U
/** Debounce time for not charging*/
#define CPT_DEBOUNCE_TIME_NO_CHARGING 3U

/* PRQA S 0857 -- #Autogenrated file.*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApCPT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Charging mode.*/
static OBC_ChargingMode CPT_chargingMode;
static OBC_ChargingMode preMode_previous;
/** Debounce time to change the charging mode.*/
static IdtDebounceControlPilot CPT_debounceTime;

#define CtApCPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApCPT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApCPT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApCPT_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApCPT_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/**
 * \brief Charging mode calculate.
 * \details This function determines the charging mode depending on the
 * duty cycle.
 * \parama[in] CPT_DutyCycle Duty cycle.
 * \param[in,out] \ref CPT_chargingMode  Charging mode.
 */
static void CPT_ModeCalculate (const IdtDutyControlPilot CPT_DutyCycle);

/**
 * \brief Max charging current allowed.
 * \details This function determines the maximum current that the EVSE can
 * supply.
 * \parama[in] CPT_DutyCycle Duty cycle.
 * \return Max current in deciAmp.
 */
static uint16 CPT_CurrentCalculate(const IdtDutyControlPilot CPT_DutyCycle);

/** Update calibratables*/
static void CPT_UpdateCalibratables(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDebounceControlPilot: Integer in interval [0...20]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtMaxInputACCurrentEVSE: Integer in interval [0...65535]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * OBC_PlugVoltDetection: Boolean
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * OBC_CP_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_value (0U)
 *   Cx1_CP_invalid_not_connected (1U)
 *   Cx2_CP_valid_full_connected (2U)
 *   Cx3_CP_invalid_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint8 *Rte_Pim_PimCPT_NvMRamMirror(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDebounceControlPilot Rte_CData_CalDebounceControlPilot(void)
 *
 *********************************************************************************************************************/


#define CtApCPT_START_SEC_CODE
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCPT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCPT_init_doc
 *********************************************************************************************************************/

/*!
 * \brief CPT initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCPT_CODE) RCtApCPT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCPT_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApCPT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCPT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCPT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	CPT_debounceTime = Rte_CData_CalDebounceControlPilot();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCPT_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data)
 *   Std_ReturnType Rte_Write_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCPT_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief CPT task.
 *
 * This function is called periodically by the scheduler. This function should
 * Update the Control Pilot state and the max input current allowed by the EVSE.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCPT_CODE) RCtApCPT_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCPT_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApCPT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCPT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCPT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static boolean CPT_FirstTime = TRUE;

	#define CtApCPT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtDutyControlPilot CPT_dutyCycle;
	/*IdtControlPilotADCPhysicalValue CPT_physicalValue;*/
	uint16 CPT_CurrentMax;

	boolean CPT_plugDetected ;
	OBC_CP_connection_Status CPT_CPstatus;
	uint8 reset_type;
	boolean shutdown_auth;
	uint8 * ramMirror;

	ramMirror = Rte_Pim_PimCPT_NvMRamMirror();

	CPT_UpdateCalibratables();

	if (FALSE != CPT_FirstTime)
	{
		/* Init values from NVM */
		CPT_chargingMode = (OBC_ChargingMode)(*ramMirror);
		preMode_previous = (OBC_ChargingMode)(*ramMirror);

		CPT_FirstTime = FALSE;
	}


	(void)Rte_Read_PpDutyControlPilot_DeDutyControlPilot(&CPT_dutyCycle);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	CPT_ModeCalculate(CPT_dutyCycle);

	/*Input current.*/
	if(Cx1_slow_charging==CPT_chargingMode)
	{
		CPT_CurrentMax = CPT_CurrentCalculate(CPT_dutyCycle);
	}
	else
	{
		CPT_CurrentMax = CPT_CURRENT_0A;
	}

	/*Plug detection*/
	if((Cx1_slow_charging==CPT_chargingMode)||(Cx3_Euro_fast_charging==CPT_chargingMode))
	{
		CPT_plugDetected = TRUE;
		CPT_CPstatus = Cx2_CP_valid_full_connected;
	}
	else
	{
		CPT_plugDetected = FALSE;
		CPT_CPstatus = Cx1_CP_invalid_not_connected;
	}


	/* Mark NvM to be saved */
	WUM_GetResetRequest(&reset_type);
	(void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&shutdown_auth);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FALSE == shutdown_auth) && (0x02U != reset_type))
	{
		*ramMirror = (uint8)CPT_chargingMode;
		(void)Rte_Call_NvMService_AC3_SRBS_CPTNvBlockNeed_SetRamBlockStatus(TRUE);
	}



	(void)Rte_Write_PpInt_OBC_ChargingMode_OBC_ChargingMode(CPT_chargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(CPT_CurrentMax);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(CPT_CPstatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(CPT_plugDetected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApCPT_STOP_SEC_CODE
#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static void CPT_ModeCalculate (const IdtDutyControlPilot CPT_DutyCycle)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCPT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCPT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 debounceCounter = 0U;

	#define CtApCPT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCPT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	#define CtApCPT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_ChargingMode preMode;
	uint8 Debouncetime;


	/*Pre mode calculation*/
	if(Cx0_no_charging_THRESHOLD > CPT_DutyCycle)
	{
		preMode = Cx0_no_charging;
	}
	else if(Cx3_Euro_fast_charging_THRESHOLD >= CPT_DutyCycle)
	{
		preMode = Cx3_Euro_fast_charging;
	}
	else if(Cx1_slow_charging_THRESHOLD >= CPT_DutyCycle)
	{
		preMode = Cx1_slow_charging;
	}
	else
	{
		preMode = Cx0_no_charging;
	}

	if(Cx0_no_charging==preMode)
	{
		Debouncetime = CPT_DEBOUNCE_TIME_NO_CHARGING;
	}
	else
	{
		Debouncetime = CPT_debounceTime;
	}

	if(preMode!= preMode_previous)
	{
		/*Reset counter*/
		debounceCounter = 0U;
	}

	if(debounceCounter<Debouncetime)
	{
		/*Update counter*/
		debounceCounter ++;
	}
	else
	{
		/*Update state*/
		CPT_chargingMode = preMode;
	}

	preMode_previous = preMode;
}

static uint16 CPT_CurrentCalculate(const IdtDutyControlPilot CPT_DutyCycle)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCPT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCPT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCPT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 currentMax;


	if(CPT_DUTY_0A_THRESHOLD > CPT_DutyCycle)
	{
		currentMax = CPT_CURRENT_0A;
	}
	else if(CPT_DUTY_6A_THRESHOLD > CPT_DutyCycle)
	{
		currentMax = CPT_CURRENT_6A;
	}
	else if(CPT_DUTY_F1 >= CPT_DutyCycle)
	{
		currentMax = (uint16)CPT_DutyCycle*(uint16)CPT_CURRENT_F1_GAIN;
	}
	else if(CPT_DUTY_F2 >= CPT_DutyCycle)
	{
		currentMax = ((uint16)(uint8)(CPT_DutyCycle-CPT_CURRENT_F2_OFFSET))*(uint16)CPT_CURRENT_F2_GAIN;
	}
	else if(CPT_DUTY_MAX >= CPT_DutyCycle)
	{
		currentMax = CPT_CURRENT_MAX;
	}
	else
	{
		currentMax = CPT_CURRENT_0A;
	}

	return currentMax;
}

static void CPT_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCPT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCPT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCPT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCPT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCPT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	CPT_debounceTime = Rte_CData_CalDebounceControlPilot();
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
