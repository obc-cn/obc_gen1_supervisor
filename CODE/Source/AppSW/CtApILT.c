/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApILT.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApILT
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApILT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup ILT
 * \author Pablo Bolas.
 * \ingroup AppSw
 * \brief Inlet manager.
 * \details This module manages the input signals.
 * \{
 * \file  ilt.c
 * \brief  Generic code of the ILT module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApILT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Autogenrated file.*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/*! Signal not available value for U8.*/
#define ILT_SNA_U8 				0xFF
/*! VCU mode request different to drive.*/
#define ILT_MODEREQ_NOT_DRIVE	0x01
/*! Conversion gain between seconds and 100 milliseconds.*/
#define ILT_S_TO_100MS			10U
/*! Conversion gain between 100 milliseconds and 10 milliseconds.*/
#define ILT_100MS_TO_10MS		10U

/* PRQA S 0857 -- #Autogenrated file.*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

/*! HMI states*/
typedef enum{
	/*! Hose disconnected.*/
	ILT_HMISTATE_DISCONNECTED = 0x00,
	/*! Charge in progress*/
	ILT_HMISTATE_INPROGRESS = 0x01,
	/*! Charge failed*/
	ILT_HMISTATE_FAILURE = 0x02,
	/*! Charge stopped*/
	ILT_HMISTATE_STOPPED = 0x03,
	/*! Charge finished*/
	ILT_HMISTATE_FINISHED = 0x04,
	/*! Number of HMI states*/
	ILT_HMISTATE_NUMBER
}ILT_HMIstate_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*!
 * \brief Push button active time.
 * \details Minimum push button active time.
 */
static uint16 ILT_TimePushButton;
/*!
 * \brief E-lock error confirmation time.
 * \details Time needed to confirm a E-lock error.
 */
static uint16 ILT_ElockTimeError;
/*!
 * \brief Max time reporting a state.
 * \details Maximum time that the module can be reporting an HIM active state.
 * This variable is read from NVM. If this time is set to 0, this feature is
 * disable.
 */
static uint32 ILT_hmiTimeMax[ILT_HMISTATE_NUMBER];

/** Debounce time for each HMI state.*/
static uint8 ILT_hmiTimeDebounce[ILT_HMISTATE_NUMBER];

#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/

/*!
 * \brief Push Button report.
 * \details This functions manages the push button state. The active state
 * should be reported for at least \ref ILT_TimePushButton.
 */
static void ILT_PushButtonReport (void);
/*!
 * \brief Report E-lock state.
 * \details This function reports the E-lock state for the LED feature.
 * This function reports if the E-lock is locked or if the E-lock has an error.
 */
static void ILT_ElockReport(void);

/*!
 * \brief LED management task.
 * \details This function sets the HMI states depending on the charge state,
 * saturation, hose state, VCU request mode.
 */
static void ILT_led_management(void);
/*!
 * \brief State report limit.
 * \details This functions limits the maximum time while a state is reported.
 * \param[in] ILT_HMIstate Current HMI state,
 * \param[in] ILT_HMIstateOut_previous Last HMI state reported.
 * \param[in] ILT_counter_max Maximum time while this state can be reported.
 * \return Reported state for the \ref ILT_HMIstate.
 */
static boolean ILT_reportlimit(ILT_HMIstate_t ILT_HMIstate,
							uint8 ILT_HMIstateOut_previous,
							uint32 ILT_counter_max);
/*!
 * \brief HMI get state.
 * \details This function determines the HMI state.
 * \param[in] ILT_chargestate Charge state.
 * \param[in] ILT_plugged Plug state.
 * \param[in] ILT_saturated Saturation state.
 * \param[in] ILT_ChargeType Charge Type.
 * \return HMI state.
 */
static ILT_HMIstate_t ILT_hmistate_calculate(uint8 ILT_chargestate,
										boolean ILT_saturated,BSI_ChargeTypeStatus ILT_ChargeType,
										OBC_Fault OBC_Fault_data, OBC_ChargingConnectionConfirmati ILT_OBC_ChargingConnectionConfirmati_data);

/**
 * \brief HIM state debounce.
 * \details This function applies a debounce over HMI state.
 * \param[in] ILT_hmiPreState State detected.
 * \return HMI state reported.
 */
static ILT_HMIstate_t ILT_hmistate_debounce(ILT_HMIstate_t ILT_hmiPreState);

/** Write output values into RTE.*/
static void ILT_writeOut(const boolean ILT_HMIreportedStates[ILT_HMISTATE_NUMBER], uint8 ILT_HMIstateU8);
/** Plant Mode Bypass*/
static void ILT_PlantMode(void);

/** Update calibratables*/
static void ILT_UpdateCalibratables(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDebounceStateFailure: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateFinished: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateInProgress: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtDebounceStateStopped: Integer in interval [0...50]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtElockTimeError: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtMaxTimeChargeError: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeChargeInProgress: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeEndOfCharge: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeGuideManagement: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtMaxTimeProgrammingCharge: Integer in interval [0...3600]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtRECHARGE_HMI_STATE: Integer in interval [0...255]
 * IdtTimePushButtonKeepValue: Integer in interval [0...600]
 *   Unit: [ms], Factor: 100, Offset: 0
 * OBC_PushChargeType: Boolean
 * VCU_ElecMeterSaturation: Boolean
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_ChargeState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_ON (0U)
 *   Cx1_OFF_not_ended (1U)
 *   Cx2_OFF_ended (2U)
 *   Cx3_Unavailable (3U)
 * BSI_ChargeTypeStatus: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_DIFFEREE (1U)
 *   Cx2_IMMEDIATE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxTimeChargeError Rte_CData_CalMaxTimeChargeError(void)
 *   IdtMaxTimeChargeInProgress Rte_CData_CalMaxTimeChargeInProgress(void)
 *   IdtMaxTimeEndOfCharge Rte_CData_CalMaxTimeEndOfCharge(void)
 *   IdtMaxTimeGuideManagement Rte_CData_CalMaxTimeGuideManagement(void)
 *   IdtMaxTimeProgrammingCharge Rte_CData_CalMaxTimeProgrammingCharge(void)
 *   IdtTimePushButtonKeepValue Rte_CData_CalTimePushButtonKeepValue(void)
 *   IdtDebounceStateFailure Rte_CData_CalDebounceStateFailure(void)
 *   IdtDebounceStateFinished Rte_CData_CalDebounceStateFinished(void)
 *   IdtDebounceStateInProgress Rte_CData_CalDebounceStateInProgress(void)
 *   IdtDebounceStateStopped Rte_CData_CalDebounceStateStopped(void)
 *   IdtElockTimeError Rte_CData_CalElockTimeError(void)
 *
 *********************************************************************************************************************/


#define CtApILT_START_SEC_CODE
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_init_doc
 *********************************************************************************************************************/

/*!
 * \brief ILT initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
		uint8 ILT_index;


		/*Get NVM variables.*/
		/*Push button*/
		ILT_TimePushButton=(uint16)Rte_CData_CalTimePushButtonKeepValue();

		/*HMI state*/
		ILT_hmiTimeMax[ILT_HMISTATE_FINISHED]=(uint32)Rte_CData_CalMaxTimeEndOfCharge();
		ILT_hmiTimeMax[ILT_HMISTATE_STOPPED]=Rte_CData_CalMaxTimeProgrammingCharge();
		ILT_hmiTimeMax[ILT_HMISTATE_FAILURE]=Rte_CData_CalMaxTimeChargeError();
		ILT_hmiTimeMax[ILT_HMISTATE_INPROGRESS]=Rte_CData_CalMaxTimeChargeInProgress();
		ILT_hmiTimeMax[ILT_HMISTATE_DISCONNECTED]=Rte_CData_CalMaxTimeGuideManagement();

		/*Debounce time*/
		ILT_hmiTimeDebounce[ILT_HMISTATE_FAILURE] = Rte_CData_CalDebounceStateFailure();
		ILT_hmiTimeDebounce[ILT_HMISTATE_FINISHED] = Rte_CData_CalDebounceStateFinished();
		ILT_hmiTimeDebounce[ILT_HMISTATE_INPROGRESS] = Rte_CData_CalDebounceStateInProgress();
		ILT_hmiTimeDebounce[ILT_HMISTATE_STOPPED] = Rte_CData_CalDebounceStateStopped();
		ILT_hmiTimeDebounce[ILT_HMISTATE_DISCONNECTED] = 0U;

		/*Time conversion from s to 100ms.*/
		for (ILT_index = 0U; ILT_index<(uint8)ILT_HMISTATE_NUMBER;ILT_index++)
		{
			ILT_hmiTimeMax[ILT_index] = (uint32)(uint16)ILT_hmiTimeMax[ILT_index] * (uint32)ILT_S_TO_100MS;
		}


		/*E-lock*/
		ILT_ElockTimeError = ((uint16)(uint8)Rte_CData_CalElockTimeError()) * (uint16)ILT_100MS_TO_10MS;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_task100msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msA_doc
 *********************************************************************************************************************/

 /*!
  * \brief ILT task.
  *
  * This function is called periodically by the scheduler. This function should
  * generate the inlet state reported by the ECU.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_task100msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msA
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	ILT_UpdateCalibratables();
	/*Push button management*/
	ILT_PushButtonReport();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_task100msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeChargeError(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeGuideManagement(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(boolean *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *   Std_ReturnType Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(IdtRECHARGE_HMI_STATE *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeChargeError(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeChargeInProgress(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeEndOfCharge(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeGuideManagement(boolean data)
 *   Std_ReturnType Rte_Write_PpLedModes_DeProgrammingCharge(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msB_doc
 *********************************************************************************************************************/

/*!
 * \brief ILT task.
 *
 * This function is called periodically by the scheduler. This function should
 * generate the inlet state reported by the ECU.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_task100msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task100msB
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	ILT_UpdateCalibratables();
	/*LED HMI*/
	ILT_led_management();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApILT_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(boolean data)
 *   Std_ReturnType Rte_Write_PpLockStateError_DeLockStateError(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief ILT task.
 *
 * This function is called periodically by the scheduler. This function should
 * generate the inlet state reported by the ECU.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApILT_CODE) RCtApILT_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApILT_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	ILT_UpdateCalibratables();
	/*E-lock Task.*/
	ILT_ElockReport();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApILT_STOP_SEC_CODE
#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static void ILT_PushButtonReport (void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean ILT_PreviousPushButtonValue = FALSE;
	static uint16 ILT_TimeKeepingValue = 0U;

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ILT_PushButtonValue;

	(void)Rte_Read_PpOutputChargePushFil_DeOutputChargePushFil(&ILT_PushButtonValue);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FALSE!=ILT_PushButtonValue) && (FALSE==ILT_PreviousPushButtonValue))
	{
		//rising edge.
		ILT_TimeKeepingValue = ILT_TimePushButton;
		//input state equal to output state.
	}else if(1U < ILT_TimeKeepingValue) //to compensate the first step.
	{
		ILT_TimeKeepingValue --;
		//force output state.
		ILT_PushButtonValue = TRUE;
	}else
	{
		//MISRA
		//input state equal to output state.
	}

	ILT_PreviousPushButtonValue = ILT_PushButtonValue;
	(void)Rte_Write_PpInt_OBC_PushChargeType_OBC_PushChargeType((OBC_PushChargeType)ILT_PushButtonValue);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void ILT_ElockReport(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static no-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 ILT_counter = 0U;

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 ILT_Elock_state;
	boolean ILT_elock_error;
	boolean ILT_lockLED;
	boolean ILT_PlantModeState;
	boolean ILT_lockLED_PlantMode;
	IdtOutputELockSensor ILT_Elock_state_temp;


	/*Get inputs from RTE.*/
	(void)Rte_Read_PpLockLED2BEPR_PlantMode_DeLockLED2BEPR_PlantMode(&ILT_lockLED_PlantMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&ILT_Elock_state_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	ILT_Elock_state=(uint8)ILT_Elock_state_temp;
	(void)Rte_Read_PpPlantModeState_DePlantModeState(&ILT_PlantModeState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*GET error state and ILT_lockLED value.*/
	switch (ILT_Elock_state) {
		case (uint8)ELOCK_LOCKED:
			ILT_elock_error = FALSE;
			ILT_lockLED = TRUE;
			break;
		case (uint8)ELOCK_UNLOCKED:
			ILT_elock_error = FALSE;
			ILT_lockLED = FALSE;
			break;
		default:
			ILT_elock_error = TRUE;
			ILT_lockLED = FALSE;
			break;
	}

	/*Debounce E-lock error activation.*/

	/*input state.*/
	if(FALSE == ILT_elock_error)
	{
		ILT_counter = 0U;
	}
	/*counter state*/
	else if(ILT_counter < ILT_ElockTimeError)
	{
		/*update counter.*/
		ILT_counter++;
		/*Force to false.*/
		ILT_elock_error = FALSE;
	}
	else
	{
		/*MISRA*/
		/*Report the input state. (TRUE)*/
	}

	if(FALSE!=ILT_PlantModeState)
	{
		ILT_lockLED = ILT_lockLED_PlantMode;
	}

	/*Set output into RTE.*/
	(void)Rte_Write_PpLockLED2BEPR_DeLockLED2BEPR(ILT_lockLED);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLockStateError_DeLockStateError(ILT_elock_error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void ILT_led_management(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* This variable should be initialize to a non valid state. Thus the counter
	 * inside ILT_reportlimit will restart. */
	static uint8 ILT_HMIstate_previous = ILT_SNA_U8;

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input variables*/
	uint8 ILT_charge_state;
	boolean ILT_satureted;
	uint8 ILT_modeRequest;
	boolean ILT_PlantModeState;
	BSI_ChargeTypeStatus ILT_ChargeType;
	OBC_Fault OBC_Fault_data;
	OBC_ChargingConnectionConfirmati OBC_ChargingConnectionConf_data;
	/*Output variables.*/
	boolean ILT_HMIreportedStates[ILT_HMISTATE_NUMBER] = {FALSE};
	uint8 ILT_HMIstateU8;


	/*Get var from DFA.*/
	(void)Rte_Read_PpInt_BSI_ChargeState_BSI_ChargeState(&ILT_charge_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(&ILT_satureted);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&ILT_modeRequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpPlantModeState_DePlantModeState(&ILT_PlantModeState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(&ILT_ChargeType);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Fault_OBC_Fault(&OBC_Fault_data);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&OBC_ChargingConnectionConf_data);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(FALSE!=ILT_PlantModeState)
	{
		ILT_PlantMode();
	}
	else
	{
		ILT_HMIstate_t ILT_preHMIstate;
		ILT_HMIstate_t ILT_HMIstate;

		/*calculate HMI state.*/
		ILT_preHMIstate = ILT_hmistate_calculate(ILT_charge_state,ILT_satureted,ILT_ChargeType,OBC_Fault_data,OBC_ChargingConnectionConf_data);

		ILT_HMIstate = ILT_hmistate_debounce(ILT_preHMIstate);

		if((ILT_HMISTATE_DISCONNECTED != ILT_HMIstate)
				|| ((uint8)ILT_MODEREQ_NOT_DRIVE != ILT_modeRequest))
		{
			ILT_HMIreportedStates[ILT_HMIstate] = ILT_reportlimit(ILT_HMIstate,
															ILT_HMIstate_previous,
															ILT_hmiTimeMax[ILT_HMIstate]);
			ILT_HMIstate_previous = (uint8)ILT_HMIstate;
		}
		else
		{
			/* None state is reported.*/
			/* Set previous state to a non valid state. Thus, when a state is
			 * reported again the counter inside ILT_reportlimit will restart.*/
			ILT_HMIstate_previous = ILT_SNA_U8;
		}
		ILT_HMIstateU8=(uint8)ILT_HMIstate;


		ILT_writeOut(ILT_HMIreportedStates,ILT_HMIstateU8);
	}


}

static void ILT_writeOut(const boolean ILT_HMIreportedStates[ILT_HMISTATE_NUMBER], uint8 ILT_HMIstateU8)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Write in RTE.*/
	(void)Rte_Write_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState((OBC_RechargeHMIState)ILT_HMIstateU8);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_DeEndOfCharge(ILT_HMIreportedStates[ILT_HMISTATE_FINISHED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_DeProgrammingCharge(ILT_HMIreportedStates[ILT_HMISTATE_STOPPED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_DeChargeError(ILT_HMIreportedStates[ILT_HMISTATE_FAILURE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_DeChargeInProgress(ILT_HMIreportedStates[ILT_HMISTATE_INPROGRESS]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpLedModes_DeGuideManagement(ILT_HMIreportedStates[ILT_HMISTATE_DISCONNECTED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


}
static void ILT_PlantMode(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ILT_HMIreportedStates[ILT_HMISTATE_NUMBER] = {FALSE};
	uint8 ILT_HMIstateU8;


	(void)Rte_Read_PpRECHARGE_HMI_STATE_PlantMode_DeRECHARGE_HMI_STATE_PlantMode(&ILT_HMIstateU8);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_PlantMode_DeEndOfCharge(&ILT_HMIreportedStates[ILT_HMISTATE_FINISHED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_PlantMode_DeProgrammingCharge(&ILT_HMIreportedStates[ILT_HMISTATE_STOPPED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_PlantMode_DeChargeError(&ILT_HMIreportedStates[ILT_HMISTATE_FAILURE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_PlantMode_DeChargeInProgress(&ILT_HMIreportedStates[ILT_HMISTATE_INPROGRESS]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_PlantMode_DeGuideManagement(&ILT_HMIreportedStates[ILT_HMISTATE_DISCONNECTED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	ILT_writeOut(ILT_HMIreportedStates,ILT_HMIstateU8);

}

static ILT_HMIstate_t ILT_hmistate_calculate(uint8 ILT_chargestate,
										boolean ILT_saturated,BSI_ChargeTypeStatus ILT_ChargeType,
										OBC_Fault OBC_Fault_data, OBC_ChargingConnectionConfirmati ILT_OBC_ChargingConnectionConfirmati_data)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	ILT_HMIstate_t ILT_hmiRet;


	/*Proximity state*/
	if(Cx2_full_connected != ILT_OBC_ChargingConnectionConfirmati_data)
	{
		ILT_hmiRet = ILT_HMISTATE_DISCONNECTED;
	}
	else if ((uint8)Cx0_ON == ILT_chargestate)
	{
		ILT_hmiRet = ILT_HMISTATE_INPROGRESS;
	}
	else if ((uint8)Cx2_OFF_ended == ILT_chargestate)
	{
		ILT_hmiRet = ILT_HMISTATE_FINISHED;
	}
	else if ((FALSE != ILT_saturated) || (OBC_FAULT_LEVEL_1 == OBC_Fault_data))
	{
		ILT_hmiRet = ILT_HMISTATE_FAILURE;
	}
	else if (((uint8)Cx1_OFF_not_ended == ILT_chargestate)&&(Cx1_DIFFEREE==ILT_ChargeType))
	{
		ILT_hmiRet = ILT_HMISTATE_STOPPED;
	}
	else /*Charge state == Unavailable.*/
	{
		ILT_hmiRet = ILT_HMISTATE_DISCONNECTED;
	}
	return ILT_hmiRet;
}

static boolean ILT_reportlimit(ILT_HMIstate_t ILT_HMIstate,
							uint8 ILT_HMIstateOut_previous,
							uint32 ILT_counter_max)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/*Same counter for all measures.*/
	static uint32 ILT_counter = 1U;

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ILT_hmiOut;


	if(0U == ILT_counter_max)
	{
		/*feature disable.*/
		ILT_hmiOut = TRUE;
	}
	else if((uint8)ILT_HMIstate != ILT_HMIstateOut_previous)
	{
		/*reset counter.*/
		ILT_counter = 1U;
		ILT_hmiOut = TRUE;
	}
	else if (ILT_counter < ILT_counter_max)
	{
		/*Update counter.*/
		ILT_counter++;
		ILT_hmiOut = TRUE;
	}
	else
	{
		/*Force report to FALSE.*/
		ILT_hmiOut = FALSE;
	}

	return ILT_hmiOut;
}

static ILT_HMIstate_t ILT_hmistate_debounce(ILT_HMIstate_t ILT_hmiPreState)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 ILT_debounceCounter = 0U;

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static ILT_HMIstate_t ILT_hmiStateOut = ILT_HMISTATE_DISCONNECTED;
	static ILT_HMIstate_t ILT_hmiStatePrevious = ILT_HMISTATE_DISCONNECTED;

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(ILT_hmiPreState!=ILT_hmiStatePrevious)
	{
		ILT_debounceCounter = 0U;
	}

	if(ILT_debounceCounter>=ILT_hmiTimeDebounce[ILT_hmiPreState])
	{
		ILT_hmiStateOut = ILT_hmiPreState;
	}
	else
	{
		ILT_debounceCounter++;
	}
	ILT_hmiStatePrevious = ILT_hmiPreState;

	return ILT_hmiStateOut;
}


static void ILT_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApILT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApILT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApILT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApILT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApILT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 ILT_index;


	ILT_TimePushButton=(uint16)Rte_CData_CalTimePushButtonKeepValue();

	/*HMI state*/
	ILT_hmiTimeMax[ILT_HMISTATE_FINISHED]=(uint32)Rte_CData_CalMaxTimeEndOfCharge();
	ILT_hmiTimeMax[ILT_HMISTATE_STOPPED]=Rte_CData_CalMaxTimeProgrammingCharge();
	ILT_hmiTimeMax[ILT_HMISTATE_FAILURE]=Rte_CData_CalMaxTimeChargeError();
	ILT_hmiTimeMax[ILT_HMISTATE_INPROGRESS]=Rte_CData_CalMaxTimeChargeInProgress();
	ILT_hmiTimeMax[ILT_HMISTATE_DISCONNECTED]=Rte_CData_CalMaxTimeGuideManagement();

	/*Debounce time*/
	ILT_hmiTimeDebounce[ILT_HMISTATE_FAILURE] = Rte_CData_CalDebounceStateFailure();
	ILT_hmiTimeDebounce[ILT_HMISTATE_FINISHED] = Rte_CData_CalDebounceStateFinished();
	ILT_hmiTimeDebounce[ILT_HMISTATE_INPROGRESS] = Rte_CData_CalDebounceStateInProgress();
	ILT_hmiTimeDebounce[ILT_HMISTATE_STOPPED] = Rte_CData_CalDebounceStateStopped();
	ILT_hmiTimeDebounce[ILT_HMISTATE_DISCONNECTED] = 0U;

	/*Time conversion from s to 100ms.*/
	for (ILT_index = 0U; ILT_index<(uint8)ILT_HMISTATE_NUMBER;ILT_index++)
	{
		ILT_hmiTimeMax[ILT_index] = (uint32)(uint16)ILT_hmiTimeMax[ILT_index] * (uint32)ILT_S_TO_100MS;
	}


	/*E-lock*/
	ILT_ElockTimeError = ((uint16)(uint8)Rte_CData_CalElockTimeError()) * (uint16)ILT_100MS_TO_10MS;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
