/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPSH.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApPSH
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPSH>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup PSH
 * \author AAC
 * \ingroup AppSw
 * \brief Implementation of Push Button processing
 * \details PSH is module in charge of process input signal from Push_button present in
 * Inlet. See PSA's document 01552_17_04212_1.0.
 * \{
 *
 * \file PSH.c
 * \brief  Generic code of the PSH module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApPSH.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
#include "AppExternals.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Conditions required to refresh the outputs*/
#define PSH_CONDITION_FUNCTION_ACTIVATION (APP_STATE_1 == PSH_AppRCDECUState) || \
          (APP_STATE_4 == PSH_AppRCDECUState) || \
          (APP_STATE_5 == PSH_AppRCDECUState)


/** Value read from RTE when the Push Button is pushed*/
#define PSH_INPUT_RAW_PUSHED          TRUE

/** Value for control flow inactive*/
#define PSH_CONTROL_FLOW_ACTIVE       TRUE

/** Output value after debouncing to indicate Push Button not pushed */
#define PSH_INPUT_FILT_NOT_PUSHED     FALSE

/** Define init value for output, required when control flow is inactive */
#define PSH_INPUT_FILT_DEFAULT_VALUE  PSH_INPUT_FILT_NOT_PUSHED

#define PSH_TASK_TIME		(100U)

 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**
 * Raw signal to filter. Initialized in DFA.
 */
static boolean PSH_InputChargePushRaw;

/**
 * Signal filtered after debounce. Initialized in DFA.
 */
static boolean PSH_OutputChargePushFil;

/**
 * Control flow signal. Initialized in DFA.
 */
static boolean PSH_CtrlFlowChargePush;

/**
 * appRCD_ECUState signal
 */
static IdtAppRCDECUState PSH_AppRCDECUState;

/**
 * Debounce for high state signal. Initialized in DFA.
 */
static IdtTimeDebChargePush PSH_TimeDebChargePushHigh;

/**
 * Debounce for low state signal. Initialized in DFA.
 */
static IdtTimeDebChargePush PSH_TimeDebChargePushLow;

static uint32 PSH_TimerPushInhib;

static boolean PSH_FirstTime;

static uint32 PSH_TimePushInhDelay;

static BSI_LockedVehicle PSH_BSI_LockedVehicle;

static OBC_RechargeHMIState PSH_OBC_RechargeHMIState;

#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/

/**
 * \brief Get input values form DFA.
 *
 * Task to acquire from DFA the required variables to perform all
 * the tasks. Variables have to be declared as static.
 */
static void PSH_variables_get(void);
/**
 * \brief Task to manage debouncing algorithm.
 *
 * This algorithm is active only if the \ref PSH_CtrlFlowChargePush signal is
 * active. In that case the \ref PSH_OutputChargePushFil signal should be Active
 * only if the \ref PSH_InputChargePushRaw has been active while the \ref
 * PSH_CounterPushChargePushed. And Not active if the \ref
 * PSH_InputChargePushRaw signal has been not active while \ref
 * PSH_CounterPushChargeNotPushed.
 *
 * If the function is disable the \ref PSH_OutputChargePushFil signal shall
 * be set to it's default value.
 */
static void PSH_PushChargeDebounce(void);
/**
 * \brief Publish to DFA.
 *
 * Task to publish to DFA the outputs of the software component.
 */
static void PSH_variable_set(void);

/** Update calibratables*/
static void PSH_UpdateCalibratables(void);

static void PSH_DeserializeNVM(void);

static boolean PSH_GetInitDelayExpired(void);

static void PSH_SerializeNVM(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtTimeDebChargePush: Integer in interval [0...15]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePushInhDelay: Integer in interval [0...1000]
 *   Unit: [s], Factor: 1, Offset: 0
 * Rte_DT_IdtPSHNvMArray_0: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_LockedVehicle: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_Locked (0U)
 *   Cx1_Locked (1U)
 *   Cx2_Overlocked (2U)
 *   Cx3_Reserved (3U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 * Array Types:
 * ============
 * IdtPSHNvMArray: Array with 4 element(s) of type Rte_DT_IdtPSHNvMArray_0
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   Rte_DT_IdtPSHNvMArray_0 *Rte_Pim_NvPSHBlockNeed_MirrorBlock(void)
 *     Returnvalue: Rte_DT_IdtPSHNvMArray_0* is of type IdtPSHNvMArray
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtTimePushInhDelay Rte_CData_CalTimePushInhDelay(void)
 *   IdtTimeDebChargePush Rte_CData_CalTimeDebChargePushHigh(void)
 *   IdtTimeDebChargePush Rte_CData_CalTimeDebChargePushLow(void)
 *   boolean Rte_CData_CalCtrlFlowChargePush(void)
 *
 *********************************************************************************************************************/


#define CtApPSH_START_SEC_CODE
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPSH_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_init_doc
 *********************************************************************************************************************/

/*!
 * \brief PSH initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPSH_CODE) RCtApPSH_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	PSH_TimeDebChargePushHigh = Rte_CData_CalTimeDebChargePushHigh();
	PSH_TimeDebChargePushLow = Rte_CData_CalTimeDebChargePushLow();
	PSH_CtrlFlowChargePush = Rte_CData_CalCtrlFlowChargePush();
	PSH_OutputChargePushFil = PSH_INPUT_FILT_DEFAULT_VALUE;
	PSH_FirstTime = TRUE;

	PSH_TimePushInhDelay = ((uint32)1000U * (uint32)Rte_CData_CalTimePushInhDelay()) / (uint32)PSH_TASK_TIME;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPSH_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_task100ms_doc
 *********************************************************************************************************************/

/*!
 * \brief PSH task.
 *
 * This function is called periodically by the scheduler. This function should
 * report the PSH state.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPSH_CODE) RCtApPSH_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPSH_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	boolean initDelayExpired;

	if (FALSE != PSH_FirstTime)
	{
		PSH_DeserializeNVM();
		PSH_FirstTime = FALSE;
	}

	initDelayExpired = PSH_GetInitDelayExpired();

	if (FALSE != initDelayExpired)
	{
		PSH_UpdateCalibratables();
		PSH_variables_get();
		PSH_PushChargeDebounce();
		PSH_variable_set();
		PSH_SerializeNVM();
	}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPSH_STOP_SEC_CODE
#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static void PSH_PushChargeDebounce(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static IdtTimeDebChargePush PSH_CounterPushChargePushed = 0U;
	static IdtTimeDebChargePush PSH_CounterPushChargeNotPushed = 0U;

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	boolean AllowUpdate;

	/* Calculate AllowUpdate condition */

	if (
			(Cx0_No_Locked == PSH_BSI_LockedVehicle)
			&&
			((Cx1_In_progress == PSH_OBC_RechargeHMIState) || (Cx3_Stopped == PSH_OBC_RechargeHMIState))
	)
	{
		PSH_TimerPushInhib = 0U;
		AllowUpdate = TRUE;
	}
	else if (
			(Cx1_Locked == PSH_BSI_LockedVehicle)
			&&
			((Cx1_In_progress == PSH_OBC_RechargeHMIState) || (Cx3_Stopped == PSH_OBC_RechargeHMIState))
	)
	{
		if (PSH_TimerPushInhib < PSH_TimePushInhDelay)
		{
			/* Saturate counter to PSH_TimePushInhDelay */
			PSH_TimerPushInhib++;
		}
		AllowUpdate = TRUE;
	}
	else
	{
		PSH_TimerPushInhib = 0U;
		AllowUpdate = FALSE;
	}


  if ( PSH_CONTROL_FLOW_ACTIVE == PSH_CtrlFlowChargePush )
  {
    if (PSH_INPUT_RAW_PUSHED == PSH_InputChargePushRaw)
    {
    	/* Input physical value to TRUE */
    	/* Additional monitoring conditions are needed */

    	if ((FALSE != AllowUpdate) && (PSH_TimerPushInhib < PSH_TimePushInhDelay))
    	{

		  if ( PSH_CounterPushChargePushed >= PSH_TimeDebChargePushLow)
		  {
			PSH_OutputChargePushFil = TRUE;
		  }
		  else
		  {
			PSH_CounterPushChargePushed++;
		  }
		  PSH_CounterPushChargeNotPushed = 0U;

    	}

    }
    else
    {
    	/* Input physical value to FALSE */
    	/* No additional monitoring conditions are needed */
      if ( PSH_CounterPushChargeNotPushed >= PSH_TimeDebChargePushHigh)
      {
        PSH_OutputChargePushFil = FALSE;
      }
      else
      {
        PSH_CounterPushChargeNotPushed++;
      }
      PSH_CounterPushChargePushed = 0U;
    }
  }
  else
  {
    PSH_OutputChargePushFil = FALSE;
    PSH_CounterPushChargeNotPushed = 0U;
    PSH_CounterPushChargePushed = 0U;
  }
}

static void PSH_variables_get(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


  (void)Rte_Read_PpInputChargePushRaw_DeInputChargePushRaw(&PSH_InputChargePushRaw);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&PSH_AppRCDECUState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(&PSH_BSI_LockedVehicle);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(&PSH_OBC_RechargeHMIState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static void PSH_variable_set(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


  if (PSH_CONDITION_FUNCTION_ACTIVATION)
  {
      (void)Rte_Write_PpOutputChargePushFil_DeOutputChargePushFil(PSH_OutputChargePushFil);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  }
}

static void PSH_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	PSH_TimeDebChargePushHigh = Rte_CData_CalTimeDebChargePushHigh();
	PSH_TimeDebChargePushLow = Rte_CData_CalTimeDebChargePushLow();
	PSH_CtrlFlowChargePush = Rte_CData_CalCtrlFlowChargePush();
	PSH_TimePushInhDelay = ((uint32)1000U * (uint32)Rte_CData_CalTimePushInhDelay()) / (uint32)PSH_TASK_TIME;

}

static void PSH_DeserializeNVM(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	uint8 * pPimData = (uint8 *)Rte_Pim_NvPSHBlockNeed_MirrorBlock();

	PSH_TimerPushInhib = 0U;

	/* PRQA S 2985,2986 ++ # Bit and masks not really needed, but kept for clarity in the operation of deserialization of NvM */
	PSH_TimerPushInhib |= ((((uint32)pPimData[0]) << 24U) & 0xFF000000U);
	PSH_TimerPushInhib |= ((((uint32)pPimData[1]) << 16U) & 0x00FF0000U);
	PSH_TimerPushInhib |= ((((uint32)pPimData[2]) << 8U) & 0x0000FF00U);
	PSH_TimerPushInhib |= (((uint32)pPimData[3]) & 0x000000FFU);
	/* PRQA S 2985,2986 -- */

}

static boolean PSH_GetInitDelayExpired(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	static uint8 PSH_InitDelayCounter = 0;

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	boolean retValue;

	/* 1 second initial delay */
	/* PSH task period is 100 ms. Then 10 counts shall be used */
	if (PSH_InitDelayCounter < 10U)
	{
		PSH_InitDelayCounter++;
		retValue = FALSE;
	}
	else
	{
		retValue = TRUE;
	}

	return retValue;

}

static void PSH_SerializeNVM(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApPSH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApPSH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApPSH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApPSH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApPSH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	uint8 resetRequest;
	boolean isShutingDown;

	uint8 * pPimData = (uint8 *)Rte_Pim_NvPSHBlockNeed_MirrorBlock();


	(void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&isShutingDown);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	WUM_GetResetRequest(&resetRequest);


	if((FALSE == isShutingDown) && (0x02U != resetRequest))
	{

		pPimData[0] = (uint8)((PSH_TimerPushInhib >> 24U) & 0x000000FFU); /*PRQA S 2985 # Bit mask not needed, but kept for clarity of the operation */
		pPimData[1] = (uint8)((PSH_TimerPushInhib >> 16U) & 0x000000FFU);
		pPimData[2] = (uint8)((PSH_TimerPushInhib >> 8U) & 0x000000FFU);
		pPimData[3] = (uint8)(PSH_TimerPushInhib & 0x000000FFU);

		(void)Rte_Call_NvMService_AC3_SRBS_NvPSHBlockNeed_SetRamBlockStatus(TRUE);
	}
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Close doxygen header*/
/**
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
