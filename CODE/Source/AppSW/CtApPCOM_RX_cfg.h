/**************************************************************************//**
 * \file	ctApPCOM_RX_cfg.h
 * \author	M0069810 Juan Carlos Romanillos <juancarlos.romanillos at es.mahle.com>
 * \date		20 nov. 2019
 * \copyright	MAHLE ELECTRONICS - 2019
 * \brief	
 * \ingroup ctApPCOM_cfg
 *****************************************************************************/

#ifndef APPSW_CTAPPCOM_RX_CFG_H_
#define APPSW_CTAPPCOM_RX_CFG_H_

/*****************************************************************************
 * INCLUDES
 *****************************************************************************/
#include "CtApPCOM_cfg.h"
#include "COM_Cbk.h"

/******************************************************************************
 * PUBLIC CONSTANT DEFINITIONS
 *****************************************************************************/

/******************************************************************************
 * PUBLIC TYPEDEFS
 *****************************************************************************/

/******************************************************************************
 * PUBLIC VARIABLE DECLARATIONS
 *****************************************************************************/

/******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES
 *****************************************************************************/


/**
 * \defgroup PCOM_RX_WRAPPERS RTE signals wrappers
 * \{
 */

/* PRQA S 0777,0779 EOF # The naming of the functions must match the names of the CAN signals */

Std_ReturnType PCOM_RX__BMS_SOC_GetInitial(uint16 * value);
Std_ReturnType PCOM_RX__BMS_SOC_GetPhysical(BMS_SOC * value);
Std_ReturnType PCOM_RX__BMS_SOC_GetDefaultSubstValue(uint16 * value);
boolean PCOM_RX__BMS_SOC_GetOutputSelector(void);
boolean PCOM_RX__BMS_SOC_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_SOC_Set(const BMS_SOC * value);
IdtSignalDiagTime PCOM_RX__BMS_SOC_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_SOC_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_SOC_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_SOC_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_Voltage_GetInitial(uint16 * value);
Std_ReturnType PCOM_RX__BMS_Voltage_GetPhysical(BMS_Voltage * value);
Std_ReturnType PCOM_RX__BMS_Voltage_GetDefaultSubstValue(uint16 * value);
boolean PCOM_RX__BMS_Voltage_GetOutputSelector(void);
boolean PCOM_RX__BMS_Voltage_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_Voltage_Set(const BMS_Voltage * value);
IdtSignalDiagTime PCOM_RX__BMS_Voltage_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_Voltage_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_Voltage_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_Voltage_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_RelayOpenReq_GetInitial(BMS_RelayOpenReq * value);
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_GetPhysical(BMS_RelayOpenReq * value);
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_GetDefaultSubstValue(BMS_RelayOpenReq * value);
boolean PCOM_RX__BMS_RelayOpenReq_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_RelayOpenReq_Set(const BMS_RelayOpenReq * value);
IdtSignalDiagTime PCOM_RX__BMS_RelayOpenReq_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_RelayOpenReq_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_RelayOpenReq_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_RelayOpenReq_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_MainConnectorState_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BMS_MainConnectorState_GetPhysical(BMS_MainConnectorState * value);
Std_ReturnType PCOM_RX__BMS_MainConnectorState_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BMS_MainConnectorState_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_MainConnectorState_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_MainConnectorState_Set(const BMS_MainConnectorState * value);
IdtSignalDiagTime PCOM_RX__BMS_MainConnectorState_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_MainConnectorState_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_MainConnectorState_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_MainConnectorState_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_GetInitial(BMS_QuickChargeConnectorState * value);
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_GetPhysical(BMS_QuickChargeConnectorState * value);
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_GetDefaultSubstValue(BMS_QuickChargeConnectorState * value);
boolean PCOM_RX__BMS_QuickChargeConnectorState_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_QuickChargeConnectorState_Set(const BMS_QuickChargeConnectorState * value);
IdtSignalDiagTime PCOM_RX__BMS_QuickChargeConnectorState_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_QuickChargeConnectorState_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_QuickChargeConnectorState_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_QuickChargeConnectorState_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_AuxBattVolt_GetInitial(uint16 * value);
Std_ReturnType PCOM_RX__BMS_AuxBattVolt_GetPhysical(BMS_AuxBattVolt * value);
Std_ReturnType PCOM_RX__BMS_AuxBattVolt_GetDefaultSubstValue(uint16 * value);
boolean PCOM_RX__BMS_AuxBattVolt_GetOutputSelector(void);
boolean PCOM_RX__BMS_AuxBattVolt_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_AuxBattVolt_Set(const BMS_AuxBattVolt * value);
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_AuxBattVolt_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_Fault_GetInitial(BMS_Fault * value);
Std_ReturnType PCOM_RX__BMS_Fault_GetPhysical(BMS_Fault * value);
Std_ReturnType PCOM_RX__BMS_Fault_GetDefaultSubstValue(BMS_Fault * value);
boolean PCOM_RX__BMS_Fault_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_Fault_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_Fault_Set(const BMS_Fault * value);
IdtSignalDiagTime PCOM_RX__BMS_Fault_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_Fault_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_Fault_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_Fault_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_GetInitial(uint16 * value);
Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_GetPhysical(BMS_HighestChargeVoltageAllow * value);
Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_GetDefaultSubstValue(uint16 * value);
boolean PCOM_RX__BMS_HighestChargeVoltageAllow_GetOutputSelector(void);
boolean PCOM_RX__BMS_HighestChargeVoltageAllow_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_HighestChargeVoltageAllow_Set(const BMS_HighestChargeVoltageAllow * value);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeVoltageAllow_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeVoltageAllow_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeVoltageAllow_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeVoltageAllow_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_GetInitial(uint16 * value);
Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_GetPhysical(BMS_HighestChargeCurrentAllow * value);
Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_GetDefaultSubstValue(uint16 * value);
boolean PCOM_RX__BMS_HighestChargeCurrentAllow_GetOutputSelector(void);
boolean PCOM_RX__BMS_HighestChargeCurrentAllow_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_HighestChargeCurrentAllow_Set(const BMS_HighestChargeCurrentAllow * value);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeCurrentAllow_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeCurrentAllow_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeCurrentAllow_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_HighestChargeCurrentAllow_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_GetInitial(BMS_OnBoardChargerEnable * value);
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_GetPhysical(BMS_OnBoardChargerEnable * value);
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_GetDefaultSubstValue(BMS_OnBoardChargerEnable * value);
boolean PCOM_RX__BMS_OnBoardChargerEnable_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_OnBoardChargerEnable_Set(const BMS_OnBoardChargerEnable * value);
IdtSignalDiagTime PCOM_RX__BMS_OnBoardChargerEnable_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_OnBoardChargerEnable_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_OnBoardChargerEnable_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_OnBoardChargerEnable_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_SlowChargeSt_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_GetPhysical(BMS_SlowChargeSt * value);
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BMS_SlowChargeSt_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_SlowChargeSt_Set(const BMS_SlowChargeSt * value);
IdtSignalDiagTime PCOM_RX__BMS_SlowChargeSt_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_SlowChargeSt_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_SlowChargeSt_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_SlowChargeSt_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_FastChargeSt_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BMS_FastChargeSt_GetPhysical(BMS_FastChargeSt * value);
Std_ReturnType PCOM_RX__BMS_FastChargeSt_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BMS_FastChargeSt_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_FastChargeSt_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_FastChargeSt_Set(const BMS_FastChargeSt * value);
IdtSignalDiagTime PCOM_RX__BMS_FastChargeSt_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_FastChargeSt_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_FastChargeSt_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_FastChargeSt_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_GetPhysical(BMS_CC2_connection_Status * value);
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BMS_CC2_connection_Status_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_CC2_connection_Status_Set(const BMS_CC2_connection_Status * value);
IdtSignalDiagTime PCOM_RX__BMS_CC2_connection_Status_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_CC2_connection_Status_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_CC2_connection_Status_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_CC2_connection_Status_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetInitial(uint16 * value);
Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetPhysical(BMS_DCRelayVoltage * value);
Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetDefaultSubstValue(uint16 * value);
boolean PCOM_RX__BMS_DC_RELAY_VOLTAGE_GetOutputSelector(void);
boolean PCOM_RX__BMS_DC_RELAY_VOLTAGE_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BMS_DC_RELAY_VOLTAGE_Set(const BMS_DCRelayVoltage * value);
IdtSignalDiagTime PCOM_RX__BMS_DC_RELAY_VOLTAGE_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_DC_RELAY_VOLTAGE_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BMS_DC_RELAY_VOLTAGE_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BMS_DC_RELAY_VOLTAGE_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BSI_MainWakeup_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BSI_MainWakeup_GetPhysical(BSI_MainWakeup * value);
Std_ReturnType PCOM_RX__BSI_MainWakeup_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BSI_MainWakeup_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BSI_MainWakeup_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BSI_MainWakeup_Set(const BSI_MainWakeup * value);
IdtSignalDiagTime PCOM_RX__BSI_MainWakeup_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_MainWakeup_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BSI_MainWakeup_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_MainWakeup_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BSI_LockedVehicle_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BSI_LockedVehicle_GetPhysical(BSI_LockedVehicle * value);
Std_ReturnType PCOM_RX__BSI_LockedVehicle_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BSI_LockedVehicle_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BSI_LockedVehicle_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BSI_LockedVehicle_Set(const BSI_LockedVehicle * value);
IdtSignalDiagTime PCOM_RX__BSI_LockedVehicle_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_LockedVehicle_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BSI_LockedVehicle_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_LockedVehicle_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_GetPhysical(BSI_ChargeTypeStatus * value);
Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__BSI_ChargeTypeStatus_GetOutputSelector(void);
boolean PCOM_RX__BSI_ChargeTypeStatus_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BSI_ChargeTypeStatus_Set(const BSI_ChargeTypeStatus * value);
IdtSignalDiagTime PCOM_RX__BSI_ChargeTypeStatus_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_ChargeTypeStatus_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BSI_ChargeTypeStatus_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_ChargeTypeStatus_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_GetInitial(BSI_PreDriveWakeup * value);
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_GetPhysical(BSI_PreDriveWakeup * value);
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_GetDefaultSubstValue(BSI_PreDriveWakeup * value);
boolean PCOM_RX__BSI_PreDriveWakeup_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BSI_PreDriveWakeup_Set(const BSI_PreDriveWakeup * value);
IdtSignalDiagTime PCOM_RX__BSI_PreDriveWakeup_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_PreDriveWakeup_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BSI_PreDriveWakeup_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_PreDriveWakeup_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_GetInitial(BSI_PostDriveWakeup * value);
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_GetPhysical(BSI_PostDriveWakeup * value);
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_GetDefaultSubstValue(BSI_PostDriveWakeup * value);
boolean PCOM_RX__BSI_PostDriveWakeup_GetOutputSelector(void);
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__BSI_PostDriveWakeup_Set(const BSI_PostDriveWakeup * value);
IdtSignalDiagTime PCOM_RX__BSI_PostDriveWakeup_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_PostDriveWakeup_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__BSI_PostDriveWakeup_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__BSI_PostDriveWakeup_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_GetInitial(BSI_VCUSynchroGPC * value);
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_GetPhysical(BSI_VCUSynchroGPC * value);
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_GetDefaultSubstValue(BSI_VCUSynchroGPC * value);
boolean PCOM_RX__VCU_SYNCHRO_GPC_GetOutputSelector(void);
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_SYNCHRO_GPC_Set(const BSI_VCUSynchroGPC * value);
IdtSignalDiagTime PCOM_RX__VCU_SYNCHRO_GPC_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_SYNCHRO_GPC_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_SYNCHRO_GPC_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_SYNCHRO_GPC_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetPhysical(BSI_VCUHeatPumpWorkingMode * value);
Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_GetOutputSelector(void);
boolean PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_Set(const BSI_VCUHeatPumpWorkingMode * value);
IdtSignalDiagTime PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_HEAT_PUMP_WORKING_MODE_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__CHARGE_STATE_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__CHARGE_STATE_GetPhysical(BSI_ChargeState * value);
Std_ReturnType PCOM_RX__CHARGE_STATE_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__CHARGE_STATE_GetOutputSelector(void);
Std_ReturnType PCOM_RX__CHARGE_STATE_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__CHARGE_STATE_Set(const BSI_ChargeState * value);
IdtSignalDiagTime PCOM_RX__CHARGE_STATE_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CHARGE_STATE_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__CHARGE_STATE_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CHARGE_STATE_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_GetPhysical(VCU_DCDCVoltageReq * value);
Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__VCU_DCDCVoltageReq_GetOutputSelector(void);
boolean PCOM_RX__VCU_DCDCVoltageReq_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_DCDCVoltageReq_Set(const VCU_DCDCVoltageReq * value);
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_DCDCVoltageReq_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_GetInitial(VCU_ActivedischargeCommand * value);
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_GetPhysical(VCU_ActivedischargeCommand * value);
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_GetDefaultSubstValue(VCU_ActivedischargeCommand * value);
boolean PCOM_RX__VCU_ActivedischargeCommand_GetOutputSelector(void);
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_ActivedischargeCommand_Set(const VCU_ActivedischargeCommand * value);
IdtSignalDiagTime PCOM_RX__VCU_ActivedischargeCommand_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_ActivedischargeCommand_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_ActivedischargeCommand_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_ActivedischargeCommand_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_DCDCActivation_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__VCU_DCDCActivation_GetPhysical(VCU_DCDCActivation * value);
Std_ReturnType PCOM_RX__VCU_DCDCActivation_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__VCU_DCDCActivation_GetOutputSelector(void);
Std_ReturnType PCOM_RX__VCU_DCDCActivation_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_DCDCActivation_Set(const VCU_DCDCActivation * value);
IdtSignalDiagTime PCOM_RX__VCU_DCDCActivation_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_DCDCActivation_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_DCDCActivation_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_DCDCActivation_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_GetInitial(DIAG_INTEGRA_ELEC * value);
Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_GetPhysical(DIAG_INTEGRA_ELEC * value);
Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__DIAG_INTEGRA_ELEC_Set(const DIAG_INTEGRA_ELEC * value);
IdtSignalDiagTime PCOM_RX__DIAG_INTEGRA_ELEC_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DIAG_INTEGRA_ELEC_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__DIAG_INTEGRA_ELEC_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DIAG_INTEGRA_ELEC_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_GetInitial(EFFAC_DEFAUT_DIAG * value);
Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_GetPhysical(EFFAC_DEFAUT_DIAG * value);
Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__EFFAC_DEFAUT_DIAG_Set(const EFFAC_DEFAUT_DIAG * value);
IdtSignalDiagTime PCOM_RX__EFFAC_DEFAUT_DIAG_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__EFFAC_DEFAUT_DIAG_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__EFFAC_DEFAUT_DIAG_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__EFFAC_DEFAUT_DIAG_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__MODE_DIAG_GetInitial(MODE_DIAG * value);
Std_ReturnType PCOM_RX__MODE_DIAG_GetPhysical(MODE_DIAG * value);
Std_ReturnType PCOM_RX__MODE_DIAG_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__MODE_DIAG_Set(const MODE_DIAG * value);
IdtSignalDiagTime PCOM_RX__MODE_DIAG_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__MODE_DIAG_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__MODE_DIAG_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__MODE_DIAG_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DATA_ACQ_JDD_BSI_2_GetInitial(DATA_ACQ_JDD_BSI_2 * value);
Std_ReturnType PCOM_RX__DATA_ACQ_JDD_BSI_2_GetPhysical(DATA_ACQ_JDD_BSI_2 * value);
Std_ReturnType PCOM_RX__DATA_ACQ_JDD_BSI_2_Set(const DATA_ACQ_JDD_BSI_2 * value);

Std_ReturnType PCOM_RX__VCU_EPWT_Status_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__VCU_EPWT_Status_GetPhysical(VCU_EPWT_Status * value);
Std_ReturnType PCOM_RX__VCU_EPWT_Status_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__VCU_EPWT_Status_GetOutputSelector(void);
Std_ReturnType PCOM_RX__VCU_EPWT_Status_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_EPWT_Status_Set(const VCU_EPWT_Status * value);
IdtSignalDiagTime PCOM_RX__VCU_EPWT_Status_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_EPWT_Status_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_EPWT_Status_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_EPWT_Status_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_GetPhysical(VCU_CompteurRazGct * value);
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__COMPTEUR_RAZ_GCT_GetOutputSelector(void);
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__COMPTEUR_RAZ_GCT_Set(const VCU_CompteurRazGct * value);
IdtSignalDiagTime PCOM_RX__COMPTEUR_RAZ_GCT_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__COMPTEUR_RAZ_GCT_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__COMPTEUR_RAZ_GCT_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__COMPTEUR_RAZ_GCT_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__CPT_TEMPOREL_GetInitial(uint32 * value);
Std_ReturnType PCOM_RX__CPT_TEMPOREL_GetPhysical(VCU_CptTemporel * value);
Std_ReturnType PCOM_RX__CPT_TEMPOREL_GetDefaultSubstValue(uint32 * value);
boolean PCOM_RX__CPT_TEMPOREL_GetOutputSelector(void);
Std_ReturnType PCOM_RX__CPT_TEMPOREL_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__CPT_TEMPOREL_Set(const VCU_CptTemporel * value);
IdtSignalDiagTime PCOM_RX__CPT_TEMPOREL_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CPT_TEMPOREL_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__CPT_TEMPOREL_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CPT_TEMPOREL_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__KILOMETRAGE_GetInitial(uint32 * value);
Std_ReturnType PCOM_RX__KILOMETRAGE_GetPhysical(VCU_Kilometrage * value);
Std_ReturnType PCOM_RX__KILOMETRAGE_GetDefaultSubstValue(uint32 * value);
boolean PCOM_RX__KILOMETRAGE_GetOutputSelector(void);
Std_ReturnType PCOM_RX__KILOMETRAGE_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__KILOMETRAGE_Set(const VCU_Kilometrage * value);
IdtSignalDiagTime PCOM_RX__KILOMETRAGE_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__KILOMETRAGE_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__KILOMETRAGE_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__KILOMETRAGE_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__POS_SHUNT_JDD_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_GetPhysical(VCU_PosShuntJDD * value);
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__POS_SHUNT_JDD_GetOutputSelector(void);
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__POS_SHUNT_JDD_Set(const VCU_PosShuntJDD * value);
IdtSignalDiagTime PCOM_RX__POS_SHUNT_JDD_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__POS_SHUNT_JDD_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__POS_SHUNT_JDD_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__POS_SHUNT_JDD_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__CDE_APC_JDD_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__CDE_APC_JDD_GetPhysical(VCU_CDEApcJDD * value);
Std_ReturnType PCOM_RX__CDE_APC_JDD_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__CDE_APC_JDD_GetOutputSelector(void);
Std_ReturnType PCOM_RX__CDE_APC_JDD_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__CDE_APC_JDD_Set(const VCU_CDEApcJDD * value);
IdtSignalDiagTime PCOM_RX__CDE_APC_JDD_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CDE_APC_JDD_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__CDE_APC_JDD_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CDE_APC_JDD_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__CDE_ACC_JDD_GetInitial(VCU_CDEAccJDD * value);
Std_ReturnType PCOM_RX__CDE_ACC_JDD_GetPhysical(VCU_CDEAccJDD * value);
Std_ReturnType PCOM_RX__CDE_ACC_JDD_GetDefaultSubstValue(VCU_CDEAccJDD * value);
boolean PCOM_RX__CDE_ACC_JDD_GetOutputSelector(void);
Std_ReturnType PCOM_RX__CDE_ACC_JDD_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__CDE_ACC_JDD_Set(const VCU_CDEAccJDD * value);
IdtSignalDiagTime PCOM_RX__CDE_ACC_JDD_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CDE_ACC_JDD_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__CDE_ACC_JDD_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__CDE_ACC_JDD_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_GetPhysical(VCU_EtatPrincipSev * value);
Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__ETAT_PRINCIP_SEV_GetOutputSelector(void);
boolean PCOM_RX__ETAT_PRINCIP_SEV_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__ETAT_PRINCIP_SEV_Set(const VCU_EtatPrincipSev * value);
IdtSignalDiagTime PCOM_RX__ETAT_PRINCIP_SEV_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ETAT_PRINCIP_SEV_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__ETAT_PRINCIP_SEV_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ETAT_PRINCIP_SEV_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_GetPhysical(VCU_EtatReseauElec * value);
Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__ETAT_RESEAU_ELEC_GetOutputSelector(void);
boolean PCOM_RX__ETAT_RESEAU_ELEC_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__ETAT_RESEAU_ELEC_Set(const VCU_EtatReseauElec * value);
IdtSignalDiagTime PCOM_RX__ETAT_RESEAU_ELEC_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ETAT_RESEAU_ELEC_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__ETAT_RESEAU_ELEC_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ETAT_RESEAU_ELEC_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__ETAT_GMP_HYB_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__ETAT_GMP_HYB_GetPhysical(VCU_EtatGmpHyb * value);
Std_ReturnType PCOM_RX__ETAT_GMP_HYB_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__ETAT_GMP_HYB_GetOutputSelector(void);
boolean PCOM_RX__ETAT_GMP_HYB_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__ETAT_GMP_HYB_Set(const VCU_EtatGmpHyb * value);
IdtSignalDiagTime PCOM_RX__ETAT_GMP_HYB_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ETAT_GMP_HYB_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__ETAT_GMP_HYB_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ETAT_GMP_HYB_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DDE_GMV_SEEM_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__DDE_GMV_SEEM_GetPhysical(VCU_DDEGMVSEEM * value);
Std_ReturnType PCOM_RX__DDE_GMV_SEEM_GetDefaultSubstValue(VCU_DDEGMVSEEM * value);
boolean PCOM_RX__DDE_GMV_SEEM_GetOutputSelector(void);
boolean PCOM_RX__DDE_GMV_SEEM_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__DDE_GMV_SEEM_Set(const VCU_DDEGMVSEEM * value);
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DDE_GMV_SEEM_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_GetPhysical(VCU_DMDMeap2SEEM * value);
Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__DMD_MEAP_2_SEEM_GetOutputSelector(void);
boolean PCOM_RX__DMD_MEAP_2_SEEM_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__DMD_MEAP_2_SEEM_Set(const VCU_DMDMeap2SEEM * value);
IdtSignalDiagTime PCOM_RX__DMD_MEAP_2_SEEM_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DMD_MEAP_2_SEEM_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__DMD_MEAP_2_SEEM_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DMD_MEAP_2_SEEM_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetInitial(VCU_StopDelayedHMIWakeup * value);
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetPhysical(VCU_StopDelayedHMIWakeup * value);
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetDefaultSubstValue(VCU_StopDelayedHMIWakeup * value);
boolean PCOM_RX__STOP_DELAYED_HMI_WAKEUP_GetOutputSelector(void);
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__STOP_DELAYED_HMI_WAKEUP_Set(const VCU_StopDelayedHMIWakeup * value);
IdtSignalDiagTime PCOM_RX__STOP_DELAYED_HMI_WAKEUP_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__STOP_DELAYED_HMI_WAKEUP_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__STOP_DELAYED_HMI_WAKEUP_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__STOP_DELAYED_HMI_WAKEUP_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_GetPhysical(VCU_ModeEPSRequest * value);
Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__MODE_EPS_REQUEST_GetOutputSelector(void);
boolean PCOM_RX__MODE_EPS_REQUEST_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__MODE_EPS_REQUEST_Set(const VCU_ModeEPSRequest * value);
IdtSignalDiagTime PCOM_RX__MODE_EPS_REQUEST_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__MODE_EPS_REQUEST_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__MODE_EPS_REQUEST_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__MODE_EPS_REQUEST_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_GetInitial(VCU_PrecondElecWakeup * value);
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_GetPhysical(VCU_PrecondElecWakeup * value);
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_GetDefaultSubstValue(VCU_PrecondElecWakeup * value);
boolean PCOM_RX__PRECOND_ELEC_WAKEUP_GetOutputSelector(void);
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__PRECOND_ELEC_WAKEUP_Set(const VCU_PrecondElecWakeup * value);
IdtSignalDiagTime PCOM_RX__PRECOND_ELEC_WAKEUP_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__PRECOND_ELEC_WAKEUP_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__PRECOND_ELEC_WAKEUP_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__PRECOND_ELEC_WAKEUP_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_GetInitial(VCU_DMDActivChiller * value);
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_GetPhysical(VCU_DMDActivChiller * value);
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_GetDefaultSubstValue(VCU_DMDActivChiller * value);
boolean PCOM_RX__DMD_ACTIV_CHILLER_GetOutputSelector(void);
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__DMD_ACTIV_CHILLER_Set(const VCU_DMDActivChiller * value);
IdtSignalDiagTime PCOM_RX__DMD_ACTIV_CHILLER_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DMD_ACTIV_CHILLER_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__DMD_ACTIV_CHILLER_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DMD_ACTIV_CHILLER_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_GetInitial(VCU_DiagMuxOnPwt * value);
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_GetPhysical(VCU_DiagMuxOnPwt * value);
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_GetDefaultSubstValue(VCU_DiagMuxOnPwt * value);
boolean PCOM_RX__DIAG_MUX_ON_PWT_GetOutputSelector(void);
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__DIAG_MUX_ON_PWT_Set(const VCU_DiagMuxOnPwt * value);
IdtSignalDiagTime PCOM_RX__DIAG_MUX_ON_PWT_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DIAG_MUX_ON_PWT_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__DIAG_MUX_ON_PWT_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__DIAG_MUX_ON_PWT_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__ABS_VehSpd_GetInitial(ABS_VehSpd * value);
Std_ReturnType PCOM_RX__ABS_VehSpd_GetPhysical(ABS_VehSpd * value);
Std_ReturnType PCOM_RX__ABS_VehSpd_GetDefaultSubstValue(ABS_VehSpd * value);
boolean PCOM_RX__ABS_VehSpd_GetOutputSelector(void);
Std_ReturnType PCOM_RX__ABS_VehSpd_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__ABS_VehSpd_Set(const ABS_VehSpd * value);
IdtSignalDiagTime PCOM_RX__ABS_VehSpd_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ABS_VehSpd_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__ABS_VehSpd_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ABS_VehSpd_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_GetInitial(ABS_VehSpdValidFlag * value);
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_GetPhysical(ABS_VehSpdValidFlag * value);
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_GetDefaultSubstValue(ABS_VehSpdValidFlag * value);
boolean PCOM_RX__ABS_VehSpdValidFlag_GetOutputSelector(void);
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__ABS_VehSpdValidFlag_Set(const ABS_VehSpdValidFlag * value);
IdtSignalDiagTime PCOM_RX__ABS_VehSpdValidFlag_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ABS_VehSpdValidFlag_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__ABS_VehSpdValidFlag_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ABS_VehSpdValidFlag_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_GetInitial(COUPURE_CONSO_CTPE2 * value);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_GetPhysical(COUPURE_CONSO_CTPE2 * value);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_GetDefaultSubstValue(COUPURE_CONSO_CTPE2 * value);
boolean PCOM_RX__COUPURE_CONSO_CTPE2_GetOutputSelector(void);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTPE2_Set(const COUPURE_CONSO_CTPE2 * value);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTPE2_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTPE2_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTPE2_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTPE2_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_GetInitial(COUPURE_CONSO_CTP * value);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_GetPhysical(COUPURE_CONSO_CTP * value);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_GetDefaultSubstValue(COUPURE_CONSO_CTP * value);
boolean PCOM_RX__COUPURE_CONSO_CTP_GetOutputSelector(void);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__COUPURE_CONSO_CTP_Set(const COUPURE_CONSO_CTP * value);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTP_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTP_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTP_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__COUPURE_CONSO_CTP_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_AccPedalPosition_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__VCU_AccPedalPosition_GetPhysical(VCU_AccPedalPosition * value);
Std_ReturnType PCOM_RX__VCU_AccPedalPosition_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__VCU_AccPedalPosition_GetOutputSelector(void);
boolean PCOM_RX__VCU_AccPedalPosition_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_AccPedalPosition_Set(const VCU_AccPedalPosition * value);
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_AccPedalPosition_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__VCU_Keyposition_GetInitial(uint8 * value);
Std_ReturnType PCOM_RX__VCU_Keyposition_GetPhysical(VCU_Keyposition * value);
Std_ReturnType PCOM_RX__VCU_Keyposition_GetDefaultSubstValue(uint8 * value);
boolean PCOM_RX__VCU_Keyposition_GetOutputSelector(void);
Std_ReturnType PCOM_RX__VCU_Keyposition_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__VCU_Keyposition_Set(const VCU_Keyposition * value);
IdtSignalDiagTime PCOM_RX__VCU_Keyposition_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_Keyposition_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__VCU_Keyposition_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__VCU_Keyposition_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_GetInitial(VCU_ElecMeterSaturation * value);
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_GetPhysical(VCU_ElecMeterSaturation * value);
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_GetDefaultSubstValue(VCU_ElecMeterSaturation * value);
boolean PCOM_RX__ELEC_METER_SATURATION_GetOutputSelector(void);
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_StubForbidden(pcom_gen_t * value);
Std_ReturnType PCOM_RX__ELEC_METER_SATURATION_Set(const VCU_ElecMeterSaturation * value);
IdtSignalDiagTime PCOM_RX__ELEC_METER_SATURATION_invalid_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ELEC_METER_SATURATION_invalid_timeHealed(void);
IdtSignalDiagTime PCOM_RX__ELEC_METER_SATURATION_forbidden_timeConfirm(void);
IdtSignalDiagTime PCOM_RX__ELEC_METER_SATURATION_forbidden_timeHealed(void);

Std_ReturnType PCOM_RX__DCHV_DCHVStatus_Set(const DCHV_DCHVStatus * value);
Std_ReturnType PCOM_RX__DCHV_DCHVStatus_GetPhysical(DCHV_DCHVStatus * value);

Std_ReturnType PCOM_RX__DCHV_ADC_VOUT_Set(const DCHV_ADC_VOUT * value);
Std_ReturnType PCOM_RX__DCHV_ADC_VOUT_GetPhysical(DCHV_ADC_VOUT * value);

Std_ReturnType PCOM_RX__DCHV_ADC_IOUT_Set(const DCHV_ADC_IOUT * value);
Std_ReturnType PCOM_RX__DCHV_ADC_IOUT_GetPhysical(DCHV_ADC_IOUT * value);

Std_ReturnType PCOM_RX__DCHV_Command_Current_Reference_Set(const DCHV_Command_Current_Reference * value);
Std_ReturnType PCOM_RX__DCHV_Command_Current_Reference_GetPhysical(DCHV_Command_Current_Reference * value);

Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_6_Set(const DCHV_ADC_NTC_MOD_6 * value);
Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_6_GetPhysical(DCHV_ADC_NTC_MOD_6 * value);

Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_5_Set(const DCHV_ADC_NTC_MOD_5 * value);
Std_ReturnType PCOM_RX__DCHV_ADC_NTC_MOD_5_GetPhysical(DCHV_ADC_NTC_MOD_5 * value);

Std_ReturnType PCOM_RX__DCLV_DCLVStatus_Set(const DCLV_DCLVStatus * value);
Std_ReturnType PCOM_RX__DCLV_DCLVStatus_GetPhysical(DCLV_DCLVStatus * value);

Std_ReturnType PCOM_RX__DCLV_Measured_Voltage_Set(const DCLV_Measured_Voltage * value);
Std_ReturnType PCOM_RX__DCLV_Measured_Voltage_GetPhysical(DCLV_Measured_Voltage * value);

Std_ReturnType PCOM_RX__DCLV_Measured_Current_Set(const DCLV_Measured_Current * value);
Std_ReturnType PCOM_RX__DCLV_Measured_Current_GetPhysical(DCLV_Measured_Current * value);

Std_ReturnType PCOM_RX__DCLV_Power_Set(const DCLV_Power * value);
Std_ReturnType PCOM_RX__DCLV_Power_GetPhysical(DCLV_Power * value);

Std_ReturnType PCOM_RX__DCLV_Input_Current_Set(const DCLV_Input_Current * value);
Std_ReturnType PCOM_RX__DCLV_Input_Current_GetPhysical(DCLV_Input_Current * value);

Std_ReturnType PCOM_RX__DCLV_Input_Voltage_Set(const DCLV_Input_Voltage * value);
Std_ReturnType PCOM_RX__DCLV_Input_Voltage_GetPhysical(DCLV_Input_Voltage * value);

Std_ReturnType PCOM_RX__DCLV_Applied_Derating_Factor_Set(const DCLV_Applied_Derating_Factor * value);
Std_ReturnType PCOM_RX__DCLV_Applied_Derating_Factor_GetPhysical(DCLV_Applied_Derating_Factor * value);


Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_A_Set(const DCLV_T_SW_BUCK_A * value);
Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_A_GetPhysical(DCLV_T_SW_BUCK_A * value);
Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_B_Set(const DCLV_T_SW_BUCK_B * value);
Std_ReturnType PCOM_RX__DCLV_T_SW_BUCK_B_GetPhysical(DCLV_T_SW_BUCK_B * value);
Std_ReturnType PCOM_RX__DCLV_T_PP_A_Set(const DCLV_T_PP_A * value);
Std_ReturnType PCOM_RX__DCLV_T_PP_A_GetPhysical(DCLV_T_PP_A * value);
Std_ReturnType PCOM_RX__DCLV_T_PP_B_Set(const DCLV_T_PP_B * value);
Std_ReturnType PCOM_RX__DCLV_T_PP_B_GetPhysical(DCLV_T_PP_B * value);
Std_ReturnType PCOM_RX__DCLV_T_L_BUCK_Set(const DCLV_T_L_BUCK * value);
Std_ReturnType PCOM_RX__DCLV_T_L_BUCK_GetPhysical(DCLV_T_L_BUCK * value);
Std_ReturnType PCOM_RX__DCLV_T_TX_PP_Set(const DCLV_T_TX_PP * value);
Std_ReturnType PCOM_RX__DCLV_T_TX_PP_GetPhysical(DCLV_T_TX_PP * value);


Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_IOUT_Set(const DCHV_IOM_ERR_OC_IOUT * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_IOUT_GetPhysical(DCHV_IOM_ERR_OC_IOUT * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OV_VOUT_Set(const DCHV_IOM_ERR_OV_VOUT * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OV_VOUT_GetPhysical(DCHV_IOM_ERR_OV_VOUT * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_Set(const DCHV_IOM_ERR_OT * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_GetPhysical(DCHV_IOM_ERR_OT * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD5_Set(const DCHV_IOM_ERR_OT_NTC_MOD5 * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD5_GetPhysical(DCHV_IOM_ERR_OT_NTC_MOD5 * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD6_Set(const DCHV_IOM_ERR_OT_NTC_MOD6 * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_NTC_MOD6_GetPhysical(DCHV_IOM_ERR_OT_NTC_MOD6 * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_UV_12V_Set(const DCHV_IOM_ERR_UV_12V * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_UV_12V_GetPhysical(DCHV_IOM_ERR_UV_12V * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_POS_Set(const DCHV_IOM_ERR_OC_POS * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_POS_GetPhysical(DCHV_IOM_ERR_OC_POS * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_NEG_Set(const DCHV_IOM_ERR_OC_NEG * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OC_NEG_GetPhysical(DCHV_IOM_ERR_OC_NEG * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_H_Set(const DCHV_IOM_ERR_CAP_FAIL_H * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_H_GetPhysical(DCHV_IOM_ERR_CAP_FAIL_H * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_L_Set(const DCHV_IOM_ERR_CAP_FAIL_L * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_CAP_FAIL_L_GetPhysical(DCHV_IOM_ERR_CAP_FAIL_L * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_IN_Set(const DCHV_IOM_ERR_OT_IN * value);
Std_ReturnType PCOM_RX__DCHV_IOM_ERR_OT_IN_GetPhysical(DCHV_IOM_ERR_OT_IN * value);

Std_ReturnType PCOM_RX__PFC_PFCStatus_Set(const PFC_PFCStatus * value);
Std_ReturnType PCOM_RX__PFC_PFCStatus_GetPhysical(PFC_PFCStatus * value);
Std_ReturnType PCOM_RX__PFC_Vdclink_V_Set(const PFC_Vdclink_V * value);
Std_ReturnType PCOM_RX__PFC_Vdclink_V_GetPhysical(PFC_Vdclink_V * value);
Std_ReturnType PCOM_RX__PFC_Temp_M1_C_Set(const PFC_Temp_M1_C * value);
Std_ReturnType PCOM_RX__PFC_Temp_M1_C_GetPhysical(PFC_Temp_M1_C * value);
Std_ReturnType PCOM_RX__PFC_Temp_M3_C_Set(const PFC_Temp_M3_C * value);
Std_ReturnType PCOM_RX__PFC_Temp_M3_C_GetPhysical(PFC_Temp_M3_C * value);
Std_ReturnType PCOM_RX__PFC_Temp_M4_C_Set(const PFC_Temp_M4_C * value);
Std_ReturnType PCOM_RX__PFC_Temp_M4_C_GetPhysical(PFC_Temp_M4_C * value);

Std_ReturnType PCOM_RX__PFC_VPH12_Peak_V_Set(const PFC_VPH12_Peak_V * value);
Std_ReturnType PCOM_RX__PFC_VPH12_Peak_V_GetPhysical(PFC_VPH12_Peak_V * value);
Std_ReturnType PCOM_RX__PFC_VPH23_Peak_V_Set(const PFC_VPH23_Peak_V * value);
Std_ReturnType PCOM_RX__PFC_VPH23_Peak_V_GetPhysical(PFC_VPH23_Peak_V * value);
Std_ReturnType PCOM_RX__PFC_VPH31_Peak_V_Set(const PFC_VPH31_Peak_V * value);
Std_ReturnType PCOM_RX__PFC_VPH31_Peak_V_GetPhysical(PFC_VPH31_Peak_V * value);

Std_ReturnType PCOM_RX__PFC_VPH12_RMS_V_Set(const PFC_VPH12_RMS_V * value);
Std_ReturnType PCOM_RX__PFC_VPH12_RMS_V_GetPhysical(PFC_VPH12_RMS_V * value);
Std_ReturnType PCOM_RX__PFC_VPH23_RMS_V_Set(const PFC_VPH23_RMS_V * value);
Std_ReturnType PCOM_RX__PFC_VPH23_RMS_V_GetPhysical(PFC_VPH23_RMS_V * value);
Std_ReturnType PCOM_RX__PFC_VPH31_RMS_V_Set(const PFC_VPH31_RMS_V * value);
Std_ReturnType PCOM_RX__PFC_VPH31_RMS_V_GetPhysical(PFC_VPH31_RMS_V * value);
Std_ReturnType PCOM_RX__PFC_IPH1_RMS_0A1_Set(const PFC_IPH1_RMS_0A1 * value);
Std_ReturnType PCOM_RX__PFC_IPH1_RMS_0A1_GetPhysical(PFC_IPH1_RMS_0A1 * value);
Std_ReturnType PCOM_RX__PFC_IPH2_RMS_0A1_Set(const PFC_IPH2_RMS_0A1 * value);
Std_ReturnType PCOM_RX__PFC_IPH2_RMS_0A1_GetPhysical(PFC_IPH2_RMS_0A1 * value);
Std_ReturnType PCOM_RX__PFC_IPH3_RMS_0A1_Set(const PFC_IPH3_RMS_0A1 * value);
Std_ReturnType PCOM_RX__PFC_IPH3_RMS_0A1_GetPhysical(PFC_IPH3_RMS_0A1 * value);

Std_ReturnType PCOM_RX__PFC_VPH1_Freq_Hz_Set(const PFC_VPH1_Freq_Hz * value);
Std_ReturnType PCOM_RX__PFC_VPH1_Freq_Hz_GetPhysical(PFC_VPH1_Freq_Hz * value);
Std_ReturnType PCOM_RX__PFC_VPH2_Freq_Hz_Set(const PFC_VPH2_Freq_Hz * value);
Std_ReturnType PCOM_RX__PFC_VPH2_Freq_Hz_GetPhysical(PFC_VPH2_Freq_Hz * value);
Std_ReturnType PCOM_RX__PFC_VPH3_Freq_Hz_Set(const PFC_VPH3_Freq_Hz * value);
Std_ReturnType PCOM_RX__PFC_VPH3_Freq_Hz_GetPhysical(PFC_VPH3_Freq_Hz * value);

Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO7_Set(const PFC_IOM_ERR_UVLO_ISO7 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO7_GetPhysical(PFC_IOM_ERR_UVLO_ISO7 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UV_12V_Set(const PFC_IOM_ERR_UV_12V * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UV_12V_GetPhysical(PFC_IOM_ERR_UV_12V * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO4_Set(const PFC_IOM_ERR_UVLO_ISO4 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_UVLO_ISO4_GetPhysical(PFC_IOM_ERR_UVLO_ISO4 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M4_Set(const PFC_IOM_ERR_OT_NTC1_M4 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M4_GetPhysical(PFC_IOM_ERR_OT_NTC1_M4 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M3_Set(const PFC_IOM_ERR_OT_NTC1_M3 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M3_GetPhysical(PFC_IOM_ERR_OT_NTC1_M3 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M1_Set(const PFC_IOM_ERR_OT_NTC1_M1 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OT_NTC1_M1_GetPhysical(PFC_IOM_ERR_OT_NTC1_M1 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_DCLINK_Set(const PFC_IOM_ERR_OV_DCLINK * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_DCLINK_GetPhysical(PFC_IOM_ERR_OV_DCLINK * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH12_Set(const PFC_IOM_ERR_OV_VPH12 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH12_GetPhysical(PFC_IOM_ERR_OV_VPH12 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH31_Set(const PFC_IOM_ERR_OV_VPH31 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH31_GetPhysical(PFC_IOM_ERR_OV_VPH31 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH23_Set(const PFC_IOM_ERR_OV_VPH23 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OV_VPH23_GetPhysical(PFC_IOM_ERR_OV_VPH23 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH3_Set(const PFC_IOM_ERR_OC_PH3 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH3_GetPhysical(PFC_IOM_ERR_OC_PH3 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH2_Set(const PFC_IOM_ERR_OC_PH2 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH2_GetPhysical(PFC_IOM_ERR_OC_PH2 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH1_Set(const PFC_IOM_ERR_OC_PH1 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_PH1_GetPhysical(PFC_IOM_ERR_OC_PH1 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT1_Set(const PFC_IOM_ERR_OC_SHUNT1 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT1_GetPhysical(PFC_IOM_ERR_OC_SHUNT1 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT2_Set(const PFC_IOM_ERR_OC_SHUNT2 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT2_GetPhysical(PFC_IOM_ERR_OC_SHUNT2 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT3_Set(const PFC_IOM_ERR_OC_SHUNT3 * value);
Std_ReturnType PCOM_RX__PFC_IOM_ERR_OC_SHUNT3_GetPhysical(PFC_IOM_ERR_OC_SHUNT3 * value);

Std_ReturnType PCOM_RX__DCLV_PWM_STOP_Set(const DCLV_PWM_STOP * value);
Std_ReturnType PCOM_RX__DCLV_PWM_STOP_GetPhysical(DCLV_PWM_STOP * value);

Std_ReturnType PCOM_RX__DCLV_OT_FAULT_Set(const DCLV_OT_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OT_FAULT_GetPhysical(DCLV_OT_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OC_A_HV_FAULT_Set(const DCLV_OC_A_HV_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OC_A_HV_FAULT_GetPhysical(DCLV_OC_A_HV_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OC_B_HV_FAULT_Set(const DCLV_OC_B_HV_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OC_B_HV_FAULT_GetPhysical(DCLV_OC_B_HV_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OV_INT_A_FAULT_Set(const DCLV_OV_INT_A_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OV_INT_A_FAULT_GetPhysical(DCLV_OV_INT_A_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OV_INT_B_FAULT_Set(const DCLV_OV_INT_B_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OV_INT_B_FAULT_GetPhysical(DCLV_OV_INT_B_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OV_UV_HV_FAULT_Set(const DCLV_OV_UV_HV_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OV_UV_HV_FAULT_GetPhysical(DCLV_OV_UV_HV_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OC_A_LV_FAULT_Set(const DCLV_OC_A_LV_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OC_A_LV_FAULT_GetPhysical(DCLV_OC_A_LV_FAULT * value);

Std_ReturnType PCOM_RX__DCLV_OC_B_LV_FAULT_Set(const DCLV_OC_B_LV_FAULT * value);
Std_ReturnType PCOM_RX__DCLV_OC_B_LV_FAULT_GetPhysical(DCLV_OC_B_LV_FAULT * value);


#endif


/* EOF */
