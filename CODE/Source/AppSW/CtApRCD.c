/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApRCD.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApRCD
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApRCD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup RCD
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief RCD input management.
 * \details This module shall filter and report the RCD signal state.
 * The filter implemented uses a debounce algorithm with two different
 * debounce specifications (one for the activation and other for the
 * deactivation).
 *
 * \{
 * \file  RCD.c
 * \brief  Generic code of the RCD module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApRCD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApRCD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*!
 * \brief RCD Enable.
 * \details This signal enables the module. If this signal is disable, the RDC
 * reported value is FALSE.
 */
static boolean RCD_CtrlFlowRCDLine;
/*!
 * \brief Rising edge debounce filter.
 * \details Time used for the rising debounce filter.
 * \note Units: deciseconds.
 */
static uint8 RCD_DebounceRCDHigh;
/*!
 * \brief Falling edge debounce filter.
 * \details Time used for the falling debounce filter.
 * \note Units: deciseconds.
 */
static uint8 RCD_DebounceRCDLow;

#define CtApRCD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApRCD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApRCD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApRCD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApRCD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

 /* PRQA S 0857,0380 -- # Max amount of macros check */
/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/** Update calibratables*/
static void RCD_UpdateCalibratables(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDebounceRCD: Integer in interval [0...16]
 *   Unit: [ms], Factor: 10, Offset: 0
 * SUPV_RCDLineState: Boolean
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDebounceRCD Rte_CData_CalDebounceRCDHigh(void)
 *   IdtDebounceRCD Rte_CData_CalDebounceRCDLow(void)
 *   boolean Rte_CData_CalCtrlFlowRCDLine(void)
 *
 *********************************************************************************************************************/


#define CtApRCD_START_SEC_CODE
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApRCD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRCD_init_doc
 *********************************************************************************************************************/

/*!
 * \brief RCD initialization function.
 * \details This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApRCD_CODE) RCtApRCD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRCD_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApRCD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRCD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRCD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	RCD_CtrlFlowRCDLine=Rte_CData_CalCtrlFlowRCDLine();
	RCD_DebounceRCDHigh=(uint8)Rte_CData_CalDebounceRCDHigh();
	RCD_DebounceRCDLow=(uint8)Rte_CData_CalDebounceRCDLow();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApRCD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRCD_task10ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief RCD task.
  *
  * This function is called periodically by the scheduler. This function should
  * report the RDC line state.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApRCD_CODE) RCtApRCD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApRCD_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApRCD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRCD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 RCD_CounterRCDActive = 0U;
	static uint8 RCD_CounterRCDNotActive = 0U;
	static boolean RCD_InputRCDFiltered = FALSE;

	#define CtApRCD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRCD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RCD_InputRCDRaw;


	RCD_UpdateCalibratables();

	(void)Rte_Read_PpExtRCDLineRaw_DeExtRCDLineRaw(&RCD_InputRCDRaw);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (TRUE == RCD_CtrlFlowRCDLine)
	  {
	    if ( TRUE == RCD_InputRCDRaw)
	    {
	      if ( RCD_CounterRCDActive < RCD_DebounceRCDHigh)
	      {
	        RCD_CounterRCDActive++;
	      }
	      else
	      {
	    	  RCD_InputRCDFiltered = TRUE;
	      }
	      RCD_CounterRCDNotActive = 0U;
	    }
	    else
	    {
	      if ( RCD_CounterRCDNotActive < RCD_DebounceRCDLow)
	      {
	        RCD_CounterRCDNotActive++;
	      }
	      else
	      {
	        RCD_InputRCDFiltered = FALSE;
	      }
	      RCD_CounterRCDActive = 0U;
	    }
	  }
	  else
	  {
	    /* Default value */
	    RCD_InputRCDFiltered = FALSE;
	    RCD_CounterRCDActive = 0U;
	    RCD_CounterRCDNotActive = 0U;
	  }

	(void)Rte_Write_PpInt_SUPV_RCDLineState_SUPV_RCDLineState((SUPV_RCDLineState)RCD_InputRCDFiltered);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApRCD_STOP_SEC_CODE
#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/** Update calibratables*/
static void RCD_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApRCD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApRCD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApRCD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApRCD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApRCD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	RCD_CtrlFlowRCDLine=Rte_CData_CalCtrlFlowRCDLine();
	RCD_DebounceRCDHigh=(uint8)Rte_CData_CalDebounceRCDHigh();
	RCD_DebounceRCDLow=(uint8)Rte_CData_CalDebounceRCDLow();
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
