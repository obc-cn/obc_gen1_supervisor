/**************************************************************************//**
 * \file	ctApJDD_cfg.h
 * \author	Juan Carlos Romanillos <juancarlos.romanillos(at)es.mahle.com
 * \date	11 dic. 2019
 * \copyright	MAHLE ELECTRONICS - 2019
 * \brief	Configuration file for JDD comunications
 *****************************************************************************/

/**
 * \defgroup ctApJDD_cfg Module description
 * \{
 */

#ifndef APPSW_CTAPJDD_CFG_H_
#define APPSW_CTAPJDD_CFG_H_

/******************************************************************************
 * PUBLIC CONSTANT DEFINITIONS                                               
 *****************************************************************************/

#define JDD_NUMBER_OF_DTCS			(69U) 					/*!< Total number of DTCs */

typedef uint32 SITUATION_VIE_JDD;
typedef uint32 KILOMETRAGE_JDD;
typedef uint32 CODES_DEFAUT;

/******************************************************************************
 * PUBLIC TYPEDEFS                                                           
 *****************************************************************************/
typedef VCU_CptTemporel jdd_timing_reference_t;
typedef uint8 jdd_dtc_id;

typedef enum
{
  JDD_STATE_NO_RAISED =0,
  JDD_STATE_RAISED
}jdd_event_state_t;

typedef enum
{
  JDD_DTC_STATE_IDLE =0,
  JDD_DTC_STATE_FAILING,
  JDD_DTC_STATE_PASSING
}jdd_dtc_state_t;

typedef enum
{
  JDD_COMM_IDLE =0,
  JDD_COMM_SEND_FRAME1,
  JDD_COMM_WAIT,
  JDD_COMM_SEND_FRAME2,
  JDD_COMM_WAIT_ACK,
  JDD_COMM_WAIT_EXCLUSION_TIME,
  JDD_COM_SEND_COMPLETED,
  JDD_COM_SEND_FAILED,
  JDD_COM_SEND_END,
  JDD_COM_DISCARD_FROMQUEUE,
  JDD_COM_FINAL_WAIT,
  JDD_COM_SLEEP
}jdd_comm_state_t;

typedef boolean (*isFailing_t)(void);

typedef struct
{
  jdd_dtc_state_t globalState;
  boolean enqued;
  struct
  {
    jdd_event_state_t state;
    jdd_timing_reference_t referenceHorarie;
    VCU_Kilometrage kilometrage;
    SITUATION_VIE_JDD situation;
  }confirmation;
  struct
  {
    jdd_event_state_t state;
    jdd_timing_reference_t referenceHorarie;
    KILOMETRAGE_JDD kilometrage;
    SITUATION_VIE_JDD situation;
  }healing;
}JDD_dtc_data_t;

typedef struct
{
  jdd_dtc_id nvmIndex;
  CODES_DEFAUT code;
  isFailing_t isFailing;
  JDD_dtc_data_t *data;
}JDD_dtc_config_t;

typedef struct
{
  jdd_dtc_state_t  failureSatus;
  jdd_comm_state_t state;
  uint16 timeout;
  uint8 retries;
  uint8 exclusionRetries;
  const JDD_dtc_config_t 	*sending;
} JDD_comm_status_t;

/******************************************************************************
 * PUBLIC VARIABLE DECLARATIONS                                              
 *****************************************************************************/
 /* - Static non-init variables declaration ---------- */
#define CtApJDD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern  JDD_dtc_data_t jdd_dtc_data[JDD_NUMBER_OF_DTCS];

#define CtApJDD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApJDD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApJDD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApJDD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApJDD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


extern const JDD_dtc_config_t 	jdd_dtc_configs [JDD_NUMBER_OF_DTCS];

/******************************************************************************
 * PUBLIC FUNCTION PROTOTYPES                                                
 *****************************************************************************/
#endif

/**\}*/

/* EOF */
