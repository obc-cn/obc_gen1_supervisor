/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLSD.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApLSD
 *  Generation Time:  2020-11-23 11:42:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLSD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup LSD
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Lock sensor diagnostic.
 * \details This module implements reports the state for the E-lock and if a
 * fault has been detected.
 *
 * \{
 * \file  LSD.c
 * \brief  Generic code of the LSD module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApLSD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/*! Signal not available for U8.*/
#define LSD_SNA_U8 						0xFF
/*! Time conversion gain from DFA to task period.*/
#define LSD_TIME_CONVERSION		5U
/*! Undefined zone value.*/
#define LSD_UNDEFINED_ZONE		LSD_DRIVE
/** Nominal Battery voltage.*/
#define LSD_NOMINAL_BATTERY_VOLT 120U
/** Battery compensation gain: 0.994*/
#define LSD_BATTERY_GAIN_HIGH			63295U
/** Battery compensation gain: 0.7*/
#define LSD_BATTERY_GAIN_LOW			45875U
/** Battery compensation gain Q factor*/
#define LSD_BATTERY_GAIN_Q		16


/* PRQA S 0857 -- #Max number of macros avoidance*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/

/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                              				                    */
/*---------------------------------------------------------------------------*/

/*! Reported states.*/
typedef enum{
	/*! Lock state.*/
	LSD_LOCK =  	0x00,
	/*! Unlock state.*/
	LSD_UNLOCK =  	0x01,
	/*! Drive state.*/
	LSD_DRIVE = 	0x02,
	/*! Number of zones.*/
	LSD_ZONES = 	3U
}LSD_zone_enum_t;

/*! Faults enumeration.*/
typedef enum{
	/*! Short circuit to positive.*/
	LSD_SCP,
	/*! Short circuit to ground.*/
	LSD_SCG,
	/*! Number of faults.*/
	LSD_NUM_FAULTS
}LSD_fault_enum_t;

/*! Debounce time specifications.*/
typedef enum{
	/*! Deactivation debounce time.*/
	LSD_DEACTIVATION = 0U, /*from true to false*/
	/*! Activation debounce time.*/
	LSD_ACTIVATION = 1U,  /*from false to true*/
	/*! Number of transitions.*/
	LSD_NUM_TRANSITION = 2U
}LSD_transition_t;
/*PRQA S 3205 --*/

/*! Zone boundary*/
typedef struct{
	/*! Threshold high.*/
	uint8 threshold_high;
	/*! Threshold low.*/
	uint8 threshold_low;
}LSD_zone_t;

/*! Debounce object.*/
typedef struct{
	/*! Output debounced.*/
	boolean output;
	/*! Time counter.*/
	uint16 counter;
	/*! Time configuration.*/
	uint16 counter_max[LSD_NUM_TRANSITION];
}LSD_debounce_t;

/*! Fault object.*/
typedef struct{
	/*! Prefault state.*/
	boolean prefault;
	/*! Fault threshold.*/
	uint8 threshold;
	/*! Debounce object.*/
	LSD_debounce_t debounce;
}LSD_fault_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*! Fault variables.*/
static LSD_fault_t LSD_fault[LSD_NUM_FAULTS];
/*! Zone parameters.*/
static LSD_zone_t LSD_zone[LSD_ZONES];
/*! E-lock sensor raw value.*/
static uint8 LSD_rawValue_in;
/*! LSD module enable.*/
static boolean LSD_enable;
/*! Battery state.*/
static uint8 LSD_battery_state;

#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/*!
 * \brief SCP Fault detection.
 * \details This functions detects if the input value is above
 * the short circuit positive threshold.
 * \param[in] LSD_batery_volt Battery Voltage
 */
static void LSD_PreFault_detection_SCP(uint8 LSD_batery_volt);
/*!
 * \brief SCG Fault detection.
 * \details This functions detects if the input value is below
 * the short circuit ground threshold.
 */
static void LSD_PreFault_detection_SCG(void);
/*!
 * \brief Debounce function.
 * \details This function will perform a debounce for the LSD_input.
 * \param[in] LSD_fault_index Fault to be debounced.
 */
static void LSD_fault_debounce(LSD_fault_enum_t LSD_fault_index);

/** Init sub-function to reduce halstead.*/
static void Init_conversion(void);
/**
 * \brief Identify state.
 * \details This function identifies the current E-lock state.
 * \param[in] LSD_battery_volt Battery voltage.
 * \return E-Lock state.
 */
static LSD_zone_enum_t LSD_IdentifyState(uint8 LSD_battery_volt);
/**
 * \brief Compensate threshold.
 * \details This function compensates the threshold taking into account the
 * battery voltage level.
 */
static uint8 LSD_CompensateThreshold(uint8 LDS_ThresholdIN, uint8 LSD_battery_volt, uint16 LSD_compensationGain);
/** Update calibratables*/
static void LSD_UpdateCalibratables(void);
/** Debounce E-lock state*/
static uint8 LSD_StateDebounce(uint8 Input);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtELockDebounce: Integer in interval [0...255]
 *   Unit: [ms], Factor: 10, Offset: 0
 * IdtELockSensorFaultThreshold: Integer in interval [0...180]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtELockSensorFaultTime: Integer in interval [0...20]
 *   Unit: [ms], Factor: 50, Offset: 0
 * IdtELockSensorLimit: Integer in interval [0...180]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtExtOpPlugLockRaw: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * OBC_ElockState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Elock_unlocked (0U)
 *   Cx1_Elock_locked (1U)
 *   Cx2_E_lock_lock_fail (2U)
 *   Cx3_E_Lock_unlock_fail (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimElockCounterSCG(void)
 *   uint16 *Rte_Pim_PimElockCounterSCP(void)
 *   boolean *Rte_Pim_PimElockPrefaultSCG(void)
 *   boolean *Rte_Pim_PimElockPrefaultSCP(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtELockDebounce Rte_CData_CalELockDebounce(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorDriveHigh(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorDriveLow(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorLockHigh(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorLockLow(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorUnlockHigh(void)
 *   IdtELockSensorLimit Rte_CData_CalELockSensorUnlockLow(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCGConfirmTime(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCGHealTime(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCPConfirmTime(void)
 *   IdtELockSensorFaultTime Rte_CData_CalElockSensorSCPHealTime(void)
 *   IdtELockSensorFaultThreshold Rte_CData_CalElockSensorThresholdSCG(void)
 *   IdtELockSensorFaultThreshold Rte_CData_CalElockSensorThresholdSCP(void)
 *   boolean Rte_CData_CalCtrlFlowPlugLock(void)
 *
 *********************************************************************************************************************/


#define CtApLSD_START_SEC_CODE
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLSD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_init_doc
 *********************************************************************************************************************/

/*!
 * \brief LSD initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLSD_CODE) RCtApLSD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	LSD_enable = Rte_CData_CalCtrlFlowPlugLock();

	LSD_zone[LSD_LOCK].threshold_low =  Rte_CData_CalELockSensorLockLow();
	LSD_zone[LSD_LOCK].threshold_high =  Rte_CData_CalELockSensorLockHigh();
	LSD_zone[LSD_UNLOCK].threshold_low =  Rte_CData_CalELockSensorUnlockLow();
	LSD_zone[LSD_UNLOCK].threshold_high =  Rte_CData_CalELockSensorUnlockHigh();
	LSD_zone[LSD_DRIVE].threshold_low =  Rte_CData_CalELockSensorDriveLow();
	LSD_zone[LSD_DRIVE].threshold_high =  Rte_CData_CalELockSensorDriveHigh();

	LSD_fault[LSD_SCP].threshold =  Rte_CData_CalElockSensorThresholdSCP();
	LSD_fault[LSD_SCG].threshold =  Rte_CData_CalElockSensorThresholdSCG();

	LSD_fault[LSD_SCP].debounce.counter_max[LSD_ACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCPConfirmTime();
	LSD_fault[LSD_SCG].debounce.counter_max[LSD_ACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCGConfirmTime();
	LSD_fault[LSD_SCP].debounce.counter_max[LSD_DEACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCPHealTime();
	LSD_fault[LSD_SCG].debounce.counter_max[LSD_DEACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCGHealTime();


	Init_conversion();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLSD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_NormalMode_ToBeDeleted_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(IdtExtOpPlugLockRaw *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(OBC_ElockState data)
 *   Std_ReturnType Rte_Write_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief LSD task.
 *
 * This function is called periodically by the scheduler. This function
 * determines the state of the E-lock and if a fault is present.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLSD_CODE) RCtApLSD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLSD_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Inputs*/
	uint8 LSD_battery_volt;
	IdtAppRCDECUState ECUstate;

	/*Outputs*/
	uint8 LSD_out_state;
	boolean LSD_monitoring_conditions = FALSE;


	LSD_UpdateCalibratables();

	(void)Rte_Read_PpExtOpPlugLockRaw_DeExtOpPlugLockRaw(&LSD_rawValue_in);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&ECUstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&LSD_battery_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&LSD_battery_volt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((uint8)LSD_SNA_U8 != LSD_rawValue_in)
	{
		if(TRUE==LSD_enable)
		{

			/* Monitoring enable conditions */
			if (LSD_battery_state == BAT_VALID_RANGE)
			{
				LSD_monitoring_conditions = TRUE;
			}

			/*E-lock State + Debounce*/
			LSD_out_state=(uint8)LSD_IdentifyState(LSD_battery_volt);
			LSD_out_state = LSD_StateDebounce(LSD_out_state);

			/*PreFault*/
			LSD_PreFault_detection_SCP(LSD_battery_volt);
			LSD_PreFault_detection_SCG();

			/*Debounce*/
			LSD_fault_debounce(LSD_SCP);
			LSD_fault_debounce(LSD_SCG);

			/*Publish output*/
			if((APP_STATE_1 == ECUstate)
			|| (APP_STATE_4== ECUstate)
			|| (APP_STATE_5== ECUstate))
			{

				/*Workaround E-Lock state report.*/
				OBC_ElockState E_lock_position = Cx0_Elock_unlocked;
				if(LSD_out_state == (uint8)LSD_LOCK)
				{
					E_lock_position = Cx1_Elock_locked;
				}
				else if(LSD_out_state == (uint8)LSD_UNLOCK)
				{
					E_lock_position = Cx0_Elock_unlocked;
				}
				else
				{
					/*MISRA*/
				}


				(void)Rte_Write_PpInt_OBC_ElockState_ToBeDeleted_OBC_ElockState(E_lock_position);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(LSD_fault[LSD_SCG].debounce.output);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(LSD_fault[LSD_SCP].debounce.output);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpOutputELockSensor_DeOutputELockSensor((IdtOutputELockSensor)LSD_out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

			}
		}
		else
		{
			/*Module disable*/
			(void)Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCG(FALSE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			(void)Rte_Write_PpELockSensorFaults_DeELockSensorFaultSCP(FALSE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			(void)Rte_Write_PpOutputELockSensor_DeOutputELockSensor((IdtOutputELockSensor)LSD_UNDEFINED_ZONE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		}
	}

	(void)Rte_Write_PpElockSensorMonitoringConditions_DeElockSensorMonitoringConditions(LSD_monitoring_conditions);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	*Rte_Pim_PimElockCounterSCG() = (uint16)LSD_fault[LSD_SCG].debounce.counter;
	*Rte_Pim_PimElockCounterSCP() = (uint16)LSD_fault[LSD_SCP].debounce.counter;

	*Rte_Pim_PimElockPrefaultSCG() = LSD_fault[LSD_SCG].prefault;
	*Rte_Pim_PimElockPrefaultSCP() = LSD_fault[LSD_SCP].prefault;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLSD_STOP_SEC_CODE
#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static void LSD_PreFault_detection_SCP(uint8 LSD_batery_volt)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 Threshold_SCP_compensate;


	Threshold_SCP_compensate = (uint16)LSD_fault[LSD_SCP].threshold + (uint16)LSD_batery_volt;

	if((uint16)LSD_NOMINAL_BATTERY_VOLT>Threshold_SCP_compensate)
	{
		/*Avoid Underflow*/
		Threshold_SCP_compensate = 0U;
	}
	else
	{
		Threshold_SCP_compensate -= (uint16)LSD_NOMINAL_BATTERY_VOLT;
	}

	/*Activation condition*/
	if(			(LSD_fault[LSD_SCP].prefault==FALSE)
			&& ((uint16)LSD_rawValue_in > Threshold_SCP_compensate)
			&& (LSD_battery_state == (uint8)BAT_VALID_RANGE))
	{
		LSD_fault[LSD_SCP].prefault = TRUE;
	}
	/*Deactivation condition*/
	else if (		(LSD_fault[LSD_SCP].prefault==TRUE)
				&& ((uint16)LSD_rawValue_in <= Threshold_SCP_compensate))
	{
		LSD_fault[LSD_SCP].prefault = FALSE;
	}
	else
	{
		/*MISRA*/
	}
}
static void LSD_PreFault_detection_SCG(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Activation condition*/
	if(			(LSD_fault[LSD_SCG].prefault==FALSE)
			&& (LSD_rawValue_in < LSD_fault[LSD_SCG].threshold)
			&& (LSD_battery_state == (uint8)BAT_VALID_RANGE))
	{
		LSD_fault[LSD_SCG].prefault = TRUE;
	}
	/*Deactivation condition*/
	else if (		(LSD_fault[LSD_SCG].prefault==TRUE)
				&& (LSD_rawValue_in >= LSD_fault[LSD_SCG].threshold))
	{
		LSD_fault[LSD_SCG].prefault = FALSE;
	}
	else
	{
		/*MISRA*/
	}
}

static void LSD_fault_debounce(LSD_fault_enum_t LSD_fault_index)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* The input state is where the functions wants to go. Therefore, depending on that state
	 * the function should use a specific debounce time.*/
	uint16 LSD_counter_max;


	if(FALSE == LSD_fault[LSD_fault_index].prefault)
	{
		LSD_counter_max = LSD_fault[LSD_fault_index].debounce.counter_max[LSD_DEACTIVATION];
	}
	else
	{
		LSD_counter_max = LSD_fault[LSD_fault_index].debounce.counter_max[LSD_ACTIVATION];
	}


	if(LSD_fault[LSD_fault_index].prefault == LSD_fault[LSD_fault_index].debounce.output)
	{
		/*Edge detected, clean counter*/
		LSD_fault[LSD_fault_index].debounce.counter=0U;
	}
	else if(LSD_fault[LSD_fault_index].debounce.counter < LSD_counter_max)
	{
		/*update counter*/
		LSD_fault[LSD_fault_index].debounce.counter ++;
	}
	else
	{
		/*Counter time reached, update output*/
		LSD_fault[LSD_fault_index].debounce.output = LSD_fault[LSD_fault_index].prefault;
		LSD_fault[LSD_fault_index].debounce.counter=0U;
	}

}
static LSD_zone_enum_t LSD_IdentifyState(uint8 LSD_battery_volt)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	LSD_zone_enum_t LSD_out_state;
	LSD_zone_t LSD_zone_compensate[LSD_ZONES];
	uint8 LSD_battery_volt_Limited;


	LSD_battery_volt_Limited = LSD_battery_volt;

	/*Compensate all thresholds*/

	/*Work around, different compensation gains. Ignore drive zone.*/
	LSD_zone_compensate[LSD_UNLOCK].threshold_high = LSD_CompensateThreshold(LSD_zone[LSD_UNLOCK].threshold_high,LSD_battery_volt,LSD_BATTERY_GAIN_LOW);
	LSD_zone_compensate[LSD_UNLOCK].threshold_low = LSD_CompensateThreshold(LSD_zone[LSD_UNLOCK].threshold_low,LSD_battery_volt,LSD_BATTERY_GAIN_LOW);

	LSD_zone_compensate[LSD_LOCK].threshold_high = LSD_CompensateThreshold(LSD_zone[LSD_LOCK].threshold_high,LSD_battery_volt_Limited,LSD_BATTERY_GAIN_HIGH);
	LSD_zone_compensate[LSD_LOCK].threshold_low = LSD_CompensateThreshold(LSD_zone[LSD_LOCK].threshold_low,LSD_battery_volt_Limited,LSD_BATTERY_GAIN_HIGH);

	LSD_zone_compensate[LSD_DRIVE].threshold_high = LSD_SNA_U8;
	LSD_zone_compensate[LSD_DRIVE].threshold_low = LSD_SNA_U8;


	/*Get zone*/
	if((LSD_rawValue_in >= LSD_zone_compensate[LSD_DRIVE].threshold_low)
		 &&(LSD_rawValue_in <= LSD_zone_compensate[LSD_DRIVE].threshold_high))
	{
		 /*Driven zone*/
		 LSD_out_state =  LSD_DRIVE;
	}
	else if((LSD_rawValue_in >= LSD_zone_compensate[LSD_UNLOCK].threshold_low)
		 &&(LSD_rawValue_in <= LSD_zone_compensate[LSD_UNLOCK].threshold_high))
	{
		 /*Unlock zone*/
		 LSD_out_state = LSD_UNLOCK;
	}
	else if ((LSD_rawValue_in >= LSD_zone_compensate[LSD_LOCK].threshold_low)
		 &&(LSD_rawValue_in <= LSD_zone_compensate[LSD_LOCK].threshold_high))
	{
		 /*Lock zone*/
		 LSD_out_state =  LSD_LOCK;
	}
	else
	{
		 /*Not inside a defined zone.*/
		 /*Drive zone.*/
		 LSD_out_state =  LSD_UNDEFINED_ZONE;
	}
	return LSD_out_state;
}
static uint8 LSD_CompensateThreshold(uint8 LDS_ThresholdIN, uint8 LSD_battery_volt, uint16 LSD_compensationGain)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 ThresholdCompensate;
	uint32 ThresholdU32_pos;
	uint32 ThresholdU32_neg;


	ThresholdU32_pos = ((uint32)LDS_ThresholdIN<<LSD_BATTERY_GAIN_Q)+
										((uint32)LSD_battery_volt*(uint32)LSD_compensationGain);
	ThresholdU32_neg = (uint32)LSD_NOMINAL_BATTERY_VOLT*(uint32)LSD_compensationGain;

	if(ThresholdU32_neg>=ThresholdU32_pos)
	{
		/*Avoid underflow*/
		ThresholdCompensate = 0U;
	}
	else
	{
		ThresholdU32_pos = (ThresholdU32_pos-ThresholdU32_neg)>>LSD_BATTERY_GAIN_Q;

		if((uint32)LSD_SNA_U8<ThresholdU32_pos)
		{
			/*Avoid Overflow*/
			ThresholdCompensate = (uint8)LSD_SNA_U8;
		}
		else
		{
			ThresholdCompensate = (uint8)ThresholdU32_pos;
		}

	}
	return ThresholdCompensate;
}
static void Init_conversion(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 LSD_fault_index;


	for(LSD_fault_index=0U;LSD_fault_index<(uint8)LSD_NUM_FAULTS;LSD_fault_index++)
	{
		LSD_fault[LSD_fault_index].debounce.counter=0U;
		LSD_fault[LSD_fault_index].debounce.output = FALSE;
		LSD_fault[LSD_fault_index].prefault = FALSE;
		/*Convert form DFA units (1ms and gain = 50) to scheduler units (10ms)*/
		/*Max DFA value is 20*/
		LSD_fault[LSD_fault_index].debounce.counter_max[LSD_ACTIVATION] *= (uint16)LSD_TIME_CONVERSION;
		LSD_fault[LSD_fault_index].debounce.counter_max[LSD_DEACTIVATION] *= (uint16)LSD_TIME_CONVERSION;
	}
}

static void LSD_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	uint8 LSD_fault_index;


	LSD_enable = Rte_CData_CalCtrlFlowPlugLock();

	LSD_zone[LSD_LOCK].threshold_low =  Rte_CData_CalELockSensorLockLow();
	LSD_zone[LSD_LOCK].threshold_high =  Rte_CData_CalELockSensorLockHigh();
	LSD_zone[LSD_UNLOCK].threshold_low = Rte_CData_CalELockSensorUnlockLow();
	LSD_zone[LSD_UNLOCK].threshold_high =  Rte_CData_CalELockSensorUnlockHigh();
	LSD_zone[LSD_DRIVE].threshold_low =  Rte_CData_CalELockSensorDriveLow();
	LSD_zone[LSD_DRIVE].threshold_high =  Rte_CData_CalELockSensorDriveHigh();

	LSD_fault[LSD_SCP].threshold =  Rte_CData_CalElockSensorThresholdSCP();
	LSD_fault[LSD_SCG].threshold =  Rte_CData_CalElockSensorThresholdSCG();

	LSD_fault[LSD_SCP].debounce.counter_max[LSD_ACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCPConfirmTime();
	LSD_fault[LSD_SCG].debounce.counter_max[LSD_ACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCGConfirmTime();
	LSD_fault[LSD_SCP].debounce.counter_max[LSD_DEACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCPHealTime();
	LSD_fault[LSD_SCG].debounce.counter_max[LSD_DEACTIVATION] = (uint16)(uint8)Rte_CData_CalElockSensorSCGHealTime();

	for(LSD_fault_index=0U;LSD_fault_index<(uint8)LSD_NUM_FAULTS;LSD_fault_index++)
	{
		/*Convert form DFA units (1ms and gain = 50) to scheduler units (10ms)*/
		/*Max DFA value is 20*/
		LSD_fault[LSD_fault_index].debounce.counter_max[LSD_ACTIVATION] *= (uint16)LSD_TIME_CONVERSION;
		LSD_fault[LSD_fault_index].debounce.counter_max[LSD_DEACTIVATION] *= (uint16)LSD_TIME_CONVERSION;
	}


}
static uint8 LSD_StateDebounce(uint8 Input)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLSD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLSD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLSD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 Counter = 0U;

	#define CtApLSD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLSD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 PreviousState = (uint8)LSD_UNLOCK;
	static uint8 OutSate = (uint8)LSD_UNLOCK;

	#define CtApLSD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLSD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(Input==OutSate)
	{
		Counter = 0U;
	}
	else if(Rte_CData_CalELockDebounce()<=Counter)
	{
		OutSate = Input;
		Counter = 0U;
	}
	else if(Input==PreviousState)
	{
		Counter++;
	}
	else
	{
		Counter = 0U;
	}

	PreviousState = Input;

	return OutSate;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
