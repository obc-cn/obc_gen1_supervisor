/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApOBC.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApOBC
 *  Generation Time:  2020-11-23 11:42:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApOBC>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup OBC
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief On board charger
 * \details This module controls the peripheral behavior.
 *
 * \{
 * \file  OBC.c
 * \brief  Generic code of the OBC module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * WdgM_CheckpointIdType
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *
 * Operation Prototypes:
 * =====================
 * CheckpointReached of Port Interface WdgM_AliveSupervision
 *   Indicates to the Watchdog Manager that a Checkpoint within a Supervised Entity has been reached.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApOBC.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "WdgM.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/** DC-link Voltage offset*/
#define OBC_DCLINK_OFFSET			500U
/** Vout Max offset.*/
#define OBC_DCHV_VOUTMAX_OFFSET		450U
/** Gain 10*/
#define OBC_CONVERSION_GAIN_10		10U
/** Gain 100*/
#define OBC_CONVERSION_GAIN_100		100U
/** Gain 10000*/
#define OBC_CONVERSION_GAIN_10000	10000U
/** Signal not available value for U16*/
#define OBC_SNA_U16					0xFFFFU
/** DC-link Voltage range for charged state.*/
#define OBC_DCLINK_PRECENTAGE		105U

/** Max current in monophasic.*/
#define OBC_CURRENT_LIMIT_HW_MONO 	300U
/** Max current in triphasic.*/
#define OBC_CURRENT_LIMIT_HW_TRI 		150U

/** Input current control loop gain*/
/*Constant time, tau (63%): 2seg*/
#define OBC_CURRENT_CONTROL_KI			30
/** Input current control loop exponent*/
#define OBC_CURRENT_CONTROL_Q				12U


/**Anti-windup marging: 2A*/
#define OBC_ANTIWINDUP_MARGIN 20
 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are auto-generated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/** General states.*/
typedef enum{
	/** OBC disable.*/
	OBC_STATE_DISABLE = 0U,
	/** PFC ON and DCHV OFF.*/
	OBC_STATE_PFC_ON,
	/** Both peripherals ON.*/
	OBC_STATE_DCHV_ON,
	/** Stop state.*/
	OBC_STATE_STOP,
	/** Discharge state*/
	OBC_STATE_DISCHARGE,
	/** POST state*/
	OBC_STATE_POST,
	/** Power off state*/
	OBC_STATE_POWER_OFF,
	/** Power connection fail*/
	OBC_STATE_FAIL
}OBC_state_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Internal peripherals state.*/
static OBC_state_t OBC_state;

/*NVM*/
/** Time between the Precharge relay is closed and the PFC is ON.*/
static uint8 OBC_TimeRelayClose;
/** DC-link voltage for triphasic.*/
static uint16 OBC_DClinkVrequired_tri;
/** DC-link volatge for monophasic.*/
static uint16 OBC_DClinkVrequired_mono;
/** Max output voltage.*/
static uint16 OBC_MaxVoutDCHV;
/** Max output current.*/
static uint16 OBC_MaxIoutDCHV;
/** Min voltage to supply the maximum power.*/
static uint8 OBC_MinVoutDCHV;
/** Last current reference.*/
static DCDC_CurrentReference OBC_CurrentReference;
/** Last High voltage allow.*/
static OBC_HighVoltConnectionAllowed OBC_HighVoltAllow;
/** Current POST polarity*/
static boolean OBC_PostPolarity;

static boolean OBC_FaultToOBC;

#define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/** \brief Voltage reference for the DCHV.
 * \details This function return the Voltage set point for the DCHV.
 */
static uint16 OBC_VoutDCHV(void);
/** \brief Current reference for the DCHV.
 *  \details This function return the Current set point for the DCHV.
 *  \param[in] DCHV_Vref DCHV voltage reference
 *  \param[in] OBCstate OBC state
 */
static uint16 OBC_IoutDCHV(uint16 DCHV_Vref, OBC_state_t OBCstate);
/** Input current max allow by HW.*/
static uint16 OBC_HWcurrentMax(void);
/** Input current max allow by Power max.*/
static uint16 OBC_PowerCurrentMax(uint16 DCHV_Vref);
/**Get minimum value*/
static uint32 OBC_GetMin(sint32 Value1, sint32 Value2);

/** \brief Start condition.
 *  \details This function will check if the conditions to keep running are met.
 *  \param[in] StopRequest Soft stop request.
 *  \param[in] InVoltMode Input Voltage state.
 */
static boolean OBC_startCondition(uint8 InVoltMode, boolean StopRequest);
/** \brief State Machine for the peripherals control.
 *  \details This function will calculate the next OBC state depending on the inputs.
 * 	\param[in] StopRequest Condition needed to stop the conversion.
 *  \param[in] StartCondition Condition needed to start the conversion.
 *  \param[in] OBC_VDCLinckRequired DC-link voltage reference.
 *  \param[in] OBC_PFC_runState PFC mode.
 *  \param[in] OBC_DCHV_Vout Output voltage DCHV.
 */
static void OBC_stateMachine( boolean StopRequest, boolean StartCondition,
						uint16 OBC_VDCLinckRequired,uint8 OBC_PFC_runState,uint8 InVoltState);

/** Report Post state to RTE.*/
static void OBC_ReportPostState(void);
/** \brief Standby state transition logic.
 *  \details Specific function for Standby state.
 *  \param[in] StartCondition Condition needed to start the conversion.
 *  \param[in] ChargingMode Charging Mode.
 *  \param[in] CHG_stateOBC OBC state.
 */
static void OBC_StateDisable(boolean StartCondition,uint8 ChargingMode, uint8 CHG_stateOBC, uint16 DClinkV, OBC_InputVoltageSt InputVoltage);

/** \brief PFC on state transition logic.
 *  \details Specific function for PFC ON state.
 *  \param[in] StopRequest Soft stop request.
 *  \param[in] StartCondition Condition needed to start the conversion.
 *  \param[in] OBC_VDCLinckRequired DC-link voltage reference.
 *  \param[in] OBC_PFC_runState PFC mode.
 *  \param[in] OBC_DCHV_Vout Output voltage DCHV.
 */
static void OBC_StatePFCon(boolean StopRequest, boolean StartCondition,
							uint16 OBC_VDCLinckRequired,uint8 OBC_PFC_runState,
							uint8 PFCstate,	uint16 DClinkV);
/** \brief DCHV on state transition logic.
 *  \details Specific function for DCHV ON state.
 *  \param[in] StopRequest Condition needed to stop the conversion.
 */
static void OBC_StateDCHVon(boolean StopRequest);
/** \brief Send peripherals state request.
 *  \details This function will send to the peripherals the state requested
 *   depending on the OBC state.
 *  \param[in] OBC_VDCLinckRequired DC-link voltage reference.
 *  \param[in] OBC_PFC_runState PFC mode.
 */
static void OBC_OutValues(SUP_CommandVDCLink_V OBC_VDCLinckRequired,SUP_RequestPFCStatus OBC_PFC_runState);

/*This functions has been added to reduce the Hasltead. It only writes the output values in the RTE.*/
static void OBC_SetOut(SUP_RequestPFCStatus StateRequestPFC,SUP_RequestStatusDCHV StateRequestDCHV,
											 SUP_CommandVDCLink_V DClinkVref,DCDC_VoltageReference DCHV_Vref, DCDC_CurrentReference DCHV_Iref,
											 boolean HW_disable, boolean OBC_HighVoltConnectionAllowed_value, boolean VCC_OBC);

/**
 * \brief Stop state.
 * \details While this state the OBC will stop the OBC.
 *  \param[in] StopHW Stop HW request.
 */
static void OBC_StateStop(boolean StopHW);
/** Discharge state*/
static void OBC_StateDischarge(uint16 DClinkV,OBC_InputVoltageSt InputVoltage ,boolean StartCondition);
/** POST of state*/
static void OBC_StatePost(uint8 DCHVstate, uint8 PFCstate);
/** Power of state*/
static void OBC_StatePowerOff(void);

/** Update calibratables*/
static void OBC_UpdateCalibratables(void);

/** Publish DTC triggers */
static void OBC_DTCTriggerReport(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_HighestChargeCurrentAllow: Integer in interval [0...2047]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * BMS_HighestChargeVoltageAllow: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * CalVDCLinkRequiredMonophasic: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 500
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCDC_VoltageReference: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * IdtMaxInputACCurrentEVSE: Integer in interval [0...65535]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * IdtMaxOutputDCHVCurrent: Integer in interval [0...200]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtMaxOutputDCHVVoltage: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 450
 * IdtMinDCHVOutputVoltage: Integer in interval [0...50]
 *   Unit: [V], Factor: 10, Offset: 0
 * IdtTimeRelaysPrechargeClosed: Integer in interval [0...200]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtVDCLinkRequiredTriphasic: Integer in interval [0...250]
 *   Unit: [V], Factor: 1, Offset: 500
 * IdtVoltageCorrectionOffset: Integer in interval [-32768...32767]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_PowerMax: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * PFC_IPH1_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH2_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH3_RMS_0A1: Integer in interval [0...1023]
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * SUP_CommandVDCLink_V: Integer in interval [0...1023]
 *   Unit: [V], Factor: 1, Offset: 0
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * PFC_PFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 *   Cx6_PFC_STATE_START_TRI (6U)
 *   Cx7_PFC_STATE_START_MONO (7U)
 *   Cx8_PFC_STATE_ERROR (8U)
 *   Cx9_PFC_STATE_DIRECTPWM (9U)
 * SUP_RequestPFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 * SUP_RequestStatusDCHV: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATE_STANDBY (0U)
 *   Cx1_DCHV_STATE_RUN (1U)
 *   Cx2_DCHV_STATE_RESET_ERROR (2U)
 *   Cx3_DCHV_STATE_DISCHARGE (3U)
 *   Cx4_DCHV_STATE_SHUTDOWN (4U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   boolean *Rte_Pim_PimOBCShutdownPathFSP(void)
 *   boolean *Rte_Pim_PimOBCShutdownPathGPIO(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxOutputDCHVCurrent Rte_CData_CalMaxOutputDCHVCurrent(void)
 *   IdtMaxOutputDCHVVoltage Rte_CData_CalMaxOutputDCHVVoltage(void)
 *   IdtMinDCHVOutputVoltage Rte_CData_CalMinDCHVOutputVoltage(void)
 *   IdtTimeRelaysPrechargeClosed Rte_CData_CalTimeRelaysPrechargeClosed(void)
 *   CalVDCLinkRequiredMonophasic Rte_CData_CalVDCLinkRequiredMonophasic(void)
 *   IdtVDCLinkRequiredTriphasic Rte_CData_CalVDCLinkRequiredTriphasic(void)
 *
 *********************************************************************************************************************/


#define CtApOBC_START_SEC_CODE
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOBC_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_init_doc
 *********************************************************************************************************************/

/*!
 * \brief OBC initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOBC_CODE) RCtApOBC_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_init
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	OBC_TimeRelayClose=(uint8)Rte_CData_CalTimeRelaysPrechargeClosed();
	OBC_DClinkVrequired_mono=(uint16)(uint8)Rte_CData_CalVDCLinkRequiredMonophasic()+(uint16)OBC_DCLINK_OFFSET;
	OBC_DClinkVrequired_tri=(uint16)(uint8)Rte_CData_CalVDCLinkRequiredTriphasic()+(uint16)OBC_DCLINK_OFFSET;
	OBC_MaxVoutDCHV=((uint16)(uint8)Rte_CData_CalMaxOutputDCHVVoltage()+(uint16)OBC_DCHV_VOUTMAX_OFFSET)
								*(uint16)OBC_CONVERSION_GAIN_10;
	OBC_MaxIoutDCHV=(uint16)(uint8)Rte_CData_CalMaxOutputDCHVCurrent()*(uint16)OBC_CONVERSION_GAIN_10;
	OBC_MinVoutDCHV=(uint8)Rte_CData_CalMinDCHVOutputVoltage();

	OBC_state = OBC_STATE_POST;
	OBC_CurrentReference = 0U;
	OBC_HighVoltAllow = FALSE;
	OBC_FaultToOBC = FALSE;
	OBC_PostPolarity = TRUE;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOBC_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(DCDC_CurrentReference data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOBC_CODE) RCtApOBC_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msA
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	(void)Rte_Write_PpInt_DCDC_CurrentReference_Delayed_DCDC_CurrentReference(OBC_CurrentReference);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltAllow);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOutputSupFaultToOBCPhysicalValue_Delayed_DeOutputSupFaultToOBCPhysicalValue(OBC_FaultToOBC);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApOBC_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
 *   Std_ReturnType Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data)
 *   Std_ReturnType Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data)
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *   Std_ReturnType Rte_Read_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean *data)
 *   Std_ReturnType Rte_Read_PpStopConditions_DeStopConditions(boolean *data)
 *   Std_ReturnType Rte_Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(IdtVoltageCorrectionOffset *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data)
 *   Std_ReturnType Rte_Write_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data)
 *   Std_ReturnType Rte_Write_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result data)
 *   Std_ReturnType Rte_Write_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msB_doc
 *********************************************************************************************************************/

/*!
  * \brief OBC task.
  *
  * This function is called periodically by the scheduler. This function should
  * update the requested state for the peripherals.
  */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApOBC_CODE) RCtApOBC_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApOBC_task10msB
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	uint8 OBC_InVoltMode;
	uint8 OBC_InVoltState;
	boolean OBC_StopCondition;
	boolean OBC_FaultStop;
	boolean OBC_StopRequest;
	boolean OBC_StartCondition;
	uint16 OBC_VDCLinckRequired;
	uint8 OBC_PFC_runState;
	boolean OBC_ProgramFlowSoftModeEnabled;

	(void)Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&OBC_ProgramFlowSoftModeEnabled);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (FALSE == OBC_ProgramFlowSoftModeEnabled)
	{
		(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP4);
	}

	OBC_UpdateCalibratables();

	(void)Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&OBC_InVoltMode);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&OBC_InVoltState);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpStopConditions_DeStopConditions(&OBC_StopCondition);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&OBC_FaultStop);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

	OBC_DTCTriggerReport();

	/*Stop Request*/
	if((FALSE!=OBC_StopCondition)||(FALSE!=OBC_FaultStop)||(Cx1_220V_AC_connected!=OBC_InVoltState))
	{
		OBC_StopRequest = TRUE;
	}
	else
	{
		OBC_StopRequest = FALSE;
	}


	if((uint8)IVM_MONOPHASIC_INPUT==OBC_InVoltMode)
	{
		OBC_VDCLinckRequired = OBC_DClinkVrequired_mono;
		OBC_PFC_runState = (uint8)Cx2_PFC_STATE_MONO;
	}
	else if((uint8)IVM_TRIPHASIC_INPUT==OBC_InVoltMode)
	{
		OBC_VDCLinckRequired = OBC_DClinkVrequired_tri;
		OBC_PFC_runState = (uint8)Cx1_PFC_STATE_TRI;
	}
	else
	{
		OBC_VDCLinckRequired = 0U;
		OBC_PFC_runState = (uint8)Cx0_PFC_STATE_STANDBY;
	}

	OBC_StartCondition = OBC_startCondition(OBC_InVoltMode, OBC_StopRequest);

	/*Update Converter state*/
	OBC_stateMachine(OBC_StopRequest, OBC_StartCondition,
					OBC_VDCLinckRequired,OBC_PFC_runState,OBC_InVoltState);

	OBC_ReportPostState();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApOBC_STOP_SEC_CODE
#include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/

static void OBC_ReportPostState(void)
{
	IdtPOST_Result PostResult;

	if(OBC_STATE_FAIL == OBC_state)
	{
		PostResult = POST_NOK;
	}
	else if((OBC_STATE_POST == OBC_state)
			||(OBC_STATE_POWER_OFF == OBC_state))
	{
		PostResult = POST_ONGOING;
	}
	else
	{
		PostResult = POST_OK;
	}

	(void)Rte_Write_PpOBC_POST_Result_DeOBC_POST_Result(PostResult);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}
static uint16 OBC_VoutDCHV(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	uint16 HighestVoltage;
	sint16 VolatgeError;
	sint32 result;


	(void)Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(&HighestVoltage);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpVoltageCorrectionOffset_DeVoltageCorrectionOffset(&VolatgeError);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

	result = (sint32)HighestVoltage - (sint32)VolatgeError;

	if(0>result)
	{
		/*Lower limitation*/
		HighestVoltage = 0U;
	}
	else if((sint32)OBC_MaxVoutDCHV<result)
	{
		/*Upper limitation*/
		HighestVoltage = OBC_MaxVoutDCHV;
	}
	else
	{
		HighestVoltage = (uint16)result;
	}

	return  HighestVoltage;
    
}
static uint16 OBC_IoutDCHV(uint16 DCHV_Vref, OBC_state_t OBCstate)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static sint32 OutputCurrentLoop  = 0;/*dA, Q12*/

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	/*Input Limitations*/
	uint16 EVSE_MaxCurrent;/*dA*/
	uint16 HW_MaxCurrent;/*dA*/
	uint16 InputCurrentMax;/*dA*/
	/*Input*/
	uint8 RealInputCurrentChannel;
	uint16 RealInputCurrent[3];/*dA*/
	/*Output Limitations*/
	uint16 BMS_MaxCurrent;/*dA*/
	uint16 Power_MaxCurrent;/*dA*/
	uint16 OutputCurrentMax;/*dA*/
	/*Real Reference*/
	uint16 RealSetPoint;
	/*Output*/
	uint16 CurrentSetPoint;


	(void)Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(&EVSE_MaxCurrent);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(&(RealInputCurrent[0]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(&(RealInputCurrent[1]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(&(RealInputCurrent[2]));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(&BMS_MaxCurrent);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(&RealSetPoint);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

	if(OBC_STATE_DCHV_ON!=OBCstate)
	{
		/*Restart integral*/
		OutputCurrentLoop = 0;
	}
	else
	{
		HW_MaxCurrent = OBC_HWcurrentMax();

		/* Calculate maximum input current channel */
		if ((RealInputCurrent[0] >= RealInputCurrent[1]) && (RealInputCurrent[0] >= RealInputCurrent[2]))
		{
			RealInputCurrentChannel = 0U;
		}
		else if ((RealInputCurrent[1] >= RealInputCurrent[0]) && (RealInputCurrent[1] >= RealInputCurrent[2]))
		{
			RealInputCurrentChannel = 1U;
		}
		else
		{
			RealInputCurrentChannel = 2U;
		}


		/*Limit input current*/
		InputCurrentMax = (uint16)OBC_GetMin((sint32)HW_MaxCurrent,(sint32)EVSE_MaxCurrent);
		/*Input current control loop*/
		OutputCurrentLoop += ((sint32)InputCurrentMax - (sint32)RealInputCurrent[RealInputCurrentChannel])*(sint32)OBC_CURRENT_CONTROL_KI;

		/*Max current limiting max output power*/
		Power_MaxCurrent = OBC_PowerCurrentMax(DCHV_Vref);

		/*Output current limitation*/
		OutputCurrentMax = (uint16)OBC_GetMin((sint32)OBC_MaxIoutDCHV,(sint32)BMS_MaxCurrent);
		OutputCurrentMax = (uint16)OBC_GetMin((sint32)OutputCurrentMax,(sint32)Power_MaxCurrent);

		/*Anti-windup saturation*/
		OutputCurrentLoop = (sint32)OBC_GetMin(OutputCurrentLoop,(sint32)(OutputCurrentMax << OBC_CURRENT_CONTROL_Q)); /* PRQA S 4398,4394 # Casting between signed and unsigned class needed. Correct behaviour has been checked */
		OutputCurrentLoop = (sint32)OBC_GetMin(OutputCurrentLoop,(sint32)(((uint32)RealSetPoint+(uint32)OBC_ANTIWINDUP_MARGIN) << OBC_CURRENT_CONTROL_Q));/* PRQA S 4398,4394 # Casting between signed and unsigned class needed. Correct behaviour has been checked */

	}

	/*Output*/
	CurrentSetPoint = (uint16)((uint32)OutputCurrentLoop>>OBC_CURRENT_CONTROL_Q);

	return CurrentSetPoint;
}
static uint16 OBC_HWcurrentMax(void)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	uint16 HW_MaxCurrent;
	IdtInputVoltageMode Mode;


	(void)Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&Mode);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

	if(IVM_MONOPHASIC_INPUT==Mode)
	{
		HW_MaxCurrent = OBC_CURRENT_LIMIT_HW_MONO;
	}
	else if(IVM_TRIPHASIC_INPUT==Mode)
	{
		HW_MaxCurrent = OBC_CURRENT_LIMIT_HW_TRI;
	}
	else
	{
		HW_MaxCurrent = 0U;
	}

	return HW_MaxCurrent;
}
static uint16 OBC_PowerCurrentMax(uint16 DCHV_Vref)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	uint16 DCHV_Vout;
	uint8 PowerMax;
	uint32 Power_MaxCurrent;


	(void)Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(&PowerMax);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_Vout);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

	if(DCHV_Vout<=((uint16)OBC_MinVoutDCHV*(uint16)OBC_CONVERSION_GAIN_100))
	{
		if(0U==DCHV_Vref)
		{
			Power_MaxCurrent = (uint32)OBC_SNA_U16;
		}
		else
		{
			Power_MaxCurrent = (uint32)PowerMax*(uint32)OBC_CONVERSION_GAIN_10000 / (uint32)DCHV_Vref;
		}

	}
	else
	{
		Power_MaxCurrent = (uint32)PowerMax*(uint32)OBC_CONVERSION_GAIN_10000 / (uint32)DCHV_Vout;
	}

	/*Limit to U16*/
	Power_MaxCurrent = OBC_GetMin((sint32)Power_MaxCurrent,(sint32)(uint32)OBC_SNA_U16);

	return (uint16)Power_MaxCurrent;
}


static uint32 OBC_GetMin(sint32 Value1, sint32 Value2)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	sint32 retVal;


	retVal = Value1;

	/*Get min value*/
	if(Value2<retVal)
	{
		retVal = Value2;
	}

	/*Lower limitation*/
	if(0>retVal)
	{
		retVal = 0;
	}

	return (uint32)retVal;
}



static boolean OBC_startCondition(uint8 InVoltMode, boolean StopRequest)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	boolean retVal;


	if((FALSE==StopRequest)
			&&(((uint8)IVM_MONOPHASIC_INPUT==InVoltMode)
					||((uint8)IVM_TRIPHASIC_INPUT==InVoltMode)))
	{
		retVal = TRUE;
	}
	else
	{
		retVal = FALSE;
	}

	return retVal;
}
static void OBC_stateMachine( boolean StopRequest, boolean StartCondition,
						uint16 OBC_VDCLinckRequired,uint8 OBC_PFC_runState,uint8 InVoltState)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	uint8 ChargingMode;
	uint8 CHG_stateOBC;
	uint8 DCHVstate;
	uint8 PFCstate;
	uint16 DClinkV;
	boolean StopHW;
	OBC_InputVoltageSt InputVoltage;

	(void)InVoltState;	/* Unused variable */

	(void)Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(&PFCstate);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(&DCHVstate);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(&DClinkV);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&ChargingMode);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_Status_OBC_Status(&CHG_stateOBC);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpRequestHWStopOBC_DeRequestHWStopOBC(&StopHW);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&InputVoltage);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/

	switch(OBC_state)
	{
		case OBC_STATE_POWER_OFF:
			OBC_StatePowerOff();
		break;

		case OBC_STATE_POST:
			OBC_StatePost(DCHVstate,PFCstate);
		break;

		case OBC_STATE_PFC_ON:
			OBC_StatePFCon(StopRequest, StartCondition, OBC_VDCLinckRequired,
					OBC_PFC_runState,PFCstate,DClinkV);
			break;

		case OBC_STATE_DCHV_ON:
			OBC_StateDCHVon(StopRequest);
			break;

		case OBC_STATE_STOP:
			OBC_StateStop(StopHW);
			break;

		case OBC_STATE_DISCHARGE:
			OBC_StateDischarge(DClinkV,InputVoltage,StartCondition);
			break;

		case OBC_STATE_DISABLE:
			OBC_StateDisable(StartCondition,ChargingMode,CHG_stateOBC,DClinkV,InputVoltage);
			break;

		default :
		case OBC_STATE_FAIL:
			/*Don't leave this state*/
			OBC_state = OBC_STATE_FAIL;
			break;
	}

	OBC_OutValues((SUP_CommandVDCLink_V)OBC_VDCLinckRequired,(SUP_RequestPFCStatus)OBC_PFC_runState);
}

static void OBC_StatePowerOff(void)
{
	 /* - Static non-init variables declaration ---------- */
	    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
	    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	    /* Static non-init variables HERE... */

	    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	    /* - Static zero-init variables declaration --------- */
	    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		static uint16 MinTime = 0U;
		static uint8 RetryNum = 0U;

	    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	    /* - Static init variables declaration -------------- */
	    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
	    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	    /* Static init variables HERE... */

	    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
	    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	    /* - Automatic variables declaration ---------------- */
		uint16 OffTime;

		OffTime = 70U + (20U*(uint16)RetryNum);

		if(5U<=RetryNum)
		{
			/*Max number of retry reached, don't try again*/
			OBC_state = OBC_STATE_FAIL;
		}
		else if(OffTime<=MinTime)
		{
			/*Min time reached, try post*/
			RetryNum++;
			MinTime = 0U;
			OBC_state = OBC_STATE_POST;
		}
		else
		{
			MinTime++;
		}
}

static void OBC_StatePost(uint8 DCHVstate, uint8 PFCstate)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 Debounce = 0U;
	static uint8 TimeOut = 0U;

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	boolean HWstop_feedback;
	boolean AtiveDischarge;


	(void)Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(&HWstop_feedback);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(&AtiveDischarge);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/



	if(FALSE!=AtiveDischarge)
	{
		/*Wait until the end of the active dicharge request.*/
		TimeOut = 0U;
		Debounce = 0U;
		OBC_PostPolarity = TRUE;
	}
	else if(100U<TimeOut)
	{
		/*TimeOut reached, restart the sequence*/
		TimeOut = 0U;
		Debounce = 0U;
		OBC_state = OBC_STATE_POWER_OFF;
		OBC_PostPolarity = TRUE;
	}
	else if((Cx0_DCHV_STATUS_STANDBY!=DCHVstate)||(Cx0_PFC_STATE_STANDBY!=PFCstate)
			|| (OBC_PostPolarity!=HWstop_feedback))

	{
		Debounce=0U;
		TimeOut++;
	}
	else if(10U>Debounce)
	{
		Debounce++;
		TimeOut++;
	}
	else if(FALSE!=OBC_PostPolarity)
	{
		/*First polarity OK*/
		Debounce=0U;
		OBC_PostPolarity = FALSE;
		TimeOut++;
	}
	else
	{
		/*Full Post OK*/
		OBC_state = OBC_STATE_DISABLE;
		Debounce = 0U;
		TimeOut = 0U;
	}

}

static void OBC_StateDisable(boolean StartCondition,uint8 ChargingMode, uint8 CHG_stateOBC, uint16 DClinkV, OBC_InputVoltageSt InputVoltage)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 RelayCounter = 0U;

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	boolean RelayPreCharge;

	(void)DClinkV;			/* Remove compilator warnings. Variable not used*/
	(void)InputVoltage; 	/* Remove compilator warnings. Variable not used*/

	(void)Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(&RelayPreCharge);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/


	if(((uint8)Cx1_slow_charging!=ChargingMode)
					||(((uint8)Cx3_conversion_working_!=CHG_stateOBC)&&((uint8)Cx5_degradation_mode!=CHG_stateOBC))
					||(TRUE!=RelayPreCharge)
					||(TRUE!=StartCondition))
	{
		RelayCounter = 0U;

	}
	else if(RelayCounter<((uint16)OBC_TimeRelayClose*(uint16)OBC_CONVERSION_GAIN_10))
	{
		RelayCounter++;

	}
	else
	{
		OBC_state=OBC_STATE_PFC_ON;
		RelayCounter = 0U;
	}
}
static void OBC_StatePFCon(boolean StopRequest, boolean StartCondition,
							uint16 OBC_VDCLinckRequired,uint8 OBC_PFC_runState,
							 uint8 PFCstate,uint16 DClinkV)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	if(FALSE!=StopRequest)
	{
		OBC_state=OBC_STATE_STOP;
	}
	else if((TRUE==StartCondition)/*Start Condition*/
		&& (PFCstate==OBC_PFC_runState)
		&& (DClinkV>=OBC_VDCLinckRequired)
		&&((uint32)DClinkV<=((uint32)OBC_VDCLinckRequired*(uint32)OBC_DCLINK_PRECENTAGE/(uint32)100U)))
	{
		OBC_state=OBC_STATE_DCHV_ON;
	}
	else
	{
		/*MISRA*/
	}

}
static void OBC_StateDCHVon(boolean StopRequest)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	if(FALSE!=StopRequest)
	{
		OBC_state=OBC_STATE_STOP;
	}
}
static void OBC_StateStop(boolean StopHW)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	if(FALSE!=StopHW)
	{
		OBC_state = OBC_STATE_DISABLE;
	}
}
static void OBC_StateDischarge(uint16 DClinkV,OBC_InputVoltageSt InputVoltage ,boolean StartCondition)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	if((15U>=DClinkV)
			||(Cx1_220V_AC_connected==InputVoltage)
			||(TRUE==StartCondition))
	{
		OBC_state = OBC_STATE_DISABLE;
	}
}
static void OBC_OutValues(SUP_CommandVDCLink_V OBC_VDCLinckRequired,SUP_RequestPFCStatus OBC_PFC_runState)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	DCDC_VoltageReference DCHV_Vref;
	DCDC_CurrentReference DCHV_Iref;


	DCHV_Vref = (DCDC_VoltageReference)OBC_VoutDCHV();
	DCHV_Iref = (DCDC_CurrentReference)OBC_IoutDCHV(DCHV_Vref,OBC_state);

	/*Use a function to set values for reduce the halstead*/

	switch(OBC_state)
	{
		case OBC_STATE_POWER_OFF:
			OBC_SetOut(
			Cx0_PFC_STATE_STANDBY,
			Cx0_DCHV_STATE_STANDBY,
			0U,
			0U,
			0U,
			TRUE,
			FALSE,
			FALSE
			);
		break;

		case OBC_STATE_POST:
			OBC_SetOut(
			Cx3_PFC_STATE_RESET_ERROR,
			Cx2_DCHV_STATE_RESET_ERROR,
			0U,
			0U,
			0U,
			OBC_PostPolarity,
			FALSE,
			TRUE
			);
			break;

		case OBC_STATE_PFC_ON:
			OBC_SetOut(
			OBC_PFC_runState,
			Cx0_DCHV_STATE_STANDBY,
			OBC_VDCLinckRequired,
			DCHV_Vref,
			DCHV_Iref,
			FALSE,
			TRUE,
			TRUE
			);
			break;

		case OBC_STATE_DCHV_ON:
			OBC_SetOut(
			OBC_PFC_runState,
			Cx1_DCHV_STATE_RUN,
			OBC_VDCLinckRequired,
			DCHV_Vref,
			DCHV_Iref,
			FALSE,
			TRUE,
			TRUE
			);
			break;

		case OBC_STATE_STOP:
			OBC_SetOut(
			Cx0_PFC_STATE_STANDBY,
			Cx0_DCHV_STATE_STANDBY,
			0U,
			DCHV_Vref,
			DCHV_Iref,
			FALSE,
			TRUE,
			TRUE
			);
			break;

		case OBC_STATE_DISCHARGE:
			OBC_SetOut(
			Cx4_PFC_STATE_DISCHARGE,
			Cx2_DCHV_STATE_RESET_ERROR,
			0U,
			0U,
			0U,
			TRUE,
			TRUE,
			TRUE
			);
			break;

		case OBC_STATE_DISABLE:
			OBC_SetOut(
			Cx3_PFC_STATE_RESET_ERROR,
			Cx2_DCHV_STATE_RESET_ERROR,
			0U,
			0U,
			0U,
			TRUE,
			TRUE,
			TRUE
			);
			break;

		default :
		case OBC_STATE_FAIL:
			OBC_SetOut(
			Cx0_PFC_STATE_STANDBY,
			Cx0_DCHV_STATE_STANDBY,
			0U,
			0U,
			0U,
			TRUE,
			FALSE,
			TRUE
			);
			break;
	}

}
static void OBC_SetOut(SUP_RequestPFCStatus StateRequestPFC,SUP_RequestStatusDCHV StateRequestDCHV,
											 SUP_CommandVDCLink_V DClinkVref,DCDC_VoltageReference DCHV_Vref, DCDC_CurrentReference DCHV_Iref,
											 boolean HW_disable, boolean OBC_HighVoltConnectionAllowed_value, boolean VCC_OBC)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	OBC_FaultToOBC = HW_disable;
	OBC_CurrentReference = DCHV_Iref;
	OBC_HighVoltAllow = OBC_HighVoltConnectionAllowed_value;
	(void)Rte_Write_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(StateRequestPFC);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(StateRequestDCHV);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(DClinkVref);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(DCHV_Vref);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(DCHV_Iref);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(HW_disable);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed_value); /* Hotfix. To be reviewed.*//* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(VCC_OBC);/* PRQA S 3110, 3200, 3417, 3426 #RTE read/write confirmation not used*/


}

static void OBC_UpdateCalibratables(void)
{

   /* - Static non-init variables declaration ---------- */
    #define CtApOBC_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApOBC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApOBC_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApOBC_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApOBC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */


	OBC_TimeRelayClose=(uint8)Rte_CData_CalTimeRelaysPrechargeClosed();
	OBC_DClinkVrequired_mono=(uint16)(uint8)Rte_CData_CalVDCLinkRequiredMonophasic()+(uint16)OBC_DCLINK_OFFSET;
	OBC_DClinkVrequired_tri=(uint16)(uint8)Rte_CData_CalVDCLinkRequiredTriphasic()+(uint16)OBC_DCLINK_OFFSET;
	OBC_MaxVoutDCHV=((uint16)(uint8)Rte_CData_CalMaxOutputDCHVVoltage()+(uint16)OBC_DCHV_VOUTMAX_OFFSET)
								*(uint16)OBC_CONVERSION_GAIN_10;
	OBC_MaxIoutDCHV=(uint16)(uint8)Rte_CData_CalMaxOutputDCHVCurrent()*(uint16)OBC_CONVERSION_GAIN_10;
	OBC_MinVoutDCHV=(uint8)Rte_CData_CalMinDCHVOutputVoltage();

}

static void OBC_DTCTriggerReport(void)
{
	boolean faultData;
	boolean Fault_feedback;

	(void)Rte_Read_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(&Fault_feedback);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((FALSE == Fault_feedback) && (FALSE != OBC_FaultToOBC))
	{
		faultData = TRUE;
	}
	else
	{
		faultData = FALSE;
	}

	(void)Rte_Write_PpOBC_ShutdownPathFault_DeOBC_ShutdownPathFault(faultData);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
