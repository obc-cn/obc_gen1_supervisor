/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtHwAbsPOM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtHwAbsPOM
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtHwAbsPOM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup POM
 * \ingroup BaseSw
 * \author Pablo Bolas.
 * \brief PWM output manager.
 * \details This module manages the PWM generation for the LEDs control.
 *
 * \{
 * \file  POM.c
 * \brief  Generic code of the POM module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtHwAbsPOM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */

#include "Pwm_17_Gtm.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/*! Frequency conversion gain LED.*/
#define POM_FREQ_LED_GAIN 		20U
/*! Frequency conversion gain RELAY.*/
#define POM_FREQ_RLY_GAIN 		1000U
/** Time conversion gain*/
#define POM_TIME_GAIN		10U
/** PWM dutycycle max*/
#define POM_DUTY_MAX		0x8000U
/** PWM input clock frequency*/
#define POM_PWM_CLK_FREQ		390000U
 /* PRQA S 0857,0380 -- # Max amount of macros check */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/
typedef enum
{
	POM_RELAY_MONO=0U,
	POM_RELAY_PRECH,
	POM_RELAY_NUM
}POM_relay_t;
/*PRQA S 3205 --*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtHwAbsPOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Time while the PWM out will be 100% (time needed to close the relay)*/
static uint16 POM_timeClose[POM_RELAY_NUM];
/** Duty cycle when the relay is closed.*/
static uint8 POM_dutyKeep[POM_RELAY_NUM];
/** Relay state counter.*/
static uint16 POM_relayStateCounter[POM_RELAY_NUM];
/** LED period*/
static uint32 POM_ledPeriod;
/** RLY period*/
static uint32 POM_relayPeriod;

#define CtHwAbsPOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtHwAbsPOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtHwAbsPOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtHwAbsPOM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtHwAbsPOM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/

/**
 * \brief Update relay Duty Cycle.
 * \details This function while update the PWM duty cycle depending on the relay
 * state.
 * \param[in] StateIn Input state.
 * \param[in] counter Pointer to a counter.
 * \param[in] TimeCloseing Time while the PWM out will be 100%.
 * \param[in] DCclosed Duty cycle when the relay is closed.
 * \return DC applied.
 */
static uint16 POM_relayDC(boolean StateIn, uint16 *counter, uint16 TimeCloseing, uint8 DCclosed);

/**
 * \brief DC conversion.
 * \details The dutycycle coming form the RTE is expressed in percentage, but
 * the PWM driver works with 0x8000 resolution.
 * \param[in] DC_percentage DC in percentage coming form RTE.
 * \return DC with 0x8000 resolution.
 */
static uint16 POM_DC_conversion(uint8 DC_percentage);

/** Update calibratables*/
static void POM_UpdateCalibratables(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDutyCloseRelay: Integer in interval [50...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtChLedRGBCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtExtPlgLedrCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtFrequencyActivateRelays: Integer in interval [0...40]
 *   Unit: [kHz], Factor: 1, Offset: 0
 * IdtFrequencyPWM: Integer in interval [0...100]
 *   Unit: [Hz], Factor: 20, Offset: 0
 * IdtTimeCloseRelay: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDutyCloseRelay Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed(void)
 *   IdtDutyCloseRelay Rte_CData_CalDutyKeepRelayMonoClosed(void)
 *   IdtFrequencyActivateRelays Rte_CData_CalFrequencyActivateRelays(void)
 *   IdtFrequencyPWM Rte_CData_CalFrequencyPWM(void)
 *   IdtTimeCloseRelay Rte_CData_CalTimeCloseOutputPrechargeRelays(void)
 *   IdtTimeCloseRelay Rte_CData_CalTimeCloseRelayMono(void)
 *
 *********************************************************************************************************************/


#define CtHwAbsPOM_START_SEC_CODE
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsPOM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_init_doc
 *********************************************************************************************************************/

/*!
 * \brief POM initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsPOM_CODE) RCtHwAbsPOM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */ 


	POM_dutyKeep[POM_RELAY_MONO]=(uint8)Rte_CData_CalDutyKeepRelayMonoClosed();
	POM_dutyKeep[POM_RELAY_PRECH]=(uint8)Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed();


	POM_timeClose[POM_RELAY_MONO] = (uint16)(uint8)Rte_CData_CalTimeCloseRelayMono()*(uint16)POM_TIME_GAIN;
	POM_timeClose[POM_RELAY_PRECH] = (uint16)(uint8)Rte_CData_CalTimeCloseOutputPrechargeRelays()*(uint16)POM_TIME_GAIN;


	/*Period conversion*/
	POM_ledPeriod = (uint32)(uint8)Rte_CData_CalFrequencyPWM();
	if ((uint32)0U != POM_ledPeriod)
	{
		/*Update default LED frequency.*/
		POM_ledPeriod = (uint32)POM_PWM_CLK_FREQ/(POM_ledPeriod*(uint32)POM_FREQ_LED_GAIN);
		Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_0,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,0U);
		Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_1,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,0U);
		Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_2,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,0U);
		Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_3,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,0U);

	}

	POM_relayPeriod = (uint32)(uint8)Rte_CData_CalFrequencyActivateRelays();
	if((uint32)0U != POM_relayPeriod)
	{
		POM_relayPeriod = (uint32)POM_PWM_CLK_FREQ/(POM_relayPeriod*(uint32)POM_FREQ_RLY_GAIN);
		Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_4,(Pwm_17_Gtm_PeriodType)POM_relayPeriod,0U);
		Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_5,(Pwm_17_Gtm_PeriodType)POM_relayPeriod,0U);
	}

	/*initialize internal variable*/
	POM_relayStateCounter[POM_RELAY_MONO]=0U;
	POM_relayStateCounter[POM_RELAY_PRECH]=0U;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsPOM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(IdtExtChLedRGBCtl *data)
 *   Std_ReturnType Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(IdtExtChLedRGBCtl *data)
 *   Std_ReturnType Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(IdtExtChLedRGBCtl *data)
 *   Std_ReturnType Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(IdtExtPlgLedrCtl *data)
 *   Std_ReturnType Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputRelayMono_DeOutputRelayMono(boolean *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief POM task.
 *
 * This function is called periodically by the scheduler. This function should
 * update the plug LED duty cycle.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsPOM_CODE) RCtHwAbsPOM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPOM_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Inputs*/
	IdtExtPlgLedrCtl Duty_Elock;
	boolean POM_relayState[POM_RELAY_NUM];
	IdtExtChLedRGBCtl Duty_in;

	/*Local variables*/
	volatile uint16 Duty_out = 0U;


	POM_UpdateCalibratables();

	(void)Rte_Read_PpExtPlgLedrCtl_DeExtPlgLedrCtl(&Duty_Elock);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputRelayMono_DeOutputRelayMono(&POM_relayState[POM_RELAY_MONO]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputPrechargeRelays_DeOutputPrechargeRelays(&POM_relayState[POM_RELAY_PRECH]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*E-Lock LED*/
	Duty_out = POM_DC_conversion((uint8)Duty_Elock);
	Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_0,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,Duty_out);

	/*POM_RELAY_MONO*/
	Duty_out = POM_relayDC(POM_relayState[POM_RELAY_MONO],
							&POM_relayStateCounter[POM_RELAY_MONO],
							POM_timeClose[POM_RELAY_MONO],
							POM_dutyKeep[POM_RELAY_MONO]);
	Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_4,(Pwm_17_Gtm_PeriodType)POM_relayPeriod,Duty_out);

	/*PWM_RELAY_PRECH*/
	Duty_out = POM_relayDC(POM_relayState[POM_RELAY_PRECH],
							&POM_relayStateCounter[POM_RELAY_PRECH],
							POM_timeClose[POM_RELAY_PRECH],
							POM_dutyKeep[POM_RELAY_PRECH]);
	Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_5,(Pwm_17_Gtm_PeriodType)POM_relayPeriod,Duty_out);



	(void)Rte_Read_PpExtChLedRGBCtl_DeExtChLedRrCtl(&Duty_in);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Duty_out = POM_DC_conversion(Duty_in);
	Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_1,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,Duty_out);

	(void)Rte_Read_PpExtChLedRGBCtl_DeExtChLedGrCtl(&Duty_in);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Duty_out = POM_DC_conversion(Duty_in);
	Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_2,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,Duty_out);

	(void)Rte_Read_PpExtChLedRGBCtl_DeExtChLedBrCtl(&Duty_in);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	Duty_out = POM_DC_conversion(Duty_in);
	Pwm_17_Gtm_SetPeriodAndDuty(Pwm_17_GtmConf_PwmChannel_PwmChannel_3,(Pwm_17_Gtm_PeriodType)POM_ledPeriod,Duty_out);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtHwAbsPOM_STOP_SEC_CODE
#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static uint16 POM_relayDC(boolean StateIn, uint16 *counter, uint16 TimeCloseing, uint8 DCclosed)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 DC_percentage =0U;
	uint16 DC_ret;


	if(NULL_PTR!=counter)
	{
		if(TRUE!=StateIn)
		{
			DC_percentage=0U;
			(*counter)=0U;
		}
		else if((*counter)<TimeCloseing)
		{
			(*counter)++;
			DC_percentage = 100U;
		}
		else
		{
			DC_percentage = DCclosed;
		}
	}

	DC_ret = POM_DC_conversion(DC_percentage);

	return DC_ret;

}

static uint16 POM_DC_conversion(uint8 DC_percentage)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 Duty_out;


	if(100U>DC_percentage)
	{
		Duty_out = (uint16)(((uint32)DC_percentage*(uint32)POM_DUTY_MAX)/(uint32)100U);
	}
	else
	{
		Duty_out = (uint16)POM_DUTY_MAX;
	}

	return Duty_out;
}

static void POM_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	POM_dutyKeep[POM_RELAY_MONO]=(uint8)Rte_CData_CalDutyKeepRelayMonoClosed();
	POM_dutyKeep[POM_RELAY_PRECH]=(uint8)Rte_CData_CalDutyKeepOutputPrechargeRelaysClosed();


	POM_timeClose[POM_RELAY_MONO] = (uint16)(uint8)Rte_CData_CalTimeCloseRelayMono()*(uint16)POM_TIME_GAIN;
	POM_timeClose[POM_RELAY_PRECH] = (uint16)(uint8)Rte_CData_CalTimeCloseOutputPrechargeRelays()*(uint16)POM_TIME_GAIN;


	/*Period conversion*/
	POM_ledPeriod = (uint32)(uint8)Rte_CData_CalFrequencyPWM();
	if ((uint32)0U != POM_ledPeriod)
	{
		/*Update default LED frequency.*/
		POM_ledPeriod = (uint32)POM_PWM_CLK_FREQ/(POM_ledPeriod*(uint32)POM_FREQ_LED_GAIN);

	}

	POM_relayPeriod = (uint32)(uint8)Rte_CData_CalFrequencyActivateRelays();
	if((uint32)0U != POM_relayPeriod)
	{
		POM_relayPeriod = (uint32)POM_PWM_CLK_FREQ/(POM_relayPeriod*(uint32)POM_FREQ_RLY_GAIN);
	}

}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
