/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtHwAbsPIM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtHwAbsPIM
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtHwAbsPIM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup PIM
 * \ingroup BaseSw
 * \author Pablo Bolas
 * \brief PWM input manager.
 * \details This module is the interface for the peripheral TMI controlled by
 * the icp. Its main
 * function is to publish the control pilot duty cycle.
 *
 * \{
 * \file  PIM.c
 * \brief  Generic code of the PIM module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtHwAbsPIM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
#include "Icu_17_GtmCcu6.h"
#include "Dio.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/** CP Max period allowed.*/
#define PIM_PERIOD_MAX	210U
/** CP Min period allowed.*/
#define PIM_PERIOD_MIN	190U
 /* PRQA S 0857,0380 -- */
/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtHwAbsPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static non-init variables HERE... */

#define CtHwAbsPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtHwAbsPIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtHwAbsPIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtHwAbsPIM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtHwAbsPIM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtDutyControlPilot: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


#define CtHwAbsPIM_START_SEC_CODE
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsPIM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPIM_init_doc
 *********************************************************************************************************************/

/*!
 * \brief PIM initialization function.
 *
 * This function initializes the icp.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsPIM_CODE) RCtHwAbsPIM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPIM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	 Icu_17_GtmCcu6_StartSignalMeasurement(IcuConf_IcuChannel_IcuChannel_0);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsPIM_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDutyControlPilot_DeDutyControlPilot(IdtDutyControlPilot data)
 *   Std_ReturnType Rte_Write_PpFreqOutRange_DeFreqOutRange(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPIM_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief PIM task.
 *
 * This function is called periodically by the scheduler. This function should
 * read the measure result from the icp.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsPIM_CODE) RCtHwAbsPIM_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsPIM_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsPIM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsPIM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsPIM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsPIM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsPIM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsPIM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	volatile Icu_17_GtmCcu6_DutyCycleType PIM_icuValue_get;
	Icu_17_GtmCcu6_DutyCycleType PIM_icuValue;
	IdtDutyControlPilot DutyCycleCP;
	boolean FreqOutofRange;


	/*Safety ASILB -> BufferMarker should be set to 0xC8*/
	PIM_icuValue_get.BufferMarker = 0xC8;
	Icu_17_GtmCcu6_GetDutyCycleValues(IcuConf_IcuChannel_IcuChannel_0,&PIM_icuValue_get);/*PRQA S 0431 #The volatile qualifier is only used to avoid code optimization.*/
	PIM_icuValue = PIM_icuValue_get;
	/*If no data is available the return value is 0*/
	/*Check frequency*/
	if(((Icu_17_GtmCcu6_ValueType)PIM_PERIOD_MAX>=PIM_icuValue.PeriodTime)
		&&((Icu_17_GtmCcu6_ValueType)PIM_PERIOD_MIN<=PIM_icuValue.PeriodTime))
	{
		/*frequency in range*/
		FreqOutofRange = FALSE;

		if(PIM_icuValue.ActiveTime<=PIM_icuValue.PeriodTime)
		{
			DutyCycleCP = (IdtDutyControlPilot)((((uint64)100U*(uint64)PIM_icuValue.ActiveTime)+((uint64)PIM_icuValue.PeriodTime>>1))/(uint64)PIM_icuValue.PeriodTime);
		}
		else
		{
			DutyCycleCP = (IdtDutyControlPilot)100U;
		}
	}
	else
	{
		/*frequency not in range.*/

		/*Get pin state*/
		Dio_LevelType PinLevel = Dio_ReadChannel(DioConf_DioChannel_SUP_DI_CTRL_PILOT);

		/*Active time = 0 means, no new measure(No edge detected)*/
		if(0U!=PIM_icuValue.PeriodTime)
		{
			/*Not valid value*/
			DutyCycleCP = (IdtDutyControlPilot)0U;
			FreqOutofRange = TRUE;
		}
		else if(STD_HIGH==PinLevel)
		{
			/*Duty cycle == 100%*/
			DutyCycleCP = (IdtDutyControlPilot)100U;
			FreqOutofRange = FALSE;
		}
		else
		{
			/*Not valid value*/
			DutyCycleCP = (IdtDutyControlPilot)0U;
			FreqOutofRange = FALSE;
		}

	}

	(void)Rte_Write_PpDutyControlPilot_DeDutyControlPilot(DutyCycleCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpFreqOutRange_DeFreqOutRange(FreqOutofRange);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,5U,DutyCycleCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtHwAbsPIM_STOP_SEC_CODE
#include "CtHwAbsPIM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
