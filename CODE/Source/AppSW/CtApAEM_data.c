/*
 * File: CtApAEM_data.c
 *
 * Code generated for Simulink model 'CtApAEM'.
 *
 * Model version                  : 1.15
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Mon Nov 11 10:01:38 2019
 *
 * Target selection: autosar.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CtApAEM.h"
#include "CtApAEM_private.h"

/* Invariant block signals (auto storage) */
const ConstB_CtApAEM_T CtApAEM_ConstB = {
  100.0,                               /* '<S202>/Sum1' */
  100.0,                               /* '<S205>/MinMax1' */
  100.0,                               /* '<S205>/MinMax2' */
  100.0,                               /* '<S166>/Sum1' */
  100.0,                               /* '<S169>/MinMax1' */
  100.0                                /* '<S169>/MinMax2' */
};

/* Constant parameters (auto storage) */
const ConstP_CtApAEM_T CtApAEM_ConstP = {
  /* Pooled Parameter (Expression: [0 1;1 0;0 1;0 1;1 0;1 0;1 0;1 0])
   * Referenced by:
   *   '<S721>/Logic'
   *   '<S730>/Logic'
   *   '<S778>/Logic'
   *   '<S785>/Logic'
   *   '<S841>/Logic'
   *   '<S848>/Logic'
   *   '<S857>/Logic'
   *   '<S876>/Logic'
   *   '<S883>/Logic'
   *   '<S31>/Logic'
   *   '<S38>/Logic'
   *   '<S50>/Logic'
   *   '<S57>/Logic'
   *   '<S64>/Logic'
   *   '<S73>/Logic'
   *   '<S82>/Logic'
   *   '<S91>/Logic'
   *   '<S805>/Logic'
   *   '<S812>/Logic'
   *   '<S821>/Logic'
   *   '<S830>/Logic'
   *   '<S104>/Logic'
   *   '<S109>/Logic'
   *   '<S125>/Logic'
   *   '<S132>/Logic'
   *   '<S139>/Logic'
   *   '<S752>/Logic'
   *   '<S759>/Logic'
   *   '<S293>/Logic'
   *   '<S305>/Logic'
   *   '<S320>/Logic'
   *   '<S332>/Logic'
   *   '<S347>/Logic'
   *   '<S359>/Logic'
   *   '<S374>/Logic'
   *   '<S386>/Logic'
   *   '<S401>/Logic'
   *   '<S413>/Logic'
   *   '<S428>/Logic'
   *   '<S440>/Logic'
   *   '<S455>/Logic'
   *   '<S467>/Logic'
   *   '<S482>/Logic'
   *   '<S494>/Logic'
   *   '<S509>/Logic'
   *   '<S521>/Logic'
   *   '<S536>/Logic'
   *   '<S548>/Logic'
   *   '<S563>/Logic'
   *   '<S575>/Logic'
   *   '<S590>/Logic'
   *   '<S602>/Logic'
   *   '<S617>/Logic'
   *   '<S629>/Logic'
   *   '<S644>/Logic'
   *   '<S656>/Logic'
   *   '<S671>/Logic'
   *   '<S683>/Logic'
   *   '<S698>/Logic'
   *   '<S710>/Logic'
   */
  { 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0 }
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
