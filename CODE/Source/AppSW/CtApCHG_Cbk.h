/*
 * CtApCHG_Cbk.h
 *
 *  Created on: 10 oct. 2019
 *      Author: M0058239
 */

#ifndef CHG_CBK_H
#define CHG_CBK_H


#include "Platform_types.h"
#include "Compiler.h"

#include "Scc_Types.h"

void CHG_SCC_Set_StateM_StateMachineStatus(Scc_StateM_StateMachineStatusType StateMachineStatus);
void CHG_SCC_Set_StateM_EnergyTransferModeFlags(uint8 EnergyTransferModeFlags);
void CHG_SCC_Set_StateM_StateMachineError(Scc_StateM_StateMachineErrorType StateMachineError);
void CHG_SCC_Set_StateM_StateMachineMessageState(Scc_StateM_MsgStateType StateMachineMessageState);

void CHG_SCC_Set_Core_StackError(Scc_StackErrorType StackError);
void CHG_SCC_Set_Core_MsgStatus(Scc_MsgStatusType MsgStatus);
void CHG_SCC_Set_Core_TCPSocketState(Scc_TCPSocketStateType TCPSocketStateType);
void CHG_SCC_Set_Core_SAPSchemaID(Scc_SAPSchemaIDType SAPSchemaID);

void CHG_SCC_Set_SLAC_AssociationStatus(uint8 AssociationStatus);
void CHG_SCC_Set_SLAC_NMK(P2CONST(Scc_BufferPointerType, AUTOMATIC, SCC_APPL_DATA) NMKPtr);


void CHG_SCC_Set_IsoDin_DC_EVSEMaximumCurrentLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumCurrentLimit, Exi_BitType Flag);
void CHG_SCC_Set_IsoDin_DC_EVSEMaximumVoltageLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumVoltageLimit, Exi_BitType Flag);
void CHG_SCC_Set_IsoDin_DC_EVSEMaximumPowerLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumPowerLimit, Exi_BitType Flag);
void CHG_SCC_Set_IsoDin_DC_EVSEPresentCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPresentCurrent);

void CHG_SCC_Set_ISO_DC_EVSEStatusCode(Exi_ISO_DC_EVSEStatusCodeType EVSEStatusCode);
void CHG_SCC_Set_ISO_EVSENotification(Exi_ISO_EVSENotificationType EVSENotification);
void CHG_SCC_Set_ISO_ResponseCode(Exi_ISO_responseCodeType ResponseCode);
void CHG_SCC_Set_ISO_Notification(P2CONST(Exi_ISO_NotificationType,  AUTOMATIC, SCC_APPL_DATA) Notification, Exi_BitType Flag);
void CHG_SCC_Set_ISO_ChargeService(P2CONST(Exi_ISO_ChargeServiceType, AUTOMATIC, SCC_APPL_DATA) ChargeService);
void CHG_SCC_Set_ISO_SAScheduleList(P2CONST(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA) SAScheduleList);

void CHG_SCC_Set_DIN_EVSEStatusCode(Exi_DIN_DC_EVSEStatusCodeType EVSEStatusCode);
void CHG_SCC_Set_DIN_EVSENotification(Exi_DIN_EVSENotificationType EVSENotification);
void CHG_SCC_Set_DIN_ResponseCode(Exi_DIN_responseCodeType ResponseCode);
void CHG_SCC_Set_DIN_ChargeService(P2CONST(Exi_DIN_ServiceChargeType, AUTOMATIC, SCC_APPL_DATA) ChargeService);

void CHG_SCC_Get_Core_ForceSAPSchema(P2VAR(Scc_ForceSAPSchemasType, AUTOMATIC, SCC_APPL_DATA) ForceSAPSchema);
void CHG_SCC_Get_SLAC_QCAIdleTimer(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) QCAIdleTimer);
void CHG_SCC_Get_SLAC_StartMode(P2VAR(EthTrcv_30_Ar7000_Slac_StartModeType, AUTOMATIC, SCC_APPL_DATA) SLACStartMode);

void CHG_SCC_Get_StateM_EnergyTransferMode(P2VAR(Scc_StateM_EnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA) EnergyTransferMode);
void CHG_SCC_Get_StateM_ChargingControl(P2VAR(Scc_StateM_ChargingControlType, AUTOMATIC, SCC_APPL_DATA) ChargingControl);
void CHG_SCC_Get_StateM_ControlPilotState(P2VAR(Scc_StateM_ControlPilotStateType, AUTOMATIC, SCC_APPL_DATA) ControlPilotState);
void CHG_SCC_Get_StateM_PWMState(P2VAR(Scc_StateM_PWMStateType, AUTOMATIC, SCC_APPL_DATA) PWMState);
void CHG_SCC_Get_StateM_FunctionControl(P2VAR(Scc_StateM_FunctionControlType, AUTOMATIC, SCC_APPL_DATA) FunctionControl);

void CHG_SCC_Get_IsoDin_DC_EVMaximumCurrentLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumCurrentLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_EVMaximumVoltageLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumVoltageLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_EVMaximumPowerLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumPowerLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_EVEnergyCapacity(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVEnergyCapacity, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_EVEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_FullSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) FullSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_BulkSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) BulkSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_EVReady(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVReady);
void CHG_SCC_Get_IsoDin_DC_EVRESSSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) EVRESSSOC);
void CHG_SCC_Get_IsoDin_DC_EVTargetVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetVoltage);
void CHG_SCC_Get_IsoDin_DC_EVTargetCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetCurrent);
void CHG_SCC_Get_IsoDin_DC_ChargingComplete(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) ChargingComplete);
void CHG_SCC_Get_IsoDin_DC_BulkChargingComplete(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) BulkChargingComplete, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_SCC_Get_IsoDin_DC_EVPowerDeliveryParameterFlag(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVPowerDeliveryParameterFlag);

void CHG_SCC_Get_ISO_ChargeProgress(P2VAR(Exi_ISO_chargeProgressType, AUTOMATIC, SCC_APPL_DATA) ChargeProgress);
void CHG_SCC_Get_ISO_ChargingSession(P2VAR(Scc_ISO_ChargingSessionType, AUTOMATIC, SCC_APPL_DATA) ChargingSession);
void CHG_SCC_Get_ISO_RequestedEnergyTransferMode(P2VAR(Exi_ISO_EnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA) RequestedEnergyTransferMode);
void CHG_SCC_Get_ISO_DC_EVErrorCode(P2VAR(Exi_ISO_DC_EVErrorCodeType, AUTOMATIC, SCC_APPL_DATA) EVErrorCode);
void CHG_SCC_Get_ISO_SAScheduleTupleID(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) SAScheduleTupleID);
void CHG_SCC_Get_ISO_SelectedPaymentOption(P2VAR(Exi_ISO_paymentOptionType, AUTOMATIC, SCC_APPL_DATA) SelectedPaymentOption);
void CHG_SCC_Get_ISO_SelectedServiceListPtr(P2VAR(Exi_ISO_SelectedServiceListType, AUTOMATIC, SCC_APPL_DATA) SelectedServiceListPtr);

void CHG_SCC_Get_DIN_EVErrorCode(P2VAR(Exi_DIN_DC_EVErrorCodeType, AUTOMATIC, SCC_APPL_DATA) EVErrorCode);
void CHG_SCC_Get_DIN_RequestedEnergyTransferMode(P2VAR(Exi_DIN_EVRequestedEnergyTransferType, AUTOMATIC, SCC_APPL_DATA) RequestedEnergyTransferMode);

void CHG_Scc_Get_IsoDin_ParamNotUsed( P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);
void CHG_Scc_Get_IsoDin_TRUE( P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag);

#endif
