/*
 * File: CtApAEM.c
 *
 * Code generated for Simulink model 'CtApAEM'.
 *
 * Model version                  : 1.15
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Mon Nov 11 10:01:38 2019
 *
 * Target selection: autosar.tlc
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "CtApAEM.h"
#include "CtApAEM_private.h"

/* Named constants for Chart: '<S97>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' */
#define CtApAEM_IN_Defaut_actif        ((uint8_T)1U)
#define CtApAEM_IN_Defaut_inactif      ((uint8_T)2U)
#define CtApAEM_IN_NO_ACTIVE_CHILD     ((uint8_T)0U)
#define CtApAEM_active                 (true)
#define CtApAEM_inactive               (false)

/* Named constants for Chart: '<S14>/F01_05_01_Machine_etats_RCD' */
#define C_IN_Preparation_Mise_en_veille ((uint8_T)1U)
#define CtA_IN_Reveil_principal_degrade ((uint8_T)4U)
#define CtA_IN_Reveil_principal_nominal ((uint8_T)5U)
#define CtApAEM_DEGRADED_MAIN_WAKEUP   (5.0)
#define CtApAEM_IN_Reveil_partiel      ((uint8_T)3U)
#define CtApAEM_IN_Transitoire         ((uint8_T)6U)
#define CtApAEM_NOMINAL_MAIN_WAKEUP    (4.0)
#define CtApAEM_PARTIAL_WAKEUP         (1.0)
#define CtApAEM_SHUTDOWN_PREPARATION   (7.0)
#define CtApAEM_TRANSITORY_STATE       (3.0)
#define CtApA_IN_Reveil_Partiel_interne ((uint8_T)2U)

/* [Enrique.Bueno] Manual change START: adding MemMap includes in order
                   to map static variables to the correct Memory Partition (QM or ASILB) */
#define CtApAEM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApAEM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Block signals (auto storage) */
B_CtApAEM_T CtApAEM_B;

/* Block states (auto storage) */
DW_CtApAEM_T CtApAEM_DW;

#define CtApAEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApAEM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/* [Enrique.Bueno] Manual change STOP */

/*
 * System reset for atomic system:
 *    '<S27>/rising_edge'
 *    '<S27>/rising_edge1'
 *    '<S28>/rising_edge'
 *    '<S28>/rising_edge1'
 *    '<S45>/rising_edge'
 *    '<S45>/rising_edge1'
 *    '<S46>/rising_edge'
 *    '<S46>/rising_edge1'
 *    '<S47>/rising_edge'
 *    '<S47>/rising_edge1'
 *    ...
 */
void CtApAEM_rising_edge_Reset(DW_rising_edge_CtApAEM_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<S32>/UnitDelay' */
  localDW->UnitDelay_DSTATE = false;
}

/*
 * Output and update for atomic system:
 *    '<S27>/rising_edge'
 *    '<S27>/rising_edge1'
 *    '<S28>/rising_edge'
 *    '<S28>/rising_edge1'
 *    '<S45>/rising_edge'
 *    '<S45>/rising_edge1'
 *    '<S46>/rising_edge'
 *    '<S46>/rising_edge1'
 *    '<S47>/rising_edge'
 *    '<S47>/rising_edge1'
 *    ...
 */
void CtApAEM_rising_edge(boolean_T rtu_Signal, B_rising_edge_CtApAEM_T *localB,
  DW_rising_edge_CtApAEM_T *localDW)
{
  /* Logic: '<S32>/Logical Operator' incorporates:
   *  Logic: '<S32>/Logical Operator1'
   *  UnitDelay: '<S32>/UnitDelay'
   */
  localB->LogicalOperator = (rtu_Signal && (!localDW->UnitDelay_DSTATE));

  /* Update for UnitDelay: '<S32>/UnitDelay' */
  localDW->UnitDelay_DSTATE = rtu_Signal;
}

/*
 * System initialize for atomic system:
 *    '<S18>/TurnOnDelay1'
 *    '<S18>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay1'
 *    '<S19>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay3'
 *    '<S20>/TurnOnDelay'
 *    '<S21>/TurnOnDelay'
 *    '<S22>/TurnOnDelay'
 *    '<S96>/TurnOnDelay1'
 *    '<S115>/TurnOnDelay1'
 *    ...
 */
void CtApAEM_TurnOnDelay1_Init(DW_TurnOnDelay1_CtApAEM_T *localDW)
{
  /* InitializeConditions for Memory: '<S31>/Memory' */
  localDW->Memory_PreviousInput = true;
}

/*
 * System reset for atomic system:
 *    '<S18>/TurnOnDelay1'
 *    '<S18>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay1'
 *    '<S19>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay3'
 *    '<S20>/TurnOnDelay'
 *    '<S21>/TurnOnDelay'
 *    '<S22>/TurnOnDelay'
 *    '<S96>/TurnOnDelay1'
 *    '<S115>/TurnOnDelay1'
 *    ...
 */
void CtApAEM_TurnOnDelay1_Reset(DW_TurnOnDelay1_CtApAEM_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<S27>/UnitDelay2' */
  localDW->UnitDelay2_DSTATE = 0.0;

  /* InitializeConditions for UnitDelay: '<S27>/UnitDelay3' */
  localDW->UnitDelay3_DSTATE = false;

  /* InitializeConditions for Memory: '<S31>/Memory' */
  localDW->Memory_PreviousInput = true;

  /* SystemReset for Atomic SubSystem: '<S27>/rising_edge' */
  CtApAEM_rising_edge_Reset(&localDW->rising_edge);

  /* End of SystemReset for SubSystem: '<S27>/rising_edge' */

  /* SystemReset for Atomic SubSystem: '<S27>/rising_edge1' */
  CtApAEM_rising_edge_Reset(&localDW->rising_edge1);

  /* End of SystemReset for SubSystem: '<S27>/rising_edge1' */
}

/*
 * Output and update for atomic system:
 *    '<S18>/TurnOnDelay1'
 *    '<S18>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay1'
 *    '<S19>/TurnOnDelay2'
 *    '<S19>/TurnOnDelay3'
 *    '<S20>/TurnOnDelay'
 *    '<S21>/TurnOnDelay'
 *    '<S22>/TurnOnDelay'
 *    '<S96>/TurnOnDelay1'
 *    '<S115>/TurnOnDelay1'
 *    ...
 */
void CtApAEM_TurnOnDelay1(boolean_T rtu_In, real_T rtu_Tdly, real_T rtu_Te,
  B_TurnOnDelay1_CtApAEM_T *localB, DW_TurnOnDelay1_CtApAEM_T *localDW)
{
  int32_T rowIdx;
  real_T rtb_Switch1_g;
  boolean_T rtb_RelationalOperator3_l;
  real_T rtb_Switch1_i;
  real_T rtb_Sum4;

  /* Switch: '<S30>/Switch1' incorporates:
   *  Constant: '<S27>/Constant8'
   *  RelationalOperator: '<S30>/Relational Operator'
   */
  if (rtu_Tdly > 0.0) {
    rtb_Switch1_g = rtu_Tdly;
  } else {
    rtb_Switch1_g = 0.0;
  }

  /* End of Switch: '<S30>/Switch1' */

  /* RelationalOperator: '<S27>/Relational Operator3' */
  rtb_RelationalOperator3_l = (rtb_Switch1_g < rtu_Te);

  /* Outputs for Atomic SubSystem: '<S27>/rising_edge' */
  CtApAEM_rising_edge(rtu_In, &localB->rising_edge, &localDW->rising_edge);

  /* End of Outputs for SubSystem: '<S27>/rising_edge' */

  /* Switch: '<S27>/Switch' incorporates:
   *  Constant: '<S27>/Constant11'
   *  Constant: '<S27>/Constant14'
   *  Logic: '<S27>/Logical Operator4'
   *  Sum: '<S27>/Sum3'
   *  UnitDelay: '<S27>/UnitDelay3'
   */
  if (localDW->UnitDelay3_DSTATE || localB->rising_edge.LogicalOperator) {
    rtb_Sum4 = 0.0;
  } else {
    /* Sum: '<S27>/Sum4' incorporates:
     *  Constant: '<S27>/Constant13'
     *  Product: '<S27>/Divide1'
     */
    rtb_Sum4 = rtu_Te * 10.0 + rtb_Switch1_g;

    /* Outputs for Atomic SubSystem: '<S27>/DetectSat' */
    /* Switch: '<S34>/Switch1' incorporates:
     *  Constant: '<S27>/Constant10'
     *  RelationalOperator: '<S34>/Relational Operator'
     *  UnitDelay: '<S27>/UnitDelay2'
     */
    if (localDW->UnitDelay2_DSTATE > 0.0) {
      rtb_Switch1_i = localDW->UnitDelay2_DSTATE;
    } else {
      rtb_Switch1_i = 0.0;
    }

    /* End of Switch: '<S34>/Switch1' */

    /* Switch: '<S35>/Switch' incorporates:
     *  RelationalOperator: '<S35>/Relational Operator'
     */
    if (rtb_Sum4 < rtb_Switch1_i) {
      rtb_Switch1_i = rtb_Sum4;
    }

    /* End of Switch: '<S35>/Switch' */
    /* End of Outputs for SubSystem: '<S27>/DetectSat' */
    rtb_Sum4 = (2.2204460492503131E-16 + rtu_Te) + rtb_Switch1_i;
  }

  /* End of Switch: '<S27>/Switch' */

  /* Outputs for Atomic SubSystem: '<S27>/rising_edge1' */

  /* RelationalOperator: '<S27>/Relational Operator4' */
  CtApAEM_rising_edge(rtb_Sum4 >= rtb_Switch1_g, &localB->rising_edge1,
                      &localDW->rising_edge1);

  /* End of Outputs for SubSystem: '<S27>/rising_edge1' */

  /* CombinatorialLogic: '<S31>/Logic' incorporates:
   *  Logic: '<S27>/Logical Operator3'
   *  Logic: '<S27>/Logical Operator5'
   *  Memory: '<S31>/Memory'
   *  Switch: '<S27>/Switch2'
   */
  rowIdx = (int32_T)((((((!rtb_RelationalOperator3_l) &&
    localB->rising_edge1.LogicalOperator) || (!!rtb_RelationalOperator3_l)) +
                       ((uint32_T)!rtu_In << 1)) << 1) +
                     localDW->Memory_PreviousInput);
  localB->Logic[0U] = CtApAEM_ConstP.pooled42[(uint32_T)rowIdx];
  localB->Logic[1U] = CtApAEM_ConstP.pooled42[rowIdx + 8U];

  /* Update for UnitDelay: '<S27>/UnitDelay2' */
  localDW->UnitDelay2_DSTATE = rtb_Sum4;

  /* Update for UnitDelay: '<S27>/UnitDelay3' */
  localDW->UnitDelay3_DSTATE = rtb_RelationalOperator3_l;

  /* Update for Memory: '<S31>/Memory' */
  localDW->Memory_PreviousInput = localB->Logic[0];
}

/*
 * Output and update for atomic system:
 *    '<S101>/DetectSat'
 *    '<S166>/DetectSat1'
 *    '<S167>/DetectSat'
 *    '<S167>/DetectSat1'
 *    '<S178>/DetectSat'
 *    '<S178>/DetectSat1'
 *    '<S179>/DetectSat'
 *    '<S179>/DetectSat1'
 *    '<S190>/DetectSat'
 *    '<S190>/DetectSat1'
 *    ...
 */
void CtApAEM_DetectSat(real_T rtu_SatMax, real_T rtu_In, real_T rtu_SatMin,
  B_DetectSat_CtApAEM_T *localB)
{
  /* MinMax: '<S103>/MinMax2' incorporates:
   *  MinMax: '<S103>/MinMax1'
   */
	uint32 var_buffer;

		if (rtu_In > rtu_SatMin){
			var_buffer = rtu_In;
		} else {
			var_buffer = rtu_SatMin;
		}

		if (var_buffer > rtu_SatMax) {
			localB->MinMax2 = rtu_SatMax;
		} else {
			if (rtu_In > rtu_SatMin){
				localB->MinMax2 = rtu_In;
			} else {
				localB->MinMax2 = rtu_SatMin;
			}
		}
}

/*
 * System reset for atomic system:
 *    '<S101>/falling_edge1'
 *    '<S9>/falling_edge'
 */
void CtApAEM_falling_edge1_Reset(DW_falling_edge1_CtApAEM_T *localDW)
{
  /* InitializeConditions for UnitDelay: '<S105>/UnitDelay' */
  localDW->UnitDelay_DSTATE = false;
}

/*
 * Output and update for atomic system:
 *    '<S101>/falling_edge1'
 *    '<S9>/falling_edge'
 */
void CtApAEM_falling_edge1(boolean_T rtu_Signal, B_falling_edge1_CtApAEM_T
  *localB, DW_falling_edge1_CtApAEM_T *localDW)
{
  /* Logic: '<S105>/Logical Operator' incorporates:
   *  Logic: '<S105>/Logical Operator1'
   *  UnitDelay: '<S105>/UnitDelay'
   */
  localB->LogicalOperator = ((!rtu_Signal) && localDW->UnitDelay_DSTATE);

  /* Update for UnitDelay: '<S105>/UnitDelay' */
  localDW->UnitDelay_DSTATE = rtu_Signal;
}

/*
 * Output and update for atomic system:
 *    '<S148>/8Bit Decoder'
 *    '<S148>/8Bit Decoder1'
 *    '<S149>/8Bit Decoder1'
 *    '<S149>/8Bit Decoder2'
 */
void CtApAEM_uBitDecoder(int32_T rtu_U8in, B_uBitDecoder_CtApAEM_T *localB)
{
  real_T rtb_Switch8_ca;

  /* Assertion: '<S150>/Assertion' incorporates:
   *  RelationalOperator: '<S150>/Relational Operator15'
   */
  utAssert(rtu_U8in < 256);

  /* RelationalOperator: '<S150>/Relational Operator8' */
  localB->RelationalOperator8 = (rtu_U8in >= 128);

  /* Switch: '<S150>/Switch8' incorporates:
   *  Constant: '<S150>/Constant8'
   *  Sum: '<S150>/Sum8'
   */
  if (localB->RelationalOperator8) {
    rtb_Switch8_ca = (real_T)rtu_U8in - 128.0;
  } else {
    rtb_Switch8_ca = rtu_U8in;
  }

  /* End of Switch: '<S150>/Switch8' */

  /* RelationalOperator: '<S150>/Relational Operator9' incorporates:
   *  Constant: '<S150>/Constant9'
   */
  localB->RelationalOperator9 = (rtb_Switch8_ca >= 64.0);

  /* Switch: '<S150>/Switch9' incorporates:
   *  Constant: '<S150>/Constant9'
   *  Sum: '<S150>/Sum9'
   */
  if (localB->RelationalOperator9) {
    rtb_Switch8_ca -= 64.0;
  }

  /* End of Switch: '<S150>/Switch9' */

  /* RelationalOperator: '<S150>/Relational Operator10' incorporates:
   *  Constant: '<S150>/Constant10'
   */
  localB->RelationalOperator10 = (rtb_Switch8_ca >= 32.0);

  /* Switch: '<S150>/Switch10' incorporates:
   *  Constant: '<S150>/Constant10'
   *  Sum: '<S150>/Sum10'
   */
  if (localB->RelationalOperator10) {
    rtb_Switch8_ca -= 32.0;
  }

  /* End of Switch: '<S150>/Switch10' */

  /* RelationalOperator: '<S150>/Relational Operator11' incorporates:
   *  Constant: '<S150>/Constant11'
   */
  localB->RelationalOperator11 = (rtb_Switch8_ca >= 16.0);

  /* Switch: '<S150>/Switch11' incorporates:
   *  Constant: '<S150>/Constant11'
   *  Sum: '<S150>/Sum11'
   */
  if (localB->RelationalOperator11) {
    rtb_Switch8_ca -= 16.0;
  }

  /* End of Switch: '<S150>/Switch11' */

  /* RelationalOperator: '<S150>/Relational Operator12' incorporates:
   *  Constant: '<S150>/Constant12'
   */
  localB->RelationalOperator12 = (rtb_Switch8_ca >= 8.0);

  /* Switch: '<S150>/Switch12' incorporates:
   *  Constant: '<S150>/Constant12'
   *  Sum: '<S150>/Sum7'
   */
  if (localB->RelationalOperator12) {
    rtb_Switch8_ca -= 8.0;
  }

  /* End of Switch: '<S150>/Switch12' */

  /* RelationalOperator: '<S150>/Relational Operator13' incorporates:
   *  Constant: '<S150>/Constant13'
   */
  localB->RelationalOperator13 = (rtb_Switch8_ca >= 4.0);

  /* Switch: '<S150>/Switch13' incorporates:
   *  Constant: '<S150>/Constant13'
   *  Sum: '<S150>/Sum13'
   */
  if (localB->RelationalOperator13) {
    rtb_Switch8_ca -= 4.0;
  }

  /* End of Switch: '<S150>/Switch13' */

  /* RelationalOperator: '<S150>/Relational Operator14' incorporates:
   *  Constant: '<S150>/Constant14'
   */
  localB->RelationalOperator14 = (rtb_Switch8_ca >= 2.0);

  /* Switch: '<S150>/Switch14' incorporates:
   *  Constant: '<S150>/Constant14'
   *  Sum: '<S150>/Sum12'
   */
  if (localB->RelationalOperator14) {
    rtb_Switch8_ca -= 2.0;
  }

  /* End of Switch: '<S150>/Switch14' */

  /* DataTypeConversion: '<S150>/Data Type Conversion' */
  localB->DataTypeConversion = (rtb_Switch8_ca != 0.0);
}

/*
 * Output and update for atomic system:
 *    '<S148>/8Bit Encoder'
 *    '<S148>/8Bit Encoder1'
 */
void CtApAEM_uBitEncoder(boolean_T rtu_Bit7, boolean_T rtu_Bit6, boolean_T
  rtu_Bit5, boolean_T rtu_Bit4, boolean_T rtu_Bit3, boolean_T rtu_Bit2,
  boolean_T rtu_Bit1, boolean_T rtu_Bit0, B_uBitEncoder_CtApAEM_T *localB)
{
  /* Sum: '<S152>/Sum' incorporates:
   *  DataTypeConversion: '<S152>/Data Type Conversion10'
   *  DataTypeConversion: '<S152>/Data Type Conversion11'
   *  DataTypeConversion: '<S152>/Data Type Conversion12'
   *  DataTypeConversion: '<S152>/Data Type Conversion13'
   *  DataTypeConversion: '<S152>/Data Type Conversion14'
   *  DataTypeConversion: '<S152>/Data Type Conversion15'
   *  DataTypeConversion: '<S152>/Data Type Conversion8'
   *  DataTypeConversion: '<S152>/Data Type Conversion9'
   *  Gain: '<S152>/Gain10'
   *  Gain: '<S152>/Gain11'
   *  Gain: '<S152>/Gain12'
   *  Gain: '<S152>/Gain13'
   *  Gain: '<S152>/Gain14'
   *  Gain: '<S152>/Gain8'
   *  Gain: '<S152>/Gain9'
   */
  localB->Sum = (((((((rtu_Bit7 << 7) + (rtu_Bit6 << 6)) + (rtu_Bit5 << 5)) +
                    (rtu_Bit4 << 4)) + (rtu_Bit3 << 3)) + (rtu_Bit2 << 2)) +
                 (rtu_Bit1 << 1)) + rtu_Bit0;
}

/*
 * Output and update for atomic system:
 *    '<S156>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S157>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S159>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S160>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S161>/Gerer_etat_Reveil_partiel_maitre_Yj'
 *    '<S162>/Gerer_etat_Reveil_partiel_maitre_Yj'
 */
void Gerer_etat_Reveil_partiel_maitr(boolean_T rtu_UCE_bMstPtlWkuYjNd, boolean_T
  rtu_UCE_bMstPtlWkuYjHldReq, boolean_T rtu_UCE_bNomMainWkuAcv, int32_T
  rtu_UCE_stRCDSt, real_T rtu_UCE_tiMinTiMstPtlWkuYj, real_T
  rtu_UCE_tiMaxTiMstPtlWkuYj, B_Gerer_etat_Reveil_partiel_m_T *localB,
  DW_Gerer_etat_Reveil_partiel__T *localDW)
{
  boolean_T rtb_LogicalOperator4_j3;
  real_T rtb_Switch_n;
  real_T tmp;

  /* Outputs for Atomic SubSystem: '<S179>/DetectSat' */

  /* Constant: '<S179>/Constant5' incorporates:
   *  Constant: '<S179>/Constant7'
   */
  CtApAEM_DetectSat(10000.0, rtu_UCE_tiMinTiMstPtlWkuYj, 0.0, &localB->DetectSat);

  /* End of Outputs for SubSystem: '<S179>/DetectSat' */

  /* Logic: '<S175>/Logical Operator4' incorporates:
   *  Constant: '<S175>/PARTIAL_WAKEUP'
   *  RelationalOperator: '<S175>/Relational Operator7'
   *  UnitDelay: '<S175>/UnitDelay1'
   */
  rtb_LogicalOperator4_j3 = (localDW->UnitDelay_j && (rtu_UCE_stRCDSt == 1));

  /* Outputs for Atomic SubSystem: '<S179>/rising_edge' */
  CtApAEM_rising_edge(rtb_LogicalOperator4_j3, &localB->rising_edge,
                      &localDW->rising_edge);

  /* End of Outputs for SubSystem: '<S179>/rising_edge' */

  /* Switch: '<S179>/Switch' incorporates:
   *  Constant: '<S175>/UCE_Sldxxms_PdV1'
   *  Logic: '<S179>/Logical Operator'
   *  Sum: '<S179>/Sum1'
   *  Switch: '<S179>/Switch1'
   *  UnitDelay: '<S179>/UnitDelay1'
   */
  if (localB->rising_edge.LogicalOperator) {
    rtb_Switch_n = 0.01 + localB->DetectSat.MinMax2;
    tmp = 0.01 + localB->DetectSat.MinMax2;
  } else {
    rtb_Switch_n = localDW->UnitDelay1_DSTATE;
    tmp = localDW->UnitDelay1_DSTATE;
  }

  /* End of Switch: '<S179>/Switch' */

  /* Outputs for Atomic SubSystem: '<S178>/DetectSat' */

  /* Constant: '<S178>/Constant5' incorporates:
   *  Constant: '<S178>/Constant7'
   */
  CtApAEM_DetectSat(10000.0, rtu_UCE_tiMaxTiMstPtlWkuYj, 0.0,
                    &localB->DetectSat_h);

  /* End of Outputs for SubSystem: '<S178>/DetectSat' */

  /* Outputs for Atomic SubSystem: '<S178>/rising_edge' */
  CtApAEM_rising_edge(rtb_LogicalOperator4_j3, &localB->rising_edge_j,
                      &localDW->rising_edge_j);

  /* End of Outputs for SubSystem: '<S178>/rising_edge' */

  /* Switch: '<S178>/Switch1' incorporates:
   *  Constant: '<S175>/UCE_Sldxxms_PdV1'
   *  Constant: '<S178>/Constant3'
   *  Constant: '<S179>/Constant1'
   *  Constant: '<S179>/Constant6'
   *  Logic: '<S175>/Logical Operator1'
   *  Logic: '<S175>/Logical Operator2'
   *  Logic: '<S175>/Logical Operator7'
   *  Logic: '<S175>/Logical Operator8'
   *  Logic: '<S178>/Logical Operator2'
   *  RelationalOperator: '<S179>/Relational Operator'
   *  Sum: '<S179>/Sum2'
   *  UnitDelay: '<S175>/UnitDelay2'
   */
  if (!(rtu_UCE_bNomMainWkuAcv || (localDW->UnitDelay && (!((rtb_Switch_n -
           2.2204460492503131E-16) - 0.01 > 0.0)) &&
        (!rtu_UCE_bMstPtlWkuYjHldReq)))) {
    /* Switch: '<S178>/Switch' incorporates:
     *  Constant: '<S175>/UCE_Sldxxms_PdV'
     *  Logic: '<S178>/Logical Operator'
     *  Sum: '<S178>/Sum1'
     *  UnitDelay: '<S178>/UnitDelay1'
     */
    if (localB->rising_edge_j.LogicalOperator) {
      rtb_Switch_n = 0.01 + localB->DetectSat_h.MinMax2;
    } else {
      rtb_Switch_n = localDW->UnitDelay1_DSTATE_o;
    }

    /* End of Switch: '<S178>/Switch' */
  } else {
    rtb_Switch_n = 0.0;
  }

  /* End of Switch: '<S178>/Switch1' */

  /* Outputs for Atomic SubSystem: '<S178>/DetectSat1' */

  /* Sum: '<S178>/Sum2' incorporates:
   *  Constant: '<S175>/UCE_Sldxxms_PdV'
   *  Constant: '<S178>/Constant6'
   *  Constant: '<S178>/Constant8'
   */
  CtApAEM_DetectSat(localB->DetectSat_h.MinMax2, (rtb_Switch_n -
    2.2204460492503131E-16) - 0.01, 0.0, &localB->DetectSat1);

  /* End of Outputs for SubSystem: '<S178>/DetectSat1' */

  /* RelationalOperator: '<S178>/Relational Operator' incorporates:
   *  Constant: '<S175>/UCE_Sldxxms_PdV'
   *  Constant: '<S178>/Constant1'
   *  Constant: '<S178>/Constant6'
   *  Sum: '<S178>/Sum2'
   */
  localB->RelationalOperator = ((rtb_Switch_n - 2.2204460492503131E-16) - 0.01 >
    0.0);

  /* Outputs for Atomic SubSystem: '<S179>/DetectSat1' */

  /* Sum: '<S179>/Sum2' incorporates:
   *  Constant: '<S175>/UCE_Sldxxms_PdV1'
   *  Constant: '<S179>/Constant6'
   *  Constant: '<S179>/Constant8'
   */
  CtApAEM_DetectSat(localB->DetectSat.MinMax2, (tmp - 2.2204460492503131E-16) -
                    0.01, 0.0, &localB->DetectSat1_f);

  /* End of Outputs for SubSystem: '<S179>/DetectSat1' */

  /* Outputs for Atomic SubSystem: '<S175>/rising_edge' */

  /* Logic: '<S175>/Logical Operator6' incorporates:
   *  Logic: '<S175>/Logical Operator5'
   */
  CtApAEM_rising_edge(rtu_UCE_bMstPtlWkuYjNd && (!rtu_UCE_bNomMainWkuAcv),
                      &localB->rising_edge_p, &localDW->rising_edge_p);

  /* End of Outputs for SubSystem: '<S175>/rising_edge' */

  /* Update for UnitDelay: '<S175>/UnitDelay2' */
  localDW->UnitDelay = localB->RelationalOperator;

  /* Update for UnitDelay: '<S175>/UnitDelay1' */
  localDW->UnitDelay_j = localB->rising_edge_p.LogicalOperator;

  /* Update for UnitDelay: '<S179>/UnitDelay1' */
  localDW->UnitDelay1_DSTATE = localB->DetectSat1_f.MinMax2;

  /* Update for UnitDelay: '<S178>/UnitDelay1' */
  localDW->UnitDelay1_DSTATE_o = localB->DetectSat1.MinMax2;
}

/*
 * Output and update for atomic system:
 *    '<S261>/16Bit Decoder'
 *    '<S262>/16Bit Decoder'
 *    '<S262>/16Bit Decoder1'
 */
void CtApAEM_u6BitDecoder(int32_T rtu_U16in, B_u6BitDecoder_CtApAEM_T *localB)
{
  real_T rtb_Switch_do;

  /* Assertion: '<S263>/Assertion' incorporates:
   *  RelationalOperator: '<S263>/Relational Operator15'
   */
  utAssert(rtu_U16in < 65536);

  /* RelationalOperator: '<S263>/Relational Operator' */
  localB->RelationalOperator = (rtu_U16in >= 32768);

  /* Switch: '<S263>/Switch' incorporates:
   *  Constant: '<S263>/Constant'
   *  Sum: '<S263>/Sum'
   */
  if (localB->RelationalOperator) {
    rtb_Switch_do = (real_T)rtu_U16in - 32768.0;
  } else {
    rtb_Switch_do = rtu_U16in;
  }

  /* End of Switch: '<S263>/Switch' */

  /* RelationalOperator: '<S263>/Relational Operator1' incorporates:
   *  Constant: '<S263>/Constant1'
   */
  localB->RelationalOperator1 = (rtb_Switch_do >= 16384.0);

  /* Switch: '<S263>/Switch1' incorporates:
   *  Constant: '<S263>/Constant1'
   *  Sum: '<S263>/Sum1'
   */
  if (localB->RelationalOperator1) {
    rtb_Switch_do -= 16384.0;
  }

  /* End of Switch: '<S263>/Switch1' */

  /* RelationalOperator: '<S263>/Relational Operator2' incorporates:
   *  Constant: '<S263>/Constant2'
   */
  localB->RelationalOperator2 = (rtb_Switch_do >= 8192.0);

  /* Switch: '<S263>/Switch2' incorporates:
   *  Constant: '<S263>/Constant2'
   *  Sum: '<S263>/Sum2'
   */
  if (localB->RelationalOperator2) {
    rtb_Switch_do -= 8192.0;
  }

  /* End of Switch: '<S263>/Switch2' */

  /* RelationalOperator: '<S263>/Relational Operator3' incorporates:
   *  Constant: '<S263>/Constant3'
   */
  localB->RelationalOperator3 = (rtb_Switch_do >= 4096.0);

  /* Switch: '<S263>/Switch3' incorporates:
   *  Constant: '<S263>/Constant3'
   *  Sum: '<S263>/Sum3'
   */
  if (localB->RelationalOperator3) {
    rtb_Switch_do -= 4096.0;
  }

  /* End of Switch: '<S263>/Switch3' */

  /* RelationalOperator: '<S263>/Relational Operator4' incorporates:
   *  Constant: '<S263>/Constant4'
   */
  localB->RelationalOperator4 = (rtb_Switch_do >= 2048.0);

  /* Switch: '<S263>/Switch4' incorporates:
   *  Constant: '<S263>/Constant4'
   *  Sum: '<S263>/Sum4'
   */
  if (localB->RelationalOperator4) {
    rtb_Switch_do -= 2048.0;
  }

  /* End of Switch: '<S263>/Switch4' */

  /* RelationalOperator: '<S263>/Relational Operator5' incorporates:
   *  Constant: '<S263>/Constant5'
   */
  localB->RelationalOperator5 = (rtb_Switch_do >= 1024.0);

  /* Switch: '<S263>/Switch5' incorporates:
   *  Constant: '<S263>/Constant5'
   *  Sum: '<S263>/Sum5'
   */
  if (localB->RelationalOperator5) {
    rtb_Switch_do -= 1024.0;
  }

  /* End of Switch: '<S263>/Switch5' */

  /* RelationalOperator: '<S263>/Relational Operator6' incorporates:
   *  Constant: '<S263>/Constant6'
   */
  localB->RelationalOperator6 = (rtb_Switch_do >= 512.0);

  /* Switch: '<S263>/Switch6' incorporates:
   *  Constant: '<S263>/Constant6'
   *  Sum: '<S263>/Sum6'
   */
  if (localB->RelationalOperator6) {
    rtb_Switch_do -= 512.0;
  }

  /* End of Switch: '<S263>/Switch6' */

  /* RelationalOperator: '<S263>/Relational Operator7' incorporates:
   *  Constant: '<S263>/Constant7'
   */
  localB->RelationalOperator7 = (rtb_Switch_do >= 256.0);

  /* Switch: '<S263>/Switch7' incorporates:
   *  Constant: '<S263>/Constant7'
   *  Sum: '<S263>/Sum14'
   */
  if (localB->RelationalOperator7) {
    rtb_Switch_do -= 256.0;
  }

  /* End of Switch: '<S263>/Switch7' */

  /* RelationalOperator: '<S263>/Relational Operator8' incorporates:
   *  Constant: '<S263>/Constant8'
   */
  localB->RelationalOperator8 = (rtb_Switch_do >= 128.0);

  /* Switch: '<S263>/Switch8' incorporates:
   *  Constant: '<S263>/Constant8'
   *  Sum: '<S263>/Sum8'
   */
  if (localB->RelationalOperator8) {
    rtb_Switch_do -= 128.0;
  }

  /* End of Switch: '<S263>/Switch8' */

  /* RelationalOperator: '<S263>/Relational Operator9' incorporates:
   *  Constant: '<S263>/Constant9'
   */
  localB->RelationalOperator9 = (rtb_Switch_do >= 64.0);

  /* Switch: '<S263>/Switch9' incorporates:
   *  Constant: '<S263>/Constant9'
   *  Sum: '<S263>/Sum9'
   */
  if (localB->RelationalOperator9) {
    rtb_Switch_do -= 64.0;
  }

  /* End of Switch: '<S263>/Switch9' */

  /* RelationalOperator: '<S263>/Relational Operator10' incorporates:
   *  Constant: '<S263>/Constant10'
   */
  localB->RelationalOperator10 = (rtb_Switch_do >= 32.0);

  /* Switch: '<S263>/Switch10' incorporates:
   *  Constant: '<S263>/Constant10'
   *  Sum: '<S263>/Sum10'
   */
  if (localB->RelationalOperator10) {
    rtb_Switch_do -= 32.0;
  }

  /* End of Switch: '<S263>/Switch10' */

  /* RelationalOperator: '<S263>/Relational Operator11' incorporates:
   *  Constant: '<S263>/Constant11'
   */
  localB->RelationalOperator11 = (rtb_Switch_do >= 16.0);

  /* Switch: '<S263>/Switch11' incorporates:
   *  Constant: '<S263>/Constant11'
   *  Sum: '<S263>/Sum11'
   */
  if (localB->RelationalOperator11) {
    rtb_Switch_do -= 16.0;
  }

  /* End of Switch: '<S263>/Switch11' */

  /* RelationalOperator: '<S263>/Relational Operator12' incorporates:
   *  Constant: '<S263>/Constant12'
   */
  localB->RelationalOperator12 = (rtb_Switch_do >= 8.0);

  /* Switch: '<S263>/Switch12' incorporates:
   *  Constant: '<S263>/Constant12'
   *  Sum: '<S263>/Sum7'
   */
  if (localB->RelationalOperator12) {
    rtb_Switch_do -= 8.0;
  }

  /* End of Switch: '<S263>/Switch12' */

  /* RelationalOperator: '<S263>/Relational Operator13' incorporates:
   *  Constant: '<S263>/Constant13'
   */
  localB->RelationalOperator13 = (rtb_Switch_do >= 4.0);

  /* Switch: '<S263>/Switch13' incorporates:
   *  Constant: '<S263>/Constant13'
   *  Sum: '<S263>/Sum13'
   */
  if (localB->RelationalOperator13) {
    rtb_Switch_do -= 4.0;
  }

  /* End of Switch: '<S263>/Switch13' */

  /* RelationalOperator: '<S263>/Relational Operator14' incorporates:
   *  Constant: '<S263>/Constant14'
   */
  localB->RelationalOperator14 = (rtb_Switch_do >= 2.0);

  /* Switch: '<S263>/Switch14' incorporates:
   *  Constant: '<S263>/Constant14'
   *  Sum: '<S263>/Sum12'
   */
  if (localB->RelationalOperator14) {
    rtb_Switch_do -= 2.0;
  }

  /* End of Switch: '<S263>/Switch14' */

  /* DataTypeConversion: '<S263>/Data Type Conversion' */
  localB->DataTypeConversion = (rtb_Switch_do != 0.0);
}

/*
 * Output and update for atomic system:
 *    '<S261>/16Bit Encoder'
 *    '<S261>/16Bit Encoder1'
 */
void CtApAEM_u6BitEncoder(boolean_T rtu_Bit15, boolean_T rtu_Bit14, boolean_T
  rtu_Bit13, boolean_T rtu_Bit12, boolean_T rtu_Bit11, boolean_T rtu_Bit10,
  boolean_T rtu_Bit9, boolean_T rtu_Bit8, boolean_T rtu_Bit7, boolean_T rtu_Bit6,
  boolean_T rtu_Bit5, boolean_T rtu_Bit4, boolean_T rtu_Bit3, boolean_T rtu_Bit2,
  boolean_T rtu_Bit1, boolean_T rtu_Bit0, B_u6BitEncoder_CtApAEM_T *localB)
{
  /* Sum: '<S264>/Sum' incorporates:
   *  DataTypeConversion: '<S264>/Data Type Conversion'
   *  DataTypeConversion: '<S264>/Data Type Conversion1'
   *  DataTypeConversion: '<S264>/Data Type Conversion10'
   *  DataTypeConversion: '<S264>/Data Type Conversion11'
   *  DataTypeConversion: '<S264>/Data Type Conversion12'
   *  DataTypeConversion: '<S264>/Data Type Conversion13'
   *  DataTypeConversion: '<S264>/Data Type Conversion14'
   *  DataTypeConversion: '<S264>/Data Type Conversion15'
   *  DataTypeConversion: '<S264>/Data Type Conversion2'
   *  DataTypeConversion: '<S264>/Data Type Conversion3'
   *  DataTypeConversion: '<S264>/Data Type Conversion4'
   *  DataTypeConversion: '<S264>/Data Type Conversion5'
   *  DataTypeConversion: '<S264>/Data Type Conversion6'
   *  DataTypeConversion: '<S264>/Data Type Conversion7'
   *  DataTypeConversion: '<S264>/Data Type Conversion8'
   *  DataTypeConversion: '<S264>/Data Type Conversion9'
   *  Gain: '<S264>/Gain'
   *  Gain: '<S264>/Gain1'
   *  Gain: '<S264>/Gain10'
   *  Gain: '<S264>/Gain11'
   *  Gain: '<S264>/Gain12'
   *  Gain: '<S264>/Gain13'
   *  Gain: '<S264>/Gain14'
   *  Gain: '<S264>/Gain2'
   *  Gain: '<S264>/Gain3'
   *  Gain: '<S264>/Gain4'
   *  Gain: '<S264>/Gain5'
   *  Gain: '<S264>/Gain6'
   *  Gain: '<S264>/Gain7'
   *  Gain: '<S264>/Gain8'
   *  Gain: '<S264>/Gain9'
   */
  localB->Sum = (((((((((((((((rtu_Bit15 << 15) + (rtu_Bit14 << 14)) +
    (rtu_Bit13 << 13)) + (rtu_Bit12 << 12)) + (rtu_Bit11 << 11)) + (rtu_Bit10 <<
    10)) + (rtu_Bit9 << 9)) + (rtu_Bit8 << 8)) + (rtu_Bit7 << 7)) + (rtu_Bit6 <<
    6)) + (rtu_Bit5 << 5)) + (rtu_Bit4 << 4)) + (rtu_Bit3 << 3)) + (rtu_Bit2 <<
    2)) + (rtu_Bit1 << 1)) + rtu_Bit0;
}

/*
 * System initialize for atomic system:
 *    '<S267>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S268>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S269>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S270>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S271>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S272>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S273>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S274>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S275>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S276>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    ...
 */
void Gerer_etat_Reveil_partie_b_Init(DW_Gerer_etat_Reveil_partie_i_T *localDW)
{
  /* SystemInitialize for Atomic SubSystem: '<S287>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&localDW->TurnOnDelay);

  /* End of SystemInitialize for SubSystem: '<S287>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S289>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&localDW->TurnOnDelay_k);

  /* End of SystemInitialize for SubSystem: '<S289>/TurnOnDelay' */
}

/*
 * Output and update for atomic system:
 *    '<S267>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S268>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S269>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S270>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S271>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S272>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S273>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S274>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S275>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    '<S276>/Gerer_etat_Reveil_partiel_esclave_Xi'
 *    ...
 */
void Gerer_etat_Reveil_partiel_escla(boolean_T rtu_UCE_bSlavePtlWkuXiReq,
  boolean_T rtu_Ext_bRCDLine, int32_T rtu_UCE_stRCDSt, boolean_T
  rtu_UCE_bNomMainWkuAcv, boolean_T rtu_UCE_bSlavePtlWkuXiAcvMod, real_T
  rtu_UCE_tiPtlWkuXiAcv, real_T rtu_UCE_tiPtlWkuXiDeac, real_T
  rtu_UCE_tiPtlWkuXiLock, B_Gerer_etat_Reveil_partiel_e_T *localB,
  DW_Gerer_etat_Reveil_partie_i_T *localDW)
{
  real_T tmp;

  /* Outputs for Atomic SubSystem: '<S287>/TurnOnDelay' */

  /* Logic: '<S287>/Logical Operator12' incorporates:
   *  Constant: '<S287>/PARTIAL_WAKEUP'
   *  Constant: '<S287>/TRANSITORY_STATE'
   *  Constant: '<S287>/UCE_Sldxxms_PdV'
   *  Logic: '<S287>/Logical Operator1'
   *  Logic: '<S287>/Logical Operator2'
   *  Logic: '<S287>/Logical Operator3'
   *  Logic: '<S287>/Logical Operator4'
   *  RelationalOperator: '<S287>/Relational Operator1'
   *  RelationalOperator: '<S287>/Relational Operator2'
   *  RelationalOperator: '<S287>/Relational Operator3'
   *  RelationalOperator: '<S287>/Relational Operator7'
   */
  CtApAEM_TurnOnDelay1((rtu_Ext_bRCDLine || rtu_UCE_bSlavePtlWkuXiAcvMod) &&
                       rtu_UCE_bSlavePtlWkuXiReq && (((rtu_UCE_stRCDSt == 1) ||
    (rtu_UCE_stRCDSt == 3)) && (!rtu_UCE_bNomMainWkuAcv)), rtu_UCE_tiPtlWkuXiAcv,
                       0.01, &localB->TurnOnDelay, &localDW->TurnOnDelay);

  /* End of Outputs for SubSystem: '<S287>/TurnOnDelay' */

  /* Outputs for Atomic SubSystem: '<S289>/TurnOnDelay' */

  /* Logic: '<S289>/Logical Operator5' incorporates:
   *  Constant: '<S289>/PARTIAL_WAKEUP'
   *  Constant: '<S289>/UCE_Sldxxms_PdV'
   *  RelationalOperator: '<S289>/Relational Operator4'
   *  RelationalOperator: '<S289>/Relational Operator5'
   */
  CtApAEM_TurnOnDelay1((!rtu_UCE_bSlavePtlWkuXiReq) && (rtu_UCE_stRCDSt == 1),
                       rtu_UCE_tiPtlWkuXiDeac, 0.01, &localB->TurnOnDelay_k,
                       &localDW->TurnOnDelay_k);

  /* End of Outputs for SubSystem: '<S289>/TurnOnDelay' */

  /* Outputs for Atomic SubSystem: '<S298>/DetectSat' */

  /* Constant: '<S298>/Constant5' incorporates:
   *  Constant: '<S298>/Constant7'
   */
  CtApAEM_DetectSat(10000.0, rtu_UCE_tiPtlWkuXiLock, 0.0, &localB->DetectSat);

  /* End of Outputs for SubSystem: '<S298>/DetectSat' */

  /* Outputs for Atomic SubSystem: '<S298>/rising_edge' */

  /* Logic: '<S288>/Logical Operator2' incorporates:
   *  UnitDelay: '<S288>/UnitDelay'
   */
  CtApAEM_rising_edge(localB->TurnOnDelay.Logic[1] && (!localDW->UnitDelay),
                      &localB->rising_edge, &localDW->rising_edge);

  /* End of Outputs for SubSystem: '<S298>/rising_edge' */

  /* Switch: '<S298>/Switch1' incorporates:
   *  Constant: '<S298>/Constant3'
   *  Logic: '<S288>/Logical Operator1'
   *  Logic: '<S298>/Logical Operator2'
   */
  if (!(localB->TurnOnDelay_k.Logic[1] || rtu_UCE_bNomMainWkuAcv)) {
    /* Switch: '<S298>/Switch' incorporates:
     *  Constant: '<S288>/UCE_Sldxxms_PdV'
     *  Logic: '<S298>/Logical Operator'
     *  Sum: '<S298>/Sum1'
     *  UnitDelay: '<S298>/UnitDelay1'
     */
    if (localB->rising_edge.LogicalOperator) {
      tmp = 0.01 + localB->DetectSat.MinMax2;
    } else {
      tmp = localDW->UnitDelay1_DSTATE;
    }

    /* End of Switch: '<S298>/Switch' */
  } else {
    tmp = 0.0;
  }

  /* End of Switch: '<S298>/Switch1' */

  /* Outputs for Atomic SubSystem: '<S298>/DetectSat1' */

  /* Sum: '<S298>/Sum2' incorporates:
   *  Constant: '<S288>/UCE_Sldxxms_PdV'
   *  Constant: '<S298>/Constant6'
   *  Constant: '<S298>/Constant8'
   */
  CtApAEM_DetectSat(localB->DetectSat.MinMax2, (tmp - 2.2204460492503131E-16) -
                    0.01, 0.0, &localB->DetectSat1);

  /* End of Outputs for SubSystem: '<S298>/DetectSat1' */

  /* RelationalOperator: '<S298>/Relational Operator' incorporates:
   *  Constant: '<S288>/UCE_Sldxxms_PdV'
   *  Constant: '<S298>/Constant1'
   *  Constant: '<S298>/Constant6'
   *  Sum: '<S298>/Sum2'
   */
  localB->RelationalOperator = ((tmp - 2.2204460492503131E-16) - 0.01 > 0.0);

  /* Update for UnitDelay: '<S288>/UnitDelay' */
  localDW->UnitDelay = localB->RelationalOperator;

  /* Update for UnitDelay: '<S298>/UnitDelay1' */
  localDW->UnitDelay1_DSTATE = localB->DetectSat1.MinMax2;
}

/* Model step function for TID1 */
void RCtApAEM_task10msA(void)          /* Sample time: [0.01s, 0.0s] */
{
  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msA_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msA_sys'
   */

  /* Outport: '<Root>/PpAppRCDECUState_DeAppRCDECUState' incorporates:
   *  DataStoreRead: '<S1>/Data Store Read'
   *  DataTypeConversion: '<S1>/Data Type Conversion'
   */
  Rte_Write_PpAppRCDECUState_DeAppRCDECUState((IdtAppRCDECUState)
    CtApAEM_DW.RCDECUState);

  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msA_at_outport_1' */
}

/* Model step function for TID2 */
void RCtApAEM_task10msB(void)          /* Sample time: [0.01s, 0.0s] */
{
  int32_T rowIdx;
  boolean_T tmp;
  boolean_T tmp_0;
  ABS_VehSpd tmp_1;
  BSI_MainWakeup tmp_2;
  BSI_PostDriveWakeup tmp_3;
  BSI_PreDriveWakeup tmp_4;
  VCU_PrecondElecWakeup tmp_5;
  int32_T rtb_DataTypeConversion;
  boolean_T rtb_LogicalOperator1;
  boolean_T rtb_Switch2_ab;
  boolean_T rtb_LogicalOperator12_jr;
  boolean_T rtb_LogicalOperator5_mj;
  real_T rtb_Gain_ae;
  boolean_T rtb_LogicalOperator;
  boolean_T rtb_LogicalOperator1_f;
  boolean_T rtb_LogicalOperator4;
  boolean_T rtb_LogicalOperator12_l;
  boolean_T rtb_RelationalOperator1_e0;
  real_T rtb_Switch2;
  boolean_T rtb_LogicalOperator4_j2;
  boolean_T rtb_LogicalOperator2_kf;

  /* Inport: '<Root>/PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP' */
  Rte_Read_PpPI_STATE_INFO_WAKEUP_DePI_STATE_INFO_WAKEUP
    (&rtb_LogicalOperator4_j2);

  /* Inport: '<Root>/PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup' */
  Rte_Read_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup
    (&rtb_LogicalOperator1_f);

  /* Inport: '<Root>/PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup' */
  Rte_Read_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(&tmp_5);

  /* Inport: '<Root>/PpInt_SUPV_RCDLineState_SUPV_RCDLineState' */
  Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(&rtb_LogicalOperator1);

  /* Inport: '<Root>/PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup' */
  Rte_Read_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(&tmp_4);

  /* Inport: '<Root>/PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup' */
  Rte_Read_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(&tmp_3);

  /* Inport: '<Root>/PpInt_BSI_MainWakeup_BSI_MainWakeup' */
  Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&tmp_2);

  /* Inport: '<Root>/PpInt_ABS_VehSpd_ABS_VehSpd' */
  Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&tmp_1);

  /* Inport: '<Root>/PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP' */
  Rte_Read_PpHV_BATT_CHARGE_WAKEUP_DeHV_BATT_CHARGE_WAKEUP
    (&rtb_LogicalOperator2_kf);

  /* Inport: '<Root>/PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP' */
  Rte_Read_PpHOLD_DISCONTACTOR_WAKEUP_DeHOLD_DISCONTACTOR_WAKEUP(&tmp_0);

  /* Inport: '<Root>/PpCOOLING_WAKEUP_DeCOOLING_WAKEUP' */
  Rte_Read_PpCOOLING_WAKEUP_DeCOOLING_WAKEUP(&tmp);

  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* DataTypeConversion: '<S4>/Data Type Conversion' */
  rtb_DataTypeConversion = tmp_2;

  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */

  /* Inport: '<Root>/PpElectronicIntegrationRequest_DeElectronicIntegrationRequest' */
  Rte_Read_PpElectronicIntegrationRequest_DeElectronicIntegrationRequest
    (&rtb_LogicalOperator12_l);

  /* Inport: '<Root>/PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC' */
  Rte_Read_PpCAN1_COM_NEED_OBC_DeCAN1_COM_NEED_OBC(&rtb_LogicalOperator4);

  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* Outputs for Enabled SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' incorporates:
   *  EnablePort: '<S3>/Enable'
   */
  if (!CtApAEM_DW.F0_Gerer_PdV_EE_generique_MODE) {
    /* SystemReset for Atomic SubSystem: '<S9>/falling_edge' */
    CtApAEM_falling_edge1_Reset(&CtApAEM_DW.falling_edge);

    /* End of SystemReset for SubSystem: '<S9>/falling_edge' */

    /* SystemReset for Atomic SubSystem: '<S9>/BasculeRS' */
    /* InitializeConditions for UnitDelay: '<S888>/UnitDelay' */
    CtApAEM_DW.UnitDelay_DSTATE = false;

    /* End of SystemReset for SubSystem: '<S9>/BasculeRS' */
    CtApAEM_DW.F0_Gerer_PdV_EE_generique_MODE = true;
  }

  /* Outputs for Enabled SubSystem: '<S3>/F01_Gerer_PdV_RCD' incorporates:
   *  EnablePort: '<S6>/Enable'
   */
  /* Constant: '<S8>/UCE_noUCETyp_C' */
  if (Rte_CData_UCE_noUCETyp_C()) {
    if (!CtApAEM_DW.F01_Gerer_PdV_RCD_MODE) {
      /* InitializeConditions for UnitDelay: '<S6>/Unit Delay1' */
      CtApAEM_DW.UnitDelay1_DSTATE_o = 0;

      /* SystemReset for Atomic SubSystem: '<S18>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1);

      /* End of SystemReset for SubSystem: '<S18>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S18>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2);

      /* End of SystemReset for SubSystem: '<S18>/TurnOnDelay2' */

      /* SystemReset for Atomic SubSystem: '<S19>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_c);

      /* End of SystemReset for SubSystem: '<S19>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S19>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_k);

      /* End of SystemReset for SubSystem: '<S19>/TurnOnDelay2' */

      /* SystemReset for Atomic SubSystem: '<S19>/TurnOnDelay3' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay3);

      /* End of SystemReset for SubSystem: '<S19>/TurnOnDelay3' */

      /* SystemReset for Atomic SubSystem: '<S20>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay);

      /* End of SystemReset for SubSystem: '<S20>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S21>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_g);

      /* End of SystemReset for SubSystem: '<S21>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S22>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_a);

      /* End of SystemReset for SubSystem: '<S22>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S96>/TurnOffDelay' */
      /* InitializeConditions for UnitDelay: '<S101>/UnitDelay1' */
      CtApAEM_DW.UnitDelay1_DSTATE_i = 0.0;

      /* InitializeConditions for UnitDelay: '<S101>/UnitDelay2' */
      CtApAEM_DW.UnitDelay2_DSTATE_o = false;

      /* InitializeConditions for Memory: '<S104>/Memory' */
      CtApAEM_DW.Memory_PreviousInput = false;

      /* SystemReset for Atomic SubSystem: '<S101>/falling_edge1' */
      CtApAEM_falling_edge1_Reset(&CtApAEM_DW.falling_edge1);

      /* End of SystemReset for SubSystem: '<S101>/falling_edge1' */

      /* SystemReset for Atomic SubSystem: '<S101>/rising_edge' */
      CtApAEM_rising_edge_Reset(&CtApAEM_DW.rising_edge);

      /* End of SystemReset for SubSystem: '<S101>/rising_edge' */

      /* End of SystemReset for SubSystem: '<S96>/TurnOffDelay' */

      /* SystemReset for Atomic SubSystem: '<S96>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_h);

      /* End of SystemReset for SubSystem: '<S96>/TurnOnDelay1' */

      /* SystemReset for Chart: '<S97>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' */
      CtApAEM_DW.is_active_c1_CtApAEM = 0U;
      CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
      CtApAEM_B.UCE_bDgoMainWkuDisrd = false;

      /* SystemReset for Atomic SubSystem: '<S115>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_m);

      /* End of SystemReset for SubSystem: '<S115>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S115>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_e);

      /* End of SystemReset for SubSystem: '<S115>/TurnOnDelay2' */

      /* SystemReset for Atomic SubSystem: '<S115>/TurnOnDelay3' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay3_o);

      /* End of SystemReset for SubSystem: '<S115>/TurnOnDelay3' */

      /* SystemReset for Chart: '<S116>/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD' */
      CtApAEM_DW.is_active_c2_CtApAEM = 0U;
      CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
      CtApAEM_B.UCE_bDgoMainWkuIncst = false;

      /* SystemReset for Atomic SubSystem: '<S12>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_i);

      /* End of SystemReset for SubSystem: '<S12>/TurnOnDelay' */

      /* SystemReset for Atomic SubSystem: '<S13>/TurnOnDelay' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay_j);

      /* End of SystemReset for SubSystem: '<S13>/TurnOnDelay' */

      /* SystemReset for Chart: '<S14>/F01_05_01_Machine_etats_RCD' */
      CtApAEM_DW.is_active_c15_CtApAEM = 0U;
      CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
      CtApAEM_B.UCE_stRCDSt = 3;

      /* SystemReset for Atomic SubSystem: '<S17>/TurnOnDelay1' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_d);

      /* End of SystemReset for SubSystem: '<S17>/TurnOnDelay1' */

      /* SystemReset for Atomic SubSystem: '<S17>/TurnOnDelay2' */
      CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_g);

      /* End of SystemReset for SubSystem: '<S17>/TurnOnDelay2' */
      CtApAEM_DW.F01_Gerer_PdV_RCD_MODE = true;
    }

    /* Inport: '<Root>/PpDiagToolsRequest_DeDiagToolsRequest' */
    Rte_Read_PpDiagToolsRequest_DeDiagToolsRequest(&rtb_LogicalOperator5_mj);

    /* Logic: '<S18>/Logical Operator1' incorporates:
     *  Constant: '<S18>/PARTIAL_WAKEUP'
     *  Constant: '<S18>/TRANSITORY_STATE'
     *  RelationalOperator: '<S18>/Relational Operator2'
     *  RelationalOperator: '<S18>/Relational Operator5'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_Switch2_ab = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 1) ||
                      (CtApAEM_DW.UnitDelay1_DSTATE_o == 3));

    /* Logic: '<S18>/Logical Operator12' incorporates:
     *  Constant: '<S18>/INAVLID_MAIN_WKU_REQ'
     *  RelationalOperator: '<S18>/Relational Operator3'
     *  RelationalOperator: '<S896>/Relational Operator'
     */
    rtb_LogicalOperator12_jr = ((rtb_DataTypeConversion == 0) &&
      rtb_LogicalOperator1 && rtb_Switch2_ab);

    /* Outputs for Atomic SubSystem: '<S18>/TurnOnDelay1' */

    /* Logic: '<S18>/Logical Operator2' incorporates:
     *  Constant: '<S18>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S18>/UCE_Sldxxms_PdV'
     *  Constant: '<S18>/UCE_tiMainWkuAcv_C'
     *  Gain: '<S26>/Gain'
     *  RelationalOperator: '<S18>/Relational Operator4'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) && rtb_Switch2_ab, 0.01 *
                         (real_T)Rte_CData_UCE_tiMainWkuAcv_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay1, &CtApAEM_DW.TurnOnDelay1);

    /* End of Outputs for SubSystem: '<S18>/TurnOnDelay1' */

    /* Outputs for Atomic SubSystem: '<S18>/TurnOnDelay2' */

    /* Gain: '<S25>/Gain' incorporates:
     *  Constant: '<S18>/UCE_Sldxxms_PdV'
     *  Constant: '<S18>/UCE_tiMainTransForc_C'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_jr, 0.1 * (real_T)
                         Rte_CData_UCE_tiMainTransForc_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay2, &CtApAEM_DW.TurnOnDelay2);

    /* End of Outputs for SubSystem: '<S18>/TurnOnDelay2' */

    /* Logic: '<S18>/Logical Operator5' incorporates:
     *  Logic: '<S18>/Logical Operator4'
     */
    rtb_LogicalOperator5_mj = (CtApAEM_B.TurnOnDelay1.Logic[1] ||
      CtApAEM_B.TurnOnDelay2.Logic[1] || (rtb_LogicalOperator12_jr &&
      rtb_LogicalOperator5_mj));

    /* RelationalOperator: '<S19>/Relational Operator2' incorporates:
     *  Constant: '<S19>/NOMINAL_MAIN_WAKEUP'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_Switch2_ab = (CtApAEM_DW.UnitDelay1_DSTATE_o == 4);

    /* Outputs for Atomic SubSystem: '<S19>/TurnOnDelay1' */

    /* Logic: '<S19>/Logical Operator4' incorporates:
     *  Constant: '<S19>/INVALID_MAIN_WKU_REQ'
     *  Constant: '<S19>/UCE_Sldxxms_PdV'
     *  Constant: '<S19>/UCE_tiMainDisrdDet_C'
     *  Gain: '<S44>/Gain'
     *  RelationalOperator: '<S19>/Relational Operator4'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 0) && rtb_Switch2_ab, 0.1 *
                         (real_T)Rte_CData_UCE_tiMainDisrdDet_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay1_c, &CtApAEM_DW.TurnOnDelay1_c);

    /* End of Outputs for SubSystem: '<S19>/TurnOnDelay1' */

    /* Gain: '<S43>/Gain' incorporates:
     *  Constant: '<S19>/UCE_tiMainIncstDet_C'
     */
    rtb_Gain_ae = 0.1 * (real_T)Rte_CData_UCE_tiMainIncstDet_C();

    /* Outputs for Atomic SubSystem: '<S19>/TurnOnDelay2' */

    /* Logic: '<S19>/Logical Operator12' incorporates:
     *  Constant: '<S19>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S19>/UCE_Sldxxms_PdV'
     *  RelationalOperator: '<S19>/Relational Operator3'
     *  RelationalOperator: '<S896>/Relational Operator'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) && (!rtb_LogicalOperator1)
                         && rtb_Switch2_ab, rtb_Gain_ae, 0.01,
                         &CtApAEM_B.TurnOnDelay2_k, &CtApAEM_DW.TurnOnDelay2_k);

    /* End of Outputs for SubSystem: '<S19>/TurnOnDelay2' */

    /* Outputs for Atomic SubSystem: '<S19>/TurnOnDelay3' */

    /* Logic: '<S19>/Logical Operator2' incorporates:
     *  Constant: '<S19>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S19>/UCE_Sldxxms_PdV'
     *  Constant: '<S19>/UCE_spdThdNomDeac_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  RelationalOperator: '<S19>/Relational Operator5'
     *  RelationalOperator: '<S19>/Relational Operator6'
     */
    CtApAEM_TurnOnDelay1(rtb_Switch2_ab && (rtb_DataTypeConversion == 1) &&
                         (tmp_1 >= Rte_CData_UCE_spdThdNomDeac_C()), rtb_Gain_ae,
                         0.01, &CtApAEM_B.TurnOnDelay3, &CtApAEM_DW.TurnOnDelay3);

    /* End of Outputs for SubSystem: '<S19>/TurnOnDelay3' */

    /* Logic: '<S19>/Logical Operator3' */
    rtb_Switch2_ab = (CtApAEM_B.TurnOnDelay1_c.Logic[1] ||
                      CtApAEM_B.TurnOnDelay2_k.Logic[1] ||
                      CtApAEM_B.TurnOnDelay3.Logic[1]);

    /* Outputs for Atomic SubSystem: '<S20>/TurnOnDelay' */

    /* Logic: '<S20>/Logical Operator12' incorporates:
     *  Constant: '<S20>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S20>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S20>/UCE_Sldxxms_PdV'
     *  Constant: '<S20>/UCE_tiMainWkuReh_C'
     *  Gain: '<S69>/Gain'
     *  RelationalOperator: '<S20>/Relational Operator2'
     *  RelationalOperator: '<S20>/Relational Operator3'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 5) &&
                         rtb_LogicalOperator1, 0.1 * (real_T)
                         Rte_CData_UCE_tiMainWkuReh_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay, &CtApAEM_DW.TurnOnDelay);

    /* End of Outputs for SubSystem: '<S20>/TurnOnDelay' */

    /* Outputs for Atomic SubSystem: '<S21>/TurnOnDelay' */

    /* Logic: '<S21>/Logical Operator2' incorporates:
     *  Constant: '<S21>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S21>/INVALID_SPEED'
     *  Constant: '<S21>/NOMINAL_MAIN_WAKEUP'
     *  Constant: '<S21>/UCE_Sldxxms_PdV'
     *  Constant: '<S21>/UCE_spdThdNomDeac_C'
     *  Constant: '<S21>/UCE_tiNomMainWkuDeac_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  Gain: '<S78>/Gain'
     *  Logic: '<S21>/Logical Operator1'
     *  RelationalOperator: '<S21>/Relational Operator2'
     *  RelationalOperator: '<S21>/Relational Operator3'
     *  RelationalOperator: '<S21>/Relational Operator4'
     *  RelationalOperator: '<S21>/Relational Operator5'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 1) && ((tmp_1 <
      Rte_CData_UCE_spdThdNomDeac_C()) || (tmp_1 == 255)) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 4), 0.01 * (real_T)
                         Rte_CData_UCE_tiNomMainWkuDeac_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay_g, &CtApAEM_DW.TurnOnDelay_g);

    /* End of Outputs for SubSystem: '<S21>/TurnOnDelay' */

    /* Outputs for Atomic SubSystem: '<S22>/TurnOnDelay' */

    /* Logic: '<S22>/Logical Operator2' incorporates:
     *  Constant: '<S22>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S22>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S22>/INVALID_MAIN_WKU_REQ'
     *  Constant: '<S22>/INVALID_SPEED'
     *  Constant: '<S22>/UCE_Sldxxms_PdV'
     *  Constant: '<S22>/UCE_spdThdDegDeac_C'
     *  Constant: '<S22>/UCE_tiDegMainWkuDeac_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  Gain: '<S87>/Gain'
     *  Logic: '<S22>/Logical Operator1'
     *  Logic: '<S22>/Logical Operator12'
     *  RelationalOperator: '<S22>/Relational Operator1'
     *  RelationalOperator: '<S22>/Relational Operator2'
     *  RelationalOperator: '<S22>/Relational Operator3'
     *  RelationalOperator: '<S22>/Relational Operator4'
     *  RelationalOperator: '<S22>/Relational Operator5'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1(((rtb_DataTypeConversion == 0) ||
                          (rtb_DataTypeConversion == 1)) &&
                         (!rtb_LogicalOperator1) && ((tmp_1 <
      Rte_CData_UCE_spdThdDegDeac_C()) || (tmp_1 == 255)) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 5), 0.1 * (real_T)
                         Rte_CData_UCE_tiDegMainWkuDeac_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay_a, &CtApAEM_DW.TurnOnDelay_a);

    /* End of Outputs for SubSystem: '<S22>/TurnOnDelay' */

    /* RelationalOperator: '<S96>/Relational Operator4' incorporates:
     *  Constant: '<S96>/INVALID_MAIN_WKU_REQ'
     */
    rtb_LogicalOperator12_jr = (rtb_DataTypeConversion == 0);

    /* Logic: '<S96>/Logical Operator' */
    rtb_LogicalOperator = !rtb_LogicalOperator12_jr;

    /* Outputs for Atomic SubSystem: '<S96>/TurnOffDelay' */
    /* MinMax: '<S101>/MinMax1' incorporates:
     *  Constant: '<S96>/UCE_tiMainWkuReh_C'
     *  Gain: '<S100>/Gain'
     */
    rtb_Gain_ae = 0.1 * (real_T)Rte_CData_UCE_tiMainWkuReh_C();

    /* RelationalOperator: '<S101>/Relational Operator1' incorporates:
     *  Constant: '<S96>/UCE_Sldxxms_PdV'
     */
    rtb_RelationalOperator1_e0 = (rtb_Gain_ae < 0.01);

    /* Outputs for Atomic SubSystem: '<S101>/falling_edge1' */
    CtApAEM_falling_edge1(rtb_LogicalOperator, &CtApAEM_B.falling_edge1,
                          &CtApAEM_DW.falling_edge1);

    /* End of Outputs for SubSystem: '<S101>/falling_edge1' */

    /* Switch: '<S101>/Switch2' incorporates:
     *  Constant: '<S101>/Constant3'
     *  Constant: '<S101>/Constant4'
     *  Constant: '<S101>/Constant6'
     *  Constant: '<S101>/Constant7'
     *  Constant: '<S96>/UCE_Sldxxms_PdV'
     *  Logic: '<S101>/Logical Operator2'
     *  Product: '<S101>/Divide2'
     *  Sum: '<S101>/Sum1'
     *  Sum: '<S101>/Sum3'
     *  UnitDelay: '<S101>/UnitDelay1'
     *  UnitDelay: '<S101>/UnitDelay2'
     */
    if (CtApAEM_DW.UnitDelay2_DSTATE_o ||
        CtApAEM_B.falling_edge1.LogicalOperator) {
      rtb_Switch2 = 0.0;
    } else {
      /* Outputs for Atomic SubSystem: '<S101>/DetectSat' */
      CtApAEM_DetectSat(0.1 + rtb_Gain_ae, CtApAEM_DW.UnitDelay1_DSTATE_i, 0.0,
                        &CtApAEM_B.DetectSat);

      /* End of Outputs for SubSystem: '<S101>/DetectSat' */
      rtb_Switch2 = 0.010000000000000222 + CtApAEM_B.DetectSat.MinMax2;
    }

    /* End of Switch: '<S101>/Switch2' */

    /* Outputs for Atomic SubSystem: '<S101>/rising_edge' */

    /* RelationalOperator: '<S101>/Relational Operator2' */
    CtApAEM_rising_edge(rtb_Switch2 >= rtb_Gain_ae, &CtApAEM_B.rising_edge,
                        &CtApAEM_DW.rising_edge);

    /* End of Outputs for SubSystem: '<S101>/rising_edge' */

    /* CombinatorialLogic: '<S104>/Logic' incorporates:
     *  Logic: '<S101>/Logical Operator1'
     *  Memory: '<S104>/Memory'
     *  Switch: '<S101>/Switch'
     */
    rowIdx = (int32_T)((((((!rtb_RelationalOperator1_e0) &&
      CtApAEM_B.rising_edge.LogicalOperator) || (!!rtb_RelationalOperator1_e0))
                         + ((uint32_T)rtb_LogicalOperator << 1)) << 1) +
                       CtApAEM_DW.Memory_PreviousInput);

    /* Update for UnitDelay: '<S101>/UnitDelay1' */
    CtApAEM_DW.UnitDelay1_DSTATE_i = rtb_Switch2;

    /* Update for UnitDelay: '<S101>/UnitDelay2' */
    CtApAEM_DW.UnitDelay2_DSTATE_o = rtb_RelationalOperator1_e0;

    /* Update for Memory: '<S104>/Memory' incorporates:
     *  CombinatorialLogic: '<S104>/Logic'
     */
    CtApAEM_DW.Memory_PreviousInput = CtApAEM_ConstP.pooled42[(uint32_T)rowIdx];

    /* End of Outputs for SubSystem: '<S96>/TurnOffDelay' */

    /* Outputs for Atomic SubSystem: '<S96>/TurnOnDelay1' */

    /* Gain: '<S99>/Gain' incorporates:
     *  Constant: '<S96>/UCE_Sldxxms_PdV'
     *  Constant: '<S96>/UCE_tiMainDisrdDet_C'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_jr, 0.1 * (real_T)
                         Rte_CData_UCE_tiMainDisrdDet_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay1_h, &CtApAEM_DW.TurnOnDelay1_h);

    /* End of Outputs for SubSystem: '<S96>/TurnOnDelay1' */

    /* Chart: '<S97>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' incorporates:
     *  CombinatorialLogic: '<S104>/Logic'
     *  Constant: '<S96>/NOMINAL_MAIN_WAKEUP'
     *  Logic: '<S96>/Logical Operator1'
     *  RelationalOperator: '<S96>/Relational Operator1'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
    /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
    if (CtApAEM_DW.is_active_c1_CtApAEM == 0U) {
      /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
      CtApAEM_DW.is_active_c1_CtApAEM = 1U;

      /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_06_Traiter_Anomalie_Reveil_principal/F01_01_06_02_Renseigner_Anomalie_Reveil_principal_pour_GD/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD */
      /* Transition: '<S114>:4' */
      CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_Defaut_inactif;

      /* Entry 'Defaut_inactif': '<S114>:2' */
      CtApAEM_B.UCE_bDgoMainWkuDisrd = CtApAEM_inactive;
    } else if (CtApAEM_DW.is_c1_CtApAEM == CtApAEM_IN_Defaut_actif) {
      /* Outputs for Atomic SubSystem: '<S96>/TurnOffDelay' */
      /* During 'Defaut_actif': '<S114>:1' */
      if (CtApAEM_ConstP.pooled42[(uint32_T)rowIdx] &&
          (CtApAEM_DW.UnitDelay1_DSTATE_o == 4)) {
        /* Transition: '<S114>:5' */
        CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_Defaut_inactif;

        /* Entry 'Defaut_inactif': '<S114>:2' */
        CtApAEM_B.UCE_bDgoMainWkuDisrd = CtApAEM_inactive;
      }

      /* End of Outputs for SubSystem: '<S96>/TurnOffDelay' */
    } else {
      /* During 'Defaut_inactif': '<S114>:2' */
      if (CtApAEM_B.TurnOnDelay1_h.Logic[1]) {
        /* Transition: '<S114>:3' */
        CtApAEM_DW.is_c1_CtApAEM = CtApAEM_IN_Defaut_actif;

        /* Entry 'Defaut_actif': '<S114>:1' */
        CtApAEM_B.UCE_bDgoMainWkuDisrd = CtApAEM_active;
      }
    }

    /* End of Chart: '<S97>/F01_01_06_02_01_Automate_Anomalie_Reveil_principal_pour_GD' */

    /* Logic: '<S98>/Logical Operator1' incorporates:
     *  Constant: '<S98>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S98>/NOMINAL_MAIN_WAKEUP'
     *  RelationalOperator: '<S98>/Relational Operator1'
     *  RelationalOperator: '<S98>/Relational Operator2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_B.LogicalOperator1_g = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
      (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

    /* Gain: '<S118>/Gain' incorporates:
     *  Constant: '<S115>/UCE_tiMainIncstDet_C'
     */
    rtb_Gain_ae = 0.1 * (real_T)Rte_CData_UCE_tiMainIncstDet_C();

    /* Outputs for Atomic SubSystem: '<S115>/TurnOnDelay1' */

    /* Logic: '<S115>/Logical Operator12' incorporates:
     *  Constant: '<S115>/ACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S115>/UCE_Sldxxms_PdV'
     *  RelationalOperator: '<S115>/Relational Operator3'
     *  RelationalOperator: '<S896>/Relational Operator'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 2) && (!rtb_LogicalOperator1),
                         rtb_Gain_ae, 0.01, &CtApAEM_B.TurnOnDelay1_m,
                         &CtApAEM_DW.TurnOnDelay1_m);

    /* End of Outputs for SubSystem: '<S115>/TurnOnDelay1' */

    /* Outputs for Atomic SubSystem: '<S115>/TurnOnDelay2' */

    /* Logic: '<S115>/Logical Operator2' incorporates:
     *  Constant: '<S115>/INACTIVE_MAIN_WKU_REQ'
     *  Constant: '<S115>/UCE_Sldxxms_PdV'
     *  Constant: '<S115>/UCE_spdThdNomDeac_C'
     *  DataTypeConversion: '<S4>/Data Type Conversion1'
     *  RelationalOperator: '<S115>/Relational Operator5'
     *  RelationalOperator: '<S115>/Relational Operator6'
     */
    CtApAEM_TurnOnDelay1((rtb_DataTypeConversion == 1) && (tmp_1 >=
      Rte_CData_UCE_spdThdNomDeac_C()), rtb_Gain_ae, 0.01,
                         &CtApAEM_B.TurnOnDelay2_e, &CtApAEM_DW.TurnOnDelay2_e);

    /* End of Outputs for SubSystem: '<S115>/TurnOnDelay2' */

    /* Logic: '<S115>/Logical Operator1' */
    rtb_LogicalOperator12_jr = (CtApAEM_B.TurnOnDelay1_m.Logic[1] ||
      CtApAEM_B.TurnOnDelay2_e.Logic[1]);

    /* Outputs for Atomic SubSystem: '<S115>/TurnOnDelay3' */

    /* RelationalOperator: '<S896>/Relational Operator' incorporates:
     *  Constant: '<S115>/UCE_Sldxxms_PdV'
     *  Constant: '<S115>/UCE_tiMainWkuReh_C'
     *  Gain: '<S119>/Gain'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator1, 0.1 * (real_T)
                         Rte_CData_UCE_tiMainWkuReh_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay3_o, &CtApAEM_DW.TurnOnDelay3_o);

    /* End of Outputs for SubSystem: '<S115>/TurnOnDelay3' */

    /* Chart: '<S116>/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD' incorporates:
     *  Constant: '<S115>/NOMINAL_MAIN_WAKEUP'
     *  Logic: '<S115>/Logical Operator3'
     *  RelationalOperator: '<S115>/Relational Operator2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
    /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
    if (CtApAEM_DW.is_active_c2_CtApAEM == 0U) {
      /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
      CtApAEM_DW.is_active_c2_CtApAEM = 1U;

      /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_01_Gerer_Reveil_principal/F01_01_07_Traiter_Incoherence_Reveil_principal/F01_01_07_02_Renseigner_Incoherence_Reveil_principal_pour_GD/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD */
      /* Transition: '<S144>:4' */
      CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_Defaut_inactif;

      /* Entry 'Defaut_inactif': '<S144>:2' */
      CtApAEM_B.UCE_bDgoMainWkuIncst = CtApAEM_inactive;
    } else if (CtApAEM_DW.is_c2_CtApAEM == CtApAEM_IN_Defaut_actif) {
      /* During 'Defaut_actif': '<S144>:1' */
      if (CtApAEM_B.TurnOnDelay3_o.Logic[1] && (CtApAEM_DW.UnitDelay1_DSTATE_o ==
           4)) {
        /* Transition: '<S144>:5' */
        CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_Defaut_inactif;

        /* Entry 'Defaut_inactif': '<S144>:2' */
        CtApAEM_B.UCE_bDgoMainWkuIncst = CtApAEM_inactive;
      }
    } else {
      /* During 'Defaut_inactif': '<S144>:2' */
      if (rtb_LogicalOperator12_jr) {
        /* Transition: '<S144>:3' */
        CtApAEM_DW.is_c2_CtApAEM = CtApAEM_IN_Defaut_actif;

        /* Entry 'Defaut_actif': '<S144>:1' */
        CtApAEM_B.UCE_bDgoMainWkuIncst = CtApAEM_active;
      }
    }

    /* End of Chart: '<S116>/F01_01_07_02_01_Automate_Incoherence_Reveil_principal_pour_GD' */

    /* Logic: '<S117>/Logical Operator1' incorporates:
     *  Constant: '<S117>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S117>/NOMINAL_MAIN_WAKEUP'
     *  RelationalOperator: '<S117>/Relational Operator1'
     *  RelationalOperator: '<S117>/Relational Operator2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_B.LogicalOperator1_h = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
      (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

    /* Outputs for Enabled SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' incorporates:
     *  EnablePort: '<S145>/Enable'
     */
    if (Rte_CData_UCE_noUCETyp_C()) {
      if (!CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel) {
        CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel = true;
      }

      /* Outputs for Atomic SubSystem: '<S148>/8Bit Decoder' */

      /* S-Function (sfix_bitop): '<S897>/Bitwise AND' incorporates:
       *  ArithShift: '<S897>/Shift Arithmetic'
       *  ArithShift: '<S897>/Shift Arithmetic1'
       *  ArithShift: '<S897>/Shift Arithmetic2'
       *  DataTypeConversion: '<S897>/Data Type Conversion1'
       *  DataTypeConversion: '<S897>/Data Type Conversion2'
       *  DataTypeConversion: '<S897>/Data Type Conversion3'
       *  DataTypeConversion: '<S897>/Data Type Conversion4'
       */
      CtApAEM_uBitDecoder(rtb_LogicalOperator2_kf | rtb_LogicalOperator4_j2 << 1
                          | tmp_0 << 2 | tmp << 3, &CtApAEM_B.uBitDecoder);

      /* End of Outputs for SubSystem: '<S148>/8Bit Decoder' */

      /* Outputs for Atomic SubSystem: '<S148>/8Bit Decoder1' */

      /* S-Function (sfix_bitop): '<S898>/Bitwise AND' incorporates:
       *  ArithShift: '<S898>/Shift Arithmetic'
       *  ArithShift: '<S898>/Shift Arithmetic1'
       *  ArithShift: '<S898>/Shift Arithmetic2'
       *  DataTypeConversion: '<S898>/Data Type Conversion'
       *  DataTypeConversion: '<S898>/Data Type Conversion1'
       *  DataTypeConversion: '<S898>/Data Type Conversion2'
       *  DataTypeConversion: '<S898>/Data Type Conversion3'
       */
      CtApAEM_uBitDecoder(rtb_LogicalOperator2_kf | rtb_LogicalOperator4_j2 << 1
                          | tmp_0 << 2 | tmp << 3, &CtApAEM_B.uBitDecoder1);

      /* End of Outputs for SubSystem: '<S148>/8Bit Decoder1' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_08_Gerer_etat_Reveil_partiel_maitre_Y8' incorporates:
       *  EnablePort: '<S162>/Enable'
       */
      /* Logic: '<S154>/Logical Operator8' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY5_C1'
       *  Constant: '<S162>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S162>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY8_C()) {
        /* Outputs for Atomic SubSystem: '<S162>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator8,
           CtApAEM_B.uBitDecoder.RelationalOperator8, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY8_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY8_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_ma_ju,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_ma_ju);

        /* End of Outputs for SubSystem: '<S162>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S154>/Logical Operator8' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_08_Gerer_etat_Reveil_partiel_maitre_Y8' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_07_Gerer_etat_Reveil_partiel_maitre_Y7' incorporates:
       *  EnablePort: '<S161>/Enable'
       */
      /* Logic: '<S154>/Logical Operator7' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY4_C1'
       *  Constant: '<S161>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S161>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY7_C()) {
        /* Outputs for Atomic SubSystem: '<S161>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator9,
           CtApAEM_B.uBitDecoder.RelationalOperator9, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY7_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY7_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_l,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_l);

        /* End of Outputs for SubSystem: '<S161>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S154>/Logical Operator7' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_07_Gerer_etat_Reveil_partiel_maitre_Y7' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_06_Gerer_etat_Reveil_partiel_maitre_Y6' incorporates:
       *  EnablePort: '<S160>/Enable'
       */
      /* Logic: '<S154>/Logical Operator6' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY3_C1'
       *  Constant: '<S160>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S160>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY6_C()) {
        /* Outputs for Atomic SubSystem: '<S160>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator10,
           CtApAEM_B.uBitDecoder.RelationalOperator10, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY6_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY6_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_j,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_j);

        /* End of Outputs for SubSystem: '<S160>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S154>/Logical Operator6' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_06_Gerer_etat_Reveil_partiel_maitre_Y6' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_05_Gerer_etat_Reveil_partiel_maitre_Y5' incorporates:
       *  EnablePort: '<S159>/Enable'
       */
      /* Logic: '<S154>/Logical Operator5' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY5_C'
       *  Constant: '<S159>/UCE_tiMaxTiMstPtlWkuY5_C'
       *  Constant: '<S159>/UCE_tiMinTiMstPtlWkuY5_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY5_C()) {
        /* Outputs for Atomic SubSystem: '<S159>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator11,
           CtApAEM_B.uBitDecoder.RelationalOperator11, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY5_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY5_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_g,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_g);

        /* End of Outputs for SubSystem: '<S159>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S154>/Logical Operator5' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_05_Gerer_etat_Reveil_partiel_maitre_Y5' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_04_Gerer_etat_Reveil_partiel_maitre_Y4' incorporates:
       *  EnablePort: '<S158>/Enable'
       */
      /* Logic: '<S154>/Logical Operator4' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY4_C'
       *  Constant: '<S158>/UCE_tiMinTiMstPtlWkuY4_C'
       *  Constant: '<S199>/UCE_Sldxxms_PdV1'
       *  Constant: '<S202>/Constant6'
       *  Constant: '<S202>/Constant8'
       *  Constant: '<S203>/Constant5'
       *  Constant: '<S203>/Constant6'
       *  Constant: '<S203>/Constant7'
       *  Constant: '<S203>/Constant8'
       *  Logic: '<S199>/Logical Operator5'
       *  Logic: '<S199>/Logical Operator6'
       *  Sum: '<S202>/Sum2'
       *  Sum: '<S203>/Sum2'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY4_C()) {
        /* Outputs for Atomic SubSystem: '<S203>/DetectSat' */
        CtApAEM_DetectSat(10000.0, (real_T)Rte_CData_UCE_tiMinTiMstPtlWkuY4_C(),
                          0.0, &CtApAEM_B.DetectSat_l);

        /* End of Outputs for SubSystem: '<S203>/DetectSat' */

        /* Logic: '<S199>/Logical Operator4' incorporates:
         *  Constant: '<S158>/UCE_tiMinTiMstPtlWkuY4_C'
         *  Constant: '<S199>/PARTIAL_WAKEUP'
         *  Constant: '<S203>/Constant5'
         *  Constant: '<S203>/Constant7'
         *  RelationalOperator: '<S199>/Relational Operator7'
         *  UnitDelay: '<S199>/UnitDelay1'
         *  UnitDelay: '<S6>/Unit Delay1'
         */
        rtb_LogicalOperator4_j2 = (CtApAEM_DW.UnitDelay_o &&
          (CtApAEM_DW.UnitDelay1_DSTATE_o == 1));

        /* Outputs for Atomic SubSystem: '<S203>/rising_edge' */
        CtApAEM_rising_edge(rtb_LogicalOperator4_j2, &CtApAEM_B.rising_edge_h,
                            &CtApAEM_DW.rising_edge_h);

        /* End of Outputs for SubSystem: '<S203>/rising_edge' */

        /* Switch: '<S203>/Switch' incorporates:
         *  Constant: '<S199>/UCE_Sldxxms_PdV1'
         *  Logic: '<S203>/Logical Operator'
         *  Sum: '<S203>/Sum1'
         *  Switch: '<S203>/Switch1'
         *  UnitDelay: '<S203>/UnitDelay1'
         */
        if (CtApAEM_B.rising_edge_h.LogicalOperator) {
          rtb_Switch2 = 0.01 + CtApAEM_B.DetectSat_l.MinMax2;
          rtb_Gain_ae = 0.01 + CtApAEM_B.DetectSat_l.MinMax2;
        } else {
          rtb_Switch2 = CtApAEM_DW.UnitDelay1_DSTATE_d;
          rtb_Gain_ae = CtApAEM_DW.UnitDelay1_DSTATE_d;
        }

        /* End of Switch: '<S203>/Switch' */

        /* Outputs for Atomic SubSystem: '<S202>/rising_edge' */
        CtApAEM_rising_edge(rtb_LogicalOperator4_j2, &CtApAEM_B.rising_edge_i,
                            &CtApAEM_DW.rising_edge_i);

        /* End of Outputs for SubSystem: '<S202>/rising_edge' */

        /* Switch: '<S202>/Switch1' incorporates:
         *  Constant: '<S199>/UCE_Sldxxms_PdV1'
         *  Constant: '<S202>/Constant3'
         *  Constant: '<S203>/Constant1'
         *  Constant: '<S203>/Constant6'
         *  Logic: '<S199>/Logical Operator1'
         *  Logic: '<S199>/Logical Operator2'
         *  Logic: '<S199>/Logical Operator7'
         *  Logic: '<S199>/Logical Operator8'
         *  Logic: '<S202>/Logical Operator2'
         *  RelationalOperator: '<S203>/Relational Operator'
         *  Sum: '<S203>/Sum2'
         *  UnitDelay: '<S199>/UnitDelay2'
         */
        if (!(rtb_LogicalOperator5_mj || (CtApAEM_DW.UnitDelay &&
              (!((rtb_Switch2 - 2.2204460492503131E-16) - 0.01 > 0.0)) &&
              (!CtApAEM_B.uBitDecoder.RelationalOperator12)))) {
          /* Switch: '<S202>/Switch' incorporates:
           *  Logic: '<S202>/Logical Operator'
           *  UnitDelay: '<S202>/UnitDelay1'
           */
          if (CtApAEM_B.rising_edge_i.LogicalOperator) {
            rtb_Switch2 = CtApAEM_ConstB.Sum1;
          } else {
            rtb_Switch2 = CtApAEM_DW.UnitDelay1_DSTATE_l;
          }

          /* End of Switch: '<S202>/Switch' */
        } else {
          rtb_Switch2 = 0.0;
        }

        /* End of Switch: '<S202>/Switch1' */

        /* Outputs for Atomic SubSystem: '<S202>/DetectSat1' */
        CtApAEM_DetectSat(CtApAEM_ConstB.MinMax2, rtb_Switch2 -
                          2.2204460492503131E-16, 0.0, &CtApAEM_B.DetectSat1_l);

        /* End of Outputs for SubSystem: '<S202>/DetectSat1' */

        /* RelationalOperator: '<S202>/Relational Operator' incorporates:
         *  Constant: '<S202>/Constant1'
         *  Constant: '<S202>/Constant6'
         *  Constant: '<S202>/Constant8'
         *  Sum: '<S202>/Sum2'
         */
        CtApAEM_B.RelationalOperator = (rtb_Switch2 - 2.2204460492503131E-16 >
          0.0);

        /* Outputs for Atomic SubSystem: '<S203>/DetectSat1' */
        CtApAEM_DetectSat(CtApAEM_B.DetectSat_l.MinMax2, (rtb_Gain_ae -
          2.2204460492503131E-16) - 0.01, 0.0, &CtApAEM_B.DetectSat1_lb);

        /* End of Outputs for SubSystem: '<S203>/DetectSat1' */

        /* Outputs for Atomic SubSystem: '<S199>/rising_edge' */
        CtApAEM_rising_edge(CtApAEM_B.uBitDecoder1.RelationalOperator12 &&
                            (!rtb_LogicalOperator5_mj),
                            &CtApAEM_B.rising_edge_iu,
                            &CtApAEM_DW.rising_edge_iu);

        /* End of Outputs for SubSystem: '<S199>/rising_edge' */

        /* Update for UnitDelay: '<S199>/UnitDelay2' incorporates:
         *  Constant: '<S199>/UCE_Sldxxms_PdV1'
         *  Constant: '<S203>/Constant6'
         *  Constant: '<S203>/Constant8'
         *  Logic: '<S199>/Logical Operator5'
         *  Logic: '<S199>/Logical Operator6'
         *  Sum: '<S203>/Sum2'
         */
        CtApAEM_DW.UnitDelay = CtApAEM_B.RelationalOperator;

        /* Update for UnitDelay: '<S199>/UnitDelay1' */
        CtApAEM_DW.UnitDelay_o = CtApAEM_B.rising_edge_iu.LogicalOperator;

        /* Update for UnitDelay: '<S203>/UnitDelay1' */
        CtApAEM_DW.UnitDelay1_DSTATE_d = CtApAEM_B.DetectSat1_lb.MinMax2;

        /* Update for UnitDelay: '<S202>/UnitDelay1' */
        CtApAEM_DW.UnitDelay1_DSTATE_l = CtApAEM_B.DetectSat1_l.MinMax2;
      }

      /* End of Logic: '<S154>/Logical Operator4' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_04_Gerer_etat_Reveil_partiel_maitre_Y4' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_03_Gerer_etat_Reveil_partiel_maitre_Y3' incorporates:
       *  EnablePort: '<S157>/Enable'
       */
      /* Logic: '<S154>/Logical Operator3' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY3_C'
       *  Constant: '<S157>/UCE_tiMaxTiMstPtlWkuY3_C'
       *  Constant: '<S157>/UCE_tiMinTiMstPtlWkuY3_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY3_C()) {
        /* Outputs for Atomic SubSystem: '<S157>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator13,
           CtApAEM_B.uBitDecoder.RelationalOperator13, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY3_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY3_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_p,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_p);

        /* End of Outputs for SubSystem: '<S157>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S154>/Logical Operator3' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_03_Gerer_etat_Reveil_partiel_maitre_Y3' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_02_Gerer_etat_Reveil_partiel_maitre_Y2' incorporates:
       *  EnablePort: '<S156>/Enable'
       */
      /* Logic: '<S154>/Logical Operator2' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY2_C'
       *  Constant: '<S156>/UCE_tiMaxTiMstPtlWkuY2_C'
       *  Constant: '<S156>/UCE_tiMinTiMstPtlWkuY2_C'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY2_C()) {
        /* Outputs for Atomic SubSystem: '<S156>/Gerer_etat_Reveil_partiel_maitre_Yj' */
        Gerer_etat_Reveil_partiel_maitr
          (CtApAEM_B.uBitDecoder1.RelationalOperator14,
           CtApAEM_B.uBitDecoder.RelationalOperator14, rtb_LogicalOperator5_mj,
           CtApAEM_DW.UnitDelay1_DSTATE_o, (real_T)
           Rte_CData_UCE_tiMinTiMstPtlWkuY2_C(), (real_T)
           Rte_CData_UCE_tiMaxTiMstPtlWkuY2_C(),
           &CtApAEM_B.Gerer_etat_Reveil_partiel_mai_k,
           &CtApAEM_DW.Gerer_etat_Reveil_partiel_mai_k);

        /* End of Outputs for SubSystem: '<S156>/Gerer_etat_Reveil_partiel_maitre_Yj' */
      }

      /* End of Logic: '<S154>/Logical Operator2' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_02_Gerer_etat_Reveil_partiel_maitre_Y2' */

      /* Outputs for Enabled SubSystem: '<S148>/F01_02_01_01_01_Gerer_etat_Reveil_partiel_maitre_Y1' incorporates:
       *  EnablePort: '<S155>/Enable'
       */
      /* Logic: '<S154>/Logical Operator1' incorporates:
       *  Constant: '<S154>/UCE_bInhPtlWkuY1_C'
       *  Constant: '<S155>/UCE_tiMinTiMstPtlWkuY1_C'
       *  Constant: '<S163>/UCE_Sldxxms_PdV1'
       *  Constant: '<S166>/Constant6'
       *  Constant: '<S166>/Constant8'
       *  Constant: '<S167>/Constant5'
       *  Constant: '<S167>/Constant6'
       *  Constant: '<S167>/Constant7'
       *  Constant: '<S167>/Constant8'
       *  Logic: '<S163>/Logical Operator5'
       *  Logic: '<S163>/Logical Operator6'
       *  Sum: '<S166>/Sum2'
       *  Sum: '<S167>/Sum2'
       */
      if (!Rte_CData_UCE_bInhPtlWkuY1_C()) {
        /* Outputs for Atomic SubSystem: '<S167>/DetectSat' */
        CtApAEM_DetectSat(10000.0, (real_T)Rte_CData_UCE_tiMinTiMstPtlWkuY1_C(),
                          0.0, &CtApAEM_B.DetectSat_k);

        /* End of Outputs for SubSystem: '<S167>/DetectSat' */

        /* Logic: '<S163>/Logical Operator4' incorporates:
         *  Constant: '<S155>/UCE_tiMinTiMstPtlWkuY1_C'
         *  Constant: '<S163>/PARTIAL_WAKEUP'
         *  Constant: '<S167>/Constant5'
         *  Constant: '<S167>/Constant7'
         *  RelationalOperator: '<S163>/Relational Operator7'
         *  UnitDelay: '<S163>/UnitDelay1'
         *  UnitDelay: '<S6>/Unit Delay1'
         */
        rtb_LogicalOperator4_j2 = (CtApAEM_DW.UnitDelay_gx &&
          (CtApAEM_DW.UnitDelay1_DSTATE_o == 1));

        /* Outputs for Atomic SubSystem: '<S167>/rising_edge' */
        CtApAEM_rising_edge(rtb_LogicalOperator4_j2, &CtApAEM_B.rising_edge_n,
                            &CtApAEM_DW.rising_edge_n);

        /* End of Outputs for SubSystem: '<S167>/rising_edge' */

        /* Switch: '<S167>/Switch' incorporates:
         *  Constant: '<S163>/UCE_Sldxxms_PdV1'
         *  Logic: '<S167>/Logical Operator'
         *  Sum: '<S167>/Sum1'
         *  Switch: '<S167>/Switch1'
         *  UnitDelay: '<S167>/UnitDelay1'
         */
        if (CtApAEM_B.rising_edge_n.LogicalOperator) {
          rtb_Switch2 = 0.01 + CtApAEM_B.DetectSat_k.MinMax2;
          rtb_Gain_ae = 0.01 + CtApAEM_B.DetectSat_k.MinMax2;
        } else {
          rtb_Switch2 = CtApAEM_DW.UnitDelay1_DSTATE_e;
          rtb_Gain_ae = CtApAEM_DW.UnitDelay1_DSTATE_e;
        }

        /* End of Switch: '<S167>/Switch' */

        /* Outputs for Atomic SubSystem: '<S166>/rising_edge' */
        CtApAEM_rising_edge(rtb_LogicalOperator4_j2, &CtApAEM_B.rising_edge_k,
                            &CtApAEM_DW.rising_edge_k);

        /* End of Outputs for SubSystem: '<S166>/rising_edge' */

        /* Switch: '<S166>/Switch1' incorporates:
         *  Constant: '<S163>/UCE_Sldxxms_PdV1'
         *  Constant: '<S166>/Constant3'
         *  Constant: '<S167>/Constant1'
         *  Constant: '<S167>/Constant6'
         *  Logic: '<S163>/Logical Operator1'
         *  Logic: '<S163>/Logical Operator2'
         *  Logic: '<S163>/Logical Operator7'
         *  Logic: '<S163>/Logical Operator8'
         *  Logic: '<S166>/Logical Operator2'
         *  RelationalOperator: '<S167>/Relational Operator'
         *  Sum: '<S167>/Sum2'
         *  UnitDelay: '<S163>/UnitDelay2'
         */
        if (!(rtb_LogicalOperator5_mj || (CtApAEM_DW.UnitDelay_g &&
              (!((rtb_Switch2 - 2.2204460492503131E-16) - 0.01 > 0.0)) &&
              (!CtApAEM_B.uBitDecoder.DataTypeConversion)))) {
          /* Switch: '<S166>/Switch' incorporates:
           *  Logic: '<S166>/Logical Operator'
           *  UnitDelay: '<S166>/UnitDelay1'
           */
          if (CtApAEM_B.rising_edge_k.LogicalOperator) {
            rtb_Switch2 = CtApAEM_ConstB.Sum1_b;
          } else {
            rtb_Switch2 = CtApAEM_DW.UnitDelay1_DSTATE_j;
          }

          /* End of Switch: '<S166>/Switch' */
        } else {
          rtb_Switch2 = 0.0;
        }

        /* End of Switch: '<S166>/Switch1' */

        /* Outputs for Atomic SubSystem: '<S166>/DetectSat1' */
        CtApAEM_DetectSat(CtApAEM_ConstB.MinMax2_e, rtb_Switch2 -
                          2.2204460492503131E-16, 0.0, &CtApAEM_B.DetectSat1);

        /* End of Outputs for SubSystem: '<S166>/DetectSat1' */

        /* RelationalOperator: '<S166>/Relational Operator' incorporates:
         *  Constant: '<S166>/Constant1'
         *  Constant: '<S166>/Constant6'
         *  Constant: '<S166>/Constant8'
         *  Sum: '<S166>/Sum2'
         */
        CtApAEM_B.RelationalOperator_m = (rtb_Switch2 - 2.2204460492503131E-16 >
          0.0);

        /* Outputs for Atomic SubSystem: '<S167>/DetectSat1' */
        CtApAEM_DetectSat(CtApAEM_B.DetectSat_k.MinMax2, (rtb_Gain_ae -
          2.2204460492503131E-16) - 0.01, 0.0, &CtApAEM_B.DetectSat1_m);

        /* End of Outputs for SubSystem: '<S167>/DetectSat1' */

        /* Outputs for Atomic SubSystem: '<S163>/rising_edge' */
        CtApAEM_rising_edge(CtApAEM_B.uBitDecoder1.DataTypeConversion &&
                            (!rtb_LogicalOperator5_mj), &CtApAEM_B.rising_edge_m,
                            &CtApAEM_DW.rising_edge_m);

        /* End of Outputs for SubSystem: '<S163>/rising_edge' */

        /* Update for UnitDelay: '<S163>/UnitDelay2' incorporates:
         *  Constant: '<S163>/UCE_Sldxxms_PdV1'
         *  Constant: '<S167>/Constant6'
         *  Constant: '<S167>/Constant8'
         *  Logic: '<S163>/Logical Operator5'
         *  Logic: '<S163>/Logical Operator6'
         *  Sum: '<S167>/Sum2'
         */
        CtApAEM_DW.UnitDelay_g = CtApAEM_B.RelationalOperator_m;

        /* Update for UnitDelay: '<S163>/UnitDelay1' */
        CtApAEM_DW.UnitDelay_gx = CtApAEM_B.rising_edge_m.LogicalOperator;

        /* Update for UnitDelay: '<S167>/UnitDelay1' */
        CtApAEM_DW.UnitDelay1_DSTATE_e = CtApAEM_B.DetectSat1_m.MinMax2;

        /* Update for UnitDelay: '<S166>/UnitDelay1' */
        CtApAEM_DW.UnitDelay1_DSTATE_j = CtApAEM_B.DetectSat1.MinMax2;
      }

      /* End of Logic: '<S154>/Logical Operator1' */
      /* End of Outputs for SubSystem: '<S148>/F01_02_01_01_01_Gerer_etat_Reveil_partiel_maitre_Y1' */

      /* Outputs for Atomic SubSystem: '<S148>/8Bit Encoder' */
      CtApAEM_uBitEncoder
        (CtApAEM_B.Gerer_etat_Reveil_partiel_ma_ju.rising_edge_p.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_l.rising_edge_p.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_j.rising_edge_p.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_g.rising_edge_p.LogicalOperator,
         CtApAEM_B.rising_edge_iu.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_p.rising_edge_p.LogicalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_k.rising_edge_p.LogicalOperator,
         CtApAEM_B.rising_edge_m.LogicalOperator, &CtApAEM_B.uBitEncoder);

      /* End of Outputs for SubSystem: '<S148>/8Bit Encoder' */

      /* Outputs for Atomic SubSystem: '<S148>/8Bit Encoder1' */
      CtApAEM_uBitEncoder
        (CtApAEM_B.Gerer_etat_Reveil_partiel_ma_ju.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_l.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_j.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_g.RelationalOperator,
         CtApAEM_B.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_p.RelationalOperator,
         CtApAEM_B.Gerer_etat_Reveil_partiel_mai_k.RelationalOperator,
         CtApAEM_B.RelationalOperator_m, &CtApAEM_B.uBitEncoder1);

      /* End of Outputs for SubSystem: '<S148>/8Bit Encoder1' */

      /* Outputs for Atomic SubSystem: '<S149>/8Bit Decoder1' */
      CtApAEM_uBitDecoder(CtApAEM_B.uBitEncoder.Sum, &CtApAEM_B.uBitDecoder1_k);

      /* End of Outputs for SubSystem: '<S149>/8Bit Decoder1' */

      /* Logic: '<S149>/Logical Operator4' */
      CtApAEM_B.LogicalOperator4_b =
        (CtApAEM_B.uBitDecoder1_k.RelationalOperator8 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator9 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator10 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator11 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator12 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator13 ||
         CtApAEM_B.uBitDecoder1_k.RelationalOperator14 ||
         CtApAEM_B.uBitDecoder1_k.DataTypeConversion);

      /* Outputs for Atomic SubSystem: '<S149>/8Bit Decoder2' */
      CtApAEM_uBitDecoder(CtApAEM_B.uBitEncoder1.Sum, &CtApAEM_B.uBitDecoder2);

      /* End of Outputs for SubSystem: '<S149>/8Bit Decoder2' */

      /* Logic: '<S149>/Logical Operator2' incorporates:
       *  Constant: '<S149>/PARTIAL_WAKEUP'
       *  Logic: '<S149>/Logical Operator1'
       *  RelationalOperator: '<S149>/Relational Operator2'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      CtApAEM_B.LogicalOperator2 =
        ((!(CtApAEM_B.uBitDecoder2.RelationalOperator8 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator9 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator10 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator11 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator12 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator13 ||
            CtApAEM_B.uBitDecoder2.RelationalOperator14 ||
            CtApAEM_B.uBitDecoder2.DataTypeConversion)) &&
         (CtApAEM_DW.UnitDelay1_DSTATE_o == 1));
    } else {
      if (CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel) {
        /* Disable for Outport: '<S145>/UCE_bfMstPtlWkuReq' */
        CtApAEM_B.uBitEncoder1.Sum = 0;
        CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel = false;
      }
    }

    /* End of Outputs for SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' */

    /* Outputs for Atomic SubSystem: '<S261>/16Bit Decoder' */

    /* S-Function (sfix_bitop): '<S899>/Bitwise AND' incorporates:
     *  ArithShift: '<S899>/Shift Arithmetic'
     *  ArithShift: '<S899>/Shift Arithmetic1'
     *  ArithShift: '<S899>/Shift Arithmetic2'
     *  DataTypeConversion: '<S899>/Data Type Conversion1'
     *  DataTypeConversion: '<S899>/Data Type Conversion2'
     *  DataTypeConversion: '<S899>/Data Type Conversion3'
     *  DataTypeConversion: '<S899>/Data Type Conversion4'
     */
    CtApAEM_u6BitDecoder(rtb_LogicalOperator1_f | tmp_3 << 1 | tmp_4 << 2 |
                         tmp_5 << 3, &CtApAEM_B.u6BitDecoder);

    /* End of Outputs for SubSystem: '<S261>/16Bit Decoder' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' incorporates:
     *  EnablePort: '<S282>/Enable'
     */
    /* Logic: '<S266>/Logical Operator16' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C8'
     *  Constant: '<S282>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S282>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S282>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S282>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S689>/Gain'
     *  Gain: '<S690>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX16_C()) {
      /* Outputs for Atomic SubSystem: '<S282>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX16AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX16Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX16Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX16Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_c,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_c);

      /* End of Outputs for SubSystem: '<S282>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator16' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' incorporates:
     *  EnablePort: '<S281>/Enable'
     */
    /* Logic: '<S266>/Logical Operator15' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C7'
     *  Constant: '<S281>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S281>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S281>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S281>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S662>/Gain'
     *  Gain: '<S663>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX15_C()) {
      /* Outputs for Atomic SubSystem: '<S281>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator1,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX15AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX15Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX15Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX15Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_m4,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_m4);

      /* End of Outputs for SubSystem: '<S281>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator15' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' incorporates:
     *  EnablePort: '<S280>/Enable'
     */
    /* Logic: '<S266>/Logical Operator14' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C6'
     *  Constant: '<S280>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S280>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S280>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S280>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S635>/Gain'
     *  Gain: '<S636>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX14_C()) {
      /* Outputs for Atomic SubSystem: '<S280>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator2,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX14AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX14Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX14Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX14Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_gv,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_gv);

      /* End of Outputs for SubSystem: '<S280>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator14' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' incorporates:
     *  EnablePort: '<S279>/Enable'
     */
    /* Logic: '<S266>/Logical Operator13' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C5'
     *  Constant: '<S279>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S279>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S279>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S279>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S608>/Gain'
     *  Gain: '<S609>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX13_C()) {
      /* Outputs for Atomic SubSystem: '<S279>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator3,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX13AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX13Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX13Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX13Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_mj,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mj);

      /* End of Outputs for SubSystem: '<S279>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator13' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' incorporates:
     *  EnablePort: '<S278>/Enable'
     */
    /* Logic: '<S266>/Logical Operator12' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C4'
     *  Constant: '<S278>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S278>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S278>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S278>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S581>/Gain'
     *  Gain: '<S582>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX12_C()) {
      /* Outputs for Atomic SubSystem: '<S278>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator4,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX12AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX12Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX12Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX12Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_es_n2,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_n2);

      /* End of Outputs for SubSystem: '<S278>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator12' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' incorporates:
     *  EnablePort: '<S277>/Enable'
     */
    /* Logic: '<S266>/Logical Operator11' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C3'
     *  Constant: '<S277>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S277>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S277>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S277>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S554>/Gain'
     *  Gain: '<S555>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX11_C()) {
      /* Outputs for Atomic SubSystem: '<S277>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator5,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX11AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX11Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX11Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX11Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_d,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_d);

      /* End of Outputs for SubSystem: '<S277>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator11' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' incorporates:
     *  EnablePort: '<S276>/Enable'
     */
    /* Logic: '<S266>/Logical Operator10' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C2'
     *  Constant: '<S276>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S276>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S276>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S276>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S527>/Gain'
     *  Gain: '<S528>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX10_C()) {
      /* Outputs for Atomic SubSystem: '<S276>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator6,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX10AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX10Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX10Deac_C(), (real_T)
        Rte_CData_UCE_tiPtlWkuX10Lock_C(),
        &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_e,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_e);

      /* End of Outputs for SubSystem: '<S276>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator10' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' incorporates:
     *  EnablePort: '<S275>/Enable'
     */
    /* Logic: '<S266>/Logical Operator9' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C1'
     *  Constant: '<S275>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S275>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S275>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S275>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S500>/Gain'
     *  Gain: '<S501>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX9_C()) {
      /* Outputs for Atomic SubSystem: '<S275>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator7,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX9AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX9Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX9Deac_C(), (real_T)Rte_CData_UCE_tiPtlWkuX9Lock_C
        (), &CtApAEM_B.Gerer_etat_Reveil_partiel_es_hk,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_hk);

      /* End of Outputs for SubSystem: '<S275>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator9' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' incorporates:
     *  EnablePort: '<S274>/Enable'
     */
    /* Logic: '<S266>/Logical Operator7' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX8_C'
     *  Constant: '<S274>/UCE_bSlavePtlWkuX8AcvMod_C'
     *  Constant: '<S274>/UCE_tiPtlWkuX8Acv_C'
     *  Constant: '<S274>/UCE_tiPtlWkuX8Deac_C'
     *  Constant: '<S274>/UCE_tiPtlWkuX8Lock_C'
     *  Gain: '<S473>/Gain'
     *  Gain: '<S474>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX8_C()) {
      /* Outputs for Atomic SubSystem: '<S274>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator8,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX8AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX8Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX8Deac_C(), (real_T)Rte_CData_UCE_tiPtlWkuX8Lock_C
        (), &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_k,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_k);

      /* End of Outputs for SubSystem: '<S274>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator7' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' incorporates:
     *  EnablePort: '<S273>/Enable'
     */
    /* Logic: '<S266>/Logical Operator8' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX7_C'
     *  Constant: '<S273>/UCE_bSlavePtlWkuX7AcvMod_C'
     *  Constant: '<S273>/UCE_tiPtlWkuX7Acv_C'
     *  Constant: '<S273>/UCE_tiPtlWkuX7Deac_C'
     *  Constant: '<S273>/UCE_tiPtlWkuX7Lock_C'
     *  Gain: '<S446>/Gain'
     *  Gain: '<S447>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX7_C()) {
      /* Outputs for Atomic SubSystem: '<S273>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.RelationalOperator9,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX7AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX7Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX7Deac_C(), (real_T)Rte_CData_UCE_tiPtlWkuX7Lock_C
        (), &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_h,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_h);

      /* End of Outputs for SubSystem: '<S273>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator8' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' incorporates:
     *  EnablePort: '<S272>/Enable'
     */
    /* Logic: '<S266>/Logical Operator5' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX6_C'
     *  Constant: '<S272>/UCE_bSlavePtlWkuX6AcvMod_C'
     *  Constant: '<S272>/UCE_tiPtlWkuX6Acv_C'
     *  Constant: '<S272>/UCE_tiPtlWkuX6Deac_C'
     *  Constant: '<S272>/UCE_tiPtlWkuX6Lock_C'
     *  Gain: '<S419>/Gain'
     *  Gain: '<S420>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX6_C()) {
      /* Outputs for Atomic SubSystem: '<S272>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator10, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX6AcvMod_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX6Acv_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX6Deac_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX6Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_es_mk,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mk);

      /* End of Outputs for SubSystem: '<S272>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator5' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' incorporates:
     *  EnablePort: '<S271>/Enable'
     */
    /* Logic: '<S266>/Logical Operator6' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX5_C'
     *  Constant: '<S271>/UCE_bSlavePtlWkuX5AcvMod_C'
     *  Constant: '<S271>/UCE_tiPtlWkuX5Acv_C'
     *  Constant: '<S271>/UCE_tiPtlWkuX5Deac_C'
     *  Constant: '<S271>/UCE_tiPtlWkuX5Lock_C'
     *  Gain: '<S392>/Gain'
     *  Gain: '<S393>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX5_C()) {
      /* Outputs for Atomic SubSystem: '<S271>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator11, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX5AcvMod_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX5Acv_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX5Deac_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX5Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_a,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_a);

      /* End of Outputs for SubSystem: '<S271>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator6' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' incorporates:
     *  EnablePort: '<S270>/Enable'
     */
    /* Logic: '<S266>/Logical Operator3' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX4_C'
     *  Constant: '<S270>/UCE_bSlavePtlWkuX4AcvMod_C'
     *  Constant: '<S270>/UCE_tiPtlWkuX4Acv_C'
     *  Constant: '<S270>/UCE_tiPtlWkuX4Deac_C'
     *  Constant: '<S270>/UCE_tiPtlWkuX4Lock_C'
     *  Gain: '<S365>/Gain'
     *  Gain: '<S366>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX4_C()) {
      /* Outputs for Atomic SubSystem: '<S270>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator12, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX4AcvMod_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX4Acv_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX4Deac_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX4Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_j,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_j);

      /* End of Outputs for SubSystem: '<S270>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator3' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' incorporates:
     *  EnablePort: '<S269>/Enable'
     */
    /* Logic: '<S266>/Logical Operator4' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX3_C'
     *  Constant: '<S269>/UCE_bSlavePtlWkuX3AcvMod_C'
     *  Constant: '<S269>/UCE_tiPtlWkuX3Acv_C'
     *  Constant: '<S269>/UCE_tiPtlWkuX3Deac_C'
     *  Constant: '<S269>/UCE_tiPtlWkuX3Lock_C'
     *  Gain: '<S338>/Gain'
     *  Gain: '<S339>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX3_C()) {
      /* Outputs for Atomic SubSystem: '<S269>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator13, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX3AcvMod_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX3Acv_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX3Deac_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX3Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_m,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_m);

      /* End of Outputs for SubSystem: '<S269>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator4' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' incorporates:
     *  EnablePort: '<S268>/Enable'
     */
    /* Logic: '<S266>/Logical Operator1' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX2_C'
     *  Constant: '<S268>/UCE_bSlavePtlWkuX2AcvMod_C'
     *  Constant: '<S268>/UCE_tiPtlWkuX2Acv_C'
     *  Constant: '<S268>/UCE_tiPtlWkuX2Deac_C'
     *  Constant: '<S268>/UCE_tiPtlWkuX2Lock_C'
     *  Gain: '<S311>/Gain'
     *  Gain: '<S312>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX2_C()) {
      /* Outputs for Atomic SubSystem: '<S268>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla
        (CtApAEM_B.u6BitDecoder.RelationalOperator14, rtb_LogicalOperator1,
         CtApAEM_DW.UnitDelay1_DSTATE_o, rtb_LogicalOperator5_mj,
         Rte_CData_UCE_bSlavePtlWkuX2AcvMod_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX2Acv_C(), 0.01 * (real_T)
         Rte_CData_UCE_tiPtlWkuX2Deac_C(), (real_T)
         Rte_CData_UCE_tiPtlWkuX2Lock_C(),
         &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_n,
         &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_n);

      /* End of Outputs for SubSystem: '<S268>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator1' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' */

    /* Outputs for Enabled SubSystem: '<S261>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' incorporates:
     *  EnablePort: '<S267>/Enable'
     */
    /* Logic: '<S266>/Logical Operator2' incorporates:
     *  Constant: '<S266>/UCE_bInhPtlWkuX1_C'
     *  Constant: '<S267>/UCE_bSlavePtlWkuX1AcvMod_C'
     *  Constant: '<S267>/UCE_tiPtlWkuX1Acv_C'
     *  Constant: '<S267>/UCE_tiPtlWkuX1Deac_C'
     *  Constant: '<S267>/UCE_tiPtlWkuX1Lock_C'
     *  Gain: '<S284>/Gain'
     *  Gain: '<S285>/Gain'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (!Rte_CData_UCE_bInhPtlWkuX1_C()) {
      /* Outputs for Atomic SubSystem: '<S267>/Gerer_etat_Reveil_partiel_esclave_Xi' */
      Gerer_etat_Reveil_partiel_escla(CtApAEM_B.u6BitDecoder.DataTypeConversion,
        rtb_LogicalOperator1, CtApAEM_DW.UnitDelay1_DSTATE_o,
        rtb_LogicalOperator5_mj, Rte_CData_UCE_bSlavePtlWkuX1AcvMod_C(), 0.01 *
        (real_T)Rte_CData_UCE_tiPtlWkuX1Acv_C(), 0.01 * (real_T)
        Rte_CData_UCE_tiPtlWkuX1Deac_C(), (real_T)Rte_CData_UCE_tiPtlWkuX1Lock_C
        (), &CtApAEM_B.Gerer_etat_Reveil_partiel_esc_g,
        &CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_g);

      /* End of Outputs for SubSystem: '<S267>/Gerer_etat_Reveil_partiel_esclave_Xi' */
    }

    /* End of Logic: '<S266>/Logical Operator2' */
    /* End of Outputs for SubSystem: '<S261>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' */

    /* Outputs for Atomic SubSystem: '<S261>/16Bit Encoder' */
    CtApAEM_u6BitEncoder
      (CtApAEM_B.Gerer_etat_Reveil_partiel_esc_c.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_m4.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_gv.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mj.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_n2.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_d.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_e.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_hk.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_k.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_h.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mk.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_a.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_j.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_m.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_n.TurnOnDelay.Logic[1],
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_g.TurnOnDelay.Logic[1],
       &CtApAEM_B.u6BitEncoder);

    /* End of Outputs for SubSystem: '<S261>/16Bit Encoder' */

    /* Outputs for Atomic SubSystem: '<S261>/16Bit Encoder1' */
    CtApAEM_u6BitEncoder
      (CtApAEM_B.Gerer_etat_Reveil_partiel_esc_c.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_m4.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_gv.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mj.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_n2.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_d.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_e.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_hk.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_k.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_h.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_es_mk.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_a.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_j.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_m.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_n.RelationalOperator,
       CtApAEM_B.Gerer_etat_Reveil_partiel_esc_g.RelationalOperator,
       &CtApAEM_B.u6BitEncoder1);

    /* End of Outputs for SubSystem: '<S261>/16Bit Encoder1' */

    /* Outputs for Atomic SubSystem: '<S262>/16Bit Decoder' */
    CtApAEM_u6BitDecoder(CtApAEM_B.u6BitEncoder.Sum, &CtApAEM_B.u6BitDecoder_f);

    /* End of Outputs for SubSystem: '<S262>/16Bit Decoder' */

    /* Outputs for Atomic SubSystem: '<S262>/16Bit Decoder1' */
    CtApAEM_u6BitDecoder(CtApAEM_B.u6BitEncoder1.Sum, &CtApAEM_B.u6BitDecoder1);

    /* End of Outputs for SubSystem: '<S262>/16Bit Decoder1' */

    /* Logic: '<S147>/Logical Operator1' incorporates:
     *  Constant: '<S262>/TRANSITORY_STATE'
     *  Logic: '<S262>/Logical Operator2'
     *  Logic: '<S262>/Logical Operator3'
     *  RelationalOperator: '<S262>/Relational Operator7'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_LogicalOperator1_f = (CtApAEM_B.LogicalOperator4_b ||
      ((CtApAEM_DW.UnitDelay1_DSTATE_o == 3) &&
       (CtApAEM_B.u6BitDecoder_f.RelationalOperator ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator1 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator2 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator3 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator4 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator5 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator6 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator7 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator8 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator9 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator10 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator11 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator12 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator13 ||
        CtApAEM_B.u6BitDecoder_f.RelationalOperator14 ||
        CtApAEM_B.u6BitDecoder_f.DataTypeConversion)));

    /* Outputs for Atomic SubSystem: '<S12>/TurnOnDelay' */

    /* Logic: '<S12>/Logical Operator12' incorporates:
     *  Constant: '<S12>/TRANSITORY_STATE'
     *  Constant: '<S12>/UCE_Sldxxms_PdV'
     *  Constant: '<S12>/UCE_tiTransitoryDeac_C'
     *  Gain: '<S717>/Gain'
     *  Logic: '<S12>/Logical Operator1'
     *  Logic: '<S12>/Logical Operator2'
     *  RelationalOperator: '<S12>/Relational Operator7'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1((!rtb_LogicalOperator1) && (!rtb_LogicalOperator5_mj) &&
                         (!rtb_LogicalOperator1_f) &&
                         (CtApAEM_DW.UnitDelay1_DSTATE_o == 3), 0.1 * (real_T)
                         Rte_CData_UCE_tiTransitoryDeac_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay_i, &CtApAEM_DW.TurnOnDelay_i);

    /* End of Outputs for SubSystem: '<S12>/TurnOnDelay' */

    /* Outputs for Atomic SubSystem: '<S13>/TurnOnDelay' */

    /* RelationalOperator: '<S13>/Relational Operator7' incorporates:
     *  Constant: '<S13>/NTERNAL_PARTIAL_WAKEUP'
     *  Constant: '<S13>/UCE_Sldxxms_PdV'
     *  Constant: '<S13>/UCE_tiMaxTiIntPtlWku_C'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    CtApAEM_TurnOnDelay1(CtApAEM_DW.UnitDelay1_DSTATE_o == 2, (real_T)
                         Rte_CData_UCE_tiMaxTiIntPtlWku_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay_j, &CtApAEM_DW.TurnOnDelay_j);

    /* End of Outputs for SubSystem: '<S13>/TurnOnDelay' */

    /* Chart: '<S14>/F01_05_01_Machine_etats_RCD' incorporates:
     *  Constant: '<S262>/PARTIAL_WAKEUP'
     *  Logic: '<S13>/Logical Operator12'
     *  Logic: '<S13>/Logical Operator2'
     *  Logic: '<S13>/Logical Operator3'
     *  Logic: '<S147>/Logical Operator2'
     *  Logic: '<S147>/Logical Operator3'
     *  Logic: '<S262>/Logical Operator4'
     *  Logic: '<S262>/Logical Operator9'
     *  RelationalOperator: '<S262>/Relational Operator1'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  Switch: '<S147>/Switch2'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
    /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
    if (CtApAEM_DW.is_active_c15_CtApAEM == 0U) {
      /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
      CtApAEM_DW.is_active_c15_CtApAEM = 1U;

      /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_05_Calculer_ETAT_APPLICATIF_UCE_RCD/F01_05_01_Machine_etats_RCD */
      /* Transition: '<S735>:7' */
      CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

      /* Entry 'Transitoire': '<S735>:2' */
      CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
    } else {
      switch (CtApAEM_DW.is_c15_CtApAEM) {
       case C_IN_Preparation_Mise_en_veille:
        /* During 'Preparation_Mise_en_veille': '<S735>:1' */
        if ((!rtb_LogicalOperator1_f) && rtb_LogicalOperator1) {
          /* Transition: '<S735>:22' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

          /* Entry 'Transitoire': '<S735>:2' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
        } else {
          if (rtb_LogicalOperator1_f) {
            /* Transition: '<S735>:21' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Reveil_partiel;

            /* Entry 'Reveil_partiel': '<S735>:4' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_PARTIAL_WAKEUP;
          }
        }
        break;

       case CtApA_IN_Reveil_Partiel_interne:
        /* During 'Reveil_Partiel_interne': '<S735>:3' */
        if (((!rtb_LogicalOperator1) && (!rtb_LogicalOperator1_f)) ||
            CtApAEM_B.TurnOnDelay_j.Logic[1]) {
          /* Transition: '<S735>:13' */
          CtApAEM_DW.is_c15_CtApAEM = C_IN_Preparation_Mise_en_veille;

          /* Entry 'Preparation_Mise_en_veille': '<S735>:1' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_SHUTDOWN_PREPARATION;
        } else if (rtb_LogicalOperator1_f) {
          /* Transition: '<S735>:19' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Reveil_partiel;

          /* Entry 'Reveil_partiel': '<S735>:4' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_PARTIAL_WAKEUP;
        } else {
          if (rtb_LogicalOperator1) {
            /* Transition: '<S735>:14' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

            /* Entry 'Transitoire': '<S735>:2' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
          }
        }
        break;

       case CtApAEM_IN_Reveil_partiel:
        /* During 'Reveil_partiel': '<S735>:4' */
        if (((!Rte_CData_UCE_noUCETyp_C()) || CtApAEM_B.LogicalOperator2) &&
            ((CtApAEM_DW.UnitDelay1_DSTATE_o == 1) &&
             (!(CtApAEM_B.u6BitDecoder1.RelationalOperator ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator1 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator2 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator3 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator4 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator5 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator6 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator7 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator8 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator9 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator10 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator11 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator12 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator13 ||
                CtApAEM_B.u6BitDecoder1.RelationalOperator14 ||
                CtApAEM_B.u6BitDecoder1.DataTypeConversion))) &&
            (!rtb_LogicalOperator5_mj)) {
          /* Transition: '<S735>:18' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

          /* Entry 'Transitoire': '<S735>:2' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
        } else {
          if (rtb_LogicalOperator5_mj) {
            /* Transition: '<S735>:10' */
            CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_nominal;

            /* Entry 'Reveil_principal_nominal': '<S735>:5' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_NOMINAL_MAIN_WAKEUP;
          }
        }
        break;

       case CtA_IN_Reveil_principal_degrade:
        /* During 'Reveil_principal_degrade': '<S735>:6' */
        if (CtApAEM_B.TurnOnDelay.Logic[1]) {
          /* Transition: '<S735>:8' */
          CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_nominal;

          /* Entry 'Reveil_principal_nominal': '<S735>:5' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_NOMINAL_MAIN_WAKEUP;
        } else {
          if (CtApAEM_B.TurnOnDelay_a.Logic[1]) {
            /* Transition: '<S735>:17' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

            /* Entry 'Transitoire': '<S735>:2' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
          }
        }
        break;

       case CtA_IN_Reveil_principal_nominal:
        /* During 'Reveil_principal_nominal': '<S735>:5' */
        if (CtApAEM_B.TurnOnDelay_g.Logic[1]) {
          /* Transition: '<S735>:20' */
          CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Transitoire;

          /* Entry 'Transitoire': '<S735>:2' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_TRANSITORY_STATE;
        } else {
          if (rtb_Switch2_ab) {
            /* Transition: '<S735>:9' */
            CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_degrade;

            /* Entry 'Reveil_principal_degrade': '<S735>:6' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_DEGRADED_MAIN_WAKEUP;
          }
        }
        break;

       default:
        /* During 'Transitoire': '<S735>:2' */
        if (CtApAEM_B.TurnOnDelay_i.Logic[1]) {
          /* Transition: '<S735>:12' */
          CtApAEM_DW.is_c15_CtApAEM = C_IN_Preparation_Mise_en_veille;

          /* Entry 'Preparation_Mise_en_veille': '<S735>:1' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_SHUTDOWN_PREPARATION;
        } else if (rtb_LogicalOperator5_mj) {
          /* Transition: '<S735>:11' */
          CtApAEM_DW.is_c15_CtApAEM = CtA_IN_Reveil_principal_nominal;

          /* Entry 'Reveil_principal_nominal': '<S735>:5' */
          CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_NOMINAL_MAIN_WAKEUP;
        } else {
          if (rtb_LogicalOperator1_f) {
            /* Transition: '<S735>:15' */
            CtApAEM_DW.is_c15_CtApAEM = CtApAEM_IN_Reveil_partiel;

            /* Entry 'Reveil_partiel': '<S735>:4' */
            CtApAEM_B.UCE_stRCDSt = (int32_T)CtApAEM_PARTIAL_WAKEUP;
          }
        }
        break;
      }
    }

    /* End of Chart: '<S14>/F01_05_01_Machine_etats_RCD' */

    /* Outputs for Enabled SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' incorporates:
     *  EnablePort: '<S15>/Enable'
     */
    if (Rte_CData_UCE_noUCETyp_C()) {
      if (!CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_) {
        /* InitializeConditions for UnitDelay: '<S15>/Unit Delay7' */
        CtApAEM_DW.UnitDelay7_DSTATE = false;

        /* InitializeConditions for UnitDelay: '<S736>/Unit Delay2' */
        CtApAEM_DW.UnitDelay2_DSTATE = false;

        /* InitializeConditions for UnitDelay: '<S739>/UnitDelay1' */
        CtApAEM_DW.UnitDelay1_DSTATE = 0.0;

        /* SystemReset for Atomic SubSystem: '<S743>/TurnOnDelay1' */
        CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay1_df);

        /* End of SystemReset for SubSystem: '<S743>/TurnOnDelay1' */

        /* SystemReset for Atomic SubSystem: '<S743>/TurnOnDelay2' */
        CtApAEM_TurnOnDelay1_Reset(&CtApAEM_DW.TurnOnDelay2_n);

        /* End of SystemReset for SubSystem: '<S743>/TurnOnDelay2' */

        /* SystemReset for Chart: '<S744>/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD' */
        CtApAEM_DW.is_active_c5_CtApAEM = 0U;
        CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_NO_ACTIVE_CHILD;
        CtApAEM_B.UCE_bDgoRCDLineScg = false;

        /* SystemReset for Atomic SubSystem: '<S739>/rising_edge' */
        CtApAEM_rising_edge_Reset(&CtApAEM_DW.rising_edge_e);

        /* End of SystemReset for SubSystem: '<S739>/rising_edge' */
        CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_ = true;
      }

      /* Outputs for Atomic SubSystem: '<S743>/TurnOnDelay1' */

      /* Logic: '<S743>/Logical Operator4' incorporates:
       *  Constant: '<S743>/UCE_Sldxxms_PdV'
       *  Constant: '<S743>/UCE_tiRCDLineScgDet_C'
       *  Gain: '<S746>/Gain'
       *  RelationalOperator: '<S896>/Relational Operator'
       *  UnitDelay: '<S15>/Unit Delay7'
       */
      CtApAEM_TurnOnDelay1((!rtb_LogicalOperator1) &&
                           CtApAEM_DW.UnitDelay7_DSTATE, 0.01 * (real_T)
                           Rte_CData_UCE_tiRCDLineScgDet_C(), 0.01,
                           &CtApAEM_B.TurnOnDelay1_df,
                           &CtApAEM_DW.TurnOnDelay1_df);

      /* End of Outputs for SubSystem: '<S743>/TurnOnDelay1' */

      /* Outputs for Atomic SubSystem: '<S743>/TurnOnDelay2' */

      /* RelationalOperator: '<S896>/Relational Operator' incorporates:
       *  Constant: '<S743>/UCE_Sldxxms_PdV'
       *  Constant: '<S743>/UCE_tiRCDLineScgReh_C'
       *  Gain: '<S747>/Gain'
       */
      CtApAEM_TurnOnDelay1(rtb_LogicalOperator1, 0.01 * (real_T)
                           Rte_CData_UCE_tiRCDLineScgReh_C(), 0.01,
                           &CtApAEM_B.TurnOnDelay2_n, &CtApAEM_DW.TurnOnDelay2_n);

      /* End of Outputs for SubSystem: '<S743>/TurnOnDelay2' */

      /* Chart: '<S744>/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD' */
      /* Gateway: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
      /* During: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
      if (CtApAEM_DW.is_active_c5_CtApAEM == 0U) {
        /* Entry: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
        CtApAEM_DW.is_active_c5_CtApAEM = 1U;

        /* Entry Internal: RCtApAEM_task10msB_sys/F0_Gerer_PdV_EE_generique/F01_Gerer_PdV_RCD/F01_06_Gerer_CMD_LIGNE_RCD_par_type1/F01_06_02_Traiter_LIGNE_RCD_CC_masse/F01_06_02_02_Renseigner_LIGNE_RCD_CC_masse_pour_GD/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD */
        /* Transition: '<S764>:4' */
        CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_Defaut_inactif;

        /* Entry 'Defaut_inactif': '<S764>:2' */
        CtApAEM_B.UCE_bDgoRCDLineScg = CtApAEM_inactive;
      } else if (CtApAEM_DW.is_c5_CtApAEM == CtApAEM_IN_Defaut_actif) {
        /* During 'Defaut_actif': '<S764>:1' */
        if (CtApAEM_B.TurnOnDelay2_n.Logic[1]) {
          /* Transition: '<S764>:5' */
          CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_Defaut_inactif;

          /* Entry 'Defaut_inactif': '<S764>:2' */
          CtApAEM_B.UCE_bDgoRCDLineScg = CtApAEM_inactive;
        }
      } else {
        /* During 'Defaut_inactif': '<S764>:2' */
        if (CtApAEM_B.TurnOnDelay1_df.Logic[1]) {
          /* Transition: '<S764>:3' */
          CtApAEM_DW.is_c5_CtApAEM = CtApAEM_IN_Defaut_actif;

          /* Entry 'Defaut_actif': '<S764>:1' */
          CtApAEM_B.UCE_bDgoRCDLineScg = CtApAEM_active;
        }
      }

      /* End of Chart: '<S744>/F01_05_02_01_Automate_LIGNE_RCD_CC_masse_pour_GD' */

      /* Outputs for Atomic SubSystem: '<S739>/DetectSat' */

      /* Constant: '<S739>/Constant5' incorporates:
       *  Constant: '<S736>/UCE_tiRCDLineCmdAcv_C'
       *  Constant: '<S739>/Constant7'
       *  Gain: '<S738>/Gain'
       */
      CtApAEM_DetectSat(10000.0, 0.01 * (real_T)Rte_CData_UCE_tiRCDLineCmdAcv_C(),
                        0.0, &CtApAEM_B.DetectSat_o);

      /* End of Outputs for SubSystem: '<S739>/DetectSat' */

      /* Outputs for Atomic SubSystem: '<S739>/rising_edge' */

      /* Logic: '<S736>/Logical Operator4' incorporates:
       *  Constant: '<S736>/PARTIAL_WAKEUP'
       *  RelationalOperator: '<S736>/Relational Operator7'
       *  UnitDelay: '<S6>/Unit Delay1'
       *  UnitDelay: '<S736>/Unit Delay2'
       */
      CtApAEM_rising_edge(CtApAEM_DW.UnitDelay2_DSTATE &&
                          (CtApAEM_DW.UnitDelay1_DSTATE_o == 1),
                          &CtApAEM_B.rising_edge_e, &CtApAEM_DW.rising_edge_e);

      /* End of Outputs for SubSystem: '<S739>/rising_edge' */

      /* Switch: '<S739>/Switch' incorporates:
       *  Constant: '<S736>/UCE_Sldxxms_PdV'
       *  Logic: '<S739>/Logical Operator'
       *  Sum: '<S739>/Sum1'
       *  Switch: '<S739>/Switch1'
       *  UnitDelay: '<S739>/UnitDelay1'
       */
      if (CtApAEM_B.rising_edge_e.LogicalOperator) {
        rtb_Gain_ae = 0.01 + CtApAEM_B.DetectSat_o.MinMax2;
      } else {
        rtb_Gain_ae = CtApAEM_DW.UnitDelay1_DSTATE;
      }

      /* End of Switch: '<S739>/Switch' */

      /* Logic: '<S736>/Logical Operator6' incorporates:
       *  Constant: '<S736>/UCE_Sldxxms_PdV'
       *  Constant: '<S739>/Constant1'
       *  Constant: '<S739>/Constant6'
       *  Logic: '<S736>/Logical Operator1'
       *  RelationalOperator: '<S739>/Relational Operator'
       *  Sum: '<S739>/Sum2'
       */
      CtApAEM_B.LogicalOperator6 = ((!CtApAEM_B.UCE_bDgoRCDLineScg) &&
        ((rtb_Gain_ae - 2.2204460492503131E-16) - 0.01 > 0.0));

      /* Outputs for Atomic SubSystem: '<S739>/DetectSat1' */

      /* Sum: '<S739>/Sum2' incorporates:
       *  Constant: '<S736>/UCE_Sldxxms_PdV'
       *  Constant: '<S739>/Constant6'
       *  Constant: '<S739>/Constant8'
       */
      CtApAEM_DetectSat(CtApAEM_B.DetectSat_o.MinMax2, (rtb_Gain_ae -
        2.2204460492503131E-16) - 0.01, 0.0, &CtApAEM_B.DetectSat1_lj);

      /* End of Outputs for SubSystem: '<S739>/DetectSat1' */

      /* Logic: '<S745>/Logical Operator4' incorporates:
       *  Constant: '<S745>/DEGRADED_MAIN_WAKEUP'
       *  Constant: '<S745>/INTERNAL_PARTIAL_WAKEUP'
       *  Constant: '<S745>/NOMINAL_MAIN_WAKEUP'
       *  Constant: '<S745>/PARTIAL_WAKEUP'
       *  Constant: '<S745>/TRANSITORY_STATE'
       *  RelationalOperator: '<S745>/Relational Operator1'
       *  RelationalOperator: '<S745>/Relational Operator2'
       *  RelationalOperator: '<S745>/Relational Operator3'
       *  RelationalOperator: '<S745>/Relational Operator4'
       *  RelationalOperator: '<S745>/Relational Operator5'
       *  UnitDelay: '<S6>/Unit Delay1'
       */
      CtApAEM_B.LogicalOperator4 = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 1) ||
        (CtApAEM_DW.UnitDelay1_DSTATE_o == 2) || (CtApAEM_DW.UnitDelay1_DSTATE_o
        == 3) || (CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
        (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

      /* SignalConversion: '<S15>/OutportBufferForUCE_bDgoRCDLineScg' */
      CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt = CtApAEM_B.UCE_bDgoRCDLineScg;

      /* Update for UnitDelay: '<S15>/Unit Delay7' */
      CtApAEM_DW.UnitDelay7_DSTATE = CtApAEM_B.LogicalOperator6;

      /* Update for UnitDelay: '<S736>/Unit Delay2' */
      CtApAEM_DW.UnitDelay2_DSTATE = CtApAEM_B.LogicalOperator4_b;

      /* Update for UnitDelay: '<S739>/UnitDelay1' */
      CtApAEM_DW.UnitDelay1_DSTATE = CtApAEM_B.DetectSat1_lj.MinMax2;
    } else {
      if (CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_) {
        /* Disable for Outport: '<S15>/UCE_bRCDLineCmd' */
        CtApAEM_B.LogicalOperator6 = false;

        /* Disable for Outport: '<S15>/UCE_bDgoRCDLineScg' */
        CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt = false;

        /* Disable for Outport: '<S15>/UCE_bMonRunRCDLineScg' */
        CtApAEM_B.LogicalOperator4 = false;
        CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_ = false;
      }
    }

    /* End of Outputs for SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

    /* Logic: '<S765>/Logical Operator1' incorporates:
     *  Constant: '<S765>/DEGRADED_MAIN_WAKEUP'
     *  Constant: '<S765>/NOMINAL_MAIN_WAKEUP'
     *  RelationalOperator: '<S765>/Relational Operator1'
     *  RelationalOperator: '<S765>/Relational Operator3'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_LogicalOperator1_f = ((CtApAEM_DW.UnitDelay1_DSTATE_o == 4) ||
      (CtApAEM_DW.UnitDelay1_DSTATE_o == 5));

    /* Switch: '<S765>/Switch1' incorporates:
     *  Constant: '<S765>/Electronic_Integration_Mode'
     *  Constant: '<S765>/PARTIAL_WAKEUP'
     *  Constant: '<S765>/TRANSITORY_STATE'
     *  Logic: '<S765>/Logical Operator10'
     *  Logic: '<S765>/Logical Operator11'
     *  Logic: '<S765>/Logical Operator12'
     *  Logic: '<S765>/Logical Operator3'
     *  RelationalOperator: '<S765>/Relational Operator2'
     *  RelationalOperator: '<S765>/Relational Operator4'
     *  RelationalOperator: '<S765>/Relational Operator6'
     *  RelationalOperator: '<S896>/Relational Operator'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    if (rtb_LogicalOperator1 && rtb_LogicalOperator1_f &&
        rtb_LogicalOperator12_l) {
      CtApAEM_B.Switch1_k = 2;
    } else {
      CtApAEM_B.Switch1_k = (rtb_LogicalOperator1_f ||
        (((CtApAEM_DW.UnitDelay1_DSTATE_o == 3) ||
          (CtApAEM_DW.UnitDelay1_DSTATE_o == 1)) && rtb_LogicalOperator4));
    }

    /* End of Switch: '<S765>/Switch1' */

    /* RelationalOperator: '<S17>/Relational Operator5' incorporates:
     *  Constant: '<S17>/SHUTDOWN_PREPARATION'
     *  UnitDelay: '<S6>/Unit Delay1'
     */
    rtb_LogicalOperator12_l = (CtApAEM_DW.UnitDelay1_DSTATE_o == 7);

    /* Outputs for Atomic SubSystem: '<S17>/TurnOnDelay1' */

    /* Gain: '<S772>/Gain' incorporates:
     *  Constant: '<S17>/UCE_Sldxxms_PdV'
     *  Constant: '<S17>/UCE_tiMinTiShutDownPrep_C'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_l, 0.1 * (real_T)
                         Rte_CData_UCE_tiMinTiShutDownPrep_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay1_d, &CtApAEM_DW.TurnOnDelay1_d);

    /* End of Outputs for SubSystem: '<S17>/TurnOnDelay1' */

    /* Logic: '<S17>/Logical Operator2' */
    rtb_LogicalOperator1_f = CtApAEM_B.TurnOnDelay1_d.Logic[1];

    /* Outputs for Atomic SubSystem: '<S17>/TurnOnDelay2' */

    /* Gain: '<S773>/Gain' incorporates:
     *  Constant: '<S17>/UCE_Sldxxms_PdV'
     *  Constant: '<S17>/UCE_tiMaxTiShutDownPrep_C'
     */
    CtApAEM_TurnOnDelay1(rtb_LogicalOperator12_l, 0.1 * (real_T)
                         Rte_CData_UCE_tiMaxTiShutDownPrep_C(), 0.01,
                         &CtApAEM_B.TurnOnDelay2_g, &CtApAEM_DW.TurnOnDelay2_g);

    /* End of Outputs for SubSystem: '<S17>/TurnOnDelay2' */

    /* Logic: '<S17>/Logical Operator1' */
    CtApAEM_B.LogicalOperator1_m = (rtb_LogicalOperator1_f ||
      CtApAEM_B.TurnOnDelay2_g.Logic[1]);

    /* SignalConversion: '<S6>/OutportBufferForUCE_bDgoMainWkuDisrd' */
    CtApAEM_B.OutportBufferForUCE_bDgoMainW_m = CtApAEM_B.UCE_bDgoMainWkuDisrd;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bDgoMainWkuIncst' */
    CtApAEM_B.OutportBufferForUCE_bDgoMain_kx = CtApAEM_B.UCE_bDgoMainWkuIncst;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bDgoRCDLineScg' */
    CtApAEM_B.OutportBufferForUCE_bDgoRCDLi_g =
      CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bMonRunRCDLineScg' */
    CtApAEM_B.OutportBufferForUCE_bMonRunRC_e = CtApAEM_B.LogicalOperator4;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bRCDLineCmd' */
    CtApAEM_B.OutportBufferForUCE_bRCDLineC_i = CtApAEM_B.LogicalOperator6;

    /* SignalConversion: '<S6>/OutportBufferForUCE_bfMstPtlWkuReq' */
    CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c = CtApAEM_B.uBitEncoder1.Sum;

    /* SignalConversion: '<S6>/OutportBufferForUCE_stRCDSt' */
    CtApAEM_B.OutportBufferForUCE_stRCDSt = CtApAEM_B.UCE_stRCDSt;

    /* Update for UnitDelay: '<S6>/Unit Delay1' */
    CtApAEM_DW.UnitDelay1_DSTATE_o = CtApAEM_B.UCE_stRCDSt;
  } else {
    if (CtApAEM_DW.F01_Gerer_PdV_RCD_MODE) {
      /* Disable for Enabled SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' */
      if (CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel) {
        /* Disable for Outport: '<S145>/UCE_bfMstPtlWkuReq' */
        CtApAEM_B.uBitEncoder1.Sum = 0;
        CtApAEM_DW.F01_02_01_Gerer_Reveils_partiel = false;
      }

      /* End of Disable for SubSystem: '<S11>/F01_02_01_Gerer_Reveils_partiels_maitres' */

      /* Disable for Enabled SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */
      if (CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_) {
        /* Disable for Outport: '<S15>/UCE_bRCDLineCmd' */
        CtApAEM_B.LogicalOperator6 = false;

        /* Disable for Outport: '<S15>/UCE_bDgoRCDLineScg' */
        CtApAEM_B.OutportBufferForUCE_bDgoRCDL_gt = false;

        /* Disable for Outport: '<S15>/UCE_bMonRunRCDLineScg' */
        CtApAEM_B.LogicalOperator4 = false;
        CtApAEM_DW.F01_06_Gerer_CMD_LIGNE_RCD_par_ = false;
      }

      /* End of Disable for SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

      /* Disable for Outport: '<S6>/UCE_stRCDSt' */
      CtApAEM_B.OutportBufferForUCE_stRCDSt = 3;

      /* Disable for Outport: '<S6>/UCE_bRCDLineCmd' */
      CtApAEM_B.OutportBufferForUCE_bRCDLineC_i = false;

      /* Disable for Outport: '<S6>/UCE_bfMstPtlWkuReq' */
      CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c = 0;

      /* Disable for Outport: '<S6>/UCE_bfSlavePtlWkuSt' */
      CtApAEM_B.u6BitEncoder1.Sum = 0;

      /* Disable for Outport: '<S6>/UCE_stRCDCAN1ComReq' */
      CtApAEM_B.Switch1_k = 0;

      /* Disable for Outport: '<S6>/UCE_bRCDShutDownAuth' */
      CtApAEM_B.LogicalOperator1_m = false;

      /* Disable for Outport: '<S6>/UCE_bDgoMainWkuDisrd' */
      CtApAEM_B.OutportBufferForUCE_bDgoMainW_m = false;

      /* Disable for Outport: '<S6>/UCE_bMonRunMainWkuDisrd' */
      CtApAEM_B.LogicalOperator1_g = false;

      /* Disable for Outport: '<S6>/UCE_bDgoMainWkuIncst' */
      CtApAEM_B.OutportBufferForUCE_bDgoMain_kx = false;

      /* Disable for Outport: '<S6>/UCE_bMonRunMainWkuIncst' */
      CtApAEM_B.LogicalOperator1_h = false;

      /* Disable for Outport: '<S6>/UCE_bDgoRCDLineScg' */
      CtApAEM_B.OutportBufferForUCE_bDgoRCDLi_g = false;

      /* Disable for Outport: '<S6>/UCE_bMonRunRCDLineScg' */
      CtApAEM_B.OutportBufferForUCE_bMonRunRC_e = false;
      CtApAEM_DW.F01_Gerer_PdV_RCD_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S3>/F01_Gerer_PdV_RCD' */

  /* Outputs for Enabled SubSystem: '<S3>/F02_Gerer_PdV_APC' incorporates:
   *  EnablePort: '<S7>/Enable'
   */
  if (CtApAEM_DW.F02_Gerer_PdV_APC_MODE) {
    /* Disable for Outport: '<S7>/UCE_stAPCSt' */
    CtApAEM_B.OutportBufferForUCE_stAPCSt = 12;
    CtApAEM_DW.F02_Gerer_PdV_APC_MODE = false;
  }

  /* End of Outputs for SubSystem: '<S3>/F02_Gerer_PdV_APC' */

  /* Switch: '<S9>/Switch1' incorporates:
   *  Constant: '<S8>/UCE_noUCETyp_C'
   */
  if (Rte_CData_UCE_noUCETyp_C()) {
    rtb_DataTypeConversion = CtApAEM_B.OutportBufferForUCE_stRCDSt;
  } else {
    rtb_DataTypeConversion = CtApAEM_B.OutportBufferForUCE_stAPCSt;
  }

  /* End of Switch: '<S9>/Switch1' */

  /* Logic: '<S9>/Logical Operator12' incorporates:
   *  Constant: '<S9>/DEGRADED_MAIN_WAKEUP'
   *  Constant: '<S9>/MAIN_WAKEUP_APC'
   *  Constant: '<S9>/NOMINAL_MAIN_WAKEUP'
   *  RelationalOperator: '<S9>/Relational Operator1'
   *  RelationalOperator: '<S9>/Relational Operator2'
   *  RelationalOperator: '<S9>/Relational Operator4'
   */
  CtApAEM_B.LogicalOperator12 = ((rtb_DataTypeConversion == 4) ||
    (rtb_DataTypeConversion == 5) || (rtb_DataTypeConversion == 12));

  /* Outputs for Atomic SubSystem: '<S9>/falling_edge' */
  CtApAEM_falling_edge1(CtApAEM_B.LogicalOperator12, &CtApAEM_B.falling_edge,
                        &CtApAEM_DW.falling_edge);

  /* End of Outputs for SubSystem: '<S9>/falling_edge' */

  /* Outputs for Atomic SubSystem: '<S9>/BasculeRS' */
  /* Logic: '<S888>/Logical Operator1' incorporates:
   *  Logic: '<S888>/Logical Operator3'
   *  Logic: '<S888>/Logical Operator7'
   *  UnitDelay: '<S888>/UnitDelay'
   */
  rtb_LogicalOperator12_l = ((CtApAEM_DW.UnitDelay_DSTATE ||
    CtApAEM_B.falling_edge.LogicalOperator) && (!CtApAEM_B.LogicalOperator12));

  /* Update for UnitDelay: '<S888>/UnitDelay' */
  CtApAEM_DW.UnitDelay_DSTATE = rtb_LogicalOperator12_l;

  /* End of Outputs for SubSystem: '<S9>/BasculeRS' */

  /* Switch: '<S9>/Switch2' incorporates:
   *  Constant: '<S8>/UCE_noUCETyp_C'
   */
  if (Rte_CData_UCE_noUCETyp_C()) {
    rowIdx = CtApAEM_B.Switch1_k;
  } else {
    rowIdx = CtApAEM_B.Switch1_a;
  }

  /* Outport: '<Root>/PpCANComRequest_DeCANComRequest' incorporates:
   *  Switch: '<S9>/Switch2'
   */
  Rte_Write_PpCANComRequest_DeCANComRequest(rowIdx);

  /* Switch: '<S9>/Switch3' incorporates:
   *  Constant: '<S8>/UCE_noUCETyp_C'
   */
  if (Rte_CData_UCE_noUCETyp_C()) {
    rtb_LogicalOperator1_f = CtApAEM_B.LogicalOperator1_m;
  } else {
    rtb_LogicalOperator1_f = CtApAEM_B.LogicalOperator1;
  }

  /* Outport: '<Root>/PpShutdownAuthorization_DeShutdownAuthorization' incorporates:
   *  Switch: '<S9>/Switch3'
   */
  Rte_Write_PpShutdownAuthorization_DeShutdownAuthorization
    (rtb_LogicalOperator1_f);

  /* Switch: '<S9>/Switch7' incorporates:
   *  Logic: '<S9>/Logical Operator1'
   *  Logic: '<S9>/Logical Operator2'
   *  Logic: '<S9>/Logical Operator3'
   *  RelationalOperator: '<S9>/Relational Operator10'
   *  RelationalOperator: '<S9>/Relational Operator3'
   *  RelationalOperator: '<S9>/Relational Operator6'
   *  RelationalOperator: '<S9>/Relational Operator7'
   *  RelationalOperator: '<S9>/Relational Operator8'
   *  RelationalOperator: '<S9>/Relational Operator9'
   *  Switch: '<S9>/Switch8'
   *  Switch: '<S9>/Switch9'
   */
  if ((!CtApAEM_B.LogicalOperator12) && (!rtb_LogicalOperator12_l)) {
    /* Outport: '<Root>/PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup' incorporates:
     *  Constant: '<S9>/PRE_MAIN_WAKEUP'
     */
    Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(0);
  } else {
    if (CtApAEM_B.LogicalOperator12 && (!rtb_LogicalOperator12_l)) {
      /* Switch: '<S9>/Switch8' incorporates:
       *  Constant: '<S9>/MAIN_WAKEUP'
       */
      rowIdx = 1;
    } else if ((!CtApAEM_B.LogicalOperator12) && rtb_LogicalOperator12_l) {
      /* Switch: '<S9>/Switch9' incorporates:
       *  Constant: '<S9>/POST_MAIN_WAKEUP'
       *  Switch: '<S9>/Switch8'
       */
      rowIdx = 2;
    } else {
      /* Switch: '<S9>/Switch8' incorporates:
       *  Constant: '<S9>/INAVLIID_VALUE'
       *  Switch: '<S9>/Switch9'
       */
      rowIdx = 3;
    }

    /* Outport: '<Root>/PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup' incorporates:
     *  Logic: '<S9>/Logical Operator3'
     *  RelationalOperator: '<S9>/Relational Operator10'
     *  RelationalOperator: '<S9>/Relational Operator9'
     *  Switch: '<S9>/Switch8'
     *  Switch: '<S9>/Switch9'
     */
    Rte_Write_PpECU_RelativeStMainWakeup_DeECU_RelativeStMainWakeup(rowIdx);
  }

  /* End of Switch: '<S9>/Switch7' */

  /* Outport: '<Root>/PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bDgoMainWkuDisrd'
   */
  Rte_Write_PpFaultMainWakeupDisrd_DeFaultMainWakeupDisrd
    (CtApAEM_B.OutportBufferForUCE_bDgoMainW_m);

  /* Outport: '<Root>/PpFaultMainWakeupIncst_DeFaultMainWakeupIncst' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bDgoMainWkuIncst'
   */
  Rte_Write_PpFaultMainWakeupIncst_DeFaultMainWakeupIncst
    (CtApAEM_B.OutportBufferForUCE_bDgoMain_kx);

  /* Outport: '<Root>/PpFaultRCDLineSC_DeFaultRCDLineSC' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bDgoRCDLineScg'
   */
  Rte_Write_PpFaultRCDLineSC_DeFaultRCDLineSC
    (CtApAEM_B.OutportBufferForUCE_bDgoRCDLi_g);

  /* Outport: '<Root>/PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bMonRunMainWkuDisrd'
   */
  Rte_Write_PpMonitoringMainWakeupDisrd_DeMonitoringMainWakeupDisrd
    (CtApAEM_B.LogicalOperator1_g);

  /* Outport: '<Root>/PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bMonRunMainWkuIncst'
   */
  Rte_Write_PpMonitoringMainWakeupIncst_DeMonitoringMainWakeupIncst
    (CtApAEM_B.LogicalOperator1_h);

  /* Outport: '<Root>/PpMonitoringRCDLineSC_DeMonitoringRCDLineSC' incorporates:
   *  SignalConversion: '<S3>/OutportBufferForUCE_bMonRunRCDLineScg'
   */
  Rte_Write_PpMonitoringRCDLineSC_DeMonitoringRCDLineSC
    (CtApAEM_B.OutportBufferForUCE_bMonRunRC_e);

  /* End of Outputs for SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */

  /* DataStoreWrite: '<S2>/Data Store Write' */
  CtApAEM_DW.RCDECUState = rtb_DataTypeConversion;

  /* Outputs for Enabled SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' incorporates:
   *  EnablePort: '<S3>/Enable'
   */
  /* Outport: '<Root>/PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue' incorporates:
   *  RelationalOperator: '<S900>/Compare'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bRCDLineCmd'
   */
  Rte_Write_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue
    (CtApAEM_B.OutportBufferForUCE_bRCDLineC_i);

  /* Outport: '<Root>/PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup' incorporates:
   *  Constant: '<S904>/Constant'
   *  DataTypeConversion: '<S902>/Data Type Conversion'
   *  RelationalOperator: '<S904>/Compare'
   *  S-Function (sfix_bitop): '<S902>/Bitwise AND'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 1) == 1);

  /* Outport: '<Root>/PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup' incorporates:
   *  Constant: '<S905>/Constant'
   *  DataTypeConversion: '<S902>/Data Type Conversion1'
   *  RelationalOperator: '<S905>/Compare'
   *  S-Function (sfix_bitop): '<S902>/Bitwise AND1'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 2) == 2);

  /* Outport: '<Root>/PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup' incorporates:
   *  Constant: '<S906>/Constant'
   *  DataTypeConversion: '<S902>/Data Type Conversion2'
   *  RelationalOperator: '<S906>/Compare'
   *  S-Function (sfix_bitop): '<S902>/Bitwise AND2'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 4) == 4);

  /* Outport: '<Root>/PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup' incorporates:
   *  Constant: '<S907>/Constant'
   *  DataTypeConversion: '<S902>/Data Type Conversion3'
   *  RelationalOperator: '<S907>/Compare'
   *  S-Function (sfix_bitop): '<S902>/Bitwise AND3'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfMstPtlWkuReq'
   */
  Rte_Write_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup
    ((CtApAEM_B.OutportBufferForUCE_bfMstPtlW_c & 8) == 8);

  /* Outport: '<Root>/PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState' incorporates:
   *  Constant: '<S908>/Constant'
   *  DataTypeConversion: '<S903>/Data Type Conversion'
   *  RelationalOperator: '<S908>/Compare'
   *  S-Function (sfix_bitop): '<S903>/Bitwise AND'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 1) == 1);

  /* Outport: '<Root>/PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState' incorporates:
   *  Constant: '<S909>/Constant'
   *  DataTypeConversion: '<S903>/Data Type Conversion1'
   *  RelationalOperator: '<S909>/Compare'
   *  S-Function (sfix_bitop): '<S903>/Bitwise AND1'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 2) == 2);

  /* Outport: '<Root>/PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState' incorporates:
   *  Constant: '<S910>/Constant'
   *  DataTypeConversion: '<S903>/Data Type Conversion2'
   *  RelationalOperator: '<S910>/Compare'
   *  S-Function (sfix_bitop): '<S903>/Bitwise AND2'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 4) == 4);

  /* Outport: '<Root>/PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState' incorporates:
   *  Constant: '<S911>/Constant'
   *  DataTypeConversion: '<S903>/Data Type Conversion3'
   *  RelationalOperator: '<S911>/Compare'
   *  S-Function (sfix_bitop): '<S903>/Bitwise AND3'
   *  SignalConversion: '<S3>/OutportBufferForUCE_bfSlavePtlWkuSt'
   */
  Rte_Write_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState
    ((CtApAEM_B.u6BitEncoder1.Sum & 8) == 8);

  /* End of Outputs for SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */
  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */

  /* Outport: '<Root>/PpECU_WakeupMain_DeECU_WakeupMain' */
  Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(CtApAEM_B.LogicalOperator12);

  /* RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* Switch: '<S901>/Switch1' incorporates:
   *  Constant: '<S901>/Constant2'
   *  Constant: '<S901>/Constant3'
   *  Constant: '<S901>/Constant4'
   *  RelationalOperator: '<S901>/Relational Operator1'
   *  Sum: '<S901>/Subtract'
   *  Switch: '<S901>/Switch'
   */
  if (rtb_DataTypeConversion > 0) {
    /* Switch: '<S901>/Switch' incorporates:
     *  Constant: '<S901>/Constant'
     *  Constant: '<S901>/Constant1'
     *  Constant: '<S901>/Constant2'
     *  RelationalOperator: '<S901>/Relational Operator'
     *  Sum: '<S901>/Subtract'
     */
    if (7 == rtb_DataTypeConversion) {
      rtb_DataTypeConversion = 6;
    }
  } else {
    rtb_DataTypeConversion = 1;
  }

  /* Outport: '<Root>/PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD' incorporates:
   *  Constant: '<S901>/Constant2'
   *  DataTypeConversion: '<S901>/Data Type Conversion'
   *  Sum: '<S901>/Subtract'
   *  Switch: '<S901>/Switch'
   *  Switch: '<S901>/Switch1'
   */
  Rte_Write_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD
    ((SUPV_ECUElecStateRCD)(rtb_DataTypeConversion - 1));

  /* End of Outputs for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */
}

/* Model initialize function */
void CtApAEM_Init(void)
{
  /* SystemInitialize for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' incorporates:
   *  SystemInitialize for SubSystem: '<Root>/RCtApAEM_task10msB_sys'
   */
  /* SystemInitialize for Enabled SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */
  /* SystemInitialize for Enabled SubSystem: '<S3>/F01_Gerer_PdV_RCD' */

  /* SystemInitialize for Atomic SubSystem: '<S18>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1);

  /* End of SystemInitialize for SubSystem: '<S18>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S18>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2);

  /* End of SystemInitialize for SubSystem: '<S18>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S19>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_c);

  /* End of SystemInitialize for SubSystem: '<S19>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S19>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_k);

  /* End of SystemInitialize for SubSystem: '<S19>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S19>/TurnOnDelay3' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay3);

  /* End of SystemInitialize for SubSystem: '<S19>/TurnOnDelay3' */

  /* SystemInitialize for Atomic SubSystem: '<S20>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay);

  /* End of SystemInitialize for SubSystem: '<S20>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S21>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_g);

  /* End of SystemInitialize for SubSystem: '<S21>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S22>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_a);

  /* End of SystemInitialize for SubSystem: '<S22>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S96>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_h);

  /* End of SystemInitialize for SubSystem: '<S96>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S115>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_m);

  /* End of SystemInitialize for SubSystem: '<S115>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S115>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_e);

  /* End of SystemInitialize for SubSystem: '<S115>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S115>/TurnOnDelay3' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay3_o);

  /* End of SystemInitialize for SubSystem: '<S115>/TurnOnDelay3' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' */

  /* SystemInitialize for Atomic SubSystem: '<S282>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_c);

  /* End of SystemInitialize for SubSystem: '<S282>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_16_Gerer_etat_Reveil_partiel_esclave_X16' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' */

  /* SystemInitialize for Atomic SubSystem: '<S281>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_m4);

  /* End of SystemInitialize for SubSystem: '<S281>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_15_Gerer_etat_Reveil_partiel_esclave_X15' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' */

  /* SystemInitialize for Atomic SubSystem: '<S280>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_gv);

  /* End of SystemInitialize for SubSystem: '<S280>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_14_Gerer_etat_Reveil_partiel_esclave_X14' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' */

  /* SystemInitialize for Atomic SubSystem: '<S279>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mj);

  /* End of SystemInitialize for SubSystem: '<S279>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_13_Gerer_etat_Reveil_partiel_esclave_X13' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' */

  /* SystemInitialize for Atomic SubSystem: '<S278>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_n2);

  /* End of SystemInitialize for SubSystem: '<S278>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_12_Gerer_etat_Reveil_partiel_esclave_X12' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' */

  /* SystemInitialize for Atomic SubSystem: '<S277>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_d);

  /* End of SystemInitialize for SubSystem: '<S277>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_11_Gerer_etat_Reveil_partiel_esclave_X11' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' */

  /* SystemInitialize for Atomic SubSystem: '<S276>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_e);

  /* End of SystemInitialize for SubSystem: '<S276>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_10_Gerer_etat_Reveil_partiel_esclave_X10' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' */

  /* SystemInitialize for Atomic SubSystem: '<S275>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_hk);

  /* End of SystemInitialize for SubSystem: '<S275>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_09_Gerer_etat_Reveil_partiel_esclave_X9' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' */

  /* SystemInitialize for Atomic SubSystem: '<S274>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_k);

  /* End of SystemInitialize for SubSystem: '<S274>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_08_Gerer_etat_Reveil_partiel_esclave_X8' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' */

  /* SystemInitialize for Atomic SubSystem: '<S273>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_h);

  /* End of SystemInitialize for SubSystem: '<S273>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_07_Gerer_etat_Reveil_partiel_esclave_X7' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' */

  /* SystemInitialize for Atomic SubSystem: '<S272>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_es_mk);

  /* End of SystemInitialize for SubSystem: '<S272>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_06_Gerer_etat_Reveil_partiel_esclave_X6' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' */

  /* SystemInitialize for Atomic SubSystem: '<S271>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_a);

  /* End of SystemInitialize for SubSystem: '<S271>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_05_Gerer_etat_Reveil_partiel_esclave_X5' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' */

  /* SystemInitialize for Atomic SubSystem: '<S270>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_j);

  /* End of SystemInitialize for SubSystem: '<S270>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_04_Gerer_etat_Reveil_partiel_esclave_X4' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' */

  /* SystemInitialize for Atomic SubSystem: '<S269>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_m);

  /* End of SystemInitialize for SubSystem: '<S269>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_03_Gerer_etat_Reveil_partiel_esclave_X3' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' */

  /* SystemInitialize for Atomic SubSystem: '<S268>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_n);

  /* End of SystemInitialize for SubSystem: '<S268>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_02_Gerer_etat_Reveil_partiel_esclave_X2' */

  /* SystemInitialize for Enabled SubSystem: '<S261>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' */

  /* SystemInitialize for Atomic SubSystem: '<S267>/Gerer_etat_Reveil_partiel_esclave_Xi' */
  Gerer_etat_Reveil_partie_b_Init(&CtApAEM_DW.Gerer_etat_Reveil_partiel_esc_g);

  /* End of SystemInitialize for SubSystem: '<S267>/Gerer_etat_Reveil_partiel_esclave_Xi' */

  /* End of SystemInitialize for SubSystem: '<S261>/F01_02_02_01_01_Gerer_etat_Reveil_partiel_esclave_X1' */

  /* SystemInitialize for Atomic SubSystem: '<S12>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_i);

  /* End of SystemInitialize for SubSystem: '<S12>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S13>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_j);

  /* End of SystemInitialize for SubSystem: '<S13>/TurnOnDelay' */

  /* SystemInitialize for Chart: '<S14>/F01_05_01_Machine_etats_RCD' */
  CtApAEM_B.UCE_stRCDSt = 3;

  /* SystemInitialize for Enabled SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

  /* SystemInitialize for Atomic SubSystem: '<S743>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_df);

  /* End of SystemInitialize for SubSystem: '<S743>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S743>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_n);

  /* End of SystemInitialize for SubSystem: '<S743>/TurnOnDelay2' */

  /* End of SystemInitialize for SubSystem: '<S6>/F01_06_Gerer_CMD_LIGNE_RCD_par_type1' */

  /* SystemInitialize for Atomic SubSystem: '<S17>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_d);

  /* End of SystemInitialize for SubSystem: '<S17>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S17>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_g);

  /* End of SystemInitialize for SubSystem: '<S17>/TurnOnDelay2' */

  /* SystemInitialize for Outport: '<S6>/UCE_stRCDSt' */
  CtApAEM_B.OutportBufferForUCE_stRCDSt = 3;

  /* End of SystemInitialize for SubSystem: '<S3>/F01_Gerer_PdV_RCD' */

  /* SystemInitialize for Enabled SubSystem: '<S3>/F02_Gerer_PdV_APC' */

  /* SystemInitialize for Atomic SubSystem: '<S796>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_l);

  /* End of SystemInitialize for SubSystem: '<S796>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S796>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_d);

  /* End of SystemInitialize for SubSystem: '<S796>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S797>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_j0);

  /* End of SystemInitialize for SubSystem: '<S797>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S798>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_h);

  /* End of SystemInitialize for SubSystem: '<S798>/TurnOnDelay' */

  /* SystemInitialize for Atomic SubSystem: '<S791>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_n);

  /* End of SystemInitialize for SubSystem: '<S791>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S791>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_kj);

  /* End of SystemInitialize for SubSystem: '<S791>/TurnOnDelay2' */

  /* SystemInitialize for Atomic SubSystem: '<S792>/TurnOnDelay' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay_m);

  /* End of SystemInitialize for SubSystem: '<S792>/TurnOnDelay' */

  /* SystemInitialize for Chart: '<S793>/F02_04_01_ECU_APC_Applicative_state_machine' */
  CtApAEM_B.UCE_stAPCSt = 12;

  /* SystemInitialize for Atomic SubSystem: '<S795>/TurnOnDelay1' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay1_md);

  /* End of SystemInitialize for SubSystem: '<S795>/TurnOnDelay1' */

  /* SystemInitialize for Atomic SubSystem: '<S795>/TurnOnDelay2' */
  CtApAEM_TurnOnDelay1_Init(&CtApAEM_DW.TurnOnDelay2_h);

  /* End of SystemInitialize for SubSystem: '<S795>/TurnOnDelay2' */

  /* SystemInitialize for Outport: '<S7>/UCE_stAPCSt' */
  CtApAEM_B.OutportBufferForUCE_stAPCSt = 12;

  /* End of SystemInitialize for SubSystem: '<S3>/F02_Gerer_PdV_APC' */
  /* End of SystemInitialize for SubSystem: '<S2>/F0_Gerer_PdV_EE_generique' */
  /* End of SystemInitialize for RootInportFunctionCallGenerator: '<Root>/RootFcnCall_InsertedFor_RCtApAEM_task10msB_at_outport_1' */

  /* SystemInitialize for Outport: '<Root>/PpECU_WakeupMain_DeECU_WakeupMain' */
  Rte_Write_PpECU_WakeupMain_DeECU_WakeupMain(CtApAEM_B.LogicalOperator12);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
