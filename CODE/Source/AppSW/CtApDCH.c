/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApDCH.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApDCH
 *  Generation Time:  2020-11-23 11:42:16
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApDCH>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApDCH.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

#define DCHV_ADC_VOUT_CODED_THRESHOLD (5050U)  ///< (505V / 0.1)
#define DCLV_INPUT_V_CODED_THRESHOLD  (505U)  ///< (505V / 1)
/* Note the factors have been multiplies by 10 to make the comparison 
 * That way we don't lose decimal digits */
#define CAL_THR_UV_TRIP_DISCHARGE_FACTOR  (1000U)  
#define BATTERY_VOLT_FACTOR               (1U)    
#define CAL_TIME_UV_TRIP_DISCHARGE_FACTOR (100U)    
#define V_TO_MV(X)                        (X*1000U)    /* PRQA S 3453 #Used in that way for better understanding*/
#define RUNNABLE_PERIOD_MS                (10U)    
#define VDC_LINK_THRESHOLD                (24U)
#define DCH_REPORT_TIMER_INITIAL_VALUE			(400U)		/* 4 seconds */
#define DCH_REPORT_THRESHOLD_VOLTAGE			(60U)		/* 60 volts */

/* PRQA S 0857 -- #Max number of macros avoidance*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static non-init variables HERE... */

#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTION                                                            */
/*---------------------------------------------------------------------------*/

/**
 * \brief  Evaluates the conditions to perform a DischargeRequest
 * 
 * \return TRUE when the dicharge has been requested, FALSE otherwise
 */
static boolean EvaluateDischargeRequest(void);

/**
 * \brief  Compares the relationship between  BatteryVoltage versus ThrTripDisch.
 * 
 * \return TRUE if the first is lower during a time greater than TimeUVTripDischarge, FALSE otherwise
 */
static boolean EvaluateBatteryAndThrTripDischVoltage(void);

/**
 * \brief  Evaluates the conditions to set the output HV physical value
 * 
 * \param[in] dischargeReqStatus -  value of discharge request in last evaluation
 */
static void EvaluateHVPhyValue(boolean dischargeReqStatus);

/**
 * \brief  Evaluates the conditions to set the output DC Link physical value
 * 
 * \param[in] dischargeReqStatus -  value of discharge request in last evaluation
 */
static void EvaluateDCLinkPhyValue(boolean dischargeReqStatus);

/**
 * \brief  Generates the CAN signal DCDCActiveDischargeSt accroding to current status
 *
 * \param[in] hv_phy_value -  Active discharge actuation signal
 */
static void DCHReportActiveDischargeStatus(boolean hv_phy_value);

/**
 * \brief  Check if the voltage HV is below the configured active discharge threshold
 */
static boolean DCH_CheckActiveDischargeVoltageUnderThreshold(void);


static void DCH_SetActiveDischargeVarValue(boolean hv_phy_value, boolean Voltage_Under_Threshold, uint16 DCH_ActDscCounter,
		DCDC_ActivedischargeSt * pDCH_DCDC_ActivedischargeStVar);

/*PRQA S 3214 ++ # Macro used for safety memory mapping regions */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtCalThresholdUVTripDischarge: Integer in interval [0...200]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtCalTimeUVTripDischarge: Integer in interval [0...200]
 *   Unit: [ms], Factor: 100, Offset: 0
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * VCU_ActivedischargeCommand: Boolean
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * DCDC_ActivedischargeSt: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_not_in_active_discharge (0U)
 *   Cx1_active_discharging (1U)
 *   Cx2_discharge_completed (2U)
 *   Cx3_discharge_failure (3U)
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * IdtPOST_Result: Enumeration of integer in interval [0...255] with enumerators
 *   POST_ONGOING (0U)
 *   POST_OK (1U)
 *   POST_NOK (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * PFC_PFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 *   Cx6_PFC_STATE_START_TRI (6U)
 *   Cx7_PFC_STATE_START_MONO (7U)
 *   Cx8_PFC_STATE_ERROR (8U)
 *   Cx9_PFC_STATE_DIRECTPWM (9U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtCalThresholdUVTripDischarge Rte_CData_CalThresholdUVTripDischarge(void)
 *   IdtCalTimeUVTripDischarge Rte_CData_CalTimeUVTripDischarge(void)
 *
 *********************************************************************************************************************/


#define CtApDCH_START_SEC_CODE
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDCH_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_init_doc
 *********************************************************************************************************************/
/*PRQA S 3214 -- # Macro used for safety memory mapping regions */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDCH_CODE) RCtApDCH_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApDCH_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand *data)
 *   Std_ReturnType Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(IdtPOST_Result *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(DCDC_ActivedischargeSt data)
 *   Std_ReturnType Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(boolean data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_task10ms_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApDCH_CODE) RCtApDCH_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApDCH_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean discharge_request;


  /****  1. Discharge request evaluation ****/
  discharge_request = EvaluateDischargeRequest();

  /****  2. HV Physical Value evaluation ****/
  EvaluateHVPhyValue(discharge_request);
  
  /****  3. DC Link Physical Value evaluation ****/
  EvaluateDCLinkPhyValue(discharge_request);
 
/*PRQA S 3214 ++ # Macro used for safety memory mapping regions */
 
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApDCH_STOP_SEC_CODE
#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void EvaluateDCLinkPhyValue(boolean dischargeReqStatus)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean dc_link_value = FALSE;

    OBC_ACRange   obc_ac_range;
    PFC_PFCStatus pfc_status;
    PFC_Vdclink_V vdc_link_value;
    IdtPOST_Result OBC_PostState;

  if (TRUE == dischargeReqStatus)
  {
    OBC_Status obc_status;
    DCDC_Status dcdc_status;


    /* PRQA S 3110, 3417, 3426   2  #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&obc_status);
    (void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&dcdc_status);
    if ((Cx3_conversion_working_ != obc_status) &&
        (Cx5_degradation_mode    != obc_status) &&
        (Cx3_conversion_working_ != dcdc_status) &&
        (Cx5_degradation_mode    != dcdc_status))
    {
      dc_link_value = TRUE;
    }
  }


    /* PRQA S 3110, 3417, 3426   4  #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_OBC_ACRange_Delayed_OBC_ACRange(&obc_ac_range);
    (void)Rte_Read_PpInt_PFC_PFCStatus_PFC_PFCStatus(&pfc_status);
    (void)Rte_Read_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(&vdc_link_value);
    (void)Rte_Read_PpOBC_POST_Result_DeOBC_POST_Result(&OBC_PostState);
  
    if ( (POST_ONGOING!=OBC_PostState)&&
    	 (Cx0_No_AC_10V_ == obc_ac_range) &&
         (Cx1_PFC_STATE_TRI != pfc_status) &&
         (Cx2_PFC_STATE_MONO != pfc_status) &&
         (VDC_LINK_THRESHOLD < vdc_link_value ))
    {
      dc_link_value = TRUE;
    }
  
  /* PRQA S 3110, 3417, 3426   1  #RTE read/write confirmation not used*/
  (void)Rte_Write_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(dc_link_value);

}

static void EvaluateHVPhyValue(boolean dischargeReqStatus)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean hv_phy_value = FALSE;


  if (TRUE == dischargeReqStatus)
  {
    OBC_Status obc_status;
    DCDC_Status dcdc_status;

    /* PRQA S 3110, 3417, 3426   2  #RTE read/write confirmation not used*/
    (void)Rte_Read_PpInt_OBC_Status_Delayed_OBC_Status(&obc_status);
    (void)Rte_Read_PpInt_DCDC_Status_Delayed_DCDC_Status(&dcdc_status);
    if ((Cx3_conversion_working_ != obc_status) &&
        (Cx5_degradation_mode    != obc_status) &&
        (Cx3_conversion_working_ != dcdc_status) &&
        (Cx5_degradation_mode    != dcdc_status))
    {
      /* PRQA S 3110, 3417, 3426   1  #RTE read/write confirmation not used*/
      hv_phy_value = TRUE;
    }
  }

  /* PRQA S 3110, 3417, 3426   1  #RTE read/write confirmation not used*/
  (void)Rte_Write_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(hv_phy_value);

  DCHReportActiveDischargeStatus(hv_phy_value);

}

static boolean EvaluateDischargeRequest(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean discharge_request = FALSE;
	boolean lost_frame_fault;
	VCU_ActivedischargeCommand active_discharge_command;
	DCHV_ADC_VOUT dchv_adc_vout;
	DCLV_Input_Voltage dclv_input_voltage;


  /* PRQA S 3110, 3417, 3426   1  # RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(&active_discharge_command);
  if (TRUE == active_discharge_command)
  {
    discharge_request = TRUE;
  }
  
  /* PRQA S 3110, 3417, 3426   1  # RTE read/write confirmation not used*/
  (void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&dchv_adc_vout);
  if (dchv_adc_vout > DCHV_ADC_VOUT_CODED_THRESHOLD)
  {
    discharge_request = TRUE;
  }
  
  /* PRQA S 3110, 3417, 3426   1  # RTE read/write confirmation not used*/
  (void)Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(&dclv_input_voltage);
  if (dclv_input_voltage > DCLV_INPUT_V_CODED_THRESHOLD)
  {
    discharge_request = TRUE;
  }
  
  /* PRQA S 3110, 3417, 3426   1  # RTE read/write confirmation not used*/
  (void)Rte_Read_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(&lost_frame_fault);
  if (TRUE == lost_frame_fault)
  {
    discharge_request = TRUE;
  }
  if (TRUE == EvaluateBatteryAndThrTripDischVoltage())
  {
    discharge_request = TRUE;
  }

  /* PRQA S 3110, 3417, 3426   1  # RTE read/write confirmation not used */
  (void)Rte_Write_PpActiveDischargeRequest_DeActiveDischargeRequest(discharge_request);

  return discharge_request;
}

static boolean EvaluateBatteryAndThrTripDischVoltage(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 discharge_condition_counter = 0;

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean            request_active_discharge = FALSE;
	uint32             phy_cal_thr_UV_trip_disch_mv;
	uint32             phy_battery_voltage_v;
	IdtBatteryVolt     battery_voltage_raw;
	uint32             cal_time_uv_trip_discharge_ms;       
	uint32             counter_limit;


  /* Get physical values from raw values */
  phy_cal_thr_UV_trip_disch_mv = ((uint32)Rte_CData_CalThresholdUVTripDischarge()) * CAL_THR_UV_TRIP_DISCHARGE_FACTOR;
  
  (void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&battery_voltage_raw);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  phy_battery_voltage_v = ((uint32)battery_voltage_raw) * BATTERY_VOLT_FACTOR;/* PRQA S 2985 #Usend in this way for better understanding*/

  /* calculate the limit of the counter based on the current calibration value */
  cal_time_uv_trip_discharge_ms = ((uint32)Rte_CData_CalTimeUVTripDischarge()) * CAL_TIME_UV_TRIP_DISCHARGE_FACTOR;
  counter_limit  = cal_time_uv_trip_discharge_ms/RUNNABLE_PERIOD_MS;
  
  /* Check the current discharge condition */
  if ( (V_TO_MV((phy_battery_voltage_v))) < (phy_cal_thr_UV_trip_disch_mv) )
  {
    if (discharge_condition_counter < counter_limit)
    {
      /* note that if the counter_limit is for example 1, we'll not request the 
       * discharge till the next tick */
       discharge_condition_counter++;
    }
    else
    {
       request_active_discharge = TRUE;
    }
  }
  else
  {
    /* The first time the condition dissapears */
    discharge_condition_counter = 0;
  }

  return request_active_discharge;
}

static void DCHReportActiveDischargeStatus(boolean hv_phy_value)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean DCH_prev_hv_phy_value = FALSE;
	static uint16 DCH_ActDscCounter = 0;

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static DCDC_ActivedischargeSt DCH_DCDC_ActivedischargeStVar = Cx0_not_in_active_discharge;

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean Voltage_Under_Threshold;


	/* Start counter */
	if (FALSE == hv_phy_value)
	{
		/* Counter not active. Active discharge not requested */
		DCH_ActDscCounter = 0;
	}
	else if (FALSE == DCH_prev_hv_phy_value)
	{
		/* Transition of hv_phy_value from FALSE to TRUE */
		/* Initialize de countdown timer */
		DCH_ActDscCounter = DCH_REPORT_TIMER_INITIAL_VALUE;
	}
	else
	{
		/* No action */
	}


	/* Get if monitored voltage signals are below the configured threshold */
	Voltage_Under_Threshold = DCH_CheckActiveDischargeVoltageUnderThreshold();


	DCH_SetActiveDischargeVarValue(hv_phy_value, Voltage_Under_Threshold, DCH_ActDscCounter,
			&DCH_DCDC_ActivedischargeStVar);

	/* Decrement counter */
	if (DCH_ActDscCounter > 0U)
	{
		DCH_ActDscCounter--;
	}

	DCH_prev_hv_phy_value = hv_phy_value;

	(void)Rte_Write_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(DCH_DCDC_ActivedischargeStVar);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void DCH_SetActiveDischargeVarValue(boolean hv_phy_value, boolean Voltage_Under_Threshold, uint16 DCH_ActDscCounter,
		DCDC_ActivedischargeSt * pDCH_DCDC_ActivedischargeStVar)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	/* Output signal calculation */
	if (FALSE == hv_phy_value)
	{
		/* Not in active discharge */
		*pDCH_DCDC_ActivedischargeStVar = Cx0_not_in_active_discharge;
	}
	else if ((FALSE == Voltage_Under_Threshold) && (0U == DCH_ActDscCounter))
	{
		/* Error in active discharge process */
		*pDCH_DCDC_ActivedischargeStVar = Cx3_discharge_failure;
	}
	else if ((FALSE == Voltage_Under_Threshold) && (Cx3_discharge_failure != *pDCH_DCDC_ActivedischargeStVar))
	{
		/* Active discharge in progress */
		*pDCH_DCDC_ActivedischargeStVar = Cx1_active_discharging;
	}
	else if ((FALSE != Voltage_Under_Threshold) && (Cx3_discharge_failure != *pDCH_DCDC_ActivedischargeStVar))
	{
		/* Active discharge ended */
		*pDCH_DCDC_ActivedischargeStVar = Cx2_discharge_completed;
	}
	else
	{
		/* No update of the signal */
		/* Misra compliance */
		/* Maybe the Cx3_discharge_failure value could be set for this case */
	}

}

static boolean DCH_CheckActiveDischargeVoltageUnderThreshold(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApDCH_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApDCH_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApDCH_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApDCH_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApDCH_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	DCHV_ADC_VOUT DCHV_Output_Voltage;
	DCLV_Input_Voltage DCLV_Input_Volt;
	boolean ret_value;


	/* Get the input voltage values */
	(void)Rte_Read_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(&DCHV_Output_Voltage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage (&DCLV_Input_Volt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* DCLV_Input_Volt is in units of 1V / bit */

	/* DCHV Output Voltage is in units of 0.1V / bit */
	/* Conversion to the same scale */
	DCHV_Output_Voltage = DCHV_Output_Voltage / 10U;

	/* Check the threshold */
	if ((DCHV_Output_Voltage < DCH_REPORT_THRESHOLD_VOLTAGE) && (DCLV_Input_Volt < DCH_REPORT_THRESHOLD_VOLTAGE))
	{
		/* Voltage under active discharge threshold */
		ret_value = TRUE;
	}
	else
	{
		ret_value = FALSE;
	}

	return ret_value;
}

/*PRQA S 3214 -- # Macro used for safety memory mapping regions */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
