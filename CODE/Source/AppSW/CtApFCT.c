/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApFCT.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApFCT
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApFCT>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup FCT
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Flow Control Temperature
 * \details This module manages the conversion and the fault detection for all the
 * line temperatures. There are two kind of  faults, short-circuit positive or
 * shot-circuit ground. These faults are active if the voltage read form the
 * \ref AIM is above or below the thresholds. These fault must be debounced and
 * reported.
 *
 * \{
 * \file  FCT.c
 * \brief  Generic code of the FCT module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApFCT.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "WdgM.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/*! Signal not available value for U16*/
#define FCT_SNA_U16				0xFFFF
/*! Number of elements inside the loop up table*/
#define FCT_TABLE_SIZE			39U
/*! Battery state not fault*/
#define FCT_BATTERY_NOT_FAULT	0U
/*! Gain used to convert degrees to decidegrees*/
#define FCT_C_TO_DECI_C			10U
/** Create an internal one second tick.*/
#define FCT_SEC_TO_DECI_SEC	10U;

/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/

/*! Temperatures managed by the module*/
typedef enum{
	/*! Alternative current line 1.*/
	FCT_AC1=0U,
	/*! Alternative current line 2.*/
	FCT_AC2,
	/*! Alternative current line 3.*/
	FCT_AC3,
	/*! Alternative current neutral.*/
	FCT_ACN,
	/*! Direct current line 1.*/
	FCT_DC1,
	/*! Direct current line 2.*/
	FCT_DC2,
	/*! Number of Temperatures managed by the module.*/
	FCT_TEMP_NUMBER
}FCT_temperature_enum_t;

/*! List of faults reported by the module*/
typedef enum{
	/*! Short circuit positive*/
	FCT_SCP=0U,
	/*! Short circuit ground*/
	FCT_SCG,
	/* Number of fault reported by the module*/
	FCT_FAULT_NUMBER
}FCT_fault_enum_t;

/*! Actions that required a debounce.*/
typedef enum{
	/*! Fault deactivation.*/
	FCT_FAULT_DEACTIVATION = 0U,
	/*! Fault activation.*/
	FCT_FAULT_ACTIVATION,
	/*! Number of actions that require a debounce*/
	FCT_FAUTL_TRANSITION_NUMBER
}FCT_fault_transition_t;

/** Limits definition*/
typedef enum{
	FCT_UPPER_LIMIT = 0,
	FCT_LOWER_LIMIT = 1,
	FCT_LIMIT_NUM = 2
}FCT_limit_t;

/*PRQA S 3205 --*/

/*!
 * \brief Debounce object.
 * \details This structure contains all the elements needed to perform a
 * debounce.
 */
typedef struct {
	/*! Output state.*/
	boolean out_state;
	/*! Debounce counter.*/
	uint16 counter;
	/*! Debounce time for each transition.*/
	uint16 counter_max[FCT_FAUTL_TRANSITION_NUMBER];
}FCT_debounce_t;


typedef struct{
	/*! Prefault state.*/
	boolean pre_fault;
	/*! Threshold to activate the fault.*/
	uint16 threshold;
	/*! Debounce object*/
	FCT_debounce_t debounce;
}FCT_fault_t;

/*!
 * \brief Point structure.
 * \details The loop up tables are use to convert voltage to temperature.
 * therefore all the elements inside the look up table link a voltage value
 * to a temperature value.
 */
typedef struct{
	/*! Temperature.*/
	uint8 temperature;
	/*! Voltage*/
	uint16 voltage_mv;
}FCT_point_t;

/*!
 * \brief Temperature structure.
 * \details This structure contains all the elements needed to manage an
 * input signal.
 *
 */
typedef struct {
	/*! Function enable.*/
	boolean enable;
	/*! Voltage value read from \ref AIM*/
	uint16 MsrTemp_Raw;
	/*! Temperature after conversion in decidegrees and -40 degrees offset.*/
	uint16 MsrTemp_Deg;
	/*! Temperature reported in decidegrees -40 degrees offset.*/
	uint16 OutputTemp_Meas;
	/*! Look up table used for the conversion.*/
	FCT_point_t table[FCT_TABLE_SIZE];
	/*! Substitute value in case of fault.*/
	uint8 substitute_val;
	/*! Fault object.*/
	FCT_fault_t fault[FCT_FAULT_NUMBER];
	/*! Ramp state.*/
	boolean ramp_active;
	/*! Ramp slope*/
	uint8 ramp_slope;
}FCT_temperature_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*!
 * \brief Temperatures array.
 * \details This array contains all the \ref FCT_temperature_t temperatures
 * managed by the module.
 */
static FCT_temperature_t FCT_temperature[FCT_TEMP_NUMBER];
/*!
 * \brief Battery state.
 * \details Current battery state.
 */
static uint8 FCT_battery_state;
/*!
 * \brief ECU state.
 * \details Current ECU state.
 */
static uint8 FCT_ecu_state;

static uint8 FCT_OneSecCounter;

static DCDC_FaultLampRequest FCT_fault_lamp_request;
static BSI_MainWakeup FCT_BSI_MainWakeup_last_state;

#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/*!
 * \brief Get DFA input.
 * \details This function gets from \red DFA all the module's inputs.
 */
static void FCT_get_inputs(void);
/*!
 * \brief Update Temperatures in DFA.
 * \details This function updates the reported temperature.
 * \param[in] FTC_temp_line Line temperature.
 */
static void FCT_tempOut_update(uint8 FTC_temp_line);

/*!
 * \brief Update Faults in DFA.
 * \details This function updates the fault state.
 * \param[in] FTC_temp_line Line temperature.
 */
static void FCT_faultOut_update(void);
/*!
 * \brief Converti Measure function.
 * \details This function is used to convert the voltage read from the AIM to
 * temperature, using the look up table and an interpolation.
 * \param[in] FCT_input Input voltage value.
 * \param[in] FCT_table Table used for the interpolation.
 * \return Temperature in deci-degrees.
 */
static uint16 FCT_ConvertMeasure(uint16 FCT_input,const FCT_point_t FCT_table[FCT_TABLE_SIZE]);

/** \brief Find the nearest edges
 * 	\details This function updates the edges if the current point is closer to the input
 * 	\param[in] FCT_input Input voltage value.
 * 	\param[in] CurrentPoint New point to be checked.
 * 	\param[in|out] Edges Current edges.
 */
static void FCT_findEdge(uint16 FCT_input, FCT_point_t CurrentPoint,
												 FCT_point_t (*Edges)[FCT_LIMIT_NUM]);
/**
 * \brief linearization function.
 * \details This function returns interpolation result.
 * \param[in] FCT_input Input point
 * \param[in] FCT_tableLimits Interpolation limits.
 * \param[in] FCT_Interval Interpolation points.
 */
static uint16 FCT_Linearize(uint16 FCT_input, const FCT_point_t FCT_tableLimits[2], const FCT_point_t FCT_Interval[2]);
/*!
 * \brief Fault debounce.
 * \details This function implements the debounce for the fault state report.
 * \param[in] FCT_temp_index Line temperature.
 * \param[in] FCT_fault_index Debounce index.
 */
static void FCT_fault_debounce(uint8 FCT_temp_index,
				FCT_fault_enum_t FCT_fault_index);
/*!
 * \brief Ramp.
 * \details This function implements a ramp.
 * \param[in] FTC_temp_line Line temperature.
 * \param[in] FCT_previou_val Last output.
 * \param[in] FCT_objective Ramp objective.
 * \param[in] FCT_slope Ramps slope.
 * \return Ramp output.
 */
static uint16 FCT_ramp(uint16 FCT_previou_val, uint16 FCT_objective,
						uint8 FCT_slope, uint8 FCT_temp_line);
/*!
 * \brief SCP pre-fault detection.
 * \details This function detects if there is a SCP fault in the input raw signal.
 * \param[in] FTC_temp_line Line temperature.
 */
static void FCT_preFault_detection_SCP(uint8 FCT_temp_line);
/*!
 * \brief SCG pre-fault detection.
 * \details This function detects if there is a SCG fault in the input raw signal.
 * \param[in] FTC_temp_line Line temperature.
 */
static void FCT_preFault_detection_SCG(uint8 FCT_temp_line);
/*!
 * \brief Output selection.
 * \details Depending on the fault state and the input value the module should
 * report a different value.
 * \param[in] FTC_temp_line Line temperature.
 */
static void FCT_output_selection(uint8 FCT_temp_line);

/** Init Sub-function used to reduce the halstead.*/
static void FCT_init_AC1(void);
/** Init Sub-function used to reduce the halstead.*/
static void FCT_init_AC2(void);
/** Init Sub-function used to reduce the halstead.*/
static void FCT_init_AC3(void);
/** Init Sub-function used to reduce the halstead.*/
static void FCT_init_ACN(void);
/** Init Sub-function used to reduce the halstead.*/
static void FCT_init_DC1(void);
/** Init Sub-function used to reduce the halstead.*/
static void FCT_init_DC2(void);

static void FCT_OnSecTick(void);


/** Update calibratables*/
static void FCT_UpdateCalibratables(void);

/*!
 * \brief Fault Lamp request signal managing.
 * \details Sets or clears the lamp request fault according to the conditions
 */
static void FCT_faultLampRequest(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtLinTableGlobalTempElement: Integer in interval [0...255]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtLinTableGlobalVoltElement: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtMsrTempRaw: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtOutputTempAftsales: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtRampGradient: Integer in interval [0...100]
 *   Unit: [deg C / s], Factor: 0.1, Offset: 0
 * IdtSubstituteValueTemp: Integer in interval [0...240]
 *   Unit: [deg C], Factor: 1, Offset: -40
 * IdtThresholdTempFault: Integer in interval [0...5000]
 *   Unit: [mV], Factor: 1, Offset: 0
 * IdtTimeDiagTemp: Integer in interval [0...1000]
 *   Unit: [ms], Factor: 100, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_RCD (0U)
 *   Cx1_No_main_wake_up_request (1U)
 *   Cx2_Main_wake_up_request (2U)
 *   Cx3_Not_valid (3U)
 * DCDC_FaultLampRequest: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_fault (0U)
 *   Cx1_Warning_limited_performance (1U)
 *   Cx2_Short_time_fault_Need_reset (2U)
 *   Cx3_Permernant_fault_need_replaced (3U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 * Array Types:
 * ============
 * IdtLinTableGlobalTemp: Array with 39 element(s) of type IdtLinTableGlobalTempElement
 * IdtLinTableGlobalVolt: Array with 39 element(s) of type IdtLinTableGlobalVoltElement
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimTempAC1CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempAC1CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempAC2CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempAC2CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempAC3CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempAC3CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempACNCounterSCG(void)
 *   uint16 *Rte_Pim_PimTempACNCounterSCP(void)
 *   uint16 *Rte_Pim_PimTempDC1CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempDC1CounterSCP(void)
 *   uint16 *Rte_Pim_PimTempDC2CounterSCG(void)
 *   uint16 *Rte_Pim_PimTempDC2CounterSCP(void)
 *   boolean *Rte_Pim_PimTempAC1PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempAC1PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempAC2PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempAC2PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempAC3PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempAC3PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempACNPrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempACNPrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempDC1PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempDC1PrefaultSCP(void)
 *   boolean *Rte_Pim_PimTempDC2PrefaultSCG(void)
 *   boolean *Rte_Pim_PimTempDC2PrefaultSCP(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_AC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_AC2(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_AC3(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_ACN(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_DC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCG_DC2(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_AC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_AC2(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_AC3(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_ACN(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_DC1(void)
 *   IdtThresholdTempFault Rte_CData_CalThresholdSCP_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCG_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeConfSCP_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCG_DC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_AC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_AC2(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_AC3(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_ACN(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_DC1(void)
 *   IdtTimeDiagTemp Rte_CData_CalTimeHealSCP_DC2(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_AC1(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_AC2(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_AC3(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_ACN(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_DC1(void)
 *   IdtRampGradient Rte_CData_CalRampGradient_DC2(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueAC1_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueAC2_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueAC3_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueACN_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueDC1_C(void)
 *   IdtSubstituteValueTemp Rte_CData_CalSubstituteValueDC2_C(void)
 *   boolean Rte_CData_CalCtrlFlowAC1Plug(void)
 *   boolean Rte_CData_CalCtrlFlowAC2Plug(void)
 *   boolean Rte_CData_CalCtrlFlowAC3Plug(void)
 *   boolean Rte_CData_CalCtrlFlowACNPlug(void)
 *   boolean Rte_CData_CalCtrlFlowDC1Plug(void)
 *   boolean Rte_CData_CalCtrlFlowDC2Plug(void)
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_AC1(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_AC2(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_AC3(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_ACN(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_DC1(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalTempElement *Rte_CData_CalLinTableGlobalTemp_DC2(void)
 *     Returnvalue: IdtLinTableGlobalTempElement* is of type IdtLinTableGlobalTemp
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_AC1(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_AC2(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_AC3(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_ACN(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_DC1(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *   IdtLinTableGlobalVoltElement *Rte_CData_CalLinTableGlobalVoltage_DC2(void)
 *     Returnvalue: IdtLinTableGlobalVoltElement* is of type IdtLinTableGlobalVolt
 *
 *********************************************************************************************************************/


#define CtApFCT_START_SEC_CODE
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCT_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_init_doc
 *********************************************************************************************************************/

/*!
 * \brief FCT initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCT_CODE) RCtApFCT_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_fault_type;
	uint8 FCT_temp_line;


	FCT_init_AC1();
	FCT_init_AC2();
	FCT_init_AC3();
	FCT_init_ACN();
	FCT_init_DC1();
	FCT_init_DC2();

	FCT_OneSecCounter = 0U;
	FCT_fault_lamp_request = Cx0_No_fault;
	FCT_BSI_MainWakeup_last_state = Cx0_Invalid_RCD;

	/* PRQA S 2991, 2995 -- */

	for(FCT_temp_line=0U;FCT_temp_line<(uint8)FCT_TEMP_NUMBER;FCT_temp_line++)
	{
		FCT_temperature[FCT_temp_line].OutputTemp_Meas = FCT_SNA_U16;
		FCT_temperature[FCT_temp_line].MsrTemp_Deg = FCT_SNA_U16;
		FCT_temperature[FCT_temp_line].ramp_active = FALSE;

		for (FCT_fault_type=0U ; FCT_fault_type<(uint8)FCT_FAULT_NUMBER; FCT_fault_type++)
		{
			FCT_temperature[FCT_temp_line].fault[FCT_fault_type].debounce.out_state = FALSE;
			FCT_temperature[FCT_temp_line].fault[FCT_fault_type].debounce.counter = 0U;
			FCT_temperature[FCT_temp_line].fault[FCT_fault_type].pre_fault = FALSE;
		}

	}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApFCT_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(IdtMsrTempRaw *data)
 *   Std_ReturnType Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(IdtMsrTempRaw *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(IdtOutputTempAftsales data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC1FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC1FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC2FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC2FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC3FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempAC3FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempACNFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempACNFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC1FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC1FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC2FaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpTempFaults_DeTempDC2FaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_task100ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief FCT task.
  *
  * This function is called periodically by the scheduler. This function should
  * report the line temperatures and update the fault signals.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApFCT_CODE) RCtApFCT_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApFCT_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_temp_line;
	boolean FCT_monitoring_conditions[FCT_TEMP_NUMBER];


	FCT_UpdateCalibratables();

	FCT_get_inputs();
	FCT_OnSecTick();

	for(FCT_temp_line=0U;FCT_temp_line<(uint8)FCT_TEMP_NUMBER;FCT_temp_line++)
	{
		FCT_monitoring_conditions[FCT_temp_line] = FALSE;

		/*SNA management*/
		if((uint16)FCT_SNA_U16 != FCT_temperature[FCT_temp_line].MsrTemp_Raw)
		{
			/*Module enable*/
			if(TRUE == FCT_temperature[FCT_temp_line].enable)
			{
				if (FCT_battery_state == BAT_VALID_RANGE)
				{
					FCT_monitoring_conditions[FCT_temp_line] = TRUE;
				}

				/*linearize*/
				FCT_temperature[FCT_temp_line].MsrTemp_Deg = FCT_ConvertMeasure(
						 FCT_temperature[FCT_temp_line].MsrTemp_Raw,
						 FCT_temperature[FCT_temp_line].table);

				/*Prefault detection*/
				FCT_preFault_detection_SCP(FCT_temp_line);
				FCT_preFault_detection_SCG(FCT_temp_line);

				/*Apply debounce*/
				FCT_fault_debounce(FCT_temp_line,FCT_SCP);
				FCT_fault_debounce(FCT_temp_line,FCT_SCG);

				/*Output selection*/
				FCT_output_selection(FCT_temp_line);

			}
			/*Default value when the module is disable*/
			/*This code will be executed only once to get the first available measure.*/
			else if((uint16)FCT_SNA_U16 == FCT_temperature[FCT_temp_line].OutputTemp_Meas)
			{

				FCT_temperature[FCT_temp_line].MsrTemp_Deg = FCT_ConvertMeasure(
										 FCT_temperature[FCT_temp_line].MsrTemp_Raw,
										 FCT_temperature[FCT_temp_line].table);
				FCT_temperature[FCT_temp_line].OutputTemp_Meas =
						FCT_temperature[FCT_temp_line].MsrTemp_Deg;

				FCT_tempOut_update(FCT_temp_line);
			}
			else
			{
				/*Module disable. The output has been set to the first available measure.*/
				/*MISRA*/
			}
			if(	   (1U==FCT_ecu_state)
					|| (3U==FCT_ecu_state)
					|| (4U==FCT_ecu_state)
					|| (5U==FCT_ecu_state)
				)
			{
				FCT_tempOut_update(FCT_temp_line);
			}

		}

	}

	(void)Rte_Write_PpTempMonitoringConditions_DeAC1TempMonitoringConditions(FCT_monitoring_conditions[FCT_AC1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempMonitoringConditions_DeAC2TempMonitoringConditions(FCT_monitoring_conditions[FCT_AC2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempMonitoringConditions_DeAC3TempMonitoringConditions(FCT_monitoring_conditions[FCT_AC3]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempMonitoringConditions_DeACNTempMonitoringConditions(FCT_monitoring_conditions[FCT_ACN]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempMonitoringConditions_DeDC1TempMonitoringConditions(FCT_monitoring_conditions[FCT_DC1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempMonitoringConditions_DeDC2TempMonitoringConditions(FCT_monitoring_conditions[FCT_DC2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	*Rte_Pim_PimTempAC1CounterSCG() = (uint16)FCT_temperature[FCT_AC1].fault[FCT_SCG].debounce.counter;
	*Rte_Pim_PimTempAC2CounterSCG() = (uint16)FCT_temperature[FCT_AC2].fault[FCT_SCG].debounce.counter;
	*Rte_Pim_PimTempAC3CounterSCG() = (uint16)FCT_temperature[FCT_AC3].fault[FCT_SCG].debounce.counter;
	*Rte_Pim_PimTempACNCounterSCG() = (uint16)FCT_temperature[FCT_ACN].fault[FCT_SCG].debounce.counter;
	*Rte_Pim_PimTempDC1CounterSCG() = (uint16)FCT_temperature[FCT_DC1].fault[FCT_SCG].debounce.counter;
	*Rte_Pim_PimTempDC2CounterSCG() = (uint16)FCT_temperature[FCT_DC2].fault[FCT_SCG].debounce.counter;

	*Rte_Pim_PimTempAC1CounterSCP() = (uint16)FCT_temperature[FCT_AC1].fault[FCT_SCP].debounce.counter;
	*Rte_Pim_PimTempAC2CounterSCP() = (uint16)FCT_temperature[FCT_AC2].fault[FCT_SCP].debounce.counter;
	*Rte_Pim_PimTempAC3CounterSCP() = (uint16)FCT_temperature[FCT_AC3].fault[FCT_SCP].debounce.counter;
	*Rte_Pim_PimTempACNCounterSCP() = (uint16)FCT_temperature[FCT_ACN].fault[FCT_SCP].debounce.counter;
	*Rte_Pim_PimTempDC1CounterSCP() = (uint16)FCT_temperature[FCT_DC1].fault[FCT_SCP].debounce.counter;
	*Rte_Pim_PimTempDC2CounterSCP() = (uint16)FCT_temperature[FCT_DC2].fault[FCT_SCP].debounce.counter;

	*Rte_Pim_PimTempAC1PrefaultSCG() =FCT_temperature[FCT_AC1].fault[FCT_SCG].pre_fault;
	*Rte_Pim_PimTempAC2PrefaultSCG() =FCT_temperature[FCT_AC2].fault[FCT_SCG].pre_fault;
	*Rte_Pim_PimTempAC3PrefaultSCG() =FCT_temperature[FCT_AC3].fault[FCT_SCG].pre_fault;
	*Rte_Pim_PimTempACNPrefaultSCG() =FCT_temperature[FCT_ACN].fault[FCT_SCG].pre_fault;
	*Rte_Pim_PimTempDC1PrefaultSCG() =FCT_temperature[FCT_DC1].fault[FCT_SCG].pre_fault;
	*Rte_Pim_PimTempDC2PrefaultSCG() =FCT_temperature[FCT_DC2].fault[FCT_SCG].pre_fault;

	*Rte_Pim_PimTempAC1PrefaultSCP() =FCT_temperature[FCT_AC1].fault[FCT_SCP].pre_fault;
	*Rte_Pim_PimTempAC2PrefaultSCP() =FCT_temperature[FCT_AC2].fault[FCT_SCP].pre_fault;
	*Rte_Pim_PimTempAC3PrefaultSCP() =FCT_temperature[FCT_AC3].fault[FCT_SCP].pre_fault;
	*Rte_Pim_PimTempACNPrefaultSCP() =FCT_temperature[FCT_ACN].fault[FCT_SCP].pre_fault;
	*Rte_Pim_PimTempDC1PrefaultSCP() =FCT_temperature[FCT_DC1].fault[FCT_SCP].pre_fault;
	*Rte_Pim_PimTempDC2PrefaultSCP() =FCT_temperature[FCT_DC2].fault[FCT_SCP].pre_fault;


	FCT_faultOut_update();

	/*Manage Fault Lamp Request*/
	FCT_faultLampRequest();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApFCT_STOP_SEC_CODE
#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/

static void FCT_get_inputs(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtAppRCDECUState ECUstate_temp;
	IdtBatteryVoltageState BatteryState_temp;
	IdtMsrTempRaw LineTemperature_temp;


	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&ECUstate_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_ecu_state=(uint8)ECUstate_temp;

	(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&BatteryState_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_battery_state = (uint8)BatteryState_temp;

	(void)Rte_Read_PpMsrTempRaw_DeMsrTempAC1Raw(&LineTemperature_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_temperature[FCT_AC1].MsrTemp_Raw = (uint16)LineTemperature_temp;
	(void)Rte_Read_PpMsrTempRaw_DeMsrTempAC2Raw(&LineTemperature_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_temperature[FCT_AC2].MsrTemp_Raw = (uint16)LineTemperature_temp;
	(void)Rte_Read_PpMsrTempRaw_DeMsrTempAC3Raw(&LineTemperature_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_temperature[FCT_AC3].MsrTemp_Raw = (uint16)LineTemperature_temp;
	(void)Rte_Read_PpMsrTempRaw_DeMsrTempACNRaw(&LineTemperature_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_temperature[FCT_ACN].MsrTemp_Raw = (uint16)LineTemperature_temp;
	(void)Rte_Read_PpMsrTempRaw_DeMsrTempDC1Raw(&LineTemperature_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_temperature[FCT_DC1].MsrTemp_Raw = (uint16)LineTemperature_temp;
	(void)Rte_Read_PpMsrTempRaw_DeMsrTempDC2Raw(&LineTemperature_temp);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	FCT_temperature[FCT_DC2].MsrTemp_Raw = (uint16)LineTemperature_temp;

}

static void FCT_tempOut_update(uint8 FTC_temp_line)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtOutputTempAftsales after_sales;
	IdtOutputTempMeas OutputTemp_Meas;


	if((uint8)FCT_TEMP_NUMBER>FTC_temp_line)
	{
		after_sales = (IdtOutputTempAftsales)(FCT_temperature[FTC_temp_line].MsrTemp_Deg/(uint16)FCT_C_TO_DECI_C);
		OutputTemp_Meas = (IdtOutputTempMeas)(FCT_temperature[FTC_temp_line].OutputTemp_Meas);
		switch(FTC_temp_line)
		{
				case (uint8)FCT_AC1:
					(void)Rte_Write_PpOutputTempMeas_DeOutputTempAC1Meas(OutputTemp_Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					(void)Rte_Write_PpOutputTempAftsales_DeOutputTempAC1Aftsales(after_sales);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					break;
				case (uint8)FCT_AC2:
					(void)Rte_Write_PpOutputTempMeas_DeOutputTempAC2Meas(OutputTemp_Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					(void)Rte_Write_PpOutputTempAftsales_DeOutputTempAC2Aftsales(after_sales);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					break;
				case (uint8)FCT_AC3:
					(void)Rte_Write_PpOutputTempMeas_DeOutputTempAC3Meas(OutputTemp_Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					(void)Rte_Write_PpOutputTempAftsales_DeOutputTempAC3Aftsales(after_sales);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					break;
				case (uint8)FCT_ACN:
					(void)Rte_Write_PpOutputTempMeas_DeOutputTempACNMeas(OutputTemp_Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					(void)Rte_Write_PpOutputTempAftsales_DeOutputTempACNAftsales(after_sales);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					break;
				case (uint8)FCT_DC1:
					(void)Rte_Write_PpOutputTempMeas_DeOutputTempDC1Meas(OutputTemp_Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					(void)Rte_Write_PpOutputTempAftsales_DeOutputTempDC1Aftsales(after_sales);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					break;
				case (uint8)FCT_DC2:
					(void)Rte_Write_PpOutputTempMeas_DeOutputTempDC2Meas(OutputTemp_Meas);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					(void)Rte_Write_PpOutputTempAftsales_DeOutputTempDC2Aftsales(after_sales);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
					break;
				default:
					/*MISRA*/
					break;
			}
	}
}

static void FCT_faultOut_update(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	(void)Rte_Write_PpTempFaults_DeTempAC1FaultSCP(FCT_temperature[FCT_AC1].fault[FCT_SCP].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempFaults_DeTempAC1FaultSCG(FCT_temperature[FCT_AC1].fault[FCT_SCG].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpTempFaults_DeTempAC2FaultSCP(FCT_temperature[FCT_AC2].fault[FCT_SCP].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempFaults_DeTempAC2FaultSCG(FCT_temperature[FCT_AC2].fault[FCT_SCG].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpTempFaults_DeTempAC3FaultSCP(FCT_temperature[FCT_AC3].fault[FCT_SCP].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempFaults_DeTempAC3FaultSCG(FCT_temperature[FCT_AC3].fault[FCT_SCG].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpTempFaults_DeTempACNFaultSCP(FCT_temperature[FCT_ACN].fault[FCT_SCP].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempFaults_DeTempACNFaultSCG(FCT_temperature[FCT_ACN].fault[FCT_SCG].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpTempFaults_DeTempDC1FaultSCP(FCT_temperature[FCT_DC1].fault[FCT_SCP].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempFaults_DeTempDC1FaultSCG(FCT_temperature[FCT_DC1].fault[FCT_SCG].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Write_PpTempFaults_DeTempDC2FaultSCP(FCT_temperature[FCT_DC2].fault[FCT_SCP].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpTempFaults_DeTempDC2FaultSCG(FCT_temperature[FCT_DC2].fault[FCT_SCG].debounce.out_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}

static uint16 FCT_ConvertMeasure(uint16 FCT_input, const FCT_point_t FCT_table[FCT_TABLE_SIZE])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_index_table;
	FCT_point_t FCT_tableLimits[FCT_LIMIT_NUM] = {{0x00,0x0000},{0x00,0xFFFF}};/*Initialize to min value*/
	FCT_point_t FCT_Interval[FCT_LIMIT_NUM] = {{0x00,0xFFFF},{0x00,0x0000}};/*Initialize to max value*/


	if(NULL_PTR!= FCT_table)
	{
		/* If two points are placed at the same position (same voltage),
		 * the function will only use the first one.*/

		/*find sector*/
		for(FCT_index_table=0U; FCT_index_table<FCT_TABLE_SIZE; FCT_index_table++)
		{
			/*find nearest points*/
			FCT_findEdge(FCT_input, FCT_table[FCT_index_table], &FCT_Interval);


			/*find table boundary*/
			if(FCT_table[FCT_index_table].voltage_mv > FCT_tableLimits[FCT_UPPER_LIMIT].voltage_mv)
			{
				FCT_tableLimits[FCT_UPPER_LIMIT] = FCT_table[FCT_index_table];
			}
			else if(FCT_table[FCT_index_table].voltage_mv < FCT_tableLimits[FCT_LOWER_LIMIT].voltage_mv)
			{
				FCT_tableLimits[FCT_LOWER_LIMIT] = FCT_table[FCT_index_table];
			}
			else
			{
				/*MISRA*/
			}
		}
	}
	return FCT_Linearize(FCT_input, FCT_tableLimits, FCT_Interval);
}
static void FCT_findEdge(uint16 FCT_input, FCT_point_t CurrentPoint,
												 FCT_point_t (*Edges)[FCT_LIMIT_NUM])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(NULL_PTR != Edges)
	{
		sint32 CurrentDif_S32 = (sint32)CurrentPoint.voltage_mv - (sint32)FCT_input;

		/*find nearest points*/
		if((sint32)0 <= CurrentDif_S32)
		{
			/*Right edge*/
			sint32 PositiveDif_S32 = (sint32)(*Edges)[FCT_UPPER_LIMIT].voltage_mv - (sint32)FCT_input;

			/*check if the interval is thiner than previous*/
			if((CurrentDif_S32 < PositiveDif_S32)||((sint32)0>PositiveDif_S32))
				{
				(*Edges)[FCT_UPPER_LIMIT]=CurrentPoint;
				}
		}
		else
		{
			/*Left edge*/
			sint32 NegativeDif_S32 = (sint32)FCT_input - (sint32)(*Edges)[FCT_LOWER_LIMIT].voltage_mv;
			/*check if the interval is thiner than previous*/
			if((-CurrentDif_S32 < NegativeDif_S32)||((sint32)0>NegativeDif_S32))
			{
				(*Edges)[FCT_LOWER_LIMIT]=CurrentPoint;
			}
		}
	}
}

static uint16 FCT_Linearize(uint16 FCT_input, const FCT_point_t FCT_tableLimits[FCT_LIMIT_NUM], const FCT_point_t FCT_Interval[FCT_LIMIT_NUM])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 FCT_result;


	/*Upper saturation*/
	if(FCT_input >= FCT_tableLimits[FCT_UPPER_LIMIT].voltage_mv)
	{
		FCT_result = (uint16)FCT_tableLimits[FCT_UPPER_LIMIT].temperature*(uint16)FCT_C_TO_DECI_C;
	}
	/*Lower saturation*/
	else if (FCT_input <= FCT_tableLimits[FCT_LOWER_LIMIT].voltage_mv)
	{
		FCT_result = (uint16)FCT_tableLimits[FCT_LOWER_LIMIT].temperature*(uint16)FCT_C_TO_DECI_C;
	}
	/*No 0 division hazard, but checked.*/
	/*The first boundary is always at the same position or at the left of the point.*/
	/*The second boundary is always at the right of the point.*/
	else if(FCT_Interval[FCT_LOWER_LIMIT].voltage_mv!=FCT_Interval[FCT_UPPER_LIMIT].voltage_mv)
	{
		/*Interpolation, overflow is under control*/
		FCT_result = (uint16)(uint32)(sint32)
				(
				((sint32)FCT_Interval[FCT_UPPER_LIMIT].temperature*(sint32)FCT_C_TO_DECI_C)
				+
				(
					(
						((sint32)FCT_Interval[FCT_LOWER_LIMIT].temperature-(sint32)FCT_Interval[FCT_UPPER_LIMIT].temperature)
						*
						((sint32)FCT_input-(sint32)FCT_Interval[FCT_UPPER_LIMIT].voltage_mv)
						*
						(sint32)FCT_C_TO_DECI_C
					)
					/
					((sint32)FCT_Interval[FCT_LOWER_LIMIT].voltage_mv-(sint32)FCT_Interval[FCT_UPPER_LIMIT].voltage_mv)
				)
				);
	}
	else
	{
		/*Not possible branch*/
		FCT_result = 0U;
	}
	return FCT_result;
}
static void FCT_preFault_detection_SCP(uint8 FCT_temp_line)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Activation condition*/
	if((FALSE==FCT_temperature[FCT_temp_line].fault[FCT_SCP].pre_fault)
		&& (FCT_temperature[FCT_temp_line].MsrTemp_Raw>FCT_temperature[FCT_temp_line].fault[FCT_SCP].threshold)
		&& (FCT_BATTERY_NOT_FAULT==FCT_battery_state))
	{
		FCT_temperature[FCT_temp_line].fault[FCT_SCP].pre_fault = TRUE;
	}
	/*Deactivation condition.*/
	else if((TRUE==FCT_temperature[FCT_temp_line].fault[FCT_SCP].pre_fault)
			&& (FCT_temperature[FCT_temp_line].MsrTemp_Raw<=FCT_temperature[FCT_temp_line].fault[FCT_SCP].threshold))
	{

		FCT_temperature[FCT_temp_line].fault[FCT_SCP].pre_fault = FALSE;
	}
	else
	{
		/*MISRA*/
	}
}

static void FCT_preFault_detection_SCG(uint8 FCT_temp_line)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Activation condition*/
	if((FALSE==FCT_temperature[FCT_temp_line].fault[FCT_SCG].pre_fault)
		&& (FCT_temperature[FCT_temp_line].MsrTemp_Raw<FCT_temperature[FCT_temp_line].fault[FCT_SCG].threshold)
		&& (FCT_BATTERY_NOT_FAULT==FCT_battery_state))
	{
		FCT_temperature[FCT_temp_line].fault[FCT_SCG].pre_fault = TRUE;
	}
	/*Deactivation condition.*/
	else if((TRUE==FCT_temperature[FCT_temp_line].fault[FCT_SCG].pre_fault)
			&& (FCT_temperature[FCT_temp_line].MsrTemp_Raw>=FCT_temperature[FCT_temp_line].fault[FCT_SCG].threshold))
	{

		FCT_temperature[FCT_temp_line].fault[FCT_SCG].pre_fault = FALSE;
	}
	else
	{
		/*MISRA*/
	}

}

static void FCT_output_selection(uint8 FCT_temp_line)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 RampObjective;


	/*Set output depending on fault state*/
	if (FCT_BATTERY_NOT_FAULT != FCT_battery_state)
	{
		/*Battery Fault*/
		 FCT_temperature[FCT_temp_line].OutputTemp_Meas =
						 (uint16)FCT_temperature[FCT_temp_line].substitute_val
						 *(uint16)FCT_C_TO_DECI_C;
	}
	else if((FALSE != FCT_temperature[FCT_temp_line].fault[FCT_SCP].debounce.out_state)
			|| (FALSE != FCT_temperature[FCT_temp_line].fault[FCT_SCG].debounce.out_state))
	{
		if(TRUE == FCT_temperature[FCT_temp_line].ramp_active)
		{
			/*Fault confirmed, ramp until substitutive value*/
			RampObjective = (uint16)FCT_temperature[FCT_temp_line].substitute_val
					 *(uint16)FCT_C_TO_DECI_C;
			/*Apply ramp to output*/
			FCT_temperature[FCT_temp_line].OutputTemp_Meas = FCT_ramp(
					FCT_temperature[FCT_temp_line].OutputTemp_Meas,
					RampObjective,
					FCT_temperature[FCT_temp_line].ramp_slope,
					FCT_temp_line);
		}
	}
	else if(TRUE == FCT_temperature[FCT_temp_line].ramp_active)
	{
		/*Leaving fault, ramp until measured temperature.*/
		RampObjective = FCT_temperature[FCT_temp_line].MsrTemp_Deg;

		/*Apply ramp to output*/
		FCT_temperature[FCT_temp_line].OutputTemp_Meas = FCT_ramp(
				FCT_temperature[FCT_temp_line].OutputTemp_Meas,
				RampObjective,
				FCT_temperature[FCT_temp_line].ramp_slope,
				FCT_temp_line);
	}
	else if((FALSE != FCT_temperature[FCT_temp_line].fault[FCT_SCP].pre_fault)
			|| (FALSE != FCT_temperature[FCT_temp_line].fault[FCT_SCG].pre_fault))
	{
		/*Pre fault detected, not update the output.*/
	}
	else
	{
		/*Don't fault detected, Don't apply ramp*/
		FCT_temperature[FCT_temp_line].OutputTemp_Meas = FCT_temperature[FCT_temp_line].MsrTemp_Deg;

	}
}

static void FCT_fault_debounce(uint8 FCT_temp_index,
								FCT_fault_enum_t FCT_fault_index)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* The input state is where the functions wants to go. Therefore, depending
	 * on that state the function should use a specific debounce time.*/
	boolean FCT_input = FCT_temperature[FCT_temp_index].fault[FCT_fault_index].pre_fault;
	uint16 FCT_counterTime_max;


	if(TRUE == FCT_input)
	{
		FCT_counterTime_max = FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.counter_max[FCT_FAULT_ACTIVATION];
	}
	else
	{
		FCT_counterTime_max = FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.counter_max[FCT_FAULT_DEACTIVATION];
	}


	if(FCT_input == FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.out_state)
	{
		/*Edge detected, clean counter*/
		FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.counter=0U;
	}
	else if(FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.counter < FCT_counterTime_max)
	{
		/*update counter*/
		FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.counter++;
	}
	else
	{
		/*Counter time reached, update output*/
		FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.out_state = FCT_input;
		FCT_temperature[FCT_temp_index].fault[FCT_fault_index].debounce.counter=0U;
		/*Ramp activation*/
		FCT_temperature[FCT_temp_index].ramp_active = TRUE;
	}

}

static uint16 FCT_ramp(uint16 FCT_previou_val, uint16 FCT_objective,
						uint8 FCT_slope, uint8 FCT_temp_line)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	sint32 FCT_result;


	if(0U==FCT_OneSecCounter)
	{
		if(FCT_objective >= FCT_previou_val)
		{
			/*positive slope*/
			FCT_result = (sint32)FCT_previou_val + (sint32)FCT_slope;
			if(FCT_result >= (sint32)FCT_objective)
			{
				/*Upper saturation*/
				FCT_result = (sint32)FCT_objective;
				/*Ramp is over.*/
				FCT_temperature[FCT_temp_line].ramp_active = FALSE;
			}
		}
		else
		{
			/*negative slop*/
			FCT_result = (sint32)FCT_previou_val - (sint32)FCT_slope;
			if(FCT_result <= (sint32)FCT_objective)
			{
				/*Lower saturation*/
				FCT_result = (sint32)FCT_objective;
				/*Ramp is over.*/
				FCT_temperature[FCT_temp_line].ramp_active = FALSE;
			}
		}
	}
	else
	{
		FCT_result = (sint32)FCT_previou_val;
	}

	/*Overflow and Underflow have been taking into account.*/
	return (uint16)(uint32)FCT_result;

}
static void FCT_init_AC1(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_table_index;


	/* Rule 5.11:The result of this logical operation is always 'true'.*/
	/* PRQA S 2991, 2995 ++ #This is a DFA protection.*/
	FCT_temperature[FCT_AC1].enable=Rte_CData_CalCtrlFlowAC1Plug();
	FCT_temperature[FCT_AC1].fault[FCT_SCP].threshold= Rte_CData_CalThresholdSCP_AC1();
	FCT_temperature[FCT_AC1].fault[FCT_SCG].threshold= Rte_CData_CalThresholdSCG_AC1();
	FCT_temperature[FCT_AC1].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCP_AC1();
	FCT_temperature[FCT_AC1].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCG_AC1();
	FCT_temperature[FCT_AC1].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCP_AC1();
	FCT_temperature[FCT_AC1].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCG_AC1();
	FCT_temperature[FCT_AC1].substitute_val = Rte_CData_CalSubstituteValueAC1_C();
	FCT_temperature[FCT_AC1].ramp_slope = Rte_CData_CalRampGradient_AC1();
	for(FCT_table_index=0U;FCT_table_index<FCT_TABLE_SIZE;FCT_table_index++){

		FCT_temperature[FCT_AC1].table[FCT_table_index].temperature =  (Rte_CData_CalLinTableGlobalTemp_AC1())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		FCT_temperature[FCT_AC1].table[FCT_table_index].voltage_mv =  (Rte_CData_CalLinTableGlobalVoltage_AC1())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}
static void FCT_init_AC2(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_table_index;


	FCT_temperature[FCT_AC2].enable=Rte_CData_CalCtrlFlowAC2Plug();
	FCT_temperature[FCT_AC2].fault[FCT_SCP].threshold=Rte_CData_CalThresholdSCP_AC2();
	FCT_temperature[FCT_AC2].fault[FCT_SCG].threshold=Rte_CData_CalThresholdSCG_AC2();
	FCT_temperature[FCT_AC2].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCP_AC2();
	FCT_temperature[FCT_AC2].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCG_AC2();
	FCT_temperature[FCT_AC2].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCP_AC2();
	FCT_temperature[FCT_AC2].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCG_AC2();
	FCT_temperature[FCT_AC2].substitute_val = Rte_CData_CalSubstituteValueAC2_C();
	FCT_temperature[FCT_AC2].ramp_slope = Rte_CData_CalRampGradient_AC2();
	for(FCT_table_index=0U;FCT_table_index<FCT_TABLE_SIZE;FCT_table_index++){
		FCT_temperature[FCT_AC2].table[FCT_table_index].temperature = (Rte_CData_CalLinTableGlobalTemp_AC2())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		FCT_temperature[FCT_AC2].table[FCT_table_index].voltage_mv = (Rte_CData_CalLinTableGlobalVoltage_AC2())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}
static void FCT_init_AC3(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_table_index;


	FCT_temperature[FCT_AC3].enable=Rte_CData_CalCtrlFlowAC3Plug();
	FCT_temperature[FCT_AC3].fault[FCT_SCP].threshold=Rte_CData_CalThresholdSCP_AC3();
	FCT_temperature[FCT_AC3].fault[FCT_SCG].threshold=Rte_CData_CalThresholdSCG_AC3();
	FCT_temperature[FCT_AC3].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCP_AC3();
	FCT_temperature[FCT_AC3].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCG_AC3();
	FCT_temperature[FCT_AC3].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCP_AC3();
	FCT_temperature[FCT_AC3].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCG_AC3();
	FCT_temperature[FCT_AC3].substitute_val = Rte_CData_CalSubstituteValueAC3_C();
	FCT_temperature[FCT_AC3].ramp_slope = Rte_CData_CalRampGradient_AC3();
	for(FCT_table_index=0U;FCT_table_index<FCT_TABLE_SIZE;FCT_table_index++){
		FCT_temperature[FCT_AC3].table[FCT_table_index].temperature = (Rte_CData_CalLinTableGlobalTemp_AC3())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		FCT_temperature[FCT_AC3].table[FCT_table_index].voltage_mv = (Rte_CData_CalLinTableGlobalVoltage_AC3())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}
static void FCT_init_ACN(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_table_index;


	FCT_temperature[FCT_ACN].enable=Rte_CData_CalCtrlFlowACNPlug();
	FCT_temperature[FCT_ACN].fault[FCT_SCP].threshold=Rte_CData_CalThresholdSCP_ACN();
	FCT_temperature[FCT_ACN].fault[FCT_SCG].threshold=Rte_CData_CalThresholdSCG_ACN();
	FCT_temperature[FCT_ACN].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCP_ACN();
	FCT_temperature[FCT_ACN].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCG_ACN();
	FCT_temperature[FCT_ACN].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCP_ACN();
	FCT_temperature[FCT_ACN].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCG_ACN();
	FCT_temperature[FCT_ACN].substitute_val = Rte_CData_CalSubstituteValueACN_C();
	FCT_temperature[FCT_ACN].ramp_slope = Rte_CData_CalRampGradient_ACN();
	for(FCT_table_index=0U;FCT_table_index<FCT_TABLE_SIZE;FCT_table_index++){
		FCT_temperature[FCT_ACN].table[FCT_table_index].temperature = (Rte_CData_CalLinTableGlobalTemp_ACN())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		FCT_temperature[FCT_ACN].table[FCT_table_index].voltage_mv = (Rte_CData_CalLinTableGlobalVoltage_ACN())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}
static void FCT_init_DC1(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_table_index;


	FCT_temperature[FCT_DC1].enable=Rte_CData_CalCtrlFlowDC1Plug();
	FCT_temperature[FCT_DC1].fault[FCT_SCP].threshold=Rte_CData_CalThresholdSCP_DC1();
	FCT_temperature[FCT_DC1].fault[FCT_SCG].threshold=Rte_CData_CalThresholdSCG_DC1();
	FCT_temperature[FCT_DC1].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCP_DC1();
	FCT_temperature[FCT_DC1].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCG_DC1();
	FCT_temperature[FCT_DC1].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCP_DC1();
	FCT_temperature[FCT_DC1].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCG_DC1();
	FCT_temperature[FCT_DC1].substitute_val = Rte_CData_CalSubstituteValueDC1_C();
	FCT_temperature[FCT_DC1].ramp_slope = Rte_CData_CalRampGradient_DC1();
	for(FCT_table_index=0U;FCT_table_index<FCT_TABLE_SIZE;FCT_table_index++){
		FCT_temperature[FCT_DC1].table[FCT_table_index].temperature = (Rte_CData_CalLinTableGlobalTemp_DC1())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		FCT_temperature[FCT_DC1].table[FCT_table_index].voltage_mv = (Rte_CData_CalLinTableGlobalVoltage_DC1())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}
static void FCT_init_DC2(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 FCT_table_index;


	FCT_temperature[FCT_DC2].enable=Rte_CData_CalCtrlFlowDC2Plug();
	FCT_temperature[FCT_DC2].fault[FCT_SCP].threshold=Rte_CData_CalThresholdSCP_DC2();
	FCT_temperature[FCT_DC2].fault[FCT_SCG].threshold=Rte_CData_CalThresholdSCG_DC2();
	FCT_temperature[FCT_DC2].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCP_DC2();
	FCT_temperature[FCT_DC2].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_ACTIVATION] = Rte_CData_CalTimeConfSCG_DC2();
	FCT_temperature[FCT_DC2].fault[FCT_SCP].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCP_DC2();
	FCT_temperature[FCT_DC2].fault[FCT_SCG].debounce.counter_max[FCT_FAULT_DEACTIVATION] = Rte_CData_CalTimeHealSCG_DC2();
	FCT_temperature[FCT_DC2].substitute_val = Rte_CData_CalSubstituteValueDC2_C();
	FCT_temperature[FCT_DC2].ramp_slope = Rte_CData_CalRampGradient_DC2();
	for(FCT_table_index=0U;FCT_table_index<FCT_TABLE_SIZE;FCT_table_index++){
		FCT_temperature[FCT_DC2].table[FCT_table_index].temperature = (Rte_CData_CalLinTableGlobalTemp_DC2())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		FCT_temperature[FCT_DC2].table[FCT_table_index].voltage_mv =  (Rte_CData_CalLinTableGlobalVoltage_DC2())[FCT_table_index];/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
}
static void FCT_OnSecTick(void)
{
	FCT_OneSecCounter ++;
	FCT_OneSecCounter %=FCT_SEC_TO_DECI_SEC;
}


/** Update calibratables*/
static void FCT_UpdateCalibratables(void)
{
	FCT_init_AC1();
	FCT_init_AC2();
	FCT_init_AC3();
	FCT_init_ACN();
	FCT_init_DC1();
	FCT_init_DC2();
}

static void FCT_faultLampRequest(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApFCT_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApFCT_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApFCT_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApFCT_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApFCT_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 temp_line;
	boolean SCG_or_SCP_detected;
	BSI_MainWakeup BSI_MainWakeup_current_state;
	boolean BSI_MainWakeup_transition_detected;

	(void)Rte_Read_PpInt_BSI_MainWakeup_BSI_MainWakeup(&BSI_MainWakeup_current_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/*Check if there is a SCP or SCG in any of the lines*/
	SCG_or_SCP_detected = FALSE;
	for(temp_line = 0U; temp_line < (uint8)FCT_TEMP_NUMBER; temp_line++)
		{
			if((FALSE != FCT_temperature[temp_line].fault[FCT_SCP].debounce.out_state)
				|| (FALSE != FCT_temperature[temp_line].fault[FCT_SCG].debounce.out_state)){

				SCG_or_SCP_detected = TRUE;

			}
		}

	/*Check if a transition in BSI_MainWakeup is detected*/
	if( (FCT_BSI_MainWakeup_last_state != Cx2_Main_wake_up_request) 
		&& (BSI_MainWakeup_current_state == Cx2_Main_wake_up_request) )
	{
		BSI_MainWakeup_transition_detected = TRUE;
	}
	else
	{
		BSI_MainWakeup_transition_detected = FALSE;
	}

	/*Set or clear the fault lamp request according to the conditions*/
	if(TRUE == SCG_or_SCP_detected){
		FCT_fault_lamp_request = Cx3_Permernant_fault_need_replaced;
	}
	else if( (FCT_fault_lamp_request == Cx3_Permernant_fault_need_replaced) 
		&& (BSI_MainWakeup_transition_detected == TRUE) )
	{
		FCT_fault_lamp_request = Cx0_No_fault;
	}
	else
	{ 
		/*MISRA*/
		/*Otherwise the fault lamp request remains unchanged*/ 
	}

	FCT_BSI_MainWakeup_last_state = BSI_MainWakeup_current_state;
	(void)Rte_Write_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(FCT_fault_lamp_request);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
