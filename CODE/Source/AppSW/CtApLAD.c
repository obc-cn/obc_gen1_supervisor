/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLAD.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApLAD
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLAD>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup LAD
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief E-Lock actuator.
 * \details This module Manages the E-lock set point.
 *
 * \{
 * \file  LAD.c
 * \brief  Generic code of the LAD module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApLAD.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/** 400ms delay.*/
#define LAD_PULSE_WIDTH	40U

#define LAD_DEAD_TIME		20U

#define LAD_INITIAL_DELAY	300U

#define LAD_UNDEFINE_STATE 0xFFU

#define LAD_THRESHOLD_GAIN	10U
#define LAD_TASK_PERIOD	(uint8)10U
#define LAD_ACTUATOR_FACTOR	100U

#define LAD_VEHSPD_FACTOR 100U

#define LAD_CURRENT_MEAS_DELAY	5U

/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

typedef enum{
	LAD_RESP_ONGOING = 0x01,
	LAD_RESP_END_OK = 0x02,
	LAD_RESP_END_NOK = 0x03,
	LAD_RESP_STOP = 0x04

}LAD_testState_t;/* PRQA S 3205 #Elemwnts of the enum are already used*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** E-Lock variant enable.*/
static boolean LAD_ElockActuatorEnable;

/** Thresholds */
static uint32 LAD_ElockOvercurrentThres;
static uint32 LAD_ElockThresholdSCG;
static uint32 LAD_ElockThresholdSCP;

static uint32 LAD_BatteryVoltMaxTest;
static uint32 LAD_BatteryVoltMinTest;
static uint32 LAD_VehStopMaxTest;

static uint32 LAD_NoCurrentThres;

/**Actuator times*/
static uint16 LAD_ElockHOpenCircuitConfirmTime;
static uint16 LAD_ElockHOpenCircuitHealTime;
static uint16 LAD_ElockHSCGConfirmTime;
static uint16 LAD_ElockHSCGHealTime;
static uint16 LAD_ElockHSCPConfirmTime;
static uint16 LAD_ElockHSCPHealTime;
static uint16 LAD_ElockLOpenCircuitConfirmTime;
static uint16 LAD_ElockLOpenCircuitHealTime;
static uint16 LAD_ElockLOvercurrentConfirmTime;
static uint16 LAD_ElockLSCGConfirmTime;
static uint16 LAD_ElockLSCGHealTime;
static uint16 LAD_ElockLSCPConfirmTime;
static uint16 LAD_ElockLSCPHealTime;
static uint16 LAD_ElockOvercurrentHealTime;

static uint16 LAD_ElockActuatorTime1;
static uint16 LAD_ElockActuatorTime2;

/** Counters*/


static IdtELockSetPoint LAD_ElockActuatorBypassSP;
static boolean LAD_ElockActuatorBypassStatus;

static uint16 LAD_ResultTestElockActuator;
static boolean LAD_ElockActuatorTestOngoing;


static VCU_EtatGmpHyb LAD_EtatGmpHyb;
static ABS_VehSpd LAD_VehSpd;
static IdtBatteryVolt LAD_BatteryVolt;


#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
/** Update calibratables*/
static void LAD_UpdateCalibratables(void);
/** Get feedback state*/
static IdtELockSetPoint LAD_feedBack_state(void);


static void LAD_diagnosisDebounce(boolean LAD_input, boolean *LAD_output,
				      uint32 LAD_confirmTime, uint32 LAD_healTime, uint32 *LAD_counter);

static void LAD_task_ElockHSCP(boolean bridgeH, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, IdtElockFeedbackLock ElockFeedbackLock, uint32 *Counter);
static void LAD_task_ElockLSCP(boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, IdtElockFeedbackUnlock ElockFeedbackUnlock, uint32 *Counter);
static void LAD_task_ElockHSCG(boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter);
static void LAD_task_ElockLSCG(boolean bridgeH, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter);
static void LAD_task_ElockHOpenCircuit(boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter);
static void LAD_task_ElockLOpenCircuit(boolean bridgeH, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter);
static void LAD_task_ElockOvercurrent(boolean bridgeH, boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter);
static void LAD_task_CheckCurrent(boolean bridgeH, boolean bridgeL);
static void LAD_task_ElockPrevioustoChange(boolean bridgeH, boolean bridgeL);
static void LAD_task_ElockActuatorTest(uint32 *Counter, IdtELockSetPoint *CurrentState,
		boolean *ElockHFaultSCP, boolean *ElockLFaultSCP, boolean *ElockHFaultSCG, boolean *ElockLFaultSCG,
		boolean *ElockHFaultOpenCircuit, boolean *ElockLFaultOpenCircuit, boolean *ElockOvercurrent);
static void LAD_checkFaultsInTest(boolean ElockHFaultSCP, boolean ElockLFaultSCP, boolean ElockHFaultSCG, boolean ElockLFaultSCG, boolean ElockHFaultOpenCircuit, boolean ElockLFaultOpenCircuit, boolean ElockOvercurrent, IdtAppRCDECUState ECUstate);
static Dcm_NegativeResponseCodeType LAD_ConditionsCheck(void);
static void LAD_setResultBit (uint16 bit);
static void LAD_UpdateConditions(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * IdtAfts_ElockActuatorTime1: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtAfts_ElockActuatorTime2: Integer in interval [0...20]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtBatteryVolt: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * IdtBatteryVoltMaxTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtBatteryVoltMinTest: Integer in interval [0...320]
 *   Unit: [mV], Factor: 100, Offset: 0
 * IdtElockActuatorTime: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtElockFeedbackCurrent: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackLock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockFeedbackUnlock: Integer in interval [0...65535]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtElockThreshold: Integer in interval [0...500]
 *   Unit: [mV], Factor: 10, Offset: 0
 * IdtVehStopMaxTest: Integer in interval [0...20]
 *   Unit: [km/h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtHWEditionDetected: Enumeration of integer in interval [0...255] with enumerators
 *   HW_EDITION_UNKNOWN (0U)
 *   HW_EDITION_D21 (1U)
 *   HW_EDITION_D31 (2U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint16 *Rte_Pim_PimElockActuatorCounterOverCurrent(void)
 *   uint16 *Rte_Pim_PimElockActuatorHCounterOpenCircuit(void)
 *   uint16 *Rte_Pim_PimElockActuatorHCounterSCG(void)
 *   uint16 *Rte_Pim_PimElockActuatorHCounterSCP(void)
 *   uint16 *Rte_Pim_PimElockActuatorLCounterOpenCircuit(void)
 *   uint16 *Rte_Pim_PimElockActuatorLCounterSCG(void)
 *   uint16 *Rte_Pim_PimElockActuatorLCounterSCP(void)
 *   IdtOutputELockSensor *Rte_Pim_PimElockSensor_PreviousToLock(void)
 *   IdtOutputELockSensor *Rte_Pim_PimElockSensor_PreviousToUnlock(void)
 *   IdtELockSetPoint *Rte_Pim_PimElockSetPoint(void)
 *   boolean *Rte_Pim_PimElockActuatorHPrefaultOpenCircuit(void)
 *   boolean *Rte_Pim_PimElockActuatorHPrefaultSCG(void)
 *   boolean *Rte_Pim_PimElockActuatorHPrefaultSCP(void)
 *   boolean *Rte_Pim_PimElockActuatorLPrefaultOpenCircuit(void)
 *   boolean *Rte_Pim_PimElockActuatorLPrefaultSCG(void)
 *   boolean *Rte_Pim_PimElockActuatorLPrefaultSCP(void)
 *   boolean *Rte_Pim_PimElockActuatorPrefaultOverCurrent(void)
 *   boolean *Rte_Pim_PimElockNotCurrentMeasured(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtElockThreshold Rte_CData_CalElockThresholdNoCurrent(void)
 *   IdtElockThreshold Rte_CData_CalElockThresholdOvercurrent(void)
 *   IdtElockThreshold Rte_CData_CalElockThresholdSCG(void)
 *   IdtElockThreshold Rte_CData_CalElockThresholdSCP(void)
 *   IdtAfts_ElockActuatorTime1 Rte_CData_CalAfts_ElockActuatorTime1(void)
 *   IdtAfts_ElockActuatorTime2 Rte_CData_CalAfts_ElockActuatorTime2(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHOpenCircuitConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHOpenCircuitHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCGConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCGHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCPConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockHSCPHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLOpenCircuitConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLOpenCircuitHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLOvercurrentConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCGConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCGHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCPConfirmTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockLSCPHealTime(void)
 *   IdtElockActuatorTime Rte_CData_CalOutputElockOvercurrentHealTime(void)
 *   boolean Rte_CData_CalEnableElockActuator(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtBatteryVoltMaxTest Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest(void)
 *   IdtBatteryVoltMinTest Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest(void)
 *   IdtVehStopMaxTest Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest(void)
 *
 *********************************************************************************************************************/


#define CtApLAD_START_SEC_CODE
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CtApLAD_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CtApLAD_init_doc
 *********************************************************************************************************************/

/*!
 * \brief LAD initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLAD_CODE) CtApLAD_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CtApLAD_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	LAD_ElockActuatorEnable=Rte_CData_CalEnableElockActuator();

	LAD_ElockOvercurrentThres = Rte_CData_CalElockThresholdOvercurrent();

	LAD_ElockThresholdSCG = Rte_CData_CalElockThresholdSCG();
	LAD_ElockThresholdSCP = Rte_CData_CalElockThresholdSCP();

	LAD_NoCurrentThres = Rte_CData_CalElockThresholdNoCurrent();

	LAD_ElockHOpenCircuitConfirmTime = Rte_CData_CalOutputElockHOpenCircuitConfirmTime();
	LAD_ElockHOpenCircuitHealTime = Rte_CData_CalOutputElockHOpenCircuitHealTime();
	LAD_ElockHSCGConfirmTime = Rte_CData_CalOutputElockHSCGConfirmTime();
	LAD_ElockHSCGHealTime = Rte_CData_CalOutputElockHSCGHealTime();
	LAD_ElockHSCPConfirmTime = Rte_CData_CalOutputElockHSCPConfirmTime();
	LAD_ElockHSCPHealTime = Rte_CData_CalOutputElockHSCPHealTime();
	LAD_ElockLOpenCircuitConfirmTime = Rte_CData_CalOutputElockLOpenCircuitConfirmTime();
	LAD_ElockLOpenCircuitHealTime = Rte_CData_CalOutputElockLOpenCircuitHealTime();
	LAD_ElockLOvercurrentConfirmTime = Rte_CData_CalOutputElockLOvercurrentConfirmTime();
	LAD_ElockLSCGConfirmTime = Rte_CData_CalOutputElockLSCGConfirmTime();
	LAD_ElockLSCGHealTime = Rte_CData_CalOutputElockLSCGHealTime();
	LAD_ElockLSCPConfirmTime = Rte_CData_CalOutputElockLSCPConfirmTime();
	LAD_ElockLSCPHealTime = Rte_CData_CalOutputElockLSCPHealTime();
	LAD_ElockOvercurrentHealTime = Rte_CData_CalOutputElockOvercurrentHealTime();

	LAD_ElockActuatorTime1 = Rte_CData_CalAfts_ElockActuatorTime1();
	LAD_ElockActuatorTime2 = Rte_CData_CalAfts_ElockActuatorTime2();

	LAD_BatteryVoltMaxTest = Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
	LAD_BatteryVoltMinTest = Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
	LAD_VehStopMaxTest = Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();

	LAD_ResultTestElockActuator = 0U;
	LAD_ElockActuatorTestOngoing = FALSE;

	LAD_UpdateConditions();
    
    
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLAD_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpBatteryVolt_DeBatteryVolt(IdtBatteryVolt *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(boolean *data)
 *   Std_ReturnType Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(boolean *data)
 *   Std_ReturnType Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(IdtElockFeedbackCurrent *data)
 *   Std_ReturnType Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(IdtElockFeedbackLock *data)
 *   Std_ReturnType Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(IdtElockFeedbackUnlock *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpHWEditionDetected_DeHWEditionDetected(IdtHWEditionDetected *data)
 *   Std_ReturnType Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(boolean data)
 *   Std_ReturnType Rte_Write_PpELockFaults_DeOutputElockOvercurrent(boolean data)
 *   Std_ReturnType Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLAD_task10ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief LAD task.
  *
  * This function is called periodically by the scheduler. This function updates
  * the bridge state to control the E-lock position.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLAD_CODE) RCtApLAD_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLAD_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static IdtELockSetPoint LAD_CurrentState;
	
	static uint32 LAD_HSCPCounter;
	static uint32 LAD_LSCPCounter;
	static uint32 LAD_HSCGCounter;
	static uint32 LAD_LSCGCounter;
	static uint32 LAD_HOpenCircuitCounter;
	static uint32 LAD_LOpenCircuitCounter;
	static uint32 LAD_OvercurrentCounter;
	static uint32 LAD_ActuatorTestCounter;

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 LAD_Counter = 0U;
	static uint8 LAD_Counter_test = 0U;
	static uint16 LAD_InitialDelay = 0U;

	static boolean LAD_ElockHFaultSCP = FALSE;
	static boolean LAD_ElockHFaultSCG = FALSE;
	static boolean LAD_ElockHFaultOpenCircuit = FALSE;
	static boolean LAD_ElockLFaultSCP = FALSE;
	static boolean LAD_ElockLFaultSCG = FALSE;
	static boolean LAD_ElockLFaultOpenCircuit = FALSE;
	static boolean LAD_ElockOvercurrent = FALSE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Inputs*/
	IdtAppRCDECUState LAD_ECUstate;
	IdtELockSetPoint LAD_SetPoint_Normal;
	IdtELockSetPoint LAD_SetPoint_Plant;
	boolean LAD_PlantModeActive;
	IdtBatteryVoltageState LAD_BatteryVoltageState;
	IdtElockFeedbackLock LAD_ElockFeedbackLock;
	IdtElockFeedbackUnlock LAD_ElockFeedbackUnlock;

	boolean LAD_ElockHFaultOpenCircuit_aux = FALSE;
	boolean LAD_ElockLFaultOpenCircuit_aux = FALSE;
	IdtOutputELockSensor FeedBack_endtest;
	IdtHWEditionDetected LAD_HWEdition;

	/*Outputs*/
	boolean LAD_bridgeH = FALSE;/* PRQA S 2981 #Variable initialized for better understanding and robustness*/
	boolean LAD_bridgeL = FALSE;/* PRQA S 2981 #Variable initialized for better understanding and robustness*/
	IdtELockSetPoint LAD_SetPoint;
	boolean LAD_EnableConditions = FALSE;/* PRQA S 2981 #Variable initialized for better understanding and robustness*/

	(void)Rte_Read_PpHWEditionDetected_DeHWEditionDetected(&LAD_HWEdition);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	LAD_UpdateCalibratables();

	if(LAD_INITIAL_DELAY>LAD_InitialDelay)
	{
		LAD_InitialDelay++;
		LAD_CurrentState = LAD_feedBack_state();
	}
	else if(FALSE != LAD_ElockActuatorEnable)
	{
		/*Module enable*/
		(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&LAD_ECUstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(&LAD_SetPoint_Normal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpElockSetpoint_PlantMode_DeElockSetpoint_PlantMode(&LAD_SetPoint_Plant);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpPlantModeState_DePlantModeState(&LAD_PlantModeActive);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&LAD_BatteryVoltageState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpElockFeedbackLock_DeElockFeedbackLock(&LAD_ElockFeedbackLock);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Read_PpElockFeedbackUnlock_DeElockFeedbackUnlock(&LAD_ElockFeedbackUnlock);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		LAD_ElockFeedbackLock = (LAD_ElockFeedbackLock * LAD_THRESHOLD_GAIN);
		LAD_ElockFeedbackUnlock = (LAD_ElockFeedbackUnlock * LAD_THRESHOLD_GAIN);

		if (HW_EDITION_D31 == LAD_HWEdition)
		{
			if (LAD_BatteryVoltageState == BAT_VALID_RANGE) /* The condition of LAD_ElockActuatorEnable == TRUE is implicit */
			{
				LAD_EnableConditions = TRUE;
			}
			(void)Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(LAD_EnableConditions);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		}
		/*Actuator Test*/
		LAD_task_ElockActuatorTest(&LAD_ActuatorTestCounter, &LAD_CurrentState, &LAD_ElockHFaultSCP, &LAD_ElockLFaultSCP,
				&LAD_ElockHFaultSCG, &LAD_ElockLFaultSCG, &LAD_ElockHFaultOpenCircuit, &LAD_ElockLFaultOpenCircuit, &LAD_ElockOvercurrent);

		/* Check if Actuator test is launched */
		if (FALSE != LAD_ElockActuatorBypassStatus)
		{
			/* Actuator Test Mode */
			LAD_SetPoint = LAD_ElockActuatorBypassSP;

			/*Able to update the state*/
			if((LAD_CurrentState != LAD_SetPoint)&&(0U==LAD_Counter_test))
			{
				/*Update counter.*/
				LAD_Counter_test = LAD_PULSE_WIDTH + LAD_DEAD_TIME;
				LAD_CurrentState = LAD_SetPoint;
			}

			if(0U==LAD_Counter_test)
			{
				LAD_bridgeH = FALSE;
				LAD_bridgeL = FALSE;
			}
			else if(LAD_DEAD_TIME>=LAD_Counter_test)
			{
				LAD_bridgeH = FALSE;
				LAD_bridgeL = FALSE;
				LAD_Counter_test --;
			}
			else if((uint8)ELOCK_SETPOINT_UNLOCKED==LAD_CurrentState)
			{
				LAD_bridgeH = FALSE;
				LAD_bridgeL = TRUE;
				LAD_Counter_test --;
			}
			else if((uint8)ELOCK_SETPOINT_LOCKED==LAD_CurrentState)
			{
				LAD_bridgeH = TRUE;
				LAD_bridgeL = FALSE;
				LAD_Counter_test --;
			}
			else
			{
				LAD_bridgeH = FALSE;
				LAD_bridgeL = FALSE;
				LAD_Counter_test --;
			}

			if (HW_EDITION_D31 == LAD_HWEdition)
			{
				LAD_task_ElockPrevioustoChange(LAD_bridgeH, LAD_bridgeL);
				LAD_task_CheckCurrent(LAD_bridgeH, LAD_bridgeL);

				/* Check Faults */
				LAD_task_ElockLSCP(LAD_bridgeL, &LAD_ElockLFaultSCP, LAD_BatteryVoltageState, LAD_ElockFeedbackUnlock, &LAD_LSCPCounter);
				LAD_task_ElockHSCP(LAD_bridgeH, &LAD_ElockHFaultSCP, LAD_BatteryVoltageState, LAD_ElockFeedbackLock, &LAD_HSCPCounter);

				LAD_task_ElockHSCG(LAD_bridgeL, &LAD_ElockHFaultSCG, LAD_BatteryVoltageState, &LAD_HSCGCounter);

				/* Comented to avoid malfunction due to the new diagnostic concept. Must be restored after rework */
				LAD_task_ElockHOpenCircuit(LAD_bridgeL, &LAD_ElockHFaultOpenCircuit, LAD_BatteryVoltageState, &LAD_HOpenCircuitCounter);

				LAD_task_ElockLSCG(LAD_bridgeH, &LAD_ElockLFaultSCG, LAD_BatteryVoltageState, &LAD_LSCGCounter);

				/* Comented to avoid malfunction due to the new diagnostic concept. Must be restored after rework */
				LAD_task_ElockLOpenCircuit(LAD_bridgeH, &LAD_ElockLFaultOpenCircuit, LAD_BatteryVoltageState, &LAD_LOpenCircuitCounter);

				LAD_task_ElockOvercurrent(LAD_bridgeH, LAD_bridgeL, &LAD_ElockOvercurrent, LAD_BatteryVoltageState, &LAD_OvercurrentCounter);

				if ((FALSE == LAD_ElockActuatorTestOngoing) && (FALSE == LAD_ElockHFaultSCG) && (FALSE == LAD_ElockLFaultSCG))
				{
					LAD_ElockHFaultOpenCircuit_aux = LAD_ElockHFaultOpenCircuit;
					LAD_ElockLFaultOpenCircuit_aux = LAD_ElockLFaultOpenCircuit;
				}
				else
				{
					LAD_ElockHFaultOpenCircuit_aux = FALSE;
					LAD_ElockLFaultOpenCircuit_aux = FALSE;
				}
			}

			/* Set results */
			LAD_checkFaultsInTest(LAD_ElockHFaultSCP, LAD_ElockLFaultSCP, LAD_ElockHFaultSCG, LAD_ElockLFaultSCG, LAD_ElockHFaultOpenCircuit_aux,
					LAD_ElockLFaultOpenCircuit_aux, LAD_ElockOvercurrent, LAD_ECUstate);

			/* Update values */
			(void)Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(LAD_ElockHFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			(void)Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(LAD_ElockLFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			(void)Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(LAD_ElockHFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			(void)Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(LAD_ElockLFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

			if ((FALSE == LAD_ElockActuatorTestOngoing) && (FALSE == LAD_ElockHFaultSCG) && (FALSE == LAD_ElockLFaultSCG))
			{
				(void)Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(LAD_ElockHFaultOpenCircuit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(LAD_ElockLFaultOpenCircuit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			}

			(void)Rte_Write_PpELockFaults_DeOutputElockOvercurrent(LAD_ElockOvercurrent);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

			if (FALSE != LAD_ElockActuatorTestOngoing) /* Do not actuate if a fault is present on the system */
			{
				(void)Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(LAD_bridgeH);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(LAD_bridgeL);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			}

			(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack_endtest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

			if ((FALSE == LAD_ElockActuatorTestOngoing) && (ELOCK_SETPOINT_UNLOCKED==LAD_CurrentState) && (ELOCK_LOCKED==FeedBack_endtest)
					&& (FALSE == LAD_ElockHFaultSCP) && (FALSE == LAD_ElockLFaultSCP) && (FALSE == LAD_ElockHFaultSCG)  && (FALSE == LAD_ElockLFaultSCG)
					 && (FALSE == LAD_ElockOvercurrent) && (FALSE == LAD_ElockHFaultOpenCircuit) && (FALSE == LAD_ElockLFaultOpenCircuit))
			{
				LAD_Counter = LAD_PULSE_WIDTH + LAD_DEAD_TIME;
			}

		}
		else
		{
			/* Normal mode and Plant mode*/
			if(FALSE != LAD_PlantModeActive)
			{
				LAD_SetPoint = LAD_SetPoint_Plant;
			}
			else
			{
				LAD_SetPoint = LAD_SetPoint_Normal;
			}

			if(		(APP_STATE_1==LAD_ECUstate)
								||(APP_STATE_3==LAD_ECUstate)
								||(APP_STATE_4==LAD_ECUstate)
								||(APP_STATE_5==LAD_ECUstate))
			{
				/*Able to update the state*/
				if((LAD_CurrentState != LAD_SetPoint)&&(0U==LAD_Counter))
				{
					/*Update counter.*/
					LAD_Counter = LAD_PULSE_WIDTH + LAD_DEAD_TIME;
					LAD_CurrentState = LAD_SetPoint;
				}


				if(0U==LAD_Counter)
				{
					LAD_bridgeH = FALSE;
					LAD_bridgeL = FALSE;
				}
				else if(LAD_DEAD_TIME>=LAD_Counter)
				{
					LAD_bridgeH = FALSE;
					LAD_bridgeL = FALSE;
					LAD_Counter --;
				}
				else if((uint8)ELOCK_SETPOINT_UNLOCKED==LAD_CurrentState)
				{
					LAD_bridgeH = FALSE;
					LAD_bridgeL = TRUE;
					LAD_Counter --;
				}
				else if((uint8)ELOCK_SETPOINT_LOCKED==LAD_CurrentState)
				{
					LAD_bridgeH = TRUE;
					LAD_bridgeL = FALSE;
					LAD_Counter --;
				}
				else
				{
					LAD_bridgeH = FALSE;
					LAD_bridgeL = FALSE;
					LAD_Counter --;
				}

				if (HW_EDITION_D31 == LAD_HWEdition)
				{
					LAD_task_ElockPrevioustoChange(LAD_bridgeH, LAD_bridgeL);
					LAD_task_CheckCurrent(LAD_bridgeH, LAD_bridgeL);

					/* Check Faults */
					LAD_task_ElockLSCP(LAD_bridgeL, &LAD_ElockLFaultSCP, LAD_BatteryVoltageState, LAD_ElockFeedbackUnlock, &LAD_LSCPCounter);
					LAD_task_ElockHSCP(LAD_bridgeH, &LAD_ElockHFaultSCP, LAD_BatteryVoltageState, LAD_ElockFeedbackLock, &LAD_HSCPCounter);

					LAD_task_ElockHSCG(LAD_bridgeL, &LAD_ElockHFaultSCG, LAD_BatteryVoltageState, &LAD_HSCGCounter);

					/* Comented to avoid malfunction due to the new diagnostic concept. Must be restored after rework */
					LAD_task_ElockHOpenCircuit(LAD_bridgeL, &LAD_ElockHFaultOpenCircuit, LAD_BatteryVoltageState, &LAD_HOpenCircuitCounter);

					LAD_task_ElockLSCG(LAD_bridgeH, &LAD_ElockLFaultSCG, LAD_BatteryVoltageState, &LAD_LSCGCounter);

					/* Comented to avoid malfunction due to the new diagnostic concept. Must be restored after rework */
					LAD_task_ElockLOpenCircuit(LAD_bridgeH, &LAD_ElockLFaultOpenCircuit, LAD_BatteryVoltageState, &LAD_LOpenCircuitCounter);

					LAD_task_ElockOvercurrent(LAD_bridgeH, LAD_bridgeL, &LAD_ElockOvercurrent, LAD_BatteryVoltageState, &LAD_OvercurrentCounter);
				}

				/* Update values */
				/*Rule 9.3.1: The value of this 'if' controlling expression is always 'true'.*/
				/*Rule 9.3.1: The result of this logical operation is always 'true'.*/
				/*PRQA S 2991,2995 ++ #DFA write is protected against no standard boolean definitions*/
				(void)Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(LAD_bridgeH);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(LAD_bridgeL);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				/*PRQA S 2991,2995 --*/

				(void)Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(LAD_ElockHFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(LAD_ElockLFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(LAD_ElockHFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(LAD_ElockLFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(LAD_ElockHFaultOpenCircuit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(LAD_ElockLFaultOpenCircuit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
				(void)Rte_Write_PpELockFaults_DeOutputElockOvercurrent(LAD_ElockOvercurrent);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
			}
		}
	}
	/* Removed redundant condition due to UT. */
	/*else if(FALSE == LAD_ElockActuatorEnable)*/
	else
	{
		LAD_bridgeH = FALSE;
		LAD_bridgeL = FALSE;

		LAD_ElockHFaultSCP = FALSE;
		LAD_ElockLFaultSCP = FALSE;
		LAD_ElockHFaultSCG = FALSE;
		LAD_ElockLFaultSCG = FALSE;
		LAD_ElockHFaultOpenCircuit = FALSE;
		LAD_ElockLFaultOpenCircuit = FALSE;
		LAD_ElockOvercurrent = FALSE;
		LAD_EnableConditions = FALSE;

		(void)Rte_Write_PpElockActuatorMonitoringConditions_DeElockActuatorMonitoringConditions(LAD_EnableConditions);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		/* Update values */
		(void)Rte_Write_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(LAD_bridgeH);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(LAD_bridgeL);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		(void)Rte_Write_PpELockFaults_DeOutputElockHFaultSCP(LAD_ElockHFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpELockFaults_DeOutputElockLFaultSCP(LAD_ElockLFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpELockFaults_DeOutputElockHFaultSCG(LAD_ElockHFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpELockFaults_DeOutputElockLFaultSCG(LAD_ElockLFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpELockFaults_DeOutputElockHFaultOpenCircuit(LAD_ElockHFaultOpenCircuit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpELockFaults_DeOutputElockLFaultOpenCircuit(LAD_ElockLFaultOpenCircuit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Write_PpELockFaults_DeOutputElockOvercurrent(LAD_ElockOvercurrent);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Actuator_test_on_recharging_plug_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_RequestResults (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret ;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if(0U != LAD_ResultTestElockActuator)
		{
			Out_ComplexStructure_0[0] = (uint8)LAD_RESP_END_NOK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1] = (uint8)(LAD_ResultTestElockActuator >> 8);
			Out_ComplexStructure_0[2] = (uint8)(LAD_ResultTestElockActuator & 0xFFU);
		}
		else
		{
			Out_ComplexStructure_0[0] = (uint8)LAD_RESP_END_OK;
			*DataLength = 3U;
			Out_ComplexStructure_0[1] = (uint8)(LAD_ResultTestElockActuator >> 8);
			Out_ComplexStructure_0[2] = (uint8)(LAD_ResultTestElockActuator & 0xFFU);
		}
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Actuator_test_on_recharging_plug_lock_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Actuator_test_on_recharging_plug_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, uint8 *Out_ComplexStructure_0, uint16 *DataLength, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_ComplexStructure_0: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_ComplexStructure_0, P2VAR(uint16, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) DataLength, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Start (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	Dcm_NegativeResponseCodeType Error;

	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	/* Reset faults to clear previous ones */
	LAD_ResultTestElockActuator = 0U;

	Error = LAD_ConditionsCheck();

	if((NULL_PTR!=Out_ComplexStructure_0)&&(NULL_PTR!=DataLength)&&(NULL_PTR!=ErrorCode))
	{
		if (0U != LAD_ResultTestElockActuator)
		{
			LAD_ElockActuatorTestOngoing = FALSE;

			Out_ComplexStructure_0[0] = (uint8)LAD_RESP_END_NOK;
			*DataLength = 1U;
			*ErrorCode = Error;
			ret = RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;
		}
		else
		{
			LAD_ResultTestElockActuator = 0U;
			LAD_ElockActuatorTestOngoing = TRUE;

			Out_ComplexStructure_0[0]=(uint8)LAD_RESP_ONGOING;
			*DataLength = 1U;
			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;
	}


  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Actuator_test_on_recharging_plug_lock_Stop
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_Actuator_test_on_recharging_plug_lock>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, uint8 *Out_RSR_04, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_DCM_E_PENDING
 *   RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApLAD_CODE) RoutineServices_Actuator_test_on_recharging_plug_lock_Stop(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) Out_RSR_04, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPLAD_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Actuator_test_on_recharging_plug_lock_Stop (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus; /* Remove compilator warning "Parameter not used " */

	if((NULL_PTR!=Out_RSR_04)&&(NULL_PTR!=ErrorCode))
	{
		*Out_RSR_04 = (uint8)LAD_RESP_STOP;
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Actuator_test_on_recharging_plug_lock_E_NOT_OK;
	}

	LAD_setResultBit(7U);

  return ret;
  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLAD_STOP_SEC_CODE
#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


static void LAD_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	LAD_ElockActuatorEnable=Rte_CData_CalEnableElockActuator();
    
	LAD_ElockOvercurrentThres = Rte_CData_CalElockThresholdOvercurrent();

	LAD_ElockThresholdSCG = Rte_CData_CalElockThresholdSCG();
	LAD_ElockThresholdSCP = Rte_CData_CalElockThresholdSCP();

	LAD_ElockHOpenCircuitConfirmTime = Rte_CData_CalOutputElockHOpenCircuitConfirmTime();
	LAD_ElockHOpenCircuitHealTime = Rte_CData_CalOutputElockHOpenCircuitHealTime();
	LAD_ElockHSCGConfirmTime = Rte_CData_CalOutputElockHSCGConfirmTime();
	LAD_ElockHSCGHealTime = Rte_CData_CalOutputElockHSCGHealTime();
	LAD_ElockHSCPConfirmTime = Rte_CData_CalOutputElockHSCPConfirmTime();
	LAD_ElockHSCPHealTime = Rte_CData_CalOutputElockHSCPHealTime();
	LAD_ElockLOpenCircuitConfirmTime = Rte_CData_CalOutputElockLOpenCircuitConfirmTime();
	LAD_ElockLOpenCircuitHealTime = Rte_CData_CalOutputElockLOpenCircuitHealTime();
	LAD_ElockLOvercurrentConfirmTime = Rte_CData_CalOutputElockLOvercurrentConfirmTime();
	LAD_ElockLSCGConfirmTime = Rte_CData_CalOutputElockLSCGConfirmTime();
	LAD_ElockLSCGHealTime = Rte_CData_CalOutputElockLSCGHealTime();
	LAD_ElockLSCPConfirmTime = Rte_CData_CalOutputElockLSCPConfirmTime();
	LAD_ElockLSCPHealTime = Rte_CData_CalOutputElockLSCPHealTime();
	LAD_ElockOvercurrentHealTime = Rte_CData_CalOutputElockOvercurrentHealTime();

	LAD_ElockActuatorTime1 = Rte_CData_CalAfts_ElockActuatorTime1();
	LAD_ElockActuatorTime2 = Rte_CData_CalAfts_ElockActuatorTime2();

	LAD_BatteryVoltMaxTest = Rte_Prm_PpBatteryVoltMaxTest_DeBatteryVoltMaxTest();
	LAD_BatteryVoltMinTest = Rte_Prm_PpBatteryVoltMinTest_DeBatteryVoltMinTest();
	LAD_VehStopMaxTest = Rte_Prm_PpVehStopMaxTest_DeVehStopMaxTest();

	LAD_UpdateConditions();
    
}

static IdtELockSetPoint LAD_feedBack_state(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtOutputELockSensor FeedBack;
	IdtELockSetPoint RetVal;


	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(ELOCK_LOCKED==FeedBack)
	{
		RetVal = ELOCK_SETPOINT_LOCKED;
	}
	else if(ELOCK_UNLOCKED==FeedBack)
	{
		RetVal = ELOCK_SETPOINT_UNLOCKED;
	}
	else
	{
		RetVal = LAD_UNDEFINE_STATE;
	}
	return RetVal;
}

static void LAD_diagnosisDebounce(boolean LAD_input, boolean *LAD_output,
				      uint32 LAD_confirmTime, uint32 LAD_healTime, uint32 *LAD_counter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
    uint32 LAD_counterTime_max;
    
	if ((NULL_PTR != LAD_counter) && (NULL_PTR != LAD_output))
	{
      if (TRUE == LAD_input)
      {
		  LAD_counterTime_max = LAD_confirmTime;
      }
      else
      {
		  LAD_counterTime_max = LAD_healTime;
      }

      if (LAD_input == *LAD_output)
      {
		  /*Edge detected, clean counter*/
		  *LAD_counter = 0U;
      }
      else if (*LAD_counter < LAD_counterTime_max)
      {
		  /*update counter*/
		  (*LAD_counter)++;
      }
      else
      {
		  /*Counter time reached, update output*/
		  *LAD_output = LAD_input;
		  *LAD_counter = 0U;
      }
   }
}

static void LAD_task_ElockHSCP(boolean bridgeH, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, IdtElockFeedbackLock ElockFeedbackLock, uint32 *Counter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 SCP_Threshold;
	uint32 confirm_time;
	uint32 heal_time;

	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		SCP_Threshold = ((uint32)LAD_ElockThresholdSCP * (uint32)LAD_THRESHOLD_GAIN);
		confirm_time = ((uint32)LAD_ElockHSCPConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockHSCPHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/
		if (FALSE == bridgeH)
		{
			if (SCP_Threshold < (uint32)ElockFeedbackLock )
			{
				*Rte_Pim_PimElockActuatorHPrefaultSCP() = TRUE;
			}
			else
			{
				*Rte_Pim_PimElockActuatorHPrefaultSCP() = FALSE;
			}
		}

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorHPrefaultSCP(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_ElockLSCP(boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, IdtElockFeedbackUnlock ElockFeedbackUnlock, uint32 *Counter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 SCP_Threshold;
	uint32 confirm_time;
	uint32 heal_time;

	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		SCP_Threshold = ((uint32)LAD_ElockThresholdSCP * (uint32)LAD_THRESHOLD_GAIN);
		confirm_time = ((uint32)LAD_ElockLSCPConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockLSCPHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/
		if (FALSE == bridgeL)
		{
			if (SCP_Threshold < (uint32)ElockFeedbackUnlock )
			{
				*Rte_Pim_PimElockActuatorLPrefaultSCP() = TRUE;
			}
			else
			{
				*Rte_Pim_PimElockActuatorLPrefaultSCP() = FALSE;
			}
		}

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorLPrefaultSCP(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_ElockHSCG(boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean LAD_HSCG_bridgeL_prev = FALSE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 confirm_time;
	uint32 heal_time;
	IdtOutputELockSensor FeedBack;


	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		confirm_time = ((uint32)LAD_ElockHSCGConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockHSCGHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/
		if ((LAD_HSCG_bridgeL_prev != bridgeL) && (FALSE == bridgeL))
		{
			if (FALSE != (*Rte_Pim_PimElockNotCurrentMeasured()))
			{
				(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

				if ((ELOCK_LOCKED == (*Rte_Pim_PimElockSensor_PreviousToUnlock())) && (ELOCK_UNLOCKED==FeedBack))
				{
					*Rte_Pim_PimElockActuatorHPrefaultSCG() = TRUE;
				}
				else
				{
					if ((ELOCK_LOCKED == (*Rte_Pim_PimElockSensor_PreviousToUnlock())) && (ELOCK_LOCKED==FeedBack))
					{
						*Rte_Pim_PimElockActuatorHPrefaultSCG() = FALSE;
					}
				}
			}
			else
			{
				*Rte_Pim_PimElockActuatorHPrefaultSCG() = FALSE;
			}
		}

		LAD_HSCG_bridgeL_prev = bridgeL;

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorHPrefaultSCG(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_ElockLSCG(boolean bridgeH, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean LAD_LSCG_bridgeH_prev = FALSE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 confirm_time;
	uint32 heal_time;
	IdtOutputELockSensor FeedBack;

	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		confirm_time = ((uint32)LAD_ElockLSCGConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockLSCGHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/
		if ((LAD_LSCG_bridgeH_prev != bridgeH) && (FALSE == bridgeH))
		{
			if (FALSE != (*Rte_Pim_PimElockNotCurrentMeasured()))
			{
				(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

				if ((ELOCK_UNLOCKED == (*Rte_Pim_PimElockSensor_PreviousToLock())) && (ELOCK_LOCKED==FeedBack))
				{
					*Rte_Pim_PimElockActuatorLPrefaultSCG() = TRUE;
				}
				else
				{
					if ((ELOCK_UNLOCKED == (*Rte_Pim_PimElockSensor_PreviousToLock())) && (ELOCK_UNLOCKED==FeedBack))
					{
						*Rte_Pim_PimElockActuatorLPrefaultSCG() = FALSE;
					}
				}
			}
			else
			{
				*Rte_Pim_PimElockActuatorLPrefaultSCG() = FALSE;
			}
		}

		LAD_LSCG_bridgeH_prev = bridgeH;

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorLPrefaultSCG(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_ElockHOpenCircuit(boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean LAD_HOpenC_bridgeL_prev = FALSE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 confirm_time;
	uint32 heal_time;
	IdtOutputELockSensor FeedBack;

	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		confirm_time = ((uint32)LAD_ElockHOpenCircuitConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockHOpenCircuitHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/

		if ((LAD_HOpenC_bridgeL_prev != bridgeL) && (FALSE == bridgeL))
		{
			if (FALSE != (*Rte_Pim_PimElockNotCurrentMeasured()))
			{
				if ((ELOCK_LOCKED == (*Rte_Pim_PimElockSensor_PreviousToUnlock())) && (ELOCK_LOCKED==FeedBack))
				{
					*Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = TRUE;
				}
				else
				{
					if ((ELOCK_LOCKED == (*Rte_Pim_PimElockSensor_PreviousToUnlock())) && (ELOCK_UNLOCKED==FeedBack))
					{
						*Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = FALSE;
					}
				}
			}
			else
			{
				*Rte_Pim_PimElockActuatorHPrefaultOpenCircuit() = FALSE;
			}
		}

		LAD_HOpenC_bridgeL_prev = bridgeL;

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorHPrefaultOpenCircuit(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_ElockLOpenCircuit(boolean bridgeH, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean LAD_LOpenC_bridgeH_prev = FALSE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 confirm_time;
	uint32 heal_time;
	IdtOutputELockSensor FeedBack;

	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		confirm_time = ((uint32)LAD_ElockLOpenCircuitConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockLOpenCircuitHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/

		if ((LAD_LOpenC_bridgeH_prev != bridgeH) && (FALSE == bridgeH))
		{
			if (FALSE != (*Rte_Pim_PimElockNotCurrentMeasured()))
			{
				if ((ELOCK_UNLOCKED == (*Rte_Pim_PimElockSensor_PreviousToLock())) && (ELOCK_UNLOCKED==FeedBack))
				{
					*Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = TRUE;
				}
				else
				{
					if ((ELOCK_UNLOCKED == (*Rte_Pim_PimElockSensor_PreviousToLock())) && (ELOCK_LOCKED==FeedBack))
					{
						*Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = FALSE;
					}
				}
			}
			else
			{
				*Rte_Pim_PimElockActuatorLPrefaultOpenCircuit() = FALSE;
			}
		}

		LAD_LOpenC_bridgeH_prev = bridgeH;

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorLPrefaultOpenCircuit(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_ElockOvercurrent(boolean bridgeH, boolean bridgeL, boolean *Fault, IdtBatteryVoltageState BatteryVoltageState, uint32 *Counter)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */
	static uint32 Overcurrent_Threshold;
	static boolean LAD_ElockActuatorHPrefaultOverCurrent;
	static boolean LAD_ElockActuatorLPrefaultOverCurrent;

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 confirm_time;
	uint32 heal_time;
	IdtElockFeedbackCurrent CurrentFeedback;

	(void)Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(&CurrentFeedback);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	CurrentFeedback = (CurrentFeedback *LAD_THRESHOLD_GAIN);

	if ((NULL_PTR != Fault) && (NULL_PTR != Counter))
	{
		Overcurrent_Threshold = ((uint32)LAD_ElockOvercurrentThres * (uint32)LAD_THRESHOLD_GAIN);
		confirm_time = ((uint32)LAD_ElockLOvercurrentConfirmTime * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
		heal_time = ((uint32)LAD_ElockOvercurrentHealTime * (uint32)LAD_ACTUATOR_FACTOR) / (uint32) LAD_TASK_PERIOD;

		/*Check Prefaults*/
		if (FALSE!= bridgeH)
		{
			if ((Overcurrent_Threshold < (uint32)CurrentFeedback))
			{
				LAD_ElockActuatorHPrefaultOverCurrent = TRUE;
			}
			else
			{
				LAD_ElockActuatorHPrefaultOverCurrent = FALSE;
			}
		}

		if (FALSE!= bridgeL)
		{
			if ((Overcurrent_Threshold < (uint32)CurrentFeedback))
			{
				LAD_ElockActuatorLPrefaultOverCurrent = TRUE;
			}
			else
			{
				LAD_ElockActuatorLPrefaultOverCurrent = FALSE;
			}
		}

		if ((FALSE != LAD_ElockActuatorHPrefaultOverCurrent) || (FALSE != LAD_ElockActuatorLPrefaultOverCurrent))
		{
			*Rte_Pim_PimElockActuatorPrefaultOverCurrent() = TRUE;
		}
		else
		{
			*Rte_Pim_PimElockActuatorPrefaultOverCurrent() = FALSE;
		}

		/* Check Faults */
		if ((BAT_UNDERVOLTAGE!= BatteryVoltageState) && (BAT_OVERVOLTAGE!= BatteryVoltageState))
		{
			LAD_diagnosisDebounce(*Rte_Pim_PimElockActuatorPrefaultOverCurrent(), Fault, confirm_time, heal_time, Counter);
		}
		else
		{
			*Fault = FALSE;
		}
	}
}

static void LAD_task_CheckCurrent(boolean bridgeH, boolean bridgeL)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static uint16 LAD_CurrMeas_Delay = 0U;
	static boolean LAD_IdleState = TRUE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	IdtElockFeedbackCurrent LAD_CurrentFeedback;

	(void)Rte_Read_PpElockFeedbackCurrent_DeElockFeedbackCurrent(&LAD_CurrentFeedback);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((FALSE != bridgeH) || (FALSE != bridgeL))
	{

		/* By default ElockNotCurrentMeasured is set, only when current is measured it is cleared while the action is performed*/
		if (FALSE != LAD_IdleState)
		{
			LAD_IdleState = FALSE;
			(*Rte_Pim_PimElockNotCurrentMeasured()) = TRUE;

		}

		if (LAD_NoCurrentThres < LAD_CurrentFeedback)
		{
			if ((LAD_CURRENT_MEAS_DELAY > LAD_CurrMeas_Delay) && (FALSE != (*Rte_Pim_PimElockNotCurrentMeasured())))
			{
				LAD_CurrMeas_Delay++;
			}
			else
			{
				(*Rte_Pim_PimElockNotCurrentMeasured()) = FALSE;
				LAD_CurrMeas_Delay = 0U;
			}
		}


	}
	else
	{
		LAD_IdleState = TRUE;
		LAD_CurrMeas_Delay = 0U;
	}

}

static void LAD_task_ElockPrevioustoChange(boolean bridgeH, boolean bridgeL)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean LAD_bridgeH_prev = FALSE;
	static boolean LAD_bridgeL_prev = FALSE;

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtOutputELockSensor FeedBack;

	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if ((LAD_bridgeH_prev != bridgeH) && (FALSE != bridgeH))
	{
		(*Rte_Pim_PimElockSensor_PreviousToLock()) = FeedBack;
	}

	if ((LAD_bridgeL_prev != bridgeL) && (FALSE != bridgeL))
	{
		(*Rte_Pim_PimElockSensor_PreviousToUnlock()) = FeedBack;
	}

	LAD_bridgeH_prev = bridgeH;
	LAD_bridgeL_prev = bridgeL;
}
/* PRQA S 3206 ++ #Variable not used but remain for function versatility*/
static void LAD_task_ElockActuatorTest(uint32 *Counter, IdtELockSetPoint *CurrentState,
		boolean *ElockHFaultSCP, boolean *ElockLFaultSCP, boolean *ElockHFaultSCG, boolean *ElockLFaultSCG,
		boolean *ElockHFaultOpenCircuit, boolean *ElockLFaultOpenCircuit, boolean *ElockOvercurrent)
{
/* PRQA S 3206 -- #Variable not used but remain for function versatility*/
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */
	static boolean LAD_ElockActuatorTestOngoing_prev;
	static boolean LAD_ActuatorTime1_Flag;
	static boolean LAD_ActuatorTime2_Flag;

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 time_data;
	IdtOutputELockSensor FeedBack;
	IdtELockSetPoint FeedBack_setpoint;

	if (FALSE != LAD_ElockActuatorTestOngoing)
	{
		(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&FeedBack);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		if(ELOCK_LOCKED==FeedBack)
		{
			FeedBack_setpoint = ELOCK_SETPOINT_LOCKED;
		}
		else if(ELOCK_UNLOCKED==FeedBack)
		{
			FeedBack_setpoint = ELOCK_SETPOINT_UNLOCKED;
		}
		else
		{
			FeedBack_setpoint = LAD_UNDEFINE_STATE;
		}

		/* Removed redundant condition due to UT */
		/*if ((FALSE == LAD_ElockActuatorTestOngoing_prev) && (FALSE != LAD_ElockActuatorTestOngoing))*/
		if (FALSE == LAD_ElockActuatorTestOngoing_prev)
		{
			/* Start test */
			LAD_ElockActuatorBypassStatus = TRUE;
			/* Clear flags */
			LAD_ActuatorTime1_Flag = FALSE;
			LAD_ActuatorTime2_Flag = FALSE;
			/* Clear Faults */
			*ElockHFaultSCG = FALSE;
			*ElockLFaultSCG = FALSE;
			*ElockHFaultOpenCircuit = FALSE;
			*ElockLFaultOpenCircuit = FALSE;
			*ElockOvercurrent = FALSE;
			/* SCP not resetted for HW protection */
			/* *ElockHFaultSCP = FALSE;*/
			/* *ElockLFaultSCP = FALSE;*/

			if (ELOCK_SETPOINT_UNLOCKED == FeedBack_setpoint)
			{
				/* Launch test for first time */
				time_data = ((uint32)LAD_ElockActuatorTime2 * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
				LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_LOCKED;
				*Counter = time_data;
				*CurrentState = ELOCK_SETPOINT_UNLOCKED;
			}
			else if (ELOCK_SETPOINT_LOCKED == FeedBack_setpoint)
			{
				/* Launch test for first time */
				time_data = ((uint32)LAD_ElockActuatorTime1 * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
				LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_UNLOCKED;
				*Counter = time_data;
				*CurrentState = ELOCK_SETPOINT_LOCKED;
			}
			else
			{
				LAD_ElockActuatorBypassSP = LAD_UNDEFINE_STATE;
				*Counter = 0;
				*CurrentState = LAD_ElockActuatorBypassSP;
			}
		}
		else
		{

			if (0U == (*Counter))
			{
				if (ELOCK_SETPOINT_UNLOCKED == FeedBack_setpoint)
				{
					if ( ELOCK_SETPOINT_LOCKED == LAD_ElockActuatorBypassSP)
					{
						/* Not locked */
						LAD_setResultBit(14U);
					}
					else
					{
						/* Check if other test is done */
						LAD_ActuatorTime1_Flag = TRUE;
						/* Removed redundant condition due to UT */
						/*if ((FALSE != LAD_ActuatorTime1_Flag) && (FALSE != LAD_ActuatorTime2_Flag))*/
						if (FALSE != LAD_ActuatorTime2_Flag)
						{
							LAD_ElockActuatorTestOngoing = FALSE;
							LAD_ActuatorTime1_Flag = FALSE;
							LAD_ActuatorTime2_Flag = FALSE;
							LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_UNLOCKED;
							*CurrentState = LAD_ElockActuatorBypassSP;
						}
						else
						{
							/* Launch following test */
							time_data = ((uint32)LAD_ElockActuatorTime2 * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
							LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_LOCKED;
							*Counter = time_data;
						}
					}
				}
				else if (ELOCK_SETPOINT_LOCKED == FeedBack_setpoint)
				{
					if ( ELOCK_SETPOINT_UNLOCKED == LAD_ElockActuatorBypassSP)
					{
						/* Not unlocked */
						LAD_setResultBit(15U);
					}
					else
					{
						LAD_ActuatorTime2_Flag = TRUE;
						/* Removed redundant condition due to UT */
						/*if ((FALSE != LAD_ActuatorTime1_Flag) && (FALSE != LAD_ActuatorTime2_Flag))*/
						if (FALSE != LAD_ActuatorTime1_Flag)
						{
							/* Check if other test is done */
							LAD_ElockActuatorTestOngoing = FALSE;
							LAD_ActuatorTime1_Flag = FALSE;
							LAD_ActuatorTime2_Flag = FALSE;
							LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_UNLOCKED;
							*CurrentState = LAD_ElockActuatorBypassSP;
						}
						else
						{
							/* Launch following test */
							time_data = ((uint32)LAD_ElockActuatorTime1 * (uint32)LAD_ACTUATOR_FACTOR)/ (uint32) LAD_TASK_PERIOD;
							LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_UNLOCKED;
							*Counter = time_data;
						}
					}
				}
				/* Removed redundant if condition due to UT */
				/*else if (LAD_UNDEFINE_STATE == FeedBack_setpoint)*/
				else
				{
					LAD_setResultBit(13U);
				}
			}
			else
			{
				/* Wait the time specified */
				(*Counter)--;
			}
		}

	}
	else
	{
		/* Test Aborted, set default values */
		LAD_ElockActuatorBypassSP = ELOCK_SETPOINT_UNLOCKED;
		LAD_ElockActuatorBypassStatus = FALSE;
	}

	/* Udpate value to detect FALSE to TRUE switch */
	LAD_ElockActuatorTestOngoing_prev = LAD_ElockActuatorTestOngoing;
}

static void LAD_checkFaultsInTest(boolean ElockHFaultSCP, boolean ElockLFaultSCP, boolean ElockHFaultSCG, boolean ElockLFaultSCG,
		boolean ElockHFaultOpenCircuit, boolean ElockLFaultOpenCircuit, boolean ElockOvercurrent, IdtAppRCDECUState ECUstate)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean ElockSensorFaultSCP;
	boolean ElockSensorFaultSCG;

	(void)Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCP(&ElockSensorFaultSCP);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpELockSensorFaults_DeELockSensorFaultSCG(&ElockSensorFaultSCG);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


		LAD_ConditionsCheck();/* PRQA S 3200 #The error code returned is not needed here*/

		if ((APP_STATE_4 != ECUstate) && (APP_STATE_5 != ECUstate))
		{
			LAD_setResultBit(2U);
		}
		if ((FALSE != ElockHFaultSCP) || (FALSE != ElockLFaultSCP))
		{
			LAD_setResultBit(4U);
		}
		if ((FALSE != ElockHFaultSCG) || (FALSE != ElockLFaultSCG))
		{
			LAD_setResultBit(5U);
		}
		if ((FALSE != ElockHFaultOpenCircuit) || (FALSE != ElockLFaultOpenCircuit))
		{
			LAD_setResultBit(6U);
		}
		if (FALSE != ElockOvercurrent)
		{
			LAD_setResultBit(9U);
		}
		if (FALSE != ElockSensorFaultSCP)
		{
			LAD_setResultBit(10U);
		}
		if (FALSE != ElockSensorFaultSCG)
		{
			LAD_setResultBit(11U);
		}

}

static Dcm_NegativeResponseCodeType LAD_ConditionsCheck(void)
{

	Dcm_NegativeResponseCodeType Errortype = DCM_E_CONDITIONSNOTCORRECT;

	/* The priority is set as: the lowest code will be the one with the highest priority,
	 * so for that reason is the last to be evaluated
	 */

	if (LAD_BatteryVoltMinTest > LAD_BatteryVolt)
	{
		LAD_setResultBit(3U);
		Errortype = DCM_E_VOLTAGETOOLOW;
	}
	if (LAD_BatteryVoltMaxTest < LAD_BatteryVolt)
	{
		LAD_setResultBit(3U);
		Errortype = DCM_E_VOLTAGETOOHIGH;
	}
	if (LAD_VehSpd > ((uint32)LAD_VehStopMaxTest * (uint32)LAD_VEHSPD_FACTOR))
	{
		LAD_setResultBit(1U);
		Errortype = DCM_E_VEHICLESPEEDTOOHIGH;
	}
	if (Cx0_PWT_inactive != LAD_EtatGmpHyb)
	{
		LAD_setResultBit(0U);
		Errortype = DCM_E_ENGINEISRUNNING;
	}

	return Errortype;
}

static void LAD_setResultBit (uint16 bit)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApLAD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLAD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLAD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLAD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLAD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
    
	/* Set bit */
	LAD_ResultTestElockActuator |= ((uint16)1U<< bit);
	/* Stop test */
	LAD_ElockActuatorTestOngoing = FALSE;
}

static void LAD_UpdateConditions(void)
{
	(void)Rte_Read_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(&LAD_EtatGmpHyb);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_ABS_VehSpd_ABS_VehSpd(&LAD_VehSpd);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpBatteryVolt_DeBatteryVolt(&LAD_BatteryVolt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
