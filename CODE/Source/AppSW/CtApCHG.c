/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApCHG.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApCHG
 *  Generation Time:  2020-11-23 11:42:16
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApCHG>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup CHG
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Charge sequence.
 * \details This module Controls the charge sequence.
 *
 * \{
 * \file  CHG.c
 * \brief  Generic code of the CHG module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * ComM_ModeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApCHG.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857, 0380 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
#include "CtApCHG_Cbk.h"
#include "Eth_30_Ar7000_Types.h"
#include "AppExternals.h"
#include "WdgM.h"
#include "EthTrcv_30_Ar7000_Priv.h"

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Max voltage range detection offset.*/
#define CHG_VOLT_OFFSET_100				100U
/** Number of phases.*/
#define CHG_NUM_PHASES					3U
/** 200ms delay.*/
#define CHG_DELAY_200ms					20U
/** Square root of 3 gain.*/
#define CHG_SQRT3_gain					37837U
/** Square root of 3 Q factor.*/
#define CHG_SQRT3_Q						16U
/** Square root of 2 gain.*/
#define CHG_SQRT2_gain					46341U
/** Square root of 2 Q factor.*/
#define CHG_SQRT2_Q						16U
/** Max allowed frequency.*/
#define CHG_FREQ_MAX					65U
/** Min allowed frequency*/
#define CHG_FREQ_MIN					45U
/** S2 open 20ms delay*/
#define CHG_S2OPEN_DELAY			2U
/** Stop HW 40ms delay*/
#define CHG_STOP_HW_DELAY			4U
/** Charge completed per thousand*/
#define CHG_PER_THOUSAND_MAX	1000U
/** Bulk completed per thousand*/
#define CHG_PER_THOUSAND_BULK	800U
/** Percentage max*/
#define CHG_MAX_PERCENTAGE		100U
/** Gain 10 definition*/
#define CHG_GAIN_10						10U
/** Gain 100 definition*/
#define CHG_GAIN_100					100U
/** Gain 1000 definition*/
#define CHG_GAIN_1000					1000U
/** Current offset*/
#define CHG_OFFSET_50					50U
/** Voltage offset*/
#define CHG_OFFSET_200				200U
/** S16 max value*/
#define CHG_S16_MAX						32767U
/** U16 max value*/
#define CHG_U16_MAX						0xFFFFU
/** U32 max value*/
#define CHG_U32_MAX						0xFFFFFFFFU
/** Voltage between Battery and relay to know that the pre-charge is completed*/
#define CHG_PRECHARGE_COMPLETE 9
/** Safety counter to avoid infinite loop*/
#define CHG_SAFETY_COUNTER		5U
/** Current max for soft stop*/
#define CHG_FAST_I_STOP				10U
/** Current max for soft stop*/
#define CHG_FAST_I_STOP_MIN		50U
/** Min tuple Id*/
#define CHG_FAST_MIN_TUPLE_ID	1U
/** Debounce time for AC invalid*/
#define CHG_AC_CONFIRM_TIME			50U
/** Maximum Power in mode 4*/
#define CHG_MODE4_MAX_POWER					10000U
/** Maximum number energy transfer mode for ISO*/
#define CHG_MODE4_ISO_NUMBER_ENERGY_TRANSFER_MODE		6U
/** SoftStop Timeout for 5A: 1seg*/
#define CHG_MODE4_SOFTSTOP_TIMEOUT	100U
/** SoftStop Force Timeout: 3seg*/
#define CHG_MODE4_SOFTSTOP_TIMEOUT_FORCE 300U
/*Time before opening contactors after opening S2*/
#define CHG_MODE4_CONTACTORS_DELAY 50U

/* PRQA S 0857, 0380 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/

typedef enum
{
	/**Downloading Firmware*/
	CHG_PLC_FIRMWARE,
	/**Idle state*/
	CHG_PLC_IDLE,
	/**Standby State*/
	CHG_PLC_STANDBY,
	/**Communications and setting*/
	CHG_PLC_INIT,
	/** OBC ready state*/
	CHG_PLC_READY,
	/**Precharge State*/
	CHG_PLC_PRECHARGE,
	/**Precharge complete, waiting for contactors to be closed*/
	CHG_PLC_PRECHARGE_COMPLETE,
	/**Charge State*/
	CHG_PLC_CHARGE,
	/**Soft stop*/
	CHG_PLC_SOFT_STOP,
	/**Emergency shutdown*/
	CHG_PLC_EMERGENCY,
	/**Stop EVES power*/
	CHG_PLC_POWER_OFF,
	/**Stop State*/
	CHG_PLC_STOP,
	/**Open Contactor*/
	CHG_PLC_CONTACTOR,
	/**Error state*/
	CHG_PLC_RST_ERROR

}CHG_PLC_InternalStates;

/** PLC variables.*/
typedef struct
{
		/*Input*/
		Scc_StateM_StateMachineStatusType SCCstateRes;
		uint16 EVSEcurrentLimit;
		uint16 EVSEvoltageLimit;
		uint16 EVSEpresentCurrent;
		uint16 EVSEpowerLimit;
		boolean EVSEstopReq;
		Scc_SAPSchemaIDType Schema;

		/*Output*/
		Scc_StateM_ChargingControlType SCCstateReq;
		Scc_StateM_FunctionControlType FunctionControl;
		DCDC_OBCMainContactorReq MainConReq;
		DCDC_OBCQuickChargeContactorReq QuickConReq;
		boolean EVReady;
		boolean ChargeComplete;
		boolean BulkChargingComplete;
		Scc_PhysicalValueType TargetVoltage;
		Scc_PhysicalValueType TargetCurrent;
		Scc_PhysicalValueType MaxVoltage;
		Scc_PhysicalValueType MaxCurrent;
		uint16 EVressSoc;
		Scc_StateM_ControlPilotStateType CPlineState;
		Exi_ISO_chargeProgressType ChargeProgress;
		Scc_StateM_PWMStateType HLCstate;
		uint8 SA_ScheduleID;
		boolean ForceElock;

		/*Internal*/
		CHG_PLC_InternalStates MachineState;
		boolean OBCready;
		boolean PowerOFF;
		boolean EmergencyShutdown;
		boolean FirwareDownloadReq;
		Scc_TCPSocketStateType TCPstate;
		boolean RSTrequest;
		boolean Retry;

		/*Debug*/
		uint8 StopReason;
		Scc_StateM_StateMachineErrorType Error;
		Scc_StackErrorType ErrorType;
		Scc_MsgStatusType MsgStatus;
		EthTrcv_30_Ar7000_Slac_StateType SLACstatus;
		uint8 SLACerror;
		boolean NMKreceived;
		boolean FirwareDownloaded;
		Scc_StateM_MsgStateType MsgState;

}CHG_PLC_data_t;

typedef struct
{
		OBC_InputVoltageSt VoltageSt;
		OBC_ACRange ACrange;
}CHG_VoltRange_t;



/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/

/* [Enrique.Bueno] Workaround START: MPU violation: CHG is ASIL SWC, but SCC Callbacks are executed in QM partition. ==================
                                     Hence, static variables defined at CHG and written into a SCC Callback will rise MPU violations.
                                     Contingency actions: remap all static variables affected from ASIL to QM partition taking profit
                                                          of TBD SWC (qm)                                                                 */
/* - Static non-init variables declaration ---------- */
#define CtApTBD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static CHG_PLC_data_t CHG_PLC_data;

static boolean CHG_OutOfSyncHit;
static boolean CHG_ResetRequestOnGoing;

#define CtApTBD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApTBD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApTBD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApTBD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApTBD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/* [Enrique.Bueno] Workaround STOP ====================================================================================================== */


/* - Static non-init variables declaration ---------- */
#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* [Enrique.Bueno] Workaround START: MPU violation: static variable moved from ASIL to QM partition */
/** PLC data needed for fast charge mode.*/
//static CHG_PLC_data_t CHG_PLC_data;
/* [Enrique.Bueno] Workaround STOP */

/** Current OBC state.*/
static OBC_Status CHG_obcState;

/** Relay S2 state.*/
static boolean CHG_relayS2state;
/** Input Voltage Mode.*/
static IdtInputVoltageMode CHG_InVlotMode;
/** Input Voltage Range.*/
static OBC_ACRange CHG_InVoltRange;

/*NVM*/
/** Upper threshold for 220V.*/
static uint16 CHG_Vrange_220_max;
/** Upper threshold for 110V.*/
static uint16 CHG_Vrange_110_max;
/** Lower threshold for 220V.*/
static uint16 CHG_Vrange_220_min;
/** Lower threshold for 110V.*/
static uint16 CHG_Vrange_110_min;
/** Threshold for disconnection range.*/
static IdtInputVoltageThreshold CHG_InputVoltageThreshold;
/** Threshold for no voltage detection..*/
static IdtThresholdNoAC CHG_ThresholdNoAC;
/** Input Voltage state*/
static OBC_InputVoltageSt VoltState;
/* [Enrique.Bueno] Workaround START: MPU violation: static variable moved from ASIL to QM partition */
//static boolean CHG_ResetRequestOnGoing;
//static boolean CHG_OutOfSyncHit;
/* [Enrique.Bueno] Workaround STOP */
static OBC_InputVoltageSt Current_VoltageSt;


/** Charging mode delayed for task A*/
static OBC_ChargingMode CHG_OBC_chargingMode_for_tack_A;

#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/** \brief Go to error state function
 *  \details This function This function evaluates if there OBC shall stop
 *  and go to error state.
 */
static boolean CHG_GotoError_fail_detection(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
																							IdtOutputELockSensor CHG_ElockState);

/** \brief AC disconnection fail
 *  \details This function detects any AC unexpected disconnection.
 */
static boolean CHG_AC_fail_detection(OBC_ChargingMode CHG_OBC_chargingMode);
/** \brief E-lock feedback fail
 *  \details This function detects any E-lock feedback fail.
 */
static boolean CHG_Elock_fail_detection(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
																		 IdtOutputELockSensor CHG_ElockState);
/** \brief CP frequency fail
 *  \details This function detects a fail in the CP frequency.
 */
static boolean CHG_CPfreq_fail_detection(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation);
/** \brief Update stop condition
 *  \details This function Updates the Stop condition
 */
static boolean CHG_stopCondition(BMS_OnBoardChargerEnable CHG_BMS_OBCenable,OBC_ChargingMode CHG_OBC_chargingMode,
																 boolean GoToError);
/** Detection for AC disconnection*/
static boolean CHG_AC_disconnected(OBC_ChargingMode CHG_OBC_chargingMode);
/** \brief  Update ready condition
 *  \details This function Updates the ready.
 */
static void CHG_OBC_StartCondition(boolean FaultChargeSoftStop,
					OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
					OBC_ChargingMode CHG_OBC_chargingMode);
/**
 * \brief HW stop request.
 * \details This function checks if the HW must be stop.
 */
static boolean CHG_RequestHWStopOBC(boolean CHG_Generalstop);

/** \brief Update relay S2 state.
 *  \details This function Updates the relay S2 state.
 */
static void CHG_relayS2(IdtOutputELockSensor CHG_ElockState, BMS_MainConnectorState CHG_contactorsState,
						BMS_OnBoardChargerEnable CHG_BMS_OBCenable, boolean CHG_Generalstop, boolean RequestHWStop,OBC_ChargingMode CHG_OBC_chargingMode);
/** S2 state in slow charging mode.*/
static void CHG_relayS2_slow(IdtOutputELockSensor CHG_ElockState, BMS_MainConnectorState CHG_contactorsState,
						 boolean CHG_Generalstop, boolean RequestHWStop, uint8 *OpenS2Counter);
/** S2 state in fast charging mode.*/
static void CHG_relayS2_fast(IdtOutputELockSensor CHG_ElockState,
						BMS_OnBoardChargerEnable CHG_BMS_OBCenable);
/**
 * \brief OBC state-machine.
 * \detail This function updates the OBC state.
 */
static void CHG_OBCstate(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
												 boolean LaunchAllowed, boolean CHG_Generalstop, boolean GoToError);
/**
 * \brief Launch conversion allowed.
 * \details This function checks if the launch conditions are met.
 */
static boolean CHG_LaunchCondition(IdtOutputELockSensor CHG_ElockState,
																	 BMS_OnBoardChargerEnable CHG_BMS_OBCenable,
																	 BMS_MainConnectorState CHG_contactorsState,
																	 OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation);
/** OBC state Off sub-function.*/
static void CHG_OBCstate_off(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation);
/** OBC state Init sub-function.*/
static void CHG_OBCstate_init(boolean LaunchAllowed, boolean GoToError);
/** OBC state Standby sub-function.*/
static void CHG_OBCstate_standby(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
																 boolean LaunchAllowed, boolean CHG_Generalstop, boolean GoToError);
/** OBC state Conversion sub-function.*/
static void CHG_OBCstate_conversion(boolean CHG_Generalstop);
/** OBC state Degradation sub-function.*/
static void CHG_OBCstate_degradation(boolean CHG_Generalstop);
/** OBC state Error sub-function.*/
static void CHG_OBCstate_error(boolean GoToError);

/**
 * \brief Input Voltage mode task.
 * \details This function manages the Input voltage detenction.
 */
static void CHG_InputVoltageMode(OBC_ChargingMode CHG_OBC_chargingMode,const uint16 CHG_RMSphase[CHG_NUM_PHASES],
																 OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation);
/** Input Voltage mode detection.*/
static void CHG_InVoltMode_identify(const uint16 CHG_RMSphase[CHG_NUM_PHASES]);

/** \brief Check triphasic condition
 *  \details This function checks if the input voltages and frequency are in triphasic
 *  acceptable range.
 *	\retval TRUE all parameters are in range.
 */
static boolean CHG_checkTriphasicCondition(const uint16 CHG_RMSphase[CHG_NUM_PHASES]);
/**
 * \brief Check Values in Range.
 * \details this function will check if all inputs values are inside a range.
 * \param[in] CHG_InValue Input values.
 * \param[in] MaxValue Upper threshold.
 * \param[in] MinValue Lower threshold.
 * \retval TRUE	All values inside the range.
 * \retval FALSE Any value outside the range.
 */
static boolean CHG_checkValuesInRange(const uint16 CHG_InValue[CHG_NUM_PHASES],uint16 MaxValue, uint16 MinValue);
/**
 * \brief Input voltage state and range detection.
 * \details This function determines the voltage state and range depending on the
 * Voltage line.
 * \param[in] CHG_RMSphase Phase voltage RMS.
 */
static void CHG_InputVoltageState(const uint16 CHG_RMSphase[CHG_NUM_PHASES]);
/** Instantaneous Voltage state*/
static CHG_VoltRange_t CHG_InstantaneousVoltagestate(const uint16 CHG_RMSphase[CHG_NUM_PHASES]);
/**
 * \brief Convert Phase values to Line values.
 */
static void CHG_PhaseToLine(const uint16 CHG_RMSphase[CHG_NUM_PHASES],uint16 (*CHG_RMSline)[CHG_NUM_PHASES]);


/** Fast charge mode task*/
static void CHG_fast_task(boolean GeneralStop, boolean FaultDetected, OBC_ChargingMode CHG_OBC_chargingMode);
/** Update charge progress.*/
static void CHG_fast_UpdateChargeState(void);
/** Update the signals that have S2 dependencies.*/
static void CHG_fast_UpsateCPlineState(void);
/** Check if an unexpected disconnection has been produced.*/
static void CHG_fast_UnexpectedDisconection(void);
/** Check if FW is allow to be download.*/
static boolean CHG_fast_FirmwareDownloadAllow(void);
/** Update charging state in fast mode.*/
static void CHG_fast_UpdateStateMachien(boolean GeneralStop, boolean FaultDetected, OBC_ChargingMode CHG_OBC_chargingMode);

/** State machine sub-function: FirmwareDownload*/
static void CHG_fast_State_Firmware(void);
/** State machine sub-function: Idle*/
static void CHG_fast_State_Idle(void);
/** State machine sub-function: Standby*/
static void CHG_fast_State_Standby(boolean FaultDetected, OBC_ChargingMode CHG_OBC_chargingMode);
/** State machine sub-function: Init*/
static void CHG_fast_State_Init(void);
/** Detect a SLAC fail while initializations*/
static void CHG_SlacFailDetection(void);
/** State machine sub-function: Ready*/
static void CHG_fast_State_Ready(void);
/** State machine sub-function: PreCharge*/
static void CHG_fast_State_PreCharge(boolean GeneralStop);
/** State machine sub-function: PreChrageComplete*/
static void CHG_fast_State_PreChrageComplete(boolean GeneralStop);
/** State machine sub-function: Charge*/
static void CHG_fast_State_Charge(boolean GeneralStop);
/** State machine sub-function: SoftStop*/
static void CHG_fast_State_SoftStop(void);
/** State machine sub-function: EmergencyShutdown*/
static void CHG_fast_State_Emergency(void);
/** State machine sub-function: PowerOff*/
static void CHG_fast_State_PoweOff(void);
/** State machine sub-function: Stop*/
static void CHG_fast_State_Stop(void);
/** State machine sub-function: Open Contactor*/
static void CHG_fast_State_Contactor(void);
/** State machine sub-function: Error*/
static void CHG_fast_State_Error(void);
/** State machine outpus depending on the current state.*/
static void CHG_fast_UpdateOutputs(void);

/** This function sets all the fast chrage variables to its defaul value*/
static void CHG_fast_default_parameters(void);
/** EndCharge_RTAB sub-task*/
static void CHG_fast_EndCharge_RTAB(void);

/** Convert to physical value type.*/
static Scc_PhysicalValueType CHG_PhysicalValueConvert(uint32 value, uint16 offset, sint8 exponent);
/** Voltage target while charging.*/
static Scc_PhysicalValueType CHG_Fast_VoltageTarget(void);
/** Current target while charging.*/
static Scc_PhysicalValueType CHG_Fast_CurrentTarget(void);
/** Convert physical value to 0,1 factor.*/
static uint16 CHG_PhysicalValue_deci(Scc_PhysicalValueType EVvalue);
/** PLC reset*/
static void CHG_resetPLC(void);

/** Update calibratables*/
static void CHG_UpdateCalibratables(void);
/** Update random number*/
extern uint16 ApplRand_GetRandNo(void);
/*Trigger FW download*/
void Appl_TriggerDownloadReqCbk(void);
/** Check for short circuits in DC temperature lines*/
static boolean CHG_DCTempLinesSC(void);

extern FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_TriggerDwld(uint8 TrcvIdx);
extern FUNC(EthTrcv_30_Ar7000_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_GetSwVersion(uint8 TrcvIdx,P2VAR(uint8, ETHTRCV_30_AR7000_APPL_VAR, ETHTRCV_30_AR7000_APPL_VAR) SwVersionPtr);
extern FUNC(Std_ReturnType, ETHTRCV_30_AR7000_CODE) EthTrcv_30_Ar7000_Slac_Reset(uint8 TrcvIdx);

void Scc_Cbk_SLAC_AssociationState(EthTrcv_30_Ar7000_Slac_StateType CurState, boolean MessageValid, void* Message);
EthTrcv_30_Ar7000_Slac_StartModeType Appl_SccDynCP_StateM_SLACStartMode(void);
uint16 Appl_SccDynCP_StateM_QCAIdleTimer(void);

extern FUNC(Std_ReturnType, TCPIP_CODE) TcpIp_Close(TcpIp_SocketIdType SocketId, boolean Abort);
extern VAR(TcpIp_SocketIdType, SCC_VAR_NOINIT)  Scc_V2GSocket;

extern VAR(EthTrcv_30_Ar7000_Slac_MgmtType, ETHTRCV_30_AR7000_VAR_NOINIT) EthTrcv_30_Ar7000_Slac_MgmtStruct;/* PRQA S 3451 #Variable used from BSW*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * BMS_HighestChargeCurrentAllow: Integer in interval [0...2047]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * BMS_HighestChargeVoltageAllow: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * BMS_OnBoardChargerEnable: Boolean
 * BMS_QuickChargeConnectorState: Boolean
 * BMS_SOC: Integer in interval [0...1023]
 * BMS_Voltage: Integer in interval [0...500]
 * EVSE_RTAB_STOP_CHARGE: Boolean
 * IdtBulkSocConf: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDemandMaxCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtEVSEMaximumPowerLimit: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtEnergyCapacity: Integer in interval [0...255]
 *   Unit: [kWh], Factor: 1, Offset: 0
 * IdtInputVoltageThreshold: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMaxDemmandCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxDemmandVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxDiscoveryCurrent: Integer in interval [0...200]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxDiscoveryVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMaxInputVoltage110V: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 100
 * IdtMaxInputVoltage220V: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 100
 * IdtMaxPrechargeCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 1, Offset: 50
 * IdtMaxPrechargeVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 200
 * IdtMinInputVoltage110V: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMinInputVoltage220V: Integer in interval [0...200]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtStopChargeDemmandTimer: Integer in interval [0...600]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTargetPrechargeCurrent: Integer in interval [0...100]
 *   Unit: [A], Factor: 1, Offset: 0
 * IdtThresholdNoAC: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtTimeConfirmNoAC: Integer in interval [0...300]
 *   Unit: [s], Factor: 1, Offset: 0
 * IdtTimeElockFaultDetected: Integer in interval [0...60]
 *   Unit: [s], Factor: 1, Offset: 0
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_OBCStartSt: Boolean
 * OBC_PlugVoltDetection: Boolean
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH1_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH2_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * PFC_VPH3_Freq_Hz: Integer in interval [0...1023]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_MainConnectorState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_contactors_opened (0U)
 *   Cx1_precharge (1U)
 *   Cx2_contactors_closed (2U)
 *   Cx3_Invalid (3U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * DCDC_OBCMainContactorReq: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_Request_to_Open_the_Contactors (0U)
 *   Cx1_Request_to_Close_the_Contactors (1U)
 *   Cx2_No_request (2U)
 *   Cx3_Reserved (3U)
 * DCDC_OBCQuickChargeContactorReq: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_Request_to_Open_the_Contactors (0U)
 *   Cx1_Request_to_Close_the_Contactors (1U)
 *   Cx2_No_request (2U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * IdtOutputELockSensor: Enumeration of integer in interval [0...2] with enumerators
 *   ELOCK_LOCKED (0U)
 *   ELOCK_UNLOCKED (1U)
 *   ELOCK_DRIVE_UNDEFINED (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_CP_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_value (0U)
 *   Cx1_CP_invalid_not_connected (1U)
 *   Cx2_CP_valid_full_connected (2U)
 *   Cx3_CP_invalid_half_connected (3U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtMaxDemmandVoltage Rte_CData_CalMaxDemmandVoltage(void)
 *   IdtMaxDiscoveryVoltage Rte_CData_CalMaxDiscoveryVoltage(void)
 *   IdtMaxPrechargeVoltage Rte_CData_CalMaxPrechargeVoltage(void)
 *   IdtStopChargeDemmandTimer Rte_CData_CalStopChargeDemmandTimer(void)
 *   IdtTimeConfirmNoAC Rte_CData_CalTimeConfirmNoAC(void)
 *   IdtBulkSocConf Rte_CData_CalBulkSocConf(void)
 *   IdtDemandMaxCurrent Rte_CData_CalDemandMaxCurrent(void)
 *   IdtEnergyCapacity Rte_CData_CalEnergyCapacity(void)
 *   IdtMaxDemmandCurrent Rte_CData_CalMaxDemmandCurrent(void)
 *   IdtMaxDiscoveryCurrent Rte_CData_CalMaxDiscoveryCurrent(void)
 *   IdtMaxInputVoltage110V Rte_CData_CalMaxInputVoltage110V(void)
 *   IdtMaxInputVoltage220V Rte_CData_CalMaxInputVoltage220V(void)
 *   IdtMaxPrechargeCurrent Rte_CData_CalMaxPrechargeCurrent(void)
 *   IdtMinInputVoltage110V Rte_CData_CalMinInputVoltage110V(void)
 *   IdtMinInputVoltage220V Rte_CData_CalMinInputVoltage220V(void)
 *   IdtTargetPrechargeCurrent Rte_CData_CalTargetPrechargeCurrent(void)
 *   IdtThresholdNoAC Rte_CData_CalThresholdNoAC(void)
 *   IdtTimeElockFaultDetected Rte_CData_CalTimeElockFaultDetected(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtInputVoltageThreshold Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold(void)
 *
 *********************************************************************************************************************/


#define CtApCHG_START_SEC_CODE
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_V2G_States_Debug_CHGDebugData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/
	 Std_ReturnType ret;
	(void)OpStatus;	/* Remove compilator warning */

	if (NULL_PTR != ErrorCode)
	{
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		ret = RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_V2G_States_Debug_CHGDebugData_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_V2G_States_Debug_CHGDebugData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) DataServices_V2G_States_Debug_CHGDebugData_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_V2G_States_Debug_CHGDebugData_ReadData (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus;	/* Remove compilator warning */

	if (NULL_PTR != Data)
	{
		Data[0] = CHG_PLC_data.StopReason;
		//Data[1] = CHG_relayS2state;
		//Data[1] = CHG_PLC_data.TCPstate;
		//Data[1] = (uint8)CHG_PLC_data.SLACerror;
		//Data[1] = (uint8)CHG_PLC_data.MsgStatus;
		if (FALSE != CHG_relayS2state)
		{
			Data[1] = ((uint8)CHG_PLC_data.SLACstatus<<4U) | ((uint8)CHG_PLC_data.MsgState<<1U) | 0x01U;
		}
		else
		{
			Data[1] = ((uint8)CHG_PLC_data.SLACstatus<<4U) | ((uint8)CHG_PLC_data.MsgState<<1U);
		}
		//Data[1] = CHG_PLC_data.FirwareDownloaded;
		//Data[1] = CHG_PLC_data.FirwareDownloadReq;
		//Data[0] = CHG_PLC_data.NMKreceived;
		//Data[1] = CHG_PLC_data.ErrorType;

		ret = RTE_E_OK;
	}
	else
	{
		ret = RTE_E_DataServices_V2G_States_Debug_CHGDebugData_E_NOT_OK;
	}


  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCHG_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_init_doc
 *********************************************************************************************************************/

/*!
 * \brief CHG initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCHG_CODE) RCtApCHG_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_init
 *********************************************************************************************************************/

	CHG_InputVoltageThreshold=Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold();
	CHG_ThresholdNoAC=Rte_CData_CalThresholdNoAC();

	CHG_Vrange_110_max = (uint16)Rte_CData_CalMaxInputVoltage110V() + (uint16)CHG_VOLT_OFFSET_100;
	CHG_Vrange_110_min = (uint16)Rte_CData_CalMinInputVoltage110V();

	CHG_Vrange_220_max = (uint16)Rte_CData_CalMaxInputVoltage220V() + (uint16)CHG_VOLT_OFFSET_100;
	CHG_Vrange_220_min = (uint16)Rte_CData_CalMinInputVoltage220V();

	CHG_obcState = Cx0_off_mode;
	CHG_relayS2state = FALSE;
	CHG_InVlotMode = IVM_NO_INPUT_DETECTED;
	CHG_InVoltRange = Cx0_No_AC_10V_;
	VoltState = Cx0_No_220V_AC;
	Current_VoltageSt = Cx0_No_220V_AC;

	CHG_ResetRequestOnGoing = FALSE;
	CHG_OutOfSyncHit = FALSE;


	/*Fast charging mode data.*/
	CHG_fast_default_parameters();

	/*Init*/
	CHG_PLC_data.MachineState = CHG_PLC_FIRMWARE;

	CHG_OBC_chargingMode_for_tack_A = Cx0_no_charging;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCHG_task10msA
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(boolean data)
 *   Std_ReturnType Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(IdtInputVoltageMode data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(OBC_ACRange data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(OBC_Status data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msA_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCHG_CODE) RCtApCHG_task10msA(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msA
 *********************************************************************************************************************/


	boolean AC_error;

	/*Task A is executed before task B. Thus, the value published in A has been calculated in B during the last cycle.*/
	(void)Rte_Write_PpInputVoltageMode_Delayed_DeInputVoltageMode_Delayed(CHG_InVlotMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_Status_Delayed_OBC_Status(CHG_obcState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_ACRange_Delayed_OBC_ACRange(CHG_InVoltRange);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	AC_error = CHG_AC_fail_detection(CHG_OBC_chargingMode_for_tack_A);

	(void)Rte_Write_PpDiagnosticNoACInput_DeDiagnosticNoACInput(AC_error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApCHG_task10msB
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint *data)
 *   Std_ReturnType Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(boolean *data)
 *   Std_ReturnType Rte_Read_PpFreqOutRange_DeFreqOutRange(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_SOC_BMS_SOC(BMS_SOC *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_Delayed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpOBCDerating_DeOBCDerating(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputELockSensor_DeOutputELockSensor(IdtOutputELockSensor *data)
 *   Std_ReturnType Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC1FaultSCG(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC1FaultSCP(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC2FaultSCG(boolean *data)
 *   Std_ReturnType Rte_Read_PpTempFaults_DeTempDC2FaultSCP(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(boolean data)
 *   Std_ReturnType Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit data)
 *   Std_ReturnType Rte_Write_PpElockFaultDetected_DeElockFaultDetected(boolean data)
 *   Std_ReturnType Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean data)
 *   Std_ReturnType Rte_Write_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(DCDC_OBCMainContactorReq data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(DCDC_OBCQuickChargeContactorReq data)
 *   Std_ReturnType Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_Status_OBC_Status(OBC_Status data)
 *   Std_ReturnType Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(boolean data)
 *   Std_ReturnType Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(boolean data)
 *   Std_ReturnType Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpStopConditions_DeStopConditions(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msB_doc
 *********************************************************************************************************************/

 /*!
  * \brief CHG task.
  *
  * This function is called periodically by the scheduler. This function should
  * Updates the OBC state.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApCHG_CODE) RCtApCHG_task10msB(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApCHG_task10msB
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	BMS_OnBoardChargerEnable CHG_BMS_OBCenable;
	OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation;
	IdtOutputELockSensor CHG_ElockState;
	BMS_MainConnectorState CHG_contactorsState;
	boolean CHG_FaultSoftStop;
	boolean CHG_Generalstop;
	boolean GoToError;
	OBC_ChargingMode CHG_OBC_chargingMode;
	uint16 CHG_RMSphase[CHG_NUM_PHASES];
	boolean LaunchAllowed;
	boolean RequestHWStop;
	uint8 FirmwareDownloadBit;
	uint8 RelayS2stateBit;

	CHG_UpdateCalibratables();

	/*Update random number generator*/
	(void)ApplRand_GetRandNo();

	(void)Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(&CHG_BMS_OBCenable);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&CHG_OBC_ConnectionConfirmation);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputELockSensor_DeOutputELockSensor(&CHG_ElockState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(&CHG_contactorsState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&CHG_FaultSoftStop);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&CHG_OBC_chargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&CHG_RMSphase[0U]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(&CHG_RMSphase[1U]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(&CHG_RMSphase[2U]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	GoToError = CHG_GotoError_fail_detection(CHG_OBC_ConnectionConfirmation,CHG_ElockState);
	CHG_InputVoltageMode(CHG_OBC_chargingMode,CHG_RMSphase,CHG_OBC_ConnectionConfirmation);
	CHG_InputVoltageState(CHG_RMSphase);


	CHG_Generalstop = CHG_stopCondition(CHG_BMS_OBCenable,CHG_OBC_chargingMode,GoToError);

	LaunchAllowed = CHG_LaunchCondition(CHG_ElockState,CHG_BMS_OBCenable,
																			CHG_contactorsState,CHG_OBC_ConnectionConfirmation);
	CHG_OBCstate(CHG_OBC_ConnectionConfirmation, LaunchAllowed, CHG_Generalstop,GoToError);

	/* This machine will provide information for the OBC ready signal, So it should be excuted before: OBC_StartCondition.
	* This machine Uses the S2state as input, So it should be excuted before the S2state is update.*/
	CHG_fast_task(CHG_Generalstop,CHG_FaultSoftStop,CHG_OBC_chargingMode);

	CHG_OBC_StartCondition(CHG_FaultSoftStop,CHG_OBC_ConnectionConfirmation,CHG_OBC_chargingMode);



	/*Must be executed after the OBCstate*/
	RequestHWStop=CHG_RequestHWStopOBC(CHG_Generalstop);
	CHG_relayS2(CHG_ElockState, CHG_contactorsState, CHG_BMS_OBCenable,
							CHG_Generalstop, RequestHWStop,CHG_OBC_chargingMode);


	if (FALSE != CHG_ResetRequestOnGoing)
	{
		CHG_ResetRequestOnGoing = FALSE;
		/* Activate physical reset. Set pin to FALSE */
		(void)Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(FALSE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		/* Release physical reset. Set pin to TRUE */
		(void)Rte_Write_PpResetPLCPhysicalValue_DeResetPLCPhysicalValue(TRUE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,0U,(uint8)CHG_PLC_data.StopReason);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,1U,(uint8)CHG_PLC_data.SCCstateRes);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,2U,(uint8)CHG_PLC_data.MsgStatus);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,3U,(uint8)CHG_PLC_data.MsgState|((uint8)CHG_PLC_data.MachineState<<4));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* FirwareDownloadReq boolean to bit mask */
	if (FALSE != CHG_PLC_data.FirwareDownloadReq)
	{
		FirmwareDownloadBit = 0x01U;
	}
	else
	{
		FirmwareDownloadBit = 0x0U;
	}
	/* CHG_relayS2state boolean to bit mask */
	if (FALSE != CHG_relayS2state)
	{
		RelayS2stateBit = 0x01U;
	}
	else
	{
		RelayS2stateBit = 0x0U;
	}
	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,4U,(uint8)CHG_PLC_data.SLACstatus|(RelayS2stateBit<<4)|(FirmwareDownloadBit<<5));/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	CHG_OBC_chargingMode_for_tack_A = CHG_OBC_chargingMode;

	if(EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[0]!=NULL_PTR)
	{
			 (void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(0U,7U,(uint8)EthTrcv_30_Ar7000_Slac_MgmtStruct.SlacProfilesSorted[0]->SlacAverageAtten);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_V2G_States_Debug_CHGInternalState>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus;	/* Remove compilator warning */

	if (NULL_PTR != ErrorCode)
	{
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		ret = RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_CHGInternalState_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_V2G_States_Debug_CHGInternalState>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_CHGInternalState_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_CHGInternalState_ReadData (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	(void)OpStatus;	/* Remove compilator warning */

	if (NULL_PTR != Data)
	{
		if (FALSE != CHG_PLC_data.FirwareDownloadReq)
		{
			Data[0] = (uint8)CHG_PLC_data.MachineState | (0x01U << 7);
		}
		else
		{
			Data[0] = (uint8)CHG_PLC_data.MachineState;
		}
		ret = RTE_E_OK;
	}
	else
	{
		ret = RTE_E_DataServices_V2G_States_Debug_CHGInternalState_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_V2G_States_Debug_SCCStateMachine>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;

	(void)OpStatus;	/* Remove compilator warning */

	if (NULL_PTR != ErrorCode)
	{
		*ErrorCode = DCM_E_POSITIVERESPONSE;
		ret = RTE_E_OK;
	}
	else
	{
		ret = RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK;
	}

  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RDataServices_V2G_States_Debug_SCCStateMachine_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_V2G_States_Debug_SCCStateMachine>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_DCM_E_PENDING
 *   RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApCHG_CODE) RDataServices_V2G_States_Debug_SCCStateMachine_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPCHG_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RDataServices_V2G_States_Debug_SCCStateMachine_ReadData (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;

	(void)OpStatus;	/* Remove compilator warning */

	if (NULL_PTR != Data)
    {
    	Data[0] = (uint8)CHG_PLC_data.MsgStatus;
    	//Data[0] = (uint8)CHG_PLC_data.SCCstateRes;
    	ret = RTE_E_OK;
    }
    else
    {
    	ret = RTE_E_DataServices_V2G_States_Debug_SCCStateMachine_E_NOT_OK;
    }
  return ret;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApCHG_STOP_SEC_CODE
#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
static boolean CHG_GotoError_fail_detection(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
																						IdtOutputELockSensor CHG_ElockState)
{
	/* - Static non-init variables declaration ---------- */
		#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		/* Static non-init variables HERE... */

		#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


		/* - Static zero-init variables declaration --------- */
		#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		/* Static zero-init variables HERE... */

		#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


		/* - Static init variables declaration -------------- */
		#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		/* Static init variables HERE... */

		#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


		/* - Automatic variables declaration ---------------- */
	boolean Goto_error;
	boolean CPfreq_Error;

	Goto_error = CHG_Elock_fail_detection(CHG_OBC_ConnectionConfirmation,CHG_ElockState);
	CPfreq_Error = CHG_CPfreq_fail_detection(CHG_OBC_ConnectionConfirmation);
	Goto_error = Goto_error || CPfreq_Error;/* PRQA S 4558, 4404, 4558 #We do not use a native bool type, GNU-C supports this implementation*/

	return Goto_error;
}
static boolean CHG_AC_fail_detection(OBC_ChargingMode CHG_OBC_chargingMode)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static uint16 NoAC_Timeout=0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	boolean AC_fail;


	if((FALSE==CHG_relayS2state)
			|| (Cx1_slow_charging!=CHG_OBC_chargingMode))
	{
		/*Don't detect the fail while stopping, it was the OBC who has trigger the disconnection*/
		NoAC_Timeout=0U;
		AC_fail = FALSE;
	}
	else if((Cx0_No_220V_AC!=VoltState)&&(Cx3_Invalid_value!=VoltState))
	{
		NoAC_Timeout=0U;
		AC_fail = FALSE;
	}
	else if(((uint16)Rte_CData_CalTimeConfirmNoAC()*(uint16)CHG_GAIN_100)>NoAC_Timeout)
	{
		NoAC_Timeout++;
		AC_fail = FALSE;
	}
	else
	{
		/*Time out for detecting AC after S2 has been closed in slow chargin mode*/
		AC_fail = TRUE;
	}

	return AC_fail;
}
static boolean CHG_Elock_fail_detection(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
																				IdtOutputELockSensor CHG_ElockState)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean Elock_fail = FALSE;
	static uint16 Elock_Debounce = 0U ;
	static uint16 InitialDelay = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IdtELockSetPoint SetPoint;
	boolean PlantMode;


	(void)Rte_Read_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(&SetPoint);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpPlantModeState_DePlantModeState(&PlantMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(300U>InitialDelay)
	{
		/*The LAD has an initial delay of 3 seg due to PXL debounce time
		for detecting the horse initial state*/
		InitialDelay++;
	}
	else if(FALSE!=PlantMode)
	{
		/*Don't set this fail in plant mode*/
		Elock_fail = FALSE;
		Elock_Debounce=0U;
	}
	else if(Cx1_not_connected==CHG_OBC_ConnectionConfirmation)
	{
		/*Clean fail after unplugging*/
		Elock_fail = FALSE;
		Elock_Debounce=0U;
	}
	else if((ELOCK_SETPOINT_LOCKED==SetPoint)&&(ELOCK_LOCKED==CHG_ElockState))
	{
		/*Lock OK*/
		Elock_Debounce=0U;
	}
	else if((ELOCK_SETPOINT_UNLOCKED==SetPoint)&&(ELOCK_UNLOCKED==CHG_ElockState))
	{
		/*unlock OK*/
		Elock_Debounce=0U;
	}
	else if((((uint16)Rte_CData_CalTimeElockFaultDetected()*CHG_GAIN_100))<Elock_Debounce)
	{
		/*Fault detected*/
		Elock_fail = TRUE;
	}
	else
	{
		Elock_Debounce++;
	}

	(void)Rte_Write_PpElockFaultDetected_DeElockFaultDetected(Elock_fail);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return Elock_fail;
}
static boolean CHG_CPfreq_fail_detection(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static boolean FreqError = FALSE;
	static uint8 DebounceTime = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean FreqOutofRange;

	(void)Rte_Read_PpFreqOutRange_DeFreqOutRange(&FreqOutofRange);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(Cx1_not_connected==CHG_OBC_ConnectionConfirmation)
	{
		/*Clean Fault*/
		FreqError = FALSE;
		DebounceTime = 0U;
	}
	else if((FALSE==FreqOutofRange)||(Cx2_full_connected!=CHG_OBC_ConnectionConfirmation))
	{
		/*No error detected*/
		DebounceTime = 0U;
	}
	else if(10U<=DebounceTime)
	{
		FreqError = TRUE;
	}
	else
	{
		DebounceTime++;
	}

	(void)Rte_Write_PpControlPilotFreqError_DeControlPilotFreqError(FreqError);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return FreqError;
}
static boolean CHG_stopCondition(BMS_OnBoardChargerEnable CHG_BMS_OBCenable,OBC_ChargingMode CHG_OBC_chargingMode,
																 boolean GoToError)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Inputs*/
	boolean CHG_discharge;
	boolean CHG_FaultChargeSoftStop;
	boolean PlantMode;
	VCU_ModeEPSRequest CHG_VCU_ModeEPSRequest;

	/*Outputs*/
	static boolean CHG_stopRequest = FALSE;
	boolean CHG_stopGeneral;
	boolean CHG_AC_discon;
	boolean CHG_DC_temp_lines_SC;

	CHG_AC_discon = CHG_AC_disconnected(CHG_OBC_chargingMode);
	CHG_DC_temp_lines_SC = CHG_DCTempLinesSC();


	(void)Rte_Read_PpFaultChargeSoftStop_DeFaultChargeSoftStop(&CHG_FaultChargeSoftStop);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(&CHG_discharge);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpPlantModeState_DePlantModeState(&PlantMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(&CHG_VCU_ModeEPSRequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((TRUE!=CHG_BMS_OBCenable)
			||(Cx0_no_charging==CHG_OBC_chargingMode)
			||(FALSE!=CHG_discharge)
			||(FALSE!=PlantMode)
			||(Cx0_OFF == CHG_VCU_ModeEPSRequest)
			||(Cx1_Active_Drive == CHG_VCU_ModeEPSRequest)
			||(Cx2_Discharge == CHG_VCU_ModeEPSRequest)
			||(FALSE!=GoToError)
			||(FALSE!= CHG_AC_discon)
			||((FALSE!=CHG_PLC_data.EVSEstopReq)&&(Cx3_Euro_fast_charging==CHG_OBC_chargingMode))
			||((TRUE == CHG_DC_temp_lines_SC)&&(Cx3_Euro_fast_charging==CHG_OBC_chargingMode))
	)
	{
		CHG_stopRequest = TRUE;
	}
	else if((Cx3_conversion_working_!=CHG_obcState) && (Cx5_degradation_mode!=CHG_obcState))
	{
		/*The Stop Request only should be cleaned outside conversion/degradation*/
		CHG_stopRequest = FALSE;
	}
	else
	{
		/*Misra*/
	}


	if((FALSE!=CHG_stopRequest)||(FALSE!=CHG_FaultChargeSoftStop))
	{
		CHG_stopGeneral = TRUE;
	}
	else
	{
		CHG_stopGeneral = FALSE;
	}


	(void)Rte_Write_PpStopConditions_DeStopConditions(CHG_stopRequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	return CHG_stopGeneral;
}
static boolean CHG_AC_disconnected(OBC_ChargingMode CHG_OBC_chargingMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean retval;

	if((Cx1_slow_charging==CHG_OBC_chargingMode)
			&&(Cx1_220V_AC_connected==VoltState)
			&&(FALSE!=CHG_relayS2state)
			&&(Cx1_220V_AC_connected!=Current_VoltageSt))
	{
		/*Unexpected AC disconnection*/
		retval = TRUE;
	}
	else
	{
		retval = FALSE;
	}

	return retval;
}

static boolean CHG_DCTempLinesSC(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean retval;
	boolean short_circuit_detected;
	/* PRQA S 2985,2986 ++ # These operation kept in this way for clarification purposes */
	retval = FALSE;
	(void)Rte_Read_PpTempFaults_DeTempDC1FaultSCG(&short_circuit_detected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	retval |= short_circuit_detected;
	(void)Rte_Read_PpTempFaults_DeTempDC1FaultSCP(&short_circuit_detected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	retval |= short_circuit_detected;
	(void)Rte_Read_PpTempFaults_DeTempDC2FaultSCG(&short_circuit_detected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	retval |= short_circuit_detected;
	(void)Rte_Read_PpTempFaults_DeTempDC2FaultSCP(&short_circuit_detected);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	retval |= short_circuit_detected;
	/* PRQA S 2985,2986 -- # These operation kept in this way for clarification purposes */

	return retval;
}

static void CHG_OBC_StartCondition(boolean FaultChargeSoftStop,
					OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
					OBC_ChargingMode CHG_OBC_chargingMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_OBCStartSt OBCready;
	OBC_CP_connection_Status CHG_CPstate;


	(void)Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(&CHG_CPstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(FALSE!=FaultChargeSoftStop)
	{
		OBCready=FALSE;
	}
	else if((Cx2_full_connected!=CHG_OBC_ConnectionConfirmation)
			||(Cx2_CP_valid_full_connected!=CHG_CPstate))
	{
		OBCready=FALSE;
	}
	else if(Cx1_slow_charging==CHG_OBC_chargingMode)
	{
		OBCready=TRUE;
	}
    else if((FALSE!=CHG_PLC_data.OBCready)&&(Cx3_Euro_fast_charging==CHG_OBC_chargingMode))
	{
		OBCready=TRUE;
	}
	else
	{
		OBCready=FALSE;
	}

	(void)Rte_Write_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBCready);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static boolean CHG_RequestHWStopOBC(boolean CHG_Generalstop)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean StopHWrequest = FALSE;
	static boolean SetPending = FALSE;
	static uint8 SetCounter = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Update window*/
	if((Cx3_conversion_working_== CHG_obcState) || (Cx5_degradation_mode == CHG_obcState))
	{
		if(FALSE==CHG_Generalstop)
		{
			StopHWrequest = FALSE;
			SetPending = FALSE;
		}
		else
		{
			SetPending = TRUE;
		}
	}


	if(TRUE!=SetPending)
	{
		SetCounter = 0U;
	}
	else if(CHG_STOP_HW_DELAY<=SetCounter)
	{
		StopHWrequest=TRUE;
		SetPending = FALSE;
		SetCounter = 0U;
	}
	else
	{
		SetCounter++;
	}

	(void)Rte_Write_PpRequestHWStopOBC_DeRequestHWStopOBC(StopHWrequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	return StopHWrequest;

}
static void CHG_relayS2(IdtOutputELockSensor CHG_ElockState, BMS_MainConnectorState CHG_contactorsState,
						BMS_OnBoardChargerEnable CHG_BMS_OBCenable, boolean CHG_Generalstop, boolean RequestHWStop,OBC_ChargingMode CHG_OBC_chargingMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 OpenS2Counter = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	if((Cx3_conversion_working_!=CHG_obcState)&&(Cx5_degradation_mode!=CHG_obcState)
				&&(FALSE!=CHG_Generalstop))
	{
		CHG_relayS2state = FALSE;
		OpenS2Counter = 0U;
	}
	else if(Cx3_Euro_fast_charging==CHG_OBC_chargingMode)
	{
		CHG_relayS2_fast(CHG_ElockState, CHG_BMS_OBCenable);
		OpenS2Counter = 0U;
	}
	else if(Cx1_slow_charging==CHG_OBC_chargingMode)
	{
		CHG_relayS2_slow(CHG_ElockState,CHG_contactorsState,
										 CHG_Generalstop, RequestHWStop, &OpenS2Counter);
	}
	else
	{
		CHG_relayS2state = FALSE;
		OpenS2Counter = 0U;
	}

	(void)Rte_Write_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(CHG_relayS2state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}
static void CHG_relayS2_slow(IdtOutputELockSensor CHG_ElockState, BMS_MainConnectorState CHG_contactorsState,
						 boolean CHG_Generalstop, boolean RequestHWStop, uint8 *OpenS2Counter)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static boolean OpenS2Pending = FALSE;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(((Cx3_conversion_working_==CHG_obcState)||(Cx5_degradation_mode==CHG_obcState))
			&&(FALSE!=CHG_Generalstop)
			&&(FALSE!=RequestHWStop))
	{
		OpenS2Pending=TRUE;
	}
	else if((ELOCK_LOCKED==CHG_ElockState)
		  &&(Cx2_contactors_closed==CHG_contactorsState)
		  &&(FALSE==CHG_Generalstop))
	{
		CHG_relayS2state=TRUE;
		/*Cancel S2 closing*/
		OpenS2Pending = FALSE;
	}
	else
	{
		/*MISRA*/
	}
	

	/*Open delay*/
	if(NULL_PTR==OpenS2Counter)
	{
		/*Not expected path.*/
		CHG_relayS2state = FALSE;
	}
	else if(FALSE==OpenS2Pending)
	{
		(*OpenS2Counter) = 0U;
	}
	else if(CHG_S2OPEN_DELAY<=*OpenS2Counter)
	{
		/*Open S2*/
		CHG_relayS2state = FALSE;
		OpenS2Pending = FALSE;
		(*OpenS2Counter) = 0U;
	}
	else
	{
		(*OpenS2Counter)++;
	}


}
static void CHG_relayS2_fast(IdtOutputELockSensor CHG_ElockState,
						BMS_OnBoardChargerEnable CHG_BMS_OBCenable)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	if((FALSE==CHG_PLC_data.OBCready)/*Regular Stop*/
			||(FALSE!=CHG_PLC_data.EmergencyShutdown))/*Emergency shutdown*/
	{
		CHG_relayS2state=FALSE;
	}
	else if((ELOCK_LOCKED==CHG_ElockState)
		    &&(FALSE!=CHG_BMS_OBCenable)
			&&(CHG_PLC_READY==CHG_PLC_data.MachineState))
	{
		CHG_relayS2state=TRUE;
	}
	else
	{
		/*MISRA*/
	}
}

static void CHG_OBCstate(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
												 boolean LaunchAllowed, boolean CHG_Generalstop, boolean GoToError)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	switch(CHG_obcState)
	{

		case Cx0_off_mode:
			CHG_OBCstate_off(CHG_OBC_ConnectionConfirmation);
			break;

		case Cx1_Init_mode:
			CHG_OBCstate_init(LaunchAllowed,GoToError);
			break;

		case Cx2_standby_mode:
			CHG_OBCstate_standby(CHG_OBC_ConnectionConfirmation,LaunchAllowed,CHG_Generalstop,GoToError);
			break;

		case Cx3_conversion_working_:
			CHG_OBCstate_conversion(CHG_Generalstop);
			break;

		case Cx5_degradation_mode:
			CHG_OBCstate_degradation(CHG_Generalstop);
			break;

		default:
		case Cx4_error_mode:
			CHG_OBCstate_error(GoToError);
			break;
	}

	(void)Rte_Write_PpInt_OBC_Status_OBC_Status(CHG_obcState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}
static boolean CHG_LaunchCondition(IdtOutputELockSensor CHG_ElockState,
																	 BMS_OnBoardChargerEnable CHG_BMS_OBCenable,
																	 BMS_MainConnectorState CHG_contactorsState,
																	 OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean launchAllowed;


	if((ELOCK_LOCKED==CHG_ElockState)
			&&(FALSE!=CHG_BMS_OBCenable)
			&&(Cx2_contactors_closed==CHG_contactorsState)
			&&(FALSE!=CHG_relayS2state)
			&&(Cx2_full_connected==CHG_OBC_ConnectionConfirmation)
		)
	{
		launchAllowed = TRUE;
	}
	else
	{
		launchAllowed = FALSE;
	}

	return launchAllowed;
}
static void CHG_OBCstate_off(OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	if(Cx2_full_connected==CHG_OBC_ConnectionConfirmation)
	{
		CHG_obcState = Cx1_Init_mode;
	}

}
static void CHG_OBCstate_init(boolean LaunchAllowed, boolean GoToError)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	OBC_Fault FaultLeve;


	(void)Rte_Read_PpInt_OBC_Fault_OBC_Fault(&FaultLeve);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((OBC_FAULT_LEVEL_1==FaultLeve)
		||(FALSE!=GoToError))
	{
		CHG_obcState = Cx4_error_mode;
	}
	else if(FALSE!=LaunchAllowed)
	{
		CHG_obcState = Cx3_conversion_working_;
	}
	else
	{
	/*MISRA do not change state.*/
	}

}
static void CHG_OBCstate_standby( OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation,
																	boolean LaunchAllowed, boolean CHG_Generalstop, boolean GoToError)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	OBC_Fault FaultLeve;


	(void)Rte_Read_PpInt_OBC_Fault_OBC_Fault(&FaultLeve);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((OBC_FAULT_LEVEL_1==FaultLeve)
		||(FALSE!=GoToError))
	{
		CHG_obcState = Cx4_error_mode;
	}
	else if(Cx2_full_connected!=CHG_OBC_ConnectionConfirmation)
	{
		CHG_obcState = Cx0_off_mode;
	}
	else if((FALSE!=LaunchAllowed)&&(FALSE==CHG_Generalstop))
	{
		CHG_obcState = Cx3_conversion_working_;
	}
	else
	{
	/*MISRA do not change state.*/
	}
}
static void CHG_OBCstate_conversion(boolean CHG_Generalstop)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean CHG_Derating;

	(void)Rte_Read_PpOBCDerating_DeOBCDerating(&CHG_Derating);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FALSE!=CHG_Generalstop)
			&&(FALSE==CHG_relayS2state))
	{
		CHG_obcState = Cx2_standby_mode;
	}
	else if(FALSE!=CHG_Derating)
	{
		CHG_obcState = Cx5_degradation_mode;
	}
	else
	{
	/*MISRA do not change state.*/
	}
}
static void CHG_OBCstate_degradation(boolean CHG_Generalstop)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean CHG_Derating;


	(void)Rte_Read_PpOBCDerating_DeOBCDerating(&CHG_Derating);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((FALSE!=CHG_Generalstop)
			&&(FALSE==CHG_relayS2state))
	{
		CHG_obcState = Cx2_standby_mode;
	}
	else if(FALSE==CHG_Derating)
	{
		CHG_obcState = Cx3_conversion_working_;
	}
	else
	{
	/*MISRA do not change state.*/
	}
}
static void CHG_OBCstate_error(boolean GoToError)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_Fault FaultLeve;


	(void)Rte_Read_PpInt_OBC_Fault_OBC_Fault(&FaultLeve);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((OBC_FAULT_LEVEL_1!=FaultLeve)
		&&(FALSE==GoToError))
	{
		CHG_obcState = Cx2_standby_mode;
	}


}
static void CHG_InputVoltageMode(OBC_ChargingMode CHG_OBC_chargingMode,
																 const uint16 CHG_RMSphase[CHG_NUM_PHASES],
																OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static IdtInputVoltageMode PreviousChatgingMode = IVM_NO_INPUT_DETECTED;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	if(Cx1_not_connected==CHG_OBC_ConnectionConfirmation)
	{
		PreviousChatgingMode = IVM_NO_INPUT_DETECTED;
	}


	if(Cx2_standby_mode==CHG_obcState)
	{
		/*The relay has been open*/
		CHG_InVlotMode = IVM_NO_INPUT_DETECTED;
	}
	else if((Cx3_conversion_working_==CHG_obcState)||(Cx5_degradation_mode==CHG_obcState))
	{
		if((Cx1_slow_charging==CHG_OBC_chargingMode)
			&&(IVM_NO_INPUT_DETECTED==CHG_InVlotMode))
		{
			/*Detection*/
			CHG_InVoltMode_identify(CHG_RMSphase);

			if(IVM_NO_INPUT_DETECTED!=CHG_InVlotMode)
			{
				/*New state has been identify*/
				if((IVM_NO_INPUT_DETECTED!=PreviousChatgingMode)
					/*Exists a previous chaging mode*/
					&&(PreviousChatgingMode!=CHG_InVlotMode))
				{
					/*A different charging mode has been detected*/
					CHG_InVlotMode = IVM_NO_INPUT_DETECTED;
				}
				else
				{
					/*Voltage mode accepted*/
					PreviousChatgingMode = CHG_InVlotMode;
				}
			}
		}
	}
	else
	{
		/*MISRA*/
	}
	(void)Rte_Write_PpInputVoltageMode_DeInputVoltageMode(CHG_InVlotMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}
static void CHG_InVoltMode_identify(const uint16 CHG_RMSphase[CHG_NUM_PHASES])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 CounterMono = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input Variables*/
	boolean CHG_L1inL1;
	boolean CHG_L1inN;


	(void)Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(&CHG_L1inL1);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(&CHG_L1inN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/*Monophasic detection*/
    if((FALSE!=CHG_checkValuesInRange(CHG_RMSphase,CHG_ThresholdNoAC,0U))
				&&((TRUE==CHG_L1inL1)||(TRUE==CHG_L1inN)))
	{
		CounterMono++;
	}
	else
	{
		CounterMono=0U;
	}

	/*Debounce*/
	if(CHG_DELAY_200ms<CounterMono)
	{
		CHG_InVlotMode=IVM_MONOPHASIC_INPUT;
		CounterMono=0U;
	}

	/*Triphasic detection*/

	if(FALSE!=CHG_checkTriphasicCondition(CHG_RMSphase))
	{
		CHG_InVlotMode=IVM_TRIPHASIC_INPUT;
		CounterMono=0U;
	}

}
static boolean CHG_checkTriphasicCondition(const uint16 CHG_RMSphase[CHG_NUM_PHASES])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Input*/
	uint16 CHG_PeakPhase[CHG_NUM_PHASES];
	uint16 CHG_Freq[CHG_NUM_PHASES];

	/*Local*/
	uint16 CHG_RMSline[CHG_NUM_PHASES];
	uint16 CHG_Peakline[CHG_NUM_PHASES];
	uint8 index;

	boolean TriConditionRMS;
	boolean TriConditionPeak;
	boolean TriConditionFreq;

	/*Output*/
	boolean ReturnValue;


	(void)Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(&CHG_PeakPhase[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(&CHG_PeakPhase[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(&CHG_PeakPhase[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	(void)Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(&CHG_Freq[0]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(&CHG_Freq[1]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(&CHG_Freq[2]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	CHG_PhaseToLine(CHG_RMSphase,&CHG_RMSline);
	CHG_PhaseToLine(CHG_PeakPhase,&CHG_Peakline);

	for(index=0U;index<CHG_NUM_PHASES;index++)
	{
		/*Convert Peak Values to RMS to use the same Range*/
		CHG_Peakline[index] = (uint16)(((uint32)CHG_Peakline[index]*(uint32)CHG_SQRT2_gain)>>CHG_SQRT2_Q);
	}

	TriConditionRMS = CHG_checkValuesInRange(CHG_RMSline,CHG_Vrange_220_max,CHG_Vrange_220_min)
						| CHG_checkValuesInRange(CHG_RMSline,CHG_Vrange_110_max,CHG_Vrange_110_min);
	TriConditionPeak = CHG_checkValuesInRange(CHG_Peakline,CHG_Vrange_220_max,CHG_Vrange_220_min)
						| CHG_checkValuesInRange(CHG_Peakline,CHG_Vrange_110_max,CHG_Vrange_110_min);
	TriConditionFreq = CHG_checkValuesInRange(CHG_Freq,CHG_FREQ_MAX,CHG_FREQ_MIN);

	if((FALSE!=TriConditionRMS)
		&&(FALSE!=TriConditionPeak)
		&&(FALSE!=TriConditionFreq))
	{
		ReturnValue = TRUE;
	}
	else
	{
		ReturnValue = FALSE;
	}

	return ReturnValue;
}

static boolean CHG_checkValuesInRange(const uint16 CHG_InValue[CHG_NUM_PHASES],uint16 MaxValue, uint16 MinValue)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean InRange = TRUE;
	uint8 index;


	for(index=0U;index<CHG_NUM_PHASES;index++)
	{
		if((CHG_InValue[index]>MaxValue)
				||(CHG_InValue[index]<MinValue))
		{
			InRange=FALSE;
		}
	}
	return InRange;
}
static CHG_VoltRange_t CHG_InstantaneousVoltagestate(const uint16 CHG_RMSphase[CHG_NUM_PHASES])
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	/*Local variables*/
	uint16 CHG_RMSline[CHG_NUM_PHASES];
	boolean Detection220;
	boolean Detection110;
	boolean Disconnection;

	/*Output*/
	CHG_VoltRange_t retVal;


	Disconnection=CHG_checkValuesInRange(CHG_RMSphase,CHG_InputVoltageThreshold,0U);

	if((IVM_MONOPHASIC_INPUT==CHG_InVlotMode)
		||(IVM_TRIPHASIC_INPUT==CHG_InVlotMode))
	{

		if(IVM_MONOPHASIC_INPUT==CHG_InVlotMode)
		{
			/*Monophasic*/
			CHG_RMSline[0]=CHG_RMSphase[0];
			CHG_RMSline[1]=CHG_RMSphase[0];
			CHG_RMSline[2]=CHG_RMSphase[0];
		}
		else
		{
			/*Triphasic*/
			/*Reduce halsted.*/
			CHG_PhaseToLine(CHG_RMSphase,&CHG_RMSline);
		}

		Detection220=CHG_checkValuesInRange(CHG_RMSline,CHG_Vrange_220_max,CHG_Vrange_220_min);
		Detection110=CHG_checkValuesInRange(CHG_RMSline,CHG_Vrange_110_max,CHG_Vrange_110_min);

		if(FALSE!=Detection220)
		{
			retVal.ACrange = Cx3_220V_170_265V_;
			retVal.VoltageSt=Cx1_220V_AC_connected;
		}
		else if(FALSE!=Detection110)
		{
			retVal.ACrange = Cx1_110V_85_132V_;
			retVal.VoltageSt=Cx1_220V_AC_connected;
		}
		else if(FALSE!=Disconnection)
		{
			retVal.ACrange = Cx0_No_AC_10V_;
			retVal.VoltageSt=Cx0_No_220V_AC;
		}
		else
		{
			retVal.ACrange = Cx2_Invalid;
			retVal.VoltageSt=Cx3_Invalid_value;
		}

	}
	else if(FALSE!=Disconnection)
	{
		retVal.ACrange = Cx0_No_AC_10V_;
		retVal.VoltageSt=Cx0_No_220V_AC;
	}
	else
	{
		retVal.ACrange = Cx2_Invalid;
		retVal.VoltageSt=Cx3_Invalid_value;
	}


	return retVal;
}
static void CHG_InputVoltageState(const uint16 CHG_RMSphase[CHG_NUM_PHASES])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 DebounceCounter=0U;
	static CHG_VoltRange_t Previous={Cx0_No_220V_AC,Cx0_No_AC_10V_};
	static OBC_InputVoltageSt VoltState_Previous=Cx0_No_220V_AC;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	CHG_VoltRange_t CurrentState;


	/*Detect Current State*/
	CurrentState =  CHG_InstantaneousVoltagestate(CHG_RMSphase);
	Current_VoltageSt = CurrentState.VoltageSt;

	/*Debounce*/
	if(Previous.ACrange!=CurrentState.ACrange)
	{
		DebounceCounter=0U;
	}
	else if(CHG_AC_CONFIRM_TIME>DebounceCounter)
	{
		DebounceCounter++;
	}
	else
	{
		/*Misra*/
	}

	Previous = CurrentState;

	if(CHG_AC_CONFIRM_TIME<=DebounceCounter)
	{
		/*Update state*/
		VoltState = CurrentState.VoltageSt;
		CHG_InVoltRange = CurrentState.ACrange;
	}

	/*Disconnection state*/
	if((Cx0_No_220V_AC==VoltState)
			&&(Cx0_No_220V_AC!=VoltState_Previous))
	{
		VoltState = Cx2_220V_AC_disconnected;
		VoltState_Previous = Cx0_No_220V_AC;
	}
	else
	{
		VoltState_Previous = VoltState;
	}


	(void)Rte_Write_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(VoltState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_OBC_ACRange_OBC_ACRange(CHG_InVoltRange);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}
static void CHG_PhaseToLine(const uint16 CHG_RMSphase[CHG_NUM_PHASES],uint16 (*CHG_RMSline)[CHG_NUM_PHASES])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(NULL_PTR != CHG_RMSline)
	{
		(*CHG_RMSline)[0]=(uint16)(((uint32)CHG_RMSphase[0]*(uint32)CHG_SQRT3_gain)>>CHG_SQRT3_Q);
		(*CHG_RMSline)[1]=(uint16)(((uint32)CHG_RMSphase[1]*(uint32)CHG_SQRT3_gain)>>CHG_SQRT3_Q);
		(*CHG_RMSline)[2]=(uint16)(((uint32)CHG_RMSphase[2]*(uint32)CHG_SQRT3_gain)>>CHG_SQRT3_Q);
	}
}


static void CHG_fast_task(boolean GeneralStop, boolean FaultDetected, OBC_ChargingMode CHG_OBC_chargingMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */



	CHG_fast_UpdateChargeState();
	CHG_fast_UpsateCPlineState();
	CHG_fast_UnexpectedDisconection();
	CHG_fast_EndCharge_RTAB();
	CHG_fast_UpdateStateMachien(GeneralStop,FaultDetected,CHG_OBC_chargingMode);
	CHG_fast_UpdateOutputs();

	(void)Rte_Write_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(CHG_PLC_data.EVSEpowerLimit);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}


static void CHG_fast_UpdateChargeState(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	(void)Rte_Read_PpInt_BMS_SOC_BMS_SOC(&CHG_PLC_data.EVressSoc);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(CHG_PER_THOUSAND_MAX<=CHG_PLC_data.EVressSoc)
	{
		CHG_PLC_data.ChargeComplete = TRUE;
		CHG_PLC_data.BulkChargingComplete = TRUE;
	}
	else if(CHG_PER_THOUSAND_BULK<=CHG_PLC_data.EVressSoc)
	{
		CHG_PLC_data.ChargeComplete = FALSE;
		CHG_PLC_data.BulkChargingComplete = TRUE;
	}
	else
	{
		CHG_PLC_data.ChargeComplete = FALSE;
		CHG_PLC_data.BulkChargingComplete = FALSE;
	}
}
static void CHG_fast_UpsateCPlineState(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_ChargingMode ChargingMode;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&ChargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	if(FALSE != CHG_relayS2state)
	{
		CHG_PLC_data.EVReady = TRUE;
		CHG_PLC_data.CPlineState = Scc_ControlPilotState_State_C_D;
	}
	else
	{
		CHG_PLC_data.EVReady = FALSE;
		CHG_PLC_data.CPlineState = Scc_ControlPilotState_State_B;
	}

	if(Cx3_Euro_fast_charging==ChargingMode)
	{
		CHG_PLC_data.HLCstate =  Scc_PWMState_HLCOnly;
	}
	else if((CHG_PLC_STANDBY != CHG_PLC_data.MachineState)
			&&(CHG_PLC_IDLE != CHG_PLC_data.MachineState)
			&&(CHG_PLC_FIRMWARE != CHG_PLC_data.MachineState))
	{
		CHG_PLC_data.HLCstate = Scc_PWMState_HLCOptional;
	}
	else
	{
		CHG_PLC_data.HLCstate = Scc_PWMState_ChargingNotAllowed;
	}

	if((Cx3_Euro_fast_charging!=ChargingMode)
			&&(CHG_PLC_FIRMWARE!=CHG_PLC_data.MachineState)
			&&(CHG_PLC_IDLE!=CHG_PLC_data.MachineState)
			&&(CHG_PLC_STANDBY!=CHG_PLC_data.MachineState)
			&&(CHG_PLC_RST_ERROR!=CHG_PLC_data.MachineState))
	{
		/*Emergency shutdown*/
		CHG_PLC_data.EmergencyShutdown = TRUE;
		CHG_PLC_data.EVSEstopReq = TRUE;
	}

}
static void CHG_fast_UnexpectedDisconection(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation_previous = Cx0_invalid_value;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_ChargingConnectionConfirmati CHG_OBC_ConnectionConfirmation;


	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&CHG_OBC_ConnectionConfirmation);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((Cx2_full_connected!=CHG_OBC_ConnectionConfirmation)
			&&(Cx2_full_connected==CHG_OBC_ConnectionConfirmation_previous)
			&&(CHG_PLC_FIRMWARE!=CHG_PLC_data.MachineState)
			&&(CHG_PLC_IDLE!=CHG_PLC_data.MachineState)
			&&(CHG_PLC_STANDBY!=CHG_PLC_data.MachineState))
	{
		/*Unexpected disconnection*/
		CHG_PLC_data.RSTrequest = TRUE;
	}

	CHG_OBC_ConnectionConfirmation_previous = CHG_OBC_ConnectionConfirmation;

}
static boolean CHG_fast_FirmwareDownloadAllow(void)
{
	OBC_ChargingMode CHG_OBC_chargingMode;

	boolean RetVal;
	DCLV_DCLVStatus DCLVstate;

	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&CHG_OBC_chargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(&DCLVstate);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((Cx0_no_charging==CHG_OBC_chargingMode)
			&&(Cx1_RUN!=DCLVstate)
			&&(Cx5_DERATED!=DCLVstate))
	{
		/*Firmware is allow to be download*/
		RetVal = TRUE;
	}
	else if(Cx3_Euro_fast_charging==CHG_OBC_chargingMode)
	{
		/*Firmware can be download if it is needed.*/
		RetVal = TRUE;
	}
	else
	{
		RetVal = FALSE;
	}

	return RetVal;
}
static void CHG_fast_UpdateStateMachien(boolean GeneralStop, boolean FaultDetected, OBC_ChargingMode CHG_OBC_chargingMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	switch (CHG_PLC_data.MachineState)
	{

		case CHG_PLC_FIRMWARE:
			CHG_fast_State_Firmware();
			break;

		case CHG_PLC_IDLE:
			CHG_fast_State_Idle();
			break;

		case CHG_PLC_STANDBY:
			CHG_fast_State_Standby(FaultDetected,CHG_OBC_chargingMode);
			break;

		case CHG_PLC_INIT:
			CHG_fast_State_Init();
			break;

		case CHG_PLC_READY:
			CHG_fast_State_Ready();
			break;

		case CHG_PLC_PRECHARGE:
			CHG_fast_State_PreCharge(GeneralStop);
			break;

		case CHG_PLC_PRECHARGE_COMPLETE:
			CHG_fast_State_PreChrageComplete(GeneralStop);
			break;

		case CHG_PLC_CHARGE:
			CHG_fast_State_Charge(GeneralStop);
			break;

		case CHG_PLC_SOFT_STOP:
			CHG_fast_State_SoftStop();
			break;

		case CHG_PLC_EMERGENCY:
			CHG_fast_State_Emergency();
			break;

		case CHG_PLC_POWER_OFF:
			CHG_fast_State_PoweOff();
			break;

		case CHG_PLC_STOP:
			CHG_fast_State_Stop();
			break;

		case CHG_PLC_CONTACTOR:
			CHG_fast_State_Contactor();
			break;

		default:
		case CHG_PLC_RST_ERROR:
			CHG_fast_State_Error();
			break;
	}
}
static void CHG_fast_State_Firmware(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 Timeout = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static boolean FirstTime = TRUE;


	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	EthTrcv_30_Ar7000_ReturnType retVal = ETHTRCV_30_AR7000_RT_PENDING;/* PRQA S 2981 #Variable Initalised for better understanding*/


	if(FALSE!=FirstTime)
	{
		(void)Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(COMM_FULL_COMMUNICATION);
		CHG_resetPLC();
		Timeout=0U;
		FirstTime = FALSE;
	}
	else if(600U<=Timeout)
	{
		CHG_PLC_data.FirwareDownloadReq = FALSE;
		Timeout=0U;
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		CHG_resetPLC();
	}
	else if(TRUE==CHG_PLC_data.FirwareDownloadReq)
	{
		retVal = EthTrcv_30_Ar7000_TriggerDwld(0);

		if(ETHTRCV_30_AR7000_RT_NOT_OK==retVal)
		{
			/*Wait for a new FW download req*/
			CHG_PLC_data.FirwareDownloadReq = FALSE;
			Timeout=0U;
			CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
			CHG_resetPLC();
		}
		else if(ETHTRCV_30_AR7000_RT_OK==retVal)
		{
			CHG_PLC_data.FirwareDownloadReq = FALSE;
			CHG_PLC_data.MachineState = CHG_PLC_IDLE;
			Timeout=0U;
		}
		else
		{
			/*Pending*/
			Timeout++;
		}
	}
	else
	{
		Timeout++;
	}
}
static void CHG_fast_State_Idle(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 buffer[100] = {0U};
	static uint16 MinTime = 0U;
	static uint16 TimeOut = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */
	static EthTrcv_30_Ar7000_ReturnType PLC_firmwareOK = ETHTRCV_30_AR7000_RT_NOT_OK;

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */


	if(FALSE!=CHG_PLC_data.FirwareDownloadReq)
	{
		/*Unexpected*/
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		PLC_firmwareOK = ETHTRCV_30_AR7000_RT_NOT_OK;
		MinTime = 0U;
		TimeOut = 0U;
	}
	else if(200U<=TimeOut)
	{
		/*it usually takes 800ms*/
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		CHG_PLC_data.RSTrequest = TRUE;
		PLC_firmwareOK = ETHTRCV_30_AR7000_RT_NOT_OK;
		MinTime = 0U;
		TimeOut = 0U;
	}
	else if(ETHTRCV_30_AR7000_RT_OK!=PLC_firmwareOK)
	{
		/*Read version until OK*/
		MinTime=0U;
		TimeOut ++;
		PLC_firmwareOK = EthTrcv_30_Ar7000_GetSwVersion(0,buffer);
	}
	else if(600U<=MinTime)
	{
		CHG_PLC_data.MachineState = CHG_PLC_STANDBY;
		PLC_firmwareOK = ETHTRCV_30_AR7000_RT_NOT_OK;
		MinTime = 0U;
		TimeOut = 0U;
	}
	else
	{
		MinTime++;
	}

}
static void CHG_fast_State_Standby(boolean FaultDetected, OBC_ChargingMode CHG_OBC_chargingMode)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean PlantMode;


	(void)Rte_Read_PpPlantModeState_DePlantModeState(&PlantMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if(FALSE==CHG_fast_FirmwareDownloadAllow())
	{
		/*Don't leave this state*/
		/*Charge not allow*/
		/*Fw download not allow*/
	}
	else if(FALSE!=CHG_PLC_data.FirwareDownloadReq)
	{
		/*Unexpected, wait until MODE 4 for downloading the FW*/
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
	}
	else if((Cx3_Euro_fast_charging==CHG_OBC_chargingMode)
			&&((Cx1_Init_mode==CHG_obcState)||(Cx2_standby_mode==CHG_obcState))
			&&(FALSE==FaultDetected)
			&&(FALSE==PlantMode))
	{
		CHG_PLC_data.MachineState = CHG_PLC_INIT;
	}
	else
	{
		/*Misra*/
	}

}
static void CHG_fast_State_Init(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	if((FALSE != CHG_PLC_data.EVSEstopReq)
			&&(Scc_SAPSchemaIDs_ISO==CHG_PLC_data.Schema))
	{
		/* Only way for stopping while parameter exchange in ISO is like an emergency shutdown*/
		CHG_PLC_data.EmergencyShutdown = TRUE;
	}


	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Close TCP.*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
	}
	else if(FALSE != CHG_PLC_data.EVSEstopReq)
	{
		/*Try to stop*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
	}
	else if(Scc_StateMachineStatus_CableCheck==CHG_PLC_data.SCCstateRes)
	{
		CHG_PLC_data.MachineState = CHG_PLC_READY;
	}
	else
	{
		/*Misra*/
	}


	/*Detect a SLAC fail*/
	CHG_SlacFailDetection();

}
static void CHG_SlacFailDetection(void)
{

	/* - Static non-init variables declaration ---------- */
		#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		/* Static non-init variables HERE... */

		#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


		/* - Static zero-init variables declaration --------- */
		#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		/* Static zero-init variables HERE... */
		static uint8 RetryNum = 0U;
		static uint16 SlacTimeout = 0U;
		static Scc_StateM_StateMachineStatusType Scc_state_delayed = Scc_StateMachineStatus_None;

		#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


		/* - Static init variables declaration -------------- */
		#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

		/* Static init variables HERE... */

		#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
		#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


		/* - Automatic variables declaration ---------------- */
		/* Automatic variables HERE... */


	/*SCC state will go to error, so we have to compare with the previous one*/
	if(Scc_StateMachineStatus_Error_Stopped!=CHG_PLC_data.SCCstateRes)
	{
		Scc_state_delayed = CHG_PLC_data.SCCstateRes;
	}


	/*Some times the TPC comunication is not stating and the SCC is discovering the portocol*/
	if(Scc_StateMachineStatus_SECCDiscoveryProtocol<Scc_state_delayed)
	{
		/*Slac and TCP opened OK*/
		RetryNum = 0U;
		SlacTimeout = 0U;
	}
	else if(FALSE != CHG_PLC_data.EVSEstopReq)
	{
		/*OverWrite the CHG status, go strait to rst error*/
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		CHG_PLC_data.Retry = TRUE;
		SlacTimeout = 0U;
		RetryNum ++;
	}
	else if((200U<=SlacTimeout)&&(ETHTRCV_30_AR7000_SLAC_STATE_PARM_DISCOVERY_REQ==CHG_PLC_data.SLACstatus))
	{
		/*Slac not started, some times with a retry is enough*/
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		CHG_PLC_data.Retry = TRUE;
		SlacTimeout = 0U;
		RetryNum ++;
	}
	else if(1400U<=SlacTimeout)
	{
		/*the PLC is stuck*/
		/*Or the TCP is taking to long for starting, just try again is enough*/
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		CHG_PLC_data.Retry = TRUE;
		SlacTimeout = 0U;
		RetryNum ++;
	}
	else
	{
		SlacTimeout++;
	}

	if((6U<=RetryNum)||(FALSE!=CHG_PLC_data.RSTrequest))
	{
		/*Slac not started, the PLC is stuck*/
		/*PLC is going to be rst due to an unexpected disconnection*/
		CHG_PLC_data.RSTrequest = TRUE;
		RetryNum = 0U;
	}

}
static void CHG_fast_State_Ready(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	if((FALSE != CHG_PLC_data.EVSEstopReq)
			&&(Scc_SAPSchemaIDs_ISO==CHG_PLC_data.Schema))
	{
		/* Only way for stopping while parameter exchange in ISO is like an emergency shutdown*/
		CHG_PLC_data.EmergencyShutdown = TRUE;
	}

	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Close TCP.*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
	}
	else if(FALSE != CHG_PLC_data.EVSEstopReq)
	{
		/*Try to stop*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
	}
	else if(FALSE!=CHG_relayS2state)/*S2 has been close*/
	{
		/*Wait until charge allow -> S2 -> state C_D -> CableCheck -> Pre Charge*/
		CHG_PLC_data.MachineState = CHG_PLC_PRECHARGE;
	}
	else
	{
		/*Misra*/
	}
}
static void CHG_fast_State_PreCharge(boolean GeneralStop)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	BMS_MainConnectorState MainConState;
	uint16 RelayVoltage;
	uint16 BatteryVolt;
	sint32 Result;


	(void)Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(&MainConState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(&RelayVoltage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(&BatteryVolt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	Result = (sint32)RelayVoltage-(sint32)BatteryVolt;

	if((FALSE != GeneralStop)
			&&(Scc_SAPSchemaIDs_ISO==CHG_PLC_data.Schema))
	{
		/* Only way for stopping while pre-charge in ISO is like an emergency shutdown*/
		CHG_PLC_data.EmergencyShutdown = TRUE;
	}

	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Close TCP.*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
	}
	else if(FALSE != GeneralStop)
	{
		/*Try to stop*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
	}
	else if((Scc_StateMachineStatus_PreCharge==CHG_PLC_data.SCCstateRes)
			&&(CHG_PRECHARGE_COMPLETE>Result)
			&&((sint32)-CHG_PRECHARGE_COMPLETE<Result)
			&&(Cx2_contactors_closed==MainConState))
	{
		CHG_PLC_data.MachineState = CHG_PLC_PRECHARGE_COMPLETE;
	}
	else
	{
		/*Misra*/
	}
}
static void CHG_fast_State_PreChrageComplete(boolean GeneralStop)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	BMS_QuickChargeConnectorState QuickConState;


	(void)Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(&QuickConState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/



	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Close TCP.*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
	}
	else if(FALSE!=GeneralStop)
	{
		/*Stop sequence*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
	}
	else if((Scc_StateMachineStatus_PowerDelivery==CHG_PLC_data.SCCstateRes) && (FALSE!=QuickConState) && ((Cx3_conversion_working_==CHG_obcState)||(Cx5_degradation_mode==CHG_obcState)))
	{
		CHG_PLC_data.MachineState = CHG_PLC_CHARGE;
	}
	else
	{
		/*Misra*/
	}

}
static void CHG_fast_State_Charge(boolean GeneralStop)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Close TCP.*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
	}
	else if(FALSE!=CHG_PLC_data.EVSEstopReq)
	{
		/*Stop Requested by EVSE*/
		CHG_PLC_data.MachineState = CHG_PLC_SOFT_STOP;
	}
	else if(FALSE!=GeneralStop)
	{
		CHG_PLC_data.MachineState = CHG_PLC_SOFT_STOP;
	}
	else
	{
		/*Misra*/
	}
}
static void CHG_fast_State_SoftStop(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 TimeOut = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Close TCP.*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
		TimeOut = 0U;
	}
	else if(CHG_FAST_I_STOP>CHG_PLC_data.EVSEpresentCurrent)
	{
		/*Stop current reached*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
		TimeOut = 0U;
	}
	else if((CHG_FAST_I_STOP_MIN>CHG_PLC_data.EVSEpresentCurrent)&&(CHG_MODE4_SOFTSTOP_TIMEOUT<TimeOut))
	{
		/*Min acceptable current reached and timeout finished*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
		TimeOut = 0U;
	}
	else if(CHG_MODE4_SOFTSTOP_TIMEOUT_FORCE<TimeOut)
	{
		/*TimeOut end*/
		CHG_PLC_data.MachineState = CHG_PLC_POWER_OFF;
		TimeOut = 0U;
	}
	else
	{
		TimeOut++;
	}
}
static void CHG_fast_State_Emergency(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 TimeOut = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	if(Scc_StateMachineStatus_SessionStop==CHG_PLC_data.SCCstateRes)
	{
		/*TCP will be closed after.*/
		CHG_PLC_data.MachineState = CHG_PLC_STOP;
		TimeOut=0U;
	}
	else if(1U<TimeOut)
	{
		/*TCP not closed*/
		(void)TcpIp_Close(Scc_V2GSocket, FALSE);
		CHG_PLC_data.MachineState = CHG_PLC_STOP;
		TimeOut=0U;
	}
	else
	{
		TimeOut++;
	}


}
static void CHG_fast_State_PoweOff(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 TimeOut = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	if(FALSE!=CHG_PLC_data.EmergencyShutdown)
	{
		/*Not expected fail while PowerDelivery OFF or SessionStop*/
		CHG_PLC_data.MachineState = CHG_PLC_EMERGENCY;
		TimeOut = 0U;
	}
	else if(FALSE!=CHG_PLC_data.PowerOFF)
	{
		/*Wait for one response before open S2*/
		CHG_PLC_data.MachineState = CHG_PLC_STOP;
		TimeOut = 0U;
	}
	else if(250U>TimeOut)
	{
		TimeOut++;
	}
	else
	{
		/*Open S2 after 2.5seg*/
		CHG_PLC_data.MachineState = CHG_PLC_STOP;
		TimeOut = 0U;
	}
}
static void CHG_fast_State_Stop(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 MinTime = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */

	if(FALSE!=CHG_relayS2state)

	{
		/*Wait until S2 is opened*/
		MinTime = 0U;
	}
	else if(CHG_MODE4_CONTACTORS_DELAY<MinTime)
	{
		/*TimeOut waiting for stop confirmation*/
		CHG_PLC_data.MachineState = CHG_PLC_CONTACTOR;
		MinTime = 0U;
	}
	else
	{
		MinTime++;
	}
}
static void CHG_fast_State_Contactor(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 TimeOut = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	BMS_QuickChargeConnectorState QuickConState;
	(void)Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(&QuickConState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((Scc_TCPSocketState_Closed==CHG_PLC_data.TCPstate)
			&&(FALSE==QuickConState))
	{
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		TimeOut = 0U;
	}
	else if(100U<TimeOut)
	{
		CHG_PLC_data.MachineState = CHG_PLC_RST_ERROR;
		TimeOut = 0U;
		/*The TCP is closed properly with the new SIP*/
		/*(void)TcpIp_Close(Scc_V2GSocket, TRUE);*/
	}
	else if(Scc_StateMachineStatus_StopCommunicationSession==CHG_PLC_data.SCCstateRes)
	{
		/*TCP is closed properly but the Scc_Cbk_TL_TCPEvent call back is not working.*/
		/*Close again the TCP for gettign the call back.*/
		/*Force TCP to be closed.*/
		/*The TCP is closed properly with the new SIP*/
		/*(void)TcpIp_Close(Scc_V2GSocket, TRUE);*/
		TimeOut++;
	}
	else
	{
		/*Wait for StopComunication session*/
		TimeOut++;
	}
}
static void CHG_fast_State_Error(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */
	static uint16 MinTime = 0U;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	uint8 CHG_SLACReset = EthTrcv_30_Ar7000_Slac_Reset(0);
	boolean CHG_FWDownloadAllow ;

	if(MinTime<100U)
	{
		MinTime++;
	}
	else if((Scc_StateMachineStatus_Initialized==CHG_PLC_data.SCCstateRes)
			&&((uint8)ETHTRCV_30_AR7000_RT_OK == CHG_SLACReset	))
	{
		MinTime=0U;
		if(FALSE!=CHG_PLC_data.FirwareDownloadReq)
		{
			CHG_PLC_data.MachineState = CHG_PLC_FIRMWARE;
			CHG_PLC_data.RSTrequest = FALSE;
		}
		else if(FALSE!=CHG_PLC_data.RSTrequest)
		{
			/*An unexpected error has been produced*/
			/*Get a new FW download request*/
			CHG_resetPLC();
			CHG_PLC_data.MachineState = CHG_PLC_FIRMWARE;
			CHG_PLC_data.RSTrequest = FALSE;
		}
		else if(FALSE!=CHG_PLC_data.Retry)
		{
			/*Retry as soon as possible*/
			CHG_PLC_data.MachineState = CHG_PLC_STANDBY;
		}
		else
		{
			/*Wait before going to standby*/
			CHG_PLC_data.MachineState = CHG_PLC_IDLE;
		}

		CHG_FWDownloadAllow = CHG_fast_FirmwareDownloadAllow();
		if((CHG_PLC_FIRMWARE==CHG_PLC_data.MachineState)
				&&(FALSE==CHG_FWDownloadAllow))
		{
			/*Not allow to downlad the FW*/
			CHG_PLC_data.MachineState = CHG_PLC_STANDBY;
		}

	}
	else
	{
		/* Only for MISRA reasons */
	}
}

static void CHG_fast_UpdateOutputs(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */
	const Scc_PhysicalValueType CHG_PhysicalValue_Zero = {0,0};
	BMS_Voltage BatteryVolt;
	boolean FWreq;
	boolean FWinProgress = FALSE;


	(void)Rte_Read_PpInt_BMS_Voltage_BMS_Voltage(&BatteryVolt);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	switch (CHG_PLC_data.MachineState)
	{
		case CHG_PLC_FIRMWARE:
			CHG_PLC_data.FunctionControl = Scc_FunctionControl_None;
			CHG_PLC_data.SCCstateReq = Scc_ChargingControl_None;
			FWinProgress = TRUE;
			break;

		case CHG_PLC_IDLE:
		case CHG_PLC_STANDBY:
			/*Do not clean the firmware download request*/
			FWreq = CHG_PLC_data.FirwareDownloadReq;
			CHG_fast_default_parameters();
			CHG_PLC_data.FirwareDownloadReq = FWreq;
			break;

		case CHG_PLC_INIT:
			CHG_PLC_data.FunctionControl = Scc_FunctionControl_ChargingMode;
			CHG_PLC_data.SCCstateReq = Scc_ChargingControl_NegotiateChargingParameters;
			CHG_PLC_data.MaxCurrent = CHG_PhysicalValueConvert(Rte_CData_CalMaxDiscoveryCurrent(),CHG_OFFSET_50,0);
			CHG_PLC_data.MaxVoltage = CHG_PhysicalValueConvert(Rte_CData_CalMaxDiscoveryVoltage(),CHG_OFFSET_200,0);
			break;

		case CHG_PLC_READY:
			CHG_PLC_data.OBCready = TRUE;
			CHG_PLC_data.TargetVoltage = CHG_PhysicalValueConvert(BatteryVolt,0U,0);
			break;

		case CHG_PLC_PRECHARGE:
			CHG_PLC_data.ForceElock = TRUE;
			CHG_PLC_data.MainConReq = Cx1_Request_to_Close_the_Contactors;
			CHG_PLC_data.TargetCurrent = CHG_PhysicalValueConvert(Rte_CData_CalTargetPrechargeCurrent(),0U,0);
			CHG_PLC_data.TargetVoltage = CHG_PhysicalValueConvert(BatteryVolt,0U,0);
			CHG_PLC_data.MaxCurrent = CHG_PhysicalValueConvert(Rte_CData_CalMaxPrechargeCurrent() ,CHG_OFFSET_50,0);
			CHG_PLC_data.MaxVoltage = CHG_PhysicalValueConvert(Rte_CData_CalMaxPrechargeVoltage(),CHG_OFFSET_200,0);
			break;

		case CHG_PLC_PRECHARGE_COMPLETE:
			CHG_PLC_data.SCCstateReq = Scc_ChargingControl_PreChargeCompleted;
			CHG_PLC_data.QuickConReq = Cx1_Request_to_Close_the_Contactors;
			break;

		case CHG_PLC_CHARGE:
			CHG_PLC_data.SCCstateReq = Scc_ChargingControl_StartPowerDelivery;
			CHG_PLC_data.ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_START;
			CHG_PLC_data.TargetCurrent = CHG_Fast_CurrentTarget();
			CHG_PLC_data.TargetVoltage = CHG_Fast_VoltageTarget();
			CHG_PLC_data.MaxCurrent = CHG_PhysicalValueConvert(Rte_CData_CalMaxDemmandCurrent() ,CHG_OFFSET_50,0);
			CHG_PLC_data.MaxVoltage = CHG_PhysicalValueConvert(Rte_CData_CalMaxDemmandVoltage(),CHG_OFFSET_200,0);
			break;

		case CHG_PLC_EMERGENCY:
		case CHG_PLC_SOFT_STOP:
			CHG_PLC_data.TargetCurrent = CHG_PhysicalValue_Zero;
			break;

		case CHG_PLC_POWER_OFF:
			CHG_PLC_data.SCCstateReq = Scc_ChargingControl_StopCharging;
			CHG_PLC_data.ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_STOP;
			break;

		case CHG_PLC_STOP:
			CHG_PLC_data.OBCready = FALSE;/*Open S2*/
			CHG_PLC_data.SCCstateReq = Scc_ChargingControl_StopCharging;
			CHG_PLC_data.ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_STOP;
			break;

		case CHG_PLC_CONTACTOR:
			CHG_PLC_data.QuickConReq = Cx0_Request_to_Open_the_Contactors;
			break;

		default:
		case CHG_PLC_RST_ERROR:
			CHG_PLC_data.ForceElock = FALSE;
			CHG_PLC_data.MainConReq = Cx2_No_request;
			CHG_PLC_data.FunctionControl = Scc_FunctionControl_Reset;
			(void)EthTrcv_30_Ar7000_Slac_Reset(0);
			break;

	}


	(void)Rte_Write_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(CHG_PLC_data.QuickConReq);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(CHG_PLC_data.MainConReq);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpForceElockCloseMode4_DeForceElockCloseMode4(CHG_PLC_data.ForceElock);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(FWinProgress);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void CHG_fast_default_parameters(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	const Scc_PhysicalValueType CHG_PhysicalValue_Zero = {0,0};

	CHG_PLC_data.EVSEcurrentLimit = 0U;
	CHG_PLC_data.EVSEvoltageLimit = 0U;
	CHG_PLC_data.EVSEpresentCurrent = 0U;
	CHG_PLC_data.EVSEpowerLimit = 0U;
	CHG_PLC_data.EVSEstopReq = FALSE;
	CHG_PLC_data.SCCstateRes = Scc_StateMachineStatus_None;
	CHG_PLC_data.SCCstateReq = Scc_ChargingControl_None;
	CHG_PLC_data.MainConReq = Cx2_No_request;
	CHG_PLC_data.QuickConReq = Cx0_Request_to_Open_the_Contactors;
	CHG_PLC_data.FunctionControl = Scc_FunctionControl_None;
	CHG_PLC_data.EVReady = FALSE;
	CHG_PLC_data.ChargeComplete = FALSE;
	CHG_PLC_data.BulkChargingComplete = FALSE;
	CHG_PLC_data.TargetVoltage = CHG_PhysicalValue_Zero;
	CHG_PLC_data.TargetCurrent = CHG_PhysicalValue_Zero;
	CHG_PLC_data.MaxVoltage = CHG_PhysicalValue_Zero;
	CHG_PLC_data.MaxCurrent = CHG_PhysicalValue_Zero;
	CHG_PLC_data.EVressSoc = 0U;
	CHG_PLC_data.CPlineState = Scc_ControlPilotState_State_B; //S2 open
	CHG_PLC_data.HLCstate = Scc_PWMState_ChargingNotAllowed; //no HLC detected
	CHG_PLC_data.ChargeProgress = EXI_ISO_CHARGE_PROGRESS_TYPE_STOP;
	CHG_PLC_data.OBCready = FALSE;
	CHG_PLC_data.SA_ScheduleID = CHG_FAST_MIN_TUPLE_ID;
	CHG_PLC_data.MsgStatus = Scc_MsgStatus_None;
	CHG_PLC_data.PowerOFF = FALSE;
	CHG_PLC_data.EmergencyShutdown = FALSE;
	CHG_PLC_data.TCPstate=Scc_TCPSocketState_Closed;
	CHG_PLC_data.FirwareDownloadReq = FALSE;
	CHG_PLC_data.ForceElock = FALSE;
	CHG_PLC_data.Schema = Scc_SAPSchemaIDs_None;
	CHG_PLC_data.RSTrequest = FALSE;
	CHG_PLC_data.Retry = FALSE;


	/*Debug*/
	CHG_PLC_data.StopReason=0U;
	CHG_PLC_data.Error = Scc_SMER_NoError;
	CHG_PLC_data.ErrorType=Scc_StackError_NoError;
	CHG_PLC_data.FirwareDownloaded=FALSE;
	CHG_PLC_data.SLACstatus=0U; /* PRQA S 1297 #Conversion allowed*/
	CHG_PLC_data.SLACerror=0U;
	CHG_PLC_data.NMKreceived = FALSE;
	CHG_PLC_data.MsgState = Scc_SMMS_PreparingRequest;

}

static void CHG_fast_EndCharge_RTAB(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 Counter = 0U;
	static BMS_QuickChargeConnectorState QuickConState_previous = Cx0_contactors_opened;

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean RTAB_StopCharge;
	BMS_QuickChargeConnectorState QuickConState;


	(void)Rte_Read_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(&QuickConState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((Cx2_contactors_closed==QuickConState_previous)
			&&(Cx0_contactors_opened==QuickConState)
			&&(0U==Counter)
			&&(CHG_PLC_data.EVSEstopReq!=FALSE))
	{
		Counter = (uint32)Rte_CData_CalStopChargeDemmandTimer()*(uint32)CHG_GAIN_10;
	}

	if(0U<Counter)
	{
		Counter--;
		RTAB_StopCharge = TRUE;
	}
	else
	{
		RTAB_StopCharge = FALSE;
	}

	QuickConState_previous = QuickConState;

	(void)Rte_Write_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(RTAB_StopCharge);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static Scc_PhysicalValueType CHG_PhysicalValueConvert(uint32 value, uint16 offset, sint8 exponent)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	Scc_PhysicalValueType RetVal;
	uint32 Value;

	if ((CHG_U32_MAX - value) < offset)
	{
		Value = (uint32)CHG_S16_MAX;
	}
	else
	{
		Value =  value + (uint32)offset;

		if((uint32)CHG_S16_MAX<Value)
		{
			Value = (uint32)CHG_S16_MAX;
		}
	}

	RetVal.Value = (sint16)(sint32)Value;
	RetVal.Exponent = exponent;

	return RetVal;
}
static Scc_PhysicalValueType CHG_Fast_VoltageTarget(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	BMS_HighestChargeVoltageAllow BMSvoltage;
	uint32 CALvoltage;
	uint16 MinValue;


	/*Inputs*/
	(void)Rte_Read_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(&BMSvoltage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	CALvoltage = ((uint32)Rte_CData_CalMaxDiscoveryVoltage()+(uint32)CHG_OFFSET_200)*(uint32)CHG_GAIN_10;

	/*Get min value*/
	MinValue = CHG_PLC_data.EVSEvoltageLimit;
	if(CALvoltage<(uint32)MinValue)
	{
		MinValue = (uint16)CALvoltage;
	}
	if(BMSvoltage<MinValue)
	{
		MinValue = BMSvoltage;
	}

	/*Result in deci volts*/

	return CHG_PhysicalValueConvert(MinValue,0U,-1);
}
static Scc_PhysicalValueType CHG_Fast_CurrentTarget(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	BMS_HighestChargeCurrentAllow BMScurrent;
	uint32 CALcurrent;
	uint32 MAXcurrent;
	uint16 MinValue;


	/*Inputs*/
	(void)Rte_Read_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(&BMScurrent);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	CALcurrent = ((uint32)Rte_CData_CalMaxDiscoveryCurrent()+(uint32)CHG_OFFSET_50)*(uint32)CHG_GAIN_10;
	MAXcurrent = ((uint32)Rte_CData_CalDemandMaxCurrent()+(uint32)CHG_OFFSET_50)*(uint32)CHG_GAIN_10;

	/*Get min value*/
	MinValue = CHG_PLC_data.EVSEcurrentLimit;
	if(CALcurrent<(uint32)MinValue)
	{
		MinValue = (uint16)CALcurrent;
	}
	if(MAXcurrent<(uint32)MinValue)
	{
		MinValue = (uint16)MAXcurrent;
	}
	if(BMScurrent<MinValue)
	{
		MinValue = BMScurrent;
	}
	/*Result in deci Amps*/
	return CHG_PhysicalValueConvert(MinValue,0U,-1);
}
static uint16 CHG_PhysicalValue_deci(Scc_PhysicalValueType EVvalue)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApCHG_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApCHG_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApCHG_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApCHG_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApCHG_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 RetVal = (uint32)(sint32)EVvalue.Value;
	sint8 Exponent = EVvalue.Exponent;
	uint8 SafetyCounter = 0U;


	/*Change factor to 0.1. (Exponent = -1)*/
	/*Valid range: U16*10000 - U16*0.00001*/
	while(((-1)!=Exponent)&&(CHG_SAFETY_COUNTER>=SafetyCounter))
	{
		SafetyCounter ++;
		if(Exponent>=0)
		{
			Exponent--;
			RetVal *= CHG_GAIN_10;
		}
		else
		{
			Exponent++;
			RetVal /= CHG_GAIN_10;
		}

		if(CHG_U16_MAX<RetVal)
		{
			RetVal = CHG_U16_MAX;
		}
	}

	return (uint16)RetVal;
}

void CHG_SCC_Set_StateM_StateMachineStatus(Scc_StateM_StateMachineStatusType StateMachineStatus)
{
	CHG_PLC_data.SCCstateRes = StateMachineStatus;
	return;
}
void CHG_SCC_Set_StateM_StateMachineError(Scc_StateM_StateMachineErrorType StateMachineError)
{
	CHG_PLC_data.Error = StateMachineError;
	if(Scc_SMER_NoError != StateMachineError)
	{
		CHG_PLC_data.EmergencyShutdown = TRUE;
		CHG_PLC_data.EVSEstopReq = TRUE;
		if(0U==CHG_PLC_data.StopReason)
		{
			CHG_PLC_data.StopReason = 30U+(uint8)StateMachineError;
		}
	}
	return;
}
void CHG_SCC_Set_StateM_EnergyTransferModeFlags(uint8 EnergyTransferModeFlags)
{
	if((FALSE==(EnergyTransferModeFlags&(uint8)Scc_EnergyTransferMode_DC_Extended)))
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 1U;
	}
	return;
}
void CHG_SCC_Set_StateM_StateMachineMessageState(Scc_StateM_MsgStateType StateMachineMessageState)
{
	CHG_PLC_data.MsgState = StateMachineMessageState;
	return;
}
void CHG_SCC_Set_Core_StackError(Scc_StackErrorType StackError)
{
	CHG_PLC_data.ErrorType = StackError;
	if(Scc_StackError_NoError!=StackError)
	{
		CHG_PLC_data.EmergencyShutdown = TRUE;
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 12U+(uint8)StackError;
	}
	return;
}
void CHG_SCC_Set_Core_MsgStatus(Scc_MsgStatusType MsgStatus)
{
	CHG_PLC_data.MsgStatus = MsgStatus;
	return;
}
void CHG_SCC_Set_Core_TCPSocketState(Scc_TCPSocketStateType TCPSocketStateType)
{
	CHG_PLC_data.TCPstate=TCPSocketStateType;
	return;
}
void CHG_SCC_Set_Core_SAPSchemaID(Scc_SAPSchemaIDType SAPSchemaID)
{
	CHG_PLC_data.Schema = SAPSchemaID;
	return;
}
void CHG_SCC_Set_SLAC_AssociationStatus(uint8 AssociationStatus)
{
	CHG_PLC_data.SLACerror=AssociationStatus;
	return;
}
void CHG_SCC_Set_SLAC_NMK(P2CONST(Scc_BufferPointerType, AUTOMATIC, SCC_APPL_DATA) NMKPtr)
{
	(void)NMKPtr;
	CHG_PLC_data.NMKreceived = TRUE;
	return;
}

void CHG_SCC_Set_IsoDin_DC_EVSEMaximumCurrentLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumCurrentLimit, Exi_BitType Flag)
{
	if(FALSE!=Flag)
	{
		if(NULL_PTR!=EVSEMaximumCurrentLimit)
		{
			CHG_PLC_data.EVSEcurrentLimit = CHG_PhysicalValue_deci(*EVSEMaximumCurrentLimit);
		}
	}
	return;
}
void CHG_SCC_Set_IsoDin_DC_EVSEMaximumVoltageLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumVoltageLimit, Exi_BitType Flag)
{
	if(FALSE!=Flag)
	{
		if(NULL_PTR!=EVSEMaximumVoltageLimit)
		{
			CHG_PLC_data.EVSEvoltageLimit = CHG_PhysicalValue_deci(*EVSEMaximumVoltageLimit);
		}
	}
	return;
}
void CHG_SCC_Set_ISO_DC_EVSEStatusCode(Exi_ISO_DC_EVSEStatusCodeType EVSEStatusCode)
{
	if(EXI_ISO_DC_EVSESTATUS_CODE_TYPE_EVSE_SHUTDOWN == EVSEStatusCode)
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 2U;
	}
	else if(EXI_ISO_DC_EVSESTATUS_CODE_TYPE_EVSE_EMERGENCY_SHUTDOWN==EVSEStatusCode)
	{
		CHG_PLC_data.EmergencyShutdown = TRUE;
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 2U;
	}
	else
	{
		/* For MISRA purposes */
	}
	return;
}
void CHG_SCC_Set_ISO_SAScheduleList(P2CONST(Exi_ISO_SAScheduleListType, AUTOMATIC, SCC_APPL_DATA) SAScheduleList)
{
	if(NULL_PTR!=SAScheduleList)
	{
		if(NULL_PTR!=(SAScheduleList->SAScheduleTuple))
		{
			/*Take first element*/
			CHG_PLC_data.SA_ScheduleID = (SAScheduleList->SAScheduleTuple)->SAScheduleTupleID;
		}
	}
}
void CHG_SCC_Set_DIN_EVSEStatusCode(Exi_DIN_DC_EVSEStatusCodeType EVSEStatusCode)
{
	if((EXI_DIN_DC_EVSESTATUS_CODE_TYPE_EVSE_SHUTDOWN == EVSEStatusCode)
		||(EXI_DIN_DC_EVSESTATUS_CODE_TYPE_EVSE_EMERGENCY_SHUTDOWN==EVSEStatusCode))
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 3U;
	}
	return;
}

void CHG_SCC_Set_ISO_EVSENotification(Exi_ISO_EVSENotificationType EVSENotification)
{
	if(EXI_ISO_EVSENOTIFICATION_TYPE_STOP_CHARGING == EVSENotification)
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 4U;
	}
	return;
}
void CHG_SCC_Set_ISO_ResponseCode(Exi_ISO_responseCodeType ResponseCode)
{
	if(EXI_ISO_RESPONSE_CODE_TYPE_FAILED<=ResponseCode)
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.EmergencyShutdown = TRUE;
		CHG_PLC_data.StopReason = 5U;
	}
	/*Power Off detection*/
	if((CHG_PLC_POWER_OFF==CHG_PLC_data.MachineState)
			&& ((Scc_StateMachineStatus_PowerDelivery==CHG_PLC_data.SCCstateRes)
					||(Scc_StateMachineStatus_SessionStop==CHG_PLC_data.SCCstateRes)
					||(Scc_StateMachineStatus_WeldingDetection==CHG_PLC_data.SCCstateRes)))
	{
		CHG_PLC_data.PowerOFF = TRUE;
	}
	return;
}
void CHG_SCC_Set_ISO_Notification(P2CONST(Exi_ISO_NotificationType,  AUTOMATIC, SCC_APPL_DATA) Notification, Exi_BitType Flag)
{
	(void)Notification;
	if(FALSE!=Flag)
	{
		/*This function only notify errors*/
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 6U;
	}
	return;
}
void CHG_SCC_Set_ISO_ChargeService(P2CONST(Exi_ISO_ChargeServiceType, AUTOMATIC, SCC_APPL_DATA) ChargeService)
{
	if(NULL_PTR!=ChargeService)
	{
		if(NULL_PTR!=ChargeService->SupportedEnergyTransferMode)
		{
			uint8 Index;
			boolean DC_extended_found = FALSE;
			for(Index = 0U;CHG_MODE4_ISO_NUMBER_ENERGY_TRANSFER_MODE>Index;Index++)
			{
				Exi_ISO_EnergyTransferModeType ChargeServiceMode = ChargeService->SupportedEnergyTransferMode->EnergyTransferMode[Index];
				if(EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_EXTENDED == ChargeServiceMode)
				{
					DC_extended_found = TRUE;
				}
			}

			if(TRUE!=DC_extended_found)
			{
				CHG_PLC_data.EVSEstopReq = TRUE;
				CHG_PLC_data.StopReason = 7U;
			}
		}
	}
	return;
}
void CHG_SCC_Set_DIN_EVSENotification(Exi_DIN_EVSENotificationType EVSENotification)
{
	if(EXI_DIN_EVSENOTIFICATION_TYPE_STOP_CHARGING==EVSENotification)
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.StopReason = 8U;
	}
	return;
}

void CHG_SCC_Set_DIN_ResponseCode(Exi_DIN_responseCodeType ResponseCode)
{
	if(EXI_DIN_RESPONSE_CODE_TYPE_FAILED<=ResponseCode)
	{
		CHG_PLC_data.EVSEstopReq = TRUE;
		CHG_PLC_data.EmergencyShutdown = TRUE;
		CHG_PLC_data.StopReason = 9U;
	}
	/*Power Off detection*/
	if((CHG_PLC_POWER_OFF==CHG_PLC_data.MachineState)
		 && ((Scc_StateMachineStatus_PowerDelivery==CHG_PLC_data.SCCstateRes)
			||(Scc_StateMachineStatus_SessionStop==CHG_PLC_data.SCCstateRes)
			||(Scc_StateMachineStatus_WeldingDetection==CHG_PLC_data.SCCstateRes)))
	{
		CHG_PLC_data.PowerOFF = TRUE;
	}

	return;
}
void CHG_SCC_Set_DIN_ChargeService(P2CONST(Exi_DIN_ServiceChargeType, AUTOMATIC, SCC_APPL_DATA) ChargeService)
{
	if(NULL_PTR!=ChargeService)
	{
		uint16 ChargeServiceType = ChargeService->EnergyTransferType;/* PRQA S 4424 #Supported enum type*/
		if((uint16)EXI_DIN_EVSESUPPORTED_ENERGY_TRANSFER_TYPE_DC_EXTENDED != ChargeServiceType)
		{
			/*No DC charge service*/
			CHG_PLC_data.EVSEstopReq = TRUE;
			CHG_PLC_data.StopReason = 10U;
		}
	}
	return;
}

void CHG_SCC_Set_IsoDin_DC_EVSEMaximumPowerLimit(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEMaximumPowerLimit, Exi_BitType Flag)
{
	volatile Scc_PhysicalValueType Local_PhisicalValue;

	if(FALSE!=Flag)
	{
		if(NULL_PTR!=EVSEMaximumPowerLimit)
		{
			Local_PhisicalValue = (*EVSEMaximumPowerLimit);
			/*Report value in W*/
			Local_PhisicalValue.Exponent--;
			CHG_PLC_data.EVSEpowerLimit = CHG_PhysicalValue_deci(Local_PhisicalValue);
		}
	}
	return;
}
void CHG_SCC_Set_IsoDin_DC_EVSEPresentCurrent(P2CONST(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVSEPresentCurrent)
{
	if(NULL_PTR!=EVSEPresentCurrent)
	{
		CHG_PLC_data.EVSEpresentCurrent = CHG_PhysicalValue_deci(*EVSEPresentCurrent);
	}
	return;
}
void CHG_SCC_Get_Core_ForceSAPSchema(P2VAR(Scc_ForceSAPSchemasType, AUTOMATIC, SCC_APPL_DATA) ForceSAPSchema)
{
	if(NULL_PTR!=ForceSAPSchema)
	{
		*ForceSAPSchema=Scc_ForceSAPSchemas_DIN_ISO;
	}
	return;
}
void CHG_SCC_Get_SLAC_QCAIdleTimer(P2VAR(uint16, AUTOMATIC, SCC_APPL_DATA) QCAIdleTimer)
{
	CHG_PLC_data.FirwareDownloaded = TRUE;
	if(NULL_PTR!=QCAIdleTimer)
	{
		/*5ms tick -> 5seg*/
		*QCAIdleTimer=1000U;
	}
}
uint16 Appl_SccDynCP_StateM_QCAIdleTimer(void)
{
	/*5ms tick -> 5seg*/
	return 1000U;
}
void CHG_SCC_Get_SLAC_StartMode(P2VAR(EthTrcv_30_Ar7000_Slac_StartModeType, AUTOMATIC, SCC_APPL_DATA) SLACStartMode)
{
	if(NULL_PTR!=SLACStartMode)
	{
		*SLACStartMode = ETHTRCV_30_AR7000_SLAC_START_MODE_NEVER_VALIDATE;
	}
}
EthTrcv_30_Ar7000_Slac_StartModeType Appl_SccDynCP_StateM_SLACStartMode(void)
{
	return ETHTRCV_30_AR7000_SLAC_START_MODE_NEVER_VALIDATE;
}

void CHG_SCC_Get_ISO_SelectedPaymentOption(P2VAR(Exi_ISO_paymentOptionType, AUTOMATIC, SCC_APPL_DATA) SelectedPaymentOption)
{
	if(NULL_PTR!=SelectedPaymentOption)
	{
		(*SelectedPaymentOption) = EXI_ISO_PAYMENT_OPTION_TYPE_EXTERNAL_PAYMENT;
	}
	return;
}
void CHG_SCC_Get_ISO_SelectedServiceListPtr(P2VAR(Exi_ISO_SelectedServiceListType, AUTOMATIC, SCC_APPL_DATA) SelectedServiceListPtr)
{
	/* [Enrique.Bueno] Workaround START: MPU violation: CHG is ASIL SWC, but SCC Callbacks are executed in ASIL partition. ==================
	                                         Hence, static variables defined at CHG and written into a SCC Callback will rise MPU violations.
	                                         Contingency actions: remap all static variables affected from ASIL to QM partition taking profit
	                                                              of TBD SWC (qm)                                                                 */
	/* - Static non-init variables declaration ---------- */
	#define CtApTBD_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static Exi_ISO_SelectedServiceType ServiceList[1];

	#define CtApTBD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApTBD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApTBD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApTBD_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApTBD_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
	/* [Enrique.Bueno] Workaround STOP ====================================================================================================== */


	ServiceList[0].ServiceID = 1U;//AC_DC charge
	ServiceList[0].ParameterSetIDFlag = FALSE;
	ServiceList[0].NextSelectedServicePtr = NULL_PTR;

	if(NULL_PTR!=SelectedServiceListPtr)
	{
		(*SelectedServiceListPtr).SelectedService = ServiceList;
	}

	return;
}
void CHG_SCC_Get_StateM_EnergyTransferMode(P2VAR(Scc_StateM_EnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA) EnergyTransferMode)
{
	if(NULL_PTR!=EnergyTransferMode)
	{
		(*EnergyTransferMode)=Scc_EnergyTransferMode_DC_Extended;
	}
	return;
}
void CHG_SCC_Get_IsoDin_DC_EVMaximumCurrentLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumCurrentLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if((NULL_PTR==EVMaximumCurrentLimit)&&(NULL_PTR!=Flag))
	{
		(*Flag) = FALSE;
	}
	else if((NULL_PTR!=EVMaximumCurrentLimit)&&(NULL_PTR!=Flag))
	{
		(*EVMaximumCurrentLimit)=CHG_PLC_data.MaxCurrent;
		(*Flag) = TRUE;
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVMaximumVoltageLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumVoltageLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if((NULL_PTR==EVMaximumVoltageLimit)&&(NULL_PTR!=Flag))
	{
		(*Flag) = FALSE;
	}
	else if((NULL_PTR!=EVMaximumVoltageLimit)&&(NULL_PTR!=Flag))
	{
		(*EVMaximumVoltageLimit)=CHG_PLC_data.MaxVoltage;
		(*Flag) = TRUE;
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVMaximumPowerLimit(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVMaximumPowerLimit, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if((NULL_PTR==EVMaximumPowerLimit)&&(NULL_PTR!=Flag))
	{
		(*Flag) = FALSE;
	}
	else if((NULL_PTR!=EVMaximumPowerLimit)&&(NULL_PTR!=Flag))
	{
		uint32 Power = (uint32)CHG_PhysicalValue_deci(CHG_PLC_data.MaxVoltage)*(uint32)CHG_PhysicalValue_deci(CHG_PLC_data.MaxCurrent);
		/*Factor conversion, from: 0.001 to 10.*/
		Power /= CHG_GAIN_1000;
		if(CHG_MODE4_MAX_POWER<Power)
		{
			/*Limit max power*/
			Power = CHG_MODE4_MAX_POWER;
		}

		(*EVMaximumPowerLimit) = CHG_PhysicalValueConvert(Power,0U,1);
		(*Flag) = TRUE;
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVEnergyCapacity(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVEnergyCapacity, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if((NULL_PTR==EVEnergyCapacity)&&(NULL_PTR!=Flag))
	{
		(*Flag) = FALSE;
	}
	else if((NULL_PTR!=EVEnergyCapacity)&&(NULL_PTR!=Flag))
	{
		/*EnergyCapacity units KWh*/
		EVEnergyCapacity->Exponent = 3;
		EVEnergyCapacity->Value = (sint16)Rte_CData_CalEnergyCapacity();
		(*Flag) = TRUE;
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_StateM_ChargingControl(P2VAR(Scc_StateM_ChargingControlType, AUTOMATIC, SCC_APPL_DATA) ChargingControl)
{
	if(NULL_PTR!=ChargingControl)
	{
		(*ChargingControl)= CHG_PLC_data.SCCstateReq;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVEnergyRequest(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVEnergyRequest, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{

	if((NULL_PTR==EVEnergyRequest) && (NULL_PTR!=Flag))
	{
		(*Flag) = FALSE;
	}
	else if((NULL_PTR!=EVEnergyRequest)
			&&(NULL_PTR!=Flag))
	{
		if (CHG_PER_THOUSAND_MAX>=CHG_PLC_data.EVressSoc)
		{
			/*EnergyCapacity units KWh, EVressSoc units 1/1000*/
			EVEnergyRequest->Exponent = 1;
			EVEnergyRequest->Value = (sint16)(uint16)(((uint32)Rte_CData_CalEnergyCapacity()*((uint32)CHG_PER_THOUSAND_MAX-(uint32)CHG_PLC_data.EVressSoc))/CHG_GAIN_10);
			(*Flag) = TRUE;
		}
		else
		{
			(*Flag) = FALSE;
		}
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_FullSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) FullSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{

	if((NULL_PTR != FullSOC)&&(NULL_PTR != Flag))
	{
		(*FullSOC) = 100;
		(*Flag) = TRUE;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_BulkSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) BulkSOC, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	uint8 BulckSoc = Rte_CData_CalBulkSocConf();

	if((NULL_PTR == BulkSOC)&&(NULL_PTR != Flag))
	{
		(*Flag) = FALSE;

	}
	else if((NULL_PTR != BulkSOC)&&(NULL_PTR != Flag))
	{
		if (CHG_MAX_PERCENTAGE>=BulckSoc)
		{
			(*BulkSOC) =  (sint8)BulckSoc;
			(*Flag) = TRUE;
		}
		else
		{
			(*Flag) = FALSE;

		}
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVReady(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVReady)
{
	if(NULL_PTR!=EVReady)
	{
		(*EVReady) = CHG_PLC_data.EVReady;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVRESSSOC(P2VAR(sint8, AUTOMATIC, SCC_APPL_DATA) EVRESSSOC)
{
	uint16 EVressSoc = CHG_PLC_data.EVressSoc / CHG_GAIN_10;

	if(NULL_PTR != EVRESSSOC)
	{
		/*SOC reported can be higher than 100%*/
		(*EVRESSSOC)=(sint8)(uint8)EVressSoc;
	}

	return;
}
void CHG_SCC_Get_StateM_ControlPilotState(P2VAR(Scc_StateM_ControlPilotStateType, AUTOMATIC, SCC_APPL_DATA) ControlPilotState)
{
	if(NULL_PTR!=ControlPilotState)
	{
		(*ControlPilotState) = CHG_PLC_data.CPlineState;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVTargetVoltage(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetVoltage)
{
	if(NULL_PTR!=EVTargetVoltage)
	{
		(*EVTargetVoltage)=CHG_PLC_data.TargetVoltage;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_EVTargetCurrent(P2VAR(Scc_PhysicalValueType, AUTOMATIC, SCC_APPL_DATA) EVTargetCurrent)
{
	if(NULL_PTR!=EVTargetCurrent)
	{
		(*EVTargetCurrent)=CHG_PLC_data.TargetCurrent;
	}

	return;
}
void CHG_SCC_Get_StateM_PWMState(P2VAR(Scc_StateM_PWMStateType, AUTOMATIC, SCC_APPL_DATA) PWMState)
{
	if(NULL_PTR!=PWMState)
	{
		(*PWMState)=CHG_PLC_data.HLCstate;
	}

	return;
}
void CHG_SCC_Get_ISO_ChargeProgress(P2VAR(Exi_ISO_chargeProgressType, AUTOMATIC, SCC_APPL_DATA) ChargeProgress)
{
	if(NULL_PTR!=ChargeProgress)
	{
		(*ChargeProgress)=CHG_PLC_data.ChargeProgress;
	}

	return;
}

void CHG_SCC_Get_ISO_ChargingSession(P2VAR(Scc_ISO_ChargingSessionType, AUTOMATIC, SCC_APPL_DATA) ChargingSession)
{
	if(NULL_PTR!=ChargingSession)
	{
		(*ChargingSession)=SCC_ISO_CHARGING_SESSION_TYPE_TERMINATE;
	}

	return;
}
void CHG_SCC_Get_ISO_RequestedEnergyTransferMode(P2VAR(Exi_ISO_EnergyTransferModeType, AUTOMATIC, SCC_APPL_DATA) RequestedEnergyTransferMode)
{
	if(NULL_PTR!=RequestedEnergyTransferMode)
	{
		(*RequestedEnergyTransferMode)=EXI_ISO_ENERGY_TRANSFER_MODE_TYPE_DC_EXTENDED;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_ChargingComplete(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) ChargingComplete)
{
	if(NULL_PTR!=ChargingComplete)
	{
		(*ChargingComplete)=CHG_PLC_data.ChargeComplete;
	}

	return;
}
void CHG_SCC_Get_IsoDin_DC_BulkChargingComplete(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) BulkChargingComplete, P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if((NULL_PTR==BulkChargingComplete)&&(NULL_PTR!=Flag))
	{
		(*Flag) = FALSE;
	}
	else if((NULL_PTR!=BulkChargingComplete)&&(NULL_PTR!=Flag))
	{
		(*BulkChargingComplete)=CHG_PLC_data.BulkChargingComplete;
		(*Flag) = TRUE;
	}
	else
	{
		/*MISRA do not change state.*/
	}

	return;
}
void CHG_SCC_Get_StateM_FunctionControl(P2VAR(Scc_StateM_FunctionControlType, AUTOMATIC, SCC_APPL_DATA) FunctionControl)
{
	if(NULL_PTR!=FunctionControl)
	{
		(*FunctionControl)=CHG_PLC_data.FunctionControl;
	}

	return;
}
void CHG_SCC_Get_ISO_DC_EVErrorCode(P2VAR(Exi_ISO_DC_EVErrorCodeType, AUTOMATIC, SCC_APPL_DATA) EVErrorCode)
{
	if(NULL_PTR!=EVErrorCode)
	{
		(*EVErrorCode) = EXI_ISO_DC_EVERROR_CODE_TYPE_NO_ERROR;
	}

	return;
}
void CHG_SCC_Get_ISO_SAScheduleTupleID(P2VAR(uint8, AUTOMATIC, SCC_APPL_DATA) SAScheduleTupleID)
{
	if(NULL_PTR!=SAScheduleTupleID)
	{
		(*SAScheduleTupleID) = CHG_PLC_data.SA_ScheduleID;
	}

	return;
}

/*DIN*/
void CHG_SCC_Get_DIN_EVErrorCode(P2VAR(Exi_DIN_DC_EVErrorCodeType, AUTOMATIC, SCC_APPL_DATA) EVErrorCode)
{
	if(NULL_PTR!=EVErrorCode)
	{
		(*EVErrorCode) = EXI_DIN_DC_EVERROR_CODE_TYPE_NO_ERROR;
	}

	return;
}
void CHG_SCC_Get_DIN_RequestedEnergyTransferMode(P2VAR(Exi_DIN_EVRequestedEnergyTransferType, AUTOMATIC, SCC_APPL_DATA) RequestedEnergyTransferMode)
{
	if(NULL_PTR!=RequestedEnergyTransferMode)
	{
		(*RequestedEnergyTransferMode)=EXI_DIN_EVREQUESTED_ENERGY_TRANSFER_TYPE_DC_EXTENDED;
	}
	return;
}
void CHG_SCC_Get_IsoDin_DC_EVPowerDeliveryParameterFlag(P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) EVPowerDeliveryParameterFlag)
{
	if(NULL_PTR!=EVPowerDeliveryParameterFlag)
	{
			(*EVPowerDeliveryParameterFlag)=TRUE;//CHG_PLC_data.PowerDeliveryDIN;
	}

	return;
}
void CHG_Scc_Get_IsoDin_ParamNotUsed( P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if(NULL_PTR!=Flag)
	{
		*Flag=FALSE;
	}

	return;
}
void CHG_Scc_Get_IsoDin_TRUE( P2VAR(boolean, AUTOMATIC, SCC_APPL_DATA) Flag)
{
	if(NULL_PTR!=Flag)
	{
		*Flag=TRUE;
	}

	return;
}


static void CHG_UpdateCalibratables(void)
{
	CHG_InputVoltageThreshold=Rte_Prm_PiInputVoltageThreshold_DeInputVoltageThreshold();
	CHG_ThresholdNoAC=Rte_CData_CalThresholdNoAC();

	CHG_Vrange_110_max = (uint16)Rte_CData_CalMaxInputVoltage110V() + (uint16)CHG_VOLT_OFFSET_100;
	CHG_Vrange_110_min = (uint16)Rte_CData_CalMinInputVoltage110V();

	CHG_Vrange_220_max = (uint16)Rte_CData_CalMaxInputVoltage220V() + (uint16)CHG_VOLT_OFFSET_100;
	CHG_Vrange_220_min = (uint16)Rte_CData_CalMinInputVoltage220V();
}



static void CHG_resetPLC(void)
{
	CHG_ResetRequestOnGoing = TRUE;
}


void EthClient_Cbk_PlcResetOnOsd(uint8 CtrlIdx, Eth_30_Ar7000_OsdResetType Mode)/* PRQA S 3206 #Function used these parameters*/
{
	CHG_resetPLC();
	CHG_OutOfSyncHit = TRUE;
}


void CHG_Eth_No_Com(void)
{
		(void)Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(COMM_NO_COMMUNICATION);
}


void CHG_Eth_Full_Com(void)
{
		(void)Rte_Call_PpComMUserNeed_EthUserRequest_RequestComMode(COMM_FULL_COMMUNICATION);
}


void Appl_TriggerDownloadReqCbk(void)
{
	CHG_PLC_data.FirwareDownloadReq = TRUE;
}

void Scc_Cbk_SLAC_AssociationState(EthTrcv_30_Ar7000_Slac_StateType CurState, boolean MessageValid, void* Message)/* PRQA S 3206 #Function used these parameters*/
{
	CHG_PLC_data.SLACstatus=CurState;
}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
