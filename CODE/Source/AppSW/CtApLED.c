/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApLED.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApLED
 *  Generation Time:  2020-11-23 11:42:18
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApLED>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup LED
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief LED state management.
 * \details This module controls the LEDs state depending on the HMI state and
 * 	E-lock state. Each state defines a blinking time and a duty cycle for each
 * 	LED. This module while switch ON and OFF the LEDs depending on the
 * 	current state.
 *
 * \{
 * \file  LED.c
 * \brief  Generic code of the LED module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_CtApLED.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Autogenrated file.*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/*! Conversion gain between 100ms to 10ms*/
#define LED_100MS_TO_10MS	10U

/* PRQA S 0857 -- #Autogenrated file.*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/

/*! Plug Led operating modes.*/
typedef enum{
	/*! Mode A (Error)*/
	LED_PLG_MODE_A = 0U,
	/*! Mode B (E-lock locked)*/
	LED_PLG_MODE_B,
	/*! Mode C (E-lock unlocked or diving)*/
	LED_PLG_MODE_C,
	/*! Number of operating modes for plug LED.*/
	LED_PLG_MODE_NUM
}LED_plug_mode_t;

/*! RGB leds*/
typedef enum{
	/*! Red*/
	LED_RGB_RED = 0U,
	/*! Green*/
	LED_RGB_GREEN,
	/*! Blue*/
	LED_RGB_BLUE,
	/*! Number of RGB LEDs*/
	LED_RGB_NUMBER
}LED_RGBleds_t;

/*! RGB operating modes*/
typedef enum{
	/*! Mode: EndOfCharge*/
	LED_RGB_MODE_END = 0U,
	/*! Mode: ProgrammingCharge*/
	LED_RGB_MODE_PROGRAMMING,
	/*! Mode: ChargeError*/
	LED_RGB_MODE_ERROR,
	/*! Mode: ChargeInProgress*/
	LED_RGB_MODE_INPROGRESS,
	/*! Mode: GuideManagement*/
	LED_RGB_MODE_GUIDE,
	/*! Number of operating modes in RGB.*/
	LED_RGB_MODE_NUMBER
}LED_RGBmode_t;

 /*PRQA S 3205 --*/

/*! RGB operating mode parameters.*/
typedef struct{
	/*! Time at ON state*/
	uint8 t_on;
	/*! Time at OFF state*/
	uint8 t_off;
	/*! Duty cycle of each led while ON state*/
	uint8 dc_on[LED_RGB_NUMBER];
}LED_rgbMode_param_t;

/*! Plug LED mode parameters*/
typedef struct{
	/*! Time at ON state*/
	uint16 t_on;
	/*! Time at OFF state*/
	uint16 t_off;
}LED_plgMode_param_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*! Plug LED's Duty cycle while ON.*/
static uint8 LED_plugDC_on;
/*! Plug modes parameters.*/
static LED_plgMode_param_t LED_plgParam[LED_PLG_MODE_NUM];
/*! RGB modes parameters.*/
static LED_rgbMode_param_t LED_RGBparam[LED_RGB_MODE_NUMBER];

#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/*!
 * \brief Plug mode calculate.
 * \details This functions returns the operating mode for the plug LED depending
 * on the E-lock state.
 * \param[in] LED_LockStateError E-lock error state.
 * \param[in] LED_LockLED2BEPR E-lock lock state.
 * \return Plug LED operating mode \ref LED_plug_mode_t
 */
static LED_plug_mode_t LED_plugMode_calculate(boolean LED_LockStateError, boolean LED_LockLED2BEPR);

/*!
 * \brief RGB LEDs operating mode.
 * \details This function calculates the RGB operating mode depending on the
 * inputs states. The inputs states are represented individually in a array.
 * Theoretically only one state can be active simultaneously.
 * \param[in] LED_signal_in Array with the state(BOOL) of each input mode.
 * \return RGB operating mode \ref LED_RGBmode_t
 */
static uint8 LED_RGBmode_calculate(const boolean LED_signal_in[LED_RGB_MODE_NUMBER]);

/** Update calibratables*/
static void LED_UpdateCalibratables(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IdtChLedCtl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyLedCharge: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtDutyPlug: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtPlgLedrCtrl: Integer in interval [0...100]
 *   Unit: [%], Factor: 1, Offset: 0
 * IdtTimeLedCharge: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimePlug: Integer in interval [0...10]
 *   Unit: [ms], Factor: 100, Offset: 0
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtAppRCDECUState: Enumeration of integer in interval [0...5] with enumerators
 *   APP_STATE_0 (0U)
 *   APP_STATE_1 (1U)
 *   APP_STATE_2 (2U)
 *   APP_STATE_3 (3U)
 *   APP_STATE_4 (4U)
 *   APP_STATE_5 (5U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueChargeError(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueChargeInProgress(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueEndOfCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueGuideManagement(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyBlueProgrammingCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenChargeError(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenChargeInProgress(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenEndOfCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenGuideManagement(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyGreenProgrammingCharge(void)
 *   IdtDutyPlug Rte_CData_CalDutyPlug(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedChargeError(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedChargeInProgress(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedEndOfCharge(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedGuideManagement(void)
 *   IdtDutyLedCharge Rte_CData_CalDutyRedProgrammingCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffChargeError(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffChargeInProgress(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffEndOfCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffGuideManagement(void)
 *   IdtTimePlug Rte_CData_CalTimeOffPlugA(void)
 *   IdtTimePlug Rte_CData_CalTimeOffPlugB(void)
 *   IdtTimePlug Rte_CData_CalTimeOffPlugC(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOffProgrammingCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnChargeError(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnChargeInProgress(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnEndOfCharge(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnGuideManagement(void)
 *   IdtTimePlug Rte_CData_CalTimeOnPlugA(void)
 *   IdtTimePlug Rte_CData_CalTimeOnPlugB(void)
 *   IdtTimePlug Rte_CData_CalTimeOnPlugC(void)
 *   IdtTimeLedCharge Rte_CData_CalTimeOnProgrammingCharge(void)
 *
 *********************************************************************************************************************/


#define CtApLED_START_SEC_CODE
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLED_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_init_doc
 *********************************************************************************************************************/

/*!
 * \brief LED initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLED_CODE) RCtApLED_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	LED_RGBparam[LED_RGB_MODE_END].dc_on[LED_RGB_RED]=(uint8)Rte_CData_CalDutyRedEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].dc_on[LED_RGB_GREEN]=(uint8)Rte_CData_CalDutyGreenEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].dc_on[LED_RGB_BLUE]=(uint8)Rte_CData_CalDutyBlueEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].t_on =(uint8)Rte_CData_CalTimeOnEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].t_off =(uint8)Rte_CData_CalTimeOffEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].dc_on[LED_RGB_RED]=(uint8)Rte_CData_CalDutyRedProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].dc_on[LED_RGB_GREEN]=(uint8)Rte_CData_CalDutyGreenProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].dc_on[LED_RGB_BLUE]=(uint8)Rte_CData_CalDutyBlueProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].t_on =(uint8)Rte_CData_CalTimeOnProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].t_off =(uint8)Rte_CData_CalTimeOffProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_ERROR].dc_on[LED_RGB_RED] =(uint8)Rte_CData_CalDutyRedChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].dc_on[LED_RGB_GREEN]=(uint8) Rte_CData_CalDutyGreenChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].dc_on[LED_RGB_BLUE] =(uint8)Rte_CData_CalDutyBlueChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].t_on =(uint8) Rte_CData_CalTimeOnChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].t_off =(uint8) Rte_CData_CalTimeOffChargeError();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].dc_on[LED_RGB_RED] =(uint8) Rte_CData_CalDutyRedChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].dc_on[LED_RGB_GREEN] =(uint8) Rte_CData_CalDutyGreenChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].dc_on[LED_RGB_BLUE] =(uint8)Rte_CData_CalDutyBlueChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].t_on =(uint8)Rte_CData_CalTimeOnChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].t_off =(uint8)Rte_CData_CalTimeOffChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_GUIDE].dc_on[LED_RGB_RED] =(uint8)Rte_CData_CalDutyRedGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].dc_on[LED_RGB_GREEN] =(uint8)Rte_CData_CalDutyGreenGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].dc_on[LED_RGB_BLUE] =(uint8)Rte_CData_CalDutyBlueGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].t_on =(uint8)Rte_CData_CalTimeOnGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].t_off =(uint8)Rte_CData_CalTimeOffGuideManagement();
	LED_plugDC_on =(uint8)Rte_CData_CalDutyPlug();

	/*Convert time units.*/
	/*Cast to U8 because they are coming form DFA U8.*/
	LED_plgParam[LED_PLG_MODE_A].t_on = ((uint16)(uint8)Rte_CData_CalTimeOnPlugA()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_A].t_off = ((uint16)(uint8)Rte_CData_CalTimeOffPlugA()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_B].t_on = ((uint16)(uint8)Rte_CData_CalTimeOnPlugB()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_B].t_off = ((uint16)(uint8)Rte_CData_CalTimeOffPlugB()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_C].t_on = ((uint16)(uint8)Rte_CData_CalTimeOnPlugC()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_C].t_off = ((uint16)(uint8)Rte_CData_CalTimeOffPlugC()) * (uint16)LED_100MS_TO_10MS;


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLED_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeChargeError(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeChargeInProgress(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeEndOfCharge(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeGuideManagement(boolean *data)
 *   Std_ReturnType Rte_Read_PpLedModes_DeProgrammingCharge(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpChLedRGBCtl_DeChLedBrCtl(IdtChLedCtl data)
 *   Std_ReturnType Rte_Write_PpChLedRGBCtl_DeChLedGrCtl(IdtChLedCtl data)
 *   Std_ReturnType Rte_Write_PpChLedRGBCtl_DeChLedRrCtl(IdtChLedCtl data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task100ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief LED task.
  *
  * This function is called periodically by the scheduler. This function should
  * update the RGB LEDs states.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLED_CODE) RCtApLED_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint16 LED_counterRGB = 0U;
	static uint8 LED_rgbMode_previous=0U;

	#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 LED_ecu_state;
	boolean LED_RGBmode_in[LED_RGB_MODE_NUMBER];
	uint8 LED_rgbMode;
	uint8 LED_dcOut[LED_RGB_NUMBER] = {0U};


	LED_UpdateCalibratables();

	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&LED_ecu_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_DeEndOfCharge(&LED_RGBmode_in[LED_RGB_MODE_END]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_DeProgrammingCharge(&LED_RGBmode_in[LED_RGB_MODE_PROGRAMMING]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_DeChargeError(&LED_RGBmode_in[LED_RGB_MODE_ERROR]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_DeChargeInProgress(&LED_RGBmode_in[LED_RGB_MODE_INPROGRESS]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLedModes_DeGuideManagement(&LED_RGBmode_in[LED_RGB_MODE_GUIDE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((1U == LED_ecu_state)
			|| (3U == LED_ecu_state)
			|| (4U == LED_ecu_state)
			|| (5U == LED_ecu_state))
	{
		LED_rgbMode = LED_RGBmode_calculate(LED_RGBmode_in);
		/*New state*/
		if(LED_rgbMode!=LED_rgbMode_previous)
		{
			LED_counterRGB = 0U;
		}
		LED_rgbMode_previous = LED_rgbMode;

		if((uint8)LED_RGB_MODE_NUMBER <= LED_rgbMode)
		{
			/*NO mode is active.*/
			/*LED_dcOut is left to 0U.*/
		}
		else
		{
			/*Led state*/
			if(LED_counterRGB < (uint16)LED_RGBparam[LED_rgbMode].t_on)
			{
				LED_dcOut[LED_RGB_RED] = LED_RGBparam[LED_rgbMode].dc_on[LED_RGB_RED];
				LED_dcOut[LED_RGB_GREEN] = LED_RGBparam[LED_rgbMode].dc_on[LED_RGB_GREEN];
				LED_dcOut[LED_RGB_BLUE] = LED_RGBparam[LED_rgbMode].dc_on[LED_RGB_BLUE];
			}
			else
			{
				/*LED_dcOut is left to 0U.*/
			}

			LED_counterRGB ++;
			/*Limit counter.*/
			if(LED_counterRGB >= ((uint16)LED_RGBparam[LED_rgbMode].t_on + (uint16)LED_RGBparam[LED_rgbMode].t_off))
			{
				LED_counterRGB = 0U;
			}

		}
	}
	else
	{
		/*LED_dcOut is left to 0U.*/
		LED_counterRGB = 0U;
	}

	(void)Rte_Write_PpChLedRGBCtl_DeChLedRrCtl((IdtChLedCtl)LED_dcOut[LED_RGB_RED]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpChLedRGBCtl_DeChLedGrCtl((IdtChLedCtl)LED_dcOut[LED_RGB_GREEN]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpChLedRGBCtl_DeChLedBrCtl((IdtChLedCtl)LED_dcOut[LED_RGB_BLUE]);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApLED_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpAppRCDECUState_DeAppRCDECUState(IdtAppRCDECUState *data)
 *   Std_ReturnType Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(boolean *data)
 *   Std_ReturnType Rte_Read_PpLockStateError_DeLockStateError(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl(IdtPlgLedrCtrl data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief LED task.
 *
 * This function is called periodically by the scheduler. This function should
 * update the E-Lock LED state.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApLED_CODE) RCtApLED_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApLED_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 LED_counter10ms = 0U;

	#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static LED_plug_mode_t LED_plgMode_previous = LED_PLG_MODE_NUM;

	#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 LED_ecu_state;
	boolean LED_Elock_error;
	boolean LED_Elock_locked;
	LED_plug_mode_t LED_plgMode;
	uint8 LED_PlgLedrCtrl=0U;


	LED_UpdateCalibratables();

	(void)Rte_Read_PpAppRCDECUState_DeAppRCDECUState(&LED_ecu_state);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLockLED2BEPR_DeLockLED2BEPR(&LED_Elock_locked);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpLockStateError_DeLockStateError(&LED_Elock_error);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if((1U == LED_ecu_state)
		|| (3U == LED_ecu_state)
		|| (4U == LED_ecu_state)
		|| (5U == LED_ecu_state))
	{
		LED_plgMode = LED_plugMode_calculate(LED_Elock_error,LED_Elock_locked);
		if(LED_plgMode!=LED_plgMode_previous)
		{
			LED_counter10ms = 0U;
		}
		LED_plgMode_previous = LED_plgMode;

		if(LED_PLG_MODE_NUM>LED_plgMode)
		{
			if(LED_counter10ms < (uint32)LED_plgParam[LED_plgMode].t_on)
			{
				LED_PlgLedrCtrl = LED_plugDC_on;
			}
			else
			{
				LED_PlgLedrCtrl = 0U;
			}

			LED_counter10ms ++;

			if(LED_counter10ms >= ((uint32)LED_plgParam[LED_plgMode].t_on + (uint32)LED_plgParam[LED_plgMode].t_off))
			{
				LED_counter10ms = 0U;
			}
		}

	}
	else
	{
		LED_PlgLedrCtrl = 0U;
	}

	(void)Rte_Write_PpPlgLedrCtrl_DePlgLedrCtrl((IdtPlgLedrCtrl)LED_PlgLedrCtrl);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApLED_STOP_SEC_CODE
#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static LED_plug_mode_t LED_plugMode_calculate(boolean LED_LockStateError, boolean LED_LockLED2BEPR)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	LED_plug_mode_t LED_plugMode_ret;


	if((TRUE == LED_LockStateError)
			&&(FALSE == LED_LockLED2BEPR ))
	{
		LED_plugMode_ret = LED_PLG_MODE_A;
	}
	else if ((FALSE == LED_LockStateError)
			&&(TRUE == LED_LockLED2BEPR ))
	{
		LED_plugMode_ret = LED_PLG_MODE_B;
	}
	else
	{
		LED_plugMode_ret = LED_PLG_MODE_C;
	}

	return LED_plugMode_ret;
}
static uint8 LED_RGBmode_calculate(const boolean LED_signal_in[LED_RGB_MODE_NUMBER])
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 LED_rgb_index;


	for(LED_rgb_index = 0U; LED_rgb_index < (uint8)LED_RGB_MODE_NUMBER; LED_rgb_index++)
	{
		if(TRUE == LED_signal_in[LED_rgb_index])
		{
			break;
		}
	}
	return LED_rgb_index;
}

static void LED_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApLED_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApLED_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApLED_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApLED_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApLED_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	LED_RGBparam[LED_RGB_MODE_END].dc_on[LED_RGB_RED]=(uint8)Rte_CData_CalDutyRedEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].dc_on[LED_RGB_GREEN]=(uint8)Rte_CData_CalDutyGreenEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].dc_on[LED_RGB_BLUE]=(uint8)Rte_CData_CalDutyBlueEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].t_on =(uint8)Rte_CData_CalTimeOnEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_END].t_off =(uint8)Rte_CData_CalTimeOffEndOfCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].dc_on[LED_RGB_RED]=(uint8)Rte_CData_CalDutyRedProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].dc_on[LED_RGB_GREEN]=(uint8)Rte_CData_CalDutyGreenProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].dc_on[LED_RGB_BLUE]=(uint8)Rte_CData_CalDutyBlueProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].t_on =(uint8)Rte_CData_CalTimeOnProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_PROGRAMMING].t_off =(uint8)Rte_CData_CalTimeOffProgrammingCharge();
	LED_RGBparam[LED_RGB_MODE_ERROR].dc_on[LED_RGB_RED] =(uint8)Rte_CData_CalDutyRedChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].dc_on[LED_RGB_GREEN]=(uint8) Rte_CData_CalDutyGreenChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].dc_on[LED_RGB_BLUE] =(uint8)Rte_CData_CalDutyBlueChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].t_on =(uint8) Rte_CData_CalTimeOnChargeError();
	LED_RGBparam[LED_RGB_MODE_ERROR].t_off =(uint8) Rte_CData_CalTimeOffChargeError();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].dc_on[LED_RGB_RED] =(uint8) Rte_CData_CalDutyRedChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].dc_on[LED_RGB_GREEN] =(uint8) Rte_CData_CalDutyGreenChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].dc_on[LED_RGB_BLUE] =(uint8)Rte_CData_CalDutyBlueChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].t_on =(uint8)Rte_CData_CalTimeOnChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_INPROGRESS].t_off =(uint8)Rte_CData_CalTimeOffChargeInProgress();
	LED_RGBparam[LED_RGB_MODE_GUIDE].dc_on[LED_RGB_RED] =(uint8)Rte_CData_CalDutyRedGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].dc_on[LED_RGB_GREEN] =(uint8)Rte_CData_CalDutyGreenGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].dc_on[LED_RGB_BLUE] =(uint8)Rte_CData_CalDutyBlueGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].t_on =(uint8)Rte_CData_CalTimeOnGuideManagement();
	LED_RGBparam[LED_RGB_MODE_GUIDE].t_off =(uint8)Rte_CData_CalTimeOffGuideManagement();
	LED_plugDC_on =(uint8)Rte_CData_CalDutyPlug();

	/*Convert time units.*/
	/*Cast to U8 because they are coming form DFA U8.*/
	LED_plgParam[LED_PLG_MODE_A].t_on = ((uint16)(uint8)Rte_CData_CalTimeOnPlugA()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_A].t_off = ((uint16)(uint8)Rte_CData_CalTimeOffPlugA()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_B].t_on = ((uint16)(uint8)Rte_CData_CalTimeOnPlugB()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_B].t_off = ((uint16)(uint8)Rte_CData_CalTimeOffPlugB()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_C].t_on = ((uint16)(uint8)Rte_CData_CalTimeOnPlugC()) * (uint16)LED_100MS_TO_10MS;
	LED_plgParam[LED_PLG_MODE_C].t_off = ((uint16)(uint8)Rte_CData_CalTimeOffPlugC()) * (uint16)LED_100MS_TO_10MS;

}
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
