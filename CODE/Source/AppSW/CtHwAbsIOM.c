/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtHwAbsIOM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtHwAbsIOM
 *  Generation Time:  2020-11-23 11:42:20
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtHwAbsIOM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup IOM
 * \ingroup BaseSw
 * \author Pablo Bolas.
 * \brief Digital Input/Output manager.
 * \details This module manages the digital input/output pins.
 * \{
 * \file  iom.c
 * \brief  Generic code of the IOM module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * WdgM_CheckpointIdType
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 * WdgM_SupervisedEntityIdType
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *
 * Operation Prototypes:
 * =====================
 * CheckpointReached of Port Interface WdgM_AliveSupervision
 *   Indicates to the Watchdog Manager that a Checkpoint within a Supervised Entity has been reached.
 *
 * ActivateSupervisionEntity of Port Interface WdgM_General
 *   Activates the supervision of the given supervised entity.
 *
 * DeactivateSupervisionEntity of Port Interface WdgM_General
 *   Deactivates the supervision of the given supervised entity.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtHwAbsIOM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/
 /* PRQA S 0857,0380 ++ # Max amount of macros check */
#include "AppCbk.h"
#include "Dio.h"
#include "AppExternals.h"
#include "Wdg_30_TLE4278G.h"
#include "WdgM.h"
#include "Mcu.h"
#include "IfxPort_reg.h"
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                              */
/*---------------------------------------------------------------------------*/


/*LED diagnosis task period*/
#define IOM_LED_PERIODE 10U

/* Defined in units of 10 ms */
#define	IOM_ESR0_WDG_DELAY_MAX_COUNT 	600U

 /* PRQA S 0857,0380 -- */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
typedef enum{
	IOM_INPUT,
	IOM_OUTPUT
}IOM_PinDirection_t;

typedef enum{
	IOM_ACTIVE_HIGH,
	IOM_ACTIVE_LOW
}IOM_polarity_t;

typedef struct {
	Dio_ChannelType ChannelId;
	IOM_PinDirection_t Direction;
	IOM_polarity_t polarity;
}IOM_PinInfromation_t;

typedef enum {
	SUP_DO_CTRL_PILOT = 0U,
	SUP_DI_PUSH_CHARGE,
	SUP_DI_RCD_LINE,
	SUP_DI_PHASE_L1,
	SUP_DI_PHASE_N,
	SUP_DO_ELOCK_L,
	SUP_DO_ELOCK_H,
	SUP_DO_DEBUG0,
	SUP_DO_DEBUG2,
	SUP_DO_EN_LEDS,
	SUP_DO_VCC_OBC,
	SUP_DO_VCC_DCLV,
	SUP_DO_FAULT_OBC,
	SUP_DO_FAULT_DCLV,
	SUP_DO_WAKE_UP,
	SUP_DO_DCLINK_DISCHARGE,
	SUP_DO_BAT_DISCHARGE,
	SUP_DO_FSP,	/* PRQA S 3205 # Value not used, but kept in this definition to be used in the initialization of the pins. */
	SUP_DO_NRESET_PLC,
	SUP_DI_LED_FAULT,
	SUP_DI_FAULT_OBC,
	SUP_DI_FAULT_DCLV,
	SUP_DO_WDT,	/* PRQA S 3205 # Value not used, but kept in this definition to be used in the initialization of the pins. */
	IOM_PIN_COUNT
} IOM_Pins_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static non-init variables HERE... */

#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

/** Debug variables on CAN*/
static uint8 IOM_CanDebug = 0U;

#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


static const IOM_PinInfromation_t IOM_PinInformation[IOM_PIN_COUNT]=
{
		{DioConf_DioChannel_SUP_DO_CTRL_PILOT,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_CTRL_PILOT
		{DioConf_DioChannel_SUP_DI_PUSH_CDE_CHARGE,IOM_INPUT,IOM_ACTIVE_LOW},//SUP_DI_PUSH_CHARGE
		{DioConf_DioChannel_WAKE_UP_uC,IOM_INPUT,IOM_ACTIVE_HIGH},//SUP_DI_RCD_LINE
		{DioConf_DioChannel_SUP_DI_ACTIVE_RELAY_N1_N2,IOM_INPUT,IOM_ACTIVE_HIGH},//SUP_DI_PHASE_L1
		{DioConf_DioChannel_SUP_PH1_IN_N_DETECTION,IOM_INPUT,IOM_ACTIVE_HIGH},//SUP_DI_PHASE_N
		{DioConf_DioChannel_SUP_DO_ELOCK_L,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_ELOCK_L
		{DioConf_DioChannel_SUP_DO_ELOCK_H,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_ELOCK_H
		{DioConf_DioChannel_SUP_DO_DEBUG0,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_DEBUG0
		{DioConf_DioChannel_SUP_DO_DEBUG2,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_DEBUG2
		{DioConf_DioChannel_SUP_DO_EN_LEDS,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_EN_LEDS
		{DioConf_DioChannel_SUP_DO_EN_VCC_OBC,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_VCC_OBC
		{DioConf_DioChannel_SUP_DO_EN_VCC_DCDC_400_12,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_VCC_DCLV
		{DioConf_DioChannel_nSUP_DO_FAULT,IOM_OUTPUT,IOM_ACTIVE_LOW},//SUP_DO_FAULT_OBC
		{DioConf_DioChannel_nSUP_DO_FAULT_DCLV,IOM_OUTPUT,IOM_ACTIVE_LOW},//SUP_DO_FAULT_DCLV
		{DioConf_DioChannel_WAKE_UP_OUT_Uc,IOM_OUTPUT,IOM_ACTIVE_HIGH},//WAKE_UP_OUT_Uc
		{DioConf_DioChannel_SUP_DO_V_DCLINK_DISCHARGE,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_DCLINK_DISCHARGE
		{DioConf_DioChannel_SUP_DO_V_BAT_DISCHARGE,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_BAT_DISCHARGE

		{DioConf_DioChannel_nSUP_DO_FSP,IOM_OUTPUT,IOM_ACTIVE_LOW},//SUP_DO_FSP
		{DioConf_DioChannel_NRSET_PLC,IOM_OUTPUT,IOM_ACTIVE_HIGH},//SUP_DO_NRESET_PLC
		{DioConf_DioChannel_SUP_DI_nFAULT_LEDS,IOM_INPUT,IOM_ACTIVE_LOW},//SUP_DI_LED_FAULT
		{DioConf_DioChannel_SUP_EN_HW_PROT,IOM_INPUT,IOM_ACTIVE_LOW},//SUP_DI_FAULT_OBC
		{DioConf_DioChannel_nSUP_DI_FAULT_DCLV,IOM_INPUT,IOM_ACTIVE_LOW},//SUP_DI_FAULT_DCLV
		{DioConf_DioChannel_SUP_DO_WDI,IOM_OUTPUT,IOM_ACTIVE_HIGH}//SUP_DO_WDT
};

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/**
 * \brief Read pin state
 * \details This function returns the pin state depending on the pin level an the
 * pin polarity.
 * \param[in] pin PIN to be read.
 * \return Pin state.
 */
static boolean IOM_GetPinState(IOM_Pins_t pin);

/**
 * \brief Write pin state
 * \details This function writes the pin level depending on the pin state and the
 * pin polarity.
 * \param[in] pin PIN to be written.
 * \param[in] state PIN state.
 */
static void IOM_SetPinState(IOM_Pins_t pin, boolean state);

static void IOM_ProcessDelayedProgramFlowActivation(void);

static void IOM_EnableESR0_Pin(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * WdgM_SupervisedEntityIdType: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 *
 *********************************************************************************************************************/


#define CtHwAbsIOM_START_SEC_CODE
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsIOM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_init_doc
 *********************************************************************************************************************/
/*!
 * \brief IOM initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Workaround.*/
	IOM_SetPinState(SUP_DO_EN_LEDS,TRUE);
	IOM_SetPinState(SUP_DO_VCC_OBC,TRUE);
	IOM_SetPinState(SUP_DO_VCC_DCLV,TRUE);
	IOM_SetPinState(SUP_DO_FAULT_OBC,FALSE);
	IOM_SetPinState(SUP_DO_FAULT_DCLV,FALSE);
	IOM_SetPinState(SUP_DO_WAKE_UP,TRUE);
	IOM_SetPinState(SUP_DO_DCLINK_DISCHARGE,FALSE);
	IOM_SetPinState(SUP_DO_BAT_DISCHARGE,FALSE);
	/* FSP pin shall be managed by SafeTlib code and SMU hw peripheral only! */
	/* IOM_SetPinState(SUP_DO_FSP,FALSE); */
	IOM_SetPinState(SUP_DO_NRESET_PLC,TRUE);

	IOM_EnableESR0_Pin();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsIOM_inputTask100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInputChargePushRaw_DeInputChargePushRaw(boolean data)
 *   Std_ReturnType Rte_Write_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_inputTask100ms_doc
 *********************************************************************************************************************/

/*!
 * \brief IOM 100ms input task.
 *
 * This function is called periodically by the scheduler. This function should
 * manage the digital input pins: read the input pins state.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_inputTask100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_inputTask100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean IOM_pinState;
	boolean IOM_ProgramFlowSoftModeEnabled;

	(void)Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&IOM_ProgramFlowSoftModeEnabled);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (FALSE == IOM_ProgramFlowSoftModeEnabled)
	{
		(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP6);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	/* Execute SBST RunPhase tests */
	SAFETLIB_Run_RunPhaseTests();

	/* Execute IOM standard actions */
	IOM_pinState = IOM_GetPinState(SUP_DI_PUSH_CHARGE);
	(void)Rte_Write_PpInputChargePushRaw_DeInputChargePushRaw(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	IOM_pinState = IOM_GetPinState(SUP_DI_LED_FAULT);
	(void)Rte_Write_PpLedRGBFaultDetectionPhysicalValue_DeLedRGBFaultDetectionPhysicalValue(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsIOM_inputTask10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpExtRCDLineRaw_DeExtRCDLineRaw(boolean data)
 *   Std_ReturnType Rte_Write_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(boolean data)
 *   Std_ReturnType Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_inputTask10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief IOM 10ms input task.
 *
 * This function is called periodically by the scheduler. This function should
 * manage the digital input pins: read the input pins state.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_inputTask10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_inputTask10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean IOM_pinState;
	boolean IOM_ProgramFlowSoftModeEnabled;

	IOM_CanDebug = 0U;

	(void)Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&IOM_ProgramFlowSoftModeEnabled);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (FALSE == IOM_ProgramFlowSoftModeEnabled)
	{
		(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP2);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	IOM_pinState = IOM_GetPinState(SUP_DI_RCD_LINE);
	(void)Rte_Write_PpExtRCDLineRaw_DeExtRCDLineRaw(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	IOM_pinState = IOM_GetPinState(SUP_DI_PHASE_L1);
	(void)Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InL1DetectionPhysicalValue(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_CanDebug |= (IOM_pinState<<0); /* PRQA S 2985 # Operation redundant. Kept for an easy reading */

	IOM_pinState = IOM_GetPinState(SUP_DI_PHASE_N);
	(void)Rte_Write_PpPhase1InDetectionPhysicalValue_DePhase1InNDetectionPhysicalValue(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_CanDebug |= (IOM_pinState<<1);

	IOM_pinState = IOM_GetPinState(SUP_DI_FAULT_OBC);
	(void)Rte_Write_PpFeedbackOBCFaultPhysicalValue_DeFeedbackOBCFaultPhysicalValue(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_CanDebug |= (IOM_pinState<<5);

	IOM_pinState = IOM_GetPinState(SUP_DI_FAULT_DCLV);
	(void)Rte_Write_PpFeedbackDCLVFaultPhysicalValue_DeFeedbackDCLVFaultPhysicalValue(IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_CanDebug |= (IOM_pinState<<6);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtHwAbsIOM_outputTask10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpFaulSignalProtocolRequestDCLV_DeFaulSignalProtocolRequestDCLV(boolean *data)
 *   Std_ReturnType Rte_Read_PpFaulSignalProtocolRequestOBC_DeFaulSignalProtocolRequestOBC(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean *data)
 *   Std_ReturnType Rte_Read_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(boolean *data)
 *   Std_ReturnType Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(boolean *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *   Std_ReturnType Rte_Call_general_Core0_ActivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_General_E_NOT_OK
 *   Std_ReturnType Rte_Call_general_Core0_DeactivateSupervisionEntity(WdgM_SupervisedEntityIdType SEID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_General_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_outputTask10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief IOM 10ms output task.
 *
 * This function is called periodically by the scheduler. This function should
 * manage output pins:  set the output state.
 * pins.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsIOM_CODE) RCtHwAbsIOM_outputTask10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtHwAbsIOM_outputTask10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint8 EnableLED_counter = 0U;
	/*static volatile WdgM_ViolationType IOM_WdgMLastViolation = 0U;*/

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	boolean IOM_pinState;
	boolean IOM_ProgramFlowSoftModeEnabled;

	(void)Rte_Read_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(&IOM_ProgramFlowSoftModeEnabled);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	if (FALSE == IOM_ProgramFlowSoftModeEnabled)
	{
		(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP5);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

	(void)Rte_Read_PpRelayS2PhysicalValue_DeRelayS2PhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_CTRL_PILOT,IOM_pinState);

	(void)Rte_Read_PpOutputEnableVCCDCLVPhysicalValue_DeOutputEnableVCCDCLVPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_VCC_DCLV,IOM_pinState);

	(void)Rte_Read_PpOutputEnableVCCOBCPhysicalValue_DeOutputEnableVCCOBCPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_VCC_OBC,IOM_pinState);

	(void)Rte_Read_PpOutputElockPhysicalValue_DeOutputElockLPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_ELOCK_L,IOM_pinState);
	IOM_CanDebug |= (IOM_pinState<<3);

	(void)Rte_Read_PpOutputElockPhysicalValue_DeOutputElockHPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_ELOCK_H,IOM_pinState);
	IOM_CanDebug |= (IOM_pinState<<2);

	(void)Rte_Read_PpOutputSupFaultToOBCPhysicalValue_DeOutputSupFaultToOBCPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_FAULT_OBC,IOM_pinState);

	(void)Rte_Read_PpOutputSupFaultToDCLVCPhysicalValue_DeOutputSupFaultToDCLVCPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_FAULT_DCLV,IOM_pinState);

	(void)Rte_Read_PpRCDLineOutputPhysicalValue_DeRCDLineOutputPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_WAKE_UP,IOM_pinState);
	IOM_CanDebug |= (IOM_pinState<<4);

	(void)Rte_Read_PiResetPLCPhysicalValue_DeResetPLCPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_NRESET_PLC,IOM_pinState);

	(void)Rte_Read_PpOutputDischargeDCLinkPhysicalValue_DeOutputDischargeDCLinkPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_DCLINK_DISCHARGE,IOM_pinState);

	(void)Rte_Read_PpOutputDischargeHVPhysicalValue_DeOutputDischargeHVPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	IOM_SetPinState(SUP_DO_BAT_DISCHARGE,IOM_pinState);
	
	(void)Rte_Read_PpEnableLedsPhysicalValue_DeEnableLedsPhysicalValue(&IOM_pinState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	if(0U<EnableLED_counter)
	{
		/*Ignore input*/
		EnableLED_counter--;
		IOM_SetPinState(SUP_DO_EN_LEDS,TRUE);
	}
	else if(FALSE==IOM_pinState)
	{
		/*Disable LEDs for during 10ms*/
		EnableLED_counter = IOM_LED_PERIODE;
		EnableLED_counter--;
		IOM_SetPinState(SUP_DO_EN_LEDS,FALSE);
	}
	else
	{
		/*LEDs enable*/
		IOM_SetPinState(SUP_DO_EN_LEDS,TRUE);
	}

	/* Delayed activation of ESR0 pin for watchdog reset to prevent wrong SW */
	/* Moved enable Reset pin to init function */

	IOM_ProcessDelayedProgramFlowActivation();


	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(3U,4U,IOM_CanDebug);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RPpSetDebugPinValue_OpSetDebugPinValue
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpSetDebugPinValue> of PortPrototype <PpSetDebugPinValue>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpSetDebugPinValue_OpSetDebugPinValue_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtHwAbsIOM_CODE) RPpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RPpSetDebugPinValue_OpSetDebugPinValue
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	IOM_Pins_t IOM_debugPIN;


	switch(debugPinID)
	{
		case DEBUG_PORT_ID_0:
			IOM_debugPIN = SUP_DO_DEBUG0;
			break;
		case DEBUG_PORT_ID_1:
			IOM_debugPIN = IOM_PIN_COUNT; //pin function has changed.
			break;
		case DEBUG_PORT_ID_2:
			IOM_debugPIN = SUP_DO_DEBUG2;
			break;
		default:
			IOM_debugPIN = IOM_PIN_COUNT;
			break;
	}

	if(IOM_PIN_COUNT>IOM_debugPIN)
	{
		IOM_SetPinState(IOM_debugPIN,debugPinValue);
	}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtHwAbsIOM_STOP_SEC_CODE
#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static boolean IOM_GetPinState(IOM_Pins_t pin)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*Check if it is an input.*/
	boolean retValue = FALSE;


	if((IOM_PIN_COUNT>pin)&&(IOM_INPUT==IOM_PinInformation[pin].Direction))
	{
			Dio_LevelType PinLevel = Dio_ReadChannel(IOM_PinInformation[pin].ChannelId);
			if((STD_HIGH==PinLevel)&&(IOM_ACTIVE_HIGH==IOM_PinInformation[pin].polarity))
			{
				retValue=TRUE;
			}
			else if ((STD_LOW==PinLevel)&&(IOM_ACTIVE_LOW==IOM_PinInformation[pin].polarity))
			{
				retValue=TRUE;
			}
			else
			{
				/*MiSRA*/
			}
	}
	return retValue;
}

static void IOM_SetPinState(IOM_Pins_t pin, boolean state)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	/*Check if it is an output.*/
	if((IOM_PIN_COUNT>pin)&&(IOM_OUTPUT==IOM_PinInformation[pin].Direction))
	{
		if(IOM_ACTIVE_HIGH==IOM_PinInformation[pin].polarity)
		{
			if(TRUE==state)
			{
				Dio_WriteChannel(IOM_PinInformation[pin].ChannelId,STD_HIGH);
			}
			else
			{
				Dio_WriteChannel(IOM_PinInformation[pin].ChannelId,STD_LOW);
			}
		}
		else if(IOM_ACTIVE_LOW==IOM_PinInformation[pin].polarity)
		{
			if(TRUE==state)
			{
				Dio_WriteChannel(IOM_PinInformation[pin].ChannelId,STD_LOW);
			}
			else
			{
				Dio_WriteChannel(IOM_PinInformation[pin].ChannelId,STD_HIGH);
			}
		}
		else
		{
			/*MISRA*/
		}

	}
}


static void IOM_ProcessDelayedProgramFlowActivation(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtHwAbsIOM_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtHwAbsIOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static uint32 IOM_ESR0DelayCounter = 0;

	#define CtHwAbsIOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtHwAbsIOM_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtHwAbsIOM_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtHwAbsIOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	if (IOM_ESR0DelayCounter < IOM_ESR0_WDG_DELAY_MAX_COUNT)
	{
		IOM_ESR0DelayCounter ++;
	}
	else if (IOM_ESR0_WDG_DELAY_MAX_COUNT == IOM_ESR0DelayCounter)
	{
		/* Activate Alive and ProgramFlow */
		(void)Rte_Call_general_Core0_ActivateSupervisionEntity(WdgMConf_WdgMSupervisedEntity_WdgMSupervisedEntity);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		(void)Rte_Call_general_Core0_ActivateSupervisionEntity(WdgMConf_WdgMSupervisedEntity_WdgMSupervisedEntityProgramFlow);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
		IOM_ESR0DelayCounter ++;
	}
	else
	{
		/* MISRA */
	}

}

static void IOM_EnableESR0_Pin(void)
{
/* PRQA S 0303 ++ # Direct access to uC registers to activate the reset pin. Access not possible through autosar MCAL */
	MCU_SFR_RUNTIME_RESETSAFETYENDINIT_TIMED(MCU_SAFETY_ENDINT_TIMEOUT);
	SCU_RSTCON.B.ESR0 = 1u;

	MCU_SFR_RUNTIME_SETSAFETYENDINIT_TIMED();
	P20_OUT.B.P7 = 1u;
	P20_IOCR4.B.PC7 = 0x10;
/* PRQA S 0303 -- */
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
