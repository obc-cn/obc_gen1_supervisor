/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApPCOM.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApPCOM
 *  Generation Time:  2020-11-23 11:42:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApPCOM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF *//* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup PCOM
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief PSA communication module.
 * \details This module manages the communication specifications from PSA.
 * There are:
 *
 * - Frame requirements as rolling counter or CRC.
 *
 * - Signal requirements as freeze the value...
 *
 * \{
 * \file  PCOM.c
 * \brief  Generic code of the PCOM module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * ComM_ModeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dem_UdsStatusByteType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * WdgM_CheckpointIdType
 *   uint16 represents integers with a minimum value of 0 and a maximum value of 65535.
 *      The order-relation on uint16 is: x < y if y - x is positive.
 *      uint16 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 1267, +10000.
 *
 *
 * Operation Prototypes:
 * =====================
 * ReleaseRUN of Port Interface EcuM_StateRequest
 *   Release of the RUN state.
 *
 * RequestRUN of Port Interface EcuM_StateRequest
 *   Request for the RUN state.
 *
 * CheckpointReached of Port Interface WdgM_AliveSupervision
 *   Indicates to the Watchdog Manager that a Checkpoint within a Supervised Entity has been reached.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApPCOM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0715 EOF # Limit control of nesting control structures made by CC calculation in UT */
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */

 /* PRQA S 0857,0380 ++ # Max amount of macros check */
/*---------------------------------------------------------------------------*/
/* INCLUDES                                                                  */
/*---------------------------------------------------------------------------*/

#ifdef TESSY_UT
#include "CtApPCOM_cfg.h"
#include "CtApPCOM_srv.h"
#include "CtApPCOM_cfg.h"
#include "WdgM.h"

#else
#include "CtApPCOM_cfg.h"
#include "CtApPCOM_srv.h"
#include "CtApPCOM_cfg.h"
#include "Mcu.h"
#include "WdgM.h"
#include "ComM.h"
#include "AppExternals.h"
#include "CanTrcv_30_Tja1145.h"
#include "ComM_EcuMBswM.h"

#include "CanSM.h"
#endif /* TESSY_UT */

/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/
/*! Signal not available value for U8.*/
#define PCOM_SNA_U8 		(uint8)0xFF
/*! Rolling counter module.*/
#define PCOM_RC_MODULE 		(uint8)0x0F
/*! Nibble mask.*/
#define PCOM_NIBBLE_MASK	(uint8)0x0F
/*! CAN data frame length.*/
#define PCOM_FRAME_LENGTH 	(uint8)8U
/*! Expected check sum result.*/
#define PCOM_CRC_EXPECTED	(uint8)0x0F
/*! PCOM task period.*/
#define PCOM_TASK_PERIOD	(uint8)5U

#define PCOM_MAX_SIGNAL_SIZE	(uint8)8U	/*PRQA S 3214 # Kept for possible future uses */

#define PCOM_C_MAX				(uint8)255U
#define PCOM_C_MIN				(uint8)0U
#define PCOM_MUTE_INC			(uint8)4U
#define PCOM_MUTE_DEC			(uint8)2U
#define PCOM_CBOFF_CONF			16U
#define PCOM_DIAGMUX_CAN_TYPEAUT_OFF	FALSE
#define PCOM_DIAGMUX_CAN_TYPEAUT_ON		TRUE		/*PRQA S 3214 # Kept for possible future uses */
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/* PRQA S 3453 ++ # The content of the macros is simple enough to justify not creatint a dedicated function */
#define PCOM_RX_setAvailabilityFlag(signalData,state)		(signalData)->data->availability=(state)
#define PCOM_RX_setDiagInvalidFault(signalData,state)		(signalData)->data->diagInvalidFault=(state)
#define PCOM_RX_setDiagForbidenFault(signalData,state)		(signalData)->data->diagForbidenFault=(state)
#define PCOM_RX_storeRaw(signalData,value)			(signalData)->data->rawValue=(*(pcom_gen_t*)(value))
#define PCOM_RX_storeMemorized(signalData,value)		(signalData)->data->memorizedValue=(* ((pcom_gen_t*)(value)))
#define PCOM_RX_storeSubstitutionVal(signalData,value)		(signalData)->data->substutionValue=(*((pcom_gen_t*)(value)))
#define PCOM_RX_signal_isInvalid(signalData, valueSignal)	((TRUE == (signalData)->get.invalid.isAvailable) && ((valueSignal) == signalData->get.invalid.value))
/* PRQA S 3453 --*/


#define PCOM_DEFAULT_INITIAL_VALUE	((pcom_gen_t)0)

#define PCOM_RX_PRESCALER				(2U)
/* PRQA S 3453 ++ # The content of the macros is simple enough to justify not creatint a dedicated function */
#define PCOM_RX_DIVIDE_PERIOD_BYPRESCALER(period)	(uint8)((period)/(uint8)(PCOM_RX_PRESCALER)) 
/* PRQA S 3453 --*/

#define		PCOM_FULLCOMM_MODE				(1)
#define		PCOM_INTEGRATION_MODE			(2)
 /* PRQA S 0857,0380 -- */
/*---------------------------------------------------------------------------*/
/* LOCAL TYPES                                                               */
/*---------------------------------------------------------------------------*/
/* Rule 2.3:  The identifier  is not used and could be removed*/
/* PRQA S 3205 ++ #These defines improve the clarity of the code even some
 * of them are not used*/

/*! RX frames faults.*/
typedef enum {
  /** Frame lost.*/
  PCOM_FAULT_FRAMELOST = 0U,
  /** Rolling counter mismatch.*/
  PCOM_FAULT_RC,
  /** Check sum mismatch.*/
  PCOM_FAULT_CHKSUM,
  /** Number of faults*/
  PCOM_FAULT_NUM
} PCOM_fault_t;
/*PRQA S 3205 --*/

/**
 *  Signal comparation with range limits
 */
typedef enum {
  PCOM_SIGNAL_RANGE_OK = 0U,/*!< Signal is in range */
  PCOM_SIGNAL_RANGE_OVER,   /*!< Signal is over range */
  PCOM_SIGNAL_RANGE_UNDER   /*!< Signal is under range */
} PCOM_signalRange_t;

typedef struct
{
	boolean fresh_frame_rcvd;
	uint8	data;
} PCOM_Integ_Mode_t;

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/

/* [Enrique.Bueno] Workaround START: MPU violation: PCOM is ASIL SWC, but COM Callbacks are executed in QM partition. ==================
                                     Hence, static variables defined at PCOM and written into a COM Callback will rise MPU violations.
                                     Contingency actions: remap all static variables affected from ASIL to QM partition taking profit
                                                          of TBD SWC (qm)                                                                 */
/* - Static non-init variables declaration ---------- */
#define CtApTBD_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Local TX frame data*/
static PCOM_TX_Frame_Data_t PCOM_TX_Frame_Data[PCOM_NUM_TX_FRAMES];
boolean PCOM_IntCANDiagnosticsEnable;

#define CtApTBD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApTBD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static uint8 PCOM_IntCANDebug1[8] = {0U,0U,0U,0U,0U,0U,0U,0U};
static uint8 PCOM_IntCANDebug2[8] = {0U,0U,0U,0U,0U,0U,0U,0U};
static uint8 PCOM_IntCANDebug3[8] = {0U,0U,0U,0U,0U,0U,0U,0U};
static uint8 PCOM_IntCANDebug4[8] = {0U,0U,0U,0U,0U,0U,0U,0U};
static boolean PCOM_C_Mute_INC_Flag = FALSE;
static boolean PCOM_C_Mute_DEC_Flag = FALSE;
static uint8 PCOM_BOREvents_count = 0U;

#define CtApTBD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApTBD_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#define CtApTBD_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
/* [Enrique.Bueno] Workaround STOP ====================================================================================================== */


/* - Static non-init variables declaration ---------- */
#define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*Dinamyc memory is not allow,*/
/** Debounce counter for all frames and all faults.*/
static uint32 PCOM_RX_DebounceCounter[PCOM_NUM_RX_FRAMES][PCOM_FAULT_NUM];

static PCOM_Integ_Mode_t PCOM_Integ_Mode;

/* [Enrique.Bueno] Workaround START: MPU violation: static variable moved from ASIL to QM partition */
/** Local TX frame data*/
//static PCOM_TX_Frame_Data_t PCOM_TX_Frame_Data[PCOM_NUM_TX_FRAMES];
/* [Enrique.Bueno] Workaround STOP */

/** Local RX frame data*/
static PCOM_RX_Frame_Data_t PCOM_RX_Frame_Data[PCOM_NUM_RX_FRAMES];
static uint8 PCOM_Debug[4] = {0,0,0,0};



static boolean PCOM_DiagCAN_typeAUT; 	/* PRQA S 3218 # Kept here for coherency */
static boolean PCOM_DiagMux_CAN_typeAUT;

static uint16 PCOM_C_Mute;

#define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

static boolean PCOM_Frame_NEW_JDD_55f_Received = FALSE;
static boolean PCOM_Frame_VCU_552_Received = FALSE;

static boolean PCOM_BusOffFault = FALSE;

static boolean PCOM_ProgramFlowSoftModeEnabled = FALSE;

#define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Set default init value to Integration mode */
static sint32 PCOM_CANComRequest = PCOM_INTEGRATION_MODE;


#define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/*!
 * \brief TX frame identifier,
 * \details This functions returns the frame index inside the configuration array.
 * this index is used to get some frame information.
 * \param[in] PCOM_id_input CAN frame ID.
 * \return TX frame index.
 */
static uint8 PCOM_TX_Frame_identifier(PduIdType PCOM_id_input);
/*!
 * \brief RX frame identifier,
 * \details This functions returns the frame index inside the configuration array.
 * this index is used to get some frame information.
 * \param[in] PCOM_id_input CAN frame ID.
 * \return RX frame index.
 */
static uint8 PCOM_RX_Frame_identifier(PduIdType PCOM_id_input);
/*!
 * \brief Fault debounce.
 * \details This function is used to perform a debounce under the PreFaults.
 * \param[in] PCOM_input PreFault value.
 * \param[in,out] PCOM_output Fault value.
 * \param[in] PCOM_confirmTime Time needed to confirm the PreFault.
 * \param[in] PCOM_healTime Time needed to heal the PreFault.
 * \param[in,out] PCOM_counter Pointer to the debounce counter.
 */
static void PCOM_RX_diagnosisDebounce(boolean PCOM_input, volatile boolean *PCOM_output,
				      uint32 PCOM_confirmTime, uint32 PCOM_healTime, uint32 *PCOM_counter);
/*!
 * \brief Write Nibble.
 * \details This function writes a specific nibble of the frame.
 * \param[in,out] frame_data Frame data.
 * \param[in] nibble_pos Nibble position.
 * \param[in] niebble_value Niebble value.
 */
static void PCOM_TX_Frame_WriteNibble(uint8 *frame_data, uint8 nibble_pos,
				      uint8 nibble_value);

/*!
 * \brief RX frame received indication sub-function: Frame lost.
 * \details This functions manages the Frame Lost fault, This function is called
 * after an RX frame has been received.
 * \param[in,out] PCOM_RX_lastDetection Time until last reception.
 * \param[in,out] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 */
static void PCOM_RX_indication_FrameLost(PCOM_RX_Frame_Data_t * PCOM_RX_frameData,
					 const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig);
/*!
 * \brief RX frame received indication sub-function: Rolling counter.
 * \details This functions manages the Rolling counter fault, This function is called
 * after an RX frame has been received.
 * \param[in,out] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 * \param[in] frame_data Data frame.
 */
static void PCOM_RX_indication_RC(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				  const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				  const volatile uint8 *frame_data);
/*!
 * \brief RX frame received indication sub-function: Check sum.
 * \details This functions manages the Check sum fault, This function is called
 * after an RX frame has been received.
 * \param[in,out] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 * \param[in] frame_data Data frame.
 */
static void PCOM_RX_indication_ChkSum(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				      const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				      const volatile uint8 *frame_data);

/*!
 * \brief RX task sub-function: Frame Lost.
 * \details This function manages the preFault debounce and generates the
 * Fault.
 * \param[in,out] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 * \param[in,out] PCOM_RX_lastDetection Time until last reception.
 * \param[in,out] PCOM_counter Pointer to the debounce counter.
 */
static void PCOM_RX_task_FrameLost(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				   const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				   uint32 *PCOM_DebounceCounter);
/*!
 * \brief RX task sub-function: Check sum.
 * \details This function manages the preFault debounce and generates the
 * Fault.
 * \param[in,out] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 * \param[in,out] PCOM_counter Pointer to the debounce counter.
 */
static void PCOM_RX_task_CheckSum(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				  const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				  uint32 *PCOM_DebounceCounter);
/*!
 * \brief RX task sub-function: Rolling counter.
 * \details This function manages the preFault debounce and generates the
 * Fault.
 * \param[in,out] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 * \param[in,out] PCOM_counter Pointer to the debounce counter.
 */
static void PCOM_RX_task_RC(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
			    const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
			    uint32 *PCOM_DebounceCounter);

/**
 * \brief RX task sub-function: Update signals.
 * \details This function updates the RX signals if the correct data has been
 * received.
 * \param[in] PCOM_RX_frameData Frame variables.
 * \param[in] PCOM_RX_frameConfig Frame information.
 */
static void PCOM_RX_task_SignalUpdate(const PCOM_RX_Frame_Config_t *PCOM_RX_frameConf,
				      PCOM_RX_Frame_Data_t *PCOM_RX_frameData);
/**
 * \brief RX task sub-function: Grid Diagnosis.
 * \details This functions sets the PCOM_gridDiagnosis flag if any fault is detected.
 *
 * \param[in] PCOM_RX_frameData Frame variables.
 * \param[out] PCOM_gridDiagnosis Pointer to PCOM_gridDiagnosis.
 * \param[in] PCOM_RX_frameConfig Frame information.
 */
static void PCOM_RX_task_GridDiagnosis(
    boolean (*PCOM_gridDiagnosis)[PCOM_CAN_NUMBER],
    PCOM_RX_Frame_Data_t PCOM_RX_frameData,
    PCOM_RX_Frame_Config_t PCOM_RX_frameConf);

/**
 * \brief Tx task sub-function: Update signals.
 * \details This function updates the TX signals if they must be sent.
 * \param[in] PCOM_TX_frameConfig Frame information.
 */
static void PCOM_TX_task_UpdateSignal(const
PCOM_TX_Frame_Config_t *frameConfig, PCOM_TX_Frame_Data_t *frameData);

/** Update calibratables*/
static void PCOM_UpdateCalibratables(void);

/**
 * \brief Tx init task sub-function: Initialize all frames signals
 * \details This function updates the TX signals with the Initial values from calibratables
 */
static void PCOM_TX_initializeFrames(void);

/**
 * \brief Tx init task sub-function: Initialize all signals inside a \p frameData
 * \details This function updates the TX signals with the Initial values
 * \param[in] frameData pointer to Frame data structure
 */

static void PCOM_TX_initializeFrameSignals(const PCOM_TX_Frame_Config_t * frameData);
/**
 * \brief Tx task sub-function: Process signals
 * \details This function updates the TX signals with with expected data
 * \param[in] frameSignals Pointer to vector of signals
 */
static void PCOM_TX_processSignals(const PCOM_TX_Frame_Signals_Config_t *frameSignals);


/**
 * \brief Tx parse sub-function: Parse inactive signals
 * \details This function updates the TX inactive signals
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_inactiveSignal(const PCOM_TX_Signals_Config_t * signalData);

/**
 * \brief Tx parse sub-function: Parse active signals
 * \details This function updates the TX active signals
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_activeSignal(const PCOM_TX_Signals_Config_t * signalData);


/**
 * \brief Bulks calculated signal to CAN signal
 * \details This function updates the CAN signal with calculated values
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_setCalculatedSignal(const PCOM_TX_Signals_Config_t * signalData);

/**
 * \brief Saturates lower signal value to CAN signal
 * \details This function updates the CAN signal with lower value
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_setLowerSignal(const PCOM_TX_Signals_Config_t * signalData);
/**
 * Saturates upper signal value to CAN signal
 * \details This function updates the CAN signal with upper value
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_setUpperSignal(const PCOM_TX_Signals_Config_t * signalData);

/**
 * \brief Bulks initial signal to CAN signal
 * \details This function updates the CAN signal with initial values
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_setInitialSignal(const PCOM_TX_Signals_Config_t * signalData);

/**
 * \brief Bulks reserved values to CAN signal
 * \details This function updates the CAN signal with reserved values
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_setReservedSignal(const PCOM_TX_Signals_Config_t * signalData);

/**
 * \brief Bulks Invalid values to CAN signal
 * \details This function updates the CAN signal with  Invalid values
 * \param[in] signalData structure with signal information
 */
static void PCOM_TX_setInvalidSignal(const PCOM_TX_Signals_Config_t * signalData);

/**
 * \brief Bulks \p valPtr function result into RTE signal
 * \details This function updates the RTE signal with the parsed \p valPtr function parsing it
 * \param[in] signalData structure with signal information
 * \param[in] pointer to function getting the value (size will be parsed)
 */
static Std_ReturnType PCOM_TX_bulkValue(const PCOM_TX_Signals_Config_t * signalData,const void *value);

/**
 * \brief Bulks \p value into RTE signal
 * \details This function updates the RTE signal with the parsed \p value
 * \param[in] signalData structure with signal information
 * \param[in] value pointer to value with data (size will be parsed)
 */
static Std_ReturnType PCOM_TX_bulkFunction(const PCOM_TX_Signals_Config_t * signalData, const getValFuncPtr valPtr);
static PCOM_signalRange_t PCOM_TX_signal_inRange(const PCOM_TX_Signals_Config_t *signalData, pcom_gen_t value);

/**
 * Initialize all RX signals in frame \p frameData
 * \param[in] frameData Configuration structure of frame
 */
static void PCOM_RX_initializeFrameSignals(const PCOM_RX_Frame_Config_t * frameData);
/**
 * Initialize RX frames
 */
static void PCOM_RX_initializeFrames(void);
/**
 * Set initial RX value in \p signalData
 * \param[in]signalData Configuration structure of signal
 */
static void PCOM_RX_setInitialSignal(const PCOM_RX_Signals_Config_t * signalData);
/**
 * Process Inactive RX signal \p signalData
 * \param[in] signalData Configuration structure of signal
 */
static void PCOM_RX_task_processSignals_inactive(const PCOM_RX_Signals_Config_t *signalData);
/**
 * Process normal RX signal flow
 * \param[in] signalData Configuration structure of signal
 */
static void PCOM_RX_task_processSignals_signalsFlow(const PCOM_RX_Signals_Config_t *signalData);
/**
 * Stores in output data from a function \p src
 * \param[in] signalData Configuration structure of signal
 * \param[in] src origin of data
 * \return Operation result
 */
static Std_ReturnType PCOM_RX_bulkFunction(const PCOM_RX_Signals_Config_t * signalData, PCOM_TX_orig_ptr_t src);
/**
 * Stores in output data from a value \p src
 * \param signalData Configuration structure of signal
 * \param[in] src origin of data
 * \return Operation result
 */
static Std_ReturnType PCOM_RX_bulkVal(const PCOM_RX_Signals_Config_t * signalData, PCOM_TX_orig_ptr_t src);
/**
 * Function to set Output signal typed
 * \param signalData Configuration structure of signal
 * \param[in] src origin of data
 * \return Operation result
 */
static Std_ReturnType PCOM_RX_setOutput(const PCOM_RX_Signals_Config_t * signalData, PCOM_TX_orig_ptr_t src);

/**
 * Process all signals in \p frameData
 * \param frameConfig[in] structure with frame configuration
 * \param frameData[in,out] structure with frame data
 */
static void PCOM_RX_processSignals(const PCOM_RX_Frame_Config_t * frameConfig, PCOM_RX_Frame_Data_t * frameData);

/**
 * Returns the \p function value with diagnostic Enable
 * \param[in] function pointer to enable results
 * \return enable status
 */
static boolean PCOM_RX_FramesGetDiagResult(PCOM_DiagEnableFunc_t function);

/**
 * Manages Output selector fault of \p signalData signal
 * \param[in,out] signalData Configuration structure of signal
 */
static void PCOM_RX_task_processSignals_Valid_OutputSelectorFault(const PCOM_RX_Signals_Config_t *signalData);
/**
 * Manages valid availability of \p signalData signal
 * \param[in,out] signalData Configuration structure of signal
 */
static void  PCOM_RX_task_processSignals_Valid_Availability(const PCOM_RX_Signals_Config_t *signalData);
/**
 * Determines whenever  value \p value is over or under range of the signal \p signalData
 * \param[in] signalData Configuration structure of signal
 * \param[in] value value to compare
 * \return value in range
 * \retval PCOM_SIGNAL_RANGE_OK Value is in range
 * \retval PCOM_SIGNAL_RANGE_UNDER Value is under range
 * \retval PCOM_SIGNAL_RANGE_OVER Value is over range
 */
static PCOM_signalRange_t PCOM_RX_signal_inRange(const PCOM_RX_Signals_Config_t *signalData, pcom_gen_t value);

/**
 * Process signal diagnostics
 * \param[in] frameConfig Structure with frame configuration
 * \param[in] frameData Structure with frame data
 * \param[in,out] signalData Structure with signal Data
 */
static void  PCOM_RX_task_processSignals_Diagnostics(const PCOM_RX_Frame_Config_t * frameConfig, PCOM_RX_Frame_Data_t * frameData, const PCOM_RX_Signals_Config_t *signalData);
/**
 * Process signal invalid diagnostics
 * \param[in] frameData Structure with frame data
 * \param[in] signalData Structure with signal Data
 */
static void  PCOM_RX_task_processSignals_Diagnostics_Invalid(const PCOM_RX_Frame_Config_t * frameConfig, const PCOM_RX_Signals_Config_t *signalData);
/**
 * Process signal forbidden diagnostics
 * \param[in] signalData Structure with signal Data
 */
static void  PCOM_RX_task_processSignals_Diagnostics_Forbidden(const PCOM_RX_Frame_Config_t * frameConfig, const PCOM_RX_Signals_Config_t *signalData);

/**
 * Process LID diagnostics
 * \param[in] frameData Structure with frame data
 * \param[in] signalData Structure with signal Data
 */
static void  PCOM_RX_task_Frame_Diagnostics_LID(PCOM_RX_Frame_Data_t * frameData,const PCOM_RX_Signals_Config_t *signalData);

/**
 * Reacts according with signals diagnostics
 * \param[in] frameConfig Structure with frame configuration
 * \param[in] frameData Structure with frame data
 * \param[in,out] signalData Structure with signal Data
 */
static void  PCOM_RX_task_processSignals_Diagnostics_Reaction(const PCOM_RX_Frame_Config_t * frameConfig, PCOM_RX_Frame_Data_t * frameData,  const PCOM_RX_Signals_Config_t *signalData);


/**
 * Publish DTC enable conditions for DGN
 */
static void PCOM_Publish_DTC_EnableConditions(void);

/**
 * Send all DTC triggers for Frame and Signal errors
 */
static void PCOM_SendRX_DTC_Triggers(void);

/**
 * Generate the diagnostic enable conditions for frames and signals.
 */
static void PCOM_Generate_DGN_conditions(void);

static void PCOM_Serialize_NVM(void);

static void PCOM_Deserialize_NVM(void);

static void PCOM_UpdatePIMs(void);

static void PCOM_BusOff_Evaluation(void);

static void PCOM_CMute_Evaluation(void);

static void PCOM_CMute_Init(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * ABS_VehSpd: Integer in interval [0...65535]
 *   Unit: [km/h], Factor: 0.01, Offset: 0
 * ABS_VehSpdValidFlag: Boolean
 * BMS_AuxBattVolt: Integer in interval [0...4095]
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * BMS_Fault: Integer in interval [0...4095]
 * BMS_HighestChargeCurrentAllow: Integer in interval [0...2047]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * BMS_HighestChargeVoltageAllow: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * BMS_OnBoardChargerEnable: Boolean
 * BMS_QuickChargeConnectorState: Boolean
 * BMS_RelayOpenReq: Boolean
 * BMS_SOC: Integer in interval [0...1023]
 * BMS_Voltage: Integer in interval [0...500]
 * BSI_PostDriveWakeup: Boolean
 * BSI_PreDriveWakeup: Boolean
 * BSI_VCUSynchroGPC: Boolean
 * COUPURE_CONSO_CTP: Boolean
 * COUPURE_CONSO_CTPE2: Boolean
 * DATA0: Integer in interval [0...255]
 * DATA1: Integer in interval [0...255]
 * DATA2: Integer in interval [0...255]
 * DATA3: Integer in interval [0...255]
 * DATA4: Integer in interval [0...255]
 * DATA5: Integer in interval [0...255]
 * DATA6: Integer in interval [0...255]
 * DATA7: Integer in interval [0...255]
 * DATA_ACQ_JDD_BSI_2: Integer in interval [0...255]
 * DCDC_CurrentReference: Integer in interval [0...2047]
 * DCDC_HighVoltConnectionAllowed: Boolean
 * DCDC_InputCurrent: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCDC_InputVoltage: Integer in interval [0...500]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCDC_OBCDCDCRTPowerLoad: Integer in interval [0...255]
 * DCDC_OVERTEMP: Boolean
 * DCDC_OutputCurrent: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCDC_OutputVoltage: Integer in interval [0...255]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCDC_Temperature: Integer in interval [0...255]
 * DCDC_VoltageReference: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_ADC_IOUT: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCHV_ADC_NTC_MOD_5: Integer in interval [0...255]
 * DCHV_ADC_NTC_MOD_6: Integer in interval [0...255]
 * DCHV_ADC_VOUT: Integer in interval [0...65535]
 *   Unit: [V], Factor: 0.1, Offset: 0
 * DCHV_Command_Current_Reference: Integer in interval [0...2047]
 * DCHV_IOM_ERR_CAP_FAIL_H: Boolean
 * DCHV_IOM_ERR_CAP_FAIL_L: Boolean
 * DCHV_IOM_ERR_OC_IOUT: Boolean
 * DCHV_IOM_ERR_OC_NEG: Boolean
 * DCHV_IOM_ERR_OC_POS: Boolean
 * DCHV_IOM_ERR_OT: Boolean
 * DCHV_IOM_ERR_OT_IN: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD5: Boolean
 * DCHV_IOM_ERR_OT_NTC_MOD6: Boolean
 * DCHV_IOM_ERR_OV_VOUT: Boolean
 * DCHV_IOM_ERR_UV_12V: Boolean
 * DCLV_Applied_Derating_Factor: Integer in interval [0...2047]
 * DCLV_Input_Current: Integer in interval [0...255]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Input_Voltage: Integer in interval [0...1000]
 *   Unit: [V], Factor: 1, Offset: 0
 * DCLV_Measured_Current: Integer in interval [0...4095]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * DCLV_Measured_Voltage: Integer in interval [0...175]
 *   Unit: [V], Factor: 0.1, Offset: 4
 * DCLV_OC_A_HV_FAULT: Boolean
 * DCLV_OC_A_LV_FAULT: Boolean
 * DCLV_OC_B_HV_FAULT: Boolean
 * DCLV_OC_B_LV_FAULT: Boolean
 * DCLV_OT_FAULT: Boolean
 * DCLV_OV_INT_A_FAULT: Boolean
 * DCLV_OV_INT_B_FAULT: Boolean
 * DCLV_OV_UV_HV_FAULT: Boolean
 * DCLV_PWM_STOP: Boolean
 * DCLV_Power: Integer in interval [0...2500]
 * DCLV_T_L_BUCK: Integer in interval [0...255]
 * DCLV_T_PP_A: Integer in interval [0...255]
 * DCLV_T_PP_B: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_A: Integer in interval [0...255]
 * DCLV_T_SW_BUCK_B: Integer in interval [0...255]
 * DCLV_T_TX_PP: Integer in interval [0...255]
 * DCLV_Temp_Derating_Factor: Integer in interval [0...2047]
 * DCLV_VoltageReference: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * DIAG_INTEGRA_ELEC: Boolean
 * Debug1_1: Integer in interval [0...255]
 * Debug1_2: Integer in interval [0...255]
 * Debug1_3: Integer in interval [0...255]
 * Debug1_4: Integer in interval [0...255]
 * Debug1_5: Integer in interval [0...255]
 * Debug1_6: Integer in interval [0...255]
 * Debug1_7: Integer in interval [0...255]
 * Debug2_0: Integer in interval [0...255]
 * Debug2_1: Integer in interval [0...255]
 * Debug2_2: Integer in interval [0...255]
 * Debug2_3: Integer in interval [0...255]
 * Debug2_4: Integer in interval [0...255]
 * Debug2_5: Integer in interval [0...255]
 * Debug2_6: Integer in interval [0...255]
 * Debug2_7: Integer in interval [0...255]
 * Debug3_0: Integer in interval [0...255]
 * Debug3_1: Integer in interval [0...255]
 * Debug3_2: Integer in interval [0...255]
 * Debug3_3: Integer in interval [0...255]
 * Debug3_4: Integer in interval [0...255]
 * Debug3_5: Integer in interval [0...255]
 * Debug3_6: Integer in interval [0...255]
 * Debug3_7: Integer in interval [0...255]
 * Debug4_0: Integer in interval [0...255]
 * Debug4_1: Integer in interval [0...255]
 * Debug4_2: Integer in interval [0...255]
 * Debug4_3: Integer in interval [0...255]
 * Debug4_4: Integer in interval [0...255]
 * Debug4_5: Integer in interval [0...255]
 * Debug4_6: Integer in interval [0...255]
 * Debug4_7: Integer in interval [0...255]
 * Degug1_0: Integer in interval [0...255]
 * EDITION_CALIB: Integer in interval [0...255]
 * EDITION_SOFT: Integer in interval [0...255]
 * EFFAC_DEFAUT_DIAG: Boolean
 * EVSE_RTAB_STOP_CHARGE: Boolean
 * IdtFrameDiagTime: Integer in interval [0...255]
 * IdtSignalDiagTime: Integer in interval [0...255]
 * IdtT_INHIB_DIAG_COM: Integer in interval [0...51]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTimeBeforeRunningDiag: Integer in interval [0...51]
 *   Unit: [ms], Factor: 100, Offset: 0
 * IdtTo_DgMux_Prod: Integer in interval [0...51]
 *   Unit: [ms], Factor: 100, Offset: 0
 * MODE_DIAG: Boolean
 * NEW_JDD_OBC_DCDC_BYTE_0: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_1: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_2: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_3: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_4: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_5: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_6: Integer in interval [0...255]
 * NEW_JDD_OBC_DCDC_BYTE_7: Integer in interval [0...255]
 * OBC_CommunicationSt: Boolean
 * OBC_CoolingWakeup: Boolean
 * OBC_DCChargingPlugAConnConf: Boolean
 * OBC_HVBattRechargeWakeup: Boolean
 * OBC_HighVoltConnectionAllowed: Boolean
 * OBC_HoldDiscontactorWakeup: Boolean
 * OBC_OBCStartSt: Boolean
 * OBC_OBCTemp: Integer in interval [0...255]
 * OBC_OutputCurrent: Integer in interval [0...511]
 * OBC_OutputVoltage: Integer in interval [0...8191]
 * OBC_PIStateInfoWakeup: Boolean
 * OBC_PlugVoltDetection: Boolean
 * OBC_PowerMax: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * OBC_PushChargeType: Boolean
 * OBC_SocketTempL: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * OBC_SocketTempN: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * PFC_IOM_ERR_OC_PH1: Boolean
 * PFC_IOM_ERR_OC_PH2: Boolean
 * PFC_IOM_ERR_OC_PH3: Boolean
 * PFC_IOM_ERR_OC_SHUNT1: Boolean
 * PFC_IOM_ERR_OC_SHUNT2: Boolean
 * PFC_IOM_ERR_OC_SHUNT3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M1: Boolean
 * PFC_IOM_ERR_OT_NTC1_M3: Boolean
 * PFC_IOM_ERR_OT_NTC1_M4: Boolean
 * PFC_IOM_ERR_OV_DCLINK: Boolean
 * PFC_IOM_ERR_OV_VPH12: Boolean
 * PFC_IOM_ERR_OV_VPH23: Boolean
 * PFC_IOM_ERR_OV_VPH31: Boolean
 * PFC_IOM_ERR_UVLO_ISO4: Boolean
 * PFC_IOM_ERR_UVLO_ISO7: Boolean
 * PFC_IOM_ERR_UV_12V: Boolean
 * PFC_IPH1_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH2_RMS_0A1: Integer in interval [0...1023]
 * PFC_IPH3_RMS_0A1: Integer in interval [0...1023]
 * PFC_Temp_M1_C: Integer in interval [0...255]
 * PFC_Temp_M3_C: Integer in interval [0...255]
 * PFC_Temp_M4_C: Integer in interval [0...255]
 * PFC_VPH12_Peak_V: Integer in interval [0...1023]
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH1_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH23_Peak_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH2_Freq_Hz: Integer in interval [0...1023]
 * PFC_VPH31_Peak_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * PFC_VPH3_Freq_Hz: Integer in interval [0...1023]
 * PFC_Vdclink_V: Integer in interval [0...1023]
 * Rte_DT_IdtPCOMNvMArray_0: Integer in interval [0...255]
 * SUPV_CoolingWupState: Boolean
 * SUPV_DTCRegistred: Boolean
 * SUPV_HVBattChargeWupState: Boolean
 * SUPV_HoldDiscontactorWupState: Boolean
 * SUPV_PIStateInfoWupState: Boolean
 * SUPV_PostDriveWupState: Boolean
 * SUPV_PreDriveWupState: Boolean
 * SUPV_PrecondElecWupState: Boolean
 * SUPV_RCDLineState: Boolean
 * SUPV_StopDelayedHMIWupState: Boolean
 * SUP_CommandVDCLink_V: Integer in interval [0...1023]
 *   Unit: [V], Factor: 1, Offset: 0
 * VCU_AccPedalPosition: Integer in interval [0...100]
 * VCU_ActivedischargeCommand: Boolean
 * VCU_CDEAccJDD: Boolean
 * VCU_CompteurRazGct: Integer in interval [0...253]
 * VCU_CptTemporel: Integer in interval [0...4294967295]
 * VCU_DCDCVoltageReq: Integer in interval [0...127]
 *   Unit: [V], Factor: 0.05, Offset: 10.6
 * VCU_DDEGMVSEEM: Integer in interval [0...100]
 * VCU_DMDActivChiller: Boolean
 * VCU_DMDMeap2SEEM: Integer in interval [0...100]
 * VCU_DiagMuxOnPwt: Boolean
 * VCU_ElecMeterSaturation: Boolean
 * VCU_Kilometrage: Integer in interval [0...16777214]
 * VCU_PrecondElecWakeup: Boolean
 * VCU_StopDelayedHMIWakeup: Boolean
 * VERSION_APPLI: Integer in interval [0...255]
 * VERSION_SOFT: Integer in interval [0...255]
 * VERSION_SYSTEME: Integer in interval [0...255]
 * VERS_DATE2_ANNEE: Integer in interval [0...99]
 * VERS_DATE2_JOUR: Integer in interval [0...31]
 * VERS_DATE2_MOIS: Integer in interval [0...12]
 * WdgM_CheckpointIdType: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * sint32: Integer in interval [-2147483648...2147483647] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_CC2_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_CC2_invalid_not_connected (1U)
 *   Cx2_CC2_valid_full_connected (2U)
 *   Cx3_CC2_invalid_half_connected (3U)
 * BMS_FastChargeSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_not_in_charging (0U)
 *   Cx1_charging (1U)
 *   Cx2_charging_fault (2U)
 *   Cx3_charging_finished (3U)
 * BMS_MainConnectorState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_contactors_opened (0U)
 *   Cx1_precharge (1U)
 *   Cx2_contactors_closed (2U)
 *   Cx3_Invalid (3U)
 * BMS_SlowChargeSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_not_in_charging (0U)
 *   Cx1_charging (1U)
 *   Cx2_charging_fault (2U)
 *   Cx3_charging_finished (3U)
 * BSI_ChargeState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_ON (0U)
 *   Cx1_OFF_not_ended (1U)
 *   Cx2_OFF_ended (2U)
 *   Cx3_Unavailable (3U)
 * BSI_ChargeTypeStatus: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_DIFFEREE (1U)
 *   Cx2_IMMEDIATE (2U)
 * BSI_LockedVehicle: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_Locked (0U)
 *   Cx1_Locked (1U)
 *   Cx2_Overlocked (2U)
 *   Cx3_Reserved (3U)
 * BSI_MainWakeup: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_RCD (0U)
 *   Cx1_No_main_wake_up_request (1U)
 *   Cx2_Main_wake_up_request (2U)
 *   Cx3_Not_valid (3U)
 * BSI_VCUHeatPumpWorkingMode: Enumeration of integer in interval [0...31] with enumerators
 *   Cx00_Stop_mode (0U)
 *   Cx01_Failure_mode_1 (1U)
 *   Cx02_Failure_mode_2 (2U)
 *   Cx03_Failure_mode_3 (3U)
 *   Cx04_Failure_mode_4 (4U)
 *   Cx05_Failure_mode_5 (5U)
 *   Cx06_Failure_mode_6 (6U)
 *   Cx07_Failure_mode_7 (7U)
 *   Cx08_AC_mode (8U)
 *   Cx09_HP_mode (9U)
 *   Cx0A_Deshum_total_heating_mode (10U)
 *   Cx0B_Deshum_simple_heating_mode (11U)
 *   Cx0C_De_icing_mode_ (12U)
 *   Cx0D_transition_mode (13U)
 *   Cx0E_Failure_mode (14U)
 *   Cx0F_Chiller_alone_mode (15U)
 *   Cx10_AC_chiffler_mode (16U)
 *   Cx11_Deshum_Chiller_mode (17U)
 *   Cx12_Reserved (18U)
 *   Cx13_Water_Heather_mode (19U)
 *   Cx14_Reserved (20U)
 *   Cx15_Reserved (21U)
 *   Cx16_Reserved (22U)
 *   Cx17_Reserved (23U)
 *   Cx18_Reserved (24U)
 *   Cx19_Reserved (25U)
 *   Cx1A_Reserved (26U)
 *   Cx1B_Reserved (27U)
 *   Cx1C_Reserved (28U)
 *   Cx1D_Reserved (29U)
 *   Cx1E_Reserved (30U)
 *   Cx1F_Reserved (31U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * DCDC_ActivedischargeSt: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_not_in_active_discharge (0U)
 *   Cx1_active_discharging (1U)
 *   Cx2_discharge_completed (2U)
 *   Cx3_discharge_failure (3U)
 * DCDC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   DCDC_FAULT_NO_FAULT (0U)
 *   DCDC_FAULT_LEVEL_1 (64U)
 *   DCDC_FAULT_LEVEL_2 (128U)
 *   DCDC_FAULT_LEVEL_3 (192U)
 * DCDC_FaultLampRequest: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_fault (0U)
 *   Cx1_Warning_limited_performance (1U)
 *   Cx2_Short_time_fault_Need_reset (2U)
 *   Cx3_Permernant_fault_need_replaced (3U)
 * DCDC_OBCMainContactorReq: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_Request_to_Open_the_Contactors (0U)
 *   Cx1_Request_to_Close_the_Contactors (1U)
 *   Cx2_No_request (2U)
 *   Cx3_Reserved (3U)
 * DCDC_OBCQuickChargeContactorReq: Enumeration of integer in interval [0...255] with enumerators
 *   Cx0_Request_to_Open_the_Contactors (0U)
 *   Cx1_Request_to_Close_the_Contactors (1U)
 *   Cx2_No_request (2U)
 * DCDC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * DCHV_DCHVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATUS_STANDBY (0U)
 *   Cx1_DCHV_STATUS_RUN (1U)
 *   Cx2_DCHV_STATUS_ERROR (2U)
 *   Cx3_DCHV_STATUS_ALARM (3U)
 *   Cx4_DCHV_STATUS_SAFE (4U)
 * DCLV_DCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_STANDBY (0U)
 *   Cx1_RUN (1U)
 *   Cx2_ERROR (2U)
 *   Cx3_ALARM (3U)
 *   Cx4_SAFE (4U)
 *   Cx5_DERATED (5U)
 *   Cx6_SHUTDOWN (6U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_UDS_STATUS_TF (1U)
 *   DEM_UDS_STATUS_TF_BflMask 1U (0b00000001)
 *   DEM_UDS_STATUS_TF_BflPn 0
 *   DEM_UDS_STATUS_TF_BflLn 1
 *   DEM_UDS_STATUS_TFTOC (2U)
 *   DEM_UDS_STATUS_TFTOC_BflMask 2U (0b00000010)
 *   DEM_UDS_STATUS_TFTOC_BflPn 1
 *   DEM_UDS_STATUS_TFTOC_BflLn 1
 *   DEM_UDS_STATUS_PDTC (4U)
 *   DEM_UDS_STATUS_PDTC_BflMask 4U (0b00000100)
 *   DEM_UDS_STATUS_PDTC_BflPn 2
 *   DEM_UDS_STATUS_PDTC_BflLn 1
 *   DEM_UDS_STATUS_CDTC (8U)
 *   DEM_UDS_STATUS_CDTC_BflMask 8U (0b00001000)
 *   DEM_UDS_STATUS_CDTC_BflPn 3
 *   DEM_UDS_STATUS_CDTC_BflLn 1
 *   DEM_UDS_STATUS_TNCSLC (16U)
 *   DEM_UDS_STATUS_TNCSLC_BflMask 16U (0b00010000)
 *   DEM_UDS_STATUS_TNCSLC_BflPn 4
 *   DEM_UDS_STATUS_TNCSLC_BflLn 1
 *   DEM_UDS_STATUS_TFSLC (32U)
 *   DEM_UDS_STATUS_TFSLC_BflMask 32U (0b00100000)
 *   DEM_UDS_STATUS_TFSLC_BflPn 5
 *   DEM_UDS_STATUS_TFSLC_BflLn 1
 *   DEM_UDS_STATUS_TNCTOC (64U)
 *   DEM_UDS_STATUS_TNCTOC_BflMask 64U (0b01000000)
 *   DEM_UDS_STATUS_TNCTOC_BflPn 6
 *   DEM_UDS_STATUS_TNCTOC_BflLn 1
 *   DEM_UDS_STATUS_WIR (128U)
 *   DEM_UDS_STATUS_WIR_BflMask 128U (0b10000000)
 *   DEM_UDS_STATUS_WIR_BflPn 7
 *   DEM_UDS_STATUS_WIR_BflLn 1
 * IdtBatteryVoltageState: Enumeration of integer in interval [0...2] with enumerators
 *   BAT_VALID_RANGE (0U)
 *   BAT_OVERVOLTAGE (1U)
 *   BAT_UNDERVOLTAGE (2U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * OBC_ACRange: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_No_AC_10V_ (0U)
 *   Cx1_110V_85_132V_ (1U)
 *   Cx2_Invalid (2U)
 *   Cx3_220V_170_265V_ (3U)
 *   Cx4_Reserved (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_CP_connection_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Invalid_value (0U)
 *   Cx1_CP_invalid_not_connected (1U)
 *   Cx2_CP_valid_full_connected (2U)
 *   Cx3_CP_invalid_half_connected (3U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_ElockState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Elock_unlocked (0U)
 *   Cx1_Elock_locked (1U)
 *   Cx2_E_lock_lock_fail (2U)
 *   Cx3_E_Lock_unlock_fail (3U)
 * OBC_Fault: Enumeration of integer in interval [0...255] with enumerators
 *   OBC_FAULT_NO_FAULT (0U)
 *   OBC_FAULT_LEVEL_1 (64U)
 *   OBC_FAULT_LEVEL_2 (128U)
 *   OBC_FAULT_LEVEL_3 (192U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 * OBC_RechargeHMIState: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Disconnected (0U)
 *   Cx1_In_progress (1U)
 *   Cx2_Failure (2U)
 *   Cx3_Stopped (3U)
 *   Cx4_Finished (4U)
 *   Cx5_Reserved (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * OBC_Status: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_off_mode (0U)
 *   Cx1_Init_mode (1U)
 *   Cx2_standby_mode (2U)
 *   Cx3_conversion_working_ (3U)
 *   Cx4_error_mode (4U)
 *   Cx5_degradation_mode (5U)
 *   Cx6_reserved (6U)
 *   Cx7_invalid (7U)
 * PFC_PFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 *   Cx6_PFC_STATE_START_TRI (6U)
 *   Cx7_PFC_STATE_START_MONO (7U)
 *   Cx8_PFC_STATE_ERROR (8U)
 *   Cx9_PFC_STATE_DIRECTPWM (9U)
 * SUPV_ECUElecStateRCD: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Partial_wakeup (0U)
 *   Cx1_Internal_wakeup (1U)
 *   Cx2_Transitory (2U)
 *   Cx3_Nominal_main_wakeup (3U)
 *   Cx4_Degraded_main_wakeup (4U)
 *   Cx5_Shutdown_preparation (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * SUP_RequestDCLVStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCLV_STATE_STANDBY (0U)
 *   Cx1_DCLV_STATE_RUN (1U)
 *   Cx2_DCLV_STATE_RESET_ERROR (2U)
 *   Cx3_DCLV_STATE_DISCHARGE (3U)
 *   Cx4_DCLV_STATE_SHUTDOWN (4U)
 * SUP_RequestPFCStatus: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PFC_STATE_STANDBY (0U)
 *   Cx1_PFC_STATE_TRI (1U)
 *   Cx2_PFC_STATE_MONO (2U)
 *   Cx3_PFC_STATE_RESET_ERROR (3U)
 *   Cx4_PFC_STATE_DISCHARGE (4U)
 *   Cx5_PFC_STATE_SHUTDOWN (5U)
 * SUP_RequestStatusDCHV: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_DCHV_STATE_STANDBY (0U)
 *   Cx1_DCHV_STATE_RUN (1U)
 *   Cx2_DCHV_STATE_RESET_ERROR (2U)
 *   Cx3_DCHV_STATE_DISCHARGE (3U)
 *   Cx4_DCHV_STATE_SHUTDOWN (4U)
 * VCU_CDEApcJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_APC_unchanged (0U)
 *   Cx1_APC_unchanged (1U)
 *   Cx2_APC_OFF (2U)
 *   Cx3_APC_ON_ (3U)
 * VCU_DCDCActivation: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Not_used (0U)
 *   Cx1_DCDC_Off (1U)
 *   Cx2_DCDC_On (2U)
 *   Cx3_Unavailable_Value (3U)
 * VCU_EPWT_Status: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_EPWT_invalid (0U)
 *   Cx1_inactive (1U)
 *   Cx2_active_no_speed_ (2U)
 *   Cx3_EPWT_is_running_with_speed (3U)
 * VCU_EtatGmpHyb: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_PWT_inactive (0U)
 *   Cx1_reserved (1U)
 *   Cx2_reserved (2U)
 *   Cx3_reserved (3U)
 *   Cx4_PWT_activation (4U)
 *   Cx5_reserved (5U)
 *   Cx6_reserved (6U)
 *   Cx7_reserved (7U)
 *   Cx8_active_PWT (8U)
 *   Cx9_reserved (9U)
 *   CxA_PWT_desactivation (10U)
 *   CxB_reserved (11U)
 *   CxC_PWT_in_hold_after_desactivation (12U)
 *   CxD_reserved (13U)
 *   CxE_PWT_at_rest (14U)
 *   CxF_reserved (15U)
 * VCU_EtatPrincipSev: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Stop (0U)
 *   Cx1_Contact (1U)
 *   Cx2_Start (2U)
 *   Cx3_Reserved (3U)
 * VCU_EtatReseauElec: Enumeration of integer in interval [0...15] with enumerators
 *   Cx0_TI_NOMINAL (0U)
 *   Cx1_TI_DEGRADE (1U)
 *   Cx2_TI_DISPO_DEM (2U)
 *   Cx3_DEMARRAGE (3U)
 *   Cx4_REDEMARRAGE (4U)
 *   Cx5_TA_NOMINAL (5U)
 *   Cx6_TA_DEGRADE (6U)
 *   Cx7_TA_SECU (7U)
 *   Cx8_Reserved (8U)
 *   Cx9_Reserved (9U)
 *   CxA_Reserved (10U)
 *   CxB_Reserved (11U)
 *   CxC_Reserved (12U)
 *   CxD_Reserved (13U)
 *   CxE_Reserved (14U)
 *   CxF_Reserved (15U)
 * VCU_Keyposition: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Key_OFF (0U)
 *   Cx1_Key_ACC (1U)
 *   Cx2_Key_ON (2U)
 *   Cx3_Key_START (3U)
 * VCU_ModeEPSRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_OFF (0U)
 *   Cx1_Active_Drive (1U)
 *   Cx2_Discharge (2U)
 *   Cx3_PI_Charge (3U)
 *   Cx4_PI_Balance (4U)
 *   Cx5_PI_Discharge (5U)
 *   Cx6_Reserved (6U)
 *   Cx7_Reserved (7U)
 * VCU_PosShuntJDD: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Park (0U)
 *   Cx1_Client (1U)
 *   Cx2_Absent (2U)
 *   Cx3_Undetermined (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * IdtPCOMNvMArray: Array with 16 element(s) of type Rte_DT_IdtPCOMNvMArray_0
 *
 * Record Types:
 * =============
 * SG_DC2: Record with elements
 *   DCDC_ActivedischargeSt of type DCDC_ActivedischargeSt
 *   DCDC_InputCurrent of type DCDC_InputCurrent
 *   DCDC_OBCDCDCRTPowerLoad of type DCDC_OBCDCDCRTPowerLoad
 *   DCDC_OBCMainContactorReq of type DCDC_OBCMainContactorReq
 *   DCDC_OBCQuickChargeContactorReq of type DCDC_OBCQuickChargeContactorReq
 *   DCDC_OutputCurrent of type DCDC_OutputCurrent
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint32 *Rte_Pim_PimBMS1_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimBMS3_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimBMS5_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimBMS6_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimBMS8_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimBMS9_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimBSIInfo_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimCtrlDCDC_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimParkCommand_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimVCU2_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimVCU3_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimVCU_552_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimVCU_BSI_Wakeup_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimVCU_PCANInfo_LostFrameCounter(void)
 *   uint32 *Rte_Pim_PimVCU_TU_LostFrameCounter(void)
 *   uint16 *Rte_Pim_PimABS_VehSpd_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimABS_VehSpd_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimBMS_SOC_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimBMS_SOC_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimBMS_SOC_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimBMS_SOC_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimBMS_Voltage_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimBMS_Voltage_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimKILOMETRAGE_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimKILOMETRAGE_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterHeal(void)
 *   uint16 *Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm(void)
 *   uint16 *Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal(void)
 *   boolean *Rte_Pim_PimABS_VehSpd_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimBMS1_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBMS3_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBMS5_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBMS6_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBMS8_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBMS9_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBMS_MainConnectorState_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimBMS_SOC_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimBMS_SOC_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimBMS_Voltage_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimBMS_Voltage_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimBSIInfo_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimCPT_TEMPOREL_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimCtrlDCDC_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimDDE_GMV_SEEM_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimETAT_GMP_HYB_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimKILOMETRAGE_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimParkCommand_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimVCU2_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimVCU3_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimVCU_552_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_AccPedalPosition_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_BSI_Wakeup_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimVCU_DCDCActivation_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_EPWT_Status_InvalidValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault(void)
 *   boolean *Rte_Pim_PimVCU_PCANInfo_LostFramePrefault(void)
 *   boolean *Rte_Pim_PimVCU_TU_LostFramePrefault(void)
 *   Rte_DT_IdtPCOMNvMArray_0 *Rte_Pim_NvPCOMBlockNeed_MirrorBlock(void)
 *     Returnvalue: Rte_DT_IdtPCOMNvMArray_0* is of type IdtPCOMNvMArray
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   uint32 Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_REFERENCE_HORAIRE(void)
 *   uint32 Rte_CData_CalSignal_VCU_552_DefaultSubstValue_CPT_TEMPOREL(void)
 *   uint32 Rte_CData_CalSignal_VCU_552_DefaultSubstValue_KILOMETRAGE(void)
 *   uint32 Rte_CData_CalSignal_VCU_552_Initial_Value_CPT_TEMPOREL(void)
 *   uint16 Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_SOC(void)
 *   uint16 Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_Voltage(void)
 *   uint16 Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_SOC(void)
 *   uint16 Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_Voltage(void)
 *   uint16 Rte_CData_CalSignal_BMS3_127_DefaultSubstValue_BMS_AuxBattVolt(void)
 *   uint16 Rte_CData_CalSignal_BMS3_127_Initial_Value_BMS_AuxBattVolt(void)
 *   uint16 Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeCurrentAllow(void)
 *   uint16 Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_HighestChargeVoltageAllow(void)
 *   uint16 Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeCurrentAllow(void)
 *   uint16 Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_HighestChargeVoltageAllow(void)
 *   uint16 Rte_CData_CalSignal_BMS9_129_DefaultSubstValue_BMS_DC_RELAY_VOLTAGE(void)
 *   uint16 Rte_CData_CalSignal_BMS9_129_Initial_Value_BMS_DC_RELAY_VOLTAGE(void)
 *   uint16 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_InputVoltage(void)
 *   uint16 Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_OutputCurrent(void)
 *   uint16 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputCurrent(void)
 *   uint16 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OutputVoltage(void)
 *   uint16 Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpd(void)
 *   uint16 Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpd(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_0F0_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_0F0_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_125_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_125_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_127_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_127_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_129_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_129_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_17B_ChkSumConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_17B_ChkSumHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_17B_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_17B_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_17B_RCConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_17B_RCHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_27A_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_27A_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_31B_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_31B_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_31E_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_31E_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_359_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_359_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_361_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_361_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_372_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_372_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_37E_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_37E_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_382_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_382_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_486_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_486_LostFrameHeal(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_552_LostFrameConfirm(void)
 *   IdtFrameDiagTime Rte_CData_CalFrame_552_LostFrameHeal(void)
 *   uint8 Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_MainConnectorState(void)
 *   uint8 Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_QuickChargeConnectorState(void)
 *   uint8 Rte_CData_CalSignal_BMS1_125_DefaultSubstValue_BMS_RelayOpenReq(void)
 *   uint8 Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_MainConnectorState(void)
 *   uint8 Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_QuickChargeConnectorState(void)
 *   uint8 Rte_CData_CalSignal_BMS1_125_Initial_Value_BMS_RelayOpenReq(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_SOC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeConfirmForbidden_BMS_Voltage(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_MainConnectorState(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_SOC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeConvirmInvalid_BMS_Voltage(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_SOC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeHealForbidden_BMS_Voltage(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_MainConnectorState(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_SOC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS1_125_TimeHealInvalid_BMS_Voltage(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS3_127_TimeConfirmForbidden_BMS_AuxBattVolt(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS3_127_TimeConfirmInvalid_BMS_AuxBattVolt(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS3_127_TimeHealForbidden_BMS_AuxBattVolt(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS3_127_TimeHealInvalid_BMS_AuxBattVolt(void)
 *   uint8 Rte_CData_CalSignal_BMS5_359_DefaultSubstValue_BMS_Fault(void)
 *   uint8 Rte_CData_CalSignal_BMS5_359_Initial_Value_BMS_Fault(void)
 *   uint8 Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_FastChargeSt(void)
 *   uint8 Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_OnBoardChargerEnable(void)
 *   uint8 Rte_CData_CalSignal_BMS6_361_DefaultSubstValue_BMS_SlowChargeSt(void)
 *   uint8 Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_FastChargeSt(void)
 *   uint8 Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_OnBoardChargerEnable(void)
 *   uint8 Rte_CData_CalSignal_BMS6_361_Initial_Value_BMS_SlowChargeSt(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeCurrentAllow(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS6_361_TimeConfirmForbidden_BMS_HighestChargeVoltageAllow(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeCurrentAllow(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS6_361_TimeHealForbidden_BMS_HighestChargeVoltageAllow(void)
 *   uint8 Rte_CData_CalSignal_BMS8_31B_DefaultSubstValue_BMS_CC2_connection_Status(void)
 *   uint8 Rte_CData_CalSignal_BMS8_31V_Initial_Value_BMS_CC2_connection_Status(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS9_129_TimeConfirmForbidden_BMS_DC_RELAY_VOLTAGE(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BMS9_129_TimeHealForbidden_BMS_DC_RELAY_VOLTAGE(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_ChargeTypeStatus(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_LockedVehicle(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_MainWakeup(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PostDriveWakeup(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_BSI_PreDriveWakeup(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_CHARGE_STATE(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_HEAT_PUMP_WORKING_MODE(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_DefaultSubstValue_VCU_SYNCHRO_GPC(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_ChargeTypeStatus(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_LockedVehicle(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_MainWakeup(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PostDriveWakeup(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_BSI_PreDriveWakeup(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_CHARGE_STATE(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_HEAT_PUMP_WORKING_MODE(void)
 *   uint8 Rte_CData_CalSignal_BSIInfo_382_Initial_Value_VCU_SYNCHRO_GPC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_BSI_ChargeTypeStatus(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BSIInfo_382_TimeConfirmForbidden_VCU_HEAT_PUMP_WORKING_MODE(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_BSI_ChargeTypeStatus(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_BSIInfo_382_TimeHealForbidden_VCU_HEAT_PUMP_WORKING_MODE(void)
 *   uint8 Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_ActivedischargeCommand(void)
 *   uint8 Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCActivation(void)
 *   uint8 Rte_CData_CalSignal_CtrlDCDC_372_DefaultSubstValue_VCU_DCDCVoltageReq(void)
 *   uint8 Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_ActivedischargeCommand(void)
 *   uint8 Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCActivation(void)
 *   uint8 Rte_CData_CalSignal_CtrlDCDC_372_Initial_Value_VCU_DCDCVoltageReq(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmForbidden_VCU_DCDCVoltageReq(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCActivation(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_CtrlDCDC_372_TimeConfirmInvalid_VCU_DCDCVoltageReq(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_CtrlDCDC_372_TimeHealForbidden_VCU_DCDCVoltageReq(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCActivation(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_CtrlDCDC_372_TimeHealInvalid_VCU_DCDCVoltageReq(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Fault(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_FaultLampRequest(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_HighVoltConnectionAllowed(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OVERTEMP(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_OutputVoltage(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Status(void)
 *   uint8 Rte_CData_CalSignal_DC1_345_Initial_Value_DCDC_Temperature(void)
 *   uint8 Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_ActivedischargeSt(void)
 *   uint8 Rte_CData_CalSignal_DC2_0C5_Initial_Value_DCDC_InputCurrent(void)
 *   uint8 Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_DCDC_RT_POWER_LOAD(void)
 *   uint8 Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_MAIN_CONTACTOR_REQ(void)
 *   uint8 Rte_CData_CalSignal_DC2_0C5_Initial_Value_OBC_QUICK_CHARGE_CONTACTOR_REQ(void)
 *   uint8 Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_DIAG_INTEGRA_ELEC(void)
 *   uint8 Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_EFFAC_DEFAUT_DIAG(void)
 *   uint8 Rte_CData_CalSignal_ELECTRON_BSI_092_Initial_Value_MODE_DIAG(void)
 *   uint8 Rte_CData_CalSignal_NEW_JDD_OBC_DCDC_5B1_Initial_Value_NOMBRE_TRAMES(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_EVSE_RTAB_STOP_CHARGE(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_CP_connection_Status(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ChargingMode(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_ElockState(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Fault(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_HighVoltConnectionAllowed(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempL(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_SocketTempN(void)
 *   uint8 Rte_CData_CalSignal_OBC1_3A3_Initial_Value_OBC_Status(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ACRange(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_ChargingConnectionConfirmation(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_CommunicationSt(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_DCChargingPlugAConnectionConfirmation(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_InputVoltageSt(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCStartSt(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_OBCTemp(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PLUG_VOLT_DETECTION(void)
 *   uint8 Rte_CData_CalSignal_OBC2_3A2_Initial_Value_OBC_PowerMax(void)
 *   uint8 Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_COOLING_WAKEUP(void)
 *   uint8 Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HVBattRechargeWakeup(void)
 *   uint8 Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_HoldDiscontactorWakeup(void)
 *   uint8 Rte_CData_CalSignal_OBC3_230_Initial_Value_OBC_PIStateInfoWakeup(void)
 *   uint8 Rte_CData_CalSignal_OBC4_439_Initial_Value_OBC_PUSH_CHARGE_TYPE(void)
 *   uint8 Rte_CData_CalSignal_OBC4_439_Initial_Value_RECHARGE_HMI_STATE(void)
 *   uint8 Rte_CData_CalSignal_ParkCommand_31E_DefaultSubstValue_VCU_EPWT_Status(void)
 *   uint8 Rte_CData_CalSignal_ParkCommand_31E_Initial_Value_VCU_EPWT_Status(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_ParkCommand_31E_TimeConfirmInvalid_VCU_EPWT_Status(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_ParkCommand_31E_TimeHealInvalid_VCU_EPWT_Status(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_COOLING_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_DTC_REGISTRED(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_ECU_ELEC_STATE_RCD(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HOLD_DISCONTACTOR_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_HV_BATT_CHARGE_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PI_STATE_INFO_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_POST_DRIVE_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRECOND_ELEC_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_PRE_DRIVE_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_RCD_LINE_STATE(void)
 *   uint8 Rte_CData_CalSignal_SUPV_V2_OBC_DCDC_591_Initial_Value_STOP_DELAYED_HMI_WUP_STATE(void)
 *   uint8 Rte_CData_CalSignal_VCU2_0F0_DefaultSubstValue_VCU_Keyposition(void)
 *   uint8 Rte_CData_CalSignal_VCU3_486_DefaultSubstValue_ELEC_METER_SATURATION(void)
 *   uint8 Rte_CData_CalSignal_VCU3_486_Initial_Value_ELEC_METER_SATURATION(void)
 *   uint8 Rte_CData_CalSignal_VCU_552_DefaultSubstValue_COMPTEUR_RAZ_GCT(void)
 *   uint8 Rte_CData_CalSignal_VCU_552_Initial_Value_COMPTEUR_RAZ_GCT(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_COMPTEUR_RAZ_GCT(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_CPT_TEMPOREL(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_552_TimeConfirmInvalid_KILOMETRAGE(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_552_TimeHealInvalid_COMPTEUR_RAZ_GCT(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_552_TimeHealInvalid_CPT_TEMPOREL(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_552_TimeHealInvalid_KILOMETRAGE(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_ACC_JDD(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_CDE_APC_JDD(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DDE_GMV_SEEM(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DIAG_MUX_ON_PWT(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_ACTIV_CHILLER(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_DMD_MEAP_2_SEEM(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_GMP_HYB(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_PRINCIP_SEV(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_ETAT_RESEAU_ELEC(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_MODE_EPS_REQUEST(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_POS_SHUNT_JDD(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_PRECOND_ELEC_WAKEUP(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_DefaultSubstValue_STOP_DELAYED_HMI_WAKEUP(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_ACC_JDD(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_CDE_APC_JDD(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DDE_GMV_SEEM(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DIAG_MUX_ON_PWT(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_ACTIV_CHILLER(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_DMD_MEAP_2_SEEM(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_GMP_HYB(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_PRINCIP_SEV(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_ETAT_RESEAU_ELEC(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_MODE_EPS_REQUEST(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_POS_SHUNT_JDD(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_PRECOND_ELEC_WAKEUP(void)
 *   uint8 Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_Initial_Value_STOP_DELAYED_HMI_WAKEUP(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DDE_GMV_SEEM(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_DMD_MEAP_2_SEEM(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_GMP_HYB(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_PRINCIP_SEV(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_ETAT_RESEAU_ELEC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmForbidden_MODE_EPS_REQUEST(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeConfirmInvalid_DDE_GMV_SEEM(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DDE_GMV_SEEM(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_DMD_MEAP_2_SEEM(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_GMP_HYB(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_PRINCIP_SEV(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_ETAT_RESEAU_ELEC(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealForbidden_MODE_EPS_REQUEST(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_TimeHealInvalid_DDE_GMV_SEEM(void)
 *   uint8 Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_ABS_VehSpdValidFlag(void)
 *   uint8 Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTP(void)
 *   uint8 Rte_CData_CalSignal_VCU_PCANInfo_17B_DefaultSubstValue_COUPURE_CONSO_CTPE2(void)
 *   uint8 Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_ABS_VehSpdValidFlag(void)
 *   uint8 Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTP(void)
 *   uint8 Rte_CData_CalSignal_VCU_PCANInfo_17B_Initial_Value_COUPURE_CONSO_CTPE2(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeConfirmInvalid_ABS_VehSpd(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_PCANInfo_17B_TimeHealInvalid_ABS_VehSpd(void)
 *   uint8 Rte_CData_CalSignal_VCU_TU_37E_DefaultSubstValue_VCU_AccPedalPosition(void)
 *   uint8 Rte_CData_CalSignal_VCU_TU_37E_Initial_Value_VCU_AccPedalPosition(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmForbidden_VCU_AccPedalPosition(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_TU_37E_TimeConfirmInvalid_VCU_AccPedalPosition(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_TU_37E_TimeHealForbidden_VCU_AccPedalPosition(void)
 *   IdtSignalDiagTime Rte_CData_CalSignal_VCU_TU_37E_TimeHealInvalid_VCU_AccPedalPosition(void)
 *   IdtT_INHIB_DIAG_COM Rte_CData_CalT_INHIB_DIAG_COM(void)
 *   IdtTimeBeforeRunningDiag Rte_CData_CalTimeBeforeRunningDiagBSIInfo(void)
 *   IdtTimeBeforeRunningDiag Rte_CData_CalTimeBeforeRunningDiagEVCU(void)
 *   IdtTimeBeforeRunningDiag Rte_CData_CalTimeBeforeRunningDiagTBMU(void)
 *   IdtTo_DgMux_Prod Rte_CData_CalTo_DgMux_Prod(void)
 *   boolean Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_MainConnectorState(void)
 *   boolean Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_QuickChargeConnectorState(void)
 *   boolean Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_RelayOpenReq(void)
 *   boolean Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_SOC(void)
 *   boolean Rte_CData_CalSignal_BMS1_125_OutputSelectorFault_BMS_Voltage(void)
 *   boolean Rte_CData_CalSignal_BMS3_127_OutputSelectorFault_BMS_AuxBattVolt(void)
 *   boolean Rte_CData_CalSignal_BMS5_359_OutputSelectorFault_BMS_Fault(void)
 *   boolean Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_FastChargeSt(void)
 *   boolean Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeCurrentAllow(void)
 *   boolean Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_HighestChargeVoltageAllow(void)
 *   boolean Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_OnBoardChargerEnable(void)
 *   boolean Rte_CData_CalSignal_BMS6_361_OutputSelectorFault_BMS_SlowChargeSt(void)
 *   boolean Rte_CData_CalSignal_BMS8_31B_OutputSelectorFault_BMS_CC2_connection_Status(void)
 *   boolean Rte_CData_CalSignal_BMS9_129_OutputSelectorFault_BMS_DC_RELAY_VOLTAGE(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_ChargeTypeStatus(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_LockedVehicle(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_MainWakeup(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PostDriveWakeup(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_BSI_PreDriveWakeup(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_CHARGE_STATE(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_HEAT_PUMP_WORKING_MODE(void)
 *   boolean Rte_CData_CalSignal_BSIInfo_382_OutputSelectorFault_VCU_SYNCHRO_GPC(void)
 *   boolean Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_ActivedischargeCommand(void)
 *   boolean Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCActivation(void)
 *   boolean Rte_CData_CalSignal_CtrlDCDC_372_OutputSelectorFault_VCU_DCDCVoltageReq(void)
 *   boolean Rte_CData_CalSignal_ParkCommand_31E_OutputSelectorFault_VCU_EPWT_Status(void)
 *   boolean Rte_CData_CalSignal_VCU2_0F0_OutputSelectorFault_VCU_Keyposition(void)
 *   boolean Rte_CData_CalSignal_VCU3_486_OutputSelectorFault_ELEC_METER_SATURATION(void)
 *   boolean Rte_CData_CalSignal_VCU_552_OutputSelectorFault_COMPTEUR_RAZ_GCT(void)
 *   boolean Rte_CData_CalSignal_VCU_552_OutputSelectorFault_CPT_TEMPOREL(void)
 *   boolean Rte_CData_CalSignal_VCU_552_OutputSelectorFault_KILOMETRAGE(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_ACC_JDD(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_CDE_APC_JDD(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DDE_GMV_SEEM(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DIAG_MUX_ON_PWT(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_ACTIV_CHILLER(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_DMD_MEAP_2_SEEM(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_GMP_HYB(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_PRINCIP_SEV(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_ETAT_RESEAU_ELEC(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_MODE_EPS_REQUEST(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_POS_SHUNT_JDD(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_PRECOND_ELEC_WAKEUP(void)
 *   boolean Rte_CData_CalSignal_VCU_BSI_Wakeup_27A_OutputSelectorFault_STOP_DELAYED_HMI_WAKEUP(void)
 *   boolean Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpd(void)
 *   boolean Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_ABS_VehSpdValidFlag(void)
 *   boolean Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTP(void)
 *   boolean Rte_CData_CalSignal_VCU_PCANInfo_17B_OutputSelectorFault_COUPURE_CONSO_CTPE2(void)
 *   boolean Rte_CData_CalSignal_VCU_TU_37E_OutputSelectorFault_VCU_AccPedalPosition(void)
 *
 *********************************************************************************************************************/


#define CtApPCOM_START_SEC_CODE
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING
 *   RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;
	 *ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING
 *   RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_ReadData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;
	Data[0] = PCOM_IntCANDiagnosticsEnable;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(const uint8 *Data, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_DCM_E_PENDING
 *   RTE_E_DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData(P2CONST(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_DATA) Data, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Internal_CAN_Frame_Lost_Diag_Enable_EnableDiag_WriteData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;
	if (FALSE == Data[0])
	{
		PCOM_IntCANDiagnosticsEnable = FALSE;
	}
	else
	{
		PCOM_IntCANDiagnosticsEnable = TRUE;
	}

	*ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_PCOM_debug_signals_PCOMDebug>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING
 *   RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_PCOM_debug_signals_PCOMDebug_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;	/* Remove compilator warning */

  *ErrorCode = DCM_E_POSITIVERESPONSE;

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_PCOM_debug_signals_PCOMDebug_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_PCOM_debug_signals_PCOMDebug>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_DCM_E_PENDING
 *   RTE_E_DataServices_PCOM_debug_signals_PCOMDebug_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_PCOM_debug_signals_PCOMDebug_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApPCOM_CODE) DataServices_PCOM_debug_signals_PCOMDebug_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPPCOM_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_PCOM_debug_signals_PCOMDebug_ReadData (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;	/* Remove compilator warning */

	Data[0] = PCOM_Debug[0];
	Data[1] = PCOM_Debug[1];
	Data[2] = PCOM_Debug[2];
	Data[3] = PCOM_Debug[3];


  return RTE_E_OK;
  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpPCOM_FrameNewJDDOBCDCDCTrx> of PortPrototype <PpPCOM_FrameNewJDDOBCDCDCTrx>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPCOM_CODE) PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpPCOM_FrameNewJDDOBCDCDCTrx_OpPCOM_FrameNewJDDOBCDCDCTrx
 *********************************************************************************************************************/

	 PCOM_Frame_5b1_SendEvent = TRUE;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpPCOM_FrameVERS_OBC_DCDCTrx> of PortPrototype <PpPCOM_FrameVERS_OBC_DCDCTrx>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPCOM_CODE) PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PpPCOM_FrameVERS_OBC_DCDCTrx_OpPCOM_FrameVERS_OBC_DCDCTrx
 *********************************************************************************************************************/

	PCOM_Frame_0ce_SendEvent = TRUE;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPCOM_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpDCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data)
 *   Std_ReturnType Rte_Write_PpDCDC_Fault_DCDC_Fault(DCDC_Fault data)
 *   Std_ReturnType Rte_Write_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data)
 *   Std_ReturnType Rte_Write_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpDCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data)
 *   Std_ReturnType Rte_Write_PpDCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data)
 *   Std_ReturnType Rte_Write_PpDCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data)
 *   Std_ReturnType Rte_Write_PpDCDC_Status_DCDC_Status(DCDC_Status data)
 *   Std_ReturnType Rte_Write_PpDCDC_Temperature_DCDC_Temperature(DCDC_Temperature data)
 *   Std_ReturnType Rte_Write_PpDCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpDCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor data)
 *   Std_ReturnType Rte_Write_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data)
 *   Std_ReturnType Rte_Write_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd data)
 *   Std_ReturnType Rte_Write_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_Fault_BMS_Fault(BMS_Fault data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_SOC_BMS_SOC(BMS_SOC data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(BSI_VCUSynchroGPC data)
 *   Std_ReturnType Rte_Write_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP data)
 *   Std_ReturnType Rte_Write_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 data)
 *   Std_ReturnType Rte_Write_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC data)
 *   Std_ReturnType Rte_Write_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG data)
 *   Std_ReturnType Rte_Write_PpInt_MODE_DIAG_MODE_DIAG(MODE_DIAG data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(VCU_AccPedalPosition data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(VCU_EPWT_Status data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_Keyposition_VCU_Keyposition(VCU_Keyposition data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_ACRange_OBC_ACRange(OBC_ACRange data)
 *   Std_ReturnType Rte_Write_PpOBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data)
 *   Std_ReturnType Rte_Write_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data)
 *   Std_ReturnType Rte_Write_PpOBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data)
 *   Std_ReturnType Rte_Write_PpOBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt data)
 *   Std_ReturnType Rte_Write_PpOBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data)
 *   Std_ReturnType Rte_Write_PpOBC_ElockState_OBC_ElockState(OBC_ElockState data)
 *   Std_ReturnType Rte_Write_PpOBC_Fault_OBC_Fault(OBC_Fault data)
 *   Std_ReturnType Rte_Write_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data)
 *   Std_ReturnType Rte_Write_PpOBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data)
 *   Std_ReturnType Rte_Write_PpOBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data)
 *   Std_ReturnType Rte_Write_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data)
 *   Std_ReturnType Rte_Write_PpOBC_PowerMax_OBC_PowerMax(OBC_PowerMax data)
 *   Std_ReturnType Rte_Write_PpOBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data)
 *   Std_ReturnType Rte_Write_PpOBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data)
 *   Std_ReturnType Rte_Write_PpOBC_Status_OBC_Status(OBC_Status data)
 *   Std_ReturnType Rte_Write_PpSG_DC2_SG_DC2(const SG_DC2 *data)
 *   Std_ReturnType Rte_Write_PpSUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data)
 *   Std_ReturnType Rte_Write_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data)
 *   Std_ReturnType Rte_Write_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data)
 *   Std_ReturnType Rte_Write_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data)
 *   Std_ReturnType Rte_Write_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data)
 *   Std_ReturnType Rte_Write_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data)
 *   Std_ReturnType Rte_Write_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data)
 *   Std_ReturnType Rte_Write_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPCOM_init_doc
 *********************************************************************************************************************/

/*!
 * \brief PCOM initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPCOM_CODE) RCtApPCOM_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPCOM_init
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    /*Initialize internal variables.*/
    uint8 PCOM_index;


/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it from WUM to PCOM SWC */
 (void) Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN();
/* [Enrique.Bueno] Workaround STOP */

  PCOM_Deserialize_NVM();

  for (PCOM_index = 0U; PCOM_index < PCOM_NUM_TX_FRAMES; PCOM_index++) {
      PCOM_TX_Frame_Data[PCOM_index].CurrentRC = 0xFFU;
  }
  for (PCOM_index = 0U; PCOM_index < PCOM_NUM_RX_FRAMES; PCOM_index++) {
      PCOM_RX_Frame_Data[PCOM_index].LostFramePreFault = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].LostFrameFault = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].ChkSumPreFault = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].ChkSumFault = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].RCPreFault = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].RCFault = FALSE;

      PCOM_RX_Frame_Data[PCOM_index].LostFrame_receiveFlag = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].LastFrameTime = 0U;
      PCOM_RX_Frame_Data[PCOM_index].RC_receiveFlag = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].NewRCValue = FALSE;
      PCOM_RX_Frame_Data[PCOM_index].LastRCValue = 0U;
      PCOM_RX_Frame_Data[PCOM_index].LostFrameConfirm = 0U;
      PCOM_RX_Frame_Data[PCOM_index].LostFrameHeal = 0U;
      PCOM_RX_Frame_Data[PCOM_index].ChkSumConfirm = 0U;
      PCOM_RX_Frame_Data[PCOM_index].ChkSumHeal = 0U;
      PCOM_RX_Frame_Data[PCOM_index].RCConfirm = 0U;
      PCOM_RX_Frame_Data[PCOM_index].RCHeal = 0U;
      PCOM_RX_Frame_Data[PCOM_index].currentDelay = 0U;
      PCOM_RX_Frame_Data[PCOM_index].receiveFlag = FALSE;

      PCOM_RX_Frame_Data[PCOM_index].DebugRCbuffer = 0U;



      if ((PCOM_CAN_INT_OBC == PCOM_RX_Frame_Config[PCOM_index].CAN_grid)
	  || (PCOM_CAN_INT_LVC
	      == PCOM_RX_Frame_Config[PCOM_index].CAN_grid)) {
	  /* RC error confirm time for internal CAN (10 periods)*/
	  PCOM_RX_Frame_Data[PCOM_index].RCConfirm = 10U;
      }


      PCOM_RX_DebounceCounter[PCOM_index][PCOM_FAULT_FRAMELOST] = 0U;
      PCOM_RX_DebounceCounter[PCOM_index][PCOM_FAULT_RC] = 0U;
      PCOM_RX_DebounceCounter[PCOM_index][PCOM_FAULT_CHKSUM] = 0U;
  }

  /*Get NVM param for all RX frames*/
  PCOM_RX_Frame_Data[0U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_0F0_LostFrameConfirm();
  PCOM_RX_Frame_Data[0U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_0F0_LostFrameHeal();
  PCOM_RX_Frame_Data[1U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_125_LostFrameConfirm();
  PCOM_RX_Frame_Data[1U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_125_LostFrameHeal();
  PCOM_RX_Frame_Data[2U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_127_LostFrameConfirm();
  PCOM_RX_Frame_Data[2U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_127_LostFrameHeal();
  PCOM_RX_Frame_Data[3U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_129_LostFrameConfirm();
  PCOM_RX_Frame_Data[3U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_129_LostFrameHeal();
  PCOM_RX_Frame_Data[4U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_17B_LostFrameConfirm();
  PCOM_RX_Frame_Data[4U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_17B_LostFrameHeal();
  PCOM_RX_Frame_Data[4U].ChkSumConfirm =
      (uint8) Rte_CData_CalFrame_17B_ChkSumConfirm();
  PCOM_RX_Frame_Data[4U].ChkSumHeal =
      (uint8) Rte_CData_CalFrame_17B_ChkSumHeal();
  PCOM_RX_Frame_Data[4U].RCConfirm =
      (uint8) Rte_CData_CalFrame_17B_RCConfirm();
  PCOM_RX_Frame_Data[4U].RCHeal = (uint8) Rte_CData_CalFrame_17B_RCHeal();
  PCOM_RX_Frame_Data[5U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_27A_LostFrameConfirm();
  PCOM_RX_Frame_Data[5U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_27A_LostFrameHeal();
  PCOM_RX_Frame_Data[6U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_359_LostFrameConfirm();
  PCOM_RX_Frame_Data[6U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_359_LostFrameHeal();
  PCOM_RX_Frame_Data[7U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_361_LostFrameConfirm();
  PCOM_RX_Frame_Data[7U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_361_LostFrameHeal();
  PCOM_RX_Frame_Data[8U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_372_LostFrameConfirm();
  PCOM_RX_Frame_Data[8U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_372_LostFrameHeal();
  PCOM_RX_Frame_Data[9U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_37E_LostFrameConfirm();
  PCOM_RX_Frame_Data[9U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_37E_LostFrameHeal();
  PCOM_RX_Frame_Data[10U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_382_LostFrameConfirm();
  PCOM_RX_Frame_Data[10U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_382_LostFrameHeal();
  PCOM_RX_Frame_Data[11U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_31B_LostFrameConfirm();
  PCOM_RX_Frame_Data[11U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_31B_LostFrameHeal();
  PCOM_RX_Frame_Data[12U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_31E_LostFrameConfirm();
  PCOM_RX_Frame_Data[12U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_31E_LostFrameHeal();
  PCOM_RX_Frame_Data[13U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_486_LostFrameConfirm();
  PCOM_RX_Frame_Data[13U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_486_LostFrameHeal();
  PCOM_RX_Frame_Data[14U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_552_LostFrameConfirm();
  PCOM_RX_Frame_Data[14U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_552_LostFrameHeal();
  PCOM_RX_Frame_Data[14U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_552_LostFrameConfirm();
  PCOM_RX_Frame_Data[14U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_552_LostFrameHeal();


  PCOM_Integ_Mode.data = 0U;
  PCOM_Integ_Mode.fresh_frame_rcvd = FALSE;

  PCOM_IntCANDiagnosticsEnable = TRUE;

  /* The frame containing the signal PedalPosition has been removed from configuration */
  /* However, the value of this signal can be requested by UDS and is also present in
   * freeze frames
   */
  /* PSA agreeded to set a constant value of 0xFF */

  (void)Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(0xFF);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPCOM_task5msRX
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 5ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpABS_VehSpd_ABS_VehSpd(ABS_VehSpd *data)
 *   Std_ReturnType Rte_Read_PpABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag *data)
 *   Std_ReturnType Rte_Read_PpBMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt *data)
 *   Std_ReturnType Rte_Read_PpBMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status *data)
 *   Std_ReturnType Rte_Read_PpBMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpBMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt *data)
 *   Std_ReturnType Rte_Read_PpBMS_Fault_BMS_Fault(BMS_Fault *data)
 *   Std_ReturnType Rte_Read_PpBMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow *data)
 *   Std_ReturnType Rte_Read_PpBMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow *data)
 *   Std_ReturnType Rte_Read_PpBMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
 *   Std_ReturnType Rte_Read_PpBMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
 *   Std_ReturnType Rte_Read_PpBMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState *data)
 *   Std_ReturnType Rte_Read_PpBMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq *data)
 *   Std_ReturnType Rte_Read_PpBMS_SOC_BMS_SOC(BMS_SOC *data)
 *   Std_ReturnType Rte_Read_PpBMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt *data)
 *   Std_ReturnType Rte_Read_PpBMS_Voltage_BMS_Voltage(BMS_Voltage *data)
 *   Std_ReturnType Rte_Read_PpBSI_ChargeState_BSI_ChargeState(BSI_ChargeState *data)
 *   Std_ReturnType Rte_Read_PpBSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus *data)
 *   Std_ReturnType Rte_Read_PpBSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
 *   Std_ReturnType Rte_Read_PpBSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup *data)
 *   Std_ReturnType Rte_Read_PpBSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpBSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup *data)
 *   Std_ReturnType Rte_Read_PpBSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode *data)
 *   Std_ReturnType Rte_Read_PpBSI_VCUSynchroGPC_BSI_VCUSynchroGPC(BSI_VCUSynchroGPC *data)
 *   Std_ReturnType Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(IdtBatteryVoltageState *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA0(DATA0 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA1(DATA1 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA2(DATA2 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA3(DATA3 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA4(DATA4 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA5(DATA5 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA6(DATA6 *data)
 *   Std_ReturnType Rte_Read_PpBootEnterData_DATA7(DATA7 *data)
 *   Std_ReturnType Rte_Read_PpCOUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP *data)
 *   Std_ReturnType Rte_Read_PpCOUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 *data)
 *   Std_ReturnType Rte_Read_PpDATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 *data)
 *   Std_ReturnType Rte_Read_PpDCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpDCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 *data)
 *   Std_ReturnType Rte_Read_PpDCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 *data)
 *   Std_ReturnType Rte_Read_PpDCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT *data)
 *   Std_ReturnType Rte_Read_PpDCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference *data)
 *   Std_ReturnType Rte_Read_PpDCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT *data)
 *   Std_ReturnType Rte_Read_PpDCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V *data)
 *   Std_ReturnType Rte_Read_PpDCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
 *   Std_ReturnType Rte_Read_PpDCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpDCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current *data)
 *   Std_ReturnType Rte_Read_PpDCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage *data)
 *   Std_ReturnType Rte_Read_PpDCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current *data)
 *   Std_ReturnType Rte_Read_PpDCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT *data)
 *   Std_ReturnType Rte_Read_PpDCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP *data)
 *   Std_ReturnType Rte_Read_PpDCLV_Power_DCLV_Power(DCLV_Power *data)
 *   Std_ReturnType Rte_Read_PpDCLV_T_L_BUCK_DCLV_T_L_BUCK(DCLV_T_L_BUCK *data)
 *   Std_ReturnType Rte_Read_PpDCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A *data)
 *   Std_ReturnType Rte_Read_PpDCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B *data)
 *   Std_ReturnType Rte_Read_PpDCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A *data)
 *   Std_ReturnType Rte_Read_PpDCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B *data)
 *   Std_ReturnType Rte_Read_PpDCLV_T_TX_PP_DCLV_T_TX_PP(DCLV_T_TX_PP *data)
 *   Std_ReturnType Rte_Read_PpDIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC *data)
 *   Std_ReturnType Rte_Read_PpEFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG *data)
 *   Std_ReturnType Rte_Read_PpMODE_DIAG_MODE_DIAG(MODE_DIAG *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V *data)
 *   Std_ReturnType Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpPFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 *data)
 *   Std_ReturnType Rte_Read_PpPFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus *data)
 *   Std_ReturnType Rte_Read_PpPFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C *data)
 *   Std_ReturnType Rte_Read_PpPFC_Temp_M3_C_PFC_Temp_M3_C(PFC_Temp_M3_C *data)
 *   Std_ReturnType Rte_Read_PpPFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH23_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpPFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V *data)
 *   Std_ReturnType Rte_Read_PpPFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V *data)
 *   Std_ReturnType Rte_Read_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(boolean *data)
 *   Std_ReturnType Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(boolean *data)
 *   Std_ReturnType Rte_Read_PpVCU_AccPedalPosition_VCU_AccPedalPosition(VCU_AccPedalPosition *data)
 *   Std_ReturnType Rte_Read_PpVCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand *data)
 *   Std_ReturnType Rte_Read_PpVCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD *data)
 *   Std_ReturnType Rte_Read_PpVCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD *data)
 *   Std_ReturnType Rte_Read_PpVCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct *data)
 *   Std_ReturnType Rte_Read_PpVCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel *data)
 *   Std_ReturnType Rte_Read_PpVCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation *data)
 *   Std_ReturnType Rte_Read_PpVCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq *data)
 *   Std_ReturnType Rte_Read_PpVCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM *data)
 *   Std_ReturnType Rte_Read_PpVCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller *data)
 *   Std_ReturnType Rte_Read_PpVCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM *data)
 *   Std_ReturnType Rte_Read_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
 *   Std_ReturnType Rte_Read_PpVCU_EPWT_Status_VCU_EPWT_Status(VCU_EPWT_Status *data)
 *   Std_ReturnType Rte_Read_PpVCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation *data)
 *   Std_ReturnType Rte_Read_PpVCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb *data)
 *   Std_ReturnType Rte_Read_PpVCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev *data)
 *   Std_ReturnType Rte_Read_PpVCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec *data)
 *   Std_ReturnType Rte_Read_PpVCU_Keyposition_VCU_Keyposition(VCU_Keyposition *data)
 *   Std_ReturnType Rte_Read_PpVCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage *data)
 *   Std_ReturnType Rte_Read_PpVCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest *data)
 *   Std_ReturnType Rte_Read_PpVCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD *data)
 *   Std_ReturnType Rte_Read_PpVCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup *data)
 *   Std_ReturnType Rte_Read_PpVCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(boolean data)
 *   Std_ReturnType Rte_Write_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpBusOFFEnableConditions_DeBusOFFEnableConditions(boolean data)
 *   Std_ReturnType Rte_Write_PpBusOffCANFault_DeBusOffCANFault(boolean data)
 *   Std_ReturnType Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo(boolean data)
 *   Std_ReturnType Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU(boolean data)
 *   Std_ReturnType Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU(boolean data)
 *   Std_ReturnType Rte_Write_PpDCLVFramesReception_Error_DeDCLVFramesReception_Error(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault(boolean data)
 *   Std_ReturnType Rte_Write_PpInt_ABS_VehSpd_ABS_VehSpd(ABS_VehSpd data)
 *   Std_ReturnType Rte_Write_PpInt_ABS_VehSpdValidFlag_ABS_VehSpdValidFlag(ABS_VehSpdValidFlag data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_AuxBattVolt_BMS_AuxBattVolt(BMS_AuxBattVolt data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_CC2_connection_Status_BMS_CC2_connection_Status(BMS_CC2_connection_Status data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_FastChargeSt_BMS_FastChargeSt(BMS_FastChargeSt data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_Fault_BMS_Fault(BMS_Fault data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_HighestChargeCurrentAllow_BMS_HighestChargeCurrentAllow(BMS_HighestChargeCurrentAllow data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_HighestChargeVoltageAllow_BMS_HighestChargeVoltageAllow(BMS_HighestChargeVoltageAllow data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_QuickChargeConnectorState_BMS_QuickChargeConnectorState(BMS_QuickChargeConnectorState data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_RelayOpenReq_BMS_RelayOpenReq(BMS_RelayOpenReq data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_SOC_BMS_SOC(BMS_SOC data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_SlowChargeSt_BMS_SlowChargeSt(BMS_SlowChargeSt data)
 *   Std_ReturnType Rte_Write_PpInt_BMS_Voltage_BMS_Voltage(BMS_Voltage data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_ChargeState_BSI_ChargeState(BSI_ChargeState data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_ChargeTypeStatus_BSI_ChargeTypeStatus(BSI_ChargeTypeStatus data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_MainWakeup_BSI_MainWakeup(BSI_MainWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_PostDriveWakeup_BSI_PostDriveWakeup(BSI_PostDriveWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_PreDriveWakeup_BSI_PreDriveWakeup(BSI_PreDriveWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_VCUHeatPumpWorkingMode_BSI_VCUHeatPumpWorkingMode(BSI_VCUHeatPumpWorkingMode data)
 *   Std_ReturnType Rte_Write_PpInt_BSI_VCUSynchroGPC_BSI_VCUSynchroGPC(BSI_VCUSynchroGPC data)
 *   Std_ReturnType Rte_Write_PpInt_COUPURE_CONSO_CTP_COUPURE_CONSO_CTP(COUPURE_CONSO_CTP data)
 *   Std_ReturnType Rte_Write_PpInt_COUPURE_CONSO_CTPE2_COUPURE_CONSO_CTPE2(COUPURE_CONSO_CTPE2 data)
 *   Std_ReturnType Rte_Write_PpInt_DATA_ACQ_JDD_BSI_2_DATA_ACQ_JDD_BSI_2(DATA_ACQ_JDD_BSI_2 data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_ADC_IOUT_DCHV_ADC_IOUT(DCHV_ADC_IOUT data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_ADC_NTC_MOD_5_DCHV_ADC_NTC_MOD_5(DCHV_ADC_NTC_MOD_5 data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_ADC_NTC_MOD_6_DCHV_ADC_NTC_MOD_6(DCHV_ADC_NTC_MOD_6 data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_ADC_VOUT_DCHV_ADC_VOUT(DCHV_ADC_VOUT data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_Command_Current_Reference_DCHV_Command_Current_Reference(DCHV_Command_Current_Reference data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_DCHVStatus_DCHV_DCHVStatus(DCHV_DCHVStatus data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_H_DCHV_IOM_ERR_CAP_FAIL_H(DCHV_IOM_ERR_CAP_FAIL_H data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_CAP_FAIL_L_DCHV_IOM_ERR_CAP_FAIL_L(DCHV_IOM_ERR_CAP_FAIL_L data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OC_IOUT_DCHV_IOM_ERR_OC_IOUT(DCHV_IOM_ERR_OC_IOUT data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OC_NEG_DCHV_IOM_ERR_OC_NEG(DCHV_IOM_ERR_OC_NEG data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OC_POS_DCHV_IOM_ERR_OC_POS(DCHV_IOM_ERR_OC_POS data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OT_DCHV_IOM_ERR_OT(DCHV_IOM_ERR_OT data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OT_IN_DCHV_IOM_ERR_OT_IN(DCHV_IOM_ERR_OT_IN data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD5_DCHV_IOM_ERR_OT_NTC_MOD5(DCHV_IOM_ERR_OT_NTC_MOD5 data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OT_NTC_MOD6_DCHV_IOM_ERR_OT_NTC_MOD6(DCHV_IOM_ERR_OT_NTC_MOD6 data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_OV_VOUT_DCHV_IOM_ERR_OV_VOUT(DCHV_IOM_ERR_OV_VOUT data)
 *   Std_ReturnType Rte_Write_PpInt_DCHV_IOM_ERR_UV_12V_DCHV_IOM_ERR_UV_12V(DCHV_IOM_ERR_UV_12V data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_DCLVStatus_DCLV_DCLVStatus(DCLV_DCLVStatus data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Input_Current_DCLV_Input_Current(DCLV_Input_Current data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Input_Voltage_DCLV_Input_Voltage(DCLV_Input_Voltage data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Measured_Current_DCLV_Measured_Current(DCLV_Measured_Current data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Measured_Voltage_DCLV_Measured_Voltage(DCLV_Measured_Voltage data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OC_A_HV_FAULT_DCLV_OC_A_HV_FAULT(DCLV_OC_A_HV_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OC_A_LV_FAULT_DCLV_OC_A_LV_FAULT(DCLV_OC_A_LV_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OC_B_HV_FAULT_DCLV_OC_B_HV_FAULT(DCLV_OC_B_HV_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OC_B_LV_FAULT_DCLV_OC_B_LV_FAULT(DCLV_OC_B_LV_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OT_FAULT_DCLV_OT_FAULT(DCLV_OT_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OV_INT_A_FAULT_DCLV_OV_INT_A_FAULT(DCLV_OV_INT_A_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OV_INT_B_FAULT_DCLV_OV_INT_B_FAULT(DCLV_OV_INT_B_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_OV_UV_HV_FAULT_DCLV_OV_UV_HV_FAULT(DCLV_OV_UV_HV_FAULT data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_PWM_STOP_DCLV_PWM_STOP(DCLV_PWM_STOP data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_Power_DCLV_Power(DCLV_Power data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_T_L_BUCK_DCLV_T_L_BUCK(DCLV_T_L_BUCK data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_T_PP_A_DCLV_T_PP_A(DCLV_T_PP_A data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_T_PP_B_DCLV_T_PP_B(DCLV_T_PP_B data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_T_SW_BUCK_A_DCLV_T_SW_BUCK_A(DCLV_T_SW_BUCK_A data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_T_SW_BUCK_B_DCLV_T_SW_BUCK_B(DCLV_T_SW_BUCK_B data)
 *   Std_ReturnType Rte_Write_PpInt_DCLV_T_TX_PP_DCLV_T_TX_PP(DCLV_T_TX_PP data)
 *   Std_ReturnType Rte_Write_PpInt_DIAG_INTEGRA_ELEC_DIAG_INTEGRA_ELEC(DIAG_INTEGRA_ELEC data)
 *   Std_ReturnType Rte_Write_PpInt_EFFAC_DEFAUT_DIAG_EFFAC_DEFAUT_DIAG(EFFAC_DEFAUT_DIAG data)
 *   Std_ReturnType Rte_Write_PpInt_MODE_DIAG_MODE_DIAG(MODE_DIAG data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OC_PH1_PFC_IOM_ERR_OC_PH1(PFC_IOM_ERR_OC_PH1 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OC_PH2_PFC_IOM_ERR_OC_PH2(PFC_IOM_ERR_OC_PH2 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OC_PH3_PFC_IOM_ERR_OC_PH3(PFC_IOM_ERR_OC_PH3 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT1_PFC_IOM_ERR_OC_SHUNT1(PFC_IOM_ERR_OC_SHUNT1 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT2_PFC_IOM_ERR_OC_SHUNT2(PFC_IOM_ERR_OC_SHUNT2 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OC_SHUNT3_PFC_IOM_ERR_OC_SHUNT3(PFC_IOM_ERR_OC_SHUNT3 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M1_PFC_IOM_ERR_OT_NTC1_M1(PFC_IOM_ERR_OT_NTC1_M1 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M3_PFC_IOM_ERR_OT_NTC1_M3(PFC_IOM_ERR_OT_NTC1_M3 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OT_NTC1_M4_PFC_IOM_ERR_OT_NTC1_M4(PFC_IOM_ERR_OT_NTC1_M4 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OV_DCLINK_PFC_IOM_ERR_OV_DCLINK(PFC_IOM_ERR_OV_DCLINK data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH12_PFC_IOM_ERR_OV_VPH12(PFC_IOM_ERR_OV_VPH12 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH23_PFC_IOM_ERR_OV_VPH23(PFC_IOM_ERR_OV_VPH23 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_OV_VPH31_PFC_IOM_ERR_OV_VPH31(PFC_IOM_ERR_OV_VPH31 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO4_PFC_IOM_ERR_UVLO_ISO4(PFC_IOM_ERR_UVLO_ISO4 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_UVLO_ISO7_PFC_IOM_ERR_UVLO_ISO7(PFC_IOM_ERR_UVLO_ISO7 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IOM_ERR_UV_12V_PFC_IOM_ERR_UV_12V(PFC_IOM_ERR_UV_12V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH1_RMS_0A1(PFC_IPH1_RMS_0A1 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH2_RMS_0A1(PFC_IPH2_RMS_0A1 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_IPH_RMS_0A1_PFC_IPH3_RMS_0A1(PFC_IPH3_RMS_0A1 data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_PFCStatus_PFC_PFCStatus(PFC_PFCStatus data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_Temp_M1_C_PFC_Temp_M1_C(PFC_Temp_M1_C data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_Temp_M3_C_PFC_Temp_M3_C(PFC_Temp_M3_C data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_Temp_M4_C_PFC_Temp_M4_C(PFC_Temp_M4_C data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH1_Freq_Hz(PFC_VPH1_Freq_Hz data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH2_Freq_Hz(PFC_VPH2_Freq_Hz data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_Freq_Hz_PFC_VPH3_Freq_Hz(PFC_VPH3_Freq_Hz data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH12_Peak_V(PFC_VPH12_Peak_V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH23_Peak_V(PFC_VPH23_Peak_V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_Peak_V_PFC_VPH31_Peak_V(PFC_VPH31_Peak_V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH23_RMS_V(PFC_VPH23_RMS_V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_VPH_RMS_V_PFC_VPH31_RMS_V(PFC_VPH31_RMS_V data)
 *   Std_ReturnType Rte_Write_PpInt_PFC_Vdclink_V_PFC_Vdclink_V(PFC_Vdclink_V data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_AccPedalPosition_VCU_AccPedalPosition(VCU_AccPedalPosition data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_ActivedischargeCommand_VCU_ActivedischargeCommand(VCU_ActivedischargeCommand data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CDEAccJDD_VCU_CDEAccJDD(VCU_CDEAccJDD data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CDEApcJDD_VCU_CDEApcJDD(VCU_CDEApcJDD data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CompteurRazGct_VCU_CompteurRazGct(VCU_CompteurRazGct data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_CptTemporel_VCU_CptTemporel(VCU_CptTemporel data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DCDCActivation_VCU_DCDCActivation(VCU_DCDCActivation data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DCDCVoltageReq_VCU_DCDCVoltageReq(VCU_DCDCVoltageReq data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DDEGMVSEEM_VCU_DDEGMVSEEM(VCU_DDEGMVSEEM data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DMDActivChiller_VCU_DMDActivChiller(VCU_DMDActivChiller data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DMDMeap2SEEM_VCU_DMDMeap2SEEM(VCU_DMDMeap2SEEM data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EPWT_Status_VCU_EPWT_Status(VCU_EPWT_Status data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_ElecMeterSaturation_VCU_ElecMeterSaturation(VCU_ElecMeterSaturation data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EtatGmpHyb_VCU_EtatGmpHyb(VCU_EtatGmpHyb data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EtatPrincipSev_VCU_EtatPrincipSev(VCU_EtatPrincipSev data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_EtatReseauElec_VCU_EtatReseauElec(VCU_EtatReseauElec data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_Keyposition_VCU_Keyposition(VCU_Keyposition data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_Kilometrage_VCU_Kilometrage(VCU_Kilometrage data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_ModeEPSRequest_VCU_ModeEPSRequest(VCU_ModeEPSRequest data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_PosShuntJDD_VCU_PosShuntJDD(VCU_PosShuntJDD data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_PrecondElecWakeup_VCU_PrecondElecWakeup(VCU_PrecondElecWakeup data)
 *   Std_ReturnType Rte_Write_PpInt_VCU_StopDelayedHMIWakeup_VCU_StopDelayedHMIWakeup(VCU_StopDelayedHMIWakeup data)
 *   Std_ReturnType Rte_Write_PpOBCFramesReception_Error_DeOBCFramesReception_Error(boolean data)
 *   Std_ReturnType Rte_Write_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(uint8 frameData)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc07988_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc07988_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(boolean *EventFailed)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_EvtInfo_DTC_0xc08913_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticInfo_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_ReadBlock(dtRef_VOID DstPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_SetRamBlockStatus(boolean RamBlockStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_ECANUserRequest_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(ComM_ModeType ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_MODE_LIMITATION, RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuM_StateRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntity_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPCOM_task5msRX_doc
 *********************************************************************************************************************/

/*!
 * \brief PCOM RX task.
 *
 * This function is called periodically by the scheduler. This function updates
 * the timeout fault for each frame. It also manages the signal
 * requirements.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPCOM_CODE) RCtApPCOM_task5msRX(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPCOM_task5msRX
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    static boolean framesIntialized = FALSE;
/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it from WUM to PCOM SWC */
    static boolean PCOM_shutdown_authorization_prev = FALSE;
/* [Enrique.Bueno] Workaround STOP */

    static boolean PCOM_reset_req_ongoing = FALSE;

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint8 PCOM_index;
    boolean PCOM_resetRequest;
    boolean PCOM_GridDiagnosis[PCOM_CAN_NUMBER];
    boolean ShutdownAuth;
    uint8 ResetRequest;
    boolean PCOM_PLCFWDownloadInProgress;

/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it from WUM to PCOM SWC */
    boolean PCOM_shutdown_authorization;
/* [Enrique.Bueno] Workaround STOP */

/* [Enrique.Bueno] Workaround START: Key on-off reset actions disabled in order to avoid SW stacks after
                                     sending twice consecutive requests */
    //uint8 resetRequest;
/* [Enrique.Bueno] Workaround STOP */


    (void)Rte_Call_alive_WdgMSupervisedEntity_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointCP0);
    (void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP0);



    (void)Rte_Read_PpPLCFWDownloadInProgress_DePLCFWDownloadInProgress(&PCOM_PLCFWDownloadInProgress);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
    PCOM_ProgramFlowSoftModeEnabled = PCOM_PLCFWDownloadInProgress;

    if (FALSE != PCOM_ProgramFlowSoftModeEnabled)
    {
    	/* The FW of the PLC is downloading. Simulate the programm flow sequence. */
    	(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP1);
    }


    (void)Rte_Write_PpProgramFlowSoftModeEnabled_DeProgramFlowSoftModeEnabled(PCOM_ProgramFlowSoftModeEnabled);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


  if(FALSE == framesIntialized)
    {
      /* First time frames are sent are initialized to init value */
      (void)PCOM_RX_initializeFrames();
      framesIntialized = TRUE;

      /* As the communication management has been removed from the BswM, then these
       * calls are needed in order to activate the communication channels
       */
      ComM_CommunicationAllowed(0,TRUE);
      ComM_CommunicationAllowed(1,TRUE);
      ComM_CommunicationAllowed(2,TRUE);

      /* The MUTE counter is also initialized */
      PCOM_CMute_Init();

    }

  PCOM_UpdateCalibratables();

  /*Initialize local variable.*/
  for (PCOM_index = 0U; PCOM_index < (uint8) PCOM_CAN_NUMBER; PCOM_index++) {
      PCOM_GridDiagnosis[PCOM_index] = FALSE;
  }

  /*Reset Request*/
  /* This code section is obsolete. It was used by the previous bootloader implementation */
  (void)Rte_Read_PpBootEnterData_DATA0(&PCOM_resetRequest);/* PRQA S 0315, 3110, 3200, 3417, 3426  #RTE read/write confirmation not used*/
  if (0U < PCOM_resetRequest) {
      Mcu_PerformReset();
  }


  for (PCOM_index = 0U; PCOM_index < PCOM_NUM_RX_FRAMES; PCOM_index++) {
      if (TRUE == PCOM_RX_Frame_Config[PCOM_index].enable) {
	  /*Frame enable*/
	/* PRQA S 0404 ++ # Use of volatile data in this way mandatory for communitaciont between task and interrupt context */
	  PCOM_RX_task_FrameLost(&PCOM_RX_Frame_Data[PCOM_index],
				 PCOM_RX_Frame_Config[PCOM_index],
				 &PCOM_RX_DebounceCounter[PCOM_index][PCOM_FAULT_FRAMELOST]);
	  PCOM_RX_task_CheckSum(&PCOM_RX_Frame_Data[PCOM_index],
				PCOM_RX_Frame_Config[PCOM_index],
				&PCOM_RX_DebounceCounter[PCOM_index][PCOM_FAULT_CHKSUM]);
	  PCOM_RX_task_RC(&PCOM_RX_Frame_Data[PCOM_index],
			  PCOM_RX_Frame_Config[PCOM_index],
			  &PCOM_RX_DebounceCounter[PCOM_index][PCOM_FAULT_RC]);

	      PCOM_RX_task_SignalUpdate(&PCOM_RX_Frame_Config[PCOM_index],&PCOM_RX_Frame_Data[PCOM_index]);

	  PCOM_RX_task_GridDiagnosis(&PCOM_GridDiagnosis,
				     PCOM_RX_Frame_Data[PCOM_index],
				     PCOM_RX_Frame_Config[PCOM_index]);

	/* PRQA S 0404 -- */
      }
  }

  /* Check the MUTE conditions */
  PCOM_CMute_Evaluation();
  /* Check the BusOff conditions */
  PCOM_BusOff_Evaluation();

  /* Send notifications for JDD processing */
  if (FALSE != PCOM_Frame_NEW_JDD_55f_Received)
  {
	  (void)Rte_Call_PpJDD_FrameNewJDD55FRcvd_OpJDD_FrameNewJDD55FRcvd();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	  PCOM_Frame_NEW_JDD_55f_Received = FALSE;
  }

  if (FALSE != PCOM_Frame_VCU_552_Received)
  {
	  (void)Rte_Call_PpJDD_FrameVCU552Rcvd_OpJDD_FrameVCU552Rcvd();/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	  PCOM_Frame_VCU_552_Received = FALSE;
  }


  (void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&ShutdownAuth);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

  if (FALSE == ShutdownAuth)
  {
	  /* Publish faults only if not shutting down */
	  for (PCOM_index = 0U; PCOM_index < (uint8) PCOM_CAN_NUMBER; PCOM_index++) {
		  if (NULL_PTR != PCOM_RX_Grid_ErrorIndication[PCOM_index]) {
		  PCOM_RX_Grid_ErrorIndication[PCOM_index](
			  PCOM_GridDiagnosis[PCOM_index]);
		  }
	  }
  }

  PCOM_Publish_DTC_EnableConditions();
  
  
  if (FALSE == ShutdownAuth)
  {
	  /* Send DTCs only if not shutting down */
	  PCOM_SendRX_DTC_Triggers();
  }

  /* Integration mode */
  if (FALSE != PCOM_Integ_Mode.fresh_frame_rcvd)
  {

	  (void)Rte_Call_PpWUM_ElectronBSI_Frame_Received_OpWUM_ElectronBSI_Frame_Received(PCOM_Integ_Mode.data);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	  PCOM_Integ_Mode.fresh_frame_rcvd = FALSE;
  }

  PCOM_Serialize_NVM();

  PCOM_UpdatePIMs();

/* [Enrique.Bueno] Workaround START: ECU State management shall be executed from ASIL partition.
                                     Agreed with Josep Canals in moving it from WUM to PCOM SWC */
  /* Manage ECU State requests */
  (void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&PCOM_shutdown_authorization);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

  /* [Enrique.Bueno] Workaround START: Key on-off reset actions disabled in order to avoid SW stacks after
                                     sending twice consecutive requests */
     //WUM_GetResetRequest(&resetRequest);
    //if((PCOM_shutdown_authorization != PCOM_shutdown_authorization_prev) && (resetRequest != 0x02))
/* [Enrique.Bueno] Workaround STOP */
    if(PCOM_shutdown_authorization != PCOM_shutdown_authorization_prev)
    {
      if(PCOM_shutdown_authorization != FALSE)
      {
    	  (void)CanTrcv_30_Tja1145_ClearTrcvWufFlag(0);
        (void)Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN();
      }
      else
      {
        (void)Rte_Call_PpEcuMUserNeed_StateRequest_RequestRUN();
      }
    }

  PCOM_shutdown_authorization_prev = PCOM_shutdown_authorization;
/* [Enrique.Bueno] Workaround STOP */


  /* Key on off reset processing moved from WUM due to MPU reasons */
  WUM_GetResetRequest(&ResetRequest);
  if (FALSE == PCOM_reset_req_ongoing)
  {
	  switch(ResetRequest)
	  {
	  case 0x01:
			/* System reset */
			Mcal_ResetSafetyENDINIT_Timed((uint32)22960U);
#ifndef TESSY_UT
			/* PRQA S 0303,0246 ++ # Direct access to uC registers needed for this specific functionality. */
			SCU_RSTCON.B.ESR0 = 0u;		/* Deactivate ESR0 reset input (watchdog) */
			SCU_RSTCON.B.SW = 0b01u;
			/* PRQA S 0303,0246 --*/
#endif /*TESSY_UT*/
      Mcal_SetSafetyENDINIT_Timed();
			Mcu_PerformReset();
		  break;
	  case 0x02:
		  /* Key on off reset requested */
#if 0
		  /* JACE: This code section can be removed. ECUM go to sleep process is no longer
		   * dependent on the state of the communication channels.
		   */
		  Rte_Call_PpComMUserNeed_ECANUserRequest_RequestComMode(COMM_NO_COMMUNICATION);
		  Rte_Call_PpComMUserNeed_IntCANUserRequest_RequestComMode(COMM_NO_COMMUNICATION);
		  CHG_Eth_No_Com();
#endif
		  (void)Rte_Call_PpEcuMUserNeed_StateRequest_ReleaseRUN();

		  PCOM_reset_req_ongoing = TRUE;
		  break;
	  case 0x03:
			 /* App reset */
			Mcal_ResetSafetyENDINIT_Timed((uint32)22960U);
#ifndef TESSY_UT		
	/* PRQA S 0303,0246 ++ # Direct access to uC registers needed for this specific functionality. */
    	SCU_RSTCON.B.ESR0 = 0u;		/* Deactivate ESR0 reset input (watchdog) */
			/* SCU_RSTCON.B.SW = 0b10u;*/
			/* Temporaly configure the Soft reset as system reset due to unexpected behaviour */
			/* To be analyzed */
			SCU_RSTCON.B.SW = 0b01u;
	/* PRQA S 0303,0246 -- */
#endif /*TESSY_UT*/			
      Mcal_SetSafetyENDINIT_Timed();
			Mcu_PerformReset();
		  break;
	  default:
		/* MISRA. no action needed */
		  break;
	  }

  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApPCOM_task5msTX
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 5ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpCANComRequest_DeCANComRequest(sint32 *data)
 *   Std_ReturnType Rte_Read_PpDCDC_InputCurrent_ProducerError_DeDCDC_InputCurrent_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCDC_InputVoltage_ProducerError_DeDCDC_InputVoltage_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCDC_OutputCurrent_ProducerError_DeDCDC_OutputCurrent_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCDC_OutputVoltage_ProducerError_DeDCDC_OutputVoltage_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpDCDC_Temperature_ProducerError_DeDCDC_Temperature_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpECU_WakeupMain_DeECU_WakeupMain(boolean *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_ActivedischargeSt_DCDC_ActivedischargeSt(DCDC_ActivedischargeSt *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Fault_DCDC_Fault(DCDC_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_InputCurrent_DCDC_InputCurrent(DCDC_InputCurrent *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_OBCMainContactorReq_DCDC_OBCMainContactorReq(DCDC_OBCMainContactorReq *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_OBCQuickChargeContactorReq_DCDC_OBCQuickChargeContactorReq(DCDC_OBCQuickChargeContactorReq *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_OutputCurrent_DCDC_OutputCurrent(DCDC_OutputCurrent *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Status_DCDC_Status(DCDC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_Temperature_DCDC_Temperature(DCDC_Temperature *data)
 *   Std_ReturnType Rte_Read_PpInt_DCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Temp_Derating_Factor_DCLV_Temp_Derating_Factor(DCLV_Temp_Derating_Factor *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference *data)
 *   Std_ReturnType Rte_Read_PpInt_EDITION_CALIB_EDITION_CALIB(EDITION_CALIB *data)
 *   Std_ReturnType Rte_Read_PpInt_EDITION_SOFT_EDITION_SOFT(EDITION_SOFT *data)
 *   Std_ReturnType Rte_Read_PpInt_EVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 *data)
 *   Std_ReturnType Rte_Read_PpInt_NEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ACRange_OBC_ACRange(OBC_ACRange *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ElockState_OBC_ElockState(OBC_ElockState *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Fault_OBC_Fault(OBC_Fault *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_Status_OBC_Status(OBC_Status *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState *data)
 *   Std_ReturnType Rte_Read_PpInt_SUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V *data)
 *   Std_ReturnType Rte_Read_PpInt_SUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_SUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus *data)
 *   Std_ReturnType Rte_Read_PpInt_SUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV *data)
 *   Std_ReturnType Rte_Read_PpInt_VERSION_APPLI_VERSION_APPLI(VERSION_APPLI *data)
 *   Std_ReturnType Rte_Read_PpInt_VERSION_SOFT_VERSION_SOFT(VERSION_SOFT *data)
 *   Std_ReturnType Rte_Read_PpInt_VERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME *data)
 *   Std_ReturnType Rte_Read_PpInt_VERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE *data)
 *   Std_ReturnType Rte_Read_PpInt_VERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR *data)
 *   Std_ReturnType Rte_Read_PpInt_VERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS *data)
 *   Std_ReturnType Rte_Read_PpOBC_OutputCurrent_ProducerError_DeOBC_OutputCurrent_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpOBC_OutputVoltage_ProducerError_DeOBC_OutputVoltage_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpOBC_SocketTempL_ProducerError_DeOBC_SocketTempL_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpOBC_SocketTempN_ProducerError_DeOBC_SocketTempN_ProducerError(boolean *data)
 *   Std_ReturnType Rte_Read_PpPlantModeState_DePlantModeState(boolean *data)
 *   Std_ReturnType Rte_Read_PpREFERENCE_HORAIRE_ProducerError_DeREFERENCE_HORAIRE_ProducerError(boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Debug1_1_Debug1_1(Debug1_1 data)
 *   Std_ReturnType Rte_Write_Debug1_2_Debug1_2(Debug1_2 data)
 *   Std_ReturnType Rte_Write_Debug1_3_Debug1_3(Debug1_3 data)
 *   Std_ReturnType Rte_Write_Debug1_4_Debug1_4(Debug1_4 data)
 *   Std_ReturnType Rte_Write_Debug1_5_Debug1_5(Debug1_5 data)
 *   Std_ReturnType Rte_Write_Debug1_6_Debug1_6(Debug1_6 data)
 *   Std_ReturnType Rte_Write_Debug1_7_Debug1_7(Debug1_7 data)
 *   Std_ReturnType Rte_Write_Debug2_0_Debug2_0(Debug2_0 data)
 *   Std_ReturnType Rte_Write_Debug2_1_Debug2_1(Debug2_1 data)
 *   Std_ReturnType Rte_Write_Debug2_2_Debug2_2(Debug2_2 data)
 *   Std_ReturnType Rte_Write_Debug2_3_Debug2_3(Debug2_3 data)
 *   Std_ReturnType Rte_Write_Debug2_4_Debug2_4(Debug2_4 data)
 *   Std_ReturnType Rte_Write_Debug2_5_Debug2_5(Debug2_5 data)
 *   Std_ReturnType Rte_Write_Debug2_6_Debug2_6(Debug2_6 data)
 *   Std_ReturnType Rte_Write_Debug2_7_Debug2_7(Debug2_7 data)
 *   Std_ReturnType Rte_Write_Debug3_0_Debug3_0(Debug3_0 data)
 *   Std_ReturnType Rte_Write_Debug3_1_Debug3_1(Debug3_1 data)
 *   Std_ReturnType Rte_Write_Debug3_2_Debug3_2(Debug3_2 data)
 *   Std_ReturnType Rte_Write_Debug3_3_Debug3_3(Debug3_3 data)
 *   Std_ReturnType Rte_Write_Debug3_4_Debug3_4(Debug3_4 data)
 *   Std_ReturnType Rte_Write_Debug3_5_Debug3_5(Debug3_5 data)
 *   Std_ReturnType Rte_Write_Debug3_6_Debug3_6(Debug3_6 data)
 *   Std_ReturnType Rte_Write_Debug3_7_Debug3_7(Debug3_7 data)
 *   Std_ReturnType Rte_Write_Debug4_0_Debug4_0(Debug4_0 data)
 *   Std_ReturnType Rte_Write_Debug4_1_Debug4_1(Debug4_1 data)
 *   Std_ReturnType Rte_Write_Debug4_2_Debug4_2(Debug4_2 data)
 *   Std_ReturnType Rte_Write_Debug4_3_Debug4_3(Debug4_3 data)
 *   Std_ReturnType Rte_Write_Debug4_4_Debug4_4(Debug4_4 data)
 *   Std_ReturnType Rte_Write_Debug4_5_Debug4_5(Debug4_5 data)
 *   Std_ReturnType Rte_Write_Debug4_6_Debug4_6(Debug4_6 data)
 *   Std_ReturnType Rte_Write_Debug4_7_Debug4_7(Debug4_7 data)
 *   Std_ReturnType Rte_Write_Degug1_0_Degug1_0(Degug1_0 data)
 *   Std_ReturnType Rte_Write_PpDCDC_CurrentReference_DCDC_CurrentReference(DCDC_CurrentReference data)
 *   Std_ReturnType Rte_Write_PpDCDC_Fault_DCDC_Fault(DCDC_Fault data)
 *   Std_ReturnType Rte_Write_PpDCDC_FaultLampRequest_DCDC_FaultLampRequest(DCDC_FaultLampRequest data)
 *   Std_ReturnType Rte_Write_PpDCDC_HighVoltConnectionAllowed_DCDC_HighVoltConnectionAllowed(DCDC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpDCDC_InputVoltage_DCDC_InputVoltage(DCDC_InputVoltage data)
 *   Std_ReturnType Rte_Write_PpDCDC_OVERTEMP_DCDC_OVERTEMP(DCDC_OVERTEMP data)
 *   Std_ReturnType Rte_Write_PpDCDC_OutputVoltage_DCDC_OutputVoltage(DCDC_OutputVoltage data)
 *   Std_ReturnType Rte_Write_PpDCDC_Status_DCDC_Status(DCDC_Status data)
 *   Std_ReturnType Rte_Write_PpDCDC_Temperature_DCDC_Temperature(DCDC_Temperature data)
 *   Std_ReturnType Rte_Write_PpDCDC_VoltageReference_DCDC_VoltageReference(DCDC_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpDCLV_VoltageReference_DCLV_VoltageReference(DCLV_VoltageReference data)
 *   Std_ReturnType Rte_Write_PpEDITION_CALIB_EDITION_CALIB(EDITION_CALIB data)
 *   Std_ReturnType Rte_Write_PpEDITION_SOFT_EDITION_SOFT(EDITION_SOFT data)
 *   Std_ReturnType Rte_Write_PpEVSE_RTAB_STOP_CHARGE_EVSE_RTAB_STOP_CHARGE(EVSE_RTAB_STOP_CHARGE data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_0_NEW_JDD_OBC_DCDC_BYTE_0(NEW_JDD_OBC_DCDC_BYTE_0 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_1_NEW_JDD_OBC_DCDC_BYTE_1(NEW_JDD_OBC_DCDC_BYTE_1 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_2_NEW_JDD_OBC_DCDC_BYTE_2(NEW_JDD_OBC_DCDC_BYTE_2 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_3_NEW_JDD_OBC_DCDC_BYTE_3(NEW_JDD_OBC_DCDC_BYTE_3 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_4_NEW_JDD_OBC_DCDC_BYTE_4(NEW_JDD_OBC_DCDC_BYTE_4 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_5_NEW_JDD_OBC_DCDC_BYTE_5(NEW_JDD_OBC_DCDC_BYTE_5 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_6_NEW_JDD_OBC_DCDC_BYTE_6(NEW_JDD_OBC_DCDC_BYTE_6 data)
 *   Std_ReturnType Rte_Write_PpNEW_JDD_OBC_DCDC_BYTE_7_NEW_JDD_OBC_DCDC_BYTE_7(NEW_JDD_OBC_DCDC_BYTE_7 data)
 *   Std_ReturnType Rte_Write_PpOBC_ACRange_OBC_ACRange(OBC_ACRange data)
 *   Std_ReturnType Rte_Write_PpOBC_CP_connection_Status_OBC_CP_connection_Status(OBC_CP_connection_Status data)
 *   Std_ReturnType Rte_Write_PpOBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati data)
 *   Std_ReturnType Rte_Write_PpOBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode data)
 *   Std_ReturnType Rte_Write_PpOBC_CommunicationSt_OBC_CommunicationSt(OBC_CommunicationSt data)
 *   Std_ReturnType Rte_Write_PpOBC_CoolingWakeup_OBC_CoolingWakeup(OBC_CoolingWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_DCChargingPlugAConnConf_OBC_DCChargingPlugAConnConf(OBC_DCChargingPlugAConnConf data)
 *   Std_ReturnType Rte_Write_PpOBC_ElockState_OBC_ElockState(OBC_ElockState data)
 *   Std_ReturnType Rte_Write_PpOBC_Fault_OBC_Fault(OBC_Fault data)
 *   Std_ReturnType Rte_Write_PpOBC_HVBattRechargeWakeup_OBC_HVBattRechargeWakeup(OBC_HVBattRechargeWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_HighVoltConnectionAllowed_OBC_HighVoltConnectionAllowed(OBC_HighVoltConnectionAllowed data)
 *   Std_ReturnType Rte_Write_PpOBC_HoldDiscontactorWakeup_OBC_HoldDiscontactorWakeup(OBC_HoldDiscontactorWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt data)
 *   Std_ReturnType Rte_Write_PpOBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt data)
 *   Std_ReturnType Rte_Write_PpOBC_OBCTemp_OBC_OBCTemp(OBC_OBCTemp data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputCurrent_OBC_OutputCurrent(OBC_OutputCurrent data)
 *   Std_ReturnType Rte_Write_PpOBC_OutputVoltage_OBC_OutputVoltage(OBC_OutputVoltage data)
 *   Std_ReturnType Rte_Write_PpOBC_PIStateInfoWakeup_OBC_PIStateInfoWakeup(OBC_PIStateInfoWakeup data)
 *   Std_ReturnType Rte_Write_PpOBC_PlugVoltDetection_OBC_PlugVoltDetection(OBC_PlugVoltDetection data)
 *   Std_ReturnType Rte_Write_PpOBC_PowerMax_OBC_PowerMax(OBC_PowerMax data)
 *   Std_ReturnType Rte_Write_PpOBC_PushChargeType_OBC_PushChargeType(OBC_PushChargeType data)
 *   Std_ReturnType Rte_Write_PpOBC_RechargeHMIState_OBC_RechargeHMIState(OBC_RechargeHMIState data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data)
 *   Std_ReturnType Rte_Write_PpOBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data)
 *   Std_ReturnType Rte_Write_PpOBC_Status_OBC_Status(OBC_Status data)
 *   Std_ReturnType Rte_Write_PpSG_DC2_SG_DC2(const SG_DC2 *data)
 *   Std_ReturnType Rte_Write_PpSUPV_CoolingWupState_SUPV_CoolingWupState(SUPV_CoolingWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_DTCRegistred_SUPV_DTCRegistred(SUPV_DTCRegistred data)
 *   Std_ReturnType Rte_Write_PpSUPV_ECUElecStateRCD_SUPV_ECUElecStateRCD(SUPV_ECUElecStateRCD data)
 *   Std_ReturnType Rte_Write_PpSUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(SUPV_HVBattChargeWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(SUPV_HoldDiscontactorWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(SUPV_PIStateInfoWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PostDriveWupState_SUPV_PostDriveWupState(SUPV_PostDriveWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PreDriveWupState_SUPV_PreDriveWupState(SUPV_PreDriveWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_PrecondElecWupState_SUPV_PrecondElecWupState(SUPV_PrecondElecWupState data)
 *   Std_ReturnType Rte_Write_PpSUPV_RCDLineState_SUPV_RCDLineState(SUPV_RCDLineState data)
 *   Std_ReturnType Rte_Write_PpSUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(SUPV_StopDelayedHMIWupState data)
 *   Std_ReturnType Rte_Write_PpSUP_CommandVDCLink_V_SUP_CommandVDCLink_V(SUP_CommandVDCLink_V data)
 *   Std_ReturnType Rte_Write_PpSUP_RequestDCLVStatus_SUP_RequestDCLVStatus(SUP_RequestDCLVStatus data)
 *   Std_ReturnType Rte_Write_PpSUP_RequestPFCStatus_SUP_RequestPFCStatus(SUP_RequestPFCStatus data)
 *   Std_ReturnType Rte_Write_PpSUP_RequestStatusDCHV_SUP_RequestStatusDCHV(SUP_RequestStatusDCHV data)
 *   Std_ReturnType Rte_Write_PpVERSION_APPLI_VERSION_APPLI(VERSION_APPLI data)
 *   Std_ReturnType Rte_Write_PpVERSION_SOFT_VERSION_SOFT(VERSION_SOFT data)
 *   Std_ReturnType Rte_Write_PpVERSION_SYSTEME_VERSION_SYSTEME(VERSION_SYSTEME data)
 *   Std_ReturnType Rte_Write_PpVERS_DATE2_ANNEE_VERS_DATE2_ANNEE(VERS_DATE2_ANNEE data)
 *   Std_ReturnType Rte_Write_PpVERS_DATE2_JOUR_VERS_DATE2_JOUR(VERS_DATE2_JOUR data)
 *   Std_ReturnType Rte_Write_PpVERS_DATE2_MOIS_VERS_DATE2_MOIS(VERS_DATE2_MOIS data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgM_CheckpointIdType CPID)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_WdgM_AliveSupervision_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPCOM_task5msTX_doc
 *********************************************************************************************************************/

/*!
 * \brief PCOM TX task.
 *
 * This function is called periodically by the scheduler. This function updates
 * the trigger pending flag for each frame. It also manages the signal
 * requirements.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPCOM_CODE) RCtApPCOM_task5msTX(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApPCOM_task5msTX
 *********************************************************************************************************************/

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    static boolean framesIntialized = FALSE;

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint8 PCOM_FrameIndex;


  /* Signal transmission management.
   * Internal, mixed and periodic frames are always updated.
   * If the event function is TRUE the event frame is updated, and mixed frame triggered.
   */
  #define PCOM_DISABLE_TX 1;

    if (FALSE == PCOM_ProgramFlowSoftModeEnabled)
    {
    	(void)Rte_Call_alive_WdgMSupervisedEntityProgramFlow_CheckpointReached(WdgMConf_WdgMCheckpoint_WdgMCheckpointProgramFlowCP1);
    }


  if(FALSE == framesIntialized)
    {
      /* First time frames are sent are initialized to init value */
#ifdef PCOM_DISABLE_TX
      (void)PCOM_TX_initializeFrames();
#endif
      framesIntialized = TRUE;
    }

  for (PCOM_FrameIndex = 0U; PCOM_FrameIndex < PCOM_NUM_TX_FRAMES;
      PCOM_FrameIndex++) {
#ifdef PCOM_DISABLE_TX
      PCOM_TX_task_UpdateSignal(&PCOM_TX_Frame_Config[PCOM_FrameIndex],&PCOM_TX_Frame_Data[PCOM_FrameIndex]);
#endif
  }

	(void)Rte_Read_PpCANComRequest_DeCANComRequest(&PCOM_CANComRequest);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	PCOM_Generate_DGN_conditions();

	(void)Rte_Write_Degug1_0_Degug1_0(PCOM_IntCANDebug1[0]);
	(void)Rte_Write_Debug1_1_Debug1_1(PCOM_IntCANDebug1[1]);
	(void)Rte_Write_Debug1_2_Debug1_2(PCOM_IntCANDebug1[2]);
	(void)Rte_Write_Debug1_3_Debug1_3(PCOM_IntCANDebug1[3]);
	(void)Rte_Write_Debug1_4_Debug1_4(PCOM_IntCANDebug1[4]);
	(void)Rte_Write_Debug1_5_Debug1_5(PCOM_IntCANDebug1[5]);
	(void)Rte_Write_Debug1_6_Debug1_6(PCOM_IntCANDebug1[6]);
	(void)Rte_Write_Debug1_7_Debug1_7(PCOM_IntCANDebug1[7]);

	(void)Rte_Write_Debug2_0_Debug2_0(PCOM_IntCANDebug2[0]);
	(void)Rte_Write_Debug2_1_Debug2_1(PCOM_IntCANDebug2[1]);
	(void)Rte_Write_Debug2_2_Debug2_2(PCOM_IntCANDebug2[2]);
	(void)Rte_Write_Debug2_3_Debug2_3(PCOM_IntCANDebug2[3]);
	(void)Rte_Write_Debug2_4_Debug2_4(PCOM_IntCANDebug2[4]);
	(void)Rte_Write_Debug2_5_Debug2_5(PCOM_IntCANDebug2[5]);
	(void)Rte_Write_Debug2_6_Debug2_6(PCOM_IntCANDebug2[6]);
	(void)Rte_Write_Debug2_7_Debug2_7(PCOM_IntCANDebug2[7]);

	(void)Rte_Write_Debug3_0_Debug3_0(PCOM_IntCANDebug3[0]);
	(void)Rte_Write_Debug3_1_Debug3_1(PCOM_IntCANDebug3[1]);
	(void)Rte_Write_Debug3_2_Debug3_2(PCOM_IntCANDebug3[2]);
	(void)Rte_Write_Debug3_3_Debug3_3(PCOM_IntCANDebug3[3]);
	(void)Rte_Write_Debug3_4_Debug3_4(PCOM_IntCANDebug3[4]);
	(void)Rte_Write_Debug3_5_Debug3_5(PCOM_IntCANDebug3[5]);
	(void)Rte_Write_Debug3_6_Debug3_6(PCOM_IntCANDebug3[6]);
	(void)Rte_Write_Debug3_7_Debug3_7(PCOM_IntCANDebug3[7]);

	(void)Rte_Write_Debug4_0_Debug4_0(PCOM_IntCANDebug4[0]);
	(void)Rte_Write_Debug4_1_Debug4_1(PCOM_IntCANDebug4[1]);
	(void)Rte_Write_Debug4_2_Debug4_2(PCOM_IntCANDebug4[2]);
	(void)Rte_Write_Debug4_3_Debug4_3(PCOM_IntCANDebug4[3]);
	(void)Rte_Write_Debug4_4_Debug4_4(PCOM_IntCANDebug4[4]);
	(void)Rte_Write_Debug4_5_Debug4_5(PCOM_IntCANDebug4[5]);
	(void)Rte_Write_Debug4_6_Debug4_6(PCOM_IntCANDebug4[6]);
	(void)Rte_Write_Debug4_7_Debug4_7(PCOM_IntCANDebug4[7]);



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RSetIntCANDebugSignal
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <OpSetIntCANDebugSignal> of PortPrototype <PpSetIntCANDebugSignal>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RSetIntCANDebugSignal_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApPCOM_CODE) RSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RSetIntCANDebugSignal
 *********************************************************************************************************************/

	if (byteNum < 8U)
	{

		switch (frameNum)
		{
		case 0:
			PCOM_IntCANDebug1[byteNum] = data;
			break;
		case 1:
			PCOM_IntCANDebug2[byteNum] = data;
			break;
		case 2:
			PCOM_IntCANDebug3[byteNum] = data;
			break;
		case 3:
			PCOM_IntCANDebug4[byteNum] = data;
			break;
		default:
			/* MISRA required */
			break;
		}

	}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApPCOM_STOP_SEC_CODE
#include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/

/** \defgroup PCOM_TX_Signal_actions TX actions to update CAN signals with calculated signals
 ** \{
 **/

static void PCOM_TX_processSignals(
    const PCOM_TX_Frame_Signals_Config_t *frameSignals) {

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint16 signalsIndex;
    boolean signalActive;


  if ((NULL_PTR != frameSignals) && (NULL_PTR != frameSignals->signalsData)) {
      for (signalsIndex = 0U; signalsIndex < frameSignals->signalsCount;
	  signalsIndex++) 
    {
	    signalActive = (boolean)frameSignals->signalsData[signalsIndex].enable();
	    if ((boolean)PCOM_SIGNAL_INACTIVE== signalActive) 
      {
	        (void) PCOM_TX_inactiveSignal(&frameSignals->signalsData[signalsIndex]);
	    } else
      {
	      (void) PCOM_TX_activeSignal(&frameSignals->signalsData[signalsIndex]);
	    }

    }
  }
}

static void PCOM_TX_initializeFrames(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint16 framesIndex;


  for (framesIndex = 0U; framesIndex < PCOM_NUM_TX_FRAMES;
      framesIndex++) {

      PCOM_TX_initializeFrameSignals(&PCOM_TX_Frame_Config[framesIndex]);
      PCOM_TX_Frame_Data[framesIndex].ComposeTimeout = PCOM_TX_SEND_FRAME_LEVEL;
  }
}

static void PCOM_TX_initializeFrameSignals(const PCOM_TX_Frame_Config_t * frameData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
  uint16 signalsIndex;


  if(frameData->mode != PCOM_EVENT)
    {
      for (signalsIndex = 0U; signalsIndex < frameData->signals.signalsCount;
	  signalsIndex++) {
	  PCOM_TX_setInitialSignal(&frameData->signals.signalsData[signalsIndex]);
      }
    }
}

static void PCOM_TX_setCalculatedSignal(const PCOM_TX_Signals_Config_t * signalData)
{
  if (NULL_PTR != signalData) {
      (void)PCOM_TX_bulkFunction(signalData,signalData->get.calculated);
  }
}

static void PCOM_TX_setLowerSignal(const PCOM_TX_Signals_Config_t * signalData)
{
  if (NULL_PTR != signalData)
    {
      (void)PCOM_TX_bulkValue(signalData,&signalData->limits.lower); /* PRQA S 0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
    }
}

static void PCOM_TX_setUpperSignal(const PCOM_TX_Signals_Config_t * signalData)
{
  if (NULL_PTR != signalData) {
      (void)PCOM_TX_bulkValue(signalData,&signalData->limits.upper);/* PRQA S 0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
  }
}

static void PCOM_TX_setInitialSignal(const PCOM_TX_Signals_Config_t * signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint32 defaultInitial =PCOM_DEFAULT_INITIAL_VALUE;


  if (NULL_PTR != signalData) {
      if(PCOM_FEATURE_NA != signalData->get.initial)
	{
	  (void)PCOM_TX_bulkFunction(signalData,signalData->get.initial);
	}
      else
	{
	  (void)PCOM_TX_bulkValue(signalData,&defaultInitial);/* PRQA S 0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
	}
  }
}

static void PCOM_TX_setReservedSignal(const PCOM_TX_Signals_Config_t * signalData)
{
  if (NULL_PTR != signalData) {
      (void)PCOM_TX_bulkValue(signalData,&signalData->values.reserved);/* PRQA S 0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
  }
}

static void PCOM_TX_setInvalidSignal(const PCOM_TX_Signals_Config_t * signalData)
{
  if (NULL_PTR != signalData) {
      (void)PCOM_TX_bulkValue(signalData, &signalData->values.invalid);/* PRQA S 0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
  }
}

static void PCOM_TX_inactiveSignal(const PCOM_TX_Signals_Config_t * signalData)
{
  if (NULL_PTR != signalData) {
      if (PCOM_SIGNAL_APPLICABLE == signalData->applicableReserved) {
	  /* Implements OBCP-24483 */
	  PCOM_TX_setReservedSignal(signalData);

      } else {
	  /* Implements OBCP-24486 */
	  PCOM_TX_setInitialSignal(signalData);
      }
  }
}

static void PCOM_TX_activeSignal(const PCOM_TX_Signals_Config_t * signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    PCOM_signalRange_t signalInRange;
    pcom_gen_t value = 0U;
    boolean producerError;


  if (NULL_PTR != signalData) {
      if(PCOM_FEATURE_NA != signalData->get.producerError)
	{
	  producerError = signalData->get.producerError();
	}
      else
	{
	  producerError = FALSE;
	}

      if (FALSE == producerError) {

	  if ((PCOM_CANSIGNAL_TYPE_UNM == signalData->canSignalType)
	      || (PCOM_CANSIGNAL_TYPE_SNM == signalData->canSignalType)) {
	      /* Implements OBCP-24490 */
	      /* Implements OBCP-24493 */
	      ((callableGeneric)(signalData->get.calculated))(&value);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */

	      signalInRange =  PCOM_TX_signal_inRange(signalData,value);

	      if(PCOM_SIGNAL_RANGE_OVER == signalInRange )
		{
		  PCOM_TX_setUpperSignal(signalData);

		}
	      else if(PCOM_SIGNAL_RANGE_UNDER == signalInRange)
		{
		  PCOM_TX_setLowerSignal(signalData);
		}
	      else
		{
		  PCOM_TX_setCalculatedSignal(signalData);
		}


	  } else if (PCOM_CANSIGNAL_TYPE_BMP == signalData->canSignalType) {

	      /* Implements OBCP-24521 */
	      PCOM_TX_setCalculatedSignal(signalData);
	  }else if ((PCOM_CANSIGNAL_TYPE_NA == signalData->canSignalType) || (PCOM_CANSIGNAL_TYPE_OTHER == signalData->canSignalType)) {

	      PCOM_TX_setCalculatedSignal(signalData);
	  }
	  else
	    {
	      /* NOT PARSED SIGNALS, DO NOTHINHG - MISRA COMPLIANCE */
	    }
      } else {
	  /* Implements OBCP-24489 */
	  PCOM_TX_setInvalidSignal(signalData);
      }
  }
}

static PCOM_signalRange_t PCOM_TX_signal_inRange(const PCOM_TX_Signals_Config_t *signalData, pcom_gen_t value)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    PCOM_signalRange_t retVal = PCOM_SIGNAL_RANGE_OK;


  if((signalData->limits.range == PCOM_CANSIGNAL_LIMITS_BOTH) || (signalData->limits.range == PCOM_CANSIGNAL_LIMITS_LOWER))
    {
      if(signalData->dataType >= PCOM_SIGNAL_TYPE_S8)
	{
	  if((pcom_limits_type_lower_t) value < (pcom_limits_type_lower_t) signalData->limits.lower)
	    {
	      retVal =  PCOM_SIGNAL_RANGE_UNDER;
	    }
	}
      else if((pcom_limits_type_higher_t) value < (pcom_limits_type_higher_t) signalData->limits.lower)
	{
	  retVal =  PCOM_SIGNAL_RANGE_UNDER;
	}
      else
	{
	  /* DO NOTHING - MISRA COMPLIANCE */
	}
    }

  if(((signalData->limits.range == PCOM_CANSIGNAL_LIMITS_BOTH) ||(signalData->limits.range == PCOM_CANSIGNAL_LIMITS_UPPER)) && (PCOM_SIGNAL_RANGE_OK == retVal))
    {
      if(signalData->dataType >= PCOM_SIGNAL_TYPE_S8)
	{
	  if((pcom_limits_type_lower_t) value > (pcom_limits_type_lower_t)signalData->limits.upper)
	    {
	      retVal =  PCOM_SIGNAL_RANGE_OVER;
	    }
	}
      else if((pcom_limits_type_higher_t) value > (pcom_limits_type_higher_t) signalData->limits.upper)
	{
	  retVal =  PCOM_SIGNAL_RANGE_OVER;
	}
      else
	{
	  /* DO NOTHING - MISRA COMPLIANCE */
	}
    }
  return retVal;
}

static Std_ReturnType PCOM_TX_bulkValue(const PCOM_TX_Signals_Config_t * signalData, const void * value)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Std_ReturnType retVal = E_NOT_OK;


  if ((NULL_PTR != signalData) && (NULL_PTR != value)) {
      switch (signalData->dataType) {
	case PCOM_SIGNAL_TYPE_BOOL:
	  {
	    retVal = ((callableBoolConst)signalData->canSignal)((const boolean*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }
	case PCOM_SIGNAL_TYPE_U8:
	  {
	    retVal = ((callableU8Const) signalData->canSignal)((const uint8*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }
	case PCOM_SIGNAL_TYPE_U16:
	  {
	    retVal = ((callableU16Const)signalData->canSignal)((const uint16*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }
	case PCOM_SIGNAL_TYPE_U32:
	  {
	    retVal = ((callableU32Const)signalData->canSignal)((const uint32*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }

	case PCOM_SIGNAL_TYPE_S8:
	  {
	    retVal = ((callableS8Const)signalData->canSignal)((const sint8*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }
	case PCOM_SIGNAL_TYPE_S16:
	  {
	    retVal = ((callableS16Const)signalData->canSignal)((const sint16*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }
	case PCOM_SIGNAL_TYPE_S32:
	  {
	    retVal = ((callableS32Const)signalData->canSignal)((const sint32*) value);/* PRQA S 0313,0316 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    break;
	  }

	default: {
	  /* Definition  error */
	  break;
	}
      }
  }
  return retVal;
}

static Std_ReturnType PCOM_TX_bulkFunction(const PCOM_TX_Signals_Config_t * signalData, const getValFuncPtr valPtr)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Std_ReturnType retVal = E_NOT_OK;
    uint8 tempValU8;
    uint16 tempValU16;
    uint32 tempValU32;
    sint8 tempValS8;
    sint16 tempValS16;
    sint32 tempValS32;
    boolean tempValBool;


  if ((NULL_PTR != signalData) && (NULL_PTR != valPtr)) {
      switch (signalData->dataType) {
	case PCOM_SIGNAL_TYPE_BOOL:
	  {
	    retVal = ((callableBool)valPtr)(&tempValBool);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableBool)signalData->canSignal)(&tempValBool);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }
	case PCOM_SIGNAL_TYPE_U8:
	  {
	    retVal = ((callableU8)valPtr)(&tempValU8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableU8) signalData->canSignal)(&tempValU8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }
	case PCOM_SIGNAL_TYPE_U16:
	  {
	    retVal = ((callableU16)valPtr)(&tempValU16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableU16)signalData->canSignal)(&tempValU16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }
	case PCOM_SIGNAL_TYPE_U32:
	  {
	    retVal = ((callableU32)valPtr)(&tempValU32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableU32)signalData->canSignal)(&tempValU32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }

	case PCOM_SIGNAL_TYPE_S8:
	  {
	    retVal = ((callableS8)valPtr)(&tempValS8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableS8)signalData->canSignal)(&tempValS8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }
	case PCOM_SIGNAL_TYPE_S16:
	  {
	    retVal = ((callableS16)valPtr)(&tempValS16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableS16)signalData->canSignal)(&tempValS16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }
	case PCOM_SIGNAL_TYPE_S32:
	  {
	    retVal = ((callableS32)valPtr)(&tempValS32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	    if(E_OK == retVal)
	      {
		retVal = ((callableS32)signalData->canSignal)(&tempValS32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
	      }
	    break;
	  }

	default:
	  {
	    /* Definition  error */
	    break;
	  }
      }
  }
  return retVal;
}


/** \} end of PCOM_RX_Signal_actions */


/**
 * \defgroup PCOM_RX_Signal_actions RX actions to update CAN signals with calculated signals
 * \{
 **/

static Std_ReturnType PCOM_RX_setOutput(const PCOM_RX_Signals_Config_t * signalData, PCOM_TX_orig_ptr_t src)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Std_ReturnType retVal = E_NOT_OK;


    if(NULL_PTR != signalData)
    {
      switch (src) {
	case PCOM_TX_ORIG_INITIAL:
	  {
	    if(PCOM_FEATURE_NA != signalData->get.initial)
	      {
		retVal = PCOM_RX_bulkFunction(signalData,PCOM_TX_ORIG_INITIAL);
	      }
	    else
	      {
		retVal = PCOM_RX_bulkVal(signalData,PCOM_TX_DEFAULT_INITIAL);
	      }
	    break;
	  }
	case PCOM_TX_ORIG_NOTAVAIL:
	  {
	    retVal = PCOM_RX_bulkFunction(signalData,PCOM_TX_ORIG_NOTAVAIL);
	    break;
	  }
	case PCOM_TX_ORIG_PHYSICAL:
	  {
	    retVal = PCOM_RX_bulkFunction(signalData,PCOM_TX_ORIG_PHYSICAL);
	    break;
	  }
	case PCOM_TX_ORIG_MEMORIZED:
	  {
	    retVal = PCOM_RX_bulkVal(signalData,PCOM_TX_ORIG_MEMORIZED);
	    break;
	  }
	case PCOM_TX_ORIG_SUBSTITUTION:
	  {
	    retVal = PCOM_RX_bulkVal(signalData,PCOM_TX_ORIG_SUBSTITUTION);
	    break;
	  }
	case PCOM_TX_ORIG_RAW:
	  {
	    retVal = PCOM_RX_bulkVal(signalData,PCOM_TX_ORIG_RAW);
	    break;
	  }
	default:
	  {
		/* MISRA required */
	    break;
	  }
      }
    }
  return retVal;
}

static Std_ReturnType PCOM_RX_bulkFunction(const PCOM_RX_Signals_Config_t * signalData, PCOM_TX_orig_ptr_t src)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Std_ReturnType retVal = E_NOT_OK;
    uint8 tempValU8;
    uint16 tempValU16;
    uint32 tempValU32;
    sint8 tempValS8;
    sint16 tempValS16;
    sint32 tempValS32;
    boolean tempValBool;
    getValFuncPtr srcGet;
    getValFuncPtr dstGet;
    boolean pipeOK = TRUE;


  /* Switch the origin of value */
  switch (src) {
    case PCOM_TX_ORIG_INITIAL:
      {
	srcGet = signalData->get.initial;
	break;
      }
    case PCOM_TX_ORIG_PHYSICAL:
      {
	srcGet = signalData->get.physical;
	break;
      }
    case PCOM_TX_ORIG_NOTAVAIL:
    default:
      {
	pipeOK = FALSE;
	break;
      }
  }

  /* Switch the destination of value */
  dstGet = signalData->set.outputValue;

  if(TRUE == pipeOK)
    {
      if ((NULL_PTR != srcGet) && (NULL_PTR != dstGet) ) {
	  switch (signalData->dataType) {
	    case PCOM_SIGNAL_TYPE_BOOL:
	      {
		retVal = ((callableBool)srcGet)(&tempValBool);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableBool)dstGet)(&tempValBool);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }
	    case PCOM_SIGNAL_TYPE_U8:
	      {
		retVal = ((callableU8)srcGet)(&tempValU8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableU8)dstGet)(&tempValU8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }
	    case PCOM_SIGNAL_TYPE_U16:
	      {
		retVal = ((callableU16)srcGet)(&tempValU16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableU16)dstGet)(&tempValU16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }
	    case PCOM_SIGNAL_TYPE_U32:
	      {
		retVal = ((callableU32)srcGet)(&tempValU32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableU32)dstGet)(&tempValU32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }

	    case PCOM_SIGNAL_TYPE_S8:
	      {
		retVal = ((callableS8)srcGet)(&tempValS8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableS8)dstGet)(&tempValS8);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }
	    case PCOM_SIGNAL_TYPE_S16:
	      {
		retVal = ((callableS16)srcGet)(&tempValS16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableS16)dstGet)(&tempValS16);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }
	    case PCOM_SIGNAL_TYPE_S32:
	      {
		retVal = ((callableS32)srcGet)(&tempValS32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		if(E_OK == retVal)
		  {
		    retVal = ((callableS32)dstGet)(&tempValS32);/* PRQA S 0313 # Cast needed to be used with any posible data type and to be general enough for all signals */
		  }
		break;
	      }

	    default:
	      {
		/* Definition  error */
		break;
	      }
	  }
      }
    }
  return retVal;
}

static Std_ReturnType PCOM_RX_bulkVal(const PCOM_RX_Signals_Config_t * signalData, PCOM_TX_orig_ptr_t src)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Std_ReturnType retVal = E_NOT_OK;
    pcom_gen_t srcVal;
    getValFuncPtr dstGet;
    boolean pipeOK = TRUE;


  /* Switch the origin of value */
  switch (src) {

    case PCOM_TX_ORIG_NOTAVAIL:
      {
	if(TRUE == signalData->get.notAvailableCAN.isAvailable)
	  {
	    srcVal =  signalData->get.notAvailableCAN.value;
	  }else{
	      pipeOK = FALSE;
	  }
	break;
      }
    case PCOM_TX_ORIG_MEMORIZED:
      {
	srcVal = signalData->data->memorizedValue;
	break;
      }
    case PCOM_TX_ORIG_SUBSTITUTION:
      {
	srcVal = signalData->data->substutionValue;
	break;
      }
    case PCOM_TX_ORIG_RAW:
      {
	srcVal = signalData->data->rawValue;
	break;
      }
    case PCOM_TX_DEFAULT_INITIAL:
      {
	srcVal = PCOM_DEFAULT_INITIAL_VALUE;
	break;
      }
    default:
      {
	pipeOK = FALSE;
	break;
      }
  }

  dstGet = signalData->set.outputValue;

  if(TRUE == pipeOK)
    {
      if (NULL_PTR != dstGet) {
	  switch (signalData->dataType) {
	    case PCOM_SIGNAL_TYPE_BOOL:
	      {
		retVal = ((callableBool)dstGet)((boolean*) &srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */

		break;
	      }
	    case PCOM_SIGNAL_TYPE_U8:
	      {
		retVal = ((callableU8)dstGet)((uint8 *) &srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */
		break;
	      }
	    case PCOM_SIGNAL_TYPE_U16:
	      {
		retVal = ((callableU16)dstGet)((uint16 *)&srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */
		break;
	      }
	    case PCOM_SIGNAL_TYPE_U32:
	      {
		retVal = ((callableU32)dstGet)((uint32 *)&srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */
		break;
	      }

	    case PCOM_SIGNAL_TYPE_S8:
	      {
		retVal = ((callableS8)dstGet)((sint8 *)&srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */
		break;
	      }
	    case PCOM_SIGNAL_TYPE_S16:
	      {
		retVal = ((callableS16)dstGet)((sint16 *)&srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */
		break;
	      }
	    case PCOM_SIGNAL_TYPE_S32:
	      {
		retVal = ((callableS32)dstGet)((sint32 *)&srcVal);/* PRQA S 0313,0310 # Cast needed to be used with any posible data type and to be general enough for all signals */
		break;
	      }

	    default:
	      {
		/* Definition  error */
		break;
	      }
	  }
      }
    }
  return retVal;
}

static void PCOM_RX_diagnosisDebounce(boolean PCOM_input, volatile boolean *PCOM_output,
				      uint32 PCOM_confirmTime, uint32 PCOM_healTime, uint32 *PCOM_counter)
{
  if ((NULL_PTR != PCOM_counter) && (NULL_PTR != PCOM_output)) {
      uint32 PCOM_counterTime_max;
      if (TRUE == PCOM_input) {
	  PCOM_counterTime_max = PCOM_confirmTime;
      } else {
	  PCOM_counterTime_max = PCOM_healTime;
      }

      if (PCOM_input == *PCOM_output) {
	  /*Edge detected, clean counter*/
	  *PCOM_counter = 0U;
      } else if (*PCOM_counter < PCOM_counterTime_max) {
	  /*update counter*/
	  (*PCOM_counter)++;
      } else {
	  /*Counter time reached, update output*/
	  *PCOM_output = PCOM_input;
	  *PCOM_counter = 0U;
      }
  }

}

static boolean PCOM_RX_FramesGetDiagResult(PCOM_DiagEnableFunc_t function)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    boolean retVal;


  if(NULL_PTR == function)
    {
      retVal = FALSE;
    }
  else{
      retVal = (boolean)function();
  }
  return retVal;
}

static void PCOM_RX_processSignals(const PCOM_RX_Frame_Config_t * frameConfig, PCOM_RX_Frame_Data_t * frameData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
  boolean diagLostFrameEnable;
  boolean diagLostFrameFault;
  boolean signalActive;
  const PCOM_RX_Signals_Config_t *rx_signal;
  uint16 signalIndex;
  boolean frameProcess = FALSE;

  if ((NULL_PTR != frameConfig) && (NULL_PTR != frameData))
  {

	frameData->LID.forbiddenPrefault = PCOM_TX_DEBOUNCE_PASSING;
	frameData->LID.forbidenFault = PCOM_TX_DIAGFAULT_NO_DETECTED;

	diagLostFrameEnable = PCOM_RX_FramesGetDiagResult(frameConfig->DiagLostFrameEnableFunc);
	diagLostFrameFault  = frameData->LostFrameFault;

	if (FALSE != frameData->receiveFlag)
	{
		frameProcess = TRUE;
	}

	for(signalIndex = 0; signalIndex < frameConfig->signals.signalsCount; signalIndex ++)
	{
	  if(PCOM_FEATURE_NA != frameConfig->signals.signalsData)
	  {
	    rx_signal = &frameConfig->signals.signalsData[signalIndex];
	    if (NULL_PTR != rx_signal)
	    {
	      signalActive = (boolean)rx_signal->enable;

	      if (FALSE != frameProcess)
	      {

	        /* Process only if the frame has been just received */
			if(signalActive == (boolean)PCOM_SIGNAL_INACTIVE)
			{
			  PCOM_RX_task_processSignals_inactive(rx_signal);
			}
			else
			{
			  if(TRUE == rx_signal->diagnosticable)
			  {
			    if(FALSE == diagLostFrameEnable)
				{
  				  if(FALSE == diagLostFrameFault)
				  {
				    /* Implements OBCP-24510 */
					PCOM_RX_task_processSignals_signalsFlow(rx_signal);
				  }
  				  /* If this condition is not met, then it shall be processed by the permanent diagnostic part */
				}
				else
			    {
				  /* Implements OBCP-24520 */
				  PCOM_RX_task_processSignals_signalsFlow(rx_signal);
			    } /* if(FALSE == diagLostFrameEnable) else */
			  }
			  else
			  {
			    /* If internal, parse directly */
			    (void)PCOM_RX_setOutput(rx_signal,PCOM_TX_ORIG_PHYSICAL);
			  } /* if(TRUE == rx_signal->diagnosticable)  else */

		    }/* if(signalActive == PCOM_SIGNAL_INACTIVE) else */

	      }/*if (FALSE != frameData->receiveFlag)*/


	      /* This part has to be executed always, independently of the reception flag of the frame */

	      if(signalActive == (boolean)PCOM_SIGNAL_INACTIVE)
	      {
  		    PCOM_RX_task_processSignals_inactive(rx_signal);
		  }
		  else
		  {
	    	if(TRUE == rx_signal->diagnosticable)
	    	{
	    	  if((FALSE == diagLostFrameEnable) && (TRUE == diagLostFrameFault))
	    	  {
	            /* Implements OBCP-24509 */
			    (void)PCOM_RX_setOutput(rx_signal,PCOM_TX_ORIG_MEMORIZED);
	    	  }

	    	  PCOM_RX_task_processSignals_Diagnostics(frameConfig,frameData,rx_signal);
	    	}
	      }

	    }/* if (NULL_PTR != rx_signal) */
	  }/* if(PCOM_FEATURE_NA != &frameConfig->signals.signalsData) */
    }/* signal FOR loop */

	if (FALSE != frameProcess)
	{
      /* This part shall avoid losing interruptions received after the frame processing is started */
	  frameData->receiveFlag = FALSE;
	}
  }/* If check pointers */
}

static void PCOM_RX_task_processSignals_inactive(const PCOM_RX_Signals_Config_t *signalData)
{
  /* Implements OBCP-24457 */
  (void)PCOM_RX_setOutput(signalData,PCOM_TX_ORIG_INITIAL);
  PCOM_RX_setAvailabilityFlag(signalData,PCOM_SIGNAL_NOTAVAILABLE);
  PCOM_RX_setDiagInvalidFault(signalData,PCOM_TX_DIAGFAULT_NO_DETECTED);
  PCOM_RX_setDiagForbidenFault(signalData,PCOM_TX_DIAGFAULT_NO_DETECTED);
}

static void PCOM_RX_task_processSignals_signalsFlow(const PCOM_RX_Signals_Config_t *signalData)
{
  PCOM_RX_task_processSignals_Valid_OutputSelectorFault(signalData);
  PCOM_RX_task_processSignals_Valid_Availability(signalData);

}

static void PCOM_RX_task_processSignals_Valid_OutputSelectorFault(const PCOM_RX_Signals_Config_t *signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    boolean outputSelectorFault;
    pcom_gen_t  defaultSubstitutionVal;


    /* Implements OBCP-25024 */

  if(NULL_PTR != signalData->get.outputSelectorFault)
    {
      outputSelectorFault = signalData->get.outputSelectorFault();

      if(TRUE == outputSelectorFault)
	{
	  PCOM_RX_storeSubstitutionVal(signalData,&signalData->data->memorizedValue);
	}
      else
	{
	  signalData->get.defaultSubstutionValue((void*) &defaultSubstitutionVal);/* PRQA S 0314 # Cast needed to be used with any posible data type and to be general enough for all signals */
	  PCOM_RX_storeSubstitutionVal(signalData,&defaultSubstitutionVal);
	}
    }

}

static void  PCOM_RX_task_processSignals_Valid_Availability(const PCOM_RX_Signals_Config_t *signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    pcom_gen_t physicalVal =0U;
    pcom_gen_t limitedVal;
    boolean forbidden;
    PCOM_signalRange_t inRange;


  if(NULL_PTR != signalData->get.physical)
    {
      signalData->get.physical((void *)&physicalVal);/* PRQA S 0314 # Cast needed to be used with any posible data type and to be general enough for all signals */
      if(signalData->porpouse != PCOM_SIGNAL_NORMAL)
	{
	  /* Rolling Counter or CRC */
	  /* Implements OBCP-24517 */
	  PCOM_RX_storeRaw(signalData,&physicalVal);
	}
      else if(((boolean)signalData->get.notAvailableCAN.isAvailable) && (physicalVal == signalData->get.notAvailableCAN.value))/* PRQA S 4558 # This platform does not support the native bool data type */
	{
	  /* Implements OBCP-24461 */
	  PCOM_RX_setAvailabilityFlag(signalData,PCOM_SIGNAL_NOTAVAILABLE);
	  PCOM_RX_storeRaw(signalData,&signalData->data->substutionValue);
	}
      else
	{
	  if(NULL_PTR != signalData->get.forbidden)
	    {
	      forbidden = signalData->get.forbidden(&physicalVal);
	    }
	  else
	    {
	      forbidden = FALSE;
	    }

	  if((TRUE == forbidden) || (PCOM_RX_signal_isInvalid((signalData),(physicalVal))))
	    {
	      /* Implements OBCP-24462  */
	      PCOM_RX_setAvailabilityFlag(signalData, PCOM_SIGNAL_AVAILABLE);
	      PCOM_RX_storeRaw(signalData,&signalData->data->memorizedValue);
	    }
	  else
	    {
	      /* Implements OBCP-24463 */
	      PCOM_RX_setAvailabilityFlag(signalData, PCOM_SIGNAL_AVAILABLE);
	      PCOM_RX_storeRaw(signalData,&physicalVal);

	      if((signalData->data->diagInvalidFault == PCOM_TX_DIAGFAULT_NO_DETECTED)  &&
		  (signalData->data->diagForbidenFault == PCOM_TX_DIAGFAULT_NO_DETECTED))
		{
		  /* Implements OBCP-24516 */
		  PCOM_RX_storeMemorized(signalData,&signalData->data->rawValue);
		}
	    }
	}

      limitedVal = (pcom_gen_t) signalData->data->rawValue;
      inRange = PCOM_RX_signal_inRange(signalData,limitedVal);

      if(PCOM_SIGNAL_RANGE_OVER == inRange)
	{
	  /* Implements OBCP-24465 */
	  PCOM_RX_storeRaw(signalData,&signalData->limits.upper);/* PRQA S 0311 # No risk detected on losing the const qualification */
	}
      else if(PCOM_SIGNAL_RANGE_UNDER == inRange)
	{
	  /* Implements OBCP-24466 */
	  PCOM_RX_storeRaw(signalData,&signalData->limits.lower);/* PRQA S 0311 # No risk detected on losing the const qualification */
	}
      else
	{
	  /* DO NOTHING - MISRA COMPLIANT */
	}
    }
}

static PCOM_signalRange_t PCOM_RX_signal_inRange(const PCOM_RX_Signals_Config_t *signalData, pcom_gen_t value)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    PCOM_signalRange_t retVal = PCOM_SIGNAL_RANGE_OK;


  if((signalData->limits.range == PCOM_CANSIGNAL_LIMITS_BOTH) || (signalData->limits.range == PCOM_CANSIGNAL_LIMITS_LOWER))
    {

      if(signalData->dataType >= PCOM_SIGNAL_TYPE_S8)
	{
	  if((pcom_limits_type_lower_t) value < (pcom_limits_type_lower_t) signalData->limits.lower)
	    {
	      retVal = PCOM_SIGNAL_RANGE_UNDER;
	    }
	}
      else if((pcom_limits_type_higher_t) value < (pcom_limits_type_higher_t) signalData->limits.lower)
	{
	  retVal =  PCOM_SIGNAL_RANGE_UNDER;
	}
      else
	{
	  /* DO NOTHING - MISRA COMPLIANCE */
	}
    }

  if(((signalData->limits.range == PCOM_CANSIGNAL_LIMITS_BOTH) ||(signalData->limits.range == PCOM_CANSIGNAL_LIMITS_UPPER)) && (PCOM_SIGNAL_RANGE_OK == retVal))
    {
      if(signalData->dataType >= PCOM_SIGNAL_TYPE_S8)
	{
	  if((pcom_limits_type_lower_t) value > (pcom_limits_type_lower_t)signalData->limits.upper)
	    {
	      retVal =  PCOM_SIGNAL_RANGE_OVER;
	    }
	}
      else if((pcom_limits_type_higher_t) value > (pcom_limits_type_higher_t) signalData->limits.upper)
	{
	  retVal =  PCOM_SIGNAL_RANGE_OVER;
	}
      else
	{
	  /* DO NOTHING - MISRA COMPLIANCE */
	}
    }
  return retVal;
}

static void  PCOM_RX_task_processSignals_Diagnostics(const PCOM_RX_Frame_Config_t * frameConfig, PCOM_RX_Frame_Data_t * frameData, const PCOM_RX_Signals_Config_t *signalData)
{
  if(signalData->porpouse == PCOM_SIGNAL_NORMAL)
    {
      PCOM_RX_task_processSignals_Diagnostics_Invalid(frameConfig,signalData);
      PCOM_RX_task_processSignals_Diagnostics_Forbidden(frameConfig, signalData);
    }
  if(TRUE == frameConfig->hasLID)
    {
      PCOM_RX_task_Frame_Diagnostics_LID(frameData,signalData);
    }
  PCOM_RX_task_processSignals_Diagnostics_Reaction(frameConfig, frameData,signalData);
}

static void  PCOM_RX_task_processSignals_Diagnostics_Invalid(const PCOM_RX_Frame_Config_t * frameConfig, const PCOM_RX_Signals_Config_t *signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
  pcom_gen_t  physicalVal = 0U;
  IdtSignalDiagTime framesLostConfirm;
  IdtSignalDiagTime framesLostHealed;

  uint32 timeConfirm;
  uint32 timeHeal;

  signalData->get.physical(&physicalVal);/* PRQA S 0315 # Cast needed to be used with any posible data type and to be general enough for all signals */

  if((*signalData->diag.invalid.enable == PCOM_TX_DIAGNOSTIC_RUNNING) && (TRUE == signalData->get.invalid.isAvailable)
      && (NULL_PTR != signalData->diag.invalid.timeConfirm) && (NULL_PTR != signalData->diag.invalid.timeHealed))
    {

      framesLostConfirm = signalData->diag.invalid.timeConfirm();
      framesLostHealed = signalData->diag.invalid.timeHealed();


      if(FALSE != (boolean)(PCOM_RX_signal_isInvalid((signalData),(physicalVal)))) /* PRQA S 2995,4304 # QAC is not able to notice that the data may be modified in interrupt context. Native bool type not supported for this platform */
	{
	  /* Implements OBCP-24467 */
	  if(signalData->data->debounce.diagInvalid.prefault == PCOM_TX_DEBOUNCE_FAILING)
	    {
	      timeConfirm = ((uint32) framesLostConfirm
		  * (uint32) frameConfig->period)
						    					/ (uint32) PCOM_TASK_PERIOD;

	      if(signalData->data->debounce.diagInvalid.counterFail < timeConfirm)
		{
		  signalData->data->debounce.diagInvalid.counterFail ++;
		}
	    }
	  else
	    {
	      signalData->data->debounce.diagInvalid.prefault = PCOM_TX_DEBOUNCE_FAILING;
	      signalData->data->debounce.diagInvalid.counterFail = 0U;
	    }
	}
      else
	{
	  if(signalData->data->debounce.diagInvalid.prefault == PCOM_TX_DEBOUNCE_PASSING)
	    {
	      timeHeal = ((uint32) framesLostHealed
		  * (uint32) frameConfig->period)
						    					/ (uint32) PCOM_TASK_PERIOD;

	      if(signalData->data->debounce.diagInvalid.counterHealed < timeHeal)
		{
		  signalData->data->debounce.diagInvalid.counterHealed ++;
		}
	    }
	  else
	    {
	      signalData->data->debounce.diagInvalid.prefault = PCOM_TX_DEBOUNCE_PASSING;
	      signalData->data->debounce.diagInvalid.counterHealed = 0U;
	    }
	}

      if( (signalData->data->debounce.diagInvalid.prefault == PCOM_TX_DEBOUNCE_FAILING) && (signalData->data->diagInvalidFault == PCOM_TX_DIAGFAULT_NO_DETECTED))
	{
          timeConfirm = ((uint32) framesLostConfirm
    	  * (uint32) frameConfig->period)
    					    					/ (uint32) PCOM_TASK_PERIOD;

	  if(signalData->data->debounce.diagInvalid.counterFail == timeConfirm)
	    {
	      /* Implements OBCP-24524 */
	      signalData->data->diagInvalidFault = PCOM_TX_DIAGFAULT_DETECTED;

	    }
	}else if( (signalData->data->debounce.diagInvalid.prefault == PCOM_TX_DEBOUNCE_PASSING) && (signalData->data->diagInvalidFault == PCOM_TX_DIAGFAULT_DETECTED))
	  {
	      timeHeal = ((uint32) framesLostHealed
		  * (uint32) frameConfig->period)
						    					/ (uint32) PCOM_TASK_PERIOD;

	    if(signalData->data->debounce.diagInvalid.counterHealed == timeHeal)
	      {
		/* Implements OBCP-24468 */
		signalData->data->diagInvalidFault = PCOM_TX_DIAGFAULT_NO_DETECTED;
	      }

	  }else{
	      /* DO NOTHING - MISRA COMPLIANCE */
	  }
    }
}

static void  PCOM_RX_task_processSignals_Diagnostics_Forbidden(const PCOM_RX_Frame_Config_t * frameConfig, const PCOM_RX_Signals_Config_t *signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
  pcom_gen_t  physicalVal = 0U;
  boolean	isForbidden;
  IdtSignalDiagTime framesLostConfirm;
  IdtSignalDiagTime framesLostHealed;

  uint32 timeConfirm;
  uint32 timeHeal;


  signalData->get.physical(&physicalVal);/* PRQA S 0313,0316,0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
  if((*signalData->diag.forbidden.enable == PCOM_TX_DIAGNOSTIC_RUNNING)
      && (NULL_PTR != signalData->diag.forbidden.timeConfirm) && (NULL_PTR != signalData->diag.forbidden.timeHealed))
    {

      framesLostConfirm = signalData->diag.forbidden.timeConfirm();
      framesLostHealed = signalData->diag.forbidden.timeHealed();


      if(PCOM_FEATURE_NA != signalData->get.forbidden)
	{
	  isForbidden = signalData->get.forbidden(&physicalVal);
	}
      else
	{
	  isForbidden = FALSE;
	}
      if(TRUE == isForbidden)
	{
	  /* Implements OBCP-24470 */
	  if(signalData->data->debounce.diagForbidden.prefault == PCOM_TX_DEBOUNCE_FAILING)
	    {
	      timeConfirm = ((uint32) framesLostConfirm
		  * (uint32) frameConfig->period)
						    					/ (uint32) PCOM_TASK_PERIOD;

	      if(signalData->data->debounce.diagForbidden.counterFail < timeConfirm)
		{
		  signalData->data->debounce.diagForbidden.counterFail ++;
		}
	    }
	  else
	    {
	      signalData->data->debounce.diagForbidden.prefault = PCOM_TX_DEBOUNCE_FAILING;
	      signalData->data->debounce.diagForbidden.counterFail = 0U;
	    }
	}
      else
	{
	  if(signalData->data->debounce.diagForbidden.prefault == PCOM_TX_DEBOUNCE_PASSING)
	    {
	      timeHeal = ((uint32) framesLostHealed
		  * (uint32) frameConfig->period)
						    					/ (uint32) PCOM_TASK_PERIOD;

	      if(signalData->data->debounce.diagForbidden.counterHealed < timeHeal)
		{
		  signalData->data->debounce.diagForbidden.counterHealed ++;
		}
	    }
	  else
	    {
	      signalData->data->debounce.diagForbidden.prefault = PCOM_TX_DEBOUNCE_PASSING;
	      signalData->data->debounce.diagForbidden.counterHealed = 0U;
	    }
	}

      /* Evaluate fails */

      if( (signalData->data->debounce.diagForbidden.prefault == PCOM_TX_DEBOUNCE_FAILING) && (signalData->data->diagForbidenFault == PCOM_TX_DIAGFAULT_NO_DETECTED))
	{
          timeConfirm = ((uint32) framesLostConfirm
    	  * (uint32) frameConfig->period)
    					    					/ (uint32) PCOM_TASK_PERIOD;

	  if(signalData->data->debounce.diagForbidden.counterFail == timeConfirm)
	    {
	      /* Implements 24523 */
	      signalData->data->diagForbidenFault = PCOM_TX_DIAGFAULT_DETECTED;

	    }
	}else if( (signalData->data->debounce.diagForbidden.prefault == PCOM_TX_DEBOUNCE_PASSING) && (signalData->data->diagForbidenFault == PCOM_TX_DIAGFAULT_DETECTED))
	  {
	      timeHeal = ((uint32) framesLostHealed
		  * (uint32) frameConfig->period)
						    					/ (uint32) PCOM_TASK_PERIOD;

	    if(signalData->data->debounce.diagForbidden.counterHealed == timeHeal)
	      {
		/* Implements 24471 */
		signalData->data->diagForbidenFault = PCOM_TX_DIAGFAULT_NO_DETECTED;
	      }
	  }else{
	      /* DO NOTHING - MISRA COMPLIANCE */
	  }
    }
}

static void  PCOM_RX_task_Frame_Diagnostics_LID(PCOM_RX_Frame_Data_t * frameData,const PCOM_RX_Signals_Config_t *signalData)
{
  /* Implements OBCP-24518 */
  if (NULL_PTR != frameData) {
      if(signalData->LIDapplicable == PCOM_SIGNAL_APPLICABLE)
	{
	  if(signalData->data->debounce.diagForbidden.prefault == PCOM_TX_DEBOUNCE_FAILING)
	    {
	      frameData->LID.forbiddenPrefault = PCOM_TX_DEBOUNCE_FAILING;
	    }
	  if(signalData->data->diagForbidenFault== PCOM_TX_DIAGFAULT_DETECTED)
	    {
	      frameData->LID.forbidenFault = PCOM_TX_DIAGFAULT_DETECTED;
	    }
	}
  }
}

static void  PCOM_RX_task_processSignals_Diagnostics_Reaction(const PCOM_RX_Frame_Config_t * frameConfig, PCOM_RX_Frame_Data_t * frameData,  const PCOM_RX_Signals_Config_t *signalData) /* PRQA S 3673 # Input parameter not defined as const even if it shall not be changed. */
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    ComM_ModeType busState;


  (void)Rte_Call_PpComMUserNeed_ECANUserRequest_GetCurrentComMode(&busState);

  if((signalData->data->diagInvalidFault == PCOM_TX_DIAGFAULT_DETECTED) ||
      (signalData->data->diagForbidenFault == PCOM_TX_DIAGFAULT_DETECTED) ||
      ((TRUE == frameConfig->hasLID) && (frameData->LostFrameFault == TRUE)) ||
      (frameData->LID.forbidenFault == PCOM_TX_DIAGFAULT_DETECTED) ||
      (frameData->ChkSumFault == TRUE) ||
      (frameData->RCFault == TRUE))
    {
      /* Implements OBCP-24477 */
      (void)PCOM_RX_setOutput(signalData,PCOM_TX_ORIG_SUBSTITUTION);
    }
  else if((signalData->data->debounce.diagInvalid.prefault == PCOM_TX_DEBOUNCE_FAILING) ||
      (signalData->data->debounce.diagForbidden.prefault == PCOM_TX_DEBOUNCE_FAILING) ||
      (frameData->LostFramePreFault == TRUE) ||
      (frameData->ChkSumPreFault == TRUE) ||
      (frameData->RCPreFault == TRUE))
    {
      /* Implements OBCP-24479 */
      (void)PCOM_RX_setOutput(signalData,PCOM_TX_ORIG_MEMORIZED);
    }
  else
    {
      /* Implements OBCP-24480 */
      (void)PCOM_RX_setOutput(signalData,PCOM_TX_ORIG_RAW);
    }
}

static void PCOM_RX_initializeFrames(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint16 framesIndex;


  for (framesIndex = 0U; framesIndex < PCOM_NUM_RX_FRAMES;
      framesIndex++) {
      PCOM_RX_initializeFrameSignals(&PCOM_RX_Frame_Config[framesIndex]);
  }
}

static void PCOM_RX_initializeFrameSignals(const PCOM_RX_Frame_Config_t * frameData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint16 signalsIndex;


  if (NULL_PTR != frameData) {
      if(PCOM_EVENT != frameData->mode)
	{
	  for (signalsIndex = 0U; signalsIndex < frameData->signals.signalsCount;
	      signalsIndex++) {
	      PCOM_RX_setInitialSignal(&frameData->signals.signalsData[signalsIndex]);
	  }
	}
  }
}

static void PCOM_RX_setInitialSignal(const PCOM_RX_Signals_Config_t * signalData)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    pcom_gen_t initalVal = 0U;


  if (NULL_PTR != signalData) {
      /* Implements OBCP-24459 */
      if(PCOM_FEATURE_NA !=signalData->get.initial)
	{
	  signalData->get.initial(&initalVal);/* PRQA S 0313,0316,0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
	}
      else
	{
	  /* IF N/A */
	  initalVal = PCOM_DEFAULT_INITIAL_VALUE;
	}
      PCOM_RX_storeRaw(signalData,&initalVal);
      PCOM_RX_storeMemorized(signalData,&initalVal);
      PCOM_RX_setAvailabilityFlag(signalData,PCOM_SIGNAL_NOTAVAILABLE);
      /* Initialize counters and vars */
      PCOM_RX_setDiagInvalidFault(signalData,PCOM_TX_DIAGFAULT_NO_DETECTED);
      PCOM_RX_setDiagForbidenFault(signalData,PCOM_TX_DIAGFAULT_NO_DETECTED);
      signalData->data->debounce.diagForbidden.prefault = PCOM_TX_DEBOUNCE_UNINIT;
      signalData->data->debounce.diagInvalid.prefault = PCOM_TX_DEBOUNCE_UNINIT;
  }
}

/** \} end of PCOM_RX_Signal_actions */


static uint8 PCOM_TX_Frame_identifier(PduIdType PCOM_id_input)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint8 PCOM_index_ret = PCOM_SNA_U8;
    uint8 PCOM_TX_index;


  for (PCOM_TX_index = 0U; PCOM_TX_index < (uint8) PCOM_NUM_TX_FRAMES;
      PCOM_TX_index++) {
      if (PCOM_TX_Frame_Config[PCOM_TX_index].CAN_Id == PCOM_id_input) {
	  PCOM_index_ret = PCOM_TX_index;
	  break;
      }
  }
  return PCOM_index_ret;
}
static uint8 PCOM_RX_Frame_identifier(PduIdType PCOM_id_input)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint8 PCOM_index_ret = PCOM_SNA_U8;
    uint8 PCOM_RX_index;


  for (PCOM_RX_index = 0U; PCOM_RX_index < (uint8) PCOM_NUM_RX_FRAMES;
      PCOM_RX_index++) {
      if (PCOM_RX_Frame_Config[PCOM_RX_index].CAN_Id == PCOM_id_input) {
	  PCOM_index_ret = PCOM_RX_index;
	  break;
      }
  }

  return PCOM_index_ret;
}
static void PCOM_TX_Frame_WriteNibble(uint8 *frame_data, uint8 nibble_pos,
				      uint8 nibble_value) {

  if (((2U * PCOM_FRAME_LENGTH ) > nibble_pos)
      && (PCOM_NIBBLE_MASK >= nibble_value) && (NULL_PTR != frame_data)) {
      uint8 nibble_byte;
      /*Position OK, Nibble value OK.*/
      nibble_byte = nibble_pos >> 1U;
      if (0U == (nibble_pos % 2U)) {
	  /*even nibble*/
	  /*Clean*/
	  frame_data[nibble_byte] &= ~PCOM_NIBBLE_MASK;
	  /*Set*/
	  frame_data[nibble_byte] |= nibble_value;
      } else {
	  /*odd nibble*/
	  /*Clean*/
	  frame_data[nibble_byte] &= PCOM_NIBBLE_MASK;
	  /*Set*/
	  frame_data[nibble_byte] |= nibble_value << 4U;
      }

  }
}


/*RX indication sub-functions*/
static void PCOM_RX_indication_FrameLost(PCOM_RX_Frame_Data_t * PCOM_RX_frameData,
					 const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig) {
  if (NULL_PTR != PCOM_RX_frameConfig.DiagLostFrameEnableFunc)
  {
      /*Frame has FrameLost diagnosis.*/
      if (TRUE == (boolean)PCOM_RX_frameConfig.DiagLostFrameEnableFunc())
      {
    	  /*FrameLost diagnosis is enable*/
    	  /*Restart lastDetection counter.*/
    	  PCOM_RX_frameData->LostFrame_receiveFlag = TRUE;
      }
  }
}
static void PCOM_RX_indication_RC(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				  const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				  const volatile uint8 *frame_data) {

/* PRQA S 0404 ++ # Use of volatile data in this way mandatory for communitaciont between task and interrupt context */
	/*Read RC, *frame has been checked before.*/

	/*Nibble position OK. No overflow*/
	volatile uint8 rolling_counter;
	uint8 rc_pos;

	/*Avoid overflow*/
	rc_pos = (PCOM_RX_frameConfig.NibbleRC>>1U)%PCOM_FRAME_LENGTH;

	rolling_counter = frame_data[rc_pos]>>((PCOM_RX_frameConfig.NibbleRC%2U)<<2U);
	rolling_counter &= PCOM_NIBBLE_MASK;

	PCOM_RX_frameData->NewRCValue = rolling_counter;
	PCOM_RX_frameData->RC_receiveFlag = TRUE;
	PCOM_RX_frameData->DebugRCbuffer <<= 4;
	PCOM_RX_frameData->DebugRCbuffer |= rolling_counter;
/* PRQA S 0404 -- */
}
static void PCOM_RX_indication_ChkSum(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				      const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				      const volatile uint8 *frame_data) {
  if ((TRUE == PCOM_RX_frameConfig.HasChkSum)
      && (NULL_PTR != PCOM_RX_frameConfig.ChkSumEnableFunc)) {
      /*Frame has Check sum diagnosis.*/
      if (TRUE == (boolean)PCOM_RX_frameConfig.ChkSumEnableFunc()) {
	  /*Check sum diagnosis is enable*/
	  uint8 frame_index;
	  /*Calculate check sum.*/
	  uint8 CheckSum_result = PCOM_RX_frameConfig.ChkSumConst;

	  for (frame_index = 0U; frame_index < PCOM_FRAME_LENGTH ;
	      frame_index++) {
	      CheckSum_result += frame_data[frame_index] & PCOM_NIBBLE_MASK;
	      CheckSum_result += frame_data[frame_index] >> 4U;
	      CheckSum_result &= PCOM_NIBBLE_MASK;
	  }
	  if (PCOM_CRC_EXPECTED == CheckSum_result) {
	      PCOM_RX_frameData->ChkSumPreFault = FALSE;
	  } else {
	      PCOM_RX_frameData->ChkSumPreFault = TRUE;
	  }

      } else {
	  PCOM_RX_frameData->ChkSumPreFault = FALSE;
      }
  } else {
      PCOM_RX_frameData->ChkSumPreFault = FALSE;
  }
}
/*TX task sub-function*/
static void PCOM_TX_task_UpdateSignal(const
PCOM_TX_Frame_Config_t *frameConfig, PCOM_TX_Frame_Data_t *frameData) {

  if (PCOM_FEATURE_NA == frameConfig->signals.signalsData) {
      /*Function error, do nothing.*/
  } else if (PCOM_PERIODIC == frameConfig->mode) {
      /*Update signals*/
      if(frameData->ComposeTimeout == PCOM_TX_SEND_FRAME_LEVEL)
	{
	  PCOM_TX_processSignals(&frameConfig->signals);
	}
      else
	{
	  frameData->ComposeTimeout -= 1U;
	}
  } else if (NULL_PTR == frameConfig->EventTriggerFunction) {
      /*Function error, do nothing.*/
  } else if (PCOM_MIXED == frameConfig->mode) {
      /*check if an event trigger must be performed*/
      if (TRUE == frameConfig->EventTriggerFunction())
	{
	  /*Update signals as frame has being triggered*/
	  PCOM_TX_processSignals(&frameConfig->signals);
	  /*Frame should be sent*/
	  Com_TriggerIPDUSend(frameConfig->CAN_Id);
	}
      else
	{	/* Not triggered, check if sending time */
	  if(frameData->ComposeTimeout == PCOM_TX_SEND_FRAME_LEVEL)
	    {
	      PCOM_TX_processSignals(&frameConfig->signals);
	    }
	  else
	    {
	      frameData->ComposeTimeout -= 1U;
	    }
	}
  } else/*PCOM_EVENT*/
    {
      if (TRUE == frameConfig->EventTriggerFunction()) {
	  PCOM_TX_processSignals(&frameConfig->signals);
	  /*Frame should be sent*/
	  Com_TriggerIPDUSend(frameConfig->CAN_Id);
      }
    }
}

/*RX task sub-functions*/
static void PCOM_RX_task_FrameLost(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				   const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				   uint32 *PCOM_DebounceCounter) {

 boolean prev_FrameFault;
/* PRQA S 0404 ++ # Use of volatile data in this way mandatory for communitaciont between task and interrupt context */
  if (NULL_PTR != PCOM_RX_frameConfig.DiagLostFrameEnableFunc) {
      /*Frame has FrameLost diagnosis*/
      if (TRUE == (boolean)PCOM_RX_frameConfig.DiagLostFrameEnableFunc()) {

    uint32 confirm_time;
	  uint32 heal_time;
	  uint32 newLastTime;

	  /*Configuration units for LostFrameConfirm are "periods"*/
		confirm_time = ((uint32) (*PCOM_RX_frameData).LostFrameConfirm
				* (uint32) PCOM_RX_frameConfig.period)
																											/ (uint32) PCOM_TASK_PERIOD;
		heal_time = ((uint32) (*PCOM_RX_frameData).LostFrameHeal
				* (uint32) PCOM_RX_frameConfig.period)
																											/ (uint32) PCOM_TASK_PERIOD;

		if(FALSE!=PCOM_RX_frameData->LostFrame_receiveFlag)
		{
			/*Frame received*/
			PCOM_RX_frameData->LostFramePreFault = FALSE;
			newLastTime = 0U;
			/*Clean Flag*/
			PCOM_RX_frameData->LostFrame_receiveFlag = FALSE;
		}
		else if(PCOM_RX_frameConfig.LostFrameDetection>PCOM_RX_frameData->LastFrameTime)
		{
			PCOM_RX_frameData->LostFramePreFault = FALSE;
			newLastTime = (uint32)PCOM_RX_frameData->LastFrameTime + (uint32)PCOM_TASK_PERIOD;
		}
		else
		{
			PCOM_RX_frameData->LostFramePreFault = TRUE;
			newLastTime = (uint32)PCOM_RX_frameData->LastFrameTime + (uint32)PCOM_TASK_PERIOD;
		}

		/*Avoid overflow*/
		if(newLastTime>PCOM_RX_frameConfig.LostFrameDetection)
		{
			PCOM_RX_frameData->LastFrameTime = PCOM_RX_frameConfig.LostFrameDetection;
		}
		else
		{
			PCOM_RX_frameData->LastFrameTime = (uint16)newLastTime;
		}



	  prev_FrameFault = PCOM_RX_frameData->LostFrameFault;

	  /*Apply debounce*/
	  PCOM_RX_diagnosisDebounce(PCOM_RX_frameData->LostFramePreFault,
				    &(PCOM_RX_frameData->LostFrameFault), confirm_time,
				    heal_time, PCOM_DebounceCounter);
	  /* Save error in Debug signal */
	  if ((PCOM_RX_frameData->LostFrameFault != prev_FrameFault) && (PCOM_RX_frameData->LostFrameFault != FALSE))
	  {
		  PCOM_Debug[0]++;		/* Error counter */
		  PCOM_Debug[1] = 0x01; /* Error type: Frame lost */
		  PCOM_Debug[2] =  PCOM_RX_frameConfig.CAN_Id;

		  PCOM_IntCANDebug4[0]++;		/* Error counter */
		  PCOM_IntCANDebug4[1] = 0x01; /* Error type: Frame lost */
		  PCOM_IntCANDebug4[1] |=  (PCOM_RX_frameConfig.CAN_Id<<2);
	  }

      } else {
	  /*Force fault to False*/
	  PCOM_RX_frameData->LostFramePreFault = FALSE;
	  PCOM_RX_frameData->LostFrameFault = FALSE;
	  PCOM_RX_frameData->LastFrameTime = 0U;
      }

  } else {
      /*Force fault to False*/
      PCOM_RX_frameData->LostFrameFault = FALSE;
      PCOM_RX_frameData->LastFrameTime = 0U;
  }
/* PRQA S 0404 -- */
}
static void PCOM_RX_task_CheckSum(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
				  const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
				  uint32 *PCOM_DebounceCounter) {
/* PRQA S 0404 ++ # Use of volatile data in this way mandatory for communitaciont between task and interrupt context */

boolean prev_FrameFault;
  if ((TRUE == PCOM_RX_frameConfig.HasChkSum)
      && (NULL_PTR != PCOM_RX_frameConfig.ChkSumEnableFunc)) {
      /*Frame has check sum*/
      if (TRUE == (boolean)PCOM_RX_frameConfig.ChkSumEnableFunc()) {
	  /*Check sum enable*/
	  /*Debounce Check sum*/

	  uint32 confirm_time;
	  uint32 heal_time;
	  /*Configuration units are "periods"*/
	  confirm_time = ((uint32) (*PCOM_RX_frameData).ChkSumConfirm
	      * (uint32) PCOM_RX_frameConfig.period)
																									    / (uint32) PCOM_TASK_PERIOD;
	  heal_time = ((uint32) (*PCOM_RX_frameData).ChkSumHeal
	      * (uint32) PCOM_RX_frameConfig.period)
																									    / (uint32) PCOM_TASK_PERIOD;

	  prev_FrameFault = PCOM_RX_frameData->ChkSumFault;

	  PCOM_RX_diagnosisDebounce(PCOM_RX_frameData->ChkSumPreFault,
				    &(PCOM_RX_frameData->ChkSumFault), confirm_time, heal_time,
				    PCOM_DebounceCounter);

	  /* Save error in Debug signal */
	  if ((PCOM_RX_frameData->ChkSumFault != prev_FrameFault) && (PCOM_RX_frameData->ChkSumFault != FALSE))
	  {
		  PCOM_Debug[0]++;		/* Error counter */
		  PCOM_Debug[1] = 0x02; /* Error type: Checksum error */
		  PCOM_Debug[2] =  PCOM_RX_frameConfig.CAN_Id;

		  PCOM_IntCANDebug4[0]++;		/* Error counter */
		  PCOM_IntCANDebug4[1] = 0x02; /* Error type: Checksum error */
		  PCOM_IntCANDebug4[1] |=  (PCOM_RX_frameConfig.CAN_Id<<2);

	  }

      } else {
	  /*Force fault to False*/
	  PCOM_RX_frameData->ChkSumFault = FALSE;
      }
  } else {
      /*Force fault to False*/
      PCOM_RX_frameData->ChkSumFault = FALSE;
  }
/* PRQA S 0404 -- */
}
static void PCOM_RX_task_RC(PCOM_RX_Frame_Data_t *PCOM_RX_frameData,
			    const PCOM_RX_Frame_Config_t PCOM_RX_frameConfig,
			    uint32 *PCOM_DebounceCounter) {
/* PRQA S 0404 ++ # Use of volatile data in this way mandatory for communitaciont between task and interrupt context */

boolean prev_FrameFault;

  if ((TRUE == PCOM_RX_frameConfig.HasRC)
      && (NULL_PTR != PCOM_RX_frameConfig.RCEnableFunc)) {
      /*Frame has Rolling counter*/
      if (TRUE == (boolean)PCOM_RX_frameConfig.RCEnableFunc()) {
	  /*Rolling counter enable*/
	  /*Debounce Rolling counter*/
	  uint32 confirm_time;
	  uint32 heal_time;
	  /*Configuration units are "periods"*/
	  confirm_time = ((uint32) (*PCOM_RX_frameData).RCConfirm
	      * (uint32) PCOM_RX_frameConfig.period)
																									    / (uint32) PCOM_TASK_PERIOD;
	  heal_time = ((uint32) (*PCOM_RX_frameData).RCHeal
	      * (uint32) PCOM_RX_frameConfig.period)
																									    / (uint32) PCOM_TASK_PERIOD;


	  if(TRUE==PCOM_RX_frameData->RC_receiveFlag)
	  {
	  	uint8 expectedVal = (PCOM_RX_frameData->LastRCValue + 1U) & PCOM_NIBBLE_MASK;
	  	if(PCOM_RX_frameData->NewRCValue!=expectedVal)
	  	{
	  		PCOM_RX_frameData->RCPreFault = TRUE;
	  	}
	  	else
	  	{
	  		PCOM_RX_frameData->RCPreFault = FALSE;
	  	}
	  	PCOM_RX_frameData->RC_receiveFlag = FALSE;
	  	PCOM_RX_frameData->LastRCValue = PCOM_RX_frameData->NewRCValue;
	  }


	  prev_FrameFault = PCOM_RX_frameData->RCFault;

	  PCOM_RX_diagnosisDebounce(PCOM_RX_frameData->RCPreFault,
				    &(PCOM_RX_frameData->RCFault), confirm_time, heal_time,
				    PCOM_DebounceCounter);

	  /* Save error in Debug signal */
	  if ((PCOM_RX_frameData->RCFault != prev_FrameFault) && (PCOM_RX_frameData->RCFault != FALSE))
	  {
		  PCOM_Debug[0]++;		/* Error counter */
		  PCOM_Debug[1] = 0x03; /* Error type: RC error */
		  PCOM_Debug[2] =  PCOM_RX_frameConfig.CAN_Id;

		  PCOM_IntCANDebug4[0]++;		/* Error counter */
		  PCOM_IntCANDebug4[1] = 0x03; /* Error type: RC error */
		  PCOM_IntCANDebug4[1] |=  (PCOM_RX_frameConfig.CAN_Id<<2);

	  }


      } else {
	  /*Force fault to False*/
	  PCOM_RX_frameData->RCFault = FALSE;
      }
  } else {
      /*Force fault to False*/
      PCOM_RX_frameData->RCFault = FALSE;
  }
/* PRQA S 0404 -- */
}
static void PCOM_RX_task_SignalUpdate(const PCOM_RX_Frame_Config_t *PCOM_RX_frameConf,
				      PCOM_RX_Frame_Data_t *PCOM_RX_frameData){
  if ((NULL_PTR != PCOM_RX_frameConf) && (NULL_PTR != PCOM_RX_frameData)) {
	  /* Process all signals parsing and diagnostics */
	  PCOM_RX_processSignals(PCOM_RX_frameConf,PCOM_RX_frameData);
  }
}


static void PCOM_RX_task_GridDiagnosis(
    boolean (*PCOM_gridDiagnosis)[PCOM_CAN_NUMBER],
    PCOM_RX_Frame_Data_t PCOM_RX_frameData,
    PCOM_RX_Frame_Config_t PCOM_RX_frameConf) {
  if ((NULL_PTR != PCOM_gridDiagnosis)
      && (PCOM_CAN_NUMBER > PCOM_RX_frameConf.CAN_grid)) {
      if ((FALSE != PCOM_RX_frameData.ChkSumFault)
	  || (FALSE != PCOM_RX_frameData.RCFault)
	  || (FALSE != PCOM_RX_frameData.LostFrameFault)) {
	  (*PCOM_gridDiagnosis)[PCOM_RX_frameConf.CAN_grid] = TRUE;
      }
  }

}

/*External services*/
boolean PCOM_Frame_TX_Update_Chk_RCService(PduIdType frameID, uint8 *frame_data)
{

    /* [Enrique.Bueno] Workaround START: MPU violation: PCOM is ASIL SWC, but COM Callbacks are executed in QM partition. ==================
                                         Hence, static variables defined at PCOM and written into a COM Callback will rise MPU violations.
                                         Contingency actions: remap all static variables affected from ASIL to QM partition taking profit
                                                              of TBD SWC (qm)                                                                 */
    /* - Static non-init variables declaration ---------- */
    #define CtApTBD_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApTBD_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApTBD_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    #define CtApTBD_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApTBD_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApTBD_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApTBD_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
    /* [Enrique.Bueno] Workaround STOP ====================================================================================================== */


    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* [Enrique.Bueno] Workaround START: MPU violation: static variable moved from ASIL to QM partition */
    //static uint8 dummy =0;
    /* [Enrique.Bueno] Workaround STOP */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    uint8 PCOM_frame_index;
    uint8 PCOM_byte_index;
    uint8 PCOM_crc_result;
    boolean PCOM_retOK = FALSE;


  if (PCOM_INTEGRATION_MODE != PCOM_CANComRequest)
  {
	  /* Only allow transmission if not in Integration Mode */

	  PCOM_frame_index = PCOM_TX_Frame_identifier(frameID);

	  if (ComConf_ComIPdu_VERS_OBC_DCDC_oE_CAN_9ac9a132_Tx == frameID)
	  {
		  /* Event frame VERS_OBC_DCDC_0CE */
		  PCOM_Frame_0ce_SendEvent = FALSE;
	  }

	  if(ComConf_ComIPdu_NEW_JDD_OBC_DCDC_oE_CAN_902371c4_Tx == frameID)
	  {
		  /* Event frame New JDD */
		  PCOM_Frame_5b1_SendEvent = FALSE;
	  }

	  if ((PCOM_NUM_TX_FRAMES > PCOM_frame_index) && (NULL_PTR != frame_data)) {
		  if (TRUE == PCOM_TX_Frame_Config[PCOM_frame_index].enable)
		  {
			  PCOM_retOK = TRUE;
	      /* Reset sending timeout */
	      PCOM_TX_Frame_Data[PCOM_frame_index].ComposeTimeout = PCOM_TX_Frame_Config[PCOM_frame_index].ComposeOffset;

			  if ((TRUE == PCOM_TX_Frame_Config[PCOM_frame_index].HasRC)
				  && (NULL_PTR
				  != PCOM_TX_Frame_Config[PCOM_frame_index].SignalUpdatedFunction)) {
				  /*Update rolling counter*/
				  if (TRUE
				  == PCOM_TX_Frame_Config[PCOM_frame_index].SignalUpdatedFunction()) {
				  PCOM_TX_Frame_Data[PCOM_frame_index].CurrentRC++;
				  PCOM_TX_Frame_Data[PCOM_frame_index].CurrentRC &=
					  PCOM_RC_MODULE;
				  }
				  /*Place Rolling counter*/
				  PCOM_TX_Frame_WriteNibble(frame_data,
							PCOM_TX_Frame_Config[PCOM_frame_index].NibbleRC,
							PCOM_TX_Frame_Data[PCOM_frame_index].CurrentRC);
			  }

			  if (TRUE == PCOM_TX_Frame_Config[PCOM_frame_index].HasChkSum) {
				  /*Calculate CRC.*/

				  /*Clean nibble*/
				  PCOM_TX_Frame_WriteNibble(frame_data,
							PCOM_TX_Frame_Config[PCOM_frame_index].NibbleChkSum,
							0U);

				  /*initialize CRC.*/
				  PCOM_crc_result =
				  PCOM_TX_Frame_Config[PCOM_frame_index].ChkSumConst;
				  for (PCOM_byte_index = 0U; PCOM_byte_index < PCOM_FRAME_LENGTH ;
				  PCOM_byte_index++) {
				  PCOM_crc_result += (frame_data[PCOM_byte_index]
								 & PCOM_NIBBLE_MASK ); 	//LSB
				  PCOM_crc_result += frame_data[PCOM_byte_index] >> 4;   //MSB
				  PCOM_crc_result &= PCOM_NIBBLE_MASK; //overflow under control.
				  }
				  PCOM_crc_result = PCOM_CRC_EXPECTED - PCOM_crc_result;

				  /*Write result*/
				  PCOM_TX_Frame_WriteNibble(frame_data,
							PCOM_TX_Frame_Config[PCOM_frame_index].NibbleChkSum,
							PCOM_crc_result);
			  }
		  }
	  }
  }
  return PCOM_retOK;

}
void PCOM_Frame_RX_IndicationService(volatile PduIdType frameID, const volatile uint8 *frame_data)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    volatile uint8 PCOM_frame_index;

/* PRQA S 0404 ++ # Use of volatile data in this way mandatory for communitaciont between task and interrupt context */
  PCOM_frame_index = PCOM_RX_Frame_identifier(frameID);

  if ((PCOM_NUM_RX_FRAMES > PCOM_frame_index) && (NULL_PTR != frame_data))
  {
      if (TRUE == PCOM_RX_Frame_Config[PCOM_frame_index].enable)
      {
      	  PCOM_RX_Frame_Data[PCOM_frame_index].receiveFlag = TRUE;

    	  PCOM_RX_indication_FrameLost(
    			  &PCOM_RX_Frame_Data[PCOM_frame_index],
				  PCOM_RX_Frame_Config[PCOM_frame_index]);
    	  PCOM_RX_indication_RC(&PCOM_RX_Frame_Data[PCOM_frame_index],
    			  PCOM_RX_Frame_Config[PCOM_frame_index], frame_data);
    	  PCOM_RX_indication_ChkSum(&PCOM_RX_Frame_Data[PCOM_frame_index],
				    PCOM_RX_Frame_Config[PCOM_frame_index], frame_data);

      }
  }

  if ((ComConf_ComIPdu_ELECTRON_BSI_oE_CAN_ab0b72ff_Rx == frameID) && (NULL_PTR != frame_data))
  {
	  /* Received frame for Integration mode control */

	  PCOM_Integ_Mode.data = frame_data[0];
	  PCOM_Integ_Mode.fresh_frame_rcvd = TRUE;
  }

  if (ComConf_ComIPdu_NEW_JDD_oE_CAN_60b2710c_Rx == frameID)
  {
	  PCOM_Frame_NEW_JDD_55f_Received = TRUE;
  }

  if (ComConf_ComIPdu_VCU_oE_CAN_8b566170_Rx == frameID)
  {
	  PCOM_Frame_VCU_552_Received = TRUE;
  }
/* PRQA S 0404 -- */
}

static void PCOM_UpdateCalibratables(void) {
  /*Get NVM param for all RX frames*/
  PCOM_RX_Frame_Data[0U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_0F0_LostFrameConfirm();
  PCOM_RX_Frame_Data[0U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_0F0_LostFrameHeal();
  PCOM_RX_Frame_Data[1U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_125_LostFrameConfirm();
  PCOM_RX_Frame_Data[1U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_125_LostFrameHeal();
  PCOM_RX_Frame_Data[2U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_127_LostFrameConfirm();
  PCOM_RX_Frame_Data[2U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_127_LostFrameHeal();
  PCOM_RX_Frame_Data[3U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_129_LostFrameConfirm();
  PCOM_RX_Frame_Data[3U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_129_LostFrameHeal();
  PCOM_RX_Frame_Data[4U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_17B_LostFrameConfirm();
  PCOM_RX_Frame_Data[4U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_17B_LostFrameHeal();
  PCOM_RX_Frame_Data[4U].ChkSumConfirm =
      (uint8) Rte_CData_CalFrame_17B_ChkSumConfirm();
  PCOM_RX_Frame_Data[4U].ChkSumHeal =
      (uint8) Rte_CData_CalFrame_17B_ChkSumHeal();
  PCOM_RX_Frame_Data[4U].RCConfirm =
      (uint8) Rte_CData_CalFrame_17B_RCConfirm();
  PCOM_RX_Frame_Data[4U].RCHeal = (uint8) Rte_CData_CalFrame_17B_RCHeal();
  PCOM_RX_Frame_Data[5U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_27A_LostFrameConfirm();
  PCOM_RX_Frame_Data[5U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_27A_LostFrameHeal();
  PCOM_RX_Frame_Data[6U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_359_LostFrameConfirm();
  PCOM_RX_Frame_Data[6U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_359_LostFrameHeal();
  PCOM_RX_Frame_Data[7U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_361_LostFrameConfirm();
  PCOM_RX_Frame_Data[7U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_361_LostFrameHeal();
  PCOM_RX_Frame_Data[8U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_372_LostFrameConfirm();
  PCOM_RX_Frame_Data[8U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_372_LostFrameHeal();
  PCOM_RX_Frame_Data[9U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_37E_LostFrameConfirm();
  PCOM_RX_Frame_Data[9U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_37E_LostFrameHeal();
  PCOM_RX_Frame_Data[10U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_382_LostFrameConfirm();
  PCOM_RX_Frame_Data[10U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_382_LostFrameHeal();
  PCOM_RX_Frame_Data[11U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_31B_LostFrameConfirm();
  PCOM_RX_Frame_Data[11U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_31B_LostFrameHeal();
  PCOM_RX_Frame_Data[12U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_31E_LostFrameConfirm();
  PCOM_RX_Frame_Data[12U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_31E_LostFrameHeal();
  PCOM_RX_Frame_Data[13U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_486_LostFrameConfirm();
  PCOM_RX_Frame_Data[13U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_486_LostFrameHeal();
  PCOM_RX_Frame_Data[14U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_552_LostFrameConfirm();
  PCOM_RX_Frame_Data[14U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_552_LostFrameHeal();
  PCOM_RX_Frame_Data[14U].LostFrameConfirm =
      (uint8) Rte_CData_CalFrame_552_LostFrameConfirm();
  PCOM_RX_Frame_Data[14U].LostFrameHeal =
      (uint8) Rte_CData_CalFrame_552_LostFrameHeal();


}

static void PCOM_Publish_DTC_EnableConditions(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    static uint16 PCOM_NetState_Counter = 0U;
    static uint16 PCOM_DiagMuxOnPwt_Counter = 0U;

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	VCU_DiagMuxOnPwt DiagMuxOnPwt_data;
	ComM_ModeType busState;
	IdtBatteryVoltageState VoltageState_data;
	boolean Net_State;

	(void)Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsBSIInfo((boolean)PCOM_DiagEnable_BSI);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsE_VCU((boolean)PCOM_DiagEnable_E_VCU);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Write_PpComsMonitoringConditions_DeComsMonitoringConditionsTBMU((boolean)PCOM_DiagEnable_TBMU);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* Mute fault conditions evaluation */
	(void)Rte_Read_PpVCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&DiagMuxOnPwt_data);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/ /* PRQA S 0313,0315 # Cast needed to be used with any posible data type and to be general enough for all signals */
	PCOM_DiagMux_CAN_typeAUT = DiagMuxOnPwt_data;

	/* LostFrame of VCU_BSI_Wakeup */
	if (FALSE == PCOM_RX_Frame_Data[5].LostFrameFault)
	{
		/* Convert time to avoid diagnostics from units 100ms per bit to 5ms op cycles */
		PCOM_DiagMuxOnPwt_Counter = (uint16)Rte_CData_CalTo_DgMux_Prod() * 20U;
	}
	else if (PCOM_DiagMuxOnPwt_Counter != 0U)
	{
		PCOM_DiagMuxOnPwt_Counter--;
	}
	else
	{
		PCOM_DiagMux_CAN_typeAUT = PCOM_DIAGMUX_CAN_TYPEAUT_OFF;
	}

	(void)Rte_Call_PpComMUserNeed_ECANUserRequest_GetCurrentComMode(&busState);

	if (COMM_FULL_COMMUNICATION != busState)
	{
		Net_State = FALSE;
		/* Convert time to avoid diagnostics from units 100ms per bit to 5ms op cycles */
		PCOM_NetState_Counter = (uint16)Rte_CData_CalT_INHIB_DIAG_COM() * 20U;
	}
	else if (PCOM_NetState_Counter != 0U)
	{
		Net_State = FALSE;
		PCOM_NetState_Counter--;
	}
	else
	{
		Net_State = TRUE;
	}

	(void)Rte_Read_PpBatteryVoltageState_DeBatteryVoltageState(&VoltageState_data);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	/* DiagCAN_typeAUT_ON = Net_State = Net_On and Batt_State = NormalVoltage and No  ConfirmedBusOFF  and DiagMux_CAN_typeAUT = DiagMux_CAN_typeAUT_ON */
	if ((PCOM_DIAGMUX_CAN_TYPEAUT_OFF != PCOM_DiagMux_CAN_typeAUT) && (FALSE != Net_State) && (BAT_VALID_RANGE == VoltageState_data) && (FALSE == PCOM_BusOffFault))
	{
		PCOM_DiagCAN_typeAUT = TRUE;
	}
	else
	{
		PCOM_DiagCAN_typeAUT = FALSE;
	}

	(void)Rte_Write_PpBusMUTEEnableConditions_DeBusMUTEEnableConditions(PCOM_DiagCAN_typeAUT);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void PCOM_SendRX_DTC_Triggers(void)
{

  (void)Rte_Write_PpDiagChecksumFaults_DeVCU_FrameChksum17B_ChkFault(PCOM_RX_Frame_Data[4].ChkSumFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagRollingCounterFaults_DeVCU_RollingCounter17B_RCFault(PCOM_RX_Frame_Data[4].RCFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS1( PCOM_RX_Frame_Data[1].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS3( PCOM_RX_Frame_Data[2].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS5( PCOM_RX_Frame_Data[6].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS6( PCOM_RX_Frame_Data[7].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS8( PCOM_RX_Frame_Data[11].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BMS9( PCOM_RX_Frame_Data[3].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_BSIInfo( PCOM_RX_Frame_Data[10].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_CtrlDCDC( PCOM_RX_Frame_Data[8].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_ParkCommand( PCOM_RX_Frame_Data[12].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU2( PCOM_RX_Frame_Data[0].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU3( PCOM_RX_Frame_Data[13].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_552( PCOM_RX_Frame_Data[14].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_BSI_Wakeup( PCOM_RX_Frame_Data[5].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_PCANInfo( PCOM_RX_Frame_Data[4].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagLostFrameFault_DeDiagLostFrameFault_VCU_TU( PCOM_RX_Frame_Data[9].LostFrameFault );/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_AuxBattVolt_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[2].signals.signalsData[0].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_DC_RELAY_VOLTAGE_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[3].signals.signalsData[0].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeCurrentAllow_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[7].signals.signalsData[1].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_HighestChargeVoltageAllow_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[7].signals.signalsData[0].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_SOC_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[1].signals.signalsData[0].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBMS_Voltage_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[1].signals.signalsData[1].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeBSI_ChargeTypeStatus_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[10].signals.signalsData[2].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeDDE_GMV_SEEM_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[6].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeDMD_MEAP_2_SEEM_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[7].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_GMP_HYB_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[5].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_PRINCIP_SEV_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[3].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeETAT_RESEAU_ELEC_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[4].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeMODE_EPS_REQUEST_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[9].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_AccPedalPosition_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[9].signals.signalsData[0].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_DCDCVoltageReq_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[8].signals.signalsData[0].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalForbidenValueFault_DeVCU_HEAT_PUMP_WORKING_MODE_ForbidenValueFault((boolean)PCOM_RX_Frame_Config[10].signals.signalsData[6].data->diagForbidenFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeABS_VehSpd_InvalidValueFault((boolean)PCOM_RX_Frame_Config[4].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_AuxBattVolt_InvalidValueFault((boolean)PCOM_RX_Frame_Config[2].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_MainConnectorState_InvalidValueFault((boolean)PCOM_RX_Frame_Config[1].signals.signalsData[3].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_SOC_InvalidValueFault((boolean)PCOM_RX_Frame_Config[1].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeBMS_Voltage_InvalidValueFault((boolean)PCOM_RX_Frame_Config[1].signals.signalsData[1].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeCOMPTEUR_RAZ_GCT_InvalidValueFault((boolean)PCOM_RX_Frame_Config[14].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeCPT_TEMPOREL_InvalidValueFault((boolean)PCOM_RX_Frame_Config[14].signals.signalsData[1].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeDDE_GMV_SEEM_InvalidValueFault((boolean)PCOM_RX_Frame_Config[5].signals.signalsData[6].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeKILOMETRAGE_InvalidValueFault((boolean)PCOM_RX_Frame_Config[14].signals.signalsData[2].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_AccPedalPosition_InvalidValueFault((boolean)PCOM_RX_Frame_Config[9].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCActivation_InvalidValueFault((boolean)PCOM_RX_Frame_Config[8].signals.signalsData[2].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_DCDCVoltageReq_InvalidValueFault((boolean)PCOM_RX_Frame_Config[8].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  (void)Rte_Write_PpDiagSignalInvalidValueFault_DeVCU_EPWT_Status_InvalidValueFault((boolean)PCOM_RX_Frame_Config[12].signals.signalsData[0].data->diagInvalidFault);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

}



static void PCOM_Generate_DGN_conditions(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    static uint32 PCOM_WupMainCounter[3] = {0,0,0};

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
	uint16 time_avoid_diag[3];
	boolean ECU_WupMain;
	uint8 index;

	boolean COOLING_WAKEUP_STATE_CANSignal;
	boolean STOP_DELAYED_HMI_WUP_STATE_CANSignal;
	boolean HV_BATT_CHARGE_WUP_STATE_CANSignal;
	boolean PI_STATE_INFO_WUP_STATE_CANSignal;
	boolean PRE_DRIVE_WUP_STATE_CANSignal;
	boolean POST_DRIVE_WUP_STATE_CANSignal;
	boolean HOLD_DISCONTACTOR_WUP_STATE_CANSignal;
	boolean PRECOND_ELEC_WUP_STATE_CANSignal;


	/* Convert time to avoid diagnostics from units 100ms per bit to 5ms op cycles */
	time_avoid_diag[0] = (uint16)Rte_CData_CalTimeBeforeRunningDiagEVCU() * 20U;
	time_avoid_diag[1] = (uint16)Rte_CData_CalTimeBeforeRunningDiagBSIInfo() * 20U;
	time_avoid_diag[2] = (uint16)Rte_CData_CalTimeBeforeRunningDiagTBMU() * 20U;

	(void)Rte_Read_PpECU_WakeupMain_DeECU_WakeupMain(&ECU_WupMain);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	for (index = 0U; index < 3U; index ++)
	{
		/* Debounce the ECU_WakeupMain signal */
		/* Added condition to add the same inhibit time after exiting electronic integration mode */
		if ((ECU_WupMain == FALSE) || (PCOM_FULLCOMM_MODE != PCOM_CANComRequest))
		{
			PCOM_WupMainCounter[index] = 0;
		}
		else
		{
			if (PCOM_WupMainCounter[index] < time_avoid_diag[index])
			{
				/* Debouncing */
				PCOM_WupMainCounter[index]++;
			}
		}
	}


	/* Set conditions for E_VCU and TBMU frames */
	if (PCOM_WupMainCounter[0] >= time_avoid_diag[0])
	{
		PCOM_DiagEnable_E_VCU = PCOM_TX_DIAGNOSTIC_RUNNING;
	}
	else
	{
		PCOM_DiagEnable_E_VCU = PCOM_TX_DIAGNOSTIC_FORBIDDEN;
	}

	if (PCOM_WupMainCounter[2] >= time_avoid_diag[2])
	{
		PCOM_DiagEnable_TBMU = PCOM_TX_DIAGNOSTIC_RUNNING;
	}
	else
	{
		PCOM_DiagEnable_TBMU = PCOM_TX_DIAGNOSTIC_FORBIDDEN;
	}


	/* Read additional signals for BSI */
	(void)Rte_Read_PpInt_SUPV_CoolingWupState_SUPV_CoolingWupState(&COOLING_WAKEUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_StopDelayedHMIWupState_SUPV_StopDelayedHMIWupState(&STOP_DELAYED_HMI_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_HVBattChargeWupState_SUPV_HVBattChargeWupState(&HV_BATT_CHARGE_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_PIStateInfoWupState_SUPV_PIStateInfoWupState(&PI_STATE_INFO_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_PreDriveWupState_SUPV_PreDriveWupState(&PRE_DRIVE_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_PostDriveWupState_SUPV_PostDriveWupState(&POST_DRIVE_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_HoldDiscontactorWupState_SUPV_HoldDiscontactorWupState(&HOLD_DISCONTACTOR_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_SUPV_PrecondElecWupState_SUPV_PrecondElecWupState(&PRECOND_ELEC_WUP_STATE_CANSignal);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	/* Evaluate the condition for activate the diagnostis over the BSIInfo frame */
	if 		((PCOM_WupMainCounter[1] >= time_avoid_diag[1]) ||
			(0x01U == COOLING_WAKEUP_STATE_CANSignal) ||
			(0x01U == STOP_DELAYED_HMI_WUP_STATE_CANSignal) ||
			(0x01U == HV_BATT_CHARGE_WUP_STATE_CANSignal) ||
			(0x01U == PI_STATE_INFO_WUP_STATE_CANSignal) ||
			(0x01U == PRE_DRIVE_WUP_STATE_CANSignal) ||
			(0x01U == POST_DRIVE_WUP_STATE_CANSignal) ||
			(0x01U == HOLD_DISCONTACTOR_WUP_STATE_CANSignal) ||
			(0x01U == PRECOND_ELEC_WUP_STATE_CANSignal))
	{
		PCOM_DiagEnable_BSI = PCOM_TX_DIAGNOSTIC_RUNNING;
	}
	else
	{
		PCOM_DiagEnable_BSI = PCOM_TX_DIAGNOSTIC_FORBIDDEN;
	}

}


static void PCOM_Serialize_NVM(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Rte_DT_IdtPCOMNvMArray_0 * pData = Rte_Pim_NvPCOMBlockNeed_MirrorBlock();
    boolean shutdownAuth;
    uint8 resetRequest;

/* PRQA S 2985 ++ # These operation might be performed without the explicit masking, but it shall be kept in this way for clarification purposes */
	pData[0] = (uint8)((PCOM_BMS_SOC_Initial >> 8) & 0x00FFU);
	pData[1] = (uint8)((PCOM_BMS_SOC_Initial >> 0) & 0x00FFU);

	pData[2] = (uint8)((PCOM_KILOMETRAGE_Initial >> 24) & 0x000000FFU);
	pData[3] = (uint8)((PCOM_KILOMETRAGE_Initial >> 16) & 0x000000FFU);
	pData[4] = (uint8)((PCOM_KILOMETRAGE_Initial >> 8) & 0x000000FFU);
	pData[5] = (uint8)((PCOM_KILOMETRAGE_Initial >> 0) & 0x000000FFU);
/* PRQA S 2985 -- */

	pData[6] = PCOM_VCU_Keyposition_Initial;


  (void)Rte_Read_PpShutdownAuthorization_DeShutdownAuthorization(&shutdownAuth);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
  WUM_GetResetRequest(&resetRequest);
  
  if ((FALSE == shutdownAuth) && (0x02U != resetRequest))
    {
      (void)Rte_Call_NvMService_AC3_SRBS_NvPCOMBlockNeed_SetRamBlockStatus(TRUE);
    }



}


static void PCOM_Deserialize_NVM(void)
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static zero-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    Rte_DT_IdtPCOMNvMArray_0 * pData = Rte_Pim_NvPCOMBlockNeed_MirrorBlock();


	PCOM_BMS_SOC_Initial = 0U;
	/* PRQA S 2985,2986 ++ # These operation might be performed without the explicit masking, but it shall be kept in this way for clarification purposes */
	PCOM_BMS_SOC_Initial |= (uint16)(((uint16)pData[0] << 8) & 0xFF00U);
	PCOM_BMS_SOC_Initial |= (uint16)(((uint16)pData[1] << 0) & 0x00FFU);


	PCOM_KILOMETRAGE_Initial = 0U;

	PCOM_KILOMETRAGE_Initial |= (uint32)(((uint32)pData[2] << 24) & 0xFF000000U);
	PCOM_KILOMETRAGE_Initial |= (uint32)(((uint32)pData[3] << 16) & 0x00FF0000U);
	PCOM_KILOMETRAGE_Initial |= (uint32)(((uint32)pData[4] << 8) & 0x0000FF00U);
	PCOM_KILOMETRAGE_Initial |= (uint32)(((uint32)pData[5] << 0) & 0x000000FFU);
	/* PRQA S 2985,2986 -- */
	PCOM_VCU_Keyposition_Initial = (uint8)pData[6];
}

static void PCOM_UpdatePIMs(void)
{

	*Rte_Pim_PimBMS1_LostFramePrefault() = PCOM_RX_Frame_Data[1].LostFramePreFault;
	*Rte_Pim_PimBMS3_LostFramePrefault() = PCOM_RX_Frame_Data[2].LostFramePreFault;
	*Rte_Pim_PimBMS5_LostFramePrefault() = PCOM_RX_Frame_Data[6].LostFramePreFault;
	*Rte_Pim_PimBMS6_LostFramePrefault() = PCOM_RX_Frame_Data[7].LostFramePreFault;
	*Rte_Pim_PimBMS8_LostFramePrefault() = PCOM_RX_Frame_Data[11].LostFramePreFault;
	*Rte_Pim_PimBMS9_LostFramePrefault() = PCOM_RX_Frame_Data[3].LostFramePreFault;
	*Rte_Pim_PimBSIInfo_LostFramePrefault() = PCOM_RX_Frame_Data[10].LostFramePreFault;
	*Rte_Pim_PimCtrlDCDC_LostFramePrefault() = PCOM_RX_Frame_Data[8].LostFramePreFault;
	*Rte_Pim_PimParkCommand_LostFramePrefault() = PCOM_RX_Frame_Data[12].LostFramePreFault;
	*Rte_Pim_PimVCU2_LostFramePrefault() = PCOM_RX_Frame_Data[0].LostFramePreFault;
	*Rte_Pim_PimVCU3_LostFramePrefault() = PCOM_RX_Frame_Data[13].LostFramePreFault;
	*Rte_Pim_PimVCU_552_LostFramePrefault() = PCOM_RX_Frame_Data[14].LostFramePreFault;
	*Rte_Pim_PimVCU_BSI_Wakeup_LostFramePrefault() = PCOM_RX_Frame_Data[5].LostFramePreFault;
	*Rte_Pim_PimVCU_PCANInfo_LostFramePrefault() = PCOM_RX_Frame_Data[4].LostFramePreFault;
	*Rte_Pim_PimVCU_TU_LostFramePrefault() = PCOM_RX_Frame_Data[9].LostFramePreFault;

	*Rte_Pim_PimBMS1_LostFrameCounter() = PCOM_RX_DebounceCounter[1][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimBMS3_LostFrameCounter() = PCOM_RX_DebounceCounter[2][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimBMS5_LostFrameCounter() = PCOM_RX_DebounceCounter[6][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimBMS6_LostFrameCounter() = PCOM_RX_DebounceCounter[7][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimBMS8_LostFrameCounter() = PCOM_RX_DebounceCounter[11][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimBMS9_LostFrameCounter() = PCOM_RX_DebounceCounter[3][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimBSIInfo_LostFrameCounter() = PCOM_RX_DebounceCounter[10][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimCtrlDCDC_LostFrameCounter() = PCOM_RX_DebounceCounter[8][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimParkCommand_LostFrameCounter() = PCOM_RX_DebounceCounter[12][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimVCU2_LostFrameCounter() = PCOM_RX_DebounceCounter[0][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimVCU3_LostFrameCounter() = PCOM_RX_DebounceCounter[13][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimVCU_552_LostFrameCounter() = PCOM_RX_DebounceCounter[14][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimVCU_BSI_Wakeup_LostFrameCounter() = PCOM_RX_DebounceCounter[5][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimVCU_PCANInfo_LostFrameCounter() = PCOM_RX_DebounceCounter[4][PCOM_FAULT_FRAMELOST];
	*Rte_Pim_PimVCU_TU_LostFrameCounter() = PCOM_RX_DebounceCounter[9][PCOM_FAULT_FRAMELOST];


	*Rte_Pim_PimABS_VehSpd_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[49].debounce.diagInvalid.prefault;
	*Rte_Pim_PimBMS_MainConnectorState_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[3].debounce.diagInvalid.prefault;
	*Rte_Pim_PimBMS_SOC_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0].debounce.diagInvalid.prefault;
	*Rte_Pim_PimBMS_Voltage_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1].debounce.diagInvalid.prefault;
	*Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[33].debounce.diagInvalid.prefault;
	*Rte_Pim_PimCPT_TEMPOREL_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[34].debounce.diagInvalid.prefault;
	*Rte_Pim_PimDDE_GMV_SEEM_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42].debounce.diagInvalid.prefault;
	*Rte_Pim_PimKILOMETRAGE_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[35].debounce.diagInvalid.prefault;
	*Rte_Pim_PimVCU_AccPedalPosition_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53].debounce.diagInvalid.prefault;
	*Rte_Pim_PimVCU_DCDCActivation_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[27].debounce.diagInvalid.prefault;
	*Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25].debounce.diagInvalid.prefault;
	*Rte_Pim_PimVCU_EPWT_Status_InvalidValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[32].debounce.diagInvalid.prefault;

	*Rte_Pim_PimABS_VehSpd_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[49].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[3].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimBMS_SOC_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimBMS_Voltage_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[33].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[34].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimKILOMETRAGE_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[35].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[27].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25].debounce.diagInvalid.counterFail;
	*Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[32].debounce.diagInvalid.counterFail;

	*Rte_Pim_PimABS_VehSpd_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[49].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimBMS_MainConnectorState_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[3].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimBMS_SOC_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimBMS_Voltage_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimCOMPTEUR_RAZ_GCT_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[33].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimCPT_TEMPOREL_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[34].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimDDE_GMV_SEEM_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimKILOMETRAGE_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[35].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimVCU_AccPedalPosition_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimVCU_DCDCActivation_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[27].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimVCU_DCDCVoltageReq_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25].debounce.diagInvalid.counterHealed;
	*Rte_Pim_PimVCU_EPWT_Status_InvalidValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[32].debounce.diagInvalid.counterHealed;

	*Rte_Pim_PimBMS_SOC_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0].debounce.diagForbidden.prefault;
	*Rte_Pim_PimBMS_Voltage_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1].debounce.diagForbidden.prefault;
	*Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[19].debounce.diagForbidden.prefault;
	*Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42].debounce.diagForbidden.prefault;
	*Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[43].debounce.diagForbidden.prefault;
	*Rte_Pim_PimETAT_GMP_HYB_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[41].debounce.diagForbidden.prefault;
	*Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[39].debounce.diagForbidden.prefault;
	*Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[40].debounce.diagForbidden.prefault;
	*Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[45].debounce.diagForbidden.prefault;
	*Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53].debounce.diagForbidden.prefault;
	*Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25].debounce.diagForbidden.prefault;
	*Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValuePrefault() = (boolean) PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[23].debounce.diagForbidden.prefault;

	*Rte_Pim_PimBMS_SOC_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[19].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[43].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[41].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[39].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[40].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[45].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25].debounce.diagForbidden.counterFail;
	*Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterConfirm() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[23].debounce.diagForbidden.counterFail;

	*Rte_Pim_PimBMS_SOC_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[0].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimBMS_Voltage_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[1].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimBSI_ChargeTypeStatus_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[19].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimDDE_GMV_SEEM_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[42].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimDMD_MEAP_2_SEEM_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[43].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimETAT_GMP_HYB_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[41].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimETAT_PRINCIP_SEV_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[39].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimETAT_RESEAU_ELEC_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[40].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimMODE_EPS_REQUEST_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[45].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimVCU_AccPedalPosition_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[53].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimVCU_DCDCVoltageReq_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[25].debounce.diagForbidden.counterHealed;
	*Rte_Pim_PimVCU_HEAT_PUMP_WORKING_MODE_ForbiddenValueCounterHeal() = PCOM_RX_Signals_Data_PCOM_RX_Signals_Data[23].debounce.diagForbidden.counterHealed;

}


void Appl_CanOverrun ( uint8 Controller )
{

    /* - Static non-init variables declaration ---------- */
    #define CtApPCOM_START_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static non-init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static zero-init variables declaration --------- */
    #define CtApPCOM_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    static volatile uint32 CanFailCounter0 = 0U;
    static volatile uint32 CanFailCounter1 = 0U;
    static volatile uint32 CanFailCounterNone = 0U;

    #define CtApPCOM_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Static init variables declaration -------------- */
    #define CtApPCOM_START_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

    /* Static init variables HERE... */

    #define CtApPCOM_STOP_SEC_VAR_INIT_UNSPECIFIED
    #include "CtApPCOM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


    /* - Automatic variables declaration ---------------- */
    /* Automatic variables HERE... */


	if(0U==Controller)
	{
		CanFailCounter0++;
	}
	else if(1U==Controller)
	{
		CanFailCounter1++;
	}
	else
	{
		/*Not expected*/
		CanFailCounterNone++;
	}

}

void PCOM_DC2_TimeoutNotification(void)
{
	PCOM_C_Mute_INC_Flag = TRUE;
	/* Transmission not done, increment the counter */
}

void PCOM_DC2_SendNotification(void)
{
	PCOM_C_Mute_DEC_Flag = TRUE;
	/* Transmission done, decrement the counter */
}

static void PCOM_BusOff_Evaluation(void)
{
	boolean Fault_data = FALSE;

	uint8 CAN_hdlr = 0U;
	uint8 State;

	(void)CanSM_CheckBorLevel(CAN_hdlr, &State);

	if (0U == State)
	{
		Fault_data = FALSE;
		PCOM_BOREvents_count = 0U;
	}
	else if (2U == State)
	{
		if (PCOM_BOREvents_count == PCOM_CBOFF_CONF)
		{
			Fault_data = TRUE;
		}
	}
	else
	{
		/* MISRA required */
	}

	PCOM_BusOffFault = Fault_data;

	(void)Rte_Write_PpBusOffCANFault_DeBusOffCANFault(Fault_data);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

static void PCOM_CMute_Evaluation(void)
{

	if (FALSE != PCOM_C_Mute_INC_Flag)
	{
		if(PCOM_C_MAX > PCOM_C_Mute)
		{
			if ((PCOM_C_Mute + PCOM_MUTE_INC) <= PCOM_C_MAX)
			{
				PCOM_C_Mute = PCOM_C_Mute + PCOM_MUTE_INC;
			}
			else
			{
				PCOM_C_Mute = PCOM_C_MAX;
			}

		}
		else
		{
			PCOM_C_Mute = PCOM_C_MAX;
		}

		PCOM_C_Mute_INC_Flag = FALSE;
	}

	if (FALSE != PCOM_C_Mute_DEC_Flag)
	{
		if(PCOM_C_MIN < PCOM_C_Mute)
		{
			if ((PCOM_C_Mute - PCOM_MUTE_DEC) >= PCOM_C_MIN)
			{
				PCOM_C_Mute = PCOM_C_Mute - PCOM_MUTE_DEC;
			}
			else
			{
				PCOM_C_Mute = PCOM_C_MIN;
			}
		}
		else
		{
			PCOM_C_Mute = PCOM_C_MIN;
		}
		PCOM_C_Mute_DEC_Flag = FALSE;
	}

	if(PCOM_C_MIN == PCOM_C_Mute)
	{
		(void)Rte_Write_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(FALSE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else if(PCOM_C_MAX == PCOM_C_Mute)
	{
		(void)Rte_Write_PpAbsentMuteNERRFault_DeAbsentMuteNERRFault(TRUE);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	else
	{
		/* nothing to do here */
	}

}

static void PCOM_CMute_Init(void)
{
	boolean EventFailed;

	(void)Rte_Call_EvtInfo_DTC_0xc08913_GetEventFailed(&EventFailed);

	if(FALSE != EventFailed)
	{
		PCOM_C_Mute = PCOM_C_MAX;
	}
	else
	{
		PCOM_C_Mute = PCOM_C_MIN;
	}

	PCOM_DiagMux_CAN_typeAUT = PCOM_DIAGMUX_CAN_TYPEAUT_OFF;
}


void PCOM_BusOffBeginNotification(uint8 NetworkHandle, uint8 * OnlineDelayCyclesPtr) /* PRQA S 3206 # The not used extra parameter is included to fulfill the function signature defined by Autosar */
{
	if (PCOM_BOREvents_count < PCOM_CBOFF_CONF)
	{
		PCOM_BOREvents_count++;
	}
	else
	{
		PCOM_BOREvents_count = PCOM_CBOFF_CONF;
	}
}


void PCOM_BusOffEndNotification(uint8 NetworkHandle) /* PRQA S 3206 # The not used extra parameter is included to fulfill the function signature defined by Autosar */
{

}


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxygen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
