/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference.
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use.
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CtApMSC.c
 *           Config:  C:/obcp11_aurix_autosar/CODE/Utils/davinci_workspace/OBCP11.dpa
 *        SW-C Type:  CtApMSC
 *  Generation Time:  2020-11-23 11:42:19
 *
 *        Generator:  MICROSAR RTE Generator Version 4.21.0
 *                    RTE Core Version 1.21.0
 *          License:  CBD1900270
 *
 *      Description:  C-Code implementation template for SW-C <CtApMSC>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */

/*!
 * \defgroup AppSw
 * \defgroup BaseSw
 *
 * \defgroup MSC
 * \ingroup AppSw
 * \author Pablo Bolas
 * \brief Miscellaneous module.
 * \details This module has miscellaneous functions.
 *
 * \{
 * \file  MSC.c
 * \brief  Generic code of the MSC module.
 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 * Dcm_OpStatusType
 *   uint8 represents integers with a minimum value of 0 and a maximum value of 255.
 *      The order-relation on uint8 is: x < y if y - x is positive.
 *      uint8 has a lexical representation consisting of a finite-length sequence 
 *      of decimal digits (#x30-#x39).
 *      
 *      For example: 1, 0, 126, +10.
 *
 *
 * Mode Declaration Groups:
 * ========================
 * WdgM_Mode
 *   The mode declaration group WdgMMode represents the modes of the Watchdog Manager module that will be notified to the SW-Cs / CDDs and the RTE.
 *
 *********************************************************************************************************************/

#include "Rte_CtApMSC.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 0857 ++ #Max number of macros avoidance*/
/* PRQA S 1503,1532 EOF # RCMA is not able to solve the external calls to tasks and services from the RTE or other modules. */
/*---------------------------------------------------------------------------*/
/* MACRO DEFINITIONS                                                         */
/*---------------------------------------------------------------------------*/

/** Gain 10.*/
#define MSC_POWER_GAIN_10				10U
/** Gain 100.*/
#define MSC_POWER_GAIN_100				100U

/** Max power in monophasic.*/
#define MSC_POWER_LIMIT_HW_MONO 		7600U
/** Max current in monophasic.*/
#define MSC_CURRENT_LIMIT_HW_MONO 		30U

/** Max power in triphasic.*/
#define MSC_POWER_LIMIT_HW_TRIP			11000U
/** Max current in triphasic.*/
#define MSC_CURRENT_LIMIT_HW_TRIP 		15U

/** Square root of 3 gain.*/
#define MSC_VOLT_PH_LN_CONVERION_GAIN	37837U
/** Square root of 3 Q factor.*/
#define MSC_VOLT_PH_LN_CONVERION_Q		16U

/** Signal not available for U16.*/
#define MSC_SNA_U16						0xFFFF
/** Signal not available for U8.*/
#define MSC_SNA_U8						0xFF

/** Degradation factor.*/
#define MSC_DERATION_DIV				10U

/** Temperature conversion gain.*/
#define MSC_TEMP_GAIN					10U
/** Temperature conversion offset.*/
#define MSC_TEMP_OFFSET					10U

/** Power filter dynamic response*/
#define MSC_FILTER_GAIN			247U
/** Power filter dynamic*/
#define MSC_FILTER_Q				8U
/** Maximum efficiency: 98%*/
#define MSC_EFFICIENCY_MAX_Q8	251U

#define MSC_POWER_LATCH_DONE		0x55U
#define MSC_POWER_LATCH_NOT_DONE	0xCCU

#define MSC_MAX_DCDC_POWER_ABSOLUTE		(2200U)

/* PRQA S 0857 -- #Max number of macros avoidance*/
/* PRQA S 3214 ++ #Following macros are autogenrated.*/

/*---------------------------------------------------------------------------*/
/* LOCAL VARIABLES                                                           */
/*---------------------------------------------------------------------------*/
/* - Static non-init variables declaration ---------- */
#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/** Realy voltage threshold.*/
static uint8 MSC_DCRelayVoltage_threshold;
/** Power max limited by calibration.*/
static uint8 MSC_obcPowerMax_cal;
/**Power filter value*/
static uint16 PowerSlow_FilteredValue;
/** Debug on CAN*/
static uint8 MSC_DebugCan;

#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static zero-init variables declaration --------- */
#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static zero-init variables HERE... */

#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/* - Static init variables declaration -------------- */
#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* Static init variables HERE... */

#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS PROTOTYPES                                                */
/*---------------------------------------------------------------------------*/
/**
 * \brief E-Lock set point.
 * \details this function determines the set point for the E-Lock position
 */
static void MSC_ElockSetPoint_10ms(void);
/**Debounce Proximity*/
static OBC_ChargingConnectionConfirmati MSC_ProximityDebounce(OBC_ChargingConnectionConfirmati Inputstate);
/**
 * \brief Maximum power delivered by the OBC.
 * \details this function determines the maximum power delivered by the OBC
 * depending on: HW limitations, EVES limitations and user limitations.
 */
static void MSC_powerMax_10ms(void);

/**
 *	\brief Maximum power delivered by the OBC in slow mode.
 *	\details  This function determines the max power delivered by the OBC in
 *	slow mode.
 *	\param[in] inputVoltageMode Input volyage mode.
 *	\param[in] PFC_PhaseVoltageRMS	Phase voltage measure.
 *	\param[in] EVSE_MaxCurrent_slow	Current limitation by EVES.
 *	\param[in] derating Derating factor.
 *	\param[out] powerMax_HW	Power limitation by hardware.
 *	\param[out] powerMax_CP	Power limitation by EVES.
 *	\param[out] OBCpowerMax	Max Power Delivered.
 */
static void MSC_PowerMax_Slow(uint8 inputVoltageMode, uint16 PFC_PhaseVoltageRMS,
								uint16 EVSE_MaxCurrent_slow, uint16 *powerMax_HW,
								uint16 *powerMax_CP, uint8 *OBCpowerMax);
/**
 * \brief Min power.
 * \details This function returns the min power.
 * \Param[in] powerMax_CAL Power limitation by calibration.
 * \param[in] powerMax_HW Power limitation by hardware.
 * \param[in] powerMax_CP Power limitation by EVES.
 * \retunr Min power
 */
static uint8 MSC_powerMin(uint8 powerMax_CAL,uint16 powerMax_HW, uint16 powerMax_CP);

/**
 * \brief Temperature selection
 * \details This function publish the line temperature depending on the charging
 * mode.
 */
static void MSC_TemperatureSelection_100ms(void);
/**
 * \brief Get max temperature
 * \details This function returns the biggest line temperature.
 * \param[in] tempAC1 Temperature AC1.
 * \param[in] tempAC2 Temperature AC2.
 * \param[in] tempAC3 Temperature AC3.
 * \return Biggest line temperature.
 */
static uint16 MSC_getMaxTemperature(IdtOutputTempMeas tempAC1,IdtOutputTempMeas tempAC2,IdtOutputTempMeas tempAC3);

/** Update calibratables*/
static void MSC_UpdateCalibratables(void);

/*Power Max Filter*/
static uint8 MSC_Power_filter(uint8 input);

/**
 *  Calculate signal DCDC_OBCDCDCRTPowerLoad
 */
static void MSC_DCDC_OBCDCDCRTPowerLoad(void);

void MSC_SaveDonePowerLatch(void);

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * BMS_DCRelayVoltage: Integer in interval [0...500]
 * BMS_OnBoardChargerEnable: Boolean
 * DCDC_OBCDCDCRTPowerLoad: Integer in interval [0...255]
 * DCLV_Applied_Derating_Factor: Integer in interval [0...2047]
 * DCLV_Power: Integer in interval [0...2500]
 * IdtEVSEMaximumPowerLimit: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtInputVoltageThreshold: Integer in interval [0...100]
 *   Unit: [V], Factor: 1, Offset: 0
 * IdtMaxInputACCurrentEVSE: Integer in interval [0...65535]
 *   Unit: [A], Factor: 0.1, Offset: 0
 * IdtOBC_PowerAvailableControlPilot: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBC_PowerAvailableHardware: Integer in interval [0...65535]
 *   Unit: [W], Factor: 1, Offset: 0
 * IdtOBC_PowerMaxCalculated: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * IdtOBC_PowerMaxValue: Integer in interval [0...25]
 *   Unit: [W], Factor: 1000, Offset: 0
 * IdtOutputTempMeas: Integer in interval [0...65535]
 *   Unit: [deg C], Factor: 0.1, Offset: -40
 * IdtPDERATING_OBC: Integer in interval [0...1024]
 * IdtTimeLockDelay: Integer in interval [0...100]
 *   Unit: [ms], Factor: 100, Offset: 0
 * OBC_OBCStartSt: Boolean
 * OBC_PowerMax: Integer in interval [0...255]
 *   Unit: [W], Factor: 100, Offset: 0
 * OBC_SocketTempL: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * OBC_SocketTempN: Integer in interval [0...190]
 *   Unit: [deg C], Factor: 1, Offset: -50
 * PFC_VPH12_RMS_V: Integer in interval [0...1023]
 * PFC_VPH23_RMS_V: Integer in interval [0...1023]
 * PFC_VPH31_RMS_V: Integer in interval [0...1023]
 * VCU_DiagMuxOnPwt: Boolean
 * boolean: Boolean (standard type)
 * dtRef_const_VOID: DataReference
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BMS_MainConnectorState: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_contactors_opened (0U)
 *   Cx1_precharge (1U)
 *   Cx2_contactors_closed (2U)
 *   Cx3_Invalid (3U)
 * BSI_LockedVehicle: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_Locked (0U)
 *   Cx1_Locked (1U)
 *   Cx2_Overlocked (2U)
 *   Cx3_Reserved (3U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * IdtDebugPortID: Enumeration of integer in interval [0...2] with enumerators
 *   DEBUG_PORT_ID_0 (0U)
 *   DEBUG_PORT_ID_1 (1U)
 *   DEBUG_PORT_ID_2 (2U)
 * IdtELockSetPoint: Enumeration of integer in interval [0...1] with enumerators
 *   ELOCK_SETPOINT_UNLOCKED (0U)
 *   ELOCK_SETPOINT_LOCKED (1U)
 * IdtInputVoltageMode: Enumeration of integer in interval [0...2] with enumerators
 *   IVM_NO_INPUT_DETECTED (0U)
 *   IVM_MONOPHASIC_INPUT (1U)
 *   IVM_TRIPHASIC_INPUT (2U)
 * OBC_ChargingConnectionConfirmati: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_invalid_value (0U)
 *   Cx1_not_connected (1U)
 *   Cx2_full_connected (2U)
 *   Cx3_CC_is_half_connected (3U)
 * OBC_ChargingMode: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_no_charging (0U)
 *   Cx1_slow_charging (1U)
 *   Cx2_China_fast_charging (2U)
 *   Cx3_Euro_fast_charging (3U)
 * OBC_InputVoltageSt: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_No_220V_AC (0U)
 *   Cx1_220V_AC_connected (1U)
 *   Cx2_220V_AC_disconnected (2U)
 *   Cx3_Invalid_value (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   IdtOBC_PowerAvailableControlPilot *Rte_Pim_PimOBC_PowerAvailableControlPilot(void)
 *   IdtOBC_PowerAvailableHardware *Rte_Pim_PimOBC_PowerAvailableHardware(void)
 *   uint8 *Rte_Pim_PimMSC_PowerLatchFlag(void)
 *   IdtOBC_PowerMaxCalculated *Rte_Pim_PimOBC_PowerMaxCalculated(void)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   IdtOBC_PowerMaxValue Rte_CData_CalOBC_PowerMaxValue(void)
 *   IdtTimeLockDelay Rte_CData_CalTimeLockDelay(void)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   IdtInputVoltageThreshold Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold(void)
 *
 *********************************************************************************************************************/


#define CtApMSC_START_SEC_CODE
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ConditionCheckRead> of PortPrototype <DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ConditionCheckRead (returns application error)
 *********************************************************************************************************************/


	   Std_ReturnType ret;
		(void)OpStatus;	/* Remove compilator warning */

		if (NULL_PTR != ErrorCode)
		{
			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
		else
		{
			ret = RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK;
		}

	    return ret;

  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_DCM_E_PENDING
 *   RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Data) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_ReadData (returns application error)
 *********************************************************************************************************************/

	Std_ReturnType ret;
	uint8 PimValue;
	(void)OpStatus;	/* Remove compilator warning */


	if (NULL_PTR != Data)
	{
		PimValue = *Rte_Pim_PimMSC_PowerLatchFlag();

		if (MSC_POWER_LATCH_NOT_DONE == PimValue)
		{
			Data[0] = 0x00;		/* Not Realized */
		}
		else
		{
			Data[0] = 0xFF;		/* Realized */
		}

		ret = RTE_E_OK;
	}
	else
	{
		ret = RTE_E_DataServices_Power_Latch_Flag_State_PLFS_PowerLatchFlagState_PLFS_E_NOT_OK;
	}

    return ret;
  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApMSC_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_init_doc
 *********************************************************************************************************************/

/*!
 * \brief MSC initialization function.
 *
 * This function initializes all the internal variables.
 * \note This function should be called before any other function inside this
 * module.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApMSC_CODE) RCtApMSC_init(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_init
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	MSC_DCRelayVoltage_threshold=(uint8)Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold();
	MSC_obcPowerMax_cal=(uint8)Rte_CData_CalOBC_PowerMaxValue();
		PowerSlow_FilteredValue = 0U;
        
		/* Power latch feature */
		/* Check value in PIM */
		if ((MSC_POWER_LATCH_DONE != *Rte_Pim_PimMSC_PowerLatchFlag()) && (MSC_POWER_LATCH_NOT_DONE != *Rte_Pim_PimMSC_PowerLatchFlag()))
		{
			/* Flash not initialized: First execution */
			*Rte_Pim_PimMSC_PowerLatchFlag() = MSC_POWER_LATCH_DONE;

			/* Immediate write into NVM */
			(void)Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(Rte_Pim_PimMSC_PowerLatchFlag());/* PRQA S 3110, 0315,3417, 3426 #RTE read/write confirmation not used*/
		}
        
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApMSC_task100ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 100ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(IdtOutputTempMeas *data)
 *   Std_ReturnType Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(IdtOutputTempMeas *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL(OBC_SocketTempL data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN(OBC_SocketTempN data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task100ms_doc
 *********************************************************************************************************************/

 /*!
  * \brief MSC task.
  *
  * This function is called periodically by the scheduler. This function calls
  * the miscellaneous 100ms functions.
  */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApMSC_CODE) RCtApMSC_task100ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task100ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	MSC_UpdateCalibratables();
	MSC_TemperatureSelection_100ms();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RCtApMSC_task10ms
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(boolean *data)
 *   Std_ReturnType Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(IdtEVSEMaximumPowerLimit *data)
 *   Std_ReturnType Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(boolean *data)
 *   Std_ReturnType Rte_Read_PpInputVoltageMode_DeInputVoltageMode(IdtInputVoltageMode *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(BMS_DCRelayVoltage *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(BMS_MainConnectorState *data)
 *   Std_ReturnType Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(BMS_OnBoardChargerEnable *data)
 *   Std_ReturnType Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(BSI_LockedVehicle *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(DCLV_Applied_Derating_Factor *data)
 *   Std_ReturnType Rte_Read_PpInt_DCLV_Power_DCLV_Power(DCLV_Power *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(OBC_ChargingConnectionConfirmati *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(OBC_ChargingMode *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(OBC_InputVoltageSt *data)
 *   Std_ReturnType Rte_Read_PpInt_OBC_OBCStartSt_OBC_OBCStartSt(OBC_OBCStartSt *data)
 *   Std_ReturnType Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(PFC_VPH12_RMS_V *data)
 *   Std_ReturnType Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(IdtMaxInputACCurrentEVSE *data)
 *   Std_ReturnType Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(IdtPDERATING_OBC *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(IdtELockSetPoint data)
 *   Std_ReturnType Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad data)
 *   Std_ReturnType Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(OBC_PowerMax data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_PpSetDebugPinValue_OpSetDebugPinValue(IdtDebugPortID debugPinID, boolean debugPinValue)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(uint8 frameNum, uint8 byteNum, uint8 data)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task10ms_doc
 *********************************************************************************************************************/

/*!
 * \brief MSC task.
 *
 * This function is called periodically by the scheduler. This function calls
 * the miscellaneous 10ms functions.
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CtApMSC_CODE) RCtApMSC_task10ms(void) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RCtApMSC_task10ms
 *********************************************************************************************************************/

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */

	MSC_DebugCan = 0U;

	MSC_UpdateCalibratables();
	MSC_ElockSetPoint_10ms();
	MSC_powerMax_10ms();
	MSC_DCDC_OBCDCDCRTPowerLoad();

	(void)Rte_Call_PpSetIntCANDebugSignal_OpSetIntCANDebugSignal(3U,2U,MSC_DebugCan);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_Powerlatch_Information_Positioning_PIP>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_RoutineStatusRecord_01_02, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_RequestResults (returns application error)
 *********************************************************************************************************************/

	(void)OpStatus;	/* Remove compilator warning */

	if((NULL_PTR!=Out_RoutineStatusRecord_01_02)&&(NULL_PTR!=ErrorCode))
	{

		*Out_RoutineStatusRecord_01_02 = 0x02; /* Routine finished OK */

		*ErrorCode = DCM_E_POSITIVERESPONSE;
	}


  return RTE_E_OK;
  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Powerlatch_Information_Positioning_PIP_Start
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Powerlatch_Information_Positioning_PIP>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(VCU_DiagMuxOnPwt *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(dtRef_const_VOID SrcPtr)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_NvMService_AC3_SRBS_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, uint8 *Out_RoutineStatusRecord_01_02, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_DCM_E_PENDING
 *   RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, CtApMSC_CODE) RoutineServices_Powerlatch_Information_Positioning_PIP_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) Out_RoutineStatusRecord_01_02, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_CTAPMSC_APPL_VAR) ErrorCode) /* PRQA S 0624, 3206 */ /* MD_Rte_0624, MD_Rte_3206 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Powerlatch_Information_Positioning_PIP_Start (returns application error)
 *********************************************************************************************************************/
	Std_ReturnType ret;
	VCU_DiagMuxOnPwt VCU_DiagMuxOnPwtData;
	(void)OpStatus;	/* Remove compilator warning */

	if((NULL_PTR!=Out_RoutineStatusRecord_01_02)&&(NULL_PTR!=ErrorCode))
	{
		/* Get Diag_Mux_On signal */
		(void)Rte_Read_PpInt_VCU_DiagMuxOnPwt_VCU_DiagMuxOnPwt(&VCU_DiagMuxOnPwtData);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

		if (0x01U == VCU_DiagMuxOnPwtData)
		{
			/* Reject the request */

			*Out_RoutineStatusRecord_01_02 = 0x02; /* Routine finished OK */

			*ErrorCode = DCM_E_CONDITIONSNOTCORRECT;
			ret = RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK;
		}
		else
		{
			/* Flash not initialized: First execution */
			*Rte_Pim_PimMSC_PowerLatchFlag() = MSC_POWER_LATCH_NOT_DONE;

			/* Immediate write into NVM */
			(void)Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(Rte_Pim_PimMSC_PowerLatchFlag());/* PRQA S 3110, 0315, 3417, 3426 #RTE read/write confirmation not used*/

			*Out_RoutineStatusRecord_01_02 = 0x02; /* Routine finished OK */

			*ErrorCode = DCM_E_POSITIVERESPONSE;
			ret = RTE_E_OK;
		}
	}
	else
	{
		if (NULL_PTR!=ErrorCode)
		{
			*ErrorCode = DCM_E_GENERALREJECT;
		}
		ret = RTE_E_RoutineServices_Powerlatch_Information_Positioning_PIP_E_NOT_OK;
	}

  return ret;
  
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CtApMSC_STOP_SEC_CODE
#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/*---------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS                                                           */
/*---------------------------------------------------------------------------*/
static void MSC_ElockSetPoint_10ms(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	OBC_ChargingConnectionConfirmati PlugConfirmation;
	BMS_MainConnectorState ConnectorState;
	BMS_OnBoardChargerEnable obcEnable;
	BSI_LockedVehicle VehicleLocked;
	OBC_InputVoltageSt VoltageState;
	BMS_DCRelayVoltage DCrelayVoltage;
	OBC_ChargingMode chargingMode;
	IdtELockSetPoint ElockSetPoint;
	boolean ForceElock_Mode4;


	(void)Rte_Read_PpInt_OBC_ChargingConnectionConfirmati_OBC_ChargingConnectionConfirmati(&PlugConfirmation);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_MainConnectorState_BMS_MainConnectorState(&ConnectorState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_OnBoardChargerEnable_BMS_OnBoardChargerEnable(&obcEnable);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BSI_LockedVehicle_BSI_LockedVehicle(&VehicleLocked);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_InputVoltageSt_OBC_InputVoltageSt(&VoltageState);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_BMS_DCRelayVoltage_BMS_DCRelayVoltage(&DCrelayVoltage);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&chargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpForceElockCloseMode4_DeForceElockCloseMode4(&ForceElock_Mode4);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/

	PlugConfirmation = MSC_ProximityDebounce(PlugConfirmation);

	if((uint8)Cx2_full_connected!=PlugConfirmation)
	{
		ElockSetPoint =  ELOCK_SETPOINT_UNLOCKED;
	}
	else if(Cx1_Locked==VehicleLocked)
	{
		ElockSetPoint = ELOCK_SETPOINT_LOCKED;
	}
	else if((Cx2_contactors_closed==ConnectorState)
			&&(FALSE!=obcEnable))
	{
		ElockSetPoint = ELOCK_SETPOINT_LOCKED;
	}
	/*Work around for fast charge mode*/
	else if(((Cx3_Euro_fast_charging==chargingMode)&&(FALSE!=obcEnable))
					||(TRUE==ForceElock_Mode4))
	{
		ElockSetPoint = ELOCK_SETPOINT_LOCKED;
	}
	else if((Cx0_No_220V_AC!=VoltageState)
			&&(Cx2_220V_AC_disconnected!=VoltageState))
	{
		ElockSetPoint = ELOCK_SETPOINT_LOCKED;
	}
	else if(DCrelayVoltage>=(uint16)MSC_DCRelayVoltage_threshold)
	{
		ElockSetPoint = ELOCK_SETPOINT_LOCKED;
	}
	else
	{
		ElockSetPoint = ELOCK_SETPOINT_UNLOCKED;
	}


	(void)Rte_Write_PpElockSetpoint_NormalMode_DeElockSetpoint_NormalMode(ElockSetPoint);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	MSC_DebugCan |= (ElockSetPoint<<7U);
}
static OBC_ChargingConnectionConfirmati MSC_ProximityDebounce(OBC_ChargingConnectionConfirmati Inputstate)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	static OBC_ChargingConnectionConfirmati PreviousState = Cx0_invalid_value;
	static OBC_ChargingConnectionConfirmati OutSate = Cx0_invalid_value;
	static uint8 Counter = 0U;

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */


	if(Cx1_not_connected==Inputstate)
	{
		OutSate = Inputstate;
		Counter = 0U;
	}
	else if(Inputstate==OutSate)
	{
		Counter = 0U;
	}
	else if(Rte_CData_CalTimeLockDelay()<=Counter)
	{
		OutSate = Inputstate;
		Counter = 0U;
	}
	else if(Inputstate==PreviousState)
	{
		Counter++;
	}
	else
	{
		Counter = 0U;
	}

	PreviousState = Inputstate;

	return OutSate;
}
static void MSC_powerMax_10ms(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*inputs*/
	OBC_ChargingMode chargingMode;
	IdtInputVoltageMode inputVoltageMode;
	PFC_VPH12_RMS_V PFC_PhaseVoltageRMS;
	IdtMaxInputACCurrentEVSE EVSE_MaxCurrent_slow;
	IdtEVSEMaximumPowerLimit EVSE_MaxPower_fast;
	boolean discharge;
	/*outputs*/
	OBC_PowerMax OBCpowerMax;
	IdtOBC_PowerAvailableHardware powerMax_HW;
	IdtOBC_PowerAvailableControlPilot powerMax_CP;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&chargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInputVoltageMode_DeInputVoltageMode(&inputVoltageMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_PFC_VPH_RMS_V_PFC_VPH12_RMS_V(&PFC_PhaseVoltageRMS);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpMaxInputACCurrentEVSE_DeMaxInputACCurrentEVSE(&EVSE_MaxCurrent_slow);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpEVSEMaximumPowerLimit_DeEVSEMaximumPowerLimit(&EVSE_MaxPower_fast);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpActiveDischargeRequest_DeActiveDischargeRequest(&discharge);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if(FALSE!=discharge)
	{
		OBCpowerMax = 0U;
	}
	else if((uint8)Cx3_Euro_fast_charging==chargingMode)
	{
		uint16 OBCpowerMax_temp = (uint16)MSC_obcPowerMax_cal*(uint16)MSC_POWER_GAIN_10;
		/*Unit conversion: form W to 100W:*/
		EVSE_MaxPower_fast /= MSC_POWER_GAIN_100;

		if(EVSE_MaxPower_fast<OBCpowerMax_temp)
		{
			OBCpowerMax_temp = EVSE_MaxPower_fast;
		}

		if((uint16)MSC_SNA_U8<OBCpowerMax_temp)
		{
			OBCpowerMax = MSC_SNA_U8;
		}
		else
		{
			OBCpowerMax = (uint8)OBCpowerMax_temp;
		}

		/*Discharge Filter*/
		PowerSlow_FilteredValue = 0U;

	}else if((uint8)Cx1_slow_charging==chargingMode)
	{
		MSC_PowerMax_Slow(inputVoltageMode,PFC_PhaseVoltageRMS,EVSE_MaxCurrent_slow,
							&powerMax_HW,&powerMax_CP,&OBCpowerMax);
		*(Rte_Pim_PimOBC_PowerAvailableHardware())=powerMax_HW;
		*(Rte_Pim_PimOBC_PowerAvailableControlPilot()) = powerMax_CP;

		OBCpowerMax = MSC_Power_filter(OBCpowerMax);
	}
	else
	{
		OBCpowerMax = 0U;
		/*Discharge Filter*/
		PowerSlow_FilteredValue = 0U;
	}


	(void)Rte_Write_PpInt_OBC_PowerMax_OBC_PowerMax(OBCpowerMax);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	MSC_DebugCan |= (uint8)(EVSE_MaxCurrent_slow/10U);

}
static void MSC_PowerMax_Slow(uint8 inputVoltageMode, uint16 PFC_PhaseVoltageRMS,
								uint16 EVSE_MaxCurrent_slow, uint16 *powerMax_HW,
								uint16 *powerMax_CP, uint8 *OBCpowerMax)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*internal Variables*/
	uint8 N_phase;
	uint16 VoltageLN;
	uint8 currentLimit_HW;
	uint16 powerLimit_HW;
	uint16 powerDeratingFactor;/*W*/
	uint32 power_temp;

	(void)Rte_Read_PpPDERATING_OBC_DePDERATING_OBC(&powerDeratingFactor);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((uint8)IVM_MONOPHASIC_INPUT==inputVoltageMode)
	{
		N_phase=1U;
		VoltageLN = PFC_PhaseVoltageRMS;
		powerLimit_HW = MSC_POWER_LIMIT_HW_MONO;
		currentLimit_HW = MSC_CURRENT_LIMIT_HW_MONO;
	}
	else if((uint8)IVM_TRIPHASIC_INPUT==inputVoltageMode)
	{
		N_phase=3U;
		VoltageLN = (uint16)(((uint32)PFC_PhaseVoltageRMS*(uint32)MSC_VOLT_PH_LN_CONVERION_GAIN)
					>>MSC_VOLT_PH_LN_CONVERION_Q);
		powerLimit_HW = MSC_POWER_LIMIT_HW_TRIP;
		currentLimit_HW = MSC_CURRENT_LIMIT_HW_TRIP;
	}
	else
	{
		N_phase=0U;
		VoltageLN=0U;
		powerLimit_HW = 0U;
		currentLimit_HW = 0U;
	}


	/*CP power*/
	power_temp = (uint32)N_phase*((uint32)EVSE_MaxCurrent_slow*(uint32)VoltageLN/(uint32)MSC_POWER_GAIN_10);
	/*Estimate maximum output power*/
	power_temp = (power_temp*(uint32)MSC_EFFICIENCY_MAX_Q8)>>8;

	if((uint32)MSC_SNA_U16<power_temp)
	{
		*powerMax_CP = (uint16)MSC_SNA_U16;
	}
	else
	{
		*powerMax_CP = (uint16)power_temp;
	}

	/*HW power*/
	power_temp = (uint32)N_phase*(uint32)VoltageLN*(uint32)currentLimit_HW;
	/*Estimate maximum output power*/
	power_temp = (power_temp*(uint32)MSC_EFFICIENCY_MAX_Q8)>>8;
	if(power_temp > (uint32)powerLimit_HW)
	{
		power_temp = (uint32)powerLimit_HW;
	}

	/*Apply derating, power_temp limited to U16*/
	if(((uint32)1U<<MSC_DERATION_DIV)>powerDeratingFactor)
	{
		power_temp = (power_temp * (uint32)powerDeratingFactor)>>MSC_DERATION_DIV;
	}


	*powerMax_HW = (uint16)power_temp;


	*OBCpowerMax = MSC_powerMin(MSC_obcPowerMax_cal,*powerMax_HW,*powerMax_CP);

}
static uint8 MSC_powerMin(uint8 powerMax_CAL,uint16 powerMax_HW, uint16 powerMax_CP)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 ret_min = (uint16)powerMax_CAL*(uint16)MSC_POWER_GAIN_10;
	uint16 power_temp;

	power_temp = powerMax_HW/MSC_POWER_GAIN_100;

	if(power_temp<ret_min)
	{
		ret_min=power_temp;
	}
	power_temp= powerMax_CP/MSC_POWER_GAIN_100;

	if(power_temp<ret_min)
	{
		ret_min=power_temp;
	}

	if((uint16)MSC_SNA_U8<=ret_min)
	{
		ret_min=(uint16)MSC_SNA_U8;
	}

	return (uint8)ret_min;
}
static void MSC_TemperatureSelection_100ms(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/*inputs*/
	OBC_ChargingMode chargingMode;
	IdtOutputTempMeas tempAC1;
	IdtOutputTempMeas tempAC2;
	IdtOutputTempMeas tempAC3;
	IdtOutputTempMeas tempACN;
	IdtOutputTempMeas tempDC1;
	IdtOutputTempMeas tempDC2;
	/*Outputs*/
	uint16 tempL;
	uint16 tempN;


	(void)Rte_Read_PpInt_OBC_ChargingMode_OBC_ChargingMode(&chargingMode);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC1Meas(&tempAC1);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC2Meas(&tempAC2);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempAC3Meas(&tempAC3);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempACNMeas(&tempACN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempDC1Meas(&tempDC1);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpOutputTempMeas_DeOutputTempDC2Meas(&tempDC2);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if((uint8)Cx3_Euro_fast_charging==chargingMode)
	{
		tempL=tempDC1;
		tempN=tempDC2;
	}
	else
	{
		tempN=tempACN;
		tempL=MSC_getMaxTemperature(tempAC1,tempAC2,tempAC3);
	}

	if((uint16)MSC_SNA_U16!=tempL)
	{
		tempL = (tempL/MSC_TEMP_GAIN) + MSC_TEMP_OFFSET;
		if((uint16)MSC_SNA_U8<tempL)
		{
			tempL=(uint16)MSC_SNA_U8;
		}
		(void)Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempL((OBC_SocketTempL)tempL);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}
	if((uint16)MSC_SNA_U16!=tempN)
	{
		tempN = (tempN/MSC_TEMP_GAIN) + MSC_TEMP_OFFSET;
		if((uint16)MSC_SNA_U8<tempN)
		{
			tempN=(uint16)MSC_SNA_U8;
		}
		(void)Rte_Write_PpInt_OBC_SocketTemp_OBC_SocketTempN((OBC_SocketTempN)tempN);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	}

}
static uint16 MSC_getMaxTemperature(IdtOutputTempMeas tempAC1,IdtOutputTempMeas tempAC2,IdtOutputTempMeas tempAC3)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint16 RetValue;


	RetValue=tempAC1;
	if(tempAC2>RetValue)
	{
		RetValue=tempAC2;
	}
	if(tempAC3>RetValue)
	{
		RetValue=tempAC3;
	}
	return RetValue;
}
static void MSC_UpdateCalibratables(void)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	/* Automatic variables HERE... */


	MSC_DCRelayVoltage_threshold=(uint8)Rte_Prm_PpInputVoltageThreshold_DeInputVoltageThreshold();
	MSC_obcPowerMax_cal=(uint8)Rte_CData_CalOBC_PowerMaxValue();
}

static uint8 MSC_Power_filter(uint8 input)
{

	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint8 output;


	PowerSlow_FilteredValue = (uint16)((((uint32)PowerSlow_FilteredValue*(uint32)MSC_FILTER_GAIN) + ((((uint32)1U<<MSC_FILTER_Q)-(uint32)MSC_FILTER_GAIN)*((uint32)input<<MSC_FILTER_Q)))>>MSC_FILTER_Q);
	output = (uint8)((PowerSlow_FilteredValue+((uint16)1U<<(MSC_FILTER_Q-1U)))>>MSC_FILTER_Q);
	return output;
}

void MSC_SaveDonePowerLatch(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
    
	/* Flash not initialized: First execution */
	*Rte_Pim_PimMSC_PowerLatchFlag() = MSC_POWER_LATCH_DONE;

	/* Immediate write into NVM */
	(void)Rte_Call_NvMService_AC3_SRBS_NvMSCBlockNeedPowerLatchFlag_WriteBlock(Rte_Pim_PimMSC_PowerLatchFlag());/* PRQA S 3110, 3417, 0315, 3426 #RTE read/write confirmation not used*/
}

static void MSC_DCDC_OBCDCDCRTPowerLoad(void)
{
	/* - Static non-init variables declaration ---------- */
	#define CtApMSC_START_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static non-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static zero-init variables declaration --------- */
	#define CtApMSC_START_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static zero-init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_ZERO_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Static init variables declaration -------------- */
	#define CtApMSC_START_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

	/* Static init variables HERE... */

	#define CtApMSC_STOP_SEC_VAR_INIT_UNSPECIFIED
	#include "CtApMSC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


	/* - Automatic variables declaration ---------------- */
	uint32 output_value;
	DCLV_Applied_Derating_Factor DCLV_Applied_Derating_Factor_Var;
	DCLV_Power DCLV_Power_Var;
	DCDC_OBCDCDCRTPowerLoad DCDC_OBCDCDCRTPowerLoad_Var;

	(void)Rte_Read_PpInt_DCLV_Applied_Derating_Factor_DCLV_Applied_Derating_Factor(&DCLV_Applied_Derating_Factor_Var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
	(void)Rte_Read_PpInt_DCLV_Power_DCLV_Power(&DCLV_Power_Var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/


	if (0U == DCLV_Applied_Derating_Factor_Var)
	{
		/* Avoid division by 0. Set output directly to 0 */
		output_value = 0U;
	}
	else
	{
		/* Normal operation */
		/* uint32 checked to be wide enough to store the output of the calculations */
		output_value =
				(uint32)((uint32)DCLV_Power_Var * (uint32)100U * (uint32)0x400)
				/
				(uint32)((uint32)MSC_MAX_DCDC_POWER_ABSOLUTE * (uint32)DCLV_Applied_Derating_Factor_Var);
	}

	/* Truncate if above 100 % */
	if (output_value > 100U)
	{
		output_value = 100U;
	}

	/* Assign to final variable */
	DCDC_OBCDCDCRTPowerLoad_Var = (DCDC_OBCDCDCRTPowerLoad)output_value;

	(void)Rte_Write_PpInt_DCDC_OBCDCDCRTPowerLoad_DCDC_OBCDCDCRTPowerLoad(DCDC_OBCDCDCRTPowerLoad_Var);/* PRQA S 3110, 3417, 3426 #RTE read/write confirmation not used*/
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
/* PRQA S 3214 --*/
/*Doxigen END*/
/*!
 * \}
 */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2012 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0624:  MISRA rule: Rule8.3
     Reason:     This MISRA violation is a consequence from the RTE requirements [SWS_Rte_01007] [SWS_Rte_01150].
                 The typedefs are never used in the same context.
     Risk:       No functional risk. Only a cast to uint8* is performed.
     Prevention: Not required.

   MD_Rte_3206:  MISRA rule: Rule2.7
     Reason:     The parameter are not used by the code in all possible code variants.
     Risk:       No functional risk.
     Prevention: Not required.

*/
