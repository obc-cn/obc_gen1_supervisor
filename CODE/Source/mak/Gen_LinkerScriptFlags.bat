@echo off


echo *
echo *********************************************************
echo *************** Gen LinkerScript Flags ******************
echo *********************************************************
echo *


rem *                                                        *
rem * Execute actions                                        *
rem *                                                        *
echo * Checking APP_HW_VERSION flag being compiled...
rem Read HW_VERSION being compiled
for /f "tokens=3 delims=	,	" %%i in ('%WINDIR%\system32\findstr.exe /b /c:"#define		APP_HW_VERSION" %~dp0\..\AppSW\AppExternals.h') do (
    set hw_version=%%i
)
echo *    - APP_HW_VERSION: %hw_version%
echo * 

rem Selecting te correct mem_flag to apply
set extendedmem_flag=UNKNOWN
if %hw_version% == APP_SAMPLES_D2_1_NO_XCP set extendedmem_flag=APP_EXTENDEDMEM
if %hw_version% == APP_SAMPLES_D2_1 set extendedmem_flag=APP_EXTENDEDMEM
if %hw_version% == APP_SAMPLES_D1_2 set extendedmem_flag=APP_STANDARDDMEM
if %hw_version% == APP_SAMPLES_D1_1 set extendedmem_flag=APP_STANDARDDMEM
if %hw_version% == APP_SAMPLES_D1_1_NO_LED set extendedmem_flag=APP_STANDARDDMEM
if %hw_version% == APP_SAMPLES_D1_NO_PLC set extendedmem_flag=APP_STANDARDDMEM
if %hw_version% == APP_SAMPLES_D1 set extendedmem_flag=APP_STANDARDDMEM
if %hw_version% == APP_SAMPLES_C1 set extendedmem_flag=APP_STANDARDDMEM
if %extendedmem_flag% == UNKNOWN goto :END_ERR_HWVERSION

echo * Generate LinkerScript's flag header file as per APP_HW_VERSION needs...
echo *
rem Generating LinkerScript's tags header file
echo #define %extendedmem_flag% > %~dp0\..\BaseSW\microsar\LinkerScriptFlags.h
if not %ERRORLEVEL% == 0 goto END_NOK
goto END_OK


rem *                                                        *
rem * End batch script                                       *
rem *                                                        *
:END_OK
echo * OK: LinkerScript file updated successully! :)
echo *
exit /b %ERRORLEVEL%

:END_NOK
echo * ERR: LinkerScript file processing failed
echo *
exit /b %ERRORLEVEL%
:END_ERR_HWVERSION
echo * ERR: HW Version setted not found
echo *      Hint: please, update Upd_LinkerScriptFlags.bat for defining the HW Version needed
echo *
