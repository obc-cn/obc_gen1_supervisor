#######################################################################################################################
# File Name  : Makefile.AURIX.HIGHTEC.ALL.make                                                                        #
# Description: Linker/Symbol Preprocessor command file generation                                                     #
# Project    : Vector Basic Runtime System                                                                            #
# Module     : BrsHw for Platform Infineon Aurix                                                                      #
#              and Compiler Hightec GNU                                                                               #
#                                                                                                                     #
#---------------------------------------------------------------------------------------------------------------------#
# COPYRIGHT                                                                                                           #
#---------------------------------------------------------------------------------------------------------------------#
# Copyright (c) 2019 by Vector Informatik GmbH.                                                  All rights reserved. #
#                                                                                                                     #
#---------------------------------------------------------------------------------------------------------------------#
# AUTHOR IDENTITY                                                                                                     #
#---------------------------------------------------------------------------------------------------------------------#
# Name                          Initials      Company                                                                 #
# ----------------------------  ------------  ------------------------------------------------------------------------#
# Benjamin Walter               visbwa        Vector Informatik GmbH                                                  #
# Roland Reinl                  virrro        Vector Informatik GmbH                                                  #
# Sascha Mauser                 vismaa        Vector Informatik GmbH                                                  #
#---------------------------------------------------------------------------------------------------------------------#
# REVISION HISTORY                                                                                                    #
#---------------------------------------------------------------------------------------------------------------------#
# Version   Date        Author  Description                                                                           #
# --------  ----------  ------  --------------------------------------------------------------------------------------#
# 01.00.00  2016-08-25  visbwa  Initial creation to separate OsCoreGen7 from Default UseCase                          #
# 01.00.01  2016-12-22  visbwa  Review according to Brs_Template 2.00.03                                              #
# 01.00.02  2017-02-23  virrro  Moved OsGen7-specific usage of USER_LINKER_COMMAND_FILE from Makefile to              #
#                               Makefile.AURIX.GNU.ALL.make                                                           #
# 01.00.03  2017-07-27  virrro  Added CC_ENV to OsGen7 link rule, as license information necessary for generation,    #
#                               added USER_LINKER_COMMAND_FILE to dependent files list                                #
# 02.00.00  2018-11-15  vismaa  Renamed GNU to HIGHTEC                                                                #
# 02.00.01  2019-02-14  virrro  Added LinkerScriptDefault to dependent files list of binary                           #
# 02.01.00  2019-02-20  virrro  Introduced LINKER_SCRIPT_TEMPLATE and changed linkage to preprocessed file            #
#######################################################################################################################

##########################################################################################################
# LINKER FILE GENERATION
##########################################################################################################

ifeq ($(OS_USECASE),OSGEN7)
else
LINKER_SCRIPT_TEMPLATE = Makefile.AURIX.HIGHTEC.ALL.LinkerScriptDefault
endif

# virrro: Additional dependency for linking. Independent of the current configuration, a linker script is required.
# This rule ensures that the linker script will be created before starting the link process.
$(PROJECT_NAME).$(BINARY_SUFFIX): $(PROJECT_NAME).$(LNK_SUFFIX)

#------------------------------------------------------------------------------
# Rule to generate linker command file (listing of all dependent files)
#------------------------------------------------------------------------------
$(PROJECT_NAME).$(LNK_SUFFIX): Makefile \
                               Makefile.config \
                               Makefile.project.part.defines \
                               Makefile.AppSW \
                               $(USER_LINKER_COMMAND_FILE) \
                               Makefile.$(PLATFORM).$(COMPILER_MANUFACTURER).$(EMULATOR).make \
                               $(LINKER_SCRIPT_TEMPLATE) \
                               ..\AppSW\AppExternals.h
ifeq ($(OS_USECASE),OSGEN7)
	if ./Gen_LinkerScriptFlags.bat; then  else exit 1; fi;  \
	$(subst \,/,$(CC_ENV)) && $(subst \,/,$(COMPILER_BIN)\tricore-gcc.exe -E -P -C $(USER_LINKER_COMMAND_FILE) -o $(PROJECT_NAME).$(LNK_SUFFIX) -I $(GENDATA_DIR))
else
	$(subst \,/,$(CC_ENV)) && $(subst \,/,$(COMPILER_BIN)\tricore-cpp.exe -E -P -C $(LINKER_SCRIPT_TEMPLATE) -o $@)
endif

#----------------------------------------------------------------------------------------------------------
# Rule to generate Motorola S-Record file
#----------------------------------------------------------------------------------------------------------
$(PROJECT_NAME).srec: $(PROJECT_NAME).$(BINARY_SUFFIX)
	@$(ECHO) Converting to Motorola S-Record...;                                                                                  \
    $(subst \,/,$(COMPILER_BIN)\tricore-objcopy.exe) -Ielf32-tricore -Osrec $(PROJECT_NAME).$(BINARY_SUFFIX) $(PROJECT_NAME).srec; \
    $(ECHO) ...done!

#----------------------------------------------------------------------------------------------------------
# Rule to generate Intel HEX file
#----------------------------------------------------------------------------------------------------------
$(PROJECT_NAME).hex: $(PROJECT_NAME).$(BINARY_SUFFIX)
	@$(ECHO) Converting to Intel HEX...;                                                                                           \
    $(subst \,/,$(COMPILER_BIN)\tricore-objcopy.exe) -Ielf32-tricore -Oihex $(PROJECT_NAME).$(BINARY_SUFFIX) $(PROJECT_NAME).hex;   \
    $(ECHO) ...done!

#----------------------------------------------------------------------------------------------------------
# Rules to generate Bootloadables S-Record files
#----------------------------------------------------------------------------------------------------------
GenBtlFiles: $(PROJECT_NAME).srec
	@$(ECHO) Generating Bootloadable S-Record files...; \
	if ../../Utils/bootloader/scripts/bootlodeablefiles_gen.bat $(PROJECT_NAME).srec $(PROJECT_NAME)_SUP_4CAL.srec $(PROJECT_NAME)_CAL.srec $(PROJECT_NAME)_SUP_4ULP.srec; then  else exit 1; fi;   \
    $(ECHO) ...done!

#----------------------------------------------------------------------------------------------------------
# Rules to generate EOL support HEX files
#----------------------------------------------------------------------------------------------------------
GenEolSupFiles: GenBtlFiles
	@$(ECHO) Generating EOL support HEX file...; \
	if ../../Utils/bootloader/scripts/fbl_app_gen.bat; then  else exit 1; fi;   \
    $(ECHO) ...done!

#----------------------------------------------------------------------------------------------------------
# Rule to generate BuidInfo file
#----------------------------------------------------------------------------------------------------------
$(PROJECT_NAME)_BuildInfo.rst: $(PROJECT_NAME).$(BINARY_SUFFIX)
	@$(ECHO) Generating BuildInfo file...;                                                                  \
	if ../../Utils/buildinfo_gen/buildinfo_gen.bat $(PROJECT_NAME)_BuildInfo.rst; then  else exit 1; fi;    \
    $(ECHO) ...done!

#End of Makefile.AURIX.HIGHTEC.ALL.make
